function update_comment_status(realtask_id){

	$("textarea[data-textarea-id="+realtask_id+"]").val('');

	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: '/task/updateStatus',
		data: ({task_id : realtask_id, filter_cat: $('#Task_filter_cat').val(), filter_status:$('#Task_filter_status').val(), filter_worker:$('#Task_filter_worker').val() }),
		success: function(data){
			console.log(data);
			$('#view-task-'+realtask_id+' #open-close-comment-form span').text( data.comment_count );
			
			$("#task-filter").html( data.filter_updater );
			
			switch(data.new_status) {
				case '2':
					$('#view-task-'+realtask_id+' #task-status-label').removeClass();
					$('#view-task-'+realtask_id+' #task-status-label').addClass('label label-warning');
					$('#view-task-'+realtask_id+' #task-status-label').text('В работе');
					break;
				case '3':
					$('#view-task-'+realtask_id+' #task-status-label').removeClass();
					$('#view-task-'+realtask_id+' #task-status-label').addClass('label label-info');
					$('#view-task-'+realtask_id+' #task-status-label').text('На проверку');
					break;
				case '4':
					$('#view-task-'+realtask_id+' #task-status-label').removeClass();
					$('#view-task-'+realtask_id+' #task-status-label').addClass('label label-danger');
					$('#view-task-'+realtask_id+' #task-status-label').text('Отложена');
					break;
				case '5':
					$('#view-task-'+realtask_id+' #task-status-label').removeClass();
					$('#view-task-'+realtask_id+' #task-status-label').addClass('label label-success');
					$('#view-task-'+realtask_id+' #task-status-label').text('Выполнена');
					break;
					
			} 
		}
	});
	
	$.ajax({
		type: 'POST',
		url: '/task/buttonStatusOnComment',
		data: ({task_id : realtask_id }),
		success: function(data){
		
			$('div[data-status-update='+realtask_id+']').html(data);
		} 
	});
}
function filter_update(){

	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: '/task/filterUpdate',
		data: ({ filter_cat: $('#Task_filter_cat').val(), filter_status:$('#Task_filter_status').val(), filter_worker:$('#Task_filter_worker').val(), filter_priority:$('#Task_filter_priority').val() }),
		success: function(json){
			if( json.success ==1 ){
				$("#task-filter").html( json.filter_updater );
			}
		}
	});
}

function update_button_status( task_id, status ){


			switch( status ) {
				case 2:
					$('#view-task-'+task_id+' #task-status-label').removeClass();
					$('#view-task-'+task_id+' #task-status-label').addClass('label label-warning');
					$('#view-task-'+task_id+' #task-status-label').text('В работе');
					break;
				case 3:
					$('#view-task-'+task_id+' #task-status-label').removeClass();
					$('#view-task-'+task_id+' #task-status-label').addClass('label label-info');
					$('#view-task-'+task_id+' #task-status-label').text('На проверку');
					break;
				case 4:
					$('#view-task-'+task_id+' #task-status-label').removeClass();
					$('#view-task-'+task_id+' #task-status-label').addClass('label label-danger');
					$('#view-task-'+task_id+' #task-status-label').text('Отложена');
					break;
				case 5:
					$('#view-task-'+task_id+' #task-status-label').removeClass();
					$('#view-task-'+task_id+' #task-status-label').addClass('label label-success');
					$('#view-task-'+task_id+' #task-status-label').text('Выполнена');
					break;
					
			} 

}

function manual_update_status_button( data_update, task_id ){

	var myRe = /data-task="([0-9]+)"/g;
	var myArray = myRe.exec(data_update);
	$('#button-status-on-comment'+myArray[1]).html(data_update);

}

jQuery(document).ready(function($) {


	jQuery(document).click(function(e) {
		$("#user-created-msg").hide(1000);
	});
	

	$('#open-close-create-task-form').click(function(e) {
		e.preventDefault();
		if( $(this).attr('data-show') == 0  ){
			$('#create-task-form-id').slideDown();
			$(this).text('Скрыть форму');
			$(this).attr('data-show', 1);
		}else{
			$('#create-task-form-id').slideUp();
			$(this).text('Создать задачу');
			$(this).attr('data-show', 0);
		}
	});
	
	

$('body').on('click','a#open-close-comment-form',function(e){
		e.preventDefault();
		
		if( $(this).attr('data-show') == 0  ){
			 $(this).next('div').next('div').slideDown();
			$(this).attr('data-show', 1);
		}else{
			 $(this).next('div').next('div').slideUp();
			$(this).attr('data-show', 0);
		}
	});
	
	
$('body').on('click','a#buttons-update',function(e){
		e.preventDefault();
		
		var current_task_id =  $(this).attr('data-task') ;

		
		$.ajax({
		type: 'POST',
		dataType: 'json',
		url: '/task/statusOnButton',
		data: ({task_id : $(this).attr("data-task"), status:$(this).attr("data-status"), filter_cat: $('#Task_filter_cat').val(), filter_status:$('#Task_filter_status').val(), filter_worker:$('#Task_filter_worker').val() }),
		success: function(json){
			if( json.success ==1 ){
				$('div[data-status-update='+current_task_id+']').html(json.render_status);
				$("#task-filter").html( json.filter_updater );
				$('#comment-list-'+current_task_id).html( json.comment_update );
			}
		}
	});
	
	
});

$('body').on('click','a#delete-task-comments',function(e){
	e.preventDefault();
	var current_task_id =  $(this).attr('data-task-id') ;
	
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: '/task/deleteWithComments',
		data: ({task_id : current_task_id, filter_cat: $('#Task_filter_cat').val(), filter_status:$('#Task_filter_status').val(), filter_worker:$('#Task_filter_worker').val() }),
		success: function(data){
			$("#task-filter").html( data.filter_updater );
			$("#data").html( data.ajax_task_list );
		}
	});
	

});


$('body').on('click','a#filter-all',function(e){
	e.preventDefault();

	$.ajax({
		type: 'POST',
		dataType: 'html',
		url: '/task/updateAjaxTaskList',
		data: ({ cat_id : 0, status: $("#statuses.active").attr("data-status") }),
		success: function(data){
			$('#data').html(data);
		}
	});
	

});

$('body').on('mouseenter mouseleave','div#allow-delete-comment',function(e){


	if( e.type == 'mouseenter'){
	
		
		$('a[data-comment-id='+$(this).attr('data-comment-id') +']').show();
		
	}else{
	
		$('a[data-comment-id='+$(this).attr('data-comment-id') +']').hide();
		
	}
	

});


$('body').on('click','a#in-view-filter-worker',function(e){
	e.preventDefault();
	
	$("a[data-worker-id="+$(this).attr("worker-id")+"]").trigger( 'click' );

});


$('body').on('click','a#in-view-filter-cat',function(e){
	e.preventDefault();
	
	$("a[data-cat-id="+$(this).attr("cat-id")+"]").trigger( 'click' );

});


$('body').on('click','a.comment-deleter',function(e){
	e.preventDefault();
	
	var task_id = $(this).attr('data-task-id');
	
	$.ajax({
		type: 'POST',
		dataType: 'html',
		url: '/task/comment/ajaxDelete',
		data: ({ comment_id : $(this).attr('data-comment-id') }),
		success: function(data){
			
			$('#comment-list-'+task_id).html(data);
		}
	});

});


$('body').on('click','a.worker-change-trigger',function(e){
	e.preventDefault();
	
	$('#worker-change-'+$(this).attr('data-task-id')).trigger( 'dblclick' );

});


$('body').on('click','input.submit-comment',function(e){
	e.preventDefault();
	
	var data_task = $(this).attr('data-task');
	
	$.ajax({
		type:'POST',
		url:'/task/comment/ajaxCreate',
		cache:false,
		data:$(this).parents("form").serialize(),
		success:function(html){
		
			$("#comment-list-"+data_task).html(html);
			
		}
	});

		
		

});

$('body').on('click','span#priority-val',function(e){
	e.preventDefault();

	$("a[data-priority-id="+$(this).text()+"]").trigger( 'click' );

});

$('body').on('dblclick','div.edit_description',function(e){
	e.preventDefault();
	
	var taskid = $(this).attr('data-task-edit-id');
	
	$.ajax({
		type:'POST',
		'dataType':'json',
		url:'/task/getTaskEditForm',
		data:({ task_id : taskid }),
		success:function(html){
		
			$('div[data-task-edit-id='+taskid+']').html(html.edit_form);
		}
	});

});


jQuery('body').on('click','#ajax-edit-form',function(){

	var task_id = $(this).attr('data-task-id');

	jQuery.ajax({
		'type':'POST',
		'url':'/task/editTask',
		'cache':false,
		'data':jQuery(this).parents("form").serialize(),
		'dataType':'json',
		'success':function(json){
			if( json.success == 1 ) {
				
				$('.task-header'+task_id).html( json.task_head );
				$('div[data-task-edit-id='+task_id+']').html( json.task_desc );
				$('.worker-list'+task_id).html( json.task_worker );

			}else{
			
				if (typeof json.Task_name != "undefined") {
					$("#Task_edit_name_em_"+task_id).text( json.Task_name );
				}else $("#Task_edit_name_em_"+task_id).text("" );
				
				if (typeof json.Task_description != "undefined") {
					$("#Task_edit_description_em_"+task_id).text( json.Task_description );
				}else $("#Task_edit_description_em_"+task_id).text("");
			}
		},
	});
		
	return false;
});

jQuery('body').on('click','#cancel-ajax-edit-form',function(e){
	e.preventDefault();
	var task_id = $(this).attr('data-task-id');

	jQuery.ajax({
		'type':'POST',
		'url':'/task/getTaskDesc',
		'cache':false,
		'data':({ task_id : task_id }),
		'success':function(html){
			$('div[data-task-edit-id='+task_id+']').html( html );
		},
	});
		

});

jQuery('body').on('click','#ajax-creat-form',function(){jQuery.ajax({'dataType':'json','success':function(json){
				if( json.success == 1 ) {
					
					$("#task-filter").html( json.filter_updater );
					$("#data").html( json.ajax_task_list );
					//$("#user-created-msg").show(1000);
					$("#Task_name").val("");
					$("#Task_name").val("");
					$("#Task_description").val("");
					$("#Task_name_em_").text("");
					$("#Task_description_em_").text("");
					$("#open-close-create-task-form").trigger( "click" );
				}else{
					if (typeof json.Task_name != "undefined") {
						$("#Task_name_em_").text( json.Task_name );
					}else $("#Task_name_em_").text("" );
					if (typeof json.Task_description != "undefined") {
						$("#Task_description_em_").text( json.Task_description );
					}else $("#Task_description_em_").text("");
				}
			},'type':'POST','url':'/task/ajaxCreate','cache':false,'data':jQuery(this).parents("form").serialize()});return false;});

jQuery('body').on('click','a.read-response',function(e){
	e.preventDefault();
	var task_id = $(this).attr('data-task-id');
	var this_response_a = $(this);

	jQuery.ajax({
		'type':'POST',
		'url':'/task/readResponse',
		'cache':false,
		'data':({ task_id : task_id }),
		'dataType':'json',
		'success':function(json){
			console.log( json );
			this_response_a.remove();
			$('.task-header'+task_id).removeClass('task-header');
			$("#task-filter").html( json.filter_updater );
		},
	});
		

});

});