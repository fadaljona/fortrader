<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Task Manager',
	'language' => 'ru',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.controllers.base.*',
		'ext.mail.YiiMailMessage',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		/*'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'admin',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),*/
		
	),

	// application components
	'components'=>array(
		
		'session' => array (
			'sessionName' => 'PHPSESSID',
			'class'=> 'CDbHttpSession',
			'autoCreateSessionTable'=> true,
			'sessionTableName' => 'taskbook_session',
			'connectionID' => 'db',
			'cookieMode' => 'allow',
			'timeout' => 3600*24*30,
			'cookieParams' => array(
				'lifetime' => 3600*24*30,
			),
		),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'autoRenewCookie' => true,
			'class' => 'WebUser',
		),
		'format'=>array(
			'class'=>'application.extensions.timeago.TimeagoFormatter',
		),
		/*'mail' => array(
			'class' => 'ext.mail.YiiMail',
			'transportType' => 'smtp',
			'transportOptions' => array(
				'host' => 'smtp.gmail.com',
				'username' => 'budko.alex.mail@gmail.com',
				'password' => '^J!I5gw0',
				'port' => '465',
				'encryption'=>'ssl',

			),
			'viewPath' => 'application.views.mail',
			'logging' => true,
			'dryRun' => false
		),*/

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName' => false,
			'rules'=>array(
				'<action:\w+>' => 'task/<action>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		'authManager' => array(
			// Будем использовать свой менеджер авторизации
			'class' => 'PhpAuthManager',
			// Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
			'defaultRoles' => array('guest'),
		),

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		/*'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'class'=>'CProfileLogRoute',
					'levels'=>'error, warning, profile, info, trace',
					//'categories'=>'*',
					//'report'=>'summary',
					'enabled'=>true,
				),
				
				array(
					'class'=>'CWebLogRoute',
				),
				
			),
		),*/

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
