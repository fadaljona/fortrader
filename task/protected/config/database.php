<?php


// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database
	
	'connectionString' => 'mysql:host='.DB_HOST.';dbname='.DB_NAME,
	'emulatePrepare' => true,
	'username' => DB_USER,
	'password' => DB_PASSWORD,
	'charset' => 'utf8',
	'enableProfiling'=>true,
	'enableParamLogging' => true,
	
);