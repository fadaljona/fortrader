<?php

class TaskController extends BaseController{
	public function filters(){
		return array(
			'accessControl',
			'postOnly + delete',
		);
	}
	public function accessRules()
	{
		return array(
			array('allow', 'actions'=>array('sendmail'), 'users'=>array('*') ),
			array(
				'allow', 
				'actions' => array(
					'index',
					'readResponse',
					'view', 
					'updateStatus',
					'buttonStatusOnComment', 
					'statusOnButton',
					'updateAjaxTaskList',
					'filterUpdate', 
					'getTaskEditForm', 
					'getTaskDesc',
					'search'
				),
				'roles' => array('employee'),
			),
			array('allow', 'actions' => array('create','ajaxCreate','editTask'), 'roles'=>array('employee') ),
			array('allow', 'actions' => array('admin','delete','deleteWithComments'), 'roles'=>array('administrator') ),
			array('deny', 'users'=>array('*') ),
		);
	}

	public function loadModel($id){
		$model=Task::model()->findByPk($id);
		if($model===null){
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='task-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
