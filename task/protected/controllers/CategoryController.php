<?php

class CategoryController extends BaseController{
	public function filters(){
		return array(
			'accessControl',
			'postOnly + delete',
		);
	}
	public function accessRules(){
		return array(
			array('allow', 'actions' => array('index','view'), 'roles' => array('administrator') ),
			array(
				'allow', 
				'actions' => array('create','update','ajaxCreate', 'activateDeactivate'),
				'roles' => array('administrator'),
			),
			array('allow', 'actions'=>array('admin','delete'), 'roles'=>array('administrator') ),
			array('deny', 'users'=>array('*') ),
		);
	}
	public function loadModel($id){
		$model=Category::model()->findByPk($id);
		if($model===null){
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='category-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
