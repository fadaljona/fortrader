<?php
	final class AdminAction extends BaseAction {
		function run() {
			$model=new Comment('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['Comment']))
				$model->attributes=$_GET['Comment'];

			$this->controller->render('admin',array(
				'model'=>$model,
			));
		}
	}
?>
