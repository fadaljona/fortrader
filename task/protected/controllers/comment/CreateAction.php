<?php
	final class CreateAction extends BaseAction {
		function run() {
			$model=new Comment;

			if(isset($_POST['Comment']))
			{
				$model->attributes=$_POST['Comment'];
				if($model->save())
					$this->controller->redirect(array('view','id'=>$model->comment_id));
			}

			$this->controller->render('create',array(
				'model'=>$model,
			));
		}
	}
?>
