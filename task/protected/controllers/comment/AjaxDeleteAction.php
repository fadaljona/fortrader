<?php
	final class AjaxDeleteAction extends BaseAction {
		function run() {
			$comment = $this->controller->loadModel($_POST['comment_id']);
	
			if( Yii::app()->user->id == $comment->user->id || Yii::app()->user->role == 'administrator' ){
			
				$comment->delete();

			}
			
			$dataProvider=new CActiveDataProvider('Comment', array(
						'criteria'=>array(
							'condition'=> "( task_id = ".$comment->task_id.")",
							'with'=>array( 'user'),
							'order'=>'t.created_date DESC',
							),
						'pagination'=>array(
												'pageSize'=>500,
										),
						)
						);
						$this->controller->renderPartial('_update_ajax_comments_list', array( 'dataProvider' => $dataProvider, 'task_id' => $comment->task_id ) );
		}
	}
?>
