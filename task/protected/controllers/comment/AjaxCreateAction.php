<?php
	final class AjaxCreateAction extends BaseAction {
		function run() {
			$model=new Comment;

			// Uncomment the following line if AJAX validation is needed
			//$this->controller->performAjaxValidation($model);

			if(isset($_POST['Comment']))
			{
				$model->attributes=$_POST['Comment'];
				$model->created_date = date("Y-m-d H:i:s");
				if($model->save()){
				
					$task_model=Task::model()->findByPk($model->task_id);
					
					Task::sendTaskEmail($task_model, 3, '', $model);
								
					
				
					
					if( $task_model->worker_id != Yii::app()->user->id ){
						if( $task_model->status ==1 ){
							$task_model->status = 2;
							$task_model->save();
							
							Task::sendTaskEmail($task_model, 1, 'В работе', 0);
							
							
						}
					}
					/*	
					$dataProvider=new CActiveDataProvider('Task', array(
					'criteria'=>array(
							'condition'=> "( t.task_id = ".$model->task_id.")",
							'with'=>array('category', 'manager', 'worker', 'comment', 'comment.user'),
							'order'=>'t.created_date DESC',
						),
					)
					);
					$this->controller->renderPartial('_update_ajax_comments_list', array( 'dataProvider' => $dataProvider ), false, true );
						*/
						
					
						$dataProvider=new CActiveDataProvider('Comment', array(
						'criteria'=>array(
							'condition'=> "( task_id = ".$model->task_id.")",
							'with'=>array( 'user'),
							'order'=>'t.created_date DESC',
							),
						'pagination'=>array(
												'pageSize'=>500,
										),
						)
						);
						$this->controller->renderPartial('_update_ajax_comments_list', array( 'dataProvider' => $dataProvider, 'task_id' => $model->task_id ) );
					}else{
					
						$dataProvider=new CActiveDataProvider('Comment', array(
						'criteria'=>array(
							'condition'=> "( task_id = ".$model->task_id.")",
							'with'=>array( 'user'),
							'order'=>'t.created_date DESC',
							),
						'pagination'=>array(
												'pageSize'=>500,
										),
						)
						);
						$this->controller->renderPartial('_update_ajax_comments_list', array( 'dataProvider' => $dataProvider, 'task_id' => $model->task_id ) );
					}
				
			}
		}
	}
?>
