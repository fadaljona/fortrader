<?php
	final class UpdateAction extends BaseAction {
		function run($id) {
			$model=$this->controller->loadModel($id);

			// Uncomment the following line if AJAX validation is needed
			// $this->controller->performAjaxValidation($model);

			if(isset($_POST['Comment']))
			{
				$model->attributes=$_POST['Comment'];
				if($model->save())
					$this->controller->redirect(array('view','id'=>$model->comment_id));
			}

			$this->controller->render('update',array(
				'model'=>$model,
			));
		}
	}
?>
