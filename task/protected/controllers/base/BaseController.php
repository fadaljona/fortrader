<?php
class BaseController extends CController {
	public $layout='//layouts/column1';
	public $menu=array();
	public $breadcrumbs=array();
	
	public function init()
	{
		if( is_user_logged_in() && (! isset( Yii::app()->user->id)) ){

			$current_user = wp_get_current_user();
			$identity = new UserIdentity( $current_user->user_login , $current_user->user_pass);
			$identity->authenticate();
			Yii::app()->user->login($identity, 3600*24*30);

		}
	}
	protected function resolveClassName( $class ) {
		return $class ? $class : get_class( $this );
	}
	function getCleanClassName( $class = null ) {
		$class = $this->resolveClassName( $class );
		$class = preg_replace( "#Controller$#", "", $class );
		$class[0] = strtolower($class[0]);
		return $class;
	}
	function actions() {
		$class = $this->getCleanClassName();
		$alias = "application.controllers.{$class}";
		return $this->getActions( $alias );
	}
	protected function getActions( $alias ) {
		$actions = Array();
		Yii::import( "{$alias}.*" );
		$path = Yii::getPathOfAlias( $alias );
		$files = glob( "{$path}/*Action.php", GLOB_NOSORT );
		if( $files ) {
			foreach( $files as $file ) {
				$fileName = basename( $file );
				$className = preg_replace( "#\.php$#i", "", $fileName );
				$clearClassName = BaseAction::getActionCleanClassName( $className );
				$actions[ $clearClassName ] = $className;
			}
		}
		return $actions;
	}
}
?>