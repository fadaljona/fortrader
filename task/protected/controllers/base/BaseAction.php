<?php
class BaseAction extends CAction {
	static function getActionCleanClassName( $class ) {
		$class = preg_replace( "#Action$#", "", $class );
		$class[0] = strtolower($class[0]);
		return $class;
	}
}
?>