<?php
	final class UpdateStatusAction extends BaseAction {
		function run() {
			$comment_count = Yii::app()->db->createCommand()
				->select('COUNT( comment_id ) as count')
				->from('taskbook_comment') 
				->where( "task_id = ".$_POST['task_id'] )
				->queryAll(); 
							
			$new_status = Yii::app()->db->createCommand()
				->select('status')
				->from('taskbook_task')  
				->where( "task_id = ".$_POST['task_id'] )
				->queryAll(); 
							
			$dataForFilter = Task::getFilterData($_POST['filter_status'], $_POST['filter_cat'], $_POST['filter_worker'], $_POST['filter_priority']);
			$filter_updater = $this->controller->renderPartial(
				'_task_filter', 
				array( 
					'dataForFilter' => $dataForFilter 
				),
				true 
			);
							
			echo json_encode(array('comment_count'=> $comment_count[0]['count'], 'new_status'=>$new_status[0]['status'], 'filter_updater' => $filter_updater ));
		}
	}
?>
