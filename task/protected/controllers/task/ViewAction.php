<?php
	final class ViewAction extends BaseAction {
		function run($id) {
			$dataProvider=new CActiveDataProvider(
				'Task', 
				array(
					'criteria'=>array(
						'condition'=> 't.task_id = '.$id,
						'with'=>array('category', 'manager', 'worker', 'comment', 'comment.user'),
						'order'=>'t.created_date DESC',
					)
				)
			);
			$this->controller->render(
				'view',
				array(
					'model'=>$this->controller->loadModel($id),
					'dataProvider' => $dataProvider,
				)
			);
		}
	}
?>
