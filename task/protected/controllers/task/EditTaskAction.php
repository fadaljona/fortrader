<?php
	final class EditTaskAction extends BaseAction {
		function run() {
			if(isset($_POST['Task'])){
				$model = $this->controller->loadModel( $_POST['Task']['task_id'] );
				$model->cat_id = $_POST['Task']['cat_id'];
				$model->description = $_POST['Task']['description'];
				$model->manager_id = $_POST['Task']['manager_id'];
				$model->name = $_POST['Task']['name'];
				$model->priority = $_POST['Task']['priority'];
				$model->worker_id = $_POST['Task']['worker_id'];
				$model->status = $_POST['Task']['status'];

				if($model->save()){
				
					$task_header = array(
						'task' => Task::objToArray($model),
						'category' => Task::objToArray( Category::model()->findByPk($model->cat_id) ),
					);
					$task_head = $this->controller->renderPartial(
						'_view_task_header', 
						array( 
							'task_header' => $task_header 
						), 
						true
					);
					
					$task_workers = array(
						'task' => Task::objToArray($model),
						'manager' => Task::objToArray( User::model()->findByPk($model->manager_id) ),
						'worker' => Task::objToArray( User::model()->findByPk($model->worker_id) ),
					);
					$task_worker = $this->controller->renderPartial(
						'_view_task_workers', 
						array( 
							'task_workers' => $task_workers 
						), 
						true
					);
					$task_worker.='<script>filter_update();</script>';
					
					echo json_encode( array( 
						'success' => '1', 
						'task_head' => $task_head, 
						'task_worker' => $task_worker,  
						'task_desc'=> Yii::app()->format->formatTtext( $model->description ) 
					) );
					
				}else{
					echo CActiveForm::validate($model);
				}
			}
		}
	}
?>
