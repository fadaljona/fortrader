<?php
	final class DeleteWithCommentsAction extends BaseAction {
		function run() {
			if( isset( $_POST['task_id'] ) ){
				$task_model = $this->controller->loadModel($_POST['task_id']);
				Comment::model()->deleteAll( array("condition"=>"task_id =  ".$task_model->task_id) );
				
				if( $task_model->delete() ){
					$dataForFilter = Task::getFilterData($_POST['filter_status'], $_POST['filter_cat'], $_POST['filter_worker'], $_POST['filter_priority']);
					$filter_updater = $this->controller->renderPartial('_task_filter', array( 'dataForFilter' => $dataForFilter ),true );
					$cat = Category::getCats();
					$post_cat_id = Task::getFilterCat( $_POST['filter_cat'] );
					$post_status = Task::getFilterStatus( $_POST['filter_status'] );
					$post_worker_id = Task::getFilterWorker( $_POST['filter_worker'] );
					$post_priority_id = Task::getFilterPriority( $_POST['filter_priority'] );
					
					$ajax_data_updater = Task::getAjaxDataUpdater($post_cat_id, $post_status, $post_worker_id, $post_priority_id);
					
					unset( $_POST['task_id'] );
					$dataProvider=new CActiveDataProvider(
						'Task', 
						array(
							'criteria' => array(
								'condition'=> Task::getSqlFilter($post_cat_id, $post_status, $post_worker_id, $post_priority_id) ,
								'with'=>array('category', 'manager', 'worker', 'comment', 'comment.user'),
								'order'=>'t.last_comment DESC',
							)
						)
					);		
					
					$ajax_task_list=$this->controller->renderPartial(
						'_jaxtasklist', 
						array(
							'ajax_data_updater' => $ajax_data_updater, 
							'dataProvider' => $dataProvider, 
							'cats' => $cat
						),
						true
					);

					echo json_encode( array( 'success'=>'1', 'filter_updater' => $filter_updater, 'ajax_task_list'=>$ajax_task_list ) );
				}
			}
		}
	}
?>
