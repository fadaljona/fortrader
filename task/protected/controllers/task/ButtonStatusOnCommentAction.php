<?php
	final class ButtonStatusOnCommentAction extends BaseAction {
		function run() {
			$new_status = Yii::app()->db->createCommand()
				->select('status')
				->from('taskbook_task') 
				->where( "task_id = ".$_POST['task_id'] )
				->queryAll();
							
			$model = $this->controller->loadModel($_POST['task_id']);
			
			$this->controller->renderPartial(
				'_status_buttons', 
				array(
					'task_id' => $_POST['task_id'], 
					'status' => $new_status[0]['status'],
					'manager_id' => $model->manager_id,
					'worker_id' => $model->worker_id,							
				)
			);
		}
	}
?>
