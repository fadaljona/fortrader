<?php
	final class GetTaskEditFormAction extends BaseAction {
		function run() {
			if( isset($_POST['task_id']) ){
				$task_model = $this->controller->loadModel( $_POST['task_id'] );
				if( Yii::app()->user->id == $task_model->manager_id || Yii::app()->user->id == $task_model->created_by ){
					
					$edit_form = $this->controller->renderPartial(
						'_edit_task_form', 
						array(
							'employee' => User::getEmployees(), 
							'cats' => Category::getCats(), 
							'task_id' => $_POST['task_id'] 
						),
						true 
					);
					
					echo json_encode( array( 'success'=>'1', 'edit_form'=>$edit_form ) );
				}
			}
		}
	}
?>
