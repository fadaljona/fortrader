<?php
	final class IndexAction extends BaseAction {
		function run() {
			
			$dataProvider=new CActiveDataProvider(
				'Task', 
				array(
					'criteria' => array(
						'condition' => Task::getSqlFilter( 0, 0, 0, 0 ),
						'with' => array('category', 'manager', 'worker', 'comment', 'comment.user'),
						'order' => 't.last_comment DESC',
					)
				)
			);

			$dataForFilter = Task::getFilterData(0, 0, 0, 0);

			$this->controller->render(
				'index',
				array(
					'dataProvider' => $dataProvider,
					'employee' => User::getEmployees(),
					'ajax_data_updater' => Task::getAjaxDataUpdater(0,0,0,0),
					'dataForFilter' => $dataForFilter
				)
			);
		}
	}
?>
