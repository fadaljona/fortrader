<?php
	final class AjaxCreateAction extends BaseAction {
		function run() {
			$model=new Task;	
			if(isset($_POST['Task'])){
				$model->attributes=$_POST['Task'];
				$model->last_comment=$model->created_date;
				if($model->save()){
					$dataForFilter = Task::getFilterData(
						$_POST['Task']['filter_status'], 
						$_POST['Task']['filter_cat'], 
						$_POST['Task']['filter_worker'], 
						$_POST['Task']['filter_priority'] 
					);
					$filter_updater = $this->controller->renderPartial(
						'_task_filter', 
						array( 
							'dataForFilter' => $dataForFilter 
						),
						true 
					);
					
					$cat = Category::getCats();
					
					$post_cat_id = Task::getFilterCat( $_POST['Task']['filter_cat'] );
					$post_status = Task::getFilterStatus( $_POST['Task']['filter_status'] );
					$post_worker_id_filter = Task::getFilterWorker( $_POST['Task']['filter_worker'] );
					$post_priority_filter = Task::getFilterPriority( $_POST['Task']['filter_priority'] );
					
					$ajax_data_updater = Task::getAjaxDataUpdater($post_cat_id,$post_status,$post_worker_id_filter,$post_priority_id);
					
					$dataProvider=new CActiveDataProvider(
						'Task', 
						array(
							'criteria'=>array(
								'condition'=> Task::getSqlFilter($post_cat_id, $post_status, $post_worker_id_filter, $post_priority_filter) ,
								'with'=>array('category', 'manager', 'worker', 'comment', 'comment.user'),
								'order'=>'t.last_comment DESC',
							)
						)
					);
					$ajax_task_list=$this->controller->renderPartial(
						'_jaxtasklist', 
						array(
							'ajax_data_updater' => $ajax_data_updater, 
							'dataProvider'=>$dataProvider, 
							'cats' => $cat
						),
						true
					);

					echo json_encode( array( 'success'=>'1', 'filter_updater' => $filter_updater, 'ajax_task_list'=>$ajax_task_list ) );
					
					Task::sendTaskEmail($model, 2, '', 0);
					
				}else{
					echo CActiveForm::validate($model);
				}
			}
		}
	}
?>
