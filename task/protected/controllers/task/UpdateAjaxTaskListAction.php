<?php
	final class UpdateAjaxTaskListAction extends BaseAction {
		function run() {
			$render_marker=1;
			if ( isset($_GET['Task_page']) || !isset( $_POST['render_marker'] ) ) $render_marker=0;
				
			$cat = Category::getCats();
			$post_cat_id = Task::getFilterCat( $_POST['cat_id'] );
			$post_status = Task::getFilterStatus( $_POST['status'] );
			$post_worker_id_filter = Task::getFilterWorker( $_POST['worker_id'] );
			$post_priority_id = Task::getFilterPriority( $_POST['priority_id'] );
			
			$ajax_data_updater = Task::getAjaxDataUpdater( $post_cat_id, $post_status, $post_worker_id_filter, $post_priority_id );
			
			$dataProvider=new CActiveDataProvider(
				'Task', 
				array(
					'criteria'=>array(
						'condition'=> Task::getSqlFilter( $post_cat_id, $post_status, $post_worker_id_filter, $post_priority_id ),
						'with'=>array('category', 'manager', 'worker', 'comment', 'comment.user'),
						'order'=>'t.last_comment DESC',
					)
				)
			);
			
			if( $render_marker ){
				$this->controller->renderPartial(
					'_jaxtasklist', 
					array(
						'ajax_data_updater' => $ajax_data_updater, 
						'dataProvider'=>$dataProvider, 
						'cats' => $cat
					)
				);
			}else{
				$dataForFilter = Task::getFilterData(0, 0, 0, 0);
				$this->controller->render(
					'index',
					array(
						'dataProvider'=>$dataProvider,
						'employee'=>User::getEmployees(),
						'ajax_data_updater' => $ajax_data_updater,
						'dataForFilter' => $dataForFilter,
					)
				);
			}
		}
	}
?>
