<?php
	final class StatusOnButtonAction extends BaseAction {
		function run() {
			$model = $this->controller->loadModel($_POST['task_id']);
			if( $_POST['status'] == 5 ){
				if( ($model->manager_id == Yii::app()->user->id) ){
					$model->status = 5;
				}else{
					$model->status = 3;
				}
			}else{
				$model->status = $_POST['status'];
			}
			
			if ( $model->save() ){

				switch ( $model ->status) {
					case '1':
						$status_label =  'Новая';
						break;
					case '2':
						$status_label =  'В работе';
						break;
					case '3':
						$status_label =  'На проверку';
						break;
					case '4':
						$status_label =  'Отложена';
						break;
					case '5':
						$status_label =  'Выполнена';
						break;
				}
				Task::sendTaskEmail($model, 1, $status_label, 0);

				$dataForFilter = Task::getFilterData($_POST['filter_status'], $_POST['filter_cat'], $_POST['filter_worker'], $_POST['filter_priority']);
				$filter_updater = $this->controller->renderPartial('_task_filter', array( 'dataForFilter' => $dataForFilter ),true );
					
				$render_status = $this->controller->renderPartial(
					'_status_buttons', 
					array(
						'task_id' => $model->task_id, 
						'status' => $model->status,
						'manager_id' => $model->manager_id,
						'worker_id' => $model->worker_id,
					),
					true
				);
															
				$comment_status = new Comment();
				$comment_status->task_id = $model->task_id;
				$comment_status->comment_text = $status_label;
				$comment_status->user_id = Yii::app()->user->id;
				$comment_status->created_date = date("Y-m-d H:i:s");
				$comment_status->save();
				
				$comment_update = Comment::getListRender( $model->task_id );
				
				echo json_encode( array( 'success' => '1', 'filter_updater' => $filter_updater, 'render_status' => $render_status, 'comment_update' => $comment_update ) );
			}
		}
	}
?>