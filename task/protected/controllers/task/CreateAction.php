<?php
	final class CreateAction extends BaseAction {
		function run() {
			$model=new Task;
			if(isset($_POST['Task'])){
				$model->attributes=$_POST['Task'];
				$model->last_comment=$model->created_date;
				if($model->save()){
					$this->controller->redirect(array('view','id'=>$model->task_id));
				}
			}

			$this->controller->render(
				'create',
				array(
					'model'=>$model,
				)
			);
		}
	}
?>
