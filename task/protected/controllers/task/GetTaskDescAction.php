<?php
	final class FilterUpdateAction extends BaseAction {
		function run() {
			if( isset($_POST['task_id']) ){
				$task_model = $this->controller->loadModel( $_POST['task_id'] );
				echo Yii::app()->format->formatTtext( $task_model->description );
			}
		}
	}
?>
