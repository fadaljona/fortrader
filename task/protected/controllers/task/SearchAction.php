<?php
	final class SearchAction extends BaseAction {
		function run($keyword) {
			$dataProvider=new CActiveDataProvider(
				'Task', 
				array(
					'criteria' => Task::getSearchFilter( $keyword ),
				)
			);

			$dataForFilter = Task::getFilterData(0, 0, 0, 0);

			$this->controller->render(
				'search',
				array(
					'dataProvider' => $dataProvider,
					'employee' => User::getEmployees(),
					'ajax_data_updater' => Task::getAjaxDataUpdater(0,0,0,0),
					'dataForFilter' => $dataForFilter
				)
			);
		}
	}
?>
