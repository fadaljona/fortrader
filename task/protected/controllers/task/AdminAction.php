<?php
	final class AdminAction extends BaseAction {
		function run() {
			$model=new Task('search');
			$model->unsetAttributes();
			if(isset($_GET['Task'])){
				$model->attributes=$_GET['Task'];
			}
			$this->controller->render('admin',array(
				'model'=>$model,
			));
		}
	}
?>
