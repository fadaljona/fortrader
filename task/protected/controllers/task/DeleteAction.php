<?php
	final class DeleteAction extends BaseAction {
		function run() {
			$this->controller->loadModel($id)->delete();
			if(!isset($_GET['ajax'])){
				$this->controller->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
	}
?>
