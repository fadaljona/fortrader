<?php
	final class ReadResponseAction extends BaseAction {
		function run() {
			if( isset($_POST['task_id']) && $_POST['task_id'] ){
				$task_model = $this->controller->loadModel( $_POST['task_id'] );
				if( $task_model->worker_id == Yii::app()->user->id ){
					$task_model->wresponse=0;
				}
				if( $task_model->manager_id == Yii::app()->user->id ){
					$task_model->mresponse=0;
				}
				
				if( $task_model->save() ){
					$dataForFilter = Task::getFilterData(-1, -1, -1, -1);
					
					$filter_updater = $this->controller->renderPartial(
						'_task_filter', 
						array( 
							'dataForFilter' => $dataForFilter 
						),
						true
					);
					
					echo json_encode( array( 'success' => '1', 'filter_updater' => $filter_updater ) );
				}
			}
		}
	}
?>
