<?php
	final class FilterUpdateAction extends BaseAction {
		function run() {
			$dataForFilter = Task::getFilterData($_POST['filter_status'], $_POST['filter_cat'], $_POST['filter_worker'], $_POST['filter_priority']);
			
			$filter_updater = $this->controller->renderPartial(
				'_task_filter', 
				array( 
					'dataForFilter' => $dataForFilter 
				),
				true 
			);

			echo json_encode( array( 'success'=>'1', 'filter_updater' => $filter_updater ) );
		}
	}
?>
