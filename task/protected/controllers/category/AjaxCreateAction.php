<?php
	final class AjaxCreateAction extends BaseAction {
		function run() {
			$model=new Category;

			if(isset($_POST['Category'])){
				$model->attributes=$_POST['Category'];
				if($model->save()){
					echo json_encode( array( 'cat_id' => $model->cat_id, 'name' => $model->name, 'success'=>'1' ) );
				}else{
					echo CActiveForm::validate($model);
				}
			}
		}
	}
?>