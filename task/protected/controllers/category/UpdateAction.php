<?php
	final class UpdateAction extends BaseAction {
		function run($id) {
			$model=$this->controller->loadModel($id);

			if(isset($_POST['Category'])){
				$model->attributes=$_POST['Category'];
				if($model->save()){
					$this->controller->redirect(array('view','id'=>$model->cat_id));
				}
			}

			$this->controller->render('update',array(
				'model'=>$model,
			));
		}
	}
?>