<?php
	final class ViewAction extends BaseAction {
		function run($id) {
			$this->controller->render('view',array(
				'model' => $this->controller->loadModel($id),
			));
		}
	}
?>
