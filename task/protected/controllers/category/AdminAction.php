<?php
	final class AdminAction extends BaseAction {
		function run() {
			$model=new Category('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['Category'])){
				$model->attributes=$_GET['Category'];
			}

			$this->controller->render(
				'admin',
				array(
					'model'=>$model,
				)
			);
		}
	}
?>