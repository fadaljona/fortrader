<?php
	final class ViewAction extends BaseAction {
		function run() {
			$model=new Category;

			if(isset($_POST['Category'])){
				$model->attributes=$_POST['Category'];
				if($model->save()){
					$this->controller->redirect(array('view','id'=>$model->cat_id));
				}
			}

			$this->controller->render('create',array(
				'model'=>$model,
			));
		}
	}
?>
