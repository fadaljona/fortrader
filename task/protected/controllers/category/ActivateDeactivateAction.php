<?php
	final class ActivateDeactivateAction extends BaseAction {
		function run() {
			if ( isset($_POST['cat_id']) ){
				$model = $this->controller->loadModel($_POST['cat_id']);
				if($_POST['active']==1){
					$model -> active = 0;
				}else{
					$model -> active =1;
				} 
				if( $model->save() ){
					echo json_encode( array( 'success'=>'1', 'active' => $model -> active, 'cat_id' => $model -> cat_id ) );
				}
			
			}
		}
	}
?>