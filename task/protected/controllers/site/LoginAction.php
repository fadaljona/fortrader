<?php
	final class LoginAction extends BaseAction {
		function run() {
			$model=new LoginForm;
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form'){
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
			if(isset($_POST['LoginForm'])){
				$model->attributes=$_POST['LoginForm'];
				$model->rememberMe=1;
				if($model->validate() && $model->login()){
					wp_signon( array( 'user_login'=>$_POST['LoginForm']['username'], 'user_password' => $_POST['LoginForm']['password'], 'remember' => true  ), false );
					$this->controller->redirect(Yii::app()->user->returnUrl);
				}
			}
			$this->controller->render('login',array('model'=>$model));
		}
	}
?>
