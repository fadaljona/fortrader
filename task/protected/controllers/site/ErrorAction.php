<?php
	final class ErrorAction extends BaseAction {
		function run() {
			if($error=Yii::app()->errorHandler->error){
				if(Yii::app()->request->isAjaxRequest)
					echo $error['message'];
				else
					$this->controller->render('error', $error);
			}
		}
	}
?>
