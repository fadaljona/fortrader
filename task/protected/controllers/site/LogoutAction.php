<?php
	final class LogoutAction extends BaseAction {
		function run() {
			Yii::app()->user->logout();
			wp_logout();
			$this->controller->redirect(Yii::app()->homeUrl);
		}
	}
?>
