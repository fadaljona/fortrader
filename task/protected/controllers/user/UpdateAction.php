<?php
	final class UpdateAction extends BaseAction {
		function run($id) {
			$model=$this->controller->loadModel($id);

			if(isset($_POST['User'])){
				$model->attributes=$_POST['User'];
				if($model->save()){
					$this->controller->redirect(array('view','id'=>$model->id));
				}
			}

			$this->controller->render('update',array(
				'model'=>$model,
			));
		}
	}
?>
