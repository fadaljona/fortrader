<?php
	final class IndexAction extends BaseAction {
		function run() {
			$dataProvider = new CActiveDataProvider(
				'User',
				array(
					'pagination' => array(
						'pageSize'=>100,
					),
				)
			);
			$this->controller->render('index', array(
				'dataProvider' => $dataProvider,
			));
		}
	}
?>