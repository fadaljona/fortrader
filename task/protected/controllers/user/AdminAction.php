<?php
	final class AdminAction extends BaseAction {
		function run() {
			$model=new User('search');
			$model->unsetAttributes();
			if(isset($_GET['User'])){
				$model->attributes=$_GET['User'];
			}
			$this->controller->render('admin',array(
				'model'=>$model,
			));
		}
	}
?>
