<?php
	final class ActivateDeactivateAction extends BaseAction {
		function run() {
			if ( isset($_POST['id']) ){
			
				$model = $this->controller->loadModel($_POST['id']);
				if($_POST['active']==1){
					$model->active = 0;
				}else{
					$model->active = 1;
				}
				if( $model->save() ){
					echo json_encode( array( 'success'=>'1', 'active' => $model -> active, 'id' => $model -> id ) );
				}
			
			}
		}
	}
?>