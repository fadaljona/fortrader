<?php
	final class LoadWpUserAction extends BaseAction {
		function run() {
			if ( isset ($_POST['user_login']) ){
				$wpUserObject = WpUsersImport::model()->findByAttributes( array('user_login' => $_POST['user_login'] ), array('select' => 'user_login, user_pass, user_email, user_nicename'));

				if($wpUserObject){
					echo json_encode( array( 'user_login' => $_POST['user_login'], 'user_pass' => $wpUserObject->user_pass, 'user_email'=>$wpUserObject->user_email, 'user_nicename'=>$wpUserObject->user_nicename, 'success'=>'1' ) );
				}
				else{
					echo json_encode( array( 'success'=>'0' ) );
				}
			}
		}
	}
?>