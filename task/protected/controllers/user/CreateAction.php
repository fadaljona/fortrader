<?php
	final class CreateAction extends BaseAction {
		function run() {
			$model=new User;

			if(isset($_POST['User'])){
				$model->attributes=$_POST['User'];
				if($model->save()){
					$this->controller->redirect(array('view','id'=>$model->id));
				}
			}

			$this->controller->render('create',array(
				'model'=>$model,
			));
		}
	}
?>