<?php
	final class AjaxCreateAction extends BaseAction {
		function run() {
			$model=new User;

			if(isset($_POST['User'])){
				$model->attributes=$_POST['User'];
				if($model->save()){
					echo json_encode( array( 'username' => $model->username, 'id' => $model->id, 'success'=>'1' ) );
				}else{
					echo CActiveForm::validate($model);
				}	
			}
		}
	}
?>
