<!--comments list-->
<?php
if($comments) echo "<hr>";
foreach( $comments as $comment ){?>

<?php 
	$who_commented='';
	if ($comment->user->id == $task_workers ['task']['manager_id'] ) 
		$who_commented='class="manager-comment"'; 
	else 
		$who_commented='class="worker-comment"'; 
?>

<div <?php echo $who_commented;?>>

	<h6><?php echo $comment->user->realName;?>&nbsp;↓</h6>

	<h6 class="pull-right">
		<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
		<?php echo Yii::app()->format->timeago( $comment->created_date ); ?>
	</h6>

	<?php 

		if( Yii::app()->user->id == $comment->user->id || Yii::app()->user->role == 'administrator' ){
			$div_commennt='id="allow-delete-comment" data-comment-id="'.$comment->comment_id.'"';
			
			$a_delete='<a href="#" class="comment-deleter" id="comment-deleter-"'.$comment->comment_id.' data-comment-id="'.$comment->comment_id.'" data-task-id="'.$comment->task_id.'" >x</a>';
			
		}else{
			$div_commennt='';
			$a_delete='';
		}
		
	?>

	<div <?php echo $div_commennt; ?> ><?php echo Yii::app()->format->formatTtext( $comment->comment_text ); ?>&nbsp;<?php echo $a_delete;?></div>
</div>
<?php }?>
