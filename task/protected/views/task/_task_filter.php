<div class="container" id="filter-container">

	<?php

		$ans_arrray = Yii::app()->db->createCommand()
					->select('task_id, name')
					->from('taskbook_task')  //Your Table name
					->where( '(manager_id='.Yii::app()->user->id.' and mresponse=1) or (worker_id='.Yii::app()->user->id.' and wresponse=1)' )
					->queryAll(); //Will get the all selected rows from table

		$allcount=count($ans_arrray);
		if($allcount){
			echo '<div class="list-group filter">';
			$active='';
			if ( $dataForFilter['filter_task_id']==0 ) $active=' active';
			
			echo CHtml::ajaxLink ("Новые ответы <span class='badge'>".$allcount."</span>",
									  Yii::App()->createUrl('task/updateAjaxTaskList'), 
									   array(
											'type' => 'POST',
											'data' => array('priority_id'=> '0', 
															'render_marker'=>1,
															'status' => '0',
															'cat_id' => '0',
															'worker_id'=> '0',
															'task_id' => 'js: $(this).attr("data-task-id")', 
														),
											'update' => '#data'
									),
									  array('class'=>'list-group-item list-group-item-danger'.$active, 'id' => 'responses', 'data-task-id' => 0)
								  );$active='';
								  
			foreach ( $ans_arrray as $response ){
				if ( $dataForFilter['filter_task_id']==$response['task_id'] ) $active=' active';
				

					echo CHtml::ajaxLink ($response['name'],
										  Yii::App()->createUrl('task/updateAjaxTaskList'), 
										  array(
											'type' => 'POST',
											'data' => array('priority_id'=> '0', 
															'render_marker'=>1,
															'status' => '0',
															'cat_id' => '0',
															'worker_id'=> '0',
															'task_id' => 'js: $(this).attr("data-task-id")', 
														),
											'update' => '#data'
										),
										  array('class'=>'list-group-item list-group-item-danger'.$active, 'id' => 'responses', 'data-task-id' => $response['task_id'])
									  );$active='';
				
				
			}
			echo '</div>';
		}
	?>


	<div class="list-group filter">

<?php 
$cats = $dataForFilter['cats'];
$cats_count = $dataForFilter['cats_count'];
$status_count = $dataForFilter['status_count'];
$count_my = $dataForFilter['count_my'];
$count_my_check = $dataForFilter['count_my_check'];
$count_my_work = $dataForFilter['count_my_work'];
$count_my_out_check = $dataForFilter['count_my_out_check'];	
$filter_status = $dataForFilter['filter_status'];	
$filter_cat = $dataForFilter['filter_cat'];

		
		$active='';

		$allcount = array_sum($status_count);
		
		//if ( $filter_status==0 ) $active=' active';
		if ( Yii::app()->session->get("post_cat_id")==0 && $filter_cat!=-1 ) $active=' active';
		if( !$allcount ) $allcount=0;
		echo CHtml::ajaxLink ("Все задачи<span class='badge'>".$allcount."</span>",
								  Yii::App()->createUrl('task/updateAjaxTaskList'), 
								  array(
										'type' => 'POST',
										'data' => array('status'=> 'js: $(this).attr("data-status")', 
														'render_marker'=>1, 
														'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
														'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
														'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
													),
										'update' => '#data'
									),
								  array('class'=>'list-group-item'.$active, 'id' => 'statuses', 'data-status'=>'0')
							  );$active='';
		

		if( !$count_my ) $count_my=0;
		if ( $filter_status==10 ) $active=' active';
		if($count_my)
		echo CHtml::ajaxLink ("Созданные Мной<span class='badge'>".$count_my."</span>",
								  Yii::App()->createUrl('task/updateAjaxTaskList'), 
								  array(
										'type' => 'POST',
										'data' => array('status'=> 'js: $(this).attr("data-status")', 
														'render_marker'=>1,
														'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
														'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
														'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
													),
										'update' => '#data'
									),
								  array('class'=>'list-group-item'.$active, 'id' => 'statuses', 'data-status'=>'10')
							  );$active='';	
		
		if( !$count_my_check ) $count_my_check=0;
		if ( $filter_status==11 ) $active=' active';
		if($count_my_check)
		echo CHtml::ajaxLink ("Мне На проверку<span class='badge'>$count_my_check</span>",
										  Yii::App()->createUrl('task/updateAjaxTaskList'), 
										  array(
											'type' => 'POST',
											'data' => array('status'=> 'js: $(this).attr("data-status")', 
															'render_marker'=>1,
															'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
															'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
															'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
														),
											'update' => '#data'
										),
										  array('class'=>'list-group-item'.$active, 'id' => 'statuses', 'data-status'=>'11')
									  );$active='';
		if( !$count_my_work ) $count_my_work=0;
		if ( $filter_status==12 ) $active=' active';
		if($count_my_work)
		echo CHtml::ajaxLink ("Мне На исполнение<span class='badge'>$count_my_work</span>",
										  Yii::App()->createUrl('task/updateAjaxTaskList'), 
										  array(
											'type' => 'POST',
											'data' => array('status'=> 'js: $(this).attr("data-status")', 
															'render_marker'=>1,
															'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
															'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
															'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
															),
											'update' => '#data'
										),
										  array('class'=>'list-group-item'.$active, 'id' => 'statuses', 'data-status'=>'12')
									  );$active='';
		if( !$count_my_out_check ) $count_my_out_check=0;
		if ( $filter_status==13 ) $active=' active';
		if($count_my_out_check)
		echo CHtml::ajaxLink ("Ушло На проверку<span class='badge'>$count_my_out_check</span>",
										  Yii::App()->createUrl('task/updateAjaxTaskList'), 
										  array(
											'type' => 'POST',
											'data' => array('status'=> 'js: $(this).attr("data-status")', 
															'render_marker'=>1,
															'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
															'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
															'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
															),
											'update' => '#data'
										),
										  array('class'=>'list-group-item'.$active, 'id' => 'statuses', 'data-status'=>'13')
									  );$active='';
		
		$lables = array('1' => 'Новые', '2' => 'В работе', '4' => 'Отложены', '5' => 'Выполнены');
		foreach( $lables as $key => $value ){
			
			if ( $filter_status==$key ) $active=' active';
			if( isset($status_count[$key]) ) $count_stat=$status_count[$key]; else $count_stat=0;
			
			if( $count_stat ){
				echo CHtml::ajaxLink ("$value<span class='badge'>".$count_stat."</span>",
										  Yii::App()->createUrl('task/updateAjaxTaskList'), 
										  array(
											'type' => 'POST',
											'data' => array('status'=> 'js: $(this).attr("data-status")', 
															'render_marker'=>1,
															'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
															'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
															'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
															),
											'update' => '#data'
										),
										  array('class'=>'list-group-item'.$active, 'id' => 'statuses', 'data-status'=>$key)
									  );$active='';
			}
		}
?>
	</div>
	<div class="list-group filter">
	<?php
		$allcount = array_sum($cats_count);
		$active='';
		if ( $filter_cat==0 ) $active=' active';
		
		echo CHtml::ajaxLink ("Все категории <span class='badge'>".$allcount."</span>",
								  Yii::App()->createUrl('task/updateAjaxTaskList'), 
								  array(
										'type' => 'POST',
										'data' => array('cat_id'=> 'js: $(this).attr("data-cat-id")', 
														'render_marker'=>1,
														'status' => 'js: $("#statuses.active").attr("data-status")',
														'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
														'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
														),
										'update' => '#data'
										),
								  array('class'=>'list-group-item'.$active, 'id' => 'filtercats', 'data-cat-id' => 0)
							  );$active='';
		foreach ( $cats as $key => $value ){
			if ( $filter_cat==$key ) $active=' active';
			if( isset($cats_count[$key]) ) $count=$cats_count[$key]; else $count=0;
			
			if( $count ){
				echo CHtml::ajaxLink ($value." <span class='badge'>".$count."</span>",
									  Yii::App()->createUrl('task/updateAjaxTaskList'), 
									  array(
										'type' => 'POST',
										'data' => array('cat_id'=> 'js: $(this).attr("data-cat-id")', 
														'render_marker'=>1,
														'status' => 'js: $("#statuses.active").attr("data-status")',
														'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
														'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
														),
										'update' => '#data'
									),
									  array('class'=>'list-group-item'.$active, 'id' => 'filtercats', 'data-cat-id' => $key)
								  );$active='';
			}
			
		}
	?>
	</div>
	
	<?php
		$allcount=0;
		foreach ( $dataForFilter['workers_for_filter'] as $workers_for_filter ) $allcount+=$workers_for_filter['count'];
		if($allcount){
	?>
	<div class="list-group filter">
	<?php
		$active='';
		if ( $dataForFilter['filter_worker']==0 ) $active=' active';
		
		echo CHtml::ajaxLink ("Мои работники <span class='badge'>".$allcount."</span>",
								  Yii::App()->createUrl('task/updateAjaxTaskList'), 
								   array(
										'type' => 'POST',
										'data' => array('worker_id'=> 'js: $(this).attr("data-worker-id")', 
														'render_marker'=>1,
														'status' => 'js: $("#statuses.active").attr("data-status")',
														'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
														'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
														),
										'update' => '#data'
								),
								  array('class'=>'list-group-item'.$active, 'id' => 'filterworkers', 'data-worker-id' => 0)
							  );$active='';
							  
		foreach ( $dataForFilter['workers_for_filter'] as $workers_for_filter ){
			if ( $dataForFilter['filter_worker']==$workers_for_filter['worker_id'] ) $active=' active';
			

				echo CHtml::ajaxLink ($workers_for_filter['realName']." <span class='badge'>".$workers_for_filter['count']."</span>",
									  Yii::App()->createUrl('task/updateAjaxTaskList'), 
									  array(
										'type' => 'POST',
										'data' => array('worker_id'=> 'js: $(this).attr("data-worker-id")', 
														'render_marker'=>1,
														'status' => 'js: $("#statuses.active").attr("data-status")',
														'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
														'priority_id'=> 'js: $("#priorities.active").attr("data-priority-id")',
													),
										'update' => '#data'
									),
									  array('class'=>'list-group-item'.$active, 'id' => 'filterworkers', 'data-worker-id' => $workers_for_filter['worker_id'])
								  );$active='';
			
			
		}
	?>
	</div>
	<?php }?>
	
	
	<div class="list-group filter">
	<?php

		$allcount=0;
		foreach ( $dataForFilter['priority_for_filter'] as $priority_for_filter ) $allcount+=$priority_for_filter['count'];
		$active='';
		if ( $dataForFilter['filter_priority']==0 ) $active=' active';
		
		echo CHtml::ajaxLink ("Приоритеты <span class='badge'>".$allcount."</span>",
								  Yii::App()->createUrl('task/updateAjaxTaskList'), 
								   array(
										'type' => 'POST',
										'data' => array('priority_id'=> 'js: $(this).attr("data-priority-id")', 
														'render_marker'=>1,
														'status' => 'js: $("#statuses.active").attr("data-status")',
														'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
														'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
													),
										'update' => '#data'
								),
								  array('class'=>'list-group-item'.$active, 'id' => 'priorities', 'data-priority-id' => 0)
							  );$active='';
							  
		foreach ( $dataForFilter['priority_for_filter'] as $priority_for_filter ){
			if ( $dataForFilter['filter_priority']==$priority_for_filter['priority'] ) $active=' active';
			

				echo CHtml::ajaxLink ($priority_for_filter['priority']." <span class='badge'>".$priority_for_filter['count']."</span>",
									  Yii::App()->createUrl('task/updateAjaxTaskList'), 
									  array(
										'type' => 'POST',
										'data' => array('priority_id'=> 'js: $(this).attr("data-priority-id")', 
														'render_marker'=>1,
														'status' => 'js: $("#statuses.active").attr("data-status")',
														'cat_id' => 'js: $("#filtercats.active").attr("data-cat-id")',
														'worker_id'=> 'js: $("#filterworkers.active").attr("data-worker-id")',
													),
										'update' => '#data'
									),
									  array('class'=>'list-group-item'.$active, 'id' => 'priorities', 'data-priority-id' => $priority_for_filter['priority'])
								  );$active='';
			
			
		}
	?>
	</div>

	
</div>

<script>
	var $container = $('#filter-container');
		// initialize
		$container.masonry({
		  itemSelector: '.filter'
	});
</script>
