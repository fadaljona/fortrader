<?php
/* @var $this TaskController */
/* @var $data Task */
?>
<?php $response_marker = (($data->manager_id == Yii::app()->user->id) && $data->mresponse) || (($data->worker_id == Yii::app()->user->id) && $data->wresponse); ?>
<div class="view comment-count-and-status" id="view-task-<?php echo $data->task_id; ?>">
<div class="panel panel-default">
	<div class="panel-heading <?php if( $response_marker ) echo 'task-header';?> task-header<?php echo $data->task_id; ?>">
		<?php 
		$task_header = array('task' => Task::objToArray($data),
							'category' => Task::objToArray($data->category),
							);
		$this->renderPartial('_view_task_header', array( 'task_header' => $task_header )); ?>
	</div>
	<div class="panel-body">
		<div data-task-edit-id="<?php echo $data->task_id; ?>" <?php if( Yii::app()->user->id == $data->manager_id || Yii::app()->user->id == $data->created_by )echo 'class="edit_description"'; ?> >
			<?php echo Yii::app()->format->formatTtext( $data->description );?>
		</div>
		<hr>
		
		
		
		<h6 class="worker-list<?php echo $data->task_id; ?>">
			<?php 
			$task_workers = array('task' => Task::objToArray($data),
							'manager' => Task::objToArray($data->manager),
							'worker' => Task::objToArray($data->worker),
							);
			$this->renderPartial('_view_task_workers', array( 'task_workers' => $task_workers )); ?>
		</h6>
		
		<div class="btn-group pull-right" id="button-status-on-comment<?php echo $data->task_id;?>" data-status-update="<?php echo $data->task_id;?>">
		<?php $this->renderPartial('_status_buttons', array('task_id' => $data->task_id, 'status' => $data->status, 'manager_id' => $data->manager_id, 'worker_id' => $data->worker_id ) ); ?>
		</div>
		
		
		
		<a href="#" class="btn btn-default btn-sm" id="open-close-comment-form" data-show="0">Комментарии&nbsp; <span class="label label-default"><?php echo count( $data->comment ); ?></span></a>
		
		<div class="form-group"></div>
		<div id="comment-form-show">
		<?php $this->renderPartial('_comment_form', array( 'task_id' =>$data->task_id )); ?>
		</div>
		
		<?php if( $response_marker ) echo '<a href="#" class="btn btn-default read-response" data-task-id="'.$data->task_id.'">Прочитано</a>';?>
			
		
		<div class="comment-list" id="comment-list-<?php echo $data->task_id; ?>">
			<?php $this->renderPartial('_comment_list', array( 'comments' =>$data->comment, 'task_workers' => $task_workers )); ?>
		</div>
		

		
		
		  
	</div>
</div>
</div>
