<?php
/* @var $this TaskController */
/* @var $model Task */
?>

<?php
	$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'ajaxUpdate'=>false,
	'pager'=>array('cssFile'=>false, 'class'=>'CLinkPager', 'header' => ''),
)); ?>
