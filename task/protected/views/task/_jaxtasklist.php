<script>
$('a#filtercats').removeClass('active');
$('a[data-cat-id=<?php echo $ajax_data_updater['cat_id']?$ajax_data_updater['cat_id']:0; ?>]').addClass('active');
$('#Task_filter_status').val(<?php echo $ajax_data_updater['status']?$ajax_data_updater['status']:0; ?>);
$('#Task_filter_cat').val(<?php echo $ajax_data_updater['cat_id']?$ajax_data_updater['cat_id']:0; ?>);
$('a#statuses').removeClass('active');
$('a[data-status=<?php echo $ajax_data_updater['status']?$ajax_data_updater['status']:0; ?>]').addClass('active');
$('a#filterworkers').removeClass('active');
$('a[data-worker-id=<?php echo $ajax_data_updater['worker_id']?$ajax_data_updater['worker_id']:0; ?>]').addClass('active');
$('#Task_filter_worker').val(<?php echo $ajax_data_updater['worker_id']?$ajax_data_updater['worker_id']:0; ?>);
$('a#priorities').removeClass('active');
$('a[data-priority-id=<?php echo $ajax_data_updater['priority_id']?$ajax_data_updater['priority_id']:0; ?>]').addClass('active');
$('#Task_filter_priority').val(<?php echo $ajax_data_updater['priority_id']?$ajax_data_updater['priority_id']:0; ?>);
$('a#responses').removeClass('active');
$('a[data-task-id=<?php echo $ajax_data_updater['task_id'] ?>]').addClass('active');
</script>


<?php
if( $ajax_data_updater['cat_id'] ){
?>

<h3>
<a id="filter-all" href="#" data-cat-id="0">Все</a>

       → <?php echo $cats[ $ajax_data_updater['cat_id'] ];?>
</h3>
<?php }?>
<hr>

<?php
	$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'ajaxUpdate'=>false,
	'pager'=>array('cssFile'=>false, 'class'=>'CLinkPager', 'header' => ''),
)); 
?>
