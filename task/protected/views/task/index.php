<?php
/* @var $this TaskController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tasks',
);

if( Yii::app()->user->checkAccess('employee') )
$this->renderPartial('_create_task_form', array('employee'=>$employee, 'cats' => $dataForFilter['cats']) ); ?>

<div id="task-filter">
<?php $this->renderPartial('_task_filter', array( 'dataForFilter' => $dataForFilter) ); ?>
</div>


<div id="data">
   <?php $this->renderPartial('_jaxtasklist', array('ajax_data_updater'=>$ajax_data_updater, 'dataProvider'=>$dataProvider, 'cats' => $dataForFilter['cats'] )); ?>
</div>