<?php
/* @var $this TaskController */
/* @var $model Task */
/* @var $form CActiveForm */
?>


<div id="edit-task-form-id">
<?php
	$model=Task::model()->findByPk($task_id);
	$form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-form',
	'action'=>Yii::App()->createUrl('task/edittask'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	//'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>TRUE,
	),
)); ?>



	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128, 'class' => 'form-control' )); ?>
		<?php echo $form->error($model,'name'); ?>
		<div style="" id="Task_edit_name_em_<?php echo $model->task_id; ?>" class="errorMessage"></div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
		<div style="" id="Task_edit_description_em_<?php echo $model->task_id; ?>" class="errorMessage"></div>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'manager_id'); ?>
		<?php echo $form->dropDownList($model,'manager_id', $employee, array('class' => 'form-control', 'options' => array( Yii::app()->user->id =>array('selected'=>true))     ) ); ?>
		<?php echo $form->error($model,'manager_id'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'worker_id'); ?>
		<?php echo $form->dropDownList($model,'worker_id', $employee, array('class' => 'form-control', 'options' => array( Yii::app()->user->id =>array('selected'=>true))   ) ); ?>
		<?php echo $form->error($model,'worker_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cat_id'); ?>
		<?php echo $form->dropDownList($model,'cat_id', $cats, array('class' => 'form-control') ); ?>
		<?php echo $form->error($model,'cat_id'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'priority'); ?>
		<?php echo $form->dropDownList($model,'priority', array( '1'=>1, '2'=>2, '3'=>3, '4'=>4, '5'=>5, '6'=>6, '7'=>7, '8'=>8, '9'=>9, '10'=>10 ), array('class' => 'form-control') ); ?>
		<?php echo $form->error($model,'priority'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', array( '1'=>'Новая', '2'=>'В работе', '3'=>'На проверку', '4'=>'Отложена', '5'=>'Выполнена' ), array('class' => 'form-control') ); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

		<?php echo $form->hiddenField($model,'task_id',array( 'value' => $model->task_id )); ?>


	<div class="row buttons edit-task-button">

		<input class="btn btn-danger pull-right" id="cancel-ajax-edit-form" type="submit" data-task-id="<?php echo $model->task_id; ?>" name="cancel-ajax-edit-form" value="Отменить!" />
		<input class="btn btn-success pull-right" id="ajax-edit-form" type="submit" data-task-id="<?php echo $model->task_id; ?>" name="ajax-edit-form" value="Готово!" />
		
	</div>

<?php $this->endWidget(); ?>

<hr>
</div>
