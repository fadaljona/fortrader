<div class="form">
<div>

<?php 
	$model = new Comment;
	$form=$this->beginWidget('CActiveForm', array(
	'id'=>'comment-form',
	'action'=>Yii::App()->createUrl('comment/ajaxcreate'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	//'enableAjaxValidation'=>true,
)); ?>


	<?php echo $form->errorSummary($model); ?>
	<?php echo $form->hiddenField($model,'task_id',array( 'value' => $task_id )); ?>
	<?php echo $form->hiddenField($model,'user_id',array( 'value' => Yii::app()->user->id )); ?>
	<?php //echo $form->hiddenField($model,'created_date',array( 'value' => date("Y-m-d H:i:s") )); ?>

	<div class="form-group">
		<?php echo $form->textArea($model,'comment_text',array('rows'=>2, 'cols'=>50, 'class' => 'form-control', 'data-textarea-id'=>$task_id )); ?>
		<?php echo $form->error($model,'comment_text'); ?>
	</div>


	<div class="row buttons">

		<input class="btn btn-default pull-right submit-comment" data-task="<?php echo $task_id; ?>" id="submit-comment<?php echo $task_id; ?>" type="submit" name="yt1<?php echo $task_id; ?>" value="Отправить!" />
	</div>

<?php $this->endWidget(); ?>
</div>
</div><!-- form -->
