<?php
$task = $task_header['task'];
$category = $task_header['category'];
?>
<h6 class="pull-right">
	<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
	<?php echo Yii::app()->format->timeago( CHtml::encode($task['created_date']) ); ?> 
	<?php  if( Yii::app()->user->checkAccess('administrator')  ){ ?>
		<span style="color:red"><a id="delete-task-comments" href="#" style="color:red" data-task-id="<?php echo $task['task_id'];?>">DELETE</a></span>
	<?php } ?>
</h6>

<h4>
	№:
	<a href="<?php echo Yii::App()->createUrl( 'task/view', array( 'id' => CHtml::encode($task['task_id']) ) ) ?>"><?php echo CHtml::encode($task['task_id']); ?></a>
	→
	<a id="in-view-filter-cat" cat-id="<?php echo $category['cat_id']; ?>" href="#"><?php echo $category['name']; ?></a>	 
	→ 
	<span>
		<?php echo CHtml::encode($task['name']); ?>
	</span>
</h4>
<?php $this->renderPartial('_status', array('status' => CHtml::encode($task['status']) ) ); ?>
		
<span>
	→
</span> 
<span class="badge priority" id="priority-change-<?php echo $task['task_id']; ?>">
	<span id="priority-val">
		<?php echo $task['priority'];?>
	</span>
</span>