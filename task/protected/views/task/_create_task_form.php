<?php
/* @var $this TaskController */
/* @var $model Task */
/* @var $form CActiveForm */
?>

<a href="#" id="open-close-create-task-form" data-show="0" class="btn btn-primary">Создать задачу</a><br /><br />
<div id="create-task-form-id">
<?php 
	$model=new Task;
	$form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-form',
	'action'=>Yii::App()->createUrl('task/ajaxCreate'),
	'clientOptions'=>array(
		'validateOnSubmit'=>TRUE,
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>
		<div style="" id="Task_name_em_" class="errorMessage"></div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
		<div style="" id="Task_description_em_" class="errorMessage"></div>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'manager_id'); ?>
		<?php echo $form->dropDownList($model,'manager_id', $employee, array('class' => 'form-control', 'options' => array( Yii::app()->user->id =>array('selected'=>true))     ) ); ?>
		<?php echo $form->error($model,'manager_id'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'worker_id'); ?>
		<?php echo $form->dropDownList($model,'worker_id', $employee, array('class' => 'form-control', 'options' => array( Yii::app()->user->id =>array('selected'=>true))   ) ); ?>
		<?php echo $form->error($model,'worker_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'cat_id'); ?>
		<?php echo $form->dropDownList($model,'cat_id', $cats, array('class' => 'form-control') ); ?>
		<?php echo $form->error($model,'cat_id'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'priority'); ?>
		<?php echo $form->dropDownList($model,'priority', array( '1'=>1, '2'=>2, '3'=>3, '4'=>4, '5'=>5, '6'=>6, '7'=>7, '8'=>8, '9'=>9, '10'=>10 ), array('class' => 'form-control') ); ?>
		<?php echo $form->error($model,'priority'); ?>
	</div>

		<?php echo $form->hiddenField($model,'created_by',array( 'value' => Yii::app()->user->id )); ?>
		<?php echo $form->error($model,'created_by'); ?>
		<?php echo $form->hiddenField($model,'status',array( 'value' => '1' )); ?>
		<?php echo $form->error($model,'status'); ?>
		<?php echo $form->hiddenField($model,'created_date',array( 'value' => date("Y-m-d H:i:s") )); ?>
		<?php echo $form->error($model,'created_date'); ?>
		
		<input type="hidden" id="Task_filter_status" name="Task[filter_status]" value="1">
		<input type="hidden" id="Task_filter_cat" name="Task[filter_cat]" value="1">
		<input type="hidden" id="Task_filter_worker" name="Task[filter_worker]" value="1">
		<input type="hidden" id="Task_filter_priority" name="Task[filter_priority]" value="1">


	<div class="row buttons">
		<input class="btn btn-success pull-right" id="ajax-creat-form" type="submit" name="yt0" value="Готово!" />	
	</div>

<?php $this->endWidget(); ?>

<hr>
</div>