<?php
/* @var $this TaskController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tasks',
);
?>


<div id="data">
   <?php $this->renderPartial('_jaxtasklist', array('ajax_data_updater'=>$ajax_data_updater, 'dataProvider'=>$dataProvider, 'cats' => $dataForFilter['cats'] )); ?>
</div>