<?php
switch ($status) {
		case '1':
			echo '<span id="task-status-label" class="label label-primary">Новая</span>';
			break;
		case '2':
			echo '<span id="task-status-label" class="label label-warning">В работе</span>';
			break;
		case '3':
			echo '<span id="task-status-label" class="label label-info">На проверку</span>';
			break;
		case '4':
			echo '<span id="task-status-label" class="label label-danger">Отложена</span>';
			break;
		case '5':
			echo '<span id="task-status-label" class="label label-success">Выполнена</span>';
			break;
	}
?>