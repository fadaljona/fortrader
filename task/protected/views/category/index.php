<?php
/* @var $this CategoryController */
/* @var $dataProvider CActiveDataProvider */
$this->renderPartial('_menu');
?>
<br /><br />
<div class="container">
	<span id="user-created-msg"></span>
	<div class="form-group">
		<label>
			Название:
		</label>
		
	</div>

<?php $this->renderPartial('_ajax_form'); ?>

</div>

<h3>Категории</h3>
<hr>
<table class="table">
	<tbody>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view', 
)); ?>
	</tbody>
</table>
<hr>