<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>


<?php
	$model=new Category;
	$form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	'action'=>Yii::App()->createUrl('category/ajaxCreate'),
	'clientOptions'=>array(
		'validateOnSubmit'=>TRUE,
	),
)); ?>


	<?php echo $form->errorSummary($model); ?>


		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128, 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>

	
	<?php echo $form->hiddenField($model,'active',array( 'value' => "1" )); ?>
	<?php echo $form->error($model,'active'); ?><br />


	<?php echo CHtml::ajaxSubmitButton( 'Готово!', Yii::App()->createUrl('category/ajaxCreate'), array(
			'dataType'=>'json',
			'success' => 'function(json){
				if( json.success == 1) {
					//$("#Category_name").val("");
					$("#user-created-msg").text("CATEGORY CREATED");
					$("#user-created-msg").show(1000);
					$("#Category_name").css({"border-color":"#ccc"});
					$("tbody").prepend( "<tr><td>"+json.name+"</td><td><button type=\"submit\" class=\"btn btn-default\">Отключить</button></td></tr>" );
					$("#Category_name").val("");
				}else{
					var tmpval = $("#Category_name").val();
					$("#Category_name").val(tmpval+" - Вероятно есть дубль");
					$("#Category_name").css({"border-color":"red"});
				}
			}',
		),
		array('class'=>'btn btn-success pull-right submit-cat')
		); ?>		

<?php $this->endWidget(); ?>

