<?php
/* @var $this CategoryController */
/* @var $data Category */
?>


<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cat_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->cat_id), array('view', 'id'=>$data->cat_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />*/?>


<tr>
	<td>
		<?php echo CHtml::encode($data->name); ?>
	</td>
	<td>
		<?php
		if($data->active == 1 ){ $label="Отключить"; $butclass="default"; }else{ $label="Включить"; $butclass="primary"; }
		echo CHtml::ajaxButton ($label,
								  Yii::App()->createUrl('category/activateDeactivate'), 
								  array(
										'dataType' => 'json',
										'type' => 'POST',
										'data' => array('cat_id'=> 'js:$(this).attr("data-id")', 'active' => 'js:$(this).attr("data-active")' ),
										'success' => 'function(html){
											if( html.active == 0 ){
												$("input[data-id="+html.cat_id+"]").attr({"data-active":0});
												$("input[data-id="+html.cat_id+"]").val("Включить");
												$("input[data-id="+html.cat_id+"]").removeClass();
												$("input[data-id="+html.cat_id+"]").addClass("btn btn-primary");
											}else{
												$("input[data-id="+html.cat_id+"]").attr({"data-active":1});
												$("input[data-id="+html.cat_id+"]").val("Отключить");
												$("input[data-id="+html.cat_id+"]").removeClass();
												$("input[data-id="+html.cat_id+"]").addClass("btn btn-default");
												
											}
										}',
									),
								  array('class'=>'btn btn-'.$butclass, 'data-id'=>$data->cat_id, 'data-active'=> $data->active )
							  );?>
	</td>
</tr>