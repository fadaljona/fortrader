<!--comments list-->
<?php
if($comments) echo "<hr>";
foreach( $comments as $comment ){?>

<h6><?php echo $comment->user->realName;?>&nbsp;↓</h6>

<h6 class="pull-right">
	<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
	<?php echo Yii::app()->format->timeago( $comment->created_date ); ?>
</h6>

<div><?php echo CHtml::encode($comment->comment_text); ?>&nbsp;</div>
<?php }?>