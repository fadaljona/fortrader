<?php
/* @var $this CommentController */
/* @var $data Comment */
//print_r($data);

?>

<?php 
	$who_commented='';
	if ($data->user->id == $manager_id ) 
		$who_commented='class="manager-comment"'; 
	else 
		$who_commented='class="worker-comment"'; 
?>

<div <?php echo $who_commented;?>>

	<h6><?php echo $data->user->realName;?>&nbsp;↓</h6>

	<h6 class="pull-right">
		<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
		<?php echo Yii::app()->format->timeago( $data->created_date ); ?>
	</h6>

	<?php 

		if( Yii::app()->user->id == $data->user->id || Yii::app()->user->role == 'administrator' ){
			$div_commennt='id="allow-delete-comment" data-comment-id="'.$data->comment_id.'"';
			
			$a_delete='<a href="#" class="comment-deleter" id="comment-deleter-"'.$data->comment_id.' data-comment-id="'.$data->comment_id.'" data-task-id="'.$data->task_id.'" >x</a>';
			
		}else{
			$div_commennt='';
			$a_delete='';
		}
		
	?>

	<div <?php echo $div_commennt; ?> ><?php echo Yii::app()->format->formatTtext( $data->comment_text ); ?>&nbsp;<?php echo $a_delete;?></div>

</div>
