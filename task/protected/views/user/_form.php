<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>



		<?php echo $form->hiddenField($model,'username',array('size'=>60,'maxlength'=>128,'class' => "form-control")); ?>

		<?php echo $form->hiddenField($model,'password',array('size'=>60,'maxlength'=>128,'class' => "form-control")); ?>



	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128,'class' => "form-control")); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'role'); ?>
      <?php echo $form->dropDownList($model,'role', array('employee' => 'employee', 'administrator' => 'administrator' ) ); ?>
		<?php echo $form->error($model,'role'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->dropDownList($model,'active', array( 0, 1 ) ); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'realName'); ?>
		<?php echo $form->textField($model,'realName',array('size'=>60,'maxlength'=>128,'class' => "form-control")); ?>
		<?php echo $form->error($model,'realName'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-success pull-left')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->