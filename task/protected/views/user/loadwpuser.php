<div class="form">

<?php 
	$model=new User;
	$form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128, 'value' => $wpUserObject->user_login)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128, 'value' => $wpUserObject->user_pass)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128, 'value' => $wpUserObject->user_email)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'role'); ?>
      <?php echo $form->dropDownList($model,'role', array( 'employee', 'admin' ) ); ?>
		<?php echo $form->error($model,'role'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->dropDownList($model,'active', array( 1, 0 ) ); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'realName'); ?>
		<?php echo $form->textField($model,'realName',array('size'=>60,'maxlength'=>128, 'value' => $wpUserObject->user_nicename)); ?>
		<?php echo $form->error($model,'realName'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::ajaxSubmitButton($model->isNewRecord ? 'Create' : 'Save', Yii::App()->createUrl('user/create'), array(
		'type' => 'POST',
		/*'beforeSend' => 'function(){
            $("#pagetest").addClass("loading");
         }',*/
	 /*'complete' => 'function(){
            $("#pagetest").removeClass("loading");
        }',*/
    // ��������� ������� ���������� � �������, ���������
    // �� CSS-��������� #output.
	//'data' => array('input'=> 'js: $("#input").val()'),
	/*'success' => 'function(html){
                alert(html)}',*/
    'update' => '#pagetest',
)); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->