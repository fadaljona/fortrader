<div class="form" id="form-user-search" style="display:none;">

<?php 
	$model=new User;
	$form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::App()->createUrl('user/ajaxCreate'),
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	//'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>TRUE,
	),
)); ?>

	<span class="user-error errorMessage"></span>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->hiddenField($model,'username',array('size'=>60,'maxlength'=>128, 'value' => '', 'class' => "form-control")); ?>
		<?php echo $form->hiddenField($model,'password',array('size'=>60,'maxlength'=>128, 'value' => '','class' => "form-control")); ?>
		<?php echo $form->hiddenField($model,'email',array('size'=>60,'maxlength'=>128, 'value' => '','class' => "form-control")); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'role'); ?>
      <?php echo $form->dropDownList($model,'role', array( 'employee' => 'employee', 'administrator' => 'administrator' ) ); ?>
		<?php echo $form->error($model,'role'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->dropDownList($model,'active', array( 0, 1 ), array( 'options' => array( '1' =>array('selected'=>true)))     ); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>


		<?php echo $form->hiddenField($model,'realName',array('size'=>60,'maxlength'=>128, 'value' => '','class' => "form-control")); ?>


	<div class="row buttons">

		<?php echo CHtml::ajaxSubmitButton('Create user', Yii::App()->createUrl('user/ajaxCreate'), array(
			'type' => 'POST',
			'dataType'=>'json',
			'success' => 'function(json){
                if( json.success == 1 ) {
					$("#form-user-search").hide();
					$("#user-created-msg").text("USER CREATED");
					$("#user-created-msg").show(1000);
					$("tbody").prepend( "<tr><td>"+json.username+"</td><td><button type=\"submit\" class=\"btn btn-default\">Отключить</button></td></tr>" );
					
				}else{
					$(".user-error").html( json.User_username );
				}
				
			}',

		),array('class'=>'btn btn-success pull-left')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->