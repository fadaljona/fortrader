<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */
?>
<?php $this->renderPartial('_menu'); ?>
<br /><br />
<div class="container" id="container">
	<span id="user-created-msg" ></span>
    <div class="form-group">
        <label>
            Wp login:
        </label>
        <input type="text" id="search-wp-user" class="form-control">
    </div>
<?php $this->renderPartial('_form_user_search'); ?>

		<?php echo CHtml::ajaxButton( 'Поиск юзера', Yii::App()->createUrl('user/loadWpUser'), array(
			'type' => 'POST',
			//'async'=false,
			'data' => array('user_login'=> 'js: $("#search-wp-user").val()'),
			'dataType'=>'json',
			'success' => 'function(json){

					if( json.success == 1 ){

						$("#search-wp-user").css({"border-color":"#ccc"});
						$("#form-user-search").css({display:"block"});
						
						$("#User_username").val( json.user_login );
						$("#User_password").val( json.user_pass );
						$("#User_email").val( json.user_email );
						$("#User_realName").val( json.user_nicename );
						
					}else{
						$("#search-wp-user").css({"border-color":"red"});
						var tmpval = $("#search-wp-user").val();
						$("#search-wp-user").val("Нет такого юзера: "+ tmpval);
					}
				}',
			
		),
		array('class'=>'btn btn-success pull-right')
		); ?>		
</div> 
   
<h3>Пользователи</h3>

<hr>
<table class="table">
	<tbody>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
	</tbody>
</table>
<hr>