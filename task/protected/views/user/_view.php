<?php
/* @var $this UserController */
/* @var $data User */
?>

<tr>
   <td>
      <?php echo CHtml::encode($data->username); ?>
   </td>
   <td>
	  
	  <?php
		if($data->active == 1 ){ $label="Отключить"; $butclass="default"; }else{ $label="Включить"; $butclass="primary"; }
		echo CHtml::ajaxButton ($label,
								  Yii::App()->createUrl('user/activateDeactivate'), 
								  array(
										'dataType' => 'json',
										'type' => 'POST',
										'data' => array('id'=> 'js:$(this).attr("data-id")', 'active' => 'js:$(this).attr("data-active")' ),
										'success' => 'function(html){
											if( html.active == 0 ){
												$("input[data-id="+html.id+"]").attr({"data-active":0});
												$("input[data-id="+html.id+"]").val("Включить");
												$("input[data-id="+html.id+"]").removeClass();
												$("input[data-id="+html.id+"]").addClass("btn btn-primary");
											}else{
												$("input[data-id="+html.id+"]").attr({"data-active":1});
												$("input[data-id="+html.id+"]").val("Отключить");
												$("input[data-id="+html.id+"]").removeClass();
												$("input[data-id="+html.id+"]").addClass("btn btn-default");
												
											}
										}',
									),
								  array('class'=>'btn btn-'.$butclass, 'data-id'=>$data->id, 'data-active'=> $data->active )
							  );?>
   </td>
</tr>



<?/*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
	<?php echo CHtml::encode($data->role); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realName')); ?>:</b>
	<?php echo CHtml::encode($data->realName); ?>
	<br />
*/?>

