<?php /* @var $this Controller */ ?>
<?php 

if(Yii::app()->user->UserActive == 0 ) {
	Yii::app()->user->logout();
}


?>
<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
    rel="stylesheet">
    
    
    <!-- blueprint CSS framework -->
 
   
   
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
   
  
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen-27f43d5231.css" rel="stylesheet">

    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	 <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
	 <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/masonry.pkgd.min.js"></script>
</head>
  
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo Yii::App()->createUrl('site/index');?>"><?php echo CHtml::encode(Yii::app()->name); ?></a>
        </div>
        <div class="navbar-collapse collapse">
          
          
          <?php $this->widget('zii.widgets.CMenu',array(
          'htmlOptions'=>array('class'=>'nav navbar-nav'),
			'items'=>array(
				array('label'=>'Задачи', 'url'=>array('/task/index')),
				array('label'=>'Пользователи', 'url'=>array('/user/index')),
				array('label'=>'Категории', 'url'=>array('/category/index')),
			//	array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest)
			),
		)); ?>
      <?php if( !Yii::app()->user->id ){?>
          <form class="navbar-form navbar-right" id="login-form" action="<?php echo Yii::App()->createUrl('site/login');?>" method="post" >
            <div class="form-group">

              <input name="LoginForm[username]" id="LoginForm_username" type="text" placeholder="Username" class="form-control" />
            </div>
            <div class="form-group">
              <input name="LoginForm[password]" id="LoginForm_password" type="password" placeholder="Password" class="form-control" />
            </div>

            <button type="submit" name="yt0" value="Login" class="btn btn-success" />
              Sign in
            </button>
          </form>
          <?php }else{?>
            <ul class="navbar-form navbar-right" style="list-style:none;">
               <li style="float:right;margin-left:5px;"><a class="btn btn-success" href="<?php echo Yii::App()->createUrl('site/logout');?>" >Logout (<?php echo Yii::app()->user->username;?>)</a></li>
               <li style="float:right;margin-left:5px;"><span class="btn btn-success" style="cursor:auto;" ><?php echo Yii::app()->user->role;?></span></li>
               <li style="float:right;margin-left:5px;"><span class="btn btn-success" style="cursor:auto;" ><?php echo Yii::app()->user->realname;?></span></li>
            </ul>
            <?}
			?>
			
			
			
		
		<?php 
			$controllerId = Yii::app()->controller->id;
			if( isset( Yii::app()->user->id ) && Yii::app()->user->UserActive && ( $controllerId == 'task' ) ){
				$searchVal = '';
				if( Yii::app()->controller->action->id == 'search' && isset($_GET['keyword']) ){
					$searchVal = $_GET['keyword'];
				}
		?>
			<form class="navbar-form navbar-right" id="search-form" action="<?php echo Yii::App()->createUrl('task/search');?>" method="get" >
				<div class="form-group">
				  <input name="keyword" id="keyword" type="text" placeholder="Search Task" class="form-control" value="<?=$searchVal?>" />
				</div>
			</form>
		<?php }?>
			
			
			
			
			
			
			
			
			
			
			
			
        </div>
        <!--/.navbar-collapse -->
      </div>
    </div>
   <?php
	if( $controllerId == 'user' || $controllerId == 'category'  ){
		$jumbotronClass = '';
	}else{
		$jumbotronClass = 'no-padding';
	}
   ?>
    <div class="jumbotron <?=$jumbotronClass?>">
		<span id="user-created-msg"></span>
    </div>
    <!-- /container -->
    
    <div class="container">

	  <?php echo $content; ?>


<?php
if( !isset( Yii::app()->user->id ) || Yii::app()->user->UserActive == 0 ){
?>
	<h1>У вас нет доступа</h1>
	<h1>Обратитесь за доступом к админу блога</h1>
<?php }?>
	  
      <footer>
        <p>
          © TASK MANAGER 2015
        </p>
      </footer>
    </div>
  </body>

</html>