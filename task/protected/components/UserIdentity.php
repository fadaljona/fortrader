<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
/*
Yii::import('application.vendors.*');
require_once('phpass/class-phpass.php');*/

require_once( Yii::getPathOfAlias( 'application.vendors').'/phpass/class-phpass1.php' );

class UserIdentity extends CUserIdentity {
    
    protected $_id;
 

    public function authenticate(){

		$wp_hasher = new PasswordHash1(8, true);
		
        $user = User::model()->find('LOWER(username)=?', array(strtolower($this->username)));
		
		$wpuser = WpUsersImport::model()->find('LOWER(user_login)=?', array(strtolower($this->username)));
		
		$valid_pass=0;
		if( strlen($this->password) <= 32 ){
			$valid_pass = $wp_hasher->CheckPassword( $this->password, $wpuser->user_pass );
		}else{
			$valid_pass = $this->password == $wpuser->user_pass;
		}
		

		
        if(($user===null) || ( !$valid_pass  )) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {

            $this->_id = $user->id;

            $this->errorCode = self::ERROR_NONE;
        }
       return !$this->errorCode;
    }
 
    public function getId(){
        return $this->_id;
    }

}