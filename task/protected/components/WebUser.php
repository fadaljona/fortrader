<?php

class WebUser extends CWebUser {
    private $_model = null;
	public $allowAutoLogin=true;
 
    function getRole() {
        if($user = $this->getModel()){

            return $user->role;
        }
    }
    
    function getEmail() {
        if($user = $this->getModel()){
            $user = $this->getModel();

            return $user->email;
        }
    }
    function getRealName() {
        if($user = $this->getModel()){
            $user = $this->getModel();

            return $user->realName;
        }
    }
    function getUserName() {
        if($user = $this->getModel()){
            $user = $this->getModel();

            return $user->username;
        }
    }
	
	function getUserActive() {
        if($user = $this->getModel()){
            $user = $this->getModel();

            return $user->active;
        }
    }
 
    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = User::model()->findByPk($this->id, array('select' => 'role, email, realName, username, active'));
        }
        return $this->_model;
    }
}

?>