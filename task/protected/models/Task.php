<?php

/**
 * This is the model class for table "taskbook_task".
 *
 * The followings are the available columns in table 'taskbook_task':
 * @property integer $task_id
 * @property string $name
 * @property string $description
 * @property integer $cat_id
 * @property integer $manager_id
 * @property integer $worker_id
 * @property integer $created_by
 * @property integer $status
 * @property string $created_date
 */
class Task extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'taskbook_task';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, cat_id, manager_id, worker_id, created_by, status, priority', 'required'),
			array('cat_id, manager_id, worker_id, created_by, status,', 'numerical', 'integerOnly'=>true),
			array('priority', 'numerical', 'integerOnly'=>true, 'min'=>1, 'max'=>10),
			array('mresponse, wresponse', 'numerical', 'integerOnly'=>true, 'min'=>0, 'max'=>1),
			array('name', 'length', 'max'=>128),
			array('name', 'length', 'min'=>5),
			array('name', 'unique'),
			array('description', 'length', 'min'=>5),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('task_id, name, description, cat_id, manager_id, worker_id, created_by, status, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'category'=>array(self::HAS_ONE, 'Category', array('cat_id'=>'cat_id') ),
			'manager'=>array(self::HAS_ONE, 'User', array('id'=>'manager_id') ),
			'worker'=>array(self::HAS_ONE, 'User', array('id'=>'worker_id') ),
			'comment'=>array(self::HAS_MANY, 'Comment', array('task_id'=>'task_id'), 'order'=>'comment.created_date DESC' ),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'task_id' => 'Task',
			'name' => 'Name',
			'description' => 'Description',
			'cat_id' => 'Cat',
			'priority' => 'Priority',
			'manager_id' => 'Manager',
			'worker_id' => 'Worker',
			'created_by' => 'Created By',
			'status' => 'Status',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('manager_id',$this->manager_id);
		$criteria->compare('worker_id',$this->worker_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function getFilterData($filter_status, $filter_cat, $filter_worker, $filter_priority)
	{
		
		if(Yii::app()->user->role == 'administrator'){
			$sql_cond= '';
			$sql_cond_without_done= 'status<>5 and status<>4';
		}else{
			$sql_cond = "manager_id = ".Yii::app()->user->id." or worker_id = ".Yii::app()->user->id." or created_by = ".Yii::app()->user->id;
			$sql_cond_without_done='('.$sql_cond.') and status<>5 and status<>4';
		}
	
		$status_count = Yii::app()->db->createCommand()
					->select('status, COUNT( status ) as count')
					->from('taskbook_task')  //Your Table name
					->group('status') 
					->where( $sql_cond )
					->queryAll(); //Will get the all selected rows from table
		$status_counts=array();
		if ( count($status_count) )
		foreach($status_count as $stat_count){
			$status_counts[ $stat_count['status'] ] = $stat_count['count'];
		}
		
		$cats_count = Yii::app()->db->createCommand()
					->select('cat_id, COUNT( cat_id ) as count')
					->from('taskbook_task')  //Your Table name
					->group('cat_id') 
					->where( $sql_cond_without_done )
					->queryAll(); //Will get the all selected rows from table
		$cats_counts=array();
		if ( count($cats_count) )
		foreach($cats_count as $cat_count){
			$cats_counts[ $cat_count['cat_id'] ] = $cat_count['count'];
		}
		
		
		$criteria = new CDbCriteria();
		$criteria->condition = 'created_by = '.Yii::app()->user->id.' and status<>4';
		$count_my = Task::Model()->count($criteria);
		
		$criteria = new CDbCriteria();
		$criteria->condition = 'manager_id = '.Yii::app()->user->id.' and status = 3';
		$count_my_check = Task::Model()->count($criteria);
		
		$criteria = new CDbCriteria();
		$criteria->condition = 'worker_id = '.Yii::app()->user->id.' and status<>5 and status<>3 and status<>4';
		$count_my_work = Task::Model()->count($criteria);
		
		$criteria = new CDbCriteria();
		$criteria->condition = 'worker_id = '.Yii::app()->user->id.' and status = 3';;
		$count_my_out_check = Task::Model()->count($criteria);
		
		$cat = Category::getCats();
		
		$workers_for_filter = Yii::app()->db->createCommand()
					->select('worker_id, COUNT( worker_id ) as count, u.realName')
					->from('taskbook_task t')  //Your Table name
					->join('taskbook_user u', 't.worker_id=u.id')
					->group('t.worker_id') 
					->where( '(t.manager_id='.Yii::app()->user->id.' or t.created_by='.Yii::app()->user->id.') and status<>5' )
					->queryAll(); //Will get the all selected rows from table
					
		$priority_for_filter = Yii::app()->db->createCommand()
					->select('priority, COUNT( priority ) as count')
					->from('taskbook_task')  //Your Table name
					->group('priority') 
					->order('count') 
					->where( $sql_cond_without_done )
					->queryAll(); //Will get the all selected rows from table

				
		$filter_task_id=-1;
		if( isset( $_POST['task_id'] ) ){
			$filter_task_id = $_POST['task_id'];
		}
		return $dataForFilter = array(
			'status_count' => $status_counts,
			'count_my' => $count_my,
			'count_my_check' => $count_my_check,
			'count_my_work' => $count_my_work,
			'count_my_out_check' => $count_my_out_check,
			'cats' => $cat,
			'cats_count' => $cats_counts,
			'workers_for_filter' => $workers_for_filter,
			'priority_for_filter' => $priority_for_filter,
			'filter_status' => $filter_status,
			'filter_cat' => $filter_cat,
			'filter_worker' => $filter_worker,
			'filter_priority' => $filter_priority,

			'filter_task_id' => $filter_task_id
		);
		
	}
	
	
	public function getAjaxDataUpdater($cat_id, $status, $worker_id, $priority_id)
	{
		$filter_task_id=-1;
		if( isset( $_POST['task_id'] ) ){
			$filter_task_id = $_POST['task_id'];
		}
		return array(
			'cat_id'=> $cat_id, 
			'status'=>$status, 
			'worker_id' => $worker_id, 
			'priority_id' => $priority_id,
			'task_id'=> $filter_task_id
		);
		
	}
	
	public function getSqlFilter($post_cat_id, $post_status, $post_worker_id_filter, $post_priority_id){
		if( isset( $_POST['task_id'] ) ) {
			if( $_POST['task_id']==0 ){
				return '(t.manager_id='.Yii::app()->user->id.' and t.mresponse=1) or (t.worker_id='.Yii::app()->user->id.' and t.wresponse=1)';
			}else{
				return 't.task_id='.$_POST['task_id'];
			}
		}
		if(Yii::app()->user->role == 'administrator'){
			$sql_cond= '( 1=1 )';
		}else{
			$sql_cond = "(t.manager_id = ".Yii::app()->user->id." or t.worker_id = ".Yii::app()->user->id." or t.created_by = ".Yii::app()->user->id.")";
		}
		
		if( $post_cat_id ){
			$sql_cond .= " and ( t.cat_id = ".$post_cat_id.")";
		}else{
			$ajax_data_updater['cat_id']=0;
		}
		
		if( $post_status and $post_status < 10 ){
			$sql_cond .= " and ( t.status = ".$post_status.")";
		}elseif( $post_status == 10 ){
			$sql_cond .= " and ( t.created_by = ".Yii::app()->user->id.") and status<>4";
		}elseif( $post_status == 11 ){
			$sql_cond .= " and ( t.manager_id = ".Yii::app()->user->id.") and ( t.status=3 )";
		}elseif( $post_status == 12 ){
			$sql_cond .= " and ( t.worker_id = ".Yii::app()->user->id.") and status<>5 and status<>3 and status<>4";
		}elseif( $post_status == 13 ){
			$sql_cond .= " and ( t.worker_id = ".Yii::app()->user->id.") and ( t.status=3 )";
		}elseif( $post_status == 0 ){
			$sql_cond .= " and ( t.status<>5 and t.status<>4 and t.status<>3 )";
		}
		
		if($post_worker_id_filter) $sql_cond .= " and ( t.worker_id=".$post_worker_id_filter." )";
		
		if( $post_priority_id ) $sql_cond .= " and ( t.priority=".$post_priority_id." )";
		
		return $sql_cond;
		
	}
	public function getSearchFilter( $keyword ){
		
		$keyword = self::escapeLike( $keyword );
		$keyword = self::filterSearch( $keyword );

		$where = '( 1=1 )';
		if(Yii::app()->user->role != 'administrator'){
			$where .= " AND ( task.manager_id = :userId OR task.worker_id = :userId OR task.created_by = :userId ) ";
		}
		$where .= " AND ( task.name LIKE :keyword OR task.description LIKE :keyword OR comment.comment_text LIKE :keyword ) ";
		
		$finedTasks = Yii::app()->db->createCommand()
			->select('task.task_id')
			->from('taskbook_task task')  //Your Table name
			->join('taskbook_comment comment', 'comment.task_id = task.task_id')
			->where( $where, array( ':userId' => Yii::app()->user->id, ':keyword' => $keyword ) )
			->queryAll(); 
		$taskIds = array();
		foreach( $finedTasks as $finedTask ){
			$taskIds[] = $finedTask['task_id'];
		}
		$uniqueTaskIds = array_unique( $taskIds );
		$idsStr = implode( '", "', $uniqueTaskIds );
		$idsStr = '"' . $idsStr . '"';
		
		$c = new CDbCriteria();
		
		$c->with = array('category', 'manager', 'worker', 'comment', 'comment.user');
		$c->order = 't.last_comment DESC';

		$c->addCondition( "
			`t`.`task_id` IN ( $idsStr )
		");

		
		return $c;
		
	}
	static function filterSearch( $key ) {
		if( substr_count( $key, "*" )) {
			$key = str_replace( "*", "%", $key );
		}
		elseif( substr_count( $key, '"' ) ){
			$key = str_replace( '"', "", $key );
		}
		else{
			$key = "%{$key}%";
		}
		return $key;
	
	}
	static function escapeLike( $text ) {
		return strtr( $text, Array( '%' => '\%', '_' => '\_', '\\' => '\\\\' ));
	}
	
	
	public function getFilterStatus($status)
	{
		if( isset( $status ) ) {
			$post_status = $status; 
			Yii::app()->session->add("status", $post_status);
		}else $post_status = Yii::app()->session->get("status");;
		
		if( !isset( $post_status ) ) $post_status=0;
		
		return $post_status;
		
	}
	
	public function getFilterWorker($worker_id)
	{
		if( isset( $worker_id ) ) {
			$worker_id_filter = $worker_id; 
			Yii::app()->session->add("worker_id_filter", $worker_id_filter);
		}else $worker_id_filter = Yii::app()->session->get("worker_id_filter");;
		
		if( !isset( $worker_id_filter ) ) $worker_id_filter=0;
		
		return $worker_id_filter;
		
	}
	
	public function getFilterPriority($priority_id)
	{
		if( isset( $priority_id ) ) {
			$priority_id_filter = $priority_id; 
			Yii::app()->session->add("priority_id_filter", $priority_id_filter);
		}else $priority_id_filter = Yii::app()->session->get("priority_id_filter");;
		
		if( !isset( $priority_id_filter ) ) $priority_id_filter=0;
		
		return $priority_id_filter;
		
	}
	
	public function getFilterCat($cat)
	{
		if( isset( $cat ) ) {
			$post_cat_id = $cat; 
			Yii::app()->session->add("post_cat_id", $post_cat_id);
		}else{ $post_cat_id = Yii::app()->session->get("post_cat_id");}
		
		if( !isset( $post_cat_id ) ) $post_cat_id=0;
		
		return $post_cat_id;
	}
	
	public function sendTaskEmail($task_model, $message_id, $status_label, $comment_model)
	{
	
		$model_worker = User::model()->findByPk($task_model->worker_id);
		$model_manager = User::model()->findByPk($task_model->manager_id);
		$model_creator = User::model()->findByPk($task_model->created_by);

			
		$setto_array = array_unique( array($model_worker->email, $model_manager->email, $model_creator->email ) );
			
		if(($key = array_search( Yii::app()->user->email ,$setto_array)) !== FALSE){
			unset($setto_array[$key]);
		}

			
		switch ( $message_id ) {
			case 1:
				$postdata = array( 
					'subject' => 'Статус задачи изменен: '.$task_model->name, 
					'body' => 'Статус задачи изменен '."\n\n\n".
							$task_model->name."\n".
							Yii::App()->createAbsoluteURL( 'task/view', array( 'id' => $task_model->task_id ) ) ."\n\n\n".
							'Новый статус: '.$status_label, 
					'setto' => $setto_array,
					);
				break;
				
			case 2:
				$postdata = array(
					'subject' => 'Новая задача: '.$task_model->name, 
					'body' => 'Для вас создана новая задача '."\n\n\n".
								$task_model->name."\n".
								Yii::App()->createAbsoluteURL( 'task/view', array( 'id' => $task_model->task_id ) ), 
					'setto' => $setto_array,
					);
				break;
			case 3:
				$postdata = array( 
					'subject' => 'Новый комментарий: '.$task_model->name, 
					'body' => 'Добавлен новый комментарий к задаче '."\n\n\n".
								$task_model->name."\n".
								Yii::App()->createAbsoluteURL( 'task/view', array( 'id' => $task_model->task_id ) )."\n\n\n".
								'Коментарий: '.$comment_model->comment_text, 
					'setto' => $setto_array,
					);
				break;
		}	

		foreach( $postdata['setto'] as  $setto ){
			$message_mail_subject = $postdata['subject'];
			$message_mail_body = $postdata['body'];
			wp_mail( $setto, $message_mail_subject, $message_mail_body );
		}
		
	}
	
	public static function objToArray($obj) 
	{
		$vars = array();
		foreach ($obj as $name => $value){
			$vars[$name] = $value;
		}
		return $vars;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Task the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
