<?php

/**
 * This is the model class for table "taskbook_comment".
 *
 * The followings are the available columns in table 'taskbook_comment':
 * @property integer $comment_id
 * @property integer $task_id
 * @property string $comment_text
 * @property integer $user_id
 * @property string $created_date
 */
class Comment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'taskbook_comment';
	}
	
	protected function beforeSave(){
	
		$task_model = Task::model()->findByPk($this->task_id);
		$task_model->last_comment=$this->created_date;
		
		if( $task_model->worker_id != $task_model->manager_id ){
		
			if( $task_model->worker_id == Yii::app()->user->id )
				$task_model->mresponse=1;
			
			if( $task_model->manager_id == Yii::app()->user->id )
				$task_model->wresponse=1;
			
		}
		
		$task_model->save();
		return parent::beforeSave();
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('task_id, comment_text, user_id', 'required'),
			array('task_id, user_id', 'numerical', 'integerOnly'=>true),
			array('created_date', 'safe'),
			array('comment_text', 'length', 'min'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('comment_id, task_id, comment_text, user_id, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>array(self::HAS_ONE, 'User', array('id'=>'user_id') ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'comment_id' => 'Comment',
			'task_id' => 'Task',
			'comment_text' => 'Comment Text',
			'user_id' => 'User',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('comment_id',$this->comment_id);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('comment_text',$this->comment_text,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	
	public function getListRender($task_id)
	{
		$dataProvider=new CActiveDataProvider('Comment', array(
							'criteria'=>array(
								'condition'=> "( task_id = ".$task_id.")",
								'with'=>array( 'user'),
								'order'=>'t.created_date DESC',
								),
							'pagination'=>array(
													'pageSize'=>500,
											),
							)
							);
		return Yii::app()->controller->renderPartial('/comment/_update_ajax_comments_list', array( 'dataProvider' => $dataProvider, 'task_id' => $task_id ),true );		
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
