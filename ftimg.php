<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/yii/yiilite.php';
$config = $_SERVER['DOCUMENT_ROOT']."/services/protected/config/default.php";
Yii::$enableIncludePath = false;
$app = Yii::createWebApplication( $config );


Yii::import( 'models.JournalModel' );
Yii::import( 'models.LanguageModel' );
Yii::import( 'models.JournalI18NModel' );

$criteria = new CDbCriteria();
$criteria->order = 'id DESC';
$criteria->limit = 1;
$model = JournalModel::model()->find($criteria);
if( !$model ) $this->throwI18NException( "Can't find EA!" );

$img = $_SERVER['DOCUMENT_ROOT'].getCoverWidth( 'width:180', $model->nameCover );
	
$mimeType = CFileHelper::getMimeType($img);

header("Content-type: {$mimeType}");
switch($mimeType) {
	case 'image/jpeg':
		$image = imagecreatefromjpeg($img);
		imagejpeg($image);
		break;
	case 'image/png':
		$image = imagecreatefrompng($img);
		imagepng($image);
		break;
	default:
		$image = imagecreate(100, 100);
		break;
}
imagedestroy($image);
Yii::app()->end();

function getCoverWidth( $style, $nameCover ) {
		
	$srcCover = strlen($nameCover) ? 'uploads/journals'."/{$nameCover}" : '';
	$srcCover = '/services/'.$srcCover;
		
	preg_match('/width:([0-9]+).*/',$style,$width_match);
	$width = $width_match[1];
		
	$image_src = $_SERVER['DOCUMENT_ROOT'].$srcCover;
		
	if( !file_exists( $image_src ) ) return $srcCover;
		
	if( isset($width) ){
		$image_src_ext = substr($srcCover, strrpos($srcCover, '.' ) );
		$image_src_without_ext = substr($srcCover, 0, strrpos($srcCover, '.' ) );
				
		if( file_exists( $_SERVER['DOCUMENT_ROOT'].$image_src_without_ext.'_'.$width.$image_src_ext ) ) 
			return $image_src_without_ext.'_'.$width.$image_src_ext;
				
				
		$image = new EasyImage( $image_src );
		$image->resize( $width );
		$image->save( $_SERVER['DOCUMENT_ROOT'].$image_src_without_ext.'_'.$width.$image_src_ext );
				
		return $image_src_without_ext.'_'.$width.$image_src_ext;
	}else
		return $srcCover;
}

?>
