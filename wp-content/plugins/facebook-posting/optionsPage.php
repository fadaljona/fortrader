<?php

add_action('admin_menu', 'facebook_posting_menu');

function facebook_posting_menu() {
	add_menu_page(__('Facebook posting settings', 'facebook_posting'), __('Facebook posting', 'facebook_posting'), 'manage_options', 'facebook-posting', 'facebook_posting_settings_page' , plugins_url('/images/icon.png', __FILE__) );
	add_submenu_page('facebook-posting', __('Facebook posting > Manage settings', 'facebook_posting'), __('Settings', 'facebook_posting'), 'manage_options', 'facebook-posting', 'facebook_posting_settings_page');
    add_submenu_page('facebook-posting', __('Facebook posting > Create Campaing', 'facebook_posting'), __('Create Campaing', 'facebook_posting'), 'manage_options', 'facebook-posting-create-campaing', 'facebook_posting_create_campaing');
    add_submenu_page('facebook-posting', __('Facebook posting > Publication queue', 'facebook_posting'), __('Publication queue', 'facebook_posting'), 'manage_options', 'facebook-posting-publication-queue', 'facebook_posting_publication_queue');
	
	add_action( 'admin_init', 'register_facebook_posting_settings' );
}



function register_facebook_posting_settings() {
	register_setting( 'facebook-posting-settings-group', 'facebook-posting-settings' );
}
function facebook_posting_create_campaing(){

	$fbPoster = FacebookPoster::getInstance();
	
	if( !$fbPoster || !$fbPoster->checkSettedAdAccId() ){
		echo "<h1>Please update settings</h1>";
		return false;
	}
	
	if( isset( $_POST['fb-campaing_noncename'] ) ){
		
		if ( !wp_verify_nonce( $_POST['fb-campaing_noncename'], plugin_basename(__FILE__) )) {
			echo 'nonce not valid';
			return false;
			
		}
		if( $_POST['fb-campaing_name'] ){
			$res = $fbPoster->createCampaing( $_POST['fb-campaing_name'] );
			if( is_array($res) ){
				echo "<h1>{$res['errMess']}</h1>";
			}else{
				echo "<h1>Campaing $res created</h1>";
			}
		}
	}
	
?>
<div class="wrap">
	<h1><?php _e('Create campaing for boost posts ads', 'facebook_posting');?></h1>
	<form method="post" action="/wp-admin/admin.php?page=facebook-posting-create-campaing">
		<input type="hidden" name="fb-campaing_noncename" id="fb-campaing_noncename" value="<?php echo wp_create_nonce( plugin_basename(__FILE__) ); ?>" />
		<input type="text" name="fb-campaing_name" id="fb-campaing_name" />
		
		<input type="submit" value="<?php _e('Create', 'facebook_posting');?>" class="button button-primary" id="submit" name="submit">
	</form>
</div>

<?php
}

function facebook_posting_publication_queue()
{
    $crons = get_option('cron');
?>
    <div class="wrap">
        <h1><?php _e('Publication queue', 'facebook_posting');?></h1>
        <table class="widefat fixed">
            <tr><th><?php _e('Task time', 'facebook_posting');?></th><th><?php _e('Now time', 'facebook_posting');?></th><th><?php _e('Post', 'facebook_posting');?></th></tr>
        <?php
        foreach ($crons as $time => $tasks) {
            foreach ($tasks as $name => $taskDetails) {
                if ($name != 'run_posting_to_fb_action') {
                    continue;
                }
                foreach ($taskDetails as $arr) {
                    echo "<tr class=\"alternate\"><td>" . date('Y-m-d H:i:s', $time) . "</td><td>".date('Y-m-d H:i:s')."</td><td>" . get_permalink($arr['args'][0]) . '</td></tr>';
                }
            }
        }
        ?>
        </table>
    </div>
    
<?php
}

function facebook_posting_settings_page() {
    $goals = [
        'APP_INSTALLS',
        'ENGAGED_USERS',
        'EVENT_RESPONSES',
        'LINK_CLICKS',
        'NONE',
        'OFFER_CLAIMS',
        'OFFSITE_CONVERSIONS',
        'PAGE_LIKES',
        'POST_ENGAGEMENT',
        'REACH',
        'VIDEO_VIEWS',
        'IMPRESSIONS',
    ];
    $billingEvents = [
        'APP_INSTALLS',
        'CLICKS',
        'IMPRESSIONS',
        'LINK_CLICKS',
        'MULTI_PREMIUM',
        'OFFER_CLAIMS',
        'PAGE_LIKES',
        'POST_ENGAGEMENT',
        'VIDEO_VIEWS'
    ];
?>
<div class="wrap">
<h1><?php _e('Facebook posting settings', 'facebook_posting');?></h1>

<div class="updated notice notice-success is-dismissible below-h2" id="message">
	<p>
		<?php _e('The application must have the permission <b>manage_pages</b> and <b>ads_management</b>', 'facebook_posting');?><br />
		<?php _e('For getting permission <b>for app</b> go to <a href="https://developers.facebook.com/tools/explorer/" target="_blank">https://developers.facebook.com/tools/explorer/</a>, select your app and <a href="http://prntscr.com/c3ibs1">http://prntscr.com/c3ibs1</a> <a href="http://prntscr.com/c3ic2w">http://prntscr.com/c3ic2w</a>', 'facebook_posting');?><br /><br />
		<?php _e('For getting long-lived user access token go to <a href="https://developers.facebook.com/tools/accesstoken/" target="_blank">https://developers.facebook.com/tools/accesstoken/</a>, find your app and for <b>User Token</b> click <b>Debug</b>, then click <b>Extend Access Token</b>', 'facebook_posting');?>
	</p>
	<button class="notice-dismiss" type="button"><span class="screen-reader-text"><?php _e('Close', 'facebook_posting');?></span></button>
</div>


<form method="post" action="options.php">
	<?php 
		settings_fields( 'facebook-posting-settings-group' );
		do_settings_sections( 'facebook-posting-settings-group' ); 
		$settings = get_option('facebook-posting-settings');
		
	?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('app_id', 'facebook_posting');?></th>
        <td><input type="text" name="facebook-posting-settings[app_id]" value="<?php echo esc_attr( $settings['app_id'] ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('app_secret', 'facebook_posting');?></th>
        <td><input type="text" name="facebook-posting-settings[app_secret]" value="<?php echo esc_attr( $settings['app_secret'] ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('page-id', 'facebook_posting');?></th>
        <td><input type="text" name="facebook-posting-settings[page-id]" value="<?php echo esc_attr( $settings['page-id'] ); ?>" /></td>
        </tr>
		
		<tr valign="top">
        <th scope="row"><?php _e('long-lived User Token', 'facebook_posting');?></th>
        <td><input type="text" name="facebook-posting-settings[ll-user-token]" value="<?php echo esc_attr( $settings['ll-user-token'] ); ?>" /></td>
        </tr>
		
		<tr valign="top">
        <th scope="row"><?php _e('New ad status', 'facebook_posting');?></th>
        <td>
			<select name="facebook-posting-settings[ad-status]">
				<option value="PAUSED" <?php if( $settings['ad-status'] == 'PAUSED' ) echo 'selected'; ?>>PAUSED</option>
				<option value="ACTIVE" <?php if( $settings['ad-status'] == 'ACTIVE' ) echo 'selected'; ?>>ACTIVE</option>
			</select>
		</td>
        </tr>
		
		<tr valign="top">
        <th scope="row"><?php _e('AD_ACCOUNT_ID', 'facebook_posting');?></th>
        <td><input type="text" name="facebook-posting-settings[ad-account-id]" value="<?php echo esc_attr( $settings['ad-account-id'] ); ?>" /></td>
        </tr>
		<tr valign="top">
        <th scope="row"><?php _e('Daily budget for new adSet', 'facebook_posting');?></th>
        <td><input type="text" name="facebook-posting-settings[daily-budget]" value="<?php echo esc_attr( $settings['daily-budget'] ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Optimization Goal', 'facebook_posting');?></th>
        <td>
            <select name="facebook-posting-settings[optimization-goal]">
            <?php 
            foreach ($goals as $goal) {
                $selected = '';
                if ($settings['optimization-goal'] == $goal) {
                    $selected = 'selected';
                }
                echo "<option value='$goal' $selected>$goal</option>";
            }
            ?>
            </select>
            <br><a href="https://developers.facebook.com/docs/marketing-api/bidding-and-optimization" target="_blank">https://developers.facebook.com/docs/marketing-api/bidding-and-optimization</a>
        </td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Billing event', 'facebook_posting');?></th>
        <td>
            <select name="facebook-posting-settings[billing-event]">
            <?php 
            foreach ($billingEvents as $event) {
                $selected = '';
                if ($settings['billing-event'] == $event) {
                    $selected = 'selected';
                }
                echo "<option value='$event' $selected>$event</option>";
            }
            ?>
            </select>
            <br><a href="https://developers.facebook.com/docs/marketing-api/bidding-and-optimization" target="_blank">https://developers.facebook.com/docs/marketing-api/bidding-and-optimization</a>
        </td>
        </tr>
		
        <?php 
			$fbPoster = FacebookPoster::getInstance();
			if( isset($settings['ad-account-id']) && $settings['ad-account-id'] && $fbPoster ){
				$auds = $fbPoster->getSavedAudiences();
				if( $auds === false ){
					
					$auds = $fbPoster->getSavedAudiences('',true);
					echo $auds;
					
					echo 'some error occured while receiving saved audiences';
					return false;
				}
		?>
			<tr valign="top">
			<th scope="row"><?php _e('Saved audience', 'facebook_posting');?></th>
			<td>
				<select name="facebook-posting-settings[audience-id]">
				<?php
					foreach( $auds as $aud ){
						$selected = '';
						if( $settings['audience-id'] == $aud['id'] ) $selected = 'selected';
						echo "<option value='{$aud['id']}' $selected>{$aud['name']}</option>";
					}
				?>
				</select>
			</td>
			</tr>
			
			<tr valign="top">
			<th scope="row"><?php _e('DEVICE_PLATFORMS', 'facebook_posting');?></th>
			<td>
				<input type="checkbox" name="facebook-posting-settings[device-platforms][]" 
					<?php 
						if( isset($settings['device-platforms']) && is_array($settings['device-platforms']) && in_array( 'mobile', $settings['device-platforms'] )) echo 'checked';
					?>  
				value="mobile">mobile 
				<input type="checkbox" name="facebook-posting-settings[device-platforms][]" 
					<?php 
						if( isset($settings['device-platforms']) && is_array($settings['device-platforms']) && in_array( 'desktop', $settings['device-platforms'] )) echo 'checked';
					?>  
				value="desktop">desktop 
			</td>
			</tr>
			
			<tr valign="top">
			<th scope="row"><?php _e('PUBLISHER_PLATFORMS', 'facebook_posting');?></th>
			<td>
				<input type="checkbox" name="facebook-posting-settings[publisher-platforms][]" 
					<?php 
						if( isset($settings['publisher-platforms']) && is_array($settings['publisher-platforms']) && in_array( 'facebook', $settings['publisher-platforms'] )) echo 'checked';
					?>  
				value="facebook">facebook 
				<input type="checkbox" name="facebook-posting-settings[publisher-platforms][]" 
					<?php 
						if( isset($settings['publisher-platforms']) && is_array($settings['publisher-platforms']) && in_array( 'audience_network', $settings['publisher-platforms'] )) echo 'checked';
					?> 
				value="audience_network">audience_network <a href="https://www.facebook.com/business/help/788333711222886">https://www.facebook.com/business/help/788333711222886</a>
			</td>
			</tr>
			
			<tr valign="top">
			<th scope="row"><?php _e('FACEBOOK_POSITIONS', 'facebook_posting');?></th>
			<td>
				<input type="checkbox" name="facebook-posting-settings[facebook-positions][]" 
					<?php 
						if( isset($settings['facebook-positions']) && is_array($settings['facebook-positions']) && in_array( 'feed', $settings['facebook-positions'] )) echo 'checked';
					?> 
				value="feed">feed 
				<input type="checkbox" name="facebook-posting-settings[facebook-positions][]" 
					<?php 
						if( isset($settings['facebook-positions']) && is_array($settings['facebook-positions']) && in_array( 'right_hand_column', $settings['facebook-positions'] )) echo 'checked';
					?> 
				value="right_hand_column">right_hand_column 
			</td>
			</tr>
			
			<tr valign="top">
			<th scope="row"><?php _e('BID_AMOUNT', 'facebook_posting');?></th>
			<td><input type="text" name="facebook-posting-settings[bid-amount]" value="<?php echo esc_attr( $settings['bid-amount'] ); ?>" /></td>
			</tr>
			
			
			<tr valign="top">
			<th scope="row"><?php _e('Campaing for boost posts', 'facebook_posting');?></th>
			<td>
				<input type="text" name="facebook-posting-settings[campaing-id]" value="<?php echo esc_attr( $settings['campaing-id'] ); ?>" /> <b><?php echo $fbPoster->getNameAndStatusSelectedCampaing(); ?></b>
			</td>
			</tr>
			
		<?php } ?>
        
		
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php }