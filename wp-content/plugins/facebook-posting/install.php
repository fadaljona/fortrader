<?php

global $jal_db_version;
$jal_db_version = '1.0';

function jal_install() {
	global $wpdb;
	global $jal_db_version;

	$table_name = $wpdb->prefix . 'facebook_posting_posts_log';
	$table_name2 = $wpdb->prefix . 'facebook_posting_ads_log';
	
	$charset_collate = $wpdb->get_charset_collate();
	
	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
		id bigint(20) UNSIGNED NOT NULL,
		publicationId varchar(255) DEFAULT '' NOT NULL,
		errorMess varchar(255) DEFAULT '' NOT NULL,
		timeStamp int(11) DEFAULT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;
	
	CREATE TABLE IF NOT EXISTS $table_name2 (
		id bigint(20) UNSIGNED NOT NULL,
		adSetId varchar(255) DEFAULT '' NOT NULL,
		adCreativeId varchar(255) DEFAULT '' NOT NULL,
		adId varchar(255) DEFAULT '' NOT NULL,
		startTimeStamp int(11) DEFAULT NULL,
		endTimeStamp int(11) DEFAULT NULL,
		errorMess varchar(255) DEFAULT '' NOT NULL,
		budget int(11) UNSIGNED NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;
	";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'jal_db_version', $jal_db_version );
}

function jal_uninstall(){
	global $wpdb;
	$table_name = $wpdb->prefix . 'facebook_posting_posts_log';
	$wpdb->query( "DROP TABLE IF EXISTS $table_name" );
	delete_option("facebook-posting-settings");
}

register_activation_hook( $pluginFile, 'jal_install' );
register_uninstall_hook( $pluginFile, 'jal_uninstall' );