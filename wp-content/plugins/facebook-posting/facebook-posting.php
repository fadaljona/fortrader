<?php
/*
Plugin Name: Posting to facebook
Description: post messagies and ads to facebook
Version: 1.0
Author: Vekshin Vladimir akadem87@gmail.com
*/

$pluginFile = __FILE__;

require_once dirname($pluginFile).'/loadTextdomain.php';
require_once dirname($pluginFile).'/install.php';
require_once dirname($pluginFile).'/FacebookPoster.php';
require_once dirname($pluginFile).'/optionsPage.php';
require_once dirname($pluginFile).'/postsOptions.php';
require_once dirname($pluginFile).'/actions.php';