<?php

add_action( 'add_meta_boxes', 'add_post_date_metaboxes' );
function add_post_date_metaboxes() {
	add_meta_box('facebook_posting_block', __('Facebook posting', 'facebook_posting'), 'facebook_posting_block', 'post', 'side', 'high');
}

function facebook_posting_block() {
	
	$fbSettings = get_option('facebook-posting-settings');
	
	$fbPoster = FacebookPoster::getInstance();
	
	
	if( !$fbPoster ){
		echo "please fill all <a href='/wp-admin/admin.php?page=facebook-posting%2FoptionsPage.php'><b>settings</b></a><br><br>
		";
	}else{
		global $post;
		
		$fbData = FacebookPoster::getPostingData( $post->ID );
		
		
		
		if( $fbData && $fbData->publicationId ){
			echo '<b>'.__('Fb publication id', 'facebook_posting') . '</b> ' . $fbData->publicationId . '<br /><br />';
		}
		if( $fbData && $fbData->errorMess ){
			echo '<b>'.__('Fb error message', 'facebook_posting') . '</b> ' . $fbData->errorMess . '<br /><br />';
		}
		
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jquery-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
		wp_enqueue_script('post-metabox.js', plugin_dir_url( __FILE__ ) . 'js/post-metabox.js', array('jquery'));
		echo '<input type="hidden" name="fb-msg_noncename" id="fb-msg_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

		
		$fb_data_style = '';
		if( !$fbData || !$fbData->publicationId ){
			$post_to_fb = get_post_meta($post->ID, 'post_to_fb', true);
			$post_to_fb_checked = '';
			$fb_data_style = "style='display:none;'";
			if( $post_to_fb ){
				$post_to_fb_checked = 'checked="checked"';
				$fb_data_style = '';
            } 
            if ($post_to_fb_checked) {
                echo '<input type="hidden" id="post_to_fb" name="post_to_fb" value="1">Scheduled ';
            } else {
                echo '<input type="checkbox" id="post_to_fb" name="post_to_fb" value="1">';
            }
			echo __('Post to facebook', 'facebook_posting') . '<br><br>';
			
		}
		$fb_msg = get_post_meta($post->ID, 'fb_msg', true);
		
		echo "<div class='fb-data' $fb_data_style>";

			if( !$fbData || !$fbData->publicationId ){
				echo __('Enter facebook message', 'facebook_posting') . ' <textarea name="fb-msg" id="fb-msg" value="'.$fb_msg.'" class="widefat">'.$fb_msg.'</textarea><br />';
			}elseif( $fb_msg ){
				echo __('Facebook message', 'facebook_posting') . ':<br /> '.$fb_msg.'<br /><br />';
			}
			
			$upload_link = esc_url( get_upload_iframe_src( 'image', $post->ID ) );
			$your_img_id = get_post_meta( $post->ID, 'fb_post_img', true );
			$your_img_src = wp_get_attachment_image_src( $your_img_id, 'full' );
			$you_have_img = is_array( $your_img_src );
		?>
			<div class="custom-img-container">
				<?php if ( $you_have_img ) : ?>
					<img src="<?php echo $your_img_src[0] ?>" alt="" style="max-width:100%;" />
				<?php endif; ?>
			</div>
			<?php if( !$fbData || !$fbData->publicationId ){?>
			<p class="hide-if-no-js">
				<a class="upload-custom-img <?php if ( $you_have_img  ) { echo 'hidden'; } ?>" 
				   href="<?php echo $upload_link ?>">
					<?php _e('Set image for facebook', 'facebook_posting') ?>
				</a>
				<a class="delete-custom-img <?php if ( ! $you_have_img  ) { echo 'hidden'; } ?>" 
				  href="#">
					<?php _e('Remove this image', 'facebook_posting') ?>
				</a>
			</p>
			<input class="fb-post-img" name="fb-post-img" type="hidden" value="<?php echo esc_attr( $your_img_id ); ?>" />
			<?php } ?>
			
			
			
			
			
			<?php 
				$adDataForPost = FacebookPoster::getAdsData($post->ID);
				
				if( $adDataForPost && $adDataForPost->adSetId ) echo "adSetId: {$adDataForPost->adSetId}<br />";
				if( $adDataForPost && $adDataForPost->adCreativeId ) echo "adCreativeId: {$adDataForPost->adCreativeId}<br />";
				if( $adDataForPost && $adDataForPost->adId ) echo "adId: {$adDataForPost->adId}<br />";
				
				if( !$adDataForPost || !$adDataForPost->adId ){
					if( $adDataForPost && $adDataForPost->errorMess ){
						echo __('Fb ads error message', 'facebook_posting') . ' ' . $adDataForPost->errorMess . "<br>";
					}
					
			?>
			<?php $post_to_fb_ads = get_post_meta($post->ID, 'post_to_fb_ads', true); ?>
			<input type="checkbox" id="post_to_fb_ads" name="post_to_fb_ads" <?php if( !$post_to_fb_ads || $post_to_fb_ads == 1 ) echo 'checked';?> value="1"><?php _e('Post to facebook ads', 'facebook_posting'); ?><br><br>
			
			<div class="fb-data-ads">
				
				<?php 
					$fb_ads_days = get_post_meta($post->ID, 'fb_ads_days', true); 
					if( !$fb_ads_days ) $fb_ads_days = 1;
				?>	
				<?php _e('Fb ads lifetime budget', 'facebook_posting'); ?><br>
				<input type="hidden" id="fb_ads_daily_budget" name="fb_ads_daily_budget"  value="<?php echo $fbSettings['daily-budget']?>">
				<input type="hidden" id="fb_ads_days" name="fb_ads_days"  value="<?php echo $fb_ads_days;?>">
				<input type="text" id="fb_ads_lifetime_budget" name="fb_ads_lifetime_budget" disabled  value="<?php echo $fbSettings['daily-budget'] * $fb_ads_days?>"><br><br>
				
				<?php
					wp_enqueue_script('jquery-ui-datepicker');
					wp_enqueue_style('jquery-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
				?>
				<?php _e('Fb ads end time', 'facebook_posting'); ?><br>
				<input type="text" id="fb_ads_end_time" name="fb_ads_end_time" <?php if( !current_user_can( 'administrator' ) ) echo "disabled";?>  value="<?php echo date('Y-m-d', time()+60*60*24 * $fb_ads_days)?>"><br><br>
			</div>
			
			<?php }else{ 
				
				_e('Fb ads lifetime budget', 'facebook_posting'); echo ' ' . $adDataForPost->budget . '<br>'; 
				_e('Fb ads start time', 'facebook_posting'); echo ' ' . date( 'Y-m-d H:i:s', $adDataForPost->startTimeStamp) . '<br>'; 
				_e('Fb ads end time', 'facebook_posting'); echo ' ' . date( 'Y-m-d H:i:s', $adDataForPost->endTimeStamp) . '<br>'; 
			
			 }?>
			
		</div>
<?php	
	}
}

function save_posting_to_fb_info($post_id, $post) {
	if ( !isset($_POST['fb-msg_noncename']) || !wp_verify_nonce( $_POST['fb-msg_noncename'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;
	
	$arrToSave = array();
	
	if( isset($_POST['post_to_fb']) && $_POST['post_to_fb'] ){
		$arrToSave['fb_msg'] = $_POST['fb-msg'];
		$arrToSave['fb_post_img'] = $_POST['fb-post-img'];
		$arrToSave['post_to_fb'] = $_POST['post_to_fb'];
	}else{
		$arrToSave['fb_msg'] = false;
		$arrToSave['fb_post_img'] = false;
		$arrToSave['post_to_fb'] = false;
	}
	if( isset($_POST['post_to_fb_ads']) && $_POST['post_to_fb_ads'] ){
		$arrToSave['post_to_fb_ads'] = $_POST['post_to_fb_ads'];
		$arrToSave['fb_ads_days'] = $_POST['fb_ads_days'];
	}else{
		$arrToSave['post_to_fb_ads'] = -1;
		$arrToSave['fb_ads_days'] = false;
	}
	
	foreach ($arrToSave as $key => $value) { 
		if( $post->post_type == 'revision' ) return; 
			
		update_post_meta($post->ID, $key, $value);
		if(!$value) delete_post_meta($post->ID, $key); 
	}
}
add_action('save_post', 'save_posting_to_fb_info', 1, 2); 

function run_posting_to_fb($post_id, $post) {
	if( $post->post_status != 'publish' ) return false;
    if( $post->post_type != 'post' ) return false;

    $permalink = get_permalink($post_id);
	if( preg_match('/(\?p=[0-9]+)$/', $permalink, $matches) || preg_match('/(\/[0-9]+)\.html$/', $permalink, $matches) ) return false;
    
    $post_to_fb = get_post_meta($post->ID, 'post_to_fb', true);
    $post_to_fb_scheduled = get_post_meta($post->ID, 'post_to_fb_scheduled', true);
    if ($post_to_fb && !$post_to_fb_scheduled) {
        wp_schedule_single_event(time() + 60*15, 'run_posting_to_fb_action', array($post->ID));
        update_post_meta($post->ID, 'post_to_fb_scheduled', 1);
    }

    $post_to_fb_ads = get_post_meta($post->ID, 'post_to_fb_ads', true);
	if( $post_to_fb_ads == 1 ){
		
		$fbData = FacebookPoster::getPostingData( $post_id );
		if( !$fbData || !$fbData->publicationId )return false;
		
		$fbPoster = FacebookPoster::getInstance();
		$fb_ads_days = get_post_meta($post->ID, 'fb_ads_days', true);
		$fbPoster->postToAds($post_id, $fb_ads_days);
	}
    
}

add_action('save_post', 'run_posting_to_fb', 10, 2);
