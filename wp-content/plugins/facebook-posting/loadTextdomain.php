<?php

add_action('plugins_loaded', 'facebook_posting_load_textdomain');
function facebook_posting_load_textdomain() {
	load_plugin_textdomain( 'facebook_posting', false, dirname( plugin_basename(__FILE__) ) . '/lang' );
}