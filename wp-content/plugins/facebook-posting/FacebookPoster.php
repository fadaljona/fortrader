<?php
require_once 'vendor/autoload.php';


use FacebookAds\Api;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Object\Values\BillingEvents;
use FacebookAds\Object\Values\OptimizationGoals;
use FacebookAds\Object\Targeting;
use FacebookAds\Object\Fields\TargetingFields;
use FacebookAds\Object\Fields\TargetingSpecsFields;
use FacebookAds\Object\Values\PageTypes;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Ad;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Logger\CurlLogger;

class FacebookPoster{
	protected static $instance;
	
	private $fb;
	private $fbSettings;
	private $pageAccessToken = false;
	private $errorMess = false;
	private $postingLogTable;
	private $adsLogTable;
    private $fbAdsInited = false;
    
	
	private function __construct() { }
	private function __clone() { }
	private function __wakeup() { }
	
	public static function getInstance() {
		if ( is_null(self::$instance) ) {
			self::$instance = new FacebookPoster;
			
			self::$instance->fbSettings = get_option('facebook-posting-settings');
			
			if( !self::$instance->fbSettings['app_id'] || !self::$instance->fbSettings['app_secret'] || !self::$instance->fbSettings['ll-user-token'] || !self::$instance->fbSettings['page-id'] ) 
				return false;
			
			self::$instance->fb = new Facebook\Facebook(array(
				'app_id' => self::$instance->fbSettings['app_id'],
				'app_secret' => self::$instance->fbSettings['app_secret'],
			));
			
			
			self::$instance->postingLogTable = self::getTableName();
			self::$instance->adsLogTable = self::getAdsTableName();
			
			self::$instance->initFbAds();
			
		}
		return self::$instance;
    }
	public static function getTableName(){
		global $wpdb;
		return $wpdb->prefix . 'facebook_posting_posts_log';
	}
	public static function getAdsTableName(){
		global $wpdb;
		return $wpdb->prefix . 'facebook_posting_ads_log';
	}
	public static function getPostingData( $postId ) {
		global $wpdb;
		$row = $wpdb->get_row( $wpdb->prepare( 
			"
				SELECT *
				FROM ".self::getTableName()." 
				WHERE id = %d
			", 
			$postId
		));
		return $row;
    }
	public static function getAdsData( $postId ) {
		global $wpdb;
		$row = $wpdb->get_row( $wpdb->prepare( 
			"
				SELECT *
				FROM ".self::getAdsTableName()." 
				WHERE id = %d
			", 
			$postId
		));
		return $row;
    }
	public function checkSettedAdAccId(){
		if( !isset( $this->fbSettings['ad-account-id'] ) || !$this->fbSettings['ad-account-id'] ) return false;
		return true;
	}
	private function getPageAccessToken(){
		if( !$this->pageAccessToken ) $this->detPageAccessToken();
		return $this->pageAccessToken;
	}
	private function detPageAccessToken(){
		try {
			$getPageToken = $this->fb->get("/{$this->fbSettings['page-id']}/?fields=access_token", $this->fbSettings['ll-user-token']);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$this->errorMess = 'Graph returned an error: ' . $e->getMessage();
			return false;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$this->errorMess = 'Facebook SDK returned an error: ' . $e->getMessage();
			return false;
		}
		
		if( $getPageToken->getHttpStatusCode() != 200 ){
			$this->errorMess = 'Http status code of PageAccessToken response not 200 ';
			return false;
		}
		
		$getPageTokenBody = $getPageToken->getDecodedBody();
		
		if( !$getPageTokenBody['access_token'] ){
			$this->errorMess = "No page access_token in body of response";
			return false;
		}
		$this->pageAccessToken = $getPageTokenBody['access_token'];
		return true;
	}
	private function savePostingLog($postId, $publicationId, $errorMessage){
		global $wpdb;
		$wpdb->replace( 
			$this->postingLogTable,
			array( 
				'id' => $postId,
				'publicationId' => $publicationId, 
				'errorMess' => $errorMessage,
				'timeStamp' => time(),
			), 
			array( 
				'%d',
				'%s', 
				'%s',
				'%d'
			) 
		);
		
	}
	private function getIdFromAdsLog($postId){
		global $wpdb;
		return $id = $wpdb->get_var( $wpdb->prepare( 
			"
				SELECT id 
				FROM {$this->adsLogTable} 
				WHERE id = %d
			", 
			$postId
		));
	}
	public function updateAdsLog($postId, $updateArr, $types){
		global $wpdb;
		

		
		if( $this->getIdFromAdsLog($postId) ){
			$wpdb->update(
				$this->adsLogTable,
				$updateArr,
				array( 'id' => $postId ), 
				$types,
				array( '%d' ) 
			);
		}else{
			$insertArr = $updateArr;
			$insertTypes = $types;
			$insertArr['id'] = $postId;
			$insertTypes[] = '%d';
			$wpdb->insert( 
				$this->adsLogTable,
				$insertArr,
				$insertTypes 
			);
		}
	}
	private function checkIfPublished($postId){
		global $wpdb;
		$publicationId = $wpdb->get_var( $wpdb->prepare( 
			"
				SELECT publicationId 
				FROM {$this->postingLogTable} 
				WHERE id = %d
			", 
			$postId
		));
		if( $publicationId ) return $publicationId;
		return false;
	}
	public function postToPage( $postId, $postData = array() ){
			
		if( $this->checkIfPublished($postId) ) return true;
		$pageAccessToken = $this->getPageAccessToken();
		if( !$pageAccessToken ){
			if( $this->errorMess ){
				$this->savePostingLog($postId, '', $this->errorMess);
			}
			return false;
		}
		
		$fbMessageData = array();
		$fbMessageData['link'] = get_permalink($postId);
		if( isset($postData['message']) && $postData['message'] ) $fbMessageData['message'] = $postData['message'];
		if( isset($postData['picture']) && $postData['picture'] ) $fbMessageData['picture'] = $postData['picture'];
		
		try {
			$postMessage = $this->fb->post(
				"/{$this->fbSettings['page-id']}/feed", 
				$fbMessageData,
				$pageAccessToken
			);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$this->savePostingLog($postId, '', 'Graph returned an error: ' . $e->getMessage());
			return false;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$this->savePostingLog($postId, '', 'Facebook SDK returned an error: ' . $e->getMessage());
			return false;
		}
		
		$postMessageBody = $postMessage->getDecodedBody();
		
		if( !$postMessageBody['id'] ){
			$this->savePostingLog($postId, '', "No publication id in response");
			return false;
		} 

		if( $postMessage->getHttpStatusCode() != 200 ){
			$this->savePostingLog($postId, '', 'Http status code of posting response not 200 ');
			return false;
		}
		
		$this->savePostingLog($postId, $postMessageBody['id'], '');
		return true;
	}
	private function getTargeting($postId){
		
		$savedAudiences = $this->getSavedAudiences('fields=name,id,targeting');
		
		if( !$savedAudiences ){
			$this->updateAdsLog($postId, array( 'errorMess' => "Can't get saved audiences" ), array('%s') );
			return false;
		}
		
		foreach( $savedAudiences as $aud ){
			if( $aud['id'] == $this->fbSettings['audience-id'] ){
				$targeting = new FacebookAds\Object\Targeting();
				foreach( $aud['targeting'] as $targetingField => $fieldVal ){
                    $targeting->{$targetingField} = $fieldVal;
				}
				break;
			} 
		}

		if( !$targeting ){
			$this->updateAdsLog($postId, array( 'errorMess' => "no targetting setted" ), array('%s') );
			return false;
		} 
        
		//https://developers.facebook.com/docs/marketing-api/targeting-specs/
		
		if( isset( $this->fbSettings['device-platforms'] ) && is_array( $this->fbSettings['device-platforms'] ) )
			$targeting->device_platforms = $this->fbSettings['device-platforms'];
		
		if( isset( $this->fbSettings['publisher-platforms'] ) && is_array( $this->fbSettings['publisher-platforms'] ) )
			$targeting->publisher_platforms = $this->fbSettings['publisher-platforms'];
		
		if( isset( $this->fbSettings['facebook-positions'] ) && is_array( $this->fbSettings['facebook-positions'] ) )
			$targeting->facebook_positions = $this->fbSettings['facebook-positions'];
		
		return $targeting;
	}
	private function createAdSet( $postId, $lifetimeBudget, $fbAdsDays, $adDataForPost ){
		
		if( $adDataForPost && $adDataForPost->adSetId ) return $adDataForPost->adSetId;
		
		$targeting = $this->getTargeting($postId);
		if( !$targeting ) return false;
		
		$startDateTime = new \DateTime("NOW");
		$start_time = $startDateTime->format(DateTime::ISO8601);
		$endDateTime = new \DateTime("+$fbAdsDays day");
		$end_time = $endDateTime->format(DateTime::ISO8601);

		$adset = new FacebookAds\Object\AdSet(null, 'act_' . $this->fbSettings['ad-account-id'] );

        $adset->setData(array(
			FacebookAds\Object\Fields\AdSetFields::NAME => 'Ad Set for ' . wp_trim_words(get_the_title($postId), 10, ''),
			FacebookAds\Object\Fields\AdSetFields::OPTIMIZATION_GOAL => $this->fbSettings['optimization-goal'],
			FacebookAds\Object\Fields\AdSetFields::BILLING_EVENT => $this->fbSettings['billing-event'],
			FacebookAds\Object\Fields\AdSetFields::BID_AMOUNT => $this->fbSettings['bid-amount']*100,
			FacebookAds\Object\Fields\AdSetFields::LIFETIME_BUDGET => $lifetimeBudget*100,
			FacebookAds\Object\Fields\AdSetFields::CAMPAIGN_ID => (string)$this->fbSettings['campaing-id'],
			FacebookAds\Object\Fields\AdSetFields::TARGETING => $targeting,
			FacebookAds\Object\Fields\AdSetFields::START_TIME => $start_time,
			FacebookAds\Object\Fields\AdSetFields::END_TIME => $end_time,
		));

		try {
			$adset->validate()->create( array( FacebookAds\Object\AdSet::STATUS_PARAM_NAME => $this->getAdStatus(),));
		}  catch(FacebookAds\Http\Exception\RequestException $e) {
			$this->updateAdsLog($postId, array( 'errorMess' => 'AdSet creating error: ' . $e->getMessage() . ' ' . $e->getErrorUserMessage() . ' ' . $e->getErrorUserTitle() ), array('%s') );
			return false;
		} catch(Exception $e) {
			$this->updateAdsLog($postId, array( 'errorMess' => 'AdSet creating error: ' . $e->getMessage() ), array('%s') );
			return false;
		}

		$this->updateAdsLog($postId, array( 'adSetId' => $adset->id, 'errorMess' => '', 'startTimeStamp' => $startDateTime->getTimestamp(), 'endTimeStamp' => $endDateTime->getTimestamp(), 'budget' => $lifetimeBudget ), array('%s', '%s', '%d', '%d', '%d') );
		return $adset->id;
	}
	private function getAdStatus(){
		if( !isset( $this->fbSettings['ad-status'] ) ){
			return 'PAUSED';
		}else{
			return $this->fbSettings['ad-status'];
		}
	}
	private function createAdCreative($postId, $publicationId, $adDataForPost){
		if( $adDataForPost && $adDataForPost->adCreativeId ) return $adDataForPost->adCreativeId;
		
		$creative = new AdCreative(null, 'act_' . $this->fbSettings['ad-account-id']);

		$creative->setData(array(
			AdCreativeFields::OBJECT_STORY_ID => $publicationId
		));

		try {
			$creative->create();
		} catch(Exception $e) {
			$this->updateAdsLog($postId, array( 'errorMess' => 'AdCreative creating error: ' . $e->getMessage() ), array('%s') );
			return false;
		}

		$this->updateAdsLog($postId, array( 'adCreativeId' => $creative->id, 'errorMess' => '' ), array('%s', '%s') );
		return $creative->id;
	}
	private function createAd($postId, $adSetId, $adCreativeId ){
		
		$data = array(
			AdFields::NAME => 'Ad ' . wp_trim_words( get_the_title($postId), 10, '' ),
			AdFields::ADSET_ID => (string)$adSetId,
			AdFields::CREATIVE => array(
				'creative_id' => (string)$adCreativeId,
			),
		);

		$ad = new Ad(null, 'act_' . $this->fbSettings['ad-account-id']);
		$ad->setData($data);


		try {
			$ad->create(array(
			  Ad::STATUS_PARAM_NAME => $this->getAdStatus(),
			));
		} catch(Exception $e) {
			$this->updateAdsLog($postId, array( 'errorMess' => 'Ad creating error: ' . $e->getMessage() ), array('%s') );
			return false;
		}
		
		$this->updateAdsLog($postId, array( 'adId' => $ad->id, 'errorMess' => '' ), array('%s', '%s') );
		return $ad->id;
		
	}
	public function postToAds( $postId, $fbAdsDays ){
		if( current_user_can( 'administrator' ) ){
			$lifetimeBudget = $fbAdsDays * $this->fbSettings['daily-budget'];
		}else{
			$lifetimeBudget = $this->fbSettings['daily-budget'];
			$fbAdsDays = 1;
		}
		
		$postingData = self::getPostingData($postId);
		if( !$postingData || !$postingData->publicationId ) return false;
		
		$adDataForPost = self::getAdsData($postId);
		if( $adDataForPost && $adDataForPost->adId ) return false;
		
		$this->initFbAds();
		$adSetId = $this->createAdSet( $postId, $lifetimeBudget, $fbAdsDays, $adDataForPost );
		if( !$adSetId ) return false;
		
		
		$adCreativeId = $this->createAdCreative($postId, $postingData->publicationId, $adDataForPost);
		if( !$adCreativeId ) return false;
		
		$adId = $this->createAd($postId, $adSetId, $adCreativeId );
		
	}
	public function getSavedAudiences( $fieldsStr = '', $returnMessage = false ){
		
		try {
			$savedAudiences = $this->fb->get( 'act_' . $this->fbSettings['ad-account-id'] . '/saved_audiences?' . $fieldsStr, $this->fbSettings['ll-user-token']);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			if( $returnMessage ) return 'Graph returned an error: ' . $e->getMessage();
			return false;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			if( $returnMessage ) return 'Facebook SDK returned an error: ' . $e->getMessage();
			return false;
		}
		
		if( $savedAudiences->getHttpStatusCode() != 200 ){
			if( $returnMessage ) return 'Http status code of get saved Audiences response not 200';
			return false;
		}
		
		$outArr = array();
		
		$feedEdge = $savedAudiences->getGraphEdge();

		if( $feedEdge ){
			foreach ($feedEdge as $status) {
				$outArr[] = $status->asArray();
			}
			
			$nextFeed = $this->fb->next($feedEdge);
			while( $nextFeed ){
				foreach ($nextFeed as $status) {
					$outArr[] = $status->asArray();
				}
				$nextFeedd = $this->fb->next($nextFeed);
				$nextFeed = $nextFeedd;
			}
		}

		return $outArr;
	}
	public function getNameAndStatusSelectedCampaing(){

		if( !isset($this->fbSettings['campaing-id']) || !$this->fbSettings['campaing-id'] ) return 'Campaing not selected';
		
		$campaign = new Campaign((string)$this->fbSettings['campaing-id']);
		
		try {
			$campaign->read(array(
				CampaignFields::ID,
                CampaignFields::NAME,
                CampaignFields::OBJECTIVE,
			));
		} catch(Exception $e) {
			return 'Reading campaing error: ' . $e->getMessage();
		}
		
		return $campaign->name;

	}
	private function initFbAds(){
		if( !$this->fbAdsInited ){
            $api = FacebookAds\Api::init($this->fbSettings['app_id'], $this->fbSettings['app_secret'], $this->fbSettings['ll-user-token']);
			$this->fbAdsInited = true;
		}
	}
	public function createCampaing( $name ){
		$this->initFbAds();
		
		$campaign = new FacebookAds\Object\Campaign(null, 'act_' . $this->fbSettings['ad-account-id'] );
		$campaign->setData(array(
			FacebookAds\Object\Fields\CampaignFields::NAME => $name,
			FacebookAds\Object\Fields\CampaignFields::OBJECTIVE => FacebookAds\Object\Values\AdObjectives::POST_ENGAGEMENT,
		));
		
		
		try {
			$campaign->create(array(
			  FacebookAds\Object\Campaign::STATUS_PARAM_NAME => $this->getAdStatus(),
			));
		} catch(Exception $e) {
			return array( 'errMess' => 'Ads api returned an error: ' . $e->getMessage() );
		}

		return $campaign->id;
				
    }
    public function scrapeForUrl($url)
    {	
        $originUrl = $url;
        $url = urlencode($url);

		$pageAccessToken = $this->getPageAccessToken();
		if( !$pageAccessToken ){
            echo "Can't get pageAccessToken\n";
			return false;
		}
		
		$data = array('scrape' => 'true');
		try {
			$postMessage = $this->fb->post(
				"/?id=$url", 
				$data,
				$pageAccessToken
			);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage() . "\n";
			return false;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage(). "\n";
			return false;
		}
		
        $postMessageBody = $postMessage->getDecodedBody();
		
		if( !isset($postMessageBody['id']) ){
			echo "No id in response $originUrl\n";
			return false;
        }
        
        if( !isset($postMessageBody['title']) ){
			echo "No title in response $originUrl\n";
			return false;
        }

        if( !isset($postMessageBody['image']) || !count($postMessageBody['image']) ){
			echo "No images in response $originUrl\n";
			return false;
        }

        if (!isset($postMessageBody['description'])) {
            echo "No description in response $originUrl\n";
			return false;
        }

		return true;
    }
    public static function runPostingToFb($postId)
    {
        $fbPoster = self::getInstance();
        $urlToCheck = get_permalink($postId);
        
        if (!$fbPoster->scrapeForUrl($urlToCheck)) {
            wp_schedule_single_event(time() + 60*15, 'run_posting_to_fb_action', array($postId));
            return false;
        }

    
		$fb_post_img = get_post_meta($postId, 'fb_post_img', true);
		$fb_msg = get_post_meta($postId, 'fb_msg', true);
		$your_img_src = wp_get_attachment_image_src( $fb_post_img, 'full' );
		$you_have_img = is_array( $your_img_src );
		$imgSrc = '';
		if( $you_have_img ) $imgSrc = $your_img_src[0];
		
		$fbPoster->postToPage($postId, array( 'message' => $fb_msg, 'picture' => $imgSrc ));
	
	
        $post_to_fb_ads = get_post_meta($postId, 'post_to_fb_ads', true);
        if( $post_to_fb_ads == 1 ){
            $fbData = self::getPostingData( $postId );
            if( !$fbData || !$fbData->publicationId )return false;
            
            $fb_ads_days = get_post_meta($postId, 'fb_ads_days', true);
            $fbPoster->postToAds($postId, $fb_ads_days);
        }
    }
	
}