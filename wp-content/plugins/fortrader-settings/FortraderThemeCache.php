<?php

class FortraderCache{
	public $keys = array();
	public $durations = array();

	public function beginCache( $key, $duration ){
		$key .= '.' . get_locale();
		if ( isset( $_SERVER['HTTPS'] ) ) {
			if ( 'on' == strtolower( $_SERVER['HTTPS'] ) ) {
				$key = $key . ".HTTPS";
			}
			if ( '1' == $_SERVER['HTTPS'] ) {
				$key = $key . ".HTTPS";
			}
        }
		
		$content = get_transient($key);
		if( !$content ){
			array_push( $this->keys, $key);
			array_push( $this->durations, $duration);
			ob_start(); 
			return true;
		}
		if( $content ){
			echo $content;
			return false;
		}
	}
	public function endCache(){
		$key = array_pop( $this->keys );
		$duration = array_pop( $this->durations );
		$content = ob_get_contents();
		ob_end_clean();
		echo $content;
		set_transient($key, $content, $duration);
	}
}