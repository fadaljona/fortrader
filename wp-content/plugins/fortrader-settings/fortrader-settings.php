<?php
/*
Plugin Name: Fortrader settings
Description: wp, yii, theme functions
Version: 1.0
Author: Vekshin Vladimir akadem87@gmail.com
*/

add_action('plugins_loaded', 'fortrader_settings_load_textdomain');
function fortrader_settings_load_textdomain() {
	load_plugin_textdomain( 'fortrader_settings', false, dirname( plugin_basename(__FILE__) ) . '/lang' );
}

require_once dirname(__FILE__).'/FortraderThemeCache.php';
global $fortraderCache;
$fortraderCache = new FortraderCache;


remove_action( 'after_password_reset', 'wp_password_change_notification' );

add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
	// Добавляет расширение 'extension' и его mime type 'mime/type'
	$existing_mimes['extension'] = 'mime/type';
	// добавляйте столько сколько вам нужно
	$existing_mimes['doc'] = 'application/msword';
	$existing_mimes['mq4'] = 'text/plain';
	$existing_mimes['mq5'] = 'text/plain';
	$existing_mimes['ex4'] = 'text/plain';
	$existing_mimes['ex5'] = 'text/plain';
	$existing_mimes['tpl'] = 'text/plain';
	$existing_mimes['zip'] = 'application/zip';
	 
	return $existing_mimes;
}

/* Подсчет количества посещений страниц
---------------------------------------------------------- */
add_action('wp_head', 'kama_postviews');
function kama_postviews() {

	/* ------------ Настройки -------------- */
	$meta_key		= 'views';	// Ключ мета поля, куда будет записываться количество просмотров.
	$who_count 		= 0; 			// Чьи посещения считать? 0 - Всех. 1 - Только гостей. 2 - Только зарегистрированых пользователей.
	$exclude_bots 	= 1;			// Исключить ботов, роботов, пауков и прочую нечесть :)? 0 - нет, пусть тоже считаются. 1 - да, исключить из подсчета.
	/* СТОП настройкам */

	global $user_ID, $post;
	if(is_singular()) {
		$id = (int)$post->ID;
		static $post_views = false;
		if($post_views) return true; // чтобы 1 раз за поток
		$post_views = (int)get_post_meta($id,$meta_key, true);
		$should_count = false;
		switch( (int)$who_count ) {
			case 0: $should_count = true;
				break;
			case 1:
				if( (int)$user_ID == 0 )
					$should_count = true;
				break;
			case 2:
				if( (int)$user_ID > 0 )
					$should_count = true;
				break;
		}
		if( (int)$exclude_bots==1 && $should_count ){
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			$notbot = "Mozilla|Opera"; //Chrome|Safari|Firefox|Netscape - все равны Mozilla
			$bot = "Bot/|robot|Slurp/|yahoo"; //Яндекс иногда как Mozilla представляется
			if ( !preg_match("/$notbot/i", $useragent) || preg_match("!$bot!i", $useragent) )
				$should_count = false;
		}

		if($should_count){
			$post_views = $post_views+1;
			update_post_meta($id, $meta_key, $post_views);
			global $wpdb;
			$wpdb->update( 
				'ft_posts_stats_common', 
				array( 
					'postViews' => $post_views,
				), 
				array( 'postId' => $id )
			);
		}
	}
	return true;
}


//преобразуе дни в дату daylife при апдейте метаданных поста  - custom field
function my_update_post_meta($meta_id, $object_id, $meta_key, $_meta_value) {
	$meta_values = get_post_meta( $object_id, 'daylife', true );
	if( !empty($meta_values) ) {
		$datetest=date_parse($meta_values);
		if( $datetest['error_count']!=0 ) {
			$current_date = new DateTime("now");
			$current_date->add(new DateInterval('P'.$meta_values.'D'));
			$current_date->format('Y-m-d H:i:s');
			update_post_meta($object_id, 'daylife', $current_date->format('Y-m-d H:i:s'));
			
		}
	}
	$meta_values = get_post_meta( $object_id, 'actualDayLife', true );
	if( !empty($meta_values) ) {
		$datetest=date_parse($meta_values);
		if( $datetest['error_count']!=0 ) {
			$current_date = new DateTime("now");
			$current_date->add(new DateInterval('P'.$meta_values.'D'));
			$current_date->format('Y-m-d H:i:s');
			update_post_meta($object_id, 'actualDayLife', $current_date->format('Y-m-d H:i:s'));
			
		}
	}
}
add_action( 'updated_postmeta', 'my_update_post_meta', 10, 4 );

//экспорт юзера при регистрации
add_action( 'user_register', 'export_user_to_mailerlite', 10, 1 );
function export_user_to_mailerlite( $user_id ) {

    $user=get_userdata($user_id);

    require_once(get_template_directory().'/unisender/functions.php');
    
    $locale = get_locale();
	if( $locale == 'ru_RU' ){
		$group_id = get_option('unisender_group_id');
	}else{
		$group_id = get_option('unisender_group_id_' . $locale);
	}
	$result = addUserToUnisender($user->data->user_email, $group_id);
}

//redirects for outbound links
add_action( 'init', 'wpse36168_add_rewrite_rule' );
/**
 * Add our rewrite rule
 */
function wpse36168_add_rewrite_rule()
{
	add_rewrite_rule(
		'^go/(.*?)$',
		'index.php?go=$matches[1]',
		'top'
	);
}

add_filter( 'query_vars', 'wpse36168_add_go_var' );
/**
 * Tell WP not to strip out or "go" query var
 */
function wpse36168_add_go_var( $vars )
{
	$vars[] = 'go';
	return $vars;
}

add_action( 'template_redirect', 'wpse36168_catch_external' );
/**
 * Catch external links from our "go" url and redirect them
 */
function wpse36168_catch_external()
{
	if( $url = get_query_var( 'go' ) )
	{
		wp_redirect( esc_url( $url ), 302 );
		exit();
	}
}


function get_formated_category_parents( $id, $link = false, $lastcat = false, $separator = '', $nicename = false, $visited = array() ) {
	global $category_breadcrumb_marker;
	global $positionCount;
	$chain = '';
	$parent = get_term( $id, 'category' );
	if ( is_wp_error( $parent ) )
		return $parent;

	$name = $parent->name;

	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
		$visited[] = $parent->parent;
		$category_breadcrumb_marker++;
		$positionCount--;
		$chain .= get_formated_category_parents( $parent->parent, $link, $separator, $nicename, $visited );
	}

	if ( $link ){
		
		if( $lastcat && !$category_breadcrumb_marker)
			1==1;
		else
			$chain .= '<li><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . esc_url( get_category_link( $parent->term_id ) ) . '" title="' . esc_attr( sprintf( __( "%s" ), $parent->name ) ) . '" itemprop="item"><span itemprop="name" class="breadcrumb-active">'.$name.'</span></a><meta itemprop="position" content="'.($positionCount+2).'" /></span></li>';
		
		$category_breadcrumb_marker--;
		$positionCount++;

	}else
		$chain .= $name.$separator;
	return $chain;
}



function fix_category_404($query) {
	if( is_category() ){
		if( strpos('_'.$_SERVER['REQUEST_URI'],'/wp-admin/') || strpos('_'.$_SERVER['REQUEST_URI'],'?category_name') ) return true;
		$langSlug = pll_current_language('slug');

		$cats = explode( '/', $_SERVER['REQUEST_URI'] );

		foreach( $cats as $cat ){
			if( $cat == $langSlug ) continue;
			if( $cat && strpos( $cat, '?' ) === false ) {
				if( $cat == 'page' ) break;
				$cat_by_slug = get_category_by_slug($cat);
				if( ! $cat_by_slug->cat_ID ){
					set_query_var('category_name', md5( time() ) );
					break;
				}
			}
		}
	}
}
add_action('parse_query','fix_category_404');

function quotes_tax_init() {
	register_taxonomy(
		'quotes',
		'post',
		array(
			'labels' => array(
				'name' => __( 'Quotes' ),
				'all_items' => __( 'All Quotes' ),
				'add_new_item' => __( 'Add New Quote Cat' ),
				'edit_item' => __( 'Edit Quote' ),
			), 
			'public' => true,
			'rewrite' => array( 'slug' => 'quotesTax' ),
			'show_tagcloud' => false,
			'hierarchical' => true,
			'show_in_nav_menus' => true,
			'meta_box_cb' => 'post_categories_meta_box',
			'capabilities' => array(
				'manage_terms' => true,
				'edit_terms' => true,
				'delete_terms' => false,
				'assign_terms' => true,
			)
		)
	);
}
add_action( 'init', 'quotes_tax_init' );

function comment_custom_avatar($html, $mixed, $size, $default, $alt){

	global $comment;
	if( !$comment ){
		$user_id = $mixed;
	}else{
		$user_id =  $comment->user_id;
		if( $user_id ){
			$imgUserId = $user_id;
		}else{
			$imgUserId = $comment->comment_author_IP;
		}
	}
	if( !$user_id ) $user_id=0;

	$author = get_userdata( $user_id ); 
	$authorName = getShowUserName($author);
	
	if( !isset($imgUserId) || !$imgUserId ) $imgUserId = $author->ID;
	
	return '<img src="'.p75GetServicesAuthorThumbnail( $size, $size, $imgUserId ).'"  alt="'.$authorName. '" title="'.$authorName.'" />';
}
add_filter( 'get_avatar', 'comment_custom_avatar', 10, 5 );



function updateCity($user_login, $user) {
	if( !$user->data->city || !$user->data->idCountry ){
		loadYii();
		$currUserModel = UserModel::model()->findByPk( $user->ID );
		
		include( WP_CONTENT_DIR."/geo/SxGeo.php" );
		$SxGeo = new SxGeo( WP_CONTENT_DIR."/geo/SxGeoCity.dat");
		$ipInfo = $SxGeo->getCityFull($_SERVER['REMOTE_ADDR']);
		
		if( !$user->data->city && $ipInfo['city']['name_ru'] ) $currUserModel->city = $ipInfo['city']['name_ru'];
		if( !$user->data->idCountry && $ipInfo['country']['iso'] ){
			$countryModel = CountryModel::model()->find(Array(
				'condition' => " `t`.`alias` = :iso ",
				'params' => Array(
					':iso' => strtolower( $ipInfo['country']['iso'] ),
				),
			));
			if( $countryModel ){
				$currUserModel->idCountry = $countryModel->id;
			}
		}
		$currUserModel->save();
	}
}
add_action('wp_login', 'updateCity', 10, 2);

function customExcerpt( $str, $length ){
	if( strlen( $str ) <= $length ) return $str;
	$startSearch = 80;
	$endStr = substr( $str, $startSearch, strlen( $str ) );
	$dotPos = strpos($endStr, '.');
	if( $dotPos == FALSE ) return substr( $str, 0, 100 ).'...';
	return substr( $str, 0, $startSearch+$dotPos ).'...';
}

function transient_theme_flush(){
	global $wpdb;
	$sql = "
			DELETE FROM $wpdb->options
			WHERE 
				option_name LIKE '_transient_theme%' OR
				option_name LIKE '_transient_timeout_theme%' OR
				option_name LIKE '_transient_comments%' OR
				option_name LIKE '_transient_timeout_comments%'
			";
	wp_cache_flush();
	return $wpdb->query( $sql );
}

add_action('admin_menu', 'theme_cache_menu');
function theme_cache_menu() {
	add_options_page(__('Fortrader settings', 'fortrader_settings'), __('Fortrader settings', 'fortrader_settings'), 'manage_options', 'theme-settings-menu', 'common_theme_settings');
}

function common_theme_settings(){
	if( isset( $_GET['action'] ) && $_GET['action'] == 'clear' ){
		if( transient_theme_flush() !== false ){
			echo '<div class="updated notice notice-success is-dismissible below-h2" id="message"><p>'.__('Cache removed', 'fortrader_settings').'</p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Close.</span></button></div>';
		}
	}
?>

<div class="wrap">
<h2><?php _e('Common settings page', 'fortrader_settings')?></h2>
	<form method="post" action="options.php">
		<?php wp_nonce_field('update-options'); ?>
		<?php echo '<input type="hidden" name="settings_noncename" id="settings_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';?>
		
		<h3><?php _e('Cache theme blocks', 'fortrader_settings')?></h3>
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><label for="theme_cache_duration"><?php _e('Cache time in seconds', 'fortrader_settings')?></label></th>
				<td><input type="number" min="60" step="60" class="small-text" name="theme_cache_duration" size="25" value='<?php echo get_option('theme_cache_duration');?>' /></td>
			</tr>
			<tr valign="top">
				<td colspan="2">
					<a class="add-new-h2" href="/wp-admin/options-general.php?page=theme-settings-menu&action=clear"><?php _e('Clear Cache'); ?></a>
				</td>
			</tr>
		</table>
			
		<h3><?php _e('Unisender settings', 'fortrader_settings')?></h3>
		<table class="form-table">
			<tr valign="top"><th scope="row"><label for="unisender_group_id"><?php _e('Group Id', 'fortrader_settings')?></label></th>
				<td><input type="text" name="unisender_group_id" size="25" value='<?php echo get_option('unisender_group_id');?>' /></td>
			</tr>
			<tr valign="top"><th scope="row"><label for="unisender_group_id_en_US"><?php _e('En group Id', 'fortrader_settings')?></label></th>
				<td><input type="text" name="unisender_group_id_en_US" size="25" value='<?php echo get_option('unisender_group_id_en_US');?>' /></td>
			</tr>
		</table>
		
		<h3><?php _e('Questions and answers', 'fortrader_settings')?></h3>
		<table class="form-table">
			<tr valign="top"><th scope="row"><label for="questions_cat_id"><?php _e('Category Id', 'fortrader_settings')?></label></th>
				<td><input type="text" name="questions_cat_id" size="25" value='<?php echo get_option('questions_cat_id');?>' /></td>
			</tr>
		</table>
		
		<h3><?php _e('Services', 'fortrader_settings')?></h3>
		<table class="form-table">
			<tr valign="top"><th scope="row"><label for="services_cat_id"><?php _e('Category Id', 'fortrader_settings')?></label></th>
				<td><input type="text" name="services_cat_id" size="25" value='<?php echo get_option('services_cat_id');?>' /></td>
			</tr>
		</table>
		
		<h3><?php _e('Fortrader contacts', 'fortrader_settings')?></h3>
		<table class="form-table">
			<tr valign="top"><th scope="row"><label for="fortraderPubName"><?php _e('fortraderPubName', 'fortrader_settings')?></label></th>
				<td><input type="text" name="fortraderPubName" size="25" value='<?php echo get_option('fortraderPubName');?>' /></td>
			</tr>
			<tr valign="top"><th scope="row"><label for="fortraderStreetAddress"><?php _e('Street Address', 'fortrader_settings')?></label></th>
				<td><input type="text" name="fortraderStreetAddress" size="25" value='<?php echo get_option('fortraderStreetAddress');?>' /></td>
			</tr>
			<tr valign="top"><th scope="row"><label for="fortraderPostalCode"><?php _e('Postal Code', 'fortrader_settings')?></label></th>
				<td><input type="text" name="fortraderPostalCode" size="25" value='<?php echo get_option('fortraderPostalCode');?>' /></td>
			</tr>
			<tr valign="top"><th scope="row"><label for="fortraderAddressLocality"><?php _e('Address Locality', 'fortrader_settings')?></label></th>
				<td><input type="text" name="fortraderAddressLocality" size="25" value='<?php echo get_option('fortraderAddressLocality');?>' /></td>
			</tr>
			<tr valign="top"><th scope="row"><label for="fortraderTelephone"><?php _e('Telephone', 'fortrader_settings')?></label></th>
				<td><input type="text" name="fortraderTelephone" size="25" value='<?php echo get_option('fortraderTelephone');?>' /></td>
			</tr>
		</table>
		
		<h3><?php _e('Search', 'fortrader_settings')?></h3>
		<table class="form-table">
			<tr valign="top"><th scope="row"><label for="search_page_id"><?php _e('Search page Id', 'fortrader_settings')?></label></th>
				<td><input type="text" name="search_page_id" size="25" value='<?php echo get_option('search_page_id');?>' /></td>
			</tr>
		</table>
		
		<h3><?php _e('Comments', 'fortrader_settings')?></h3>
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><label for="comments_stop_words"><?php _e('Comments stop words', 'fortrader_settings')?></label></th>
				<td>
					<textarea rows="5" cols="50" name="comments_stop_words" value='<?php echo get_option('comments_stop_words');?>'><?php echo get_option('comments_stop_words');?></textarea>
				</td>
			</tr>
		</table>
		
		<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="theme_cache_duration,unisender_group_id, unisender_group_id_en_US,questions_cat_id, services_cat_id, fortraderStreetAddress, fortraderPubName, fortraderPostalCode, fortraderAddressLocality, fortraderTelephone, search_page_id, comments_stop_words" />			
	</form>
</div>
<?php
}
function delete_theme_cache(){
	 //transient_theme_flush();
	 file_put_contents( dirname(__FILE__) . '/removeWpFileCache.txt', '1');
}
add_action( 'save_post', 'delete_theme_cache' );

function loadYii(){ 
	if( !class_exists( 'Yii' ) ){
		//error_reporting( E_ALL );
		require_once dirname(__FILE__) .'/../../../yii/yii.php';
		$config = dirname(__FILE__) ."/../../../services/protected/config/default.php";
		require_once dirname(__FILE__) .'/../../../services/protected/components/sys/WebApplication.php';
		Yii::$enableIncludePath = false;
		
		$app = new WebApplication( $config );
		
		Yii::setPathOfAlias('webroot',dirname(__FILE__) .'/../../../services');
		

		$availableLanguages = LanguageModel::getExistsAliases();
		$cookieKey = Yii::App()->params[ 'languageCookieVarName' ];
		$cookieLanguage = &$_COOKIE[ $cookieKey ];
		$user = Yii::App()->user->getModel();
		if( !empty( $cookieLanguage )) {
			if( in_array( $cookieLanguage, $availableLanguages )) {
				Yii::app()->setLanguage( $cookieLanguage );
				if( $user and $user->language != $cookieLanguage ) {
					$user->language = $cookieLanguage;
					$user->save();
				}
				return;
			}
		}
		if( $user and $user->language ) {
			if( in_array( $user->language, $availableLanguages )) {
				Yii::app()->setLanguage( $user->language );
				return;
			}
		}
		if( $user and !$user->language ) {
			$preferredLanguages = Yii::App()->request->getPreferredLanguages();
			$preferredLanguages = array_intersect( $preferredLanguages, $availableLanguages );
			if( $preferredLanguages ) {
				$language = array_shift( $preferredLanguages );
				Yii::app()->setLanguage( $language );
				$user->language = $language;
				$user->save();
				return;
			}
		}
		

	}
}

add_action( 'init', 'load_yii_for_profile_avatar' );
function load_yii_for_profile_avatar(){
	global $pagenow;
	if( $pagenow == 'profile.php' || $pagenow == 'user-edit.php' ){
		loadYii();
	}
}
add_action( 'init', function(){
	if( $_SERVER['REQUEST_URI'] == '/' || substr( $_SERVER['REQUEST_URI'], 1, 1 ) == '?' || substr( $_SERVER['REQUEST_URI'], 0, 6 ) == '/page/' ) loadYii();
} );

add_action( 'all_admin_notices', 'edit_avatar_in_user_profile' );
function edit_avatar_in_user_profile(){
	global $pagenow;
	if( $pagenow == 'profile.php' || $pagenow == 'user-edit.php' ){
		
		Yii::import( 'models.forms.UserAvatarFormModel' );
		if( isset($_GET['user_id']) ){
			$userId = $_GET['user_id'];
		}else{
			$userId = get_current_user_id();
		}
		$itCurrentModel = false;
		$model = UserModel::model()->findByPk( $userId );
		$avatarFormModel = new UserAvatarFormModel();
		$avatarFormModel->load( $model->id );
		$appController = new CController('default');
		
		require_once(get_template_directory().'/templates/user-profile-avatar.php');
	}
	
}

function russianDays( $days ){
	$lastNum = $days % 10;
	if( $lastNum == 1 ){
		return __('1day', 'fortrader_settings');
	}elseif( $lastNum >= 2 && $lastNum <= 4 ){
		return __('24day', 'fortrader_settings');
	}elseif( $lastNum == 0 || ( $lastNum >= 5 && $lastNum <= 9 ) ){
		return __('59day', 'fortrader_settings');
	}
}

//lazy-load in theme

function removeProtocol( $src ){
	if( strpos('http', $src) == 0 ){
		$src = str_replace( 'http://', '//', $src);
		$src = str_replace( 'https://', '//', $src);
	}
	return $src;
}

function httpsProtocol( $src ){
	if( strpos('https', $src) === false ) return str_replace( 'http://', 'https://', $src);
	return $src;
}

add_filter('wpseo_canonical', function( $canonical ){ return httpsProtocol( $canonical ); }, 100, 1);

function renderImg( $src, $width, $height, $atl, $print ){
	$pixelSrc = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
	$src = removeProtocol( $src );
	
	$htmlW = '';
	$htmlH = '';
	if( $width ) $htmlW = 'width="'.$width.'"';
	if( $height ) $htmlH = 'height="'.$height.'"';
	
	$returnImg = '<img class="lazy" data-original="'.$src.'" src="'.$pixelSrc.'"  alt="'.$atl.'" title="'.$atl.'" />
			<noscript>
				<img class="lazy" src="'.$src.'" '.$htmlW.' '.$htmlH.' alt="'.$atl.'" title="'.$atl.'" />
			</noscript>
		';
	if( $print ){
		echo $returnImg;
	}else{
		return $returnImg;
	}
}

//lazy-load in post content
add_filter( 'the_content', 'lazy_load_filter_images', 10 );

function lazy_load_filter_images( $content ){
	$pixelSrc = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
	if ( is_admin() ) {
		return $content;
	}

	$matches = array();
	preg_match_all( '/<img[\s\r\n]+.*?>/is', $content, $matches );

	$search = array();
	$replace = array();

	$i = 0;
	foreach ( $matches[0] as $imgHTML ){

		if (  ! preg_match( "/src=['\"]data:image/is", $imgHTML ) ){
			$i++;
			$replaceHTML = '';
			$replaceHTML = preg_replace( '/<img(.*?)src=/is', '<img$1src="' . $pixelSrc . '" data-original=', $imgHTML );
			
			if ( preg_match( '/class=["\']/i', $replaceHTML ) ) {
				$replaceHTML = preg_replace( '/class=(["\'])(.*?)["\']/is', 'class=$1lazy $2$1', $replaceHTML );
			}else{
				$replaceHTML = preg_replace( '/<img/is', '<img class="lazy "', $replaceHTML );
			}
			if ( $include_noscript ){
				$replaceHTML .= '<noscript>' . $imgHTML . '</noscript>';
			}
			array_push( $search, $imgHTML );
			$replaceHTML = removeProtocol( $replaceHTML );
			array_push( $replace, $replaceHTML );
		}
	}
	$search = array_unique( $search );
	$replace = array_unique( $replace );
	$content = str_replace( $search, $replace, $content );
	return $content;
}

add_filter( 'the_content', 'linksInContent', 10 );
function linksInContent( $content ){
	if ( is_admin() ) {
		return $content;
	}
	return preg_replace_callback('/href="([^"]+fortrader.org[^"]+)"/', function($matches){
		return 'href="'.removeProtocol( $matches[1] ).'"';
	}, $content);
}

add_filter( 'single_template', 'change_template_for_question', PHP_INT_MAX, 2 );
function change_template_for_question( $single_template ){
	if ( has_category( get_option('questions_cat_id') ) ) {
        $single_template = get_template_directory() . '/single-question.php';
    }
    return $single_template;
}

add_action( 'login_footer', 'googleAnaliticsToLoginPage' );
function googleAnaliticsToLoginPage(){
	echo "
	<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			  ga('create', 'UA-34000109-1', 'auto');
			  ga('send', 'pageview');
			</script>
	";
}

/* posts stats start */
function updateCommentStats( $postId ){
	$commentsCount = wp_count_comments( $postId );
	global $wpdb;
	$wpdb->update( 
		'ft_posts_stats_common', 
		array( 
			'commentCount' => $commentsCount->approved,
		), 
		array( 'postId' => $postId )
	);
}








function deleteYiiCommentCache($postId){
	loadYii();
	$langs = Yii::app()->params['langs'];
		
	foreach( $langs as $lang ){
		Yii::app()->cache->delete('commmentsList'.$postId . 'admin' . $lang['langAlias'] );
		Yii::app()->cache->delete('commmentsList'.$postId . 'other' . $lang['langAlias'] );
			
		Yii::app()->cache->delete('commmentsList_singleQuoteList.admin' . $lang['langAlias'] );
		Yii::app()->cache->delete('commmentsList_singleQuoteList.other' . $lang['langAlias'] );
		
		Yii::app()->cache->delete('commmentsList_singleBrokerList.admin' . $lang['langAlias'] );
		Yii::app()->cache->delete('commmentsList_singleBrokerList.other' . $lang['langAlias'] );
		Yii::app()->cache->delete('commmentsList_singleBinaryBrokerList.admin' . $lang['langAlias'] );
		Yii::app()->cache->delete('commmentsList_singleBinaryBrokerList.other' . $lang['langAlias'] );
	}
}
function deleteWpCommentCache($postId){
	
	$appendHTTPS = '';
	if ( isset( $_SERVER['HTTPS'] ) ) {
		if ( 'on' == strtolower( $_SERVER['HTTPS'] ) ) {
			$appendHTTPS = ".HTTPS";
		}
		if ( '1' == $_SERVER['HTTPS'] ) {
			$appendHTTPS = ".HTTPS";
		}
	}
	
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'adminrate' ) );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'adminnewer' ) );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'adminolder' ) );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'otherrate' ) );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'othernewer' ) );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'otherolder' ) );
	
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'adminrate' ) . $appendHTTPS );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'adminnewer' ) . $appendHTTPS );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'adminolder' ) . $appendHTTPS );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'otherrate' ) . $appendHTTPS );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'othernewer' ) . $appendHTTPS );
	delete_transient( 'comments_' . md5('wp_list_comments'.$postId . 'otherolder' ) . $appendHTTPS );
	
}


add_action( 'wp_insert_comment', 'comment_inserted', 99, 2 );
function comment_inserted($comment_id, $comment_object) {
	if( $comment_object->comment_post_ID && $comment_object->comment_approved ){
		deleteWpCommentCache($comment_object->comment_post_ID);
		deleteYiiCommentCache($comment_object->comment_post_ID);
		updateCommentStats( $comment_object->comment_post_ID );
		updateServicesCommentCount($comment_object->comment_post_ID);
	}
}

add_action( 'wp_set_comment_status', 'comment_status_changed', 99, 2 );
function comment_status_changed( $comment_id, $status ){
	$comment_object = get_comment( $comment_id, OBJECT );
	deleteWpCommentCache($comment_object->comment_post_ID);
	deleteYiiCommentCache($comment_object->comment_post_ID);
	updateCommentStats( $comment_object->comment_post_ID );
	updateServicesCommentCount($comment_object->comment_post_ID);
}
add_action( 'edit_comment', 'comment_edited', 99, 2 );
function comment_edited( $comment_ID, $data ){
	$comment_object = get_comment( $comment_ID, OBJECT );
	deleteWpCommentCache($comment_object->comment_post_ID);
	deleteYiiCommentCache($comment_object->comment_post_ID);
	updateServicesCommentCount($comment_object->comment_post_ID);
}

add_action( 'deleted_comment', 'comment_deleted', 99, 1 );
function comment_deleted( $comment_id ){
	$comment_object = get_comment( $comment_id, OBJECT );
	deleteWpCommentCache($comment_object->comment_post_ID);
	deleteYiiCommentCache($comment_object->comment_post_ID);
	updateCommentStats( $comment_object->comment_post_ID );
	updateServicesCommentCount($comment_object->comment_post_ID);
}

function updateServicesCommentCount($postId){
	global $wpdb;
	$decomPost = $wpdb->get_row( $wpdb->prepare( 
		"
			SELECT *
			FROM ft_decomments_posts
			WHERE id = %d
		", 
		$postId
	));

	if( $decomPost ){
		$commentCount = $wpdb->get_var( $wpdb->prepare( 
			"
				SELECT count(comment_ID)
				FROM $wpdb->comments
				WHERE comment_post_ID = %d AND comment_approved = 1
			", 
			$postId
		));
		if( $decomPost->commentCount != $commentCount ){
			$wpdb->update( 
				'ft_decomments_posts', 
				array( 
					'commentCount' => $commentCount,
				), 
				array( 'id' => $postId )
			);
		}
	}
}















add_action( 'save_post', 'update_post_stats' );

function update_post_stats( $post_id ){
	
	$url = get_permalink( $post_id );
	$parsedUrl = parse_url( $url );
	
	global $wpdb;
	
	$exist = $wpdb->get_var( $wpdb->prepare(" SELECT postId FROM ft_posts_stats_common WHERE postId = %d", $post_id) );
	
	if( $exist ){
		$updated = $wpdb->update( 
			'ft_posts_stats_common', 
			array( 
				'url' => $parsedUrl['path'],
				'title' => get_the_title( $post_id ),
				'postDate' => get_the_date( 'Y-m-d H:i:s', $post_id ),
			), 
			array( 'postId' => $post_id )
		);
	}else{
		
			$wpdb->insert( 
				'ft_posts_stats_common', 
				array( 
					'postId' => $post_id, 
					'url' => $parsedUrl['path'],
					'title' => get_the_title( $post_id ),
					'postDate' => get_the_date( 'Y-m-d H:i:s', $post_id ),
				), 
				array( 
					'%d', 
					'%s',
					'%s',
					'%s',
				) 
			);
		
	}
	
	
}

add_action( 'admin_init', 'delete_post_stats_init' );

function delete_post_stats_init() {
    if ( current_user_can( 'delete_posts' ) )
        add_action( 'delete_post', 'delete_post_stats', 10 );
}

function delete_post_stats( $post_id ){
	global $wpdb;
	$wpdb->delete( 'ft_posts_stats_common', array( 'postId' => $post_id ) );
	$wpdb->delete( 'ft_posts_stats_traffic', array( 'postId' => $post_id ) );
}
/* posts stats end */

/*social users*/
add_action( 'wsl_process_login_authenticate_wp_user_start', 'social_user_created' );

function social_user_created( $user_id ) {
	$tempDir = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();
	$tempName = md5( time() . $user_id );
	$tempFile = $tempDir . '/' . $tempName .'.img';

	loadYii();

	global $wpdb;
	$sql = "SELECT photourl FROM `{$wpdb->prefix}wslusersprofiles` where user_id = %d and photourl <> '' ";
	$photoUrl = $wpdb->get_var( $wpdb->prepare( $sql, $user_id ), 0, 0 );

	file_put_contents( $tempFile, file_get_contents($photoUrl) );
	$photoUrlType = exif_imagetype( $tempFile );

	$imageTypes = array(
		1 => 'gif',
		2 => 'jpg',
		3 => 'png'
	);
	$tempFileNew = $tempDir . '/' . $tempName . '.' . $imageTypes[$photoUrlType];
	rename( $tempFile, $tempFileNew );

	$userModel = UserModel::model()->findByPk( $user_id );

	$avatarFile = Yii::App()->params['pathServicesDir'] . '/' . $userModel->avatar->path;
	
	if( filesize( $avatarFile ) != filesize( $tempFileNew ) ){
		$userModel->uploadAvatar( $tempFileNew );
	}
	unlink( $tempFileNew );
}

/*random avatars*/
add_action( 'user_register', 'new_user_created', 10, 1 );

function new_user_created( $user_id ) {
	loadYii();
	$user = UserModel::model()->findByPk( $user_id );
	if(!$user->sex) $user->sex = 'Male';
	$user->uploadRandomAvatar(true);
	$user->save();
}


/*copying firstname and lastname to wp_users*/
add_action( 'profile_update', 'copy_fields_to_wp_users', 10, 1 );
add_action( 'user_register', 'copy_fields_to_wp_users', 10, 1 );
function copy_fields_to_wp_users( $user_id ) {
	$firstName = get_user_meta( $user_id, 'first_name', true );
	$lastName = get_user_meta( $user_id, 'last_name', true );

		global $wpdb;
		$wpdb->update( 
			$wpdb->users, 
			array( 
				'firstName' => $firstName,
				'lastName' => $lastName 
			), 
			array( 'ID' => $user_id ), 
			array( 
				'%s',
				'%s'
			), 
			array( '%d' ) 
		);
}

function getShowUserName( $user ){
	if( !$user ) return false;
	if( strlen( $user->firstName ) && strlen( $user->lastName ) ) return $user->firstName .' '. $user->lastName;
	if( strlen( $user->firstName ) ) return $user->firstName;
	if( strlen( $user->lastName ) ) return $user->lastName;
	if( strlen( $user->display_name )) return $user->display_name;
	if( strlen( $user->user_nicename )) return $user->user_nicename;
	return $user->user_login;
}


add_action('set_auth_cookie', 'user_loged_in', 10, 5);
function user_loged_in( $auth_cookie, $expire, $expiration, $user_id, $scheme ) {
	if (isset($_COOKIE['user_loged_in'])) {
		unset($_COOKIE['user_loged_in']);
	}
	setcookie('user_loged_in', 'loged_in', $expire, '/');
}
add_action('wp_logout', 'user_loged_out');
function user_loged_out() {
	if (isset($_COOKIE['user_loged_in'])) {
		unset($_COOKIE['user_loged_in']);
	}
	setcookie('user_loged_in', 'loged_in', time()-3600*24, '/');
	
	loadYii();
	if( !isset( Yii::app()->params['wpLogOut'] ) ){
		Yii::app()->params['wpLogOut'] = true;
		Yii::app()->user->logout();
	}
}

function wsl_render_auth_widget_in_comment_form_fortrader(){
	if( !is_user_logged_in() && comments_open() ){
		get_template_part('templates/wsl-comment-widget');
	}
}

function getPostPreviewText($words, $trimWpseo = false){
	$seoDesc = get_post_meta (get_the_ID(),'_yoast_wpseo_metadesc',true);
	if( $seoDesc && $trimWpseo ) $seoDesc = wp_trim_words( $seoDesc, $words, '' );
	if( !$seoDesc ) $seoDesc = wp_trim_words( get_the_excerpt(), $words, '' );
	return $seoDesc;
}


function save_post_for_quotes_search( $post_id ) {
	$postCats = wp_get_post_categories( $post_id );
	if( in_array( get_option('services_cat_id'), $postCats ) ) return true;
	
	$post = get_post($post_id);
	if( $post->post_status == 'publish' && $post->post_type == 'post' ){
		loadYii();
		$postForSearch = new QuotesPostsForKeywordsSearchModel;
		$postForSearch->id = $post_id;
		if( $postForSearch->validate() ) $postForSearch->save();
	}
}
add_action( 'save_post', 'save_post_for_quotes_search' );
add_action( 'untrashed_post', 'save_post_for_quotes_search' );


function delete_post_from_quotes( $post_id ){
	loadYii();
	$model = QuotesPostsForKeywordsSearchModel::model()->findByPk($post_id);
	if( $model ) $model->delete();
	
	QuotesKeywordPostsModel::model()->deleteAll(
		" postId = :id ", 
		array(':id' => $post_id ) 
	);
}
add_action( 'deleted_post', 'delete_post_from_quotes' );



function correct_og_prefix( $input ){
	if( is_single() ){
		return str_replace( 'og: http://ogp.me/ns#', 'og: http://ogp.me/ns# article: http://ogp.me/ns/article#', $input);
	}else{
		return $input;
	}
	
}
add_filter( 'language_attributes', 'correct_og_prefix', 100, 1 );

/* remove wp js scripts in services to footer */
function footer_enqueue_scripts() {
	if( servicesPart() ){
		remove_action('wp_head', 'wp_print_scripts');
		//remove_action('wp_head', 'wp_print_styles', 8);
		remove_action('wp_head', 'wp_print_head_scripts', 9);
		remove_action('wp_head', 'wp_enqueue_scripts', 1);
		//add_action('wp_footer', 'wp_print_styles', 5);
		add_action('wp_footer', 'wp_print_scripts', 5);
		add_action('wp_footer', 'wp_enqueue_scripts', 5);
		add_action('wp_footer', 'wp_print_head_scripts', 5);
	}
}
add_action('after_setup_theme', 'footer_enqueue_scripts');

/* remove wp js scripts in services */
function wpdocs_dequeue_script() {
	if( servicesPart() ) wp_dequeue_script( 'decomments' );
}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );

function servicesPart(){
	if( class_exists( 'Yii' ) ){
		if( isset( Yii::app()->params['wpBeginLoad'] ) && Yii::app()->params['wpBeginLoad'] ){
			return true;
		}
	}
	return false;
}

/* remove sitemap image tag  image:image*/
function remove_sitemap_image_tag($images, $pid){
	return array();
}
add_filter('wpseo_sitemap_urlimages', 'remove_sitemap_image_tag', 100, 2);

/* remove quotes taxonomy from sitemap */
function remove_quotes_tax_from_sitemap($bool, $tax){
	if( $tax == 'quotes' ) return true;
	return false;
}
add_filter('wpseo_sitemap_exclude_taxonomy', 'remove_quotes_tax_from_sitemap', 100, 2);

/* add services sitemap */
function add_services_sitemap($str = ''){
	loadYii();
	if( strpos(Yii::app()->cacheSiteMap->cachePath, '/services/') === false ) Yii::app()->cacheSiteMap->cachePath = dirname(__FILE__) .'/../../../services/' . Yii::app()->cacheSiteMap->cachePath;
	return Yii::app()->cacheSiteMap->get('servicesSitemaps');
}
add_filter('wpseo_sitemap_index', 'add_services_sitemap', 100, 1);


function wpseoSitemapsHomeUrl($url, $path, $orig_scheme, $blog_id){
	if( ( strpos($_SERVER['REQUEST_URI'], '/sitemap_index') === 0 || strpos($_SERVER['REQUEST_URI'], '-sitemap') ) && strpos('https', $url) === false ){
		return str_replace( 'http://', 'https://', $url);
	}
	return $url;
}
add_filter('home_url', 'wpseoSitemapsHomeUrl', 100, 4);


/* search stats */
function save_search_keyword(){
	if( isset( $_GET['q'] ) && $_GET['q'] && is_singular() ){
		global $post;
		if( $post->ID == get_option('search_page_id') ){
			$searchKey = htmlentities(strip_tags(str_replace( array('"', "'"), '',urldecode( $_GET['q'] ) )));
			echo "
<script>
sendPost( 'searchKey=$searchKey', '/services/admin/postsStats/ajaxSearchLog' );
</script>
			";
		}
	}
}
add_action('wp_head', 'save_search_keyword', 1);

/* light box for single page */
function initLightBox(){
	if( is_single() ){
		wp_enqueue_style( 'lightbox', get_bloginfo('template_url').'/plagins/lightbox/css/lightbox.css', array(), null );
		wp_enqueue_script('prepareContentForLightBox', get_bloginfo('template_url').'/plagins/lightbox/prepareContentForLightBox.js', array('jquery'), null );
		wp_enqueue_script('lightbox', get_bloginfo('template_url').'/plagins/lightbox/js/lightbox.js', array('jquery', 'prepareContentForLightBox'), null );
	}
}
add_action('wp_head', 'initLightBox', 100);

remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

function addPropertyStylesheet( $buffer ){
	return ( str_replace("rel='stylesheet'", "rel='stylesheet' property='stylesheet'", $buffer) );
}

function addScopedToWslStyle(){
	$content = ob_get_clean();
	$content = str_replace('<style type="text/css">', '<style type="text/css" scoped>', $content);
	ob_start();
	echo '<div>' . $content . '</div>';
}
add_action('wsl_render_auth_widget_end', 'addScopedToWslStyle', 100);

function printWslUrls(){
	echo '
		<input type="hidden" id="wsl_popup_base_url" value="/wp-login.php?action=wordpress_social_authenticate&#038;mode=login&#038;" />
		<input type="hidden" id="wsl_login_form_uri" value="/wp-login.php" />
	';
}
add_action('wp_footer', 'printWslUrls', 100);


/**
 * Disable the WPSEO v3.1+ Primary Category feature.
 */
add_filter( 'wpseo_primary_term_taxonomies', '__return_empty_array' );


/* hide services posts from wp-admin/edit.php */

function hideServicesPosts( $request ){
	global $pagenow;
	if ( $pagenow == 'edit.php' ) {
		if( !isset( $request['cat'] ) ){
			$servicesCatId = get_option('services_cat_id');
			$request = array_merge(
				$request,
				array(
					'category__not_in' => array( $servicesCatId )
				)
			);
		}
	}
	return $request;
}
add_filter( 'request', 'hideServicesPosts' );

/* change domain to files */



function getDataForServicesComments($rote, $modelId, $postId, $comment = false){
	
	$subject = $postTitle = $postLink = '';
	
	switch ($rote) {
		case "binary/single":
			loadYii();
			$model = BrokerModel::model()->findByPk( $modelId );
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'Binary options rating') . '. ' . $model->officialName . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Binary options rating') . '. ' . $model->officialName;
			$postLink = $model->absoluteSingleURL;
			
			break;
		case "broker/single":
			loadYii();
			$model = BrokerModel::model()->findByPk( $modelId );
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'Broker ratings') . '. ' . $model->officialName . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Broker ratings') . '. ' . $model->officialName;
			$postLink = $model->absoluteSingleURL;
			
			break;
		case "currencyRates/single":
			loadYii();
			$model = CurrencyRatesModel::model()->findByPk( $modelId );
			if( !$model ) break;
			
			
			
			$decomPost = DecommentsPostsModel::model()->findByPk($postId);
			$type = str_replace('.singleCurrencyRates', '', $decomPost->cat);
			
			
			if( $type == 'cbr' ){
				
				if( $comment )
				$subject = Yii::t('*', 'CBR Currency Rates') . '. ' . $model->name . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
				
				$postTitle = Yii::t('*', 'CBR Currency Rates') . '. ' . $model->name;
				$postLink = $model->absoluteSingleURL;
			}elseif( $type == 'ecb' ){
				$model->dataType = $type;
				
				if( $comment )
				$subject = Yii::t('*', 'ECB Currency Rates') . '. ' . $model->name . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
				
				$postTitle = Yii::t('*', 'ECB Currency Rates') . '. ' . $model->name;
				$postLink = $model->absoluteSingleURL;
			}
		
			
			
			break;
		case "currencyRates/exchangePage":
			loadYii();
			$model = CurrencyRatesConvertModel::model()->find(array(
				'condition' => ' `t`.`idPair` = :id ',
				'params' => array( ':id' =>  $modelId )
			));
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'Currency Rates exchange page ') . '. ' . $model->pairTitle . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Currency Rates exchange page ') . '. ' . $model->pairTitle;
			$postLink = $model->absoluteSingleURL;
			
			break;
		case "journal/single":
			loadYii();
			$model = JournalModel::model()->findByPk( $modelId );
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'Journal') . '. ' . $model->name . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Journal') . '. ' . $model->name;
			$postLink = $model->absoluteSingleURL;
			
			break;
		case "quotesNew/single":
			loadYii();
			$model = QuotesSymbolsModel::model()->findByPk( $modelId );
			if( !$model ) break;
			$name = $model->name;
			if( $model->tooltip )$name = $model->tooltip;
			
			if( $comment )
			$subject = Yii::t('*', 'Quotes') . '. ' . $name . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Quotes') . '. ' . $name;
			$postLink = $model->absoluteSingleURL;
			
			break;
		case "quotesNew/tradersRating":
			loadYii();
			
			if( $comment )
			$subject = Yii::t('*', 'Rating of traders') . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Rating of traders');
			$postLink = Yii::App()->createAbsoluteURL( 'quotesNew/tradersRating' );
			
			break;
		case "newsAggregator/index":
			loadYii();
			
			if( $comment )
			$subject = Yii::t('*', 'News Aggregator') . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'News Aggregator');
			$postLink = Yii::App()->createAbsoluteURL( 'newsAggregator/index' );
			
			break;
		case "interestRates/index":
			loadYii();
			
			if( $comment )
			$subject = Yii::t('*', 'Bank Interest Rates') . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Bank Interest Rates');
			$postLink = Yii::App()->createAbsoluteURL( 'interestRates/index' );
			
			break;
		case "contest/single":
			loadYii();
			$model = ContestModel::model()->findByPk( $modelId );
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'Contest') . '. ' . $model->name . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Contest') . '. ' . $model->name;
			$postLink = $model->absoluteSingleURL;
			
			break;
		case "contest/list":
			loadYii();
			
			if( $comment )
			$subject = Yii::t('*', 'Contests main page') . ' ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Contests main page');
			$postLink = Yii::App()->createAbsoluteURL( 'contest/list' );
			
			break;
		case "contestMember/single":
			loadYii();
			$model = ContestMemberModel::model()->findByPk( $modelId );
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'Contest Member') . '. ' . $model->user->showName . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Contest Member') . '. ' . $model->user->showName;
			$postLink = $model->absoluteSingleURL;
			
			break;
		case "monitoring/single":
			loadYii();
			$model = ContestMemberModel::model()->findByPk( $modelId );
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'Monitoring Member') . '. ' . $model->user->showName . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Monitoring Member') . '. ' . $model->user->showName;
			$postLink = $model->absoluteMonitoringSingleURL;
			
			break;
		case "monitoring/index":
			loadYii();
			
			if( $comment )
			$subject = Yii::t('*', 'Monitoring') . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Monitoring');
			$postLink = Yii::App()->createAbsoluteURL( 'monitoring/index' );
			
			break;
		case "calendarEvent/single":
			loadYii();
			$model = CalendarEvent2Model::model()->findByPk( $modelId );
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'Calendar Event') . '. ' . $model->indicatorName . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Calendar Event') . '. ' . $model->indicatorName;
			$postLink = $model->absoluteSingleURL;
			
			break;
		case "informers/single":
			loadYii();
			$model = InformersCategoryModel::model()->findByPk( $modelId );
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'Informer') . '. ' . $model->title . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'Informer') . '. ' . $model->title;
			$postLink = $model->absoluteSingleURL;
			
			break;
		case "user/single":
			loadYii();
			$model = UserModel::model()->findByPk( $modelId );
			if( !$model ) break;
			
			if( $comment )
			$subject = Yii::t('*', 'User profile') . '. ' . $model->showName . '. ' . $comment->comment_author . '. ' . mb_substr($comment->comment_content, 0, 200);
			
			$postTitle = Yii::t('*', 'User profile') . '. ' . $model->showName;
			$postLink = $model->absoluteSingleURL;
			
			break;
	
	}
	
	
	
	return array(
		'subject' => $subject,
		'postTitle' => $postTitle,
		'postLink' => $postLink,
	);
}

function modify_comment_moderation_subject( $subject, $comment_id ){
	$comment = get_comment( $comment_id );
	$categories = wp_get_post_categories( $comment->comment_post_ID );
	
	if( $categories[0] != get_option('services_cat_id') ) return $subject;
	
	$commentPost = get_post( $comment->comment_post_ID );
	$title = str_replace( 'DO NOT DELETE__', '', $commentPost->post_title );
	$routes = explode('.', $title);
	$rote = $routes[0] . '/' . $routes[1];
	if( $routes[2] ) {
		$expArgs = explode('_', $routes[2]);
		$modelId = $expArgs[1];
	}
	
	$data = getDataForServicesComments($rote, $modelId, $comment->comment_post_ID, $comment);
	
	return $data['subject'];
}
add_filter( 'comment_moderation_subject', 'modify_comment_moderation_subject', 100, 2 );
add_filter( 'comment_notification_subject', 'modify_comment_moderation_subject', 100, 2 );

function modify_comment_moderation_text($notify_message, $comment_id){
	
	$comment = get_comment($comment_id);
	if( $comment->comment_type == 'trackback' || $comment->comment_type == 'pingback' ) return $notify_message;
	$categories = wp_get_post_categories( $comment->comment_post_ID );
	
	if( $categories[0] != get_option('services_cat_id') ) return $notify_message;
	
	$post = get_post($comment->comment_post_ID);
	$postTitle = $post->post_title;
	$postLink = get_permalink($comment->comment_post_ID);
	
	$title = str_replace( 'DO NOT DELETE__', '', $post->post_title );
	$routes = explode('.', $title);
	$rote = $routes[0] . '/' . $routes[1];
	if( $routes[2] ) {
		$expArgs = explode('_', $routes[2]);
		$modelId = $expArgs[1];
	}
	

	$data = getDataForServicesComments($rote, $modelId, $comment->comment_post_ID);
	
	$postTitle = $data['postTitle'];
	$postLink = $data['postLink'];
	
	$postLink = str_replace('/services', '', $postLink);
	
	global $wpdb;
	
	


	$comment_content = wp_specialchars_decode( $comment->comment_content );
	$comment_author_domain = @gethostbyaddr($comment->comment_author_IP);
	$comments_waiting = $wpdb->get_var("SELECT count(comment_ID) FROM $wpdb->comments WHERE comment_approved = '0'");
	
	if( $comment->comment_approved == 1 ){
		$notify_message  = sprintf( __( 'New comment on your post "%s"' ), $postTitle ) . "\r\n";
			
		$notify_message .= sprintf( __( 'Author: %1$s (IP: %2$s, %3$s)' ), $comment->comment_author, $comment->comment_author_IP, $comment_author_domain ) . "\r\n";
		$notify_message .= sprintf( __( 'Email: %s' ), $comment->comment_author_email ) . "\r\n";
		$notify_message .= sprintf( __( 'URL: %s' ), $comment->comment_author_url ) . "\r\n";
		$notify_message .= sprintf( __('Comment: %s' ), "\r\n" . $comment_content ) . "\r\n\r\n";
		$notify_message .= __( 'You can see all comments on this post here:' ) . "\r\n";
			
		$notify_message .= $postLink . "#comments\r\n\r\n";
		$notify_message .= sprintf( __('Permalink: %s'), $postLink . '#comment-' . $comment_id ) . "\r\n";

		if ( user_can( $post->post_author, 'edit_comment', $comment->comment_ID ) ) {
			if ( EMPTY_TRASH_DAYS ) {
				$notify_message .= sprintf( __( 'Trash it: %s' ), admin_url( "comment.php?action=trash&c={$comment->comment_ID}#wpbody-content" ) ) . "\r\n";
			} else {
				$notify_message .= sprintf( __( 'Delete it: %s' ), admin_url( "comment.php?action=delete&c={$comment->comment_ID}#wpbody-content" ) ) . "\r\n";
			}
			$notify_message .= sprintf( __( 'Spam it: %s' ), admin_url( "comment.php?action=spam&c={$comment->comment_ID}#wpbody-content" ) ) . "\r\n";
		}
	}else{
		$notify_message  = sprintf( __('A new comment on the post "%s" is waiting for your approval'), $postTitle ) . "\r\n";
		$notify_message .= $postLink . "\r\n\r\n";
		$notify_message .= sprintf( __( 'Author: %1$s (IP: %2$s, %3$s)' ), $comment->comment_author, $comment->comment_author_IP, $comment_author_domain ) . "\r\n";
		$notify_message .= sprintf( __( 'Email: %s' ), $comment->comment_author_email ) . "\r\n";
		$notify_message .= sprintf( __( 'URL: %s' ), $comment->comment_author_url ) . "\r\n";
		$notify_message .= sprintf( __( 'Comment: %s' ), "\r\n" . $comment_content ) . "\r\n\r\n";
		
		$notify_message .= sprintf( __('Approve it: %s'),  admin_url("comment.php?action=approve&c=$comment_id") ) . "\r\n";
		if ( EMPTY_TRASH_DAYS )
			$notify_message .= sprintf( __('Trash it: %s'), admin_url("comment.php?action=trash&c=$comment_id") ) . "\r\n";
		else
			$notify_message .= sprintf( __('Delete it: %s'), admin_url("comment.php?action=delete&c=$comment_id") ) . "\r\n";
		$notify_message .= sprintf( __('Spam it: %s'), admin_url("comment.php?action=spam&c=$comment_id") ) . "\r\n";

		$notify_message .= sprintf( _n('Currently %s comment is waiting for approval. Please visit the moderation panel:',
			'Currently %s comments are waiting for approval. Please visit the moderation panel:', $comments_waiting), number_format_i18n($comments_waiting) ) . "\r\n";
		$notify_message .= admin_url("edit-comments.php?comment_status=moderated") . "\r\n";
	}
	
	return $notify_message;
}
add_filter( 'comment_moderation_text', 'modify_comment_moderation_text', 100, 2 );
add_filter( 'comment_notification_text', 'modify_comment_moderation_text', 100, 2 );



add_filter( 'the_content', 'addOriginSrc', 10 );

function addOriginSrc( $content ){
	if ( ! preg_match_all( '/<img [^>]+>/', $content, $matches ) ) {
		return $content;
	}
	$selected_images = $attachment_ids = array();

	foreach( $matches[0] as $image ) {
		if ( false === strpos( $image, ' data-origin-src=' ) && preg_match( '/wp-image-([0-9]+)/i', $image, $class_id ) &&
			( $attachment_id = absint( $class_id[1] ) ) ) {

			$selected_images[ $image ] = $attachment_id;
			$attachment_ids[ $attachment_id ] = true;
		}
	}

	foreach ( $selected_images as $image => $attachment_id ) {
		$content = str_replace( $image, str_replace('/>', 'data-origin-src="'.removeProtocol( wp_get_attachment_url($attachment_id) ), $image).'" />', $content );
	}

	return $content;
}

function getContestRegisterLink( $userId ){
	global $wpdb;
	$contest = $wpdb->get_row( "SELECT `id`, `slug` FROM `ft_contest` WHERE `status` = 'registration' ORDER BY `end` DESC", OBJECT );
	if( !$contest ) return false;
	$registerLink = "<a href='/contests/{$contest->slug}#contestRegistrationForm'>".__('Register in the contest', 'fortrader_settings')."</a>";
	if( !$userId ){
		return $registerLink;
	}
	$contestMember = $wpdb->get_row( $wpdb->prepare("SELECT `id` FROM `ft_contest_member` WHERE `idContest` = %d AND `idUser` = %d ", $contest->id, $userId ), OBJECT );
	if( $contestMember ) return false;
	return $registerLink;
}





add_filter( 'pre_comment_approved' , 'filterCommentByStopWords' , '99', 2 );
function filterCommentByStopWords( $approved , $commentdata ){
	$stopWords = get_option('comments_stop_words');
	if( !$stopWords ) return $approved;
	$stopWordsArr = explode(',', $stopWords);
	if( !count($stopWordsArr) ) return $approved;
	
	require_once dirname(__FILE__) .'/../../../services/protected/libs/CommonLib.php';
	$stemmedContent = CommonLib::stemedText( $commentdata['comment_content'], 'STEM_RUSSIAN_UNICODE');
	
	foreach( $stopWordsArr as $word ){
		$stemmedWord = CommonLib::stemedText( $word, 'STEM_RUSSIAN_UNICODE');
		if( strpos(' ' . $stemmedContent . ' ', ' ' . $stemmedWord . ' ') !== false ){
			return 0;
		}
	}
	return $approved;
}

function admin_post_comments_links( $url, $post, $leavename=false ) {
	global $pagenow;
	
	if( $pagenow != 'edit-comments.php' && $pagenow != 'comment.php' ) return $url;
	if( strpos($post->post_title, 'DO NOT DELETE__') === false ) return $url;
	
	$title = str_replace( 'DO NOT DELETE__', '', $post->post_title );
	$routes = explode('.', $title);
	$rote = $routes[0] . '/' . $routes[1];
	if( $routes[2] ) {
		$expArgs = explode('_', $routes[2]);
		$modelId = $expArgs[1];
	}
	
	loadYii();
	if( !isset($modelId) || !$modelId || !is_numeric($modelId) ) $url = Yii::App()->createAbsoluteURL($rote);
	
	$data = getDataForServicesComments($rote, $modelId, $post->ID);
	
	return str_replace( '/services', '', $data['postLink'] );
}
add_filter( 'post_link', 'admin_post_comments_links', 10, 3 );

function admin_comments_link( $link, $comment, $args, $cpage ){
	
	$post = get_post( $comment->comment_post_ID );
	$newUrl = admin_post_comments_links( $link, $post, false );
	if( $newUrl != $link ) $newUrl .= "#comment-{$comment->comment_ID}";
	
	return $newUrl;
}
add_filter( 'get_comment_link', 'admin_comments_link', 10, 4 );

function getServicesPostTitle( $title, $postId ){
	global $pagenow;
	
	if( $pagenow != 'edit-comments.php' && $pagenow != 'comment.php' ) return $title;
	$post = get_post($postId);
	if( strpos($post->post_title, 'DO NOT DELETE__') === false ) return $title;

	$title = str_replace( 'DO NOT DELETE__', '', $post->post_title );
	$routes = explode('.', $title);
	$rote = $routes[0] . '/' . $routes[1];
	if( $routes[2] ) {
		$expArgs = explode('_', $routes[2]);
		$modelId = $expArgs[1];
	}
	

	$data = getDataForServicesComments($rote, $modelId, $postId);
	return $data['postTitle'];
}

function admin_post_comments_title( $title, $id = null ) {
	if( !$id ) return $title;

    return getServicesPostTitle( $title, $id );
}
add_filter( 'the_title', 'admin_post_comments_title', 10, 2 );



function removeServiceCatPostFromSitemapJoin($bool, $post_type){
	if( $post_type == 'post' ){
		global $wpdb;

		return "LEFT JOIN {$wpdb->term_relationships} AS r ON  {$wpdb->posts}.ID = r.object_ID
		LEFT JOIN {$wpdb->term_taxonomy} AS tax ON r.term_taxonomy_id = tax.term_taxonomy_id";
	}
}
add_filter('wpseo_posts_join','removeServiceCatPostFromSitemapJoin', 10, 2);
add_filter('wpseo_typecount_join','removeServiceCatPostFromSitemapJoin', 10, 2);

function removeServiceCatPostFromSitemapWhere($bool, $post_type){
	if( $post_type == 'post' ){
		global $wpdb;

		return "AND tax.term_id <> " . get_option('services_cat_id') . " AND tax.taxonomy = 'category' ";
	}
}

add_filter('wpseo_posts_where','removeServiceCatPostFromSitemapWhere', 10, 2);
add_filter('wpseo_typecount_where','removeServiceCatPostFromSitemapWhere', 10, 2);

function get_template_part_by_locale( $template ){
	if( !$template ) return false;
	$locale = get_locale();
	if( $locale != 'ru_RU' ){
		get_template_part( $template . '_' . $locale );
	}else{
		get_template_part( $template );
	}
}
function get_lang_url_prefix(){
	$slug = pll_current_language('slug');
	if( $slug == 'ru' ) return '';
	return '/' . $slug;
}



function disable_default_query( $sql, WP_Query &$wpQuery ) {
    if ( $_SERVER['REQUEST_URI'] == '/' && $wpQuery->is_main_query() ) {
        // prevent SELECT FOUND_ROWS() query
        $wpQuery->query_vars['no_found_rows'] = true;

        // prevent post term and meta cache update queries 
        $wpQuery->query_vars['cache_results'] = false;

        return false;
    }
    return $sql;
}
add_filter( 'posts_request', 'disable_default_query', 10, 2 );

add_filter( 'wp_mail_smtp_custom_options', function($phpmailer){
	$phpmailer->SMTPOptions = array(
		'ssl' => array(
			'verify_peer' => false,
			'verify_peer_name' => false,
			'allow_self_signed' => true
		)
	);
	return $phpmailer;
}, 10, 1 );


add_filter( 'wpseo_og_article_published_time', 'removeOgDates', 10, 1 );
add_filter( 'wpseo_og_article_modified_time', 'removeOgDates', 10, 1 );
add_filter( 'wpseo_og_og_updated_time', 'removeOgDates', 10, 1 );
function removeOgDates($content){
	if( get_post_meta(get_the_ID(), 'hide_date', true) ) return false;
	return $content;
}


add_filter( 'the_content', 'addClassToDownloadLink', 10 );

function addClassToDownloadLink( $content ){
	
	$downloadWordsStr = 'скачать|download';
	
	
	if ( ! preg_match_all( '/<a[^>]+>[^<]*?('.$downloadWordsStr.')[^<]*?<\/a>/ui', $content, $matches ) ) {
		return $content;
	}
	
	foreach( $matches[0] as $link ){
		$content = str_replace($link, str_replace('<a', '<a onclick="sendPost( \'current_post_id=\'+current_post_id+\'&referrer=\'+encodeURIComponent(document.referrer), \'/services/admin/postsStats/ajaxCountDownloadsFromContent\' );"', $link), $content);
	}

	return $content;
}

add_filter( 'pre_get_posts', function( $query ){
	if ($query->is_feed) {
        $query->set('category__not_in', array( get_option('questions_cat_id'), get_option('services_cat_id') ) );
    }
    return $query;
}, 10, 1 );




?>