<?php
/*
  Plugin Name: percent-rates
  Plugin URI: http://mrdealer.ru/
  Description:Percent rates
  Author: SMaster
  Version: 0.1
  Author URI: http://nashe-vse.ru/
*/

//************* Init ******************

if (SMASTER!==true) {
    
    define ('SMASTER', true);
    
    include 'functions.inc.php';
    include 'smarty/Smarty.class.php';
    
    $sitepath = urlparse();
    
    $dir = pathinfo(__FILE__, PATHINFO_DIRNAME);
    $tpl = new Smarty();
    $tpl->caching      = Smarty::CACHING_LIFETIME_CURRENT;
    $tpl->cache_dir    = "$dir/cache/";
    $tpl->compile_dir  = "$dir/templates_c/";
    //$tpl->registerFilter('pre', 'pre_iconv');
    
}

define('PERCENT_RATES_PER_PAGE', 10);
define('AMDATA_STEPS', 20);

//************* Filters ***************

add_filter('init', 'percent_rates_init');

function after_header_end() {
	ob_end_flush();
}

function before_header_start() {
	ob_start('change_title_tag_m');
}

function change_title_tag_m($head) {
	$title = percent_rates_wp_title();
	return preg_replace('/<title>[^<]*<\/title>/i', '<title>'.$title.'</title>'."\n\n".'<meta name="title" content="'.$title.'" />', $head);
}

function percent_rates_init() {
    
    global $sitepath, $tpl, $wpdb, $company_id, $company_id_url;
    
    if ($sitepath[0]=='bankrates') {
        
        add_filter('the_content', 'percent_rates_the_content');
       // add_filter('wp_title', 'percent_rates_wp_title');
		
		add_action('template_redirect', 'before_header_start', 0);
		add_action('wp_head', 'after_header_end', 1000);
        
        $current_user = wp_get_current_user();
        if (is_super_admin($current_user->ID)) {
            $sitepath['admin'] = true;
        }
        
        if ($sitepath[1] && function_exists("percent_rates_redirect_{$sitepath[1]}")) {
            $tpl->caching = Smarty::CACHING_OFF;
            if ($url=call_user_func("percent_rates_redirect_{$sitepath[1]}")) {
                redirect($url);
            }
        }
        elseif (!$sitepath[1] || function_exists("percent_rates_{$sitepath[1]}")) {
            $_SERVER['REQUEST_URI'] = $sitepath['admin'] ? $_SERVER['REQUEST_URI'].'admin' :  $_SERVER['REQUEST_URI'];
            if ($sitepath[1]) {
                if ($tpl->isCached(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/{$sitepath[1]}.tpl", $_SERVER['REQUEST_URI'])) {
                    $ok = true;
                }
                else {
                    $ok = call_user_func("percent_rates_{$sitepath[1]}");
                }
                $_GET['hide_title'] = true;
            }
            else {
                if ($tpl->isCached(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/index.tpl", $_SERVER['REQUEST_URI'])) {
                    $ok = true;
                }
                else {
                    $ok = percent_rates();
                }
            }
            if ($ok) {
               $_SERVER['REQUEST_URI_SAVE'] = $_SERVER['REQUEST_URI'];
               $_SERVER['REQUEST_URI'] = "/{$sitepath[0]}.html";
            }
        }
    }
    
}

function percent_rates_the_content($content) {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->assign_by_ref('sitepath', $sitepath);
    $tpl->assign('content', $content);
    
    $template = $sitepath[1] ? "{$sitepath[1]}.tpl" : 'index.tpl';
    $content = $tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/$template", $_SERVER['REQUEST_URI_SAVE']);
    
    return $content;
    
}

function percent_rates_wp_title() {
    
    global $sitepath, $tpl, $wpdb;
    
	$percent = 'Процентные ставки';
	$title = $percent;
    if ($sitepath[1]=='filter') {
        if ($sitepath['id']) {
            $sql = "SELECT economic_indicators.indicator_name_ru, banks.bank_name_ru, countries.country_name_ru FROM bankrates JOIN economic_indicators ON bankrates.rate_name_id=economic_indicators.id_indicator JOIN banks ON bankrates.bank_id=banks.bank_id JOIN countries ON bankrates.country_id=countries.id_country WHERE bankrates.id={$sitepath[id]}";
            $row = $wpdb->get_row($sql, ARRAY_A);
            $title = "{$row['indicator_name_ru']} &ndash; {$row['bank_name_ru']} &ndash; {$row[country_name_ru]} &ndash; $percent";
        }
        elseif ($sitepath['country']) {
            $sql = "SELECT * FROM countries WHERE id_country={$sitepath[country]}";
            $row = $wpdb->get_row($sql, ARRAY_A);
            $title = "{$row[country_name_ru]} &ndash; $percent";
        }
        elseif ($sitepath['bank']) {
            $sql = "SELECT * FROM banks WHERE bank_id={$sitepath[bank]}";
            $row = $wpdb->get_row($sql, ARRAY_A);
            $title = "{$row[bank_name_ru]} &ndash; $percent";
        }
        else {
            $title = $percent;
        }
        //$title .= ' &mdash; ';
    }
    
    return $title;
    
}

//************* Content ***************

function percent_rates_comparison($a, $b) {
    
    return $a['date_mod']<$b['date_mod'] ? 1 : -1;
}

function percent_rates() {
    
    global $sitepath, $tpl, $wpdb;
    
    // Следующие изменения
    $sql = "SELECT DISTINCT bankrates.*, banks.bank_name_ru AS bank_name,
            countries.country_name_ru AS country_name, countries.country_iso AS country_iso,
            economic_indicators.indicator_name_ru AS indicator_name
            FROM bankrates
            JOIN bankrates_values ON bankrates.id=bankrates_values.rate_id
            JOIN economic_indicators ON bankrates.rate_name_id=economic_indicators.id_indicator
            JOIN countries ON bankrates.country_id=countries.id_country
            JOIN banks ON bankrates.bank_id=banks.bank_id
            WHERE bankrates_values.forecast AND bankrates_values.date_mod>=NOW()
            ORDER BY bankrates_values.date_mod";
    if ($data = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($data as $key=>$item) {
            // Ближайшее значение
            $sql = "SELECT value, date_mod
                    FROM bankrates_values
                    WHERE rate_id={$item[id]} AND forecast AND date_mod>=NOW()
                    ORDER BY date_mod DESC
                    LIMIT 1";
            if ($row = $wpdb->get_row($sql, ARRAY_A)) {
                $data[$key]['value'] = $row['value'];
                $data[$key]['date_mod'] = $row['date_mod'];
            }
            // Название банка + название индекса (если у банка их несколько)
            $sql = "SELECT * FROM bankrates WHERE bank_id={$item[bank_id]} AND id!={$item[id]} LIMIT 1";
            $data[$key]['name'] = $wpdb->get_row($sql, ARRAY_A)
                                ? "{$item[bank_name]} ({$item[indicator_name]})"
                                : $item['bank_name'];
        }
        $tpl->assign('featured', $data);
    }
    
    // Индексы - наше все
   $sql = "SELECT DISTINCT bankrates.*, banks.bank_name_ru AS bank_name,
            countries.country_name_ru AS country_name, countries.country_iso AS country_iso,
            economic_indicators.indicator_name_ru AS indicator_name
            FROM bankrates
            LEFT JOIN bankrates_values ON bankrates.id=bankrates_values.rate_id
            JOIN economic_indicators ON bankrates.rate_name_id=economic_indicators.id_indicator
            JOIN countries ON bankrates.country_id=countries.id_country
            JOIN banks ON bankrates.bank_id=banks.bank_id
            WHERE (!bankrates_values.forecast AND bankrates_values.date_mod<=NOW()) OR (bankrates_values.forecast IS NULL AND bankrates_values.date_mod IS NULL)";
    $page = pager($sql, PERCENT_RATES_PER_PAGE, $sitepath['page'], "/{$sitepath[0]}/{$sitepath[parts][bank]}{$sitepath[parts][country]}{$sitepath[parts][id]}", true);
    if ($page['data']) {
        foreach ($page['data'] as $key=>$item) {
            $sql = "SELECT value, date_mod
                    FROM bankrates_values
                    WHERE rate_id={$item[id]} AND !forecast AND date_mod<=NOW()
                    ORDER BY date_mod DESC
                    LIMIT 2";
            $rows = $wpdb->get_results($sql, ARRAY_A);
            if ($rows || $sitepath['admin']) {
                $page['data'][$key]['value'] = $rows[0]['value'];
                $page['data'][$key]['date_mod'] = $rows[0]['date_mod'];
                if (count($rows)>=2) {
                    $page['data'][$key]['diff'] = $rows[0]['value']-$rows[1]['value'];
                }
                $filename = "/wp-content/plugins/percent-rates/images/{$item[country_iso]}.png";
                if (file_exists($_SERVER['DOCUMENT_ROOT'].$filename)) {
                    $page['data'][$key]['image'] = $filename;
                }
            }
            $ids[$item['id']] = $item['id'];
        }
        $ids = my64_encode(implode(', ', $ids));
        $tpl->assign('ids', $ids);
        usort($page['data'], 'percent_rates_comparison');
        $tpl->assign('page', $page);
    }
    
    return true;
    
}

function percent_rates_filter() {
    
    global $sitepath, $tpl, $wpdb;
    
    $percent = 'Процентные ставки';
	
    if ($sitepath['id']) {
		$sql = "SELECT economic_indicators.indicator_name_ru, banks.bank_name_ru, banks.bank_id FROM bankrates JOIN economic_indicators ON bankrates.rate_name_id=economic_indicators.id_indicator JOIN banks ON bankrates.bank_id=banks.bank_id WHERE bankrates.id={$sitepath[id]}";
        $row = $wpdb->get_row($sql, ARRAY_A);


		$title = " <a href='http://fortrader.org/bankrates/'>$percent</a> &ndash; {$row['indicator_name_ru']}";
		$breadcrumbs = "<a href='/bankrates/'>$percent</a> &rarr; <a href='/bankrates/filter/bank-{$row[bank_id]}/'>{$row[bank_name_ru]}</a> &rarr; {$row[indicator_name_ru]}";
    }
    elseif ($sitepath['country']) {
        $sql = "SELECT * FROM countries WHERE id_country={$sitepath[country]}";
        $row = $wpdb->get_row($sql, ARRAY_A);
        $title = "<a href='http://fortrader.org/bankrates/'>$percent</a> &ndash; {$row[country_name_ru]}";
		$breadcrumbs = "<a href='/bankrates/'>$percent</a> &rarr; {$row[country_name_ru]}";
    }
    elseif ($sitepath['bank']) {
        $sql = "SELECT * FROM banks WHERE bank_id={$sitepath[bank]}";
        $row = $wpdb->get_row($sql, ARRAY_A);
        $title = "<a href='http://fortrader.org/bankrates/'>$percent</a> &ndash; {$row[bank_name_ru]}";
		$breadcrumbs = "<a href='/bankrates/'>$percent</a> &rarr; {$row[bank_name_ru]}";
    }
    else {
        $title = $percent;
    }
    $tpl->assign('title', $title);
	$tpl->assign('breadcrumbs', $breadcrumbs);

    
    $bank = $sitepath['bank'] ? "bankrates.bank_id={$sitepath[bank]}" : 1;
    $country = $sitepath['country'] ? "bankrates.country_id={$sitepath[country]}" : 1;
    $id = $sitepath['id'] ? "bankrates.id={$sitepath[id]}" : 1;
    $featured = $sitepath['id']
              ? '((!bankrates_values.forecast AND bankrates_values.date_mod<=NOW()) OR (bankrates_values.forecast AND bankrates_values.date_mod>NOW()))'
              : '(!bankrates_values.forecast AND bankrates_values.date_mod<=NOW())';
    
    // Виды ставок
    $sql = "SELECT DISTINCT bankrates.*, banks.bank_name_ru AS bank_name,
            countries.country_name_ru AS country_name, countries.country_iso AS country_iso,
            economic_indicators.indicator_name_ru AS indicator_name
            FROM bankrates
            JOIN bankrates_values ON bankrates.id=bankrates_values.rate_id
            JOIN economic_indicators ON bankrates.rate_name_id=economic_indicators.id_indicator
            JOIN countries ON bankrates.country_id=countries.id_country
            JOIN banks ON bankrates.bank_id=banks.bank_id
            WHERE $featured AND $bank AND $country AND $id";
    $tpl->assign('types', $wpdb->get_results($sql, ARRAY_A));
    
    // Список значений ставок
    $sql = "SELECT bankrates.*, banks.bank_name_ru AS bank_name,
            countries.country_name_ru AS country_name, countries.country_iso AS country_iso,
            economic_indicators.indicator_name_ru AS indicator_name,
            bankrates_values.value, bankrates_values.date_mod, bankrates_values.forecast, bankrates_values.id AS value_id
            FROM bankrates
            JOIN bankrates_values ON bankrates.id=bankrates_values.rate_id
            JOIN economic_indicators ON bankrates.rate_name_id=economic_indicators.id_indicator
            JOIN countries ON bankrates.country_id=countries.id_country
            JOIN banks ON bankrates.bank_id=banks.bank_id

            WHERE $featured AND $bank AND $country AND $id
			ORDER BY date_mod DESC";
    $page = pager($sql, PERCENT_RATES_PER_PAGE, $sitepath['page'], "/{$sitepath[0]}/filter/{$sitepath[parts][bank]}{$sitepath[parts][country]}{$sitepath[parts][id]}");
    if ($page['data']) {
        foreach ($page['data'] as $key=>$item) {
            $filename = "/wp-content/plugins/percent-rates/images/{$item[country_iso]}.png";
            if (file_exists($_SERVER['DOCUMENT_ROOT'].$filename)) {
                $page['data'][$key]['image'] = $filename;
            }
            $sql = "SELECT value, date_mod
                    FROM bankrates_values
                    WHERE rate_id={$item[id]} AND $featured AND id!={$item[value_id]} AND date_mod<='{$item[date_mod]}'
                    ORDER BY date_mod DESC
                    LIMIT 1";
            if ($row = $wpdb->get_row($sql, ARRAY_A)) {
                $page['data'][$key]['diff'] = $item['value']-$row['value'];
            }
            $ids[$item['id']] = $item['id'];
        }
        $ids = my64_encode(implode(', ', $ids));
        $tpl->assign('ids', $ids);
        usort($page['data'], 'percent_rates_comparison');
        $tpl->assign('page', $page);
    }
    
    return true;
    
}

function percent_rates_amdata() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    $bank = $sitepath['bank'] ? "bankrates.bank_id={$sitepath[bank]}" : 1;
    $country = $sitepath['country'] ? "bankrates.country_id={$sitepath[country]}" : 1;
    $id = $sitepath['id'] ? "bankrates.id={$sitepath[id]}" : 1;
    $ids = $sitepath['vars']['ids'] ? "bankrates.id IN (".my64_decode($sitepath['vars']['ids']).")" : 1;
    
    $sql = "SELECT UNIX_TIMESTAMP(MIN(bankrates_values.date_mod)) AS min, UNIX_TIMESTAMP(MAX(bankrates_values.date_mod)) AS max
            FROM bankrates
            JOIN bankrates_values ON bankrates.id=bankrates_values.rate_id
            JOIN economic_indicators ON bankrates.rate_name_id=economic_indicators.id_indicator
            JOIN countries ON bankrates.country_id=countries.id_country
            JOIN banks ON bankrates.bank_id=banks.bank_id
            WHERE !bankrates_values.forecast AND bankrates_values.date_mod<=NOW() AND $bank AND $country AND $id AND $ids";
    if ($row = $wpdb->get_row($sql, ARRAY_A)) {
        
        $steps = AMDATA_STEPS;
        $step = ($row['max']-$row['min'])/$steps;
        for ($i=0; $i<$steps; $i++) {
            $x[$i] = array('min'=>round($row['min']+$i*$step), 'max'=>round($row['min']+($i+1)*$step));
        }
        $tpl->assign('x', $x);
        
        $sql = "SELECT DISTINCT bankrates.*, banks.bank_name_ru AS bank_name,
                countries.country_name_ru AS country_name, economic_indicators.indicator_name_ru AS indicator_name
                FROM bankrates
                JOIN bankrates_values ON bankrates.id=bankrates_values.rate_id
                JOIN economic_indicators ON bankrates.rate_name_id=economic_indicators.id_indicator
                JOIN countries ON bankrates.country_id=countries.id_country
                JOIN banks ON bankrates.bank_id=banks.bank_id
                WHERE !bankrates_values.forecast AND bankrates_values.date_mod<=NOW() AND $bank AND $country AND $id AND $ids";
        $ids = array();
        if ($data = $wpdb->get_results($sql, ARRAY_A)) {
            foreach ($data as $key=>$item) {
                $last = 0;
                for ($i=0; $i<$steps; $i++) {
                    $sql = "SELECT value
                            FROM bankrates_values
                            WHERE !forecast AND date_mod<=NOW() AND rate_id={$item[id]}
                            AND date_mod>=FROM_UNIXTIME({$x[$i][min]}) AND date_mod<=FROM_UNIXTIME({$x[$i][max]})
                            ORDER BY date_mod DESC LIMIT 1";
                    if ($row = $wpdb->get_row($sql, ARRAY_A)) {
                        $data[$key]['data'][$i] = $row['value'];
                        $last = $row['value'];
                    }
                    else {
                        $data[$key]['data'][$i] = $last;
                    }
                }
            }
        }
        $tpl->assign('data', $data);
        
    }
    
    header('Content-type: text/xml');
    die($tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/amdata.tpl", $_SERVER['REQUEST_URI']));
    
}

function percent_rates_amsettings() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    header('Content-type: text/xml');
    die($tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/amsettings.tpl", $_SERVER['REQUEST_URI']));
    
}

function percent_rates_add() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin'] || !$sitepath['id']) {
        return false;
    }
    
    return true;
    
}

function percent_rates_edit() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin'] || !$sitepath['id']) {
        return false;
    }
    
    $sql = "SELECT *, UNIX_TIMESTAMP(date_mod) AS date_mod FROM bankrates_values WHERE id={$sitepath[id]}";
    $data = $wpdb->get_row($sql, ARRAY_A);
    $tpl->assign('data', $data);
    
    return true;
    
}

function percent_rates_redirect_insert() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin'] || !$sitepath['id']) {
        return false;
    }
    
    $value = (float)str_replace(',', '.', $_POST['value']);
    $forecast = $_POST['forecast'] ? 1 : 0;
    $sql = "INSERT INTO bankrates_values (rate_id, date_mod, value, forecast) VALUES (
            {$sitepath[id]},
            '{$_POST[timestamp][Date_Year]}-{$_POST[timestamp][Date_Month]}-{$_POST[timestamp][Date_Day]} {$_POST[timestamp][Time_Hour]}:{$_POST[timestamp][Time_Minute]}:00',
            $value,
            $forecast
            )";
    $wpdb->get_row($sql);
    
    $tpl->clearAllCache();
    
    return "/{$sitepath[0]}/filter/{$sitepath[parts][id]}";
    
}

function percent_rates_redirect_update() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin'] || !$sitepath['id']) {
        return false;
    }
    
    $sql = "SELECT * FROM bankrates_values WHERE id={$sitepath[id]}";
    $data = $wpdb->get_row($sql, ARRAY_A);
    
    $value = (float)str_replace(',', '.', $_POST['value']);
    $forecast = $_POST['forecast'] ? 1 : 0;
    $sql = "UPDATE bankrates_values SET
            date_mod = '{$_POST[timestamp][Date_Year]}-{$_POST[timestamp][Date_Month]}-{$_POST[timestamp][Date_Day]} {$_POST[timestamp][Time_Hour]}:{$_POST[timestamp][Time_Minute]}:00',
            value = $value,
            forecast = $forecast
            WHERE id={$sitepath[id]}";
    $wpdb->get_row($sql);
    
    $tpl->clearAllCache();
    
    return "/{$sitepath[0]}/filter/id-{$data[rate_id]}/";
    
}

function percent_rates_redirect_del() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin'] || !$sitepath['id']) {
        return false;
    }
    
    $sql = "DELETE FROM bankrates_values WHERE id={$sitepath[id]}";
    $wpdb->get_row($sql);
    
    $tpl->clearAllCache();
    
    return $_SERVER['HTTP_REFERER'];
    
}

function percent_rates_indexadd() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin']) {
        return false;
    }
    
    // Банки
    $sql = "SELECT * FROM banks ORDER BY bank_name_ru";
    if ($rows = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($rows as $item) {
            $banks[$item['bank_id']] = $item['bank_name_ru'];
        }
        $tpl->assign('banks', $banks);
    }
    
    // Страны
    $sql = "SELECT * FROM countries ORDER BY country_name_ru";
    if ($rows = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($rows as $item) {
            $countries[$item['id_country']] = $item['country_name_ru'];
        }
        $tpl->assign('countries', $countries);
    }
    
    // Индикаторы
    $sql = "SELECT * FROM economic_indicators ORDER BY indicator_name_ru";
    if ($rows = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($rows as $item) {
            $indicators[$item['id_indicator']] = $item['indicator_name_ru'];
        }
        $tpl->assign('indicators', $indicators);
    }
    
    return true;
    
}

function percent_rates_redirect_indexinsert() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin']) {
        return false;
    }
    
    $sql = "INSERT INTO bankrates (bank_id, country_id, rate_name_id) VALUES ({$_POST['bank']}, {$_POST['country']}, {$_POST['indicator']})";
    $wpdb->get_row($sql);
    
    $tpl->clearAllCache();
    
    return "/{$sitepath[0]}/";
    
}

function percent_rates_bankadd() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin']) {
        return false;
    }
    
    return true;
    
}

function percent_rates_redirect_bankinsert() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin']) {
        return false;
    }
    
    $sql = "INSERT INTO banks (bank_name_ru, bank_name_en) VALUES ('".mysql_escape_string($_POST['bank_name_ru'])."', '".mysql_escape_string($_POST['bank_name_en'])."')";
    $wpdb->get_row($sql);
    
    $tpl->clearAllCache();
    
    return "/{$sitepath[0]}/";
    
}
