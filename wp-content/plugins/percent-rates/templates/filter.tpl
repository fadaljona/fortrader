<div class="article-info">
	<a href="/">Главная</a> &rarr; {$breadcrumbs}
</div>
<h1 class="pd">{$title}</h1>
{if count($types)>1}
  <div class="options2">
    <div class="zag2">Виды ставок</div>
    {foreach $types as $item}
      <a href="/{$sitepath.0}/filter/id-{$item.id}/">{$item.indicator_name|escape}</a>
      {if !$item@last}<br />{/if}
    {/foreach}
  </div>
{/if}
{include file=$smarty.server.DOCUMENT_ROOT|cat:'/wp-content/plugins/percent-rates/templates/rates.tpl'}