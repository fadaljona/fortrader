<?xml version="1.0" encoding="UTF-8"?>
<chart>
  <series>
    {foreach $x as $item}
      <value xid="{$item@index}">{$item.max|date_format:'%d/%m/%Y'}</value>
    {/foreach}
  </series>
  <graphs>
    {foreach $data as $item}
      <graph gid="{$item@iteration}" title="{$item.indicator_name|escape} ({$item.country_name|escape})" line_width="2">
        {foreach $item.data as $item2}
          <value xid="{$item2@index}"{if $item2@index>10}{/if}>{{$item2}|string_format:'%.03f'}</value>
        {/foreach} 
      </graph>
    {/foreach}
  </graphs>
</chart>