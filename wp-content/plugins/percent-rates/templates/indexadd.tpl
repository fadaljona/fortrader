<h1 class="pd">Добавление индекса</h1>
<br /><br />
<form id="form_sranks" action="/{$sitepath.0}/indexinsert/" method="POST" class="validate">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
    <tr>
      <td width="23%">Банк</td>
      <td width="77%">{html_options class='sel forselect' name='bank' options=$banks}</td>
    </tr>
    <tr>
      <td>Страна</td>
      <td>{html_options class='sel forselect' name='country' options=$countries}</td>
    </tr>
    <tr>
      <td>Индикатор</td>
      <td>{html_options class='sel forselect' name='indicator' options=$indicators}</td>
    </tr>
  </table>
  <div class="dotted"></div>
  <input class="sub3" type="submit" value="Добавить" />
</form>