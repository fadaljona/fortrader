<div class="clear"></div>
 <p><a href="http://fortrader.org/zasedaniya-centrobankov-mira-v-2015-godu.html">Календарь заседаний Центробанков мира на 2015 год</a></p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="forum3">
  <tr>
    <th width="5%">&nbsp;</th>
    <th width="9%">&nbsp;</th>
    <th width="29%" class="left">Банк</th>
    <th width="29%">Ставка</th>
    {if $sitepath.admin}<th width="0%">&nbsp;</th>{/if}
    <th width="11%">Текущее значение</th>
    <th width="17%">Последее изменение</th>
  </tr>
  {foreach $page.data as $item}
    <tr>
      <td{if $item.forecast} style="background-color:#fdd;"{/if} class="num2">{counter}</td>
      <td{if $item.forecast} style="background-color:#fdd;"{/if} class="num">{if $item.image}<a href="/{$sitepath.0}/filter/bank-{$item.bank_id}/"><img src="{$item.image}" /></a>{/if}</td>
      <td{if $item.forecast} style="background-color:#fdd;"{/if} class="left f14"><a href="/{$sitepath.0}/filter/bank-{$item.bank_id}/">{$item.bank_name|escape}</a></td>
      <td{if $item.forecast} style="background-color:#fdd;"{/if}><a href="/{$sitepath.0}/filter/id-{$item.id}/" class="grey3">{$item.indicator_name|escape}</a></td>
      {if $sitepath.admin}
        <td class="num" style="white-space:nowrap;">
          <a href="/{$sitepath.0}/add/id-{$item.id}/" style="color:#900; text-decoration:none;"><img src="/wp-content/plugins/percent-rates/images/add.png" /></a>
          {if $item.value_id}
            <a href="/{$sitepath.0}/edit/id-{$item.value_id}/" style="color:#900; text-decoration:none;"><img src="/wp-content/plugins/percent-rates/images/edit.png" /></a>
            <a href="/{$sitepath.0}/del/id-{$item.value_id}/" style="color:#900; text-decoration:none;" onclick="return confirm('Удалить значение?');"><img src="/wp-content/plugins/percent-rates/images/del.png" /></a>
          {/if}
        </td>
      {/if}
      <td{if $item.forecast} style="background-color:#fdd;"{/if}>{$item.value}%<br /></td>
      <td{if $item.forecast} style="background-color:#fdd;"{/if}>
        {$item.date_mod|date_format:'%d.%m.%Y'}<br />
        {if $item.diff}<span class="{if $item.diff>0}green{else}red{/if} f10">{if $item.diff>0}+{/if}{$item.diff|string_format:'%.02f'}</span>{/if}
      </td>
    </tr>
  {/foreach}
</table>
{if $page.links}
  <div class="pages">
    {if $page.links.prev}<a class="prev" href="{$page.links.prev}">Пред.</a>{/if}
    <div class="pages2">
      {foreach $page.links.list as $item}
        <a href="{$item}"{if $item@key==$page.pages.current} class="on"{/if}>{$item@key}</a>{if !$item@last} &bull;{/if}
      {/foreach}
    </div>
    {if $page.links.next}<a class="next" href="{$page.links.next}">След.</a>{/if}
  </div>
  <div class="clear"></div>
  <br /><br />
{/if}
<h4>Динамика изменения процентных ставок</h4>
<div id="chartdiv"></div>
<script type="text/javascript" src="/wp-includes/amcharts/flash/swfobject.js"></script>
<script type="text/javascript">
    var params = {
        bgcolor:'#FFFFFF'
    };
    {*var urlpart = '{$sitepath.parts.bank}{$sitepath.parts.country}{$sitepath.parts.id}';*}
    var urlpart = '{if $ids}ids-{$ids}/{/if}';
    var flashVars = {
        path:'/wp-includes/amcharts/flash/',
        settings_file:'/{$sitepath.0}/amsettings/'+urlpart,
        data_file:'/{$sitepath.0}/amdata/'+urlpart
    };
    swfobject.embedSWF("/wp-includes/amcharts/flash/amline.swf", "chartdiv", "470", "470", "8.0.0", "/wp-includes/amcharts/flash/expressInstall.swf", flashVars, params);
</script>
