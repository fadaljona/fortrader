<div class="article-info">
	<a href="/">Главная</a> &rarr; Процентные ставки
</div>

{if $sitepath.admin}
  <div class="zag2">Администрирование</div>
  <a href="/{$sitepath.0}/indexadd/">Добавить индекс</a><br />
  <a href="/{$sitepath.0}/bankadd/">Добавить банк</a>
  <div class="clear"></div><br /><br />
{/if}
{if $featured}
  <div class="options2">
    <div class="zag2">Следующие заседания</div>
    {foreach $featured as $item}
      {if $item@index==ceil(count($featured)/2)}
        </div>
        <div class="options2">
        <div class="zag2">&nbsp;</div>
      {/if}
      <a href="/{$sitepath.0}/filter/id-{$item.id}/">{$item.name|escape}</a> - {$item.date_mod|date_format:'%d.%m.%Y'}<br />
    {/foreach}
  </div>
{/if}
{include file=$smarty.server.DOCUMENT_ROOT|cat:'/wp-content/plugins/percent-rates/templates/index_rates.tpl'}
<div class="katalog">
  <div class="kat-name"></div>
  <h4>Справочная информация</h4>
  {$content}
</div>