<h1 class="pd">{if $sitepath.1==add}Добавление{else}Редактирование{/if} значения</h1>
<br /><br />
<form action="/{$sitepath.0}/{if $sitepath.1==add}insert{else}update{/if}/{$sitepath.parts.id}" method="POST" class="validate">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
    <tr>
      <td width="23%">Дата</td>
      <td width="77%">{html_select_date field_array='timestamp' field_order='DMY' month_format='%b' start_year=1990 end_year=2020 all_extra='class="sel forselect" style="width:60px;"' time=$data.date_mod}&nbsp;&nbsp;&nbsp;{html_select_time field_array='timestamp' display_seconds=false all_extra='class="sel forselect" style="width:60px;"' time=$data.date_mod}</td>
    </tr>
    <tr>
      <td>Значение</td>
      <td><input class="inp2 required" name="value" type="text" value="{$data.value}" /></td>
    </tr>
    <tr>
      <td></td>
      <td><label><input name="forecast" type="checkbox"{if $data.foreacast} checked="checked"{/if} /></label> Прогнозируемое значение</td>
    </tr>
  </table>
  <div class="dotted"></div>
  <input class="sub3" type="submit" value="{if $sitepath.1==add}Добавить{else}Редактировать{/if}" />
</form>