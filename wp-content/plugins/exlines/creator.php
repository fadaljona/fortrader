<?php

class ExLineCreator {

    var $images = array(
        'zero_406x34' => array(
            'function' => 'createZeroImg',
            'start_point' => 165,
            'end_point' => 392,
            'text_block' => array(
                'start' => array(6, 6),
                'size' => array(76, 26),
                'color' => '939393',
            ),
            'counter' => array(
                'black' => array(
                    1 => array(97, 8),
                    array(127, 8),
                    'font-size' => 14,
                ),
                'info' => array(
                    1 => array(116, 13),
                    array(146, 13),
                    'font-size' => 9,
                ),
            ),
            'bar_height' => 34,
            'bar_top_margin' => 1,
            'markers' => 18,
        ),
        'zero_400x82' => array(
            'function' => 'createZeroImg',
            'start_point' => 14,
            'end_point' => 382,
            'text_block' => array(
                'start' => array(20, 6),
                'size' => array(286, 20),
                'color' => '939393',
            ),
            'counter' => array(
                'black' => array(
                    1 => array(309, 6),
                    array(339, 6),
                    'font-size' => 15,
                ),
                'info' => array(
                    1 => array(328, 11),
                    array(358, 11),
                    'font-size' => 9,
                ),
            ),
            'bar_height' => 82,
            'bar_top_margin' => 1,
            'markers' => 18,
        ),
        'zero_300x40_b' => array(
            'function' => 'createZeroImg',
            'start_point' => 87,
            'end_point' => 286,
            'text_block' => array(
                'start' => array(7, 6),
                'size' => array(78, 16),
                'color' => '939393',
            ),
            'counter' => array(
                'black' => array(
                    1 => array(10, 23),
                    array(38, 23),
                    'font-size' => 9,
                ),
                'info' => array(
                    1 => array(25, 25),
                    array(54, 25),
                    'font-size' => 9,
                ),
            ),
            'bar_height' => 40,
            'bar_top_margin' => 0,
            'markers' => 16,
        ),
        'zero_300x40' => array(
            'function' => 'createZeroImg',
            'start_point' => 87,
            'end_point' => 286,
            'text_block' => array(
                'start' => array(7, 4),
                'size' => array(76, 12),
                'color' => '939393',
            ),
            'counter' => array(
                'black' => array(
                    1 => array(10, 17),
                    array(40, 17),
                    'font-size' => 15,
                ),
                'info' => array(
                    1 => array(29, 24),
                    array(59, 24),
                    'font-size' => 9,
                ),
            ),
            'bar_height' => 40,
            'bar_top_margin' => 0,
            'markers' => 16,
        ),
        'gold_400x82' => array(
            'function' => 'createGoldImg',
            'start_point' => 20,
            'end_point' => 379,
            'text_block' => array(
                'start' => array(25, 4),
                'size' => array(275, 25),
                'color' => 'bf8301',
            ),
            'counter' => array(
                'start' => array(305, 10),
                'font-size' => 11,
            ),
            'bar_height' => 24,
            'bar_top_margin' => 28,
            'markers' => 18,
            'l_width' => 11,
            'r_width' => 11,
        ),
        'gold_406x34' => array(
            'function' => 'createGoldImg',
            'start_point' => 106,
            'end_point' => 389,
            'text_block' => array(
                'start' => array(11, 5),
                'size' => array(95, 15),
                'color' => 'bf8301',
            ),
            'counter' => array(
                'start' => array(11, 20),
                'font-size' => 11,
            ),
            'bar_height' => 7,
            'bar_top_margin' => 7,
            'markers' => 15,
            'l_width' => 3,
            'r_width' => 3,
        ),
        'gold_300x40' => array(
            'function' => 'createGoldImg',
            'start_point' => 100,
            'end_point' => 290,
            'text_block' => array(
                'start' => array(11, 5),
                'size' => array(90, 15),
                'color' => 'bf8301',
            ),
            'counter' => array(
                'start' => array(11, 25),
                'font-size' => 11,
            ),
            'bar_height' => 7,
            'bar_top_margin' => 9,
            'markers' => 10,
            'l_width' => 3,
            'r_width' => 3,
        ),
    );

    function __construct() {
        $this->font = ABSPATH . PLUGINDIR . "/exlines/lines/font.ttf";
        $this->font_small = ABSPATH . PLUGINDIR . "/exlines/lines/arial.ttf";
        $this->image_path = ABSPATH . PLUGINDIR . '/exlines/lines/';
    }

    function createText($font = '', $width = 50, $height = 50, $color = '000000', $text = 'Test', $font_size = 0) {
        $path = ABSPATH . PLUGINDIR . "/exlines/lines/tmp/" . uniqid(time()) . ".png";

        $command = "/usr/bin/convert"
                . (($width && $height) ? " -size " . $width . "x" . $height : '')
                . (($font_size) ? " -pointsize " . $font_size : '')
                . " -font " . $font . " -background none -geometry +0+0"
                . " -fill \#" . $color
                . (($width && $height) ? " caption:\"" . $text . "\" " : " label:\"" . $text . "\" ")
                . $path;
        system($command, $out);
//        var_dump($out, $command);

        $img = imagecreatefrompng($path);
        @chmod($path, 0777);
        @unlink($path);
        return $img;
    }

    function createDateText($params = array(), $markers = 0, $short = false) {
        global $wpdb;
        $result = array(11, 'л', 11, 'мес', 10);
        $datetime1 = (int) $params[1];
        $datetime2 = (int) $params[0];
//        $interval = date_diff($datetime1, $datetime2);

        $interval = $this->dateDiff($datetime1, $datetime2);

        if (($interval->m + ($interval->y * 12)) > $markers) {
            $result[0] = $interval->y;
            $result[1] = ($short) ? 'л' : 'лет';
            $result[2] = $interval->m;
            $result[3] = 'мес';
            $result[4] = $interval->y + $interval->m / 12;
        } else {
            $result[0] = $interval->m + ($interval->y * 12);
            $result[1] = ($short) ? 'м' : 'мес';
            $result[2] = $interval->d;
            $result[3] = 'дн.';
            $result[4] = $interval->m + $interval->d / 30;
        }

//        var_dump(
//                date_diff($datetime1, $datetime2),
//                date('Y-m-d', $params[1]),
//                date('Y-m-d', $params[0]),
//                $result[4]
//        );

        if ($result[4] > $markers)
            $result[4] = $markers;

        return $result;
    }

    function createImg($settings = '', $fill = null, $text = null, $name = null) {
        $text = str_replace('"', '\"', preg_replace('/[^A-zА-я0-9\-\+\=\,\.\_\(\$\@\&\%\:\?\)\n\'\"\s]+/ui', '', $text));

        if (
                !$text ||
                !isset($this->images[$settings]) ||
                !file_exists($this->image_path . '/' . $settings . '/background.png') ||
                !method_exists($this, $this->images[$settings]['function'])
        )
            return;

        $function = $this->images[$settings]['function'];
        $dst = $this->$function($settings, $fill, $text);
        $name = (!$name) ? md5(uniqid() . time() . $text . implode(',', $fill)) : $name;

        //imagepng($dst, $this->image_path . '/cache/' . $name);
        //@chmod($this->image_path . '/cache/' . $name, 0777);
		
	     imagepng($dst, '/var/www/fortrader/domains/files.fortrader.org/exlines_cache/' . $name);
         @chmod('/var/www/fortrader/domains/files.fortrader.org/exlines_cache/' . $name, 0777);

        return array(
            'name' => $name,
            'text' => $text,
            'template' => $settings,
            'date' => (int) $fill[0],
        );
    }

    function createZeroImg($settings, $fill = array(), $text = 'Test') {
        $settings_a = $this->images[$settings];
        $date_txt = $this->createDateText($fill, $settings_a['markers'], true);
        $params = getimagesize($this->image_path . '/' . $settings . '/background.png');

        $dst = imagecreatetruecolor($params[0], $params[1]);
        imagealphablending($dst, false);
        $col = imagecolorallocatealpha($dst, 255, 255, 255, 127);
        imagefilledrectangle($dst, 0, 0, $params[0], $params[1], $col);
        imagealphablending($dst, true);
        imagesavealpha($dst, true);

        $step = ($settings_a['end_point'] - $settings_a['start_point']) / $settings_a['markers'];

        imagecopyresampled($dst,
                imagecreatefrompng($this->image_path . '/' . $settings . '/loaded.png'),
                0, $settings_a['bar_top_margin'], 0, 0, $params[0],
                $settings_a['bar_height'], 1, $settings_a['bar_height']
        );

        imagecopyresampled($dst,
                imagecreatefrompng($this->image_path . '/' . $settings . '/empty.png'),
                $settings_a['start_point'] + ($step * $date_txt[4]),
                $settings_a['bar_top_margin'], 0, 0, $params[0],
                $settings_a['bar_height'], 1, $settings_a['bar_height']
        );

        imagecopy($dst,
                imagecreatefrompng($this->image_path . '/' . $settings . '/background.png'),
                0, 0, 0, 0, $params[0], $params[1]
        );

        imagecopy($dst, $this->createText(
                        $this->font,
                        $settings_a['text_block']['size'][0],
                        $settings_a['text_block']['size'][1],
                        $settings_a['text_block']['color'],
                        mb_strtoupper($text)
                ),
                $settings_a['text_block']['start'][0],
                $settings_a['text_block']['start'][1],
                0, 0,
                $settings_a['text_block']['size'][0],
                $settings_a['text_block']['size'][1]
        );

        $block_size = ($settings != 'zero_300x40_b') ? 15 : 11;

        $tmp = $this->createText(
                        $this->font_small,
                        0, 0,
                        'ffffff',
                        $date_txt[0], $settings_a['counter']['black']['font-size']
        );
        $t_w = imagesx($tmp);
        $t_h = imagesy($tmp);

        if ($t_w < $block_size) {
            $settings_a['counter']['black'][1][0] += $block_size - $t_w;
        }
        imagecopy($dst, $tmp,
                $settings_a['counter']['black'][1][0],
                $settings_a['counter']['black'][1][1],
                0, 0, $t_w, $t_h
        );
        $tmp = $this->createText(
                        $this->font_small,
                        0, 0,
                        'ffffff',
                        $date_txt[2], $settings_a['counter']['black']['font-size']
        );
        $t_w = imagesx($tmp);
        $t_h = imagesy($tmp);
        if ($t_w < $block_size) {
            $settings_a['counter']['black'][2][0] += $block_size - $t_w;
        }
        imagecopy($dst, $tmp,
                $settings_a['counter']['black'][2][0],
                $settings_a['counter']['black'][2][1],
                0, 0, $t_w, $t_h
        );

        $tmp = $this->createText(
                        $this->font_small,
                        0, 0,
                        '000000',
                        $date_txt[1], $settings_a['counter']['info']['font-size']
        );
        imagecopy($dst, $tmp,
                $settings_a['counter']['info'][1][0],
                $settings_a['counter']['info'][1][1],
                0, 0, imagesx($tmp), imagesy($tmp)
        );

        $tmp = $this->createText(
                        $this->font_small,
                        0, 0,
                        '000000',
                        $date_txt[3], $settings_a['counter']['info']['font-size']
        );
        imagecopy($dst, $tmp,
                $settings_a['counter']['info'][2][0],
                $settings_a['counter']['info'][2][1],
                0, 0, imagesx($tmp), imagesy($tmp)
        );

        return $dst;
    }

    function createGoldImg($settings, $fill = array(), $text = null) {
        $settings_a = $this->images[$settings];
        $date_txt = $this->createDateText($fill, $settings_a['markers']);
        $params = getimagesize($this->image_path . '/' . $settings . '/background.png');

        $dst = imagecreatetruecolor($params[0], $params[1]);
        imagealphablending($dst, false);
        $col = imagecolorallocatealpha($dst, 255, 255, 255, 127);
        imagefilledrectangle($dst, 0, 0, $params[0], $params[1], $col);
        imagealphablending($dst, true);
        imagesavealpha($dst, true);

        $src = imagecreatefrompng($this->image_path . '/' . $settings . '/background.png');
        $loaded = imagecreatefrompng($this->image_path . '/' . $settings . '/line.png');

        $r_c = imagecreatefrompng($this->image_path . '/' . $settings . '/r_corner.png');
        $r_c_size = array(
            'width' => imagesx($r_c),
            'height' => imagesy($r_c),
        );

        $l_c = imagecreatefrompng($this->image_path . '/' . $settings . '/l_corner.png');
        $l_c_size = array(
            'width' => imagesx($l_c),
            'height' => imagesy($l_c),
        );

        $step = ($settings_a['end_point'] - $settings_a['start_point']) / $settings_a['markers'];

        imagecopy($dst, $src, 0, 0, 0, 0, $params[0], $params[1]);
        $size = ($step * $date_txt['4']) - ($settings_a['end_point'] - $settings_a['start_point']);

        imagecopyresampled($dst, $loaded,
                $settings_a['start_point'] + $r_c_size['width'],
                $settings_a['bar_top_margin'], 0, 0,
                ($step * $date_txt['4']) - ($r_c_size['width'] + $l_c_size['width']) + ($size >= 0 ? 1 : 0),
                $settings_a['bar_height'], 1, $settings_a['bar_height']
        );

        imagecopy($dst, $l_c,
                $settings_a['start_point'],
                $settings_a['bar_top_margin'], 0, 0,
                (($step * $date_txt['4']) > $l_c_size['width'] ? $l_c_size['width'] : $step * $date_txt['4']),
                $settings_a['bar_height']
        );


        imagecopy($dst, $r_c,
                $settings_a['end_point'] - $r_c_size['width'],
                $settings_a['bar_top_margin'], 0, 0,
                ($size == 0 ? $r_c_size['width'] : 0),
                $settings_a['bar_height']
        );

        imagecopy($dst, $this->createText(
                        $this->font,
                        $settings_a['text_block']['size'][0],
                        $settings_a['text_block']['size'][1],
                        $settings_a['text_block']['color'],
                        $text
                ),
                $settings_a['text_block']['start'][0],
                $settings_a['text_block']['start'][1],
                0, 0,
                $settings_a['text_block']['size'][0],
                $settings_a['text_block']['size'][1]
        );

        array_pop($date_txt);
        $tmp = $this->createText(
                        $this->font_small,
                        0, 0,
                        'ffffff',
                        implode(' ', (array) $date_txt), $settings_a['counter']['font-size']
        );
        imagecopy($dst, $tmp,
                $settings_a['counter']['start'][0],
                $settings_a['counter']['start'][1],
                0, 0, imagesx($tmp), imagesy($tmp)
        );

        return $dst;
    }

    function dateDiff($time1, $time2) {
        if (!is_int($time1)) {
            $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
            $time2 = strtotime($time2);
        }

        if ($time1 > $time2) {
            $ttime = $time1;
            $time1 = $time2;
            $time2 = $ttime;
        }

        $intervals = array('y'=>'year', 'm'=>'month', 'd'=>'day', 'h'=>'hour', 'min'=>'minute', 'sec'=>'second');
        $diffs = new stdClass();

        foreach ($intervals as $k => $interval) {
            $diffs->$k = 0;
            $ttime = strtotime("+1 " . $interval, $time1);

            while ($time2 >= $ttime) {
                $time1 = $ttime;
                $diffs->$k++;

                $ttime = strtotime("+1 " . $interval, $time1);
            }
        }


        return $diffs;
    }

}
?>

