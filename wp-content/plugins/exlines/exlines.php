<?php

/*
  Plugin Name: exlines
  Plugin URI: none
  Description: биржевые линейки
  Author: Krasu
  Version: 0.1
  Author URI: none
 */
if (!function_exists('add_action')) {
    $wp_root = '../../..';
    if (file_exists($wp_root . '/wp-load.php')) {
        require_once($wp_root . '/wp-load.php');
    } else {
        require_once($wp_root . '/wp-config.php');
    }
}

if (!class_exists('ExLines')) {
    /* @var $wpdb wpdb */

    class ExLines {

        var $url_prefix = 'lines'; //equal to page slug
        /**
         * @var wpdb
         */
        var $db;
        var $user;
        var $texts = array();

        function __construct() {
            global $wpdb;
            $this->db = $wpdb;

            if (file_exists(ABSPATH . PLUGINDIR . "/exlines/texts") && is_readable(ABSPATH . PLUGINDIR . "/exlines/texts")) {
                $this->texts = explode("\n", file_get_contents(ABSPATH . PLUGINDIR . "/exlines/texts"));
            }
        }

        function getAllLines() {
            return $this->db->get_results('SELECT * FROM exlines_lines');
        }

        function getLine($name) {
            return $this->db->get_row($this->db->prepare(
                            'SELECT * FROM exlines_lines WHERE uid= %s'
                            , $name));
        }

        function insertLine($params) {
            return $this->db->insert('exlines_lines', array(
                'uid' => $params['name'],
                'date' => $params['date'],
                'template' => $params['template'],
                'text' => $params['text'],
                'created' => time(),
                'last_access' => 0,
            ));
        }

        function deleteLine($name) {
            if ($this->db->get_row($this->db->prepare(
                                    'DELETE FROM exlines_lines WHERE uid= %s'
                                    , $name))) {
              //  unlink(ABSPATH . PLUGINDIR . "/exlines/lines/cache/" . $lines[$i]->uid);
			 unlink("/var/www/fortrader/domains/files.fortrader.org/exlines_cache/" . $lines[$i]->uid);  
			}
        }

        function updateLineAccess($name) {
            return $this->db->get_row($this->db->prepare(
                            'UPDATE exlines_lines SET '
                            . 'view_count=view_count+1, last_access = "' . time() . '" WHERE uid= %s'
                            , $name));
        }

    }

    function exlines_WP_addAdminPage() {
        if (function_exists('add_menu_page')) {
            add_menu_page('Биржевые линейки', 'Биржевые линейки', 'manage_options', 'exlines/admin_page.php', '', plugins_url('exlines/marker.png'));
        }
    }

    add_action('admin_menu', 'exlines_WP_addAdminPage');
}
?>