=== Plugin Name ===
Contributors: krasu
Donate link: none
Tags: external content
Requires at least: 2.0.2
Tested up to: 2.9.2
Stable tag: 2.9.2

fxnow_top_brokers

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload this folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Place `<?php do_action('plugin_name_hook'); ?>` in your templates

