<?php
if (!function_exists('add_action')) {
    die();
}

if (!current_user_can('manage_options')) {
    die('Access Denied');
}


if (isset($_POST['save_phrases'])) {
    file_put_contents(ABSPATH . PLUGINDIR . "/exlines/texts", $_POST['texts']);
}

if (file_exists(ABSPATH . PLUGINDIR . "/exlines/texts") && is_readable(ABSPATH . PLUGINDIR . "/exlines/texts")) {
    $texts = file_get_contents(ABSPATH . PLUGINDIR . "/exlines/texts");
}
$total_lines_count = $wpdb->get_var('SELECT COUNT(uid) FROM exlines_lines');
$new_line = $wpdb->get_row('SELECT * FROM exlines_lines WHERE last_access!=0 ORDER BY created DESC LIMIT 1');
$old_line = $wpdb->get_row('SELECT * FROM exlines_lines WHERE last_access!=0 ORDER BY created ASC LIMIT 1');
$total_views_count = $wpdb->get_var('SELECT SUM(view_count) FROM exlines_lines');
$total_unactive_lines_count = $wpdb->get_row('SELECT COUNT(uid) as cnt, SUM(view_count) as view FROM exlines_lines WHERE last_access=0');
$last_month_lines_lines_count = $wpdb->get_row('SELECT COUNT(uid) as cnt, SUM(view_count) as view FROM exlines_lines WHERE last_access >=' . (time() - 3600 * 24 * 30));
$last_week_lines_lines_count = $wpdb->get_row('SELECT COUNT(uid) as cnt, SUM(view_count) as view FROM exlines_lines WHERE last_access >=' . (time() - 3600 * 24 * 7));
$last_day_lines_lines_count = $wpdb->get_row('SELECT COUNT(uid) as cnt, SUM(view_count) as view FROM exlines_lines WHERE last_access >='
                . mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')) . ' AND last_access <='
                . mktime(0, 0, 0, date('m'), date('d'), date('Y')));
?>
<style type="text/css" media="screen">
    #icon-wp-lines {
        background:url("<?php echo plugins_url() ?>/exlines/marker.png") no-repeat scroll 8px 8px transparent;
    }
</style>
<div class="wrap">
    <div id="icon-wp-lines" class="icon32"></div>
    <h2>Биржевые линейки</h2>

    <table width="100%">
        <tr>
            <td>
                <h3 class="hndle"><span>Статистика</span></h3>
                <div class="inside">
                    <p class="sub">Всего линеек: <?php echo (int) $total_lines_count; ?></p>
                    <table>
                        <tr>
                            <td>Активных за последний месяц:</td>
                            <td><?php echo (int) $last_month_lines_lines_count; ?></td>
                        </tr>
                        <tr>
                            <td>Активных за последнюю неделю:</td>
                            <td><?php echo (int) $last_week_lines_lines_count->cnt; ?></td>
                        </tr>
                        <tr>
                            <td>Активных вчера:</td>
                            <td><?php echo (int) $last_day_lines_lines_count->cnt; ?></td>
                        </tr>
                        <tr>
                            <td>Неактивированных:</td>
                            <td><?php echo (int) $total_unactive_lines_count->cnt; ?></td>
                        </tr>
                    </table>
                    <p class="sub">Всего показов: <?php echo (int) $total_views_count ?></p>
                    <table>
                        <tr>
                            <td>Показов за последний месяц:</td>
                            <td><?php echo (int) $last_month_lines_lines_count->view; ?></td>
                        </tr>
                        <tr>
                            <td>Показов за последнюю неделю:</td>
                            <td><?php echo (int) $last_week_lines_lines_count->view; ?></td>
                        </tr>
                        <tr>
                            <td>Показов вчера:</td>
                            <td><?php echo (int) $last_day_lines_lines_count->view; ?></td>
                        </tr>
                    </table>
                </div>
            </td>
            <td align="right" valign="top">
                <br />
                <form action="" method="post">
                    <textarea id="texts" name="texts" cols="45" rows="10"><?php echo $texts; ?></textarea>
                    <input type="submit" value="Сохранить список фраз" name="save_phrases" />
                </form>
            </td>
        </tr>
    </table>

    <br /><br />
    <table>
        <tr>
            <td colspan="2">Самая новая (активная):</td>
        </tr>
        <tr>
            <td colspan="2">
                <img src="<?php bloginfo('home'); ?>/ftline.php?line=<?php echo $new_line->uid ?>&nostat=1" alt="alt"/><br />
                <?php echo 'Зарегистрирована: ' . date('d.m.Y H:i:s', $new_line->created) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><br /><br /></td>
        </tr>
        <tr>
            <td colspan="2">Самая старая (активная):</td>
        </tr>
        <tr>
            <td colspan="2">
                <img src="<?php bloginfo('home'); ?>/ftline.php?line=<?php echo $old_line->uid ?>&nostat=1" alt="alt"/><br />
                <?php echo 'Зарегистрирована: ' . date('d.m.Y H:i:s', $old_line->created) ?>
            </td>
        </tr>
    </table>
</div>
