{include file=$smarty.server.DOCUMENT_ROOT|cat:'/wp-content/plugins/siterating/templates/header.tpl'}
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="forum3">
  <tr>
    <th width="4%">&nbsp;</th>
    <th width="8%">&nbsp;</th>
    <th width="39%" class="left">Сайт</th>
    <th width="12%">Хиты</th>
    <th width="13%">Хосты</th>
    <th width="7%">ТИЦ</th>
    <th width="8%">PR</th>
  </tr>
  {foreach $page.data as $item}
    <tr{if $item.status==new || $item.status==declined} style="background-color:#{if $item.status==new}fdd{else}ddf{/if};"{/if}>
      <td class="num2">{$item.position}</td>
      <td class="num">
        <a href="/{$sitepath.0}/detailed/id-{$item.id}/"><img src="/wp-content/plugins/siterating/images/{if $item.locked}lock{else}bars{/if}.png" alt="#" /></a>
        {if $sitepath.admin || $sitepath.user==$item.user_id}<a href="/{$sitepath.0}/edit/id-{$item.id}/"><img src="/wp-content/plugins/siterating/images/edit.png" alt="#" /></a>{/if}
      </td>
      <td class="left f14"><a target="_blank" href="/go.php?siteId={$item.id}">{$item.name|escape}</a></td>
      <td>{$item.today_hits}{if $item.diff.hits}<br /><span class="{if $item.diff.hits>0}green{else}red{/if} f10">{if $item.diff.hits>0}+{/if}{$item.diff.hits}</span>{/if}</td>
      <td>{$item.today_hosts}{if $item.diff.hosts}<br /><span class="{if $item.diff.hosts>0}green{else}red{/if} f10">{if $item.diff.hosts>0}+{/if}{$item.diff.hosts}</span>{/if}</td>
      <td>{if $item.tic==-1}&ndash;{else}{$item.tic}{/if}</td>
      <td>{if $item.pr==-1}&ndash;{else}{$item.pr}{/if}</td>
    </tr>
  {/foreach}
</table>
{if $page.links}
  <div class="pages">
    {if $page.links.prev}<a class="prev" href="{$page.links.prev}">Пред.</a>{/if}
    <div class="pages2">
      {foreach $page.links.list as $item}
        <a href="{$item}"{if $item@key==$page.pages.current} class="on"{/if}>{$item@key}</a>{if !$item@last} &bull;{/if}
      {/foreach}
    </div>
    {if $page.links.next}<a class="next" href="{$page.links.next}">След.</a>{/if}
  </div>
  <div class="clear"></div>
{/if}
<br /><br /><h4>Статистика рейтинга</h4><br />
 <table width="60%" border="0" cellspacing="0" cellpadding="0" class="tabl">
  <tr>
    <td class="grey2">Пользователей:</td>
    <td>{$stat.users}</td>
  </tr>
  <tr>
    <td class="grey2">Всего сайтов:</td>
    <td>{$stat.sites}</td>
  </tr>
  <tr>
    <td class="grey2">Общее кол-во хитов:</td>
    <td>{$stat.hits}</td>
  </tr>
  <tr>
    <td class="grey2">Общее кол-во хостов:</td>
    <td>{$stat.hosts}</td>
  </tr>
  <tr>
    <td class="grey2">Обновление ТИЦ:</td>
    <td>{$settings.tic|date_format:'%d.%m.%Y'}</td>
  </tr>
  <tr>
    <td class="grey2">Обновление PR:</td>
    <td>{$settings.pr|date_format:'%d.%m.%Y'}</td>
  </tr>
</table>
<p>&nbsp;</p>
<h4>Статистика посещения всех сайтов рейтинга</h4>
<div class="katalog">
  <div id="chartdiv"></div>
  <script type="text/javascript" src="/wp-includes/amcharts/flash/swfobject.js"></script>
  <script type="text/javascript">
    var params = {
        bgcolor:'#FFFFFF'
    };
    var flashVars = {
        path:'/wp-includes/amcharts/flash/',
        settings_file:'/{$sitepath.0}/amsettings/',
        data_file:'/{$sitepath.0}/amdata/{$sitepath.parts.id}'
    };
    swfobject.embedSWF("/wp-includes/amcharts/flash/amline.swf", "chartdiv", "470", "350", "8.0.0", "/wp-includes/amcharts/flash/expressInstall.swf", flashVars, params);
  </script>
</div>