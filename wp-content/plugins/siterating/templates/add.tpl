<div class="title">{if $sitepath.1==edit}Редактирование сайта {$data.url}{else}Добавить сайт в рейтинг{/if}</div>
{include file=$smarty.server.DOCUMENT_ROOT|cat:'/wp-content/plugins/siterating/templates/header.tpl'}
<div class="title3">Добавить свой сайт в рейтинг</div><br />
{include file=$smarty.server.DOCUMENT_ROOT|cat:'/wp-content/plugins/siterating/templates/form.tpl'}
<br /><br />
<h4>Дополнительная информация</h4>
<div class="katalog">
  <div class="kat-name">Что дает участие в рейтинге сайтов?</div>
  <p>Принимая участие в нашем рейтинге сайтов, вы сможете получить до  30% посетителей нашего сайта. Чем выше рейтинг вашего сайта, тем большее  доверие, он вызывает у ваших посетителей.</p>
</div>
<br />