<div class="title">Статистика сайта {$data.url}</div>
{include file=$smarty.server.DOCUMENT_ROOT|cat:'/wp-content/plugins/siterating/templates/header.tpl'}
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="forum3">
  <tr>
    <th width="4%">Год</th>
    <th width="8%">Число</th>
    <th width="9%">ХОСТЫ</th>
    <th width="10%">ХИТЫ</th>
    <th width="15%">Место в рейтинге</th>
  </tr>
  {foreach $page.data as $item}
    <tr>
      <td class="num2">{if $year!=$item.date|date_format:'%Y'}{$item.date|date_format:'%Y'}{assign var=year value=$item.date|date_format:'%Y'}{/if}</td>
      <td class="num">{$item.date|date_format:'%d'} {$months[intval($item.date|date_format:'%m')]}</td>
      <td>{$item.hosts}{if $item.diff.hosts}<br /><span class="{if $item.diff.hosts>0}green{else}red{/if} f10">{if $item.diff.hosts>0}+{/if}{$item.diff.hosts}</span>{/if}</td>
      <td>{$item.hits}{if $item.diff.hits}<br /><span class="{if $item.diff.hits>0}green{else}red{/if} f10">{if $item.diff.hits>0}+{/if}{$item.diff.hits}</span>{/if}</td>
      <td>{$item.position}{if $item.diff.position}<br /><span class="{if $item.diff.position>0}red{else}green{/if} f10">{if $item.diff.position>0}+{/if}{$item.diff.position}</span>{/if}</td>
    </tr>
  {/foreach}
</table>
{if $page.links}
  <div class="pages">
    {if $page.links.prev}<a class="prev" href="{$page.links.prev}">Пред.</a>{/if}
    <div class="pages2">
      {foreach $page.links.list as $item}
        <a href="{$item}"{if $item@key==$page.pages.current} class="on"{/if}>{$item@key}</a>{if !$item@last} &bull;{/if}
      {/foreach}
    </div>
    {if $page.links.next}<a class="next" href="{$page.links.next}">След.</a>{/if}
  </div>
  <div class="clear"></div>
  <br /><br />
{/if}
{if $page.data}
  <h4>Динамика посещения сайта</h4>
  <div class="katalog">
    <div id="chartdiv"></div>
    <script type="text/javascript" src="/wp-includes/amcharts/flash/swfobject.js"></script>
    <script type="text/javascript">
        var params = {
            bgcolor:'#FFFFFF'
        };
        var flashVars = {
            path:'/wp-includes/amcharts/flash/',
            settings_file:'/{$sitepath.0}/amsettings/',
            data_file:'/{$sitepath.0}/amdata/{$sitepath.parts.id}'
        };
        swfobject.embedSWF("/wp-includes/amcharts/flash/amline.swf", "chartdiv", "470", "350", "8.0.0", "/wp-includes/amcharts/flash/expressInstall.swf", flashVars, params);
    </script>
  </div>
{/if}
<h4>СТАТИСТИКА</h4><br />
<table width="60%" border="0" cellspacing="0" cellpadding="0" class="tabl">
  {*<tr>
    <td class="grey2">Всего посетителей сайта:</td>
    <td>234</td>
  </tr>*}
  <tr>
    <td class="grey2">В среднем посещений в день:</td>
    <td>{$data.stat.avg_hosts}</td>
  </tr>
  <tr>
    <td class="grey2">Максимум посетителей в день:</td>
    <td>{$data.stat.max_hosts}</td>
  </tr>
  {if $data.tic>-1}
    <tr>
      <td class="grey2"> ТиЦ:</td>
      <td>{$data.tic}</td>
    </tr>
  {/if}
  {if $data.pr>-1}
    <tr>
      <td class="grey2"> PR:</td>
      <td>{$data.pr}</td>
    </tr>
  {/if}
</table>
{*<h4>Дополнительная информация</h4>
<div class="katalog">
   <div class="kat-name"></div>
 Данный сайт так же участвует в <a href="#">каталоге форекс сайтов</a>, <a href="#">рейтинге партнеров</a>, <a href="#">рейтинге блогеров</a>.
</div><br />*}