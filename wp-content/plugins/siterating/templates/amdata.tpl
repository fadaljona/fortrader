<?xml version="1.0" encoding="UTF-8"?>
<chart>
  <series>
    {foreach $data as $item}
      <value xid="{$item@index}">{$item.date|date_format:'%d.%m.%Y'}</value>
    {/foreach}
  </series>
  <graphs>
    <graph gid="1" title="Хосты{if !$sitepath.id}, тыс{/if}" bullet="round" line_width="2">
      {foreach $data as $item}
        <value xid="{$item@index}">{$item.hosts}</value>
      {/foreach}
    </graph>
    <graph gid="2" title="Хиты{if !$sitepath.id}, тыс{/if}" bullet="round" line_width="2">
      {foreach $data as $item}
        <value xid="{$item@index}">{$item.hits}</value>
      {/foreach}
    </graph>
  </graphs>
</chart>