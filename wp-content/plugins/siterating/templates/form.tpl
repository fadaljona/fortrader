<form action="/{$sitepath.0}/{if $sitepath.1==add}insert/{else}update/{$sitepath.parts.id}{$sitepath.parts.sign}{/if}" method="post" class="validate" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
  <tr>
    <td>Название сайта</td>
    <td><input class="inp2 required" name="name" type="text" value="{$data.name|escape}" /></td>
  </tr>
  <tr>
    <td width="23%">Адрес сайта</td>
    <td width="77%"><input class="inp2 required" name="url" type="text" value="{$data.url|escape}" /></td>
  </tr>
  <tr>
    <td>Рубрика</td>
    <td>{html_options name=rubric_id class='inp2 required' options=$rubrics value=$data.rubric_id}</td>
  </tr>
  <tr>
    <td>Доступ к статистике</td>
    <td>{html_radios options=$locked selected=$data.locked separator='&nbsp;&nbsp;'}</td>
  </tr>
  <tr>
    <td>Описание сайта</td>
    <td><textarea class="area" name="description" cols="" rows="">{$data.description|escape}</textarea></td>
  </tr>
  {if $data.logo}
  <tr>
    <td>Логотип</td>
    <td><img src="{$data.logo}" /></td>
  </tr>
  {/if}
  <tr>
    <td>{if $data.logo}Новый логотип{else}Логотип{/if}</td>
    <td><input type="file" name="logo" /></td>
  </tr>
  {if $sitepath.admin && $sitepath.1==edit}
    <tr>
      <td>Статус</td>
      <td>{html_options name=status options=$status class=inp2 selected=$data.status}</td>
    </tr>
  {/if}
  {if $sitepath.1==edit}
    <tr>
      <td></td>
      <td><a href="/{$sitepath.0}/del/{$sitepath.parts.id}{$sitepath.parts.sign}" onclick="return confirm('Удалить сайт из рейтинга?');">Удалить</a></td>
    </tr>
  {/if}
</table>
<div class="dotted"></div>
<input class="sub3" type="submit" value="{if $sitepath.1==add}Добавить{else}Отправить{/if}" />
</form>