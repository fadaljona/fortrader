<?php
/*
  Plugin Name: siterating
  Plugin URI: http://mrdealer.ru/
  Description:Percent rates
  Author: SMaster
  Version: 0.1
  Author URI: http://nashe-vse.ru/
*/

//************* Init ******************

if (SMASTER!==true) {
    
    define ('SMASTER', true);
    
    include 'functions.inc.php';
    include 'smarty/Smarty.class.php';
    
    $sitepath = urlparse();
    
    $dir = pathinfo(__FILE__, PATHINFO_DIRNAME);
    $tpl = new Smarty();
    $tpl->caching      = Smarty::CACHING_LIFETIME_CURRENT;
    $tpl->cache_dir    = "$dir/cache/";
    $tpl->compile_dir  = "$dir/templates_c/";
    //$tpl->registerFilter('pre', 'pre_iconv');
    
}

define('SITERATING_PER_PAGE', 20);

//************* Filters ***************

add_filter('init', 'siterating_init');

function siterating_init() {
    
    global $sitepath, $tpl, $wpdb, $company_id, $company_id_url;
    
    if ($sitepath[0]=='siterating') {
        
        add_filter('the_content', 'siterating_the_content');
        add_filter('wp_title', 'siterating_wp_title');
        
        $current_user = wp_get_current_user();
        $sitepath['user'] = $current_user->ID;
        if (is_super_admin($current_user->ID)) {
            $sitepath['admin'] = true;
        }
        
        if ($sitepath[1] && function_exists("siterating_redirect_{$sitepath[1]}")) {
            $tpl->caching = Smarty::CACHING_OFF;
            if ($url=call_user_func("siterating_redirect_{$sitepath[1]}")) {
                redirect($url);
            }
        }
        elseif (!$sitepath[1] || function_exists("siterating_{$sitepath[1]}")) {
            $_SERVER['REQUEST_URI'] = $sitepath['admin'] ? $_SERVER['REQUEST_URI'].'admin' :  $_SERVER['REQUEST_URI'];
            if ($sitepath[1]) {
                if ($tpl->isCached(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/{$sitepath[1]}.tpl", $_SERVER['REQUEST_URI'])) {
                    $ok = true;
                }
                else {
                    siterating_header();
                    $ok = call_user_func("siterating_{$sitepath[1]}");
                }
                $_GET['hide_title'] = true;
            }
            else {
                if ($tpl->isCached(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/index.tpl", $_SERVER['REQUEST_URI'])) {
                    $ok = true;
                }
                else {
                    siterating_header();
                    $ok = siterating();
                }
            }
            if ($ok) {
                $_SERVER['REQUEST_URI_SAVE'] = $_SERVER['REQUEST_URI'];
                $_SERVER['REQUEST_URI'] = "/{$sitepath[0]}.html";
            }
        }
    }
    
}

function siterating_the_content($content) {
    
    /*global $sitepath, $tpl, $wpdb;
    
    $tpl->assign_by_ref('sitepath', $sitepath);
    $tpl->assign('content', $content);
    
    $template = $sitepath[1] ? "{$sitepath[1]}.tpl" : 'index.tpl';
    $content = $tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/$template", $_SERVER['REQUEST_URI_SAVE']);
    */
    return $content;
    
}

function siterating_wp_title($title) {
    
    global $sitepath, $tpl, $wpdb;
    
    if ($sitepath[1]=='detailed' && $sitepath['id']) {
        $sql = "SELECT * FROM stat_sites WHERE id={$sitepath[id]}";
        $row = $wpdb->get_row($sql, ARRAY_A);
        return "Статистика сайта {$row['url']} &ndash; ";
    }
    
    return $title;
    
}

function siterating_header() {
    
    global $sitepath, $tpl, $wpdb;
    
    // Настройки
    $sql = "SELECT * FROM stat_settings";
    if ($rows=$wpdb->get_results($sql, ARRAY_A)) {
        foreach ($rows as $item) {
            $settings[$item['var']] = $item['value'];
        }
    }
    $tpl->assign('settings', $settings);
    
    // Очищаем кеш нужных шаблонов
    if ($settings['cleared']<$settings['update']) {
        $sql = "UPDATE stat_settings SET value=CAST(UNIX_TIMESTAMP(NOW()) AS CHAR(20)) WHERE var='cleared'";
        $wpdb->query($sql);
        $tpl->clearCache(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/index.tpl");
        $tpl->clearCache(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/detailed.tpl");
    }
    
    // Новички рейтинга
    $sql = "SELECT * FROM stat_sites WHERE status='approved' ORDER BY add_date DESC LIMIT 3";
    $novices = $wpdb->get_results($sql, ARRAY_A);
    $tpl->assign('novices', $novices);
    
}

//************* Content ***************

function siterating() {
    
    global $sitepath, $tpl, $wpdb;
    
    siterating_header();
    
    // Количество сайтов
    $sql = "SELECT COUNT(*) AS count FROM stat_sites";
    $row = $wpdb->get_row($sql, ARRAY_A);
    $stat['sites'] = $row['count'];
    
    // Элементы запроса
    $status = $sitepath['admin'] ? 1 : "status='approved'";
    $order_by = $sitepath['admin'] ? "status_order_by, " : '';
    
    // Сайты страницы
    $sql = "
	SELECT 
		stat_sites.*, 
		stat_sites.hosts AS today_hosts, 
		stat_sites.users AS today_users, 
		stat_sites.hits AS today_hits,
		stat_days.hosts, 
		stat_days.users, 
		stat_days.hits,
		IF(stat_sites.status='new', 0, IF(stat_sites.status='approved', 1, 2)) AS status_order_by
	FROM 
		stat_sites 
	LEFT JOIN 
		stat_days ON stat_sites.id=stat_days.site_id AND ( ( stat_days.date BETWEEN CONCAT(CURDATE() - INTERVAL 22 DAY, ' ', '00:00') AND CONCAT(CURDATE() - INTERVAL 22 DAY, ' ', '23:59') ) OR stat_days.date IS NULL )
	WHERE 
		$status AND stat_days.id IS NOT NULL
	ORDER BY  
		$order_by stat_days.date DESC, stat_sites.hosts DESC;
";
    $page = pager($sql, SITERATING_PER_PAGE, $sitepath['page'], "/{$sitepath[0]}/", true);
    if ($page['data']) {
        foreach ($page['data'] as $key=>$item) {
            
            // Вычисляем общую позицию
            $page['data'][$key]['position'] = ($sitepath['page'] ? ($sitepath['page']-1)*SITERATING_PER_PAGE : 0) + $key + 1;
            
            // Вчерашнее значение и сравнение с позавчерашним
            /*$sql = "SELECT * FROM stat_days WHERE site_id={$item[id]} AND stat_days.date=CURDATE() - INTERVAL 2 DAY";
            if ($row = $wpdb->get_row($sql, ARRAY_A)) {
                $page['data'][$key]['diff'] = array(
                    'hosts' => $item['hosts'] - $row['hosts'],
                    'users' => $item['users'] - $row['users'],
                    'hits'  => $item['hits']  - $row['hits']
                );
            }*/
            
            // Сравнение с сегодняшним значением
            $page['data'][$key]['diff'] = array(
                'hosts' => $item['today_hosts'] - $item['hosts'],
                'users' => $item['today_users'] - $item['users'],
                'hits'  => $item['today_hits']  - $item['hits']
            );
            
        }
    }
    $tpl->assign('page', $page);
    
    // Общая статистика за сегодня
    $sql = "SELECT COUNT(id) AS sites, SUM(hosts) AS hosts, SUM(users) AS users, SUM(hits) AS hits FROM stat_sites";
    $stat = $wpdb->get_row($sql, ARRAY_A);
    $tpl->assign('stat', $stat);
    
    return true;
    
}

function siterating_img() {
    
    global $sitepath, $tpl, $wpdb;
    
    $_GET['id'] = intval($_GET['id']);
    if ($_GET['id'] && $wpdb->get_row("SELECT * FROM stat_sites WHERE id={$_GET['id']} AND status='approved'")) {
        
        // Определим IP-адрес и адрес прокси
        if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
            $proxy = $_SERVER['HTTP_CLIENT_IP'] ? $_SERVER['HTTP_CLIENT_IP'] : $_SERVER['REMOTE_ADDR'];
            list($ip) = explode(',', $_SERVER["HTTP_X_FORWARDED_FOR"]);
            $ip = trim($ip);
        } else {
            $ip = $_SERVER['HTTP_CLIENT_IP'] ? $_SERVER['HTTP_CLIENT_IP'] : $_SERVER['REMOTE_ADDR'];
        }
        $ip = ip2long($ip);
        $user_md5 = md5($ip.$proxy.$_GET['width'].$_GET['height'].$_GET['colors'].$_SERVER['HTTP_USER_AGENT']);
        $width = intval($_GET['width']);
        $height = intval($_GET['height']);
        $colors = intval($_GET['colors']);
        
        $sql = $wpdb->prepare( "INSERT INTO stat_log (site_id, ip, user_md5, width, height, colors, agent, page, referer) VALUES({$_GET[id]},'$ip','$user_md5',$width,$height,$colors,SUBSTRING(%s, 1, 256),SUBSTRING(%s, 1, 256),SUBSTRING(%s, 1, 256))", $_SERVER['HTTP_USER_AGENT'], $_GET['page'], $_GET['referer'] );
        $wpdb->get_row($sql);
        
    }
    
    $dir = "{$_SERVER[DOCUMENT_ROOT]}/wp-content/plugins/siterating/images";
    $filename = file_exists("$dir/cache/{$_GET[id]}.png")
              ? "$dir/cache/{$_GET[id]}.png"
              : "$dir/img_new.png";
    
    header('Content-type: image/png');     
    die(file_get_contents($filename));
    
}

function siterating_amdata() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    for ($i=60; $i; $i--) {
        $sql = $sitepath['id']
             ? "SELECT *, UNIX_TIMESTAMP(date) AS date FROM stat_days WHERE site_id={$sitepath['id']} AND date=CURDATE() - INTERVAL $i DAY"
             : "SELECT SUM(stat_days.hosts)/1000 AS hosts, SUM(stat_days.users)/1000 AS users,
                SUM(stat_days.hits)/1000 AS hits, UNIX_TIMESTAMP(stat_days.date) AS date
                FROM stat_days JOIN stat_sites ON stat_days.site_id=stat_sites.id
                WHERE stat_days.date=CURDATE() - INTERVAL $i DAY AND stat_sites.status='approved'
                GROUP BY stat_days.date";
        if ($row = $wpdb->get_row($sql, ARRAY_A)) {
            $tpl->append('data', $row);
        }
    }
    
    header('Content-type: text/xml');
    die($tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/amdata.tpl", $_SERVER['REQUEST_URI']));
    
}

function siterating_amsettings() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    header('Content-type: text/xml');
    die($tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/amsettings.tpl", $_SERVER['REQUEST_URI']));
    
}

function siterating_my() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['admin'] && !$sitepath['user']) {
        redirect('/wp-login.php');
    }
    if (!$sitepath['admin'] || !$sitepath['id']) {
        $sitepath['id'] = $sitepath['user'];
    }
    
    $sql = "SELECT * FROM stat_sites WHERE user_id={$sitepath['id']} ORDER BY name";
    $data = $wpdb->get_results($sql, ARRAY_A);
    $tpl->assign('data', $data);
    
    return true;
    
}

function siterating_counter() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['id']) {
        return false;
    }
    if (!$sitepath['admin'] && !$sitepath['user']) {
        redirect('/wp-login.php');
    }
    
    $sql = "SELECT * FROM stat_sites WHERE id={$sitepath['id']} ORDER BY name";
    $data = $wpdb->get_row($sql, ARRAY_A);
    $tpl->assign('data', $data);
    
    $_SERVER['HTTP_HOST'] = preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']);
    
    return true;
    
}

function siterating_detailed() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['id']) {
        return false;
    }
    
    $sql = "SELECT * FROM stat_sites WHERE id={$sitepath['id']} ORDER BY name";
    $data = $wpdb->get_row($sql, ARRAY_A);
    
    // Если заблокирована, выключаем кеширование ипроверяем пользователя
    if ($data['locked']) {
        if (!$sitepath['admin'] && !$sitepath['user']) {
            redirect('/wp-login.php');
        }
        $tpl->caching = false;
    }
    
    // Максимум, в среднем посетителей в день
    $sql = "SELECT ROUND(MAX(hosts)) AS max_hosts, ROUND(AVG(hosts)) AS avg_hosts,
            ROUND(MAX(users)) AS max_users, ROUND(AVG(users)) AS avg_users,
            ROUND(MAX(hits)) AS max_hits, ROUND(AVG(hits)) AS avg_hits
            FROM stat_days WHERE site_id={$sitepath['id']}";
    $data['stat'] = $wpdb->get_row($sql, ARRAY_A);
    $tpl->assign('data', $data);
    
    // Данные страницы
    $sql = "SELECT *, UNIX_TIMESTAMP(date) AS date FROM stat_days WHERE site_id={$sitepath['id']} ORDER BY date DESC";
    $page = pager($sql, SITERATING_PER_PAGE, $sitepath['page'], "/{$sitepath[0]}/detailed/{$sitepath[parts][id]}");
    if ($page['data']) {
        foreach ($page['data'] as $key=>$item) {
            
            // Позиция
			if( isset( $item['id'] ) && $item['id'] ){
				$sql = "SELECT COUNT(*) AS count FROM stat_days WHERE id!={$item['id']} AND date=FROM_UNIXTIME({$item['date']}) AND hits>{$item['hits']}";
				$row = $wpdb->get_row($sql, ARRAY_A);
				$page['data'][$key]['position'] = $row['count']+1;
			}
            
            // Изменение по отношению к предыдущему дню
            $sql = "SELECT * FROM stat_days WHERE site_id={$sitepath['id']} AND date=FROM_UNIXTIME({$item['date']}) - INTERVAL 1 DAY";
            $row = $wpdb->get_row($sql, ARRAY_A);
            $page['data'][$key]['diff'] = array(
                'hosts' => $item['hosts']-$row['hosts'],
                'users' => $item['users']-$row['users'],
                'hits'  => $item['hits']-$row['hits']
            );
            
            // Изменение позиции по отношению к предыдущему дню
			if( isset( $row['id'] ) && $row['id'] ){
				$sql = "SELECT COUNT(*) AS count FROM stat_days WHERE id!={$row['id']} AND date=FROM_UNIXTIME({$item['date']}) - INTERVAL 1 DAY AND hits>{$row['hits']}";
				$row = $wpdb->get_row($sql, ARRAY_A);
				$page['data'][$key]['diff']['position'] = $page['data'][$key]['position']-$row['count']-1;
			}
            
        }
    }
    $tpl->assign('page', $page);
    
    // Месяцы
    $months = array(1=>'Янв', 2=>'Фев', 3=>'Мар', 4=>'Апр', 5=>'Мая', 6=>'Июн', 7=>'Июл', 8=>'Авг', 9=>'Сен', 10=>'Окт', 11=>'Ноя', 12=>'Дек');
    foreach ($months as $key=>$item) {
        $months[$key] = $item;
    }
    $tpl->assign('months', $months);
    
    $_SERVER['HTTP_HOST'] = preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']);
    
    return true;
    
}

function siterating_add() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['user']) {
        redirect('/wp-login.php');
    }
    
    // Достаем рубрики
    $rubrics[''] = '-- Выберите рубрику --';
    $sql = "SELECT * FROM stat_rubrics ORDER BY name";
    if ($rows = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($rows as $row) {
            $rubrics[$row['id']] = $row['name'];
        }
    }
    $tpl->assign('rubrics', $rubrics);
    
    $locked = array('Публичный', 'Закрытый');
    $tpl->assign('locked', $locked);
    
    $tpl->assign('data', array('locked'=>0));
    
    return true;
    
}

function siterating_edit() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['id']) {
        return false;
    }
    if ($sitepath['vars']['sign']==md5("It is sign for {$sitepath['id']}")) {
        $sitepath['admin'] = true;
    }
    
    // Достаем данные
    $sql = "SELECT * FROM stat_sites WHERE id={$sitepath[id]}";
    $data = $wpdb->get_row($sql, ARRAY_A);
    if (!$sitepath['admin'] && $data['user_id']!=$sitepath['user']) {
        redirect('/wp-login.php');
    }
    
    // Логотип
    $filename = "/wp-content/plugins/siterating/images/logos/{$data[id]}.gif";
    if (file_exists($_SERVER['DOCUMENT_ROOT'].$filename)) {
        $data['logo'] = $filename;
    }
    $tpl->assign('data', $data);
    
    // Достаем рубрики
    $sql = "SELECT * FROM stat_rubrics ORDER BY name";
    if ($rows = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($rows as $row) {
            $rubrics[$row['id']] = $row['name'];
        }
    }
    $tpl->assign('rubrics', $rubrics);
    
    $locked = array('Публичный', 'Закрытый');
    $tpl->assign('locked', $locked);
    
    $status = array(
                  'new'=>'На проверке',
                  'approved'=> 'Одобрен',
                  'declined'=> 'Отклонен'
              );
    $tpl->assign('status', $status);
    
    return true;
    
}

function siterating_redirect_insert() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['user']) {
        return false;
    }
    
    // Достаем URL в красивом виде
    $urlparts = parse_url(str_replace('\\\\', '/', $_POST['url']));
    $urlparts['host'] = preg_replace('/^www\./', '', $urlparts['host']);
    $url = $urlparts['host'] . ($urlparts['path']=='/' ? '' : $urlparts['path']) . ($urlparts['query'] ? '?'.$urlparts['query'] : '');
    
    $locked = intval($_POST['locked']);
    $wpdb->insert( 
		'stat_sites', 
		array( 
			'user_id' => $sitepath['user'], 
			'rubric_id' => $_POST['rubric_id'],
			'name' => $_POST['name'],
			'url' => $url,
			'description' => $_POST['description'],
			'locked' => $locked,
		), 
		array( 
			'%d', 
			'%d',
			'%s',
			'%s',
			'%s',
			'%d',
		) 
	);
	$id = $wpdb->insert_id;
    
    // Загружаем логотип
    if (is_uploaded_file($_FILES['logo']['tmp_name'])) {
        $filename = "/wp-content/plugins/siterating/images/logos/$id.gif";
        img($_FILES['logo']['tmp_name'], $filename, 99, 48);
    }
    
    // Передаем переменные в шаблон, чистим кэш
    $tpl->assign('sitepath', $sitepath);
    $tpl->assign('url', $url);
    $tpl->assign('id', $id);
    $tpl->assign('sign', md5("It is sign for $id"));
    $tpl->clearAllCache();
    
    // Найдем мыло админа
    $sql = "SELECT * FROM wp_users WHERE ID=1";
    $row = $wpdb->get_row($sql, ARRAY_A);
    
    // Отправим письмо админу
    include 'htmlMimeMail/htmlMimeMail.php';
    $mail = new htmlMimeMail();
    $mail->setHtmlCharset('utf-8');
	$mail->setHeadCharset('utf-8');
    $mail->setFrom('noreply@'.preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']));
    $subject = "Заявка на добавление нового сайта в рейтинг";
    $mail->setSubject($subject);
    $mail->setHtml($tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME).'/templates/email_insert.tpl'));
    @$mail->send(array($row['user_email']));
    
    return "/{$sitepath[0]}/counter/id-$id/";
    
}

function siterating_redirect_update() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if ((!$sitepath['id'] || !$sitepath['user']) && $sitepath['vars']['sign']!=md5("It is sign for {$sitepath['id']}")) {
        return false;
    }
    if ($sitepath['vars']['sign']==md5("It is sign for {$sitepath['id']}")) {
        $sitepath['admin'] = true;
    }
    
    $sql = "SELECT * FROM stat_sites WHERE id={$sitepath[id]}";
    $data = $wpdb->get_row($sql, ARRAY_A);
    if (!$sitepath['admin'] && $data['user_id']!=$sitepath['user']) {
        return false;
    }
    $tpl->assign('data', $data);
    
    // Достаем URL в красивом виде
    $urlparts = parse_url(str_replace('\\\\', '/', $_POST['url']));
    $urlparts['host'] = preg_replace('/^www\./', '', $urlparts['host']);
    $url = $urlparts['host'] . ($urlparts['path']=='/' ? '' : $urlparts['path']) . ($urlparts['query'] ? '?'.$urlparts['query'] : '');
    
    $locked = intval($_POST['locked']);
    $status = $sitepath['admin'] ? ", status='{$_POST['status']}'" : '';
	$status_val = $sitepath['admin'] ? $_POST['status'] : '';
   /* $sql = "UPDATE stat_sites SET
            rubric_id={$_POST['rubric_id']},
            name='".mysql_escape_string($_POST['name'])."',
            url='".mysql_escape_string($url)."',
            description='".mysql_escape_string($_POST['description'])."',
            locked=$locked
            $status
            WHERE id={$sitepath['id']}";
    $wpdb->get_row($sql);
	*/
	
	$wpdb->update( 
		'stat_sites', 
		array( 
			'rubric_id' => $_POST['rubric_id'],
			'name' => $_POST['name'],
			'url' => $url,
			'description' => $_POST['description'],
			'locked' => $locked,
			'status' => $status_val,
		), 
		array( 'id' => $sitepath['id'] ), 
		array( 
			'%d',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s',
		), 
		array( '%d' ) 
	);
		
	
    $tpl->clearAllCache();
    
    // Загружаем логотип
    if (is_uploaded_file($_FILES['logo']['tmp_name'])) {
        $filename = "/wp-content/plugins/siterating/images/logos/{$sitepath[id]}.gif";
        img($_FILES['logo']['tmp_name'], $filename, 99, 48);
    }
    
    if ($sitepath['admin'] && $data['status']!=$_POST['status']) { // Отправляем письмо об изменении статуса?
        
        // Найдем мыло пользователя
        $sql = "SELECT * FROM wp_users WHERE ID={$data['user_id']}";
        $row = $wpdb->get_row($sql, ARRAY_A);
        
        // Отправим письмо админу
        include 'htmlMimeMail/htmlMimeMail.php';
        $mail = new htmlMimeMail();
        $mail->setHtmlCharset('utf-8');
		$mail->setHeadCharset('utf-8');
        $mail->setFrom('noreply@'.preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']));
        $subject = "Статус участия вашего сайта {$data['url']} в рейтинге форекс сайтов изменен";
        $mail->setSubject($subject);
        $mail->setHtml($tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME).'/templates/email_update.tpl'));
        @$mail->send(array($row['user_email']));
        
    }
    
    if ($sitepath['vars']['sign']) {
        $url = "/{$sitepath[0]}/";
    }
    else {
        $url = "/{$sitepath[0]}/my/".($sitepath['admin'] ? "id-{$data['user_id']}/" : '');
    }
    
    return $url;
    
}

function siterating_redirect_del() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if ((!$sitepath['id'] || !$sitepath['user']) && $sitepath['vars']['sign']!=md5("It is sign for {$sitepath['id']}")) {
        return false;
    }
    if ($sitepath['vars']['sign']==md5("It is sign for {$sitepath['id']}")) {
        $sitepath['admin'] = true;
    }
    
    $sql = "SELECT * FROM stat_sites WHERE id={$sitepath[id]}";
    $data = $wpdb->get_row($sql, ARRAY_A);
    if (!$sitepath['admin'] && $data['user_id']!=$sitepath['user']) {
        return false;
    }
    
    // Удаляем
    $wpdb->get_row("DELETE FROM stat_sites WHERE id={$sitepath['id']}");
    $wpdb->get_row("DELETE FROM stat_log WHERE site_id={$sitepath['id']}");
    $wpdb->get_row("DELETE FROM stat_days WHERE site_id={$sitepath['id']}");
    $wpdb->get_row("DELETE FROM stat_reviews WHERE site_id={$sitepath['id']}");
    @unlink("{$_SERVER[DOCUMENT_ROOT]}/wp-content/plugins/siterating/images/logos/{$sitepath[id]}.gif");
    
    if ($sitepath['vars']['sign']) {
        $url = "/{$sitepath[0]}/";
    }
    else {
        $url = "/{$sitepath[0]}/my/".($sitepath['admin'] ? "id-{$data['user_id']}/" : '');
    }
    
    return $url;
    
}

function siterating_import() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin']) {
        return false;
    }
    
    // Рубрики
    /*$sql = "SELECT * FROM siterating.catalog";
    if ($data = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($data as $item) {
            $sql = "SELECT * FROM stat_rubrics WHERE id={$item['nom']}";
            $sql = $wpdb->get_row($sql)
                 ? "UPDATE stat_rubrics SET name='".mysql_escape_string($item['kat'])."' WHERE id={$item['nom']}"
                 : "INSERT INTO stat_rubrics (id, name) VALUES ({$item['nom']}, '".mysql_escape_string($item['kat'])."')";
            $wpdb->get_row($sql);
        }
    }
    
    // Сайты
    $sql = "SELECT * FROM siterating.sites";
    if ($data = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($data as $item) {
            
            // URL
            $urlparts = parse_url('http://'.$item['url']);
            $urlparts['host'] = preg_replace('/^www\./', '', $urlparts['host']);
            $url = $urlparts['host'] . ($urlparts['path']=='/' ? '' : $urlparts['path']) . ($urlparts['query'] ? '?'.$urlparts['query'] : '');
            
            // Втыкаем
            $sql = "SELECT * FROM stat_sites WHERE id={$item['id']}";
            $sql = $wpdb->get_row($sql)
                 ? "UPDATE stat_sites SET
                    add_date = '{$item['date']}',
                    user_id = {$item['id_user']},
                    rubric_id = {$item['id_kat']},
                    name = '".mysql_escape_string($item['sitename'])."',
                    url = '".mysql_escape_string($url)."',
                    description = '".mysql_escape_string($item['title']?$item['title']:$item['desc'])."',
                    locked = ".($item['open'] ? 0 : 1).",
                    status = '".($item['status'] ? 'declined' : 'approved')."',
                    tic = {$item['tic']},
                    pr = {$item['pr']}
                    WHERE id={$item['id']}"
                 : "INSERT INTO stat_sites (id, add_date, user_id, rubric_id, name, url, description, locked, status, tic, pr) VALUES (
                    {$item['id']},
                    '{$item['date']}',
                    {$item['id_user']},
                    {$item['id_kat']},
                    '".mysql_escape_string($item['sitename'])."',
                    '".mysql_escape_string($url)."',
                    '".mysql_escape_string($item['title']?$item['title']:$item['desc'])."',
                    ".($item['open'] ? 0 : 1).",
                    '".($item['status'] ? 'declined' : 'approved')."',
                    {$item['tic']},
                    {$item['pr']}
                    )";
            $wpdb->get_row($sql);
            
        }
    }*/
    
    // Дневные значения
    $sql = "SELECT * FROM siterating.all";
    if ($data = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($data as $item) {
            $sql = "SELECT * FROM stat_days WHERE id={$item['id']}";
			
			$sql = $wpdb->get_row($sql);
			
			if( $sql ){
				$wpdb->update( 
					'stat_days', 
					array( 
						'date' => $item['date'],
						'site_id' => $item['id_site'],
						'hosts' => $item['hosts'],
						'hits' => $item['hits'],
						'users' => $item['users'],
					), 
					array( 'id' => $item['id'] ), 
					array( 
						'%s',
						'%d',
						'%d',
						'%d',
						'%d',
					), 
					array( '%d' ) 
				);
			}else{
				$wpdb->insert( 
					'stat_days', 
					array( 
						'id' => $item['id'], 
						'date' => $item['date'],
						'site_id' => $item['id_site'], 
						'hosts' => $item['hosts'], 
						'hits' => $item['hits'], 
						'users' => $item['users'], 
					), 
					array( 
						'%d',
						'%s', 
						'%d', 
						'%d',
						'%d',
						'%d',
					) 
				);
			}
			
            /*$sql = $wpdb->get_row($sql)
                 ? "UPDATE stat_days SET
                    date = '{$item['date']}',
                    site_id = {$item['id_site']},
                    hosts = {$item['hosts']},
                    hits = {$item['hits']},
                    users = {$item['users']}
                    WHERE id={$item['id']}"
                 : "INSERT INTO stat_days (id, date, site_id, hosts, hits, users) VALUES (
                    {$item['id']},
                    '{$item['date']}',
                    {$item['id_site']},
                    {$item['hosts']},
                    {$item['hits']},
                    {$item['users']}
                    )";
            $wpdb->get_row($sql);*/
        }
    }
    
}
