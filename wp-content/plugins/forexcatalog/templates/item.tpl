<div class="item">{$data.position}. {$data.url}</div>
<div class="txt5 grey">{$data.name|escape}</div><br />
{if $data.logo}<a href="http://{$data.url}"><img class="pic" src="{$data.logo}" alt="#" /></a>{/if}
<div class="rt1">
  РЕЙТИНГ<br />
  ОТЗЫВЫ<br />
  КАТЕГОРИЯ<br />
</div>
<div class="rt2">
  <div class="rt"><div class="rt-on" style="width:{$data.rank*20}%"></div></div>
  <span class="green">{intval($data.positive)}</span> | <span class="red">{$data.count-$data.positive}</span><br />
  <a href="/{$sitepath.0}/rubric/id-{$data.rubric_id}/">{$data.rubric_name|escape}</a><br />
</div>
<div class="clear"></div>
{if $data.description}
  <div class="dotted"></div>
  <h2>Описание сайта</h2>
  <div class="txt5 grey">{$data.description}</div>
  <p>&nbsp;</p>
{/if}
{if $data.reviews}
  <div class="title2">ОТЗЫВЫ И ПОЖЕЛАНИЯ</div>
  {foreach $data.reviews as $item}
    <div class="review">
      <div class="review2">
	  <a {if $item.user_login}href="/author/{$item.user_login}.html"{/if}>{$item.name|default:$item.user_login|escape}</a>  
	  |  <span class="{if $item.rank>=3}green{else}red{/if}">{if $item.rank>=3}положительный{else}отрицательный{/if}</span> 
	  |  {$item.timestamp|date_format:'%d/%m/%Y &middot; %H:%M'}</div>
      {$item.text|nl2br}
    </div>
  {/foreach}
  <div class="all"><a href="/{$sitepath.0}/reviews/{$sitepath.parts.id}">Читать все отзывы</a> {$data.count}</div>
{/if}
<a name="form"></a>
<div class="title3">Добавить ОТЗЫВ</div><br />
<form action="/{$sitepath.0}/submit/{$sitepath.parts.id}" method="POST" class="validate">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
    {if !$sitepath.user}
    <tr>
      <td>Имя</td>
      <td><input class="inp2 required" name="name" type="text" /></td>
    </tr>
    <tr>
      <td>E-mail</td>
      <td><input class="inp2 required email" name="email" type="text" /></td>
    </tr>
    {/if}
    <tr>
      <td width="23%">Ваша оценка</td>
      <td width="77%"><select class="sel forselect" name="rank">
        <option value="" selected>- Не выбрано</option>
        <option value="1">- ужасно</option>
        <option value="2">- плохо</option>
        <option value="3">- обычно</option>
        <option value="4">- хорошо</option>
        <option value="5">- отлично</option>
      </select></td>
    </tr>
    <tr>
      <td>Комментарий</td>
      <td><textarea class="area required" name="text" cols="" rows=""></textarea></td>
    </tr>
    <tr>
      <td></td>
      <td><label><input name="subscribe" type="checkbox" id="checkbox" checked="checked" /></label> Сообщать о дальнейших обсуждениях</td>
    </tr>
  </table>
  <div class="dotted"></div>
  <input class="sub3" type="submit" value="Добавить" />
</form>