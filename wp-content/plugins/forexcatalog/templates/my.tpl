<div class="title">Мои сайты</div>
{include file=$smarty.server.DOCUMENT_ROOT|cat:'/wp-content/plugins/forexcatalog/templates/header.tpl'}
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="forum3">
  <tr>
    <th width="4%">&nbsp;</th>
    <th width="10%">Статус</th>
    <th width="8%">&nbsp;</th>
    <th width="29%" class="left">Сайт</th>
    <th width="9%">Код</th>
  </tr>
  {foreach $data as $item}
    <tr>
      <td class="num2">{$item@iteration}</td>
      <td class="{if $item.status==off}red{else}num{/if}">{if $item.status==new}На проверке{else}{if $item.status==approved}Одобрен{else}{if $item.status==off}*Отключен{else}Отклонен{/if}{/if}{/if}</td>
      <td class="num"><a href="/{$sitepath.0}/edit/id-{$item.id}/"><img src="/wp-content/plugins/forexcatalog/images/edit.png" alt="#" /></a></td>
      <td class="left f14"><a href="/{$sitepath.0}/item/id-{$item.id}/">{$item.name|escape}</a></td>
      <td><a href="/{$sitepath.0}/counter/id-{$item.id}/">Получить код</a></td>
    </tr>
  {/foreach}
  
  {if $all}
  
  <tr height="10" bgcolor="#ddd">
	<td colspan="5" align="center"><a href="/{$sitepath.0}/my/page-{$prev}/"><<</a>&nbsp;&nbsp;Остальные сайты&nbsp;&nbsp;<a href="/{$sitepath.0}/my/page-{$next}/">>></a></td>
  </tr>
  
  {foreach $all as $item}
    <tr>
      <td class="num2">{$item@iteration}</td>
      <td class="{if $item.status==off}red{else}num{/if}">{if $item.status==new}На проверке{else}{if $item.status==approved}Одобрен{else}{if $item.status==off}*Отключен{else}Отклонен{/if}{/if}{/if}</td>
      <td class="num"><a href="/{$sitepath.0}/edit/id-{$item.id}/"><img src="/wp-content/plugins/forexcatalog/images/edit.png" alt="#" /></a></td>
      <td class="left f14"><a href="/{$sitepath.0}/item/id-{$item.id}/">{$item.name|escape}</a></td>
      <td><a href="/{$sitepath.0}/counter/id-{$item.id}/">Получить код</a></td>
    </tr>
  {/foreach}
  
  {/if}
  
</table>
<br /><br />
<p>* В случае если кнопка не будет установлена на сайте, сайт не будет показываться в каталоге.</p>
<div class="clear"></div><br />
<h4>Дополнительная информация</h4>
<div class="katalog">
  <div class="kat-name">Принимайте участие в других сервисах</div>
  <p>Мы рекомендуем вам так же поучаствовать в следующих наших сервисах для ваших сайтов: <a href="#">рейтинг партнеров</a>, <a href="#">каталог сайтов по форекс</a> , <a href="#">новостной информер для сайта</a></p>
  <div class="kat-name">У Вас есть свой аккаунт на форуме?</div>
  Сделайте себе красивую подпись, воспользуйтесь <a href="#">биржевой линейкой</a>
  <p>&nbsp;</p>
</div>