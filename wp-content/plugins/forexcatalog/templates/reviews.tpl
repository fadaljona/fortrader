<div class="item">Отзывы о {$data.url}</div>
<div class="txt5 grey">{$data.name|escape}</div>
{if $data.logo}<a href="http://{$data.url}"><img class="pic" src="{$data.logo}" alt="#" /></a>{/if}
<div class="rt1">
  РЕЙТИНГ<br />
  ОТЗЫВЫ<br />
  КАТЕГОРИЯ<br />
</div>
<div class="rt2">
  <div class="rt"><div class="rt-on" style="width:{$data.rank*20}%"></div></div>
  <span class="green">{intval($data.positive)}</span> | <span class="red">{$data.count-$data.positive}</span><br />
  <a href="/{$sitepath.0}/rubric/id-{$data.rubric_id}/">{$data.rubric_name|escape}</a><br />
</div>
<div class="clear"></div>
<div class="dotted"></div>
<div class="all"><a href="/{$sitepath.0}/item/{$sitepath.parts.id}">Подробнее о сайте</a></div>
<div class="title2">ОТЗЫВЫ</div>
{foreach $page.data as $item}
  <div class="review">
    <div class="review2"><a {if $item.user_login}href="/author/{$item.user_login}.html"{/if} name="item-{$item.id}">{$item.name|default:$item.user_login|escape}</a>  |  <span class="{if $item.rank>=3}green{else}red{/if}">{if $item.rank>=3}положительный{else}отрицательный{/if}</span> |  {$item.timestamp|date_format:'%d/%m/%Y &middot; %H:%M'}</div>
    {if $sitepath.admin}
      <form action="/{$sitepath.0}/deny/id-{$item.id}/{$sitepath.parts.page}" method="POST">
        <textarea class="area required" name="text" cols="" rows="">{$item.text}</textarea><br /><br />
        <b>Скрыть:</b> <input type="text" name="deny" value="{$item.deny|escape}" style="width:350px;" /><br /><br />
        <input type="submit" value="Отправить" /> <a href="/{$sitepath[0]}/delete/id-{$item.id}/" onclick="return confirm('Вы уверены?');">Удалить</a>
      </form>
    {else}
      {if $item.deny}Скрыто по причине: {$item.deny|escape}{else}{$item.text|nl2br}{/if}
    {/if}
  </div>
{/foreach}
<div class="all"><a href="/{$sitepath.0}/item/{$sitepath.parts.id}#form">Добавить свой отзыв</a></div>
{if $page.links}
  <div class="pages">
    {if $page.links.prev}<a class="prev" href="{$page.links.prev}">Пред.</a>{/if}
    <div class="pages2">
      {foreach $page.links.list as $item}
        <a href="{$item}"{if $item@key==$page.pages.current} class="on"{/if}>{$item@key}</a>{if !$item@last} &bull;{/if}
      {/foreach}
    </div>
    {if $page.links.next}<a class="next" href="{$page.links.next}">След.</a>{/if}
  </div>
{/if}