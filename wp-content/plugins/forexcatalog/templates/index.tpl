{if $sitepath.1=='rubric'}<div class="title">Сайты из рубрики &laquo;{$data.name|escape}&raquo;</div>{/if}
{include file=$smarty.server.DOCUMENT_ROOT|cat:'/wp-content/plugins/forexcatalog/templates/header.tpl'}
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="forum3">
  <tr>
    <th width="7%">&nbsp;</th>
    <th width="59%" class="left">Cайт</th>
    <th width="13%" align="right">Рейтинг</th>
    <th width="9%">Отзывов</th>
  </tr>
  {foreach $page.data as $item}
    <tr>
      <td class="num">{($sitepath.page-1)*$smarty.const.FOREXCATALOG_PER_PAGE + $item@iteration}</td>
      <td class="left"><a href="/{$sitepath.0}/item/id-{$item.id}/">{$item.url}</a><br />{$item.name|escape}</td>
      <td><div class="rt"><div class="rt-on" style="width:{round($item.rank*20)}%"></div></div></td>
      <td><span class="rt2"><span class="green">{intval($item.positive)}</span> | <span class="red">{$item.count-$item.positive}</span></span></td>
    </tr>
  {/foreach}
</table>
{if $page.links}
  <div class="pages">
    {if $page.links.prev}<a class="prev" href="{$page.links.prev}">Пред.</a>{/if}
    <div class="pages2">
      {foreach $page.links.list as $item}
        <a href="{$item}"{if $item@key==$page.pages.current} class="on"{/if}>{$item@key}</a>{if !$item@last} &bull;{/if}
      {/foreach}
    </div>
    {if $page.links.next}<a class="next" href="{$page.links.next}">След.</a>{/if}
  </div>
  <div class="clear"></div>
{/if}
<div class="clear"></div><br />
<h4><br />РУБРИКИ САЙТОВ</h4>
<div class="katalog">
  {*<div class="kat-name">Аналитика, Рекомендации</div>*}
  <br />
  {foreach $rubrics as $item}
    <a href="/{$sitepath.0}/rubric/id-{$item.id}/">{$item.name|escape}</a>&nbsp;<sup>{$item.count}</sup>
    {if !$item@last},{/if}
  {/foreach}
</div>
<br />
<h4>ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ</h4>
<br /><br />
<div class="kat">
  <strong>Зачем участвовать в форекс каталоге сайтов?</strong><br /><br />
  Добавляя сайт в каталог и размещая кнопку рейтинга на вашем сайте, вы можете видеть, как пользователи относятся к вашему ресурсу. Они оставляют критику и пожелания - вы улучшаете ваш сервис.<br /><br />
  <div class="all2"></div>
</div>
