<?php
/**
 * ���������� �������
 *
 * @link http://nashe-vse.ru/
 * @copyright ��������� �����
 * @author SMaster
 */

function pager($query, $count, $page, $link = '', $slow_mode = false) {
    
    global $wpdb;
    
    $page = (int)$page>0 ? (int)$page : 1;
    $count = (int)$count>0 ? (int)$count : 20;
    
    $url = parse_url($link);
    if ($url['query']) {
        $link = $url['path'];
        $get = "?{$url['query']}";
    }
    
    if ($slow_mode) {
        $err = true;
    }
    else {
        preg_match('/select (.*) from/is', $query, $matches);
        $count_query = str_replace($matches[1], 'COUNT(*) AS count', $query);
        if ($row = $wpdb->get_row($count_query, ARRAY_A)) {
            $results_count = $row['count'];
            $page = min($page, ceil($results_count/$count));
            if( ($page-1) < 0 ){
				$page_lim = 0;
			}else{
				$page_lim = $page-1;
			}
            $limit_query = $query . ' LIMIT ' . $count*$page_lim . ', ' . $count;
            if (!($out['data']=$wpdb->get_results($limit_query, ARRAY_A))) {
                $err = true;
            }
        }
        else {
            $err = true;
        }
    }
    
    if ($err) { // �����-�� ������ � ��������: �� ������� ������ (UNION ��� �������� �����) ��� ��� ����� � ���� LIMIT
        $data = $wpdb->get_results($query, ARRAY_A);
        if (!$data) {
            return;
        }
        $results_count = count($data);
        $page = min($page, ceil($results_count/$count));
        $i = 0;
        foreach ($data as $row) {
            if ($i>=$count*($page-1)) {
                $out['data'][] = $row;
            }
            if ($i>=$count*$page-1) {
                break;
            }
            $i++;
        }
    }
    
    if ($page < ceil($results_count/$count)) {
        $out['pages']['next'] =  $page+1;
    }
    if ($page>1) {
        $out['pages']['prev'] = $page-1;
    }
    $out['pages']['current'] = $page;
    $out['pages']['count'] = ceil($results_count/$count);
    
    if ($link && $out['pages']['count']>1) {
        if ($out['pages']['next']) {
            $out['links']['next'] = "{$link}page-{$out['pages']['next']}/$get";
        }
        if ($out['pages']['prev']) {
            $out['links']['prev'] = $out['pages']['prev']==1 ? $link.$get : "{$link}page-{$out['pages']['prev']}/$get";
        }
        $out['links']['current'] = "{$link}page-{$out['pages']['current']}/$get";
        for ($i = max(1, $out['pages']['current']-11); $i <= min($out['pages']['count'], $out['pages']['current']+11); $i++) {
            $out['links']['list'][$i] = $i==1 ? $link.$get : "{$link}page-$i/$get";
        }
    }
    
    return $out;
    
}

/**
 * �������� ����������� �����������
 *
 * @return array
 * @param string  $src    ���� ������
 * @param string  $dest   ���� ����
 * @param integer $width  ������
 * @param integer $height ������
 * @param boolean $strict ������ ��������� � �������. ���� ���������� � false, �� ��� ������� ������� �������� ���������� ��������
 */
function img($src, &$dest, $width = 0, $height = 0, $strict = true) {
    
    $img['info'] = @getimagesize($src);
    switch ($img['info'][2]) {
        case 1:
            $img['handler'] = imagecreatefromgif($src);
            $img['ext'] = 'gif';
            break;

        case 2:
            $img['handler'] = imagecreatefromjpeg($src);
            $img['ext'] = 'jpg';
            break;

        case 3:
            $img['handler'] = imagecreatefrompng($src);
            $img['ext'] = 'png';
            break;

        default:
            $img = null;
            break;
    }
    
    if ($img) {
        $dest = str_replace('#', $img['ext'], $dest);
        @unlink("{$_SERVER['DOCUMENT_ROOT']}$dest");
        
        if ((!$width && !$height) || (!$strict && $img['info'][0]<=$width && $img['info'][1]<=$height)) {
            copy($src, "{$_SERVER['DOCUMENT_ROOT']}$dest");
            return $img['info'][3];
        }
        
        /*$width_ratio = $width/$img['info'][0];
        $height_ratio = $height/$img['info'][1];
        if( $width_ratio > $height_ratio) {
            $width = round($height_ratio*$img['info'][0]);
        }
        else {
            $height = round($width_ratio*$img['info'][1]);
        }*/
        $dest_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($dest_image, $img['handler'], 0, 0, 0, 0, $width, $height, $img['info'][0], $img['info'][1]);
        switch ($img['info'][2]) {
            case 1:
                $result = imagegif($dest_image, "{$_SERVER['DOCUMENT_ROOT']}$dest");
                break;
            case 2:
                $result = imagejpeg($dest_image, "{$_SERVER['DOCUMENT_ROOT']}$dest", 75);
                break;
            case 3:
                $result = imagepng($dest_image, "{$_SERVER['DOCUMENT_ROOT']}$dest");
                break;
        }
        imagedestroy($dest_image);
        
        $img['info'] = getimagesize("{$_SERVER['DOCUMENT_ROOT']}$dest");
        return $img['info'][3];
    }
    
    return null;
    
}

/**
 * ������ URL
 *
 * @return array
 * @param int $skip ���������� ����������� ��������� � ������ ����
 * @param string $url URL ��� �������
 */
function urlparse($skip = 0, $url = null) {
    
    $path_parts = pathinfo(preg_replace('/\?.*/', '', is_null($url) ? $_SERVER['REQUEST_URI'] : $url));
    
    if ($path_parts['basename']) {
        $path_parts['filename'] = $path_parts['extension']
                                ? substr($path_parts['basename'], 0, -strlen($path_parts['extension'])-1)
                                : $path_parts['basename'];
    }
    
    if (strlen($path_parts['dirname']) > 1) {
        $_sitepath = explode('/', substr($path_parts['dirname'], 1));
    }
    if ($path_parts['filename']) {
        $_sitepath[] = $path_parts['filename'];
    }
    
    if ($skip) {
        $count = count($_sitepath);
        for ($i=0; $i<min($skip, $count); $i++) {
            array_shift($_sitepath);
        }
    }
    if (count($_sitepath)) {
        foreach ($_sitepath as $part) {
            if (preg_match("/^([a-z]+)-(.+)$/", $part, $matches)) {
                // ���� �������� ����� � �������, �� � ���� ������ ��������� ������ �� �����
                $stop = true;
                $sitepath['vars'][$matches[1]] = $matches[2];
                $sitepath['parts'][$matches[1]] = $part.'/';
                if (preg_match("/^([a-z]+)-([0-9]+)$/", $part, $matches)) {
                    $sitepath[$matches[1]] = $matches[2];
                }
            }
            elseif(!$stop) {
                $sitepath[] = $part;
            }
        }
    }
    
    return $sitepath;

}

/** 
 * ��������
 */
function redirect($url) {
    
    header ("Location: $url");
    die();
    
}

function my64_encode($str) {
    
    $str = base64_encode($str);
    if (preg_match('/==$/', $str)) {
        return 2 . substr($str, 0, strlen($str)-2);
    }
    if (preg_match('/=$/', $str)) {
        return 1 . substr($str, 0, strlen($str)-1);
    }
    return 0 . $str;
    
}

function my64_decode($str) {
    
    $n = (int)$str{0};
    $str = substr($str, 1);
    for ($i=0; $i<$n; $i++) {
        $str .= '=';
    }
    
    return base64_decode($str);
    
}

function mysql_escape_string_dummy($str) {
    
    return $str;
    
}

function pre_iconv($content) {
    return iconv('windows-1251', 'utf-8', $content);
}
