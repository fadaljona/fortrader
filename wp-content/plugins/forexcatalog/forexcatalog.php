<?php
/*
  Plugin Name: forexcatalog
  Plugin URI: http://mrdealer.ru/
  Description:Percent rates
  Author: SMaster
  Version: 0.1
  Author URI: http://nashe-vse.ru/
*/

//************* Init ******************

if (SMASTER!==true) {
    
    define ('SMASTER', true);
    
    include 'functions.inc.php';
    include 'smarty/Smarty.class.php';
    
    $sitepath = urlparse();
    
    $dir = pathinfo(__FILE__, PATHINFO_DIRNAME);
    $tpl = new Smarty();
    $tpl->caching      = Smarty::CACHING_LIFETIME_CURRENT;
    $tpl->cache_dir    = "$dir/cache/";
    $tpl->compile_dir  = "$dir/templates_c/";
    //$tpl->registerFilter('pre', 'pre_iconv');
    
}

define('FOREXCATALOG_PER_PAGE', 20);

//************* Filters ***************

add_filter('init', 'forexcatalog_init');

function forexcatalog_init() {
    
    global $sitepath, $tpl, $wpdb, $company_id, $company_id_url;
    
    if ($sitepath[0]=='forexcatalog') {
        
        add_filter('the_content', 'forexcatalog_the_content');
        add_filter('wp_title', 'forexcatalog_wp_title');
        
        $current_user = wp_get_current_user();
        $sitepath['user'] = $current_user->ID;
        if (is_super_admin($current_user->ID)) {
            $sitepath['admin'] = true;
        }
        
        if ($sitepath[1] && function_exists("forexcatalog_redirect_{$sitepath[1]}")) {
            $tpl->caching = Smarty::CACHING_OFF;
            if ($url=call_user_func("forexcatalog_redirect_{$sitepath[1]}")) {
                redirect($url);
            }
        }
        elseif (!$sitepath[1] || function_exists("forexcatalog_{$sitepath[1]}")) {
            $_SERVER['REQUEST_URI'] = $sitepath['admin'] ? $_SERVER['REQUEST_URI'].'admin' :  $_SERVER['REQUEST_URI'];
            if ($sitepath[1]) {
                if ($tpl->isCached(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/{$sitepath[1]}.tpl", $_SERVER['REQUEST_URI'])) {
                    $ok = true;
                }
                else {
                    forexcatalog_header();
                    $ok = call_user_func("forexcatalog_{$sitepath[1]}");
                }
                $_GET['hide_title'] = true;
            }
            else {
                if ($tpl->isCached(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/index.tpl", $_SERVER['REQUEST_URI'])) {
                    $ok = true;
                }
                else {
                    forexcatalog_header();
                    $ok = forexcatalog();
                }
            }
            if ($ok) {
                $_SERVER['REQUEST_URI_SAVE'] = $_SERVER['REQUEST_URI'];
                $_SERVER['REQUEST_URI'] = "/{$sitepath[0]}.html";
            }
        }
    }
    
}

function forexcatalog_the_content($content) {
    
   /* global $sitepath, $tpl, $wpdb;
    
    $tpl->assign_by_ref('sitepath', $sitepath);
    $tpl->assign('content', $content);
    
    $template = $sitepath[1] ? "{$sitepath[1]}.tpl" : 'index.tpl';
    $content = $tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME)."/templates/$template", $_SERVER['REQUEST_URI_SAVE']);
    */
    return $content;
    
}

function forexcatalog_wp_title($title) {
    
    global $sitepath, $tpl, $wpdb;
    
    if (in_array($sitepath[1], array('item', 'reviews')) && $sitepath['id'] && $t = $wpdb->get_var($wpdb->prepare("SELECT url FROM stat_sites WHERE id={$sitepath[id]}", 0))) {
        $title = ($sitepath[1]=='reviews' ? iconv('windows-1251', 'utf-8', '������ � ') : '') . "$t &mdash; ";
    }
    if ($sitepath[1]=='rubric' && $sitepath['id']) {
        $sql = "SELECT * FROM stat_rubrics WHERE id={$sitepath[id]}";
        $row = $wpdb->get_row($sql, ARRAY_A);
        $title = iconv('windows-1251', 'utf-8', '����� �� ������� ')."&laquo;{$row[name]}&raquo; &mdash; ";
    }
    
    return $title;
    
}

function forexcatalog_header() {
    
    global $sitepath, $tpl, $wpdb;
    
    // ���������
    $sql = "SELECT * FROM stat_settings";
    if ($rows=$wpdb->get_results($sql, ARRAY_A)) {
        foreach ($rows as $item) {
            $settings[$item['var']] = $item['value'];
        }
    }
    $tpl->assign('settings', $settings);
    
    // ������� ��������
    $sql = "SELECT * FROM stat_sites WHERE status='approved' ORDER BY add_date DESC LIMIT 3";
    $novices = $wpdb->get_results($sql, ARRAY_A);
    $tpl->assign('novices', $novices);
    
}

//************* Content ***************

function forexcatalog() {
    
    global $sitepath, $tpl, $wpdb;
    
    forexcatalog_header();
    
    // ������� �������?
    if ($sitepath['id']) {
        $rubric = "stat_sites.rubric_id={$sitepath[id]}";
        $sql = "SELECT * FROM stat_rubrics WHERE id={$sitepath[id]}";
        if (!($data = $wpdb->get_row($sql, ARRAY_A))) {
            return false;
        }
        $tpl->assign('data', $data);
    }
    else {
        $rubric = 1;
    }
    
    // ������ ��������
    $sql = "SELECT stat_sites.*,
            IF (stat_sites.cat_date < NOW() - INTERVAL 1 DAY, 'off', stat_sites.status) AS status,
            AVG(stat_reviews.rank) AS rank
            FROM stat_sites LEFT JOIN stat_reviews ON stat_sites.id=stat_reviews.site_id
            WHERE stat_sites.cat_date >= NOW() - INTERVAL 1 DAY AND $rubric
            GROUP BY stat_sites.id
            ORDER BY rank DESC, url";
    $page = pager($sql, FOREXCATALOG_PER_PAGE, $sitepath['page'], "/{$sitepath[0]}/".($sitepath[1] ? "{$sitepath[1]}/" : '').$sitepath['parts']['id'], true);
    if ($page['data']) {
        foreach ($page['data'] as $key=>$item) {
            $page['data'][$key]['count'] = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM stat_reviews WHERE site_id={$item[id]} AND !LENGTH(deny)",1));
            $page['data'][$key]['positive'] = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM stat_reviews WHERE site_id={$item[id]} AND !LENGTH(deny) AND rank>=3",1));
            $page['data'][$key]['review'] = $wpdb->get_row("SELECT * FROM stat_reviews WHERE site_id={$item[id]} AND !LENGTH(deny) ORDER BY timestamp DESC LIMIT 1", ARRAY_A);
        }
    }
    $tpl->assign('page', $page);
    $sitepath['page'] = $sitepath['page'] ? $sitepath['page'] : 1;
    
    // �������
    $sql = "SELECT stat_rubrics.*, COUNT(stat_sites.id) AS count
            FROM stat_rubrics LEFT JOIN stat_sites ON stat_rubrics.id=stat_sites.rubric_id
            GROUP BY stat_sites.rubric_id
            ORDER BY stat_rubrics.name";
    $rubrics = $wpdb->get_results($sql, ARRAY_A);
    $tpl->assign('rubrics', $rubrics);
    
    return true;
    
}

function forexcatalog_rubric() {
    
    global $sitepath, $tpl, $wpdb;
    
    return forexcatalog();
    
}

function forexcatalog_img() {
    
    global $sitepath, $tpl, $wpdb;
    
    $_GET['id'] = intval($_GET['id']);
    if ($_GET['id']) {
        $wpdb->get_row("UPDATE stat_sites SET cat_date=NOW() WHERE id={$_GET['id']}");
    }
    
    header('Content-type: image/png');     
    die(file_get_contents("{$_SERVER[DOCUMENT_ROOT]}/wp-content/plugins/forexcatalog/images/img.png"));
    
}

function forexcatalog_my() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['admin'] && !$sitepath['user']) {
        redirect('/wp-login.php');
    }
    if (!$sitepath['admin'] || !$sitepath['id']) {
        $sitepath['id'] = $sitepath['user'];
    }
    
    $sql = "SELECT *, IF (cat_date < NOW() - INTERVAL 1 DAY, 'off', stat_sites.status) AS status FROM stat_sites WHERE user_id={$sitepath['id']} ORDER BY name";
    $data = $wpdb->get_results($sql, ARRAY_A);
    $tpl->assign('data', $data);

    if (is_super_admin())
    {
        $page_size = 15;

        if (isset($sitepath['page']))
        {
            $page = $sitepath['page'];
            $offset = $page_size * ($page - 1);
            $limit = "LIMIT $offset, $page_size";
        }
        else
            $limit = "LIMIT 0, 10";

        $sql = "SELECT *, IF (cat_date < NOW() - INTERVAL 1 DAY, 'off', stat_sites.status) AS status
            FROM stat_sites WHERE user_id <> {$sitepath['id']} ORDER BY add_date DESC $limit";

        $prev = $sitepath['page'] - 1;
        if ($prev <= 0)
            $prev = 1;

        $tpl->assign('next', $sitepath['page'] + 1);
        $tpl->assign('prev', $prev);
        $tpl->assign('all', $wpdb->get_results($sql, ARRAY_A));
    }
    
    return true;
    
}

function forexcatalog_counter() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['id']) {
        return false;
    }
    if (!$sitepath['admin'] && !$sitepath['user']) {
        redirect('/wp-login.php');
    }
    
    $sql = "SELECT * FROM stat_sites WHERE id={$sitepath['id']} ORDER BY name";
    $data = $wpdb->get_row($sql, ARRAY_A);
    $tpl->assign('data', $data);
    
    $_SERVER['HTTP_HOST'] = preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']);
    
    return true;
    
}

function forexcatalog_item() {
    
    global $sitepath, $tpl, $wpdb;
    
    $sql = "SELECT stat_sites.*, IF(stat_reviews.rank, AVG(stat_reviews.rank), 0) AS rank,
            stat_rubrics.name AS rubric_name
            FROM stat_sites LEFT JOIN stat_reviews ON stat_sites.id=stat_reviews.site_id
            LEFT JOIN stat_rubrics ON stat_sites.rubric_id=stat_rubrics.id
            WHERE stat_sites.id={$sitepath[id]}
            GROUP BY stat_reviews.site_id";
    if (!($data = $wpdb->get_row($sql, ARRAY_A))) {
        return false;
    }
    $deny = $sitepath['admin'] ? 1 : "!LENGTH(deny)";
    $data['count']    = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM stat_reviews WHERE site_id={$sitepath[id]} AND $deny", 0));
    $data['positive'] = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM stat_reviews WHERE site_id={$sitepath[id]} AND $deny AND rank>=3", 0));
    $data['reviews']  = $wpdb->get_results("SELECT stat_reviews.*, wp_users.user_login, wp_users.user_email FROM stat_reviews LEFT JOIN wp_users ON stat_reviews.user_id=wp_users.ID WHERE stat_reviews.site_id={$sitepath[id]} AND !LENGTH(stat_reviews.deny) ORDER BY stat_reviews.timestamp DESC LIMIT 2", ARRAY_A);
    
    $data['position'] = 1;
    $sql = "SELECT stat_sites.*, IF(stat_reviews.rank, AVG(stat_reviews.rank), 0) AS rank
            FROM stat_sites LEFT JOIN stat_reviews ON stat_sites.id=stat_reviews.site_id
            GROUP BY stat_sites.id
            HAVING rank>={$data[rank]}
            ORDER BY rank DESC, url";
    if ($r = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($r as $item) {
            if ($item['id']==$sitepath['id']) {
                break;
            }
            $data['position']++;
        }
    }
    
    // �������
    $filename = "/wp-content/plugins/siterating/images/logos/{$data[id]}.gif";
    if (file_exists($_SERVER['DOCUMENT_ROOT'].$filename)) {
        $data['logo'] = $filename;
    }
    
    $tpl->assign('data', $data);
    
    return true;
    
}

function forexcatalog_reviews() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!forexcatalog_item()) {
        return false;
    }
    
    $deny = $sitepath['admin'] ? 1 : "!LENGTH(stat_reviews.deny)";
    
    $sql = "SELECT stat_reviews.*, wp_users.user_login, wp_users.user_email
            FROM stat_reviews LEFT JOIN wp_users ON stat_reviews.user_id=wp_users.ID
            WHERE stat_reviews.site_id={$sitepath[id]} AND $deny
            ORDER BY stat_reviews.timestamp DESC";
    $page = pager($sql, SRANKS_PER_PAGE, $sitepath['page'], "/{$sitepath[0]}/reviews/{$sitepath[parts][id]}");
    $tpl->assign('page', $page);
    
    return true;
    
}

function forexcatalog_contains_url($text) {
    $hasHttp = strpos($text, 'http') !== false;
    $hasHref = strpos($text, '<a href') !== false;

    return $hasHttp || $hasHref;
}

function forexcatalog_redirect_submit() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['id'] || !$_POST) {
        return false;
    }
    $data = $wpdb->get_row("SELECT * FROM stat_sites WHERE id={$sitepath[id]}", ARRAY_A);
    if (!$data) {
        return false;
    }

    if (forexcatalog_contains_url($_POST['text']))
        return false;
    
    $user_id = intval($sitepath['user']);
    $subscribe = $_POST['subscribe'] ? 1 : 0;
    $rank = (int)$_POST['rank'];

	$wpdb->insert( 
		'stat_reviews', 
		array( 
			'site_id' => $sitepath['id'], 
			'user_id' => $user_id,
			'name' => mysql_escape_string_dummy($_POST['name']),
			'email' => mysql_escape_string_dummy($_POST['email']),
			'subscribe' => $subscribe,
			'rank' => $rank,
			'text' => mysql_escape_string_dummy($_POST['text']),
			'deny' => '',
		), 
		array( 
			'%d', 
			'%d',
			'%s',
			'%s',
			'%d',
			'%d',
			'%s',
			'%s',
		) 
	);
	$id = $wpdb->insert_id;
	
	
	
    $tpl->clearAllCache();
    
    $sql = "SELECT stat_reviews.*, wp_users.user_login, wp_users.user_email
            FROM stat_reviews LEFT JOIN wp_users ON stat_reviews.user_id=wp_users.ID
            WHERE stat_reviews.site_id={$sitepath[id]} AND stat_reviews.id!=$id
            AND stat_reviews.subscribe";
    if ($data = $wpdb->get_results($sql, ARRAY_A)) {
        
        $host = preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']);
        $tpl->assign('data', $data);
        $tpl->assign('link', "http://$host/{$sitepath[0]}/reviews/{$sitepath[parts][id]}#item-$id");
        
        include 'htmlMimeMail/htmlMimeMail.php';
        $mail = new htmlMimeMail();
        $mail->setHtmlCharset('utf-8');
        $mail->setFrom("noreply@$host");
        $subject = '=?koi8-r?B?'.base64_encode(convert_cyr_string("��������� � ����� ������",'w','k')).'?=';
        $mail->setSubject($subject);
        
        foreach ($data as $item) {
            $email = $item['user_email'] ? $item['user_email'] : $item['email'];
            $tpl->assign('ulink', "http://$host/{$sitepath[0]}/unsubscribe/id-{$item[id]}/sign-".md5("unsubscribe: id{$item[id]}").'/');
            $mail->is_built = false;
            $mail->setHtml($tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME).'/templates/email_submit.tpl'));
            @$mail->send(array($email));
        }
        
    }
    
    return "/{$sitepath[0]}/reviews/{$sitepath[parts][id]}#item-$id";
    
}

function forexcatalog_unsubscribe() {
    
    global $sitepath, $tpl, $wpdb;
    
    if ($sitepath['vars']['sign']==md5("unsubscribe: id{$sitepath[id]}")) {
        $sql = "UPDATE stat_reviews SET subscribe=0 WHERE id={$sitepath[id]}";
        $wpdb->get_row($sql);
    }
    
    return true;
    
}

function forexcatalog_redirect_deny() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin'] || !$sitepath['id'] || !$_POST) {
        return false;
    }
    $data = $wpdb->get_row("SELECT * FROM stat_reviews WHERE id={$sitepath[id]}", ARRAY_A);
    if (!$data) {
        return false;
    }
    
    $sql = "UPDATE stat_reviews SET text='".mysql_escape_string_dummy($_POST['text'])."', deny='".mysql_escape_string_dummy($_POST['deny'])."' WHERE id={$sitepath['id']}";
    $wpdb->get_row($sql);
    $tpl->clearAllCache();
    
    return "/{$sitepath[0]}/reviews/id-{$data[site_id]}/{$sitepath[parts][page]}#item-{$sitepath[id]}";
    
}

function forexcatalog_redirect_delete() {
    
    global $sitepath, $tpl, $wpdb;
    
    if (!$sitepath['admin'] || !$sitepath['id']) {
        return false;
    }
    $data = $wpdb->get_row("SELECT * FROM stat_reviews WHERE id={$sitepath[id]}", ARRAY_A);
    if (!$data) {
        return false;
    }
    
    $sql = "DELETE FROM stat_reviews WHERE id={$sitepath['id']}";
    $wpdb->get_row($sql);
    $tpl->clearAllCache();
    
    return "/{$sitepath[0]}/reviews/id-{$data[site_id]}/";
    
}

function forexcatalog_add() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['user']) {
        redirect('/wp-login.php');
    }
    
    // ������� �������
    $rubrics[''] = '-- Выберите рубрику --'; //iconv('windows-1251', 'utf-8', '-- Выберите рубрику --');
    $sql = "SELECT * FROM stat_rubrics ORDER BY name";
    if ($rows = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($rows as $row) {
            $rubrics[$row['id']] = $row['name'];
        }
    }
    $tpl->assign('rubrics', $rubrics);

    $locked = array('Открыт', 'Закрыт');
    //$locked = array(iconv('windows-1251', 'utf-8', 'Открыт'), iconv('windows-1251', 'utf-8', 'Закрыт'));
    $tpl->assign('locked', $locked);
    
    $tpl->assign('data', array('locked'=>0));
    
    return true;
    
}

function forexcatalog_edit() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['id']) {
        return false;
    }
    if ($sitepath['vars']['sign']==md5("It is sign for {$sitepath['id']}")) {
        $sitepath['admin'] = true;
    }
    
    // ������� ������
    $sql = "SELECT * FROM stat_sites WHERE id={$sitepath[id]}";
    $data = $wpdb->get_row($sql, ARRAY_A);
    if (!$sitepath['admin'] && $data['user_id']!=$sitepath['user']) {
        redirect('/wp-login.php');
    }
    
    // �������
    $filename = "/wp-content/plugins/siterating/images/logos/{$data[id]}.gif";
    if (file_exists($_SERVER['DOCUMENT_ROOT'].$filename)) {
        $data['logo'] = $filename;
    }
    $tpl->assign('data', $data);
    
    // ������� �������
    $sql = "SELECT * FROM stat_rubrics ORDER BY name";
    if ($rows = $wpdb->get_results($sql, ARRAY_A)) {
        foreach ($rows as $row) {
            $rubrics[$row['id']] = $row['name'];
        }
    }
    $tpl->assign('rubrics', $rubrics);
    
    //$locked = array(iconv('windows-1251', 'utf-8', '���������'), iconv('windows-1251', 'utf-8', '��������'));
    $locked = array('Открыт', 'Закрыт');
    $tpl->assign('locked', $locked);
    
    $status = array(
                  'new'=>'Новый',
                  'approved'=>'Подтвержден',
                  'declined'=>'Отклонен'
              );
    $tpl->assign('status', $status);
    
    return true;
    
}

function forexcatalog_redirect_insert() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if (!$sitepath['user']) {
        return false;
    }
    
    // ������� URL � �������� ����
    $urlparts = parse_url(str_replace('\\\\', '/', $_POST['url']));
    $urlparts['host'] = preg_replace('/^www\./', '', $urlparts['host']);
    $url = $urlparts['host'] . ($urlparts['path']=='/' ? '' : $urlparts['path']) . ($urlparts['query'] ? '?'.$urlparts['query'] : '');
    
    $locked = intval($_POST['locked']);

	$wpdb->insert( 
		'stat_sites', 
		array( 
			'user_id' => $sitepath['user'], 
			'rubric_id' => $_POST['rubric_id'],
			'name' => mysql_escape_string($_POST['name']),
			'url' => mysql_escape_string($url),
			'description' => mysql_escape_string($_POST['description']),
			'locked' => $locked,
		), 
		array( 
			'%d', 
			'%d',
			'%s',
			'%s',
			'%s',
			'%d',
		) 
	);
	$id = $wpdb->insert_id;
	
    
    // ��������� �������
    if (is_uploaded_file($_FILES['logo']['tmp_name'])) {
        $filename = "/wp-content/plugins/siterating/images/logos/$id.gif";
        img($_FILES['logo']['tmp_name'], $filename, 99, 48);
    }
    
    // �������� ���������� � ������, ������ ���
    $tpl->assign('sitepath', $sitepath);
    $tpl->assign('url', $url);
    $tpl->assign('id', $id);
    $tpl->assign('sign', md5("It is sign for $id"));
    $tpl->clearAllCache();
    
    // ������ ���� ������
    $sql = "SELECT * FROM wp_users WHERE ID=1";
    $row = $wpdb->get_row($sql, ARRAY_A);
    
    // �������� ������ ������
    include 'htmlMimeMail/htmlMimeMail.php';
    $mail = new htmlMimeMail();
    $mail->setHtmlCharset('utf-8');
    $mail->setFrom('noreply@'.preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']));
    $subject = '=?koi8-r?B?'.base64_encode(convert_cyr_string("������ �� ���������� ������ ����� � �������",'w','k')).'?=';
    $mail->setSubject($subject);
    $mail->setHtml($tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME).'/templates/email_insert.tpl'));
    @$mail->send(array($row['user_email']));
    
    return "/{$sitepath[0]}/counter/id-$id/";
    
}

function forexcatalog_redirect_update() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if ((!$sitepath['id'] || !$sitepath['user']) && $sitepath['vars']['sign']!=md5("It is sign for {$sitepath['id']}")) {
        return false;
    }
    if ($sitepath['vars']['sign']==md5("It is sign for {$sitepath['id']}")) {
        $sitepath['admin'] = true;
    }
    
    $sql = "SELECT * FROM stat_sites WHERE id={$sitepath[id]}";
    $data = $wpdb->get_row($sql, ARRAY_A);
    if (!$sitepath['admin'] && $data['user_id']!=$sitepath['user']) {
        return false;
    }
    $tpl->assign('data', $data);
    
    // ������� URL � �������� ����
    $urlparts = parse_url(str_replace('\\\\', '/', $_POST['url']));
    $urlparts['host'] = preg_replace('/^www\./', '', $urlparts['host']);
    $url = $urlparts['host'] . ($urlparts['path']=='/' ? '' : $urlparts['path']) . ($urlparts['query'] ? '?'.$urlparts['query'] : '');
    
    $locked = intval($_POST['locked']);
    $status = $sitepath['admin'] ? ", status='{$_POST['status']}'" : '';
    $sql = "UPDATE stat_sites SET
            rubric_id={$_POST['rubric_id']},
            name='".mysql_escape_string($_POST['name'])."',
            url='".mysql_escape_string($url)."',
            description='".mysql_escape_string($_POST['description'])."',
            locked=$locked
            $status
            WHERE id={$sitepath['id']}";
    $wpdb->get_row($sql);
    $tpl->clearAllCache();
    
    // ��������� �������
    if (is_uploaded_file($_FILES['logo']['tmp_name'])) {
        $filename = "/wp-content/plugins/siterating/images/logos/{$sitepath[id]}.gif";
        img($_FILES['logo']['tmp_name'], $filename, 99, 48);
    }
    
    if ($sitepath['admin'] && $data['status']!=$_POST['status']) { // ���������� ������ �� ��������� �������?
        
        // ������ ���� ������������
        $sql = "SELECT * FROM wp_users WHERE ID={$data['user_id']}";
        $row = $wpdb->get_row($sql, ARRAY_A);
        
        // �������� ������ ������
        include 'htmlMimeMail/htmlMimeMail.php';
        $mail = new htmlMimeMail();
        $mail->setHtmlCharset('utf-8');
        $mail->setFrom('noreply@'.preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']));
        $subject = '=?koi8-r?B?'.base64_encode(convert_cyr_string("������ ������� ������ ����� {$data['url']} � �������� ������ ������ �������",'w','k')).'?=';
        $mail->setSubject($subject);
        $mail->setHtml($tpl->fetch(pathinfo(__FILE__, PATHINFO_DIRNAME).'/templates/email_update.tpl'));
        @$mail->send(array($row['user_email']));
        
    }
    
    if ($sitepath['vars']['sign']) {
        $url = "/{$sitepath[0]}/";
    }
    else {
        $url = "/{$sitepath[0]}/my/".($sitepath['admin'] ? "id-{$data['user_id']}/" : '');
    }
    
    return $url;
    
}

function forexcatalog_redirect_del() {
    
    global $sitepath, $tpl, $wpdb;
    
    $tpl->caching = false;
    
    if ((!$sitepath['id'] || !$sitepath['user']) && $sitepath['vars']['sign']!=md5("It is sign for {$sitepath['id']}")) {
        return false;
    }
    if ($sitepath['vars']['sign']==md5("It is sign for {$sitepath['id']}")) {
        $sitepath['admin'] = true;
    }
    
    $sql = "SELECT * FROM stat_sites WHERE id={$sitepath[id]}";
    $data = $wpdb->get_row($sql, ARRAY_A);
    if (!$sitepath['admin'] && $data['user_id']!=$sitepath['user']) {
        return false;
    }
    
    // �������
    $wpdb->get_row("DELETE FROM stat_sites WHERE id={$sitepath['id']}");
    $wpdb->get_row("DELETE FROM stat_log WHERE site_id={$sitepath['id']}");
    $wpdb->get_row("DELETE FROM stat_days WHERE site_id={$sitepath['id']}");
    $wpdb->get_row("DELETE FROM stat_reviews WHERE site_id={$sitepath['id']}");
    @unlink("{$_SERVER[DOCUMENT_ROOT]}/wp-content/plugins/siterating/images/logos/{$sitepath[id]}.gif");
    
    if ($sitepath['vars']['sign']) {
        $url = "/{$sitepath[0]}/";
    }
    else {
        $url = "/{$sitepath[0]}/my/".($sitepath['admin'] ? "id-{$data['user_id']}/" : '');
    }
    
    return $url;
    
}
