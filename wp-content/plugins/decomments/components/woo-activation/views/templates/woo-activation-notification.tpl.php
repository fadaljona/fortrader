<h1 id="title"><?php esc_html_e( 'Activate plugin', DECOM_LANG_DOMAIN ); ?></h1>
<?php esc_html_e( 'Component will be activated by administrator soon.', DECOM_LANG_DOMAIN ); ?>
