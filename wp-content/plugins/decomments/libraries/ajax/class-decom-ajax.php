<?php

class Decom_Ajax {
	public function __construct() {
		add_action( 'wp_ajax_decom_get_comment', array( &$this, 'getCommentByID' ) );
		add_action( 'wp_ajax_nopriv_decom_get_comment', array( &$this, 'getCommentByID' ) );

		add_action( 'wp_ajax_decom_save_edit_comment', array( &$this, 'saveEditComment' ) );
		add_action( 'wp_ajax_nopriv_decom_save_edit_comment', array( &$this, 'saveEditComment' ) );
	}

	public function saveEditComment() {

		$comment_id = $_POST['comment_id'];
		$nonce      = $_POST['_nonce'];
		$content    = urldecode( $_POST['content'] );

		$post['id']      = $comment_id;
		$post['content'] = $content;

		if ( user_can( get_current_user_id(), 'administrator' ) ) {
			$post['is_m'] = 1;
		}

		$comments_controller = DECOM_Loader_MVC::getComponentController( 'comments', 'comments' );
		$comments_controller->editComments( $get, $post );


		$args = array(
			'comment_ID'      => $comment_id,
			'comment_content' => $content,
		);

		die();
	}

	public function getCommentByID() {

		$comment_id = $_POST['comment_id'];

		$comment         = get_comment( $comment_id );
		$comment_content = $comment->comment_content;
		$comment_content = str_replace( '<blockquote><div><cite>', '[quote]', $comment_content );
		$comment_content = str_replace( '</cite></div></blockquote>', '[/quote]', $comment_content );

		$comment_content = str_replace( '<blockquote><cite>', '[quote]', $comment_content );
		$comment_content = str_replace( '</cite></blockquote>', '[/quote]', $comment_content );

		$res = array(
			'content'     => $comment_content,
			'name_button' => __( 'Save', DECOM_LANG_DOMAIN )
		);

		echo json_encode( $res );
		die();
	}
}

$Deco_Ajax = new Decom_Ajax();