<?php

class DECOM_Email {

	public function sendNotificationEmail( array $messages ) {

//		$headers = 'Content-type: text/html; charset=utf-8' . "\r\n";
//		$res     = @wp_mail( $messages['email'], $messages['subject'], $messages['body'], $headers );

		return $res;
	}

	public function notyfy_post_comments( array $email_array, $comment ) {
		
		$model_notifications = DECOM_Loader_MVC::getComponentModel( 'notification-messages', 'notification-messages' );
		$post                = get_post( $comment->comment_post_ID );
		
		$categories = wp_get_post_categories( $comment->comment_post_ID );
		$servicesCat = get_option('services_cat_id');
		
		if( $categories[0] != $servicesCat ){
			$post_title       = apply_filters( 'the_title', $post->post_title );
			$comment_post_url = get_permalink( $comment->comment_post_ID );
			$comment_link     = $comment_post_url . '#comment-' . $comment->comment_ID;
		}else{
			$title = str_replace( 'DO NOT DELETE__', '', $post->post_title );
			$routes = explode('.', $title);
			$rote = $routes[0] . '/' . $routes[1];
			if( $routes[2] ) {
				$expArgs = explode('_', $routes[2]);
				$modelId = $expArgs[1];
			}
			$data = getDataForServicesComments($rote, $modelId, $comment->comment_post_ID, $comment);
			$post_title       = $data['postTitle'];
			$comment_post_url = str_replace('/services', '', $data['postLink']);
			$comment_link     = $comment_post_url . '#comment-' . $comment->comment_ID;
		}
		$comment_author   = $comment->comment_author;
		$comment_text     = apply_filters( 'decomments_comment_text', $comment->comment_content );
		$comment_creation_date = date( "Y-m-d H:i:s" );
		
		$emails_list_post = array();
		$mail_my_comment = array();
		
		if( is_array( $email_array['new_post_comment'] ) ){
			if(($key = array_search($comment->comment_author_email, $email_array['new_post_comment'])) !== false) unset($email_array['new_post_comment'][$key]);
			$emails_list_post = $email_array['new_post_comment'];
		}else{
			if( $comment->comment_author_email != $email_array['new_post_comment'] ){
				$emails_list_post = array( $email_array['new_post_comment'] );
			}
		}
		if( is_array( $email_array['new_comment_to_comment'] ) ){
			if(($key = array_search($comment->comment_author_email, $email_array['new_comment_to_comment'])) !== false) unset($email_array['new_comment_to_comment'][$key]);
			$mail_my_comment  = $email_array['new_comment_to_comment'];
		}else{
			if( $comment->comment_author_email != $email_array['new_comment_to_comment'] ){
				$mail_my_comment  = array( $email_array['new_comment_to_comment'] );
			}
		}
		
		$emails_list_post = array_diff ( $emails_list_post , $mail_my_comment );

		$site_name = str_replace( array( 'http://', 'https://' ), '', get_bloginfo( 'url' ) );


		// отправка письма к комментарию
		if ( $mail_my_comment ) {
			$messageMyComment = $model_notifications->getNotificationLocale( 'new_comment_to_comment' );
			foreach ( $mail_my_comment as $email ) {
				if( trim($email) ){
					$messagesComment = $this->substitution( $messageMyComment->notification_title, $messageMyComment->notification_text, $post_title, $comment_author, $comment_text, $comment_link, $comment_post_url, $comment_creation_date, $email, 1, $comment->comment_post_ID );

					wp_mail( $email, $messagesComment['subject'], $messagesComment['text'], $headers );
				}
				
			}
		}

		//отправка письма к посту
		if ( $emails_list_post ) {

			$messagePostComment = $model_notifications->getNotificationLocale( 'new_post_comment' );

			foreach ( $emails_list_post as $email ) {
				if( trim($email) ){
					$messagesPost = $this->substitution( $messagePostComment->notification_title, $messagePostComment->notification_text, $post_title, $comment_author, $comment_text, $comment_link, $comment_post_url, $comment_creation_date, $email, 2, $comment->comment_post_ID );
					
					wp_mail( $email, $messagesPost['subject'], $messagesPost['text'], $headers );
				}
			}
		}

	}

	public function substitution( $message_title, $message_text, $post_title, $comment_author, $comment_text, $comment_link, $comment_post_url, $comment_creation_date, $email = '', $type = 0, $postId ) {
		$message['subject'] = preg_replace( '/%COMMENTED_POST_TITLE%/', $post_title, $message_title );
		$message['text']    = preg_replace( '/%COMMENTED_POST_TITLE%/', $post_title, $message_text );

		$message['text']    = preg_replace( '/%COMMENT_TEXT%/', strip_tags($comment_text), $message['text'] );
		$message['text']    = preg_replace( '/%COMMENT_AUTHOR%/', $comment_author, $message['text'] );
		$message['text']    = preg_replace( '/%COMMENT_LINK%/', $comment_link, $message['text'] );
		$message['text']    = preg_replace( '/%COMMENTED_POST_URL%/', $comment_post_url, $message['text'] );
		$message['text']    = preg_replace( '/%COMMENT_CREATION_DATE%/', $comment_creation_date, $message['text'] );

		$siteUrl = str_replace( 'http://', 'https://', get_bloginfo( 'url' ) );

		$link = $siteUrl . "/comments-unsubscribe/?h=". md5(UNSUBSCRIBE_SALT . $email . $postId) ."&e=$email&p=$postId";



		$message['text'] = "{$message['text']}


		
". __( 'Unsubscribe from comment emails on this post', DECOM_LANG_DOMAIN )." {$link}&t=single
". __( 'Unsubscribe from all comment emails', DECOM_LANG_DOMAIN )." {$link}&t=all
";



		//var_dump($message);
		return $message;
	}


	public function endOutput( $endMessage ) {
		ignore_user_abort( true );
		set_time_limit( 0 );
		header( "Content-Length: " . strlen( $endMessage ) . "\r\n" );
		header( "Connection: close\r\n" );
		echo $endMessage;
		echo str_repeat( "\r\n", 10 ); // just to be sure
		flush();
	}

	public function endOutput2( $content ) {
		ignore_user_abort( true );
		set_time_limit( 0 );
		$headers = get_headers( home_url(), 1 );
		foreach ( $headers as $header => $value ) {
			if ( $header == 0 ) {
				header( $value . "\r\n" );
			} else {
				header( $header . ': ' . $value . "\r\n" );
			}
			if ( $header == 'Vary' ) {
				header( "Content-Length: " . ( strlen( $content ) - 10 ) . "\r\n" );
			}
		}
		header( "\r\n" );
		echo $content;
		echo str_repeat( "\r\n", 10 );
		flush();
		ob_clean();
	}

	public function endOutput1( $content ) {
		$headers = get_headers( home_url(), 1 );
		header( "HTTP/1.1 200 OK" );
		header( "Cache-Control:no-cache, must-revalidate, max-age=0\r\n" );
		header( "Connection:Keep-Alive\r\n" );
		header( "Content-Encoding:gzip\r\n" );
		header( "Content-Length: " . strlen( $content ) . "\r\n" );
		header( "Content-Type:text/html\r\n" );
		header( "Date:{$headers['Date']}\r\n" );
		header( "Expires:{$headers['Expires']}\r\n" );
		header( "Keep-Alive:timeout=5, max=100\r\n" );
		header( "Pragma:no-cache\r\n" );
		header( "Server:Apache/2.2.22 (Ubuntu)\r\n" );
		header( "Vary:Accept-Encoding\r\n" );
		header( "X-Powered-By:PHP/5.4.9-4ubuntu2.2\r\n\r\n" );
		echo $content;


	}

}
