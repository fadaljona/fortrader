<?php
defined( 'ABSPATH' ) or die( 'Cheatin\' uh?' );

/**
 * Link to the configuration page of the plugin
 *
 * @since 1.0
 */
add_filter( 'plugin_action_links_' . plugin_basename( DECOM_FILE ), '__decomments_settings_action_links' );
function __decomments_settings_action_links( $actions ) {
	array_unshift( $actions, sprintf( '<a href="%s">%s</a>', admin_url( 'edit-comments.php?page=decomments-index' ), __( 'Settings', DECOM_LANG_DOMAIN ) ) );

	return $actions;
}
