<?php
/*
Plugin Name: it-finance.com грабер 
Plugin URI: 
Description: Вставляет график в пост по [wp-it-finance].
Version: 1.0
Author: Pti_the_Leader
Author URI: http://ptipti.ru/
Author Email: ptipti@gala.net
*/

add_filter ('the_content', 'it_finance');

function it_finance ($content = '') {

if (preg_match ('/\[wp\-it\-finance\]/', $content)) :
	$options = array (
	2152 => 'AUD/CAD',
	1296 => 'AUD/CHF',
	2294 => 'AUD/CZK',
	2417 => 'AUD/DKK',
	1029 => 'AUD/HKD',
	2313 => 'AUD/HUF',
	5153 => 'AUD/JPY',
	40877 => 'AUD/MXN',
	2268 => 'AUD/NOK',
	1799 => 'AUD/NZD',
	2127 => 'AUD/PLN',
	2126 => 'AUD/SEK',
	1890 => 'AUD/SGD',
	4811 => 'AUD/USD',
	1864 => 'AUD/ZAR',
	2352 => 'CAD/CHF',
	2379 => 'CAD/CZK',
	1863 => 'CAD/DKK',
	2291 => 'CAD/HKD',
	2068 => 'CAD/HUF',
	1965 => 'CAD/JPY',
	40882 => 'CAD/MXN',
	2280 => 'CAD/NOK',
	2296 => 'CAD/PLN',
	2221 => 'CAD/SEK',
	2304 => 'CAD/SGD',
	1273 => 'CAD/ZAR',
	2368 => 'CHF/CZK',
	2134 => 'CHF/DKK',
	980 => 'CHF/HKD',
	2310 => 'CHF/HUF',
	1975 => 'CHF/JPY',
	40884 => 'CHF/MXN',
	553 => 'CHF/NOK',
	2086 => 'CHF/PLN',
	2364 => 'CHF/SEK',
	2374 => 'CHF/SGD',
	2315 => 'CHF/ZAR',
	2049 => 'DKK/CZK',
	2388 => 'DKK/HKD',
	1620 => 'DKK/HUF',
	40889 => 'DKK/MXN',
	2372 => 'DKK/NOK',
	254 => 'DKK/PLN',
	2288 => 'DKK/SEK',
	1820 => 'DKK/SGD',
	2351 => 'DKK/ZAR',
	4908 => 'EUR/AUD',
	5883 => 'EUR/CAD',
	1931 => 'EUR/CHF',
	5419 => 'EUR/CZK',
	25050 => 'EUR/DKK',
	5101 => 'EUR/GBP',
	2264 => 'EUR/HKD',
	24774 => 'EUR/HUF',
	3484 => 'EUR/JPY',
	40891 => 'EUR/MXN',
	8636 => 'EUR/NOK',
	5243 => 'EUR/NZD',
	2019 => 'EUR/PLN',
	1964 => 'EUR/SEK',
	2426 => 'EUR/SGD',
	5255 => 'EUR/USD',
	2347 => 'EUR/ZAR',
	1814 => 'GBP/AUD',
	2371 => 'GBP/CAD',
	1955 => 'GBP/CHF',
	2293 => 'GBP/CZK',
	2044 => 'GBP/DKK',
	2322 => 'GBP/HKD',
	695 => 'GBP/HUF',
	1968 => 'GBP/JPY',
	40900 => 'GBP/MXN',
	2082 => 'GBP/NOK',
	2326 => 'GBP/NZD',
	2390 => 'GBP/PLN',
	2410 => 'GBP/SEK',
	2377 => 'GBP/SGD',
	1934 => 'GBP/USD',
	2402 => 'GBP/ZAR',
	2353 => 'JPY/CZK',
	2073 => 'JPY/DKK',
	1255 => 'JPY/HKD',
	2059 => 'JPY/HUF',
	41437 => 'JPY/MXN',
	2038 => 'JPY/NOK',
	2252 => 'JPY/PLN',
	2415 => 'JPY/SEK',
	2129 => 'JPY/SGD',
	2312 => 'JPY/ZAR',
	2330 => 'NOK/CZK',
	1644 => 'NOK/HKD',
	1236 => 'NOK/HUF',
	41430 => 'NOK/MXN',
	1326 => 'NOK/PLN',
	2076 => 'NOK/SEK',
	1146 => 'NOK/SGD',
	2004 => 'NOK/ZAR',
	2222 => 'NZD/CAD',
	2133 => 'NZD/CHF',
	1143 => 'NZD/CZK',
	2370 => 'NZD/DKK',
	187 => 'NZD/HKD',
	2261 => 'NZD/HUF',
	2331 => 'NZD/JPY',
	41431 => 'NZD/MXN',
	1939 => 'NZD/NOK',
	2303 => 'NZD/PLN',
	182 => 'NZD/SEK',
	2409 => 'NZD/SGD',
	9383 => 'NZD/USD',
	173 => 'NZD/ZAR',
	9738 => 'USD/CAD',
	1951 => 'USD/CHF',
	2003 => 'USD/CZK',
	5511 => 'USD/DKK',
	39821 => 'USD/HKD',
	5886 => 'USD/HUF',
	2020 => 'USD/JPY',
	19632 => 'USD/MXN',
	5257 => 'USD/NOK',
	1981 => 'USD/PLN',
	1996 => 'USD/SEK',
	25672 => 'USD/SGD',
	2018 => 'USD/ZAR',
	2118 => 'XAG/USD',
	2054 => 'XAU/USD'
);

	$selected = $_POST['pair'];
	if (!$selected) $selected = 5255;

	foreach ($options as $key => $value) :
		$option .= '<option ';
		if ($key == $selected) $option .= 'SELECTED ';
		$option .= 'value="'.$key.'">'.$value.'</option>';
	endforeach;


	$css = '<link rel="stylesheet" type="text/css" href="'.trailingslashit (plugins_url (basename(dirname (__FILE__)))).'style.css" />';

	$style = $_POST['style'];
	if ($style && preg_match ('/classic/i', $style)) :
		$url = 'rt2.it-finance.com/FXStreetLightPlus/itchartslight';
		$cla = ' active';
	else :
		$url = 'fx1.it-finance.com/FXStreetFlashPublic/itcharts';
		$dea = ' active';
	endif;

	$javascript = "
<script type=\"text/javascript\">
function post_to_url (param, value) {
        var form = document.createElement('form');
        form._submit_function_ = form.submit;
        form.setAttribute('method', 'post');
        form.setAttribute('action', '');
        var hiddenField = document.createElement('input');
        hiddenField.setAttribute('type', 'hidden');
        hiddenField.setAttribute('name', param);
        hiddenField.setAttribute('value', value);
        form.appendChild(hiddenField);
        document.body.appendChild(form);
        form._submit_function_();
}
</script>";

	$it_finance = $css.'
<div class="section-applet section">
	<div class="applet-header">
		<div class="layout-bar bar-utilities" style="height: 38px;">
			<div class="bar-left">
				<label for="ddlid"><span class="icon i-selection"></span>Select Pair:</label>
				<select onChange="post_to_url (\'pair\', this.value);">
					'.$option.'
				</select>
			</div>
			<div class="bar-right">
				<div class="float-r"><a href="#" class="btn it-detach center" onClick="var newWindow = window.open (\''.trailingslashit (plugins_url (basename(dirname (__FILE__)))).'graph.php?id='.$selected.'\', \'Graph\', \'scrollbars=no,resizable=yes,location=no,directories=no,menubar=no,toolbar=no,status=no\'); newWindow.focus(); return false;" title="Detach this chart in a new window"><span class="icon i-detach"></span>Detach</a></div>
				<div class="float-r"><div class="layout-btns-pill">
					<a href="javascript:post_to_url (\'style\', \'default\');" class="btn-pill pill-first'.$dea.'">Default</a>
					<a href="javascript:post_to_url (\'style\', \'classic\');" class="btn-pill pill-last'.$cla.'">Classic</a>
				</div></div>
			</div>
		</div>
	</div>
	<iframe src="http://'.$url.'.phtml?id='.$selected.'&news=1" width="100%" height="600" frameborder="0" style="border:0px none"></iframe>
	<div class="applet-footer">
		<div class="layout-bar bar-footer">
			<div class="bar-left">
			    <p>
				    Solution by <a href="http://www.prorealtime.com/"><strong>ProRealTime</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;
				    Data Source: <a href="http://www.rtfx.com/"><strong>RTFX Ltd</strong></a><br />
				    All times within the application are <strong>CET (GMT+1)</strong>
			    </p>
			    <p><a href="http://www.java.com/en/download/installed.jsp">Problems with the Java Applet? Verify version.</a></p>

			</div>
			<div class="bar-right">
                        
			</div>
		</div>
	</div>
</div>
'.$javascript;
	$content = preg_replace('/\[wp\-it\-finance\]/', $it_finance, $content);
endif;

return $content;

}
?>