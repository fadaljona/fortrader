(function() {
	tinymce.PluginManager.add('info_block', function( editor, url ) {
		var sh_tag = 'info_block';

		//helper functions 
		function getAttr(s, n) {
			n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
			return n ?  window.decodeURIComponent(n[1]) : '';
		};
		function getCon(s) {
			n = new RegExp('Content\:\:([^<]+)<', 'g').exec(s);
			return n ?  n[1] : '';
		};

		function html( cls, data ,con) {
			
			
			var linkText = getAttr(data,'linkText');
			var linkUrl = getAttr(data,'linkUrl');
			var imageUrl = getAttr(data,'imageUrl');
			var align = getAttr(data,'align');
			
			var imageStr = '';
			if( imageUrl ) imageStr = '<img src="'+imageUrl+'">';
			
			if( linkText && linkUrl ) linkText = '<h5><a href="'+linkUrl+'" target="_blank">'+linkText+'</a></h5>';

			data = window.encodeURIComponent( data );
			content = window.encodeURIComponent( con );

			return '<div class="mceItemInfoBlock ' + cls + '" style="border:1px solid #23282D;border-radius:5px;display:inline-block;max-width:300px;margin:20px;float:'+align+'" data-sh-attr="' + data + '" >'+linkText+imageStr+'<br><span class="mceItemInfoBlockContent">Content::'+ con+'</span></div>';
		}

		function replaceShortcodes( content ) {
			return content.replace( /\[info_block([^\]]*)\]([^\[]*)\[\/info_block\]/g, function( all,attr,con) {
				return html( 'wp-info_block', attr , con);
			});
		}

		function restoreShortcodes( content ) {
			return content.replace( /(?:<p(?: [^>]+)?>)*(<div class="mceItemInfoBlock [^>]+>.*?<span class="mceItemInfoBlockContent">[^<]+<\/span><\/div>)(?:<\/p>)*/g, function( match, block ) {

				var data = getAttr( block, 'data-sh-attr' );
				var con = getCon( block );

				if ( data ) {
					return '<p>[' + sh_tag + data + ']' + con + '[/'+sh_tag+']</p>';
				}
				return match;
			});
		}

		//add popup
		editor.addCommand('info_block_popup', function(ui, v) {
			//setup defaults
			var linkText = '';
			if (v.linkText)
				linkText = v.linkText;
			var linkUrl = '';
			if (v.linkUrl)
				linkUrl = v.linkUrl;
			var imageUrl = '';
			if (v.imageUrl)
				imageUrl = v.imageUrl;
			var align = 'left';
			if (v.align)
				align = v.align;
			var content = '';
			if (v.content)
				content = v.content;
			
			var targetBlock = false;
			if( v.targetBlock ) targetBlock = v.targetBlock;

			editor.windowManager.open( {
				title: 'Info Block Shortcode',
				body: [
					{
						type: 'textbox',
						name: 'linkText',
						label: 'Link text',
						value: linkText,
						tooltip: 'Required'
					},
					{
						type: 'textbox',
						name: 'linkUrl',
						label: 'Link',
						value: linkUrl,
						tooltip: 'Required'
					},
					{
						type: 'textbox',
						name: 'imageUrl',
						label: 'Image Url',
						value: imageUrl,
						tooltip: 'Leave blank for none'
					},
					{
						type: 'listbox',
						name: 'align',
						label: 'Align',
						value: align,
						'values': [
							{text: 'Left', value: 'left'},
							{text: 'Right', value: 'right'},
						],
						tooltip: 'Select the align'
					},
					{
						type: 'textbox',
						name: 'content',
						label: 'Content',
						value: content,
						multiline: true,
						minWidth: 300,
						minHeight: 100,
						tooltip: 'Required'
					}
				],
				onsubmit: function( e ) {
					var shortcode_str = '[' + sh_tag + ' align="'+e.data.align+'"';
					
					if (typeof e.data.linkText != 'undefined' && e.data.linkText.length)
						shortcode_str += ' linkText="' + e.data.linkText + '"';
					
					if (typeof e.data.linkUrl != 'undefined' && e.data.linkUrl.length)
						shortcode_str += ' linkUrl="' + e.data.linkUrl + '"';
					
					if (typeof e.data.imageUrl != 'undefined' && e.data.imageUrl.length)
						shortcode_str += ' imageUrl="' + e.data.imageUrl + '"';

					
					shortcode_str += ']' + e.data.content + '[/' + sh_tag + ']';
					
					if( targetBlock ) targetBlock.parentElement.removeChild(targetBlock);
	
					editor.insertContent( shortcode_str);
				}
			});
	      	});


		editor.addButton('info_block', {
			icon: 'info_block',
			tooltip: 'Info Block',
			onclick: function() {
				editor.execCommand('info_block_popup','',{
					linkText : '',
					linkUrl : '',
					imageUrl : '',
					align   : 'left',
					content: ''
				});
			}
		});

		//replace shortcode 
		editor.on('BeforeSetcontent', function(event){ 
			event.content = replaceShortcodes( event.content );
		});

		//replace from image placeholder to shortcode
		editor.on('GetContent', function(event){
			event.content = restoreShortcodes(event.content);
		});

		//open popup on placeholder double click
		editor.on('Click',function(e) {	
			if ( e.target.className.indexOf('wp-info_block') > -1 ) {
				var title = e.target.attributes['data-sh-attr'].value;
				title = window.decodeURIComponent(title);
				var content = getCon( e.target.innerHTML + '<' );

				editor.execCommand('info_block_popup','',{
					linkText : getAttr(title,'linkText'),
					linkUrl : getAttr(title,'linkUrl'),
					imageUrl : getAttr(title,'imageUrl'),
					align   : getAttr(title,'align'),
					content: content,
					targetBlock: e.target
				});
			}
		});
	});
})();