<?php
/*
Plugin Name: Info block shortcode
Plugin URI:
Description: Info block shortcode for TinyMce
Version: 1.0
Author: Vekshin Vladimir akadem87@gmail.com
*/
class Info_block_shortcode{

	public $shortcode_tag = 'info_block';

	function __construct($args = array()){

		add_shortcode( $this->shortcode_tag, array( $this, 'shortcode_handler' ) );
		
		if ( is_admin() ){
			add_action('admin_head', array( $this, 'admin_head') );
			add_action( 'admin_enqueue_scripts', array($this , 'admin_enqueue_scripts' ) );
		}
	}
	function removeProtocol( $src ){
		if( strpos('http', $src) == 0 ){
			$src = str_replace( 'http://', '//', $src);
			$src = str_replace( 'https://', '//', $src);
		}
		return $src;
	}
	function shortcode_handler($atts , $content = null){
		extract( shortcode_atts(
			array(
				'linktext' => '',
				'linkurl' => '',
				'imageurl' => '',
				'align' => 'left',
			), $atts )
		);
		
		if( !$content ) return '';
		
		$output = '<div class="info_box align'.$align.'">';
		
		if( $imageurl && $linkurl && $linktext ){
			$output .= '<div class="info_box_img"><a href="'. $this->removeProtocol( $linkurl ).'"><img alt="'.$linktext.'" src="'. $this->removeProtocol( $imageurl ) .'"></a></div>';
		}
		
		if( $linkurl && $linktext ){
			$output .= '<h5 class="info_box_title"><a href="'. $this->removeProtocol( $linkurl ).'">'.$linktext.'</a></h5>';
		}
		
		$output .= '<p>'.$content.'</p>';
		
		$output .= '</div>';
		

		return $output;
	}


	function admin_head() {
		if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
			return;
		}
		if ( 'true' == get_user_option( 'rich_editing' ) ) {
			add_filter( 'mce_external_plugins', array( $this ,'mce_external_plugins' ) );
			add_filter( 'mce_buttons', array($this, 'mce_buttons' ) );
		}
	}

	function mce_external_plugins( $plugin_array ) {
		$plugin_array[$this->shortcode_tag] = plugins_url( 'js/mce-button.js' , __FILE__ );
		return $plugin_array;
	}
	function mce_buttons( $buttons ) {
		array_push( $buttons, $this->shortcode_tag );
		return $buttons;
	}
	function admin_enqueue_scripts(){
		 wp_enqueue_style('info_block_shortcode', plugins_url( 'css/mce-button.css' , __FILE__ ) );
	}
}

new Info_block_shortcode();