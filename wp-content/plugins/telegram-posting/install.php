<?php

global $posting_to_telegram_db_version;
$posting_to_telegram_db_version = '1.0';

function telegram_posting_install()
{
    global $wpdb;
    global $posting_to_telegram_db_version;

    $table_name = $wpdb->prefix . 'telegram_posting_posts_log';
    
    $charset_collate = $wpdb->get_charset_collate();
    
    $sql = "CREATE TABLE IF NOT EXISTS $table_name (
        id bigint(20) UNSIGNED NOT NULL,
        message_id bigint(20) UNSIGNED DEFAULT NULL,
        error text DEFAULT NULL,
        timestamp int(11) DEFAULT NULL,
        UNIQUE KEY id (id)
    ) $charset_collate;
    ";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);

    add_option('posting_to_telegram_db_version', $posting_to_telegram_db_version);
}

function telegram_posting_uninstall()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'telegram_posting_posts_log';
    $wpdb->query("DROP TABLE IF EXISTS $table_name");
    delete_option("telegram-posting-settings");
}

register_activation_hook($pluginFile, 'telegram_posting_install');

register_uninstall_hook($pluginFile, 'telegram_posting_uninstall');
