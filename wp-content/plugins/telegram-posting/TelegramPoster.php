<?php
require __DIR__ . '/vendor/autoload.php';

use \unreal4u\TelegramAPI\HttpClientRequestHandler;
use \unreal4u\TelegramAPI\TgLog;
use \unreal4u\TelegramAPI\Telegram\Methods\SendMessage;

class TelegramPoster
{
    private $settings;

    private $postingLogTable;

    protected static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
            self::$instance->settings = get_option('telegram-posting-settings');

            if (!self::$instance->settings['bot_token'] || !self::$instance->settings['user_chat_id']) {
                return false;
            }

            self::$instance->postingLogTable = self::getTableName();
        }
        return self::$instance;
    }

    public static function getTableName()
    {
        global $wpdb;
        return $wpdb->prefix . 'telegram_posting_posts_log';
    }

    public static function getPostingData($postId)
    {
        global $wpdb;
        $row = $wpdb->get_row($wpdb->prepare(
            "SELECT * FROM " . self::getTableName() . " WHERE id = %d",
            $postId
        ));
        return $row;
    }

    private function savePostingLog($postId, $messageId, $error, $time = false)
    {
        if (!$time) {
            $time = time();
        }
        global $wpdb;
        $wpdb->replace(
            $this->postingLogTable,
            array(
                'id' => $postId,
                'message_id' => $messageId,
                'error' => $error,
                'timeStamp' => $time,
            ),
            array(
                '%d',
                '%d',
                '%s',
                '%d'
            )
        );
    }

    public function sendPost($postId)
    {
        $messageText = '<a href="' . get_permalink($postId) . '">' . get_the_title($postId) . '</a>';
        if ($imgSrc = p75GetThumbnail($postId, 120, 100, "")) {
            $messageText .= ' <a href="' . $imgSrc . '">&#8205;</a>';
        }

        $loop = \React\EventLoop\Factory::create();
        $handler = new HttpClientRequestHandler($loop);
        $tgLog = new TgLog($this->settings['bot_token'], $handler);

        $sendMessage = new SendMessage();
        $sendMessage->chat_id = $this->settings['user_chat_id'];
        $sendMessage->text = $messageText;
        $sendMessage->parse_mode = "html";

        $tgLog->performApiRequest($sendMessage)->then(
            function ($response) use ($postId) {
                $this->savePostingLog($postId, $response->message_id, '', $response->date);
            },
            function (\Exception $exception) use ($postId) {
                $this->savePostingLog($postId, null, 'Exception ' . get_class($exception) . ' caught, message: ' . $exception->getMessage());
            }
        );

        $loop->run();
    }

    public function urlsToHtmlLinks($text)
    {
        if (!$text) {
            return false;
        }

        $url_pattern = '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/';
        $replace = '<a href="$0" target="_blank" title="$0">$0</a>';
        return $text = preg_replace($url_pattern, $replace, $text);
    }

    /**
     * Undocumented function
     *
     * @param integer $commentId
     * @param string $title post title
     * @param string $url post url
     * @return array
     */
    public function sendComment($commentId, $title = '', $url = '')
    {
        if (!current_user_can('editor') && !current_user_can('administrator')) {
            return [
                'posted' => false,
                'errorMsg' => 'you can\'t do it'
            ];
        }

        if (!$commentId) {
            return [
                'posted' => false,
                'errorMsg' => 'comment not found'
            ];
        }

        $comment = get_comment($commentId);

        if (!$comment) {
            return [
                'posted' => false,
                'errorMsg' => 'comment not found'
            ];
        }

        if (get_comment_meta($commentId, 'postedToTelegram', true)) {
            return [
                'posted' => false,
                'errorMsg' => 'already posted'
            ];
        }

        $messageText = '';

        $commentPics = get_comment_meta($commentId, 'decom_attached_pictures', true);
        if ($commentPics) {
            $commentPics = unserialize($commentPics);

            if (!empty($commentPics[0])) {
                $imgSrc = wp_get_attachment_url($commentPics[0]);
                if ($imgSrc) {
                    $messageText .= "<a href='{$imgSrc}'>&#8205;</a> ";
                }
            }
        }

        $messageText .= $this->urlsToHtmlLinks($comment->comment_content);

        if ($title && $url) {
            $messageText .= PHP_EOL . "<a href='$url' target='_blank' title='$title'>$title</a>";
        }

        $loop = \React\EventLoop\Factory::create();
        $handler = new HttpClientRequestHandler($loop);
        $tgLog = new TgLog($this->settings['bot_token'], $handler);

        $sendMessage = new SendMessage();
        $sendMessage->chat_id = $this->settings['user_chat_id'];
        $sendMessage->text = $messageText;
        $sendMessage->parse_mode = "html";

        $tgLog->performApiRequest($sendMessage)->then(
            function ($response) use ($commentId) {
                update_comment_meta($commentId, 'postedToTelegram', true);
            },
            function (\Exception $exception) use ($commentId) {
                update_comment_meta($commentId, 'postedToTelegram', false);
                update_comment_meta($commentId, 'postedToTelegramErrMsg', 'Exception ' . get_class($exception) . ' caught, message: ' . $exception->getMessage());
            }
        );

        $loop->run();

        if (get_comment_meta($commentId, 'postedToTelegram', true)) {
            comment_edited($commentId, '');
            return [
                'posted' => true
            ];
        } else {
            return [
                'posted' => false,
                'errorMsg' => get_comment_meta($commentId, 'postedToTelegramErrMsg', true)
            ];
        }
    }
}
