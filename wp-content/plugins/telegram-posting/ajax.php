<?php

function ajaxPostToTelegram()
{
    if (empty($_GET['comment_id'])) {
        die();
    }

    $title = !empty($_GET['title']) ? $_GET['title'] : '';
    $url = !empty($_GET['url']) ? $_GET['url'] : '';

    $telegramPoster = TelegramPoster::getInstance();
    echo json_encode($telegramPoster->sendComment($_GET['comment_id'], $title, $url));
    die();
}


add_action('wp_ajax_nopriv_postToTelegram', 'ajaxPostToTelegram');
add_action('wp_ajax_postToTelegram', 'ajaxPostToTelegram');
