<?php

add_action('add_meta_boxes', function () {
    add_meta_box('telegram_posting_block', __('Telegram posting'), 'telegram_posting_block', 'post', 'side', 'high');
});

function telegram_posting_block()
{
    $telegramPoster = TelegramPoster::getInstance();
    
    if (!$telegramPoster) {
        echo "please fill all <a href='/wp-admin/admin.php?page=telegram-posting'><b>settings</b></a><br><br>";
    } else {
        global $post;
        
        $telegramData = TelegramPoster::getPostingData($post->ID);
        
        if ($telegramData && $telegramData->message_id) {
            echo '<b>'.__('Message id') . '</b> ' . $telegramData->message_id . '<br />
            <b>'.__('Date') . '</b> ' . date('Y-m-d H:i:s', $telegramData->timestamp) . '<br /><br />';
        }
        if ($telegramData && $telegramData->error) {
            echo '<b>'.__('Error message') . '</b> ' . $telegramData->error . '<br /><br />';
        }
        
        echo '<input type="hidden" name="telegram-msg_noncename" id="telegram-msg_noncename" value="' . wp_create_nonce(plugin_basename(__FILE__)) . '" />';

        if (!$telegramData || !$telegramData->message_id) {
            $post_to_telegram = get_post_meta($post->ID, 'post_to_telegram', true);
            $post_to_telegram_checked = '';
            if ($post_to_telegram) {
                $post_to_telegram_checked = 'checked="checked"';
            }
            echo '<input type="checkbox" id="post_to_telegram" name="post_to_telegram" value="1" ' . $post_to_telegram_checked . '>';
            echo __('Post to telegram') . '<br><br>';
        }
    }
}

function save_telegram_posting_to_db($post_id, $post)
{
    if (!isset($_POST['telegram-msg_noncename']) || !wp_verify_nonce($_POST['telegram-msg_noncename'], plugin_basename(__FILE__))) {
        return $post->ID;
    }
    if (!current_user_can('edit_post', $post->ID)) {
        return $post->ID;
    }
        
    $arrToSave = array();
    
    if (isset($_POST['post_to_telegram']) && $_POST['post_to_telegram']) {
        $arrToSave['post_to_telegram'] = $_POST['post_to_telegram'];
    } else {
        $arrToSave['post_to_telegram'] = false;
    }
    
    foreach ($arrToSave as $key => $value) {
        if ($post->post_type == 'revision') {
            return;
        }
        if (!$value) {
            delete_post_meta($post->ID, $key);
        } else {
            update_post_meta($post->ID, $key, $value);
        }
    }
}
add_action('save_post', 'save_telegram_posting_to_db', 1, 2);

function run_telegram_posting($post_id, $post)
{
    if ($post->post_status != 'publish') {
        return false;
    }
    if ($post->post_type != 'post') {
        return false;
    }

    $permalink = get_permalink($post_id);
    if (preg_match('/(\?p=[0-9]+)$/', $permalink, $matches) || preg_match('/(\/[0-9]+)\.html$/', $permalink, $matches)) {
        return false;
    }
    
    $post_to_telegram = get_post_meta($post->ID, 'post_to_telegram', true);
    $telegramData = TelegramPoster::getPostingData($post_id);

    if ($post_to_telegram && (!$telegramData || !$telegramData->message_id)) {
        $telegramPoster = TelegramPoster::getInstance();
        $telegramPoster->sendPost($post_id);
    }
}

add_action('save_post', 'run_telegram_posting', 10, 2);
