<?php
/*
Plugin Name: Posting to telegram
Description: post messages to telegram
Version: 1.0
Author: Vekshin Vladimir akadem87@gmail.com
*/

$pluginFile = __FILE__;

require_once dirname($pluginFile).'/install.php';
require_once dirname($pluginFile).'/TelegramPoster.php';
require_once dirname($pluginFile).'/optionsPage.php';
require_once dirname($pluginFile).'/postsOptions.php';
require_once dirname($pluginFile).'/ajax.php';
