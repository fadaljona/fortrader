<?php

add_action('admin_menu', 'telegram_posting_menu');

function telegram_posting_menu()
{
    add_menu_page(__('Telegram posting settings'), __('Telegram posting'), 'manage_options', 'telegram-posting', 'telegram_posting_settings_page' , plugins_url('/images/icon.png', __FILE__));
    add_submenu_page('telegram-posting', __('Telegram posting > Manage settings'), __('Settings'), 'manage_options', 'telegram-posting', 'telegram_posting_settings_page');
    
    add_action('admin_init', 'register_telegram_posting_settings');
}

function register_telegram_posting_settings()
{
    register_setting('telegram-posting-settings-group', 'telegram-posting-settings');
}

function telegram_posting_settings_page()
{
?>
<div class="wrap">
<h1><?php _e('Telegram settings');?></h1>

<div class="updated notice notice-success is-dismissible below-h2" id="message">
    <p>
        <ol>
            <li>Create a channel (if you don't already have one).</li>
            <li>Create a bot (if you don't already have one).</li>
            <li>Go to channel options and select 'Administrator' option.</li>
            <li>Select 'Add Administrator' option.</li>
            <li>Search the username of your bot and add it as administrator.</li>
            <li>Copy the bot token (you got it in step two) and paste it in the below field.</li>
            <li>Enter the username of the channel and hit SAVE button!!!</li>
        </ol>
    </p>
    <button class="notice-dismiss" type="button"><span class="screen-reader-text"><?php _e('Close');?></span></button>
</div>


<form method="post" action="options.php">
    <?php 
        settings_fields('telegram-posting-settings-group');
        do_settings_sections('telegram-posting-settings-group'); 
        $settings = get_option('telegram-posting-settings'); 
    ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('bot_token');?></th>
        <td><input type="text" name="telegram-posting-settings[bot_token]" style="width:100%;" value="<?php echo esc_attr($settings['bot_token']); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('user_chat_id');?></th>
        <td><input type="text" name="telegram-posting-settings[user_chat_id]" value="<?php echo esc_attr($settings['user_chat_id']); ?>" /></td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php }
