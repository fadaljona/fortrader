<?php
if (!function_exists('add_action')) {
    $wp_root = '../../..';
    if (file_exists($wp_root . '/wp-load.php')) {
        require_once($wp_root . '/wp-load.php');
    } else {
        require_once($wp_root . '/wp-config.php');
    }
}
if (!current_user_can('manage_polls')) {
    die('Access Denied');
}

/* @var $ExPolls ExPolls */
$poll = $ExPolls->getPollByIdOrSlug(intval($_GET['expoll_id']));
if (isset($_GET['del'], $_GET['expoll_id'])) {
    if ( $poll ) $ExPolls->deletePoll($poll->ID);
?> 
    <script type="text/javascript">
        window.location = '<?php echo str_replace('\'', '&rsquo;', urldecode($_REQUEST['return_path']))?>'
    </script>
<?
    exit();
}


if ($poll && $poll->expired > 0) {
    $poll->expired_hour = date('H', $poll->expired);
    $poll->expired_min = date('i', $poll->expired);
}
?>
        <link rel="stylesheet" type="text/css" href="/wp-includes/js/jquery/ui/css/ui-lightness/jquery-ui-1.8.6.custom.css" media="all" />
<script type="text/javascript" src="/wp-includes/js/jquery/ui/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript">
    var a=new Array();
    var b=new Array();

    var single = new Array();
    var j = new Array();
    var h = new Array();

    single['а'] = 'a';
    single['б'] = 'b';
    single['в'] = 'v';
    single['г'] = 'g';
    single['д'] = 'd';
    single['е'] = 'e';
    single['з'] = 'z';
    single['и'] = 'i';
    single['к'] = 'k';
    single['л'] = 'l';
    single['м'] = 'm';
    single['н'] = 'n';
    single['о'] = 'o';
    single['п'] = 'p';
    single['р'] = 'r';
    single['с'] = 's';
    single['т'] = 't';
    single['у'] = 'u';
    single['ф'] = 'f';
    single['ц'] = 'c';
    single['ы'] = 'y';

    single['А'] = 'A';
    single['Б'] = 'B';
    single['В'] = 'V';
    single['Г'] = 'G';
    single['Д'] = 'D';
    single['Е'] = 'E';
    single['З'] = 'Z';
    single['И'] = 'I';
    single['К'] = 'K';
    single['Л'] = 'L';
    single['М'] = 'M';
    single['Н'] = 'N';
    single['О'] = 'O';
    single['П'] = 'P';
    single['Р'] = 'R';
    single['С'] = 'S';
    single['Т'] = 'T';
    single['У'] = 'U';
    single['Ф'] = 'F';
    single['Ц'] = 'C';
    single['Ы'] = 'Y';

    j['ё'] = 'o';
    j['й'] = 'j';
    j['ю'] = 'u';
    j['я'] = 'a';

    j['Ё'] = 'O';
    j['Й'] = 'J';
    j['Ю'] = 'U';
    j['Я'] = 'A';

    h['ж'] = 'z';
    h['х'] = 'k';
    h['ч'] = 'c';
    h['ш'] = 's';
    h['щ'] = 'sh';
    h['э'] = 'e';

    h['Ж'] = 'Z';
    h['Х'] = 'K';
    h['Ч'] = 'C';
    h['Ш'] = 'S';
    h['Щ'] = 'SH';
    h['Э'] = 'E';
    
    jQuery.fn.slugify = function(obj) {

        function findA(c)
        {
            for (i=0; i < a.length; i++)
                if (a[i] == c) return i;
            return -1;
        }

        function transliterateText(src)
        {
        
            var res = '';

            for (i=0; i < src.length; i++)
            {
                c = src.charAt(i);
                c1 = src.charAt(i+1);
                if (c == 'j' && j[c1] != null)
                {
                    res += j[c1];
                    i++;
                }
                else if (c1 == 'h')
                {
                    if (c == 's' && c1=='h' && src.charAt(i+2) == 'h')
                    {
                        res += h['sh'];
                        i += 2;
                    }
                    else
                    {
                        if (h[c] != null)
                            res += h[c];
                        else
                            res +=c;
                        i++;
                    }
                }
                else if (single[c] != null && single[c] != null)
                {
                    res += single[c];
                }
                else
                {
                    res += c;
                }
            }
            //alert(res);

            return res;
        }
        jQuery(this).data('obj', jQuery(obj));
        jQuery(this).keyup(function() {
            var obj = jQuery(this).data('obj');
            var slug = transliterateText(jQuery(this).val());
            slug = slug.replace(/\s+/g,'-').replace(/[^a-zA-Z0-9\-\_]/g,'').toLowerCase();
            jQuery(this).data('obj').val(slug);
        });
        jQuery(this).blur(function() {
            var obj = jQuery(this).data('obj');
            var slug = transliterateText(jQuery(this).val());
            slug = slug.replace(/\s+/g,'-').replace(/[^a-zA-Z0-9\-\_]/g,'').toLowerCase();
            jQuery(this).data('obj').val(slug);
        });

    }
    
    var $ = jQuery

    $(function() {
<?php if (isset($_REQUEST['return_path']) && $_REQUEST['return_path']): ?>
            var return_path = '<?php echo str_replace('\'', '&rsquo;', urlencode($_REQUEST['return_path'])); ?>'
<?php endif; ?>
            $.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
            $( "#expoll_title" ).slugify('#expoll_slug')
            $( "#expired" ).datepicker({
                showOn: "button",
                altFormat: 'd.m.yy',
                buttonImage: "/wp-includes/images/fugue_calendar.png",
                buttonImageOnly: true
            });

            $('#answer_add').click(function() {
                var trc = $('<tr>').appendTo('#poll_answers')
                var tdc = $('<td>').appendTo(trc)
                $('<input>').attr({type: 'button', 'class': 'button delete_answer', 'value': 'Удалить'}).appendTo(tdc)
                tdc = $('<td>').appendTo(trc)
                $('<input>').attr({type: 'text', 'size': 50, 'maxlength': 200, 'name': 'answers_new[]'}).appendTo(tdc)
                tdc = $('<td>').appendTo(trc)
            })

            $('.delete_answer').live('click', function() {
                var trc = $(this).parents('tr')
                if (trc.attr('id')) {
                    $.post('<?php echo admin_url('admin-ajax.php?action=expoll_remove_answer') ?>', {
                        'id': trc.attr('id').split('-')[2]
                    }, function(answer) {
                        if (answer.success) {
                            var votes = parseInt($(trc.children()[2]).children('strong').html())
                            var total_votes = parseInt($('#epoll_total_vote_count').html()) - votes
                            if (total_votes > 0) {
                                $('#epoll_total_vote_count').html(total_votes)
                            }
                            trc.remove()
                        } else {
                            show_notify(answer.error, 'error')
                        }
                    }, 'json')
                } else {
                    trc.remove()
                }
                window.trc = trc
            })

            $('#answers_save').click(function() {
                var dataObject = {
                    'poll_id': $('#expoll_id').val()
                }

                $('.expoll_answer').each(function(x,y) {
                    if ($(y).val()) {
                        dataObject['exists['+$(y).parents('tr').attr('id').split('-')[2]+']'] = $(y).val()
                    }
                })

                $('input[name="answers_new[]"]').each(function(x,y) {
                    if ($(y).val()) {
                        dataObject['new_answers['+x+']'] = $(y).val()
                    }
                })

                $('#answer_controls').hide()

                $.post('<?php echo admin_url('admin-ajax.php?action=expoll_save_answers') ?>', dataObject, function(answer) {
                    $('#poll_answers').html('')
                    var total_votes = 0
                    for (var i = 0; i < answer.answers.length; i++) {
                        var trc = $('<tr>').attr({'id': 'poll-answer-'+answer.answers[i].ID}).appendTo('#poll_answers')
                        var tdc = $('<td>').appendTo(trc)

                        $('<input>').attr({type: 'button', 'class': 'button delete_answer', 'value': 'Удалить'}).appendTo(tdc)
                        tdc = $('<td>').appendTo(trc)
                        $('<input>').attr({
                            type: 'text', 'size': 50, 'maxlength': 200,
                            'name': 'answer['+answer.answers[i].ID+']',
                            'value': answer.answers[i].name, 'class':'expoll_answer'
                        }).appendTo(tdc)
                        tdc = $('<td>').appendTo(trc)
                        $('<strong>').text(parseInt(answer.answers[i].vote_count)).appendTo(tdc)
                        total_votes += parseInt(answer.answers[i].vote_count)
                    }
                    $('#epoll_total_vote_count').html(total_votes)
                }, 'json')

                $('#answer_controls').show()
            })

            function show_notify(textValue, className) {
                $('#message').show()
                $('#message').text(textValue)
                $('#message').attr('class', className)
                setTimeout("$('#message').hide()", 5000)
            }

            function save_expoll() {
                var dataObject = {
                    'ID': 0,
                    'title': $('#expoll_title').val(),
                    'slug': $('#expoll_slug').val(),
                    'desc': $('#expoll_desc').val(),
                    'quest': $('#expoll_quest').val(),
					'seo_title': $('#expoll_seo_title').val(),
					'seo_description': $('#expoll_seo_description').val(),
                    'multi': $('#expoll_multi').val(),
                    'state': $('#expoll_state').val()
                }

                if ($('#expoll_id').val()) {
                    dataObject.ID = $('#expoll_id').val()
                } else {
                    dataObject.state = 2
                }

                if ($('#expired').val().split('.').length == 3) {
                    var date = $('#expired').val().split('.')
                    var unix = Date.UTC(date[2], date[1], date[0], $('#expired_hour').val(), $('#expired_min').val(), 0, 0)
                    dataObject.expired = parseInt(unix/1000)
                } else {
                    dataObject.expired = -1
                }

                $('#poll_controls').hide()

                $.post('<?php echo admin_url('admin-ajax.php?action=expoll_save_poll') ?>', dataObject, function(answer) {
                    if (answer.error) {
                        show_notify(answer.error, 'error')
                    } else if (answer.new_poll) {
                        window.location = '<?php echo $_SERVER['REQUEST_URI'] ?>&expoll_id='+answer.new_poll
                    } else {
                        show_notify('Изменения сохранены', 'updated')
                    }
                    $('#poll_controls').show()
                }, 'json')

            }

            $('input[name="do"]').click(function() {
                switch ($(this).attr('id')) {
                    case 'save_return':
                        save_expoll();
                        window.location = return_path
                        break;
                    case 'save':
                        save_expoll();
                        break;
                    case 'return':
                        if (window.confirm('Вы уверены, что хотите отменить изменения?')) {
                            window.location = return_path
                        }
                        break;
                    case 'cancel':
                        if (window.confirm('Вы уверены, что хотите отменить изменения?')) {
                            window.location = window.location.href
                        }
                        break;
                    default:
                        break;
                }
            })
        });
    </script>
    <style type="text/css" media="screen">
        #icon-wp-polls {
            background:url("<?php echo plugins_url() ?>/expolls/poll.png") no-repeat scroll 8px 8px transparent;
        }
        textarea {
            width: 90%;
        }
    </style>

    <div class="wrap">
        <input type="hidden" id="expoll_id" name="expoll_id" value="<?php echo ($poll) ? $poll->ID : '' ?>" />
        <div id="icon-wp-polls" class="icon32"><br /></div>
        <h2><?php echo ($poll) ? 'Редактировать опрос' : 'Добавить опрос'; ?></h2>
        <!-- Poll Question -->
        <h3>Информация</h3>
        <table class="form-table">
            <tr>
                <th width="30%" scope="row" valign="top"><label for="expoll_title">Заголовок</label></th>
                <td width="70%"><input type="text" size="70" name="title" id="expoll_title" value="<?php echo ($poll) ? htmlspecialchars($poll->title) : ''; ?>" /></td>
            </tr>
            <tr>
                <th width="30%" scope="row" valign="top"><label for="expoll_slug">Url</label></th>
                <td width="70%"><input type="text" size="70" name="slug" id="expoll_slug" value="<?php echo ($poll) ? $poll->slug : ''; ?>" /></td>
            </tr>
            <tr>
                <th width="30%" scope="row" valign="top">Описание (если отсутствует заменяется на вопрос)</th>
                <td width="70%"><textarea id="expoll_desc" name="desc"><?php echo ($poll) ? htmlspecialchars($poll->desc) : ''; ?></textarea></td>
            </tr>
            <tr>
                <th width="30%" scope="row" valign="top">Вопрос (если отсутствует заменяется на описание)</th>
                <td width="70%"><textarea id="expoll_quest" name="quest"><?php echo ($poll) ? htmlspecialchars($poll->quest) : ''; ?></textarea></td>
            </tr>
			
			<tr>
                <th width="30%" scope="row" valign="top">Тайтл</th>
                <td width="70%"><textarea id="expoll_seo_title" name="seo_title"><?php echo ($poll) ? htmlspecialchars($poll->seo_title) : ''; ?></textarea></td>
            </tr>
			
			<tr>
                <th width="30%" scope="row" valign="top">Description</th>
                <td width="70%"><textarea id="expoll_seo_description" name="seo_title"><?php echo ($poll) ? htmlspecialchars($poll->seo_description) : ''; ?></textarea></td>
            </tr>
			
            <tr>
                <th width="30%" scope="row" valign="top">Разрешить выбор больше одного варианта ответа</th>
                <td width="70%">
                    <select name="pollq_multiple_yes" id="expoll_multi" size="1">
                        <option value="0" <?php echo!$poll->multi ? 'selected="selected"' : ''; ?>>Нет</option>
                        <option value="1" <?php echo $poll->multi ? 'selected="selected"' : ''; ?>>Да</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th width="30%" scope="row" valign="top">Статус</th>
                <td width="70%">
                <?php if ($poll): ?>
                    <select name="pollq_multiple_yes" id="expoll_state" size="1">
                        <option value="0" <?php echo!$poll->state ? 'selected="selected"' : ''; ?>>Закрыт</option>
                        <option value="1" <?php echo $poll->state == 1 ? 'selected="selected"' : ''; ?>>Открыт</option>
                        <option value="2" <?php echo $poll->state == 2 ? 'selected="selected"' : ''; ?>>Скрыт</option>
                    </select>
                <?php else: ?>
                        Скрыт
                <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th width="30%" scope="row" valign="top">Истекает (оставьте поле пустым, если хотите сделать опрос бессрочным)</th>
                    <td width="70%">
                        <input type="text" id="expired" name="expired" value="<?php echo $poll->expired > 0 ? date('d.m.Y', $poll->expired) : 'бессрочно' ?>" />
                        <select name="expired_hour" id="expired_hour">
                    <?php for ($i = 0; $i < 24; $i++): ?>
                            <option value="<?php echo $i ?>" <?php echo $poll->expired_hour == $i ? 'selected="selected"' : ''; ?>><?php echo $i ?></option>
                    <?php endfor; ?>
                        </select>
                        :
                        <select name="expired_min" id="expired_min">
                    <?php for ($i = 0; $i < 60; $i++): ?>
                                <option value="<?php echo $i ?>" <?php echo $poll->expired_min == $i ? 'selected="selected"' : ''; ?>><?php echo $i ?></option>
                    <?php endfor; ?>
                            </select>

                        </td>
                    </tr>
                </table>
                <div id="message" class="updated" style="display: none;"></div>
                <p style="text-align: center;" id="poll_controls">
        <?php if ($poll): ?>
        <?php if (isset($_REQUEST['return_path']) && $_REQUEST['return_path']): ?>
                                        <input type="button" class="button" name="do" id="save_return" value="Сохранить и вернуться" />
        <?php endif; ?>
        <?php if (isset($_REQUEST['return_path']) && $_REQUEST['return_path']): ?>
                                            <input type="button" class="button" name="do" id="return" value="Отменить и вернуться" />
        <?php endif; ?>
        <?php endif; ?>
                                            <input type="button" name="do" id="save" value="Сохранить" class="button" />
                                            <input type="button" name="do" id="cancel" value="Отменить" class="button" />
                                        </p>

    <?php if ($poll): ?>
                                                <!-- Poll Answers -->
                                                <h3>Варианты ответа</h3>
                                                <table class="form-table">
                                                    <thead>
                                                        <tr>
                                                            <th width="20%" scope="row" valign="top"></th>
                                                            <th width="60%" scope="row" valign="top">Текст</th>
                                                            <th width="20%" scope="row" valign="top" >Кол-во голосов</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="poll_answers">
            <?php $answers = $ExPolls->getPollAnswers($poll->ID); ?>
            <?php foreach ($answers as $poll_answer): ?>
                                                    <tr id="poll-answer-<?php echo $poll_answer->ID ?>">
                                                        <td>
                                                            <input type="button" value="Удалить" class="button delete_answer" />
                                                        </td>
                                                        <td>
                                                            <input type="text" size="50" name="answer[<?php echo $poll_answer->ID ?>]" value="<?php echo htmlspecialchars($poll_answer->name) ?>" class="expoll_answer" />
                                                        </td>
                                                        <td>
                                                            <strong><?php echo (int) $poll_answer->vote_count ?></strong>
                                                        </td>
                                                    </tr>
            <?php endforeach; ?>
                                                </tbody>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="button" value="Добавить ответ" class="button" id="answer_add" /></td>
                                                        <td><input type="button" value="Сохранить список ответов" class="button" id="answers_save" /></td>
                                                        <td><strong id="epoll_total_vote_count"><?php echo $poll->vote_count ?></strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
    <?php endif; ?>
</div>
