<?php
/*
  Plugin Name: expolls
  Plugin URI: none
  Description: expolls
  Author: Krasu
  Version: 0.1
  Author URI: none
 */
if (!function_exists('add_action')) {
    $wp_root = '../../..';
    if (file_exists($wp_root . '/wp-load.php')) {
        require_once($wp_root . '/wp-load.php');
    } else {
        require_once($wp_root . '/wp-config.php');
    }
}

function expolls_rewrite_rules_array($existing_rules) {
    $url_prefix = 'polls';
    $new_rules = array(
        $url_prefix . '/([^/]+)\.html$' =>
        'index.php?pagename=' . $url_prefix . '&expoll_slug=$matches[1]',
        $url_prefix . '/([^/]+)\.html/page/([0-9]+)/?$' =>
        'index.php?pagename=' . $url_prefix . '&expoll_slug=$matches[1]&expoll_page=$matches[2]',
        $url_prefix . '/page/([0-9]+)/?$' =>
        'index.php?pagename=' . $url_prefix . '&expoll_page=$matches[1]',
    );
    //die(var_dump($new_rules + $existing_rules));
    return $new_rules + $existing_rules;
}

add_action( 'wp_loaded','my_flush_rules' );
function my_flush_rules(){
    $rules = get_option( 'rewrite_rules' );

	//if ( ! isset( $rules['polls/([^/]+)\.html$'] ) ) {
		global $wp_rewrite;
	   	$wp_rewrite->flush_rules();
	//}
}

if (!class_exists('ExPolls')) {

    class ExPolls {

        var $table_prefix = 'expolls_';
        var $url_prefix = 'polls'; //equal to page slug
        var $db;
        var $site_name;
        var $site_email;
        var $salt = 'sdmasdjakdjwlk34m2.,r k{)(*(&P(JAlkrmw4.kml 8Y(*&)(*&YOI';
        var $cookie_name = '078f608b8b3bf8309301dead7a5b61d6';

        function __construct() {
            global $wpdb;

            $this->site_name = $_SERVER['SERVER_NAME'];
            $this->site_email = 'subs@' . $_SERVER['SERVER_NAME'];
            $this->db = $wpdb;
        }

        function WP_addFilters() {
            add_filter('rewrite_rules_array', 'expolls_rewrite_rules_array');
            add_filter('query_vars', array(&$this, 'WP_insert_query_vars'));
            //add_filter('wp_title', array(&$this, 'WP_changeTitle'));
            add_action('template_redirect', array(&$this, 'WP_redirect'));
            add_action('admin_menu', array(&$this, 'WP_addAdminPage'));
            add_action('wp_ajax_expoll_save_poll', array(&$this, 'savePoll'));
            add_action('wp_ajax_expoll_save_answers', array(&$this, 'savePollAnswers'));
            add_action('wp_ajax_expoll_remove_answer', array(&$this, 'deleteAnswer'));
            add_action('wp_insert_comment', array(&$this, 'modifyComment'));
            add_action('comment_post', array(&$this, 'WP_checkCommentStatus'));
            add_action('wp_set_comment_status', array(&$this, 'WP_checkCommentStatus'));
        }

        function WP_checkCommentStatus($comment_id, $comment_status = null) {
            $data = get_commentdata($comment_id, 0, TRUE);
            $meta_data = get_comment_meta($comment_id, 'expoll_id', TRUE);
            if ($data['comment_approved'] == '1' && $meta_data) {
                $this->notifyComment($data, $meta_data);
            }
        }

        function notifyComment($comment_data, $poll_id) {
            $sql = '
                SELECT
                    u.user_login, u.user_email, u.display_name, um.user_id
                FROM ' . $this->db->users . ' as u , ' . $this->db->usermeta . ' as um
                WHERE um.user_id=u.ID AND um.meta_key="expoll_subsribe_' . (int) $poll_id.'"'
            ;

            $subscribers = $this->db->get_results($sql);
            $poll = $this->getPollByIdOrSlug($poll_id);

            if (is_array($subscribers) && !empty($subscribers)) {

                $message = file_get_contents(dirname(__FILE__) . '/email_template.html');
                $message = str_replace('{{post_url}}', 'http://' . $_SERVER['SERVER_NAME'] . '/' . $this->url_prefix . '/' . $poll->slug . '/', $message);
                $message = str_replace('{{comment_url}}', 'http://' . $_SERVER['SERVER_NAME'] . '/' . $this->url_prefix . '/' . $poll->slug . '/#comment-' . $comment_data['comment_ID'], $message);
                $message = str_replace('{{post_name}}', $poll->title, $message);
                $message = str_replace('{{author_name}}', $comment_data['comment_author'], $message);
                $message = str_replace('{{comment}}', $comment_data['comment_content'], $message);
                $subject = 'Новый комментарий к опросу "' . $poll->title . '"';

                foreach ($subscribers as $subscriber) {
                    if ($subscriber->user_email == $comment_data['comment_author_email'] || !is_email($subscriber->user_email)) {
                        continue;
                    }

                    $message_final = str_replace('{{unsubscribe_url}}',
                                    'http://' . $_SERVER['SERVER_NAME'] . '/'
                                    . '?expoll_unsubscribe=1&email=' . urlencode($subscriber->user_email)
                                    . '&poll=' . $poll_id
                                    . '&verify=' . $this->generate_key($subscriber->user_email)
                                    , $message);
                    $this->send_mail($subscriber->user_email, $subject, $message_final);
                }
            }
        }

        function subscribeToComments($poll_id) {
            get_current_user();
            global $current_user;

            if ($current_user->ID) {
                update_usermeta($current_user->ID, 'expoll_subsribe_' . $poll_id, 1);
            }
        }

        function unsubscribe($email, $poll_id, $verify) {
            if (!is_email($email))
                return;

            $sql = 'SELECT * FROM ' . $this->db->users . ' WHERE user_email="' . $email . '"';
            $user = $this->db->get_row($sql);
            if ($this->validate_key($verify, $email) && $user) {
                $this->db->get_results('DELETE FROM ' . $this->db->usermeta . ' WHERE user_id=' . $user->ID . ' AND meta_key="expoll_subsribe_' . (int) $poll_id . '"');
            }
        }

        function generate_key($email) {
            return md5(md5($this->salt . $data));
        }

        function validate_key($key, $email) {
            if ($key == $this->generate_key($email))
                return true;
        }

        function send_mail($to, $subject, $message) {
            $subject = '[' . get_bloginfo('name') . '] ' . $subject;

            // strip out some chars that might cause issues, and assemble vars
            $site_name = str_replace('"', "'", get_bloginfo('name'));
            $site_email = str_replace(array('<', '>'), array('', ''), $this->site_email);
            $charset = get_settings('blog_charset');

            $headers = "From: \"{$site_name}\" <{$site_email}>\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: text/html; charset=\"{$charset}\"\n";
            return wp_mail($to, $subject, $message, $headers);
        }

        function WP_addAdminPage() {
            if (function_exists('add_menu_page')) {
                add_menu_page('Опросы', 'Опросы', 'manage_polls', 'expolls/admin_page.php', '', plugins_url('expolls/poll.png'));
            }
            if (function_exists('add_submenu_page')) {
                add_submenu_page('expolls/admin_page.php', 'Добавить', 'Добавить', 'manage_polls', 'expolls/admin_edit_page.php');
            }
        }

        function savePoll() {
            $poll = new stdClass();
            $poll->ID = intval($_POST['ID']);
            $poll->title = html_entity_decode(trim($_POST['title']));
            $poll->desc = html_entity_decode(trim($_POST['desc']));
            $poll->quest = html_entity_decode(trim($_POST['quest']));
            $poll->multi = intval($_POST['multi']) ? 1 : 0;
            $poll->state = $poll->ID ? intval($_POST['state']) : 2;
            $poll->expired = intval($_POST['expired']) > 0 ? intval($_POST['expired']) : -1;
            $poll->slug = preg_replace('/[^a-z0-9\-_]+/', '', $_POST['slug']);
			$poll->seo_title = html_entity_decode(trim($_POST['seo_title']));
			$poll->seo_description = html_entity_decode(trim($_POST['seo_description']));

            if ($poll->state > 2 || $poll->state < 0) {
                $poll->state = 2;
            }

            if (!$poll->slug) {
                $return = array('error' => 'Вы не указали URL');
                exit(json_encode($return));
            }

            $exists = $this->db->get_var('SELECT ID FROM `' . $this->table_prefix . 'polls` WHERE slug="' . $poll->slug . '" LIMIT 1');
            if ($poll->ID && $exists == $poll->ID) {
                $exists = 0;
            }

            if ($exists) {
                $return = array('error' => 'Такой url уже существует в таблице');
                exit(json_encode($return));
            }

            if ($poll->ID) {
                $answers = $this->db->get_var('SELECT COUNT(`ID`) FROM `' . $this->table_prefix . 'answers` WHERE poll_id="' . $poll->ID . '"');
                $this->db->update($this->table_prefix . 'polls', array(
                    'ID' => $poll->ID,
                    'title' => $poll->title,
                    'desc' => $poll->desc,
                    'quest' => $poll->quest,
					'seo_title' => $poll->seo_title,
					'seo_description' => $poll->seo_description,
                    'slug' => $poll->slug,
                    'multi' => $poll->multi,
                    'state' => ($answers ? $poll->state : 2),
                    'expired' => $poll->expired,
                        ), array('ID' => $poll->ID));
            } else {
                $this->db->insert($this->table_prefix . 'polls', array(
                    'title' => $poll->title,
                    'slug' => $poll->slug,
                    'desc' => $poll->desc,
                    'quest' => $poll->quest,
					'seo_title' => $poll->seo_title,
					'seo_description' => $poll->seo_description,
                    'multi' => $poll->multi,
                    'state' => $poll->state,
                    'expired' => $poll->expired,
                ));
            }

            if ($this->db->last_error) {
                $return = array('error' => 'Ошибка БД');
            } elseif (!$poll->ID) {
                $return = array('new_poll' => $this->db->insert_id);
            } else {
                $return = array('success' => 1);
            }

            exit(json_encode($return));
        }

        function savePollAnswers() {
            $poll_id = intval($_POST['poll_id']);

            if (isset($_POST['new_answers']) && is_array($_POST['new_answers'])) {
                $insert_values = array();
                foreach ($_POST['new_answers'] as $answer) {
                    $insert_values[] = '(' . $poll_id . ',"' . $answer . '")';
                }

                if (!empty($insert_values)) {
                    $this->db->get_row('INSERT INTO `' . $this->table_prefix . 'answers` (poll_id, name) VALUES ' . implode(', ', $insert_values));
                }
            }

            if (isset($_POST['exists']) && is_array($_POST['exists'])) {
                foreach ($_POST['exists'] as $key => $answer) {
                    $this->db->update($this->table_prefix . 'answers', array(
                        'name' => $answer,
                            ), array('ID' => intval($key)));
                }
            }

            exit(json_encode(array('answers' => $this->db->get_results('SELECT * FROM `' . $this->table_prefix . 'answers` WHERE poll_id="' . $poll_id . '" ORDER BY `ID` ASC'))));
        }

        function deleteAnswer() {
            $id = intval($_POST['id']);
            $poll_data = $this->db->get_row('SELECT `poll_id`, `vote_count` FROM `' . $this->table_prefix . 'answers` WHERE `ID`="' . $id . '" LIMIT 1');

            $this->db->get_row('DELETE FROM `' . $this->table_prefix . 'answers` WHERE `ID`="' . $id . '" LIMIT 1');

            if ($this->db->last_error) {
                $return = array('error' => 'Ошибка БД');
            } else {
                $answers = $this->db->get_var('SELECT COUNT(`ID`) FROM `' . $this->table_prefix . 'answers` WHERE poll_id="' . $poll->ID . '"');
                $this->db->get_var('
                    UPDATE `' . $this->table_prefix . 'polls` SET
                        vote_count=vote_count-' . (int) $poll_data->vote_count . ' '
                        . ((!$answers) ? ', state = 2 ' : '' ) . '
                    WHERE `ID`="' . $poll_data->ID . '"
                            ');
                $return = array('success' => 1);
            }

            exit(json_encode($return));
        }

        function deletePoll($poll_id = null) {
            $id = intval($poll_id);
            $this->db->get_row('DELETE FROM `' . $this->table_prefix . 'polls` WHERE `ID`="' . $id . '" LIMIT 1');
            $this->db->get_row('DELETE FROM `' . $this->table_prefix . 'answers` WHERE `poll_id`="' . $id . '" LIMIT 1');
            $this->db->get_row('DELETE FROM `' . $this->table_prefix . 'votes` WHERE `poll_id`="' . $id . '" LIMIT 1');
        }

        function modifyComment($id, $comment = null) {
            if (isset($_REQUEST['expoll_add_comment'])) {
                $poll = $this->getPollByIdOrSlug($_REQUEST['expoll_add_comment']);

                if (isset($_REQUEST['expoll_comment_subscribe'])) {
                    $this->subscribeToComments($poll->ID);
                }

                add_comment_meta($id, 'expoll_id', $poll->ID);
            }
            return $location;
        }

        function getPollsStats($polls_array = array()) {
            $count = count($polls_array);

            for ($i = 0; $i < $count; $i++) {

                if ($polls_array[$i]->desc && !$polls_array[$i]->quest) {
                    $polls_array[$i]->quest = $polls_array[$i]->desc;
                } elseif ($polls_array[$i]->quest && !$polls_array[$i]->desc) {
                    $polls_array[$i]->desc = $polls_array[$i]->quest;
                }

                $unix = explode(' ', $polls_array[$i]->created);
                $unix[0] = explode('-', $unix[0]);
                $unix[1] = explode(':', $unix[1]);

                $polls_array[$i]->created = mktime(
                                $unix[1][0], $unix[1][1], $unix[1][2],
                                $unix[0][1], $unix[0][2], $unix[0][0]
                );
            }
            return $polls_array;
        }

        function getPolls($offset = 5, $limit = 10, $where = null, $order = 'created DESC') {
            $sql = "SELECT * FROM `" . $this->table_prefix . "polls` WHERE 1=1 ";
            
            if ($where) {
                $sql .= $where;
            }

            if ($order) {
                $sql .= " ORDER BY " . $order . " ";
            }

            if (intval($limit)) {
                if (intval($offset)) {
                    $sql .= "LIMIT " . intval($offset) . ", " . intval($limit);
                } else {
                    $sql .= "LIMIT " . intval($limit);
                }
            }
			//echo $sql;
//            echo ('<pre>');
//            var_dump($this->db->get_results($sql));
//            echo ('</pre>');
            return $this->getPollsStats($this->db->get_results($sql));
        }

        function getPollComments($offset = 5, $limit = 10, $where = null, $join = null, $count = null) {
            $post = query_posts('pagename=' . $this->url_prefix);
            if (!is_array($post) || empty($post)) {
                return;
            }

            $post = $post[0]->ID;

            $query = " FROM " . $this->db->comments . " c ";
            $query .= " LEFT JOIN " . $this->db->posts . " p ON c.comment_post_ID = p.ID ";
            $query .= $join;
            $query .= "  WHERE 1=1 ";
            $query .= " AND c.comment_approved = '1'";
            $query .= " AND c.comment_post_ID = '$post'";
            $query .= $where;

            $orderby = "GROUP BY c.comment_ID ORDER BY c.comment_date_gmt DESC LIMIT " . (int) $offset . ", " . (int) $limit;

            if (!$count) {
                $comments = $this->db->get_results("SELECT * $query $orderby");
                return $comments;
            } else {
                $total = $this->db->get_var("SELECT COUNT(c.comment_ID) $query");
                return $total;
            }
        }

        function getPollByIdOrSlug($id = null) {
            $poll = null;
            if (is_numeric($id)) {
                $key = 'expolls_poll_with_id_'.intval($id);
                $poll = isset($this->$key) ? $this->$key : null;
                if (!$poll) {
                    $poll = $this->getPolls(0, 1, 'AND `ID` = ' . intval($id));
                    $this->$key = $poll;
                }
            } elseif (trim($id)) {
                $key = 'expolls_poll_with_slug_' . trim($id);
                $poll = isset($this->$key) ? $this->$key : null;
                if (!$poll) {
                    $poll = $this->getPolls(0, 1, 'AND `slug` = "' . trim($id) . '"');
                    $this->$key = $poll;
                }
            }

            if (is_array($poll) && isset($poll[0]) && $poll[0]) {
                return $poll[0];
            } else {
                return FALSE;
            }
        }
		

        function getLastPoll() {
            $poll = $this->getPolls(0, 1, 'AND state = 1');

            if (is_array($poll) && isset($poll[0]) && $poll[0]) {
                $poll = $poll[0];
                $poll->answers = $this->getPollAnswers($poll->ID);
                $poll->answers_count = count($poll->answers);
                return $poll;
            } else {
                return FALSE;
            }
        }

        function getPollsCount($where = null) {
            $sql = "
            SELECT
                COUNT(`" . $this->table_prefix . "polls`.`ID`)
            FROM
                `" . $this->table_prefix . "polls` 
            WHERE
                1=1
            ";

            if ($where)
                $sql .= $where;

            return $this->db->get_var($sql);
        }

        function getPollAnswers($poll_id = null) {
            $poll = $this->getPollByIdOrSlug($poll_id);
            $sql = "SELECT * FROM `" . $this->table_prefix . "answers` WHERE poll_id=" . intval($poll_id) . " ORDER BY `ID` ASC";
            $answers = $this->db->get_results($sql);

            for ($i = 0; $i < count($answers); $i++) {
                if ($poll->vote_count && $answers[$i]->vote_count) {
                    $answers[$i]->percent = round((int) $answers[$i]->vote_count / ((int) $poll->vote_count * 0.01), 2);
                } else {
                    $answers[$i]->percent = 0;
                }
            }

            return $answers;
        }

        function getCheckSql($user_id = 0) {
            $sql = "SELECT poll_id FROM `" . $this->table_prefix . "votes` WHERE ";
            if (intval($user_id)) {
                $sql .= "(wp_user_id = '" . intval($user_id) . "'"." or ip = '" . $_SERVER['REMOTE_ADDR'] . "')";
            } else {
                $sql .= "ip = '" . $_SERVER['REMOTE_ADDR'] . "' AND wp_user_id = 0";
            }

            return $sql;
        }
		
		function getRandomPoll() {

		
            $poll = $this->getPolls(0, 1, '','rand()');
			$poll = $poll[0];
            $poll->answers = $this->getPollAnswers($poll->ID);
            $poll->answers_count = count($poll->answers);
			
            return $poll;

            
        }

        function getLastUnvotedPoll($user_id = 0) {

            $ids = $this->db->get_results($this->getCheckSql($user_id), ARRAY_N);
			

			
            if (!$ids || !is_array($ids)) {
                $ids = array(0);
            }
            else {
                $count_ids = count($ids);
                $tmp_ids = array();
                for ($i = 0; $i < $count_ids; $i++) {
                    $tmp_ids[] = $ids[$i][0];
                }
                $ids = $tmp_ids;
				$ids=array_unique($ids);
            }
			
			//print_r($ids);
			
            $poll = $this->getPolls(0, 1, ' AND `ID` NOT IN (' . implode(',', $ids) . ') AND state = 1 ');
			
			//print_r($poll);

            if (is_array($poll) && isset($poll[0]) && $poll[0]) {
                $poll = $poll[0];
                $poll->answers = $this->getPollAnswers($poll->ID);
                $poll->answers_count = count($poll->answers);
                return $poll;
            } else {
                return 0;
            }
            
        }

        function getAnswerById($id = null) {
            $poll = $this->getPollByIdOrSlug($poll_id);
            $sql = "SELECT * FROM `" . $this->table_prefix . "answers` WHERE `ID`=" . intval($id);
            return $this->db->get_row($sql);
        }

        function vote($poll_id = null, $answer_id = null) {
            $poll = $this->getPollByIdOrSlug($poll_id);
            if (!$poll || $poll->state == 0) {
                return 0;
            }

            if (is_array($answer_id) && !$poll->multi) {
                $answer_id = $answer_id[0];
            }

            if ($this->checkVoted($poll->ID)) {
                return -2;
            }

            get_current_user();
            global $current_user;

            if (is_array($answer_id)) {
                $answer_ids = $answer_id;
                $i = 0;

                foreach ($answer_ids as $answer_id) {
                    $answer = $this->getAnswerById($answer_id);
                    if (!$answer || $answer->poll_id != $poll->ID) {
                        continue;
                    }
					
					$host = @gethostbyaddr($_SERVER['REMOTE_ADDR']);
					if( !$host ) $host = '';

                    $this->db->insert($this->table_prefix . "votes", array(
                        'answer_id' => $answer->ID,
                        'poll_id' => $poll->ID,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'host' => $host,
                        'wp_user_id' => $current_user->ID,
                        'time' => time(),
                    ));

                    if ($this->db->insert_id) {
                        $this->db->get_results('UPDATE ' . $this->table_prefix . 'answers SET vote_count=vote_count+1 WHERE `ID`=' . $answer->ID);
                        $i++;
                    }
                }

                if ($i > 0) {
                    $this->db->get_results('UPDATE ' . $this->table_prefix . 'polls SET vote_count=vote_count+1 WHERE `ID`=' . $poll->ID);
                }
            } else {
                $answer = $this->getAnswerById($answer_id);
                if (!$answer || $answer->poll_id != $poll->ID) {
                    return -1;
                }
				
				$host = $_SERVER['REMOTE_HOST'];
				if( !$host ) $host = '';

                $this->db->insert($this->table_prefix . "votes", array(
                    'answer_id' => $answer->ID,
                    'poll_id' => $poll->ID,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'host' => $host,
                    'wp_user_id' => $current_user->ID,
                    'time' => time(),
                ));

                if ($this->db->insert_id) {
                    $this->db->get_results('UPDATE ' . $this->table_prefix . 'polls SET vote_count=vote_count+1 WHERE `ID`=' . $poll->ID);
                    $this->db->get_results('UPDATE ' . $this->table_prefix . 'answers SET vote_count=vote_count+1 WHERE `ID`=' . $answer->ID);
                }
            }

            $vote_cookie = setcookie($this->cookie_name . $poll->ID, $poll->ID, time() + 30000000, COOKIEPATH);

            return 1;
        }

        function WP_changeTitle($title = '') {
            $add = '';
			
            if (get_query_var('expoll_slug')) {
                $poll = $this->getPollByIdOrSlug(get_query_var('expoll_slug'));
                if ($poll) {
                    $add = $poll->title . ' &laquo;
                ';
                }
            }

            return $add . $title;
        }
		
		function before_header() {
			ob_start( array(&$this,'change_title_tag') );
		}

		function change_title_tag($head) {
			$without_poll=substr( $_SERVER['REQUEST_URI'],  7 );
			$poll = $this->getPollByIdOrSlug( substr($without_poll,0,-5) );
			$title='';
			$description='';
					
			if( $poll->seo_title ){
				$title=$poll->seo_title;
			}else{
				$title=$poll->title;
			}
					
			if( $poll->seo_description ){
				$description=$poll->seo_description;
			}else{
				$description=$poll->desc;
			}

			return preg_replace('/<title>[^<]*<\/title>/i', '<title>'.$title.'</title>'."\n\n".'<meta name="description" content="'.$description.'" />', $head);
		}	
					
		function after_header() {
			ob_end_flush();
		}
		

		function WP_seo_meta() {
		
			$without_poll=substr( $_SERVER['REQUEST_URI'],  7 );
			if( strpos('_'.$_SERVER['REQUEST_URI'], '/polls/') && !strpos( $without_poll, '/' ) ){
				add_action('template_redirect', array(&$this, 'before_header'), 0);
				add_action('wp_head', array(&$this, 'after_header'), 1000);
			}
        }

        function WP_getTemplate() {
            if (get_query_var('expoll_slug')) {
                $poll = $this->getPollByIdOrSlug(get_query_var('expoll_slug'));
                if ($poll) {
                    return 'poll-page';
                }
            }

            return 'main';
        }

        function WP_insert_query_vars($vars) {
            array_push($vars, 'expoll_slug');
            array_push($vars, 'expoll_page');
            return $vars;
        }

        function WP_redirect($vars) {
            if (get_query_var('expoll_slug')) {
                $poll = $this->getPollByIdOrSlug(get_query_var('expoll_slug'));
                if (!$poll || $poll->state == 2) {
                    header('location:/' . $this->url_prefix . '/');
                }
            }
        }

        function checkVoted($poll_id) {
//            return false;
            
            get_current_user();
            global $current_user;

            if (isset($_COOKIE[$this->cookie_name . $poll_id]))
                return TRUE;
			//echo 'gggg';
            $sql = $this->getCheckSql($current_user->ID) . " AND poll_id='" . $poll_id . "' LIMIT 1";
            
            return $this->db->get_var($sql);
        }

    }

    $ExPolls = new ExPolls();

    $ExPolls->WP_addFilters();
	$ExPolls->WP_seo_meta();

    if (isset($_GET['expoll_unsubscribe'], $_GET['verify'], $_GET['email']) && $ExPolls->validate_key($_GET['verify'], $_GET['email'])) {
        $_GET['email'] = urldecode($_GET['email']);
        $ExPolls->unsubscribe($_GET['email'], $_GET['poll'], $_GET['verify']);
        header('Location: http://' . $_SERVER['SERVER_NAME'] . '/');
        exit();
    }

    if (isset($_POST['expoll_vote'], $_POST['expoll_poll_id'])) {
        $expoll_vote_result = $ExPolls->vote($_POST['expoll_poll_id'], $_POST['expoll_vote']);
        if ($expoll_vote_result > 0) {
            $key = 'expolls_poll_with_id_' . $_POST['expoll_poll_id'];
            unset($ExPolls->$key);
            exit(json_encode(array('success' => 1,
                        'answers' => $ExPolls->getPollAnswers($_POST['expoll_poll_id']),
                        'poll' => $ExPolls->getPollByIdOrSlug($_POST['expoll_poll_id']),
                    )));
        } else {
            exit(json_encode(array('error' => 1,
									'poll_id'=>$_POST['expoll_poll_id'],
									'vote'=>$_POST['expoll_vote'],
									'error_no'=>$expoll_vote_result
			)));
        }
    }

    function expoll_comment_template3($comment, $args, $depth) {
        global $ExPolls;
        $GLOBALS['comment'] = $comment;
        echo '<div class="review"><div class="review2">'. $comment->comment_author .' &bull; ' . strftime('%d/%m &bull; %H:%M', $comment->comment_data) .'</div>' . comment_text() . '</div>';
    }
    
    function expoll_comment_template2($comment, $args, $depth) {
        global $ExPolls;
        $GLOBALS['comment'] = $comment;
?>

        <li>
            <a name="comment-<?php comment_ID(); ?>"></a>
    <?php if ($comment->comment_approved == '0') : ?>
            <em><?php _e('Your comment is awaiting moderation.') ?></em>
            <br />
    <?php endif; ?>
            <div id="comment-<?php comment_ID(); ?>" class="mycomment">
        <?php $authordata = get_comment_author(); ?>
        <?php $user = get_userdata($comment->user_id); ?>
        <?php if (!empty($comment->user_id)): ?>
                <a href="/profile/<?php echo $user->user_login ?>">
            <?php if (file_exists(ABSPATH . '/user_avatars/' . $user->ID . '.png')) : ?>
                    <img class="comment-author-avatar" src="/image.php/<?php echo $user->ID ?>.png?width=16&amp;height=16&amp;cropratio=1:1&amp;image=<?php echo '/user_avatars/' . $user->ID ?>.png" alt="" title="" />
            <?php else: ?>
                        <img class="comment-author-avatar" src="<?php bloginfo('template_directory'); ?>/images/man.png" alt="" title="" />
            <?php endif; ?>
                    </a>
                    <a href="/profile/<?php echo $user->user_login ?>"><strong><?php echo $user->display_name ?></strong></a>
        <?php else: ?>
                            <strong><?php echo $authordata ?></strong>
        <?php endif; ?>
                            , <?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?>,
                            <a href="<?php echo '/' . $ExPolls->url_prefix . '/' . $comment->slug . '/' ?>"><?php echo $comment->title ?></a>

                            <div><?php comment_text() ?></div>
                            <div class="reply">
            <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                        </div>
                    </div>
                </li>

<?php
                        }
}
?>
