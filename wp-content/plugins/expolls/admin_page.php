<?php
if (!function_exists('add_action')) {
    $wp_root = '../../..';
    if (file_exists($wp_root . '/wp-load.php')) {
        require_once($wp_root . '/wp-load.php');
    } else {
        require_once($wp_root . '/wp-config.php');
    }
}
if (!current_user_can('manage_polls')) {
    die('Access Denied');
}

if (isset($_POST['exp_poll'], $_POST['doaction'], $_POST['which_action']) && is_array($_POST['exp_poll'])) {
    foreach ($_POST['exp_poll'] as $key => $value) {
        $_POST['exp_poll'][$key] = intval($key);
    }

    switch ($_POST['which_action']) {
        case 'ch_state':
            $wpdb->get_row('UPDATE ' . $ExPolls->table_prefix . 'polls SET state=IF(state=1, 0, 1) WHERE `ID` IN (' . implode(',', $_POST['exp_poll']) . ') AND state IN (0,1)');
            break;
        case 'hide':
            $wpdb->get_row('UPDATE ' . $ExPolls->table_prefix . 'polls SET state=2 WHERE `ID` IN (' . implode(',', $_POST['exp_poll']) . ')');
            break;
        case 'delete':
            $wpdb->get_row('DELETE FROM ' . $ExPolls->table_prefix . 'polls WHERE `ID` IN (' . implode(',', $_POST['exp_poll']) . ')');
            $wpdb->get_row('DELETE FROM ' . $ExPolls->table_prefix . 'answers WHERE `poll_id` IN (' . implode(',', $_POST['exp_poll']) . ')');
            $wpdb->get_row('DELETE FROM ' . $ExPolls->table_prefix . 'votes WHERE `poll_id` IN (' . implode(',', $_POST['exp_poll']) . ')');
            break;

        default:
            break;
    }
}

/* @var $ExPolls ExPolls */
$where = '';
if (isset($_REQUEST['s']) && trim($_REQUEST['s']) != '') {
    $s = trim($_REQUEST['s']);
    $where .= ' AND (
        `' . $ExPolls->table_prefix . '.polls`.`title` LIKE "%' . $s . '%"
         OR `' . $ExPolls->table_prefix . '.polls`.`desc` LIKE "%' . $s . '%"
         OR `' . $ExPolls->table_prefix . '.polls`.`quest` LIKE "%' . $s . '%"
    )';
}

if (isset($_REQUEST['state'])) {
    switch ($_REQUEST['state']) {
        case 'open':
            $where .= ' AND (state = 1 AND expired < "' . time() . '")';
            break;
        case 'expired':
            $where .= ' AND expired >= "' . time() . '"';
            break;
        case 'closed':
            $where .= ' AND state = 0';
            break;
        case 'hidden':
            $where .= ' AND state = 2';
            break;

        default:
            break;
    }
}

$total_polls_count = $ExPolls->getPollsCount();
$now_polls_count = $ExPolls->getPollsCount($where);
$open_polls_count = $ExPolls->getPollsCount(' AND (state = 1 AND expired < "' . time() . '")');
$closed_polls_count = $ExPolls->getPollsCount(' AND state = 0');
$hidden_polls_count = $ExPolls->getPollsCount(' AND state = 2');
$expired_polls_count = $ExPolls->getPollsCount(' AND expired > "' . time() . '"');
$pages = get_pagination($now_polls_count, $_REQUEST['now_page'], 20);
$polls = $ExPolls->getPolls($pages['start'], 20, $where);
$polls_count = count($polls);
$expolls_page_url = admin_url('admin.php?page=expolls/admin_page.php');
$expolls_page_edit_url = admin_url('admin.php?page=expolls/admin_edit_page.php');
?>
<style type="text/css" media="screen">
    #icon-wp-polls {
        background:url("<?php echo plugins_url() ?>/expolls/poll.png") no-repeat scroll 8px 8px transparent;
    }
</style>
<div class="wrap">
    <div id="icon-wp-polls" class="icon32"></div>
    <h2>Управление опросами</h2>
    <form method="post" action="<?php echo str_replace('now_page=', '', $_SERVER["REQUEST_URI"]); ?>" id="posts-filter">
        <ul class="subsubsub">
            <li><a class="<?php if (!$_REQUEST['state'])
    echo 'current' ?>" href="<?php echo $expolls_page_url ?>">Все <span class="count">(<?php echo (int) $total_polls_count ?>)</span></a> |</li>
            <li><a class="<?php if ($_REQUEST['state'] == 'open')
                       echo 'current' ?>"  href="<?php echo $expolls_page_url ?>&state=open">Открытые <span class="count">(<?php echo (int) $open_polls_count ?>)</span></a> |</li>
                <li><a class="<?php if ($_REQUEST['state'] == 'closed')
                           echo 'current' ?>"  href="<?php echo $expolls_page_url ?>&state=closed">Закрытые <span class="count">(<?php echo (int) $closed_polls_count ?>)</span></a> |</li>
                    <li><a class="<?php if ($_REQUEST['state'] == 'expired')
                               echo 'current' ?>"  href="<?php echo $expolls_page_url ?>&state=expired">Истекшие <span class="count">(<?php echo (int) $expired_polls_count ?>)</span></a></li>
                        <li><a class="<?php if ($_REQUEST['state'] == 'hidden')
                                   echo 'current' ?>"  href="<?php echo $expolls_page_url ?>&state=hidden">Скрытые <span class="count">(<?php echo (int) $hidden_polls_count ?>)</span></a></li>
                        </ul>
                        <p class="search-box">
                            <label for="page-search-input" class="screen-reader-text">Найти сайт:</label>
                            <input type="text" value="<?php echo isset($_REQUEST['s']) ? $_REQUEST['s'] : '' ?>" name="s" id="page-search-input">
                            <input type="submit" class="button" value="Найти">
                        </p>
                    </form>
                    <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" id="posts-filter">
                        <div class="tablenav">
                            <div class="alignleft actions">
                                <select name="which_action">
                                    <option value="" selected="selected"><?php _e('Bulk Actions'); ?></option>
                                    <option value="ch_state">Открыть/Закрыть</option>
                                    <option value="hide">Скрыть</option>
                                    <option value="delete"><?php _e('Delete Permanently'); ?></option>
                                </select>
                                <input type="submit" value="<?php esc_attr_e('Apply'); ?>" name="doaction" id="doaction" class="button-secondary action" />
                            </div>
                            <div class="tablenav-pages">
                <?php if ($now_polls_count > 20): ?>
                                       <span class="displaying-num">Показано <?php echo $pages['start'] + 1 ?>&ndash;<?php echo $pages['start'] + 20 ?> из <?php echo $now_polls_count ?></span>
                <?php if ($pages['prev']): ?>
                                           <a href="<?php echo $expolls_page_url ?>&now_page=<?php echo $pages['prev'] ?>"  class="prev page-numbers">«</a>
                <?php endif; ?>
                <?php foreach ($pages['pages'] as $i): ?>
                <?php if ($i['url']): ?>
                                                   <a href="<?php echo $expolls_page_url ?>&now_page=<?php echo $i['num'] ?>" class="page-numbers"><?php echo $i['num'] ?></a>
                <?php else: ?>
                                                       <span class="page-numbers current"><?php echo $i['num'] ?></span>
                <?php endif; ?>
                <?php endforeach; ?>
                <?php if ($pages['next']): ?>
                                                           <a href="<?php echo $expolls_page_url ?>&now_page=<?php echo $pages['next'] ?>"  class="next page-numbers">»</a>
                <?php endif; ?>
                <?php endif; ?>

                                                           <br class="clear">
                                                       </div>
                                                   </div>
                                                   <table cellspacing="0" class="widefat fixed">
                                                       <thead>
                                                           <tr>
                                                               <th style="width: 1%"></th>
                                                               <th>Вопрос</th>
                                                               <th style="width: 7%">Голосов</th>
                                                               <th style="width: 10%">Начат</th>
                                                               <th style="width: 10%">Истекает</th>
                                                               <th style="width: 7%">Статус</th>
                                                               <th style="width: 3%"></th>
                                                               <th style="width: 3%"></th>
                                                           </tr>
                                                       </thead>
            <?php if ($polls_count): ?>
            <?php for ($i = 0; $i < $polls_count; $i++): ?>
                                                                   <tr>
                                                                       <td align="center">
                                                                           <input type="checkbox" id="exp_poll[<?php echo $polls[$i]->ID ?>]" name="exp_poll[<?php echo $polls[$i]->ID ?>]" value="1" />
                                                                       </td>
                                                                       <td>
                    <?php echo $polls[$i]->title ?>
                                                               </td>
                                                               <td align="center">
                    <?php echo (int) $polls[$i]->vote_count ?>
                                                               </td>
                                                               <td>
                    <?php echo date('d.m.Y', $polls[$i]->created_unix) ?>
                                                               </td>
                                                               <td align="center">
                    <?php echo ($polls[$i]->expired > 0) ? date('d.m.Y', $polls[$i]->expired) : '&mdash;' ?>
                                                               </td>
                                                               <td>
                    <?php if ($polls[$i]->state == 1)
                                                                       echo 'Открыт'; elseif ($polls[$i]->state == 2)
                                                                       echo 'Скрыт'; else
                                                                       echo 'Закрыт' ?>
                                                                   </td>
                                                                   <td align="center">
                                                                       <a href="<?php echo $expolls_page_edit_url ?>&expoll_id=<?php echo $polls[$i]->ID ?>&return_path=<?php echo urldecode($_SERVER['REQUEST_URI']) ?>">
                                                                           <img src="<?php bloginfo('template_directory') ?>/images/fugue_edit.png" alt="Редактировать" title="Редактировать" />
                                                                       </a>
                                                                   </td>
                                                                   <td align="center">
                                                                       <a href="<?php echo $expolls_page_edit_url ?>&del=1&expoll_id=<?php echo $polls[$i]->ID ?>&return_path=<?php echo urldecode($_SERVER['REQUEST_URI']) ?>">
                                                                           <img src="<?php bloginfo('template_directory') ?>/images/fugue_del.png" alt="Удалить" title="Удалить" />
                                                                       </a>
                                                                   </td>
                                                               </tr>
<?php endfor; ?>
<?php endif; ?>
                                                                   </table>

                                                                   <div class="tablenav">
                                                                       <div class="tablenav-pages">
<?php if ($now_polls_count > 20): ?>
                                                                           <span class="displaying-num">Показано <?php echo $pages['start'] + 1 ?>&ndash;<?php echo $pages['start'] + 20 ?> из <?php echo $now_polls_count ?></span>
                <?php if ($pages['prev']): ?>
                                                                               <a href="<?php echo $expolls_page_url ?>&now_page=<?php echo $page['prev'] ?>"  class="prev page-numbers">«</a>
<?php endif; ?>
                <?php foreach ($pages['pages'] as $i): ?>
<?php if ($i['url']): ?>
                                                                                       <a href="<?php echo $expolls_page_url ?>&now_page=<?php echo $i['num'] ?>" class="page-numbers"><?php echo $i['num'] ?></a>
                <?php else: ?>
                                                                                           <span class="page-numbers current"><?php echo $i['num'] ?></span>
<?php endif; ?>
                <?php endforeach; ?>
                <?php if ($pages['next']): ?>
                                                                                               <a href="<?php echo $expolls_page_url ?>&now_page=<?php echo $pages['next'] ?>"  class="next page-numbers">»</a>
<?php endif; ?>
<?php endif; ?>

                                                                                               <br class="clear">
                                                                                           </div>
                                                                                           <div class="alignleft actions">
                                                                                               <select name="which_action">
                                                                                                   <option value="" selected="selected"><?php _e('Bulk Actions'); ?></option>
                                                                                                   <option value="ch_state">Открыть/Закрыть</option>
                                                                                                   <option value="hide">Скрыть</option>
                                                                                                   <option value="delete"><?php _e('Delete Permanently'); ?></option>
                                                                                               </select>
                                                                                               <input type="submit" value="<?php esc_attr_e('Apply'); ?>" name="doaction" id="doaction" class="button-secondary action" />
            </div>
        </div>
    </form>
</div>
