=== Disable new user email notifications ===
Contributors: Weston Deboer
Author URI: http://www.westondeboer.com
Tags: register, disable, email, notification, new user
Requires at least: 2.1
Tested up to: 2.9.2
Stable tag: trunk

This plugin will disable new user email notifications that get sent to the admin email.

== Description ==

This plugin will disable new user email notifications that get sent to the admin email.

It will not work with register plus if that is enabled.

[Plugin URI]: (http://www.tipsforwordpress.com)

Features:
--------
 * Disables new user email notifications that get sent to the admin account

== Installation ==

1. Upload the `disable-new-user-notifications` folder to the `/wp-content/plugins/` directory, or download through the `Plugins` menu in WordPress

2. Activate the plugin through the `Plugins` menu in WordPress. 

3. There are no options or settings, it is all done automatically.

== Changelog ==

= 1.0 =
- (13 March 2010) Initial Release