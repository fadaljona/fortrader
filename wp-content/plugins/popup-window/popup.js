function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return null;
}

jQuery(document).ready(function() {
	var wWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
	
	if( getCookie('popup_displayed') != null ){
		createCookie('popup_displayed', 'true', 7);
	}
	
	if( getCookie('popup_right_bottom_displayed') != null ){
		createCookie('popup_right_bottom_displayed', 'true', 30);
	}
	
	if( wWidth > 800 && getCookie('popup_right_bottom_displayed') == null ){
		var hideRightPopupBlock = 0;
		if( 700 > parseInt(jQuery(document).height() - jQuery(window).height() - jQuery(this).scrollTop()) ){
			jQuery('.right-footer-popup-block').animate({
				bottom: '10px'
			}, 300, function() {});
			hideRightPopupBlock = 1;
		}else{
			jQuery('.right-footer-popup-block').animate({
				bottom: '-360px'
			}, 300, function() {});
			hideRightPopupBlock = 0;
		}
		
		jQuery(window).scroll(function(){

			if( 700 > parseInt(jQuery(document).height() - jQuery(window).height() - jQuery(this).scrollTop()) ){
				if( hideRightPopupBlock == 0 ){
					jQuery('.right-footer-popup-block').animate({
						bottom: '10px'
					}, 300, function() {});
				}
				hideRightPopupBlock = 1;
			}else{
				if( hideRightPopupBlock == 1 ){
					jQuery('.right-footer-popup-block').animate({
						bottom: '-360px'
					}, 300, function() {});
				}
				
				hideRightPopupBlock = 0;
			}
		});
	}
})