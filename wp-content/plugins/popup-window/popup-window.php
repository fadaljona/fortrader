<?php
/*
Plugin Name: Popup window
Plugin URI: mailto:4eladi@gmail.com
Description: Adds popup window with specified html code
Version: 1.0
Author: Anton Orlov
Author URI: mailto:4eladi@gmail.com
*/

	
function ppw_install()
{
	add_option('ppw_delay', '5', 'Delay before popup-window will be displayed, seconds');
	add_option('ppw_disable', '0', 'Disable Popup Window');
	add_option('ppw_close_time', '10', 'Delay before popup-window will be closed, seconds');
}

function ppw_uninstall()
{
	delete_option('ppw_delay');
	delete_option('ppw_disable');
	delete_option('ppw_close_time');
}

function ppw_head()
{
	$close_time = get_option('ppw_close_time')+get_option('ppw_delay');
	if (is_single()) echo "<style type='text/css' media='screen'>

/* Dialog
---------------------------------------- */
.right-footer-popup-block{
	position:fixed;
	bottom:-360px;
	right:10px;
}
.right-footer-popup-block .dialog-social{
	width:375px;
}
.right-footer-popup-block .fb-like{
    height: 25px;
    overflow: hidden;
}
.right-footer-popup-block .dialog-social-widgets{
	height:55px;
}
.dialog-social {
   /* position: fixed;
    display: none;*/
    background: #fff;
    z-index: 500;
    width: 445px;
}

.dialog-social-center {
    left: 50%;
    top: 50%;
}

.dialog-social-bg {
    position: fixed;
    display: none;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, .3);
    z-index: 450;
}

.dialog-social-header {
    background: #3a5897;
}

.dialog-social-title {
    font-size: 18px;
    line-height: 1.3em;
    padding-bottom: 20px;
	position:relative;
}

.dialog-social-lnk {
    padding: 12px 0 10px 22px;
    display: inline-block;
}
.d-close {position: absolute; width: 22px; height: 22px; padding: 10px; right: 18px; top: 21px; cursor: pointer;}
.dialog-social .d-close {
    top: 7px;
    background: url(/wp-content/plugins/popup-window/dialog-img/i.png) no-repeat -22px 0;
    padding: 0;
    width: 42px;
    height: 42px;
    right: 8px;
}

.dialog-social-widgets {
    padding-bottom: 0;
    height: 90px;
    overflow: visible;
    margin-top: 15px;
}

.dialog-social-auth {
    padding: 12px 12px 20px 12px;
    text-align: center;
}

.dialog-social-auth-link {
    border-bottom: 1px dashed #000;
    text-decoration: none;
    font-size: 12px;
	color:#000;
}

/*~~~~~~~ FB ~~~~~~~*/
.dialog-social-fb {
    /*-moz-transform: translate(-220px, -290px);
    -o-transform: translate(-220px, -290px);
    -ms-transform: translate(-220px, -290px);
    -webkit-transform: translate(-220px, -290px);
    transform: translate(-220px, -290px);*/
}

.dialog-social-fb .dialog-social-title {
    background: url(/wp-content/plugins/popup-window/dialog-img/fb-dialog-bg.png) no-repeat 0 100%;
    text-align: left;
    color: #fff;
}

.dialog-social-fb .dialog-social-widgets {
    padding-left: 25px;
}

.dialog-social-fb .dialog-social-auth {
    background: #dededd;
}

.dialog-social-fb .dialog-social-message-block-title {
    background-image: none;
    padding-bottom: 0;
}


.dialog-social-message-block-text {
    border-top: solid 1px #9daccb;
    padding: 22px 66px 22px 22px;
}	
	
#parent-popup {
	position: fixed;
	background: rgba(0,0,0,0.52);
	height: 100%;
	width: 100%;
	z-index: 9900;
	top: 0;
	left: 0;
}
#popup {
	position: fixed;
	background-color: #ffffff;
	width: 445px;
	height:324px;
	top: 50%;
	margin-top: -250px;
	left: 50%;
	margin-left: -225px;
	/*color: #333333;
	font-family: 'Lora';
	font-weight: bold;
	font-style: normal;
	font-size: 17px;*/
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
}
#like-text {
	border-bottom: 2px solid #cccccc; 
	margin: 12px 15px 5px 15px; 
	padding-bottom: 10px;
	line-height: 1.2em;
}
#close-text {
	cursor: pointer; 
	border-top: 2px solid #cccccc; 
	margin: 5px 15px 12px 15px;
	padding-top: 10px;
	font-size: 15px;
	font-weight: normal;
	font-style: normal;
}
#close-text:hover {
	text-decoration: underline;
}</style>

	<script type=\"text/javascript\">
/*	
		

		function closePopup() {
			document.getElementById('parent-popup').style.display = 'none';
		}
		
		var popupDisplayed = false;
		
		jQuery(document).ready(function() {
			
			var wWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
			
			if( getCookie('popup_services_displayed') != null && getCookie('popupNewsShown') != null ){
				var popupServicesArr = self.getCookie('popup_services_displayed').split('|');
				var d = new Date();
				
				var timeToshow = Math.floor(parseFloat( popupServicesArr[0] ) / 3 * 2);

				if ( getCookie('popup_displayed') == null && ( parseInt( d.getTime() ) - parseInt( popupServicesArr[1] ) ) > timeToshow * 60 * 1000 && wWidth > 800 ) {
					setTimeout(function(){
						document.getElementById('parent-popup').style.display = 'block';
						createCookie('popup_displayed', 'true', 7);
					}," . get_option('ppw_delay') * 1000 . ");
					popupDisplayed = true;
				}
			}
			
			
			setTimeout(function(){
				closePopup();
			}," . $close_time * 1000 . ");
			
			
			jQuery(document).click( function(event){
				if( jQuery(event.target).closest('#popup').length ) return;
				jQuery('#parent-popup').css({display:'none'});
				event.stopPropagation();
			});
		
		});*/
	</script>\n";
}

function ppw_footer()
{
	if (is_single()){
		wp_enqueue_script('facebook.jssdk', 'http://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5', array('jquery'), null );
		wp_enqueue_script('popup-window.js', '/wp-content/plugins/popup-window/popup.js', array('jquery', 'facebook.jssdk'));
		
		echo file_get_contents(dirname(__FILE__) . '/code.txt');
		echo file_get_contents(dirname(__FILE__) . '/code2.txt');
	} 
}
	
function admin_ppw_options()
{ ?><div class="wrap"><h2>Popup window options</h2>
<?php
	if ($_REQUEST['submit']) {
		update_ppw_options();
	}
	print_ppw_form();
?>		
</div>
<?php }
	
function update_ppw_options()
{
	$updated = false;

	if ( isset($_REQUEST['ppw_delay']) && ( $_REQUEST['ppw_delay'] < $_REQUEST['ppw_close_time'] ) ) {
		update_option('ppw_delay', stripcslashes($_REQUEST['ppw_delay']));
		update_option('ppw_disable', stripcslashes($_REQUEST['ppw_disable']));
		update_option('ppw_close_time', stripcslashes($_REQUEST['ppw_close_time']));
		$updated = true;
	}
		
	if ($updated) { ?>
	<div id="message" class="updated fade">
		<p>Options saved</p>
	</div>
<?php } else { ?>
	<div id="message" class="error fade">
		<p>Nothing to save</p>
	</div>
<?php }
}
	
function print_ppw_form()
{
	$ppw_delay = get_option('ppw_delay');
	$ppw_close_time = get_option('ppw_close_time');
	$ppw_disable = get_option('ppw_disable');
	if( $ppw_disable )
		$disable_checked = 'checked';
	else
		$disable_checked = '';
?>
		<form method="post">
		<table border="0" cellspacing="3px">
			<tr>
				<td><label for="ppw_delay">Delay before popup-window will be displayed, seconds:</label></td>  
				<td><input type="text" size="10" name="ppw_delay" value="<?php echo $ppw_delay; ?>"></td>
			</tr>
			<tr>
				<td><label for="ppw_disable">Disable Popup Window:</label></td>  
				<td><input type="checkbox" name="ppw_disable" value='1' <?php echo $disable_checked; ?>></td>
			</tr>
			<tr>
				<td><label for="ppw_close_time">Delay before popup-window will be closed, seconds:</label></td>  
				<td><input type="text" size="10" name="ppw_close_time" value="<?php echo $ppw_close_time; ?>"></td>
			</tr>
		</table>
		<br />
		<input type="submit" class="button-primary" name="submit" value="Save">
		</form> 
<?php
}
	
function ppw_menu()
{
	add_options_page(
		'Popup window',	// page title
		'Popup window',	// sub-menu title
		'manage_options',		// access/capability
		__FILE__,				// file
		'admin_ppw_options'	// function
	);
}

add_action('admin_menu', 'ppw_menu');

$ppw_disable = get_option('ppw_disable');
if( !$ppw_disable ){
	add_action('wp_footer','ppw_footer');
	add_action('wp_head','ppw_head');
}

register_activation_hook(__FILE__, 'ppw_install');
register_deactivation_hook(__FILE__, 'ppw_uninstall');