<?php
/*
Plugin Name: Styles and scripts modifier
Description: fortrader.org to files.fortrader.org, version for nginx cache, minifier
Version: 1.0
Author: Vekshin Vladimir akadem87@gmail.com
*/

require_once 'vendor/autoload.php';

use MatthiasMullie\Minify;

define("STYLES_SCRIPTS_PLUGIN_DIR_URL", plugin_dir_url( __FILE__ ) );
define("STYLES_SCRIPTS_PLUGIN_DIR", str_replace('\\', '/', __DIR__ ) );

function changeDomainToFiles( $src, $handle ){
	$parsedSrc = parse_url($src);
	$path = $parsedSrc['path'];
	if( file_exists( $fileName = $_SERVER['DOCUMENT_ROOT'] . $path ) ){
		$mtime = filemtime( $fileName );
		
		$baseName = basename($fileName);
		$newBaseFileName = md5($fileName . '.time.' . $mtime) . '-' . $baseName;
		$newFileName = STYLES_SCRIPTS_PLUGIN_DIR . '/data/' . $newBaseFileName;
		
		if( !file_exists($newFileName) ){
			$fileExtension = strtolower(substr($baseName, strrpos($baseName, '.')+1));
			
			if( $fileExtension == 'css' )
				$minifier = new Minify\CSS($fileName);
			else
				$minifier = new Minify\JS($fileName);

			$minifier->minify($newFileName);
		}
		
		$src = STYLES_SCRIPTS_PLUGIN_DIR_URL . 'data/' . $newBaseFileName;
		
		if( strpos('http', $src) == 0 ){
			$src = str_replace( 'http://', '//', $src);
			$src = str_replace( 'https://', '//', $src);
		}
	}
	return str_replace( 'fortrader.org', 'files.fortrader.org', $src);
}
add_filter( 'style_loader_src', 'changeDomainToFiles', 10, 2 );
add_filter( 'script_loader_src', 'changeDomainToFiles', 10, 2 );