<?php
/*
Plugin Name: Деактиватор плагинов
Description: Деактивирует плагины
Version: 1
Author: Vladimir akadem87@gmail.com
*/

global $pluginsNotNeeded;
$pluginsNotNeeded = array();

if (!strpos('_'.$_SERVER['REQUEST_URI'], '/cryptocurrencies') &&
    !strpos('_'.$_SERVER['REQUEST_URI'], '/wp-admin')
) {
    $pluginsNotNeeded[] = 'chatbro/index.php';
}

if( strpos($_SERVER['REQUEST_URI'], '/wp-admin') !== false ){
	$pluginsNotNeeded[] = 'styles-scripts/styles-scripts.php';
}

if( 
	!strpos('_'.$_SERVER['REQUEST_URI'], '.html') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/wp-admin/') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/wp-comments-post.php') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/wp-cron.php') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/services/') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/quotes') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/brokersrating') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/binaryrating') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/currencyrates') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/journal') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/contests') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/contestMember') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/bankrates') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/news') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/currencyconverter') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/informers') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/economic-calendar') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/?p=') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/monitoring') && 
	!strpos('_'.$_SERVER['REQUEST_URI'],'/author')
){
	$pluginsNotNeeded[] = 'decomments/decomments.php';
}
if( !strpos('_'.$_SERVER['REQUEST_URI'], '.html') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-admin/') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-cron.php') ){
	$pluginsNotNeeded[] = 'wp-jquery-lightbox/wp-jquery-lightbox.php';
}
if( !strpos('_'.$_SERVER['REQUEST_URI'], '/learn/forex-test/') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-admin/') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-cron.php') ){
	$pluginsNotNeeded[] = 'wp-pro-quiz/wp-pro-quiz.php';
}
if( !strpos('_'.$_SERVER['REQUEST_URI'], '/forexcatalog') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-admin/') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-cron.php') ){
	$pluginsNotNeeded[] = 'forexcatalog/forexcatalog.php';
}
if( !strpos('_'.$_SERVER['REQUEST_URI'], '/bankrates') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-admin/') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-cron.php') ){
	$pluginsNotNeeded[] = 'percent-rates/percent-rates.php';
}
if( !strpos('_'.$_SERVER['REQUEST_URI'], '/siterating') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-admin/') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-cron.php') ){
	$pluginsNotNeeded[] = 'siterating/siterating.php';
}
if( !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-admin/') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-cron.php') ){
	$pluginsNotNeeded[] = 'original-texts-yandex-webmaster/index-ortext-yandex.php';
	$pluginsNotNeeded[] = 'copyscape-premium/copyscape.php';
	$pluginsNotNeeded[] = 'tinymce-advanced/tinymce-advanced.php';
	$pluginsNotNeeded[] = 'webmaster-yandex/webmaster-yandex.php';
	$pluginsNotNeeded[] = 'wp-optimize/wp-optimize.php';
	$pluginsNotNeeded[] = 'rustolat/rus-to-lat.php';
}
if( !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-login.php') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-admin/') && !strpos('_'.$_SERVER['REQUEST_URI'],'/wp-cron.php') ){
	$pluginsNotNeeded[] = 'bm-custom-login/bm-custom-login.php';
	$pluginsNotNeeded[] = 'google-apps-login/google_apps_login.php';
}

add_filter( 'option_active_plugins', 'deactivate_decomments_plugin' );

function deactivate_decomments_plugin( $plugins ) {

    global $pluginsNotNeeded;

	foreach ( $pluginsNotNeeded as $plugin ) {
		$key = array_search( $plugin, $plugins );
		if ( false !== $key ) {
			unset( $plugins[ $key ] );
		}
	}
	
	global $wpFromConsole;
	if( isset($wpFromConsole) && $wpFromConsole === true ){
		unset($plugins);
		$plugins = array('fortrader-settings/fortrader-settings.php');
	}
 
	return $plugins;
}