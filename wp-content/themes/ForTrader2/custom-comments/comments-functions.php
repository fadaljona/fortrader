<?php
require_once( 'inc/custom_wp_comment_query.php' );
require_once( 'inc/custom_comment_walker.php' );
require_once( 'inc/complaints_list_table.php' );
require_once( 'inc/best_comment_walker.php' );

function comment_custom_avatar($html, $mixed, $size, $default, $alt){

	if( ! stristr( strtolower( $html ), 'gravatar.com' ) )
	{
		return $html;
	}
	
	global $comment, $wpdb;
	
	if( ! $comment->user_id ){
		
		return '';
		
	}
	
	$sql="SELECT middlePath FROM ft_user_avatar WHERE idUser=".$comment->user_id;
	$avatar_src = $wpdb->get_var( $sql, 0, 0 );
	if( !$avatar_src ) {
		$wsl_html = '';
	}else{
		$wsl_html = '<img src="/services/' . $avatar_src . '" class="avatar avatar-services avatar-' . $size . ' photo" height="' . $size . '" width="' . $size . '" />';
	}
	
	
	return $wsl_html;
}


add_filter( 'get_avatar', 'comment_custom_avatar', 10, 5 );





function replace_reply_link_class($class){
    $class = str_replace('comment-reply-link', 'comment-reply-link coment_reply log_coment_reply_btn f_right', $class);
    return $class;
}

add_filter('comment_reply_link', 'replace_reply_link_class');




function rateComment() {

	if( isset( $_POST['data_commen_id'] ) ){
		
		$good_ratiing = 0;
		$bad_ratiing = 0;
		if( $_POST['data_rating'] > 0 ){

			$good_ratiing = saveVoteUp( $_POST['data_commen_id'] );
			
		}else{
			
			$bad_ratiing = saveVoteDown( $_POST['data_commen_id'] );
		}
		
	}
	
	$out_array = array(
			'good_ratiing' => $good_ratiing,
			'bad_ratiing' => $bad_ratiing
		);
		
	echo json_encode( $out_array );


	
	die();

}

add_action( 'wp_ajax_nopriv_rateComment', 'rateComment' );
add_action( 'wp_ajax_rateComment', 'rateComment' );




function createComplaint() {

	global $wpdb;

	
	$current_user = wp_get_current_user();
	
	if( checkUserCanComplaint( $_POST['data_commen_id'], $current_user->ID, $_SERVER['REMOTE_ADDR']  ) ){
	
		$sql="insert into ".$wpdb->prefix."comment_complaints (comment_id,user_id,user_ip) VALUES(".$_POST['data_commen_id'].",".$current_user->ID.",'".$_SERVER['REMOTE_ADDR']."')";
		$result = $wpdb->query($sql);
		if( $result ){
			$out_array = array(
				'created' => 1,
			);
		}else{
			$out_array = array(
				'created' => 0,
			);
		}
	}
	echo json_encode( $out_array );
	die();

}

add_action( 'wp_ajax_nopriv_createComplaint', 'createComplaint' );
add_action( 'wp_ajax_createComplaint', 'createComplaint' );


function createUnRegisteredComment() {

	parse_str($_POST['form'], $form);
	
	//print_r( $form );
	$user = wp_get_current_user();
	$comment_post_ID = isset($form['comment_post_ID']) ? (int) $form['comment_post_ID'] : 0;
	
	$comment_author       = ( isset($form['author']) )  ? trim(strip_tags($form['author'])) : null;
	$comment_author_email = '';//wp_slash( $user->user_email );
	$comment_author_url   = '';//wp_slash( $user->user_url );
	$comment_content      = ( isset($form['comment']) ) ? trim($form['comment']) : null;
	$comment_type = '';
	$comment_parent = isset($form['comment_parent']) ? absint($form['comment_parent']) : 0;
	
	$commentdata = compact('comment_post_ID', 'comment_author', 'comment_author_email', 'comment_author_url', 'comment_content', 'comment_type', 'comment_parent', 0);
	$comment_id = wp_new_comment( $commentdata );
	
	$redirect_to = ( isset($form['redirect_to']) )  ? trim(strip_tags($form['redirect_to'])) : null;
	
	if($comment_id)
		$out_array = array(
				'created' => 1,
			);
	
	if( ! strpos ( $_POST['comment_url'], 'comments=1') ){
		$out_array['redirect'] = 1;
		$out_array['redirectto'] = $redirect_to;
	}
	
	echo json_encode( $out_array );
	die();

}

add_action( 'wp_ajax_nopriv_createUnRegisteredComment', 'createUnRegisteredComment' );
add_action( 'wp_ajax_createUnRegisteredComment', 'createUnRegisteredComment' );


function saveVoteUp( $comment_id ){

	global $wpdb;

	$current_user = wp_get_current_user();
	
	$sql="SELECT good_vote_count FROM ".$wpdb->prefix."comment_votes WHERE comment_id=$comment_id";
	$good_vote_count = $wpdb->get_var( $sql, 0, 0 );

	if( checkUserCanVote( $comment_id, $current_user->ID, $_SERVER['REMOTE_ADDR']  ) ){
	
		$good_vote_count+=1;
		
		$sql="update ".$wpdb->prefix."comment_votes set good_vote_count=$good_vote_count WHERE comment_id=$comment_id";
		$result = $wpdb->query($sql);
		
		if( !$result ){
			$sql="insert into ".$wpdb->prefix."comment_votes (comment_id,good_vote_count,bad_vote_count) VALUES($comment_id,1,0)";
			$result = $wpdb->query($sql);
		}
		
		$sql="insert into ".$wpdb->prefix."comment_voted_users (comment_id,user_id,user_ip) VALUES($comment_id,$current_user->ID,'".$_SERVER['REMOTE_ADDR']."')";
		$result2 = $wpdb->query($sql);
		
		
	
	}
	
	return $good_vote_count;
}

function saveVoteDown( $comment_id ){

	global $wpdb;

	$current_user = wp_get_current_user();
	
	$sql="SELECT bad_vote_count FROM ".$wpdb->prefix."comment_votes WHERE comment_id=$comment_id";
	$bad_vote_count = $wpdb->get_var( $sql, 0, 0 );

	if( checkUserCanVote( $comment_id, $current_user->ID, $_SERVER['REMOTE_ADDR']  ) ){
	
		$bad_vote_count-=1;
		
		$sql="update ".$wpdb->prefix."comment_votes set bad_vote_count=$bad_vote_count WHERE comment_id=$comment_id";
		$result = $wpdb->query($sql);
		
		if( !$result ){
			$sql="insert into ".$wpdb->prefix."comment_votes (comment_id,good_vote_count,bad_vote_count) VALUES($comment_id,0,-1)";
			$result = $wpdb->query($sql);
		}
		
		$sql="insert into ".$wpdb->prefix."comment_voted_users (comment_id,user_id,user_ip) VALUES($comment_id,$current_user->ID,'".$_SERVER['REMOTE_ADDR']."')";
		$result2 = $wpdb->query($sql);
		
		
	
	}
	
	return $bad_vote_count;
}

function checkUserCanVote( $comment_id, $user_id, $user_ip  ){

	global $wpdb;
	
	if( $user_id > 0 ){
		
		$sql="SELECT comment_id FROM ".$wpdb->prefix."comment_voted_users WHERE comment_id=$comment_id and user_id=$user_id ;";
		$com_id = $wpdb->get_var( $sql, 0, 0 );
		if( $com_id )
			return false;
		else
			return true;
			
	}else{
	
		$sql="SELECT comment_id FROM ".$wpdb->prefix."comment_voted_users WHERE comment_id=$comment_id and user_id=0 and user_ip='$user_ip' ;";
		$com_id = $wpdb->get_var( $sql, 0, 0 );
		if( $com_id )
			return false;
		else
			return true;
		
	}
	
}

function checkUserCanComplaint( $comment_id, $user_id, $user_ip  ){

	global $wpdb;
	
	if( $user_id > 0 ){
		
		$sql="SELECT comment_id FROM ".$wpdb->prefix."comment_complaints WHERE comment_id=$comment_id and user_id=$user_id ;";
		$com_id = $wpdb->get_var( $sql, 0, 0 );
		if( $com_id )
			return false;
		else
			return true;
			
	}else{
	
		$sql="SELECT comment_id FROM ".$wpdb->prefix."comment_complaints WHERE comment_id=$comment_id and user_id=0 and user_ip='$user_ip' ;";
		$com_id = $wpdb->get_var( $sql, 0, 0 );
		if( $com_id )
			return false;
		else
			return true;
		
	}
	
}

add_action('admin_menu', 'comment_complaints_menu');

function comment_complaints_menu() {
	add_comments_page('Comment complaints', 'Complaints', 'manage_options', 'comment-complaints', 'comment_complaints_func');
}

function comment_complaints_func(){

	
	$wp_list_table = new Complaints_List_Table();

	if( isset( $_POST['commentids'] ) ){
		
		if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {

            $nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
            $action = 'bulk-' . $wp_list_table->_args['plural'];

            if ( ! wp_verify_nonce( $nonce, $action ) )
                wp_die( 'Nope! Security check failed!' );

        }

        $action = $wp_list_table->current_action();
		global $wpdb;

        switch ( $action ) {

            case 'trash':

				$sql = "update $wpdb->comments set comment_approved='trash' where comment_ID in (".implode(',',$_POST['commentids']).")";
				$wpdb->query($sql);
			   
                break;

            case 'remove_complaint':
               
				$sql = "delete from wp_comment_complaints where comment_id in (".implode(',',$_POST['commentids']).")";
				$wpdb->query($sql);

                break;

            default:
                // do nothing or something else
                return;
                break;
        }
	}

	
	$wp_list_table->prepare_items();

	echo '<form id="comments-form"  method="post">';
	$wp_list_table->display();
	echo '</form>';

}


function delete_custom_comment_data($comment_id) {
	
	global $wpdb;
	$sql = "delete from wp_comment_votes where comment_id=".$comment_id;
	$wpdb->query($sql);
	$sql = "delete from wp_comment_voted_users where comment_id=".$comment_id;
	$wpdb->query($sql);
	$sql = "delete from wp_comment_complaints where comment_id=".$comment_id;
	$wpdb->query($sql);

}
add_action( 'delete_comment', 'delete_custom_comment_data', 10, 4 );









