<?php 
		$order='ASC';
		$orderby='comments.comment_date';
		//$get_params='?comments=1';
		
		$sort_active_new='';
		$sort_active_old='current';
		$sort_active_best='';
		
		
		if( isset( $_GET['sort'] ) ){
			switch ($_GET['sort']) {
				case 0:
					$order='DESC';
					$sort_active_new='current';
					$sort_active_old='';
					$sort_active_best='';
					break;
				case 1:
					$order='ASC';
					$sort_active_new='';
					$sort_active_old='current';
					$sort_active_best='';
					break;
				case 2:
					$order='DESC';
					$orderby='comment_votes.good_vote_count';
					$sort_active_new='';
					$sort_active_old='';
					$sort_active_best='current';
					break;
			}
			$get_params.='?sort='.$_GET['sort'];
		}else{
			$get_params.='?sort=1';
		}
		
		$unregister = array(
			'user_id' => 0,
			'user__not_id' => 2,
			'post_id' => $post->ID,
			'orderby' => $orderby,
			'order' => $order,
		);

		$comment_query_un = new customWP_Comment_Query(  );
		$unregister_comments = $comment_query_un->query( $unregister );
		
		$registered = array(
			'user_id' => 2,
			'user__not_id' => 0,
			'post_id' => $post->ID,
			'orderby' => $orderby,
			'order' => $order,
			
		);

		$comment_query_reg = new customWP_Comment_Query(  );
		$registered_comments = $comment_query_reg->query( $registered );
		
		if( isset( $_GET['anon'] ) ){
			$registered_active='';
			$un_registered_active='active';
		}else{
			$registered_active='active';
			$un_registered_active='';
		}
		
		//print_r($registered_comments);
		
		$registered_count = count($registered_comments);
		$unregister_count = count($unregister_comments);

?>
      
		<div class="coments_tabs_box" id="coments_tabs_box">
			<ul class="coments_tabs_list clearfix">
				<li class="coments_tabs_list_item f_left <?php echo $registered_active; ?>">Зарегистрированные <span><?php if($registered_count) echo '('.$registered_count.')'; ?></span></li>
				<li class="coments_tabs_list_item f_left <?php echo $un_registered_active; ?>">АНОНИМНЫЕ <span><?php if($unregister_count) echo '('.$unregister_count.')'; ?></span></li>
			</ul>
			<div class="coments_tabs_content">
				<div class="registered_coments <?php echo $registered_active; ?>">
					<ul class="coments_tabs_list1 clearfix">
						<li class="coments_tabs_list1_item f_left <?php echo $sort_active_new; ?>"><a href="<?php the_permalink(); ?>?sort=0#coments_tabs_box">Новые</a></li>
						<li class="coments_tabs_list1_item f_left <?php echo $sort_active_old; ?>"><a href="<?php the_permalink(); ?>?sort=1#coments_tabs_box">Старые</a></li>
						<li class="coments_tabs_list1_item f_left <?php echo $sort_active_best; ?>"><a href="<?php the_permalink(); ?>?sort=2#coments_tabs_box">Лучшие</a></li>
					</ul>
					<hr class="separator_h"/>
			
					<?php 
						if( $_GET['sort']==2 ){
							$args = array( 'style' => 'div', 'avatar_size' => '64', 'anon' => 0 );
							foreach ( $registered_comments as $comment ){
								//print_r($comment);
								comment_walk_best( $comment, 1, $args );
							}
						}else{
							wp_list_comments( array( 'style' => 'div', 'avatar_size' => '64', 'anon' => 0, 'walker' => new customCommentWalker() ),$registered_comments );
						}
					?>


							<div id="respond" class="comment-respond">
								<a rel="nofollow" id="cancel-comment-reply-link" href="#respond" class="fa fa-reply-close" style="display:none;"></a>
										<div class="add_reply_box registered_coments_form">
											<div class="leave_comment_box">
												<h5 class="leave_comment_title">Комментировать:</h5>
												<?php //print_r($registered_comments); ?>
											<?php 
												$current_user = wp_get_current_user();
												$user_identity = $current_user->user_login;
												if($user_identity):
					
												if( get_the_author_meta('first_name',$current_user->ID ) || get_the_author_meta('last_name',$current_user->ID )  ){
													$username=get_the_author_meta('first_name',$current_user->ID ).' '.get_the_author_meta('last_name',$current_user->ID );
												}else{
													$username=$user_identity;
												}
												echo '<p class="logged-in-as">' .
												sprintf(
												__( 'Вы вошли как <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Выйти?</a>' ),
												  admin_url( 'profile.php' ),
												  $username,
												  wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
												) . '</p>';
												
												else : ?>
												<div class="enter_social_box clearfix">
													<span class="f_left">Войти через</span>
													<ul class="enter_social_list f_left clearfix">
														<li><a rel="nofollow" href="javascript:void(0);" data-provider="Vkontakte" class="vk wp-social-login-provider wp-social-login-provider-vkontakte"></a></li>

														<?php/*<li><a href="#" class="tw"></a></li>
														<li><a href="#" class="in"></a></li>
														
														<li><a href="#" class="yn"></a></li>
														<li><a href="#" class="c"></a></li>*/?>
														
														<li><a rel="nofollow" href="javascript:void(0);"  class="b wp-social-login-provider wp-social-login-provider-facebook" data-provider="Facebook"></a></li>
														
														<li><a rel="nofollow" href="javascript:void(0);"  class="g_plus wp-social-login-provider wp-social-login-provider-google" data-provider="Google"></a></li>

													</ul><br />
													Либо авторизуйтесь через форму <a href="<?php echo wp_login_url( get_permalink() ); ?>" class="std_link">WP</a>
											
													<input type="hidden" id="wsl_popup_base_url" value="<?php echo $_SERVER['HTTP_HOST'];?>/wp-login.php?action=wordpress_social_authenticate&#038;mode=login&#038;" />
													<input type="hidden" id="wsl_login_form_uri" value="<?php echo $_SERVER['HTTP_HOST'];?>/wp-login.php" />

													
												</div>
												<p class="leave_comment_text mb25">
													Для того, чтобы комментировать анонимно,<br>
													<a class="anonym_coments_link" href="javascript:;">нажмите сюда</a>
												</p>
												
												<?php endif;?>
												<?php if($user_identity):?>
												<form action="/wp-comments-post.php" method="post" id="commentform" class="comment-form forms">
													<textarea class="leave_comment_input mb10" id="comment" name="comment" aria-required="true" placeholder="Напишите свой комментарий..."></textarea>
													<div class="al_right">
														<button class="leave_comment_publish" name="submit" value="Submit"><span>Опубликовать</span></button>

													</div>
													
													<input type='hidden' name='comment_post_ID' value='<?php echo $post->ID;?>' id='comment_post_ID' />
													<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
													<input type='hidden' name='redirect_to' id='redirect_to' value='<?php the_permalink(); echo $get_params;?>' />
													
													
												</form>
												<?php else: ?>
												<form class="comment-form forms">
													<textarea class="leave_comment_input mb10" aria-required="true" placeholder="Напишите свой комментарий..."></textarea>
													<div class="al_right">
														<button class="leave_comment_publish not-enter-comment" name="submit" value="Submit"><span>Опубликовать</span></button>

													</div>
												</form>
												<?php endif; ?>
											</div>
											<hr class="separator_h"/>
										</div>
							</div>

	
	
	
				</div>
				<div class="anonym_coments <?php echo $un_registered_active; ?>">
					<ul class="coments_tabs_list1 clearfix">
						<li class="coments_tabs_list1_item f_left <?php echo $sort_active_new; ?>"><a href="<?php the_permalink(); ?>?anon=1&sort=0#coments_tabs_box">Новые</a></li>
						<li class="coments_tabs_list1_item f_left <?php echo $sort_active_old; ?>" ><a href="<?php the_permalink(); ?>?anon=1&sort=1#coments_tabs_box">Старые</a></li>
						<li class="coments_tabs_list1_item f_left <?php echo $sort_active_best; ?>"><a href="<?php the_permalink(); ?>?anon=1&sort=2#coments_tabs_box">Лучшие</a></li>
					</ul>
					<hr class="separator_h"/>
					
					<?php 
						if( $_GET['sort']==2 ){
							$args = array( 'style' => 'div', 'avatar_size' => '0', 'anon' => 1 );
							foreach ( $unregister_comments as $comment ){
								//print_r($comment);
								comment_walk_best( $comment, 1, $args );
							}
						}else{
							wp_list_comments( array( 'style' => 'div', 'avatar_size' => '0', 'anon' => 1, 'walker' => new customCommentWalker() ),$unregister_comments );
						}
					?>
					
			
					<div id="respond-anon" class="comment-respond">
						<div class="add_reply_box anonym_coments_form">
							<div class="leave_comment_box">
								<h5 class="leave_comment_title">Комментировать:</h5>
								<p class="leave_comment_text mb25">
									Вы находитесь на странице анонимных комментариев, чтобы комментировать как зарегистрированный пользователь,
									<a class="registered_coments_link" href="javascript:;">зайдите сюда</a>
								</p>
								<form action="/wp-comments-post.php" method="post" id="commentform" class="comment-form forms">
									<input class="leave_comment_input mb20 author-name" name="author" type="text" placeholder="Имя"/>
									<textarea id="comment" name="comment" class="leave_comment_input mb10" placeholder="Напишите свой комментарий..."></textarea>
									<div class="al_right">
										<button class="leave_comment_publish unregistered_submit"><span>Опубликовать</span></button>
									</div>
									
									<input type='hidden' name='comment_post_ID' value='<?php echo $post->ID;?>' id='comment_post_ID' />
									<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
									<input type='hidden' name='redirect_to' id='redirect_to' value='<?php the_permalink(); echo $get_params;?>&anon=1#coments_tabs_box' />
								</form>
							</div>
							
							<hr class="separator_h"/>
						</div>
					</div>
				
				</div>
			</div>
		</div>
