<?php
class customCommentWalker extends Walker_Comment {

	public $wellcome_to_discuss = 0;
	/**
	 * What the class handles.
	 *
	 * @see Walker::$tree_type
	 *
	 * @since 2.7.0
	 * @var string
	 */
	public $tree_type = 'comment';

	/**
	 * DB fields to use.
	 *
	 * @see Walker::$db_fields
	 *
	 * @since 2.7.0
	 * @var array
	 */
	public $db_fields = array ('parent' => 'comment_parent', 'id' => 'comment_ID');

	/**
	 * Start the list before the elements are added.
	 *
	 * @see Walker::start_lvl()
	 *
	 * @since 2.7.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of comment.
	 * @param array $args Uses 'style' argument for type of HTML list.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth + 1;

		switch ( $args['style'] ) {
			case 'div':
				break;
			case 'ol':
				$output .= '<ol class="children">' . "\n";
				break;
			case 'ul':
			default:
				$output .= '<ul class="children">' . "\n";
				break;
		}
	}

	/**
	 * End the list of items after the elements are added.
	 *
	 * @see Walker::end_lvl()
	 *
	 * @since 2.7.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of comment.
	 * @param array  $args   Will only append content if style argument value is 'ol' or 'ul'.
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth + 1;

		switch ( $args['style'] ) {
			case 'div':
				break;
			case 'ol':
				$output .= "</ol><!-- .children -->\n";
				break;
			case 'ul':
			default:
				$output .= "</ul><!-- .children -->\n";
				break;
		}
	}

	/**
	 * Traverse elements to create list from elements.
	 *
	 * This function is designed to enhance Walker::display_element() to
	 * display children of higher nesting levels than selected inline on
	 * the highest depth level displayed. This prevents them being orphaned
	 * at the end of the comment list.
	 *
	 * Example: max_depth = 2, with 5 levels of nested content.
	 * 1
	 *  1.1
	 *    1.1.1
	 *    1.1.1.1
	 *    1.1.1.1.1
	 *    1.1.2
	 *    1.1.2.1
	 * 2
	 *  2.2
	 *
	 * @see Walker::display_element()
	 * @see wp_list_comments()
	 *
	 * @since 2.7.0
	 *
	 * @param object $element           Data object.
	 * @param array  $children_elements List of elements to continue traversing.
	 * @param int    $max_depth         Max depth to traverse.
	 * @param int    $depth             Depth of current element.
	 * @param array  $args              An array of arguments.
	 * @param string $output            Passed by reference. Used to append additional content.
	 * @return null Null on failure with no changes to parameters.
	 */
	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {

		if ( !$element )
			return;

		$id_field = $this->db_fields['id'];
		$id = $element->$id_field;

		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );

		// If we're at the max depth, and the current element still has children, loop over those and display them at this level
		// This is to prevent them being orphaned to the end of the list.
		if ( $max_depth <= $depth + 1 && isset( $children_elements[$id]) ) {
			foreach ( $children_elements[ $id ] as $child )
				$this->display_element( $child, $children_elements, $max_depth, $depth, $args, $output );

			unset( $children_elements[ $id ] );
		}

	}

	/**
	 * Start the element output.
	 *
	 * @since 2.7.0
	 *
	 * @see Walker::start_el()
	 * @see wp_list_comments()
	 *
	 * @param string $output  Passed by reference. Used to append additional content.
	 * @param object $comment Comment data object.
	 * @param int    $depth   Depth of comment in reference to parents.
	 * @param array  $args    An array of arguments.
	 */
	public function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
		$depth++;
		$GLOBALS['comment_depth'] = $depth;
		$GLOBALS['comment'] = $comment;

		if ( !empty( $args['callback'] ) ) {
			ob_start();
			call_user_func( $args['callback'], $comment, $args, $depth );
			$output .= ob_get_clean();
			return;
		}

		if ( ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) && $args['short_ping'] ) {
			ob_start();
			$this->ping( $comment, $depth, $args );
			$output .= ob_get_clean();
		} elseif ( 'html5' === $args['format'] ) {
			ob_start();
			$this->html5_comment( $comment, $depth, $args );
			$output .= ob_get_clean();
		} else {
			ob_start();
			$this->comment( $comment, $depth, $args );
			$output .= ob_get_clean();
		}
		

	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @since 2.7.0
	 *
	 * @see Walker::end_el()
	 * @see wp_list_comments()
	 *
	 * @param string $output  Passed by reference. Used to append additional content.
	 * @param object $comment The comment object. Default current comment.
	 * @param int    $depth   Depth of comment.
	 * @param array  $args    An array of arguments.
	 */
	public function end_el( &$output, $comment, $depth = 0, $args = array() ) {
		if ( !empty( $args['end-callback'] ) ) {
			ob_start();
			call_user_func( $args['end-callback'], $comment, $args, $depth );
			$output .= ob_get_clean();
			return;
		}
		
		if( $this->wellcome_to_discuss && !$comment->comment_parent ){
		
			if( $comment->user_id==0 ){
				$part_discus_class =  'part_discus-anon';
			}else{
				$part_discus_class =  'part_discus-reg';
			}
		
			$output .= '
			<div class="al_right mb20">
					<a href="javascript:;" class="part_discus '.$part_discus_class.'" data-comment-id="'. $comment->comment_ID.'"><i class="fa fa-reply-all"></i>Принять участие в дискусии</a>
			</div>
			<hr class="separator_h"/>';

			$this->wellcome_to_discuss=0;
		}
		/*if ( 'div' == $args['style'] )
			$output .= "</div><!-- #comment-## -->\n";
		else
			$output .= "</li><!-- #comment-## -->\n";*/
	}

	/**
	 * Output a pingback comment.
	 *
	 * @access protected
	 * @since 3.6.0
	 *
	 * @see wp_list_comments()
	 *
	 * @param object $comment The comment object.
	 * @param int    $depth   Depth of comment.
	 * @param array  $args    An array of arguments.
	 */
	protected function ping( $comment, $depth, $args ) {
		$tag = ( 'div' == $args['style'] ) ? 'div' : 'li';
?>
		<<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
			<div class="comment-body">
				<?php _e( 'Pingback:' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit' ), '<span class="edit-link">', '</span>' ); ?>
			</div>
<?php
	}

	/**
	 * Output a single comment.
	 *
	 * @access protected
	 * @since 3.6.0
	 *
	 * @see wp_list_comments()
	 *
	 * @param object $comment Comment to display.
	 * @param int    $depth   Depth of comment.
	 * @param array  $args    An array of arguments.
	 */
	protected function comment( $comment, $depth, $args ) {
		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
		if( $comment->user_id ){
			$container_class = 'reg-container';
		}else{
			$container_class = 'anon-container';
		}
?>
		<<?php echo $tag; ?> <?php comment_class( $comment->comment_parent ? 'coments_container '.$container_class.' clearfix coment_level2' : 'coments_container '.$container_class.' clearfix' ); ?> id="comment-<?php comment_ID(); ?>" itemprop="comment" itemscope itemtype="http://schema.org/Comment">
		<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID(); ?>" class="comment-body">
		<?php endif; ?>
		<div class="coments_avatar f_left" <?php if ( 0 != $comment->user_id ) echo 'style="display:block;"'; ?>>
			<?php if ( 0 != $comment->user_id ) echo get_avatar( $comment, 64 ); ?>
		</div>
		<?php if( $comment->comment_parent ) $this->wellcome_to_discuss = 1; ?>

			<div class="wrapper p_rel pl20 <?php if( $comment->user_id==0 )echo 'pl20-anon';  ?>">
			
				<?php
					$current_user = wp_get_current_user();
					if( $current_user->ID > 0 ){
						if( $comment->complaints_user_id ){
							$complaintclass="complaint";
							$complainttext = 'Жалоба отправлена';
						}else{
							$complaintclass="sent-complaint";
							$complainttext = 'Пожаловаться';
						}
					}else{
						if( $comment->complaints_user_ip ){
							$complaintclass="complaint";
							$complainttext = 'Жалоба отправлена';
						}else{
							$complaintclass="sent-complaint";
							$complainttext = 'Пожаловаться';
						}
					}
				?>
			
				<div class="coment_close p_abs <?php echo $complaintclass; ?>" data-commen-id="<?php echo $comment->comment_ID;?>">
					<span class="coment_close_text p_abs"><?php echo $complainttext; ?></span>
				</div>
				
				
				<div class="">
					<?php
						if($comment->user_id > 0){
							if( get_the_author_meta('first_name',$comment->user_id ) || get_the_author_meta('last_name',$comment->user_id )  ){
								$username=get_the_author_meta('first_name',$comment->user_id ).' '.get_the_author_meta('last_name',$comment->user_id );
							}else{
								$username=$comment->comment_author;
							}
						}else{
							$username=$comment->comment_author;
						}
					?>
					<span class="user_name" itemprop="creator"><?php echo $username;?></span>
					<span class="data_time"><?php echo date('d.m.Y h:m' , strtotime($comment->comment_date));?></span>
					<meta itemprop="datePublished" content="<?php echo date('Y-m-d h:m' , strtotime($comment->comment_date));?>" /> 
				</div>

			<p class="coment_text" itemprop="text">
				<?php if ( '0' == $comment->comment_approved ) : ?>
				<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ) ?></em>
				<br />
				<?php endif; ?>
				
				<?php echo $comment->comment_content; ?>
			</p>
			
			<div class="clearfix mb20">
				<div class="rating_link_box f_left">
					<?php
						$current_user = wp_get_current_user();
						if( $current_user->ID > 0 ){
							if( $comment->voted_user_id ){
								$voteclass="no-cursor";
							}else{
								$voteclass="rate_comment";
							}
						}else{
							if( $comment->user_ip ){
								$voteclass="no-cursor";
							}else{
								$voteclass="rate_comment";
							}
						}
					?>
					<a href="javascript:;" class="rating_link_please <?php echo $voteclass; ?>" data-rating="1" data-commen-id="<?php echo $comment->comment_ID;?>">
						<i class="fa fa-thumbs-up"></i>
						<?php 
							if( $comment->good_vote_count ){
								echo $comment->good_vote_count;
							}else{
								echo '0';
							}
						?>
					</a>
					<a href="javascript:;" class="rating_link_displease <?php echo $voteclass; ?>" data-rating="-1" data-commen-id="<?php echo $comment->comment_ID;?>">
						<i class="fa fa-thumbs-down"></i>
						<?php 
							if( $comment->bad_vote_count ){
								echo $comment->bad_vote_count;
							}else{
								echo '0';
							}
						?>
					</a>
				</div>
				<?php if( ! $comment->comment_parent ){?>
					<a href="javascript:;" class="coment_reply log_coment_reply_btn <?php $current_user = wp_get_current_user(); if( $comment->user_id==0 && $current_user->ID == 0 )echo 'anon-reply'; else echo 'reg-reply';  ?> f_right" data-comment-id="<?php echo $comment->comment_ID;?>">Ответить</a>
				<?php } ?>
				<?php/*
					if( ! $comment->comment_parent ){
						comment_reply_link( array_merge( $args, array(
							'add_below' => $add_below,
							'depth'     => $depth,
							'max_depth' => $args['max_depth'],
							'before'    => '',
							'after'     => '',
							'reply_text' => 'Ответить',
							'reply_to_text' => 'Ответить',
						) ) );
					}*/
				?>
			</div>

			
			
			</div>
		<hr class="separator_h"/>
		
		<div class="add_reply_box d_none">
		</div>
		

		<?php if ( 'div' != $args['style'] ) : ?>
		</div>
		<?php endif; ?>
		
		</div>
<?php
	}

	/**
	 * Output a comment in the HTML5 format.
	 *
	 * @access protected
	 * @since 3.6.0
	 *
	 * @see wp_list_comments()
	 *
	 * @param object $comment Comment to display.
	 * @param int    $depth   Depth of comment.
	 * @param array  $args    An array of arguments.
	 */
	protected function html5_comment( $comment, $depth, $args ) {
		$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';
?>
		<<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( $this->has_children ? 'parent' : '' ); ?>>
			<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<?php if ( 0 != $comment->user_id ) echo get_avatar( $comment, 64 ); ?>
						<?php printf( __( '%s <span class="says">says:</span>' ), sprintf( '<b class="fn">%s</b>', get_comment_author_link() ) ); ?>
					</div><!-- .comment-author -->

					<div class="comment-metadata">
						<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID, $args ) ); ?>">
							<time datetime="<?php comment_time( 'c' ); ?>">
								<?php printf( _x( '%1$s at %2$s', '1: date, 2: time' ), get_comment_date(), get_comment_time() ); ?>
							</time>
						</a>
						<?php edit_comment_link( __( 'Edit' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .comment-metadata -->

					<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></p>
					<?php endif; ?>
				</footer><!-- .comment-meta -->

				<div class="comment-content">
					<?php comment_text(); ?>
				</div><!-- .comment-content -->

				<?php
				comment_reply_link( array_merge( $args, array(
					'add_below' => 'div-comment',
					'depth'     => $depth,
					'max_depth' => $args['max_depth'],
					'before'    => '<div class="reply">',
					'after'     => '</div>'
				) ) );
				?>
			</article><!-- .comment-body -->
<?php
	}
}