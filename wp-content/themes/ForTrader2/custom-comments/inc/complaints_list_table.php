<?php
if(!class_exists('WP_List_Table')){
   require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


class Complaints_List_Table extends WP_List_Table {

   /**
    * Constructor, we override the parent to pass our own arguments
    * We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
    */
    function __construct() {
       parent::__construct( array(
      'singular'=> 'wp_list_text_link', //Singular label
      'plural' => 'wp_list_test_links', //plural label, also this well be one of the table css class
      'ajax'   => false //We won't support Ajax for this table
      ) );
    }
	
	/**
	 * Add extra markup in the toolbars before or after the list
	 * @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
	 */
	function extra_tablenav( $which ) {
	   if ( $which == "top" ){
		  //The code that goes before the table is here
		  echo "<h2>Жалобы на коментарии</h2>";
	   }
	   if ( $which == "bottom" ){
		  //The code that goes after the table is there
		  echo "Жалобы на коментарии";
	   }
	}
	
	/**
	 * Define the columns that are going to be used in the table
	 * @return array $columns, the array of columns to use with the table
	 */
	function get_columns() {
	   return $columns= array(
		  'cb' => '<input type="checkbox" />',
		  'comment_ID'=>__('ID'),
		  'comment_content'=>__('Text'),
		  'count'=>__('Count complaints'),
		  //'remove_comment'=>__('Remove Comment'),
		  //'remove_complaint'=>__('Remove Complaint'),
	   );
	}
	
	/**
	 * Decide which columns to activate the sorting functionality on
	 * @return array $sortable, the array of columns that can be sorted by the user
	 */
	/*public function get_sortable_columns() {
	   return $sortable = array(
		  'col_link_id'=>'link_id',
		  'col_link_name'=>'link_name',
		  'col_link_visible'=>'link_visible'
	   );
	}*/
	
	/**
	 * Prepare the table with different parameters, pagination, columns and table elements
	 */
	function prepare_items() {
	   global $wpdb, $_wp_column_headers;
	   $screen = get_current_screen();

	   /* -- Preparing your query -- */
			$query = '
				SELECT comments.comment_ID, comments.comment_content, comment_complaints.count
				FROM wp_comments as comments 
				LEFT JOIN (SELECT DISTINCT comment_id, COUNT( comment_id ) AS count FROM  wp_comment_complaints WHERE 1 GROUP BY comment_id ) as comment_complaints ON comment_complaints.comment_id = comments.comment_ID 
				where comment_complaints.count IS NOT NULL and comments.comment_approved=1
			';



	   /* -- Pagination parameters -- */
			//Number of elements in your table?
			$totalitems = $wpdb->query($query); //return the total number of affected rows
			//How many to display per page?
			$perpage = 10;
			//Which page is this?
			$paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
			//Page Number
			if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
			//How many pages do we have in total?
			$totalpages = ceil($totalitems/$perpage);
			//adjust the query to take pagination into account
		   if(!empty($paged) && !empty($perpage)){
			  $offset=($paged-1)*$perpage;
			 $query.=' LIMIT '.(int)$offset.','.(int)$perpage;
		   }

	   /* -- Register the pagination -- */
		  $this->set_pagination_args( array(
			 "total_items" => $totalitems,
			 "total_pages" => $totalpages,
			 "per_page" => $perpage,
		  ) );
		  //The pagination links are automatically built according to those parameters

	   /* -- Register the Columns -- */
		  $columns = $this->get_columns();
		  $_wp_column_headers[$screen->id]=$columns;

	   /* -- Fetch the items -- */
		  $this->items = $wpdb->get_results($query);

		$this->process_bulk_action();
		  
	}
	
	/**
	 * Display the rows of records in the table
	 * @return string, echo the markup of the rows
	 */
	function display_rows() {

	   //Get the records registered in the prepare_items method
	   $records = $this->items;

	   //Get the columns registered in the get_columns and get_sortable_columns methods
	   //list( $columns, $hidden ) = $this->get_column_info();
	   $columns = $this->get_columns();
	   
	    foreach ( $columns as $column_name => $column_display_name ) {
			if( $column_name !='cb' )
				echo '<th>'.$column_display_name.'</th>';
			else
				echo '<th><input type="checkbox" id="cb-select-all"></th>';
		}

	   //Loop for each record
	   if(!empty($records)){foreach($records as $rec){


		echo '<tr id="record_'.$rec->link_id.'">';
		
		  foreach ( $columns as $column_name => $column_display_name ) {

			 //Style attributes for each col
			 $class = "class='$column_name column-$column_name comment byuser comment-author-id8648681 even thread-even depth-1 approved'";
			 $style = "";
			 if ( in_array( $column_name, $hidden ) ) $style = ' style="display:none;"';
			 $attributes = $class . $style;

			 //edit link
			 $editlink  = '/wp-admin/link.php?action=edit&link_id='.(int)$rec->link_id;

			 //Display the cell
			 switch ( $column_name ) {
				case "cb":  echo '<td '.$attributes.'><input id="cb-select-comment" type="checkbox" name="commentids[]" value="'.$rec->comment_ID.'" /></td>';   break;
				case "comment_ID":  echo '<td '.$attributes.'>'.stripslashes($rec->comment_ID).'</td>';   break;
				case "comment_content": echo '<td '.$attributes.'>'.stripslashes($rec->comment_content).'</td>'; break;
				case "count": echo '<td '.$attributes.'>'.stripslashes($rec->count).'</td>'; break;
				//case "remove_comment": echo '<td '.$attributes.'><a href="#" id="comment-to-trash" data-comment-id="'.$rec->comment_ID.'" >Trash</a></td>'; break;
				//case "remove_complaint": echo '<td '.$attributes.'><a href="#" id="complaint-remove" data-comment-id="'.$rec->comment_ID.'">Remove</a></td>'; break;			
				
			 }
		  }

		  //Close the line
		  echo'</tr>';
	   }}
	}
	
	function display() {
		extract( $this->_args );

		wp_nonce_field( "fetch-list-" . get_class( $this ), '_ajax_fetch_list_nonce' );

		$this->display_tablenav( 'top' );

?>
<table class="<?php echo implode( ' ', $this->get_table_classes() ); ?>">
	<thead>
	<tr>
		<?php $this->print_column_headers(); ?>
	</tr>
	</thead>

	<tfoot>
	<tr>
		<?php $this->print_column_headers( false ); ?>
	</tr>
	</tfoot>

	<tbody id="the-comment-list" data-wp-lists="list:comment">
		<?php $this->display_rows_or_placeholder(); ?>
	</tbody>

	<tbody id="the-extra-comment-list" data-wp-lists="list:comment" style="display: none;">
		<?php $this->items = $this->extra_items; $this->display_rows(); ?>
	</tbody>
</table>
<script>
jQuery('body').on('click','#cb-select-all',function(e){

	if( jQuery(this).attr('checked')=='checked' ){
		jQuery('input#cb-select-comment').attr({'checked':'checked'});
	}else{
		jQuery('input#cb-select-comment').removeAttr('checked');
	}
	
});
</script>
<?php

		$this->display_tablenav( 'bottom' );
	}
	
	function get_bulk_actions() {
		global $comment_status;

		$actions = array();
		$actions['trash'] = __( 'Move to Trash' );
		$actions['remove_complaint'] = __( 'Remove Complaint' );

		return $actions;
	}
	
	public function process_bulk_action() {

        // security check!
        

        return;
    }
	
	

}
