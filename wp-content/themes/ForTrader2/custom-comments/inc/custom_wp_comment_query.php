<?php
class customWP_Comment_Query extends WP_Comment_Query {
	
	public function query( $query_vars ) {
		global $wpdb;

		$defaults = array(
			'author_email' => '',
			'author__in' => '',
			'author__not_in' => '',
			'include_unapproved' => '',
			'fields' => '',
			'ID' => '',
			'comment__in' => '',
			'comment__not_in' => '',
			'karma' => '',
			'number' => '',
			'offset' => '',
			'orderby' => '',
			'order' => 'DESC',
			'parent' => '',
			'post_author__in' => '',
			'post_author__not_in' => '',
			'post_ID' => '',
			'post_id' => 0,
			'post__in' => '',
			'post__not_in' => '',
			'post_author' => '',
			'post_name' => '',
			'post_parent' => '',
			'post_status' => '',
			'post_type' => '',
			'status' => 'all',
			'type' => '',
			'type__in' => '',
			'type__not_in' => '',
			'user_id' => '',
			'user__not_id' => '',
			'search' => '',
			'count' => false,
			'meta_key' => '',
			'meta_value' => '',
			'meta_query' => '',
			'date_query' => null, // See WP_Date_Query
		);

		$this->query_vars = wp_parse_args( $query_vars, $defaults );
		
		
		$where = ' where (1=1) ';
		
		if( $this->query_vars['user_id'] == 0 ){
			$where .= 'and (comments.user_id ='.$this->query_vars['user_id'].' or ( comments.user_id <> 0 and ( SELECT user_id FROM wp_comments WHERE comment_ID = comments.comment_parent  )=0 )) ';
		}
		if( $this->query_vars['user__not_id'] == 0 ){
			$where .= 'and (comments.user_id <>'.$this->query_vars['user__not_id'].' and ( comments.comment_parent=0 or ( SELECT user_id FROM wp_comments WHERE comment_ID = comments.comment_parent  )<>0) ) ';
		}
		if( isset( $this->query_vars['post_id'] ) ){
			$where .= 'and comments.comment_post_ID='.$this->query_vars['post_id'].' ';
		}
		$where .= " and (  comments.comment_approved = '1'  )";
		
		$orderby=' ';
		if( isset( $this->query_vars['orderby'] ) ){
			$orderby .= 'ORDER BY '.$this->query_vars['orderby'].' ';
		}
		$order='';
		if( isset( $this->query_vars['order'] ) ){
			$order .= ' '.$this->query_vars['order'].' ';
		}
		
		
		$statuses = $this->query_vars['status'];
		
		$current_user = wp_get_current_user();
		
		if( $current_user->ID ){
			$subwhere = ' user_id='.$current_user->ID;
		}else{
			$subwhere = " user_ip='".$_SERVER['REMOTE_ADDR']."' and user_id=0";
		}
		

		$sql="
		SELECT comments.comment_ID, comments.comment_post_ID, comments.comment_author, comments.comment_author_email, comments.comment_author_url, comments.comment_author_IP, comments.comment_date, comments.comment_date_gmt, comments.comment_content, comments.comment_approved, comments.comment_agent, comments.comment_type, comments.comment_parent, comments.user_id, comment_votes.good_vote_count, comment_votes.bad_vote_count, comment_voted_users.user_id as voted_user_id, comment_voted_users.user_ip,  comment_complaints.user_id as complaints_user_id, comment_complaints.user_ip as complaints_user_ip
		FROM wp_comments as comments
		LEFT JOIN wp_comment_votes as comment_votes  ON comment_votes.comment_id = comments.comment_ID 
		LEFT JOIN (select * from wp_comment_voted_users where ".$subwhere." ) as comment_voted_users  ON comment_voted_users.comment_id = comments.comment_ID
		LEFT JOIN (select * from wp_comment_complaints where ".$subwhere." ) as comment_complaints  ON comment_complaints.comment_id = comments.comment_ID
		".
		$where.$orderby.$order;
		
		
		$results = $wpdb->get_results( $sql );
		/**
		 * Filter the comment query results.
		 *
		 * @since 3.1.0
		 *
		 * @param array            $results  An array of comments.
		 * @param WP_Comment_Query &$this    Current instance of WP_Comment_Query, passed by reference.
		 */
		$comments = apply_filters_ref_array( 'the_comments', array( $results, &$this ) );

		wp_cache_add( $cache_key, $comments, 'comment' );

		return $comments;
	}


}