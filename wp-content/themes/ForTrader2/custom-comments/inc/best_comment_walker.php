<?php

function comment_walk_best( $comment, $depth, $args ) {
		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
?>
		<<?php echo $tag; ?> <?php comment_class( $comment->comment_parent ? 'coments_container clearfix' : 'coments_container clearfix' ); ?> id="comment-<?php comment_ID(); ?>" itemprop="comment" itemscope itemtype="http://schema.org/Comment">
		<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID(); ?>" class="comment-body">
		<?php endif; ?>
		<div class="coments_avatar f_left" <?php if ( 0 != $comment->user_id ) echo 'style="display:block;"'; ?>>
			<?php if ( 0 != $comment->user_id ) echo get_avatar( $comment, 64 ); ?>
		</div>


			<div class="wrapper p_rel pl20 <?php if( $comment->user_id==0 )echo 'pl20-anon';  ?>">
			
				<?php
					$current_user = wp_get_current_user();
					if( $current_user->ID > 0 ){
						if( $comment->complaints_user_id ){
							$complaintclass="complaint";
							$complainttext = 'Жалоба отправлена';
						}else{
							$complaintclass="sent-complaint";
							$complainttext = 'Пожаловаться';
						}
					}else{
						if( $comment->complaints_user_ip ){
							$complaintclass="complaint";
							$complainttext = 'Жалоба отправлена';
						}else{
							$complaintclass="sent-complaint";
							$complainttext = 'Пожаловаться';
						}
					}
				?>
			
				<div class="coment_close p_abs <?php echo $complaintclass; ?>" data-commen-id="<?php echo $comment->comment_ID;?>">
					<span class="coment_close_text p_abs"><?php echo $complainttext; ?></span>
				</div>
				
				
				<div class="">
					<?php
						if($comment->user_id > 0){
							if( get_the_author_meta('first_name',$comment->user_id ) || get_the_author_meta('last_name',$comment->user_id )  ){
								$username=get_the_author_meta('first_name',$comment->user_id ).' '.get_the_author_meta('last_name',$comment->user_id );
							}else{
								$username=$comment->comment_author;
							}
						}else{
							$username=$comment->comment_author;
						}
					?>
					<span class="user_name" itemprop="creator"><?php echo $username;?></span>
					<span class="data_time"><?php echo date('d.m.Y h:m' , strtotime($comment->comment_date));?></span>
					<meta itemprop="datePublished" content="<?php echo date('Y-m-d h:m' , strtotime($comment->comment_date));?>" /> 
				</div>

			<p class="coment_text" itemprop="text">
				<?php if ( '0' == $comment->comment_approved ) : ?>
				<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ) ?></em>
				<br />
				<?php endif; ?>
				
				<?php echo $comment->comment_content; ?>
			</p>
			
			<div class="clearfix mb20">
				<div class="rating_link_box f_left">
					<?php
						$current_user = wp_get_current_user();
						if( $current_user->ID > 0 ){
							if( $comment->voted_user_id ){
								$voteclass="no-cursor";
							}else{
								$voteclass="rate_comment";
							}
						}else{
							if( $comment->user_ip ){
								$voteclass="no-cursor";
							}else{
								$voteclass="rate_comment";
							}
						}
					?>
					<a href="javascript:;" class="rating_link_please <?php echo $voteclass; ?>" data-rating="1" data-commen-id="<?php echo $comment->comment_ID;?>">
						<i class="fa fa-thumbs-up"></i>
						<?php 
							if( $comment->good_vote_count ){
								echo $comment->good_vote_count;
							}else{
								echo '0';
							}
						?>
					</a>
					<a href="javascript:;" class="rating_link_displease <?php echo $voteclass; ?>" data-rating="-1" data-commen-id="<?php echo $comment->comment_ID;?>">
						<i class="fa fa-thumbs-down"></i>
						<?php 
							if( $comment->bad_vote_count ){
								echo $comment->bad_vote_count;
							}else{
								echo '0';
							}
						?>
					</a>
				</div>
			</div>

			
			
			</div>
		<hr class="separator_h"/>
		
		<div class="add_reply_box d_none">
		</div>
		

		<?php if ( 'div' != $args['style'] ) : ?>
		</div>
		<?php endif; ?>
		
		</div>
<?php
	}