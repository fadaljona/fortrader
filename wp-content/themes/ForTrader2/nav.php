<?php
// Вызов постраничной навигации
global $wp_query;
$max_page = $wp_query->max_num_pages;
$nump=2;  /*Количество отображаемых подряд номеров страниц*/
 
if($max_page>1){
    $paged = intval(get_query_var('paged'));
    if(empty($paged) || $paged == 0) $paged = 1;
 
    echo '<div class="pages">';
	
      
    echo '<a class="prev" href="'.get_pagenum_link($paged-1).'">Пред.</a>';
    
		
	echo '<div class="pages2">';	

    if($paged!=1) echo '<a href="'.get_pagenum_link(1).'">1</a>';
        else echo '<span class="on">1</span>'; 
 
    if($paged-$nump>1) $start=$paged-$nump; else $start=2;
    if($paged+$nump<$max_page) $end=$paged+$nump; else $end=$max_page-1;
 
    if($start>2) echo '... <a href="'.get_pagenum_link($max_page).'">'.$max_page.'</a>';
 
    for ($i=$start;$i<=$end+1;$i++)
     {
     if($paged!=$i) echo ' ٠ <a href="'.get_pagenum_link($i).'">'.$i.'</a>';
        else echo ' ٠ <span class="on">'.$i.'</span>';
     }
 
    if($end<$max_page-1) echo '  ...  <a href="'.get_pagenum_link($max_page).'">'.$max_page.'</a>';
     echo '</div>';
    if($paged!=$max_page) echo '<a class="next" href="'.get_pagenum_link($paged+1).'">След.</a>';
        else echo '<span class="next">След.</span> ';
    echo '</div>' ;
    }
?>