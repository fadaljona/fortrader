<?php get_header(); ?>     

  <div class="content">
  
    <div class="title"><?php single_cat_title(); ?></div>
    <div class="txt5"><?php echo category_description(); ?></div>
   
   
  
<?php 

$q = "SELECT P.ID AS post_ID, MAX(C.comment_ID) AS comment_ID, P.post_title, P.comment_count 
      FROM wp_comments C 
         LEFT JOIN wp_posts P ON P.ID=C.comment_post_ID 
         LEFT OUTER JOIN wp_term_relationships R ON R.object_id=C.comment_post_ID  
         LEFT OUTER JOIN wp_term_taxonomy T ON T.term_taxonomy_id=R.term_taxonomy_id 
      WHERE T.taxonomy='category' AND C.comment_approved='1' 
      GROUP BY C.comment_post_ID 
      ORDER BY MAX(C.comment_date) DESC 
      LIMIT 20;";	
 
$comments = $wpdb->get_results($q, OBJECT);	
 

foreach($comments as $comment){
   echo '<div class="new5">';
   echo '<a class="name" href="'.get_permalink($comment->post_ID).'">'.wordwrap(stripslashes($comment->post_title),40,' ',true).'</a>';
   echo '<div class="date">Всего комментариев: '.$comment->comment_count.'</div>
         <div class="clear"></div>   
         </div>';
}	
	
?>
  


 


    
  </div><!--END .content -->

 
<?php get_sidebar(); ?>	  
<?php get_footer(); ?>