<?php

/* @var $ExPolls ExPolls */
/* @var $wpdb wpdb */
$per_page = 5;
$comments_per_page = 10;
$poll = $ExPolls->getPollByIdOrSlug(get_query_var('expoll_slug'));
$poll->answers = $ExPolls->getPollAnswers($poll->ID);
$poll->answers_count = count($poll->answers);

$total_polls_count = $ExPolls->getPollsCount();
$pages = get_pagination($total_polls_count, get_query_var('expoll_page'), $per_page);
$polls = $ExPolls->getPolls($pages['start'], $per_page, ' AND `ID`!=' . $poll->ID);

$total_comments_count = $ExPolls->getPollComments(0, 10,
                ' AND cm.meta_key="expoll_id" AND cm.meta_value=' . (int) $poll->ID,
                ' LEFT JOIN ' . $wpdb->commentmeta . ' AS cm ON c.comment_ID = cm.comment_id',
                true
);
$comments_pages = get_pagination($total_comments_count, $_REQUEST['comments_page'], $comments_per_page);
$poll_comments = $ExPolls->getPollComments($comments_pages['start'], $comments_per_page,
                ' AND cm.meta_key="expoll_id" AND cm.meta_value=' . (int) $poll->ID,
                ' LEFT JOIN ' . $wpdb->commentmeta . ' AS cm ON c.comment_ID = cm.comment_id'
);

$polls_count = count($polls);
$voted = $ExPolls->checkVoted($poll->ID);

?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<h1 class="pd"><?php echo $poll->title ?></h1>
<div class="dotted"></div>
<p class="f13 grey2"><?php echo $poll->quest ?></p>

<div class="opr expoll_container">
<input name="expoll-id" type="hidden" value="<?php echo $poll->ID ?>" />
<?php if ($poll->state == 1 && !$voted): ?>
  <?php for ($i = 0; $i < $poll->answers_count; $i++): ?>
    <?php if (!$poll->multi): ?>
      <input name="expoll-answer" type="radio" value="<?php echo $poll->answers[$i]->ID ?>" />
    <?php else: ?>
      <input name="expoll-answer" type="checkbox" value="<?php echo $poll->answers[$i]->ID ?>" />
    <?php endif; ?>
    <?php echo $poll->answers[$i]->name ?><br />
  <?php endfor; ?>
  <br /><a class="ostav2 expoll_vote" href="javascript:void(0)" onclick="location.reload()">Проголосовать!</a>
<?php else: ?>
  <?php for ($i = 0; $i < $poll->answers_count; $i++): ?>
    <div class="fr"><?php echo (int) $poll->answers[$i]->percent ?>% <span class="grey">(<?php echo $poll->answers[$i]->vote_count ?> голоса)</span></div>
    <span class="f13"><?php echo $poll->answers[$i]->name ?></span>
    <div class="rank">  <div class="rank-on" style="width:<?php echo (int) $poll->answers[$i]->percent ?>%"></div></div>
  <?php endfor; ?>
  <div><br /><strong>Всего голосов: <?php echo $poll->vote_count ?></strong></div>
<?php endif; ?>
</div>

<div class="dotted"></div><?php comments_template('/custom_pages/expolls/comments.php'); ?>

<?php if ($polls_count): ?>
  <h5>Другие опросы</h5>
  <ul class="list prognoz f14">
    <?php for ($i = 0; $i < $polls_count; $i++): ?>
  	  <li><a href="<?php echo '/' . $ExPolls->url_prefix . '/' . $polls[$i]->slug ?>.html"><?php echo $polls[$i]->title ?></a></li>
    <?php endfor; ?>
  </ul>
<?php endif; ?>
