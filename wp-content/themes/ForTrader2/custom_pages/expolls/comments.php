
<?php
global $ExPolls, $poll_comments, $poll, $comments_pages;
// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
    die('Please do not load this page directly. Thanks!');

if (post_password_required ()) {
?>
    <p class="nocomments">Запись защищена паролем. Пожалуйста, введите пароль для ее просмотра.</p>
<?php
    return;
}
?>

<?php if (count($poll_comments)): ?>
  <br />
  <div class="title2">Комментарии</div>
  <?php wp_list_comments('callback=' . (!$poll && function_exists('expoll_comment_template3') ? 'expoll_comment_template3' : 'mytheme_comment'), $poll_comments); ?>
<?php endif; ?>

<?php if (count($comments_pages['pages'])>1): ?>
<div class="pages">
  <?php if ($comments_pages['prev']): ?>
    <a class="prev" href="<?php echo '/' . $ExPolls->url_prefix . '/' . $poll->slug . '.html?comments_page=' . $comments_pages['prev']['num'] ?>">Пред.</a>
  <?php endif; ?>
  <div class="pages2">
    <?php foreach ((array) $comments_pages['pages'] as $i): ?>
      <?php if ($i['url']): ?>
        <a href="<?php echo '/' . $ExPolls->url_prefix . '/' . $poll->slug . '.html/?comments_page=' . $i['num'] ?>"><?php echo $i['num'] ?></a>
      <?php else: ?>
        <a href="<?php echo '/' . $ExPolls->url_prefix . '/' . $poll->slug . '.html/?comments_page=' . $i['num'] ?>" class="on"><?php echo $i['num'] ?></a>
      <?php endif; ?>
      <?php if ($i['num'] < $comments_pages['total_pages']): ?>&bull;<?php endif ?>
    <?php endforeach; ?>
  </div>
  <?php if ($comments_pages['next']): ?>
    <a class="next" href="<?php echo '/' . $ExPolls->url_prefix . '/' . $poll->slug . '.html?comments_page=' . $comments_pages['next']['num'] ?>">След.</a>
  <?php endif; ?>
</div>
<?php endif; ?>

<!-- You can start editing here. -->

<?php if (comments_open ()) : ?>
  <div id="respond">
    <div class="cancel-comment-reply">
      <small><?php cancel_comment_reply_link(); ?></small>
    </div>
    <br /><br /><div class="title3">Добавить КОММЕНТАРИЙ</div><br />
    <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
      <p>Чтобы оставить комментарий, Вы должны <a href="<?php echo wp_login_url(get_permalink()); ?>">войти в систему</a>.</p>
    <?php else : ?>
    <?php if ($poll) : ?>
      <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
          <?php if (is_user_logged_in ()) : ?>
            <tr>
              <td width="23%">Имя</td>
              <td width="77%">Вы в системе как <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Выйти из системы">Выйти &raquo;</a></td>
            </tr>
          <?php else : ?>
            <tr>
              <td width="23%">Имя</td>
              <td width="77%"><input class="area1 inp2" type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> /></td>
            </tr>
            <tr>
              <td>E-mail</td>
              <td><input class="area1 inp2" type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> /></td>
            </tr>
          <?php endif; ?>
          <tr>
            <td>Комментарий</td>
            <td><textarea class="area" name="comment" id="comment" cols="58" rows="10" tabindex="4"></textarea></td>
          </tr>
          <tr>
            <td></td>
            <td>  <?php global $current_user; ?>
                  <?php if ($current_user->ID): ?>
                                  <input type="checkbox" id="expoll_comment_subscribe" name="expoll_comment_subscribe" value="1" /> <label for="expoll_comment_subscribe">Сообщать о дальнейших обсуждениях</label>
                  <?php endif; ?>
             </td>
          </tr>
        </table>
        <div class="dotted"></div>
        <input class="mnenie sub3" name="submit" type="submit" id="submit" tabindex="5" value="<?php _e('&#1044;&#1086;&#1073;&#1072;&#1074;&#1080;&#1090;&#1100;', 'kubrick'); ?>" />
        <?php comment_id_fields(); ?>
        <?php do_action('comment_form', $post->ID); ?>
        <input type="hidden" id="expoll_add_comment" name="expoll_add_comment" value="<?php echo $poll->ID ?>" />
        <input type="hidden" id="redirect_to" name="redirect_to" value="/<?php echo $ExPolls->url_prefix . '/' . $poll->slug ?>.html" />
      </form>
    <?php endif; ?>
  <?php endif; // If registration required and not logged in  ?>
</div>
<?php endif; // if you delete this the sky will fall on your head  ?>
