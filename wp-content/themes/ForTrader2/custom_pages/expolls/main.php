<?php
/* @var $ExPolls ExPolls */
$total_polls_count = $ExPolls->getPollsCount(' AND state!=2');
$pages = get_pagination($total_polls_count, get_query_var('expoll_page'), $per_page);
$polls = $ExPolls->getPolls($pages['start'], $per_page, ' AND state!=2');
$polls_count = count($polls);

$comments_per_page = 10;
$total_comments_count = $ExPolls->getPollComments(0, 10, null, null, true);
$comments_pages = get_pagination($total_comments_count, $_REQUEST['comments_page'], $comments_per_page);
$poll_comments = $ExPolls->getPollComments($comments_pages['start'], $comments_per_page,
                ' AND cm.meta_key="expoll_id"',
                ' LEFT JOIN ' . $wpdb->commentmeta . ' AS cm ON c.comment_ID = cm.comment_id'
                . ' LEFT JOIN ' . $ExPolls->table_prefix . 'polls AS pl ON cm.meta_value = pl.ID',
                false);
?>
<h1 class="point"><? echo the_title(); ?>. Архив голосования</h1>

<div class="comm2">
    Мы предлагаем вам оставить свой след в оценке личности и предпочтений трейдера, а также в
    обсуждении мировых событий. Вы всегда можете оставить свое мнение на предложенные вопросы,
    ну а мы  постараемся задавать их почаще, чтобы вам было интересно общаться.
</div>

<br />

<?php for ($i = 0; $i < $polls_count; $i++): ?>
<div class="quest">
      <div class="question">#<?php echo $polls[$i]->ID ?> <a href="<?php echo '/' . $ExPolls->url_prefix . '/' . $polls[$i]->slug ?>.html"><?php echo $polls[$i]->title ?></a></div>
        <div class="date"><?php echo strftime('%d/%m &bull; %H:%M', $polls[$i]->created); ?>bull; &</div>
        <div class="qwe"><?php echo $polls[$i]->desc ?></div>
      <div class="all2"><a href="<?php echo '/' . $ExPolls->url_prefix . '/' . $polls[$i]->slug ?>.html">Голосов: <?php echo $polls[$i]->vote_count ?></a></div>
    </div>
<?php endfor; ?>

<div class="pages">
  <?php if ($pages['prev']): ?>
    <a class="prev" href="<?php echo '/' . $ExPolls->url_prefix . '/page/' . $pages['prev']['num'] . '/' ?>">Пред.</a>
  <?php endif; ?>
  <div class="pages2">
    <?php foreach ($pages['pages'] as $i) {
              $pages_polls[] = '<a href="/'.$ExPolls->url_prefix.'/page/'.$i['num'].'/"'.($i['url'] ? '' : ' class="on"').'>'.$i['num'].'</a>';
          }
          if ($pages_polls) {
              echo implode(' &bull; ', $pages_polls);
          }
    ?>
  </div>
  <?php if ($pages['next']): ?>
    <a class="next" href="<?php echo '/' . $ExPolls->url_prefix . '/page/' . $pages['next']['num'] . '/' ?>">След.</a>
  <?php endif; ?>
</div>

<?php
/*
echo '<div class="clear" style="height:25px;"></div>';
comments_template('/custom_pages/expolls/comments.php');
*/