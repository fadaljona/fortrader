  <br />
  <h4>ВАШЕ МНЕНИЕ</h4>
  <?php
	include_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-content/plugins/expolls/expolls.php' ); 
	global $ExPolls;
	$user_ID = get_current_user_id();
	$poll = $ExPolls->getLastUnvotedPoll($user_ID);
	if($poll){
	$poll->answers = $ExPolls->getPollAnswers($poll->ID);
	$poll->answers_count = count($poll->answers);
	
?>
<div class="opr expoll_container_sidebar">
<div class="vopros"><?php echo $poll->title ?></div>
<div class="otvety">
			<input name="expoll-id" type="hidden" value="<?php echo $poll->ID ?>" />
			
			  <?php for ($i = 0; $i < $poll->answers_count; $i++): ?>
				<?php if (!$poll->multi): ?>
				  <input name="expoll-answer" type="radio" value="<?php echo $poll->answers[$i]->ID ?>" />
				<?php else: ?>
				  <input name="expoll-answer" type="checkbox" value="<?php echo $poll->answers[$i]->ID ?>" />
				<?php endif; ?>
				<?php echo $poll->answers[$i]->name ?><br />
			  <?php endfor; ?>  
</div>
<a class="sub2 ostav2 expoll_vote_from_sidebar" href="javascript:void(0)" onclick="submitclick();">Ответить</a>
<div class="all"><a href="/<?php echo $ExPolls->url_prefix;?>.html">Другие опросы</a></div>
</div>
<?php
}else{
?>
<div class="opr expoll_container_sidebar">
<div class="vopros">Вы прошли все опросы</div>
<div class="all"><a href="/<?php echo $ExPolls->url_prefix;?>.html">Другие опросы</a></div>
</div>
<?php
}
?>
<script>
function submitclick(){
        var container = (jQuery('.expoll_vote_from_sidebar').parent(jQuery('.expoll_container')).length) ? jQuery('.expoll_vote_from_sidebar').parent(jQuery('.expoll_container')) : jQuery('.expoll_vote_from_sidebar').parent().parent(jQuery('.expoll_container'))

        var button = jQuery('.expoll_vote_from_sidebar')

        var dataObject = {
            'expoll_poll_id': jQuery('.expoll_container_sidebar input[name="expoll-id"]').val()
        }


        if ( jQuery('.expoll_container_sidebar input:checked').length<1) {
            return false;
        }

        var i = 0
        jQuery('.expoll_container_sidebar input:checked').each(function(x,y) {
            dataObject['expoll_vote['+i+']'] = jQuery(y).val()
            i++
        })
        
        jQuery.post('/wp-content/plugins/expolls/expolls.php', dataObject, function(answer) {
            if (answer.error) {
                alert('Во время голосования произошла ошибка '+answer.error_no);
				console.log( answer );
                button.show()
            } else {
                //exit("<meta http-equiv='refresh' content='0; url= $_SERVER[PHP_SELF]'>");

				document.location.href = '/polls/'+answer.poll.slug+'.html';
            }
        }, 'json')

        if (i) {
            jQuery('.expoll_vote_from_sidebar').hide()
        }
}
</script>