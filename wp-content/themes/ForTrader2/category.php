<?php get_header(); ?>     

	<div class="content">
		<div class="article-info">
			<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
			</span>
			<?php 
				$cur_cat_id = get_cat_id( single_cat_title("",false) );
				global $category_breadcrumb_marker;
				$category_breadcrumb_marker=0;
				echo get_formated_category_parents( $cur_cat_id, true, true);
			?>
		</div>

  
     <h1 class="tl"><?php  single_cat_title(); ?></h1>
	 
	 <?php $chiled_cats = get_categories( array(
		'parent' => $cur_cat_id
	 ) ); 
	 if( count($chiled_cats) ) {
	 ?> 
	 
	 <div class="catagory_name_box clearfix">
		<?php 
		$cat_line=1;
		foreach( $chiled_cats as $chiled_cat ){ ?>
		<div class="catagory_name_item f_left">
			<a href="<?php echo get_category_link($chiled_cat->cat_ID);?>" class="catagory_name_link"><?php echo $chiled_cat->cat_name; ?></a>
		</div>
		<?php 
			if( $cat_line == 2){
				echo '<div class="clear"></div>';
				$cat_line=0;
			}
			$cat_line++;
		} ?>
	</div>
	<h2 class="title1">Все статьи категории</h2>
	 <?php } ?>
   
	<div class="news_catagory_box clearfix">
  <?php 
	$big_cat=0;
	if (have_posts()) : while (have_posts()) : the_post(); 

		switch ($big_cat) {
			case 0:
				$cat_class = 'w100';
				$post_imgs = p75GetThumbnailForCat($post->ID, 'big');
				$big_cat++;
				break;
			case 1:
				$cat_class = 'w50';
				$post_imgs = p75GetThumbnailForCat($post->ID, 'small');
				$big_cat++;
				break;
			case 2:
				$cat_class = 'w50';
				$post_imgs = p75GetThumbnailForCat($post->ID, 'small');
				$big_cat=0;
				break;
		}
	?> 
	
	
		<div class="news_catagory_item f_left <?php echo $cat_class;?>">
			<div>
				<div class="news_catagory_img">
					<a href="<?php the_permalink(); ?>"><img src="<?php echo $post_imgs; ?>" alt="<?php the_title(); ?>" /></a>
				</div>
				<div class="title-wrapper">
					<a href="<?php the_permalink(); ?>" class="news_catagory_title"><?php the_title(); ?></a> 
					<?php if( get_comments_number() ){?><a class="comm-count" href="<?php the_permalink(); ?>#coments_tabs_box"><?php comments_number( '', '(1)', '(%)' ); ?></a><?php } ?>
				</div>
				<div>
					<?php
						$cat_name = '';
						$cat_url = '';
						$catmeta = get_post_meta($post->ID, '_category_permalink', true);
						if( isset ( $catmeta ) && $catmeta ){
							$category_meta = get_category($catmeta);
							$cat_name = $category_meta->cat_name;
							$cat_url = get_category_link($category_meta->cat_ID);
						}else{
							$post_categories = wp_get_post_categories( $post->ID );
							sort($post_categories);
							$category_def = get_category($post_categories[0]);
							$cat_name = $category_def->cat_name;
							$cat_url = get_category_link($post_categories[0]);
						}
					?>
					<a href="<?php echo $cat_url;?>" class="catagory_it"><?php echo $cat_name;?></a>
					<div class="date d_ib pr10"><?php the_time('d/n ٠ G:i'); ?></div>
					<div class="user_post_coment_box d_ib">
						<img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"><?php echo get_post_meta ($post->ID,'views',true); ?>
					</div>
				</div>
			</div>
		</div>
		
  <?php /*$i++; if ($i < 4) { ?>
  
    <div class="new4">
    <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(99,99), array('class' => 'pic')); } 
	  else {?>
      <img class="pic" src="<?php echo p75GetThumbnail($post->ID, 99, 99, ""); ?>"  
      alt="<?php  single_cat_title(); ?> - <?php the_title(); ?>" 
      title="<?php  single_cat_title(); ?> - <?php the_title(); ?>" /> 
	  <? } ?></a> 
    <div class="name_h2"><a class="name" href="<?php the_permalink(); ?>">
	<?php  if($i==1){ ?><?php  } ?> <?php the_title(); ?></a> 
	<?php if( get_comments_number() ){?><a class="comm-count" href="<?php the_permalink(); ?>#coments_tabs_box"><?php comments_number( '', '(1)', '(%)' ); ?></a><?php } ?>
    </div>
    <div class="date"><?php the_time('d/n ٠ G:i'); ?>
     |   <img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"/><?php echo get_post_meta ($post->ID,'views',true); ?></div>
    <div class="txt2">
    <?php  if($i==3){ ?>
    <em><?php  single_cat_title(); ?>:</em>
    <?php  } ?>
    <?php the_excerpt2($post, 30); ?></div> 
    <br />
    
    </div>
    
  <?php } else { ?>
  
     <div class="new5">
    <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(59,59), array('class' => 'pic')); } 
	  else {?>
      <img class="pic" src="<?php echo p75GetThumbnail($post->ID, 80, 60, ""); ?>" 
      alt="<?php  single_cat_title(); ?> - <?php the_title(); ?>" 
      title="<?php  single_cat_title(); ?> - <?php the_title(); ?>" /> <? } ?></a>
    <a class="name" href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
	<?php if( get_comments_number() ){?><a class="comm-count" href="<?php the_permalink(); ?>#coments_tabs_box"><?php comments_number( '', '(1)', '(%)' ); ?></a><?php } ?>
    <div class="date"><?php the_time('d/n ٠ G:i'); ?>
    |   <img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"/><?php echo get_post_meta ($post->ID,'views',true); ?></div>
    <div class="clear"></div>   
    </div>
 
    
	<?php }*/ ?>
  
	
  
	<?php endwhile;  ?>
	</div>
  
	<?php get_template_part('nav'); else : endif; wp_reset_query(); ?><br />
  
<br />
<br />
  
    <div class="txt5">

	<?php
if ( template_get_page_number()==0 ):
preg_match ('!\[description=(.*)\]!iU', category_description (), $match);
$out = $match[1] ? $match[1] : '';
if ($out=='') {	?> <h2 class="tl">О Разделе "<?php single_cat_title(); ?>"</h2> <?php
echo category_description ();} else {echo $out;}
endif;
?>
    
    </div>
    
  </div>
<!--END .content -->

 
<?php get_sidebar(); ?>	  
<?php get_footer(); ?>
