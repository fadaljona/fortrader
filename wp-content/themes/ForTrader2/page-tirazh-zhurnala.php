<?
get_header();

if (have_posts ()) {
    while (have_posts ())
        the_post();
} else {
    include 'page-tirazh-zhurnala_404.php';
    exit();
}

if (!function_exists('get_pagination') || !function_exists('magazine_circulation_prepare')) {
    include 'page-tirazh-zhurnala_404.php';
    exit();
}

$category_id = 1064;
$per_page = 100;
$category = get_category($category_id);
$count = $category->category_count;

if (!$count) {
    include 'page-tirazh-zhurnala_404.php';
    exit();
}

$_REQUEST['page'] = get_query_var('paged');
$pages = get_pagination($count, $_REQUEST['page'], $per_page);
$magazines = query_posts('posts_per_page='.$per_page.'&offset='.$pages['start'].'&cat=' . $category_id);

//prepare our data
magazine_circulation_prepare($magazines);

echo '<div class="content">';

?>

    <h1 class="titl3"><?php the_title(); ?></h1>
	<div class="category">	
<p>В разделе «Тираж журнала» представлено количество скачанных экземпляров различных номеров журнала для форекс трейдеров ForTrader.ru. Познакомившись с данной статистикой, вы можете скачать номер биржевого журнала или же обратиться к его содержанию и прочитать интересующие вас форекс статьи на сайте.
</p>
	</div>
	
    <table width="100%" border="0" cellspacing="9" cellpadding="0" class="jurnal">
        <tr>
            <td width="12%">&nbsp;</td>
            <td width="69%" class="publ">Публикация</td>
            <td class="ekz" width="19%">Скачано</td>
        </tr>
    <?php $now_count = count($magazines); ?>
    <?php $i = 1; ?>
    <?php foreach ($magazines as $magazine): ?>
        <tr <?php if ($i == $now_count) { echo 'class="last"'; }?>>
            <td>
                <a href="<?php echo get_permalink($magazine->ID)?>">
                    <img src="<?php echo p75GetThumbnail($magazine->ID, 76, 102)?>" alt="<?php echo $magazine->title; ?>. Читать онлайн" title="<?php echo $magazine->title; ?>. Читать онлайн" width="76" height="102"/>
                </a>
            </td>
            <td class="opys">
                <h4><?php echo $magazine->title; ?></h4>
                Выпуск №<?php echo $magazine->num; ?>, <?php echo $magazine->date; ?><br />
                <a href="<?php echo $magazine->pdf ?>">Скачать номер</a>&nbsp;|&nbsp;
                <a href="<?php echo get_permalink($magazine->ID)?>">Читать он лайн</a>
            </td>
            <td class="ekz"><?php echo $magazine->downloads; ?><br />экземпляров</td>
        </tr>
    <?php $i ++; ?>
    <?php endforeach; ?>
    </table>
    <br />
    <?php
    $tmp_wp_query = $wp_query;
	$wp_query->found_posts = $count;
	$wp_query->max_num_pages = $pages['total_pages'];
    set_query_var('paged', $_REQUEST['page']);
    ?>
    <div class="pg"><?php  kama_pagenavi(); ?></div>
    <?php
    $wp_query = $tmp_wp_query;
    ?>
<br /><div class="clear"></div><div class="dotted"></div></div>


<?php
get_sidebar();
get_footer();
