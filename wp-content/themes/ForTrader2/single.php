<?php get_header(); ?>
<?php
	$category = get_the_category();
	$wrap_itemtype = "http://schema.org/Article";
	foreach( $category as $cat ){
		if( $cat->cat_ID == 24 ){
			$wrap_itemtype = "http://schema.org/ScholarlyArticle";
		}
	}
?>
<div class="content text" itemscope itemtype="<?php echo $wrap_itemtype;?>">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   
     <div class="article-info">
		<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<a href="/" itemprop="url"><span itemprop="title">Главная</span></a>
		</span>
		<? 
			$category = get_the_category();
			$articleSection = '';
			$catmeta = get_post_meta($post->ID, '_category_permalink', true);
			if( isset ( $catmeta ) && $catmeta ){
				$category_meta = get_category($catmeta);
				echo get_formated_category_parents( $catmeta, true);
				$articleSection = $category_meta->cat_name;
			}else{
				$iter=1;
				foreach( $category as $breadcrumb_cat ){
					echo '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> → <a title="'.$breadcrumb_cat->cat_name.'" href="'.get_category_link( $breadcrumb_cat->cat_ID ).'" itemprop="url"><span itemprop="title">'.$breadcrumb_cat->cat_name.'</span></a></span>';
					if( $iter )
						$articleSection .= $breadcrumb_cat->cat_name;
					else
						$articleSection .= ', '.$breadcrumb_cat->cat_name;
					$iter=0;
				}
			
			}
			
			
			rsort($category);
			$cat_add_id = $category[0]->term_id;
		?>
	</div>
	
	<a itemprop="discussionUrl" style="display:none;" href='<?php the_permalink();?>#coments_tabs_box'><?php the_permalink();?>#coments_tabs_box</a>
	<img itemprop="image" style="display:none;" src="<?php echo p75GetThumbnail($post->ID, 400, 300, ""); ?>"></img>
	<meta itemprop="commentCount" content="<?php comments_number( '0', '1', '%' ); ?>" />
	<meta itemprop="articleSection" content="<?php echo $articleSection;?>" /> 
	<meta itemprop="datePublished" content="<?php the_time('Y-m-d h:m');?>" /> 
    
	<?php /*if( isset( $_GET['comments'] ) ){ ?>
	
		<h1 class="pd"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 

		</h1>
	
	<?php }else{*/ ?>
	
		<h1 class="pd"><span itemprop="headline name"><?php the_title(); ?> </span>
		<?php if( get_comments_number() ){?><a class="comm-count" href="<?php the_permalink(); ?>#coments_tabs_box"><?php comments_number( '', '(1)', '(%)' ); ?></a><?php } ?>
		</h1>
	
	<?php //} ?>

     <?php $pageUrl = get_permalink();
					//  $pageDescription=kama_meta_description('',80,1); 
					//  $text=$pageDescription;
					  $pageImage=p75GetThumbnail($post->ID, 60, 60, "");?>  
                      
   	<div class="social-buttons">
		<span class="t"></span>
		<span class="b"></span>
		<table>
			<tbody><tr>
				<td>
     <div class="article-info"  style="width: 440px;">
     	<span itemprop="author"><?php the_author_posts_link(); ?></span>
		<div class="meta">
			<span class="views"><i></i><b><img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"/>
			<?php echo get_post_meta ($post->ID,'views',true); ?></b>
			</span>
			<span class="date" ><i></i><b><img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/date.png"/>
			<time><?php the_time('d.m.Y'); ?></time></b>
			</span>
		</div>
	</div>
				</td>
			</tr>
		</tbody></table>
	</div>
	
	
	
	
	<?php
		/*if( isset( $_GET['comments'] ) ){
			require_once( 'custom-comments/comments-full.php' );
		}else{*/
	?>
	
	
	
	
	
	
	
	
	
	
	<?php $compil_url = get_post_meta($post->
	ID, 'compil_url', true); if ($compil_url) { ?> <br/>
	Материал: <span><?php echo $compil_url; ?></span><?php } ?>
	<!--div class="dotted">
	</div-->
	
	<?php
		$meta_description = get_post_meta( $post->ID, '_yoast_wpseo_metadesc', true );
		if( isset ( $meta_description ) && $meta_description ){
			echo '<div class="article-lead" itemprop="description">'.$meta_description.'</div> <br />';
		}
	?>
	
	<div class="cnt news_body" itemprop="articleBody">
		<?php the_content(); ?>
	</div>

<span class="cat-links"  itemprop="articleSection"><p><?php single_cat_title('В рубрике:'); ?></p></span>
<?php $ist = get_post_meta($post->
		ID, 'ist', true); if ($ist) { ?>	<div class="copy"> ПО МАТЕРИАЛАМ <span><?php echo $ist; ?> </span> 	</div><?php } ?>
	               <?php  if (!$ist) { ?> <div class="copy"> <a target="_blank" href ="http://fortrader.ru"><img src="http://files.fortrader.ru/uploads/2015/01/mark-wite1.png" > </a> </div><?php } ?>


	<?php $compil_text = get_post_meta($post->ID, 'compil_text', true); if ($compil_text) 
	{ ?><div class="copy"><blockquote>/<?php echo $compil_text; ?>/</blockquote></div>
	<?php } ?>
	<div class="clear"></div>

    
<div class="social-buttons">
		<span class="b"></span>
		<table  style="top:2px;">
			<tbody><tr>
				<td class="with-padding" style="font-size: 0.8em;">

<?php /* <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,gplus" data-yashareTheme="counter" params.elementStyle.border="true" ></div>*/?>
<div>
<script type="text/javascript" src="//yandex.st/share/share.js"
charset="utf-8"></script>
<div style="float:left" class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="yaru,vkontakte,facebook,twitter" data-yashareTheme="counter"></div>
<div style="float:left"><div class="g-plusone" data-annotation="none"></div></div>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
{lang: 'ru'}
</script>
<div style="clear:both"></div>
</div> 

				</td>
			</tr>
		</tbody></table>
	</div>

	<div class="copi">Разрешается использовать материалы, размещенные на сайте журнала FORTRADER.ru на сторонних интернет-сайтах, порталах и печатных СМИ в любом возможном варианте на условиях размещения индексируемой ссылки на сайт fortrader.ru(для интернет-источников).</div>
	
	<!--subscribe-form-->
	 <div class="subscribeform">
	<div id="mlb2-407543" class="ml-subscribe-form">
	<div class="ml-vertical-align-center">

    <div class="subscribe-form ml-block-success" style="display:none">

        <div class="form-section">

            <h4></h4>
            <p>Спасибо! Вы успешно подписаны на рассылку самых интересных статей из мира трейдинга!</p>

        </div>
    </div>


    <form class="ml-block-form" action="https://app.mailerlite.com/webforms/submit/h0b5s1" data-code="h0b5s1" method="POST" target="_blank">

    <div class="subscribe-form">

        <div class="form-section">
        </div>

        <div class="form-section">
        <div class="form-group ml-field-email ml-validate-required ml-validate-email">
                        <input type="text" name="fields[email]" class="form-control" placeholder="Email*" value="">
        </div>
        </div>





        <input type="hidden" name="ml-submit" value="1" />
        <button type="submit">Подписаться на новости fortrader.org →</button>

    </div>

    </form>



</div>
</div>


    <script type="text/javascript" src="//mailerlite.com/js/w/webforms.js?v2"></script>
	<!--subscribe-form end -->
    </div>

   
<div class="abtitle">Рекламa</div>
<div class="block1">   
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 463x250 1 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-6643182737842994"
     data-ad-slot="3913636746"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>

<?php 
	/*if( ! isset( $_GET['comments'] ) )
		require_once( 'custom-comments/for-full-post.php' );*/
	?>

<div class="columnSingle">
                        <h5>Другие статьи по теме</h5>
				<?php /*
				$mycat=get_the_category(); $mycat=$mycat[0];
				
					$postslist = get_posts('numberposts=4&orderby=rand&category='.$mycat->cat_ID.'&exclude='.get_the_ID()); foreach ($postslist as $post) : setup_postdata($post);
				?>
				<div class="new2">
					<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(60,60), array('class' => 'pic4')); } 
					else {?><img class="pic4" src="<?php echo p75GetThumbnail($post->ID, 60, 40, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> <? } ?>
					</a>  
					<b><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></b>
                                        <span class="date">|  <img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"/><?php echo get_post_meta ($post->ID,'views',true); ?></span>
					<div class="clear"></div>
				</div> 
				<?php endforeach; ?>     


			<?php 
					$postslist = get_posts('numberposts=4&order=DESC&category='.$mycat->cat_ID.'&exclude='.get_the_ID()); foreach ($postslist as $post) : setup_postdata($post);
				?>
				<div class="new2">
					<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(60,60), array('class' => 'pic4')); } 
					else {?><img class="pic4" src="<?php echo p75GetThumbnail($post->ID, 60, 40, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> <? } ?>
					</a>  
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        <span class="date"><?php the_time('d/n ٠ G:i'); ?></span>
					<div class="clear"></div>
				</div> 
				<?php endforeach; */?> 
				

				<?php

				$cat_rec_ids=array(11737, 9148, 11735, 641, 11723, 4343, 1, 5, 11729, 11730, 11731, 11732); //id категорий к которым нужны последние статьи вместо рандомных
				$number_posts=4;
				
				$random_marker='rand';
				if( is_single()) {
					$categories=get_the_category( $GLOBALS['post']->ID );
					$category=array();
					foreach( $categories as $cat ){
						$category[]=$cat->cat_ID;
					}
					$result_compare = array_intersect($category, $cat_rec_ids);
					if (!empty($result_compare)) $random_marker='date';
				}
				$r = new WP_Query( array(
					'posts_per_page'      => $number_posts,
					'no_found_rows'       => true,
					'post_status'         => 'publish',
					'ignore_sticky_posts' => true,
					'orderby'             => $random_marker,
					'category__in'        => $category,
					'post__not_in'        => array(get_the_ID()),
				)  );

				if ($r->have_posts()) :

				 while ( $r->have_posts() ) : $r->the_post(); ?>
					<div class="new2">
						<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(60,60), array('class' => 'pic4')); } 
					else {?><img class="pic4" src="<?php echo p75GetThumbnail($post->ID, 60, 40, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> <? } ?></a>
					<b><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
					<?php if( get_comments_number() ){?><a class="comm-count" href="<?php the_permalink(); ?>#coments_tabs_box"><?php comments_number( '', '(1)', '(%)' ); ?></a><?php } ?>
					</b>
                                        <span class="date">|  <img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"/><?php echo get_post_meta ($post->ID,'views',true); ?></span>
					</div>
				<?php endwhile;
				wp_reset_postdata();
				endif;?>
				
				
				
				
				
<div class="clear" style="padding-top:25px"></div>  
</div><!--END .columnSingle-->

	
	<div class="columnSingle">
		<h5>Рекомендованные статьи</h5>
		
		<?php
		$number_posts=4;
		
		$current_date1 = new DateTime("now");
		$currentdate1=$current_date1->format('Y-m-d H:i:s');
		 
		$r = new WP_Query( array(
			'posts_per_page'      => $number_posts,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true,
			'post__not_in'        => array(get_the_ID()),
			'meta_query' => array(
							array(
								'key'     => 'important',
								'value'   => '1',
								'compare' => '=',
							),
							array(
								'key'     => 'daylife',
								'value'   => $currentdate1,
								'compare' => '>',
							),
			),
			
			
		) );
?>
    <div class="col col-xxl" id="top-picks">
        <div class="top-picks-items">
<?php
		if ($r->have_posts()) :

				  while ( $r->have_posts() ) : $r->the_post(); ?>
					<?php/*<div class="new2">
						<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(60,60), array('class' => 'pic4')); } 
					else {?><img class="pic4" src="<?php echo p75GetThumbnail($post->ID, 60, 40, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> <? } ?></a>
					<b><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></b>
                                        <span class="date">|  <img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"/><?php echo get_post_meta ($post->ID,'views',true); ?></span>
					</div>*/?>
					
					
					
					<div class="top-picks-item">
                        <div class="top-picks-wrapper">
                            <a href="<?php the_permalink(); ?>">
                               <?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(220,146), array('class' => 'pic4')); } 
					else {?><img src="<?php echo p75GetThumbnail($post->ID, 220, 146, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> <? } ?>
                            </a>
                            <h3 class="fauxh5-item-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
								<?php if( get_comments_number() ){?><a class="comm-count" href="<?php the_permalink(); ?>#coments_tabs_box"><?php comments_number( '', '(1)', '(%)' ); ?></a><?php } ?>
                            </h3>
                        </div>
                    </div>
					
					
					
					
				<?php endwhile;
				wp_reset_postdata();
				endif;?>
				
		</div>
    </div>
							
	<div class="clear" style="padding-top:25px"></div>  
	</div><!--END .columnSingle-->


	 <div class="clear" style="padding-top:17px"></div>

	<?php endwhile; else : ?>
	<h2 class="center">Не найдено</h2>
	<p class="center">
		Такой страницы не существует
	</p>
	<?php endif; ?>
	<br/>
    
	<div class="clear"></div>
	<div class="dotted"></div>
	



<script type="text/javascript" src="<?php echo get_home_url().'/services'.Yii::app()->createUrl('advertisementZone/jsRender', array( 'id' => 2 )); ?>"></script>
	
	<!--/* OpenX iFrame Tag v2.8.7 *
<iframe id='a92b0f5c' name='a92b0f5c' src='http://adv.advstatus.ru/www/delivery/afr.php?zoneid=40&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;ct0=INSERT_CLICKURL_HERE' frameborder='0' scrolling='no' width='460' height='180'><a href='http://adv.advstatus.ru/www/delivery/ck.php?n=aa5f7d8b&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://adv.advstatus.ru/www/delivery/avw.php?zoneid=40&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=aa5f7d8b&amp;ct0=INSERT_CLICKURL_HERE' border='0' alt='' /></a></iframe>
/-->

	<br/>
    <?php  //the_smli(); ?>
	<!--strong style="font-size: 15px;">Популярные запросы посетители нашего сайта: </strong-->


  
	<div class="title2">
<?php // the_category(', '); ?>
	</div>

	<?php require_once( 'custom-comments/comments-full.php' );?>

	<br/>
	<br/>

</div>
<!--END .content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
