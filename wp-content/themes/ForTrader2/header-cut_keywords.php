<?php header("Last-Modified: " . date('r', strtotime($post->post_modified))); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

		<!-- Update your html tag to include the itemscope and itemtype attributes. -->
		<html itemscope itemtype="http://schema.org/Article">

		<!-- Add the following three tags inside head. -->
		<meta itemprop="name" content="Журнал для трейдеров">
		<meta itemprop="description" content="Интересная информация по биржевой торговле">
		<meta itemprop="image" content="http://fortrader.ru/wp-content/themes/ForTrader2/img/logo.png">

        <title><?php if (function_exists('wp_head')): //wp  ?><?php kama_meta_title(); ?> <?php elseif (function_exists('bb_head')): //forum ?><?php bb_title() ?><?php endif; ?></title>
		<?php if (function_exists('kama_meta_keywords') && !is_category() ): //wp ?> <?php kama_meta_keywords(); ?><? endif; ?>
		<?php if (function_exists('kama_meta_description')): //wp ?> <?php kama_meta_description(); ?><? endif; ?>

		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?rnd=<?=2?>" type="text/css" media="screen" />
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="http://fortrader.ru/rss/rss_all.php" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		
		<?php //wp_deregister_script('jquery'); ?>
		<?php wp_head(); ?>
		
		<!--script src="http://quote.rbc.ru/static/v2/js/jquery.js" type="text/javascript"></script-->
		<!--script type='text/javascript' src='/wp-includes/js/jquery/jquery.js'></script-->
		<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script-->
		<script  type='text/javascript' src='/wp-includes/js/jquery/validate.js'></script>
		<script   type='text/javascript' src='/wp-includes/js/jquery/validate.messages_ru.js'></script>
		<script  type="text/javascript" src="/wp-includes/js/ajax_expolls.js"></script>

		<style>
			input.error, select.error, textarea.error {
				border:solid 1px #900;
			}
			label.error {
				color:#900;
			}
		</style>
		<meta name="alpari_partner" content="1209762" />
	</head>
	<body>
		<?php /* new year (расскоментировать)>
		<div style="background:url(<?php bloginfo('template_directory'); ?>/img/nybg.png) top center repeat-x">
		<new year */?>
		<div class="reklup">Реклама</div>
		<div class="rekl940">
			<!--/* OpenX iFrame Tag v2.8.10 */-->
			<iframe id='a3d1bb34' name='a3d1bb34' src='http://adv.advstatus.ru/www/delivery/afr.php?zoneid=12&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;ct0=INSERT_CLICKURL_HERE' frameborder='0' scrolling='no' width='930' height='60'><a href='http://adv.advstatus.ru/www/delivery/ck.php?n=a48e0d1f&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'  rel='nofollow'><img src='http://adv.advstatus.ru/www/delivery/avw.php?zoneid=12&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a48e0d1f&amp;ct0=INSERT_CLICKURL_HERE' border='0' alt=''  rel='nofollow'/></a></iframe>
		</div>
		
		<?php /* bantik (расскомментировать)>
		<div class="wrap" style="background:none !important">
		<img style="position:absolute; float:left; top:160px; margin-left:-16px" src="<?php bloginfo('template_directory'); ?>/img/bant.png" alt="" />
		<div class="top" style="border-bottom:1px dotted #000">
		<bantik */?>
		
		<div style="position:fixed;bottom:75px; width:100%;">
			<div style="width:960px; position:relative;margin:0 auto;">
				<div style="position:absolute; right:100%;bottom:0;">
					<script type="text/javascript" src="<?php echo get_home_url().'/services'.Yii::app()->createUrl('advertisementZone/jsRender', array( 'id' => 20 )); ?>"></script>
				</div>
				<div style="position:absolute; left:100%;bottom:0;">
					<script type="text/javascript" src="<?php echo get_home_url().'/services'.Yii::app()->createUrl('advertisementZone/jsRender', array( 'id' => 21 )); ?>"></script>
				</div>
			</div>
		</div>
		<style>
			.ft_adZone_20, .ft_adZone_21{
				padding: 5px !important;
			}
		</style>
		
		<!--закоментировать между-->
		<div class="wrap" style="position:relative; z-index:1;">
			<div class="top" >
				<!--закоментировать этим-->
				<ul class="top-menu fr">
					<?php if (!(current_user_can('level_0'))){ ?>   
						<!--li class="spc"></li-->
						<li><a href="<?php  bloginfo('home'); ?>/wp-login.php">Вход</a></li> 
					<?php } else { ?>  
						<li>Привет, <?php global $user_identity; echo $user_identity; ?></li>  
						<li class="spc"></li>
						<li><?php wp_loginout(); ?></li> 
					<?php }?> 
				</ul>
				<div class="podil">
					<div class="social_links" style="float: right;">
						<a href="http://fortrader.ru/link/link.php?id=4226977" rel="nofollow" target="_blank"><img alt="RSS" src="http://files.fortrader.ru/icon/icon_feed.png"></a>
						<a href="http://fortrader.ru/link/link.php?id=3" rel="nofollow" target="_blank"><img alt="Twitter" src="http://files.fortrader.ru/icon/icon_twitter.png"></a>
						<a href="http://fortrader.ru/link/link.php?id=2" rel="nofollow" target="_blank"><img alt="Facebook" src="http://files.fortrader.ru/icon/icon_facebook.png"></a>
						<a href="http://fortrader.ru/link/link.php?id=1" rel="nofollow" target="_blank"><img alt="vkontakte" src="http://files.fortrader.ru/icon/icon_vkontakte.png"></a>
					</div>
				</div>
				<ul class="top-menu">
					<li><a href="<?php bloginfo('home'); ?>/about.html" title="О журнале">О журнале</a></li>
					<li class="spc"></li>
					<li><a href="<?php bloginfo('home'); ?>/autors.html" title="Авторам">Авторам</a></li>
					<li class="spc"></li>
					<li><a href="<?php bloginfo('home'); ?>/contact.html" title="Контакты">Контакты</a></li>
					<li class="spc"></li>
					<li><a href="<?php bloginfo('home'); ?>/risk.html" title="Предупреждение о риске">О риске</a></li>
					<li class="spc"></li>
					<li><a href="<?php bloginfo('home'); ?>/reklama.html" title="Реклама">Реклама</a></li>
					<li class="spc"></li>
				</ul>
			</div><!--END .top -->
			<table width="100%" border="0">
				<tr>
					<td>  <a href="<?php bloginfo('home'); ?>"><img class="logo" src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="ForTrader" /></a></td>
					<td>
						<div class="banr">
							<!--/* OpenX iFrame Tag v2.8.10 */-->
							<iframe id='a869ee3e' name='a869ee3e' src='http://adv.advstatus.ru/www/delivery/afr.php?zoneid=5&amp;cb=<?php echo time(); ?>&amp;ct0=INSERT_CLICKURL_HERE' frameborder='0' scrolling='no' width='468' height='60'>
							<a href='http://adv.advstatus.ru/www/delivery/ck.php?n=ae9cd320&amp;cb=<?php echo time(); ?>' target='_blank' rel='nofollow'><img src='http://adv.advstatus.ru/www/delivery/avw.php?zoneid=5&amp;cb=<?php echo time(); ?>&amp;n=ae9cd320&amp;ct0=INSERT_CLICKURL_HERE' border='0' alt=''  rel='nofollow'/></a></iframe>
							<!--script type="text/javascript" src="<?php echo get_home_url().'/services'.Yii::app()->createUrl('advertisementZone/jsRender', array( 'id' => 8 )); ?>"></script-->
						</div>
					</td>
					<td align="center" valign="middle"> 
						<!-- Put this div tag to the place, where the Like block will be -->
						<!--form method="get" id="searchform" action="">
						<input value="" class="search" name="s" id="s" type="text" />
						<input type="submit" id="searchsubmit" style="display: none;" value="Search" />
						</form-->
					</td>
					<td align="center" valign="middle">
						<br />
						<br />
					</td>
				</tr>
			</table>
			<div class="clear"></div>
			<?php wp_nav_menu(array('menu' => 'menu')); ?>
			<div class="clear"></div>
			<!--
			<div style="padding-bottom:10px">
				<img style="border-width:0px;margin-right: 4px; margin-bottom: -3px; padding-left:10px;" src="http://files.fortrader.ru/icon/ico_robots1.jpg" height="16" width="16"/> <b ><a href="http://fortrader.ru/services/ea/list/">Мониторинг советников </a></b>
				<img style="border-width:0px;margin-right: 4px; margin-bottom: -3px; padding-left:10px" src="http://files.fortrader.ru/icon/contest.gif" height="16" width="16"/> <b ><a href="http://fortrader.ru/services/contest/list/">Форекс конкурсы</a></b>
            </div>
			-->
			
			<!--
			<a href="/services/journal/last" class="iA i00 i01">Журнал</a>
			<a href="/services/ea/list" class="iA i00 i02">Советники</a>
			<a href="/services/" class="iA i00 i03">Чат</a>
			<a href="/services/broker/list" class="iA i00 i04">Брокеры</a>
			<a href="/services/contest/list" class="iA i00 i05">Конкурсы</a>
			-->
			
			<a href="/services/journal/last" class="iA i01 i01_1"><i></i>Журнал</a>
			<a href="/services/ea/list" class="iA i01 i01_2"><i></i>Советники</a>
			<!--<a href="/services/" class="iA i01 i01_3"><i></i>Чат</a>-->
			<a href="/services/broker/list" class="iA i01 i01_4"><i></i>Брокеры</a>
			<a href="/services/contest/list" class="iA i01 i01_5"><i></i>Конкурсы</a>
			<a href="/services/calendarEvent/list" class="iA i01 i01_6"><i></i>Экономический календарь</a>
			
			<div class="clear" style="padding-top:10px;"></div>
			
			<div class="reklup">Реклама</div>
			<div class="rekl940x50">
				<!--/* OpenX iFrame Tag v2.8.10 */-->
				<iframe id='aa476674' name='aa476674' src='http://adv.advstatus.ru/www/delivery/afr.php?zoneid=9&amp;cb=<?php echo time(); ?>&amp;ct0=INSERT_CLICKURL_HERE' frameborder='0' scrolling='no' width='930' height='60'>
				<a href='http://adv.advstatus.ru/www/delivery/ck.php?n=a484030b&amp;cb=<?php echo time(); ?>' target='_blank' rel='nofollow' >
					<img src='http://adv.advstatus.ru/www/delivery/avw.php?zoneid=9&amp;cb=<?php echo time(); ?>&amp;n=a484030b&amp;ct0=INSERT_CLICKURL_HERE' border='0' alt=''/>
			   </a></iframe>
			</div>
			<!-- Put the following javascript before the closing </head> tag. -->
			
			<script>
				(function() {
					var cx = '015824500756150665004:koszn_wgu4c';
					var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
					gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
				})();
			</script>
			<!-- Place this tag where you want the search box to render -->
			<gcse:searchbox-only></gcse:searchbox-only>