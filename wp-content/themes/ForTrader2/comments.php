<?php

	if (isset($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');
	
	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', 'kubrick'); ?></p> 
	<?php
		return;
	}
?>








<?php if ( comments_open() ) : ?>

<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>

<?php else : ?>


<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

  <div class="title3">Добавить КОММЕНТАРИЙ</div><br />
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form">
 
<?php if ( is_user_logged_in() ) : ?>

  <tr>

    <td width="23%">Имя</td>
    <td width="77%">Вы в системе как <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Выйти из системы">Выйти &raquo;</a></td>

  </tr>



<?php else : ?>


   

  <tr>

    <td width="23%">Имя</td>
    <td width="77%"><input class="inp2" type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> /></td>

  </tr>


  <tr>

    <td>E-mail</td>

    <td><input class="inp2" type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> /></td>

  </tr>

<?php endif; ?>

  <tr>

    <td>Комментарий</td>

    <td><textarea class="area" name="comment" id="comment" cols="58" rows="10" tabindex="4"></textarea></td>

  </tr>

</table>


 <div class="dotted"></div>
   
   <input name="submit" type="submit" class="sub3" alt="Submit Comment" id="comment-submit" tabindex="5" value="Добавить" />
   
   

<?php comment_id_fields(); ?> 



<?php do_action('comment_form', $post->ID); ?>



</form>



<?php endif; // If registration required and not logged in ?>




<?php endif; // if you delete this the sky will fall on your head ?>

 

<?php if ( have_comments() ) : ?>

  <div class="title2">Комментарии</div>

 <?php else : // this is displayed if there are no comments so far ?>


	<?php if ( comments_open() ) : ?>

		
	<?php endif; ?>

<?php endif; ?>

<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>

<p>Чтобы оставить комментарий, Вы должны <a href="<?php echo wp_login_url( get_permalink() ); ?>">войти в систему</a>.
</p>
	<?php endif; ?>

<?php wp_list_comments('callback=mytheme_comment'); ?>  

<div class="nav_url"></div>
