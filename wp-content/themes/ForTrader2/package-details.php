<?php get_header(); ?>
<div class="content text">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="article-info">
		<? the_category(' → ', 'multiple'); ?>
		<div class="meta">
			<span class="views"><i></i><b><img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"/><?php echo get_post_meta ($post->
			ID,'views',true); ?></b></span>
			<span class="date"><i></i><b><img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/date.png"/><?php the_time('d.m.Y'); ?>
			 , <?php the_time('G:i'); ?>
			</b></span>
		</div>
	</div>
	<h1 class="pd"><?php echo $wpdm_package_title; ?>
	</h1>
	<div class="authors">
		<div class="short">
			<span><?php the_author_posts_link(); ?>
			</span>
		</div>
		<div data-author-id="38435" class="detail hidden">
			<div class="hr">
			</div>
			<div id="author_info_38435">
			</div>
		</div>
	</div>
     <?php $pageUrl = get_permalink();
					  $pageDescription=kama_meta_description('',80,1); 
					  $text=$pageDescription;
					  $pageImage=p75GetThumbnail($post->ID, 60, 60, "");?>  
                      
   	<div class="social-buttons">
		<span class="t"></span>
		<span class="b"></span>
		<table>
			<tbody><tr>
				<td>
                            
                
<div id="vk_like_top" style="top: -13px; height: 22px; width: 132px; background: none repeat scroll 0% 0% transparent; position: relative; clear: both;">
					<script type="text/javascript">

  					
document.write(VK.Share.button({
  url: '<?php echo $pageUrl; ?>',
  title: '<?php echo the_title(); ?>',
  description: '<?php echo $pageDescription; ?>',
  image: '<?php echo $pageImage; ?>',
  noparse: true}));

                       
                    </script>
                    </div>
				</td>
				<td class="with-padding">
<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
<script type="IN/Share" data-url="<?php echo $pageUrl; ?>" data-counter="right"></script>			
									</td>
				<td class="with-padding">		
<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</td>
				<td class="with-padding">
					<fb:like href="<?php echo $pageUrl; ?>" send="false" layout="button_count" width="130" show_faces="true"></fb:like>
				</td>
				<td>
					<!--a data-id="blog-export" data-action="open-inline-window" class="dynamic grey-grey" onclick="return false;" href="">Код для блога</a-->
				</td>
			</tr>
		</tbody></table>
	</div>
	<?php $compil_url = get_post_meta($post->
	ID, 'compil_url', true); if ($compil_url) { ?> <br/>
	<noindex>Материал: <span><?php echo $compil_url; ?>
	</span></noindex><?php } ?>
	<!--div class="dotted">
	</div-->
	<div class="cnt news_body">
		<?php the_content(); ?>
	</div>
	<p>
		ForTrader.Ru - <?php the_category(', '); ?>
	</p>
	<div class="copy">
<?php $ist = get_post_meta($post->
		ID, 'ist', true); if ($ist) { ?> ПО МАТЕРИАЛАМ <span><?php echo $ist; ?>
		</span><?php } ?>
	</div>
	<div class="copy">
<?php $compil_text = get_post_meta($post->
		ID, 'compil_text', true); if ($compil_text) { ?>
		<blockquote>
			/<?php echo $compil_text; ?>
			/
		</blockquote>
<?php } ?>
	</div>
	<?php endwhile; else : ?>
	<h2 class="center">Не найдено</h2>
	<p class="center">
		Такой страницы не существует
	</p>
	<?php endif; ?>
	<br/>
    <div class="social-buttons">
		<span class="t"></span>
		<span class="b"></span>
		<table>
			<tbody><tr>
				<td class="with-padding">
                            
                
<div id="vk_like_top1" style="height: 22px; width: 135px; background: none repeat scroll 0% 0% transparent; position: relative; clear: both; top: -12px;">
					<script type="text/javascript">
					document.write(VK.Share.button({
  url: '<?php echo $pageUrl; ?>',
  title: '<?php echo the_title(); ?>',
  description: '<?php echo $pageDescription; ?>',
  image: '<?php echo $pageImage; ?>',
  noparse: true},{type:'round', text: 'Мне нравится' }));
                    </script>
                    </div>
				</td>
				<td class="with-padding">
<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
<script type="IN/Share" data-url="<?php echo $pageUrl; ?>" data-counter="right"></script>			
									</td>
				<td class="with-padding">		
<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</td>
				<td class="with-padding">
					<fb:like href="<?php echo $pageUrl; ?>" send="false" layout="button_count" width="130" show_faces="true"></fb:like>
				</td>
				<td>
					<!--a data-id="blog-export" data-action="open-inline-window" class="dynamic grey-grey" onclick="return false;" href="">Код для блога</a-->
				</td>
			</tr>
		</tbody></table>
	</div>
    
	<script type="text/javascript" src="/js/jquery.addtocopy.js"></script>
	<script type="text/javascript">
$(document).ready(function() {
$('.news_body').addtocopy({});
});
	</script>
	<div class="clear">
	</div>
	<div class="dotted">
	</div>
	<script type="text/javascript">
   var sppc_site      = '26';
   var sppc_channel   = '64';
   var sppc_dimension = '2';
   var sppc_width     = '468';
   var sppc_height    = '60';
   var sppc_palette   = '12';
   var sppc_user      = '1';
	</script>
	<script type="text/javascript" src="http://adv.advstatus.ru/text/show.js"></script>
	<br/>
	<br/>
	<!-- Яндекс.Директ -->
	<div id="yandex_ad">
	</div>
	<script type="text/javascript">
(function(w, d, n, s, t) {
    w[n] = w[n] || [];
    w[n].push(function() {
        Ya.Direct.insertInto(74745, "yandex_ad", {
            site_charset: "windows-1251",
            ad_format: "direct",
            font_size: 0.9,
            type: "flat",
            limit: 2,
            title_font_size: 1,
            site_bg_color: "FFFFFF",
            header_bg_color: "FEEAC7",
            title_color: "333333",
            url_color: "0066CC",
            all_color: "0066CC",
            text_color: "666666",
            hover_color: "0066FF",
            favicon: true
        });
    });
    t = d.documentElement.firstChild;
    s = d.createElement("script");
    s.type = "text/javascript";
    s.src = "http://an.yandex.ru/system/context.js";
    s.setAttribute("async", "true");
    t.insertBefore(s, t.firstChild);
})(window, document, "yandex_context_callbacks");
	</script>
	<!--/* OpenX iFrame Tag v2.8.7 *
<iframe id='a92b0f5c' name='a92b0f5c' src='http://adv.advstatus.ru/www/delivery/afr.php?zoneid=40&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;ct0=INSERT_CLICKURL_HERE' frameborder='0' scrolling='no' width='460' height='180'><a href='http://adv.advstatus.ru/www/delivery/ck.php?n=aa5f7d8b&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://adv.advstatus.ru/www/delivery/avw.php?zoneid=40&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=aa5f7d8b&amp;ct0=INSERT_CLICKURL_HERE' border='0' alt='' /></a></iframe>
/-->

	<br/>
	<table class="table6" width="100%">
	<seo_links>
	<tr>
		<num> 3 </num>
		<prolog>
		<td>
			</prolog>
			<epilog><br/>
		</td>
		</epilog>
		<description><span class="text">
		<br/> Самая подробная информация, не пропусти! | <br/> Читайте подробнее на страницах сайта | <br/> Читай подробности | <br/> Раскрываем тему на портале трейдеров | <br/> Важно только для форекс трейдеров | <br/> Подробности только для читателей сайта | <br/> Только для читателей сайта | <br/> Только для участников сайта | <br/> Толко для посетителей сайта | <br/> Эксклюзивная информация на нашем сайте | <br/> Впервые раскрывает наш портал | <br/> Узнай больше от экспертов портала | <br/> Аналитика и рекомендации бесплатно на форекс сайте | <br/> Прогнозы специалистов бесплатно | <br/> Бесплатные статьи от работающих трейдеров| <br/> Биржевой трейдинг на нашем портале | <br/> Форекс он-лайн на портале трейдеров | <br/> Forex trading от fortrader.ru | <br/> Все о валюте и биржах | <br/> Думай как трейдер | <br/> Стань трейдером вместе с гуру форекса | <br/> Научись трейдингу сам с журналом для трейдеров | <br/> Мысли как биржевой трейдер | <br/> Попробуй себя в трейдинге | <br/> Форекс самообразование на ForTrader.ru <br/> | Самообучение трейдингу на портале </span></description>
	</tr>
	</seo_links>
	</table>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php comments_template(); ?>
	<?php endwhile; else : ?>
	<?php endif; ?>
	<div class="title2">
<?php // the_category(', '); ?>
	</div>

	<br/>
	<br/>
	<!-- VK Widget -->
	<div id="vk_groups1">
	</div>
	<script type="text/javascript">
VK.Widgets.Group("vk_groups1", {mode: 2, width: "450", height: "1290", wide: 1}, 4226976);
	</script>
</div>
<!--END .content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
