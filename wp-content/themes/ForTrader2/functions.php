<?php

add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
// Добавляет расширение 'extension' и его mime type 'mime/type'
$existing_mimes['extension'] = 'mime/type';
// добавляйте столько сколько вам нужно
$existing_mimes['doc'] = 'application/msword';
$existing_mimes['mq4'] = 'text/plain';
$existing_mimes['mq5'] = 'text/plain';
$existing_mimes['ex4'] = 'text/plain';
$existing_mimes['ex5'] = 'text/plain';
$existing_mimes['tpl'] = 'text/plain';
$existing_mimes['zip'] = 'application/zip';
 
return $existing_mimes;
}

//function my_myme_types($mime_types){
//    $mime_types['mq4'] = 'text/plain';
//    return $mime_types;
//}
//add_filter('upload_mimes', 'my_myme_types', 1, 1);

//Удаляем генератор
remove_action('wp_head', 'wp_generator');

require_once(dirname(__FILE__).'/class.php');

function trim_title_words($count, $after) {
$title = get_the_title();
$words = split(' ', $title);
if (count($words) > $count) {
array_splice($words, $count);
$title = implode(' ', $words);
}
else $after = '';
echo $title . $after;
}

/* Подсчет количества посещений страниц
---------------------------------------------------------- */
add_action('wp_head', 'kama_postviews');
function kama_postviews() {

/* ------------ Настройки -------------- */
$meta_key		= 'views';	// Ключ мета поля, куда будет записываться количество просмотров.
$who_count 		= 0; 			// Чьи посещения считать? 0 - Всех. 1 - Только гостей. 2 - Только зарегистрированых пользователей.
$exclude_bots 	= 1;			// Исключить ботов, роботов, пауков и прочую нечесть :)? 0 - нет, пусть тоже считаются. 1 - да, исключить из подсчета.
/* СТОП настройкам */

global $user_ID, $post;
	if(is_singular()) {
		$id = (int)$post->ID;
		static $post_views = false;
		if($post_views) return true; // чтобы 1 раз за поток
		$post_views = (int)get_post_meta($id,$meta_key, true);
		$should_count = false;
		switch( (int)$who_count ) {
			case 0: $should_count = true;
				break;
			case 1:
				if( (int)$user_ID == 0 )
					$should_count = true;
				break;
			case 2:
				if( (int)$user_ID > 0 )
					$should_count = true;
				break;
		}
		if( (int)$exclude_bots==1 && $should_count ){
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			$notbot = "Mozilla|Opera"; //Chrome|Safari|Firefox|Netscape - все равны Mozilla
			$bot = "Bot/|robot|Slurp/|yahoo"; //Яндекс иногда как Mozilla представляется
			if ( !preg_match("/$notbot/i", $useragent) || preg_match("!$bot!i", $useragent) )
				$should_count = false;
		}

		if($should_count)
			if( !update_post_meta($id, $meta_key, ($post_views+1)) ) add_post_meta($id, $meta_key, 1, true);
	}
	return true;
}

/** мета заголовок (title): 
  - Первый параметр функции это разделитель, второй название блога (если не указать берется из Настроек).
  - Для меток и категорий указывается в настройках в описании (в настройках, по аналогии с пунком 6 Platinum SEO Pack, см. выше) так: [title=Заголовок]
  - Для страниц или постов, если вы хотите чтобы заголовок страницы отличался от заголовка поста, то создайте произвольное поле title и впишите туда свое название
  ------------------------------------- */
/*function kama_meta_title($sep=" — ", $bloginfo_name='') {
    global $wp_query, $post;
    if (!$bloginfo_name)
        $bloginfo_name = get_bloginfo('name');
    $wp_title = wp_title($sep, 0, 'right');

    $category = get_the_category();
    $cat = $category[0]->cat_name;

    if (is_category() || is_tag()) {
        $desc = $wp_query->queried_object->description;
        if ($desc)
            preg_match('!\[title=(.*)\]!iU', $desc, $match);
        $out = $match[1] ? $match[1] . $sep : ("") . " $wp_title";
    }
	 elseif (is_home())
	  $out="Форекс журнал для трейдеров – информация о ФИНАМ, ММВБ, РТС, а также цены на золото, медь, нефть, курсы валют на Fortrader.ru"; 
    elseif (is_singular())
        $out = ($free_title = get_post_meta($post->ID, "title", true)) ? $free_title . $sep : $wp_title;
    elseif (is_author())
        $out = "Статьи $wp_title";
    elseif (is_day() || is_month() || is_year())
        $out = "Архив за: $wp_title";
    elseif (is_search())
        $out = 'Результаты поиска по запросу: ' . strip_tags($_GET['s']) . $sep;
    elseif (is_404())
        $out = "Ошибка 404 - страница не существует" . $sep . $wp_title;
	
	if(is_singular()){
    $out = trim($out . $bloginfo_name);}
	else{ $out = trim($out);}
	
    if ($paged = get_query_var('paged'))
        $out = "$out (страница $paged)";
		
	if (strpos($_SERVER['REQUEST_URI'], 'siteraiting') != 0) {
	
    $out = 'Рейтинг forex сайтов';
}	
    return print $out;
}*/

/** мета описание (description): 
  - Для главной страницы описание указывается в функции, так: kama_meta_description ('Здесь описание блога');
  - Для страниц меток и категорий указывается в описании (в настройках, по аналогии с пунком 6 Platinum SEO Pack, см. выше), так: [description=текст, описание]
  - У постов сначала проверяется, произвольное поле description, если оно есть описание берется оттуда, потом проверяется поле "цитата", если цитаты нет, то описание берется как начальная часть контента.
  - вторым параметром в функции указывается колличество символов для описания: kama_meta_description ('Описание для главной страницы',200);
  ------------------------------------- */
/*function kama_meta_description($home_description='', $maxchar=200, $flag=0) {
    global $wp_query, $post;
    if (is_singular()) {
        if ($descript = get_post_meta($post->ID, "description", true))
            $out = $descript;
        elseif ($post->post_excerpt != '')
            $out = trim(strip_tags($post->post_excerpt));
        else
            $out = trim(strip_tags($post->post_content));

        $char = iconv_strlen($out, 'utf-8');
        if ($char > $maxchar) {
            $out = iconv_substr($out, 0, $maxchar, 'utf-8');
            $words = split(' ', $out);
            $maxwords = count($words) - 1; //убираем последнее слово, ибо оно в 99% случаев неполное  
            $out = join(' ', array_slice($words, 0, $maxwords)) . ' ...';
        }
    //} elseif (is_category() || is_tag()) {
    } elseif (is_category() || is_tag()) {
        $desc = $wp_query->queried_object->description;
        if ($desc)
            preg_match('!\[description=(.*)\]!iU', $desc, $match);
        $out = $match[1] ? $match[1] : '';
    }
    elseif (is_home())
        $out = $home_description;
    if ($out) {
        $out = str_replace(array("\n", "\r"), ' ', strip_tags($out));
        $out = preg_replace("@\[.*?\]@", '', $out); //удаляем шоткоды  
		if($flag==0){
        return print "<meta name='description' content='$out' />\n";
		}
		else { return $out ; }
    }
    else
        return false;
}*/

/** метатег keywords: 
  - Для главной страницы, ключевые слова указываются в функции так: kama_meta_keywords ('слово1, слово2, слово3');
  - Также можно вписать ключевые слова во второй параметр, они будут отображаться (добавляться) на всех страницах сайта: kama_meta_keywords ('<ключевики для главной>','<сквозные ключевики>');
  - Чтобы задать свои keywords для записи, создайте произвольное поле keywords и впишите в значения необходимые ключевые слова. Если такого поля у записи нет, то ключевые слова генерируются из меток и названия категории(й).
  - Для страниц меток и категорий ключевые слова указываетются в описании (в настройках, по аналогии с пунком 6 Platinum SEO Pack, см. выше) так: [keywords=слово1, слово2, слово3]
  ------------------------------------- */
function kama_meta_keywords($home_keywords='', $def_keywords='') {
    global $wp_query, $post;
    if (is_single() && !$out = get_post_meta($post->ID, 'keywords', true)) {
        $out = '';
        $res = wp_get_object_terms($post->ID, array('post_tag', 'category'), array('orderby' => 'none')); // получаем категории и метки  
        if ($res)
            foreach ($res as $tag)
                $out .= " {$tag->name}";
        $out = str_replace(' ', ', ', trim($out));
        $out = "$out $def_keywords";
    }
    //elseif (is_category() || is_tag()) {
		
    elseif (is_category() || is_tag()) {
        $desc = $wp_query->queried_object->description;
        if ($desc)
            preg_match('!\[keywords=(.*)\]!iU', $desc, $match);
        //$out = $match[1] ? $match[1] : '';
		$out = '';
        //$out = "$out $def_keywords";
		 $out = '';
    }
    elseif (is_home()) {
        $out = $home_keywords;
    }
    if ($out)
        return print "<meta name='keywords' content='$out' />\n";
    return false;
}

	function slimbox_create($content){
	return preg_replace('/<a(.*?)href=(.*?).(jpg|jpeg|png|gif|bmp|ico)"(.*?)>/i', '<a$1href=$2.$3" $4 rel="lightbox[roadtrip]">', $content);
	}
	add_filter('the_content', 'slimbox_create', 2);


/** Функция для вывода записей по произвольному полю содержащему числовое значение. 
------------------------------------- 
Параметры передаваемые функции (в скобках дефолтное значение): 
num (10) - количество постов. 
key (views) - ключ произвольного поля, по значениям которого будет проходить выборка. 
order (DESC) - порядок вывода записей. Чтобы вывести сначала менее просматириваемые устанавливаем order=1 
format(0) - Формат выводимых ссылок. По дефолту такой: ({a}{title}{/a}). Можно использовать, например, такой: {date:j.M.Y} - {a}{title}{/a} ({views}, {comments}). 
days(0) - число последних дней, записи которых нужно вывести по количеству просмотров. Если указать год (2011,2010), то будут отбираться популярные записи за этот год. 
cache (0) - использовать кэш или нет. Варианты 1 - кэширование включено, 0 - выключено (по дефолту). 
echo (1) - выводить на экран или нет. Варианты 1 - выводить (по дефолту), 0 - вернуть для обработки (return). 
Пример вызова: kama_get_most_viewed("num=5 &key=views &cache=1 &format={a}{title}{/a} - {date:j.M.Y} ({views}) ({comments})"); 
*/  
function kama_get_most_viewed($args=''){  
    parse_str($args, $i);  
    $num    = isset($i['num']) ? $i['num']:10;  
    $key    = isset($i['key']) ? $i['key']:'views';  
    $order  = isset($i['order']) ? 'ASC':'DESC';  
    $cache  = isset($i['cache']) ? 1:0;  
    $days   = isset($i['days']) ? (int)$i['days']:0;  
    $echo   = isset($i['echo']) ? 0:1;  
    $format = isset($i['format']) ? stripslashes($i['format']):0;  
    global $wpdb,$post;  
    $cur_postID = $post->ID;  
  
    if( $cache ){ $cache_key = (string) md5( __FUNCTION__ . serialize($args) );  
        if ( $cache_out = wp_cache_get($cache_key) ){ //получаем и отдаем кеш если он есть  
            if ($echo) return print($cache_out); else return $cache_out;  
        }  
    }  
  
    if( $days ){  
        $AND_days = "AND post_date > CURDATE() - INTERVAL $days DAY";  
        if( strlen($days)==4 )  
            $AND_days = "AND YEAR(post_date)=" . $days;  
    }  
  
    $sql = "SELECT p.ID, p.post_title, p.post_date, p.guid, p.comment_count, (pm.meta_value+0) AS views  
    FROM $wpdb->posts p  
        LEFT JOIN $wpdb->postmeta pm ON (pm.post_id = p.ID)  
    WHERE pm.meta_key = '$key' $AND_days  
        AND p.post_type = 'post'  
        AND p.post_status = 'publish'  
    ORDER BY views $order LIMIT $num";  
    $results = $wpdb->get_results($sql);  
    if( !$results ) return false;  
  
    $out= '';  
    preg_match( '!{date:(.*?)}!', $format, $date_m );  
    foreach( $results as $pst ){  
        $x == 'li1' ? $x = 'li2' : $x = 'li1';  
        if ( (int)$pst->ID == (int)$cur_postID ) $x .= " current-item";  
        $Title = $pst->post_title;  
        $a1 = "<a href='". get_permalink($pst->ID) ."' title='{$pst->views} просмотров: $Title'>";  
        $a2 = "</a>";  
        $comments = $pst->comment_count;  
        $views = $pst->views;  
        if( $format ){  
            $date = apply_filters('the_time', mysql2date($date_m[1],$pst->post_date));  
            $Sformat = str_replace ($date_m[0], $date, $format);  
            $Sformat = str_replace(array('{a}','{title}','{/a}','{comments}','{views}'), array($a1,$Title,$a2,$comments,$views), $Sformat);  
        }  
        else $Sformat = $a1.$Title.$a2; 
		
		/*выводим только картинки наш супер мега хак*/		
		$img="<img class='pic' src='" .  p75GetThumbnail($pst->ID, 32, 32, ""). "' alt='" . get_the_title($pst->ID) . "' title='". get_the_title($pst->ID) . "' />";
		$img=$a1.$img.$a2;
		
        $out .= $img;/*"$Sformat"." <div class='clear'></div>"; */ 
    }  
  
    if( $cache ) wp_cache_add($cache_key, $out);  
  
    if( $echo )  
        return print $out;  
    else  
        return $out;  
}  

/* удаляем category 

function seocategorydel($catlink1) {
    $catlink1 = str_replace('/category', '', $catlink1);
    return $catlink1;
}

add_filter('category_link', 'seocategorydel', 1, 1);
 удаляем category */


// Обрезаем анонсы статей

function the_excerpt2($post, $count = 20) {

    if(empty($post)) {

        the_excerpt();

        return false;

    }

    $content = trim(strip_tags($post->post_content));
	$content=preg_replace('/\[caption.*?\[\/caption\]/si', '', $content);

    $content = explode(' ', $content);

    $content = array_slice($content, 0, $count);

    $content = implode(' ', $content);
	

    echo ''.$content.'...';    return true;

    return true;

}



// Подключаем динамические сайдбары для рекламы





if ( function_exists('register_sidebar') )

register_sidebar(array('name'=>'Баннер 300x250',

));



register_sidebar(array('name'=>'Баннер 120x600',

));



register_sidebar(array('name'=>'Баннер 120x600 нижний',

));



// Подключаем поддержку динамических меню и превью



if (function_exists('add_theme_support')) {

    add_theme_support('menus');

    add_theme_support('post-thumbnails' );

}



/*



1. он перехватывает все вызовы ф-ции меню в теме, если вы хотите это исключить (и вызывать вручную, что позволит использовать и стандартный вывод) то просто закомм. строчку 



add_filter( 'wp_nav_menu_args', 'wp_nav_custom_menu' );







и вызывайте напрямую через wp_nav_custom_menu



2. рекомендаю не использовать повторяющиеся структуры в меню (т.е. одну и туже страницу или категорию в двух разделах)







*/







// старт обработчика меню



function wp_nav_custom_menu($args)



{



	wp_reset_query();



	



	if($args['menu']!=null){$menu_title=$args['menu'];}



	//	получаю список элементов меню



	//	смотрю какая страница



	//



	//	если главная страница	$id_check = null



	//	если категория			$id_check = ID_category



	//	если страница			$id_check = ID_page



	//	если запись				$id_check = ID_category



	



	//	вывожу все записи уровня 1



	//	вывожу все записи с этим родителем(уровень 2)



	



	



	$items = wp_get_nav_menu_items($menu_title); 



	$id_check=-1;



	$cur_id=-1;



	



	if(is_category())



		{



			$title = single_cat_title('',false);


			if (get_cat_ID($title)==9) {
                $cur_id = 26130;
                $id_check = 26129;
            }
            else {
			    $id_check = get_parent_id(get_cat_ID($title),$items);
			    $cur_id=$id_check[1];
                $id_check=$id_check[0];
            }



			unset($title);



		}



	if(is_single())



		{



			$cat_id=get_the_category(get_the_ID());



			$id_check = get_parent_id($cat_id[0]->cat_ID,$items);



			$cur_id=$id_check[1];



			$id_check=$id_check[0];



			unset($cat_id);



		}



	if(is_page())



		{

		    

			$id_check = get_parent_id(get_the_ID(),$items);

            

			$cur_id=$id_check[1];



			$id_check=$id_check[0];



		}



		
    


	echo '<ul id="menu-menu" class="menu">';



	if($id_check==-1){ echo '<li id="menu-item-home" class="menu-item-type-custom activ_menu"><a href="'.get_bloginfo('url').'">ГЛАВНАЯ</a></li>';}else



		{



			echo '<li id="menu-item-home" class="menu-item-type-custom"><a href="'.get_bloginfo('url').'">ГЛАВНАЯ</a></li>';



		}



	for($i=0;$i<sizeof($items);$i++)



		{



			if($items[$i]->menu_item_parent==0)



				{



					echo '<li id="menu-item-'.$items[$i]->ID.'" class="menu-item-type-custom';



					if($id_check==$items[$i]->ID)



						{



							echo ' activ_menu';



						}



					echo '"><a href="';



					$first_childr=get_count_children($items[$i]->ID,$items);



					if($first_childr!=0)



						{



							echo $first_childr->url.'">'.$items[$i]->title.'</a></li>';



						}else



							{



								echo $items[$i]->url.'">'.$items[$i]->title.'</a></li>';



							}



				}



		}



	echo '</ul>';



	if(get_count_children($id_check,$items)!=0)



		{



	echo '<ul id="menu-menu-1" class="podmenu">';



	for($i=0;$i<sizeof($items);$i++)



		{



			if($items[$i]->menu_item_parent==$id_check)



				{



					echo '<li><a ';



if($cur_id==$items[$i]->ID)



{



echo 'class="podmenu_activ" ';



}



echo 'href="'.$items[$i]->url.'">'.$items[$i]->title.'</a></li>';



				}



		}



	echo '</ul>';



		}



}







function get_count_children($id_check,$items)



	{



		for($i=0;$i<sizeof($items);$i++)



			{



				if($items[$i]->menu_item_parent==$id_check)



					{



						return $items[$i];



					}



			}



		return 0;



	}







function get_parent_id($id_object,$array)



	{



		for($i=0;$i<sizeof($array);$i++)



			{



				if($array[$i]->object_id==$id_object)



					{



						if($array[$i]->menu_item_parent==0){return -1;}



						return array('0'=>$array[$i]->menu_item_parent,'1'=>$array[$i]->ID);	



						



					}



			}



		return array('0'=>-1,'1'=>-1);



	}







add_filter( 'wp_nav_menu_args', 'wp_nav_custom_menu' );



// конец обработчика меню








function mytheme_comment($comment, $args, $depth) {

    $GLOBALS['comment'] = $comment;

    ?>





        <?php if ($comment->comment_approved == '0') : ?>

            <em><?php _e('Your comment is awaiting moderation.') ?></em>

            <br />

        <?php endif; ?>

     <div class="review">



            <?php $authordata = get_comment_author(); ?>

            <?php $user = get_userdata($comment->user_id); ?>

            <?php if (!empty($comment->user_id)): ?>

 

             <div class="review2">

             <a href="/profile/<?php echo $user->user_login ?>"><?php echo $user->display_name ?></a>

                |  <?php comment_time('d/n ٠ G:i'); ?>

             </div>

            <?php else: ?>

                <?php echo '<div class="review2">' . $comment->comment_author.' ٠ '.date('d/n ٠ G:i', $comment->comment_date) . '</div>'; ?>

            <?php endif; ?>



            <?php remove_filter('comment_text', 'wpautop', 30);  comment_text() ?>



        </div>



    <?php

}


// Форекс статьи



function cat_roller() {

    global $wpdb;

    global $post;

    $tmp_post = $post;

    $categories = get_categories($args);

    $i = 0;

    foreach ($categories as $category) {

        if ($category->term_id == 11683  || $category->term_id == 11724 || $category->term_id == 11726 || 
                
                $category->term_id == 11691 || $category->term_id == 11684 || $category->term_id == 9527 || 
	
		$category->term_id == 842 || $category->term_id == 1822 || $category->term_id == 9 ||

                $category->term_id == 5 || $category->term_id == 1320 || $category->term_id == 832 ||

                $category->term_id == 639 || $category->term_id == 2284 || $category->term_id == 301 ||

                $category->term_id == 7 || $category->term_id == 1786 || $category->term_id == 1994 ||

                $category->term_id == 1933 || $category->term_id == 1787 || $category->term_id == 2321 ||

                $category->term_id == 823 || $category->term_id == 6 || $category->term_id == 4219 || 
				
		$category->term_id == 8606 || $category->term_id == 7489 || $category->term_id == 9872 || 

                $category->term_id == 9 || $category->term_id == 11737 || $category->term_id == 3821) {

            query_posts('cat=' . $category->term_id . "&showposts=1&orderby=post_date_gmt DESC");

            if (have_posts()) : while (have_posts()) : the_post();

                    $data[$category->term_id] = strtotime($post->post_date);

                endwhile;

            endif;

            wp_reset_query();

            $i++;

        }

    }

    arsort($data);

    $xxx = 1;

    ?>

            <?
            $exclude_array = array();
            while (list($key, $value) = each($data)) {

                $x = $x + 1;

                ///////////////////////////////////////////////////////////////////////////////////////////////////  

                ?>



                        

    



                    <?php

                    $postslist = get_posts('numberposts=1&category=' . $key . '&exclude='.implode(',',$exclude_array).'&orderby=post_date_gmt DESC');

                    foreach ($postslist as $post) :

                        setup_postdata($post);
			$exclude_array[] = $post->ID;
                        ?>


  <div class="new3"> <div class="kat"><?php the_category(', '); ?></div> 

<a href="<?php the_permalink(); ?>">

<?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(59,59), array('class' => 'pic')); } 

	  else {?><img class="pic" src="<?php echo p75GetThumbnail($post->ID, 100, 75, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> <? } ?>

</a>

    <a class="name" href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
	<?php if( get_comments_number() ){?><a class="comm-count" href="<?php the_permalink(); ?>#coments_tabs_box"><?php comments_number( '', '(1)', '(%)' ); ?></a><?php } ?>
    <span class="date">|  <?php the_time('d/n ٠ G:i'); ?></span>
    <div class="clear" style="padding-top:5px"></div> 
    <div><?php the_excerpt2($post, 16); ?></div> 

    <div class="clear" style="padding-top:5px"></div>   
    <div class="else"><?php previous_post_link('%link', '%title</a> <span class="date">|  %date</span>', true); ?></div>

  </div> 

<?php endforeach; ?>

                    <?php $post = $tmp_post; ?>

                     





                <?

                ///////////////////////////////////////////////////////////////////////////////////////////////////  

                if ($x > 6) {

                    break;

                }

            }

            ?>



<?php }

function get_pagination($total_count = 0, $now_page = 1, $per_page = 10, $count_pages = 11, $additional = '') {
    $total_pages = round($total_count / $per_page);
    $now_page = $now_page > $total_pages ? $total_pages : $now_page;
    $now_page = $now_page <= 0 ? 1 : $now_page;

    if ($total_pages <= 1) {
        return array(
            'total_pages' => 1,
            'count' => 1,
            'pages' => array(),
            'start' => 0,
            'prev' => null,
            'next' => null,
        );
    }

    $half_pages = round(($count_pages - 1) / 2);
    $pages = $prev = $next = array();
    $prefix = '?';

    if ($additional) {
        $prefix = '?' . $additional . '&';
    }

    if ($now_page + $half_pages > $total_pages)
        $negative_half_pages = $half_pages + ($half_pages - ($total_pages - $now_page));
    else
        $negative_half_pages = $half_pages;

    for ($i = ($now_page - 1); $i > $now_page - 1 - $negative_half_pages; $i--) {
        $num = $i;
        if ($num < 1) {
            break;
        }

        $pages[] = array(
            'url' => $prefix . 'page=' . $num,
            'num' => $num,
        );

        if (++$num == $now_page) {
            $prev = $pages[count($pages) - 1];
        }
    }
    $pages = array_reverse($pages);
    $pages[] = array(
        'url' => '',
        'num' => $now_page,
    );

    if ($now_page - $half_pages <= 0)
        $positive_half_pages = $half_pages + ($half_pages - $now_page + 1);
    else
        $positive_half_pages = $half_pages;

    for ($i = 1; $i <= $positive_half_pages; $i++) {
        $num = $now_page + $i;
        if ($num > $total_pages)
            break;
        $pages[] = array(
            'url' => $prefix . 'page=' . $num,
            'num' => $num,
        );

        if (--$num == $now_page) {
            $next = $pages[count($pages) - 1];
        }
    }

    return array(
        'count' => count($pages),
        'last_page' => isset($pages[count($pages) - 1]) ? $pages[count($pages) - 1]['num'] : 0,
        'first_page' => isset($pages[0]) ? $pages[0]['num'] : 0,
        'total_pages' => $total_pages,
        'pages' => $pages,
        'start' => ($now_page * $per_page) - $per_page,
        'prev' => $prev,
        'next' => $next,
    );
}
function template_get_page_number() {
  global $paged;
  return apply_filters( 'template_get_page_number', $paged );
}

function kama_pagenavi($before='', $after='', $echo=true) {
    /* ================ Настройки ================ */
    $text_num_page = 'Страница {current} из {last}'; // Текст для количества страниц. {current} заменится текущей, а {last} последней. Пример: 'Страница {current} из {last}' = Страница 4 из 60
    $num_pages = 10; // сколько ссылок показывать
    $stepLink = ''; // после навигации ссылки с определенным шагом (значение = число (какой шаг) или '', если не нужно показывать). Пример: 1,2,3...10,20,30
    $dotright_text = '…'; // промежуточный текст "до".
    $dotright_text2 = '…'; // промежуточный текст "после".
    $backtext = '« назад'; // текст "перейти на предыдущую страницу". Ставим '', если эта ссылка не нужна.
    $nexttext = 'вперед »'; // текст "перейти на следующую страницу". Ставим '', если эта ссылка не нужна.
    $first_page_text = '« к началу'; // текст "к первой странице" или ставим '', если вместо текста нужно показать номер страницы.
    $last_page_text = 'в конец »'; // текст "к последней странице" или пишем '', если вместо текста нужно показать номер страницы.
    /* ================ Конец Настроек ================ */

    global $wp_query;
    $posts_per_page = (int) $wp_query->query_vars[posts_per_page];
    $paged = (int) $wp_query->query_vars[paged];
    $max_page = $wp_query->max_num_pages;

    if ($max_page <= 1)
        return false; //проверка на надобность в навигации

    if (empty($paged) || $paged == 0)
        $paged = 1;

    $pages_to_show = intval($num_pages);
    $pages_to_show_minus_1 = $pages_to_show - 1;

    $half_page_start = floor($pages_to_show_minus_1 / 2); //сколько ссылок до текущей страницы
    $half_page_end = ceil($pages_to_show_minus_1 / 2); //сколько ссылок после текущей страницы

    $start_page = $paged - $half_page_start; //первая страница
    $end_page = $paged + $half_page_end; //последняя страница (условно)

    if ($start_page <= 0)
        $start_page = 1;
    if (($end_page - $start_page) != $pages_to_show_minus_1)
        $end_page = $start_page + $pages_to_show_minus_1;
    if ($end_page > $max_page) {
        $start_page = $max_page - $pages_to_show_minus_1;
        $end_page = (int) $max_page;
    }

    if ($start_page <= 0)
        $start_page = 1;

    $out = ''; //выводим навигацию
    $out.= $before . "<div class='wp-pagenavi'>\n";
    if ($text_num_page) {
        $text_num_page = preg_replace('!\{current\}|\{last\}!', '%s', $text_num_page);
        $out.= sprintf("<span class='pages'>$text_num_page</span>", $paged, $max_page);
    }

    if ($backtext && $paged != 1)
        $out.= '<a href="' . get_pagenum_link(($paged - 1)) . '">' . $backtext . '</a>';

    if ($start_page >= 2 && $pages_to_show < $max_page) {
        $out.= '<a href="' . get_pagenum_link() . '">' . ($first_page_text ? $first_page_text : 1) . '</a>';
        if ($dotright_text && $start_page != 2)
            $out.= '<span class="extend">' . $dotright_text . '</span>';
    }

    for ($i = $start_page; $i <= $end_page; $i++) {
        if ($i == $paged) {
            $out.= '<span class="current">' . $i . '</span>';
        } else {

            $out.= '<a href="' . get_pagenum_link($i) . '">' . $i . '</a>';
        }
    }

    //ссылки с шагом
    if ($stepLink && $end_page < $max_page) {
        for ($i = $end_page + 1; $i <= $max_page; $i++) {
            if ($i % $stepLink == 0 && $i !== $num_pages) {
                if (++$dd == 1)
                    $out.= '<span class="extend">' . $dotright_text2 . '</span>';
                $out.= '<a href="' . get_pagenum_link($i) . '">' . $i . '</a>';
            }
        }
    }

    if ($end_page < $max_page) {
        if ($dotright_text && $end_page != ($max_page - 1))
            $out.= '<span class="extend">' . $dotright_text2 . '</span>';
        $out.= '<a href="' . get_pagenum_link($max_page) . '">' . ($last_page_text ? $last_page_text : $max_page) . '</a>';
    }

    if ($nexttext && $paged != $end_page)
        $out.= '<a href="' . get_pagenum_link(($paged + 1)) . '">' . $nexttext . '</a>';

    $out.= "</div>" . $after . "\n";
    if ($echo)
        echo $out;
    else
        return $out;
}

//преобразуе дни в дату daylife при апдейте метаданных поста  - custom field
function my_update_post_meta($meta_id, $object_id, $meta_key, $_meta_value) {
	$meta_values = get_post_meta( $object_id, 'daylife' );
	if( !empty($meta_values[0]) ) {
		$datetest=date_parse($meta_values[0]);
		
		if( $datetest[error_count]!=0 ) {
			$current_date = new DateTime("now");
			$current_date->add(new DateInterval('P'.$meta_values[0].'D'));
			$current_date->format('Y-m-d H:i:s');
			update_post_meta($object_id, 'daylife', $current_date->format('Y-m-d H:i:s'));
			
		}
	}

}
add_action( 'updated_postmeta', 'my_update_post_meta', 10, 4 );

//экспорт юзера при регистрации
add_action( 'user_register', 'export_user_to_mailerlite', 10, 1 );
function export_user_to_mailerlite( $user_id ) {

	//update_user_meta($user_id, 'first_namerrr', 'ggggggggg');
	
	$user=get_userdata($user_id);

	require_once(get_template_directory().'/mailerlite/ML_Subscribers.php');
	$api_key = get_option('mailer_lite_api_key');
	$group_id = get_option('mailer_lite_group_id');
	$ML_Subscribers = new ML_Subscribers( $api_key );
			
			$subscriber = array(
				'email' => $user->data->user_email,
				'name' => $user->data->user_login,
				'fields' => array(
				   array( 'name' => 'name', 'value' => $user->data->login ),
				   array( 'name' => 'email', 'value' => $user->data->user_email )
				)
			);
	$subscriber = $ML_Subscribers->setId( $group_id )->add( $subscriber, 1 );

}










//redirects for outbound links
add_action( 'init', 'wpse36168_add_rewrite_rule' );
/**
 * Add our rewrite rule
 */
function wpse36168_add_rewrite_rule()
{
	add_rewrite_rule(
		'^go/(.*?)$',
		'index.php?go=$matches[1]',
		'top'
	);
}

add_filter( 'query_vars', 'wpse36168_add_go_var' );
/**
 * Tell WP not to strip out or "go" query var
 */
function wpse36168_add_go_var( $vars )
{
	$vars[] = 'go';
	return $vars;
}

add_action( 'template_redirect', 'wpse36168_catch_external' );
/**
 * Catch external links from our "go" url and redirect them
 */
function wpse36168_catch_external()
{
	if( $url = get_query_var( 'go' ) )
	{
		wp_redirect( esc_url( $url ), 302 );
		exit();
	}
}


require_once( 'custom-comments/comments-functions.php' );


function get_formated_category_parents( $id, $link = false, $lastcat = false, $separator = '', $nicename = false, $visited = array() ) {
	global $category_breadcrumb_marker;
	$chain = '';
	$parent = get_term( $id, 'category' );
	if ( is_wp_error( $parent ) )
		return $parent;

	$name = $parent->name;

	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
		$visited[] = $parent->parent;
		$category_breadcrumb_marker++;
		$chain .= get_formated_category_parents( $parent->parent, $link, $separator, $nicename, $visited );
	}

	if ( $link ){
		
		if( $lastcat && !$category_breadcrumb_marker)
			1==1;
		else
			$chain .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> → <a href="' . esc_url( get_category_link( $parent->term_id ) ) . '" title="' . esc_attr( sprintf( __( "%s" ), $parent->name ) ) . '" itemprop="url"><span itemprop="title">'.$name.'</span></a></span>';
		
		$category_breadcrumb_marker--;

	}else
		$chain .= $name.$separator;
	return $chain;
}



function fix_category_404($query) {

	if( is_category() ){
	
		$cats = explode( '/', $_SERVER['REQUEST_URI'] );

		foreach( $cats as $cat ){
			if( $cat ) {
			
				if( $cat == 'page' ) break;

				$cat_by_slug = get_category_by_slug($cat);
				if( ! $cat_by_slug->cat_ID ){
				
					set_query_var('category_name', md5( time() ) );
					break;
					
				}
				
			}
		}
		
	}
}
add_action('parse_query','fix_category_404');








add_action('admin_menu', 'mailer_lite_menu');

function mailer_lite_menu() {
	add_options_page('Mailer lite', 'Mailer lite', 'manage_options', 'mailer-lite-menu', 'mailer_lite_func');
}

function mailer_lite_func(){
?>

    <div class="wrap">
<?php
	if (isset($_POST["update_settings"])) {
	
		if ( !wp_verify_nonce( $_POST['settings_noncename'], plugin_basename(__FILE__) )) {
			return 0;
		}
		
		update_option('mailer_lite_api_key',trim( $_POST["mailer_lite_api_key"] ) );
		update_option('mailer_lite_group_id',trim( $_POST["mailer_lite_group_id"] ) );

		echo '<h2 style="color:#0074A2;">Settings Saved</h2>';
	}
?>
        <h2>Mailer Lite Settings</h2>
		<div id="poststuff" class="metabox-holder"><div id="normal-sortables" class="meta-box-sortables"><div id="ghpseo_options-page-expert-settings" class="postbox " >
<h3 class='hndle'><span>Edit Settings if you need</span></h3>
<div class="inside">

	<form method="POST" action="">
		<?php echo '<input type="hidden" name="settings_noncename" id="settings_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';?>
		<table class="form-table">
			<tr valign="top"><th scope="row"><label for="mailer_lite_api_key">Your API KEY</label></th>
				<td><input type="text" name="mailer_lite_api_key" size="25" value='<?php echo get_option('mailer_lite_api_key');?>' /></td>
			</tr>
			<tr valign="top"><th scope="row"><label for="mailer_lite_group_id">Group Id</label></th>
				<td><input type="text" name="mailer_lite_group_id" size="25" value='<?php echo get_option('mailer_lite_group_id');?>' /></td>
			</tr>
				
		</table>
		<input type="hidden" name="update_settings" value="Y" />
		<input name="save" class="button button-primary button-large" id="publish" accesskey="p" value="Update" type="submit">
	</form>
		
</div>
</div>
</div>
</div>
 
    </div>
<?php
}





