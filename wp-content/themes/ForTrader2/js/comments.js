jQuery(document).ready(function(){

			jQuery('.coments_tabs_list_item').on('click',function(){
				var jQuerythis = jQuery(this),
					index = jQuerythis.index(),
					parent = jQuerythis.parents('.coments_tabs_box');
					

				if( jQuerythis.attr('data-link') != undefined ){
					if( jQuerythis.hasClass('active') ){
						window.location.href = jQuerythis.attr('data-link');
					}
				}
				
				
				jQuerythis.addClass('active').siblings().removeClass('active');
				parent.find('.coments_tabs_content>div').eq(index).addClass('active').siblings().removeClass('active');
			});

			jQuery('.reg-reply').on('click',function(){
				
				jQuery(this).parents('.coments_container').find('.add_reply_box').html( jQuery('#respond').find('.add_reply_box').html() );
				jQuery(this).parents('.coments_container').find('#comment_parent').val( jQuery(this).attr('data-comment-id') );
				jQuery(this).parents('.coments_container').find('.leave_comment_title').text('Ответить:');
				if( jQuery(this).parents('.coments_container').hasClass('anon-container') ){
					
					var redirectto = jQuery(this).parents('.coments_container').find('#redirect_to').val();
					jQuery(this).parents('.coments_container').find('#redirect_to').val( redirectto + '&anon=1' );
					
				}
				
				jQuery(this).parents('.coments_container').find('.add_reply_box').slideToggle(300);
			});
			
			jQuery('.part_discus-reg').on('click',function(){
			
				jQuery('.reg-reply[data-comment-id='+jQuery(this).attr("data-comment-id")+']').trigger( 'click' );
				//jQuery('body').scrollTo('#respond',{duration:'slow', offsetTop : '50'});
				
				jQuery('html, body').animate({
					scrollTop: jQuery('.reg-reply[data-comment-id='+jQuery(this).attr("data-comment-id")+']').offset().top
				}, 1000);
				
			});
			
			
			jQuery('.anon-reply').on('click',function(){
				
				jQuery(this).parents('.coments_container').find('.add_reply_box').html( jQuery('#respond-anon').find('.add_reply_box').html() );
				jQuery(this).parents('.coments_container').find('#comment_parent').val( jQuery(this).attr('data-comment-id') );
				jQuery(this).parents('.coments_container').find('.leave_comment_title').text('Ответить:');
				
				jQuery(this).parents('.coments_container').find('.add_reply_box').slideToggle(300);
			});
			
			jQuery('.part_discus-anon').on('click',function(){
			
				jQuery('.anon-reply[data-comment-id='+jQuery(this).attr("data-comment-id")+']').trigger( 'click' );
				//jQuery('body').scrollTo('#respond',{duration:'slow', offsetTop : '50'});
				
				jQuery('html, body').animate({
					scrollTop: jQuery('.anon-reply[data-comment-id='+jQuery(this).attr("data-comment-id")+']').offset().top
				}, 1000);
				
			});
			
			jQuery('.rate_comment').on('click',function(){
			
				var current_rate_item = jQuery(this);
				var data_commen_id = jQuery(this).attr('data-commen-id');
			
				jQuery.ajax({
					type: 'POST',
					url: '/wp-admin/admin-ajax.php',
					dataType: 'json',
					data: {
						action: 'rateComment',
						data_rating: jQuery(this).attr('data-rating'),
						data_commen_id: data_commen_id,
					},
					success: function(data, textStatus, XMLHttpRequest){
						
						if( data.good_ratiing > 0 ){
							current_rate_item.html( '<i class="fa fa-thumbs-up"></i>'+data.good_ratiing );
						}else{
							current_rate_item.html( '<i class="fa fa-thumbs-down"></i>'+data.bad_ratiing );
						}
						jQuery('.rate_comment[data-commen-id='+data_commen_id+']').addClass('no-cursor');
						jQuery('.rate_comment[data-commen-id='+data_commen_id+']').removeClass('rate_comment');
						

					}
				});
				
			});
			
			jQuery('.sent-complaint').on('click',function(){
			
				var current_complaint = jQuery(this);
				var data_commen_id = jQuery(this).attr('data-commen-id');
			
				jQuery.ajax({
					type: 'POST',
					url: '/wp-admin/admin-ajax.php',
					dataType: 'json',
					data: {
						action: 'createComplaint',
						data_commen_id: data_commen_id,
					},
					success: function(data, textStatus, XMLHttpRequest){
						
						if(data.created){
							current_complaint.addClass('complaint');
							current_complaint.removeClass('sent-complaint');
							current_complaint.html( '<span class="coment_close_text p_abs">Жалоба отправлена</span>' );
						}

					}
				});
				
			});
			
			
			jQuery('body').on('click','.unregistered_submit',function(e){
			
				e.preventDefault();
				
				if( jQuery(this).parents('#commentform').find('textarea').val().length < 4  ){
				
					alert('Длина коментария должна быть больше 4');
				
				}else if( jQuery(this).parents('#commentform').find('.author-name').val().length < 3 ){
				
					alert('Длина Имени должна быть больше 3');
				
				}else{
				
					jQuery.ajax({
						type: 'POST',
						url: '/wp-admin/admin-ajax.php',
						dataType: 'json',
						data: {
							action: 'createUnRegisteredComment',
							form: jQuery(this).parents("form").serialize(),
							comment_url:window.location.href,
						},
						success: function(data, textStatus, XMLHttpRequest){
							
							if(data.created){
								alert('Коментарий отправлен на модерацию! Ожидайте');
							}
							if( data.redirect ){
								window.location = data.redirectto;
							}

						}
					});
					
				}
				
			
				/*var current_complaint = jQuery(this);
				var data_commen_id = jQuery(this).attr('data-commen-id');*/
			
				/**/
				
			});
			
			

	


			// registered to transition
			jQuery('body').on('click','.registered_coments_link',function(e){
				jQuery(this).parents('.coments_tabs_box')
					.find('.coments_tabs_list_item')
					.eq(0)
					.addClass('active')
					.siblings()
					.removeClass('active');
				jQuery(this).parents('.coments_tabs_box')
					.find('.coments_tabs_content>div')
					.eq(0)
					.addClass('active')
					.siblings()
					.removeClass('active');
				var offset = jQuery('.registered_coments_form').offset().top;
				jQuery("body,html").animate({
					scrollTop: offset - 50
				},1000)
			});
			
			jQuery('body').on('click','.not-enter-comment',function(e){
				e.preventDefault();
				alert('Войдите на сайт либо используйте форму анонимных коментариев');
			});

			// transition to anonymous
			jQuery('.anonym_coments_link').on('click',function(){
				jQuery(this).parents('.coments_tabs_box')
					.find('.coments_tabs_list_item')
					.eq(1)
					.addClass('active')
					.siblings()
					.removeClass('active');
				jQuery(this).parents('.coments_tabs_box')
					.find('.coments_tabs_content>div')
					.eq(1)
					.addClass('active')
					.siblings()
					.removeClass('active');
				var offset = jQuery('.anonym_coments_form').offset().top;
				jQuery("body,html").animate({
					scrollTop: offset - 50
				},1000)
			});

});