var $ = jQuery

$(function() {
    var params = {
        'colors': {}
    }
    $('td.colors input[type="button"]').click(function() {
        var color = $(this).prev().val().replace('#', '')
        if (color) {
            params['colors'][$(this).parent().prev().attr('id').replace('cl', '')] = color
            generateUri()
        }
    })
    
    $('#header').change(function() {
        var header = $(this).val()
        if (header) {
            params['header'] = header
            generateUri()
        }
    })

    $('#blockwidth').change(function() {
        var size = parseInt($(this).val())
        if (size > 0) {
            params['size'] = size
            generateUri()
        }
    })

    $('#scale').change(function() {
        params['scale'] = ($(this).val() != '%') ? 'px' : '%'
        generateUri()
    })

    $('#numnews').change(function() {
        var size = parseInt($(this).val())
        if (size > 0) {
            params['count'] = size
            generateUri()
        }
    })

    $('.position').click(function() {
        if ($(this).val() == 1) {
            params['direction'] = 1
        } else {
            delete params['direction']
        }
        generateUri()
    })

    $('.encoding').click(function() {
        params['encoding'] = $(this).val()
        generateUri()
    })

    $('#withpic').click(function() {
        params['with_photo'] = $(this).attr('checked')
        generateUri()
    })

    $('td.colors div').click(function() {
        var color = $(this).css('background-color')
        
        if (color != 'transparent') {
            color = RGBtoHex($(this).css('background-color'))
        }
        
        $(this).parent().next('td').find('input:first').val(color)
        $(this).parent().find('div.selected').removeClass('selected')
        $(this).toggleClass('selected')

        params['colors'][$(this).parent().attr('id').replace('cl', '')] = color
        generateUri()
    })

    $('#ni_cats input').click(function() {
        $('#chk_all').removeAttr('checked')
        var elems = $('#ni_cats input:checked')
        var tmp = []
        for (var i = 0; i < elems.length; i++) {
            tmp.push($(elems[i]).val())
        }

        params['categories'] = tmp.join(',')
        generateUri()
    })
    
    $('#chk_all').click(function() {
        $('#ni_cats input').removeAttr('checked')
        delete params['categories']
        generateUri()
    })


    function generateUri() {
        var tmp = []
        for (var v in params) {
            if (v == 'colors') {
                if (typeof(params[v][0]) == 'undefined') params[v][0] = 'null'
                if (typeof(params[v][1]) == 'undefined') params[v][1] = 'null'
                if (typeof(params[v][2]) == 'undefined') params[v][2] = 'null'

                tmp.push('colors='+params[v][0]+','+params[v][1]+','+params[v][2])
                continue
            }
            
            tmp.push(v+'='+params[v])
        }

        var uri = encodeURI(tmp.join('&'))

        $('#htmlcode').val($('#htmlcode').val().replace(/(fninfo.php\?)[^"]+/gi, '$1'+'v=1&'+uri, 'gi'))

        var src = $('#script_container').attr('src').replace(/(fninfo.php\?)[^"]+/gi, '$1'+'v=15&'+uri, 'gi').replace(/encoding\=[0-9]+/, '')
        $('#fortrader_newsinformer').remove()

        var scr = document.getElementById('script_container')
        if ((scr!==null) && scr.parentNode) scr.parentNode.removeChild(scr)
        scr = document.createElement('script')
        scr.id = 'script_container'
        scr.src = src
        document.body.appendChild(scr);
    }
    
    function RGBtoHex(str) {
        if (str.charAt(0) != '#') {
            str = str.replace(/[^0-9,]/gi, '', 'gi').split(',')
            return toHex(str[0])+toHex(str[1])+toHex(str[2])
        }
        return str.replace('#', '')
    }
    
    function toHex(N) {
        N = parseInt(N);
        if (N == 0 || isNaN(N)) return "00";
        N = Math.max(0,N);
        N = Math.min(N,255);
        N = Math.round(N);

        return "0123456789ABCDEF".charAt((N-N%16)/16)
        + "0123456789ABCDEF".charAt(N%16);
    }

})

