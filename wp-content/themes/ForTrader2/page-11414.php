<?php wp_enqueue_style('newsinformer', get_bloginfo('template_directory') . '/css/newsinformer.css') ?>
<?php get_header();
the_post(); ?>
<?php
$exlude_cats = array(960, 961);

function ni_filter_cats($elem) {
    return (is_numeric($elem->name)) ? false : true;
}
?>
<div class="clear" style="height:23px;"></div>
<div class="article">

    <h1 class="point"><?php the_title(); ?></h1>


    <div class="text"><?php the_content(); ?></div>

    <div class="entry">
        <table cellspacing="0" cellpadding="0" border="0" class="tb_main">
            <tbody>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" border="0" class="colors">
                            <tbody>
                                <tr>
                                    <td>Цвет фона</td><td>Код цвета</td>
                                </tr>
                                <tr>
                                    <td class="colors" id="cl0">
                                        <div style="background-color:#fff;" class="selected"><input type="radio" checked="" name="col_bg1"></div>
                                        <div style="background-color:#ddd;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#FFBBBB;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#FFDDDD;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#FFEEAA;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#FFFFBB;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#BBEEBB;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#CCFFCC;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#AADDFF;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#CCEEFF;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#EECCFF;"><input type="radio" name="col_bg1"></div>
                                        <div style="background-color:#FFDDFF;"><input type="radio" name="col_bg1"></div>
                                    </td>
                                    <td>
                                        <input size="7" value="ffffff" id="col3">
                                        <input type="button" value="OK">
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table cellspacing="0" cellpadding="0" border="0" id="cols1" class="colors">
                            <tbody>
                                <tr>
                                    <td>Цвет рамки</td><td>Код цвета</td>
                                </tr>
                                <tr>
                                    <td class="colors" id="cl1">
                                        <div style="background-color:transparent;"><input type="radio" checked="" name="col_bg2"></div>
                                        <div style="background-color:#000000;" class="selected"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#666;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#990000;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#FF0000;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#FF6600;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#FFAA00;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#006600;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#009900;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#0033CC;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#0066CC;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#6833CC;"><input type="radio" name="col_bg2"></div>
                                        <div style="background-color:#9933CC;"><input type="radio" name="col_bg2"></div>
                                    </td>
                                    <td>
                                        <input size="7" value="000000" id="col3">
                                        <input type="button" value="OK">
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table cellspacing="0" cellpadding="0" border="0" id="cols1" class="colors">
                            <tbody>
                                <tr>
                                    <td>Цвет ссылок</td><td>Код цвета</td>
                                </tr>
                                <tr>
                                    <td class="colors" id="cl2">
                                        <div style="background-color:transparent;"><input type="radio" checked="" name="col_bg"></div>
                                        <div style="background-color:#000000;" class="selected"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#666;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#990000;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#FF0000;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#FF6600;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#FFAA00;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#006600;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#009900;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#0033CC;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#0066CC;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#6833CC;"><input type="radio" name="col_bg"></div>
                                        <div style="background-color:#9933CC;"><input type="radio" name="col_bg"></div>
                                    </td>
                                    <td>
                                        <input size="7" value="000000" id="col3">
                                        <input type="button" value="OK">
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table cellspacing="0" cellpadding="2" border="0">
                            <tbody>
                                <tr>
                                    <td><b>Расположение блока:</b></td>
                                    <td>
                                        <input type="radio" checked="checked" id="pos1" value="0" name="position"  class="position">
                                        <label for="pos1">Горизонтальное</label><br>
                                        <input type="radio" id="pos2" value="1" name="position" class="position">
                                        <label for="pos2">Вертикальное</label>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>Заголовок блока:</b>
                                    </td>
                                    <td>
                                        <input id='header' value='НОВОСТИ'>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Ширина блока:</b></td>
                                    <td>
                                        <input value="100" id="blockwidth" size="2">
                                        <select id="scale">
                                            <option value="px">px</option>
                                            <option selected="" value="%">%</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Количество новостей:</b>
                                    </td>
                                    <td>
                                        <input id="numnews" value="5" size="2">
                                        <input type="checkbox" checked="" id="withpic">
                                        <label for="withpic">С картинкой</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </tbody>
        </table>

        <b>Категории новостей:</b><br>
        <input type="checkbox" checked="checked" id="chk_all">
        <label for="chk_all">Все категории</label>
        <table id="ni_cats">
            <tbody>
                <tr>
                    <td valign="top">
                        <?php $cats = get_terms('category', 'hierarchical=false&exclude=' . implode(',', (array) $exlude_cats)) ?>
                        <?php $cats = array_values(array_filter($cats, 'ni_filter_cats')); ?>
                        <?php $cats_count = count($cats); ?>
                        <?php $column = (int) ($cats_count / 2); ?>
                        <?php for ($i = 0; $i < $cats_count; $i++): ?>
                            <input type="checkbox" id="ni_cat_<?php echo $cats[$i]->term_id ?>" value="<?php echo $cats[$i]->term_id ?>">
                            <label for="ni_cat_<?php echo $cats[$i]->term_id ?>"><?php echo $cats[$i]->name ?></label>
                            <br>
                        <?php if ($i % $column == 0 && $i)
                                echo '</td><td valign="top">' ?>
                        <?php endfor; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div id="block_container">
                </div>
                <script id="script_container" type="text/javascript" src="<?php echo get_option('home')?>/fninfo.php?v=1"></script>

                <div>
                    HTML-код информера:<br>
                    <textarea readonly="" onfocus="this.select()" id="htmlcode" rows="6" cols="50">
<!-- News informer -->
<script type="text/javascript" src="<?php echo get_option('home')?>/fninfo.php?v=1"></script>
<!-- End of news informer -->
                    </textarea><br>
                    <b>Кодировка:</b>
                    <input type="radio" value="0" class="encoding" name="encoding" id="enc1"> <label for="enc1">windows-1251</label>
                    <input type="radio" value="1" class="encoding" name="encoding" id="enc2"> <label for="enc2">koi8-r</label>
                    <input type="radio" checked="checked" value="3" class="encoding" name="encoding" id="enc3"> <label for="enc3">UTF-8</label>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo get_bloginfo('template_directory') . '/js/newsinformer.js' ?>"></script>
<?php get_footer(); ?>