<?
get_header();

if (have_posts ()) {
    while (have_posts ()) {
        the_post();
    }
}
else {
    $fail = true;
}

if (!isset($ExPolls) || !is_object($ExPolls) || get_class($ExPolls) != 'ExPolls') {
    $fail = true;
}

if (isset($fail)) {
    include (get_template_directory() . '/404.php');
    exit();
}

/* @var $ExPolls ExPolls */
$per_page = get_option('posts_per_page');
get_current_user();

echo '<div class="content">';
if (!$_GET['hide_title']) {
    echo '<div class="title">';
    the_title();
    echo '</div><div class="dotted"></div>';
}
include (get_template_directory() . '/custom_pages/expolls/' . $ExPolls->WP_getTemplate() . '.php');
echo '<br /><div class="clear"></div><div class="dotted"></div></div>';

get_sidebar();
get_footer();
