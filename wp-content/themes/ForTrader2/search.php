<?php get_header(); ?>     

  <div class="content">
  
    <div class="title"><?php single_cat_title(); ?></div>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?> 


     <div class="new5">
    <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(59,59), array('class' => 'pic')); } 
	  else {?><img class="pic" src="<?php echo p75GetThumbnail($post->ID, 59, 59, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> <? } ?></a>
    <a class="name" href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
    <div class="date"><?php the_time('d/n ٠ G:i'); ?></div>
        <div class="txt2"><?php remove_filter('the_excerpt', 'wpautop'); the_excerpt(); ?></div> 
    <div class="clear"></div>   
    </div>
 
    
  <?php endwhile;  else : ?>
  
  		<h2 class="center">Не найдено</h2>

		<p class="center">Такой страницы не существует</p>
  
  
  <?php endif; ?>


    
  </div><!--END .content -->

 
<?php get_sidebar(); ?>	  
<?php get_footer(); ?>