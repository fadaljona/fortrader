<?php get_header(); ?>     
  

<?php
$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
?>
  
	<div class="user_box clearfix p_rel">
		<h2 class="user_name f_left"><?php the_author(); ?></h2>
		<div class="user_avatar p_abs">
			<?php
				$sql="SELECT middlePath FROM ft_user_avatar WHERE idUser=".$author->ID;
				if( $author->ID ) $avatar_src = $wpdb->get_var( $sql, 0, 0 );
				if( !$avatar_src || !$author->ID ) $avatar_src = 'assets/avatars/noavatar-middle.png';
			?>
			<img src="/services/<?php echo $avatar_src;?>" alt="" />
		</div>
		<?php
			$user_url = get_the_author_meta( 'user_url', $author->ID );
			if( $user_url ){
		?>
		<a href="/go/<?php echo preg_replace( "(^https?://)", "", $user_url ); ?>" class="user_link f_right"><?php echo $user_url; ?></a>
		<?php }?>
	</div>
	<div class="user_info">
		<p>
		
		<?php echo get_the_author_meta( 'description', $author->ID ); ?><br />
	<?php //if( $user_url ){ echo $user_url ?>  <?php // } echo antispambot( get_the_author_meta( 'user_email', $author->ID ) ); ?>
		</p>
	</div>
	<div class="us_page_content clearfix">
		<div class="coll_left f_left">
			<h5 class="h5">Популярные статьи автора</h5>
			
			
			<?php		
				$popularPosts = new WP_Query();
				$popularPosts->query(array(
					'posts_per_page'	=> 10,	
					'author'			=> $author->ID,
					'meta_key'			=> 'views',
					'orderby'			=> 'meta_value_num',
					'order'				=> 'DESC',
				));
			
			while ($popularPosts->have_posts()) : $popularPosts->the_post();	?>
			
			<div class="user_post clearfix">
				<div class="user_post_img f_left">
					<a href="<?php the_permalink(); ?>"><img src="<?php echo p75GetThumbnail($post->ID, 140, 140, ""); ?>" alt="<?php the_title(); ?>" /></a>
				</div>
				<div class="user_post_description">
					<h6 class="user_post_title">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
						<?php if( get_comments_number() ){?><a class="comm-count" href="<?php the_permalink(); ?>#coments_tabs_box"><?php comments_number( '', '(1)', '(%)' ); ?></a><?php } ?>
					</h6>
					<div class="date"><?php the_time('d/n ٠ G:i'); ?></div>
					<p>
						<?php the_excerpt2($post, 22); ?>
					</p>
					<div class="user_post_coment_box">
						<span class="coment"><?php comments_number('0', '1', '%'); ?> </span>
						<img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"><?php echo get_post_meta ($post->
			ID,'views',true); ?>
					</div>
				</div>
			</div>
			
			<?php endwhile; ?>

		</div>
		
		
		<div class="coll_right f_right">
			<h5 class="h5">Последние статьи автора</h5>
			
			<?php		
				$recentPosts = new WP_Query();
				$recentPosts->query(array(
					'posts_per_page'	=> 10,	
					'author'			=> $author->ID,
					'orderby'			=> 'date',
					'order'				=> 'DESC',
				));
			
			while ($recentPosts->have_posts()) : $recentPosts->the_post();	?>
			
			<div class="user_post clearfix">
				<div class="user_post_img f_left">
					<a href="<?php the_permalink(); ?>"><img src="<?php echo p75GetThumbnail($post->ID, 140, 140, ""); ?>" alt="<?php the_title(); ?>" /></a>
				</div>
				<div class="user_post_description">
					<h6 class="user_post_title">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
						<?php if( get_comments_number() ){?><a class="comm-count" href="<?php the_permalink(); ?>#coments_tabs_box"><?php comments_number( '', '(1)', '(%)' ); ?></a><?php } ?>
					</h6>
					<div class="date"><?php the_time('d/n ٠ G:i'); ?></div>
					<p>
						<?php the_excerpt2($post, 22); ?>
					</p>
					<div class="user_post_coment_box">
						<span class="coment"><?php comments_number('0', '1', '%'); ?> </span>
						<img style="border-width:0px;margin-right: 4px;" src="http://files.fortrader.ru/images/eye.png"><?php echo get_post_meta ($post->
			ID,'views',true); ?>
					</div>
				</div>
			</div>
			
			<?php endwhile; ?>

		</div>
		
		
	</div>
  
  
  
  
  
  
  
  
  
<?php /*
    <div class="title">

    <?php the_author(); ?>  для журнала FORTRADER.ru</div>

    <div class="txt5"><?php echo category_description(); ?></div>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?> 

  <?php $i++; if ($i < 4) { ?>

    <div class="new4">

    <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(99,99), array('class' => 'pic')); } 

	  else {?><img class="pic" src="<?php echo p75GetThumbnail($post->ID, 99, 99, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> <? } ?></a> 

    <a class="name" href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 

    <div class="date"><?php the_time('d/n ٠ G:i'); ?></div>

    <div class="txt2"><?php the_excerpt2($post, 22); ?></div> 

    <br />

<?php
$posttags = get_the_tags($id);

if ($posttags) {
foreach($posttags as $tag) {
$y = $tag->name .',';
$x[$id] .= $y;

}}
$postslist = get_posts('exclude='.$id.'&tag='.$x[$id].'&numberposts=2'); foreach ($postslist as $post) : setup_postdata($post);
?>

    <div class="else"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>

   <?php endforeach; ?>    

    <div class="clear"></div>

    </div>

  <?php } else { ?>

     <div class="new5">

    <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(array(59,59), array('class' => 'pic')); } 

	  else {?><img class="pic" src="<?php echo p75GetThumbnail($post->ID, 59, 59, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> <? } ?></a>

    <a class="name" href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 

    <div class="date"><?php the_time('d/n ٠ G:i'); ?></div>

    <div class="clear"></div>   

    </div>

  <?php } endwhile;  get_template_part('nav'); else : endif; wp_reset_query();*/ ?>



<?php // get_sidebar(); ?>
<div class="clear"></div>

<?php get_footer(); ?>
