<?php
if (!class_exists('ExLines')) {
    include '404.php';
} else {
    include ABSPATH . '/' . PLUGINDIR . '/exlines/creator.php';
    $creator = new ExLineCreator();
    $ExLines = new ExLines();
//    echo '<pre>';var_dump($creator->images);
    get_header();
?>
    <div class="article">
        <h1 class="titl3"><?php the_title(); ?></h1>
       
	   <b>Выберите тип линейки</b>
        <br /><br />
    <?php foreach ($creator->images as $key => $val): ?>
        <div style="margin-bottom: 30px;">
            <input type="radio" id="<?php echo $key ?>" name="line-select" value="1" class="line-select" />
            <label for="<?php echo $key ?>">
            <?php echo array_pop(explode('_', str_replace('_b', '', $key))) ?>
            <br />
            <img src="<?php bloginfo('home') ?>/wp-content/plugins/exlines/lines/<?php echo $key ?>/preview.png" alt=""/>
        </label>
    </div>
    <?php endforeach; ?>

            <label for="line-date"><b>Укажите дату (наши линейки поддерживают как прямой, так и обратный отсчет)</b></label>
            <br /><br />
            <input type="text" id="line-date" name="line-date" value="" />
            <br /><br />

            <b>Выбирете текст из списка или укажите свой</b>
            <br /><br />
            <input type="text" id="line-text" name="line-text" value="" />
            <select id="line-text-select" name="line-text-select">
                <option value=""></option>
        <?php foreach ($ExLines->texts as $val): if (!trim($val))
                    continue; ?>
                <option value="<?php echo $val ?>"><?php echo $val ?></option>
        <?php endforeach; ?>
            </select>

            <br /><br />
            <br /><br />
            <input type="button" id="line-get-preview" name="line-get-preview" value="Получить код и посмотреть превью" />

            <div id="line-preview-block" class="line-preview-block" style="display:none;">
                <img src="<?php bloginfo('home') ?>/ftline.php?nostat=1&line=___this_is_a_line_name_place___" alt=""/>
                <br />
<br />
<strong>Изображение в тексте (BB-code): </strong><br>
            <textarea name="" cols="60" rows="4">[URL="<?php bloginfo('home') ?>/lines.html"] [IMG]<?php bloginfo('home') ?>/ftline.php?line=___this_is_a_line_name_place___[/IMG][/URL]</textarea><br>

                <strong>HTML изображение в тексте: </strong><br>
                <textarea name="" cols="60" rows="4">&lt;a href="<?php bloginfo('home') ?>/lines.html"&gt; &lt;img src="<?php bloginfo('home') ?>/ftline.php?line=___this_is_a_line_name_place___" alt="#" /&gt; &lt;/a&gt;</textarea><br>
            </div>
        </div>

        <link rel="stylesheet" type="text/css" href="<?php bloginfo('home') ?>/wp-includes/js/jquery/ui/css/ui-lightness/jquery-ui-1.8.6.custom.css" media="all" />
        <script type="text/javascript" src="<?php bloginfo('home') ?>/wp-includes/js/jquery/ui/js/jquery-ui-1.8.6.custom.min.js"></script>
        <script type="text/javascript">
            $ = jQuery;
            $(document).ready(function(){

            $('#line-preview-block textarea').click(function() {
                $(this).select()
            })

            $('#line-preview-block strong').click(function() {
                $(this).next().next('textarea').select()
            })

            $('#line-get-preview').click(function() {
                //check params
                if (typeof($('.line-select:checked').attr('id')) == "undefined") {
                    alert('Вы забыли указать тип линейки')
                    $('.line-select:first').focus()
                    return;
                }

                if (!$('#line-text').val()) {
                    alert('Вы забыли указать текст')
                    $('#line-text').focus()
                    return;
                }


                if (!$('#line-date').val()) {
                    alert('Вы забыли указать дату')
                    $('#line-date').focus()
                    return;
                }

                var date = $('#line-date').val()
                date = new Date(date.split('.')[2], date.split('.')[1], date.split('.')[0])

                if (isNaN(date)) {
                    alert('Вы указали неверную дату')
                    $('#line-date').focus()
                    return;
                }

                $.post('<?php bloginfo('home') ?>/ftline.php', {
                    'text': $('#line-text').val(),
                    'date': $('#line-date').val(),
                    'template': $('.line-select:checked').attr('id')
                }, function(reply) {
                    if (reply.error) {
                        alert('Серверная ошибка, пожалуйста, попробуйте позже')
                    } else {
                        $('.line-preview-block-clone').remove()

                        var clone = $('<div>').attr({
                            'class': 'line-preview-block-clone line-preview-block'
                        }).html($('#line-preview-block').html().replace(/___this_is_a_line_name_place___/g, reply.url)).appendTo('.article')

                        clone.show()
                    }
                }, 'json')
            })

            $('#line-date').datepicker({
                'yearsRange': '<?php echo date('Y') - 30 ?>:<?php echo date('Y') + 30 ?>',
                'changeYear': true
            });

            $('#line-text-select').change(function() {
                if ($(this).val()) {        
                    $('#line-text').val($(this).val())
                } else {
                    $('#line-text').val('')
                }
            });
            });
        </script>
<?php
                get_footer();
            }
?>