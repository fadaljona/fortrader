<?php get_header();?>           
<?php
global $fortraderCache;
$cacheDuration = get_option('theme_cache_duration');
$cache_key = 'theme_' . md5($_SERVER['REQUEST_URI']);
if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
?>
<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part">
		<?php get_template_part_by_locale( 'templates/banner-728x90' );?>
		<?php $excludePostArr = array();?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="clearfix">
				<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
				<div class="breadcrumbs alignleft">
					<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
						<li>
							<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a href="<?php echo pll_home_url();?>" itemprop="item"><span itemprop="name" class="breadcrumb-active"><?php _e("Home", 'ForTraderMaster'); ?></span></a>
								<meta itemprop="position" content="1" />
							</span>
						</li>
					</ul>
				</div>
				<!-- - - - - - - - - - - - - - End of Breadcrumbs - - - - - - - - - - - - - - - - -->
				<!-- - - - - - - - - - - - - - Article Info - - - - - - - - - - - - - - - - -->
				<div class="article_info clearfix alignright">
					<?php
						$commentNumber = get_comments_number();
						if( $commentNumber ){
							echo '<span class="comment">'.$commentNumber.'</span>';
						}
					?>
					<span class="views"><?php echo get_post_meta ($post->ID,'views',true) ? get_post_meta ($post->ID,'views',true) : 0; ?></span>
					<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d F, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
				</div>
				<!-- - - - - - - - - - - - - - End of Article Info - - - - - - - - - - - - - - - - -->
			</div><!-- / .cleafix -->
			<hr>
			<h1 class="title2"><?php the_title();?><?php edit_post_link(' <i></i>'); ?></h1>
			<?php get_template_part( 'templates/sm-top-post-buttons' ); ?>
			<!-- - - - - - - - - - - - - - Article Post - - - - - - - - - - - - - - - - -->
			<article class="clearfix main-post-content">
				<?php the_content(); ?>
				<div class="author_article clearfix">
					<?php 		
						$author = get_userdata( $post->post_author ); 
						$authorName = getShowUserName( $author );
					?> 
					<a href="<?php echo get_author_posts_url($author->ID);?>" class="author_avatar alignleft">
						<?php renderImg( p75GetServicesAuthorThumbnail( 107, 107, $author->ID ), 107, 107, $authorName, 1 );?>
					</a>
					<div class="author_text">
						<h5 class="author_name"><a href="<?php echo get_author_posts_url($author->ID);?>"><?php echo $authorName;?></a></h5>
						<p><?php echo $author->about;?></p>
					</div>
				</div><!--/ .author_article -->
				<div class="clearfix">
					<a href="<?php echo get_author_posts_url($author->ID);?>" class="article_btn alignright"><?php _e("Other articles by this author", 'ForTraderMaster'); ?></a>
					<div class="box1">
					<?php
						$tw_link = get_user_meta( $author->ID, 'tw_link', true );
						$fb_link = get_user_meta( $author->ID, 'fb_link', true );
						$vk_link = get_user_meta( $author->ID, 'vk_link', true );
					?>
						<ul class="social_list clearfix">
							<?php if($tw_link){?><li><a href="<?php echo $tw_link;?>" class="tw"></a></li><?php }?>
							<?php if($fb_link){?><li><a href="<?php echo $fb_link;?>" class="fb"></a></li><?php }?>
							<?php if($vk_link){?><li><a href="<?php echo $vk_link;?>" class="vk"></a></li><?php }?>
						</ul>
						<i class="icon pencil_icon"></i>
					</div><!--/ .box1 -->
				</div><!--/ .clearfix -->
				<?php
					$posttags = get_the_tags();
					if( !empty($posttags) ){
				?>
					<section class="section_offset">
						<h3 class="page_title2"><?php _e("INFORMATION BY TOPIC", 'ForTraderMaster'); ?>:</h3>
						<!-- - - - - - - - - - - - - - Button Box - - - - - - - - - - - - - - - - -->
						<div class="info_buttons clearfix">
						<?php 
							foreach( $posttags as $tag ){
						?>
								<a href="<?php echo get_tag_link($tag->term_id);?>" class="categories_btn"><?php echo $tag->name?></a>
						<?php } ?>
						</div>
						<!-- - - - - - - - - - - - - - End of Button Box - - - - - - - - - - - - - - - - -->
					</section>
				<?php } ?>
			</article><!--/ .section_offset -->
			<!-- - - - - - - - - - - - - - End of Article Post - - - - - - - - - - - - - - - - -->
			<?php get_template_part( 'templates/sm-bottom-post-buttons' ); ?>

			<div class="blockdiv3 clearfix">
				<div><?php dynamic_sidebar('300x250-2-post-banner');?></div>
				<div><?php dynamic_sidebar('300x250-3-post-banner');?></div>
			</div>

		<?php endwhile; else : ?>
			<h2 class="center"><?php _e("Not found", 'ForTraderMaster'); ?></h2>
			<p class="center"><?php _e("This page does not exist", 'ForTraderMaster'); ?></p>
		<?php endif; ?>
			
			<?php /*Сегодня*/?>	
			<?php get_template_part( 'templates/single-today-block' ); ?>

			<?php /*Редактор рекомендует*/?>	
			<?php get_template_part( 'templates/single-editor-recommends' ); ?>

	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php $fortraderCache->endCache(); } ?>
	<?php get_sidebar();?>
<?php get_footer();?>