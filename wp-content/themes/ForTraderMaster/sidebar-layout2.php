<!-- - - - - - - - - - - - - - Sidebar - - - - - - - - - - - - - - - - -->

<aside class="sidebar fx_sidebar">
    <div class="fx_sb-head">
        <a href="/en/currencyconverter"><?php _e("Currency converter", 'ForTraderMaster'); ?></a>
    </div>
    <div class="fx_sidebar-wr">

        <?php get_template_part('templates/sidebar-currency-converter');?>

        <?php get_template_part_by_locale('templates/sidebar-banner-300x600'); ?>

        <?php get_template_part('templates/sidebar-articles-tabs'); ?>

        <?php /*<div class="fx_sb-section">

            <div class="fx_sb-title">
                <a href="#">FT magazine latest release</a>
            </div>

            <div class="fx_sb-section-box fx_sb-releases">
                <div class="fx_box-item">
                    <a href="#" class="fx_box-link">The price of gold forecast forex trader, the course today <span class="fx_box-count">15</span></a>
                </div>
            </div>

        </div>*/?>

        <?php get_template_part_by_locale('templates/sidebar-top-accounts'); ?>

        <?php /*<div class="fx_sb-section">
            <div class="fx_sb-title"><a href="#">Top EA</a></div>
            <div class="fx_sb-section-box fx_sb-ea">
                                    
                <div class="fx_rating-item fx_border-left clearfix">
                    <div>
                        <a href="#" class="fx_rating-subitem fx_ea-item">
                            Profitable scalping strategy for M1 timeframe <span class="fx_strong">Tricolor</span>
                        </a>
                    </div>

                    <div>        
                        <div class="reviewStars-input">
                            <input class="star-4" type="radio" name="reviewStars"/>
                            <label title="gorgeous" for="star-4"></label>
                            <input class="star-3" type="radio" name="reviewStars"/>
                            <label title="good" for="star-3"></label>
                            <input class="star-2" type="radio" checked name="reviewStars"/>
                            <label title="regular" for="star-2"></label>
                            <input class="star-1" type="radio" name="reviewStars"/>
                            <label title="poor" for="star-1"></label>
                            <input class="star-0" type="radio" name="reviewStars"/>
                            <label title="bad" for="star-0"></label>
                        </div>
                    </div>
                </div>

            </div>
        </div>*/?>

        <?php get_template_part('templates/sidebar-banks-rate');?>

                            
        <?php /*<div class="fx_sb-section">
            <div class="fx_sb-title"><a href="#">Top Brokers</a></div>
            <div class="fx_sb-section-box fx_sb-brokers">
                <div class="fx_rating-item clearfix">
                    <div>
                        <img src="http://web.com/ft/images/logo_forex-time.jpg" alt="" class="fx_rating-logo">
                        <span class="fx_rating-name">ForexTime</span>
                    </div>
                    <div>
                        <div class="reviewStars-input">
                            <input class="star-4" type="radio" name="reviewStars"/>
                            <label title="gorgeous" for="star-4"></label>
                            <input class="star-3" type="radio" name="reviewStars"/>
                            <label title="good" for="star-3"></label>
                            <input class="star-2" type="radio" checked name="reviewStars"/>
                            <label title="regular" for="star-2"></label>
                            <input class="star-1" type="radio" name="reviewStars"/>
                            <label title="poor" for="star-1"></label>
                            <input class="star-0" type="radio" name="reviewStars"/>
                            <label title="bad" for="star-0"></label>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="fx_sb-section">
            <div class="fx_sb-title"><a href="#">Top Indicators</a></div>
            <div class="fx_sb-section-box fx_sb-indicators">
                <div class="fx_rating-item fx_border-left clearfix">
                    <div>
                        <a href="#" class="fx_rating-subitem fx_indicators-item">
                            <div>Binary Options Indicator</div>
                            <div>Six Second Trades</div>
                        </a>
                    </div>
                    <div>
                        <div class="reviewStars-input">
                            <input class="star-4" type="radio" name="reviewStars"/>
                            <label title="gorgeous" for="star-4"></label>
                            <input class="star-3" type="radio" name="reviewStars"/>
                            <label title="good" for="star-3"></label>
                            <input class="star-2" type="radio" checked name="reviewStars"/>
                            <label title="regular" for="star-2"></label>
                            <input class="star-1" type="radio" name="reviewStars"/>
                            <label title="poor" for="star-1"></label>
                            <input class="star-0" type="radio" name="reviewStars"/>
                            <label title="bad" for="star-0"></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>*/?>

        <?php get_template_part( 'templates/sidebar-fixed-ads' ); ?>

</div>
</aside>

<!-- - - - - - - - - - - - - - End of Sidebar - - - - - - - - - - - - - - - - -->