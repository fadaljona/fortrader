<!DOCTYPE html>
<html lang="<?=$language?>" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
    <head>
        <!-- Basic page needs
        ============================================ -->
        <title><?=$title?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content="<?=$description?>">
        <meta name="keywords" content="<?=$keywords?>">
        <meta name="author" content="">

        <link rel="publisher" href="<?=Yii::app()->params['googlePlus']?>"/>
        <meta property="og:locale" content="<?php
            if ($language == 'ru') {
                echo 'ru_RU';
            } elseif ($language == 'en_us') {
                echo 'en_US';
            } else {
                echo $language;
            }
        ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?=$title?>" />
        <meta property="og:description" content="<?=$description?>" />
        <meta property="og:url" content="<?=Yii::App()->request->getHostInfo() . Yii::App()->request->getUrl()?>" />
        <meta property="og:site_name" content="<?=$siteName?>" />
        <meta property="article:publisher" content="<?=Yii::app()->params['facebook']?>" />
        <meta property="article:section" content="<?=$ogSection?>" />

        <meta property="og:image" content="<?=$ogImage?>" />
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:description" content="<?=$description?>"/>
        <meta name="twitter:title" content="<?=$title?>"/>
        <meta name="twitter:site" content="@<?=Yii::app()->params['twitter']?>"/>
        <meta name="twitter:image" content="<?=$ogImage?>"/>
        <meta name="twitter:creator" content="@<?=Yii::app()->params['twitter']?>"/>

        <!-- Mobile specific metas
        ============================================ -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<?php
    if ($this->beginCache('wp_get_header_layout2_part1'.Yii::app()->language . CommonLib::sslRequestMarker(), array('duration'=>60*60*24))) {
        CommonLib::loadWp();
?>
        <!-- Favicon
        ============================================ -->
        <link href="<?php bloginfo('template_url'); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" >

         <!-- Libs CSS
        ============================================ -->
        <?php wp_enqueue_style('owl.carousel.css', get_bloginfo('template_url').'/plagins/owlCarousel/css/owl.carousel.css', array(), null);?>
        <?php wp_enqueue_style('jquery.formstyler.css', get_bloginfo('template_url').'/plagins/jQueryFormStyler/jquery.formstyler.css', array(), null);?>

        <!-- Theme CSS
        ============================================ -->
        <?php wp_enqueue_style('style.css', get_bloginfo('stylesheet_url'), array(), null);?>
        <?php wp_enqueue_style('fx_style.css', get_bloginfo('template_url').'/css/fx_style.css', array(), null);?>
        <?php wp_enqueue_style('fx_exchange-style.css', get_bloginfo('template_url').'/css/fx_exchange-style.css', array(), null);?>

        <!-- Media Queries CSS
        ============================================ -->
        <?php wp_enqueue_style('responsive.css', get_bloginfo('template_url').'/css/responsive.css', array(), null);?>
        <?php wp_enqueue_style('fx_responsive.css', get_bloginfo('template_url').'/css/fx_responsive.css', array(), null);?>
        <?php wp_enqueue_style('fx_exchange-responsive.css', get_bloginfo('template_url').'/css/fx_exchange-responsive.css', array(), null);?>


        <!-- Old IE 
        ============================================ -->

        <!--[if lt IE 8]>
        <div style=' clear: both; text-align:center; position: relative;'>
          <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
          </a>
        </div>
        <![endif]-->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="<?php echo get_bloginfo('template_url');?>/js/html5.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_bloginfo('template_url');?>/css/ie.css">
        <![endif]-->

        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
        <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        
        <?php 
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('wp_print_styles', 'print_emoji_styles');
            wp_enqueue_script("jquery", false, array(), null, false);
            wp_head();
        ?>

<script>
function sendPost( params, url ){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send( params );
}
var wAWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
</script>	



<?php

$currenciesForConverter = CurrencyRatesModel::getEcbCurrenciesForConverter();
$sidebarConverterFlagStyles = '.fx_convert-flag-eu {background: url(/uploads/country/flags/shiny/16/EU.png) center left 10px no-repeat;}.fx_sb-convert-select.fx_convert-flag-eu .jq-selectbox__select-text {background: url(/uploads/country/flags/shiny/16/EU.png) center left no-repeat;}';
foreach ($currenciesForConverter as $key => $val) {
    $flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $val['country']);
    if ($flagSrc) {
        $sidebarConverterFlagStyles .= ".fx_convert-flag-{$val['country']} {background: url({$flagSrc}) center left 10px no-repeat;}.fx_sb-convert-select.fx_convert-flag-{$val['country']} .jq-selectbox__select-text {background: url({$flagSrc}) center left no-repeat;}";
    }
}
    echo '<style>'.$sidebarConverterFlagStyles.'</style>';
?>

<?php if($_SERVER['HTTP_HOST'] == 'fortrader.org' && isset($_SERVER['HTTPS']) && ( $_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == '1' ) ){?>
<link rel="manifest" href="/manifest.json">
  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
  <script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
      appId: "8d23dc04-4397-4214-af27-8a03ef6e4ace",
      autoRegister: true,
      notifyButton: {
          enable: true, 
		  text: {
            'tip.state.unsubscribed': '<?php _e("Subscribe to notifications", 'ForTraderMaster'); ?>',
            'tip.state.subscribed': "<?php _e("You're subscribed to notifications", 'ForTraderMaster'); ?>",
            'tip.state.blocked': "<?php _e("You've blocked notifications", 'ForTraderMaster'); ?>",
            'message.prenotify': '<?php _e("Click to subscribe to notifications", 'ForTraderMaster'); ?>',
            'message.action.subscribed': "<?php _e("Thanks for subscribing!", 'ForTraderMaster'); ?>",
            'message.action.resubscribed': "<?php _e("You're subscribed to notifications", 'ForTraderMaster'); ?>",
            'message.action.unsubscribed': "<?php _e("You won't receive notifications again", 'ForTraderMaster'); ?>",
            'dialog.main.title': '<?php _e("Manage Site Notifications", 'ForTraderMaster'); ?>',
            'dialog.main.button.subscribe': '<?php _e("SUBSCRIBE", 'ForTraderMaster'); ?>',
            'dialog.main.button.unsubscribe': '<?php _e("UNSUBSCRIBE", 'ForTraderMaster'); ?>',
            'dialog.blocked.title': '<?php _e("Unblock Notifications", 'ForTraderMaster'); ?>',
            'dialog.blocked.message': "<?php _e("Follow these instructions to allow notifications:", 'ForTraderMaster'); ?>"
        },
		displayPredicate: function() {
            return OneSignal.isPushNotificationsEnabled()
                .then(function(isPushEnabled) {
                    return !isPushEnabled;
                });
        },
		offset: {
			bottom: '30px',
			right: '80px'
		},
      },
	  safari_web_id: 'web.onesignal.auto.2eb33d58-6740-481f-b49f-67bae0b73a06'
    }]);
  </script>
<?php }?>

	
    </head>
<?php $this->endCache(); } ?> 
    <body <?=$bodyClass?>>
        
        <!-- - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->

        <div class="fx_wrapper">

            <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

            <header id="header"  class="fx_header clearfix">

                <div class="fx_container">

<?php
if ($this->beginCache('wp_get_header_layout2_part2'.Yii::app()->language . CommonLib::sslRequestMarker(), array('duration'=>60*60*24))) {
     CommonLib::loadWp();
?>
                    <!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->
                    <div class="logo fx_logo clearfix">
                      <a href="<?php echo get_lang_url_prefix();?>/" title="Home Page">
                          HomeStyle
                          <img src="<?php echo removeProtocol( get_bloginfo('template_url') ); ?>/images/fx_logo.jpg" alt="">
                      </a>
                      <a href="#" class="fx_mob-menu"></a>
                    </div>
                    <!-- - - - - - - - - - - - - - End of Logo - - - - - - - - - - - - - - - - -->                
                    <!-- - - - - - - - - - - - - - Header List - - - - - - - - - - - - - - - - -->
                    <div class="header_top_btn_box fx_webmaster">
                        <a href="<?php echo get_lang_url_prefix();?>/informers" class="header_top_button fx_header-top_btn"><?php _e("For webmaster", 'ForTraderMaster'); ?></a>
                    </div>
                    <!-- - - - - - - - - - - - - - End of Header Top List - - - - - - - - - - - - - - - - -->
<?php $this->endCache(); } ?>  

                    <!-- - - - - - - - - - - - - - Enter Box - - - - - - - - - - - - - - - - -->
                    
                    <div class="enter_box alignright fx_enterbox clearfix">
                    <?php
                    if (Yii::App()->user->isGuest) {
                            echo $langSwitcher;
                    ?>   
                        <a href="<?=Yii::App()->createUrl('default/login')?>" class="fx_header-exit fx_header_login arcticmodal" data-modal="#logIn"><?=Yii::t( '*', 'Enter' )?></a>
                        <a href="<?=Yii::App()->createUrl('default/login')?>" class="fx_header-exit fx_header_register arcticmodal" data-modal="#registration"><?=Yii::t( '*', 'Registration' )?></a>

                    <?php } else {
                        $model = Yii::App()->user->getModel();
                    ?>  
                        
                        <span><?=Yii::t( '*', 'Hello' )?>, <a href="<?php echo $model->singleURL;?>"><strong><?=CHtml::encode($model->wpShowName)?></strong></a></span>

                        <?php echo $langSwitcher; ?>

                        <a href="<?=Yii::App()->createUrl('default/logout', array('returnURL' => Yii::App()->request->getHostInfo() . Yii::App()->request->getUrl()) )?>" class="fx_header-exit" rel="nofollow"><?=Yii::t( '*', 'Logout' )?></a>

                    <?php }?> 

                    </div>
                    
                    <!-- - - - - - - - - - - - - - End of Enter Box - - - - - - - - - - - - - - - - -->

                </div>
                
            </header>

            <div class="fx_container fx_bg">

                <!-- - - - - - - - - - - - - - End of Header - - - - - - - - - - - - - - - - -->

<?php 
if ($this->beginCache('wp_get_header_layout2_part4'.Yii::app()->language . CommonLib::sslRequestMarker(), array('duration'=>60*60*24))) {
    CommonLib::loadWp();
    get_template_part_by_locale('templates/header-services-nav-layout2'); 
?>

                <!-- - - - - - - - - - - - - - Banner 1063*90 - - - - - - - - - - - - - - - - -->
                
                <div class=" fx_long-blockdiv">
                    


                </div>

                <!-- - - - - - - - - - - - - - End of Banner 1063*90 - - - - - - - - - - - - - - - - -->

                <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->
<?php
    get_template_part('templates/header-nav-layout2');
    $this->endCache();
}
?>
                <div id="content" class="clearfix">