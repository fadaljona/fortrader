<!DOCTYPE html>
<html <?php language_attributes(); ?>>

    <head>

        <!-- Basic page needs
        ============================================ -->
        <title><?php wp_title(); ?></title>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
		

        <!-- Mobile specific metas
        ============================================ -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicon
        ============================================ -->
		<link href="<?php bloginfo('template_url'); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" >

        <!-- Theme CSS
        ============================================ -->

		<?php /*<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="all" />*/?>
		<?php wp_enqueue_style( 'style.css', get_bloginfo('stylesheet_url'), array(), null );?>

        <!-- Media Queries CSS
        ============================================ -->
        <?/*<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/responsive.css">*/?>
		<?php wp_enqueue_style( 'responsive.css', get_bloginfo('template_url').'/css/responsive.css', array(), null );?>
		
		 <!-- Libs CSS
        ============================================ -->
        <?/*<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plagins/owlCarousel/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plagins/liMarquee/css/liMarquee.css">*/?>
		
		<?php wp_enqueue_style( 'jquery.arcticmodal-0.3.css', get_bloginfo('template_url').'/plagins/arcticmodal/jquery.arcticmodal-0.3.css', array(), null );?>
		<?php wp_enqueue_style( 'owl.carousel.css', get_bloginfo('template_url').'/plagins/owlCarousel/css/owl.carousel.css', array(), null );?>
		<?php wp_enqueue_style( 'liMarquee.css', get_bloginfo('template_url').'/plagins/liMarquee/css/liMarquee.css', array(), null );?>
        <?php
            if (is_single()) {
                wp_enqueue_style('buttons.css', get_bloginfo('template_url').'/css/buttons.css', array(), null);
            }
        ?>

        <!-- Old IE 
        ============================================ -->

        <!--[if lt IE 8]>
        <div style=' clear: both; text-align:center; position: relative;'>
          <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
          </a>
        </div>
        <![endif]-->
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_url'); ?>/css/ie.css">
        <![endif]-->
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
		<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		
		<!-- Include Libs & Plugins
        ============================================ -->
	
<script>
function sendPost( params, url ){
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send( params );
}
	var yiiBaseURL = "/services";
	var yiiLanguage = "ru";
	var yiiDebug = true;
	var wAWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
</script>	
		
		
<?php 
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
?>


<?php
/*wp_enqueue_script('wQuotesPanel.js', '/services/js/widgets/wQuotesPanel.js', array('jquery'), null); 

wp_enqueue_style( 'wQuotesPanel.css', '/services/css/wQuotesPanel.css', array(), null );*/
?>
		<?php wp_enqueue_script("jquery", false, array(), null, false); ?>
		<?php wp_head(); ?>
		<!--Relap -->
		<script type="text/javascript" async src="https://relap.io/api/v6/head.js?token=edOqPTG1zDHMBgMG"></script>
		<meta name="alpari_partner" content="1209762" />
		<meta name="AForex" content="X6G1D">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		 google_ad_client: "ca-pub-6643182737842994",
		 enable_page_level_ads: true
		  });
</script>

<?php if($_SERVER['HTTP_HOST'] == 'fortrader.org' && isset($_SERVER['HTTPS']) && ( $_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == '1' ) ){?>
<link rel="manifest" href="/manifest.json">
  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
  <script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
      appId: "8d23dc04-4397-4214-af27-8a03ef6e4ace",
      autoRegister: true,
      notifyButton: {
          enable: true, 
		  text: {
            'tip.state.unsubscribed': '<?php _e("Subscribe to notifications", 'ForTraderMaster'); ?>',
            'tip.state.subscribed': "<?php _e("You're subscribed to notifications", 'ForTraderMaster'); ?>",
            'tip.state.blocked': "<?php _e("You've blocked notifications", 'ForTraderMaster'); ?>",
            'message.prenotify': '<?php _e("Click to subscribe to notifications", 'ForTraderMaster'); ?>',
            'message.action.subscribed': "<?php _e("Thanks for subscribing!", 'ForTraderMaster'); ?>",
            'message.action.resubscribed': "<?php _e("You're subscribed to notifications", 'ForTraderMaster'); ?>",
            'message.action.unsubscribed': "<?php _e("You won't receive notifications again", 'ForTraderMaster'); ?>",
            'dialog.main.title': '<?php _e("Manage Site Notifications", 'ForTraderMaster'); ?>',
            'dialog.main.button.subscribe': '<?php _e("SUBSCRIBE", 'ForTraderMaster'); ?>',
            'dialog.main.button.unsubscribe': '<?php _e("UNSUBSCRIBE", 'ForTraderMaster'); ?>',
            'dialog.blocked.title': '<?php _e("Unblock Notifications", 'ForTraderMaster'); ?>',
            'dialog.blocked.message': "<?php _e("Follow these instructions to allow notifications:", 'ForTraderMaster'); ?>"
        },
		displayPredicate: function() {
            return OneSignal.isPushNotificationsEnabled()
                .then(function(isPushEnabled) {
                    return !isPushEnabled;
                });
        },
		offset: {
			bottom: '30px',
			right: '80px'
		},
      },
	  safari_web_id: 'web.onesignal.auto.2eb33d58-6740-481f-b49f-67bae0b73a06'
    }]);
  </script>
<?php }?>


<?php 
$nowPost = is_single() || is_page();
if( $nowPost ){  
	$currentPostId = get_the_ID(); 
?>
<script>
var current_post_id = <?=$currentPostId?>;
</script>
<?php
}
?>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-6643182737842994",
          enable_page_level_ads: true
     });
</script>
  
    </head>

    <body>
	
	<!-- Facebook Pixel Code-->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','//connect.facebook.net/en_US/fbevents.js');
		
		fbq('init', '1407368206230683');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none" alt="Facebook Pixel Code"
		src="https://www.facebook.com/tr?id=1407368206230683&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
        
        <!-- - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->

        <div class="container white_container">

            <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

            <header id="header" class="clearfix" itemscope itemtype="http://schema.org/WPHeader">
                
                <!-- - - - - - - - - - - - - - Enter Box - - - - - - - - - - - - - - - - -->
                
                <div class="enter_box alignright clearfix">
					<?php if (!is_user_logged_in()){ ?>   
                        <!--li class="spc"></li-->
                        
                        <?php echo headerLangSwitcher();?>

						<a href="/wp-login.php" id="topLogin" class="arcticmodal" data-modal="#logIn" rel="nofollow"><?php _e("Login", 'ForTraderMaster'); ?></a>
						<a href="/wp-login.php?action=register" class="arcticmodal" data-modal="#registration" rel="nofollow"><?php _e("Registration", 'ForTraderMaster'); ?></a>
					<?php } else { 
							$currentUser = wp_get_current_user();
							$userName = getShowUserName( $currentUser );
					?>  
						<span><?php _e("Hi", 'ForTraderMaster'); ?>, <a href="<?php echo get_author_posts_url($currentUser->ID);?>"><strong><?=$userName?></strong></a></span>
                        
                        <?php echo headerLangSwitcher();?>
                        
                        <a href="<?php echo wp_logout_url( str_replace( '/' . pll_current_language('slug'), '', home_url() ) . $_SERVER['REQUEST_URI'] ); ?>" rel="nofollow"><?php _e("Logout", 'ForTraderMaster'); ?></a>
					<?php }?> 
                
                </div>
                
                <!-- - - - - - - - - - - - - - End of Enter Box - - - - - - - - - - - - - - - - -->

                <!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->

                <div class="logo">
					<a href="<?php echo get_lang_url_prefix();?>/" title="Home Page">
						<img src="<?php echo removeProtocol( get_bloginfo('template_url') ); ?>/images/logon.png" alt="">
					</a>
                </div>
				<div class="header_top_btn_box">
					<a href="<?php echo get_lang_url_prefix();?>/informers" class="header_top_button"><?php _e("For webmaster", 'ForTraderMaster'); ?></a>
				</div>
                 
                <!-- - - - - - - - - - - - - - End of Logo - - - - - - - - - - - - - - - - -->
            </header>

            <!-- - - - - - - - - - - - - - End of Header - - - - - - - - - - - - - - - - -->
            
            <?php get_template_part_by_locale( 'templates/header-banner' ); ?>

             <?php get_template_part( 'templates/header-nav' ); ?>

            <?php //if(is_home()) get_template_part( 'templates/rolling-menu' ); ?>


            <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

            <div id="content" class="clearfix">
