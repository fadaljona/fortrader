            </div>

            <!-- - - - - - - - - - - - - - End of Content - - - - - - - - - - - - - - - - -->
            
        </div><!-- / .container -->

        <!-- - - - - - - - - - - - - - End of Container - - - - - - - - - - - - - - - - -->


        <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

        <footer id="footer" itemscope itemtype="http://schema.org/WPFooter">

            <!-- - - - - - - - - - - - - - Footer Top - - - - - - - - - - - - - - - - -->
            
            <div class="footer_top">
                
                <div class="container">
                    
                    <div class="clearfix">

                        <!-- - - - - - - - - - - - - - Social List - - - - - - - - - - - - - - - - -->
                        <?php get_template_part_by_locale( 'templates/footer-sm-buttons' )?>
                        <!-- - - - - - - - - - - - - - End of Social List - - - - - - - - - - - - - - - - -->

                        <!-- - - - - - - - - - - - - - Newsletter Form - - - - - - - - - - - - - - - - -->

                        <form id="subscribeFormFooter" class="newsletter_form alignright">
						
							
							<span class="form-error valid-user-email"></span>
							<span class="form-error valid-all-form"></span>
                            
                            <label><?php _e("Subscribe to our newsletter", 'ForTraderMaster'); ?>:</label>

                            <input type="text" class="user-email" name="email" placeholder="<?php _e("Your email", 'ForTraderMaster'); ?>">
                            
                            <button type="submit"><?php _e("Subscribe", 'ForTraderMaster'); ?></button>
							
                        </form>

                        <div>
                            <a href="#" data-modal="#footerPopupSubscribe" class="arcticmodal share_btn">
                                <?php _e("Subscribe to ForTrader.org", 'ForTraderMaster'); ?>
                            </a>
                        </div>
						
						
						
						<!-- - - - - - - - - - - - - - Modal Window - - - - - - - - - - - - - - - - -->
						<div class="hide">
							<div id="footerPopupSubscribe" class="modal_box">
								<div class="arcticmodal-close modal_close"><i class="fa fa-times"></i></div>
								<div class="modal_inner">	
									<p class="modal_text"><?php _e("Subscribe to our newsletter and receive the latest news", 'ForTraderMaster'); ?></p>
									
									<form id="footerPopupSubscribeForm" method="post" action="./">
										<span class="form-error valid-all-form"></span>
										<span class="form-error valid-user-email"></span>
										<div class="subscribe">
											<input type="text" class="invalid user-email" name="email" placeholder="<?php _e("Email", 'ForTraderMaster'); ?>">
											<button class="" type="submit"><?php _e("Subscribe", 'ForTraderMaster'); ?></button>
										</div>    
									</form>
									
								</div>
							</div>	
						</div>
						<!-- - - - - - - - - - - - - - End of Modal Window - - - - - - - - - - - - - - - - -->
						<?php wp_enqueue_script('jquery.arcticmodal-0.3.min', get_bloginfo("stylesheet_directory") . '/plagins/arcticmodal/jquery.arcticmodal-0.3.min.js', array('jquery'), null); ?>

                        <!-- - - - - - - - - - - - - - End of Newsletter Form - - - - - - - - - - - - - - - - -->

                    </div>

                </div><!-- / .container -->

            </div><!-- / .footer_top -->

            <div class="container">
                
            <!-- - - - - - - - - - - - - - Footer Middle - - - - - - - - - - - - - - - - -->
            <?php get_template_part_by_locale( 'templates/footer_links' ); ?>
            <!-- - - - - - - - - - - - - - End of Footer Middle - - - - - - - - - - - - - - - - -->


            <!-- - - - - - - - - - - - - - Footer Bottom - - - - - - - - - - - - - - - - -->

            <div class="footer_bottom">
                
                <p>
					<?php _e("footerText", 'ForTraderMaster'); ?> <?php _e("footerTitle", 'ForTraderMaster'); ?> 2008 - <?php echo date('Y', time());?>. <a href="/sitemap.xml" class="link_accent"><?php _e("Site map (XML)", 'ForTraderMaster'); ?></a>
                </p>

                <ul class="list4">
<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter17779042 = new Ya.Metrika({ id:17779042, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/17779042" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			  ga('create', 'UA-34000109-1', 'auto');
			  ga('send', 'pageview');
			</script>           
                    <li><!--LiveInternet counter--><script type="text/javascript">document.write("<a href='//www.liveinternet.ru/stat/fortrader.ru/' "+"target=_blank><img src='//counter.yadro.ru/hit?t14.5;r"+escape(document.referrer)+((typeof(screen)=="undefined")?"":";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+";h"+escape(document.title.substring(0,80))+";"+Math.random()+"' alt='' title='LiveInternet: показано число просмотров за 24"+" часа, посетителей за 24 часа и за сегодня' "+"border='0' width='88' height='31'><\/a>")</script><!--/LiveInternet--></li>
                    <li><a href="#"><img src="<?php echo removeProtocol( get_bloginfo('template_url') ); ?>/images/ban_list_img4.jpg" alt=""></a></li>
                    <li><a href="#"><img src="<?php echo removeProtocol( get_bloginfo('template_url') ); ?>/images/ban_list_img5.jpg" alt=""></a></li>

                </ul>

            </div>

            <!-- - - - - - - - - - - - - - End of Footer Bottom - - - - - - - - - - - - - - - - -->

            </div>
            
        </footer>

        <div id="go_up"></div>

        <!-- - - - - - - - - - - - - - End of Footer - - - - - - - - - - - - - - - - -->
		
		<?php if( !is_user_logged_in() ) get_template_part( 'templates/popup-login' ); ?>
		
		<?php get_template_part( 'templates/popup-news' );?>
		
		<div class="loader-wrap"><div class="loader"><?php _e("Loading...", 'ForTraderMaster'); ?></div></div>
	
<?php 
$nowPost = is_single() || is_page();
if( $nowPost ){  $currentPostId = get_the_ID(); }
?>
<script>

<?php if( $nowPost ){  ?>

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return null;
}

function sendStats(){
	var prev_post_id = getCookie('prev_post_id');
	createCookie('prev_post_id', current_post_id, '');
	sendPost( 'prev_post_id='+prev_post_id+'&current_post_id='+current_post_id+'&referrer='+encodeURIComponent(document.referrer), '/services/admin/postsStats/ajaxAddTrafficStats' );
}
sendStats();


<?php } ?>
</script>


<script>
var jsThemeTexts = {
	captchaVlidationFail: "<?php _e("CAPTCHA validation fails, pass validation. If you can't-refresh the page", 'ForTraderMaster');?>",
};
</script>

<?php wp_enqueue_script('jquery-migrate', false, array(), null, true); ?>
<?php wp_enqueue_script('jquery.easing.1.3.js', get_bloginfo("stylesheet_directory") . '/js/jquery.easing.1.3.js', array('jquery'), null, true); ?>
<?php wp_enqueue_script('modernizr.js', get_bloginfo("stylesheet_directory") . '/js/modernizr.js', array('jquery'), null, true); ?>
<?php wp_enqueue_script('owl.carousel.js', get_bloginfo("stylesheet_directory") . '/plagins/owlCarousel/js/owl.carousel.js', array('jquery'), null, true); ?>
<?php wp_enqueue_script('jquery.liMarquee.js', get_bloginfo("stylesheet_directory") . '/plagins/liMarquee/js/jquery.liMarquee.js', array('jquery'), null, true); ?>
<?php wp_enqueue_script('script.core.js', get_bloginfo("stylesheet_directory") . '/js/script.core.js', array('jquery'), null, true); ?>
<?php wp_enqueue_script('script.plugins.js', get_bloginfo("stylesheet_directory") . '/js/script.plugins.js', array('jquery'), null, true); ?>
<?php
if( is_home() ){
	wp_enqueue_script('buttonSlider.js', get_bloginfo("stylesheet_directory") . '/js/buttonSlider.js', array('script.plugins.js'), null, true);
}
?>
<?php wp_enqueue_script('ajax_expolls.js', '/wp-includes/js/ajax_expolls.js', array('jquery'), null, true); ?>
<?php wp_enqueue_script('jquery.lazyload.js', get_bloginfo("stylesheet_directory") . '/js/jquery.lazyload.js', array('jquery'), null, true); ?>
<?php wp_enqueue_script('facebook.jssdk', '//connect.facebook.net/'.get_locale().'/sdk.js#xfbml=1&version=v2.5', array('jquery'), null, true );?>


<?php 
	ob_start("addPropertyStylesheet");
	wp_footer(); 
	ob_end_flush();
?>
<script>
jQuery(function() {
	jQuery("div.left_part img.lazy").lazyload({
		effect : "fadeIn"
	}).removeClass('lazy');
	jQuery("aside.sidebar img.lazy").lazyload({
		effect : "fadeIn"
	}).removeClass('lazy');
});
</script>














<!--social buttons-->
<script >
  window.___gcfg = {
    lang: '<?php echo pll_current_language('slug');?>'
  };
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>

<script>

FB.Event.subscribe('edge.create', function( url, html_element ) {
	
	var currentButton = jQuery(html_element);
	if( currentButton.hasClass('like-panel') ){
		anyLikesClick();
	}
	if( currentButton.hasClass('popup-like') ){
		popupClickLikes()
	}
	if( currentButton.hasClass('center-popup-like') ){
		setTimeout(function(){
			jQuery('#popup .js-form-block').hide();
			jQuery('#popup').css({height:'118px'});
			jQuery('#popup .js-message-block').show();
		},2000);
	}
	
});


window.twttr = (function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0],
	t = window.twttr || {};
	if (d.getElementById(id)) return t;
	js = d.createElement(s);
	js.id = id;
	js.async = true;
	js.src = "https://platform.twitter.com/widgets.js";
	fjs.parentNode.insertBefore(js, fjs);
	t._e = [];
	t.ready = function(f) {
		t._e.push(f);
	};
	return t;
}(document, "script", "twitter-wjs"));

twttr.ready(function (twttr) {
	twttr.events.bind(
		'click', 
		function (event){ 
			anyLikesClick();
		}
	);
});

function plusClick(args){
	anyLikesClick();
	sendUpdateSm();
}

function anyLikesClick(){
	ga('send', 'event', 'socialClick', 'anyLikes');
	sendUpdateSm();
}
function shareVkFacebookClick(){
	ga('send', 'event', 'socialClick', 'shareVkFacebook');
	sendUpdateSm();
}
function popupClickLikes(){
	ga('send', 'event', 'socialClick', 'popupClick');
	sendUpdateSm();
}
function popupClickNews( url ){
	ga('send', 'event', 'contentClick', 'popupNewsClick', url, {
     'transport': 'beacon',
     'hitCallback': function(){document.location = url;}
   });
}
function sendUpdateSm(){
	<?php if( $nowPost ){ ?>sendPost( 'id='+current_post_id, '/services/admin/postsStats/ajaxSetSmMarker' );<?php }?>
}

</script>

<?php if($_SERVER['HTTP_HOST'] == 'fortrader.org' && pll_current_language('slug') == 'ru'){?>
<script>
;(function($){
	var vkOpenClose = {
		init: function(){
			document.write( '<script type="text\/javascript" src="\/\/vk.com\/js\/api\/openapi.js?137"><\/script><!-- VK Widget --><div id="vk_community_messages"><\/div><script type="text\/javascript"> VK.Widgets.CommunityMessages("vk_community_messages", 4226976, {expanded: 0,disableExpandChatSound: "1",tooltipButtonText: "Есть вопрос?"}); <\/script>' );
		},
		createCookie: function(name) {
			var value = 1;
			var date = new Date();
			date.setTime(date.getTime() + ( 60 * 10 * 1000 ) );
			var expires = "; expires=" + date.toGMTString();
			document.cookie = name + "=" + value + expires + "; path=/";
		},
		getCookie: function(c_name) {
			if (document.cookie.length > 0) {
				c_start = document.cookie.indexOf(c_name + "=");
				if (c_start != -1) {
					c_start = c_start + c_name.length + 1;
					c_end = document.cookie.indexOf(";", c_start);
					if (c_end == -1) {
						c_end = document.cookie.length;
					}
					return unescape(document.cookie.substring(c_start, c_end));
				}
			}
			return null;
		}
	};
	var wWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
	if( wWidth > 700 ){
		vkOpenClose.init();
	}
})(jQuery);
</script>
<?php }?>



		<?php $insertId = rand();?><div id="<?php echo $insertId;?>"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("<?php echo $insertId;?>");s = document.createElement("script");s.src = "//fortrader.org/services/15135sz/jsRender?id=30&insertToId=<?php echo $insertId;?>&width="+screen.width;s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
    </body>
</html>