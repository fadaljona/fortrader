<?php get_header();?>
<?php
	$category = get_the_category();
	$wrap_itemtype = "http://schema.org/Article";
	
	$postCatsArr = array();
	
	foreach( $category as $cat ){
		if( $cat->cat_ID == 641 || $cat->cat_ID == 5 ){
			$wrap_itemtype = "http://schema.org/NewsArticle";
		}
		$postCatsArr[] = $cat->cat_ID;
	}
	
	$adsCatDetected = false;
	
	
	if( in_array( 11683, $postCatsArr ) || in_array( 1933, $postCatsArr ) ){
		$adsCatDetected = true;	
	}else{
		$chiled_cats = get_categories( array('parent' => 11683)); 
		foreach( $chiled_cats as $cat ){
			if( in_array( $cat->cat_ID, $postCatsArr ) ){
				$adsCatDetected = true;
				break;
			}
		}
		if( !$adsCatDetected ){
			$chiled_cats = get_categories( array('parent' => 1933)); 
			foreach( $chiled_cats as $cat ){
				if( in_array( $cat->cat_ID, $postCatsArr ) ){
					$adsCatDetected = true;
					break;
				}
			}
		}
	}
?>
<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part" itemscope itemtype="<?php echo $wrap_itemtype; ?>">
		<?php get_template_part_by_locale( 'templates/banner-728x90' );?>
		<?php $excludePostArr = array();?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="clearfix">
				<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
				<div class="breadcrumbs alignleft">
					<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
						<li>
							<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a href="<?php echo pll_home_url();?>" itemprop="item"><span itemprop="name"><?php _e("Home", 'ForTraderMaster'); ?></span></a>
								<meta itemprop="position" content="1" />
							</span>
						</li>
					<?php
						$excludePostArr[] = get_the_ID();
						$catmeta = get_post_meta(get_the_ID(), '_category_permalink', true);
						if( is_array($catmeta) ) $catmeta = $catmeta['category'];
						$articleSection = '';
						if( isset ( $catmeta ) && $catmeta ){
							$category_meta = get_category($catmeta);
							echo get_formated_category_parents( $catmeta, true);
							$articleSection = $category_meta->cat_name;
						}else{
							$i=2;
							foreach( $category as $breadcrumb_cat ){
								echo '<li><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="'.$breadcrumb_cat->cat_name.'" href="'.get_category_link( $breadcrumb_cat->cat_ID ).'" itemprop="item"><span class="breadcrumb-active" itemprop="name">'.$breadcrumb_cat->cat_name.'</span></a><meta itemprop="position" content="'.$i.'" /></span></li>';
								$tmpCat = $breadcrumb_cat->cat_ID;
								
								if( $i == 2 )
									$articleSection .= $breadcrumb_cat->cat_name;
								else
									$articleSection .= ', '.$breadcrumb_cat->cat_name;
								
								$i++;
							}
						}
						if( $catmeta ){
							$postCat = $catmeta;
						}else{
							$postCat = $tmpCat;
						}
					?>
					</ul>
				</div>
				
				<!-- - - - - - - - - - - - - - End of Breadcrumbs - - - - - - - - - - - - - - - - -->
				<!-- - - - - - - - - - - - - - Article Info - - - - - - - - - - - - - - - - -->
				<div class="article_info clearfix alignright">
					<?php
						$commentNumber = get_comments_number();
					?>
					<a itemprop="discussionUrl" href="<?php the_permalink();?>#comments"><span class="comment"><?php echo $commentNumber; ?></span></a>
					<span class="views"><?php echo get_post_meta (get_the_ID(),'views',true) ? get_post_meta (get_the_ID(),'views',true) : 0; ?></span>
					<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d F, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
				</div>
				<!-- - - - - - - - - - - - - - End of Article Info - - - - - - - - - - - - - - - - -->
			</div><!-- / .cleafix -->
			<hr>
			<h1 class="title2" itemprop="headline name"><?php the_title();?><?php edit_post_link(' <i></i>'); ?></h1>
			<?php get_template_part( 'templates/sm-top-post-buttons' ); ?>
			<!-- - - - - - - - - - - - - - Article Post - - - - - - - - - - - - - - - - -->
			<?php
				$aricleContent = apply_filters( 'the_content' ,get_the_content() );
				$yoastWpseoMetadesc = get_post_meta (get_the_ID(),'_yoast_wpseo_metadesc',true);
				if( $yoastWpseoMetadesc ){
					echo '<blockquote>'.$yoastWpseoMetadesc.'</blockquote>';
					$articleDesc = $yoastWpseoMetadesc;
				}else{
					$articleDesc = wp_trim_words($aricleContent, 50);
				}
			?>
			
			<span itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
				<img itemprop="contentUrl url" style="display:none;" alt="contentUrl" src="<?php echo p75GetThumbnail($post->ID, 400, 300, ""); ?>" />
				<meta itemprop="height" content="300" />
				<meta itemprop="width" content="400" />
			</span>
			<meta itemprop="commentCount" content="<?php echo $commentNumber; ?>" />
			<meta itemprop="articleSection" content="<?php echo $articleSection;?>" /> 
			<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?>
				<meta itemprop="datePublished" content="<?php the_time('Y-m-d h:m');?>" />
				<meta itemprop="dateModified" content="<?php echo date('Y-m-d h:m', strtotime($post->post_modified));?>" />
			<?php }?>
			<meta itemprop="description" content="<?php echo $articleDesc;?>" /> 
			<meta itemprop="mainEntityOfPage" content="<?php the_permalink();?>" /> 
			
			<?php $insertId = rand();?><div id="<?php echo $insertId;?>" style="margin-bottom: 15px;"></div><script type="text/javascript">(function(s, t) {if( wAWidth <= 767 ){t = document.getElementById("<?php echo $insertId;?>");s = document.createElement("script");s.src = "//fortrader.org/services/15135sz/jsRender?id=41&insertToId=<?php echo $insertId;?>";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);} })(window, document );</script>
			
			<article class="clearfix main-post-content">
				<div itemprop="articleBody">	
				<?php 
					if( in_array( 1822, $postCatsArr ) ){
						$foundImages = 0;
						$adsBlock = '<div id="1484557648781"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1484557648781");s = document.createElement("script");s.src = "//fortrader.org/services/15135sz/jsRender?id=52&insertToId=1484557648781";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>';
						$replacedContent = preg_replace('/(<a href="[^>]+?><img[^>]+?><\/a>)/', '${1}'.$adsBlock.' ', $aricleContent, 1, $foundImages);
						if( !$foundImages ){
							$replacedContent = preg_replace('/(<img[^>]+?>)/', '${1}'.$adsBlock.' ', $aricleContent, 1, $foundImages);
						}
						echo $replacedContent;
					}else{
						echo $aricleContent; 
					}
					
				?>
				</div>
				
				<?php
					$istMeta = get_post_meta (get_the_ID(),'ist',true);
					if( $istMeta ){
				?>
					<div class="info_buttons clearfix istmeta">
						<?=str_replace( '<a ', '<a class="categories_btn" ', $istMeta )?>
					</div>
				<?php }; ?>
				
				<?php $insertId = rand();?><div id="<?php echo $insertId;?>"></div><script type="text/javascript">(function(s, t) {if( wAWidth <= 767 ){t = document.getElementById("<?php echo $insertId;?>");s = document.createElement("script");s.src = "//fortrader.org/services/15135sz/jsRender?id=42&insertToId=<?php echo $insertId;?>";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);} })(window, document );</script>
				
				<span itemprop="publisher" itemscope itemtype="http://schema.org/Organization" style="display:none">
					<span itemprop="name"><?php echo get_option('fortraderPubName');?></span>
					<span itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
						<img itemprop="contentUrl url" alt="contentUrl" src="<?php echo get_site_url();?>/wp-content/themes/ForTraderMaster/images/logo.png" />
						<meta itemprop="height" content="62" />
						<meta itemprop="width" content="205" />
					</span>
					<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<span itemprop="streetAddress"><?php echo get_option('fortraderStreetAddress');?></span>
						<span itemprop="postalCode"><?php echo get_option('fortraderPostalCode');?></span>
						<span itemprop="addressLocality"><?php echo get_option('fortraderAddressLocality');?></span>
					</span>
					<span itemprop="telephone"><?php echo get_option('fortraderTelephone');?></span>
				</span>
				
				<?php if( $adsCatDetected ){ ?>
					<?php $insertId = rand();?><div id="<?php echo $insertId;?>"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("<?php echo $insertId;?>");s = document.createElement("script");s.src = "//fortrader.org/services/15135sz/jsRender?id=46&insertToId=<?php echo $insertId;?>";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
				<?php } ?>
				
                <?php if(has_tag('bitcoin')) {?>
				<div id="1501232523211" style="margin-top:30px;"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1501232523211");s = document.createElement("script");s.src = "https://fortrader.org/services/15135sz/jsRender?id=58&insertToId=1501232523211";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
                <?php } else { ?>
                <div id="1532617667263"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1532617667263");s = document.createElement("script");s.src = "https://fortrader.org/services/15135sz/jsRender?id=60&insertToId=1532617667263";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
                <?php }?>
				
				<div class="author_article clearfix" itemprop="author" itemscope itemtype="http://schema.org/Person">
					<?php 		
						$author = get_userdata( $post->post_author ); 
				
						$authorName = getShowUserName( $author );
					?> 
					<a href="<?php echo get_author_posts_url($author->ID);?>" class="author_avatar alignleft">
						<?php renderImg( p75GetServicesAuthorThumbnail( 107, 107, $author->ID ), 107, 107, $authorName, 1 );?>
					</a>
					<div class="author_text">
						<h5 class="author_name"><a itemprop="url" href="<?php echo get_author_posts_url($author->ID);?>"><span itemprop="name"><?php echo $authorName;?></span></a></h5>
						<p itemprop="jobTitle"><?php echo get_user_meta( $author->ID, 'description', true );?></p>
					</div>
				</div><!--/ .author_article -->
				
				<div class="clearfix">
					<a href="<?php echo get_author_posts_url($author->ID);?>" class="article_btn alignright"><?php _e("Articles by this author", 'ForTraderMaster'); ?></a>
					<div class="box1 social_list clearfix">
						<div style="height:25px;"></div> 
					<?php
					/*	$tw_link = get_user_meta( $author->ID, 'tw_link', true ) ? get_user_meta( $author->ID, 'tw_link', true ) : 'https://twitter.com/fortrader';
						$fb_link = get_user_meta( $author->ID, 'fb_link', true ) ? get_user_meta( $author->ID, 'fb_link', true ) : 'https://www.facebook.com/fortrader';
						$vk_link = get_user_meta( $author->ID, 'vk_link', true ) ? get_user_meta( $author->ID, 'vk_link', true ) : 'https://vk.com/fortrader';
					?>
						<ul class="social_list clearfix">
							<?php if($tw_link){?><li><a href="<?php echo $tw_link;?>" class="tw" rel="nofollow"></a></li><?php }?>
							<?php if($fb_link){?><li><a href="<?php echo $fb_link;?>" class="fb" rel="nofollow"></a></li><?php }?>
							<?php if($vk_link){?><li><a href="<?php echo $vk_link;?>" class="vk" rel="nofollow"></a></li><?php }?>
						</ul>*/?>
					</div><!--/ .box1 -->
				</div><!--/ .clearfix -->
				<section class="section_offset">
				<?php
					$posttags = get_the_tags();
					if( !empty($posttags) ){
				?>
					
						<h3 class="page_title2"><?php _e("INFORMATION BY TOPIC", 'ForTraderMaster'); ?>:</h3>
						<!-- - - - - - - - - - - - - - Button Box - - - - - - - - - - - - - - - - -->
						<div class="info_buttons clearfix">
						<?php 
							foreach( $posttags as $tag ){
						?>
								<a href="<?php echo get_tag_link($tag->term_id);?>" class="categories_btn"><?php echo $tag->name?></a>
						<?php } ?>
						</div>
						<!-- - - - - - - - - - - - - - End of Button Box - - - - - - - - - - - - - - - - -->
					
				<?php } ?>
				</section>
			</article><!--/ .section_offset -->
			<!-- - - - - - - - - - - - - - End of Article Post - - - - - - - - - - - - - - - - -->
			
			<?php //get_template_part( 'templates/sm-bottom-post-buttons' ); ?>

			<div class="blockdiv3 clearfix">
<!-- Yandex.RTB R-A-341126-2 -->
<div id="yandex_rtb_R-A-341126-2"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-341126-2",
                renderTo: "yandex_rtb_R-A-341126-2",
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>
				<!-- div><?php get_template_part( 'templates/google-subscribe-portrait-single' );?></div -->
				<!-- div><?php dynamic_sidebar('300x250-3-post-banner');?></div-->
			</div>

			<?php get_template_part( 'templates/comments' );?>
		<?php endwhile; else : ?>
			<h2 class="center"><?php _e("Not found", 'ForTraderMaster'); ?></h2>
			<p class="center"><?php _e("This page does not exist", 'ForTraderMaster'); ?></p>
		<?php endif; ?>
			
		<?php 
			$cacheDuration = get_option('theme_cache_duration'); 
			global $fortraderCache;
			$cache_key = 'theme_' . md5('under-post-from-this-cat-'.$postCat);
			if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
		?>
			<?php /*Ещё из этой категории*/?>
			<section class="section_offset">
				<h3 class="page_title2"><?php _e("More from this category", 'ForTraderMaster'); ?><i class="icon files_icon"></i></h3>
				<hr>
				<div class="news_post_box clearfix single-load-more-wrapper" data-cat-id="<?php echo $postCat; ?>">
					<?php
						$r = new WP_Query( array(
							'post_status'  => 'publish',
							'post_type' => 'post',
							'orderby' => 'date',
							'order'   => 'DESC',
							'posts_per_page' => 4,
							'cat' => $postCat,
							'post__not_in' => $excludePostArr,
							'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
							'paged' => 1,
						) );
					?>
					<?php
						$max_num_pages = $r->max_num_pages;
						if ($r->have_posts()) :
						while ( $r->have_posts() ) : $r->the_post();
							get_template_part( 'templates/loop-post-big' );
						endwhile;
						wp_reset_postdata();
						endif;
					?>
				</div>
				<?php if( $max_num_pages > 1 ){ ?>
					<a href="#" class="load_btn single-load-more-posts chench_btn" data-cat-id="<?php echo $postCat; ?>" data-exclude="<?php echo implode( ',', $excludePostArr ); ?>" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
				<?php }?>
			</section>
		<?php $fortraderCache->endCache(); } ?>
			
		<?php
			$cache_key = 'theme_' . md5('under-post-today-block');
			if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
		?>	
			<?php /*Сегодня*/?>	
			<?php get_template_part( 'templates/single-today-block' ); ?>
		<?php $fortraderCache->endCache(); }?>

		<?php
			$cache_key = 'theme_' . md5('under-post-actual-block');
			if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
		?>
			<?php /*Актуально*/?>
			<?php get_template_part( 'templates/single-focus' ); ?>
		<?php $fortraderCache->endCache(); } ?>
			
		<?php
			$cache_key = 'theme_' . md5('under-post-editor-recommends-block');
			if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
		?>
			<?php /*Редактор рекомендует*/?>	
			<?php get_template_part( 'templates/single-editor-recommends' ); ?>
		<?php $fortraderCache->endCache(); }?>
		


	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php get_sidebar();?>
<?php get_footer();?>
