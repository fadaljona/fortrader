<!DOCTYPE html>
<html <?php language_attributes(); ?>>

    <head>

        <!-- Basic page needs
        ============================================ -->
        <title>Financial Portal ForTrader.org</title>
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />

        <!-- Mobile specific metas
        ============================================ -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Favicon
        ============================================ -->
        <link href="<?php bloginfo('template_url'); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" >

         <!-- Libs CSS
        ============================================ -->
        <?php wp_enqueue_style( 'jquery.arcticmodal-0.3.css', get_bloginfo('template_url').'/plagins/arcticmodal/jquery.arcticmodal-0.3.css', array(), null );?>
        <?php wp_enqueue_style( 'owl.carousel.css', get_bloginfo('template_url').'/plagins/owlCarousel/css/owl.carousel.css', array(), null );?>
        <?php wp_enqueue_style( 'jquery.formstyler.css', get_bloginfo('template_url').'/plagins/jQueryFormStyler/jquery.formstyler.css', array(), null );?>

        <!-- Theme CSS
        ============================================ -->
        <?php wp_enqueue_style( 'style.css', get_bloginfo('stylesheet_url'), array(), null );?>
        <?php wp_enqueue_style( 'fx_style.css', get_bloginfo('template_url').'/css/fx_style.css', array(), null );?>
        <?php wp_enqueue_style('fx_exchange-style.css', get_bloginfo('template_url').'/css/fx_exchange-style.css', array(), null);?>

        <!-- Media Queries CSS
        ============================================ -->
        <?php wp_enqueue_style('responsive.css', get_bloginfo('template_url').'/css/responsive.css', array(), null );?>
        <?php wp_enqueue_style('fx_responsive.css', get_bloginfo('template_url').'/css/fx_responsive.css', array(), null );?>
        <?php wp_enqueue_style('fx_exchange-responsive.css', get_bloginfo('template_url').'/css/fx_exchange-responsive.css', array(), null);?>


        <!-- Old IE 
        ============================================ -->

        <!--[if lt IE 8]>
        <div style=' clear: both; text-align:center; position: relative;'>
          <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
          </a>
        </div>
        <![endif]-->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="<?php echo get_bloginfo('template_url');?>/js/html5.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_bloginfo('template_url');?>/css/ie.css">
        <![endif]-->

        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
		<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        
        <?php 
            remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );
            wp_enqueue_script("jquery", false, array(), null, false);
            wp_head();
        ?>

<?php 
$nowPost = is_single() || is_page();
if( $nowPost ){  
	$currentPostId = get_the_ID(); 
?>
<script>
var current_post_id = <?=$currentPostId?>;
</script>
<?php
}
?>
<script>
function sendPost( params, url ){
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send( params );
}
	var wAWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
</script>	



<?php
    loadYii(); 
    $currenciesForConverter = CurrencyRatesModel::getEcbCurrenciesForConverter();
    $sidebarConverterFlagStyles = '.fx_convert-flag-eu {background: url(/uploads/country/flags/shiny/16/EU.png) center left 10px no-repeat;}.fx_sb-convert-select.fx_convert-flag-eu .jq-selectbox__select-text {background: url(/uploads/country/flags/shiny/16/EU.png) center left no-repeat;}';
    foreach( $currenciesForConverter as $key => $val ){
        $flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $val['country']);
        if( $flagSrc ) $sidebarConverterFlagStyles .= ".fx_convert-flag-{$val['country']} {background: url({$flagSrc}) center left 10px no-repeat;}.fx_sb-convert-select.fx_convert-flag-{$val['country']} .jq-selectbox__select-text {background: url({$flagSrc}) center left no-repeat;}";
    }
    echo '<style>'.$sidebarConverterFlagStyles.'</style>';
?>

    </head>

    <body>
    
        
        <!-- - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->

        <div class="fx_wrapper">

            <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

            <header id="header"  class="fx_header clearfix">

                <div class="fx_container">

                    <!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->

                    <div class="logo fx_logo clearfix">

                      <a href="<?php echo get_lang_url_prefix();?>/" title="Home Page">
                          HomeStyle
                          <img src="<?php echo removeProtocol( get_bloginfo('template_url') ); ?>/images/fx_logo.jpg" alt="">
                      </a>

                      <a href="#" class="fx_mob-menu"></a>

                    </div>
                     
                    <!-- - - - - - - - - - - - - - End of Logo - - - - - - - - - - - - - - - - -->                

                    <!-- - - - - - - - - - - - - - Header List - - - - - - - - - - - - - - - - -->
                    
                    <div class="header_top_btn_box fx_webmaster">
                        
                        <a href="<?php echo get_lang_url_prefix();?>/informers" class="header_top_button fx_header-top_btn"><?php _e("For webmaster", 'ForTraderMaster'); ?></a>

                    </div>
                    
                    <!-- - - - - - - - - - - - - - End of Header Top List - - - - - - - - - - - - - - - - -->
                
                    <!-- - - - - - - - - - - - - - Enter Box - - - - - - - - - - - - - - - - -->
                    
                    <div class="enter_box alignright fx_enterbox clearfix">
                        <?php if (!is_user_logged_in()){ ?>
                            <?php echo headerLangSwitcher();?>
                            <a href="/wp-login.php" class="fx_header-exit fx_header_login arcticmodal" data-modal="#logIn"><?php _e("Login", 'ForTraderMaster'); ?></a>
                            <a href="/wp-login.php?action=register" class="fx_header-exit fx_header_register arcticmodal" data-modal="#registration"><?php _e("Registration", 'ForTraderMaster'); ?></a>
                        <?php } else { 
                            $currentUser = wp_get_current_user();
							$userName = getShowUserName( $currentUser );
                        ?>
                            <span><?php _e("Hi", 'ForTraderMaster'); ?>, <a href="<?php echo get_author_posts_url($currentUser->ID);?>"><strong><?=$userName?></strong></a></span>

                            <?php echo headerLangSwitcher();?>

                            <a href="<?php echo wp_logout_url( str_replace( '/' . pll_current_language('slug'), '', home_url() ) . $_SERVER['REQUEST_URI'] ); ?>" class="fx_header-exit" rel="nofollow"><?php _e("Logout", 'ForTraderMaster'); ?></a>
                        <?php }?>
                    </div>
                    
                    <!-- - - - - - - - - - - - - - End of Enter Box - - - - - - - - - - - - - - - - -->

                </div>
                
            </header>


            <div class="fx_container fx_bg">

                <!-- - - - - - - - - - - - - - End of Header - - - - - - - - - - - - - - - - -->


                <?php get_template_part_by_locale('templates/header-services-nav-layout2'); ?>


                <!-- - - - - - - - - - - - - - Banner 1063*90 - - - - - - - - - - - - - - - - -->
                
                <div class=" fx_long-blockdiv">
                    
                    <div id="1509959267243"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1509959267243");s = document.createElement("script");s.src = "https://fortrader.org/services/15135sz/jsRender?id=51&insertToId=1509959267243";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>

                </div>

                <!-- - - - - - - - - - - - - - End of Banner 1063*90 - - - - - - - - - - - - - - - - -->

                <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->
                <?php get_template_part('templates/header-nav-layout2'); ?>
                <div id="content" class="clearfix">