<?php
function ajaxLoadPostsForHome() {

	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 3,
		'cat' => $_POST['cat'],
		'paged' => $_POST['page'],
	) );
	if ($r->have_posts()) :
		while ( $r->have_posts() ) : $r->the_post();
			get_template_part( 'templates/loop-post-col3' );
		endwhile;
		wp_reset_postdata();
	endif;
	die();
}
function ajaxLoadPostsBrokerNews() {

	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 6,
		'cat' => $_POST['cat'],
		'paged' => $_POST['page'],
	));
	if ($r->have_posts()) :
		while ( $r->have_posts() ){
			$r->the_post();
			echo '<div class="post_col2 post_col_sm1">';
			get_template_part( 'templates/loop-post-small-no-border' );
			echo '</div><!--/ .post_col -->';
		}
		wp_reset_postdata();
	endif;
	die();
}
function ajaxLoadPostsCalendarNews() {

	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 6,
		'paged' => $_POST['page'],
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'index',
				'value' => '',
				'compare' => '!=',
			),
			array(
				'key' => 'country',
				'value' => '',
				'compare' => '!=',
			),
		)
	));
	if ($r->have_posts()) :
		while ( $r->have_posts() ){
			$r->the_post();
			echo '<div class="post_col2 post_col_sm1">';
			get_template_part( 'templates/loop-post-small-no-border' );
			echo '</div><!--/ .post_col -->';
		}
		wp_reset_postdata();
	endif;
	die();
}
function ajaxLoadPostsCalendarEventNews() {

	if( !isset($_POST['country']) || !$_POST['country'] || !isset($_POST['index']) || !$_POST['index'] ) return false;

	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 6,
		'paged' => $_POST['page'],
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'index',
				'value' => $_POST['index'],
				'compare' => '=',
			),
			array(
				'key' => 'country',
				'value' => $_POST['country'],
				'compare' => '=',
			),
		)
	));
	if ($r->have_posts()) :
		while ( $r->have_posts() ){
			$r->the_post();
			echo '<div class="post_col2 post_col_sm1">';
			get_template_part( 'templates/loop-post-small-no-border' );
			echo '</div><!--/ .post_col -->';
		}
		wp_reset_postdata();
	endif;
	die();
}
function ajaxLoadAuthorsForHome() {

	$r = new WP_User_Query( array(
		'number' => 4,
		'offset' => 4 * $_POST['page'],
		'meta_query' => array(
			array(
				'key'     => 'show_user_on_home',
				'value'   => '1',
				'compare' => '=',
			),
		),
	));
	$authors = $r->get_results();
	if( count($authors) ){
		foreach ($authors as $author){
				$authorName = getShowUserName( $author );
		?>
			<div class="post_col4 post_col_sm2">
				<figure class="post_autor">
					<a href="<?php echo get_author_posts_url($author->ID);?>">
						<img src="<?php echo p75GetServicesAuthorThumbnail( 175, 200, $author->ID );?>"  alt="<?php echo $authorName;?>" title="<?php echo $authorName;?>" />
					</a>
					<figcaption>
						<h6><a href="<?php echo get_author_posts_url($author->ID);?>"><?php echo $authorName;?></a></h6>
						<p><?php echo $author->about;?></p>
					</figcaption>
				</figure>
			</div><!-- / .post_col4 -->
		<?php } ?>
		<div class="clear"></div>
	<?
	}
	die();
}
function ajaxLoadMoreIndicators() {
	?>
	<div class="post_box">
		<div class="post_col2 post_col_sm1">
			<div class="blockdiv1"><?php dynamic_sidebar('home-indicator-ads');?></div>
		</div><!-- / .post_col2 -->
		<?php
			$r = new WP_Query( array(
				'post_status'  => 'publish',
				'post_type' => 'post',
				'orderby' => 'date',
				'order'   => 'DESC',
				'posts_per_page' => 2,
				'cat' => $_POST['cat'],
				'offset' => $_POST['offset'],
			) );
			if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post();
				get_template_part( 'templates/loop-post-col4' );
				$excludePostArr[] = get_the_ID();
			endwhile;
			wp_reset_postdata();
			endif;
		?>
	</div><!-- / .post_box -->
	<div class="post_box">
		<?php
			$r = new WP_Query( array(
				'post_status'  => 'publish',
				'post_type' => 'post',
				'orderby' => 'date',
				'order'   => 'DESC',
				'posts_per_page' => 4,
				'cat' => $_POST['cat'],
				'offset' => $_POST['offset']+2,
			) );
			if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post();
				get_template_part( 'templates/loop-post-col4' );
				$excludePostArr[] = get_the_ID();
			endwhile;
			wp_reset_postdata();
			endif;
		?>
		<div class="clear"></div>
	</div><!-- / .post_box -->
	<?php
	die();
}
function ajaxLoadMoreTagPosts() {

	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 3,
		'tag_id' => $_POST['tagId'],
		'paged' => $_POST['tagPage'],
	) );
	if ($r->have_posts()) :
		while ( $r->have_posts() ) : $r->the_post();
			get_template_part( 'templates/loop-post-col3' );
		endwhile;
		wp_reset_postdata();
	endif;
	die();
}
function ajaxSingleLoadMorePosts() {

	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 4,
		'cat' => $_POST['cat'],
		'paged' => $_POST['page'],
		'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
		'post__not_in' => explode(',', $_POST['exclude']),

	) );
	if ($r->have_posts()) :
		while ( $r->have_posts() ) : $r->the_post();
			get_template_part( 'templates/loop-post-big' );
		endwhile;
		wp_reset_postdata();
	endif;
	die();
}
function ajaxCategoryLoadMorePosts() {

	if( $_POST['sort'] == 'views' ){
		$queryArgs = array(
			'post_status'  => 'publish',
			'post_type' => 'post',
			'meta_key' => 'views',
			'orderby' => 'meta_value_num',
			'order'   => 'DESC',
			'posts_per_page' => get_option('posts_per_page'),
			'cat' => $_POST['cat'],
			'paged' => $_POST['page'],
		);
	}else{
		$queryArgs = array(
			'post_status'  => 'publish',
			'post_type' => 'post',
			'orderby' => 'date',
			'order'   => 'DESC',
			'posts_per_page' => get_option('posts_per_page'),
			'cat' => $_POST['cat'],
			'paged' => $_POST['page'],
		);
	}

	$r = new WP_Query( $queryArgs );
	if ($r->have_posts()) :
		$i=0;

		while ( $r->have_posts() ) : $r->the_post();

			if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
			if( $i<2 ) get_template_part( 'templates/loop-post-big-cat' );
			if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );
			if( $i==2 ) echo '<div class="post_box">';
			if( $i>1 && $i< 5 ) get_template_part( 'templates/loop-post-col3-cat' );
			if( $i==4 ) echo '</div><hr />';

			if( $i==5 ) get_template_part( 'templates/loop-post-2-befor' );
			if( $i<7 && $i>4 ) get_template_part( 'templates/loop-post-big-cat' );
			if( $i==6 ) get_template_part( 'templates/loop-post-2-after' );
			if( $i==7 ) echo '<div class="post_box">';
			if( $i>6 && $i< 10 ) get_template_part( 'templates/loop-post-col3-cat' );
			if( $i==9 ) echo '</div><hr />';

			if( $i==10 ) echo '<div class="post_box">';
			if( $i>9 ) get_template_part( 'templates/loop-post-col3-cat' );
			if( ($i>9) && ( ($i+1-10)%3 == 0 ) ) echo '<div class="clear"></div>';

		$i++;
		endwhile;
		if( $i<2 )get_template_part( 'templates/loop-post-2-after' );
		if( $i>2 && $i< 5 ) echo '</div><hr />';
		if( $i<7 && $i>5 ) get_template_part( 'templates/loop-post-2-after' );
		if( $i>7 && $i< 10 ) echo '</div><hr />';

		if( $i>10 ) echo '</div><hr />';

		wp_reset_postdata();
	endif;
	die();
}
function ajaxCategoryLoadMoreQuestions() {

	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => $_POST['perPage'],
		'cat' => get_option('questions_cat_id'),
		'paged' => $_POST['page'],
	) );
	if ($r->have_posts()) :
		echo '<div class="post_box">';
		while ( $r->have_posts() ) : $r->the_post();
			get_template_part( 'templates/loop-post-question' );
		endwhile;
		echo '</div>';
		wp_reset_postdata();
	endif;
	die();
}
function ajaxLoadMoreToday42() {

	$today = getdate();
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 6,
		'paged' => $_POST['page'],
		'year' => $today["year"],
		'monthnum' => $today["mon"],
		'day' => $today["mday"],
		'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,

	) );
	if ($r->have_posts()) :
		$i=0;
		while ( $r->have_posts() ) : $r->the_post();
			if( $i==0 ) get_template_part( 'templates/loop-post-4-befor' );
			if( $i<4 ) get_template_part( 'templates/loop-post-4' );
			if( $i==1 ) echo '<div class="clear"></div>';
			if( $i==3 ) get_template_part( 'templates/loop-post-4-after' );
			if( $i==4 ) get_template_part( 'templates/loop-post-2-befor' );
			if( $i>3 ) get_template_part( 'templates/loop-post-2' );
			if( $i==5 ) get_template_part( 'templates/loop-post-2-after' );
			$i++;
		endwhile;
		if( $i<4 )get_template_part( 'templates/loop-post-4-after' );
		if( $i>4 && $i<6 )get_template_part( 'templates/loop-post-2-after' );
		wp_reset_postdata();
	endif;
	die();
}
function ajaxLoadMoreQuotesTagsPosts42() {
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 4,
		'paged' => $_POST['page'],
		'tax_query' => array(
			array(
				'taxonomy' => 'quotes',
				'field'    => 'term_id',
				'terms'    => $_POST['term'],
			),
		),
		'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
	) );

	if ($r->have_posts()) :
		$i=0;
		while ( $r->have_posts() ) : $r->the_post();
			if( $i==0 ) get_template_part( 'templates/loop-post-4-befor-no-hr' );
			if( $i<6 ) get_template_part( 'templates/loop-post-4' );
			if( (($i+1)%2)==0 ) echo '<div class="clear"></div>';
			if( $i==5 ) get_template_part( 'templates/loop-post-4-after' );
			$i++;
		endwhile;
		if( $i<6 )get_template_part( 'templates/loop-post-4-after' );
		wp_reset_postdata();
	endif;
	die();
}
function ajaxLoadMoreRecommendateSingle() {

	$current_date1 = new DateTime("now");
	$currentdate1=$current_date1->format('Y-m-d H:i:s');
	$r = new WP_Query( array(
	'post_status' => 'publish',
	'post_type' => 'post',
	'posts_per_page' => 4,
	'paged' => $_POST['page'],
	'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'important',
				'value'   => '1',
				'compare' => '=',
			),
			array(
				'key'     => 'daylife',
				'value'   => $currentdate1,
				'compare' => '>',
			),
		),
	) );
	if ($r->have_posts()) :
		while ( $r->have_posts() ) : $r->the_post();
			get_template_part( 'templates/loop-post-big' );
		endwhile;
		wp_reset_postdata();
	endif;
	die();
}
function ajaxLoadMoreFocusSingle() {

	$current_date1 = new DateTime("now");
	$currentdate1=$current_date1->format('Y-m-d H:i:s');
	$r = new WP_Query( array(
	'post_status' => 'publish',
	'post_type' => 'post',
	'posts_per_page' => 4,
	'paged' => $_POST['page'],
	'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'focus',
				'value'   => '1',
				'compare' => '=',
			),
			array(
				'key'     => 'actualDaylife',
				'value'   => $currentdate1,
				'compare' => '>',
			),
		),
	) );
	if ($r->have_posts()) :
		while ( $r->have_posts() ) : $r->the_post();
			get_template_part( 'templates/loop-post-big' );
		endwhile;
		wp_reset_postdata();
	endif;
	die();
}
function ajaxLoadMoreAuthorPosts() {


	switch ($_POST['type']){
		case 'fresh':
			$r = new WP_Query( array(
				'post_status'  => 'publish',
				'post_type' => 'post',
				'orderby' => 'date',
				'order'   => 'DESC',
				'paged' => $_POST['page'],
				'posts_per_page' => 14,
				'author' => $_POST['author'],
			) );
			break;
		case 'popular':
			$r = new WP_Query( array(
				'post_status'  => 'publish',
				'post_type' => 'post',
				'meta_key' => 'views',
				'orderby' => 'meta_value_num',
				'order'   => 'DESC',
				'posts_per_page' => 14,
				'paged' => $_POST['page'],
				'author' => $_POST['author'],
			) );
			break;
		case 'commented':
			$r = new WP_Query( array(
				'post_status'  => 'publish',
				'post_type' => 'post',
				'orderby' => 'comment_count',
				'order'   => 'DESC',
				'posts_per_page' => 14,
				'paged' => $_POST['page'],
				'author' => $_POST['author'],
			) );
			break;
	}

	if ($r->have_posts()):
		$i=0;
		while ( $r->have_posts() ) : $r->the_post();
			if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
			if( $i<2 ) get_template_part( 'templates/loop-post-big' );
			if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );

			if( $i==2 ) echo '<div class="post_box">';
			if( $i>1 ) get_template_part( 'templates/loop-post-col3' );

			if( $i>2 && ( ($i+1-2)%3 == 0 ) ) echo '<div class="clear"></div><hr>';

			if( $i>2 && ( ($i+1-2)%6 == 0 ) && $i<13 ) {echo '<div class="blockdiv1">'; dynamic_sidebar('banner-728x90'); echo '</div>';};

			$i++;

		endwhile;
		if($i<2) get_template_part( 'templates/loop-post-2-after' );
		if($i>2) echo '</div>';
		wp_reset_postdata();
	endif;

	die();
}
function ajaxUpdateSidebarRates(){
	global $wpdb;
	$curr_array = array();
	if ($data = $wpdb->get_results("SELECT * FROM ft_course ORDER BY curr_date DESC LIMIT 2", ARRAY_A)) {
		foreach ($data as $currency){
			$change = ($currency['movement'] > 0) ? '+' : '';
			$change .= round($currency['movement'], 2);
			$curr_array[$currency['cbr_id']] = array(
				'change'=>$change,
				'value'=>round($currency['val'], 2),
				'date'=>date('d/m', strtotime($currency['curr_date'])),
				'movement'=>$currency['movement'],
			);
		}

	}
	?>
	<h4><?php _e("Rate of Exchange", 'ForTraderMaster'); ?> <span class="grey_color"><?php echo $curr_array[840]['date']; ?></span><i class="icon rates_icon"></i></h4>
	<div class="rates_box clearfix">
		<div>
			<i class="icon dollar_icon"></i>
			<span><?php echo $curr_array[840]['value']; ?></span> <span class="<?php if($curr_array[840]['movement']>0) echo "green_color"; else echo "red_color";?>"><?php echo $curr_array[840]['change']; ?></span>
		</div>
		<div>
			<i class="icon euros_icon"></i>
			<span><?php echo $curr_array[978]['value']; ?></span> <span class="<?php if($curr_array[978]['movement']>0) echo "green_color"; else echo "red_color";?>"><?php echo $curr_array[978]['change']; ?></span>
		</div>
	</div>
	<div class="time_box align_right">
		<time datetime="<?php echo date( 'Y-m-d' );?>"><?php echo mysql2date('d F, Y',  date('Y-m-d') ); ?></time>
	</div>
	<?php
	die();
}
function ajaxLoadMorePostsForSlider() {

	if( $_POST['type'] == 'recommend' ){
		$current_date1 = new DateTime("now");
		$currentdate1=$current_date1->format('Y-m-d H:i:s');
		$r = new WP_Query( array(
			'post_status' => 'publish',
			'post_type' => 'post',
			'posts_per_page' => $_POST['postsToLoad'],
			'offset' => $_POST['offset'],
			'meta_query' => array(
				'relation' => 'AND',
					array(
						'key'     => 'important',
						'value'   => '1',
						'compare' => '=',
					),
					array(
						'key'     => 'daylife',
						'value'   => $currentdate1,
						'compare' => '>',
					),

				),
			)
		);


		$found_posts = $r->found_posts - $_POST['offset'];
		$pagesToShow = $found_posts;
		$ost = $found_posts % $_POST['postsToLoad'];
		if( $ost !=0 ) $pagesToShow = $found_posts + $_POST['postsToLoad'] - $ost;
		$pagesToShow = $pagesToShow / $_POST['postsToLoad'];

		ob_start();

		$i=0;
		if ($r->have_posts()) :
		while ( $r->have_posts() ) : $r->the_post(); ?>
			<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
				<!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
				<figure class="post">
					<a href="<?php the_permalink();?>"><img src="<?php echo p75GetThumbnail(get_the_ID(), 236, 200, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /></a>
					<div class="post_info clearfix">
						<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d M, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
						<span><?php echo get_post_meta (get_the_ID(),'views',true); ?></span>
					</div>
					<hr>
					<figcaption><a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a></figcaption>
				</figure>
				<!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
			</div>
		<?php
				$i++;
			endwhile;
			wp_reset_postdata();
		endif;

		$content = ob_get_contents();
		ob_end_clean();
		echo json_encode( array('pages' => $pagesToShow, 'content' => $content) );

	}elseif($_POST['type'] == 'beginer' || $_POST['type'] == 'professional' || $_POST['type'] == 'binnary'){
		if($_POST['type'] == 'beginer') $catIds='11806,11724,24,2321';
		elseif( $_POST['type'] == 'professional' ) $catIds='6,11726,2284,7,3821,9527,7489';
		elseif( $_POST['type'] == 'binnary' ) $catIds='11683,11761,11801';
		$r = new WP_Query( array(
			'post_status'  => 'publish',
			'post_type' => 'post',
			'orderby' => 'date',
			'order'   => 'DESC',
			'posts_per_page' => $_POST['postsToLoad'],
			'offset' => $_POST['offset'],
			'cat' => $catIds,
			) );

		$found_posts = $r->found_posts - $_POST['offset'];
		$pagesToShow = $found_posts;
		$ost = $found_posts % $_POST['postsToLoad'];
		if( $ost !=0 ) $pagesToShow = $found_posts + $_POST['postsToLoad'] - $ost;
		$pagesToShow = $pagesToShow / $_POST['postsToLoad'];

		ob_start();

		if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post(); ?>
				<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
					<!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
					<figure class="post big">
						<figcaption>
							<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
						</figcaption>
						<a href="<?php the_permalink();?>"><img src="<?php echo p75GetThumbnail(get_the_ID(), 345, 200, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /></a>
						<div class="post_info clearfix">
							<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d M, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
							<span><?php echo get_post_meta (get_the_ID(),'views',true); ?></span>
						</div>
						<hr>
					</figure>
					<!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
				</div>
		<?php
			endwhile;
			wp_reset_postdata();
			endif;

		$content = ob_get_contents();
		ob_end_clean();
		echo json_encode( array('pages' => $pagesToShow, 'content' => $content) );

	}elseif( $_POST['type'] == 'broker-news' ){
		$blockCategoryId = 11737;
		$r = new WP_Query( array(
			'post_status' => 'publish',
			'post_type' => 'post',
			'posts_per_page' => $_POST['postsToLoad'],
			'offset' => $_POST['offset'],
			'orderby' => 'date',
			'order'   => 'DESC',
			'cat' => $blockCategoryId,
		) );

		$found_posts = $r->found_posts - $_POST['offset'];
		$pagesToShow = $found_posts;
		$ost = $found_posts % $_POST['postsToLoad'];
		if( $ost !=0 ) $pagesToShow = $found_posts + $_POST['postsToLoad'] - $ost;
		$pagesToShow = $pagesToShow / $_POST['postsToLoad'];

		ob_start();

		if ($r->have_posts()) :
		while ( $r->have_posts() ) : $r->the_post(); ?>
		<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
			<!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
			<figure class="post">
				<a href="<?php the_permalink();?>"><img src="<?php echo p75GetThumbnail(get_the_ID(), 236, 200, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /></a>
				<div class="post_info clearfix">
					<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d M, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
					<span><?php echo get_post_meta (get_the_ID(),'views',true); ?></span>
				</div>
				<hr>
				<figcaption><a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a></figcaption>
			</figure>
			<!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
		</div>
	<?php
		endwhile;
		wp_reset_postdata();
		endif;

		$content = ob_get_contents();
		ob_end_clean();
		echo json_encode( array('pages' => $pagesToShow, 'content' => $content) );

	}elseif( $_POST['type'] == 'books' ){
		$blockCategoryId = 1822;
		$r = new WP_Query( array(
			'post_status'  => 'publish',
			'post_type' => 'post',
			'orderby' => 'date',
			'order'   => 'DESC',
			'posts_per_page' => $_POST['postsToLoad'],
			'offset' => $_POST['offset'],
			'cat' => $blockCategoryId,
		) );

		$found_posts = $r->found_posts - $_POST['offset'];
		$pagesToShow = $found_posts;
		$ost = $found_posts % $_POST['postsToLoad'];
		if( $ost !=0 ) $pagesToShow = $found_posts + $_POST['postsToLoad'] - $ost;
		$pagesToShow = $pagesToShow / $_POST['postsToLoad'];

		ob_start();

		if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post(); ?>
				<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
					<figure class="book_post">
						<div class="book_img">
							<a href="<?php the_permalink();?>"><img src="<?php echo p75GetThumbnail(get_the_ID(), 130, 180, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /></a>
						</div>
						<figcaption>
							<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
						</figcaption>
					</figure>
				</div>
	<?php
			endwhile;
			wp_reset_postdata();
		endif;

		$content = ob_get_contents();
		ob_end_clean();
		echo json_encode( array('pages' => $pagesToShow, 'content' => $content) );

	}elseif( $_POST['type'] == 'strategies' ){
		$blockCategoryId = 1933;
		$r = new WP_Query( array(
			'post_status'  => 'publish',
			'post_type' => 'post',
			'orderby' => 'date',
			'order'   => 'DESC',
			'posts_per_page' => $_POST['postsToLoad'],
			'offset' => $_POST['offset'],
			'cat' => $blockCategoryId,
		) );

		$found_posts = $r->found_posts - $_POST['offset'];
		$pagesToShow = $found_posts;
		$ost = $found_posts % $_POST['postsToLoad'];
		if( $ost !=0 ) $pagesToShow = $found_posts + $_POST['postsToLoad'] - $ost;
		$pagesToShow = $pagesToShow / $_POST['postsToLoad'];

		ob_start();

		if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post(); ?>
				<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
					<figure class="post1 clearfix">
						<a href="<?php the_permalink();?>"><img src="<?php echo p75GetThumbnail(get_the_ID(), 168, 154, ""); ?>"  alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /></a>
						<figcaption>
							<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
						</figcaption>
					</figure>
				</div>
	<?php
			endwhile;
			wp_reset_postdata();
		endif;

		$content = ob_get_contents();
		ob_end_clean();
		echo json_encode( array('pages' => $pagesToShow, 'content' => $content) );

	}
	die();
}

function ajaxPublishNewQuestion(){

	$formValues = array();
	parse_str($_POST['questionForm'], $formValues);

	$outArr = array(
		'error' => 'no'
	);

	$user_ID = get_current_user_id();

	if( !$user_ID ){
		if(
			!isset($formValues['question-author-name']) || !$formValues['question-author-name'] ||
			!isset($formValues['question-author-email']) || !$formValues['question-author-email'] ||
			!isset($formValues['question-title']) || !$formValues['question-title'] ||
			!isset($formValues['question-body']) || !$formValues['question-body'] ||
			!isset($formValues['g-recaptcha-response']) || !$formValues['g-recaptcha-response']
		){
			$outArr['error'] = __("Form not filled completely", 'ForTraderMaster');
			echo json_encode( $outArr );
			die();
		}

		require_once( get_template_directory().'/includes/recaptchalib.php' );
		$secret = "6LcP1w4TAAAAAB3vS7XAd5p_B-LqjMW4BhyPrayJ";
		$reCaptcha = new ReCaptcha($secret);
		$response = null;

		$response = $reCaptcha->verifyResponse(
			$_SERVER["REMOTE_ADDR"],
			$formValues['g-recaptcha-response']
		);
		if ($response != null && $response->success){
			$outArr['ReCaptchaSuccess'] = 1;
		}else{
			$outArr['error'] = __("CAPTCHA validation fails, pass validation. If you can't-refresh the page", 'ForTraderMaster');
			echo json_encode( $outArr );
			die();
		}
	}else{
		if(
			!isset($formValues['question-title']) || !$formValues['question-title'] ||
			!isset($formValues['question-body']) || !$formValues['question-body']
		){
			$outArr['error'] = __("Form not filled completely", 'ForTraderMaster');
			echo json_encode( $outArr );
			die();
		}
	}

	$newPost = array(
		'post_title' => trim(strip_tags($formValues['question-title'])),
		'post_content' => trim(strip_tags($formValues['question-body'])),
		'post_status' => 'pending',
		'post_author' => 1,
		'post_category' => array( get_option('questions_cat_id') )
	);
	$newPostId = wp_insert_post( $newPost );
	if( $newPostId > 0 ){
		$outArr['message'] = __("Question sent to moderation. Expect", 'ForTraderMaster');

		if( $user_ID ){
			update_post_meta( $newPostId, 'question-author-id', $user_ID );
		}else{
			update_post_meta( $newPostId, 'question-author-id', 0 );
			update_post_meta( $newPostId, 'question-author-name', trim(strip_tags($formValues['question-author-name'])) );
			update_post_meta( $newPostId, 'question-author-email', trim(strip_tags($formValues['question-author-email'])) );
		}


	}else{
		$outArr['error'] = __("Error during create question", 'ForTraderMaster');
	}
	echo json_encode( $outArr );

	die();
}

function ajaxLoginLogout(){
	if( !$_POST['action'] || !$_POST['actionType'] || !$_POST['form'] ) die();
	$actionType = $_POST['actionType'];
	$form = array();
	parse_str($_POST['form'], $form);

	switch ($actionType){
		case 'logIn':
			if( !$form['username'] || !$form['password'] ) die();
			$remember = false;
			if( isset( $form['remember'] ) ) $remember = true;

			$info = array();
			$info['user_login'] = $form['username'];
			$info['user_password'] = $form['password'];
			$info['remember'] = $remember;

			$user_signon = wp_signon( $info, true );
			if ( is_wp_error($user_signon) ){
				echo json_encode(array('success'=>false, 'message'=> __("Invalid username or password", 'ForTraderMaster') ) );
			} else {
				echo json_encode(array('success'=>true, 'refresh'=>true ));
			}

			break;
		case 'registration':
			if( !$form['email'] || !$form['username'] ) die();

			$user_registered = register_new_user( $form['username'], $form['email'] );

			if ( is_wp_error($user_registered) ){
				$messageText='';
				$i=0;
				foreach( $user_registered->errors as $errors ){
					foreach( $errors as $error ){
						if( $i>0 ){
							$messageText .= '<br />' . $error;
						}else{
							$messageText .= $error;
						}
						$i++;
					}
				}
				echo json_encode(array('success'=>false, 'message'=> $messageText ) );
			} else {
				echo json_encode(array('success'=>true, 'message'=>__("Registration is complete. Check your mail.", 'ForTraderMaster') ));
			}
			break;
		case 'lostpassword':
			if(!$form['username'] ) die();
			$_POST['user_login'] = $form['username'];

			$lostpassword= retrieveWpPassword();

			if ( is_wp_error($lostpassword) ){
				$messageText='';
				$i=0;
				foreach( $lostpassword->errors as $errors ){
					foreach( $errors as $error ){
						if( $i>0 ){
							$messageText .= '<br />' . $error;
						}else{
							$messageText .= $error;
						}
						$i++;
					}
				}
				echo json_encode(array('success'=>false, 'message'=> $messageText ) );
			} else {
				echo json_encode(array('success'=>true, 'message'=>__("You sent an email with a confirmation link.", 'ForTraderMaster') ));
			}
			break;
		case 'subscribe':
			if(!$form['email'] ) die();
			$email = $form['email'];

			require_once(get_template_directory().'/unisender/functions.php');

			$locale = get_locale();
			if( $locale == 'ru_RU' ){
				$group_id = get_option('unisender_group_id');
			}else{
				$group_id = get_option('unisender_group_id_' . $locale);
			}
			$result = addUserToUnisender($email, $group_id);

			if( $result ){
				$outArr = array(
					'success' => true,
					'message' => __("You are subscribed", 'ForTraderMaster') . '<br />',
				);
			}else{
				$outArr = array(
					'success' => false,
					'message' => __("An error occurred", 'ForTraderMaster') . '<br />',
				);
			}
			echo json_encode( $outArr );
			break;
	}

	die();
}

function ajaxPopupNewsPagination(){
	if( !$_POST['action'] || !$_POST['pageNum'] || !$_POST['pagesToShow'] || !$_POST['postsPerPage'] || !$_POST['currentPage'] || !$_POST['maxPage'] ) die();

	$currentPage = intval( $_POST['currentPage'] );
	$postsPerPage = intval( $_POST['postsPerPage'] );
	$pagesToShow = intval( $_POST['pagesToShow'] );
	$maxPage = intval( $_POST['maxPage'] );
	$pageNum = $_POST['pageNum'];

	if( !$currentPage || !$postsPerPage || !$pagesToShow || !$maxPage || ( !intval( $pageNum ) && $pageNum != 'first' && $pageNum != 'prev' && $pageNum != 'next' && $pageNum != 'last'  ) ) die();

	if( $pageNum == 'first' ) $pageNum = 1;
	if( $pageNum == 'prev' ) { if( $currentPage > 1 ) $pageNum = $currentPage-1; else $pageNum = $currentPage; }
	if( $pageNum == 'next' ) { if( $currentPage < $maxPage ) $pageNum = $currentPage+1; else $pageNum = $maxPage; }
	if( $pageNum == 'last' ) $pageNum = $maxPage;

	if( $pageNum == $currentPage ){
		echo json_encode( array( 'same_page' => 1 ) );
		die();
	}

	$current_date1 = new DateTime("now");
	$currentdate1=$current_date1->format('Y-m-d H:i:s');
	$r = new WP_Query( array(
		'post_status' => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => $postsPerPage,
		'offset' => ($pageNum-1) * $postsPerPage,
		'meta_query' => array(
		'relation' => 'AND',
			array(
				'key'     => 'important',
				'value'   => '1',
				'compare' => '=',
			),
			array(
				'key'     => 'daylife',
				'value'   => $currentdate1,
				'compare' => '>',
			),
		),
	));
	ob_start();
		if ($r->have_posts()) :
			$i=0;
			while ( $r->have_posts() ) : $r->the_post();
				if( $i % 2 == 0 ) echo '<div class="clear"></div>';
				get_template_part( 'templates/loop-post-popup-news' );
				$i++;
			endwhile;
		endif;
	$content = ob_get_clean();

	echo json_encode( array( 'same_page' => 0, 'content' => $content, 'pageNum' => $pageNum ) );

	die();
}

add_action( 'wp_ajax_nopriv_loadPostsForHome', 'ajaxLoadPostsForHome' );
add_action( 'wp_ajax_loadPostsForHome', 'ajaxLoadPostsForHome' );
add_action( 'wp_ajax_nopriv_loadAuthorsForHome', 'ajaxLoadAuthorsForHome' );
add_action( 'wp_ajax_loadAuthorsForHome', 'ajaxLoadAuthorsForHome' );

add_action( 'wp_ajax_nopriv_loadMoreIndicators', 'ajaxLoadMoreIndicators' );
add_action( 'wp_ajax_loadMoreIndicators', 'ajaxLoadMoreIndicators' );

add_action( 'wp_ajax_nopriv_loadMoreTagPosts', 'ajaxLoadMoreTagPosts' );
add_action( 'wp_ajax_loadMoreTagPosts', 'ajaxLoadMoreTagPosts' );

add_action( 'wp_ajax_nopriv_singleLoadMorePosts', 'ajaxSingleLoadMorePosts' );
add_action( 'wp_ajax_singleLoadMorePosts', 'ajaxSingleLoadMorePosts' );

add_action( 'wp_ajax_nopriv_categoryLoadMorePosts', 'ajaxCategoryLoadMorePosts' );
add_action( 'wp_ajax_categoryLoadMorePosts', 'ajaxCategoryLoadMorePosts' );

add_action( 'wp_ajax_nopriv_categoryLoadMoreQuestions', 'ajaxCategoryLoadMoreQuestions' );
add_action( 'wp_ajax_categoryLoadMoreQuestions', 'ajaxCategoryLoadMoreQuestions' );

add_action( 'wp_ajax_nopriv_loadMoreToday42', 'ajaxLoadMoreToday42' );
add_action( 'wp_ajax_loadMoreToday42', 'ajaxLoadMoreToday42' );

add_action( 'wp_ajax_nopriv_loadMoreQuotesTagsPosts42', 'ajaxLadMoreQuotesTagsPosts42' );
add_action( 'wp_ajax_loadMoreQuotesTagsPosts42', 'ajaxLoadMoreQuotesTagsPosts42' );

add_action( 'wp_ajax_nopriv_loadMoreRecommendateSingle', 'ajaxLoadMoreRecommendateSingle' );
add_action( 'wp_ajax_loadMoreRecommendateSingle', 'ajaxLoadMoreRecommendateSingle' );

add_action( 'wp_ajax_nopriv_loadMoreFocusSingle', 'ajaxLoadMoreFocusSingle' );
add_action( 'wp_ajax_loadMoreFocusSingle', 'ajaxLoadMoreFocusSingle' );

add_action( 'wp_ajax_nopriv_loadMoreAuthorPosts', 'ajaxLoadMoreAuthorPosts' );
add_action( 'wp_ajax_loadMoreAuthorPosts', 'ajaxLoadMoreAuthorPosts' );

add_action( 'wp_ajax_nopriv_updateSidebarRates', 'ajaxUpdateSidebarRates' );
add_action( 'wp_ajax_updateSidebarRates', 'ajaxUpdateSidebarRates' );

add_action( 'wp_ajax_nopriv_ajaxLoginLogout', 'ajaxLoginLogout' );
add_action( 'wp_ajax_ajaxLoginLogout', 'ajaxLoginLogout' );

add_action( 'wp_ajax_nopriv_ajaxPopupNewsPagination', 'ajaxPopupNewsPagination' );
add_action( 'wp_ajax_ajaxPopupNewsPagination', 'ajaxPopupNewsPagination' );

add_action( 'wp_ajax_nopriv_loadMorePostsForSlider', 'ajaxLoadMorePostsForSlider' );
add_action( 'wp_ajax_loadMorePostsForSlider', 'ajaxLoadMorePostsForSlider' );

add_action( 'wp_ajax_nopriv_publishNewQuestion', 'ajaxPublishNewQuestion' );
add_action( 'wp_ajax_publishNewQuestion', 'ajaxPublishNewQuestion' );

add_action( 'wp_ajax_nopriv_loadPostsBrokerNews', 'ajaxLoadPostsBrokerNews' );
add_action( 'wp_ajax_loadPostsBrokerNews', 'ajaxLoadPostsBrokerNews' );

add_action( 'wp_ajax_nopriv_loadPostsCalendarNews', 'ajaxLoadPostsCalendarNews' );
add_action( 'wp_ajax_loadPostsCalendarNews', 'ajaxLoadPostsCalendarNews' );

add_action( 'wp_ajax_nopriv_loadPostsCalendarEventNews', 'ajaxLoadPostsCalendarEventNews' );
add_action( 'wp_ajax_loadPostsCalendarEventNews', 'ajaxLoadPostsCalendarEventNews' );
