<?php get_header();?>
<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part">
		<?php get_template_part_by_locale( 'templates/banner-728x90' );?>
		<?php $excludePostArr = array();?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="clearfix">
				<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
				<div class="breadcrumbs alignleft">
					<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
						<li>
							<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a href="<?php echo pll_home_url();?>" itemprop="item"><span itemprop="name"><?php _e("Home", 'ForTraderMaster'); ?></span></a>
								<meta itemprop="position" content="1" />
							</span>
						</li>
                    <?php
                        $category = get_the_category();
						$excludePostArr[] = get_the_ID();
						$catmeta = get_post_meta(get_the_ID(), '_category_permalink', true);
						if( is_array($catmeta) ) $catmeta = $catmeta['category'];
						$articleSection = '';
						if( isset ( $catmeta ) && $catmeta ){
							$category_meta = get_category($catmeta);
							echo get_formated_category_parents( $catmeta, true);
							$articleSection = $category_meta->cat_name;
						}else{
							$i=2;
							foreach( $category as $breadcrumb_cat ){
								echo '<li><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a title="'.$breadcrumb_cat->cat_name.'" href="'.get_category_link( $breadcrumb_cat->cat_ID ).'" itemprop="item"><span class="breadcrumb-active" itemprop="name">'.$breadcrumb_cat->cat_name.'</span></a><meta itemprop="position" content="'.$i.'" /></span></li>';
								$tmpCat = $breadcrumb_cat->cat_ID;
								
								if( $i == 2 )
									$articleSection .= $breadcrumb_cat->cat_name;
								else
									$articleSection .= ', '.$breadcrumb_cat->cat_name;
								
								$i++;
							}
						}
						if( $catmeta ){
							$postCat = $catmeta;
						}else{
							$postCat = $tmpCat;
						}
					?>
					</ul>
				</div>
				<!-- - - - - - - - - - - - - - End of Breadcrumbs - - - - - - - - - - - - - - - - -->
				<!-- - - - - - - - - - - - - - Article Info - - - - - - - - - - - - - - - - -->
				<div class="article_info clearfix alignright">
					<?php
						$commentNumber = get_comments_number();
					?>
					<?php if( $commentNumber ){?> <a href="#comments"><span class="comment"><?php echo $commentNumber; ?></span></a><?php } ?>
					<span class="views"><?php echo get_post_meta (get_the_ID(),'views',true) ? get_post_meta (get_the_ID(),'views',true) : 0; ?></span>
					<time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d F, Y',  get_the_date('Y-m-d') ); ?></time>
				</div>
				<!-- - - - - - - - - - - - - - End of Article Info - - - - - - - - - - - - - - - - -->
			</div><!-- / .cleafix -->
			<hr>
			<h1 class="title2"><?php the_title();?><?php edit_post_link(' <i></i>'); ?></h2>
			<?php get_template_part( 'templates/sm-top-post-buttons' ); ?>
			<!-- - - - - - - - - - - - - - Article Post - - - - - - - - - - - - - - - - -->
			<?php
				$yoastWpseoMetadesc = get_post_meta (get_the_ID(),'_yoast_wpseo_metadesc',true);
				if( $yoastWpseoMetadesc ){
					echo '<blockquote>'.$yoastWpseoMetadesc.'</blockquote>';
				}
			?>
			<article class="clearfix main-post-content">

				<?php the_content(); ?>
				
				<?php
					$questionAuthorId = get_post_meta (get_the_ID(),'question-author-id',true);
				?>

				
				<div class="author_article clearfix">
					<?php 		
						$author = get_userdata( $questionAuthorId ); 
						
						if( $questionAuthorId ){
				
							$authorName = getShowUserName( $author );
							
							$authorInfo = '
							<a href="'.get_author_posts_url($author->ID).'" class="author_avatar alignleft">
								'.renderImg( p75GetServicesAuthorThumbnail( 107, 107, $author->ID ), 107, 107, $authorName, 0 ).'
							</a>
							<div class="author_text">
								<h5 class="author_name"><a href="'.get_author_posts_url($author->ID).'">'. $authorName.'</a></h5>
								<p>'. get_user_meta( $author->ID, 'description', true ).'</p>
							</div>';
						}else{
							$authorName = get_post_meta (get_the_ID(),'question-author-name',true);
							$userEmail = '';
							if( current_user_can( 'administrator' ) ) $userEmail = get_post_meta (get_the_ID(),'question-author-email',true);
							$authorInfo = '
							<span class="author_avatar alignleft">
								'.renderImg( p75GetServicesAuthorThumbnail( 107, 107, $author->ID ), 107, 107, $authorName, 0 ).'
							</span>
							<div class="author_text">
								<h5 class="author_name">'. $authorName.'</h5>
								<p>'.$userEmail.'</p>
							</div>';
						}
						echo $authorInfo;
					?> 
					
				</div><!--/ .author_article -->


			</article><!--/ .section_offset -->
			<!-- - - - - - - - - - - - - - End of Article Post - - - - - - - - - - - - - - - - -->
			
			<?php get_template_part( 'templates/sm-bottom-post-buttons' ); ?>
			<?php comments_template( 'templates/comment' ); ?>

			<div class="blockdiv3 clearfix">
				<div><?php dynamic_sidebar('300x250-2-post-banner');?></div>
				<div><?php dynamic_sidebar('300x250-3-post-banner');?></div>
			</div>
			
		<?php endwhile; else : ?>
			<h2 class="center"><?php _e("Not found", 'ForTraderMaster'); ?></h2>
			<p class="center"><?php _e("This page does not exist", 'ForTraderMaster'); ?></p>
		<?php endif; ?>


			
			
			
		<?php 
			$cacheDuration = get_option('theme_cache_duration'); 
			global $fortraderCache;
			
			$cache_key = 'theme_' . md5('under-post-today-block');
			if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
		?>
			<?php /*Сегодня*/?>	
			<?php get_template_part( 'templates/single-today-block' ); ?>
		<?php $fortraderCache->endCache(); } ?>

		<?php
			$cache_key = 'theme_' . md5('under-post-actual-block');
			if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
		?>
			<?php /*Актуально*/?>
			<?php get_template_part( 'templates/single-focus' ); ?>
		<?php $fortraderCache->endCache(); }?>
			
		<?php
			$cache_key = 'theme_' . md5('under-post-editor-recommends-block');
			if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
		?>
			<?php /*Редактор рекомендует*/?>	
			<?php get_template_part( 'templates/single-editor-recommends' ); ?>
		<?php $fortraderCache->endCache(); } ?>
		


	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php get_sidebar();?>
<?php get_footer();?>