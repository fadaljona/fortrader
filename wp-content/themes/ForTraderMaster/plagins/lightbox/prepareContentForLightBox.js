!function( $ ) {	
	var $contentWraper = $('.main-post-content');
	var $imgs = $contentWraper.find('img');
	$imgs.each(function() {
		var $this = $( this );
		var src = $this.attr( 'data-origin-src' );
		if( src ){
			var title = $this.attr('alt');
			$this.replaceWith( '<a href="' + src + '" data-lightbox="roadtrip" data-title="'+title+'">' + $this.prop('outerHTML') + '</a>' );
		}
	});
}( window.jQuery );