<?php get_header();?>
<?php
global $fortraderCache;
$cacheDuration = get_option('theme_cache_duration');
$cache_key = 'theme_' . md5($_SERVER['REQUEST_URI']);
if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
?>
<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part">
		<?php get_template_part_by_locale( 'templates/banner-728x90' );?>
			<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
			<div class="breadcrumbs">
				<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
					<li>
						<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a href="<?php echo pll_home_url();?>" itemprop="item"><span itemprop="name"><?php _e("Home", 'ForTraderMaster'); ?></span></a>
							<meta itemprop="position" content="1" />
						</span>
					</li>
					<?php 
						$cur_cat_id = get_cat_id( single_cat_title("",false) );
						global $category_breadcrumb_marker;
						$category_breadcrumb_marker=0;
						echo get_formated_category_parents( $cur_cat_id, true, true);
					?>
				</ul>
			</div>
			
			<!-- - - - - - - - - - - - - - End of Breadcrumbs - - - - - - - - - - - - - - - - -->
			<hr>
			<section class="section_offset"> 
				<h2 class="page_title1"><?php  single_cat_title(); ?><i class="icon student_icon"></i></h2>
				<hr class="separator1">
				
				<a href="/ask-question.html" class="button1 alignright"><?php _e("Ask A Question", 'ForTraderMaster'); ?></a>
				<div class="clear"></div>
				
			</section>
			<?php
				$per_page = 50;
				$args = array_merge(
					$wp_query->query_vars, 
					array(
						'posts_per_page' => $per_page,
					)
				);
						query_posts( $args );
			?>
			<section class="section_offset">
				<?php
					if (have_posts()) : 
						echo '<div class="category-posts-wrapper"><div class="post_box">';
						while (have_posts()) : the_post();
							get_template_part( 'templates/loop-post-question' );
						endwhile;
					echo '</div></div>';
					global $wp_query;
					$max_page = $wp_query->max_num_pages;
					if( $max_page > 1 ){
						echo '<a href="#" class="load_btn category-load-more-questions chench_btn" data-per-page="'. $per_page .'" data-page="1" data-text="'. __("Load more questions", 'ForTraderMaster').'" data-shot-text="'. __("Show more", 'ForTraderMaster').'"></a>';
					}
					//get_template_part('templates/pagination');
					endif;
				?>
			</section>

			<?php
				$currentCat = get_category( $cur_cat_id );
				if($currentCat->category_description){
			?>
				<article class="section_offset">
					<h2 class="page_title1"><?php _e("About Section", 'ForTraderMaster'); ?> <a href="#" class="link_accent">«<?php echo $currentCat->cat_name; ?>»</a></h2>
					<?php echo apply_filters('the_content', $currentCat->category_description); ?>
				</article>
			<?php } ?>

	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php $fortraderCache->endCache(); } ?>
	<?php get_sidebar();?>
<?php get_footer();?>