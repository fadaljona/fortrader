;(function($){

	"use strict";

	var BSCore = {
		DOMReady: function(){
			var self = this;
			self.buttonSlider();
		},

		/**
		**	Nav Buttons 
		**/

		sliderLoad: false,
		spinnerWrap: '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>',
		buttonSlider : function(){
			var self = this;
			$('.nav_buttons>a').on('click',function(event){

				var parent = $(this).parents('.turn_box'),
					turn = $(this).parents('.turn_box').find('.turn_content'),
					owl = parent.find(".owl-carousel");

				if($(this).hasClass('prev_btn')){

					owl.trigger('owl.prev');
					event.preventDefault();

				}
				else if($(this).hasClass('next_btn')){
	
					if( self.sliderLoad ) return false;
					
					var carouselWrapper = owl.closest('.carousel-wrapper');
					var owlWrapper = owl.closest('.owl-carousel');
					var currentItem = parseInt(owlWrapper.attr('data-currentItem'));
					var maximumItem = parseInt(owlWrapper.attr('data-maximumItem'));
					var postsToLoad = 3;

					if( parseInt(owlWrapper.attr('data-pages-to-load')) == 1 ){
						owl.trigger('owl.next');
					}else if( currentItem == 0 || maximumItem == 0 ){
						self.sliderLoad = true;

						carouselWrapper.append( self.spinnerWrap );
						$.ajax({
							type: 'POST',
							url: '/wp-admin/admin-ajax.php',
							dataType: 'json',
							data: {
								action: 'loadMorePostsForSlider',
								postsToLoad: postsToLoad,
								type: owlWrapper.attr('data-type'),
								offset: owlWrapper.attr('data-offset'),
							},
							success: function(data){
								owl.data('owlCarousel').addItem( data.content );
								owl.trigger('owl.next');
								owlWrapper.attr({
									'data-offset': parseInt(owlWrapper.attr('data-offset')) + postsToLoad,
									'data-pages-to-load': parseInt( data.pages )
								});
								self.sliderLoad = false;
								carouselWrapper.find( '.spinner-wrap' ).remove();
							},
							error: function(MLHttpRequest, textStatus, errorThrown){
								alert('error loading');
								self.sliderLoad = false;
							}
						});

					}else if( currentItem == maximumItem || currentItem < 0 || currentItem > maximumItem ){
						self.sliderLoad = true;
						if( currentItem < 0 )currentItem = maximumItem;
						
						carouselWrapper.append( self.spinnerWrap );
						$.ajax({
							type: 'POST',
							url: '/wp-admin/admin-ajax.php',
							dataType: 'json',
							data: {
								action: 'loadMorePostsForSlider',
								postsToLoad: postsToLoad,
								type: owlWrapper.attr('data-type'),
								offset: owlWrapper.attr('data-offset'),
							},
							success: function(data){
								owl.data('owlCarousel').addItem(data.content);
								owlWrapper.attr({
									'data-currentItem':currentItem+1,
									'data-maximumItem':maximumItem + postsToLoad
								});
								owl.data('owlCarousel').jumpTo(currentItem+1);
								owlWrapper.attr({
									'data-offset': parseInt(owlWrapper.attr('data-offset')) + postsToLoad,
									'data-pages-to-load': parseInt( data.pages )
								});
								self.sliderLoad = false;
								carouselWrapper.find( '.spinner-wrap' ).remove();
							},
							error: function(MLHttpRequest, textStatus, errorThrown){
								alert('error loading');
								self.sliderLoad = false;
							}
						});

					}else{
						owl.trigger('owl.next');
					}

					event.preventDefault();

				}
				else if($(this).hasClass('turn_btn')){

					turn.slideToggle(500);
					$(this).toggleClass("active");
					event.preventDefault();
				}

			});
			
		},

	}

	$(function(){
		BSCore.DOMReady();
	});

})(jQuery);