;(function($){

	"use strict";

	var Core = {

		DOMReady: function(){

			var self = this;
			
			self.loading = false;

			self.jqueryExtend();
			self.checkSubmenu();
			self.headerInfo();
			self.footerList();
			self.additionalMenuOpen();
			self.additionalMenuClose();
			self.tabsBox();
			
			
			self.dropdoun();
			//self.optionsBox();
			
			self.tablQuotesSlider.init();
			
			self.moreTools();
			self.getCode();
			
			self.headerList();
			self.siteList();
			self.goUpAnimate();
			self.goUp();
			self.nextButtonHover();
			self.popupLoginLogout();
			self.footerSubscribe();
			
			
			$('.load-more-btn').click(function(e){
				e.preventDefault();
				self.loadMoreFromCat( $(this).attr('data-cat-id'), $(this).attr('data-page') );
			});
			$('.load-more-broker-news').click(function(e){
				e.preventDefault();
				self.loadMoreBrokerNews( $(this).attr('data-cat-id'), $(this).attr('data-page') );
			});
			$('.load-more-calendar-news').click(function(e){
				e.preventDefault();
				self.loadMoreCalendarNews( $(this) );
			});
			$('.load-more-calendar-event-news').click(function(e){
				e.preventDefault();
				self.loadMoreCalendarEventNews( $(this) );
			});
			$('.load-more-authors').click(function(e){
				e.preventDefault();
				self.loadMoreAuthors( $(this).attr('data-page') );
			});
			$('.load-more-indicators').click(function(e){
				e.preventDefault();
				self.loadMoreIndicators( $(this).attr('data-cat-id'), $(this).attr('data-offset') );
			});
			/*$('.tag-load-link').click(function(e){
				e.preventDefault();
				if( $(this).hasClass('categories_btn') ){
					$('.tag-load-link.categories_btn').removeClass('tag-active');
					$(this).addClass('tag-active');
				}
				self.loadMoreTagPosts( $(this).attr('data-tag-id'), $(this).attr('data-tag-page'), $(this) );
			});*/
			$('.single-load-more-posts').click(function(e){
				e.preventDefault();
				self.singleLoadMorePosts( $(this).attr('data-cat-id'), $(this).attr('data-page'), $(this).attr('data-exclude') );
			});
			$('.category-load-more-posts').click(function(e){
				e.preventDefault();
				self.categoryLoadMorePosts( $(this).attr('data-cat-id'), $(this).attr('data-page'), $(this).attr('data-sort') );
			});
			$('.category-load-more-questions').click(function(e){
				e.preventDefault();
				self.categoryLoadMoreQuestions( $(this).attr('data-per-page'), $(this).attr('data-page') );
			});
			$('.load-more-today-42').click(function(e){
				e.preventDefault();
				self.loadMoreToday42( $(this).attr('data-page'), $(this).attr('data-exclude') );
			});
			$('.load-more-quotes-tags-posts-42').click(function(e){
				e.preventDefault();
				self.loadMoreQuotesTagsPosts42( $(this).attr('data-page'), $(this).attr('data-term') );
			});
			$('.load-more-recommendate-single').click(function(e){
				e.preventDefault();
				self.loadMoreRecommendateSingle( $(this).attr('data-page') );
			});
			$('.load-more-focus-single').click(function(e){
				e.preventDefault();
				self.loadMoreFocusSingle( $(this).attr('data-page') );
			});
			$('.load-more-author-posts').click(function(e){
				e.preventDefault();
				self.loadMoreAuthorPosts( $(this).attr('data-page'), $(this).attr('data-type'), $(this).attr('data-author') );
			});
			/*$('.currency-rates .rates_icon').click(function(e){
				e.preventDefault();
				self.updateSidebarRates( );
			});*/
			$('.ask_question textarea').focusin(function() {
				$('.ask_question .ask-question-button').show();
			});
			$('form.ask_question').click(function(e) {
				if( !$(e.target).hasClass( 'ask-question-button' ) ) e.stopPropagation();
			});
			$(document).click(function() {
				$('.ask_question .ask-question-button').hide();
			});
			$('.ask-question-button').click(function(e){
				$(this).hide();
			});
			
			self.optionsButtonClick();
			self.closeOptions();
			
			self.showPopupNewsAsLastPopup();
			
			self.popupNewsPagination();
			
			self.openNewDropDownList();
			
			/**  add 00.02.16   **/
			self.dropdounRequirements();

			self.languageDropdown();
		},
		openNewDropDownList:function() {
			var self = this;
			$('body').on('click','.newDropDownList .open_select',function(e){
				e.preventDefault();
				var currentList = $(this).closest('.newDropDownList').find('ul.dropDown');
				
				if( currentList.hasClass('opened') ){
					$('.newDropDownList ul.dropDown').removeClass('opened');
				}else{
					$('.newDropDownList ul.dropDown').removeClass('opened');
					currentList.addClass('opened');
				}

				e.stopPropagation();
			});
		},
		showPopupNewsAsLastPopup:function() {
			var self = this;

			if( self.getCookie('popup_services_displayed') != null ){
				var popupServicesArr = self.getCookie('popup_services_displayed').split('|');
				var d = new Date();
				
				var timeToshow = Math.floor(parseFloat( popupServicesArr[0] ) / 3);
				if( self.getCookie('popupNewsShown') == null && ( parseInt( d.getTime() ) - parseInt( popupServicesArr[1] ) ) > timeToshow * 60 * 1000 ){
					self.showPopupNews( popupServicesArr[0] - timeToshow );
				}
			}
		},
		showPopupNews:function( cookieTime ) {
			var wWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
			var self = this;
			if( wWidth > 700 ){
				$( "body" ).mouseleave(function( event ) {	
					if( self.getCookie('popupNewsShown') != null ){
						self.createCookie('popupNewsShown', 'true', cookieTime);
						return false;
					} 
					if( event.pageY <= $(window).scrollTop() ){
						if($('#more_news').length){
							$('#more_news').arcticmodal({
								overlay: {
									css: {
										opacity: .2
									}
								},
								openEffect:{
									speed: 100
								},
								beforeOpen: function(data, el) {
									var height = parseInt( $(window).height() ) * 2;
									
									$('.arcticmodal-container').css({
										'position':'fixed',
										'-webkit-transform': 'translate(0, -'+ height +'px)',
										'-ms-transform': 'translate(0, -'+ height +'px)',
										'-o-transform': 'translate(0, -'+ height +'px)',
										'transform': 'translate(0, -'+ height +'px)',
										'-webkit-transition': 'all 1s',
										'-o-transition': 'all 1s',
										'transition': 'all 1s',
									})

								},
								afterOpen: function(data, el) {
									$('.arcticmodal-container').css({
										'-webkit-transform': 'translate(0)',
										'-ms-transform': 'translate(0)',
										'-o-transform': 'translate(0)',
										'transform': 'translate(0)',
									});
								},
							});
						}
						self.createCookie('popupNewsShown', 'true', cookieTime);
					}
				});
			}
		},
		createCookie:function(name, value, mins) {
			var expires;
			if (mins) {
				var date = new Date();
				date.setTime(date.getTime() + (mins * 60 * 1000));
				expires = "; expires=" + date.toGMTString();
			}else {
				expires = "";
			}
			document.cookie = name + "=" + value + expires + "; path=/";
		},

		getCookie:function(cookieName) {
			if (document.cookie.length > 0) {
				var cookieStart = document.cookie.indexOf(cookieName + "=");
				if (cookieStart != -1) {
					cookieStart = cookieStart + cookieName.length + 1;
					var cookieEnd = document.cookie.indexOf(";", cookieStart);
					if (cookieEnd == -1) {
						cookieEnd = document.cookie.length;
					}
					return unescape(document.cookie.substring(cookieStart, cookieEnd));
				}
			}
			return null;
		},
		
		popupNewsPagination:function() {
			var self = this;
			var spinner = $('#more_news .spinner-wrap');
			var contentWrapper = $('#more_news .modal_news_container');
			$('body').on('click','.navigation_box_modal_news a',function(e){
				e.preventDefault();
				if( self.loading ) return false;
				var $this = $(this);
				var wrapper = $this.closest('.navigation_box_modal_news');
				var dataToSend = {
					pageNum: $this.attr('data-act'),
					pagesToShow: wrapper.attr('data-pages-to-show'),
					postsPerPage: wrapper.attr('data-posts-per-page'),
					maxPage: wrapper.attr('data-max-page'),
					currentPage: wrapper.find('li.current a').attr('data-act'),
					action: 'ajaxPopupNewsPagination',
				};
				if( dataToSend.currentPage == dataToSend.pageNum ){
					self.loading = false;
					return false;
				}
				self.loading = true;
				spinner.removeClass('hide');
				$.ajax({
					dataType: 'json',
					type: 'POST',
					url: '/wp-admin/admin-ajax.php',
					data: dataToSend,
					'success':function(json){
						if( json != null && json.same_page == 0 ){
							contentWrapper.html( json.content );
							$("#more_news img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
							self.gereratePaginationForPopupNews(json.pageNum, dataToSend.maxPage, dataToSend.pagesToShow);				
						}
						spinner.addClass('hide');
						self.loading = false;
					}
				});
			});
		},
		gereratePaginationForPopupNews:function(pageNum, maxPage, pagesToShow) {
			pageNum = parseInt(pageNum);maxPage = parseInt(maxPage);pagesToShow = parseInt(pagesToShow);
			
			var navBlock = $('#more_news .navigation_box_modal_news');
			var prev = navBlock.find('.nav_list_my_prev');
			var next = navBlock.find('.nav_list_my_next');
			var first = navBlock.find('.first');
			var last = navBlock.find('.last');
			var liWrap = navBlock.find('.nav_list_my_style');
			prev.addClass('hide');next.addClass('hide');first.addClass('hide');last.addClass('hide');
			
			if( pageNum > 1 ) prev.removeClass('hide');
			if( pageNum != maxPage ) next.removeClass('hide');
			if( pageNum > 3 && maxPage > 5 ) first.removeClass('hide');
			if( pageNum <= maxPage - 3 && maxPage > 5 ) last.removeClass('hide');
			
			var pagesStr = '';
			var rounder = pagesToShow / 2 | 0;
			var start = pageNum - rounder;
			if( start < 1 ) {
				start = 1;	
				var end = pageNum + rounder + rounder-1;
			}else{
				var end = pageNum + rounder;
			}
			
			if( end > maxPage ){
				start = start - end + maxPage;
				if( start < 1 ) start = 1;	
				end = maxPage;
			}
			
			var i = start;
			for (i ; i <= end; i++) {
				var currentClass = '';
				if( pageNum == i ) currentClass = 'current';
				pagesStr = pagesStr + "<li class='"+currentClass+"'><a data-act='"+i+"' href='#'>"+i+"</a></li>";
			}
			liWrap.html( pagesStr );
		},
		
		optionsButtonClick:function() {
			$('.turn_box .options_btn:not(.customBtnHandler)').click(function(e){
				e.preventDefault();
				var $this = $(this),
					parent = $this.closest(".turn_box");
					
				var marker = 1;
				if( $this.hasClass("active") ) marker = 0;
				if( marker ){
					$('.options_box').removeClass('opened');
					$('.options_btn').removeClass('active');	
				}
				$this.toggleClass("active");
				parent.find('.options_box').toggleClass("opened");	
			});
			
			$('.turn_box .turn_btn').click(function(e){
				e.preventDefault();
				var $this = $(this),
					turn = $(this).parents('.turn_box').find('.turn_content');
					
				turn.slideToggle(500);
				$this.toggleClass("active");
				e.preventDefault();
			});
		},
		closeOptions:function() {
			$(document).click(function(event){
				if(!$(event.target).closest('.options_box:not(.customBtnHandler),.options_btn:not(.customBtnHandler)').length){
					$('.options_box:not(.customBtnHandler)').removeClass('opened');
					$('.options_btn:not(.customBtnHandler)').removeClass('active');
				}
			});
		},
		
		footerSubscribe: function(){
			var self = this;
			$('#subscribeFormFooter button').on('click',function(event){
				event.preventDefault();
				var userEmailValid = self.checkEmail( '#subscribeFormFooter', 'user-email');
				if (  userEmailValid ){
					self.popupLoginLogoutAjax( '#subscribeFormFooter', $('#subscribeFormFooter').serialize(), 'subscribe' );
				}
			});
			$('#footerPopupSubscribeForm button').on('click',function(event){
				event.preventDefault();
				var userEmailValid = self.checkEmail( '#footerPopupSubscribeForm', 'user-email');
				if (  userEmailValid ){
					self.popupLoginLogoutAjax( '#footerPopupSubscribeForm', $('#footerPopupSubscribeForm').serialize(), 'subscribe' );
				}
			});
		},
		
		popupLoginLogout: function(){
			var self = this;
			$('#logInForm .modal_button').on('click',function(event){
				event.preventDefault();
				var userNameValid = self.checkInputs( '#logInForm', 'user-name', 1, 'Имя пользователя не может быть пустым' );
				var userPasswordValid = self.checkInputs( '#logInForm', 'user-password', 5, 'Пароль слишком короткий - минимум 5 символов' );
				
				if (  userNameValid && userPasswordValid ){
					self.popupLoginLogoutAjax( '#logInForm', $('#logInForm').serialize(), 'logIn' );
				}
			});
			$('#registrationForm  .modal_button').on('click',function(event){
				event.preventDefault();
				var userNameValid = self.checkInputs( '#registrationForm', 'user-name', 1, 'Имя пользователя не может быть пустым' );
				var userEmailValid = self.checkEmail( '#registrationForm', 'user-email');
				
				if( $('#registrationForm #g-recaptcha-response').val() == '' ){
					var captchaValid = false;
					$('#registrationForm .valid-g-recaptcha').html( jsThemeTexts.captchaVlidationFail );
				}else{
					var captchaValid = true;
					$('#registrationForm .valid-g-recaptcha').html('');
				}
				
				if (  userNameValid && userEmailValid && captchaValid ){
					self.popupLoginLogoutAjax( '#registrationForm', $('#registrationForm').serialize(), 'registration' );
				}
			});
			$('#lostpasswordForm .modal_button').on('click',function(event){
				event.preventDefault();
				var userNameValid = self.checkInputs( '#lostpasswordForm', 'user-name', 1, 'Имя пользователя не может быть пустым' );
				
				if (  userNameValid ){
					self.popupLoginLogoutAjax( '#lostpasswordForm', $('#lostpasswordForm').serialize(), 'lostpassword' );
				}
			});
			$('#subscribeForm button').on('click',function(event){
				event.preventDefault();
				var userEmailValid = self.checkEmail( '#subscribeForm', 'user-email');
				if (  userEmailValid ){
					self.popupLoginLogoutAjax( '#subscribeForm', $('#subscribeForm').serialize(), 'subscribe' );
				}
			});
		},
		popupLoginLogoutAjax: function( formId, serializedForm, actionType ){
			var self = this;
			if( self.loading ) return false;
			$(formId + ' input').prop('disabled', true);

			self.loading = true;
			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'ajaxLoginLogout',
					form: serializedForm,
					actionType: actionType,
				},
				'success':function(json){
					if( json != null ){		
						if( json.success == false ){
							$(formId + ' .valid-all-form').html(json.message);
							$(formId + ' input').prop('disabled', false);
						}
						if( json.success == true ){
							if( json.refresh == true ){	
								location.reload();
								return false;
							}
							$(formId + ' .valid-all-form').html(json.message);
							$(formId + ' button').prop('disabled', true);
						}
					}
					self.loading = false
				}
			});
		},
		checkInputs: function( formId, selector, valLength, errorMessage ){
			var inputEl = $( formId + ' .' + selector );
			var errorEl = $( formId + ' .valid-' + selector );
			if( inputEl.val() != '' && inputEl.val().length >= valLength ) {
						inputEl.removeClass('invalid');
						errorEl.html('');
						return true;
			} else {
				inputEl.addClass('invalid');errorEl.html( errorMessage );
				return false;
			}
		},
		checkEmail: function( formId, selector ){
			var inputEl = $( formId + ' .' + selector );
			var errorEl = $( formId + ' .valid-' + selector );
			if(inputEl.val() != '') {
					var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
					if(pattern.test(inputEl.val())){
						inputEl.removeClass('invalid');
						errorEl.html('');
						return true;
					} else {
						inputEl.addClass('invalid');
						errorEl.html('Не верный email<br />');
						return false;
					}
				} else {
					inputEl.addClass('invalid');
					errorEl.html('Email не может быть пустым<br />');
					return false;
				}
		},
		
		updateSidebarRates: function( pageNum, pageType, author ){
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'updateSidebarRates',
				},
				success: function(data){
					$('.currency-rates').html(data);
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});
		},
		
		loadMoreAuthorPosts: function( pageNum, pageType, author ){

			var currentButton = $('.load-more-author-posts[data-type='+pageType+']');
			var currentPostBox = currentButton.parent('div');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadMoreAuthorPosts',
					page: parseInt(pageNum)+1,
					type:pageType,
					author:author,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentButton.before('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		loadMoreRecommendateSingle: function( pageNum ){

			var currentButton = $('.load-more-recommendate-single');
			var currentPostBox = $('.load-more-recommendate-single-wrap');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadMoreRecommendateSingle',
					page: parseInt(pageNum)+1,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		loadMoreFocusSingle: function( pageNum ){

			var currentButton = $('.load-more-focus-single');
			var currentPostBox = $('.load-more-focus-single-wrap');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadMoreFocusSingle',
					page: parseInt(pageNum)+1,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		loadMoreToday42: function( pageNum ){

			var currentButton = $('.load-more-today-42');
			var currentPostBox = $('.load-more-today-42-wrap');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadMoreToday42',
					page: parseInt(pageNum)+1,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		loadMoreQuotesTagsPosts42: function( pageNum, term ){

			var currentButton = $('.load-more-quotes-tags-posts-42');
			var currentPostBox = $('.load-more-quotes-tags-posts-42-wrap');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadMoreQuotesTagsPosts42',
					page: parseInt(pageNum)+1,
					term: term,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		categoryLoadMoreQuestions: function( perPage, pageNum ){

			var currentButton = $('.category-load-more-questions');
			var currentPostBox = $('.category-posts-wrapper');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'categoryLoadMoreQuestions',
					page: parseInt(pageNum)+1,
					perPage:perPage,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		categoryLoadMorePosts: function( catId, pageNum, sort ){

			var currentButton = $('.category-load-more-posts');
			var currentPostBox = $('.category-posts-wrapper');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'categoryLoadMorePosts',
					page: parseInt(pageNum)+1,
					cat:catId,
					sort:sort,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		singleLoadMorePosts: function( catId, pageNum, exclude ){

			var currentButton = $('.single-load-more-posts[data-cat-id='+catId+']');
			var currentPostBox = $('.single-load-more-wrapper[data-cat-id='+catId+']');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'singleLoadMorePosts',
					page: parseInt(pageNum)+1,
					cat:catId,
					exclude:exclude,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		loadMoreTagPosts: function( tagId, tagPage, clickedTag ){

			var currentButton = clickedTag.closest('section').find('.load_btn.tag-load-link.chench_btn');
			var currentPostBox = clickedTag.closest('section').find('.interesting-tags-wrapper');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadMoreTagPosts',
					tagPage: tagPage,
					tagId:tagId,
					
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.css({'display':'block'});
					if( parseInt(tagPage) ==1 ){
						currentPostBox.html(data);
					}else{
						currentPostBox.append('<div class="clear"></div>'+data);
					}
					currentButton.attr({
						'data-tag-id':tagId,
						'data-tag-page':parseInt(tagPage)+1,
					});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		loadMoreIndicators: function( catId, offset ){


			var currentButton = $('.load-more-indicators');
			var currentPostBox = $('.load-indicators-wrapper');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadMoreIndicators',
					offset: offset,
					cat:catId,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-offset':parseInt(offset)+6});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		loadMoreAuthors: function( pageNum ){

			var currentButton = $('.load-more-authors');
			var currentPostBox = $('.load-more-authors-wrapper');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadAuthorsForHome',
					page: parseInt(pageNum),
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		
		loadMoreFromCat: function( catId, pageNum ){

			var currentButton = $('.load-more-btn[data-cat-id='+catId+']');
			var currentPostBox = $('.load-more-wrapper[data-cat-id='+catId+']');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadPostsForHome',
					page: parseInt(pageNum)+1,
					cat:catId,
					
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		loadMoreBrokerNews: function( catId, pageNum ){

			var currentButton = $('.load-more-broker-news[data-cat-id='+catId+']');
			var currentPostBox = $('.load-more-wrapper[data-cat-id='+catId+']');
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadPostsBrokerNews',
					page: parseInt(pageNum)+1,
					cat:catId,
					
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});

		},
		loadMoreCalendarNews: function( currentButton ){
			
			var pageNum = currentButton.attr('data-page')
			var currentPostBox = currentButton.closest('section').find('.load-more-wrapper');		
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadPostsCalendarNews',
					page: parseInt(pageNum)+1,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});
		},
		loadMoreCalendarEventNews: function( currentButton ){
			
			var pageNum = currentButton.attr('data-page');
			var index = currentButton.attr('data-index');
			var country = currentButton.attr('data-country');
			
			var currentPostBox = currentButton.closest('section').find('.load-more-wrapper');		
			var tmpLoader = $('.loader-wrap').clone();
			currentPostBox.after( tmpLoader );
			currentButton.hide();
			tmpLoader.show();
			
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: {
					action: 'loadPostsCalendarEventNews',
					page: parseInt(pageNum)+1,
					index: index,
					country: country,
				},
				success: function(data){
					tmpLoader.remove();
					if( data!='' ) currentButton.show();
					currentPostBox.append('<div class="clear"></div>'+data);
					currentButton.attr({'data-page':parseInt(pageNum)+1});
					$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
				},
				error: function(MLHttpRequest, textStatus, errorThrown){
					alert('error loading');
				}
			});
		},

		windowScroll: function(){

			var self = this;
			
			self.goUp();
		},

		jqueryExtend: function(){

			$.fn.extend({

				/**
				** Custom select
				**/

				customSelect : function(){

					// template
					var template = "<div class='active_option open_select'><div class='inner'></div><i class='fa fa-angle-down'></i></div><ul class='options_list dropDown'></ul>";

					return this.each(function(){

						var $this = $(this);

						$this.prepend(template);

						var active = $this.children('.active_option'),
							activeInner = active.children('.inner'),
							list = $this.children('.options_list'),
							select = $this.children('select').hide(),
							options = select.children('option');

						active.on('click', function(){

							active.add(list).toggleClass('opened');

						});

						activeInner.text( 
							select.children('option[selected]').text() ? 
							select.children('option[selected]').text() : 
							options.eq(0).text()
						);

						options.each(function(){

							var template = $('<li></li>'),
								val = $(this).val(),
								label = $(this).text();

							template.html('<a data-value="' + val + '" href="javascript:;">' + label + '</a>');

							list.append(template);

						});

						list.on("click", "li", function(){

							var vl = $(this).find('a').data('value');
								activeInner.text( $(this).text() );
								select.val(vl);
								select.change();
								

							$(this).addClass('active').siblings().removeClass('active');

							if(!Core.TRANSITIONSUPPORTED){

								$(this).closest('.dropDown').add(active).removeClass("active");

							}else{

								$(this).closest(list).add(active).removeClass("opened");

							}

							$('.dropDown,.active_option').removeClass('opened');

						});

						$(document).on('click', function(event){

							if(!$(event.target).closest('.custom_select').length){

								$('.dropDown,.active_option').removeClass('opened');

							}

						});

					});

				},

			});
		},

		/**
		**	Add class if li has submenu
		**/

		checkSubmenu : function(){

			$(".main_menu>li").each(function(){

				var $this = $(this);

				if ($this.is(':has(ul)')){

					$this.addClass('has_submenu');

				};

			});

		},


		additionalMenuOpen : function(){


			$('.additional_menu_box button').on('click',function(){
		    	
		    	$(this).toggleClass('active');
		    	$(".additional_menu").toggleClass('active');

		    });

		    if($('.touch').length || $(window).width()<768) {

				$('nav li').each(function(){

					if($(">ul", this)[0]){

						$(">a", this).toggle(


							function(){
								$(this).parent().toggleClass('active');
								$(this).next("ul").slideToggle();
								return false;
							},
							function(){
								window.location.href = $(this).attr("href");
							}
						);

					} 

				});

		    }

		},


		additionalMenuClose : function(){

			$(document).click(function(event) {

				if ($(event.target).closest(".additional_menu_box").length) return;
				$(".additional_menu").removeClass('active');
				event.stopPropagation();

			});

			$(document).click(function(event) {

				if ($(event.target).closest("li.has_submenu").length) return;
				$("li.has_submenu>a").removeClass('active');
				$("li.has_submenu>a").next("ul").slideUp();
				event.stopPropagation();

			});

		},

		
		
		/**
		**	Tabs
		**/

		tabsBox : function(){
			
		
			$('body').on('click','.tabs_list>li',function(){

				var $this = $(this),
					tab = $this.closest('.tabs_box'), 
					index = $this.index();

				$this.addClass('active').siblings().removeClass('active');
				tab.find(".tabs_contant>div").eq(index).addClass('active').siblings().removeClass('active');

			});

		},

		/**
		**	Header list open on responsive 
		**/

		headerList : function(){

			$('.header_list_btn').on('click',function(){

				$(this).toggleClass('active');
				$('.header_list').toggleClass('active');

			});

			$(document).click(function(event) {

				if ($(event.target).closest(".header_list,.header_list_btn").length) return;
				$(".header_list").removeClass('active');
				$(".header_list_btn").removeClass('active');
				event.stopPropagation();

			});

		},


		/**
		**	Site List accordion 
		**/

		siteList : function(){

			if($('.site_list').length){

				$(".site_list>h5").on('click',function(){

					var windowWidth = $(window).width(),
						$this = $(this);

					if(windowWidth>992){
						return false;
					}
					else{

						$this.toggleClass('active');
						$this.next("ul").slideToggle();
					}
				});
			}

		},


		/**
		**	Footer List accordion 
		**/

		footerList : function(){

			$(".list_box>h6").on('click',function(){

				var windowWidth = $(window).width(),
					$this = $(this);

				if(windowWidth>767){
					return false;
				}
				else{

					$this.toggleClass('active');
					$this.next("ul").stop().slideToggle();
				}

			});

		},


		/**
		**	Header info box tabs 
		**/

		headerInfo : function(){

			$(".quotes_box>.header_info_title").addClass("active").next().addClass('active');

			$('.header_info_title').on("click",function(){

				var $this = $(this),
					parent = $this.parent();

				$this.addClass('active')
					 .next()
					 .addClass('active')
					 .parent()
					 .siblings()
					 .find('.header_info_title')
					 .removeClass('active')
					 .next()
					 .removeClass('active');
			});

		},



		/**
		**	Go Up button 
		**/


		goUp : function(){

			var windowHeight = $(window).height(),
				windowScroll = $(window).scrollTop();

			if(windowScroll>windowHeight/2){

				$('#go_up').addClass('active');

			}
			else{

				$('#go_up').removeClass('active');

			}

		},

		goUpAnimate :function(){

			$('#go_up').on('click',function () {

				if($.browser.safari){

					$('body').animate( { scrollTop: 0 }, 1100 );

				}else{

					$('html,body').animate( { scrollTop: 0}, 1100 );

				}

				return false;

			});

		},



		/**
		**	Next button hover 
		**/

		nextButtonHover : function(){

			$('.next_btn').hover(

					function(){
						$('.materials_box').addClass('active');
					},

					function(){
						$('.materials_box').removeClass('active');
					}
				);
		},


		/**
		**	Digital Watch 
		**/

		digitalWatch : function() {
		    
		    if($("#digital_watch").length){

			    var self = this;
			    var date = new Date();
			    var hours = date.getHours();
			    var minutes = date.getMinutes();
			    var seconds = date.getSeconds();
			    if (hours < 10) hours = "0" + hours;
			    if (minutes < 10) minutes = "0" + minutes;
			    if (seconds < 10) seconds = "0" + seconds;
			    document.getElementById("digital_watch").innerHTML = hours + ":" + minutes + ":" + seconds;
			    setTimeout(function(){
			    	self.digitalWatch();
			    },1000);
			    
		    }
		
		},


		/**
		**	Dropdoun 
		**/

		dropdoun : function(){

			$('.dropdown_btn').on("click",function(e){

				e.preventDefault();

				$(this).toggleClass('active').next().toggleClass('active');
			});

			$(document).on('click', function(event){

				if(!$(event.target).closest('.dropdown_box').length){

					$('.dropdoun, .dropdown_btn').removeClass('active');

				}

			});

		},


		/**
		**	Dropdoun 
		**/

		/*optionsBox : function(){

			$(".turn_box").on("click",".options_btn",function(){

				var $this = $(this),
					parent = $this.closest(".turn_box");

				$this.toggleClass("active");
				parent.find('.options_box').toggleClass("opened");				

			});

			$(document).on("click",function(event){

				if(!$(event.target).closest('.options_box,.options_btn').length){

					$('.options_box').removeClass('opened');
					$('.options_btn').removeClass('active');

				}

			});

		},*/


		/**
		**	Dropdoun 
		**/

		tablQuotesSlider : {

			init: function(){

				var self = this;
				
				self.ww = $(window);
				self.table = $('.tabl_quotes table');
				self.tableNav = $('.tabl_quotes_nav');
				self.nav = $('tabl_quotes_nav');
				self.itemWidth;
				self.itemQt = self.table.attr('data-item') ? self.table.attr('data-item') : 4;

				$('.tabl_quotes tr').addClass('tabl_quotes_item');

				self.createBtn();
				self.WidthSlideItem();
				self.moveSlider();

				$(window).on("resize", function(){

					self.WidthSlideItem();
				
				});



			},

			WidthSlideItem: function(){

				var self = this,
					wrapBox = $('.tabl_quotes').parents('.tabs_contant').length ? $('.tabl_quotes').parents('.tabs_contant').width() : $('.tabl_quotes').width();

				self.itemWidth = wrapBox/self.itemQt;

				if(self.ww.width() < 992){

					$('.tabl_quotes tr').width(self.itemWidth);

				}

				self.table.each(function(){

						var $this = $(this),
							qItem = $(this).find("tbody>tr").length;
						$(this).width(qItem*self.itemWidth +1);

				});
				
				if(self.ww.width() < 992){

					/*set first td height*/
					
					self.table.each(function(){

						var $this = $(this),
							firstTdHeight = 0,
							$trs = $(this).find("tbody>tr");
							
						$trs.each(function(){
							$(this).find("td:first").css({
								'height': 'auto',
							});
						});
						
						$trs.each(function(){
							var tmpHeight = $(this).find("td:first").outerHeight();
							if( firstTdHeight < tmpHeight ) firstTdHeight = tmpHeight;
						});
						
						$trs.each(function(){
							$(this).find("td:first").css({
								'height': firstTdHeight + 'px',
							});
						});
						
					});

				}else{
					self.table.each(function(){

						var $this = $(this),
							$trs = $(this).find("tbody>tr");
							
						$trs.each(function(){
							$(this).find("td:first").css({
								'height': 'auto',
							});
						});
					});
				}


			},

			createBtn : function(){

				var self = this;

				self.table.each(function(){

					var $this = $(this),
					template = '<div class="tabl_quotes_nav clearfix"><button class="prev disable"><i class="fa fa-angle-left"></i></button><button class="next"><i class="fa fa-angle-right"></i></button></div>',
						qItem = $this.find("tbody>tr").length;

					if(qItem > self.itemQt){
						$this.closest('.turn_content').prepend(template);
					}

				});

			},

			moveSlider : function(){

				var self = this;

				$('body').on("click",".tabl_quotes_nav>*:not(.process)",function(){

					if($(this).hasClass('disable')) return false;

					var $this = $(this),
						wrapSlider = $this.closest('.turn_content').find('.tabl_quotes'),
						slider = wrapSlider.find('table'),
						sliderWidth = slider.width(),
						wrapOffset = wrapSlider.offset().left,
						sliderOffset = slider.offset().left,
						lastOffset = wrapSlider.width() - slider.width(),
						position = wrapOffset - sliderOffset,
						prevItem = - position + self.itemWidth,
						nextItem = - position - self.itemWidth;


					if($this.hasClass("next")){

						$this.addClass('process');
						slider.css({
							'left' : nextItem
						});
						setTimeout(function(){
							$this.removeClass('process');
						},500);

						$this.siblings().removeClass('disable');

						if((nextItem - self.itemWidth) < lastOffset){

							$this.addClass('disable');
						
						}

					}
					else{

						$this.addClass('process');
						slider.css({
							'left' : prevItem
						});
						setTimeout(function(){
							$this.removeClass('process');
						},500);

						$this.siblings().removeClass('disable');

						if((prevItem + self.itemWidth) > 0 ){

							$this.addClass('disable');
						
						}

					}

				});

			},

		},


		/**
		**	More Tools
		**/

		moreTools : function(){

			$('.more_tools_btn').on('click','a', function(){

				$(this).toggleClass('active');
				$(this).parent().next('.more_tools_box').slideToggle();

			});

		},


		/**
		**	More Tools
		**/

		getCode : function(){

			$(".agreement_box").on("click","label", function(){

				var input = $(this).parent().find('input'),
					button = $(this).parents('.get_code_box').find('.edition_btn'),
					box = $(this).parents('.get_code_box').find('.get_code');

				if(!input.is(':checked')){
					
					box.slideDown();
					button.removeClass('disabled');

				}
				else{

					box.slideUp();
					button.addClass('disabled');			
				
				}

			});

		},
		

		/**
		**	Dropdoun Requirements  add 00.02.16
		**/

		dropdounRequirements : function(){

			$('.requirements_dropdown_button').on("click",function(){

				$(this).parent('.requirements_dropdown').toggleClass('active').next().slideToggle();

			});


		},
/**
		**	Language Dropdown
		**/


		languageDropdown: function(){

			$('.language_dropdown_label').on("click", function(){

				$(this).closest('.language_dropdown').toggleClass('opened');

			});

			$(document).on('click', function(event){

				if(!$(event.target).closest('.language_dropdown').length){
					$('.language_dropdown').removeClass('opened');
				}
	
			});

		},

	}


	$(function(){

		Core.DOMReady();

	});

	$(window).scroll(function(){

		Core.windowScroll();
		
	});
	
	var newBox         = $('.news_top_box'),
		dropdownNews   = $('.dropdown_news'),
		linkNewsBtns = $('.link_news_btns'),
		linkNewsBox  = $('.link_news_box');

	if(newBox.length || linkNewsBtns.length){
		$('body').on('click','.news_top_box',function(){
			$(this).toggleClass('active').next(dropdownNews).slideToggle();
		});
		$('body').on('click','.link_news_btns',function(){
			$(this).toggleClass('active').next(linkNewsBox).slideToggle();
		});
	}


})(jQuery);


;(function($){
		/* ------------------------------------------------
		FILTER ON CLICK AND CHANGE POSITION START
		------------------------------------------------ */

				//скрипт  считывает сколько ширина скрола / старт

				var scrollWidth;
				function detectScrollBarWidth(){
					var div = document.createElement('div');
					div.className = "detect_scroll_width";
					document.body.appendChild(div);
					scrollWidth = div.offsetWidth - div.clientWidth;
					document.body.removeChild(div);
				}
				detectScrollBarWidth();

				//скрипт  считывает сколько ширина скрола / конец

				// скрипт переставляет блок на определенной ширине в другой блок / старт
				var drop  = $('.dropdown_news'),
				    share = $('.date_with_share_box');

				function filterPosition(){
		          var bodyWidth = $(window).width();
		          if(bodyWidth + scrollWidth <= 479 && $('body').hasClass('filterPosition')){
		            $('.link_news_container').before($('.btn_red'));
		            $('body').removeClass('filterPosition');
		          }
		          else if(bodyWidth + scrollWidth > 479 && !$('body').hasClass('filterPosition')){
		            $(".last_news_title").append($('.btn_red'));
		            $('body').addClass('filterPosition');
		          }
		        } 
		        filterPosition();
				$(window).on('resize',function(){
			        setTimeout(function(){
		            	filterPosition();
		            },200);
	            });

				// скрипт переставляет блок на определенной ширине в другой блок / конец


		/* ------------------------------------------------
		FILTER ON CLICK AND CHANGE POSITION END
		------------------------------------------------ */
})(jQuery);