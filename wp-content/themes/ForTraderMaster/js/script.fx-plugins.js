;(function($){
    
        "use strict";
    
        $(document).ready(function(){
    
            /* ------------------------------------------------
                    Owl carousel
            ------------------------------------------------ */
    
                if($('.owl-carousel').length){
    
                    $('.owl-carousel').each(function(){
    
                        var $this = $(this);
    
                        if($this.hasClass('full_width')){
    
                            $this.owlCarousel({
    
                                navigation : true, // Show next and prev buttons
                                slideSpeed : 300,
                                paginationSpeed : 400,
                                singleItem:true
    
                            });
    
                        }
                        else{
    
                            var desktop = $this.attr('data-desktop'),
                                large = $this.attr('data-large'),
                                medium = $this.attr('data-medium'),
                                small = $this.attr('data-small'),
                                extraSmall = $this.attr('data-extra-small');
    
                            $this.owlCarousel({
    
                                itemsCustom : [
                                    [0, extraSmall],
                                    [480, small],
                                    [768, medium],
                                    [992, large],
                                    [1200, desktop]
                                ],
                                navigation : true
    
                            });
    
                        }
    
                    });
    
                }
    
            /* ------------------------------------------------
                    End of Owl carousel
            ------------------------------------------------ */

            /* ------------------------------------------------
				Arcticmodal
		------------------------------------------------ */

			$(".arcticmodal").on("click",function(){
                var modal = $(this).attr("data-modal");
                $(modal).arcticmodal();
                return false;
            });
                
            /* ------------------------------------------------
               End of Arcticmodal
            ------------------------------------------------ */
    
        });
    
        $(window).load(function(){
    
    
            /*-----------------------------------------------
                Mobile footer list
            ------------------------------------------------*/
    
                (function() {
                    var width = $(window).width();
                    if( width < 480) {
                        $(".fx_footer-mob-list").css({display: "none"});
                        $(".fx_footer-mob-btn").click(function(){
                            $(this).next(".fx_footer-mob-list").slideToggle();
                            $(this).find(".fx_ic-drop").toggleClass("fx_ic-drop-active");
                        });
                    }
                }());
    
            /*----------------------------------------------
                End of the Mobile footer list
            -----------------------------------------------*/
    
            /*----------------------------------------------
                Mobile menu
            -----------------------------------------------*/
    
                $(".fx_mob-menu").click(function() {
                    $(this).toggleClass("fx_mob-menu-active");
                    $(".fx_menu").slideToggle();
                });
    
            /*----------------------------------------------
                End of the Mobile menu
            -----------------------------------------------*/
    
        });
    
    })(jQuery);