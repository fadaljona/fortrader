;(function($){
    
        "use strict";
    
        var FxCore = {
    
            DOMReady: function(){
                var self = this;
                self.buttonSlider();
                self.goUpAnimate();
                self.goUp();
                self.languageDropdown();
                self.popupLoginLogout();
                self.footerSubscribe();

                self.additionalMenuOpen();
            },

            windowLoad: function(){
                var self = this;
            },

            windowScroll: function(){
                var self = this;
                self.goUp();
            },
            windowResize: function(){
                var self = this;
            },

            additionalMenuOpen : function(){


                $('.additional_menu_box button').on('click',function(){
    
                    $(this).toggleClass('active');
                    $(".additional_menu").toggleClass('active');
    
                });
    
                if($('.touch').length || $(window).width()<768) {
    
                    $('nav li').each(function(){
    
                        if($(">ul", this)[0]){
    
                            $(">a", this).toggle(
    
    
                                function(){
                                    $(this).parent().toggleClass('active');
                                    $(this).next("ul").slideToggle();
                                    return false;
                                },
                                function(){
                                    window.location.href = $(this).attr("href");
                                }
                            );
    
                        }
    
                    });
    
                }
    
            },
            footerSubscribe: function(){
                var self = this;
                $('#subscribeFormFooter input[type="submit"]').on('click',function(event){
                    event.preventDefault();
                    var userEmailValid = self.checkEmail( '#subscribeFormFooter', 'user-email');
                    if (  userEmailValid ){
                        self.popupLoginLogoutAjax( '#subscribeFormFooter', $('#subscribeFormFooter').serialize(), 'subscribe' );
                    }
                });
            },
            popupLoginLogout: function(){
                var self = this;
                $('#logInForm .modal_button').on('click',function(event){
                    event.preventDefault();
                    var userNameValid = self.checkInputs( '#logInForm', 'user-name', 1, 'Username can not be empty' );
                    var userPasswordValid = self.checkInputs( '#logInForm', 'user-password', 5, 'Password is too short - at least 5 characters' );
                    
                    if (  userNameValid && userPasswordValid ){
                        self.popupLoginLogoutAjax( '#logInForm', $('#logInForm').serialize(), 'logIn' );
                    }
                });
                $('#registrationForm  .modal_button').on('click',function(event){
                    event.preventDefault();
                    var userNameValid = self.checkInputs( '#registrationForm', 'user-name', 1, 'Username can not be empty' );
                    var userEmailValid = self.checkEmail( '#registrationForm', 'user-email');
                    
                    if( $('#registrationForm #g-recaptcha-response').val() == '' ){
                        var captchaValid = false;
                        $('#registrationForm .valid-g-recaptcha').html( jsThemeTexts.captchaVlidationFail );
                    }else{
                        var captchaValid = true;
                        $('#registrationForm .valid-g-recaptcha').html('');
                    }
                    
                    if (  userNameValid && userEmailValid && captchaValid ){
                        self.popupLoginLogoutAjax( '#registrationForm', $('#registrationForm').serialize(), 'registration' );
                    }
                });
                $('#lostpasswordForm .modal_button').on('click',function(event){
                    event.preventDefault();
                    var userNameValid = self.checkInputs( '#lostpasswordForm', 'user-name', 1, 'Username can not be empty' );
                    
                    if (  userNameValid ){
                        self.popupLoginLogoutAjax( '#lostpasswordForm', $('#lostpasswordForm').serialize(), 'lostpassword' );
                    }
                });
                $('#subscribeForm button').on('click',function(event){
                    event.preventDefault();
                    var userEmailValid = self.checkEmail( '#subscribeForm', 'user-email');
                    if (  userEmailValid ){
                        self.popupLoginLogoutAjax( '#subscribeForm', $('#subscribeForm').serialize(), 'subscribe' );
                    }
                });
            },
            popupLoginLogoutAjax: function( formId, serializedForm, actionType ){
                var self = this;
                if( self.loading ) return false;
                $(formId + ' input').prop('disabled', true);
    
                self.loading = true;
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: '/wp-admin/admin-ajax.php',
                    data: {
                        action: 'ajaxLoginLogout',
                        form: serializedForm,
                        actionType: actionType,
                    },
                    'success':function(json){
                        if( json != null ){		
                            if( json.success == false ){
                                $(formId + ' .valid-all-form').html(json.message);
                                $(formId + ' input').prop('disabled', false);
                            }
                            if( json.success == true ){
                                if( json.refresh == true ){	
                                    location.reload();
                                    return false;
                                }
                                $(formId + ' .valid-all-form').html(json.message);
                                $(formId + ' button').prop('disabled', true);
                            }
                        }
                        self.loading = false
                    }
                });
            },
            checkInputs: function( formId, selector, valLength, errorMessage ){
                var inputEl = $( formId + ' .' + selector );
                var errorEl = $( formId + ' .valid-' + selector );
                if( inputEl.val() != '' && inputEl.val().length >= valLength ) {
                            inputEl.removeClass('invalid');
                            errorEl.html('');
                            return true;
                } else {
                    inputEl.addClass('invalid');errorEl.html( errorMessage );
                    return false;
                }
            },
            checkEmail: function( formId, selector ){
                var inputEl = $( formId + ' .' + selector );
                var errorEl = $( formId + ' .valid-' + selector );
                if(inputEl.val() != '') {
                        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                        if(pattern.test(inputEl.val())){
                            inputEl.removeClass('invalid');
                            errorEl.html('');
                            return true;
                        } else {
                            inputEl.addClass('invalid');
                            errorEl.html('Not valid email<br />');
                            return false;
                        }
                    } else {
                        inputEl.addClass('invalid');
                        errorEl.html('Email can not be empty<br />');
                        return false;
                    }
            },
    
            /**
            **	Nav Buttons
            **/
    
            buttonSlider : function(){
                $('.nav_buttons>*').on('click',function(event){

                    var parent = $(this).parents('.turn_box'),
                        turn = $(this).parents('.turn_box').find('.turn_content'),
                        owl = parent.find(".owl-carousel");
    
                    if($(this).hasClass('prev_btn')){
    
                        owl.trigger('owl.prev');
                        event.preventDefault();
    
                    }
                    else if($(this).hasClass('next_btn')){
    
                        owl.trigger('owl.next');
                        event.preventDefault();
    
                    }
                    else if($(this).hasClass('turn_btn')){
    
                        turn.slideToggle(500);
                        $(this).toggleClass("active");
                        event.preventDefault();
                    }
    
                });
    
            },
    
            /**
            **	Go Up button
            **/
    
            goUp : function(){
    
                var windowHeight = $(window).height(),
                    windowScroll = $(window).scrollTop();
    
                if(windowScroll>windowHeight/2){
    
                    $('#go_up').addClass('active');
    
                }
                else{
    
                    $('#go_up').removeClass('active');
    
                }
    
            },
    
            goUpAnimate :function(){
    
                $('#go_up').on('click',function () {
    
                    if($.browser.safari){
    
                        $('body').animate( { scrollTop: 0 }, 1100 );
    
                    }else{
    
                        $('html,body').animate( { scrollTop: 0}, 1100 );
    
                    }
    
                    return false;
    
                });
    
            },
    
            /**
            **	Language Dropdown
            **/
    
            languageDropdown: function(){
    
                $('.language_dropdown_label').on("click", function(){
    
                    $(this).closest('.language_dropdown').toggleClass('opened');
    
                });
    
                $(document).on('click', function(event){
    
                    if(!$(event.target).closest('.language_dropdown').length){
                        $('.language_dropdown').removeClass('opened');
                    }
        
                });
    
            },
    
        }
    
        $(function(){
    
            FxCore.DOMReady();
    
        });
    
        $(window).load(function(){
    
            FxCore.windowLoad();
    
        });
    
        $(window).scroll(function(){
    
            FxCore.windowScroll();
    
        });
    
        $(window).on("resize",function(){
    
            FxCore.windowResize();
    
        });

    })(jQuery);