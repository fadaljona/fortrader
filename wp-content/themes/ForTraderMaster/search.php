<?php get_header();?>           
<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part">
		<div class="blockdiv1"><?php dynamic_sidebar('banner-728x90');?></div>
		<hr>

		<section class="section_offset">
			<?php
				if (have_posts()) : 
					global $wp_query;
			?> 
			<h2 class="page_title1 clearfix">
				<?php _e("Site search", 'ForTraderMaster'); ?>
				<span class="sub_title1 alignright"><?php _e("Found results: about", 'ForTraderMaster'); ?> <?php echo $wp_query->found_posts; ?></span>
			</h2>
			<hr class="separator1">
			<!-- - - - - - - - - - - - - - Search Result - - - - - - - - - - - - - - - - -->
			<div class="search_result_box clearfix">
				<?php 
					while (have_posts()) : the_post();
						get_template_part( 'templates/loop-search' );
					endwhile;
				?>
			</div>
			<!-- - - - - - - - - - - - - - End of Search Result - - - - - - - - - - - - - - - - -->
			<?php 
				get_template_part('templates/pagination');
				else: ?>
				<h2 class="clearfix"><?php _e("Nothing found", 'ForTraderMaster'); ?>.</h2>
				<h2 class="clearfix"><?php _e("Try changing your search query", 'ForTraderMaster'); ?></h2>
			<?php endif; ?>
		</section>
		<?php get_template_part('templates/single-editor-recommends');?>


	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php get_sidebar();?>
<?php get_footer();?>