<?php get_header();

if (have_posts ()) {
    while (have_posts ()) {
        the_post();
    }
}
else {
    $fail = true;
}
?> 
	<div class="left_part">
		<?php get_template_part_by_locale( 'templates/banner-728x90' );?>
		<div class="clearfix">
			<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
			<div class="breadcrumbs">
				<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
					<li>
						<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a href="<?php echo pll_home_url();?>" itemprop="item"><span itemprop="name"><?php _e("Home", 'ForTraderMaster'); ?></span></a>
							<meta itemprop="position" content="1" />
						</span>
					</li>
					<li>
						<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a href="<?php the_permalink(); ?>" itemprop="item"><span class="breadcrumb-active" itemprop="name"><?php the_title();?></span></a>
							<meta itemprop="position" content="2" />
						</span>
					</li>
				</ul>
			</div>
			
			<!-- - - - - - - - - - - - - - End of Breadcrumbs - - - - - - - - - - - - - - - - -->
		</div><!-- / .cleafix -->
		<hr>
	<?php
		if (!isset($ExPolls) || !is_object($ExPolls) || get_class($ExPolls) != 'ExPolls') {
			$fail = true;
		}

		if (isset($fail)) {
			include (get_template_directory() . '/404.php');
			exit();
		}

		/* @var $ExPolls ExPolls */
		$per_page = get_option('posts_per_page');
		get_current_user();

		include (get_template_directory() . '/custom_pages/expolls/' . $ExPolls->WP_getTemplate() . '.php');

	?>
	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php get_sidebar();?>
<?php get_footer();?>
