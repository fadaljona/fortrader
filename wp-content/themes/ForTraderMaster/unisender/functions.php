<?php

function addUserToUnisender($email, $listId)
{
    $api_key = "6by6bkwnt4zgt6muz5aerdzsefhir5w97sfti1do";

    if (!$email) {
        return false;
    }


    $POST = array(
        'api_key' => $api_key,
        'list_ids' => $listId,
        'fields[email]' => $email,
        'request_ip' => $_SERVER['REMOTE_ADDR'],
        'tags' => urlencode("Added using API")
    );


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $POST);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt(
        $ch,
        CURLOPT_URL,
        'https://api.unisender.com/ru/api/subscribe?format=json'
    );
    $result = curl_exec($ch);

    if ($result) {
        $jsonObj = json_decode($result);
      
        if (null===$jsonObj) {
            return false;
        } elseif (!empty($jsonObj->error)) {
            return false;
        } else {
            return true;
        }
    }
    return false;
}
