<?php get_header();?>
<?php
global $fortraderCache;
$locale = get_locale();
$cacheDuration = get_option('theme_cache_duration');
$cache_key = 'theme_' . md5('page-404') . $locale;
if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
?>           
	<!-- - - - - - - - - - - - - - Error - - - - - - - - - - - - - - - - -->
	<div class="error_block">
		<h2 class="error_title">
			<span class="red_color"><?php _e("Error 404", 'ForTraderMaster'); ?></span>
			<strong><?php _e("page not found", 'ForTraderMaster'); ?></strong>
		</h2>
		<h3 class="error_sub_title"><?php _e("Something went wrong", 'ForTraderMaster'); ?></h3>
		<span class="error_text">
			<?php _e("Return to the home page or use the site search", 'ForTraderMaster'); ?>
		</span>
		<div>
			<img src="<?php bloginfo('template_url'); ?>/images/error_img.jpg" alt="">
		</div>
		<a href="/" class="come_back_btn"><?php _e("Return to home page", 'ForTraderMaster'); ?></a>
	</div>
	<!-- - - - - - - - - - - - - - End of Error - - - - - - - - - - - - - - - - -->
<?php $fortraderCache->endCache(); } ?>
<?php get_footer();?>