<?php

//Loading theme textdomain
function locale_theme_setup() {
	$path = get_template_directory() . '/languages';
    $loaded = load_theme_textdomain( 'ForTraderMaster', $path );
}
add_action( 'after_setup_theme', 'locale_theme_setup' );





// remove admin bar on front page
show_admin_bar(false);

register_sidebar(array('name'=>'header-banner','before_widget'=>'','after_widget'=>'','before_title'=>'','after_title'=>''));
register_sidebar(array('name'=>'banner-728x90','before_widget'=>'','after_widget'=>'','before_title'=>'','after_title'=>''));
register_sidebar(array('name'=>'home-indicator-ads','before_widget'=>'','after_widget'=>'','before_title'=>'','after_title'=>''));
register_sidebar(array('name'=>'300x250-2-post-banner','before_widget'=>'','after_widget'=>'','before_title'=>'','after_title'=>''));
register_sidebar(array('name'=>'300x250-3-post-banner','before_widget'=>'','after_widget'=>'','before_title'=>'','after_title'=>''));
	
function register_my_menu() {
  register_nav_menus(
    array(
	  'header-menu' => __( 'Main Menu' ),
	  'footer-menu1' => __( 'Footer Menu Analysis & Forecasts' ),
	  'footer-menu2' => __( 'Newbie on Forex' ),
	  'footer-menu3' => __( 'Our Services' ),
    )
  );
}
add_action( 'init', 'register_my_menu' );


require_once( 'includes/mainMenuWalker.php' );
require_once( 'includes/footerMenuWalker.php' );
require_once( 'includes/ajax.php' );
require_once( 'includes/retrieveWpPassword.php' );





add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
	if( in_array('current-menu-ancestor', $classes) ){
		$classes[] = 'current ';
	}
	if( in_array('current-menu-item', $classes) ){
		$classes[] = 'current ';
	}
	return $classes;
}

//галочка в профиле юзера
if( current_user_can( 'administrator' ) ){
	add_action( 'show_user_profile', 'extra_user_profile_fields' );
	add_action( 'edit_user_profile', 'extra_user_profile_fields' );
	add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
	add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );
}
function extra_user_profile_fields( $user ) { ?>
	<h3><?php _e("Only admins can view", 'ForTraderMaster'); ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="show_user_on_home"><?php _e("Show on home?", 'ForTraderMaster'); ?></label></th>
			<td>
				<fieldset>
				<label for="show_user_on_home">
				<input type="checkbox" <?php if( get_user_meta( $user->ID, 'show_user_on_home', true ) ) echo 'checked="checked"'?> value="1" name="show_user_on_home">
				<?php _e("Show user on home", 'ForTraderMaster'); ?></label><br>
				</fieldset>
			</td>
		</tr>
	</table>
<?php }
function save_extra_user_profile_fields( $user_id ) {
	update_user_meta( $user_id, 'show_user_on_home', $_POST['show_user_on_home'] );
}






//социальные ссылки

add_action( 'show_user_profile', 'sm_user_profile_fields' );
add_action( 'edit_user_profile', 'sm_user_profile_fields' );
add_action( 'personal_options_update', 'save_sm_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_sm_user_profile_fields' );

function sm_user_profile_fields( $user ) { ?>
	<h3><?php _e("User social buttons", 'ForTraderMaster'); ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="tw_link"><?php _e("Twitter", 'ForTraderMaster'); ?></label></th>
			<td><input type="text" class="regular-text" value="<?php echo get_user_meta( $user->ID, 'tw_link', true ); ?>" id="tw_link" name="tw_link"></td>
		</tr>
		<tr>
			<th><label for="fb_link"><?php _e("Facebook", 'ForTraderMaster'); ?></label></th>
			<td><input type="text" class="regular-text" value="<?php echo get_user_meta( $user->ID, 'fb_link', true ); ?>" id="fb_link" name="fb_link"></td>
		</tr>
		<tr>
			<th><label for="vk_link"><?php _e("Vkontakte", 'ForTraderMaster'); ?></label></th>
			<td><input type="text" class="regular-text" value="<?php echo get_user_meta( $user->ID, 'vk_link', true ); ?>" id="vk_link" name="vk_link"></td>
		</tr>
	</table>
<?php }
function save_sm_user_profile_fields( $user_id ) {
	update_user_meta( $user_id, 'tw_link', $_POST['tw_link'] );
	update_user_meta( $user_id, 'fb_link', $_POST['fb_link'] );
	update_user_meta( $user_id, 'vk_link', $_POST['vk_link'] );
}

function get_category_tags( $catId ) {
	global $wpdb;
	$sql = "SELECT terms2.name as tag_name, terms2.term_id as tag_id
		FROM
			wp_posts as p1
			LEFT JOIN wp_term_relationships as r1 ON p1.ID = r1.object_ID
			LEFT JOIN wp_term_taxonomy as t1 ON r1.term_taxonomy_id = t1.term_taxonomy_id
			LEFT JOIN wp_terms as terms1 ON t1.term_id = terms1.term_id,

			wp_posts as p2
			LEFT JOIN wp_term_relationships as r2 ON p2.ID = r2.object_ID
			LEFT JOIN wp_term_taxonomy as t2 ON r2.term_taxonomy_id = t2.term_taxonomy_id
			LEFT JOIN wp_terms as terms2 ON t2.term_id = terms2.term_id
		WHERE
			t1.taxonomy = 'category' AND p1.post_status = 'publish' AND terms1.term_id = ".$catId." AND
			t2.taxonomy = 'post_tag' AND p2.post_status = 'publish'
			AND p1.ID = p2.ID
		ORDER by tag_id";
	$tmpTags = $wpdb->get_results($sql);
	$tags = array();
	$tmpTagId=0;
	$i=-1;
	foreach( $tmpTags as $tmpTag ){
		if( $tmpTag->tag_id ){
			if( $tmpTagId != $tmpTag->tag_id ){
				$i=$i+1;
				$tags[$i]['name'] = $tmpTag->tag_name;
				$tags[$i]['link'] = get_tag_link( $tmpTag->tag_id );
				$tags[$i]['id'] = $tmpTag->tag_id;
				$tags[$i]['count'] = 1;
				$tmpTagId = $tmpTag->tag_id;
			}else{
				$tags[$i]['count'] += 1;
			}
			
		}
	}
	return $tags;
}

//для опросов
function get_pagination($total_count = 0, $now_page = 1, $per_page = 10, $count_pages = 11, $additional = '') {
    $total_pages = round($total_count / $per_page);
    $now_page = $now_page > $total_pages ? $total_pages : $now_page;
    $now_page = $now_page <= 0 ? 1 : $now_page;

    if ($total_pages <= 1) {
        return array(
            'total_pages' => 1,
            'count' => 1,
            'pages' => array(),
            'start' => 0,
            'prev' => null,
            'next' => null,
        );
    }

    $half_pages = round(($count_pages - 1) / 2);
    $pages = $prev = $next = array();
    $prefix = '?';

    if ($additional) {
        $prefix = '?' . $additional . '&';
    }

    if ($now_page + $half_pages > $total_pages)
        $negative_half_pages = $half_pages + ($half_pages - ($total_pages - $now_page));
    else
        $negative_half_pages = $half_pages;

    for ($i = ($now_page - 1); $i > $now_page - 1 - $negative_half_pages; $i--) {
        $num = $i;
        if ($num < 1) {
            break;
        }

        $pages[] = array(
            'url' => $prefix . 'page=' . $num,
            'num' => $num,
        );

        if (++$num == $now_page) {
            $prev = $pages[count($pages) - 1];
        }
    }
    $pages = array_reverse($pages);
    $pages[] = array(
        'url' => '',
        'num' => $now_page,
    );

    if ($now_page - $half_pages <= 0)
        $positive_half_pages = $half_pages + ($half_pages - $now_page + 1);
    else
        $positive_half_pages = $half_pages;

    for ($i = 1; $i <= $positive_half_pages; $i++) {
        $num = $now_page + $i;
        if ($num > $total_pages)
            break;
        $pages[] = array(
            'url' => $prefix . 'page=' . $num,
            'num' => $num,
        );

        if (--$num == $now_page) {
            $next = $pages[count($pages) - 1];
        }
    }

    return array(
        'count' => count($pages),
        'last_page' => isset($pages[count($pages) - 1]) ? $pages[count($pages) - 1]['num'] : 0,
        'first_page' => isset($pages[0]) ? $pages[0]['num'] : 0,
        'total_pages' => $total_pages,
        'pages' => $pages,
        'start' => ($now_page * $per_page) - $per_page,
        'prev' => $prev,
        'next' => $next,
    );
}


add_image_size( 'new-theme-post-size', 730 );

add_filter( 'image_size_names_choose', 'new_theme_sizes' );

function new_theme_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'new-theme-post-size' => 'Для поста',
    ) );
}



//questions

add_action( 'admin_menu', 'add_question_post_menu_bubble' );
function add_question_post_menu_bubble() {
	global $menu;
	global $wpdb;
	$query = "SELECT COUNT(*) FROM $wpdb->posts WHERE post_status = 'pending' AND post_author = '1'";
	$questionCount = $wpdb->get_var($query);
	if ( $questionCount) {
		foreach ( $menu as $key => $value ) {
			if ( $menu[$key][2] == 'edit.php' ) {
				$menu[$key][0] .= ' <span class="awaiting-mod count-15"><span class="pending-count">' . $questionCount . '</span></span>';
				return;
			}
		}
	}
}


add_action( 'add_meta_boxes', 'add_post_hide_date_metabox' );
function add_post_hide_date_metabox() {
	add_meta_box('post_hide_date', __("Hide date", 'ForTraderMaster'), 'post_hide_date', array('post', 'page'), 'side', 'high');
}
function post_hide_date() {
	global $post;
	echo '<input type="hidden" name="hide_date_noncename" id="hide_date_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	$hide_date = get_post_meta($post->ID, 'hide_date', true);
	
	$checked = '';
	if( $hide_date ) $checked = 'checked';
	echo '<input type="checkbox" id="hide_date" '.$checked.' name="hide_date"  value="1">' . __('Hide date?', 'ForTraderMaster') . '<br><br>';
}

function save_hide_date($post_id, $post) {
	
	if ( !isset($_POST['hide_date_noncename']) || !wp_verify_nonce( $_POST['hide_date_noncename'], plugin_basename(__FILE__) )) {
		return $post->ID;
	}
	if( $post->post_type == 'revision' ) return; 
	if ( !current_user_can( 'edit_post', $post->ID )) return $post->ID;
	
	
	if( isset($_POST['hide_date']) ){
		update_post_meta($post->ID, 'hide_date', $_POST['hide_date']);
		if(!$_POST['hide_date']) delete_post_meta($post->ID, 'hide_date'); 
	}

}
add_action('save_post', 'save_hide_date', 1, 2); 







add_action( 'add_meta_boxes', function() {
    add_meta_box('post_for_crypto_currencies_beginer', __("For crypto currencies beginer", 'ForTraderMaster'), function() {
        global $post;
        echo '<input type="hidden" name="crypto_currencies_beginer_noncename" id="crypto_currencies_beginer_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
        $for_crypto_currencies_beginer = get_post_meta($post->ID, 'for_crypto_currencies_beginer', true);
        
        $checked = '';
        if( $for_crypto_currencies_beginer ) $checked = 'checked';
        echo '<input type="checkbox" id="for_crypto_currencies_beginer" '.$checked.' name="for_crypto_currencies_beginer"  value="1">' . __('For crypto currencies beginer?', 'ForTraderMaster') . '<br><br>';
    }, array('post'), 'normal', 'high');
});

add_action('save_post', function($post_id, $post) {
    if ( !isset($_POST['crypto_currencies_beginer_noncename']) || !wp_verify_nonce( $_POST['crypto_currencies_beginer_noncename'], plugin_basename(__FILE__) )) {
		return $post->ID;
	}
	if( $post->post_type == 'revision' ) return; 
	if ( !current_user_can( 'edit_post', $post->ID )) return $post->ID;
	
	
	if( isset($_POST['for_crypto_currencies_beginer']) ){
		update_post_meta($post->ID, 'for_crypto_currencies_beginer', $_POST['for_crypto_currencies_beginer']);
		if(!$_POST['for_crypto_currencies_beginer']) delete_post_meta($post->ID, 'for_crypto_currencies_beginer'); 
	}
}, 1, 2); 





function print_comments_number(){
	$number = get_comments_number();
	if( !$number ) return false;
	echo '<span class="red_color">(' . $number . ')</span>';
}
function print_comments_number_l2( $print = true ){
	$number = get_comments_number();
	if( !$number ) return false;
	if( $print ){
		echo '<span class="fx_box-count">(' . $number . ')</span>';
	}else{
		return '<span class="fx_box-count">' . $number . '</span>';
	}
}

function addRecaptchaToRegisterForm() {
	
	echo "<script src='https://www.google.com/recaptcha/api.js'></script>";
	echo '<div class="g-recaptcha" data-sitekey="6Lf_WxsUAAAAAKksUMKf2cjvKpCKbLZRyi3kg_Ev" style="transform:scale(0.9);-webkit-transform:scale(0.9);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>';

}
add_action('register_form', 'addRecaptchaToRegisterForm'); 



function checkRecaptcha($errors, $sanitized_user_login, $user_email){
	if( isset($_POST['form']) ) {
		$formVals = array();
		parse_str($_POST['form'], $formVals);
		$gRecaptchaResponse = $formVals['g-recaptcha-response'];
	}else{
		$gRecaptchaResponse = $_POST['g-recaptcha-response'];
	}
	
	require_once( get_template_directory().'/includes/recaptchalib.php' );
	$secret = "6Lf_WxsUAAAAAL6bs7UCcBCXfsZ-PX3eEL_GgYZk";
	$reCaptcha = new ReCaptcha($secret);
	$response = null;

	$response = $reCaptcha->verifyResponse(
		$_SERVER["REMOTE_ADDR"],
		$gRecaptchaResponse
	);
	if ($response != null && $response->success){
		$outArr['ReCaptchaSuccess'] = 1;
	}else{
		$errors->add( 'captcha_not_valid', __("CAPTCHA validation fails, pass validation. If you can't-refresh the page", 'ForTraderMaster') );
	}

	return $errors;
}
add_filter('registration_errors' , 'checkRecaptcha' , 10 , 3);













function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

function blue_red_mce_before_init_insert_formats( $init_array ) {
	$style_formats = array(
		array(  
			'title' => __('Blue content block', 'fortrader_settings'),  
			'block' => 'div',  
			'classes' => 'snoska snoska_blue',
			'wrapper' => true,
		),
		array(  
			'title' => __('Red content block', 'fortrader_settings'),  
			'block' => 'div',  
			'classes' => 'snoska snoska_red',
			'wrapper' => true,
		),
	);  
	$init_array['style_formats'] = json_encode( $style_formats );  
	return $init_array;  
} 
add_filter( 'tiny_mce_before_init', 'blue_red_mce_before_init_insert_formats' ); 

function addCustomEditorStyle() {
    add_editor_style( 'css/custom-editor-style.css' );
}
add_action( 'init', 'addCustomEditorStyle' );


function headerLangSwitcher(){
	$outStr = '<div class="language_dropdown">';
	$currentLang = '';
	$allLangs = '';
    $translations = pll_the_languages(array('raw'=>1));

    foreach ($translations as $i => $translationItem) {
        if ($translationItem['no_translation']) {
            unset($translations[$i]);
        }
    }
	
	if( count($translations) > 1 ){
		foreach( $translations as $translation ){
			if( $translation['current_lang'] == 1 ){
				$currentLang = '<div class="language_dropdown_label fx_language-drop-label"><img src="/uploads/country/flags/shiny/16/'. strtoupper( str_replace('.png', '', substr($translation['flag'], strrpos($translation['flag'], '/')+1) ) ) .'.png" height="16" width="16" alt="'.$translation['name'].'">'.$translation['name'].'</div>';
			}else{
				$allLangs .= '<li><a href="'.$translation['url'].'"><img src="/uploads/country/flags/shiny/16/'. strtoupper( str_replace('.png', '', substr($translation['flag'], strrpos($translation['flag'], '/')+1) ) ) .'.png" height="16" width="16" alt="'.$translation['name'].'">'.$translation['name'].'</a></li>';
			}
		}
		
		$outStr .= $currentLang;
		if( count($translations) > 1 ) $outStr .= '<ul class="language_dropdown_list">';
		$outStr .= $allLangs;
		if( count($translations) > 1 ) $outStr .= '</ul>';
		$outStr .= '</div><!-- / .languages_dropdown -->';
		return $outStr;
	}
}

add_action('embed_content_meta', function (){
	remove_action( 'embed_content_meta', 'print_embed_comments_button');
}, 0);
