
                </div>
                <!-- - - - - - - - - - - - - - End of Content - - - - - - - - - - - - - - - - -->
            </div>
        </div><!-- / .container -->
        <!-- - - - - - - - - - - - - - End of Container - - - - - - - - - - - - - - - - -->
        <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->

        <footer class="fx_footer">
            <div class="fx_container clearfix">
                <div class="fx_footer-social-box clearfix">
                    <div>
                        <div class="fx_footer-title"><?php _e("Join us on social networks", 'ForTraderMaster'); ?></div>
                        <div class="fx_footer-socials clearfix">
                            <div>
                                <a href="https://plus.google.com/+fortraderru1" target="_blank" class="fx_social-button btn_gplus">
                                    <i class="fx_social-icon"></i>
                                    Google+
                                </a>
                            </div>
                            <div>
                                <a href="https://www.facebook.com/fortrader" target="_blank" class="fx_social-button btn_facebook">
                                    <i class="fx_social-icon"></i>
                                    facebook
                                </a>
                            </div>
                            <div>
                                <a href="https://twitter.com/fortrader" target="_blank" class="fx_social-button btn_twitter">
                                    <i class="fx_social-icon"></i>
                                    twitter
                                </a>
                            </div>
                            <div>
                                <a href="https://www.youtube.com/channel/UCta2JmRlsjWsQead2-CJk5g" target="_blank" class="fx_social-button btn_youtube">
                                    <i class="fx_social-icon"></i>
                                    youtube
                                </a>
                            </div>
                            <div>
                                <a href="https://t.me/fortraderorg" target="_blank" class="fx_social-button btn_telegram">
                                    <i class="fx_social-icon"></i>
                                    telegram
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fx_footer-subscribe-box clearfix">
                    <div>
                        <div class="fx_footer-title"><?php _e("Subscribe to our newsletter", 'ForTraderMaster'); ?></div>
                        <form class="fx_footer-subscribe fx_subscribe-ru" id="subscribeFormFooter">
                            <input type="email" name="email" class='user-email' placeholder="<?php _e("Your email", 'ForTraderMaster'); ?>">
                            <input type="submit" value="<?php _e("Subscribe", 'ForTraderMaster'); ?>">

                            <span class="form-error valid-user-email"></span>
                            <span class="form-error valid-all-form"></span>
                        </form>
                    </div>
                </div>
            </div>

            <div class="fx_footer-copyright text_uppercase">
                <div class="fx_container clearfix position_relative">
                    © 2008–<?php echo date('Y')?>. <a href="<?php echo get_home_url();?>">ForTrader.org</a> <?php _e("All rights reserved.", 'ForTraderMaster'); ?>
                    <div class="fx_footer-metrics">

                        <!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter17779042 = new Ya.Metrika({ id:17779042, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/17779042" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->

                        <a href="#" class="fx_metrics-btn"><img src="<?php echo removeProtocol(get_bloginfo('template_url')); ?>/images/ban_list_img4.jpg" alt=""></a>

                        <!--LiveInternet counter--><script type="text/javascript">document.write("<a class='fx_metrics-btn' href='//www.liveinternet.ru/stat/fortrader.ru/' "+"target=_blank><img src='//counter.yadro.ru/hit?t14.5;r"+escape(document.referrer)+((typeof(screen)=="undefined")?"":";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+";h"+escape(document.title.substring(0,80))+";"+Math.random()+"' alt='' title='LiveInternet: показано число просмотров за 24"+" часа, посетителей за 24 часа и за сегодня' "+"border='0' width='88' height='31'><\/a>")</script><!--/LiveInternet-->
                        
                    </div>
                </div>
            </div>
        </footer>


        <div id="go_up"></div>

        <?php if( !is_user_logged_in() ) get_template_part( 'templates/popup-login' ); ?>

        <!-- - - - - - - - - - - - - - End of Footer - - - - - - - - - - - - - - - - -->

        <!-- Include Libs & Plugins
        ============================================ -->
        <?php wp_enqueue_script('jquery.arcticmodal-0.3.min', get_bloginfo("stylesheet_directory") . '/plagins/arcticmodal/jquery.arcticmodal-0.3.min.js', array('jquery'), null); ?>
        <?php wp_enqueue_script('jquery-migrate', false, array(), null, true); ?>
        <?php wp_enqueue_script('jquery.easing.1.3.js', get_bloginfo("stylesheet_directory") . '/js/jquery.easing.1.3.js', array('jquery'), null, true); ?>
        <?php wp_enqueue_script('modernizr.js', get_bloginfo("stylesheet_directory") . '/js/modernizr.js', array('jquery'), null, true); ?>
        <?php wp_enqueue_script('owl.carousel.js', get_bloginfo("stylesheet_directory") . '/plagins/owlCarousel/js/owl.carousel.js', array('jquery'), null, true); ?>
        <?php wp_enqueue_script('jquery.formstyler.js', get_bloginfo("stylesheet_directory") . '/plagins/jQueryFormStyler/jquery.formstyler.js', array('jquery'), null, true); ?>
        <?php wp_enqueue_script('jquery.lazyload.js', get_bloginfo("stylesheet_directory") . '/js/jquery.lazyload.js', array('jquery'), null, true); ?>

        <script>
        jQuery(function() {
            jQuery("div.left_part img.lazy").lazyload({
                effect : "fadeIn"
            }).removeClass('lazy');
            jQuery("aside.sidebar img.lazy").lazyload({
                effect : "fadeIn"
            }).removeClass('lazy');
        });



<?php if( $nowPost ){  ?>

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return null;
}

function sendStats(){
	var prev_post_id = getCookie('prev_post_id');
	createCookie('prev_post_id', current_post_id, '');
	sendPost( 'prev_post_id='+prev_post_id+'&current_post_id='+current_post_id+'&referrer='+encodeURIComponent(document.referrer), '/services/admin/postsStats/ajaxAddTrafficStats' );
}
sendStats();


<?php } ?>
        
function plusClick(args){
	anyLikesClick();
	sendUpdateSm();
}

function anyLikesClick(){
	ga('send', 'event', 'socialClick', 'anyLikes');
	sendUpdateSm();
}
function shareVkFacebookClick(){
	ga('send', 'event', 'socialClick', 'shareVkFacebook');
	sendUpdateSm();
}
function popupClickLikes(){
	ga('send', 'event', 'socialClick', 'popupClick');
	sendUpdateSm();
}
function popupClickNews( url ){
	ga('send', 'event', 'contentClick', 'popupNewsClick', url, {
     'transport': 'beacon',
     'hitCallback': function(){document.location = url;}
   });
}
function sendUpdateSm(){
	<?php if( $nowPost ){ ?>sendPost( 'id='+current_post_id, '/services/admin/postsStats/ajaxSetSmMarker' );<?php }?>
}



(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
ga('create', 'UA-34000109-1', 'auto');
ga('send', 'pageview');
			


        </script>


        <!-- Theme files
        ============================================ -->
        <?php wp_enqueue_script('script.fx-core.js', get_bloginfo("stylesheet_directory") . '/js/script.fx-core.js', array('jquery'), null, true); ?>
        <?php wp_enqueue_script('script.fx-plugins.js', get_bloginfo("stylesheet_directory") . '/js/script.fx-plugins.js', array('jquery'), null, true); ?>



        <?php wp_footer();?>
    </body>
</html>