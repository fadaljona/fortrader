<!DOCTYPE html>
<html lang="<?=$language?>" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
    <head>
        <!-- Basic page needs
        ============================================ -->
		<title><?=$title?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description" content="<?=$description?>">
		<meta name="keywords" content="<?=$keywords?>">
		<meta name="author" content="">
		
		
		<link rel="publisher" href="<?=Yii::app()->params['googlePlus']?>"/>
		<meta property="og:locale" content="<?php 
			if($language == 'ru'){
				echo 'ru_RU';
			}elseif($language == 'en_us'){
				echo 'en_US';
			}else{
				echo $language;
			}
		?>" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="<?=$title?>" />
		<meta property="og:description" content="<?=$description?>" />
		<meta property="og:url" content="<?=Yii::App()->request->getHostInfo() . Yii::App()->request->getUrl()?>" />
		<meta property="og:site_name" content="<?=$siteName?>" />
		<meta property="article:publisher" content="<?=Yii::app()->params['facebook']?>" />
		<meta property="article:section" content="<?=$ogSection?>" />

		<meta property="og:image" content="<?=$ogImage?>" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:description" content="<?=$description?>"/>
		<meta name="twitter:title" content="<?=$title?>"/>
		<meta name="twitter:site" content="@<?=Yii::app()->params['twitter']?>"/>
		<meta name="twitter:image" content="<?=$ogImage?>"/>
		<meta name="twitter:creator" content="@<?=Yii::app()->params['twitter']?>"/>
		
		
		
		
        <!-- Mobile specific metas
        ============================================ -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Favicon
        ============================================ -->
<?php 
	if($this->beginCache('wp_get_header_part1'.Yii::app()->language . CommonLib::sslRequestMarker(), array('duration'=>60*60*24))) {
		CommonLib::loadWp();
		
?>		
		<link href="<?php bloginfo('template_url'); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" >
        
        <!-- Theme CSS
        ============================================ -->
		<?php /*<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="all" />*/?>
		<?php wp_enqueue_style( 'style.css', get_bloginfo('stylesheet_url'), array(), null );?>
        <!-- Media Queries CSS
        ============================================ -->
        <?/*<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/responsive.css">*/?>
		<?php wp_enqueue_style( 'responsive.css', get_bloginfo('template_url').'/css/responsive.css', array(), null );?>
		
		 <!-- Libs CSS
        ============================================ -->
        <?/*<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plagins/owlCarousel/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plagins/liMarquee/css/liMarquee.css">*/?>
		<?php wp_enqueue_style( 'owl.carousel.css', get_bloginfo('template_url').'/plagins/owlCarousel/css/owl.carousel.css', array(), null );?>
		<?php wp_enqueue_style( 'liMarquee.css', get_bloginfo('template_url').'/plagins/liMarquee/css/liMarquee.css', array(), null );?>
		
        <!-- Old IE 
        ============================================ -->
        <!--[if lt IE 8]>
        <div style=' clear: both; text-align:center; position: relative;'>
          <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
          </a>
        </div>
        <![endif]-->
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_url'); ?>/css/ie.css">
        <![endif]-->
		<script>
function sendPost( params, url ){
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send( params );
}
			var yiiBaseURL = "<?=$baseUrl?>";
			var yiiLanguage = "<?=$language?>";
			var yiiDebug = <?=CommonLib::boolToStr( YII_DEBUG )?>;
			var wAWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		</script>	
		<style>a{color:#454242;}a:hover{color:#146eb3;}</style>
<?php 
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	wp_head(); 
?>
	<meta name="alpari_partner" content="1209762" />

	
	
<?php if($_SERVER['HTTP_HOST'] == 'fortrader.org' && isset($_SERVER['HTTPS']) && ( $_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == '1' ) ){?>
<link rel="manifest" href="/manifest.json">
  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
  <script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
      appId: "8d23dc04-4397-4214-af27-8a03ef6e4ace",
      autoRegister: true,
      notifyButton: {
          enable: true, 
		  text: {
            'tip.state.unsubscribed': '<?php _e("Subscribe to notifications", 'ForTraderMaster'); ?>',
            'tip.state.subscribed': "<?php _e("You're subscribed to notifications", 'ForTraderMaster'); ?>",
            'tip.state.blocked': "<?php _e("You've blocked notifications", 'ForTraderMaster'); ?>",
            'message.prenotify': '<?php _e("Click to subscribe to notifications", 'ForTraderMaster'); ?>',
            'message.action.subscribed': "<?php _e("Thanks for subscribing!", 'ForTraderMaster'); ?>",
            'message.action.resubscribed': "<?php _e("You're subscribed to notifications", 'ForTraderMaster'); ?>",
            'message.action.unsubscribed': "<?php _e("You won't receive notifications again", 'ForTraderMaster'); ?>",
            'dialog.main.title': '<?php _e("Manage Site Notifications", 'ForTraderMaster'); ?>',
            'dialog.main.button.subscribe': '<?php _e("SUBSCRIBE", 'ForTraderMaster'); ?>',
            'dialog.main.button.unsubscribe': '<?php _e("UNSUBSCRIBE", 'ForTraderMaster'); ?>',
            'dialog.blocked.title': '<?php _e("Unblock Notifications", 'ForTraderMaster'); ?>',
            'dialog.blocked.message': "<?php _e("Follow these instructions to allow notifications:", 'ForTraderMaster'); ?>"
        },
		displayPredicate: function() {
            return OneSignal.isPushNotificationsEnabled()
                .then(function(isPushEnabled) {
                    return !isPushEnabled;
                });
        },
		offset: {
			bottom: '30px',
			right: '80px'
		},
      },
	  safari_web_id: 'web.onesignal.auto.2eb33d58-6740-481f-b49f-67bae0b73a06'
    }]);
  </script>
<?php }?>

	
    </head>
<?php $this->endCache(); } ?> 
    <body <?=$bodyClass?>>
	
	<!-- Facebook Pixel Code-->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','//connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1407368206230683');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none" alt="Facebook Pixel Code"
		src="https://www.facebook.com/tr?id=1407368206230683&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
		
        <!-- - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->
        <div class="container white_container">
            <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->
            <header id="header" class="clearfix" itemscope itemtype="http://schema.org/WPHeader">       
				<!-- - - - - - - - - - - - - - Enter Box - - - - - - - - - - - - - - - - -->
                <div class="enter_box alignright clearfix">
					<?php 
						if ( Yii::App()->user->isGuest ){
					?>   
						<?php echo $langSwitcher; ?>
						
						<a href="<?=Yii::App()->createUrl( 'default/login')?>" id="topLogin" class="arcticmodal" data-modal="#logIn" rel="nofollow"><?=Yii::t( '*', 'Enter' )?></a>
						<a href="<?=Yii::App()->createUrl( 'default/login')?>" class="arcticmodal" data-modal="#registration" rel="nofollow"><?=Yii::t( '*', 'Registration' )?></a>
					<?php } else {
						$model = Yii::App()->user->getModel();	
						?>  
						<span><?=Yii::t( '*', 'Hello' )?>, <a href="<?php echo $model->singleURL;?>"><strong><?=CHtml::encode($model->wpShowName)?></strong></a></span>
						
						<?php echo $langSwitcher; ?>
						
						<a href="<?=Yii::App()->createUrl( 'default/logout', array('returnURL' => Yii::App()->request->getHostInfo() . Yii::App()->request->getUrl()) )?>" rel="nofollow"><?=Yii::t( '*', 'Logout' )?></a>
					<?php }?> 
                </div>
                <!-- - - - - - - - - - - - - - End of Enter Box - - - - - - - - - - - - - - - - -->
<?php 
	if($this->beginCache('wp_get_header_part2'.Yii::app()->language . CommonLib::sslRequestMarker(), array('duration'=>60*60*24))) { 
		CommonLib::loadWp();
?>
                <!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->
                <div class="logo">
					<a href="<?php echo get_lang_url_prefix();?>/" title="Home Page">
						<img src="<?php echo removeProtocol( get_bloginfo('template_url') ); ?>/images/logon.png" alt="">
					</a>
                </div>
                <!-- - - - - - - - - - - - - - End of Logo - - - - - - - - - - - - - - - - -->                
				<div class="header_top_btn_box">
					<a href="<?php echo get_lang_url_prefix();?>/informers" class="header_top_button"><?php _e("For webmaster", 'ForTraderMaster'); ?></a>
				</div>
            </header>
            <!-- - - - - - - - - - - - - - End of Header - - - - - - - - - - - - - - - - -->
            
            <?php get_template_part_by_locale( 'templates/header-banner' ); ?>

             <?php get_template_part( 'templates/header-nav' ); ?>
            <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->
            <div id="content" class="clearfix">
<?php $this->endCache(); } ?>  