<?php

/* @var $ExPolls ExPolls */
/* @var $wpdb wpdb */
$per_page = 5;
$comments_per_page = 10;
$poll = $ExPolls->getPollByIdOrSlug(get_query_var('expoll_slug'));
$poll->answers = $ExPolls->getPollAnswers($poll->ID);
$poll->answers_count = count($poll->answers);

$total_polls_count = $ExPolls->getPollsCount();
$pages = get_pagination($total_polls_count, get_query_var('expoll_page'), $per_page);
$polls = $ExPolls->getPolls($pages['start'], $per_page, ' AND `ID`!=' . $poll->ID);

$total_comments_count = $ExPolls->getPollComments(0, 10,
                ' AND cm.meta_key="expoll_id" AND cm.meta_value=' . (int) $poll->ID,
                ' LEFT JOIN ' . $wpdb->commentmeta . ' AS cm ON c.comment_ID = cm.comment_id',
                true
);
$comments_pages = get_pagination($total_comments_count, $_REQUEST['comments_page'], $comments_per_page);

$poll_comments = $ExPolls->getPollComments($comments_pages['start'], $comments_per_page,
                ' AND cm.meta_key="expoll_id" AND cm.meta_value=' . (int) $poll->ID,
                ' LEFT JOIN ' . $wpdb->commentmeta . ' AS cm ON c.comment_ID = cm.comment_id'
);

$polls_count = count($polls);
$voted = $ExPolls->checkVoted($poll->ID);

?>
<h2><?php echo $poll->title ?></h2>
<p><?php echo $poll->quest ?></p>



<div class="opr expoll_container">
	<input name="expoll-id" type="hidden" value="<?php echo $poll->ID ?>" />
	<?php if ($poll->state == 1 && !$voted): ?>
		<div class="interview_box section_offset">
			<form>
				<input name="expoll-id" type="hidden" value="<?php echo $poll->ID ?>" />
				<?php 
					for($i = 0; $i < $poll->answers_count; $i++){ ?>
						<style scoped>
							.interview.before-line<?php echo $poll->answers[$i]->ID ?>::before{
								width:<?php echo (int) $poll->answers[$i]->percent ?>%;
							}
						</style>
						<?php
						if (!$poll->multi){ ?>
							<div class="interview before-line<?php echo $poll->answers[$i]->ID ?>">
								<input type="radio" id="interview<?php echo $poll->answers[$i]->ID ?>" name="expoll-answer" value="<?php echo $poll->answers[$i]->ID ?>">
								<label for="interview<?php echo $poll->answers[$i]->ID ?>" class="square_input"><i></i><?php echo $poll->answers[$i]->name ?></label>
							</div>
					<?php }else{ ?>
							<div class="interview before-line<?php echo $poll->answers[$i]->ID ?>">
								<input type="checkbox" id="interview<?php echo $poll->answers[$i]->ID ?>" name="expoll-answer" value="<?php echo $poll->answers[$i]->ID ?>">
								<label for="interview<?php echo $poll->answers[$i]->ID ?>" class="square_input"><i></i><?php echo $poll->answers[$i]->name ?></label>
							</div>
					<?php }; ?>
				<?php }; ?> 
				<div class="clearfix">
					<button class="reply_btn expoll_vote"><?php _e("Vote!", 'ForTraderMaster'); ?></button>
				</div>
			</form>
		</div>
	<?php else: ?>
		<?php for ($i = 0; $i < $poll->answers_count; $i++): ?>
			<div class="interview_box section_offset">
				<form>
					<input name="expoll-id" type="hidden" value="<?php echo $poll->ID ?>" />
					<?php 
						for($i = 0; $i < $poll->answers_count; $i++){ ?>
							<style scoped>
								.interview.before-line<?php echo $poll->answers[$i]->ID ?>::before{
									width:<?php echo (int) $poll->answers[$i]->percent ?>%;
								}
							</style>
							<?php if (!$poll->multi){ ?>
								<div class="interview before-line<?php echo $poll->answers[$i]->ID ?>">
									<label for="interview<?php echo $poll->answers[$i]->ID ?>" class="no-poll"><?php echo $poll->answers[$i]->name ?> <?php echo (int) $poll->answers[$i]->percent ?>% (<?php echo $poll->answers[$i]->vote_count ?>)</label>
								</div>
						<?php }else{ ?>
								<div class="interview before-line<?php echo $poll->answers[$i]->ID ?>">
									<label for="interview<?php echo $poll->answers[$i]->ID ?>" class="no-poll"><?php echo $poll->answers[$i]->name ?> <?php echo (int) $poll->answers[$i]->percent ?>% (<?php echo $poll->answers[$i]->vote_count ?>)</label>
								</div>
						<?php }; ?>
					<?php }; ?> 
				</form>
			</div>
		<?php endfor; ?>
	<?php endif; ?>
</div>

<div class="dotted"></div>














<input type="hidden" id="expoll_add_comment" name="expoll_add_comment" value="<?php echo $poll->ID ?>" />
<?php

$deCommentsTemplatesPath = $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/decomments/templates/decomments/';

global $wp_query;

if ( isset( $ajax ) ) {
	$post_id = $ajax_post_id;
} elseif ( isset( $jScroll_post_id ) ) {
	// TODO: replace global post
	global $post;
	global $wp_query;

	$post_id = $jScroll_post_id;
	$post    = get_post( $post_id );
	query_posts( array( 'p' => $post_id ) );
//	setup_postdata( get_post( $post_id ) );
	echo '<div class="decom_dop_bloc">';
	$_SERVER['REQUEST_URI'] = get_permalink( $post_id );
} else {
	$post    = get_post();
	$post_id = $post->ID;
	echo '<div class="decom_dop_bloc">';
}
$decom_settings = decom_get_options();
$settings       = $decom_settings;
if ( isset( $decom_settings['show_avatars'] ) && intval( $decom_settings['show_avatars'] ) ) {
	$da_position = $decom_settings['display_avatars_right'] ? ' decomments-avatar-right' : '';
} else {
	$da_position = ' no-avatar';
}
$current_user_id = DECOM_Loader_MVC::getComponentModel( 'comments', 'user' )->getCurrentUserId();
$model_comments = DECOM_Loader_MVC::getComponentModel( 'comments', 'comments' );
$model_votes    = DECOM_Loader_MVC::getComponentModel( 'comments', 'comments-votes' );
//print_r( $decom_settings );
$display_round_avatars = isset( $decom_settings['display_round_avatars'] ) ? intval( $decom_settings['decom_display_round_avatars'] ) : 0;
$comments_per_page     = - 1;
$is_comments_open      = comments_open();
$comments_per_page     = ( isset( $decom_settings['page_comments'] ) && intval( $decom_settings['page_comments'] ) && isset( $decom_settings['comments_per_page'] ) ) ? intval( $decom_settings['comments_per_page'] ) : 0;
$all_comments          = $model_comments->getComments( $post_id );
if ( $all_comments ) {
	$all_comments_compact = $model_votes->filter_array_comments( $all_comments );
	$user_voice           = $model_votes->user_voice_like( $all_comments_compact );
} else {
	$user_voice = array();
}
$max_comments_votes_1  = array();
$max_comments_votes_2  = array();
$comments_branch_1     = array();
$comments_branch_2     = array();
$comments_branch_merge = array();
$user_sort             = getUserSort( $current_user_id );
if ( $decom_settings['show_two_comments_highest_ranking_top_list'] ) {
	$max_comments_votes = $model_comments->getPostMaxCommentKarma( $post_id, $decom_settings['best_comment_min_likes_count'], $user_sort );
	if ( array_key_exists( 0, $max_comments_votes ) ) {
		$comments_branch_1    = $model_comments->getCommentsBranchByParentId( $post_id, $max_comments_votes[0]->comment_ID );
		$max_comments_votes_1 = $model_comments->getCommentsByIds( $comments_branch_1 );
	}
	if ( array_key_exists( 1, $max_comments_votes ) ) {
		$comments_branch_2    = $model_comments->getCommentsBranchByParentId( $post_id, $max_comments_votes[1]->comment_ID );
		$max_comments_votes_2 = $model_comments->getCommentsByIds( $comments_branch_2 );
	}
	if ( is_array( $comments_branch_1 ) && is_array( $comments_branch_2 ) ) {

		$comments_branch_merge = array_merge( $comments_branch_1, $comments_branch_2 );
	} else {
		if ( is_array( $comments_branch_1 ) ) {
			$comments_branch_merge = $comments_branch_1;
		}
	}
}
$comments = $poll_comments;// = $model_comments->getComments( $post_id, $comments_branch_merge );
if ( $comments ) {
	$pages_count = get_comment_pages_count( $comments, $comments_per_page );
} else {
	$pages_count = 0;
}
if ( isset( $ajaxCurrentPage ) && $ajaxCurrentPage ) {
	if ( $comment_paginate_action == 'next' && $pages_count > $ajaxCurrentPage ) {
		$current_comments_page = $ajaxCurrentPage + 1;
	} elseif ( $comment_paginate_action == 'previous' && $pages_count > 1 ) {
		$current_comments_page = $ajaxCurrentPage - 1;
	} elseif ( $comment_paginate_action == 'end' ) {
		$current_comments_page = $pages_count;
	} elseif ( $comment_paginate_action == 'beginning' ) {
		$current_comments_page = 1;
	}
} elseif ( isset( $wp_query->query_vars['cpage'] ) ) {
	$page                  = $wp_query->query_vars['cpage'];
	$current_comments_page = $wp_query->query_vars['cpage'];
} else {
	$current_comments_page = 1;
}
$votes            = $model_votes->choiceVoiceAllComment( $comments );
$height_auth_form = 200;
$social_links     = getSocialLinks( $height_auth_form );
if ( isset( $ajax ) ) {
	wp_list_comments( array(
		'callback'          => 'decom_render_comment',
		'end-callback'      => 'decom_end_comment',
		'style'             => 'div',
		'walker'            => DECOM_Loader_MVC::getComponentClass( 'comments', 'comments-walker' ),
		'settings'          => $settings,
		'votes'             => $votes,
		'user_voice'        => $user_voice,
		'reverse_top_level' => $user_sort,
		'ajax_post_id'      => $ajax_post_id,
		'per_page'          => $comments_per_page,
		'page'              => $ajax_page_num
	), $comments );
	die();
}
if ( post_password_required() ) {
	return;
}
if ( ! $is_comments_open ) {
	if ( ! is_page() ) {
		?>
		<br /><br />
		<p class="nocomments"><?php esc_html_e( 'Comments are closed.', DECOM_LANG_DOMAIN ); ?></p>
		<?php
	}

	return;
}
?>
<a name="comments"></a>
<div id="decomments-comment-section" class="decomments-comment-section decomments-block-form-wrapper"
     data-modal-alert='<?php include $deCommentsTemplatesPath.'parts/modal_alerts.php'; ?>'
     data-modal-addimage='<?php include $deCommentsTemplatesPath.'parts/modal_addimage.php'; ?>'
     data-modal-quote='<?php include $deCommentsTemplatesPath.'parts/modal_quote.php'; ?>'
     data-modal-preview='<?php include $deCommentsTemplatesPath.'parts/modal_preview.php'; ?>'
     data-modal-sub='<?php include $deCommentsTemplatesPath.'parts/modal_subscribe.php'; ?>'
	>
	<script type="text/javascript">
		var decom_post_id = "<?php echo $post_id; ?>";
		var decom_user_id = "<?php echo $current_user_id; ?>";
		var decom_plugin_url = "<?php echo DECOM_COMPONENTS_URL . '/comments/assets'; ?>";
		var decom_position_form = "<?php echo $settings['comment_form_up'] ? 1 : 0; ?>";
		var decom_comment_single_translate = "<?php echo ' ' . _n( 'comment', 'comments', 1, DECOM_LANG_DOMAIN ); ?>";
		var decom_comment_twice_translate = "<?php echo ' ' . _n( 'comment', 'comments', 2, DECOM_LANG_DOMAIN ); ?>";
		var decom_comment_plural_translate = "<?php echo ' ' . _n( 'comment', 'comments', 5, DECOM_LANG_DOMAIN ); ?>";
	</script>
	<?php wp_comment_form_unfiltered_html_nonce(); ?>
	<?php add_thickbox(); ?>

	<?php
	$social_iсon   = '';
	$login_success = '';
	if ( is_user_logged_in() ) {
		$current_user   = wp_get_current_user();
		$active_plugins = $decom_settings['active_plugins'];
		if ( is_array( $active_plugins ) && in_array( 'wordpress-social-login/wp-social-login.php', $active_plugins ) ) {
			$social_iсon = $model_comments->selectSocial( $current_user->ID ) ? $model_comments->selectSocial( $current_user->ID ) : '';
		}

		$display_round_avatars_class = $display_round_avatars ? ' decomments-round-avatar' : '';
		$login_success .= '<figure class="decomments-user-thumb' . $display_round_avatars_class . '">';
		$login_success .= get_avatar( $current_user->ID, 80, '', $current_user->display_name );
		$login_success .= '</figure>';


	} else {
		$login_form = '';
		ob_start();
		?>
		<div class="decomments-enter-row">

			<form id="decomments-enterform" class="decomments-enterform" action="#" method="post">

				<fieldset>

					<div class="de-form-field">
						<label for="decom-name-author"><?php _e( 'Name', DECOM_LANG_DOMAIN ); ?><em>*</em></label>
						<input id="decom-name-author" type="text" value="<?php echo $_COOKIE['decommentsa']; ?>" />
					</div>

					<div class="de-form-field">
						<label for="decom-mail-author"><?php _e( 'E-mail', DECOM_LANG_DOMAIN ); ?><em>*</em></label>

						<input id="decom-mail-author" type="text" value="<?php echo $_COOKIE['decommentse']; ?>" />
					</div>

					<?php if ( $settings['enable_field_website'] == 1 ) { ?>
						<div class="de-form-field">
							<label for="decom-site-author"><?php _e( 'Website', DECOM_LANG_DOMAIN ); ?></label>

							<input id="decom-site-author" type="text" value="<?php echo $_COOKIE['decommentsu']; ?>" />
						</div>
					<?php } ?>

				</fieldset>

				<p class="decomments-enterform-message decomments-error-message">
					<i class="decomments-icon-warning"></i>
					<?php esc_html_e( 'E-mail is already registered on the site. Please use the ', DECOM_LANG_DOMAIN ) ?>
					<a href="#" id="decomments-show-loginform"><?php esc_html_e( 'login form', DECOM_LANG_DOMAIN ) ?></a>
					<?php esc_html_e( ' or ', DECOM_LANG_DOMAIN ) ?>
					<a href="#" id="decomments-show-enterform"><?php esc_html_e( 'enter another', DECOM_LANG_DOMAIN ) ?></a>.
				</p>

			</form>

			<form id="decomments-loginform" class="decomments-loginform" action="<?php echo get_option( 'home' ) . '/wp-login.php' ?>" method="post">

				<p class="decomments-loginform-message decomments-error-message">
					<i class="decomments-icon-warning"></i> <?php esc_html_e( 'You entered an incorrect username or password', DECOM_LANG_DOMAIN ) ?>
				</p>

				<fieldset>

					<div class="de-form-field">
						<input type="text" name="log" id="log" value="" size="20" />
						<label for="log"><?php esc_html_e( 'Name', DECOM_LANG_DOMAIN ) ?>:</label>
					</div>

					<div class="de-form-field">
						<input type="password" name="pwd" id="pwd" size="20" />
						<label for="pwd"><?php echo esc_html_e( 'Password', DECOM_LANG_DOMAIN ) ?>:</label>
					</div>

				</fieldset>

				<button type="submit" class="decomments-button decomments-button-submit"><?php esc_html_e( 'Log in', DECOM_LANG_DOMAIN ) ?></button>
				<input id="submit-form" type="hidden" name="submit" />

				<?php do_action( 'login_form' ); ?>
				<input type="hidden" name="redirect_to" value="" />
				<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>

			</form>

		</div>

		<?php

		$login_form = ob_get_contents();
		ob_end_clean();
	}

	if ( $settings['comment_form_up'] ) {
		include_once( $deCommentsTemplatesPath.'comment-form.php' );
	}

	$number        = count( $poll_comments );
	$have_comments = $number > 0 ? true : false;

	if ( $have_comments ) { ?>

		<div class="decomments-head">

			<?php
			if ( $settings['output_total_number_comments_top'] ) {

				$display = ( $number == 0 ) ? ' decomments-hide' : ' decomments-block';

				$comment_str = '<h3><i><span class="decomments-comments-number' . $display . '">' . $number . '</span><span class="decomments-comment-title">' . ' ' . _n( 'comment', 'comments', $number, DECOM_LANG_DOMAIN ) . '</span></i></h3>';
				echo $comment_str;
			}
			?>

		</div>


		<div class="decomments-comment-list<?php echo $da_position; ?>">

			<div class="loader-ball-scale lbs-remove">
				<div></div>
				<div></div>
				<div></div>
			</div>
			<?php
			if ( count( $max_comments_votes_1 ) > 0 ) {
				wp_list_comments( array(
					'callback'     => 'decom_render_comment',
					'end-callback' => 'decom_end_comment',
					'style'        => 'div',
					'walker'       => DECOM_Loader_MVC::getComponentClass( 'comments', 'comments-walker' ),
					'settings'     => $settings,
					'votes'        => $votes,
					'user_voice'   => $user_voice,
					'max'          => true
				), $max_comments_votes_1 );
			}

			if ( count( $max_comments_votes_2 ) > 0 ) {
				wp_list_comments( array(
					'callback'     => 'decom_render_comment',
					'end-callback' => 'decom_end_comment',
					'style'        => 'div',
					'walker'       => DECOM_Loader_MVC::getComponentClass( 'comments', 'comments-walker' ),
					'settings'     => $settings,
					'votes'        => $votes,
					'user_voice'   => $user_voice,
					'max'          => true
				), $max_comments_votes_2 );
			}

			wp_list_comments( array(
				'callback'          => 'decom_render_comment',
				'end-callback'      => 'decom_end_comment',
				'style'             => 'div',
				'walker'            => DECOM_Loader_MVC::getComponentClass( 'comments', 'comments-walker' ),
				'settings'          => $settings,
				'votes'             => $votes,
				'user_voice'        => $user_voice,
				'reverse_top_level' => $user_sort,
				'per_page'          => $comments_per_page,
				'page'              => $current_comments_page ? $current_comments_page : '1'
			), $comments );
			?>
		</div><!-- .commentlist -->

		<?php

		$deco_ajax_navy = $settings['deco_ajax_navy'];

		if ( $deco_ajax_navy ) {

			if ( $deco_ajax_navy == 'deco_show_more_comments_prevnext' ) { ?>
				<nav id="comment-nav-below" class="navigation decomments-navigation" role="navigation" page="<?php echo $current_comments_page; ?>" pages-count="<?php echo $pages_count; ?>">
					<?php otherPagination( $pages_count, $post_id, $current_comments_page ); ?>
				</nav>
				<?php
			} else if ( $deco_ajax_navy == 'deco_show_more_comments_lazy' && $pages_count > 1 ) { ?>
				<div class="decomments-ajax-paginate-lazy" data-cur-page="1" data-comments-perpage="<?php echo $comments_per_page; ?>" data-page-count="<?php echo $pages_count; ?>" page="2" pages-count="<?php echo $pages_count; ?>">
					<div class="decomments-paginate-loader" style="display: none;">
						<div class="loader-ball-scale">
							<div></div>
							<div></div>
							<div></div>
						</div>
					</div>
					<a href="#" class="decomments-button decomments-loader-btn"><?php esc_html_e( 'Load more', DECOM_LANG_DOMAIN ); ?>
						<i class="decomments-icon-angle-double-right"></i></a>
				</div>
				<?php
			} else if ( $deco_ajax_navy == 'deco_show_more_comments_onebutton' && $pages_count > 1 ) { ?>
				<div class="decomments-ajax-paginate" data-cur-page="1" data-comments-perpage="<?php echo $comments_per_page; ?>" data-page-count="<?php echo $pages_count; ?>" page="2" pages-count="<?php echo $pages_count; ?>">
					<div class="decomments-paginate-loader" style="display: none;">
						<div class="loader-ball-scale">
							<div></div>
							<div></div>
							<div></div>
						</div>
						<span style="position: relative;top: -11px;"><?php esc_html_e( 'Loading comments..', DECOM_LANG_DOMAIN ); ?></span>
					</div>
					<a href="#" class="decomments-button decomments-loader-btn"><?php esc_html_e( 'Load more', DECOM_LANG_DOMAIN ); ?>
						<i class="decomments-icon-angle-double-right"></i></a>
				</div>

				<?php
			}

		}


	} // have_comments()

	if ( ! $settings['comment_form_up'] ) {
		include_once( $deCommentsTemplatesPath.'comment-form.php' );
	} ?>

</div>
</div>





















































<?php //comments_template('/custom_pages/expolls/comments.php'); ?>

<?php if ($polls_count): ?>
	<h4><?php _e("Other polls", 'ForTraderMaster'); ?></h4>
	<ul class="list1 semibold">
		<?php for ($i = 0; $i < $polls_count; $i++): ?>
			<li><a href="<?php echo '/' . $ExPolls->url_prefix . '/' . $polls[$i]->slug ?>.html"><?php echo $polls[$i]->title ?></a></li>
		<?php endfor; ?>
	</ul>
<?php endif; ?>
