<?php
/* @var $ExPolls ExPolls */
$total_polls_count = $ExPolls->getPollsCount(' AND state!=2');
$pages = get_pagination($total_polls_count, get_query_var('expoll_page'), $per_page);
$polls = $ExPolls->getPolls($pages['start'], $per_page, ' AND state!=2');
$polls_count = count($polls);

$comments_per_page = 10;
$total_comments_count = $ExPolls->getPollComments(0, 10, null, null, true);
$comments_pages = get_pagination($total_comments_count, $_REQUEST['comments_page'], $comments_per_page);
$poll_comments = $ExPolls->getPollComments($comments_pages['start'], $comments_per_page,
                ' AND cm.meta_key="expoll_id"',
                ' LEFT JOIN ' . $wpdb->commentmeta . ' AS cm ON c.comment_ID = cm.comment_id'
                . ' LEFT JOIN ' . $ExPolls->table_prefix . 'polls AS pl ON cm.meta_value = pl.ID',
                false);
?>
<h2><?php the_title(); ?>. <?php _e("Voting archive", 'ForTraderMaster'); ?><i class="icon feather_icon"></i></h2>
<div class="comm2">
	<?php _e("PolllsArchiveDesc", 'ForTraderMaster'); ?>
</div>

<br />

<?php for ($i = 0; $i < $polls_count; $i++): ?>

<figure class="search_result">
	<figcaption>
		#<?php echo $polls[$i]->ID ?> <a href="<?php echo '/' . $ExPolls->url_prefix . '/' . $polls[$i]->slug ?>.html"><?php echo $polls[$i]->title ?> <span class="red_color">(<?php _e("Votes", 'ForTraderMaster'); ?> <?php echo $polls[$i]->vote_count ?>)</span></a>
		<hr>
		<p><?php echo wp_trim_words( $polls[$i]->desc);?></p>
	</figcaption>
</figure>
<?php endfor; ?>

<div class="navigation_box clearfix">
	<?php if ($pages['prev']): ?>
		<a class="prev" href="<?php echo '/' . $ExPolls->url_prefix . '/page/' . $pages['prev']['num'] . '/' ?>"><span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span></a>
	<?php endif; ?>
	<?php if ($pages['next']): ?>
		<a class="next" href="<?php echo '/' . $ExPolls->url_prefix . '/page/' . $pages['next']['num'] . '/' ?>"><span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span></a>
	<?php endif; ?>
	<div class="box1 align_center"><ul class="nav_list d_ib">
	<?php 
		$pages_polls='';
		foreach ($pages['pages'] as $i) {
			if( $i['url'] == '' ){
				$pages_polls .= '<li class="current"><span>'.$i['num'].'</span></li>';
			}else{
				$pages_polls .= '<li><a href="/'.$ExPolls->url_prefix.'/page/'.$i['num'].'/">'.$i['num'].'</a></li>';
			}
		}
		if ($pages_polls) {
			echo $pages_polls;
		}
    ?>
	</ul></div>
</div>
