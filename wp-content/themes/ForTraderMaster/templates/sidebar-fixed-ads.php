<div class="inner-fixed-blockdiv default">
<?php get_template_part( 'templates/sidebar-banner-300x600' );?>
</div>
<script>
!function( $ ) {
	if( wAWidth > 767 ){
		var sidebar = jQuery('.sidebar');
		var content = jQuery('.left_part');
		var sidebarH = sidebar.height();
		
		var fixedBlockdiv = jQuery('.inner-fixed-blockdiv');
		
		var correctContentH = setInterval(function () {
			
			var contentH = content.height();
			
			if( contentH > sidebarH + 625 ){
				
				clearInterval(correctContentH);
				
				fixedBlockdiv.css({'display':'block'});
				sidebar.css({'height':contentH+'px'});
				sidebarH = sidebar.height();
				var sidebarEnd = sidebar.offset().top + sidebarH - 625;
				var fixedBlockdivOffset = fixedBlockdiv.offset().top-10;
				
				var correctHeight = setInterval(function () {
					var contentHTmp = content.height();
					var siteWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;		
					if( siteWidth <= 767 ){
						sidebar.css({'height':'auto'});
					}else{
						if( contentH != contentHTmp ) contentH = contentHTmp;
						sidebar.css({'height':contentH+'px'});
						sidebarH = sidebar.height();
						sidebarEnd = sidebar.offset().top + sidebarH - 625;
						if( fixedBlockdiv.hasClass("default") ){
							fixedBlockdivOffset = fixedBlockdiv.offset().top-10;
						}
					}
				}, 500);


				if ( jQuery(window).scrollTop() > fixedBlockdivOffset && ( fixedBlockdiv.hasClass("default") || fixedBlockdiv.hasClass("absolute") ) && sidebarEnd > jQuery(window).scrollTop()  ){
					fixedBlockdiv.removeClass("default absolute").addClass("fixed");
				} else if( jQuery(window).scrollTop() <= fixedBlockdivOffset && ( fixedBlockdiv.hasClass("fixed") || fixedBlockdiv.hasClass("absolute") ) && sidebarEnd > jQuery(window).scrollTop() ) {
					fixedBlockdiv.removeClass("fixed absolute").addClass("default");
				}else if(  sidebarEnd <= jQuery(window).scrollTop() ) {
					fixedBlockdiv.removeClass("fixed default").addClass("absolute");
				}

				jQuery(window).scroll(function(){
					if ( jQuery(this).scrollTop() > fixedBlockdivOffset && ( fixedBlockdiv.hasClass("default") || fixedBlockdiv.hasClass("absolute") ) && sidebarEnd > jQuery(this).scrollTop()  ){
						fixedBlockdiv.removeClass("default absolute").addClass("fixed");
					} else if( jQuery(this).scrollTop() <= fixedBlockdivOffset && ( fixedBlockdiv.hasClass("fixed") || fixedBlockdiv.hasClass("absolute") ) && sidebarEnd > jQuery(this).scrollTop() ) {
						fixedBlockdiv.removeClass("fixed absolute").addClass("default");
					}else if(  sidebarEnd <= jQuery(this).scrollTop() ) {
						fixedBlockdiv.removeClass("fixed default").addClass("absolute");
					}
				});
			}
		}, 1000);
	}
}( window.jQuery );
</script>