<section class="section_offset">
	<h3 class="page_title2">
		<?php _e("Interesting tags", 'ForTraderMaster'); ?>
		<span class="sub_title"><?php _e("Choose topic", 'ForTraderMaster'); ?></span>
		<i class="icon folder_icon"></i>
	</h3>
	<hr>
		<div class="categories_box clearfix">
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/best-indicators/">Лучшие</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/pribilny-indicators-forex/">Прибыльные</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/tochnie-indikatory/">Точные</a>
				<a data-tag-page="1" data-tag-id="2590" class="categories_btn tag-load-link" href="/tag/indikatory-bez-zapazdivaniya/">Без запаздывания</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/operezhayuschie-indikatory/">Опережающие</a>
				<a data-tag-page="1" data-tag-id="11029" class="categories_btn tag-load-link" href="/tag/indikatory-vhoda-forex/">Для входа</a>
				<a data-tag-page="1" data-tag-id="11542" class="categories_btn tag-load-link" href="/tag/indikatori-vyhoda/">Для выхода</a>
				<a data-tag-page="1" data-tag-id="2073" class="categories_btn tag-load-link" href="/tag/indicators-with-alert/">С алертом</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/forecast-indicators/">Прогнозирующие</a>
				<a data-tag-page="1" data-tag-id="457" class="categories_btn tag-load-link" href="/tag/indicators-macd/">MACD</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/stochastic/">Stochastic</a>
				<a data-tag-page="1" data-tag-id="2073" class="categories_btn tag-load-link" href="/tag/rsi/">RSI</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/power-trend-indicators/">Силы тренда</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/fibo-levels/">Фибо уровни</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/parabolic/">Parabolic SAR</a>
				<a data-tag-page="1" data-tag-id="395" class="categories_btn tag-load-link" href="/tag/pivot-point-indicators/">Pivot Point</a>
				<a data-tag-page="1" data-tag-id="10893" class="categories_btn tag-load-link" href="/tag/zigzag-indicators/">ZigZag</a>
				<a data-tag-page="1" data-tag-id="719" class="categories_btn tag-load-link" href="/tag/ichimoku-indicators/">Ишимоку</a>
				<a data-tag-page="1" data-tag-id="627" class="categories_btn tag-load-link" href="/tag/indicators-cci/">CCI</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/news-indicators/">Для новостей</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/elliot-waves-indicators/">Эллиотта</a>
				<a data-tag-page="1" data-tag-id="261" class="categories_btn tag-load-link" href="/tag/price-action-indicators/">Price Action</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/skolzyashhie-srednie/">Скользящие средние</a>
				<a data-tag-page="1" class="categories_btn tag-load-link" href="/tag/indicator-trading-session/">Сессий</a>
				<a data-tag-page="1" data-tag-id="2425" class="categories_btn tag-load-link" href="/tag/indicators-2015/">2015</a>
				<a data-tag-page="1" data-tag-id="2648" class="categories_btn tag-load-link" href="/tag/indicators-2014/">2014</a>
				<a data-tag-page="1" data-tag-id="330" class="categories_btn tag-load-link" href="/tag/indicators-mt4/">Для MT4</a>
				<a data-tag-page="1" data-tag-id="2630" class="categories_btn tag-load-link" href="/tag/indicators-mt5/">Для MT5</a>
			</div>
		<hr>
	<!-- - - - - - - - - - - - - - Post Box - - - - - - - - - - - - - - - - -->
	<div class="post_box interesting-tags-wrapper">

	</div><!--  / .post_box -->
	<a data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" class="load_btn tag-load-link chench_btn" href="#"></a>
	<!-- - - - - - - - - - - - - - End of Post Box - - - - - - - - - - - - - - - - -->
</section>
