<?php
	$catId='14230';//через запятую несколько категорий
?>
<section class="turn_box">
	<div class="clearfix position-relative">
		<h3 class="page_title2 alignleft"><?php echo get_cat_name($catId); ?></h3>
		<!-- - - - - - - - - - - - - - Nav Buttons - - - - - - - - - - - - - - - - -->
		<div class="nav_buttons alignright">
			<a href="#" class="turn_btn"><span class="tooltip"><?php _e("Minimize", 'ForTraderMaster'); ?></span></a>
			<a href="#" class="prev_btn"><span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span></a>
			<a href="#" class="next_btn"><span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span></a>
		</div>
		<!-- - - - - - - - - - - - - - End of Nav Buttons - - - - - - - - - - - - - - - - -->
	</div><!-- / .clearfix -->
	<hr>
	<!-- - - - - - - - - - - - - - Turn Content - - - - - - - - - - - - - - - - -->
	<div class="turn_content">
		<div class="clearfix">
			<div class="beginner_right">
			<div class="carousel-wrapper">
				<!-- - - - - - - - - - - - - - Post Slider - - - - - - - - - - - - - - - - -->
				<div class="owl-carousel full_width" data-currentitem="0" data-maximumitem="1" data-type="beginer" data-offset="10">
				<?php
					$r = new WP_Query( array(
						'post_status'  => 'publish',
						'post_type' => 'post',
						'orderby' => 'date',
						'order'   => 'DESC',
						'posts_per_page' => 1,
						'cat' => $catId,
					) );
				?>
				<?php
					$i=0;
					$excludePostArr = array();
					if ($r->have_posts()) :
					while ( $r->have_posts() ) : $r->the_post(); ?>
						<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
							<!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
							<figure class="post big">
								<figcaption>
									<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
								</figcaption>
								<a href="<?php the_permalink();?>">
									<?php renderImg( p75GetThumbnail($post->ID, 345, 200, ""), 345, 200, get_the_title(), 1 );?>
								</a>
								<div class="post_info clearfix">
									<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d M, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
									<span><?php echo get_post_meta ($post->ID,'views',true); ?></span>
								</div>
								<hr>
							</figure>
							<!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
						</div>
				<?php 
						$i++;
					$excludePostArr[] = $post->ID;
					endwhile;
					wp_reset_postdata();
					endif;
				?>
				</div>
			</div>
				<!-- - - - - - - - - - - - - - End of Post Slider - - - - - - - - - - - - - - - - -->
				<!-- - - - - - - - - - - - - - Post Small - - - - - - - - - - - - - - - - -->
				
				
				<?php
					$r = new WP_Query( array(
						'post_status'  => 'publish',
						'post_type' => 'post',
						'orderby' => 'date',
						'order'   => 'DESC',
						'posts_per_page' => 2,
						'cat' => $catId,
						'post__not_in' => $excludePostArr,
					) );
				?>
				<?php
					if ($r->have_posts()) :
					while ( $r->have_posts() ) : $r->the_post(); ?>
						<figure class="post_small">
							<a href="<?php the_permalink();?>">
								<?php renderImg( p75GetThumbnail($post->ID, 127, 70, ""), 127, 70, get_the_title(), 1 );?>
							</a>
							<figcaption>
								<a href="<?php the_permalink();?>"><?php the_title();?></a>
								<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d M, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
							</figcaption>
						</figure>
				<?php 
					$excludePostArr[] = $post->ID;
					endwhile;
					wp_reset_postdata();
					endif;
				?>
				<!-- - - - - - - - - - - - - - End of Post Small - - - - - - - - - - - - - - - - -->
			</div><!-- / .beginner_right -->
			<div class="beginner_left">
				<h4 class="title1 red_border"><?php _e("The most important", 'ForTraderMaster'); ?> <i class="icon student_icon"></i></h4>
					<!-- - - - - - - - - - - - - - List 2 - - - - - - - - - - - - - - - - -->
					<ul class="list2">
					<?php
						$r = new WP_Query( array(
							'post_status'  => 'publish',
							'post_type' => 'post',
							'orderby' => 'date',
							'order'   => 'DESC',
							'posts_per_page' => 7,
							'cat' => $catId,
							'post__not_in' => $excludePostArr,
							'meta_query' => array(
										array(
											'key'     => 'important',
											'value'   => '1',
											'compare' => '=',
										),
							),
						) );
					?>
					<?php
						if ($r->have_posts()) :
						while ( $r->have_posts() ) : $r->the_post(); ?>
							<li><a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a></li>
					<?php 
						$excludePostArr[] = $post->ID;
						endwhile;
						wp_reset_postdata();
						endif;
					?>
					
					</ul>
					<!-- - - - - - - - - - - - - - End of List 2 - - - - - - - - - - - - - - - - -->
			</div><!-- / .beginner_left -->
		</div><!-- / .clearfix -->
	</div><!-- / .turn_content -->
<!-- - - - - - - - - - - - - - End of Turn Content - - - - - - - - - - - - - - - - -->
</section>