<figure class="search_result">
	<a href="<?php the_permalink();?>" class="search_img">
		<?php renderImg( p75GetThumbnail($post->ID, 221, 166, ""), 221, 166, get_the_title(), 1 );?>
	</a>
	<figcaption>
		<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
		<hr>
		<p><?php echo getPostPreviewText(20);?></p>
	</figcaption>
</figure>