<div class="header_info_box clearfix">
	<!-- - - - - - - - - - - - - - Quotes - - - - - - - - - - - - - - - - -->
	<div class="quotes_box clearfix">
		<div class="header_info_title">Котировки</div>
		<div class="header_info_container QuotesPanel" id="wQuotesPanel">
			<div class="wQuotes">
			<div class="header_info_slider owl-carousel " data-desktop="4" data-large="3" data-medium="2" data-small="1" data-extra-small="1">
				<?php

					$appController = new CController('default');
					ob_start();
					$appController->widget('widgets.quotes.QuotesPanelWidget',Array('view' => 'panel')); 
					$quotesHtml = ob_get_contents();
					ob_end_clean();

					$quotesHtml=str_replace("\n","",$quotesHtml);
					$quotesHtml=str_replace("\r","",$quotesHtml);
					$quotesHtml=preg_replace('/<div class\="wQuoteItemDelete" title\="Удалить с панели" data-id="[0-9]+"><div class\="wQuoteItemBtnImage"><\/div><\/div>/',"",$quotesHtml);

					preg_match('/<div class="wQuotes">(.*)<\/div>[\s,\t]*<div class\="wPlusBtn"/', $quotesHtml, $quotesSlides);
					echo $quotesSlides[1];
					
					preg_match('/<script>(.*)<\/script>/', $quotesHtml, $quotesJs);
					echo '<script>if( screen.width > 700 ){ '.$quotesJs[1].' }</script>';
				?>
				<?php/*<span><strong>USD/RUB:</strong>55,7345<i></i></span>
				<span><strong>EUR/RUB:</strong>62,6259<i></i></span>
				<span><strong>EUR/USD:</strong>1,11874<i></i></span>
				<span><strong>GOLD:</strong>1176,83<i></i></span>
				<span><strong>Oil:</strong>58,22<i></i></span>*/?>
			
			</div>
			</div>
		</div><!--/ .header_info_container -->
	</div><!--/ .quotes_box -->
	<!-- - - - - - - - - - - - - - End of Quotes - - - - - - - - - - - - - - - - -->
	<!-- - - - - - - - - - - - - - Focus - - - - - - - - - - - - - - - - -->
	<div class="focus_box clearfix">
		<div class="header_info_title">В фокусе</div>
		<div class="header_info_container">
			<!-- - - - - - - - - - - - - - Focus Slidet - - - - - - - - - - - - - - - - -->
			<div class="header_info_slider owl-carousel" data-desktop="2" data-large="2" data-medium="1" data-small="1" data-extra-small="1">
				<?php
					$r = new WP_Query( array(
						'post_status'         => 'publish',
						'ignore_sticky_posts' => true,
						'posts_per_page' => 5,
						'meta_query' => array(
										array(
											'key'     => 'focus',
											'value'   => '1',
											'compare' => '=',
										),
						),
					) );
					if ($r->have_posts()) :
					while ( $r->have_posts() ) : $r->the_post(); ?>
						<p>
							<a href="<?php the_permalink();?>"><?php echo wp_trim_words( get_the_title(), 5, '...');?></a> 
							<?php print_comments_number(); ?>
							<strong>/</strong>
						</p>
				<?php 
					endwhile;
					wp_reset_postdata();
					endif;
				?>
			</div>
			<!-- - - - - - - - - - - - - - End of Focus Slider - - - - - - - - - - - - - - - - -->
		</div><!--/ .header_info_container -->
	</div><!--/ .focus_box -->
</div><!--/ .header_info_box -->
<!-- - - - - - - - - - - - - - End of Quotes End Focus Bosxs - - - - - - - - - - - - - - - - -->