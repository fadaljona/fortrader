<figure class="post_small no_border">
	<a href="<?php the_permalink();?>"><?php renderImg( p75GetThumbnail($post->ID, 120, 70, ""), 120, 70, get_the_title(), 1 );?></a>
	<figcaption>
		<?php
			$charsCount = 45;
			$loopTitle = get_the_title();
			if( iconv_strlen( $loopTitle ) > $charsCount ) $loopTitle = mb_substr($loopTitle, 0, $charsCount) . ' ...';
		?>
		<a href="<?php the_permalink();?>"><?php echo $loopTitle;?></a>
		<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d F, H:i',  get_the_date('Y-m-d H:i:s') ); ?></time><?php } ?>
	</figcaption>
</figure>