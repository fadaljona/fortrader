<?php

$lang = 'en';

if (class_exists('Yii')) {
    if (Yii::app()->language == 'ru') {
        $lang = 'ru';
    }
} else {
    if (pll_current_language('slug') == 'ru') {
        $lang = 'ru';
    }
}
?>
<div class="social_yandex_buttons">
	<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
	<script src="//yastatic.net/share2/share.js"></script>
	<div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,linkedin,lj,viber,whatsapp,skype,telegram" data-counter="" data-lang="<?php echo $lang;?>" style="height:25px;"></div>					
</div><!--/ .box1 -->