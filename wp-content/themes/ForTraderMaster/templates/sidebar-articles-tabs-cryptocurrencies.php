<?php
    $cat = 14205;
    $days = 30;

    $postsInCat = [];

    $r1 = new WP_Query(array(
        'post_status'  => 'publish',
        'post_type' => 'post',
        'posts_per_page' => 5,
        'meta_key' => 'views',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'category__in' => $cat ,
        'date_query' => array(
            array(
                'after'     => date('Y-m-d H:i:s', time()-60*60*24*$days),
                'inclusive' => true,
            ),
        ),
    ));

    $postBlocks = '';

    if ($r1->have_posts()) {
        while ($r1->have_posts()) {
            $r1->the_post();
        
            $author = get_userdata($post->post_author);
            $authorName = getShowUserName($author);
        
            $postBlocks .= '<a href="'.get_the_permalink().'" class="fx_sb-section-item fx_sb-news-item clearfix">';
                $postBlocks .= '<div><img src="'.p75GetThumbnail($post->ID, 50, 40, "").'" alt="" class="fx_sb-news-img"></div>';
                $postBlocks .= '<div>';
                    $postBlocks .= '<div class="fx_sb-news-text">'.get_the_title().' '.print_comments_number_l2(false).'</div>';
                    $postBlocks .= '<div class="fx_sb-news-date">'.$authorName.' - '.get_the_date('d/m/Y').'</div>';
                $postBlocks .= '</div>';
            $postBlocks .= '</a>';

            $postsInCat[] = $post->id;
        }
    }

    $r2 = new WP_Query(array(
        'post_status'  => 'publish',
        'post_type' => 'post',
        'posts_per_page' => 5,
        'meta_key' => 'views',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'category__not_in' => [$cat, get_option('questions_cat_id'), get_option('services_cat_id')] ,
        'post__not_in' => $postsInCat,
        'date_query' => array(
            array(
                'after'     => date('Y-m-d H:i:s', time()-60*60*24*$days),
                'inclusive' => true,
            ),
        ),
    ));

    if ($r1->have_posts() || $r2->have_posts()) {
?>
<div class="fx_sb-section mpaBlock">
    <div class="fx_sb-title">
        <span><?php _e("Best of the week", 'ForTraderMaster'); ?></span>
    </div>
    <div class="fx_sb-buttons clearfix">
        <?php if ($r1->have_posts()) {?><a href="<?php echo get_category_link($cat); ?>" class="active"><?php _e("Crypto currency", 'ForTraderMaster'); ?></a><? }?>
        <?php if ($r2->have_posts()) {?><a href="/" <?php if (!$r1->have_posts()) echo 'class="active"'; ?>><?php _e("Other", 'ForTraderMaster'); ?></a><? }?>
    </div>
<?php if ($r1->have_posts()) {?>
    <div class="fx_sb-section-box fx_sb-most-articles active">
        <?php echo $postBlocks;?>
        <a href="<?php echo get_category_link($cat); ?>" class="fx_sb-more"><?php _e("View more articles", 'ForTraderMaster'); ?></a>
    </div>
<? }?>
<?php if ($r2->have_posts()) {?>
    <div class="fx_sb-section-box fx_sb-most-articles <?php if (!$r1->have_posts()) echo 'active'; ?>">
<?php
$postBlocks = '';
while ($r2->have_posts()) {
    $r2->the_post();

    $author = get_userdata($post->post_author);
    $authorName = getShowUserName($author);

    $postBlocks .= '<a href="'.get_the_permalink().'" class="fx_sb-section-item fx_sb-news-item clearfix">';
        $postBlocks .= '<div><img src="'.p75GetThumbnail($post->ID, 50, 40, "").'" alt="" class="fx_sb-news-img"></div>';
        $postBlocks .= '<div>';
            $postBlocks .= '<div class="fx_sb-news-text">'.get_the_title().' '.print_comments_number_l2(false).'</div>';
            $postBlocks .= '<div class="fx_sb-news-date">'.$authorName.' - '.get_the_date('d/m/Y').'</div>';
        $postBlocks .= '</div>';
    $postBlocks .= '</a>';
}
echo $postBlocks;
?>
    </div>
<? }?>
</div>


<?php add_action('wp_footer', function () { ?>
<script>
!function( $ ) {
    $('.mpaBlock .fx_sb-buttons a').click(function() {
        if($(this).hasClass('active')) return false;
        var index =  $('.mpaBlock .fx_sb-buttons a').index( $(this) );

        $('.mpaBlock .fx_sb-buttons a').removeClass('active');
        $(this).addClass('active');
        $('.mpaBlock .fx_sb-most-articles').removeClass('active');
        $('.mpaBlock .fx_sb-most-articles').eq(index).addClass('active');

        return false;
    });
}( window.jQuery );
</script>	
<?php }, 100); ?>

<?php }?>