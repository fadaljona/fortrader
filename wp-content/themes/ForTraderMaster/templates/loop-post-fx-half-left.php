<a href="<?php the_permalink();?>" class="fx_half-left-item clearfix">
    <?php renderImg(p75GetThumbnail($post->ID, 120, 100, ""), 120, 100, get_the_title(), 1);?>
    <div>
        <div class="fx_half-item-text"><?php the_title();?></div>
        <div class="fx_half-item-date"><?php echo date('d/m/Y', strtotime($post->post_date))?></div>
    </div>
</a>