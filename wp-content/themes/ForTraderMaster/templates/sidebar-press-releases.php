<?php
	$blockCategoryId = 10;
?>
<hr class="separator2">
<h4 class="page_title3"><?php _e("Press releases", 'ForTraderMaster'); ?><i class="icon thumbs_up_icon"></i></h4>
<hr class="separator2">
<!-- - - - - - - - - - - - - - List 5 - - - - - - - - - - - - - - - - -->
<?php
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 3,
		'cat' => $blockCategoryId,
	) );
	if ($r->have_posts()) :
		echo '<ul class="list5">';
		while ( $r->have_posts() ) : $r->the_post(); 
			get_template_part( 'templates/loop-post-li-h6' );
		endwhile;
		wp_reset_postdata();
		echo '</ul>';
	endif;
?>