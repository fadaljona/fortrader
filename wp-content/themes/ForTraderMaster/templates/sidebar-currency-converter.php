<?php
    loadYii();

    $currenciesForConverter = CurrencyRatesModel::getEcbCurrenciesForConverter();
    $defaultCurrency = CurrencyRatesModel::getDefaultCurrency('ecb');

    $model = CurrencyRatesModel::model()->findBySlug('usd');
    if (!$model) {
        $model = CurrencyRatesModel::model()->find();
    }

?>
<div class="fx_sb-section wSideBarCurrencyConverter">
    <div class="fx_sb-convert">

        <div class="fx_sb-convert-row clearfix">
            <input type="text" value="100" class="fx_sb-convert-input converterInpFrom converter_form_inp">
            <select class="fx_sb-convert-select js_fx-sb-select converterSelect converterSelectFrom">
            <?php
                echo CHtml::tag(
                    'option', 
                    array(
                        'data-value' => 1,
                        'value' => $defaultCurrency->id,
                        'data-restitle' => $defaultCurrency->name,
                        'class' => 'fx_convert-flag fx_convert-flag-eu',
                    ),
                    $defaultCurrency->code
                );
                foreach ($currenciesForConverter as $key => $val) {
                    $selected = false;
                    if( $key == $model->code ) $selected = true;
                    echo CHtml::tag(
                        'option',
                        array(
                            'data-value' => $val['value'],
                            'value' => $val['id'],
                            'data-restitle' => $val['name'],
                            'selected' => $selected,
                            'class' => 'fx_convert-flag fx_convert-flag-'.$val['country'],
                        ),
                        $key
                    );
                }
            ?>
            </select>
        </div>

        <div class="fx_sb-convert-row clearfix">           
            <?php $convertResult = number_format(1/$currenciesForConverter[$model->code]['value']*100, 2, '.', '');?>
            <input type="text" readonly value="<?php echo $convertResult;?>" class="fx_sb-convert-input converterInpTo converter_form_inp">
            <select class="fx_sb-convert-select js_fx-sb-select converterSelect converterSelectTo">
            <?php 
                echo CHtml::tag(
                    'option', 
                    array(
                        'data-value' => 1,
                        'value' => $defaultCurrency->id,
                        'selected' => true,
                        'data-restitle' => $defaultCurrency->name,
                        'class' => 'fx_convert-flag fx_convert-flag-eu',
                    ), 
                    $defaultCurrency->code
                );
                foreach ($currenciesForConverter as $key => $val) {
                    echo CHtml::tag(
                        'option',
                        array(
                            'data-value' => $val['value'],
                            'value' => $val['id'],
                            'data-restitle' => $val['name'],
                            'class' => 'fx_convert-flag fx_convert-flag-'.$val['country'],
                        ),
                        $key
                    );
                }
            ?>
            </select>
      </div>        
    </div>

    <div class="fx_sb-result-convert">                 
        <div class="fx_sb-result-convert-input"><span>100 <?php echo $currenciesForConverter[$model->code]['name']?></span> <?php echo Yii::t('*', 'equals');?></div>
        <div class="fx_sb-result-convert-output"><?php echo $convertResult;?> <?php echo $defaultCurrency->name;?></div>
    </div>
</div>
<?php
wp_enqueue_script('wSidebarCurrencyConverterWidget.js', '/js/widgets/wSidebarCurrencyConverterWidget.js', array('jquery'), null);
add_action('wp_footer', function () {
?>
<script>
!function( $ ) {

wSidebarCurrencyConverterWidgetOpen({
    selector: 'aside .wSideBarCurrencyConverter',
    sendConverterDataUrl: '<?php echo Yii::app()->createUrl('currencyRates/ajaxConvertRequest');?>',
     type: 'ecb'
});
}( window.jQuery );
</script>
<?php }, 100); ?>