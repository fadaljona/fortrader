<div>
	<figure class="post big">
		<figcaption>
			<a href="<?php the_permalink();?>" target="_blank"><?php the_title();?> <?php print_comments_number(); ?></a>
		</figcaption>
		<a href="<?php the_permalink();?>" target="_blank">
			<?php renderImg( p75GetThumbnail($post->ID, 346, 200, ""), 346, 200, get_the_title(), 1 );?>
		</a>
		<div class="post_info clearfix">
			<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d F, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
			<span><?php echo get_post_meta ($post->ID,'views',true); ?></span>
		</div>
		<hr>
	</figure>
</div><!-- / .news_post_left -->