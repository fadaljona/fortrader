<h4 class="page_title3"><?php _e("Comments", 'ForTraderMaster'); ?><i class="icon comment_icon"></i></h4>
<!-- - - - - - - - - - - - - - Comments - - - - - - - - - - - - - - - - -->
<?php
	$recent_comments = get_comments( array(
		'number'    => 4,
		'status'    => 'approve'
	) );
	foreach( $recent_comments as $comment ){

		if( $comment->user_id>0 ){
			$author = get_userdata( $comment->user_id ); 
			
			$authorName = getShowUserName( $author );
			$authorName = '<figcaption><a href="'.get_author_posts_url($author->ID).'">'.$authorName.'</a></figcaption>'

			if( $author->city ){
				$city='<span>г. '.$author->city.'</span>';
			}else{
				$city='';
			}
		}else{
			$city='';
			$authorName=$comment->comment_author;
		}
?>
	<figure class="post">
		<div class="comments">
			<div>
				<div class="coments_avatar">
					<?php renderImg( p75GetServicesAuthorThumbnail( 50, 50, $author->ID ), 50, 50, $authorName, 1 );?>
				</div>
				<h6 class="comment_autor">
					<?php echo $authorName; ?>
					<?php echo $city;?>
				</h6>
			</div>
			<?php 
				$commentContent = customExcerpt( strip_tags( apply_filters('the_content', $comment->comment_content) ), 100 );
				$pollId = get_comment_meta( $comment->comment_ID, 'expoll_id', true );
				if( $pollId ){
					global $ExPolls;
					$poll = $ExPolls->getPollByIdOrSlug($pollId);
			?>
					<figcaption><a href="/<?php echo $ExPolls->url_prefix . '/' . $poll->slug;?>.html#comment-<?php echo $comment->comment_ID;?>"><?php echo $commentContent; ?></a></figcaption>
			<?php
				}else{?>
					<figcaption><a href="<?php echo get_permalink($comment->comment_post_ID);?>#comment-<?php echo $comment->comment_ID;?>"><?php echo $commentContent; ?></a></figcaption>
			<?php	} ?>
				
		</div>
	</figure>
<?php } ?>
<!-- - - - - - - - - - - - - - End of Comments - - - - - - - - - - - - - - - - -->