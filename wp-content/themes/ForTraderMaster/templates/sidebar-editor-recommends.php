<hr class="separator2">
<h4 class="page_title3"><?php _e("The editor recommends", 'ForTraderMaster'); ?><i class="icon thumbs_up_icon"></i></h4>
<hr>
<?php
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 5,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'important',
				'value'   => '1',
				'compare' => '=',
			),
			array(
				'key'     => 'daylife',
				'value'   => $currentdate1,
				'compare' => '>',
			),
		),
	) );
	if ($r->have_posts()) :
		$i=0;
		while ( $r->have_posts() ) : $r->the_post(); 
			get_template_part( 'templates/loop-post-big' );
			if( $i==2 ) get_template_part( 'templates/banners-small' );
			$i++;
		endwhile;
		wp_reset_postdata();
	endif;
?>
<!-- - - - - - - - - - - - - - End of Post Big - - - - - - - - - - - - - - - - -->