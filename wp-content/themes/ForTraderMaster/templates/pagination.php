<?php
// Вызов постраничной навигации
global $wp_query;
$max_page = $wp_query->max_num_pages;
$nump=4;  /*Количество отображаемых подряд номеров страниц*/
 
if($max_page>1){
    $paged = intval(get_query_var('paged'));
    if(empty($paged) || $paged == 0) $paged = 1;
 
    echo '<div class="navigation_box clearfix">';
	
		if($paged>1) echo '<a class="prev" href="'.get_pagenum_link($paged-1).'"><span class="tooltip">'. __("Previous", 'ForTraderMaster') .'</span></a>';
		
		if($paged!=$max_page) echo '<a class="next" href="'.get_pagenum_link($paged+1).'"><span class="tooltip">'. __("Next", 'ForTraderMaster') .'</span></a>';
		
			
		echo '<div class="box1 align_center"><ul class="nav_list d_ib">';	

			if($paged!=1) echo '<li><a href="'.get_pagenum_link(1).'">1</a></li>';
				else echo '<li class="current"><span>1</span></li>'; 
		 
			if($paged-$nump>1) $start=$paged-$nump; else $start=2;
			if($paged+$nump<$max_page) $end=$paged+$nump; else $end=$max_page-1;
		 
			if($start>2) echo '<li><span>...</span></li> <li><a href="'.get_pagenum_link($max_page).'">'.$max_page.'</a></li>';
		 
			for ($i=$start;$i<=$end+1;$i++)
			 {
			 if($paged!=$i) echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
				else echo '<li class="current"><span>'.$i.'</span></li>';
			 }
		 
			if($end<$max_page-1) echo '<li><span>...</span></li><li><a href="'.get_pagenum_link($max_page).'">'.$max_page.'</a></li>';
		
		echo '</ul></div>';
		 
		 
		
    echo '</div>' ;
    }
?>