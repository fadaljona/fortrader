<?php
	$brokers = BrokerModel::model()->findAll(array(
		'condition' => " `t`.`showOnHome`=1 ",
	));

if($brokers){
	shuffle($brokers);
?>
<section class="section_offset turn_box">
	<div class="clearfix position-relative">
		<h3 class="page_title2 alignleft"><?php _e("Brokers", 'ForTraderMaster'); ?></h3>
		<!-- - - - - - - - - - - - - - Nav Buttons - - - - - - - - - - - - - - - - -->
		<div class="nav_buttons alignright">
			<a href="#" class="prev_btn"><span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span></a>
			<a href="#" class="next_btn"><span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span></a>
		</div>
		<!-- - - - - - - - - - - - - - End of Nav Buttons - - - - - - - - - - - - - - - - -->
	</div>
	<hr>
	<?php if(count($brokers)){ ?>
	<div class="post_carousel owl-carousel" data-desktop="3" data-large="3" data-medium="1" data-small="1" data-extra-small="1" data-pages-to-load="1">
		<?php 
			$i=0;
			foreach( $brokers as $broker ){
				
		?>
		<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
			<figure class="broker_post">
				<a href="<?php echo Yii::app()->createUrl('broker/single', array( 'id' => $broker->id ) ); ?>">
					<img title="<?php echo $broker->shortName;?>" alt="<?php echo $broker->shortName;?>" src="<?php echo p75GetServicesThumbnail( $broker->getSrcHomeImage(), 413, 211 );?>" class="" style="display: inline;">
				</a>
				<p><?php echo $broker->brandName?></p>
				<?php
					if( strlen( $broker->site )){
						$link = $broker->getRedirectURL( 'site' );
					}else{
						$link = Yii::App()->createURL( 'broker/single', Array( 'id' => $broker->id ));
					}
				?>
				<a href="<?=$link?>" target="_blank" class="account_btn"><?php _e("Read more", 'ForTraderMaster'); ?></a>
			</figure>
		</div>
		<?php $i++; } ?>
	</div><!-- / .owl-carousel -->
	<?php } ?>
</section>
<?php } ?>