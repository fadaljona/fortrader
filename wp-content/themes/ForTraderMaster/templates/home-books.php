<?php
	$blockCategoryId = 1822;
?>
<section class="turn_box">
	<div class="clearfix position-relative">
		<h3 class="page_title2 alignleft"><?php _e("Books", 'ForTraderMaster'); ?></h3>
		<!-- - - - - - - - - - - - - - Nav Buttons - - - - - - - - - - - - - - - - -->
		<div class="nav_buttons alignright">
			<a href="#" class="prev_btn"><span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span></a>
			<a href="#" class="next_btn"><span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span></a>
		</div>
		<!-- - - - - - - - - - - - - - End of Nav Buttons - - - - - - - - - - - - - - - - -->
	</div>
	<hr>
	<div class="carousel-wrapper">
	<div class="post_carousel owl-carousel" data-desktop="4" data-large="3" data-medium="2" data-small="2" data-extra-small="1" data-currentitem="0" data-maximumitem="1" data-type="books" data-offset="4">
	<?php
		$r = new WP_Query( array(
			'post_status'  => 'publish',
			'post_type' => 'post',
			'orderby' => 'date',
			'order'   => 'DESC',
			'posts_per_page' => 4,
			'cat' => $blockCategoryId,
		) );
	?>
	<?php
		$i=0;
		if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post(); ?>
				<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
					<figure class="book_post">
						<div class="book_img">
							<a href="<?php the_permalink();?>">
								<?php renderImg( p75GetThumbnail($post->ID, 130, 180, ""), 130, 180, get_the_title(), 1 );?>
							</a>
						</div>
						<figcaption>
							<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
						</figcaption>
					</figure>
				</div>
	<?php 
				$i++;
			endwhile;
			wp_reset_postdata();
		endif;
	?>
	</div>
	</div>
</section>
<hr class="separator2">
