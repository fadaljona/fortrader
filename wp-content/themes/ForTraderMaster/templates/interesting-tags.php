<section class="section_offset">
	<h3 class="page_title2">
		<?php _e("Interesting tags", 'ForTraderMaster'); ?>
		<span class="sub_title"><?php _e("Choose topic", 'ForTraderMaster'); ?></span>
		<i class="icon folder_icon"></i>
	</h3>
	<hr>
	<?php
		$posttags = get_tags( array(
			'orderby' => 'count',
			'order' => 'DESC',
			'number' => 15,
		) );
		if( count($posttags) ){
	?>
	<div class="categories_box clearfix">
		<?php foreach( $posttags as $tag ){?>
		<a href="<?php echo get_tag_link($tag->term_id);?>" class="categories_btn tag-load-link" data-tag-id="<?php echo $tag->term_id;?>" data-tag-page="1"><?php echo $tag->name;?></a>
		<?php } ?>
	</div>
	<?php } ?>
	<hr>
	<!-- - - - - - - - - - - - - - Post Box - - - - - - - - - - - - - - - - -->
	<div class="post_box interesting-tags-wrapper">

	</div><!--  / .post_box -->
	<a href="#" class="load_btn tag-load-link chench_btn" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
	<!-- - - - - - - - - - - - - - End of Post Box - - - - - - - - - - - - - - - - -->
</section>