<?php 
get_template_part( 'templates/banner-728x90' );

global $fortraderCache;

$locale = get_locale();

$cacheDuration = get_option('theme_cache_duration');
$cache_key = 'theme_' . md5('home-content-before-calendar') . $locale;

if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
	get_template_part( 'templates/recent-popular' );
	get_template_part( 'templates/editor-recommends' );
	get_template_part( 'templates/economy-news' );
    get_template_part( 'templates/interesting-tags-manual' );
	get_template_part( 'templates/trade-ideas' );
    get_template_part( 'templates/crypto-currencies-on-home' );
	get_template_part( 'templates/technical-analysis' );
$fortraderCache->endCache(); }


get_template_part( 'templates/economic-calendar' );
		

$cache_key = 'theme_' . md5('home-content-under-calendar') . $locale;
if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
		get_template_part( 'templates/magazine-on-home' );
		get_template_part( 'templates/for-beginer' );
		get_template_part( 'templates/home-contest' );
		get_template_part( 'templates/for-professional' );
		get_template_part( 'templates/broker-news' );
		get_template_part( 'templates/home-brokers' );
		get_template_part( 'templates/binary-options' );
		get_template_part( 'templates/persons' );
		get_template_part( 'templates/home-books' );
		get_template_part( 'templates/home-indicators' );
		get_template_part( 'templates/interesting-tags-manual-indicators' );
		get_template_part( 'templates/home-partners' );
		get_template_part( 'templates/trading-strategies' );
		get_template_part( 'templates/interesting-tags-manual-strategies' );
		get_template_part( 'templates/advisers' );
		get_template_part( 'templates/home-authors' );	
$fortraderCache->endCache(); }
?>