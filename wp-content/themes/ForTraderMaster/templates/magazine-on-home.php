<?php
	$journalMainCat = 73;
	$lastJournal = JournalModel::model()->find(Array(
			'condition' => " `t`.`status` = '0' ",
			'order' => " `t`.`date` DESC, `t`.`id` DESC ",
			'limit' => 1
	));
?>
<h2 class="page_title1">
	<?php _e("Magazine for traders", 'ForTraderMaster'); ?> <a href="<?php echo $lastJournal->getSingleURL();?>" class="link_accent">#<?php echo $lastJournal->number;?></a>
	<?php 
		if($lastJournal->free){
			$type = CommonLib::getExtension( $lastJournal->pathJournal );
			echo '<a href="'.$lastJournal->getDownloadJournalURL().'" class="icon pdf_icon"></a>';
		}else{
			$JournalSettings = JournalSettingsModel::getModel();
			echo '<a href="'.$JournalSettings->buyLink.'" class="icon dollar_icon"></a>';
		}
		if( $lastJournal->digest ){
			$link = $lastJournal->digest;
		}else{
			$link = $lastJournal->singleUrl;
		}
		$inner = $lastJournal->getCurrentInner();
		
	?>
</h2>
<hr class="separator1">
<div class="clearfix section_offset">
	<div class="image_box alignleft">
		<a href="<?php echo $lastJournal->singleUrl; ?>"><?php echo removeProtocol( $lastJournal->getCover(Array( 'style' => "max-width:243px;" )) )?></a>
	</div>
	<div class="wrapper">
		<ul class="list1">
			<?php
			if( !$inner ){
				$r = new WP_Query( array(
					'post_status'  => 'publish',
					'post_type' => 'post',
					'orderby' => 'date',
					'order'   => 'DESC',
					'posts_per_page' => 5,
					'meta_query' => array(
										array(
											'key'     => 'journal',
											'value'   => $lastJournal->number,
											'compare' => '=',
										),
					),
				) );
			?>
			<?php
				if ($r->have_posts()) :
					while ( $r->have_posts() ) : $r->the_post(); ?>
						<li>
							<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
						</li>
				<?php 
					endwhile;
					wp_reset_postdata();
				endif;
			}else{
				foreach( $inner as $i=>$_inner ){
					if( isset($_inner['url']) && $_inner['url'] ){
						$headerInner = CHtml::link( $_inner['header'], $_inner['url'] );
					}else{
						$headerInner = $_inner['header'];
					}
					echo CHtml::openTag('li');
						echo $headerInner;
					echo CHtml::closeTag('li');
				}
			}
			?>          
		</ul>
		<div class="clearfix">
			<a href="<?php echo esc_url( get_category_link( $journalMainCat ) ); ?>" class="archive_btn"><?php _e("Archive", 'ForTraderMaster'); ?></a>
			<a href="<?php echo $link; ?>" class="edition_btn chench_btn" data-text="<?php _e("Read the magazine for free", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Read the magazine", 'ForTraderMaster'); ?>"></a>
		</div>
	</div>
</div><!--/ .section_offset -->