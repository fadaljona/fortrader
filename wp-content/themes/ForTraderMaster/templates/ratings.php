<?php 
    loadYii(); 
    Yii::import( 'components.widgets.WebRatingWidget' );

    wp_enqueue_script('wCommonRatingWidget.js', CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wCommonRatingWidget.js' ), array('jquery'), null);

    add_action('wp_footer',function(){
?>
<script>
var topAccountsRatingWidget;
;(function($){
	topAccountsRatingWidget = wCommonRatingWidgetOpen({
        selector: '.topAccountsRating',
        modelName: 'ContestMember',
        cutStrToGetId: 'top-account-id',
        ajaxAddMarkURL: '<?php echo Yii::app()->createUrl('commonRating/ajaxAddMark')?>'
	});
})(jQuery);
</script>
<?php
    }, 100);
?>

<div class="fx_section">
    <div class="fx_content-title clearfix">
        <span class="fx_content-title-link"><?php echo Yii::t('*', "Ratings"); ?></span>
        <img src="<?php echo get_bloginfo('template_url');?>/images/fx_icon-rating.png" alt="" class="fx_title-icon">
    </div>

    <div class="fx_section-box clearfix">

        <?php
            $c = new CDbCriteria;
            $c->with['stats'] = array();
            $c->with['server'] = array();
            $c->with['mt5AccountInfo'] = array();
            $c->with['commonRatingStats'] = array();

            $c->addCondition( ' `t`.`idContest` = 0 ' );
            $c->order = ' `stats`.`gain` DESC ';
            $c->limit = 10;

            $monitoringSettings = MonitoringSettingsModel::getModel();
            $inactiveDaysCondition = '';
            /*if( $monitoringSettings->inactiveDays ){
                $inactiveDaysCondition = ' AND `mt5AccountInfo`.`ACCOUNT_UPDATE` > :inactiveDaysTime';
                $c->params[':inactiveDaysTime'] = time() - 60*60*24*$monitoringSettings->inactiveDays;
            }*/
            $c->addCondition( " `mt5AccountInfo`.`ACCOUNT_UPDATE` IS NOT NULL AND `mt5AccountInfo`.`last_error_code` <> -255 AND `mt5AccountInfo`.`last_error_code` <> -1 AND `mt5AccountInfo`.`last_error_code` <> -254 AND `mt5AccountInfo`.`last_error_code` <> -3" . $inactiveDaysCondition);

            $models = ContestMemberModel::model()->findAll($c);
            $controller = Yii::app()->createController("/");
        ?>
        <div class="fx_rating-half topAccountsRating">
            <div class="fx_half-title"><?php echo Yii::t('*', "Top Accounts"); ?></div>
            <div class="fx_rating-list-wr">
            <?php foreach( $models as $model ){ ?>

                <div class="fx_rating-item fx_border-left clearfix">
                    <div>
                        <a href="<?php echo $model->singleURL;?>" class="fx_rating-subitem fx_account-item">
                        <?php
                            echo $model->accName;
                            if( $model->stats->gain ){
                                if( $model->stats->gain >0 ) echo '<span class="fx_account-up">+'.$model->stats->gain.' %</span>';
                                else echo '<span class="fx_account-down">'.$model->stats->gain.' %</span>';
                            }
                        ?>
                            
                        </a>
                    </div>
                    <div class='top-account-id<?php echo $model->id;?>'>
                    <?php
                        $ratingWidget = new WebRatingWidget(Array(
                            'type' => 'wp',
                            'ns' => "top-account-id" . $model->id,
                            'currentValue' => $model->commonRatingStats ? $model->commonRatingStats->average : 0,
                            'maxValue' => 10,
                            'returnNs' => true,
                            'onChangeValue' => "topAccountsRatingWidget.onChangeMark",
                            'readOnly' => Yii::App()->user->isGuest ? true : false,
                        ));
                        $ratingWidget->run();
                       
                    ?>
                    </div>
                </div>

            <?php } ?>

            </div>
        </div>

        
        <?php
            $c = new CDbCriteria;
            $c->with['bank'] = array('with' => array( 'currentLanguageI18N' ));
            $c->with['currentLanguageI18N'] = array();
            $c->order = ' `t`.`order` DESC ';

            $c->addCondition( " `cLI18NInterestRatesRateModel`.`title` <> '' AND `cLI18NInterestRatesRateModel`.`title` IS NOT NULL AND `cLI18NInterestRatesBankModel`.`title` <> '' AND `cLI18NInterestRatesBankModel`.`title` IS NOT NULL " );

            $models = InterestRatesRateModel::model()->findAll($c);
        ?>
        <div class="fx_rating-half">
            <div class="fx_half-title"><?php echo Yii::t('*', "Bank Interest Rates"); ?></div>
            <div class="fx_rating-list-wr fx_banks">

                <?php foreach( $models as $model ){ ?>
                    <div class="fx_rating-item fx_border-left clearfix">
                        <div>
                            <span class="fx_rating-subitem fx_banks-item">
                                <?php echo $model->bank->country->getImgFlag("shiny", 16);?>
                                <span><?php echo $model->bank->title; ?> <?php if( $model->bank->shortTitle ) echo '('.$model->bank->shortTitle.')';?></span>
                            </span>
                        </div>
                        <div>
                        <?php 
                            if( $model->lastValue->value < 0 )echo '<span class="fx_banks-rate fx_color-red">'.$model->lastValue->value.'%</span>';
                            else echo '<span class="fx_banks-rate">'.$model->lastValue->value.'%</span>';
                        ?>
                        
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>


<?php /*        
        <div class="fx_rating-half">
            <div class="fx_half-title">Top Brokers</div>
            <div class="fx_rating-list-wr fx_brokers">
                
                <div class="fx_rating-item clearfix">
                    <div>
                        <img src="<?php echo get_bloginfo('template_url');?>/images/logo_forex-time.jpg" alt="" class="fx_rating-logo">
                        <span class="fx_rating-name">ForexTime</span>
                    </div>
                    <div>
                        <div class="reviewStars-input">
                            <input class="star-4" type="radio" name="reviewStars"/>
                            <label title="gorgeous" for="star-4"></label>
                            <input class="star-3" type="radio" name="reviewStars"/>
                            <label title="good" for="star-3"></label>
                            <input class="star-2" type="radio" checked name="reviewStars"/>
                            <label title="regular" for="star-2"></label>
                            <input class="star-1" type="radio" name="reviewStars"/>
                            <label title="poor" for="star-1"></label>
                            <input class="star-0" type="radio" name="reviewStars"/>
                            <label title="bad" for="star-0"></label>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="fx_rating-half">
            <div class="fx_half-title">Top Strategy</div>
            <div class="fx_rating-list-wr fx_strategy">
                   
                <div class="fx_rating-item fx_border-left">
                    <div><a href="#" class="fx_rating-subitem fx_strategy-item">The Bladerunner Trade</a></div>
                    <div>
                        <div class="reviewStars-input">
                            <input class="star-4" type="radio" name="reviewStars"/>
                            <label title="gorgeous" for="star-4"></label>
                            <input class="star-3" type="radio" name="reviewStars"/>
                            <label title="good" for="star-3"></label>
                            <input class="star-2" type="radio" checked name="reviewStars"/>
                            <label title="regular" for="star-2"></label>
                            <input class="star-1" type="radio" name="reviewStars"/>
                            <label title="poor" for="star-1"></label>
                            <input class="star-0" type="radio" name="reviewStars"/>
                            <label title="bad" for="star-0"></label>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="fx_rating-half">
            <div class="fx_half-title">Top Indicators</div>
            <div class="fx_rating-list-wr fx_indicators">
                                        
                <div class="fx_rating-item fx_border-left clearfix">
                    <div>
                        <a href="#" class="fx_rating-subitem fx_indicators-item">
                            <div>Binary Options Indicator</div>
                            <div>Six Second Trades</div>
                        </a>
                    </div>
                    <div>
                        <div class="reviewStars-input">
                            <input class="star-4" type="radio" name="reviewStars"/>
                            <label title="gorgeous" for="star-4"></label>
                            <input class="star-3" type="radio" name="reviewStars"/>
                            <label title="good" for="star-3"></label>
                            <input class="star-2" type="radio" checked name="reviewStars"/>
                            <label title="regular" for="star-2"></label>
                            <input class="star-1" type="radio" name="reviewStars"/>
                            <label title="poor" for="star-1"></label>
                            <input class="star-0" type="radio" name="reviewStars"/>
                            <label title="bad" for="star-0"></label>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="fx_rating-half">
            <div class="fx_half-title">Top EA</div>
            <div class="fx_rating-list-wr">
                                        
                <div class="fx_rating-item fx_border-left clearfix">
                    <div>
                        <a href="#" class="fx_rating-subitem fx_ea-item">
                            Profitable scalping strategy for M1 timeframe <span class="fx_strong">Tricolor</span>
                        </a>
                    </div>
                    <div>
                        <div class="reviewStars-input">
                            <input class="star-4" type="radio" name="reviewStars"/>
                            <label title="gorgeous" for="star-4"></label>
                            <input class="star-3" type="radio" name="reviewStars"/>
                            <label title="good" for="star-3"></label>
                            <input class="star-2" type="radio" checked name="reviewStars"/>
                            <label title="regular" for="star-2"></label>
                            <input class="star-1" type="radio" name="reviewStars"/>
                            <label title="poor" for="star-1"></label>
                            <input class="star-0" type="radio" name="reviewStars"/>
                            <label title="bad" for="star-0"></label>
                        </div>
                    </div>
                </div>

            </div>
        </div>
*/?>

    </div>
</div>