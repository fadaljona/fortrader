<?php
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 
	
	global $sidebarPoolCss;
	$sidebarPoolCss = '';
	
	if( is_plugin_active( 'expolls/expolls.php' ) ) {
		global $ExPolls;
		if( !$ExPolls ) $ExPolls = new ExPolls();
		$user_ID = get_current_user_id();
		$poll = $ExPolls->getLastUnvotedPoll($user_ID);
		$poll_vote_count = $poll->vote_count ;
		if($poll){
			$poll->answers = $ExPolls->getPollAnswers($poll->ID);
			$poll->answers_count = count($poll->answers);
			
		?>
			<hr class="separator2">
			<h4 class="page_title3 clearfix"><?php _e("Your opinion", 'ForTraderMaster'); ?> <?php if ( $poll_vote_count > 35 ) echo '<span class="view">(<span class="votescount">'.$poll_vote_count.'</span> '.__("opinions", 'ForTraderMaster').')</span>';?></h4>
			<hr>
			<div class="interview_box section_offset expoll_container_sidebar">
				<form>
					<h6><?php echo $poll->title ?></h6>
					<div class="jq-anwears">
						
						<?php 
							$sidebarPoolCss .= '<style>';
							$contentStr = '';
							for($i = 0; $i < $poll->answers_count; $i++){
								$sidebarPoolCss .= '.interview.before-line'. $poll->answers[$i]->ID .'::before{width:'. (int) $poll->answers[$i]->percent .'%;}';
								
								$contentStr .= '<div class="interview before-line'. $poll->answers[$i]->ID .'">
										<input type="';
								
								if (!$poll->multi){
									$contentStr .= 'radio';
								}else{
									$contentStr .= 'checkbox';
								}
								$contentStr .= '" id="interview'. $poll->answers[$i]->ID .'" name="expoll-answer" value="'. $poll->answers[$i]->ID .'">
										<label for="interview'. $poll->answers[$i]->ID .'" class="square_input"><i></i>'. $poll->answers[$i]->name .'</label>
									</div>';
							}; 
							$sidebarPoolCss .= '</style>';
							echo $contentStr;
						?> 
						<input name="expoll-id" type="hidden" value="<?php echo $poll->ID ?>" />
					</div>
					<div class="clearfix">
						<a href="/<?php echo $ExPolls->url_prefix;?>.html" class="more_interview_btn" target="_blank"><?php _e("Other polls", 'ForTraderMaster'); ?></a>
						<button class="reply_btn expoll_vote_from_sidebar"><?php _e("Reply", 'ForTraderMaster'); ?></button>
					</div>
					
				</form>
			</div>
		<?php
		}else{
			$poll = $ExPolls->getRandomPoll();
			$poll->answers = $ExPolls->getPollAnswers($poll->ID);
			$poll->answers_count = count($poll->answers);
			$poll_vote_count = $poll->vote_count ;
			?>
			<hr class="separator2">
			<h4 class="page_title3 clearfix"><?php if ( $poll_vote_count > 35 ) echo '<span class="view">(<span class="votescount">'.$poll_vote_count.'</span> '.__("opinions", 'ForTraderMaster').')</span>';?></h4>
			<hr>
			<div class="interview_box section_offset expoll_container_sidebar">
				<form>
					<h6><?php echo $poll->title ?></h6>
					<div class="jq-anwears">
						<?php 
							$sidebarPoolCss .= '<style>';
							$contentStr = '';
							for($i = 0; $i < $poll->answers_count; $i++){
								$sidebarPoolCss .= ".interview.before-line{$poll->answers[$i]->ID}::before{width:"  . (int) $poll->answers[$i]->percent . "%;}";
								$contentStr .= '<div class="interview before-line'. $poll->answers[$i]->ID .'">
									<label class="no-poll">'. $poll->answers[$i]->name .' '. (int) $poll->answers[$i]->percent .'% ('. $poll->answers[$i]->vote_count .')</label>
								</div>';
							}; 
						$sidebarPoolCss .= '</style>';
						echo $contentStr;
						?> 
						<input name="expoll-id" type="hidden" value="<?php echo $poll->ID ?>" />
					</div>
					<div class="clearfix">
						<a href="/<?php echo $ExPolls->url_prefix;?>.html" class="more_interview_btn"><?php _e("Other polls", 'ForTraderMaster'); ?></a>
					</div>
					
				</form>
			</div>
			<?php
		}
	}
	function sidebar_poll_css( ){
		global $sidebarPoolCss;
		echo "<script>document.write('$sidebarPoolCss')</script>";
	}
	add_action('wp_footer','sidebar_poll_css', 100);
?>