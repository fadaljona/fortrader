<?php
	$blockCategoryId = 641;
?>
<hr class="separator2">
<h4 class="page_title3"><?php echo get_cat_name( $blockCategoryId );?><i class="icon analitic_icon"></i></h4>
<hr>
<!-- - - - - - - - - - - - - - Post Small - - - - - - - - - - - - - - - - -->
<?php
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 3,
		'cat' => $blockCategoryId,
	) );
	if ($r->have_posts()) :
		while ( $r->have_posts() ) : $r->the_post(); 
			get_template_part( 'templates/loop-post-small' );
		endwhile;
		wp_reset_postdata();
	endif;
?>
<!-- - - - - - - - - - - - - - End of Post Small - - - - - - - - - - - - - - - - -->