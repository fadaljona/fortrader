<?php
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 6,
		'paged' => 1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'index',
				'value' => '',
				'compare' => '!=',
			),
			array(
				'key' => 'country',
				'value' => '',
				'compare' => '!=',
			),
		)
	));
	if ($r->have_posts()){
	
?>
<!-- - - - - - - - - - - - - - Financial news - - - - - - - - - - - - - - - - -->
<section class="section_offset">
	<hr class="separator2">
	<h3><?php _e("News", 'ForTraderMaster'); ?></h3>
	<hr>
	<div class="post_box load-more-wrapper">
		<?php
		while ( $r->have_posts() ){ 
			$r->the_post();
			echo '<div class="post_col2 post_col_sm1">';
			get_template_part( 'templates/loop-post-small-no-border' );
			echo '</div><!--/ .post_col -->';
		}
		?>
	</div>
	<a data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>" data-text="<?php _e("Show more news", 'ForTraderMaster'); ?>" data-page="1" class="load_btn load-more-calendar-news chench_btn" href="#"></a>
</section>
<!-- - - - - - - - - - - - - - End of Financial news - - - - - - - - - - - - - - - - -->
<?php }; ?>