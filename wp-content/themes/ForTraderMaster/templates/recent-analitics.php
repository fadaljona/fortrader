<?php
	$blockCategoryId = 641;
?>
<section class="section_offset">
	<h3 class="page_title2"><?php echo get_cat_name( $blockCategoryId );?><i class="icon newspaper_icon"></i></h3>
	<hr>       
	<?php
		$r = new WP_Query( array(
			'post_status'  => 'publish',
			'post_type' => 'post',
			'orderby' => 'date',
			'order'   => 'DESC',
			'posts_per_page' => 16,
			'cat' => $blockCategoryId,
		) );
	?>
	<?php
		$i=0;
		if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post(); ?>
			<?php if($i<2){ ?>
				<?php if( $i==0 ) echo '<div class="news_post_box clearfix">'; ?>
				<div>
					<figure class="post big">
						<a href="<?php the_permalink();?>">
							<?php renderImg( p75GetThumbnail($post->ID, 346, 200, ""), 346, 200, get_the_title(), 1 );?>
						</a>
						<div class="post_info clearfix">
							<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d F, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
							<span><?php echo get_post_meta ($post->ID,'views',true); ?></span>
						</div>
						<hr>
						<figcaption>
							<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
						</figcaption>
					</figure>
				</div><!-- / .news_post_left -->
				<?php if( $i==1 ) echo '</div><!-- / .clearfix -->'; ?>	
			<?php } 
			if( $i<5 && $i>1 ){
				if($i==2) echo '<div class="post_box">';
				get_template_part( 'templates/loop-post-col3' );
				if($i==4) echo '</div><!--  / .post_box --><hr>';
			} 
			if( $i<9 && $i>4 ){
				if($i==5) echo '<div class="clearfix"><div class="news_left1">';?>
					<figure class="post_small2">
						<a href="<?php the_permalink();?>">
							<?php renderImg( p75GetThumbnail($post->ID, 120, 100, ""), 120, 100, get_the_title(), 1 );?>
						</a>
						<figcaption>
							<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
							<p><?php echo getPostPreviewText(12);?></p>
						</figcaption>
					</figure>
				<?php if($i==8) echo '</div><!--  / .news_left1 -->';
			} 
			if( $i<16 && $i>8 ){
				if($i==9) echo '<div class="news_right1"><ul class="list2">';?>
					<li><a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a></li>
				<?php if($i==15) echo '</ul></div><!--  / .news_right1 --></div><!--  / .clearfix -->';
			} 
			$i++;
			endwhile;
			//закрытие тегов если постов < 16
			if($i<2) echo '</div><!-- / .clearfix -->';
			if( $i<4 && $i>2 )  echo '</div><!--  / .post_box --><hr>';
			if( $i<8 && $i>5 ) echo '</div><!--  / .news_left1 --></div><!--  / .clearfix -->';
			if( $i<15 && $i>9 ) echo '</ul></div><!--  / .news_right1 --></div><!--  / .clearfix -->';
			wp_reset_postdata();
		endif;
	?>
</section>