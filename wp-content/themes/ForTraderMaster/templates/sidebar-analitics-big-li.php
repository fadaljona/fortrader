<?php
	$blockCategoryId = '641,14205';
?>
<h4 class="page_title3"><?php _e("Analytics", 'ForTraderMaster'); ?><i class="icon news_icon"></i></h4>
<hr>
<?php
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 6,
		'cat' => $blockCategoryId,
	) );
	if ($r->have_posts()) :
		$i=0;
		while ( $r->have_posts() ) : $r->the_post(); 
			if( $i<2 ) get_template_part( 'templates/loop-post-big' );
			if( $i==2 ) echo '<ul class="list1 semibold">';
			if( $i>=2 ) get_template_part( 'templates/loop-post-li' );
			$i++;
		endwhile;
		if( $i>=3 ) echo '</ul>';
		wp_reset_postdata();
	endif;
?>