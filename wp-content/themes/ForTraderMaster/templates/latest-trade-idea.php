<?php
    $blockCategoryId = 14281;
    
    $r = new WP_Query( array(
        'post_status'  => 'publish',
        'post_type' => 'post',
        'orderby' => 'date',
        'order'   => 'DESC',
        'posts_per_page' => 9,
        'category__in' => $blockCategoryId ,
    ) );

    if ($r->have_posts()) :
?>
<div class="fx_section">
    <div class="fx_content-title clearfix">
        <h3 class="fx_content-title-link"><?php _e("Latest Trader's Ideas", 'ForTraderMaster'); ?></h3>
        <img src="<?php echo get_bloginfo('template_url');?>/images/fx_icon-news.png" alt="" class="fx_title-icon">
    </div>
    <div class="fx_section-box clearfix">
        <?php 
            $i=0;
            while ( $r->have_posts() ) : $r->the_post(); 
                if( $i == 0) echo '<div class="fx_box-half">';
                if( $i == 3) echo ' <div class="fx_box-half fx_latest-right">';

                if( $i<=2 ){
                    echo '<a href="'.get_permalink().'" class="fx_half-left-item clearfix">';
                        echo '<img src="'.p75GetThumbnail(get_the_ID(), 120, 100, "").'" alt="'.get_the_title().'" class="fx_sb-news-img">';
                        echo ' <div><span>'. get_the_title() .'.</span> '. getPostPreviewText(8, true) .'</div>';
                    echo '</a>';
                }else{
                    echo '<a href="'.get_permalink().'" class="fx_half-right-item">'. get_the_title() .'</a>';
                }

                if( $i == 2) echo '</div>';

            $i++; 
            endwhile; 
            if( $i<3 ) echo '</div>';
            if( $i>=3 ) echo '</div>';
        ?>
    </div>
</div>
<?php endif; ?>