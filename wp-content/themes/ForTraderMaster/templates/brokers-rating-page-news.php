<?php
	$catId = 7489;
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 6,
		'cat' => $catId,
		'paged' => 1,
	));
	if ($r->have_posts()){
	
?>
<!-- - - - - - - - - - - - - - Financial news - - - - - - - - - - - - - - - - -->
<section class="section_offset">
	<hr class="separator2">
	<h3><?php _e("Brokers News", 'ForTraderMaster'); ?></h3>
	<hr>
	<div class="post_box load-more-wrapper" data-cat-id="<?php echo $catId; ?>">
		<?php
		while ( $r->have_posts() ){ 
			$r->the_post();
			echo '<div class="post_col2 post_col_sm1">';
			get_template_part( 'templates/loop-post-small-no-border' );
			echo '</div><!--/ .post_col -->';
		}
		?>
	</div>
	<a data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-page="1" data-cat-id="<?php echo $catId;?>" class="load_btn load-more-broker-news chench_btn" href="#"></a>
</section>
<!-- - - - - - - - - - - - - - End of Financial news - - - - - - - - - - - - - - - - -->
<?php }; ?>