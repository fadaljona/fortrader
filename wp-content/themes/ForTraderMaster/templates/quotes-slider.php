<?php
    loadYii(); 

    $c = new CDbCriteria;
    $c->with = array( 
        'symbols' => array(
            'with' => array('currentLanguageI18N')
        ),
        'currentLanguageI18N',
    );
    $c->order = " `cLI18NQuotesCategoryModel`.`order`, `cLI18NQuotesSymbolsModel`.`weightInCat` DESC ";

    $c->condition = " `cLI18NQuotesCategoryModel`.`name` IS NOT NULL AND `cLI18NQuotesCategoryModel`.`name` <> '' AND `cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '' AND `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `symbols`.`sourceType` = 'own' ";
   
    $quotesCats = QuotesCategoryModel::model()->findAll($c);    
?>
<div class="fx_section turn_box wHomeQuotesSlider">
    <div class="fx_content-title">
        <a href="<?php echo Yii::app()->createUrl('quotesNew/list');?>" class="fx_content-title-link"><?php echo Yii::t('*', "Quotes"); ?></a>
        <span class="fx_content-date"><?php echo Yii::app()->dateFormatter->format( '/ EE dd / LLLL / hh:mm', time()) . strtolower( Yii::app()->dateFormatter->format( 'a', time()) );?></span>
        <div class="nav_buttons fx_slider-nav alignright">
            <a href="#" class="prev_btn fx_slider-prev"><span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span></a>
            <a href="#" class="next_btn fx_slider-next"><span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span></a>
        </div>
    </div>
    <div class="fx_slider-wr owl-carousel post_carousel" data-desktop="3" data-large="2" data-medium="1" data-small="1" data-extra-small="1">
        <?php
            foreach( $quotesCats as $cat ){
                echo CHtml::openTag('div');
                    echo CHtml::openTag('figure', array('class' => 'post'));
                        echo CHtml::openTag('div', array('class' => 'fx_sl-item'));
                            echo CHtml::tag('div', array('class' => 'fx_half-title'), $cat->name);
                            echo CHtml::openTag('div', array('class' => 'fx_sl-data-wr'));
                            
                            foreach( $cat->symbols as $symbol ){
                                echo CHtml::openTag('a', array('class' => 'fx_data-item clearfix', 'href' => $symbol->singleURL));
                                    $symbolName = $symbol->nalias ? $symbol->nalias : $symbol->name;
                                    echo CHtml::tag('span', array(), $symbol->getIcon16Img('fx_sl-flag') . $symbolName);

									$colorClass = 'fx_data-gray';
									if( $symbol->tickData->trend == 1 ){
										$colorClass = 'fx_data-green';
									}elseif( $symbol->tickData->trend == -1 ){
										$colorClass = 'fx_data-red';
									}
									$bid = $symbol->tickData->bid;
									if( $symbol->lastBid ){
										$data_last_bid = $symbol->lastBid;
										if( !isset( $symbol->tickData->precision ) ){
											$precision = strlen($bid) - strpos( $bid, '.' ) - 1;
										}else{
											$precision = $symbol->tickData->precision;
										}
									}

                                    echo CHtml::tag('span', array(
                                        'class' => $colorClass . ' pid-' . $symbol->name . '-bid' ,
                                        'data-precision' => $precision,
                                        'data-last-bid' => $data_last_bid,

                                    ), $bid);

                                echo CHtml::closeTag('a');
                            }

                            echo CHtml::closeTag('div');
                        echo CHtml::closeTag('div');
                    echo CHtml::closeTag('figure');
                echo CHtml::closeTag('div');
            }
        ?>
        
    </div>
</div>
<?php
    wp_enqueue_script('autobahn.min.js', '/js/autobahn.min.js', array('jquery'), null);
    wp_enqueue_script('wHomeQuotesSliderWidget.js', '/js/widgets/wHomeQuotesSliderWidget.js', array('jquery'), null);

    add_action('wp_footer',function(){ 
        $quotesSettings = QuotesSettingsModel::getInstance();
?>
<script>
!function( $ ) {
	var connUrl = '<?php echo $quotesSettings->wsServerUrl; ?>';
	var quotesKey = 'ALL';

    wHomeQuotesSliderWidgetOpen({
		selector: '.wHomeQuotesSlider',
		connUrl: connUrl,
		quotesKey: quotesKey,
		timeDataOffset: <?php echo $quotesSettings->realQuotesDataTimeDifference;?>,
		lang: '<?php echo DecommentsPostsModel::getCatLangSuffix();?>',
	});
}( window.jQuery );
</script>
<?php }, 100); ?>