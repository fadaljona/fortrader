<?php $catId = 14205;?>
<div class="fx_box-half fx_latest-right">
    <div class="fx_half-header clearfix">
        <span class="fx_half-header--left"><?php _e("News", 'ForTraderMaster'); ?></span>
        <a href="<?php echo get_category_link($catId);?>" class="fx_half-header--right"><?php _e("All news", 'ForTraderMaster'); ?></a>
    </div>
<?php
    $r = new WP_Query(array(
        'post_status'  => 'publish',
        'post_type' => 'post',
        'orderby' => 'date',
        'order'   => 'DESC',
        'posts_per_page' => 6,
        'cat' => $catId,
        'paged' => 1,
    ));
    if ($r->have_posts()) {
        while ($r->have_posts()) {
            $r->the_post();
            echo '<a href="'.get_the_permalink().'" class="fx_half-right-item">'.get_the_title().'</a>';
        }
    }
?>
</div>