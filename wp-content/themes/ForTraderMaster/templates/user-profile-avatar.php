<?php wp_enqueue_script('wUserProfileWidget.js', '/services/js/widgets/forms/wUserAvatarFromWpAdminFormWidget.js', array('jquery'), null); ?>

<h2><?php _e("Member Avatar", 'ForTraderMaster'); ?></h2>
<div class="wUserProfileWidget">
	<span class="profile-picture wAvatarHolder" style="display:inline-block;">
		<?=$model->getHTMLMiddleAvatar( Array( 'class' => 'wAvatarImage' ));?>
	</span>
	<?$form = $appController->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data', 'style' => "margin:0", )))?>
	<?=$form->hiddenField( $avatarFormModel, 'ID' )?>
	<?=$form->fileField( $avatarFormModel, 'avatar', Array( 'class' => "wAvatar", 'style' => 'position:absolute;height:0;width:0;margin:0;padding:0;border:0;' ))?>
		<iframe name="iframeForAvatar" id="iframeForAvatar" style="display:none"></iframe>
    <?$appController->endWidget()?>
    <a href="#" class="page-title-action wChange"><?php _e("Change Avatar", 'ForTraderMaster'); ?></a>
</div>
<?php
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lDeleting' => 'Deleting...',
		'lSaved' => 'Saved',
		'lError' => 'Error!',
		'lDeleteConfirm' => 'Delete?',
		'lNoFile' => 'No file ...',
		'lChoose' => 'Choose',
		'lChange' => 'Change',
		'lSet' => 'Set avatar',
		'lChange' => 'Change avatar',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<script>
var nsActionView = {};
var yiiBaseURL = "/services";
jQuery(document).ready(function($) {
	!function( $ ) {
        var ns = nsActionView;
        var ls = <?=json_encode( $jsls )?>;
		ns.wUserAvatarFormWidget = wUserAvatarFromWpAdminFormWidgetOpen({
            ajax: true,
            ls: ls,
			selector: '.wUserProfileWidget',
			idUser: <?=Yii::app()->user->id?>,
			ajaxDeleteURL: '<?=Yii::app()->createUrl('user/ajaxDeleteAvatar')?>',
			ajaxSubmitURL: '<?=Yii::app()->createUrl('user/iframeUploadAvatar')?>',
		});
	}( window.jQuery );

});
</script>