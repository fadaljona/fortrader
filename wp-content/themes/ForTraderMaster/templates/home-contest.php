<?php
	$contests = ContestModel::model()->findAll(Array(
		'condition' => " `t`.`status` = 'registration' or `t`.`status` = 'started' ",
		'order' => " `t`.`createdDT` DESC",
		'limit' => 1,
	));
	if( !$contests ){
		$contests = ContestModel::model()->findAll(Array(
			'condition' => " `t`.`status` = 'completed' AND DATE_ADD( `t`.`end`, INTERVAL 7 DAY ) > NOW() ",
			'order' => " `t`.`end` DESC",
			'limit' => 1,
		));
	}
	foreach( $contests as $contest ){
		$singleUrl = str_replace('/services', '', $contest->singleUrl);
		if( $contest->status == 'registration' ){
			if( strtotime( $contest->begin ) <= time() ){
				$contest->status = 'started';
			}else{
				$days = (( strtotime( $contest->begin ) - time() + 3600*24 ) - ( strtotime( $contest->begin ) - time() ) % (3600*24))/(3600*24);
				$title = __("Prior to the contest", 'ForTraderMaster') . ': <span>'.$days.' '.russianDays($days).'</span>';
				$contestLink = '<a href="'.$singleUrl.'#contestRegistrationForm" class="registration_btn">'.__("Registration", 'ForTraderMaster').'</a>';
			}
		}
		if( $contest->status == 'started' ){
			$days = (( strtotime( $contest->end ) - time() + 3600*24 ) - ( strtotime( $contest->end ) - time() ) % (3600*24))/(3600*24);
			$title =  __("Before the end of the contest", 'ForTraderMaster') . ': <span>'.$days.' '.russianDays($days).'</span>';
			$contestLink = '<a href="'.$singleUrl.'" class="registration_btn">'.__("Look contest", 'ForTraderMaster').'</a>';
		}
		if( $contest->status == 'completed' ){
			$title = __("Contest completed", 'ForTraderMaster');
			$contestLink = '<a href="'.$singleUrl.'" class="registration_btn">'.__("Look contest results", 'ForTraderMaster').'</a>';
		}
?><section>
	<h2 class="page_title1">
		<?php _e("Contest", 'ForTraderMaster'); ?> <a href="<?php echo $singleUrl;?>" class="link_accent">«<?php echo $contest->currentLanguageI18N->name;?>»</a>
		<i class="icon trophy_icon"></i>
	</h2>
	<hr class="separator1">
	<h3 class="competition_title">
		<?php echo $title;?>
	</h3>
	<div class="competition_box clearfix">
		<div class="competition_img alignleft">
			<a href="<?php echo $singleUrl;?>">
			<?php renderImg( p75GetServicesThumbnail($contest->getSrcImage(), 365, 350 ), 365, 350, $contest->currentLanguageI18N->name, 1 );?>
			</a>
		</div>
		<div class="competition_description">
			<div class="competition_description_title">
				<h5><?php _e("The prize fund", 'ForTraderMaster'); ?></h5>
				<hr>
				<span>$<?php echo CommonLib::numberFormat( $contest->sumPrizes )?></span>
				<hr>
				<p><span><? echo CommonLib::numberFormat( $contest->getCountPrizes() )?></span> <?php _e("Prize places", 'ForTraderMaster'); ?></p>      
				<?php echo $contestLink;?>
				<div> <i class="icon trophy_icon"></i></div>
			</div><!-- / .competition_description_title -->
		</div><!-- / .competition_description -->
	</div><!-- / .clearfix -->
	<p>
		<?php echo $contest->currentLanguageI18N->getHTMLDescription();?>
	</p>
	<hr class="separator2">
</section>
	<?php }?>