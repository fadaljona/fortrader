<?php
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 4,
		'post__in' => explode(',', $model->wpPosts),
	));
	if ($r->have_posts()){
	
?>
<!-- - - - - - - - - - - - - - Financial news - - - - - - - - - - - - - - - - -->
<section class="section_offset">
	<hr class="separator2">
	<h3><?php _e("NEWS", 'ForTraderMaster'); ?> <?=$model->brandName?></h3>
	<hr>
	<div class="post_box">
		<?php
		$i=1;
		while ( $r->have_posts() ){ 
			$r->the_post();
			if( $i % 2 == 1 ) echo '<div class="post_col2 post_col_sm1">';
				get_template_part( 'templates/loop-post-small-no-border' );
			if( $i % 2 == 0 ) echo '</div><!--/ .post_col -->';
			$i++;
		}
		if( ($i-1) %2 !=0 ) echo '</div><!--/ .post_col -->';
		?>
	</div>
</section>
<!-- - - - - - - - - - - - - - End of Financial news - - - - - - - - - - - - - - - - -->
<?php }; ?>