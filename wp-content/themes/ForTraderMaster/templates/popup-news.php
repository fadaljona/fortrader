<?php
	$posts_per_page = 8;
	$current_date1 = new DateTime("now");
	$currentdate1=$current_date1->format('Y-m-d H:i:s');
	$r = new WP_Query( array(
		'post_status' => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => $posts_per_page,
		'meta_query' => array(
		'relation' => 'AND',
			array(
				'key'     => 'important',
				'value'   => '1',
				'compare' => '=',
			),
			array(
				'key'     => 'daylife',
				'value'   => $currentdate1,
				'compare' => '>',
			),		
		),
	));
	if ($r->have_posts()) :
?>
<div class="hide">
	<div id="more_news" class="modal_box1">
		<div class="box-modal_close arcticmodal-close"><i class="fa fa-times icon-close-modal-news"></i></div>
		<div class="modal-news-attention-box"><img src="<?php echo removeProtocol( get_bloginfo('template_url') ); ?>/images/attention.png" class="modal-news-attention-img" height="128" width="202" alt=""></div>
		<h4 class="modal-news-title"><?php _e("News that may interest you!", 'ForTraderMaster'); ?></h4>
		<div class="modal_news_container clearfix">
		<?php 
			$i=0;
			while ( $r->have_posts() ) : $r->the_post(); 
				if( $i % 2 == 0 ) echo '<div class="clear"></div>';
				get_template_part( 'templates/loop-post-popup-news' );
				$i++;
			endwhile;
			$max_page = $r->max_num_pages;
			$pagesToShow = 5;
			if( $max_page < $pagesToShow ) $pagesToShow = $max_page;
		?>
		</div> <!--/ modal_news_container  -->
		<?php if( $max_page > 1 ): ?>
		<div class="align_center">
			<div data-max-page="<?php echo $max_page;?>" data-pages-to-show="<?php echo $pagesToShow;?>" data-posts-per-page="<?php echo $posts_per_page;?>" class="navigation_box_2 navigation_box_modal_news  clearfix">
				<a href="#" data-act="first" class="first hide"><?php _e("First", 'ForTraderMaster'); ?></a>
				<a href="#" data-act="prev" class="hide nav_list_my_prev prev">
					<span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span>
				</a>
				
				
				<div class="box3 align_center">
					<ul class="nav_list_2 nav_list_my_style">
						<li class="current"><a data-act='1' href="#">1</a></li>
						<?php 
							for( $i=2; $i <= $pagesToShow; $i++  ){
								echo "<li><a data-act='{$i}' href='#'>{$i}</a></li>";
							}
						?>
					</ul>
				</div>
				<a href="#" data-act='next' class=" nav_list_my_next next">
					<span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span>
				</a>
				<?php if( $pagesToShow < $max_page ){?><a href="#" data-act='last' class="last"><?php _e("Last", 'ForTraderMaster'); ?></a><?php } ?>
				
			</div><!--/ navigation_box_2 -->
		</div>
		<?php endif; ?>
		<div class="spinner-wrap hide">
			<div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>