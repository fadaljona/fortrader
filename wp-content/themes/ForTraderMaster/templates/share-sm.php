<?php 
    $shareUrl = 'https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    
    global $smShareL2ButtonsJs;
    $smShareL2ButtonsJs="
<script>
var windowClosed = 0;
var sm = '';

function CreateWindow( url ){
    windowClosed = 0;
    wnd=window.open( url, 'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no' );
    timerTop = setTimeout('checkWindow( )', 1000);
}
function checkWindow( ){
    if( wnd.closed == true && windowClosed == 0 ){
        windowClosed = 1;
        return true;
    }
    timerTop = setTimeout('checkWindow( sm )', 1000);
}

!function( $ ) {
    
    $('.fx_social-item .fx_facebook').click(function(e){
		CreateWindow( 'https://www.facebook.com/sharer/sharer.php?sdk=joey&u=$shareUrl&display=popup&ref=plugin&src=share_button' );
		sm = 'fb';
		shareVkFacebookClick();
		return false;
    });
    $('.fx_social-item .fx_twitter').click(function(){
		CreateWindow( 'http://twitter.com/share?url=$shareUrl'  );
		sm = 'tw';
		anyLikesClick();
		return false;
	});
    $('.fx_social-item .fx_gplus').click(function(){
		CreateWindow( 'http://plus.google.com/share?url=$shareUrl'  );
		sm = 'gp';
		anyLikesClick();
		return false;
    });
    $('.fx_social-item .fx_linkedin').click(function(){
		CreateWindow( 'https://www.linkedin.com/shareArticle?url=$shareUrl'  );
		sm = 'in';
		anyLikesClick();
		return false;
	});
}( window.jQuery );
</script>";

    add_action('wp_footer', function(){
        global $smShareL2ButtonsJs;
        echo $smShareL2ButtonsJs;
    }, 100);
?>
<div class="fx_section">
    <div class="fx_content-title clearfix">
        <span class="fx_content-title-link fx_social-title">Share on social networks</span>
    </div>
    <div class="fx_section-box fx_socila-wr clearfix">
        <div class="fx_social-item">
            <a href="#" class="fx_social-btn fx_gplus">Google+</a>
        </div>

        <div class="fx_social-item">
            <a href="#" class="fx_social-btn fx_facebook">facebook</a>
        </div>

        <div class="fx_social-item">
            <a href="#" class="fx_social-btn fx_twitter">twitter</a>
        </div>

        <div class="fx_social-item">
            <a href="#" class="fx_social-btn fx_linkedin">linkedin</a>
        </div>

    </div>

</div>