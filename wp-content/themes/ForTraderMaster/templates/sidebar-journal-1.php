<!-- - - - - - - - - - - - - - sidebarJournal=1 - - - - - - - - - - - - - - - - -->
<?php
	$r = new WP_Query( array(
	'post_status' => 'publish',
	'post_type' => 'post',
	'posts_per_page' => 4,
	'meta_query' => array(
			array(
				'key'     => 'sidebarJournal',
				'value'   => '1',
				'compare' => '=',
			),
		),
	));
	if ($r->have_posts()) :
		while ( $r->have_posts() ) : $r->the_post();
			get_template_part( 'templates/loop-post-big' );
		endwhile;
		wp_reset_postdata();
	endif;
?>
<!-- - - - - - - - - - - - - - End of Post Big - - - - - - - - - - - - - - - - -->