<div class="footer_middle clearfix">
                
                <div class="list_box">
                    
                    <h6>Аналитика и прогнозы</h6>

                    <!-- - - - - - - - - - - - - - List 3 - - - - - - - - - - - - - - - - -->

                    <ul class="list3">

                        <li><a href="http://fortrader.org/fundamental/prognoz-forex-segodnya/">Форекс прогноз сегодня</a></li>
                        <li><a href="http://fortrader.org/fundamental/tehnicheskiy-analiz/">Технический анализ</a></li>
                        <li><a href="http://fortrader.org/fundamental/forex-prognoz-kommentrii/">Комментарии экспертов</a></li>
                        <li><a href="http://fortrader.org/fundamental/forex-news/">Форекс новости</a></li>
                        <li><a href="http://fortrader.org/fundamental/news/">Экономические новости</a></li>
                        <li><a href="http://fortrader.org/opinion/">В курсе рынка</a></li>
                        <li><a href="http://fortrader.org/fundamental/kurs-rublya/">Курс рубля сегодня</a></li>
                        <li><a href="http://fortrader.org/fundamental/kurs-dollara-prognoz/">Прогноз курса доллара</a></li>
                        <li><a href="http://fortrader.org/fundamental/kurs-euro-prognoz/">Прогноз курса евро</a></li>
                        <li><a href="http://fortrader.org/fundamental/cena-neft-prognoz/">Цена на нефть</a></li>
                        <li><a href="http://fortrader.org/fundamental/cena-na-zoloto-kurs-segodnya-dinamika-prognoz.html">Золото прогноз</a></li>

                    </ul>

                    <!-- - - - - - - - - - - - - - End of List 3 - - - - - - - - - - - - - - - - -->

                </div><!-- / .list_box -->

                <div class="list_box">
                    
                    <h6>Новичку на Форекс</h6>

                    <!-- - - - - - - - - - - - - - List 3 - - - - - - - - - - - - - - - - -->

                    <ul class="list3">

                        <li><a href="http://fortrader.org/forex-for-beginers/foreks-nachalo-kak-stat-trejderom-za-6-shagov.html">С чего начать?</a></li>
                        <li><a href="#">Основы рынка Форекс</a></li>
                        <li><a href="http://fortrader.org/forex-strategy/simple-strategy/">Простые стратегии</a></li>
                        <li><a href="http://fortrader.org/forex-knigi/">Книги форекс</a></li>
                        <li><a href="http://fortrader.org/forex-video/">Видео форекс</a></li>
                        <li><a href="http://fortrader.org/birzhevoj-slovar/">Словарь терминов</a></li>
                        <li><a href="http://fortrader.org/learn/brokers-forex-trading/">О форекс брокерах</a></li>
                        <li><a href="http://fortrader.org/currency/">Валюты на форекс</a></li>
			<li><a href="/voprosy-i-otvety/">Вопросы и ответы</a></li>
			<li><a href="/ask-question.html">Задать вопрос</a></li>

                    </ul>

                    <!-- - - - - - - - - - - - - - End of List 3 - - - - - - - - - - - - - - - - -->

                </div><!-- / .list_box -->

                <div class="list_box">
                    
                    <h6>Практику трейдинга</h6>

                    <!-- - - - - - - - - - - - - - List 3 - - - - - - - - - - - - - - - - -->

                    <ul class="list3">

                        <li><a href="/learn/">Мастер-классы</a></li>
                        <li><a href="/indicators-forex/">Индикаторы форекс MT4</a></li>
                        <li><a href="/indicator-forex-metatrader5/">Индикаторы форекс MT5</a></li>
                        <li><a href="/forex-strategy/">Стратегии форекс</a></li>
                        <li><a href="/forex-ea-testing/">Советники Форекс</a></li>
                        <li><a href="/learn/mql/">Программирование MQL</a></li>
                        <li><a href="/economic-forex/">Макроэкономические показатели</a></li>
                        <li><a href="/price-pattern-teh-analyse/">Фигуры теханализа</a></li>
                        <li><a href="/articles_forex/">Форекс статьи</a></li>
                        <li><a href="/pamm-investment/">ПАММ инвестиции</a></li>
                        <li><a href="/binary-options/">Бинарные опционы</a></li>
                    </ul>

                    <!-- - - - - - - - - - - - - - End of List 3 - - - - - - - - - - - - - - - - -->

                </div><!-- / .list_box -->

                <div class="list_box">
                    
                    <h6>Брокеры форекс</h6>

                    <!-- - - - - - - - - - - - - - List 3 - - - - - - - - - - - - - - - - -->

                    <ul class="list3">

                        <li><a href="/brokersrating">Рейтинг форекс брокеров</a></li>
                        <li><a href="/forex-brokers-news/">Брокеры Форекс: новости, события, факты</a></li>
                        <li><a href="/learn/brokers-forex-trading/">О форекс брокерах</a></li>
                        <li><a href="/forex-regulators/">Регуляторы Форекс</a></li>
                        <li><a href="/inter/">Интервью с форекс брокером</a></li>
                        <li><a href="/currencyrates/archive">Архив курсов валют ЦБ РФ</a></li>

                    </ul>

                    <!-- - - - - - - - - - - - - - End of List 3 - - - - - - - - - - - - - - - - -->

                </div><!-- / .list_box -->

            </div>