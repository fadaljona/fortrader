<?php
	$freshPosts = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 14,
		'paged' => 1,
		'author' => $model->id,
		'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
	) );
	$popularPosts = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'meta_key' => 'views',
		'orderby' => 'meta_value_num',
		'order'   => 'DESC',
		'posts_per_page' => 14,
		'paged' => 1,
		'author' => $model->id,
		'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
	) );
	$mostCommentedPosts = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'comment_count',
		'order'   => 'DESC',
		'posts_per_page' => 14,
		'paged' => 1,
		'author' => $model->id,
		'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
	) );
	if( !$freshPosts->have_posts() && !$popularPosts->have_posts() && !$mostCommentedPosts->have_posts() ){
		echo '<!--no author posts-->';
	}else{
?>
<section class="section_offset marginTop20">
	<div class="inside-form__title"><?=Yii::t('*', 'Articles of the author')?></div>
	<div class="tabs_box section_offset">
		<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
		<ul class="tabs_list clearfix PFDregular">
			<?php 
				$freshMarker=1;
				if ($freshPosts->have_posts()){
					?><li class="active"><a href="javascript:;"><?php _e("Fresh", 'ForTraderMaster'); ?></a></li><?php 
				}else{ 
					$freshMarker=0; $popularCssClass="active"; 
				}
				if ($popularPosts->have_posts()){
					?><li class="<?php echo $popularCssClass; ?>"><a href="javascript:;"><?php _e("Popular", 'ForTraderMaster'); ?></a></li><?php 
				}elseif(!$freshMarker){
					$commentCssClass="active"; 
				} ?>
				<li class="<?php echo $commentCssClass;?>"><a href="javascript:;"><?php _e("Comments", 'ForTraderMaster'); ?></a></li>
		</ul>
		<!-- - - - - - - - - - - - - - End of Tabs List  - - - - - - - - - - - - - - - - -->
		<!-- - - - - - - - - - - - - - Tabs Contant - - - - - - - - - - - - - - - - -->
		<div class="tabs_contant">
			<!-- - - - - - - - - - - - - - Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<?php if ($freshPosts->have_posts()): ?>
			<div class="active fresh-posts">
			<?php 
				$i=0;
				while ( $freshPosts->have_posts() ) : $freshPosts->the_post(); 
					if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
					if( $i<2 ) get_template_part( 'templates/loop-post-big' );
					if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );
							
					if( $i==2 ) echo '<div class="post_box">';
					if( $i>1 ) get_template_part( 'templates/loop-post-col3' );
							
					if( $i>2 && ( ($i+1-2)%3 == 0 ) ) echo '<div class="clear"></div><hr>';
					
					if( $i>2 && ( ($i+1-2)%6 == 0 ) && $i<13 ) {echo '<div class="blockdiv1">'; dynamic_sidebar('banner-728x90'); echo '</div>';};
					
					$i++;
								
				endwhile;
				if($i<2) get_template_part( 'templates/loop-post-2-after' );
				if($i>2) echo '</div>';
				wp_reset_postdata();
			?>
				<a href="#" class="load_btn load-more-author-posts chench_btn" data-author="<?php echo $model->id;?>" data-type="fresh" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
			</div>
			<?php endif;?>
						
			<!-- - - - - - - - - - - - - - End of Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<!-- - - - - - - - - - - - - - Tabs Item 2 - - - - - - - - - - - - - - - - -->
			<?php if ($popularPosts->have_posts()): ?>
			<div class="<?php echo $popularCssClass; ?> popular-posts">
			<?php 
				$i=0;
				while ( $popularPosts->have_posts() ) : $popularPosts->the_post(); 
					if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
					if( $i<2 ) get_template_part( 'templates/loop-post-big' );
					if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );
							
					if( $i==2 ) echo '<div class="post_box">';
					if( $i>1 ) get_template_part( 'templates/loop-post-col3' );
							
					if( $i>2 && ( ($i+1-2)%3 == 0 ) ) echo '<div class="clear"></div><hr>';
					
					if( $i>2 && ( ($i+1-2)%6 == 0 ) && $i<13 ) {echo '<div class="blockdiv1">'; dynamic_sidebar('banner-728x90'); echo '</div>';};
					
					$i++;
								
				endwhile;
				if($i<2) get_template_part( 'templates/loop-post-2-after' );
				if($i>2) echo '</div>';
				wp_reset_postdata();
			?>
				<a href="#" class="load_btn load-more-author-posts chench_btn" data-author="<?php echo $model->id;?>" data-type="popular" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
			</div>
			<?php endif;?>

			<!-- - - - - - - - - - - - - - End of Tabs Item 2 - - - - - - - - - - - - - - - - -->
			<!-- - - - - - - - - - - - - - Tabs Item 3 - - - - - - - - - - - - - - - - -->
			<?php if ($mostCommentedPosts->have_posts()): ?>
			<div class="<?php echo $commentCssClass; ?> commented-posts">	
			<?php 
				$i=0;
				while ( $mostCommentedPosts->have_posts() ) : $mostCommentedPosts->the_post(); 
					if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
					if( $i<2 ) get_template_part( 'templates/loop-post-big' );
					if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );
							
					if( $i==2 ) echo '<div class="post_box">';
					if( $i>1 ) get_template_part( 'templates/loop-post-col3' );
							
					if( $i>2 && ( ($i+1-2)%3 == 0 ) ) echo '<div class="clear"></div><hr>';
					
					if( $i>2 && ( ($i+1-2)%6 == 0 ) && $i<13 ) {echo '<div class="blockdiv1">'; dynamic_sidebar('banner-728x90'); echo '</div>';};
					
					$i++;
								
				endwhile;
				if($i<2) get_template_part( 'templates/loop-post-2-after' );
				if($i>2) echo '</div>';
				wp_reset_postdata();
			?>
				<a href="#" class="load_btn load-more-author-posts chench_btn" data-author="<?php echo $model->id;?>" data-type="commented" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
			</div>
			<?php endif;?>
			<!-- - - - - - - - - - - - - - End of Tabs Item 3 - - - - - - - - - - - - - - - - -->
		</div>
		<!-- - - - - - - - - - - - - - End of Tabs Contant - - - - - - - - - - - - - - - - -->
	</div><!--/ .tabs_box -->
</section>
<?php }?>