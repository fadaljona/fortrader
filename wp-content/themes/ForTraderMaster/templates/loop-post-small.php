<figure class="post_small">
	<a href="<?php the_permalink();?>">
		<?php renderImg( p75GetThumbnail($post->ID, 100, 70, ""), 100, 70, get_the_title(), 1 );?>
	</a>
	<figcaption>
		<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
		<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d F, H:i',  get_the_date('Y-m-d H:i') ); ?></time><?php } ?>
	</figcaption>
</figure>