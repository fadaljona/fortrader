<section class="section_offset">
	<?php
		$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 4,
		'paged' => 1,
		'tax_query' => array(
			array(
				'taxonomy' => 'quotes',
				'field'    => 'term_id',
				'terms'    => $symbol->wpTerm,
			),
		),
		'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
	) );
		$max_num_pages = $r->max_num_pages;
		if ($r->have_posts()) :
	?>
	<h3 class="page_title2"><?php
	if( $symbol->wpPostsTitle ){
		echo $symbol->wpPostsTitle;
	}else{
		$symName = $symbol->nalias ? $symbol->nalias : $symbol->name;
		echo Yii::t( '*', 'Published with the tag' ) . ' ' . $symName;
	}
	?></h3>
	<div class="load-more-quotes-tags-posts-42-wrap">
	<?php $i=0;
		while ( $r->have_posts() ) : $r->the_post();
			if( $i==0 ) get_template_part( 'templates/loop-post-4-befor-no-hr' );
			if( $i<6 ) get_template_part( 'templates/loop-post-4' );
			if( (($i+1)%2)==0 ) echo '<div class="clear"></div>';
			if( $i==5 ) get_template_part( 'templates/loop-post-4-after' );
			$i++;
		endwhile;
		if( $i<6 )get_template_part( 'templates/loop-post-4-after' );
	?>
	</div>
	<?php if( $max_num_pages > 1 ){ ?>
		<a href="#" class="load_btn load-more-quotes-tags-posts-42 chench_btn" data-term="<?=$symbol->wpTerm?>" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>"  data-page="1" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
	<?php }?>
	<?php
					
		wp_reset_postdata();
	endif;
	?>
</section>