<?php
	$blockCategoryId = 1933;
	$subCategories = get_categories( array( 'parent' => $blockCategoryId ) );
?>
<section class="section_offset turn_box">
	<div class="clearfix position-relative">
		<h3 class="page_title2 alignleft"><?php _e("Trading strategies", 'ForTraderMaster'); ?></h3>
		<!-- - - - - - - - - - - - - - Nav Buttons - - - - - - - - - - - - - - - - -->
		<div class="nav_buttons alignright">
			<a href="#" class="turn_btn"><span class="tooltip"><?php _e("Minimize", 'ForTraderMaster'); ?></span></a>
			<a href="#" class="prev_btn"><span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span></a>
			<a href="#" class="next_btn"><span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span></a>
		</div>
		<!-- - - - - - - - - - - - - - End of Nav Buttons - - - - - - - - - - - - - - - - -->
	</div>
	<hr>
	<!-- - - - - - - - - - - - - - Turn Content - - - - - - - - - - - - - - - - -->
	<div class="turn_content clearfix">
		<div class="beginner_right">
			<div class="carousel-wrapper">
			<div class="post_carousel owl-carousel"  data-desktop="2" data-large="2" data-medium="1" data-small="1" data-extra-small="1" data-currentitem="0" data-maximumitem="1" data-type="strategies" data-offset="6">
			<?php
				$r = new WP_Query( array(
					'post_status'  => 'publish',
					'post_type' => 'post',
					'orderby' => 'date',
					'order'   => 'DESC',
					'posts_per_page' => 2,
					'cat' => $blockCategoryId,
				) );
				?>
				<?php
					$i=0;
					$excludePostArr = array();
					if ($r->have_posts()) :
					while ( $r->have_posts() ) : $r->the_post(); ?>
				<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
					<figure class="post1 clearfix">
						<a href="<?php the_permalink();?>">
							<?php renderImg( p75GetThumbnail($post->ID, 168, 154, ""), 168, 154, get_the_title(), 1 );?>
						</a>
						<figcaption>
							<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
						</figcaption> 
					</figure>
				</div>
				<?php 
						$i++;
						$excludePostArr[] = $post->ID;
					endwhile;
					wp_reset_postdata();
					endif;
				?>
			</div><!-- / .post_carousel -->
			</div>
		</div><!-- / .beginner_right -->
		<div class="beginner_left">
			<!-- - - - - - - - - - - - - - List 2 - - - - - - - - - - - - - - - - -->
			<?php
				if( count($subCategories) ){
			?>
				<ul class="list2">
					<?php foreach( $subCategories as $subCategory ){ ?>
					<li><a href="<?php echo get_category_link($subCategory->cat_ID);?>"><?php echo $subCategory->name;?> <span class="red_color">(<?php echo $subCategory->count;?>)</span></a></li>
					<?php } ?>
				</ul>
			<?php } ?>
			<!-- - - - - - - - - - - - - - End of List 2 - - - - - - - - - - - - - - - - -->
		</div><!-- / .beginner_left -->
	</div><!-- / .turn_content -->
	<div class="post_box">
		<?php
			$r = new WP_Query( array(
				'post_status'  => 'publish',
				'post_type' => 'post',
				'orderby' => 'date',
				'order'   => 'DESC',
				'posts_per_page' => 4,
				'cat' => $blockCategoryId,
				'post__not_in' => $excludePostArr,
			) );
			if ($r->have_posts()) :
				while ( $r->have_posts() ) : $r->the_post(); 
					get_template_part( 'templates/loop-post-col4' );
				endwhile;
				wp_reset_postdata();
			endif;
		?>
	</div><!-- / .post_box -->
	<!-- - - - - - - - - - - - - - End of Turn Content - - - - - - - - - - - - - - - - -->
</section>