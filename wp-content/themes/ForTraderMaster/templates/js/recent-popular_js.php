<script>
;(function($){
	var newsSliderJs = function() {
		var self = {
			init:function() {
				self.firstOpenNewsList();
				self.newsList();
			},
			firstOpenNewsList : function(){
				if(('.news_slider').length){
					$('.news_slider .news_list>li:nth-child(1),.news_slider .news_list>li:nth-child(2),.news_slider .news_list>li:nth-child(3),.news_slider .news_list>li:nth-child(4),.news_slider .news_list>li:nth-child(5)').addClass('active');
				}
			},
			newsList : function(){
				$('.slider_nav_prev').on('click',function(){
					var parent = $(this).parents('.news_slider'),
						firstItem = parent.find('.news_list>li.active').index(),
						showItem = parent.find(".news_list>li.active").eq(0).prev();
					if(firstItem == 0){return false}
					parent.find(".news_list>li.active").eq(4).slideUp(300).removeClass("active");
					showItem.slideDown(300).addClass('active');

				});
				$('.slider_nav_next').on('click',function(){
					var parent = $(this).parents('.news_slider'),
						last = parent.find('.news_list>li:last').index(),
						lastActive = parent.find(".news_list>li.active").eq(4).index(),
						showItem = parent.find(".news_list>li.active").eq(4).next();
					if(last == lastActive){return false}
					parent.find(".news_list>li.active").eq(0).slideUp(300).removeClass("active");;
					showItem.slideDown(300).addClass('active');
				});
			},
		};
		self.init();
		return self;
	}
	newsSliderJs();
})(jQuery);
</script>