<script>
;(function($){
	var dsplayMainMenu = function() {
		var self = {
			init:function() {
				self.additionalMenuItem();
				$(window).on("resize",function(){
					self.windowResize();	
                });
                $(window).on("load",function(){
					self.windowLoad();
				});
			},
			windowLoad: function(){
				setTimeout(function(){
					self.additionalMenuWidth();
				},500);
			},
			additionalMenuItem : function(){
				var navWrapWidth = $(".nav_wrap").width(),
					fullWidth = navWrapWidth - 162,
					itemWidth = 0,
					navWidth;

				if(!$('.nav_wrap').hasClass("responsive_menu") && $(window).width()<=767){
					$(".additional_menu>li").each(function(){
						$(this).appendTo(".main_menu");
					});
					$(".main_menu>li").each(function(){
						var $this = $(this);
						if ($(this).is(':has(ul)')){
							$this.addClass('has_submenu');
						};
						$(this).appendTo(".additional_menu");
					});
					$('.nav_wrap').addClass("responsive_menu clearfix");
				}
				else if($(window).width()>=768){
					$('.nav_wrap').removeClass("responsive_menu clearfix")
					$("nav").css({"width":"auto"});
					$(".additional_menu>li").each(function(){
						$(this).appendTo(".main_menu");
					});
					$(".main_menu>li").each(function(){
						var $this = $(this);
						itemWidth += $(this).width();
						if(itemWidth > fullWidth){
							$this.appendTo(".additional_menu");
						}				
					});
					setTimeout(function(){
						navWidth = $(".main_menu").width();
						$("nav").width(navWidth +60);
					},500);
				}
				$(".nav_wrap").addClass('show');
			},
			windowResize: function(){
				self.additionalMenuItem();
				setTimeout(function(){
					self.additionalMenuWidth();
				},500);
			},
			additionalMenuWidth : function(){
				var searchFormWidth = $('.search_box').width(),
					menuWidh = searchFormWidth + 62;
				$(".additional_menu").width(menuWidh);
			},
		};
		self.init();
		return self;
	}
	dsplayMainMenu();
})(jQuery);
</script>