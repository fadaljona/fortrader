<?php
    wp_enqueue_script('timer.jquery.plugin.js', get_bloginfo("stylesheet_directory") . '/js/timer/jquery.plugin.js', array('jquery'), null);
    wp_enqueue_script('timer.jquery.countdown.js', get_bloginfo("stylesheet_directory") . '/js/timer/jquery.countdown.js', array('jquery'), null);
    
    $timezone = 'Europe/Moscow';
	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	
    $begin = date( "Y-m-d H:i:s", time());
    
    loadYii(); 
    $calendarEvents = CalendarEvent2ValueModel::model()->findAll(Array(
		'condition' => " `t`.`dt` >= :begin ",
		'order' => " `t`.`dt` ",
		'with' => array(
			'event' => array(
				'with' => array( 'country' )
			)
		),
		'params' => Array(
			':begin' => $begin,
		),
		'limit' => 5,
    ));
    
    global $calendarEventsJs;
	$calendarEventsJs = '';
?>
<div class="fx_section">

    <div class="fx_content-title">
        <a href="#" class="fx_content-title-link"><?php echo Yii::t('*', 'Economic calendar');?></a>
        <span class="fx_content-date"><?php echo Yii::app()->dateFormatter->format( '/ EEEE dd / LLLL / hh:mm', time()) . strtolower( Yii::app()->dateFormatter->format( 'a', time()) );?></span>
        <img src="<?php echo Yii::app()->params['wpThemeUrl'];?>/images/fx_icon-calendar.png" alt="" class="fx_title-icon">
    </div>

    <div class="fx_section-box clearfix">
        <div class="fx_calendar-left">
            <div class="fx_half-title"><?php echo Yii::t('*', 'Upcoming events');?></div>
            
            <div class="fx_rating-list-wr fx_calendar">
            <?php
                $timeList = '';
                foreach( $calendarEvents as $eventVal ){
                    $seriousClass = '';

                    if( $eventVal->serious == 3 )
                        $seriousClass = 'red';
                    elseif( $eventVal->serious == 2 )
                        $seriousClass = 'orange';
                    else
                        $seriousClass = 'green';

                    $seriousBorderClass = '';
                    if( $seriousClass ) $seriousBorderClass = 'fx_border-' . $seriousClass;
                    echo CHtml::openTag('div', array('class' => 'fx_rating-item fx_border-left clearfix ' . $seriousBorderClass));
                        echo CHtml::openTag('div');
                            echo CHtml::openTag('a', array('class' => 'fx_rating-subitem fx_banks-item', 'href' => $eventVal->event->singleURL));
                                echo CHtml::tag('span', array(), $eventVal->event->indicatorName);
                            echo CHtml::closeTag('a');
                        echo CHtml::closeTag('div');
                    echo CHtml::closeTag('div');


                    $seriousTimeClass = '';
                    $seriousTimeImg = '';
                    if( $seriousClass ) $seriousTimeClass = 'fx_time-' . $seriousClass;
                    if( $seriousClass ) $seriousTimeImg = '<img src="'.Yii::app()->params['wpThemeUrl'].'/images/fx_icon-clock-'.$seriousClass.'.png" alt="">';

                    $time = strtotime( $eventVal->dt );
					$time -= time();
                    $timeStr = gmdate("H", $time).':'.gmdate("i", $time).':'.gmdate("s", $time);
                    
                    $calendarEventsJs .= "
                        <script>
                            jQuery(document).ready(function($) {
                                var endDate{$eventVal->id} = new Date();
                                endDate{$eventVal->id}.setSeconds(endDate{$eventVal->id}.getSeconds()+$time);
                                $('.event_time{$eventVal->id}').countdown({until: endDate{$eventVal->id}, compact: true, description: '', format: 'HMS'});
                            });
                        </script>
                    ";

                    $timeList .= CHtml::openTag('div', array('class' => 'fx_rating-item'));
                        $timeList .= CHtml::openTag('div', array('class' => 'fx_cal-time-item ' . $seriousTimeClass));
                            $timeList .= CHtml::tag('span', array( 'class' => 'event_time' . $eventVal->id ), $timeStr);
                            $timeList .= $seriousTimeImg;
                        $timeList .= CHtml::closeTag('div');
                    $timeList .= CHtml::closeTag('div');

                }
            ?>
                
            </div>
        </div>

        <div class="fx_calendar-right">              
            <div class="fx_half-title"><?php echo Yii::t('*', 'Time to start');?></div>

            <div class="fx_rating-list-wr fx_cal-wr">
                <?php echo $timeList;?>
   
            </div>

        </div>

    </div>

</div>
<?php
	add_action('wp_footer',function(){
        global $calendarEventsJs;
		echo $calendarEventsJs;
    }, 100);
?>