<section class="section_offset">
	<h3 class="page_title2">
		<?php _e("Interesting tags", 'ForTraderMaster'); ?>
		<span class="sub_title"><?php _e("Choose topic", 'ForTraderMaster'); ?></span>
		<i class="icon folder_icon"></i>
	</h3>
	<hr>
		<div class="categories_box clearfix">
				<a data-tag-page="1" data-tag-id="457" class="categories_btn tag-load-link" href="/tag/profit-strategy/">Прибыльные</a>
				<a data-tag-page="1" data-tag-id="395" class="categories_btn tag-load-link" href="/tag/best-strategy/">Лучшие</a>
				<a data-tag-page="1" data-tag-id="2073" class="categories_btn tag-load-link" href="/tag/short-term-strategy/">Краткосрочные</a>
				<a data-tag-page="1" data-tag-id="10893" class="categories_btn tag-load-link" href="/tag/intraday-forex-strategy/">Внутридневные</a>
				<a data-tag-page="1" data-tag-id="2590" class="categories_btn tag-load-link" href="/tag/long-term-forex-strategy/">Долгосрочные</a>
				<a data-tag-page="1" data-tag-id="11029" class="categories_btn tag-load-link" href="/tag/wihtout-indicators-strategy/">Без индикаторов</a>
				<a data-tag-page="1" data-tag-id="11542" class="categories_btn tag-load-link" href="/tag/tehnical-analisys-strategy/">По фигурам теханализа</a>
				<a data-tag-page="1" data-tag-id="330" class="categories_btn tag-load-link" href="/tag/renko-strategy/">Ренко</a>
				<a data-tag-page="1" data-tag-id="2630" class="categories_btn tag-load-link" href="/tag/martingale-strategy-forex/">Мартингейл</a>
				<a data-tag-page="1" data-tag-id="719" class="categories_btn tag-load-link" href="/tag/elliott-waves-strategy/">Волны Эллиотта</a>
				<a data-tag-page="1" data-tag-id="627" class="categories_btn tag-load-link" href="/tag/price-action-strategy/">Price Action</a>
			</div>
		<hr>
	<!-- - - - - - - - - - - - - - Post Box - - - - - - - - - - - - - - - - -->
	<div class="post_box interesting-tags-wrapper">

	</div><!--  / .post_box -->
	<a data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" class="load_btn tag-load-link chench_btn" href="#"></a>
	<!-- - - - - - - - - - - - - - End of Post Box - - - - - - - - - - - - - - - - -->
</section>
