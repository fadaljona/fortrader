<!--Everything for beginners-->
<?php
$r = new WP_Query(array(
    'post_status' => 'publish',
    'post_type' => 'post',
    'orderby' => 'date',
    'order'   => 'DESC',
    'posts_per_page' => 12,
    'meta_query' => array(
        array(
            'key'     => 'for_crypto_currencies_beginer',
            'value'   => '1',
            'compare' => '=',
        ),
    ),
));
if ($r->have_posts()) {
?>
<div class="fx_section" id="cryptoCurrenciesBeginer">
    <div class="fx_content-title clearfix">
        <h3 class="fx_content-title-link"><?php _e("Everything for beginners", 'ForTraderMaster'); ?></h3>
        <img src="<?php echo get_bloginfo('template_directory');?>/images/junior_icon.png" alt="" class="fx_title-icon">
    </div>

    <div class="fx_section-box">
        <div class="fx_tag-box clearfix">
        <?php
        while ($r->have_posts()) {
            $r->the_post();
            echo '<a href="'.get_the_permalink().'" class="fx_tag-item"><span>'.get_the_title().'</span></a>';
        }
        ?>
        </div>
    </div>
</div>
<?php
}
?>