<?php 
    loadYii(); 
    Yii::import( 'components.widgets.WebRatingWidget' );

    wp_enqueue_script('wCommonRatingWidget.js', CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wCommonRatingWidget.js' ), array('jquery'), null);

    add_action('wp_footer',function(){
?>
<script>
var topAccountsRatingSidebarWidget;
;(function($){
	topAccountsRatingSidebarWidget = wCommonRatingWidgetOpen({
        selector: '.topAccountsRatingSidebar',
        modelName: 'ContestMember',
        cutStrToGetId: 'top-account-sideber-id',
        ajaxAddMarkURL: '<?php echo Yii::app()->createUrl('commonRating/ajaxAddMark')?>'
	});
})(jQuery);
</script>
<?php
    }, 100);
?>
<div class="fx_sb-section topAccountsRatingSidebar">
    <div class="fx_sb-title"><span><?php echo Yii::t('*', "Top Accounts"); ?></span></div>

    <div class="fx_sb-section-box">
                        
    <?php
            $c = new CDbCriteria;
            $c->with['stats'] = array();
            $c->with['server'] = array();
            $c->with['mt5AccountInfo'] = array();
            $c->with['commonRatingStats'] = array();

            $c->addCondition( ' `t`.`idContest` = 0 ' );
            $c->order = ' `stats`.`gain` DESC ';
            $c->limit = 10;

            $monitoringSettings = MonitoringSettingsModel::getModel();
            $inactiveDaysCondition = '';
            /*if( $monitoringSettings->inactiveDays ){
                $inactiveDaysCondition = ' AND `mt5AccountInfo`.`ACCOUNT_UPDATE` > :inactiveDaysTime';
                $c->params[':inactiveDaysTime'] = time() - 60*60*24*$monitoringSettings->inactiveDays;
            }*/
            $c->addCondition( " `mt5AccountInfo`.`ACCOUNT_UPDATE` IS NOT NULL AND `mt5AccountInfo`.`last_error_code` <> -255 AND `mt5AccountInfo`.`last_error_code` <> -1 AND `mt5AccountInfo`.`last_error_code` <> -254 AND `mt5AccountInfo`.`last_error_code` <> -3" . $inactiveDaysCondition);

            $models = ContestMemberModel::model()->findAll($c);
            $controller = Yii::app()->createController("/");
    ?>

            <?php foreach( $models as $model ){ ?>

                <div class="fx_rating-item fx_border-left clearfix">
                    <div>
                        <a href="<?php echo $model->singleURL;?>" class="fx_rating-subitem fx_account-item">
                        <?php
                            echo $model->accName;
                            if( $model->stats->gain ){
                                if( $model->stats->gain >0 ) echo '<span class="fx_account-up">+'.$model->stats->gain.' %</span>';
                                else echo '<span class="fx_account-down">'.$model->stats->gain.' %</span>';
                            }
                        ?>
                            
                        </a>
                    </div>
                    <div class='top-account-sideber-id<?php echo $model->id;?>'>
                    <?php
                        $ratingWidget = new WebRatingWidget(Array(
                            'type' => 'wp',
                            'ns' => "top-account-sideber-id" . $model->id,
                            'currentValue' => $model->commonRatingStats ? $model->commonRatingStats->average : 0,
                            'maxValue' => 10,
                            'returnNs' => true,
                            'onChangeValue' => "topAccountsRatingSidebarWidget.onChangeMark",
                            'readOnly' => Yii::App()->user->isGuest ? true : false,
                        ));
                        $ratingWidget->run();
                       
                    ?>
                    </div>
                </div>

            <?php } ?>
                                   

    </div>
</div>