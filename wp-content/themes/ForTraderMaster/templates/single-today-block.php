<?php
		$today = getdate();
		$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 6,
		'paged' => 1,
		'year' => $today["year"],
		'monthnum' => $today["mon"],
		'day' => $today["mday"],
		'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
	) );
		$max_num_pages = $r->max_num_pages;
		if ($r->have_posts()) :
	?>
<section class="section_offset">
	<h3 class="page_title2"><?php _e("Today", 'ForTraderMaster'); ?><i class="icon newspaper_icon"></i></h3>
	<div class="load-more-today-42-wrap">
	<?php $i=0;
		while ( $r->have_posts() ) : $r->the_post();
			if( $i==0 ) get_template_part( 'templates/loop-post-4-befor' );
			if( $i<4 ) get_template_part( 'templates/loop-post-4' );
			if( $i==1 ) echo '<div class="clear"></div>';
			if( $i==3 ) get_template_part( 'templates/loop-post-4-after' );
			if( $i==4 ) get_template_part( 'templates/loop-post-2-befor' );
			if( $i>3 ) get_template_part( 'templates/loop-post-2' );
			if( $i==5 ) get_template_part( 'templates/loop-post-2-after' );
			$i++;
		endwhile;
		if( $i<4 )get_template_part( 'templates/loop-post-4-after' );
		if( $i>4 && $i<6 )get_template_part( 'templates/loop-post-2-after' );
	?>
	</div>
	<?php if( $max_num_pages > 1 ){ ?>
		<a href="#" class="load_btn load-more-today-42 chench_btn" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>"  data-page="1" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
	<?php }?>
</section>
<?php
					
	wp_reset_postdata();
	endif;
?>