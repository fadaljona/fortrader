<?php
	$r = new WP_User_Query( array(
		'number' => 4,
		'meta_query' => array(
			array(
				'key'     => 'show_user_on_home',
				'value'   => '1',
				'compare' => '=',
			),
		),
	));
	$authors = $r->get_results();
	if( count($authors) ){ 
?>
<section class="section_offset">
	<h3 class="page_title2"><?php _e("Authors", 'ForTraderMaster'); ?><i class="icon feather_icon"></i></h3>
	<hr>
	<div class="post_box load-more-authors-wrapper">
		<?php foreach ($authors as $author){ 
				$authorName = getShowUserName( $author );
		?>
			<div class="post_col4 post_col_sm2">
				<figure class="post_autor">
					<a href="<?php echo get_author_posts_url($author->ID);?>">
						<?php renderImg( p75GetServicesAuthorThumbnail( 175, 200, $author->ID ), 175, 200, $authorName, 1 );?>
					</a>
					<figcaption>
						<h6><a href="<?php echo get_author_posts_url($author->ID);?>"><?php echo $authorName;?></a></h6>
						<p><?php echo $author->about;?></p>
					</figcaption>
				</figure>
			</div><!-- / .post_col4 -->
		<?php } ?>
		<div class="clear"></div>
	</div><!-- / .post_box -->
	<?php if( count($authors) == 4 ){?>
		<a href="#" class="load_btn load-more-authors chench_btn" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
	<?php } ?>
</section>
	<?php } ?>