<?php
	CommonLib::loadWp();
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => $num,
		'post__in' => $postIds,
	));
	
	while ( $r->have_posts() ){ 
		$r->the_post();
		echo '<div class="post_col2 post_col_sm1">';
			get_template_part( 'templates/loop-post-small-no-border' );
		echo '</div><!--/ .post_col -->';
	}		
?>