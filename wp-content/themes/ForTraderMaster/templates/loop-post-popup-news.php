<figure class="post_modal_new">
	<a href="<?php the_permalink();?>" onclick="popupClickNews('<?php the_permalink();?>'); return false;" class="post_modal_new_img"><img src="<?php echo p75GetThumbnail($post->ID, 233, 100, "")?>" alt="<?php echo get_the_title()?>" title="<?php echo get_the_title()?>" /></a>
	<figcaption class="post_modal_new_description">
		<a href="<?php the_permalink();?>" onclick="popupClickNews('<?php the_permalink();?>'); return false;" class="modal_link_title"><?php the_title();?></a>
		<p class="modal_text"><?php echo wp_trim_words( getPostPreviewText(12), 10 );?></p>
	</figcaption>
</figure>