<?php
	$current_date1 = new DateTime("now");
	$currentdate1=$current_date1->format('Y-m-d H:i:s');
	$r = new WP_Query( array(
	'post_status' => 'publish',
	'post_type' => 'post',
	'posts_per_page' => 4,
	'paged' => 1,
	'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'important',
				'value'   => '1',
				'compare' => '=',
			),
			array(
				'key'     => 'daylife',
				'value'   => $currentdate1,
				'compare' => '>',
			),
		),
	) );
	
	$max_num_pages = $r->max_num_pages;
	if ($r->have_posts()) :
?>
<section class="section_offset">
	<h3 class="page_title2"><?php _e("The editor recommends", 'ForTraderMaster'); ?><i class="icon thumbs_up_icon"></i></h3>
	<hr>
	<div class="news_post_box clearfix load-more-recommendate-single-wrap">
		<?php
			while ( $r->have_posts() ) : $r->the_post();
				get_template_part( 'templates/loop-post-big' );
			endwhile;
		?>
	</div>
	<a href="#" class="load_btn chench_btn load-more-recommendate-single" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
</section>
	<?php
		wp_reset_postdata();
	endif;
	?>
