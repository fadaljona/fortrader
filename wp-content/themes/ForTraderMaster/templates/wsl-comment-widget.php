<?php
$ruLang = false;
	
if( class_exists( 'Yii' ) ){
	if( Yii::app()->language == 'ru' ){ $ruLang = true; }
		
	$connectTitle = Yii::t('smTexts', 'Enter using');
}else{
	if( pll_current_language('slug') == 'ru' ){ $ruLang = true;}
		
	$connectTitle = __("Enter using", 'ForTraderMaster');
}
?>
<div class="decomments-social-login-widget">
	<div class="wp-social-login-widget">
		<div class="wp-social-login-connect-with"><?php echo $connectTitle; ?>:</div>
		<div class="wp-social-login-provider-list">
			<?php /*<a rel="nofollow" href="javascript:void(0);" title="Connect with Facebook" class="wp-social-login-provider wp-social-login-provider-facebook" data-provider="Facebook">
				Facebook
			</a>
            */?>
			<a rel="nofollow" href="javascript:void(0);" title="Connect with Google" class="wp-social-login-provider wp-social-login-provider-google" data-provider="Google">
				Google
			</a>
<?php /*if( $ruLang ){ ?>
			<a rel="nofollow" href="javascript:void(0);" title="Connect with ВКонтакте" class="wp-social-login-provider wp-social-login-provider-vkontakte" data-provider="Vkontakte">
				ВКонтакте
			</a>
<?php } */?>
			<a rel="nofollow" href="javascript:void(0);" title="Connect with FT" data-modal="#logIn" class="arcticmodal wp-social-login-provider wp-social-login-provider-fortrader">
				FT
			</a>
		</div>
		<div class="wp-social-login-widget-clearing"></div>
	</div>
</div>