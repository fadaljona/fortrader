<section class="turn_box">
	<div class="clearfix position-relative">
		<h3 class="page_title2 alignleft"><?php _e("The editor recommends", 'ForTraderMaster'); ?></h3>
		<!-- - - - - - - - - - - - - - Nav Buttons - - - - - - - - - - - - - - - - -->
		<div class="nav_buttons alignright">
			<a href="#" class="prev_btn"><span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span></a>
			<a href="#" class="next_btn"><span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span></a>
		</div>
		<!-- - - - - - - - - - - - - - End of Nav Buttons - - - - - - - - - - - - - - - - -->
	</div>
	<hr>
	<div class="carousel-wrapper">
	<div class="owl-carousel post_carousel" data-desktop="3" data-large="2" data-medium="1" data-small="1" data-extra-small="1" data-currentitem="0" data-maximumitem="1" data-type="recommend" data-offset="3">
		<?php
			$current_date1 = new DateTime("now");
			$currentdate1=$current_date1->format('Y-m-d H:i:s');
			$r = new WP_Query( array(
				'post_status' => 'publish',
				'post_type' => 'post',
				'posts_per_page' => 3,
				'meta_query' => array(
						'relation' => 'AND',
						array(
							'key'     => 'important',
							'value'   => '1',
							'compare' => '=',
						),
						array(
							'key'     => 'daylife',
							'value'   => $currentdate1,
							'compare' => '>',
						),
					
				),
			) );
		?>
		<?php
			$max_num_pages = $r->max_num_pages;
			$i=0;
			if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post(); ?>
			<div <?php if( $i > 0 ) echo 'data-mobile-hidden="1"'; ?>>
				<!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
				<figure class="post">
					<a href="<?php the_permalink();?>"><?php renderImg( p75GetThumbnail($post->ID, 236, 200, ""), 236, 200, get_the_title(), true );?></a>
					<div class="post_info clearfix">
						<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d M, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
						<span><?php echo get_post_meta ($post->ID,'views',true); ?></span>
					</div>
					<hr>
					<figcaption><a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a></figcaption>
				</figure>
				<!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
			</div>
		<?php 
				$i++;
			endwhile;
			wp_reset_postdata();
			endif;
		?>
	</div><!--  / .owl-carousel -->
	</div>
</section>