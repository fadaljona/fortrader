<?php get_header('layout2');?>

    <!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->    
    <div class="left_part fx_left-part">

        <?php /*<div class="fx_section">
            <div class="fx_content-title clearfix">
                <a href="#" class="fx_content-title-link">The latest release FT magazine</a>
                <img src="http://web.com/ft/images/fx_icon-book.png" alt="" class="fx_title-icon">
            </div>
            <div class="fx_section-box clearfix">
                <div class="fx_box-small"><img src="http://web.com/ft/images/fx_box-img.jpg" alt="" class="fx_box-img"></div>
                <div class="fx_box-medium">
                    <div class="fx_box-item">
                        <a href="#" class="fx_box-link">How to save money? New 6 painless ways</a>
                    </div>
                    <div class="fx_box-item">
                        <a href="#" class="fx_box-link">The price of gold forecast forex trader, the course today <span class="fx_box-count">15</span></a>
                    </div>
                    <div class="fx_box-item">
                        <a href="#" class="fx_box-link">Rising oil prices: a new hope</a>
                    </div>
                    <div class="fx_box-item">
                        <a href="#" class="fx_box-link">Statistics of the United States will try to support the dollar <span class="fx_box-count">4</span></a>
                    </div>
                    <div class="fx_box-item">
                        <a href="#" class="fx_box-link">How to trade the ruble, euro and the dollar on the exchange</a>
                    </div>
                    <div class="fx_box-item">
                        <a href="#" class="fx_box-link">Forex on the Moscow Stock Exchange <span class="fx_box-count">7</span></a>
                    </div>
                    <a href="#" class="fx_dw-btn">Download PDF</a>
                </div>
            </div>
        </div>*/?>

        

        <?php /*<div class="fx_section">
            <div class="fx_content-title clearfix">
                <a href="#" class="fx_content-title-link">Forex Contests</a>
                <img src="http://web.com/ft/images/fx_icon-contest.png" alt="" class="fx_title-icon">
            </div>
            <div class="fx_section-box clearfix">
                <div class="fx_box-half">
                    <div class="fx_contest-wr">
                        <div class="fx_contest-title"><span class="fx_ct-blue">Forex Forsage 3</span></div>
                        <div class="fx_contest-item clearfix">
                            <div>Prize Fund:</div>
                            <div><span class="fx_ct-blue">$ 10000</span></div>
                        </div>
                        <div class="fx_contest-item clearfix">
                            <div>Prize places:</div>
                            <div><span class="fx_ct-blue">200</span></div>
                        </div>
                        <div class="fx_contest-item fx_contest-item-red clearfix">
                            <div>Time remaining:</div>
                            <div><span class="fx_ct-red">2 Days, 23 Hours</span></div>
                        </div>
                        <div class="fx_contest-buttons clearfix">
                            <a href="#" class="fx_button-red">Watch Contest</a>
                            <a href="#" class="fx_btn-prev">Back</a>
                            <a href="#" class="fx_btn-next">Next</a>
                        </div>
                    </div>
                </div>
                <div class="fx_box-half fx_img-center"><img src="http://web.com/ft/images/fx_img-cup.jpg" alt="" class="fx_contest-img"></div>
            </div>
        </div>*/?>
		
		<?php //get_template_part('templates/latest-forex-news');?>
		
		<?php get_template_part('templates/latest-trade-idea');?>

        <?php get_template_part('templates/quotes-slider');?>

        <?php get_template_part('templates/ratings');?>

        <?php get_template_part('templates/economic-calendar-l2');?>
		
		<?php get_template_part('templates/latest-promo');?>

        <?php get_template_part('templates/share-sm');?>

    </div>
    <!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->

    <?php get_sidebar('layout2');?>
<?php get_footer('layout2');?>
