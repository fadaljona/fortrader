<?php $catId = 14230;?>
<div class="fx_box-half crypto_box-half">
    <div class="fx_half-header clearfix">
        <span class="fx_half-header--left"><?php _e("Articles", 'ForTraderMaster'); ?></span>
        <a href="<?php echo get_category_link($catId);?>" class="fx_half-header--right"><?php _e("All articles", 'ForTraderMaster'); ?></a>
    </div>
<?php
    $r = new WP_Query(array(
        'post_status'  => 'publish',
        'post_type' => 'post',
        'orderby' => 'date',
        'order'   => 'DESC',
        'posts_per_page' => 6,
        'cat' => $catId,
        'paged' => 1,
    ));
    if ($r->have_posts()) {
        while ($r->have_posts()) {
            $r->the_post();
            get_template_part('templates/loop-post-fx-half-left');
        }
    }
?>
</div>