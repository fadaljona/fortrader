<?php 
$smBtnsCount = 1;
if( get_locale() == 'ru_RU' ){
	$smBtnsCount = 1;
}
?>
<!-- - - - - - - - - - - - - - Modal Window - - - - - - - - - - - - - - - - -->
<div class="hide">
	<div id="logIn" class="modal_box">
		<div class="arcticmodal-close modal_close"><i class="fa fa-times"></i></div>
		<div class="modal_header">
			<div class="modal_logo">FT</div>
			<h6 class="modal_header_title"><?php _e("Log in using your login", 'ForTraderMaster'); ?></h6>
			<a href="#" class="arcticmodal arcticmodal-close" data-modal="#registration"><?php _e("or sign up", 'ForTraderMaster'); ?></a>
		</div>
		<div class="modal_inner">
		
			<form id="logInForm" method="post" action="./">
				<span class="form-error valid-all-form"></span>
				<span class="form-error valid-user-name"></span>
				<input type="text" class="modal_input user-name" name="username"  placeholder="<?php _e("Username", 'ForTraderMaster'); ?>">
			
				<span class="form-error valid-user-password"></span>
				<input type="password" name="password" class="modal_input invalid user-password" placeholder="<?php _e("Password", 'ForTraderMaster'); ?>">
				
				<button class="modal_button" type="submit"><?php _e("Sign in", 'ForTraderMaster'); ?></button>
				<div class="modal_social_box">
					<h6 class="modal_title"><span><?php _e("Login with Account", 'ForTraderMaster'); ?>:</span></h6>
					<ul class="modal_social clearfix smBtnsCount<?php echo $smBtnsCount;?>">
						<li><a href="javascript:void(0);" class="google_plus wp-social-login-provider" data-provider="Google"><i class="fa fa-google-plus"></i>Google</a></li>
						<?php /*<li><a href="javascript:void(0);" class="fb wp-social-login-provider" data-provider="Facebook"><i class="fa fa-facebook"></i>Facebook</a></li>
						<?php if( get_locale() == 'ru_RU' ){ ?><li><a href="javascript:void(0);" class="vk wp-social-login-provider" data-provider="Vkontakte"><i class="fa fa-vk"></i><?php _e("Vkontakte", 'ForTraderMaster'); ?></a></li><?php } ?>
                        */?>
					</ul>
				</div>
				<div class="modal_remember clearfix">
					<div class="modal_remember_inner alignleft">
						<input type="checkbox" id="remember_me" name="remember">
						<label for="remember_me"><?php _e("Remember me", 'ForTraderMaster'); ?></label>
					</div>
					<a href="#" class="arcticmodal arcticmodal-close alignright" data-modal="#lostpassword"><?php _e("Forgot your password?", 'ForTraderMaster'); ?></a>
				</div>
				<div class="modal_title"></div>
			</form>
			
			<p class="modal_text"><?php _e("Subscribe to our newsletter and receive the latest news", 'ForTraderMaster'); ?></p>
			
			<form id="subscribeForm" method="post" action="./">
				<span class="form-error valid-all-form"></span>
				<span class="form-error valid-user-email"></span>
				<div class="subscribe">
					<input type="text" class="invalid user-email" name="email" placeholder="<?php _e("Email", 'ForTraderMaster'); ?>">
					<button class="" type="submit"><?php _e("Subscribe", 'ForTraderMaster'); ?></button>
				</div>    
			</form>
			
		</div>
	</div>
	<div id="registration" class="modal_box">
		<div class="arcticmodal-close modal_close"><i class="fa fa-times"></i></div>
		<div class="modal_header">
			<div class="modal_logo">FT</div>
			<h6 class="modal_header_title"><?php _e("Sign up", 'ForTraderMaster'); ?></h6>
			<a href="#" class="arcticmodal arcticmodal-close" data-modal="#logIn"><?php _e("or login", 'ForTraderMaster'); ?></a>
		</div>
		<div class="modal_inner">
		
			<form id="registrationForm" method="post" action="./">
				<span class="form-error valid-all-form"></span>
				<span class="form-error valid-user-name"></span>
				<input type="text" class="modal_input user-name" name="username"  placeholder="<?php _e("Username", 'ForTraderMaster'); ?>">
				
				<span class="form-error valid-user-email"></span>
				<input type="email" class="modal_input invalid user-email" name="email" placeholder="<?php _e("Email", 'ForTraderMaster'); ?>">
				
				<span class="form-error valid-g-recaptcha"></span>
				<div id="registrationForm_grecaptcha" data-sitekey="6Lf_WxsUAAAAAKksUMKf2cjvKpCKbLZRyi3kg_Ev"></div>
				
				<button class="modal_button" type="submit"><?php _e("Sign up", 'ForTraderMaster'); ?></button>
				<p class="modal_text"><?php _e("Registration confirmation will be sent to your e-mail", 'ForTraderMaster'); ?></p>
				<div class="modal_social_box">
					<h6 class="modal_title"><span><?php _e("Or register through", 'ForTraderMaster'); ?>:</span></h6>
					<ul class="modal_social clearfix smBtnsCount<?php echo $smBtnsCount;?>">
						<li><a href="javascript:void(0);" class="google_plus wp-social-login-provider" data-provider="Google"><i class="fa fa-google-plus"></i>Google</a></li>
						<?php /*<li><a href="javascript:void(0);" class="fb wp-social-login-provider" data-provider="Facebook"><i class="fa fa-facebook"></i>Facebook</a></li>
						<?php if( get_locale() == 'ru_RU' ){ ?><li><a href="javascript:void(0);" class="vk wp-social-login-provider" data-provider="Vkontakte"><i class="fa fa-vk"></i><?php _e("Vkontakte", 'ForTraderMaster'); ?></a></li><?php } ?>
                        */?>
					</ul>
				</div>
			</form>
			
		</div>
	</div>	
	<div id="lostpassword" class="modal_box">
		<div class="arcticmodal-close modal_close"><i class="fa fa-times"></i></div>
		<div class="modal_header">
			<div class="modal_logo">FT</div>
			<h6 class="modal_header_title"><?php _e("Forgot your password?", 'ForTraderMaster'); ?></h6>
			<a href="#" class="arcticmodal arcticmodal-close" data-modal="#registration"><?php _e("sign up", 'ForTraderMaster'); ?></a>
			<a href="#" class="arcticmodal arcticmodal-close" data-modal="#logIn"><?php _e("sign in", 'ForTraderMaster'); ?></a>
		</div>
		<div class="modal_inner">
		
			<form id="lostpasswordForm" method="post" action="./">
				<span class="form-error valid-all-form"></span>
				<span class="form-error valid-user-name"></span>
				<input type="text" class="modal_input user-name" name="username" placeholder="<?php _e("Username or e-mail", 'ForTraderMaster'); ?>">
				
				<button class="modal_button" type="submit"><?php _e("Restore password", 'ForTraderMaster'); ?></button>
				<p class="modal_text"><?php _e("Enter your username or e-mail.", 'ForTraderMaster'); ?></p>
				<p class="modal_text"><?php _e("You will receive an email with a link to create a new password.", 'ForTraderMaster'); ?></p>
				<div class="modal_social_box">
					<h6 class="modal_title"><span><?php _e("Or register through", 'ForTraderMaster'); ?>:</span></h6>
					<ul class="modal_social clearfix smBtnsCount<?php echo $smBtnsCount;?>">
						<li><a href="javascript:void(0);" class="google_plus wp-social-login-provider" data-provider="Google"><i class="fa fa-google-plus"></i>Google</a></li>
						<?php /*<li><a href="javascript:void(0);" class="fb wp-social-login-provider" data-provider="Facebook"><i class="fa fa-facebook"></i>Facebook</a></li>
						<?php if( get_locale() == 'ru_RU' ){ ?><li><a href="javascript:void(0);" class="vk wp-social-login-provider" data-provider="Vkontakte"><i class="fa fa-vk"></i><?php _e("Vkontakte", 'ForTraderMaster'); ?></a></li><?php } ?>
                        */?>
					</ul>
				</div>
			</form>
			
		</div>
	</div>	
</div>
<!-- - - - - - - - - - - - - - End of Modal Window - - - - - - - - - - - - - - - - -->
<?php wp_enqueue_style( 'jquery.arcticmodal-0.3.css', get_bloginfo('template_url').'/plagins/arcticmodal/jquery.arcticmodal-0.3.css', array(), null );?>
<?php wp_enqueue_script('jquery.arcticmodal-0.3.min', get_bloginfo("stylesheet_directory") . '/plagins/arcticmodal/jquery.arcticmodal-0.3.min.js', array('jquery'), null); ?>

<?php wp_enqueue_script('recaptchaApi.js', 'https://www.google.com/recaptcha/api.js?onload=onloadRecaptchaCallback&render=explicit', array(), null, true); ?>
<script>
var onloadRecaptchaCallback = function() {
	if( document.getElementById('page-ask-questions_grecaptcha') !== null ){
		grecaptcha.render('page-ask-questions_grecaptcha', {
			'sitekey' : '6LcP1w4TAAAAAO8JccJ44FS2qL0pB1sVFEfItiD_',
			'theme' : 'light'
		});
	}
	if( document.getElementById('registrationForm_grecaptcha') !== null ){
		grecaptcha.render('registrationForm_grecaptcha', {
			'sitekey' : '6Lf_WxsUAAAAAKksUMKf2cjvKpCKbLZRyi3kg_Ev',
			'theme' : 'light'
		});
	}
};
</script>