<section class="section_offset turn_box">
	<div class="clearfix position-relative">
		<h3 class="page_title2 alignleft"><?php _e("Our partners", 'ForTraderMaster'); ?></h3>
		<!-- - - - - - - - - - - - - - Nav Buttons - - - - - - - - - - - - - - - - -->
		<div class="nav_buttons alignright">
			<a href="#" class="prev_btn"><span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span></a>
			<a href="#" class="next_btn"><span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span></a>
		</div>
		<!-- - - - - - - - - - - - - - End of Nav Buttons - - - - - - - - - - - - - - - - -->
	</div>
	<hr>
	<div class="post_carousel owl-carousel" data-desktop="5" data-large="3" data-medium="2" data-small="1" data-extra-small="1" data-pages-to-load="1">
		<div>
			<div class="parner_logo">
				<a href="http://www.fxteam.ru/" rel="nofollow" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/fxteam.jpg" alt="FxTeam"></a>
			</div>
		</div>
		<div data-mobile-hidden="1">
			<div class="parner_logo">
				<a href="http://fortrader.ru/73ft" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/f4u.png" alt="Forex4you"></a>
			</div>
		</div>
		<div data-mobile-hidden="1">
			<div class="parner_logo">
				<a href="http://stforex.ru/?partner=21045" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/stforex.jpg" alt="STForex"></a>
			</div>
		</div>
		<div data-mobile-hidden="1">
			<div class="parner_logo">
				<a href="http://fortrader.ru/wkvn" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/alpari.png" alt="Alpari"></a>
			</div>
		</div>
		<div data-mobile-hidden="1">
			<div class="parner_logo">
				<a href="http://fortrader.ru/ywqn" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/exness.png" alt="Exness"></a>
			</div>
		</div>
		<div data-mobile-hidden="1">
			<div class="parner_logo">
				<a href="http://www.fxpro.com/ib/ru/usd/258565" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/fxpro.png" alt="FxPro"></a>
			</div>
		</div>
	</div><!-- / .owl-carousel -->
</section>
