<?php
    $catsArray = array(641, 4343);

    foreach ($catsArray as $id => $cat) {
        $catsArray[$id] = pll_get_term($cat);
        if (!$catsArray[$id]) {
            unset($catsArray[$id]);
        }
    }

    $resultCats = array();
    foreach( $catsArray as $cat ){
        $r = new WP_Query( array(
            'post_status'  => 'publish',
            'post_type' => 'post',
            'posts_per_page' => 5,
            'meta_key' => 'views',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
            'category__in' => $cat ,
        ) );
        if ($r->have_posts()){
            $resultCats[$cat] = $r;
        }
    }
    if( count($resultCats) ){
?>
<div class="fx_sb-section mpaBlock">
    <div class="fx_sb-title">
        <span><?php _e("Most popular articles", 'ForTraderMaster'); ?></span>
    </div>
    
    <div class="fx_sb-buttons clearfix">
    <?php
        $postBlocks = '';
        $i=0;
        foreach( $resultCats as $catId => $query ){
            $activeClass = '';
            if( !$i ) $activeClass = 'active';
            echo '<a href="'. get_category_link($catId) .'" class="'.$activeClass.'">'.get_cat_name($catId).'</a>';
            
            $postBlocks .= '<div class="fx_sb-section-box fx_sb-most-articles '.$activeClass.'">';
            
            while ( $query->have_posts() ){
                $query->the_post();
               
                $author = get_userdata( $post->post_author ); 
				$authorName = getShowUserName( $author );

                $postBlocks .= '<a href="'.get_the_permalink().'" class="fx_sb-section-item fx_sb-news-item clearfix">';
                    $postBlocks .= '<div><img src="'.p75GetThumbnail($post->ID, 50, 40, "").'" alt="" class="fx_sb-news-img"></div>';
                    $postBlocks .= '<div>';
                        $postBlocks .= '<div class="fx_sb-news-text">'.get_the_title().' '.print_comments_number_l2(false).'</div>';
                        $postBlocks .= '<div class="fx_sb-news-date">'.$authorName.' - '.get_the_date('d/m/Y').'</div>';
                    $postBlocks .= '</div>';
                $postBlocks .= '</a>';
            }
            $postBlocks .= '<a href="'.get_category_link($catId).'" class="fx_sb-more">'.__("View more articles", 'ForTraderMaster').'</a>';
            $postBlocks .= '</div>';
            $i=1;
        }
    ?>
    </div>

    <?php echo $postBlocks;?>

</div>

<?php add_action('wp_footer',function(){ ?>
<script>
!function( $ ) {
    $('.mpaBlock .fx_sb-buttons a').click(function() {
        if($(this).hasClass('active')) return false;
        var index =  $('.mpaBlock .fx_sb-buttons a').index( $(this) );

        $('.mpaBlock .fx_sb-buttons a').removeClass('active');
        $(this).addClass('active');
        $('.mpaBlock .fx_sb-most-articles').removeClass('active');
        $('.mpaBlock .fx_sb-most-articles').eq(index).addClass('active');

        return false;
    });
}( window.jQuery );
</script>	
<?php }, 100); ?>

<?php }?>