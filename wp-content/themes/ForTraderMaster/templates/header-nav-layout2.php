<div class="nav_wrap fx_nav">
	<!-- - - - - - - - - - - - - - Main Navigation - - - - - - - - - - - - - - - - -->
		
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container'=> '', 'menu_class'=>'main_menu fx_main-menu PFDregular alignleft', 'walker'=>new mainMenuWalker() ) );?>
		<!-- - - - - - - - - - - - - - End of Additional Menu - - - - - - - - - - - - - - - - -->
		<div class="additional_menu_box fx_additional-menu alignleft">
			<button></button>
			<ul class="additional_menu"></ul>
		</div>
		<!-- - - - - - - - - - - - - - End of Additional Menu - - - - - - - - - - - - - - - - -->
	</nav>
	<!-- - - - - - - - - - - - - - End of Main Navigation - - - - - - - - - - - - - - - - -->
	<!-- - - - - - - - - - - - - - Searchform - - - - - - - - - - - - - - - - -->
	<?php get_template_part_by_locale( 'templates/top_search_box' ); ?>
	<!-- - - - - - - - - - - - - - End of Searchform - - - - - - - - - - - - - - - - -->
</div><!--/ .nav_wrap -->

<?php get_template_part( 'templates/js/main_menu_js' ); ?>


<script>
	!function( $ ) {	
		$('.new_menu a').each(function( index, element ){
			if( document.location.pathname.indexOf( $(this).attr('href') ) == 0 ) $(this).addClass('active');
		});
	}( window.jQuery );
</script>


