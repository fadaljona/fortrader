<?php
	$blockCategoryIds = '3821,11683,24,11737,5,639,9527,2284,1994,11726,11691,1787,832,1320,2321,11724,9,1822,6,8972,11684,9872,14230';
?>
<hr class="separator2">
<h4 class="page_title3"><?php _e("Recent educational articles", 'ForTraderMaster'); ?><i class="icon cup_icon"></i></h4>
<hr>
<?php
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'posts_per_page' => 10,
        'cat' => $blockCategoryIds,
	) );
	if ($r->have_posts()) :
		$i=0;
        while ( $r->have_posts() ) : $r->the_post(); 
            if ($i > 4) {
                continue;
            }
            $cats = wp_get_post_categories(get_the_ID());
            if (in_array(14205, $cats) && count($cats) == 1) {
                continue;
            }

			if( $i<2 ) get_template_part( 'templates/loop-post-big' );
			if( $i>=2 ) get_template_part( 'templates/loop-post-small' );
			$i++;
		endwhile;
		wp_reset_postdata();
	endif;
?>