<?php
	$blockCategoryId = 24;
?>
<!-- - - - - - - - - - - - - - End of List 5 - - - - - - - - - - - - - - - - -->
<h4 class="page_title3"><?php _e("Broker dictionary", 'ForTraderMaster'); ?><i class="icon dictionary_icon"></i></h4>
<hr>
<!-- - - - - - - - - - - - - - Dictionary - - - - - - - - - - - - - - - - -->
<ul class="list1">
	<?php
		$r = new WP_Query( array(
			'post_status'  => 'publish',
			'post_type' => 'post',
			'orderby' => 'date',
			'order'   => 'DESC',
			'posts_per_page' => 5,
			'cat' => $blockCategoryId,
		) );
		if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post(); 
				get_template_part( 'templates/loop-post-li' );
			endwhile;
			wp_reset_postdata();
		endif;
	?>
</ul>
<!-- - - - - - - - - - - - - - End of Dictionary - - - - - - - - - - - - - - - - -->