<div class="post_col4 post_col_sm2 post_col_xs1">
	<figure class="post1 clearfix">
		<a href="<?php the_permalink();?>">
			<?php renderImg( p75GetThumbnail($post->ID, 175, 160, ""), 175, 160, get_the_title(), 1 );?>
		</a>
		<figcaption>
			<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
		</figcaption> 
	</figure>
</div><!-- / .post_col4 -->