<figure class="post">
	<figcaption>
		<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
	</figcaption>
	<div class="post_info clearfix">
		<time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d M, Y',  get_the_date('Y-m-d') ); ?></time>
		<?php if(get_post_meta ($post->ID,'views',true)){?><span><?php echo get_post_meta ($post->ID,'views',true); ?></span><?php }?>
	</div>
	<hr>
</figure>
<!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
