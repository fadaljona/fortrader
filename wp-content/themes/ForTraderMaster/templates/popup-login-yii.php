<?php 
$smBtnsCount = 1;
if( Yii::app()->language == 'ru' ){
	$smBtnsCount = 1;
}
?>
<!-- - - - - - - - - - - - - - Modal Window - - - - - - - - - - - - - - - - -->
<div class="hide">
	<div id="logIn" class="modal_box">
		<div class="arcticmodal-close modal_close"><i class="fa fa-times"></i></div>
		<div class="modal_header">
			<div class="modal_logo">FT</div>
			<h6 class="modal_header_title"><?=Yii::t('popup-login-yii', 'Log in using your login')?></h6>
			<a href="#" class="arcticmodal arcticmodal-close" data-modal="#registration"><?=Yii::t('popup-login-yii', 'or sign up')?></a>
		</div>
		<div class="modal_inner">
		
			<form id="logInForm" method="post" action="./">
				<span class="form-error valid-all-form"></span>
				<span class="form-error valid-user-name"></span>
				<input type="text" class="modal_input user-name" name="username"  placeholder="<?=Yii::t('popup-login-yii', 'Username')?>">
			
				<span class="form-error valid-user-password"></span>
				<input type="password" name="password" class="modal_input invalid user-password" placeholder="<?=Yii::t('popup-login-yii', 'Password')?>">
				
				<button class="modal_button" type="submit"><?=Yii::t('popup-login-yii', 'Sign in')?></button>
				<div class="modal_social_box">
					<h6 class="modal_title"><span><?=Yii::t('popup-login-yii', 'Login with Account')?>:</span></h6>
					<ul class="modal_social clearfix smBtnsCount<?php echo $smBtnsCount;?>">
						<li><a href="javascript:void(0);" class="google_plus wp-social-login-provider" data-provider="Google"><i class="fa fa-google-plus"></i>Google</a></li>
						<?php /*<li><a href="javascript:void(0);" class="fb wp-social-login-provider" data-provider="Facebook"><i class="fa fa-facebook"></i>Facebook</a></li>
						<?php if( Yii::app()->language == 'ru' ){ ?><li><a href="javascript:void(0);" class="vk wp-social-login-provider" data-provider="Vkontakte"><i class="fa fa-vk"></i>Вконтакте</a></li><?php } ?>
                        */?>
					</ul>
				</div>
				<div class="modal_remember clearfix">
					<div class="modal_remember_inner alignleft">
						<input type="checkbox" id="remember_me" name="remember">
						<label for="remember_me"><?=Yii::t('popup-login-yii', 'Remember me')?></label>
					</div>
					<a href="#" class="arcticmodal arcticmodal-close alignright" data-modal="#lostpassword"><?=Yii::t('popup-login-yii', 'Forgot your password?')?></a>
				</div>
				<div class="modal_title"></div>
			</form>
			
			<p class="modal_text"><?=Yii::t('popup-login-yii', 'Subscribe to our newsletter and receive the latest news')?></p>
			
			<form id="subscribeForm" method="post" action="./">
				<span class="form-error valid-all-form"></span>
				<span class="form-error valid-user-email"></span>
				<div class="subscribe">
					<input type="text" class="invalid user-email" name="email" placeholder="<?=Yii::t('popup-login-yii', 'Email')?>">
					<button class="" type="submit"><?=Yii::t('popup-login-yii', 'Subscribe')?></button>
				</div>    
			</form>
			
		</div>
	</div>
	<div id="registration" class="modal_box">
		<div class="arcticmodal-close modal_close"><i class="fa fa-times"></i></div>
		<div class="modal_header">
			<div class="modal_logo">FT</div>
			<h6 class="modal_header_title"><?=Yii::t('popup-login-yii', 'Sign up')?></h6>
			<a href="#" class="arcticmodal arcticmodal-close" data-modal="#logIn"><?=Yii::t('popup-login-yii', 'or login')?></a>
		</div>
		<div class="modal_inner">
		
			<form id="registrationForm" method="post" action="./">
				<span class="form-error valid-all-form"></span>
				<span class="form-error valid-user-name"></span>
				<input type="text" class="modal_input user-name" name="username"  placeholder="<?=Yii::t('popup-login-yii', 'Username')?>">
				
				<span class="form-error valid-user-email"></span>
				<input type="email" class="modal_input invalid user-email" name="email" placeholder="<?=Yii::t('popup-login-yii', 'Email')?>">
				
				<span class="form-error valid-g-recaptcha"></span>
				<script src='https://www.google.com/recaptcha/api.js'></script>
				<div id="registrationForm_grecaptcha" class="g-recaptcha" data-sitekey="6Lf_WxsUAAAAAKksUMKf2cjvKpCKbLZRyi3kg_Ev"></div>
				
				<button class="modal_button" type="submit"><?=Yii::t('popup-login-yii', 'Sign up')?></button>
				<p class="modal_text"><?=Yii::t('popup-login-yii', 'Registration confirmation will be sent to your e-mail')?></p>
				<div class="modal_social_box">
					<h6 class="modal_title"><span><?=Yii::t('popup-login-yii', 'Or register through')?>:</span></h6>
					<ul class="modal_social clearfix smBtnsCount<?php echo $smBtnsCount;?>">
						<li><a href="javascript:void(0);" class="google_plus wp-social-login-provider" data-provider="Google"><i class="fa fa-google-plus"></i>Google</a></li>
						<?php /*<li><a href="javascript:void(0);" class="fb wp-social-login-provider" data-provider="Facebook"><i class="fa fa-facebook"></i>Facebook</a></li>
						<?php if( Yii::app()->language == 'ru' ){ ?><li><a href="javascript:void(0);" class="vk wp-social-login-provider" data-provider="Vkontakte"><i class="fa fa-vk"></i>Вконтакте</a></li><?php } ?>
                        */?>
					</ul>
				</div>
			</form>
			
		</div>
	</div>	
	<div id="lostpassword" class="modal_box">
		<div class="arcticmodal-close modal_close"><i class="fa fa-times"></i></div>
		<div class="modal_header">
			<div class="modal_logo">FT</div>
			<h6 class="modal_header_title"><?=Yii::t('popup-login-yii', 'Forgot your password?')?></h6>
			<a href="#" class="arcticmodal arcticmodal-close" data-modal="#registration"><?=Yii::t('popup-login-yii', 'sign up')?></a>
			<a href="#" class="arcticmodal arcticmodal-close" data-modal="#logIn"><?=Yii::t('popup-login-yii', 'sign in')?></a>
		</div>
		<div class="modal_inner">
		
			<form id="lostpasswordForm" method="post" action="./">
				<span class="form-error valid-all-form"></span>
				<span class="form-error valid-user-name"></span>
				<input type="text" class="modal_input user-name" name="username" placeholder="<?=Yii::t('popup-login-yii', 'Username or e-mail')?>">
				
				<button class="modal_button" type="submit"><?=Yii::t('popup-login-yii', 'Restore password')?></button>
				<p class="modal_text"><?=Yii::t('popup-login-yii', 'Enter your username or e-mail.')?></p>
				<p class="modal_text"><?=Yii::t('popup-login-yii', 'You will receive an email with a link to create a new password.')?></p>
				<div class="modal_social_box">
					<h6 class="modal_title"><span><?=Yii::t('popup-login-yii', 'Or register through')?>:</span></h6>
					<ul class="modal_social clearfix smBtnsCount<?php echo $smBtnsCount;?>">
						<li><a href="javascript:void(0);" class="google_plus wp-social-login-provider" data-provider="Google"><i class="fa fa-google-plus"></i>Google</a></li>
						<?php /*<li><a href="javascript:void(0);" class="fb wp-social-login-provider" data-provider="Facebook"><i class="fa fa-facebook"></i>Facebook</a></li>
						<?php if( Yii::app()->language == 'ru' ){ ?><li><a href="javascript:void(0);" class="vk wp-social-login-provider" data-provider="Vkontakte"><i class="fa fa-vk"></i>Вконтакте</a></li><?php } ?>
                        */?>
					</ul>
				</div>
			</form>
			
		</div>
	</div>	
</div>
<!-- - - - - - - - - - - - - - End of Modal Window - - - - - - - - - - - - - - - - -->