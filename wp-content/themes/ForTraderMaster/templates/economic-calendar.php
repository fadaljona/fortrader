<?php
	wp_enqueue_script('timer.jquery.plugin.js', get_bloginfo("stylesheet_directory") . '/js/timer/jquery.plugin.js', array('jquery'), null);
	wp_enqueue_script('timer.jquery.countdown.js', get_bloginfo("stylesheet_directory") . '/js/timer/jquery.countdown.js', array('jquery'), null);
	
	wp_enqueue_style( 'bootstrap-formhelpers.css', get_bloginfo("stylesheet_directory") . '/formHelpers/css/bootstrap-formhelpers.css' );
	wp_enqueue_style( 'bootstrap-formhelpers-countries.flags.css', get_bloginfo("stylesheet_directory") . '/formHelpers/css/bootstrap-formhelpers-countries.flags.css' );
	
	$timezone = 'Europe/Moscow';
	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	
	$begin = date( "Y-m-d H:i:s", time());
	
	loadYii();
	
	$calendarEvents = CalendarEvent2ValueModel::model()->findAll(Array(
		'condition' => " `t`.`dt` >= :begin ",
		'order' => " `t`.`dt` ",
		'with' => array(
			'event' => array(
				'with' => array( 'country' )
			)
		),
		'params' => Array(
			':begin' => $begin,
		),
		'limit' => 5,
	));
	global $calendarEventsJs;
	$calendarEventsJs = '';
	
?>
<section class="section_offset">
	<h3 class="page_title2">
		<?php _e("Economic calendar", 'ForTraderMaster'); ?>
		<span class="sub_title"><?php echo mysql2date('l d F, H:i',  date('Y-m-d H:i', time()) ); ?></span>
		<i class="icon clock_icon"></i>
	</h3>
	<hr>
	<table class="events_table">
		<tr>
			<th class="event_item"><?php _e("Coming events", 'ForTraderMaster'); ?></th>
			<th class="event_time"><?php _e("Time prior to the event", 'ForTraderMaster'); ?></th>
		</tr>
		<?php foreach( $calendarEvents as $model ){ ?>
		<tr>
			<td class="event_item">
				<?php 
					if( $model->event->country ) {
						echo $model->event->country->getImgFlag('shiny', 16) . ' ';
					}
					echo CHtml::link( CHtml::encode( $model->event->indicatorName ), $model->event->singleURL); 
				?>
			</td>
			<td class="event_time event_time<?php echo $model->id;?>">
				<?php 
					$time = strtotime( $model->dt );
					$time -= time();
					echo gmdate("H", $time).':'.gmdate("i", $time).':'.gmdate("s", $time);
					
					$calendarEventsJs .= "
						<script>
							jQuery(document).ready(function($) {
								var endDate{$model->id} = new Date();
								endDate{$model->id}.setSeconds(endDate{$model->id}.getSeconds()+$time);
								$('.event_time{$model->id}').countdown({until: endDate{$model->id}, compact: true, description: '', format: 'HMS'});
							});
						</script>
					";
				?>
			</td>
		</tr>
				
		<?php }?>
		

	</table>
	<a href="<?php echo Yii::app()->createUrl('calendarEvent/list'); ?>" class="load_btn"><?php _e("Go to calendar", 'ForTraderMaster'); ?></a>
</section>
<?php
	function economic_calendar_js( ){
		global $calendarEventsJs;
		echo $calendarEventsJs;
	}
	add_action('wp_footer','economic_calendar_js', 100);
?>