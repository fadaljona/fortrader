<div class="tabs_box section_offset">
<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
	<ul class="tabs_list clearfix PFDregular">
		<li class="active"><a href="javascript:;"><?php _e("Fresh", 'ForTraderMaster'); ?></a></li>
		<li><a href="javascript:;"><?php _e("Most read", 'ForTraderMaster'); ?></a></li>
	</ul>
	<!-- - - - - - - - - - - - - - End of Tabs List  - - - - - - - - - - - - - - - - -->
	<!-- - - - - - - - - - - - - - Tabs Contant - - - - - - - - - - - - - - - - -->
	<div class="tabs_contant">
		<!-- - - - - - - - - - - - - - Tabs Item 1 - - - - - - - - - - - - - - - - -->
		<div class="active">
			<!-- - - - - - - - - - - - - - News List Slider - - - - - - - - - - - - - - - - -->
			<div class="news_slider p_rel">
				<div class="news_slider_nav">
					<a class="slider_nav_prev" href="javascript:;"><span class="tooltip"><?php _e("Previous", 'ForTraderMaster'); ?></span></a>
					<a class="slider_nav_next" href="javascript:;"><span class="tooltip"><?php _e("Next", 'ForTraderMaster'); ?></span></a>
				</div>
				<ul class="news_list">
				<?php
					$r = new WP_Query( array(
						'post_status'  => 'publish',
						'post_type' => 'post',
						'orderby' => 'date',
						'order'   => 'DESC',
						'posts_per_page' => 15,
						'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
					) );
				?>
				<?php
					if ($r->have_posts()) :
					while ( $r->have_posts() ) : $r->the_post(); ?>
						<?php if( get_the_date('Y-m-d') == date('Y-m-d') ){?>
							<li><time datetime="<?php echo get_the_date( 'Y-m-d H:i' );?>"><span><?php echo get_the_date( 'H:i' );?></span></time>
						<?php }else{?>
							<li class="li-time-date"><time class="time-date" datetime="<?php echo get_the_date( 'Y-m-d H:i' );?>"><span><?php echo mysql2date('d M, Y',  get_the_date('Y-m-d') ); ?></span></time>
						<?php } ?>
						<p><a href="<?php the_permalink();?>"><?php the_title();?>
						<?php print_comments_number(); ?>
						</a></p></li>				
				<?php 
					endwhile;
					wp_reset_postdata();
					endif;
				?>
				</ul>
			</div><!--/ .news_slider -->
			<!-- - - - - - - - - - - - - - End of News List Slider - - - - - - - - - - - - - - - - -->
		</div>
		<!-- - - - - - - - - - - - - - End of Tabs Item 1 - - - - - - - - - - - - - - - - -->
		<!-- - - - - - - - - - - - - - Tabs Item 2 - - - - - - - - - - - - - - - - -->
		<div>
			<!-- - - - - - - - - - - - - - News List Slider - - - - - - - - - - - - - - - - -->
			<div class="news_slider p_rel">
				<div class="news_slider_nav"><a class="slider_nav_prev" href="javascript:;"></a><a class="slider_nav_next" href="javascript:;"></a></div>
					<ul class="news_list">
					<?php
						$r = new WP_Query( array(
							'post_status'  => 'publish',
							'post_type' => 'post',
							'posts_per_page' => 15,
							'meta_key' => 'views',
							'orderby' => 'meta_value_num',
							'order' => 'DESC',
							'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
							'date_query' => array(
								array(
									'after'     => date('Y-m-d H:i:s', time()-60*60*24*7),
									'inclusive' => true,
								),
							),
						) );
					?>
					<?php
						if ($r->have_posts()) :
						while ( $r->have_posts() ) : $r->the_post(); ?>
							<?php if( get_the_date('Y-m-d') == date('Y-m-d') ){?>
								<li><time datetime="<?php echo get_the_date( 'Y-m-d H:i' );?>"><span><?php echo get_the_date( 'H:i' );?></span></time>
							<?php }else{?>
								<li class="li-time-date"><time class="time-date" datetime="<?php echo get_the_date( 'Y-m-d H:i' );?>"><span><?php echo mysql2date('d M, Y',  get_the_date('Y-m-d') ); ?></span></time>
							<?php } ?>
							<p><a href="<?php the_permalink();?>"><?php the_title();?>
							<?php print_comments_number(); ?>
							</a></p></li>
					<?php 
						endwhile;
						wp_reset_postdata();
						endif;
					?>
					</ul>
			</div><!--/ .news_slider -->
			<!-- - - - - - - - - - - - - - End of News List Slider - - - - - - - - - - - - - - - - -->
		</div>
		<!-- - - - - - - - - - - - - - End of Tabs Item 2 - - - - - - - - - - - - - - - - -->
	</div>
	<!-- - - - - - - - - - - - - - End of Tabs Contant - - - - - - - - - - - - - - - - -->
</div><!--/ .tabs_box -->
<?php get_template_part( 'templates/js/recent-popular_js' ); ?>