<?php
	$blockCategoryId = 823;
	$subCategories = get_categories( array( 'parent' => $blockCategoryId ) );
	$rightRows = round( count($subCategories) / 6 );
	if( !$rightRows ) $rightRows=1;
?>
<section class="section_offset turn_box">
	<div class="clearfix position-relative">
		<h3 class="page_title2 alignleft"><?php echo get_cat_name( $blockCategoryId );?></h3>
		<!-- - - - - - - - - - - - - - Nav Buttons - - - - - - - - - - - - - - - - -->
		<div class="nav_buttons alignright">
			<a href="#" class="turn_btn"><span class="tooltip"><?php _e("Minimize", 'ForTraderMaster'); ?></span></a>
		</div>
		<!-- - - - - - - - - - - - - - End of Nav Buttons - - - - - - - - - - - - - - - - -->
	</div>
	<hr>
	<!-- - - - - - - - - - - - - - Turn Content - - - - - - - - - - - - - - - - -->
	<div class="turn_content">
		<div class="clearfix">
			<div class="beginner_right">
				<div class="post_box">
					<?php
						$r = new WP_Query( array(
							'post_status'  => 'publish',
							'post_type' => 'post',
							'orderby' => 'date',
							'order'   => 'DESC',
							'posts_per_page' => $rightRows * 2,
							'cat' => $blockCategoryId,
						) );
					?>
					<?php
					$i=0;
					$excludePostArr = array();
					if ($r->have_posts()) :
					while ( $r->have_posts() ) : $r->the_post(); 
					$i++;
					?>
					<div class="post_col2 post_col_xs1">
						<figure class="post1 clearfix">
							<a href="<?php the_permalink();?>">
								<?php renderImg( p75GetThumbnail($post->ID, 168, 154, ""), 168, 154, get_the_title(), 1 );?>
							</a>
							<figcaption>
								<a href="<?php the_permalink();?>"><?php the_title();?> <?php print_comments_number(); ?></a>
							</figcaption> 
						</figure>
					</div><!-- / .post_col2 -->
					<?php 
						if( $i==2 ){
							echo '<div class="clear"></div>';
							$i=0;
						} 
						$excludePostArr[] = $post->ID;
						endwhile;
						wp_reset_postdata();
						endif;
					?>
				</div><!-- / .post_box -->
			</div><!-- / .beginner_right -->
			<div class="beginner_left">
				<!-- - - - - - - - - - - - - - List 2 - - - - - - - - - - - - - - - - -->
				<?php
				if( count($subCategories) ){
				?>
				<ul class="list2">
					<?php foreach( $subCategories as $subCategory ){ ?>
					<li><a href="<?php echo get_category_link($subCategory->cat_ID);?>"><?php echo $subCategory->name;?> <span class="red_color">(<?php echo $subCategory->count;?>)</span></a></li>
					<?php } ?>
				</ul>
				<?php } ?>
				<!-- - - - - - - - - - - - - - End of List 2 - - - - - - - - - - - - - - - - -->
			</div><!-- / .beginner_left -->
		</div>
		<div class="post_box">
			<?php
				$r = new WP_Query( array(
					'post_status'  => 'publish',
					'post_type' => 'post',
					'orderby' => 'date',
					'order'   => 'DESC',
					'posts_per_page' => 4,
					'cat' => $blockCategoryId,
					'post__not_in' => $excludePostArr,
				) );
				if ($r->have_posts()) :
				while ( $r->have_posts() ) : $r->the_post(); 
					get_template_part( 'templates/loop-post-col4' );
					$excludePostArr[] = $post->ID;
				endwhile;
				wp_reset_postdata();
				endif;
			?>
		</div><!-- / .post_box -->
		<div class="load-indicators-wrapper">
			
		</div>
		<a href="#" class="load_btn load-more-indicators chench_btn" data-cat-id="<?php echo $blockCategoryId; ?>" data-offset="<?php echo count($excludePostArr); ?>" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
	</div><!-- / .turn_content -->
	<!-- - - - - - - - - - - - - - End of Turn Content - - - - - - - - - - - - - - - - -->
</section>
