<section class="section_offset">
	<h3 class="page_title2">
		<?php _e("Interesting tags", 'ForTraderMaster'); ?>
		<span class="sub_title"><?php _e("Choose topic", 'ForTraderMaster'); ?></span>
		<i class="icon folder_icon"></i>
	</h3>
	<hr>
		<div class="categories_box clearfix">
				<a data-tag-page="1" data-tag-id="457" class="categories_btn tag-load-link" href="/tag/eur-usd-forecast/">Евро/доллар прогноз</a>
				<a data-tag-page="1" data-tag-id="395" class="categories_btn tag-load-link" href="/tag/gbp-usd-forecast/">Фунт/доллар прогноз</a>
				<a data-tag-page="1" data-tag-id="328" class="categories_btn tag-load-link" href="/tag/bitcoin/">Bitcoin</a>
				<a data-tag-page="1" data-tag-id="261" class="categories_btn tag-load-link" href="/tag/forex-russia/">Форекс в России</a>
				<a data-tag-page="1" data-tag-id="261" class="categories_btn tag-load-link" href="/tag/brexit/">Brexit</a>
				<a data-tag-page="1" data-tag-id="261" class="categories_btn tag-load-link" href="/tag/donald-trump/">Трамп</a>
				<a data-tag-page="1" data-tag-id="261" class="categories_btn tag-load-link" href="/tag/opek/">ОПЕК+</a>
			</div>
		<hr>
	<!-- - - - - - - - - - - - - - Post Box - - - - - - - - - - - - - - - - -->
	<div class="post_box interesting-tags-wrapper">

	</div><!--  / .post_box -->
	<a data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" class="load_btn tag-load-link chench_btn" href="#"></a>
	<!-- - - - - - - - - - - - - - End of Post Box - - - - - - - - - - - - - - - - -->
</section>
