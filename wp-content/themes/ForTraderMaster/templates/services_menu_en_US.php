<div class="new_menu clearfix">
	<a href="/en/informers" class="new_menu_link" data-icon="new_icon10"><i class="ico"></i><span>Informers</span></a>
	<a href="/en/quotes" class="new_menu_link" data-icon="new_icon2"><i class="ico"></i><span>Quotes</span></a>
	<a href="/en/quotes/charts" class="new_menu_link" data-icon="new_icon5"><i class="ico"></i><span>Quotes charts</span></a>
	
	<a data-icon="new_icon9" class="new_menu_link" href="/en/bankrates"><i class="ico"></i><span>Bank Interest Rates</span></a>
	
	<a data-icon="new_icon3" class="new_menu_link" href="/en/currencyrates"><i class="ico"></i><span>Currancy Rates</span></a>
	<a data-icon="new_icon3" class="new_menu_link" href="/en/currencyconverter"><i class="ico"></i><span>Currency Converter</span></a>
	<a data-icon="new_icon10" class="new_menu_link" href="/en/economic-calendar"><i class="ico"></i><span>Economic calendar</span></a>
    <a href="/monitoring" class="new_menu_link" data-icon="new_icon2"><i class="ico"></i><span>Monitoring</span></a>
    <a href="/en/bitcoin-and-cryptocurrency/" class="new_menu_link" data-icon="new_icon3" data-stiker="New"><i class="ico"></i><span>Crypto Currencies</span></a>
</div>