<?php
	$blockCategoryId = 9872;
?>
<section class="section_offset">
	<h3 class="page_title2">
		<?php _e("Advisers", 'ForTraderMaster'); ?>
		<span class="sub_title"><?php _e("Forex expert advisors", 'ForTraderMaster'); ?></span>
		<i class="icon microphone"></i>
	</h3>
	<hr>
	<!-- - - - - - - - - - - - - - Post Box - - - - - - - - - - - - - - - - -->
	<div class="post_box load-more-wrapper" data-cat-id="<?php echo $blockCategoryId; ?>">
		<?php
			$r = new WP_Query( array(
				'post_status'  => 'publish',
				'post_type' => 'post',
				'orderby' => 'date',
				'order'   => 'DESC',
				'posts_per_page' => 3,
				'cat' => $blockCategoryId,
				'paged' => 1,
			) );
		?>
		<?php
			$max_num_pages = $r->max_num_pages;
			if ($r->have_posts()) :
			while ( $r->have_posts() ) : $r->the_post(); ?>

			<?php get_template_part( 'templates/loop-post-col3' ); ?>
		<?php 
			endwhile;
			wp_reset_postdata();
			endif;
		?>
	</div><!--  / .post_box -->
	<?php if( $max_num_pages > 1 ){ ?>
		<a href="#" class="load_btn load-more-btn chench_btn" data-cat-id="<?php echo $blockCategoryId; ?>" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
	<?php } ?>
	<!-- - - - - - - - - - - - - - End of Post Box - - - - - - - - - - - - - - - - -->
	
</section>