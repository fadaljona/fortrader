<?php get_template_part('templates/svg_sprite');?>
<div class="fx_menu clearfix">
    <div class="fx-menu__row js_menu-slider owl-carousel clearfix" data-desktop="7" data-large="5" data-medium="4" data-small="1" data-extra-small="1">

        <div class="fx-menu__item">
            <a href="/cryptocurrencies" class="fx-menu__link fx-menu_green">
                <svg class="svg_icon svg_cryptocurrencies">
                    <use xlink:href="#icon-cryptocurrencies"></use>
                </svg>
                <span>Криптовалюты</span>
            </a>
            
            <a href="/quotes" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_forex">
                    <use xlink:href="#icon-forex"></use>
                </svg>
                <span>Котировки</span>
            </a>
        </div>
        <div class="fx-menu__item">
            <a href="/currencyrates" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_exchangerate">
                    <use xlink:href="#icon-exchange_rate"></use>
                </svg>
                <span>Курс валют ЦБ РФ</span>
            </a>
            
            <a href="/binaryrating" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_borate">
                    <use xlink:href="#icon-bo_rate"></use>
                </svg>
                <span>Рейтинг БО</span>
            </a>
        </div>
        <div class="fx-menu__item">
            <a href="/brokersrating" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_brokersrate">
                    <use xlink:href="#icon-brokers_rate"></use>
                </svg>
                <span>Рейтинг брокеров</span>
            </a>
            <?php /*<a href="/quotes/charts" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_forex">
                    <use xlink:href="#icon-forex"></use>
                </svg>
                <span>Графики котировок</span>
            </a>*/?>
            <a href="/informers" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_informers">
                    <use xlink:href="#icon-informers"></use>
                </svg>
                <span>Информеры</span>
            </a>
        </div>
        <div class="fx-menu__item">
            <a href="/journal" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_journal">
                    <use xlink:href="#icon-journal"></use>
                </svg>
                <span>Биржевой журнал</span>
            </a>
            <a href="/contests" class="fx-menu__link fx-menu_orange">
                <svg class="svg_icon svg_contest">
                    <use xlink:href="#icon-contest"></use>
                </svg>
                <span>Форекс конкурсы</span>
            </a>
        </div>
        <div class="fx-menu__item">
            <a href="/news" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_news">
                    <use xlink:href="#icon-news"></use>
                </svg>
                <span>Биржевые новости</span>
            </a>
            <a href="/bankrates" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_interestrate">
                    <use xlink:href="#icon-interest_rate"></use>
                </svg>
                <span>Процентные ставки</span>
            </a>
        </div>
        <div class="fx-menu__item">
            <a href="/currencyconverter" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_converter">
                    <use xlink:href="#icon-converter"></use>
                </svg>
                <span>Конвертер валют</span>
            </a>
            <a href="/economic-calendar" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_calendar">
                    <use xlink:href="#icon-calendar"></use>
                </svg>
                <span>Экономический <br />календарь</span>
            </a>
        </div>
        <div class="fx-menu__item">
            <a href="/monitoring" class="fx-menu__link fx-menu_green">
                <svg class="svg_icon svg_monitoring">
                    <use xlink:href="#icon-monitoring"></use>
                </svg>
                <span>Мониторинг счетов</span>
            </a>
            <a href="http://forexsystemsru.com/" class="fx-menu__link fx-menu_default">
                <svg class="svg_icon svg_forum">
                    <use xlink:href="#icon-forum"></use>
                </svg>
                <span>Форум трейдеров</span>
            </a>
        </div>


<?php/*        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_informers">
                                    <use xlink:href="#icon-informers"></use>
                                </svg>
                                <span>Информеры</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_service">
                                    <use xlink:href="#icon-service"></use>
                                </svg>
                                <span>Этот сервис ;-)</span>
                            </a>
        </div>

                        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_borate">
                                    <use xlink:href="#icon-bo_rate"></use>
                                </svg>
                                <span>Рейтинг БО</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_brokersrate">
                                    <use xlink:href="#icon-brokers_rate"></use>
                                </svg>
                                <span>Рейтинг брокеров</span>
                            </a>
                        </div>

                        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_exchangerate">
                                    <use xlink:href="#icon-exchange_rate"></use>
                                </svg>
                                <span>Курс валют ЦБ РФ</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_forum">
                                    <use xlink:href="#icon-forum"></use>
                                </svg>
                                <span>Форум трейдеров</span>
                            </a>
                        </div>

                        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_journal">
                                    <use xlink:href="#icon-journal"></use>
                                </svg>
                                <span>Биржевой журнал</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_news">
                                    <use xlink:href="#icon-news"></use>
                                </svg>
                                <span>Биржевые новости</span>
                            </a>
                        </div>

                        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_forex">
                                    <use xlink:href="#icon-forex"></use>
                                </svg>
                                <span>Форекс котировки</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_interestrate">
                                    <use xlink:href="#icon-interest_rate"></use>
                                </svg>
                                <span>Процентные ставки</span>
                            </a>
                        </div>

                        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_converter">
                                    <use xlink:href="#icon-converter"></use>
                                </svg>
                                <span>Конвертер валют</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_earate">
                                    <use xlink:href="#icon-ea_rate"></use>
                                </svg>
                                <span>Рейтинг EA</span>
                            </a>
                        </div>

                        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_widgets">
                                    <use xlink:href="#icon-widgets"></use>
                                </svg>
                                <span>Виджеты</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_strategyrate">
                                    <use xlink:href="#icon-strategy_rate"></use>
                                </svg>
                                <span>Рейтинг стратегий</span>
                            </a>
                        </div>

                        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_calendar">
                                    <use xlink:href="#icon-calendar"></use>
                                </svg>
                                <span>Экономический <br />календарь</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_indicatorsrate">
                                    <use xlink:href="#icon-indicators_rate"></use>
                                </svg>
                                <span>Рейтинг индикаторов</span>
                            </a>
                        </div>

                        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_bankrate">
                                    <use xlink:href="#icon-bank_rate"></use>
                                </svg>
                                <span>Банковские ставки</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_default">
                                <svg class="svg_icon svg_somerate">
                                    <use xlink:href="#icon-some_rate"></use>
                                </svg>
                                <span>Рейтинг брокеров</span>
                            </a>
                        </div>

                        <div class="fx-menu__item">
                            <a href="#" class="fx-menu__link fx-menu_orange">
                                <svg class="svg_icon svg_contest">
                                    <use xlink:href="#icon-contest"></use>
                                </svg>
                                <span>Конкурсы</span>
                            </a>
                            <a href="#" class="fx-menu__link fx-menu_green">
                                <svg class="svg_icon svg_monitoring">
                                    <use xlink:href="#icon-monitoring"></use>
                                </svg>
                                <span>Мониторинг счетов</span>
                            </a>
                        </div>*/?>
    </div>
</div>