<?php loadYii(); ?>
<div class="fx_sb-section">
    <div class="fx_sb-title"><a href='<?php echo Yii::app()->createUrl('interestRates/index');?>'><?php echo Yii::t('*', "Bank Interest Rates"); ?></a></div>
    <div class="fx_sb-section-box fx_sb-banks">
                                    
        <?php
            $c = new CDbCriteria;
            $c->with['bank'] = array('with' => array( 'currentLanguageI18N' ));
            $c->with['currentLanguageI18N'] = array();
            $c->order = ' `t`.`order` DESC ';

            $c->addCondition( " `cLI18NInterestRatesRateModel`.`title` <> '' AND `cLI18NInterestRatesRateModel`.`title` IS NOT NULL AND `cLI18NInterestRatesBankModel`.`title` <> '' AND `cLI18NInterestRatesBankModel`.`title` IS NOT NULL " );

            $models = InterestRatesRateModel::model()->findAll($c);
        ?>

                <?php foreach( $models as $model ){ ?>
                    <div class="fx_rating-item fx_border-left clearfix">
                        <div>
                            <span class="fx_rating-subitem fx_banks-item">
                                <?php echo $model->bank->country->getImgFlag("shiny", 16);?>
                                <?php
                                    $title = $model->bank->shortTitle;
                                    $strLen = 27;


                                    if (mb_strlen($model->bank->title) > $strLen) {
                                        if ($model->bank->shortTitle) {
                                            $title = $model->bank->shortTitle;
                                        } else {
                                            $title = mb_substr($model->bank->title, 0, $strLen-3) . '...';
                                        }
                                    } else {
                                        if ($model->bank->shortTitle && mb_strlen($model->bank->title . ' ('.$model->bank->shortTitle.')') > $strLen) {
                                            $title = $model->bank->shortTitle;
                                        } elseif ($model->bank->shortTitle) {
                                            $title = $model->bank->title . ' ('.$model->bank->shortTitle.')';
                                        } else {
                                            $title = $model->bank->title;
                                        }
                                    }
                                ?>
                                <span><?php echo $title;?></span>
                            </span>
                        </div>
                        <div>
                        <?php 
                            if( $model->lastValue->value < 0 )echo '<span class="fx_banks-rate fx_color-red">'.$model->lastValue->value.'%</span>';
                            else echo '<span class="fx_banks-rate">'.$model->lastValue->value.'%</span>';
                        ?>
                        
                        </div>
                    </div>
                <?php } ?>

                                 

                                   

                                    

                                   

    </div>
</div>