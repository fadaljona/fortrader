<?php 
	$shareUrl = 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
	$themeUrl = 'http://' . $_SERVER["HTTP_HOST"] . '/wp-content/themes/ForTraderMaster/';
?>
<!-- - - - - - - - - - - - - - Share List - - - - - - - - - - - - - - - - -->
<ul class="social_list clearfix">
	<li><a href="#" class="tw share_tw"></a></li>
	<li><a href="#" class="fb share_fb"></a></li>
	<li><a href="#" class="vk share_vk"></a></li>
	<li><a href="#" class="google share_gp"></a></li>
</ul>
<!-- - - - - - - - - - - - - - End of Share List - - - - - - - - - - - - - - - - -->


<?php
global $smSmallTopButtonsJs;
$smSmallTopButtonsJs="
var windowClosed = 0;
var sm = '';

function CreateWindow( url ){
	windowClosed = 0;
	wnd=window.open( url, 'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no' );
	timerTop = setTimeout('checkWindow( )', 1000);
}
function checkWindow( ){

	if( wnd.closed == true && windowClosed == 0 ){
		windowClosed = 1;
		return true;
	}
	timerTop = setTimeout('checkWindow( sm )', 1000);
}
jQuery(document).ready(function($) {

	jQuery('.share_fb').click(function(e){
		CreateWindow( 'https://www.facebook.com/sharer/sharer.php?sdk=joey&u=$shareUrl&display=popup&ref=plugin&src=share_button' );
		sm = 'fb';
		shareVkFacebookClick();
		return false;
	});
	jQuery('.share_vk').click(function(){
		CreateWindow( 'http://vkontakte.ru/share.php?url=$shareUrl' );
		sm = 'vk';
		shareVkFacebookClick();
		return false;
	});
	
	jQuery('.share_tw').click(function(){
		CreateWindow( 'http://twitter.com/share?url=$shareUrl'  );
		sm = 'tw';
		anyLikesClick();
		return false;
	});
	
	jQuery('.share_gp').click(function(){
		CreateWindow( 'http://plus.google.com/share?url=$shareUrl'  );
		sm = 'gp';
		anyLikesClick();
		return false;
	});
	
});
";



if( class_exists( 'Yii' ) ){
	Yii::app()->clientScript->registerScript('smSmallTopButtonsJs', $smSmallTopButtonsJs, CClientScript::POS_END);
}else{
	add_action('wp_footer', function(){
		global $smSmallTopButtonsJs;
		echo '<script>';
		echo $smSmallTopButtonsJs;
		echo '</script>';
	}, 100);
}