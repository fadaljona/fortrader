<?php
	$blockCategoryId = 9527;
?>
<hr class="separator2">
<h4 class="page_title3"><?php _e("It is interesting", 'ForTraderMaster'); ?><i class="icon cup_icon"></i></h4>
<hr>
<?php
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'rand',
		'posts_per_page' => 3,
		'cat' => $blockCategoryId,
	) );
	if ($r->have_posts()) :
		///$i=0;
		while ( $r->have_posts() ) : $r->the_post(); 
			if( $i<3 ) get_template_part( 'templates/loop-post-big' );
			//if( $i==3 ) echo '<hr>';
			//if( $i>=3 ) get_template_part( 'templates/loop-post-small' );
			//$i++;
		endwhile;
		wp_reset_postdata();
	endif;
?>