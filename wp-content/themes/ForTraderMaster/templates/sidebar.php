<?php
	$locale = get_locale();
	$cacheDuration = get_option('theme_cache_duration');
	global $fortraderCache;
	
		if( is_home() ){
			get_template_part( 'templates/sidebar-rates' ); 
            get_template_part( 'templates/sidebar-banner-300x600' );
            get_template_part( 'templates/telegram-btn' );
			get_template_part( 'templates/sidebar-informer' );
			get_template_part( 'templates/sidebar-banners-small' );
			get_template_part( 'templates/google-subscribe-portrait' );
			echo '<hr class="separator2">';
			get_template_part( 'templates/sidebar-pool' );
			
			$cache_key = 'theme_' . md5('home-sidebar') . $locale;
			
			if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
				get_template_part( 'templates/sidebar-for-trader-big-li' );
				get_template_part( 'templates/sidebar-banners-small-2' );
				get_template_part( 'templates/sidebar-press-releases' );
				get_template_part( 'templates/sidebar-interesting' );
				//get_template_part( 'templates/sidebar-banner-300x250-2' );
				get_template_part( 'templates/sidebar-broker-dictionary' );
				get_template_part( 'templates/sidebar-google-subscribe' ); 

				get_template_part( 'templates/sidebar-fixed-ads' );
			$fortraderCache->endCache(); }

		}else{
			echo '<hr class="separator2">';
			get_template_part( 'templates/sidebar-rates' ); 
            get_template_part( 'templates/sidebar-banner-300x600' );
            get_template_part( 'templates/telegram-btn' );
			
			$cache_key = 'theme_' . md5('sub-page-sidebar') . $locale;
			if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
				get_template_part( 'templates/sidebar-analitics-big-li' );
				get_template_part( 'templates/sidebar-banners-small' );
				get_template_part( 'templates/sidebar-edu-articles' );
				get_template_part( 'templates/sidebar-indicators-and-strategies' );
			$fortraderCache->endCache(); }
				
			echo '<hr class="separator2">';
			get_template_part( 'templates/sidebar-pool' );
			get_template_part( 'templates/sidebar-google-subscribe' ); 

			//get_template_part( 'templates/sidebar-300x250' );
			get_template_part( 'templates/sidebar-fixed-ads' );
		}
	?>