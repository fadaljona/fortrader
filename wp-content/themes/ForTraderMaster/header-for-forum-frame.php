<!DOCTYPE html>
<html lang="<?=$language?>" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
    <head>
        <!-- Basic page needs
        ============================================ -->
		<title><?=$title?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <!-- Mobile specific metas
        ============================================ -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Favicon
        ============================================ -->
<?php 
	if($this->beginCache('wp_get_header_part1-header-for-forum-frame.php'.Yii::app()->language . CommonLib::sslRequestMarker(), array('duration'=>60*60*24))) {
		CommonLib::loadWp();
?>		

        <!-- Theme CSS
        ============================================ -->
	
		<?php wp_enqueue_style( 'style.css', get_bloginfo('stylesheet_url'), array(), null );?>
		
		<style>.container{width:100% !important;}</style>		
		
        <!-- Media Queries CSS
        ============================================ -->
		<?php wp_enqueue_style( 'responsive.css', get_bloginfo('template_url').'/css/modal-responsive.css', array(), null );?>
		
		 <!-- Libs CSS
        ============================================ -->
		
        <!-- Old IE 
        ============================================ -->
        <!--[if lt IE 8]>
        <div style=' clear: both; text-align:center; position: relative;'>
          <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
          </a>
        </div>
        <![endif]-->
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo('template_url'); ?>/css/ie.css">
        <![endif]-->
		<script>
			var yiiBaseURL = "<?=$baseUrl?>";
			var yiiLanguage = "<?=$language?>";
			var yiiDebug = <?=CommonLib::boolToStr( YII_DEBUG )?>;
			var wAWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		</script>	
		<style>a{color:#454242;}a:hover{color:#146eb3;}</style>
<?php 
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	wp_head(); 
?>
    </head>
<?php $this->endCache(); } ?> 
    <body <?=$bodyClass?>>
	


		
        <!-- - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->
        <div class="container white_container">
            <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->
            <header id="header" class="clearfix" itemscope itemtype="http://schema.org/WPHeader">       
				<!-- - - - - - - - - - - - - - Enter Box - - - - - - - - - - - - - - - - -->
                <div class="enter_box alignright clearfix">
					<?php 
						if ( Yii::App()->user->isGuest ){
					?>   
						
						<a href="<?=Yii::App()->createUrl( 'default/login')?>" id="topLogin" class="arcticmodal" data-modal="#logIn" rel="nofollow"><?=Yii::t( '*', 'Enter' )?></a>
						<a href="<?=Yii::App()->createUrl( 'default/login')?>" class="arcticmodal" data-modal="#registration" rel="nofollow"><?=Yii::t( '*', 'Registration' )?></a>
					<?php } else {
						$model = Yii::App()->user->getModel();	
						?>  
						<span><?=Yii::t( '*', 'Hello' )?>, <strong><?=CHtml::encode($model->wpShowName)?></strong></span>
						
						<?php echo $langSwitcher; ?>
						
						<a href="<?=Yii::App()->createUrl( 'default/logout', array('returnURL' => Yii::App()->request->getHostInfo() . Yii::App()->request->getUrl()) )?>" rel="nofollow"><?=Yii::t( '*', 'Logout' )?></a>
					<?php }?> 
                </div>
                <!-- - - - - - - - - - - - - - End of Enter Box - - - - - - - - - - - - - - - - -->
<?php 
	if($this->beginCache('wp_get_header_part2-header-for-forum-frame.php'.Yii::app()->language . CommonLib::sslRequestMarker(), array('duration'=>60*60*24))) {
		CommonLib::loadWp();
?>
                <!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->
                <?php /*<div class="logo">
					<a href="<?php echo get_lang_url_prefix();?>/" title="Home Page">
						<img src="<?php echo removeProtocol( get_bloginfo('template_url') ); ?>/images/logon.png" alt="">
					</a>
                </div>*/?>
               
            </header>

            <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->
            <div id="content" class="clearfix">
<?php $this->endCache(); } ?>  