<?php get_header();?>
<?php
global $fortraderCache;
$cacheDuration = get_option('theme_cache_duration');
$cache_key = 'theme_' . md5($_SERVER['REQUEST_URI']);
if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
?>           
<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part">
		<div class="blockdiv1"><?php dynamic_sidebar('banner-728x90');?></div>
		<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
		<div class="clearfix">
			<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
			<div class="breadcrumbs">
				<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
					<li>
						<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a href="<?php echo pll_home_url();?>" itemprop="item"><span itemprop="name"><?php _e("Home", 'ForTraderMaster'); ?></span></a>
							<meta itemprop="position" content="1" />
						</span>
					</li>
					<li><span><?php _e("About the author", 'ForTraderMaster'); ?></span></li>
				</ul>
			</div>
			<!-- - - - - - - - - - - - - - End of Breadcrumbs - - - - - - - - - - - - - - - - -->
		</div><!-- / .cleafix -->
		<hr>
		<?php 
			$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
	
			$authorName = getShowUserName( $author );

			$tw_link = get_user_meta( $author->ID, 'tw_link', true ) ? get_user_meta( $author->ID, 'tw_link', true ) : 'https://twitter.com/fortrader';
			$fb_link = get_user_meta( $author->ID, 'fb_link', true ) ? get_user_meta( $author->ID, 'fb_link', true ) : 'https://www.facebook.com/fortrader';
			$vk_link = get_user_meta( $author->ID, 'vk_link', true ) ? get_user_meta( $author->ID, 'vk_link', true ) : 'https://vk.com/fortrader';
				
			$authorEditLink = '';
			if( current_user_can( 'administrator' ) || get_current_user_id() == $author->ID ){
				$authorEditLink = '<a href="/wp-admin/user-edit.php?user_id='.$author->ID.'" class="post-edit-link"><i></i></a>';
			}
		?>
		<h1 class="page_title1 author_title"><?php echo $authorName; echo $authorEditLink;?><i class="icon feather_icon"></i></h1>
		<hr class="separator1">
		<!-- - - - - - - - - - - - - - Author Block - - - - - - - - - - - - - - - - -->
		<div class="clearfix">
			<div class="author_box clearfix">
				<div class="image_box alignleft">
					<?php renderImg( p75GetServicesAuthorThumbnail( 170, 170, $author->ID ), 170, 170, $authorName, 1 );?>
				</div>
				<p><?php echo get_user_meta( $author->ID, 'description', true );?></p>
			</div><!--/ .author_box -->
			<?php if( $tw_link || $fb_link || $vk_link ){?>
				<div class="author_social">
					<span><?php _e("Author on social networks", 'ForTraderMaster'); ?></span>
					<ul class="social_list d_ib">
						<?php if($tw_link){?><li><a href="<?php echo $tw_link;?>" class="tw" rel="nofollow"></a></li><?php }?>
						<?php if($fb_link){?><li><a href="<?php echo $fb_link;?>" class="fb" rel="nofollow"></a></li><?php }?>
						<?php if($vk_link){?><li><a href="<?php echo $vk_link;?>" class="vk" rel="nofollow"></a></li><?php }?>
					</ul>
				</div>
			<?php } ?>
		</div><!--/ .clearfix -->
		<!-- - - - - - - - - - - - - - End of Author Block - - - - - - - - - - - - - - - - -->
		<!-- - - - - - - - - - - - - - Tabs - - - - - - - - - - - - - - - - -->

		<div class="tabs_box section_offset">
			<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
			<?php
				$freshPosts = new WP_Query( array(
					'post_status'  => 'publish',
					'post_type' => 'post',
					'orderby' => 'date',
					'order'   => 'DESC',
					'posts_per_page' => 14,
					'paged' => 1,
					'author' => $author->ID,
					'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
				) );
				$popularPosts = new WP_Query( array(
					'post_status'  => 'publish',
					'post_type' => 'post',
					'meta_key' => 'views',
					'orderby' => 'meta_value_num',
					'order'   => 'DESC',
					'posts_per_page' => 14,
					'paged' => 1,
					'author' => $author->ID,
					'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
				) );
				$mostCommentedPosts = new WP_Query( array(
					'post_status'  => 'publish',
					'post_type' => 'post',
					'orderby' => 'comment_count',
					'order'   => 'DESC',
					'posts_per_page' => 14,
					'paged' => 1,
					'author' => $author->ID,
					'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
				) );
			?>
			<ul class="tabs_list clearfix PFDregular">
				<?php 
					$freshMarker=1;
					if ($freshPosts->have_posts()){
						?><li class="active"><a href="javascript:;"><?php _e("Fresh", 'ForTraderMaster'); ?></a></li><?php 
					}else{ 
						$freshMarker=0; $popularCssClass="active"; 
					}
					if ($popularPosts->have_posts()){
						?><li class="<?php echo $popularCssClass; ?>"><a href="javascript:;"><?php _e("Popular", 'ForTraderMaster'); ?></a></li><?php 
					}elseif(!$freshMarker){
						$commentCssClass="active"; 
					} ?>
					<li class="<?php echo $commentCssClass;?>"><a href="javascript:;"><?php _e("Comments", 'ForTraderMaster'); ?></a></li>
			</ul>
			<!-- - - - - - - - - - - - - - End of Tabs List  - - - - - - - - - - - - - - - - -->
			<!-- - - - - - - - - - - - - - Tabs Contant - - - - - - - - - - - - - - - - -->
			<div class="tabs_contant">
				<!-- - - - - - - - - - - - - - Tabs Item 1 - - - - - - - - - - - - - - - - -->
				<?php if ($freshPosts->have_posts()): ?>
				<div class="active fresh-posts">
				<?php 
					$i=0;
					while ( $freshPosts->have_posts() ) : $freshPosts->the_post(); 
						if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
						if( $i<2 ) get_template_part( 'templates/loop-post-big' );
						if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );
								
						if( $i==2 ) echo '<div class="post_box">';
						if( $i>1 ) get_template_part( 'templates/loop-post-col3' );
								
						if( $i>2 && ( ($i+1-2)%3 == 0 ) ) echo '<div class="clear"></div><hr>';
						
						if( $i>2 && ( ($i+1-2)%6 == 0 ) && $i<13 ) {echo '<div class="blockdiv1">'; dynamic_sidebar('banner-728x90'); echo '</div>';};
						
						$i++;
								
					endwhile;
					if($i<2) get_template_part( 'templates/loop-post-2-after' );
					if($i>2) echo '</div>';
					wp_reset_postdata();
				?>
					<a href="#" class="load_btn load-more-author-posts chench_btn" data-author="<?php echo $author->ID;?>" data-type="fresh" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
				</div>
				<?php endif;?>
						
				<!-- - - - - - - - - - - - - - End of Tabs Item 1 - - - - - - - - - - - - - - - - -->
				<!-- - - - - - - - - - - - - - Tabs Item 2 - - - - - - - - - - - - - - - - -->
				<?php if ($popularPosts->have_posts()): ?>
				<div class="<?php echo $popularCssClass; ?> popular-posts">
				<?php 
					$i=0;
					while ( $popularPosts->have_posts() ) : $popularPosts->the_post(); 
						if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
						if( $i<2 ) get_template_part( 'templates/loop-post-big' );
						if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );
								
						if( $i==2 ) echo '<div class="post_box">';
						if( $i>1 ) get_template_part( 'templates/loop-post-col3' );
								
						if( $i>2 && ( ($i+1-2)%3 == 0 ) ) echo '<div class="clear"></div><hr>';
						
						if( $i>2 && ( ($i+1-2)%6 == 0 ) && $i<13 ) {echo '<div class="blockdiv1">'; dynamic_sidebar('banner-728x90'); echo '</div>';};
						
						$i++;
								
					endwhile;
					if($i<2) get_template_part( 'templates/loop-post-2-after' );
					if($i>2) echo '</div>';
					wp_reset_postdata();
				?>
					<a href="#" class="load_btn load-more-author-posts chench_btn" data-author="<?php echo $author->ID;?>" data-type="popular" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
				</div>
				<?php endif;?>

				<!-- - - - - - - - - - - - - - End of Tabs Item 2 - - - - - - - - - - - - - - - - -->
				<!-- - - - - - - - - - - - - - Tabs Item 3 - - - - - - - - - - - - - - - - -->
				<?php if ($mostCommentedPosts->have_posts()): ?>
				<div class="<?php echo $commentCssClass; ?> commented-posts">
				<?php 
					$i=0;
					while ( $mostCommentedPosts->have_posts() ) : $mostCommentedPosts->the_post(); 
						if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
						if( $i<2 ) get_template_part( 'templates/loop-post-big' );
						if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );
								
						if( $i==2 ) echo '<div class="post_box">';
						if( $i>1 ) get_template_part( 'templates/loop-post-col3' );
								
						if( $i>2 && ( ($i+1-2)%3 == 0 ) ) echo '<div class="clear"></div><hr>';
						
						if( $i>2 && ( ($i+1-2)%6 == 0 ) && $i<13 ) {echo '<div class="blockdiv1">'; dynamic_sidebar('banner-728x90'); echo '</div>';};
						
						$i++;
								
					endwhile;
					if($i<2) get_template_part( 'templates/loop-post-2-after' );
					if($i>2) echo '</div>';
					wp_reset_postdata();
				?>
					<a href="#" class="load_btn load-more-author-posts chench_btn" data-author="<?php echo $author->ID;?>" data-type="commented" data-page="1" data-text="<?php _e("Show more from this category", 'ForTraderMaster'); ?>" data-shot-text="<?php _e("Show more", 'ForTraderMaster'); ?>"></a>
				</div>
				<?php endif;?>
				<!-- - - - - - - - - - - - - - End of Tabs Item 3 - - - - - - - - - - - - - - - - -->
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs Contant - - - - - - - - - - - - - - - - -->
		</div><!--/ .tabs_box -->
	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php $fortraderCache->endCache(); } ?>
	<?php get_sidebar();?>
<?php get_footer();?>