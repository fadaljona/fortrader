<?php get_header();?>           
<?php
global $fortraderCache;
$cacheDuration = get_option('theme_cache_duration');
$cache_key = 'theme_' . md5($_SERVER['REQUEST_URI']);
if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
?>
<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part">
		<div class="blockdiv1"><?php dynamic_sidebar('banner-728x90');?></div>
			<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
			<div class="clearfix">
				<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
				<div class="breadcrumbs">
					<ul class="clearfix">
						<li><a href="/"><?php _e("Home", 'ForTraderMaster'); ?></a></li>
						<li><span><?php single_cat_title(); ?></span></li>
					</ul>
				</div>
				<!-- - - - - - - - - - - - - - End of Breadcrumbs - - - - - - - - - - - - - - - - -->
			</div><!-- / .cleafix -->
			<hr>
			<section class="section_offset"> 
				<h2 class="page_title1"><?php _e("Tag", 'ForTraderMaster'); ?> <span href="#" class="link_accent"><?php single_cat_title(); ?></span><i class="icon student_icon"></i></h2>
				<hr class="separator1">

			</section>
			<section class="section_offset">
				<?php
					$i=0;
					if (have_posts()) : 
						while (have_posts()) : the_post();
							if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
							if( $i<2 ) get_template_part( 'templates/loop-post-big' );
							if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );
							if( $i==2 ) echo '<div class="post_box">';
							if( $i>1 && $i< 5 ) get_template_part( 'templates/loop-post-col3' );
							if( $i==4 ) echo '</div><hr />';
							
							if( $i==5 ) get_template_part( 'templates/loop-post-2-befor' );
							if( $i<7 && $i>4 ) get_template_part( 'templates/loop-post-big' );
							if( $i==6 ) get_template_part( 'templates/loop-post-2-after' );
							if( $i==7 ) echo '<div class="post_box">';
							if( $i>6 && $i< 10 ) get_template_part( 'templates/loop-post-col3' );
							if( $i==9 ) echo '</div><hr />';
							
							if( $i==10 ) echo '<div class="post_box">';
							if( $i>9 ) get_template_part( 'templates/loop-post-col3' );
							if( ($i>9) && ( ($i+1-10)%3 == 0 ) ) echo '<div class="clear"></div>';
							
							$i++;
						endwhile;
						
						if( $i<2 )get_template_part( 'templates/loop-post-2-after' );
						if( $i>2 && $i< 5 ) echo '</div><hr />';
						if( $i<7 && $i>5 ) get_template_part( 'templates/loop-post-2-after' );
						if( $i>7 && $i< 10 ) echo '</div><hr />';
						
						if( $i>10 ) echo '</div><hr />';
					
					get_template_part('templates/pagination');
					endif;
				?>
			</section>
			
			<?php
				$currentTag = get_term_by( 'slug', get_query_var('tag'), 'post_tag' );
				if($currentTag->description){
			?>
				<article class="section_offset">
					<h2 class="page_title1"><?php _e("About tag", 'ForTraderMaster'); ?> <a href="<?php echo get_tag_link($currentTag->term_id); ?>" class="link_accent">«<?php echo $currentTag->name; ?>»</a></h2>
					<?php echo apply_filters('the_content', $currentTag->description); ?>
				</article>
			<?php } ?>


	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php get_sidebar();?><?php $fortraderCache->endCache(); } ?>
<?php get_footer();?>