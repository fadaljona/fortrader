<?php
/*
Template Name: Ask Questions
*/
get_header();?>  
<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part">
		<?php get_template_part_by_locale( 'templates/banner-728x90' );?>
		<?php $excludePostArr = array();?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="clearfix">
				<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
				<?php
					$questionCatId = get_option('questions_cat_id');
				?>
				<div class="breadcrumbs alignleft">
					<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
						<li>
							<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a href="<?php echo pll_home_url();?>" itemprop="item"><span itemprop="name"><?php _e("Home", 'ForTraderMaster'); ?></span></a>
								<meta itemprop="position" content="1" />
							</span>
						</li>
						<li>
							<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a href="<?php echo get_category_link( $questionCatId );?>" itemprop="item">
									<span itemprop="name" class="breadcrumb-active"><?php echo get_cat_name( $questionCatId ); ?></span>
									<meta itemprop="position" content="2" />
								</a>
							</span>
						</li>
					</ul>
				</div>
				
				<!-- - - - - - - - - - - - - - End of Breadcrumbs - - - - - - - - - - - - - - - - -->
				<!-- - - - - - - - - - - - - - Article Info - - - - - - - - - - - - - - - - -->
				<div class="article_info clearfix alignright">
					<?php
						$commentNumber = get_comments_number();
						if( $commentNumber ){
							echo '<span class="comment">'.$commentNumber.'</span>';
						}
					?>
					<span class="views"><?php echo get_post_meta ($post->ID,'views',true) ? get_post_meta ($post->ID,'views',true) : 0; ?></span>
					<?php if( !get_post_meta(get_the_ID(), 'hide_date', true) ){?><time datetime="<?php echo get_the_date( 'Y-m-d' );?>"><?php echo mysql2date('d F, Y',  get_the_date('Y-m-d') ); ?></time><?php } ?>
				</div>
				<!-- - - - - - - - - - - - - - End of Article Info - - - - - - - - - - - - - - - - -->
			</div><!-- / .cleafix -->
			<hr>
			<h1 class="title2"><?php the_title();?><?php edit_post_link(' <i></i>'); ?></h2>
			<?php get_template_part( 'templates/sm-top-post-buttons' ); ?>
			<!-- - - - - - - - - - - - - - Article Post - - - - - - - - - - - - - - - - -->
			<article class="clearfix">
				<?php the_content(); ?>
				
				<form class="ask_question single-page" method="post">
				<h2><?php _e("All fields required", 'ForTraderMaster'); ?></h2>
				<?php 
					$user_ID = get_current_user_id(); 
					if( !$user_ID ){
				?>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<label for="question-author-name"><?php _e("Enter your name", 'ForTraderMaster'); ?></label>
								<input class="ask-input-field" type="text" value="" name="question-author-name" id="question-author-name" placeholder="<?php _e("Your name", 'ForTraderMaster'); ?>" />
								<span class="form-error" id="valid-question-author-name"></span>
							</td>
							<td>
								<label for="question-author-email"><?php _e("Enter your email", 'ForTraderMaster'); ?></label>
								<input class="ask-input-field" type="text" value="" name="question-author-email" id="question-author-email" placeholder="<?php _e("Your email", 'ForTraderMaster'); ?>" />
								<span class="form-error" id="valid-question-author-email"></span>
							</td>
						</tr>
					</table>
					
					<?php } ?>
				
					<label for="question-title"><?php _e("Enter your question", 'ForTraderMaster'); ?></label>
					<input class="ask-input-field" type="text" value="" name="question-title" id="question-title" placeholder="<?php _e("Your question", 'ForTraderMaster'); ?>" />
					<span class="form-error" id="valid-question-title"></span>
					
					<label for="question-body"><?php _e("Enter description of your question", 'ForTraderMaster'); ?></label>
					<textarea name="question-body" id="question-body" placeholder="<?php _e("Description of your question", 'ForTraderMaster'); ?>" cols="50" rows="10"><?php 
						if(isset($_POST['question']) && $_POST['question']) echo trim(strip_tags($_POST['question']));
					?></textarea>
					<span class="form-error" id="valid-question-body"></span>
					<button type="submit" id="submit-question" class="button1 alignright"><?php _e("Submit question", 'ForTraderMaster'); ?></button>
				
				<?php if( !$user_ID ){ ?>	
					<div id="page-ask-questions_grecaptcha" data-sitekey="6LcP1w4TAAAAAO8JccJ44FS2qL0pB1sVFEfItiD_"></div>
					<span class="form-error" id="valid-g-recaptcha"></span>
				<?php }?>
				
					<span class="form-error" id="valid-all-form"></span>
				</form>
				<div class="success-message"></div>

<script>

jQuery(document).ready(function($) {
<?php $user_ID = get_current_user_id(); ?>
<?php if( !$user_ID ){ ?>
	jQuery('#question-author-name').blur(function() {
        check_inputs( jQuery(this).attr('id'), 2, '<?php _e("The name may not be less than 2 characters", 'ForTraderMaster'); ?>' );
	});
	jQuery('#question-author-email').blur(function() {
        check_email( jQuery(this).attr('id') );
	});
<?php }?>

	jQuery('#question-title').blur(function() {
        check_inputs( jQuery(this).attr('id'), 10, '<?php _e("The question may not be less than 10 characters", 'ForTraderMaster'); ?><br />' );
	});
	jQuery('#question-body').blur(function() {
        check_inputs( jQuery(this).attr('id'), 10, '<?php _e("Description of the question may not be less than 10 characters", 'ForTraderMaster'); ?>' );
	});
	
	jQuery('#submit-question').click(function(e){
		e.preventDefault();
		
		var mark1 = true;
		var mark2 = true;
		var mark3 = true;
<?php if( !$user_ID ){ ?>
		mark1 = check_inputs( 'question-author-name', 2, '<?php _e("The name may not be less than 2 characters", 'ForTraderMaster'); ?>' );
		mark3 = check_email( 'question-author-email' );
<?php }?>
		var mark4 = check_inputs( 'question-title', 10, '<?php _e("The question may not be less than 10 characters", 'ForTraderMaster'); ?><br />' );
		var mark5 = check_inputs( 'question-body', 10, '<?php _e("Description of the question may not be less than 10 characters", 'ForTraderMaster'); ?>' );
		
		var mark6 = true;
<?php if( !$user_ID ){ ?>
		if( jQuery('#g-recaptcha-response').val() == '' ){
			var mark6 = false;
			jQuery('#valid-g-recaptcha').html('<?php _e("CAPTCHA validation fails, pass validation. If you can't-refresh the page", 'ForTraderMaster'); ?>');
		}else{
			var mark6 = true;
			jQuery('#valid-g-recaptcha').html('');
		}
<?php }?>
		
		var sumMarker = mark1 && mark2 && mark3 && mark4 && mark5 && mark6;
		
		if (  sumMarker ){
			jQuery('#valid-all-form').text( '' );
			jQuery.ajax({
				dataType: 'json',
				type: 'POST',
				url: '<?php echo admin_url('admin-ajax.php'); ?>',
				data: {
					action: 'publishNewQuestion',
					questionForm: jQuery('.ask_question.single-page').serialize(),
				},
				'success':function(json){
					if( json.error != 'no' ){
						jQuery('#valid-all-form').text( json.error );
					}else{
						jQuery('.ask_question.single-page').hide();
						jQuery('.success-message').html('<h2>'+json.message+'</h2>');
					}
					/*if( json.success == 'success' ) {
						jQuery('.subscribe-form-wrapper').html( '<h4 class="title1 box_style2">'+json.message+'<i class="fa fa-paper-plane"></i></h4>' );
					}else{
						jQuery('#valid-all-form').text( json.message );
					}*/
				}
			});
		}
		
	});
	

});
function check_inputs( selector, valLength, errorMessage ){

	if( jQuery('#'+selector).val() != '' && jQuery('#'+selector).val().length >= valLength ) {
                jQuery('#'+selector).css({'border' : '1px solid #e3e3e3'});
				jQuery('#valid-'+selector).html('');
				return true;
    } else {
        jQuery('#'+selector).css({'border' : '1px solid #ff0000'});jQuery('#valid-'+selector).html( errorMessage );
		return false;
    }
}
function check_email( selector ){

	if(jQuery('#'+selector).val() != '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test(jQuery('#'+selector).val())){
                jQuery('#'+selector).css({'border' : '1px solid #e3e3e3'});
				jQuery('#valid-'+selector).html('');
				return true;
            } else {
                jQuery('#'+selector).css({'border' : '1px solid #ff0000'});
                jQuery('#valid-'+selector).html('<?php _e("Wrong email", 'ForTraderMaster'); ?><br />');
				return false;
            }
        } else {
            jQuery('#'+selector).css({'border' : '1px solid #ff0000'});
            jQuery('#valid-'+selector).html('<?php _e("Email can not be empty", 'ForTraderMaster'); ?><br />');
			return false;
        }
}
</script>			

			</article><!--/ .section_offset -->
			<!-- - - - - - - - - - - - - - End of Article Post - - - - - - - - - - - - - - - - -->
			<?php get_template_part( 'templates/sm-bottom-post-buttons' ); ?>

			<div class="blockdiv3 clearfix">
				<div><?php dynamic_sidebar('300x250-2-post-banner');?></div>
				<div><?php dynamic_sidebar('300x250-3-post-banner');?></div>
			</div>

		<?php endwhile; else : ?>
			<h2 class="center"><?php _e("Not found", 'ForTraderMaster'); ?></h2>
			<p class="center"><?php _e("This page does not exist", 'ForTraderMaster'); ?></p>
		<?php endif; ?>
			
			<?php /*Сегодня*/?>	
			<?php get_template_part( 'templates/single-today-block' ); ?>

			<?php /*Редактор рекомендует*/?>	
			<?php get_template_part( 'templates/single-editor-recommends' ); ?>

	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php get_sidebar();?>
<?php get_footer();?>