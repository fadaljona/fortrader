<?php get_header();?>
<?php
global $fortraderCache;
$cacheDuration = get_option('theme_cache_duration');
$cache_key = 'theme_' . md5($_SERVER['REQUEST_URI']);
if( $fortraderCache->beginCache( $cache_key, $cacheDuration) ) {
?>           
<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part">
		<?php get_template_part_by_locale( 'templates/banner-728x90' );?>
			<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
			<div class="breadcrumbs">
				<ul class="clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
					<li>
						<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a href="<?php echo pll_home_url();?>" itemprop="item"><span itemprop="name"><?php _e("Home", 'ForTraderMaster'); ?></span></a>
							<meta itemprop="position" content="1" />
						</span>
					</li>
					<?php 
						$cur_cat_id = get_cat_id( single_cat_title("",false) );
						global $category_breadcrumb_marker;
						$category_breadcrumb_marker=0;
						
						$catsBreadcrumbs = get_formated_category_parents( $cur_cat_id, true, true);
						if ( !is_wp_error( $catsBreadcrumbs ) ){
							echo $catsBreadcrumbs;
						}
					?>
				</ul>
			</div>
			<!-- - - - - - - - - - - - - - End of Breadcrumbs - - - - - - - - - - - - - - - - -->
			<hr>
			<section class="section_offset paddingBottom0"> 
				<h1 class="page_title1"><?php  single_cat_title(); ?><i class="icon student_icon"></i></h1>
				<hr class="separator1">

				<?php
					if( $cur_cat_id == 11731 ){
						echo '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:105px" scrolling="no" src="https://fortrader.org/informers/getInformer?st=24&cat=13&title=undefined&mult=0.71&showGetBtn=0&w=0&hideDiff=1&colors=titleTextColor%3Dfff%2CtitleBackgroundColor%3Db3b1b1%2CsymbolTextColor%3D000000%2CtableTextColor%3D444%2CborderTdColor%3Db3b1b1%2CtableBorderColor%3Db3b1b1%2CtrBackgroundColor%3Dffffff%2CitemImgBg%3De6b8af%2CprofitTextColor%3D89bb50%2CprofitBackgroundColor%3Deaf7e1%2ClossTextColor%3Dff1616%2ClossBackgroundColor%3Df6e1e1%2CinformerLinkTextColor%3D454242%2CinformerLinkBackgroundColor%3Df1f1f1&items=47%2C25460&columns="></iframe>';
					}
				?>
				
				<!-- - - - - - - - - - - - - - Category - - - - - - - - - - - - - - - - -->
				<?php 
					$chiled_cats = get_categories( array(
						'parent' => $cur_cat_id
					) ); 
					if( count($chiled_cats) ) {
				?> 
					<ul class="category_box clearfix">
						<?php
							foreach( $chiled_cats as $chiled_cat ){ ?>
								<li>
									<a href="<?php echo get_category_link($chiled_cat->cat_ID);?>" class="category_link"><span><?php echo $chiled_cat->cat_name; ?></span><i></i></a>
								</li>
							<?php } ?>
					</ul>
				<?php } ?>
				<!-- - - - - - - - - - - - - - End of Category - - - - - - - - - - - - - - - - -->
			</section>
			
			
			
			<div class="section_offset">
				<?php if( count($chiled_cats) ) { ?>
					<h2 class="page_title1">
						<?php _e("All articles", 'ForTraderMaster'); ?> <a href="<?php echo get_category_link($cur_cat_id);?>" class="link_accent"><?php single_cat_title(); ?></a>
						<i class="icon category_icon"></i>
					</h2>
					<hr class="separator1">
				<?php } ?>
				
	
				
				<div class="tabs_box section_offset paddingBottom0">
					<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
					<ul class="tabs_list clearfix PFDregular">
						<li class="<?php if( !isset($_GET['sort']) ) echo "active";?>"><a href="<?php echo strtok( $_SERVER['REQUEST_URI'], '?');?>"><?php _e("Fresh", 'ForTraderMaster'); ?></a></li>
						<li class="<?php if( isset($_GET['sort']) ) echo "active";?>"><a href="<?php echo strtok( $_SERVER['REQUEST_URI'], '?');?>?sort=views"><?php _e("Most read", 'ForTraderMaster'); ?></a></li>
					</ul>
					<!-- - - - - - - - - - - - - - End of Tabs List  - - - - - - - - - - - - - - - - -->
				</div>				
				

				
				<?php
					global $wp_query;
					if( isset($_GET['sort']) ){
						$sortType = 'views';
						$queryArgs = array( 
							'meta_key' => 'views',
							'orderby' => 'meta_value_num',
							'order'   => 'DESC', 
						);
					}else{
						$queryArgs = array( 
							'orderby' => 'date',
							'order'   => 'DESC', 
						);
						$sortType = 'default';
					}
					
					query_posts( array_merge( $wp_query->query_vars, $queryArgs ) );
					
					$i=0;
					if (have_posts()) : 
						echo '<div class="category-posts-wrapper">';
						while (have_posts()) : the_post();
							if( $i==0 ) get_template_part( 'templates/loop-post-2-befor' );
							if( $i<2 ) get_template_part( 'templates/loop-post-big-cat' );
							if( $i==1 ) get_template_part( 'templates/loop-post-2-after' );
							if( $i==2 ) echo '<div class="post_box">';
							if( $i>1 && $i< 5 ) get_template_part( 'templates/loop-post-col3-cat' );
							if( $i==4 ) echo '</div><hr />';
							
							if( $i==5 ) get_template_part( 'templates/loop-post-2-befor' );
							if( $i<7 && $i>4 ) get_template_part( 'templates/loop-post-big-cat' );
							if( $i==6 ) get_template_part( 'templates/loop-post-2-after' );
							if( $i==7 ) echo '<div class="post_box">';
							if( $i>6 && $i< 10 ) get_template_part( 'templates/loop-post-col3-cat' );
							if( $i==9 ) echo '</div><hr />';
							
							if( $i==10 ) echo '<div class="post_box">';
							if( $i>9 ) get_template_part( 'templates/loop-post-col3-cat' );
							if( ($i>9) && ( ($i+1-10)%3 == 0 ) ) echo '<div class="clear"></div>';
							
							$i++;
						endwhile;
						
						if( $i<2 )get_template_part( 'templates/loop-post-2-after' );
						if( $i>2 && $i< 5 ) echo '</div><hr />';
						if( $i<7 && $i>5 ) get_template_part( 'templates/loop-post-2-after' );
						if( $i>7 && $i< 10 ) echo '</div><hr />';
						
						if( $i>10 ) echo '</div><hr />';
					echo '</div>';
					
					$max_page = $wp_query->max_num_pages;
					if( $max_page > 1 ){
						echo '<a href="#" class="load_btn category-load-more-posts chench_btn" data-cat-id="'. $cur_cat_id .'" data-page="1" data-sort="'.$sortType.'" data-text="'.__("Show more from this category", 'ForTraderMaster').'" data-shot-text="'.__("Show more", 'ForTraderMaster').'"></a>';
					}
					//get_template_part('templates/pagination');
					endif;
				?>
			</div>

			<?php
				$currentCat = get_category( $cur_cat_id );
				if($currentCat->category_description){
			?>
				<article class="section_offset">
					<h2 class="page_title1"><?php _e("About Section", 'ForTraderMaster'); ?> <a href="<?php echo get_category_link($cur_cat_id);?>" class="link_accent">«<?php echo $currentCat->cat_name; ?>»</a></h2>
					<?php echo apply_filters('the_content', $currentCat->category_description); ?>
				</article>
			<?php } ?>
			
			<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
			<script src="//yastatic.net/share2/share.js"></script>
			<div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,linkedin,lj,viber,whatsapp,skype,telegram" data-counter="" style="height:25px;"></div>  

	</div>
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php $fortraderCache->endCache(); } ?>
	<?php get_sidebar();?>
<?php get_footer();?>