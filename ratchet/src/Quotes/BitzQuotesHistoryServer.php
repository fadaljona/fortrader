<?php
namespace Quotes;

use \ZMQ;
use \ZMQContext;

class BitzQuotesHistoryServer
{
    protected $socketConnect;
    protected $documentRoot;
    protected $periods;
    protected $controlData = array();
    protected $usdData = array();
    
    protected $timeout = 60 * 1000;
    protected $iterationDelay = 1;
    protected $circleDelay = 30;
    protected $klineUrl = 'https://api.bit-z.com/api_v1/kline?coin=';

    protected $quotesCount;
    protected $filesForIter;
    
    protected $quotes;

    protected $db;
    protected $pdo = null;

    protected $queue = array();

    public function __construct($socketConnect, $configFromYii)
    {
        $this->socketConnect = $socketConnect;
        $this->documentRoot = $configFromYii->documentRoot;

        $this->quotes = $configFromYii->quotes;
        $this->db = $configFromYii->db;

        $sortedQuotesArr = array();
        $has_btc_usdt = false;
        $has_eth_usdt = false;
        foreach ($this->quotes as $quote) {
            if ($quote->bitzSymbol == 'btc_usdt') {
                $has_btc_usdt = true;
                $sortedQuotesArr[] = $quote;
            }
            if ($quote->bitzSymbol == 'eth_usdt') {
                $has_eth_usdt = true;
                $sortedQuotesArr[] = $quote;
            }
        }
        if (!$has_btc_usdt) {
            $btcUsdt = new \stdClass;
            $btcUsdt->id = null;
            $btcUsdt->name = 'btc_usdt';
            $btcUsdt->bitzSymbol = 'btc_usdt';
            $btcUsdt->toUsd = null;
            $sortedQuotesArr[] = $btcUsdt;
        }
        if (!$has_eth_usdt) {
            $ethUsdt = new \stdClass;
            $ethUsdt->id = null;
            $ethUsdt->name = 'eth_usdt';
            $ethUsdt->bitzSymbol = 'eth_usdt';
            $ethUsdt->toUsd = null;
            $sortedQuotesArr[] = $ethUsdt;
        }
        foreach ($this->quotes as $quote) {
            if ($quote->bitzSymbol != 'btc_usdt' && $quote->bitzSymbol != 'eth_usdt') {
                $sortedQuotesArr[] = $quote;
            }
        }
        $this->quotes = $sortedQuotesArr;

        $this->setQueue();

        echo '[' . date('Y-m-d H:i:s', time()) . '] constructor done ' . PHP_EOL;
    }
    private function setPeriods()
    {
        $this->periods = array(
            '1d' => array(
                'periodVal' => 1440,
                'frontPeriod' => 'D1'
            ),
            '1h' => array(
                'periodVal' => 60,
                'frontPeriod' => 'H1'
            ),
        );
    }
    private function setControlData()
    {
        foreach ($this->quotes as $quote) {
            foreach ($this->periods as $periodName => $period) {
                $this->controlData[$quote->bitzSymbol . $quote->toUsd][$periodName] = '';
            }
        }
        foreach ($this->periods as $periodName => $period) {
            $this->usdData['btc_usdt'][$periodName] = '';
            $this->usdData['eth_usdt'][$periodName] = '';
        }
    }
    private function getPeriods()
    {
        if (!$this->periods) {
            $this->setPeriods();
        }
        return $this->periods;
    }
    private function setQueue()
    {
        $taskOther = array();
        $quotes = $this->getQuotes();
        $periods = $this->getPeriods();
        
        foreach ($periods as $periodName => $period) {
            foreach ($quotes as $quote) {
                $this->queue[] = array(
                    'periodName' => $periodName,
                    'periodVal' => $period['periodVal'],
                    'frontPeriod' => $period['frontPeriod'],
                    'quote' => $quote->name,
                    'quoteId' => $quote->id,
                    'histSymbol' => $quote->bitzSymbol,
                    'from' => substr($quote->bitzSymbol, 0, strpos($quote->bitzSymbol, '_')),
                    'to' => substr($quote->bitzSymbol, strpos($quote->bitzSymbol, '_')+1),
                    'toUsd' => $quote->toUsd
                );
            }
        }
    }
    private function setQuotes()
    {
        $this->quotesCount = count($this->quotes);
        $this->filesForIter = $this->quotesCount * count($this->getPeriods());
        
        $this->setControlData();
    }
    private function getQuotes()
    {
        $this->setQuotes();
        return $this->quotes;
    }

    private function saveData($data, $period, $quoteId)
    {
        $returnStr = '';
        $deleteSql = 'DELETE FROM `ft_quotes_history_data` WHERE `resolution` = :period AND `symbolId` = :symbolId AND `barTime` >= :barTime';
        $deleteParams = array( ':period' => $period, ':symbolId' => $quoteId );
        
        $insertSql = 'INSERT INTO `ft_quotes_history_data` (`symbolId`, `barTime`, `high`, `low`, `open`, `close`, `volume`, `resolution`) VALUES ';
        $insertParams = array( ':symbolId' => $quoteId, ':resolution' => $period );
        
        $i=0;
        foreach ($data as $oneData) {
            $time = $oneData[0] / 1000;
            if ($time < 0) {
                continue;
            }
            if ($i==0) {
                $deleteParams[':barTime'] = $time;
                $returnStr .= json_encode($oneData);
            }
            if ($i) {
                $insertSql .= ',';
                $returnStr .= "\n" . json_encode($oneData);
            }
            $insertSql .= "( :symbolId, :barTime$i, :high$i, :low$i, :open$i, :close$i, :volume$i, :resolution)";
            $insertParams[":barTime$i"] = $time;
            $insertParams[":open$i"] = $oneData[1];
            $insertParams[":high$i"] = $oneData[2];
            $insertParams[":low$i"] = $oneData[3];
            $insertParams[":close$i"] = $oneData[4];
            $insertParams[":volume$i"] = $oneData[5];
            $i++;
        }
        
        $pdo = $this->getDbConnection();
        
        try {
            $stmt = $pdo->prepare($deleteSql);
            $stmt->execute($deleteParams);
            
            $stmt = $pdo->prepare($insertSql);
            $stmt->execute($insertParams);
        } catch (\PDOException $e) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] '.$e->getMessage().' ' . PHP_EOL;
        }
        return $returnStr;
    }

    private function getDbConnection()
    {
        if ($this->pdo == null) {
            $this->initDbConnection();
        }

        try {
            $this->pdo->query("SELECT 1");
        } catch (\PDOException $e) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] DB Connection failed, reinitializing... ' . PHP_EOL;
            $this->initDbConnection();
        }
        return $this->pdo;
    }
    private function initDbConnection()
    {
        try {
            echo '[' . date('Y-m-d H:i:s', time()) . '] Opening new DB connection... ' . PHP_EOL;
            $this->pdo = new \PDO($this->db['connectionString'], $this->db['username'], $this->db['password']);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            die($e->getMessage());
        }
    }
    protected function sendAlert($period, $symbol, $msg, $url, $dataDate = '')
    {
        if ($dataDate) {
            $dataDate = '[dataDate='.$dataDate.']';
        }
        echo '[' . date('Y-m-d H:i:s', time()) . '] WARNING - ' . $msg . '. ' . $period
        .'. '. $symbol . ' [url=' . $url . '] ' . $dataDate . PHP_EOL;
    }
    private function countVals($mainPairData, $toData)
    {
        $mainPairData = end($mainPairData);
        $toData = end($toData);

        return date('Y-m-d H:i', $toData[0] / 1000) . ';' . $mainPairData[1] * $toData[1] . ';' . $mainPairData[2] * $toData[2] . ';' . $mainPairData[3] * $toData[3] . ';' . $mainPairData[4] * $toData[4] . ';' . $toData[5];
    }
    private function calculateUsdCandles($usdData, $toUsdData)
    {
        $returnArr = array();
        $i=0;
        foreach ($toUsdData as $id => $data) {
            if (!isset($usdData[$data[0]])) {
                continue;
            }
            $returnArr[$i] = $data;
            $returnArr[$i][1] = \CommonLib::roundQuotesData($data[1] * $usdData[$data[0]][1]);
            $returnArr[$i][2] = \CommonLib::roundQuotesData($data[2] * $usdData[$data[0]][2]);
            $returnArr[$i][3] = \CommonLib::roundQuotesData($data[3] * $usdData[$data[0]][3]);
            $returnArr[$i][4] = \CommonLib::roundQuotesData($data[4] * $usdData[$data[0]][4]);
            $i++;
        }
        return $returnArr;
    }
    public function run()
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
        $socket->connect($this->socketConnect);
        
        $lastTimeArr = array();

        echo '[' . date('Y-m-d H:i:s', time()) . '] BitzQuotesHistoryServer started ' . PHP_EOL;
    
        while (true) {
            foreach ($this->queue as $id => $file) {
                $url = $this->klineUrl .$file['histSymbol'].'&type=' . $file['periodName'];

                $fileStr = self::downloadFileFromWww($url, $this->timeout);

                if (!$fileStr) {
                    $this->sendAlert($file['periodName'], $file['quote'], 'no data', $url);
                    continue;
                }
                
                $fileData = json_decode($fileStr);
                if ($fileData->code) {
                    $this->sendAlert($file['periodName'], $file['quote'], 'error code ' . $fileData->code, $url);
                    continue;
                }
    
                if (!isset($fileData->data) || !isset($fileData->data->datas) || !isset($fileData->data->datas->data)) {
                    $this->sendAlert($file['periodName'], $file['quote'], 'no data', $url);
                    continue;
                }
    
                $quoteData = json_decode($fileData->data->datas->data);
                
                if ($file['to'] == 'usdt') {
                    unset($this->usdData[$file['histSymbol']][$file['periodName']]);
                    $this->usdData[$file['histSymbol']][$file['periodName']] = array();
                    foreach ($quoteData as $data) {
                        $this->usdData[$file['histSymbol']][$file['periodName']][$data[0]] = $data;
                    }
                }

                if (isset($lastTimeArr[$file['quote']][$file['periodName']])) {
                    $dataToSave = self::getDataAfterLastDate(
                        $quoteData,
                        $lastTimeArr[$file['quote']][$file['periodName']]
                    );
                } else {
                    $dataToSave = $quoteData;
                }

                if ($file['toUsd']) {
                    if (isset($this->usdData[$file['to'] . '_usdt'][$file['periodName']]) &&
                        $this->usdData[$file['to'] . '_usdt'][$file['periodName']]
                    ) {
                        $dataToSave = $this->calculateUsdCandles(
                            $this->usdData[$file['to'] . '_usdt'][$file['periodName']],
                            $dataToSave
                        );
                    }
                }

                $lastTime = end($dataToSave)[0] / 1000;
                reset($dataToSave);
                $lastTimeArr[$file['quote']][$file['periodName']] = $lastTime;
                $encodedDataToSave = json_encode($dataToSave);

                $name = $file['quote'] . '_' .  $file['frontPeriod'];
                    
                if ($this->controlData[$file['histSymbol'] . $file['toUsd']][$file['periodName']] == $encodedDataToSave) {
                    $this->sendAlert($file['periodName'], $file['quote'], 'no update', $url, date('Y-m-d H:i:s', $lastTime));
                } else {
                    $this->controlData[$file['histSymbol'] . $file['toUsd']][$file['periodName']] = $encodedDataToSave;
                    
                    if ($file['quoteId']) {
                        $this->saveData($dataToSave, $file['periodVal'], $file['quoteId']);
                        $lastStrsToPush = self::getStrsToPush($dataToSave);
                        
                        $socket->send(json_encode(array( 'key' => $name, 'data' => $lastStrsToPush )));
                    }
                }
                sleep($this->iterationDelay);
            }
            sleep($this->circleDelay);
        }
    }




    public static function downloadFileFromWww($url, $timeout = 500)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return trim($data);
    }
    public static function getStrsToPush($data)
    {
        $outStr = '';
        foreach ($data as $oneData) {
            if ($outStr) {
                $outStr = PHP_EOL . $outStr;
            }
            $outStr = date('Y-m-d H:i', $oneData[0] / 1000) . ';' . $oneData[1] . ';' . $oneData[2] . ';' . $oneData[3] . ';' . $oneData[4] . ';' . $oneData[5] . $outStr;
        }
        return $outStr;
    }
    public static function getDataAfterLastDate($data, $time)
    {
        $time = $time * 1000;
        $tmpData = $data;
        foreach ($tmpData as $index => $oneData) {
            if ($oneData[0] < $time) {
                unset($tmpData[$index]);
            }
        }
        return $tmpData;
    }
}
