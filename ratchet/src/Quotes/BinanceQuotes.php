<?php
namespace Quotes;

use \ZMQ;
use \ZMQContext;
use \Amp\Delayed;
use \Amp\Websocket;

class BinanceQuotes extends BaseQuotes
{
    const KLINES_URL = 'https://api.binance.com/api/v1/klines?symbol=';
    const WS_URL = 'wss://stream.binance.com:9443/stream?streams=';

    protected $periods;
    protected $quotes;

    protected $symbolNameToQuote;
    protected $historyData = [];
    protected $tickData = [];

    private $debug = false;

    public function __construct($socketConnect, $configFromYii)
    {
        parent::__construct($socketConnect, $configFromYii);

        if (!$configFromYii->quotes) {
            return false;
        }

        $this->setPeriods();
        $this->quotes = $configFromYii->quotes;
        $this->setUsdQuotes();

        $this->setSymbolNameToQuote();
    }

    private function setSymbolNameToQuote()
    {
        foreach ($this->quotes as $quote) {
            $this->symbolNameToQuote[$quote->histName][] = $quote;
        }
    }

    private function setUsdQuotes()
    {
        $has_BTCUSDT = false;
        $has_ETHUSDT = false;
        $has_BNBUSDT = false;

        $sortedQuotesArr = array();

        foreach ($this->quotes as $quote) {
            if ($quote->histName == 'BTCUSDT') {
                $has_BTCUSDT = true;
                $sortedQuotesArr[] = $quote;
            }
            if ($quote->histName == 'ETHUSDT') {
                $has_ETHUSDT = true;
                $sortedQuotesArr[] = $quote;
            }
            if ($quote->histName == 'BNBUSDT') {
                $has_BNBUSDT = true;
                $sortedQuotesArr[] = $quote;
            }
        }
        if (!$has_BTCUSDT) {
            $btcUsdt = new \stdClass;
            $btcUsdt->id = null;
            $btcUsdt->name = 'BTCUSDT';
            $btcUsdt->histName = 'BTCUSDT';
            $btcUsdt->toUsd = null;
            $btcUsdt->from = 'BTC';
            $btcUsdt->to = 'USDT';
            $sortedQuotesArr[] = $btcUsdt;
        }
        if (!$has_ETHUSDT) {
            $ethUsdt = new \stdClass;
            $ethUsdt->id = null;
            $ethUsdt->name = 'ETHUSDT';
            $ethUsdt->histName = 'ETHUSDT';
            $ethUsdt->toUsd = null;
            $ethUsdt->from = 'ETH';
            $ethUsdt->to = 'USDT';
            $sortedQuotesArr[] = $ethUsdt;
        }
        if (!$has_BNBUSDT) {
            $bnbUsdt = new \stdClass;
            $bnbUsdt->id = null;
            $bnbUsdt->name = 'BNBUSDT';
            $bnbUsdt->histName = 'BNBUSDT';
            $bnbUsdt->toUsd = null;
            $bnbUsdt->from = 'BNB';
            $bnbUsdt->to = 'USDT';
            $sortedQuotesArr[] = $bnbUsdt;
        }

        foreach ($this->quotes as $quote) {
            if ($quote->histName != 'BTCUSDT' && $quote->histName != 'ETHUSDT' && $quote->histName != 'BNBUSDT') {
                $sortedQuotesArr[] = $quote;
            }
        }
        $this->quotes = $sortedQuotesArr;
    }

    private function setPeriods()
    {
        $this->periods = array(
            '1M' => array(
                'periodVal' => 43200,
                'frontPeriod' => 'MN1'
            ),
            '1w' => array(
                'periodVal' => 10080,
                'frontPeriod' => 'W1'
            ),
            '1d' => array(
                'periodVal' => 1440,
                'frontPeriod' => 'D1'
            ),
            '4h' => array(
                'periodVal' => 240,
                'frontPeriod' => 'H4'
            ),
            '1h' => array(
                'periodVal' => 60,
                'frontPeriod' => 'H1'
            ),
        );
    }

    /**
     * get klines via https://api.binance.com/api/v1/klines?symbol= and save to db
     *
     * response
     * [
     *  [
     *      [0] => 1525024800000        Open time (time in miliseconds)
     *      [1] => 9330.00000000        Open
     *      [2] => 9348.56000000        High
     *      [3] => 9300.00000000        Low
     *      [4] => 9344.00000000        Close
     *      [5] => 759.62952300         Volume
     *      [6] => 1525028399999        Close time
     *      [7] => 7084131.61168081     Quote asset volume
     *      [8] => 7491                 Number of trades
     *      [9] => 445.02713000         Taker buy base asset volume
     *      [10] => 4151387.89048224    Taker buy quote asset volume
     *      [11] => 0                   Ignore
     *  ]
     * ]
     *
     * @return void
     */
    private function getKlinesViaHttp()
    {
        foreach ($this->periods as $interval => $periodData) {
            foreach ($this->quotes as $quote) {
                $url = self::KLINES_URL . $quote->histName . "&interval={$interval}";
                $data = self::downloadFileFromWww($url, 5000);

                if (!$data) {
                    $this->sendAlert($periodData['frontPeriod'], $quote->histName, "Can't get data for url ", $url);
                    continue;
                }

                $decodedData = json_decode($data);

                if (!is_array($decodedData)) {
                    $this->sendAlert($periodData['frontPeriod'], $quote->histName, "Data not in json format ", $url);
                    continue;
                }

                $dataToInsert = [];

                foreach ($decodedData as $kline) {
                    $recountedKline = $this->saveDataToHistoryData(
                        $quote,
                        $kline[2],
                        $kline[3],
                        $kline[1],
                        $kline[4],
                        $kline[5],
                        $kline[0] / 1000,
                        $interval
                    );

                    if ($recountedKline) {
                        $dataToInsert[] = $recountedKline;
                    }
                }

                if ($dataToInsert && $quote->id) {
                    $inserttedRows = $this->saveDataToDb($dataToInsert, $quote->id, $this->periods[$interval]['periodVal']);
                    echo "[" . date('Y-m-d H:i:s', time()) . "] symbol {$quote->id} {$quote->name} {$this->periods[$interval]['frontPeriod']} inserted rows {$inserttedRows}" . PHP_EOL;
                }
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param array $dataToInsert
     * @param integer $quoteId
     * @param integer $resolution
     * @return integer|boolean $rowsCount - count of inserted rows
     */
    private function saveDataToDb($dataToInsert, $quoteId, $resolution)
    {
        $deleteSql = 'DELETE FROM `ft_quotes_history_data` WHERE `resolution` = :period AND `symbolId` = :symbolId AND `barTime` >= :barTime';
        $deleteParams = array( ':period' => $resolution, ':symbolId' => $quoteId );

        $insertSql = 'INSERT INTO `ft_quotes_history_data` (`symbolId`, `barTime`, `high`, `low`, `open`, `close`, `volume`, `resolution`) VALUES ';
        $insertParams = array( ':symbolId' => $quoteId, ':resolution' => $resolution );

        $i=0;
        foreach ($dataToInsert as $oneData) {
            if ($i==0) {
                $deleteParams[':barTime'] = $oneData['time'];
            }
            if ($i) {
                $insertSql .= ',';
            }
            $insertSql .= "( :symbolId, :barTime$i, :high$i, :low$i, :open$i, :close$i, :volume$i, :resolution)";
            $insertParams[":barTime$i"] = $oneData['time'];
            $insertParams[":open$i"] = $oneData['open'];
            $insertParams[":high$i"] = $oneData['high'];
            $insertParams[":low$i"] = $oneData['low'];
            $insertParams[":close$i"] = $oneData['close'];
            $insertParams[":volume$i"] = $oneData['volume'];
            $i++;
        }

        $pdo = $this->getDbConnection();
        
        try {
            $stmt = $pdo->prepare($deleteSql);
            $stmt->execute($deleteParams);
            
            $stmt = $pdo->prepare($insertSql);
            $stmt->execute($insertParams);
        } catch (\PDOException $e) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] '.$e->getMessage().' ' . PHP_EOL;
            return false;
        }

        return $stmt->rowCount();
    }

    private function updateTickDataTable($symbol, $bid, $ask, $time)
    {
        $deleteSql = "DELETE FROM `ft_quotes_tick_data` WHERE `symbol` = :symbol ";
        $deleteParams = [':symbol' => $symbol];

        $insertSql = 'INSERT INTO `ft_quotes_tick_data` (`symbol`, `bid`, `ask`, `time`, `updatedDT`, `spread`, `trend`, `pcp`) VALUES 
            (:symbol, :bid, :ask, :time, :updatedDT, 0, 0, 0)';

        $insertParams = [
            ':symbol' => $symbol,
            ':bid' => $bid,
            ':ask' => $ask,
            ':time' => $time,
            ':updatedDT' => date("Y-m-d H:i:s", time()-60*60*1)
        ];

        $pdo = $this->getDbConnection();

        try {
            $stmt = $pdo->prepare($deleteSql);
            $stmt->execute($deleteParams);
            
            $stmt = $pdo->prepare($insertSql);
            $stmt->execute($insertParams);
        } catch (\PDOException $e) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] '.$e->getMessage().' ' . PHP_EOL;
            return false;
        }

        return $stmt->rowCount();
    }

    /**
     * save data to historyData and return arr to insert in db
     *
     * @param stdClass $quote
     * @param float $high
     * @param float $low
     * @param float $open
     * @param float $close
     * @param integer $volume
     * @param integer $time
     * @param string $interval
     * @return array|false
     */
    private function saveDataToHistoryData($quote, $high, $low, $open, $close, $volume, $time, $interval)
    {
        if (!$quote->toUsd) {
            return $this->historyData[$quote->histName][$interval][$time] = [
                'high' => $high,
                'low' => $low,
                'open' => $open,
                'close' => $close,
                'volume' => $volume,
                'time' => $time,
            ];
        }

        if (empty($this->historyData[$quote->to . 'USDT'][$interval][$time])) {
            return false;
        }

        $usdData = $this->historyData[$quote->to . 'USDT'][$interval][$time];

        return $this->historyData[$quote->histName . '_USDT'][$interval][$time] = [
            'high' => \CommonLib::roundQuotesData($high * $usdData['high']),
            'low' => \CommonLib::roundQuotesData($low * $usdData['low']),
            'open' => \CommonLib::roundQuotesData($open * $usdData['open']),
            'close' => \CommonLib::roundQuotesData($close * $usdData['close']),
            'volume' => $volume,
            'time' => $time,
        ];
    }

    private function getWsUrl()
    {
        $klineStreams = [];
        $tickerStreams = [];
        
        foreach ($this->quotes as $quote) {
            $symbol = strtolower($quote->histName);

            foreach ($this->periods as $interval => $periodData) {
                $klineStreams[] = "{$symbol}@kline_{$interval}";
            }
            
            $tickerStreams[] = "{$symbol}@ticker";
        }

        array_unique($klineStreams);
        array_unique($tickerStreams);

        return self::WS_URL . implode('/', $klineStreams) . '/' . implode('/', $tickerStreams);
    }

    /**
     * stdClass Object
     *(
     *    [t] => 1528632000000      Kline start time
     *    [T] => 1528646399999      Kline close time
     *    [s] => BTCUSDT            Symbol
     *    [i] => 4h                 Interval
     *    [f] => 50098994           First trade ID
     *    [L] => 50100043           Last trade ID
     *    [o] => 7235.10000000      Open price
     *    [c] => 7250.00000000      Close price
     *    [h] => 7250.01000000      High price
     *    [l] => 7232.90000000      Low price
     *    [v] => 199.19977100       Base asset volume
     *    [n] => 1050               Number of trades
     *    [x] =>                    Is this kline closed?
     *    [q] => 1442647.20137963   Quote asset volume
     *    [V] => 132.97849000       Taker buy base asset volume
     *    [Q] => 962950.14234465    Taker buy quote asset volume
     *    [B] => 0                  Ignore
     *)
     */
    private function wsKlineDataReceived($data)
    {
        foreach ($this->symbolNameToQuote[$data->s] as $quote) {
            $recountedKline = $this->saveDataToHistoryData(
                $quote,
                $data->h,
                $data->l,
                $data->o,
                $data->c,
                $data->q,
                $data->t / 1000,
                $data->i
            );

            if ($recountedKline && $quote->id) {
                $this->saveDataToDb([$recountedKline], $quote->id, $this->periods[$data->i]['periodVal']);

                $dataToSend = json_encode(array(
                    'key' => $quote->name . '_' .  $this->periods[$data->i]['frontPeriod'],
                    'data' => date('Y-m-d H:i', $recountedKline['time']) . ';' .
                    $recountedKline['open'] . ';' .
                    $recountedKline['close'] . ';' .
                    $recountedKline['high'] . ';' .
                    $recountedKline['low'] . ';' .
                    $recountedKline['volume']
                ));

                $this->socket->send($dataToSend);

                $this->printDebugMsg($dataToSend);
            }
        }
    }

    private function printDebugMsg($msg)
    {
        if (!$this->debug) {
            return false;
        }
        echo $msg . PHP_EOL;
    }

    /**
     *stdClass Object
     *(
     *    [e] => 24hrTicker     Event type
     *    [E] => 1528632592515  Event time  (milliseconds)
     *    [s] => QTUMETH        Symbol
     *    [p] => -0.00037200    Price change
     *    [P] => -1.686         Price change percent
     *    [w] => 0.02182289     Weighted average price
     *    [x] => 0.02205800     Previous day's close price
     *    [c] => 0.02169000     Current day's close price
     *    [Q] => 1.52000000     Close trade's quantity
     *    [b] => 0.02169000     Best bid price
     *    [B] => 17.89000000    Best bid quantity
     *    [a] => 0.02174600     Best ask price
     *    [A] => 3.34000000     Best bid quantity
     *    [o] => 0.02206200     Open price
     *    [h] => 0.02221400     High price
     *    [l] => 0.02111200     Low price
     *    [v] => 46748.59000000 Total traded base asset volume
     *    [q] => 1020.18939900  Total traded quote asset volume
     *    [O] => 1528546192515  Statistics open time
     *    [C] => 1528632592515  Statistics close time
     *    [F] => 2661492        First trade ID
     *    [L] => 2665416        Last trade Id
     *    [n] => 3925           Total number of trades
     *)
     */
    private function wsTickerDataReceived($data)
    {
        foreach ($this->symbolNameToQuote[$data->s] as $quote) {
            if (!empty($this->tickData[$data->s]) && $this->tickData[$data->s]['compare'] == $data->b . $data->a) {
                continue;
            }

            $time = date('H:i:s', ($data->E - ($data->E % 1000)) / 1000);

            $this->updateTickDataTable($data->s, $data->b, $data->a, $time);

            $this->tickData[$data->s] = [
                'bid' => $data->b,
                'ask' => $data->a,
                'compare' => $data->b . $data->a
            ];

            if (!$quote->id) {
                continue;
            }

            $dataToSend = '';

            if (!$quote->toUsd) {
                $dataToSend = json_encode(array(
                    'bid' => (float)$data->b,
                    'ask' => (float)$data->a,
                    'time' => $time,
                ));
            } elseif (!empty($this->tickData[$quote->to . 'USDT'])) {
                $this->printDebugMsg('to usd');
                $dataToSend = json_encode(array(
                    'bid' => \CommonLib::roundQuotesData($data->b * $this->tickData[$quote->to . 'USDT']['bid']),
                    'ask' => \CommonLib::roundQuotesData($data->a * $this->tickData[$quote->to . 'USDT']['ask']),
                    'time' => $time,
                ));
            }

            if ($dataToSend) {
                $this->printDebugMsg(json_encode([
                    'key' => 'ALL',
                    'data' => [
                        $quote->name => $dataToSend
                    ]
                ]));

                $this->socket->send(json_encode([
                    'key' => 'ALL',
                    'data' => [
                        $quote->name => $dataToSend
                    ]
                ]));
            }
        }
    }

    /**
     * ws responses
     * Received:stdClass Object
     *(
     *    [stream] => dashbtc@ticker
     *    [data] => stdClass Object
     *        (
     *            [e] => 24hrTicker     Event type
     *            [E] => 1528630999468  Event time
     *            [s] => DASHBTC        Symbol
     *            [p] => 0.00021800     Price change
     *            [P] => 0.549          Price change percent
     *            [w] => 0.03944111     Weighted average price
     *            [x] => 0.03971300     Previous day's close price
     *            [c] => 0.03989900     Current day's close price
     *            [Q] => 1.23900000     Close trade's quantity
     *            [b] => 0.03985200     Best bid price
     *            [B] => 4.08300000     Best bid quantity
     *            [a] => 0.03990000     Best ask price
     *            [A] => 2.44700000     Best ask quantity
     *            [o] => 0.03968100     Open price
     *            [h] => 0.04010500     High price
     *            [l] => 0.03867800     Low price
     *            [v] => 17554.73900000 Total traded base asset volume
     *            [q] => 692.37834870   Total traded quote asset volume
     *            [O] => 1528544599463  Statistics open time
     *            [C] => 1528630999463  Statistics close time
     *            [F] => 3273094        First trade ID
     *            [L] => 3286309        Last trade Id
     *            [n] => 13216          Total number of trades
     *        )
     *
     *)
     *
     *Received:stdClass Object
     *(
     *    [stream] => dashbtc@kline_1h
     *    [data] => stdClass Object
     *        (
     *            [e] => kline                  Event type
     *            [E] => 1528630999924          Event time
     *            [s] => DASHBTC                Symbol
     *            [k] => stdClass Object
     *                (
     *                    [t] => 1528628400000  Kline start time
     *                    [T] => 1528631999999  Kline close time
     *                    [s] => DASHBTC        Symbol
     *                    [i] => 1h             Interval
     *                    [f] => 3285531        First trade ID
     *                    [L] => 3286310        Last trade ID
     *                    [o] => 0.03968900     Open price
     *                    [c] => 0.03987600     Close price
     *                    [h] => 0.04010500     High price
     *                    [l] => 0.03958700     Low price
     *                    [v] => 548.97700000   Base asset volume
     *                    [n] => 780            Number of trades
     *                    [x] =>                Is this kline closed?
     *                    [q] => 21.89919085    Quote asset volume
     *                    [V] => 270.71600000   Taker buy base asset volume
     *                    [Q] => 10.80139704    Taker buy quote asset volume
     *                    [B] => 0              Ignore
     *                )
     *
     *        )
     *
     *)
     *
     * @return void
     */
    public function run()
    {

        if (!$this->quotes) {
            return false;
        }

        $this->connectToSocket();

        echo '[' . date('Y-m-d H:i:s', time()) . '] binance started ' . PHP_EOL;

        $this->getKlinesViaHttp();

        \Amp\Loop::run(function () {
            /** @var \Amp\Websocket\Connection $connection */
            $connection = yield Websocket\connect($this->getWsUrl());
        
            while ($message = yield $connection->receive()) {
                $payload = yield $message->buffer();

                $streamData = json_decode($payload);

                if ($streamData->data->e == 'kline') {
                    $this->wsKlineDataReceived($streamData->data->k);
                }

                if ($streamData->data->e == '24hrTicker') {
                    $this->wsTickerDataReceived($streamData->data);
                }
        
                yield new Delayed(1000);
            }
        });
    }
}
