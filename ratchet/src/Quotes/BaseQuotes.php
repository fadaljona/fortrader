<?php
namespace Quotes;

use \ZMQ;
use \ZMQContext;

class BaseQuotes
{
    protected $socketConnect;
    protected $socket = null;
    protected $documentRoot;
    protected $db;
    protected $pdo = null;

    public function __construct($socketConnect, $configFromYii)
    {
        $this->socketConnect = $socketConnect;
        $this->documentRoot = $configFromYii->documentRoot;
        $this->db = $configFromYii->db;

        date_default_timezone_set($configFromYii->statsDataTimeZone);
    }

    protected function getDbConnection()
    {
        if ($this->pdo == null) {
            $this->initDbConnection();
        }

        try {
            $this->pdo->query("SELECT 1");
        } catch (\PDOException $e) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] DB Connection failed, reinitializing... ' . PHP_EOL;
            $this->initDbConnection();
        }
        return $this->pdo;
    }

    protected function connectToSocket()
    {
        $context = new ZMQContext();
        $this->socket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
        $this->socket->connect($this->socketConnect);
    }

    protected function initDbConnection()
    {
        try {
            echo '[' . date('Y-m-d H:i:s', time()) . '] Opening new DB connection... ' . PHP_EOL;
            $this->pdo = new \PDO($this->db['connectionString'], $this->db['username'], $this->db['password']);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            die($e->getMessage());
        }
    }

    public static function downloadFileFromWww($url, $timeout = 500)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return trim($data);
    }

    protected function sendAlert($period, $symbol, $msg, $url, $dataDate = '')
    {
        if ($dataDate) {
            $dataDate = '[dataDate='.$dataDate.']';
        }
        echo '[' . date('Y-m-d H:i:s', time()) . '] WARNING - ' . $msg . '. ' . $period
        .'. '. $symbol . ' [url=' . $url . '] ' . $dataDate . PHP_EOL;
    }
}
