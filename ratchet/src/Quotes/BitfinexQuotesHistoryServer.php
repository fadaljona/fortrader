<?php
namespace Quotes;

use \ZMQ;
use \ZMQContext;

class BitfinexQuotesHistoryServer
{
    protected $socketConnect;
    protected $documentRoot;
    protected $periods;
    protected $controlData = array();
    
    protected $timeout = 60 * 1000;
    protected $iterationDelay = 10;
    protected $circleDelay = 30;
    protected $sourceUrl = 'https://api.bitfinex.com/v2/candles/trade:';

    protected $quotesCount;
    protected $filesForIter;
    
    protected $quotes;

    protected $db;
    protected $pdo = null;

    protected $queue = array();

    public function __construct($socketConnect, $configFromYii)
    {
        $this->socketConnect = $socketConnect;
        $this->documentRoot = $configFromYii->documentRoot;

        $this->quotes = $configFromYii->quotes;
        $this->db = $configFromYii->db;

        $this->setQueue();
        echo '[' . date('Y-m-d H:i:s', time()) . '] constructor done ' . PHP_EOL;
    }
    private function setPeriods()
    {
        $this->periods = array(
            '1M' => array(
                'periodVal' => 43200,
                'frontPeriod' => 'MN1'
            ),
            '7D' => array(
                'periodVal' => 10080,
                'frontPeriod' => 'W1'
            ),
            '1D' => array(
                'periodVal' => 1440,
                'frontPeriod' => 'D1'
            ),
            '1h' => array(
                'periodVal' => 60,
                'frontPeriod' => 'H1'
            ),
        );
    }

    private function setControlData()
    {
        foreach ($this->quotes as $quote) {
            foreach ($this->periods as $periodName => $period) {
                $this->controlData[$quote->histName][$periodName] = '';
            }
        }
    }
    private function getPeriods()
    {
        if (!$this->periods) {
            $this->setPeriods();
        }
        return $this->periods;
    }
    private function setQueue()
    {
        $quotes = $this->getQuotes();

        $periods = $this->getPeriods();

        foreach ($periods as $periodName => $period) {
            foreach ($quotes as $quote) {
                $this->queue[] = array(
                    'periodName' => $periodName,
                    'periodVal' => $period['periodVal'],
                    'frontPeriod' => $period['frontPeriod'],
                    'quote' => $quote->name,
                    'quoteId' => $quote->id,
                    'histSymbol' => $quote->histName,
                );
            }
        }
    }
    private function setQuotes()
    {
        $this->quotesCount = count($this->quotes);
        $this->filesForIter = $this->quotesCount * count($this->getPeriods());

        $this->setControlData();
    }
    private function getQuotes()
    {
        $this->setQuotes();
        return $this->quotes;
    }
    private function executeFirstTask()
    {
        $tasks = $this->queue;
        foreach ($tasks as $file) {
            $url = $this->sourceUrl . $file['periodName'] .':'. $file['histSymbol'] . '/hist';

            $fileStr = self::downloadFileFromWww($url, $this->timeout);
            
            if (!$fileStr) {
                $this->sendAlert($file['periodName'], $file['quote'], 'no data', $url);
                continue;
            }
            
            $fileData = json_decode($fileStr);

            if ($fileData[0] == 'error') {
                $this->sendAlert($file['periodName'], $file['quote'], implode(' ', $fileData), $url);
                continue;
            }

            $this->saveData($fileData, $file['periodVal'], $file['quoteId']);
            
            sleep($this->iterationDelay);
        }
        return;
    }
    private function saveData($data, $period, $quoteId)
    {
        $returnStr = '';
        $deleteSql = 'DELETE FROM `ft_quotes_history_data` WHERE `resolution` = :period AND `symbolId` = :symbolId AND `barTime` >= :barTime';
        $deleteParams = array( ':period' => $period, ':symbolId' => $quoteId, ':barTime' => $data[count($data)-1][0] / 1000 );
        
        $insertSql = 'INSERT INTO `ft_quotes_history_data` (`symbolId`, `barTime`, `high`, `low`, `open`, `close`, `volume`, `resolution`) VALUES ';
        $insertParams = array( ':symbolId' => $quoteId, ':resolution' => $period );

        $i=0;
        foreach ($data as $oneData) {

            $time = $oneData[0] / 1000;
            if ($i) {
                $insertSql .= ',';
            }
            $insertSql .= "( :symbolId, :barTime$i, :high$i, :low$i, :open$i, :close$i, :volume$i, :resolution)";
            $insertParams[":barTime$i"] = $time;
            $insertParams[":open$i"] = $oneData[1];
            $insertParams[":close$i"] = $oneData[2];
            $insertParams[":high$i"] = $oneData[3];
            $insertParams[":low$i"] = $oneData[4];
            $insertParams[":volume$i"] = $oneData[5];
            $i++;
        }

        $pdo = $this->getDbConnection();

        try {
            $stmt = $pdo->prepare($deleteSql);
            $stmt->execute($deleteParams);
            
            $stmt = $pdo->prepare($insertSql);
            $stmt->execute($insertParams);
        } catch (\PDOException $e) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] '.$e->getMessage().' ' . PHP_EOL;
        }
        return;
    }

    private function getDbConnection()
    {
        if ($this->pdo == null) {
            $this->initDbConnection();
        }

        try {
            $this->pdo->query("SELECT 1");
        } catch (\PDOException $e) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] DB Connection failed, reinitializing... ' . PHP_EOL;
            $this->initDbConnection();
        }
        return $this->pdo;
    }
    private function initDbConnection()
    {
        try {
            echo '[' . date('Y-m-d H:i:s', time()) . '] Opening new DB connection... ' . PHP_EOL;
            $this->pdo = new \PDO($this->db['connectionString'], $this->db['username'], $this->db['password']);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            die($e->getMessage());
        }
    }
    protected function sendAlert($period, $symbol, $msg, $url, $dataDate = '')
    {
        if ($dataDate) {
            $dataDate = '[dataDate='.$dataDate.']';
        }
        echo '[' . date('Y-m-d H:i:s', time()) . '] WARNING - ' . $msg . '. ' . $period 
        .'. '. $symbol . ' [url=' . $url . '] ' . $dataDate . PHP_EOL;
    }

    public function run()
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
        $socket->connect($this->socketConnect);
        
        echo '[' . date('Y-m-d H:i:s', time()) . '] Execute first task ' . PHP_EOL;

        $this->executeFirstTask();
        
        echo '[' . date('Y-m-d H:i:s', time()) . '] Task Executed ' . PHP_EOL;

        echo '[' . date('Y-m-d H:i:s', time()) . '] Candles Server started ' . PHP_EOL;
    
        while (true) {
            foreach ($this->queue as $file) {
                $url = $this->sourceUrl . $file['periodName'] .':'. $file['histSymbol'] . '/last';

                $fileStr = self::downloadFileFromWww($url, $this->timeout);

                if (!$fileStr) {
                    $this->sendAlert($file['periodName'], $file['quote'], 'no data', $url);
                    continue;
                }
                
                $fileData = json_decode($fileStr);
                if ($fileData[0] == 'error') {
                    $this->sendAlert($file['periodName'], $file['quote'], implode(' ', $fileData), $url);
                    continue;
                }

                $name = $file['quote'] . '_' .  $file['frontPeriod'];
                    
                if ($this->controlData[$file['histSymbol']][$file['periodName']] == $fileStr) {
                    $this->sendAlert($file['periodName'], $file['quote'], 'no update', $url, date('Y-m-d H:i:s'));
                } else {
                    $this->controlData[$file['histSymbol']][$file['periodName']] = $fileStr;
                    
                    $this->saveData(array($fileData), $file['periodVal'], $file['quoteId']);
                        
                    $dataToSend = json_encode(array(
                        'key' => $name,
                        'data' => date('Y-m-d H:i', $fileData[0] / 1000) . ';' .
                        $fileData[1] . ';' .
                        $fileData[3] . ';' .
                        $fileData[4] . ';' .
                        $fileData[2] . ';' .
                        $fileData[5]
                    ));

                    $socket->send($dataToSend);
                }
                sleep($this->iterationDelay);
            }
            sleep($this->circleDelay);
        }
    }




    public static function downloadFileFromWww($url, $timeout = 500)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return trim($data);
    }
}
