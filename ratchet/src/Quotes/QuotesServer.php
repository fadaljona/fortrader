<?php
namespace Quotes;
use \ZMQ;
use \ZMQContext;

class QuotesServer{
	protected $delay;
	protected $quotesSource;
	protected $hoursCorrection;
	protected $socketConnect;
	protected $timezone = 'Europe/Moscow';
	protected $timeOffset;
	protected $documentRoot;
	protected $periodGetQuotesControl;
	protected $notificationEmails;
	protected $quotes;
	
	public function __construct( $delay, $socketConnect, $configFromYii ) {
		$this->documentRoot = $configFromYii->documentRoot;
		
        $this->delay = $delay;
		$this->quotesSource = explode('||',$configFromYii->quotesTickSource);

		foreach( $this->quotesSource as &$source ){
			$sourceArr = explode('|',$source );
			if( isset( $sourceArr[1] ) ){
				$this->hoursCorrection[$sourceArr[0]] = $sourceArr[1];
				$source = $sourceArr[0];
			}
		}
		unset($source);

		$this->socketConnect = $socketConnect;
		date_default_timezone_set( $this->timezone );
		$this->timeOffset = $configFromYii->realQuotesDataTimeDifference * 3600;
		$this->periodGetQuotesControl = $configFromYii->periodGetQuotesControl *60;
		$this->notificationEmails = explode(',', $configFromYii->notificationEmails);
		$this->quotes = $configFromYii->quotes;
		
		echo '[' . date('Y-m-d H:i:s', time()) . '] constructor done ' . PHP_EOL;
		
    }
	public function run(){
		$context = new ZMQContext();
		$socket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
		$socket->connect($this->socketConnect);

		$oldQuotes = $this->parseQuotes( );
		$oldQuotesStr = implode('', $oldQuotes);
		
		$startTime = time();

		echo '[' . date('Y-m-d H:i:s', time()) . '] QuotesServer started ' . PHP_EOL;
		
		while(true){
			$weekDay = date( 'N', time() + $this->timeOffset );
			
			if( $weekDay != 6 && $weekDay != 7 ){
				
				$quotesArr = $this->parseQuotes( );
		
				$checkTime = time()-$startTime;
				if( $checkTime > $this->periodGetQuotesControl ){
					$quotesStr = implode('', $quotesArr);
					if( !$quotesStr || $quotesStr == $oldQuotesStr ){
						$this->sendAlert();
					}
					$oldQuotesStr = $quotesStr;
					$startTime = time();
				}
				
				$changedQuotes = array();
				foreach( $quotesArr as $quote => $val ){
					if( isset($oldQuotes[$quote]) && isset($this->quotes[$quote]) && $oldQuotes[$quote] != $val ){
						$changedQuotes[$this->quotes[$quote]] = $val;
					}
				}
				if( count($changedQuotes) ){
					$socket->send(json_encode( array( 'key' => 'ALL', 'data' => $changedQuotes ) ));
				}
				$oldQuotes = $quotesArr;
				unset($changedQuotes);
				usleep($this->delay);
				
			}else{
				sleep(600);
			}
		}
	}
	protected function parseQuotes(){
		
		$prevSavedDataFile = $this->documentRoot . '/services/protected/data/prevQuotesTickData.txt';
		$prevSavedData = unserialize( file_get_contents($prevSavedDataFile) );
		
		$sources = array_reverse($this->quotesSource);
		
		$summArr = array();
		
		foreach( $sources as $source ){
			$parsedData = $this->parseOneFile( $source );
			foreach( $parsedData as $key => $data ){
					
				if( isset($summArr[$key]) ) continue;
				if( $data['origin'] == $prevSavedData[$source][$key] ) continue;
				$summArr[$key] = $data['toSend'];
			}
		}
		
		return $summArr;
	}
	protected function parseOneFile( $url ){
		$quotesStr = $this->curlGetQuotes( $url );
		$quotesStr = str_replace(',', '.', $quotesStr);
		$quotes = explode(PHP_EOL, $quotesStr);
		if( !$quotes ) return false;
		$quotesArr = array();
		foreach( $quotes as $quote ){
			$delimerPos = strpos( $quote, ';' );
			$symbol = substr( $quote, 0, $delimerPos );
			if( !isset( $this->quotes[$symbol] ) ) continue;

			$timeStartPos = strpos( $quote, ';', strpos( $quote, ';', strpos( $quote, ';' )+1 )+1 )+1;
			$timeEndPos = strpos( $quote, ';', $timeStartPos );

			if( $timeStartPos == $timeEndPos ){
				$quote = substr( $quote, 0, $timeStartPos) . date('H:i:s') . substr( $quote, $timeEndPos);
			}

		//	echo $timeStartPos . "\t$timeEndPos\t" . substr( $quote, $timeStartPos, 8) . "\t" . $quote . "\n";

			if( isset( $this->hoursCorrection[$url] ) ){
				$timeVal = substr( $quote, $timeStartPos, 8);
				$quote = str_replace( $timeVal, $this->correctTime( $timeVal, $this->hoursCorrection[$url] ), $quote );
			}
			
			$tmpArr = explode(';', $quote);
			$name = @$tmpArr[0];
			
			$tmpData = array(
				'bid' => @$tmpArr[1],
				'ask' => @$tmpArr[2],
				'time' => @$tmpArr[3],
				'spread' => @$tmpArr[4],
				'trend' => @$tmpArr[5],
				'pcp' => @$tmpArr[9],
			);
			$quotesArr[$name] = array(
				'toSend' => json_encode($tmpData),
				'origin' => $quote
			);
		}
	//	print_r( $quotesArr );
		return $quotesArr;
	}
	protected function correctTime( $timeStr, $offset = 0 ){
		if( !$timeStr ) return false;
		$time = strtotime( $timeStr ) + 60*60*24 + $offset * 60 * 60;
		return date('H:i:s', $time);
	}
	protected function curlGetQuotes( $url ){
		$header[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; 
		$header[] = "Cache-Control: max-age=0"; 
		$header[] = "Connection: keep-alive"; ; 
		$header[] = "Accept-Language: ru-RU,ru;q=0.5"; 
		$header[] = "Pragma: ";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
		return curl_exec($ch);
	}
	protected function sendAlert(){
		echo '[' . date('Y-m-d H:i:s', time()) . '] WARNING - tick quotes problem. Cheched period ' . $this->periodGetQuotesControl . PHP_EOL;
	/*	$mail = new \PHPMailer;
		$mail->CharSet = 'UTF-8';
		$mail->setFrom('admin3@fortrader.org', 'ForTrader.Ru');
		foreach($this->notificationEmails as $email){
			$mail->ClearAllRecipients();
			$mail->addAddress($email, $email);
			$mail->Subject = 'Проблема с тиковыми котировками '.date('Y-m-d H:i:s', time());
			$mail->Body = 'Тиковые котировки не обновляются в течении '.$this->periodGetQuotesControl . 
			"\n\n" . 'Письмо отправлено по времени сервера '.date('Y-m-d H:i:s', time()) . 
			"\n\n" . md5(time());
			$mail->send();
		}*/
	}
}
