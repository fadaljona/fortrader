<?php
namespace Quotes;
use \ZMQ;
use \ZMQContext;

class QuotesHistoryServer{
	protected $socketConnect;
	protected $documentRoot;
	protected $timeout;	//milliseconds - таймаут на получение данных
	protected $ratio1MtoOther;	//соотношения частоты обновления минутного периода к остальным(количество проходов для обновления всех периодов 1 раз и $ratio1MtoOther раз минутного) - 5 означает что все остальные периоды будут обновляться в 5 раз реже минутных
	protected $iterationTime;	//секунды - время 1 итерации - $ratio1MtoOther количество итераций, если файлы итерации получены быстрее $iterationTime то ждем до $iterationTime
	protected $histUrlData;

	protected $periods;
	protected $quotes;
	
	protected $quotesCount;
	protected $addToFirstIter;
	protected $filesForIter;
	protected $queue = array();
	protected $symbolGetQuotesControl = 'EURUSD';
	protected $periodGetQuotesControl;
	protected $notificationEmails;
	protected $controlData = array();
	
	protected $db;
	protected $pdo = null;
	
	public function __construct( $timeout, $ratio1MtoOther, $iterationTime, $socketConnect, $configFromYii ) {
		$this->documentRoot = $configFromYii->documentRoot;
		
		$this->timeout = $timeout;
		$this->ratio1MtoOther = $ratio1MtoOther; 
		$this->iterationTime = $iterationTime;
		$this->histUrlData = $configFromYii->statsUrlData;
		
		$this->symbolGetQuotesControl = $configFromYii->symbolGetQuotesControl;
		$this->periodGetQuotesControl = $configFromYii->periodGetQuotesControl * 60 ;
		$this->notificationEmails = explode(',', $configFromYii->notificationEmails);

		$this->socketConnect = $socketConnect;
		date_default_timezone_set( $configFromYii->statsDataTimeZone );
		
		$this->quotes = $configFromYii->quotes;
		$this->db = $configFromYii->db;
		
		$this->setQueue();
		
		echo '[' . date('Y-m-d H:i:s', time()) . '] constructor done ' . PHP_EOL;
    }
	private function setPeriods(){
		$this->periods = array(
			'MN1' => array(
				'periodVal' => 43200,
				'daysOffset' => 450,
				'firstDaysOffset' => 7200,
			),
			'W1' => array(
				'periodVal' => 10080,
				'daysOffset' => 105,
				'firstDaysOffset' => 4200,
			),
			'D1' => array(
				'periodVal' => 1440,
				'daysOffset' => 15,
				'firstDaysOffset' => 1460,
			),
			'H4' => array(
				'periodVal' => 240,
				'daysOffset' => 3,
				'firstDaysOffset' => 250,
			),
			'H1' => array(
				'periodVal' => 60,
				'daysOffset' => 1,
				'firstDaysOffset' => 62,
			),
			/*'30M' => array(
				'periodVal' => 30,
				'daysOffset' => 1,
				'firstDaysOffset' => 60,
			),
			'15M' => array(
				'periodVal' => 15,
				'daysOffset' => 1,
				'firstDaysOffset' => 60,
			),
			'5M' => array(
				'periodVal' => 5,
				'daysOffset' => 1,
				'firstDaysOffset' => 5,
			),*/
		);
	}
	private function setControlData(){
		foreach( $this->quotes as $quote ){
			foreach($this->periods as $key => $val){
				$this->controlData[$quote->name][$key] = '';
			}
		}
	}
	private function getPeriods(){
		if( !$this->periods ){
			$this->setPeriods();
		}
		return $this->periods;
	}
	private function setQuotes(){
		$this->quotesCount = count($this->quotes);
		$filesToSaveCount = $this->quotesCount * count($this->getPeriods()) + $this->quotesCount * $this->ratio1MtoOther;
		$this->addToFirstIter = $filesToSaveCount % $this->ratio1MtoOther;
		$this->filesForIter = ($filesToSaveCount - $this->addToFirstIter) / $this->ratio1MtoOther;
		
		$this->setControlData();
	}
	private function getQuotes(){
		$this->setQuotes();
		return $this->quotes;
	}
	private function setQueue(){
		$task1M = array();
		$taskOther = array();
		$quotes = $this->getQuotes();
		$periods = $this->getPeriods();
		
		/*foreach( $quotes as $quote ){
			$task1M[] = array(
				'url' => $this->histUrlData . $quote->histName . '&period=1&',
				'daysOffset' => 1,
				'periodName' => '1M',
				'periodVal' => 1,
				'firstDaysOffset' => 3,
				'quote' => $quote->name,
				'quoteId' => $quote->id,
			);
		}*/
		
		foreach( $periods as $periodName => $period ){
			foreach( $quotes as $quote ){
				$taskOther[] = array(
					'currentHistAliasKey' => 0,
					'periodParam' => "&period={$period['periodVal']}&",
					'daysOffset' => $period['daysOffset'],
					'periodName' => $periodName,
					'periodVal' => $period['periodVal'],
					'firstDaysOffset' => $period['firstDaysOffset'],
					'quote' => $quote->name,
					'quoteId' => $quote->id,
					'histAliases' => $quote->histAliases,
				);
			}
		}

		for($i=0; $i < $this->ratio1MtoOther; $i++){
			$this->queue = array_merge($this->queue, $task1M);
			for( $j=0; $j < ( $this->filesForIter - $this->quotesCount ); $j++ ){
				array_push( $this->queue, $taskOther[0] );
				$taskOther = array_slice( $taskOther, 1);
			}
			if( $i==0 ){
				for( $j=0; $j < $this->addToFirstIter; $j++ ){
					array_push( $this->queue, $taskOther[0] );
					$taskOther = array_slice( $taskOther, 1);
				}
			}
		}
	}
	protected function sendAlert($period, $symbol, $msg, $url, $dataDate = ''){
		if( $dataDate ) $dataDate = '[dataDate='.$dataDate.']';
		echo '[' . date('Y-m-d H:i:s', time()) . '] WARNING - ' . $msg . '. ' . $period 
		.'. '. $symbol . ' [url=' . $url . '] ' . $dataDate . PHP_EOL;
	}
	private function getFirstTask(){
		$quotes = $this->getQuotes();
		$periods = $this->getPeriods();
		$task = array();
		/*foreach( $quotes as $quote ){
			$task[] = array(
				'url' => $this->histUrlData . $quote->histName . '&period=1&',
				'daysOffset' => 1,
				'periodName' => '1M',
				'periodVal' => 1,
				'firstDaysOffset' => 3,
				'quote' => $quote->name,
				'quoteId' => $quote->id,
			);
		}*/
		foreach( $periods as $periodName => $period ){
			foreach( $quotes as $quote ){
				$task[] = array(
					'url' => $this->histUrlData . $quote->histAliases[0] . "&period={$period['periodVal']}&",
					'daysOffset' => $period['daysOffset'],
					'periodName' => $periodName,
					'periodVal' => $period['periodVal'],
					'firstDaysOffset' => $period['firstDaysOffset'],
					'quote' => $quote->name,
					'quoteId' => $quote->id,
					'histAliases' => $quote->histAliases,
				);
			}
		}
		return $task;
	}
	private function executeFirstTask(){
		$tasks = $this->getFirstTask();
		$lastTimeArr = array();
		foreach( $tasks as $file ){
			$url = $file['url'] . 'datestart=' . date('d.m.Y', time() - 60*60*24 *  $file['firstDaysOffset']  ) . '&dateend=' . date('d.m.Y', time()+ 60*60*24);
			
			$fileStr = self::downloadFileFromWww($url, $this->timeout);
			if( !$fileStr ){
				$this->sendAlert($file['periodName'], $file['quote'], 'no data', $url );
				continue;
			}
			$name = $file['quote'] . '_' .  $file['periodName'];
			
			$this->saveData( $fileStr, $file['periodVal'], $file['quoteId'] );
			
			$lastTimeArr[$file['quote']][$file['periodName']] = self::getLastDate( $fileStr);
		}
		return $lastTimeArr;
	}
	private function setupDataArr(){
		$dataArr = array();
		$quotes = $this->getQuotes();
		$periods = $this->getPeriods();
		/*foreach( $quotes as $quote ){
			$dataArr[ $quote->name . '_1M' ] = 'q';
		}*/
		foreach( $periods as $periodName => $period ){
			foreach( $quotes as $quote ){
				$dataArr[ $quote->name . '_'.$periodName ] = 'q';
			}
		}
		return $dataArr;
	}
	private function getDbConnection() {
		if ($this->pdo == null) {
			$this->initDbConnection();
		}

		try {
			$this->pdo->query("SELECT 1");
        } catch (\PDOException $e) {
			echo '[' . date('Y-m-d H:i:s', time()) . '] DB Connection failed, reinitializing... ' . PHP_EOL;
			$this->initDbConnection();
        }
		return $this->pdo;
	}
	private function initDbConnection() {
		try {
			echo '[' . date('Y-m-d H:i:s', time()) . '] Opening new DB connection... ' . PHP_EOL;
			$this->pdo = new \PDO($this->db['connectionString'], $this->db['username'], $this->db['password']);
			$this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch (\PDOException $e) {
			die($e->getMessage());
		}
	}
	private function saveData( $data, $period, $quoteId ){
		$explodedDataToStrs = explode("\n", $data);
		$returnStr = '';
		$deleteSql = 'DELETE FROM `ft_quotes_history_data` WHERE `resolution` = :period AND `symbolId` = :symbolId AND `barTime` >= :barTime';
		$deleteParams = array( ':period' => $period, ':symbolId' => $quoteId );
		$insertSql = 'INSERT INTO `ft_quotes_history_data` (`symbolId`, `barTime`, `high`, `low`, `open`, `close`, `volume`, `resolution`) VALUES ';
		$insertParams = array( ':symbolId' => $quoteId, ':resolution' => $period );
		
		$i=0;
		foreach( $explodedDataToStrs as $str ){
			$explodedStr = explode(';', $str);
			$time = strtotime( trim( $explodedStr[0] ) );
			if( $i==0 ) {
				$deleteParams[':barTime'] = $time;
				$returnStr .= str_replace( $explodedStr[0], $time, $str );
			}
			if( $i ){ 
				$insertSql .= ',';
				$returnStr .= "\n" . str_replace( $explodedStr[0], $time, $str );
			}
			$insertSql .= "( :symbolId, :barTime$i, :high$i, :low$i, :open$i, :close$i, :volume$i, :resolution)";
			$insertParams[":barTime$i"] = $time;
			$insertParams[":open$i"] = trim( $explodedStr[1] ) ;
			$insertParams[":high$i"] = trim( $explodedStr[2] ) ;
			$insertParams[":low$i"] = trim( $explodedStr[3] ) ;
			$insertParams[":close$i"] = trim( $explodedStr[4] ) ;
			$insertParams[":volume$i"] = trim( $explodedStr[5] ) ;
			$i++;
		}
		
		$pdo = $this->getDbConnection();
		
		try {
			$stmt = $pdo->prepare( $deleteSql );
			$stmt->execute( $deleteParams );
			
			$stmt = $pdo->prepare( $insertSql );
			$stmt->execute( $insertParams );
		}catch (\PDOException $e) {
			echo '[' . date('Y-m-d H:i:s', time()) . '] '.$e->getMessage().' ' . PHP_EOL;
		}
		return $returnStr;
	}
	
	
	public function run(){
		$context = new ZMQContext();
		$socket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
		$socket->connect($this->socketConnect);	
		
		echo '[' . date('Y-m-d H:i:s', time()) . '] Execute first task ' . PHP_EOL;
		
		$lastTimeArr = $this->executeFirstTask();
		
		echo '[' . date('Y-m-d H:i:s', time()) . '] Task Executed ' . PHP_EOL;
		
		$dataArr = $this->setupDataArr();
		
		//$monitorTime = time();
		
		echo '[' . date('Y-m-d H:i:s', time()) . '] QuotesHistoryServer started ' . PHP_EOL;
	
		while(true){
			$weekDay = date( 'N', time() + 60 * 60 );
			
			if( $weekDay != 6 && $weekDay != 7 ){
				
				$startTime = time();
				foreach( $this->queue as $id => $file ){

					//ожидание если отработал раньше
					if( ($id+1) % $this->filesForIter == 0 ){
						usleep( 50000 );
						$lastIterTime = time()-$startTime;
						if( $lastIterTime < $this->iterationTime ){
							sleep($this->iterationTime-$lastIterTime);
						}
						$startTime = time();
					}
					
					//запрос данных с последней даты
					if( isset( $lastTimeArr[$file['quote']] ) && isset( $lastTimeArr[$file['quote']][$file['periodName']] ) ){
						$timeStart = $lastTimeArr[$file['quote']][$file['periodName']];
					}else{
						$timeStart = time() - 60*60*24 *  $file['firstDaysOffset'];
					}
					$url = $this->histUrlData . $file['histAliases'][$file['currentHistAliasKey']] . $file['periodParam'] . 'datestart=' . date('d.m.Y', $timeStart ) . '&dateend=' . date('d.m.Y', time()+ 60*60*24);
					$fileStr = self::downloadFileFromWww($url, $this->timeout);

					if( !$fileStr ){
						$this->sendAlert($file['periodName'], $file['quote'], 'no data', $url );
						
						
						if( count($file['histAliases']) > $file['currentHistAliasKey'] && count($file['histAliases']) != 1 ){
							if( count($file['histAliases']) == $file['currentHistAliasKey'] +1 ){
								$this->queue[$id]['currentHistAliasKey'] = 0;
							}else{
								$this->queue[$id]['currentHistAliasKey'] = $file['currentHistAliasKey'] +1;
							}
							$this->sendAlert($file['periodName'], $file['quote'], 'changed alias to ' . $file['histAliases'][$this->queue[$id]['currentHistAliasKey']], '');
						}
						
						
						continue;
					} 
					
					if( isset( $lastTimeArr[$file['quote']][$file['periodName']] ) ){
						$dataToSave = self::getContentAfterLastDate($fileStr, $lastTimeArr[$file['quote']][$file['periodName']]);
					}else{
						$dataToSave = $fileStr;
					}
					
					$lastTimeArr[$file['quote']][$file['periodName']] = self::getLastDate($dataToSave);
					$lastStr = self::getLastStrs( $dataToSave, 1 );
					$name = $file['quote'] . '_' .  $file['periodName'];
					
					/*if( $this->symbolGetQuotesControl == $file['quote'] && (time()-$monitorTime) > $this->periodGetQuotesControl ){
						if( $this->controlData[$file['periodName']] == $dataToSave ){
							$this->sendAlert($file['periodName'], $file['quote']);
						}
						$this->controlData[$file['periodName']] = $dataToSave;
						$monitorTime = time();
					}*/	
					
					if( $file['periodName'] != '1M' && $this->controlData[$file['quote']][$file['periodName']] == $dataToSave ){
						$this->sendAlert($file['periodName'], $file['quote'], 'no update', $url, substr( $lastStr, 0, strpos( $lastStr, ';' ) ) );
						
					
						if( count($file['histAliases']) > $file['currentHistAliasKey'] && count($file['histAliases']) != 1 ){
							if( count($file['histAliases']) == $file['currentHistAliasKey'] +1 ){
								$this->queue[$id]['currentHistAliasKey'] = 0;
							}else{
								$this->queue[$id]['currentHistAliasKey'] = $file['currentHistAliasKey'] +1;
							}
							$this->sendAlert($file['periodName'], $file['quote'], 'changed alias to ' . $file['histAliases'][$this->queue[$id]['currentHistAliasKey']], '');
						}
						
						
					}
					$this->controlData[$file['quote']][$file['periodName']] = $dataToSave;
					
					
					if( isset($dataArr[$name]) && $dataArr[$name] != $lastStr ){
						$dataToSave = $this->saveData( $dataToSave, $file['periodVal'], $file['quoteId'] );
						$lastStrsToPush = self::getLastStrs( $dataToSave, 50 );//50 последних строк
						$socket->send(json_encode( array( 'key' => $name, 'data' => $lastStrsToPush ) ));
						//echo $lastStrsToPush;	
						//echo "______{$name}________\n\n";
					}	
					$dataArr[$name] = $lastStr;
				}
				
			}else{
				sleep(600);
			}
		}
	}

	static function downloadFileFromWww( $url, $timeout=500 ) {
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_TIMEOUT_MS, $timeout);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT_MS, $timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return trim($data);
	}
	static function getLastNumPos( $fileStr, $iters, $lastStrPos = false ){
		if( $lastStrPos === false ){
			$lastStrPos = strrpos($fileStr, "\n");
		}else{
			$lastStrPos = strrpos($fileStr, "\n", $lastStrPos-strlen($fileStr)-1 );
		}
		if( $lastStrPos && ($iters > 1) ){	
			return self::getLastNumPos( $fileStr, $iters-1, $lastStrPos );
		}else{
			return $lastStrPos;
		}
	}
	static function getLastStrs( $fileStr, $num ){
		return trim( substr( $fileStr , self::getLastNumPos( $fileStr, $num)  ) );
	}
	static function getLastDate( $str ){
		$str = self::getLastStrs( $str, 1 );
		$exploded = explode(';', $str);
		return strtotime( $exploded[0] );
	}
	static function getContentAfterLastDate($fileStr, $date){
		$date = date('Y-m-d H:i', $date);
		return trim( substr( $fileStr , strrpos($fileStr, $date) ) );
	}
	
}