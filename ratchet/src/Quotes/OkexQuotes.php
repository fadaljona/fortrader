<?php
namespace Quotes;

use \ZMQ;
use \ZMQContext;

class OkexQuotes
{
    protected $socketConnect;
    protected $documentRoot;
    protected $periods;
    protected $controlData = array();
    protected $tickControlData = array();
    
    protected $timeout = 60 * 1000;
    protected $iterationDelay = 2;
    protected $circleDelay = 10;
    protected $histSourceUrl = 'https://www.okex.com/api/v1/kline.do?symbol=';
    protected $tickSourceUrl = 'https://www.okex.com/api/v1/ticker.do?symbol=';

    protected $quotesCount;
    
    protected $histQuotes;
    protected $tickQuotes;
    protected $lastTimeArr;

    protected $db;
    protected $pdo = null;

    protected $queue = array();

    public function __construct($socketConnect, $configFromYii)
    {
        $this->socketConnect = $socketConnect;
        $this->documentRoot = $configFromYii->documentRoot;

        $this->histQuotes = $configFromYii->histQuotes;
        $this->tickQuotes = $configFromYii->tickQuotes;
        $this->db = $configFromYii->db;
        $this->setPeriods();

        $this->setQueue();
        echo '[' . date('Y-m-d H:i:s', time()) . '] constructor done ' . PHP_EOL;
    }
    private function setPeriods()
    {
        $this->periods = array(
            '1week' => array(
                'periodVal' => 10080,
                'frontPeriod' => 'W1'
            ),
            '1day' => array(
                'periodVal' => 1440,
                'frontPeriod' => 'D1'
            ),
            '1hour' => array(
                'periodVal' => 60,
                'frontPeriod' => 'H1'
            ),
        );
    }

    private function setControlData()
    {
        foreach ($this->histQuotes as $quote) {
            foreach ($this->periods as $periodName => $period) {
                $this->controlData[$quote->histName][$periodName] = '';
            }
        }
        foreach ($this->tickQuotes as $symbol => $quote) {
            $this->tickControlData[$symbol] = '';
        }
    }
    private function setQueue()
    {
        $quotes = $this->getQuotes();

        foreach ($this->periods as $periodName => $period) {
            foreach ($quotes as $quote) {
                $this->queue[] = array(
                    'type' => 'hist',
                    'periodName' => $periodName,
                    'periodVal' => $period['periodVal'],
                    'frontPeriod' => $period['frontPeriod'],
                    'quote' => $quote->name,
                    'quoteId' => $quote->id,
                    'histSymbol' => $quote->histName,
                );
            }
        }
        foreach ($this->tickQuotes as $symbol => $quote) {
            $this->queue[] = array(
                'type' => 'tick',
                'quote' => $quote,
                'symbol' => $symbol
            );
        }
    }
    private function setQuotes()
    {
        $this->quotesCount = count($this->histQuotes);
        $this->setControlData();
    }
    private function getQuotes()
    {
        $this->setQuotes();
        return $this->histQuotes;
    }
    private function saveData($data, $period, $quoteId)
    {
        $returnStr = '';
        $deleteSql = 'DELETE FROM `ft_quotes_history_data` WHERE `resolution` = :period AND `symbolId` = :symbolId AND `barTime` >= :barTime';
        $deleteParams = array( ':period' => $period, ':symbolId' => $quoteId );
        
        $insertSql = 'INSERT INTO `ft_quotes_history_data` (`symbolId`, `barTime`, `high`, `low`, `open`, `close`, `volume`, `resolution`) VALUES ';
        $insertParams = array( ':symbolId' => $quoteId, ':resolution' => $period );
        
        $i=0;
        foreach ($data as $oneData) {
            $time = $oneData[0] / 1000;
            if ($time < 0) {
                continue;
            }

            $prevBarTimeIndex = ':barTime' . ($i-1);
            if ($i > 1 && $insertParams[$prevBarTimeIndex] == $time) {
                continue;
            }

            if ($i==0) {
                $deleteParams[':barTime'] = $time;
                $returnStr .= json_encode($oneData);
            }
            if ($i) {
                $insertSql .= ',';
                $returnStr .= "\n" . json_encode($oneData);
            }
            $insertSql .= "( :symbolId, :barTime$i, :high$i, :low$i, :open$i, :close$i, :volume$i, :resolution)";
            $insertParams[":barTime$i"] = $time;
            $insertParams[":open$i"] = $oneData[1];
            $insertParams[":high$i"] = $oneData[2];
            $insertParams[":low$i"] = $oneData[3];
            $insertParams[":close$i"] = $oneData[4];
            $insertParams[":volume$i"] = $oneData[5];
            $i++;
        }

        
        $pdo = $this->getDbConnection();
        
        try {
            $stmt = $pdo->prepare($deleteSql);
            $stmt->execute($deleteParams);
            
            $stmt = $pdo->prepare($insertSql);
            $stmt->execute($insertParams);
        } catch (\PDOException $e) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] '.$e->getMessage().' ' . PHP_EOL;
        }
        return $returnStr;
    }

    private function getDbConnection()
    {
        if ($this->pdo == null) {
            $this->initDbConnection();
        }

        try {
            $this->pdo->query("SELECT 1");
        } catch (\PDOException $e) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] DB Connection failed, reinitializing... ' . PHP_EOL;
            $this->initDbConnection();
        }
        return $this->pdo;
    }
    private function initDbConnection()
    {
        try {
            echo '[' . date('Y-m-d H:i:s', time()) . '] Opening new DB connection... ' . PHP_EOL;
            $this->pdo = new \PDO($this->db['connectionString'], $this->db['username'], $this->db['password']);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            die($e->getMessage());
        }
    }
    protected function sendAlert($period, $symbol, $msg, $url, $dataDate = '')
    {
        if ($dataDate) {
            $dataDate = '[dataDate='.$dataDate.']';
        }
        echo '[' . date('Y-m-d H:i:s', time()) . '] WARNING - ' . $msg . '. ' . $period 
        .'. '. $symbol . ' [url=' . $url . '] ' . $dataDate . PHP_EOL;
    }

    public function run()
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
        $socket->connect($this->socketConnect);
        
        echo '[' . date('Y-m-d H:i:s', time()) . '] Started ' . PHP_EOL;
    
        while (true) {
            foreach ($this->queue as $file) {
                if ($file['type'] == 'hist') {
                    $this->histIteration($file, $socket);
                } else {
                    $this->tickIteration($file, $socket);
                }
                sleep($this->iterationDelay);
            }
            sleep($this->circleDelay);
        }
    }
    private function tickIteration($file, $socket)
    {
        $url = $this->tickSourceUrl . $file['symbol'];
        $data = $this->parseQuotes($url);
        if (!$data) {
            return false;
        }
        if ($this->tickControlData[$file['symbol']] != $data['compare']) {
            $socket->send(json_encode(array( 'key' => 'ALL', 'data' => array(
                $file['quote'] => $data['toSend']
            ))));
            $this->tickControlData[$file['symbol']] == $data['compare'];
        }
    }
    protected function parseQuotes($url)
    {
        $json = $this->curlGetQuotes($url);

        if (!$json) {
            $this->sendAlert('', '', "No data from okex", $url);
            return false;
        }

        $json = json_decode($json);

        if (!$json) {
            $this->sendAlert('', '', "No data from okex", $url);
            return false;
        }

        return array(
            'toSend' => json_encode(array(
                'bid' => $json->ticker->buy,
                'ask' => $json->ticker->sell,
                'time' => date('H:i:s', $json->date - 60*60*2),
            )),
            'compare' => $json->ticker->buy . '__' . $json->ticker->sell,
        );
    }
    private function histIteration($file, $socket)
    {
        $url = $this->histSourceUrl . $file['histSymbol'] .'&type='. $file['periodName'];
        $fileStr = self::downloadFileFromWww($url, $this->timeout);

        if (!$fileStr) {
            $this->sendAlert($file['periodName'], $file['quote'], 'no data', $url);
            return;
        }
                
        $fileData = json_decode($fileStr);

        if (!$fileData || !count($fileData)) {
            $this->sendAlert($file['periodName'], $file['quote'], 'no data', $url);
            return;
        }

        $name = $file['quote'] . '_' .  $file['frontPeriod'];

        if (isset($this->lastTimeArr[$file['quote']][$file['periodName']])) {
            $dataToSave = self::getDataAfterLastDate(
                $fileData,
                $this->lastTimeArr[$file['quote']][$file['periodName']]
            );
        } else {
            $dataToSave = $fileData;
        }

        $lastTime = end($dataToSave)[0] / 1000;
        reset($dataToSave);
        $this->lastTimeArr[$file['quote']][$file['periodName']] = $lastTime;
        $encodedDataToSave = json_encode($dataToSave);

                    
        if ($this->controlData[$file['histSymbol']][$file['periodName']] == $encodedDataToSave) {
            $this->sendAlert($file['periodName'], $file['quote'], 'no update', $url, date('Y-m-d H:i:s'));
        } else {
            $this->controlData[$file['histSymbol']][$file['periodName']] = $encodedDataToSave;
                    
            $this->saveData($dataToSave, $file['periodVal'], $file['quoteId']);
                        
            $lastStrsToPush = self::getStrsToPush($dataToSave);
            $socket->send(json_encode(array( 'key' => $name, 'data' => $lastStrsToPush )));
        }
    }


    public static function getStrsToPush($data)
    {
        $outStr = '';
        foreach ($data as $oneData) {
            if ($outStr) {
                $outStr = PHP_EOL . $outStr;
            }
            $outStr = date('Y-m-d H:i', $oneData[0] / 1000) . ';' . $oneData[1] . ';' . $oneData[2] . ';' . $oneData[3] . ';' . $oneData[4] . ';' . $oneData[5] . $outStr;
        }
        return $outStr;
    }
    public static function getDataAfterLastDate($data, $time)
    {
        $time = $time * 1000;
        $tmpData = $data;
        foreach ($tmpData as $index => $oneData) {
            if ($oneData[0] < $time) {
                unset($tmpData[$index]);
            }
        }
        return $tmpData;
    }
    protected function curlGetQuotes($url)
    {
        $header[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; 
        $header[] = "Cache-Control: max-age=0"; 
        $header[] = "Connection: keep-alive"; ; 
        $header[] = "Accept-Language: ru-RU,ru;q=0.5"; 
        $header[] = "Pragma: ";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
        return curl_exec($ch);
    }
    public static function downloadFileFromWww($url, $timeout = 500)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return trim($data);
    }
}
