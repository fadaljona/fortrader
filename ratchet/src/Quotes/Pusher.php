<?php
namespace Quotes;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;

class Pusher implements WampServerInterface {
     /**
     * A lookup of all the topics clients have subscribed to
     */
    protected $subscribedTopics = array();
	protected $symbolsToKeys = array();
	protected $summCount;
	protected $limitCount = 900;

    public function onSubscribe(ConnectionInterface $conn, $topic) {
		$topic->autoDelete = true;
		$key = $topic->getId();
		
		if( $key != 'ALL' && strpos( $key, '_' ) === false ){
			$keyExploded = explode(',', $key);
			sort( $keyExploded );
			$key = implode( ',', $keyExploded );
		
			foreach( $keyExploded as $symbol ){
				if( isset( $this->symbolsToKeys[$symbol] ) && !in_array( $key, $this->symbolsToKeys[$symbol] ) ){	
					$this->symbolsToKeys[$symbol][] = $key;
				}elseif( !isset( $this->symbolsToKeys[$symbol] ) ){
					$this->symbolsToKeys[$symbol][] = $key;
				}
			}
		}
		$this->subscribedTopics[$key] = $topic;
		
		$summ = 0;
		foreach( $this->subscribedTopics as $topic ){
			$summ = $summ + $topic->count();
		}
		$this->summCount = $summ;
		echo '[' . date('Y-m-d H:i:s', time()) . '] subscribers count ' . $summ . PHP_EOL;
		
    }
    public function onUnSubscribe(ConnectionInterface $conn, $topic) {
    }
    public function onOpen(ConnectionInterface $conn) {

		$origin = $conn->__get('wrappedConn')->WebSocket->request->getHeader('origin');
		if( !isset( $origin ) || !$origin ){ $conn->close(); return false; } 
		$origin = $origin->toArray();
		if( !$origin ) { $conn->close(); return false; } 
		if( !isset( $origin[0] ) || !$origin[0] ) { $conn->close(); return false; } 
		$origin = $origin[0];
		$originDomain = substr( $origin, strrpos( $origin, '/')+1 );

		if( $originDomain != 'fortrader.org' && $originDomain != 'devv.fortrader.ru' && $originDomain != 'fortrader.eveem.in' ){
			if( !$conn->__get('wrappedConn')->WebSocket->request->getCookie('ftWssAllow') ){
				$conn->close();
				return false;
			}	
		}

		if( $this->summCount >= $this->limitCount ) $conn->close();
    }
    public function onClose(ConnectionInterface $conn) {

		$origin = $conn->__get('wrappedConn')->WebSocket->request->getHeader('origin');
		if( !isset( $origin ) || !$origin ) return false;
		$origin = $origin->toArray();
		if( !$origin ) return false;
		if( !isset( $origin[0] ) || !$origin[0] ) return false;
		$origin = $origin[0];
		$originDomain = substr( $origin, strrpos( $origin, '/')+1 );

		if( $originDomain != 'fortrader.org' && $originDomain != 'devv.fortrader.ru' ){
			if( !$conn->__get('wrappedConn')->WebSocket->request->getCookie('ftWssAllow') ){
				return false;
			}	
		}


		$summ = 0;
		foreach( $this->subscribedTopics as $key => $topic ){
			
			$this->subscribedTopics[$key] = $topic->remove( $conn );
			
			if( !$this->subscribedTopics[$key]->count() ){
				unset( $this->subscribedTopics[$key] );

				foreach( $this->symbolsToKeys as $symbol => $keysArr ){
					$deleteKey = array_search( $key, $keysArr );
					if( $deleteKey !== false ){
						unset( $this->symbolsToKeys[$symbol][$deleteKey] );
						if( !count( $this->symbolsToKeys[$symbol] ) ) unset( $this->symbolsToKeys[$symbol] );
					}
				}
			}else{
				$summ = $summ + $this->subscribedTopics[$key]->count();
			}
		}
		$this->summCount = $summ;
		echo '[' . date('Y-m-d H:i:s', time()) . '] subscribers count ' . $summ . PHP_EOL;
    }
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
    }
    public function onError(ConnectionInterface $conn, \Exception $e) {
    }
	/**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onQuotes($quotes) {
		
		//avaliable keys - ALL, period keys
        $quotesData = json_decode($quotes, true);
		
		//parse data with key ALL to find other keys
		if( $quotesData['key'] == 'ALL' ){
			$arrayToSend = array();
			
			foreach( $quotesData['data'] as $symbol => $val ){
				
				if( isset( $this->symbolsToKeys[$symbol] ) ){
					
					foreach( $this->symbolsToKeys[$symbol] as $wampKey ){
						
						if( isset( $arrayToSend[$wampKey] ) ) continue;
						
						$wampKeyExp = explode(',', $wampKey);
						
						foreach( $wampKeyExp as $wampKeySymbol ){
							
							if( isset( $quotesData['data'][$wampKeySymbol] ) ){
								
								$arrayToSend[$wampKey][$wampKeySymbol] = $quotesData['data'][$wampKeySymbol];
								
							}
							
						}
						
					}
				
				}
			}
			
			foreach( $arrayToSend as $key => $data ){
				$this->subscribedTopics[$key]->broadcast($data);
			}

		}
		
		//period updates and ALL tick
		if (!array_key_exists($quotesData['key'], $this->subscribedTopics)) {
			return;
		}
		$topic = $this->subscribedTopics[$quotesData['key']];
		$topic->broadcast($quotesData['data']);
        
    }
}
