<?php
namespace Quotes;

use \ZMQ;
use \ZMQContext;

use \Amp\Delayed;
use \Amp\Websocket;

class BitfinexQuotesServer
{
    protected $wsServer = 'wss://api.bitfinex.com/ws/2';
    
    protected $socketConnect;
    protected $timezone = 'Europe/Moscow';

    protected $quotes;
    protected $chanels = array();

    public function __construct($socketConnect, $configFromYii)
    {
        $this->socketConnect = $socketConnect;
        date_default_timezone_set($this->timezone);
        $this->quotes = $configFromYii->quotes;

        if (!$this->quotes) {
            echo '[' . date('Y-m-d H:i:s', time()) . '] no one quotes in config ' . PHP_EOL;
            return false;
        }

        echo '[' . date('Y-m-d H:i:s', time()) . '] constructor done ' . PHP_EOL;
    }
    public function run()
    {
        global $ZMQsocket;
        $context = new ZMQContext();
        $ZMQsocket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
        $ZMQsocket->connect($this->socketConnect);

        echo '[' . date('Y-m-d H:i:s', time()) . '] QuotesServer started ' . PHP_EOL;

        \Amp\Loop::run(function () {
            /** @var \Amp\Websocket\Connection $connection */
            $connection = yield Websocket\connect($this->wsServer);
            global $ZMQsocket;

            foreach ($this->quotes as $symbol => $quoteName) {
                yield $connection->send(json_encode([
                    'event' => 'subscribe',
                    'channel' => 'ticker',
                    'symbol' => $symbol
                ]));
            }
        
            while ($message = yield $connection->receive()) {
                $msg = yield $message->buffer();
                
                if (strpos($msg, 'subscribed')) {
                    $data = json_decode($msg);
                    if (isset($this->quotes[$data->symbol]) && isset($data->chanId)) {
                        $this->chanels[$data->chanId] = $this->quotes[$data->symbol];
                    }
                } elseif (strpos($msg, '[') === 0) {
                    $quotesDataArr = json_decode($msg);
                    
                    if (isset($quotesDataArr[0]) &&
                        isset($quotesDataArr[1]) &&
                        is_array($quotesDataArr[1]) &&
                        isset($this->chanels[$quotesDataArr[0]])
                    ) {
                        $dataToSend = json_encode(array( 'key' => 'ALL', 'data' => array(
                            $this->chanels[$quotesDataArr[0]] => json_encode(array(
                                'bid' => $quotesDataArr[1][0],
                                'ask' => $quotesDataArr[1][2],
                                'time' => date('H:i:s', time()-60*60*1),
                            ))
                        ) ));
                        $ZMQsocket->send($dataToSend);
                    }
                }
        
            
                yield new Delayed(1000);
            }
        });
    }

    protected function sendAlert($msg)
    {
        echo '[' . date('Y-m-d H:i:s', time()) . '] WARNING. ' . $msg . PHP_EOL;
    }
}
