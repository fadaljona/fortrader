<?php
namespace Quotes;

class Helper
{
    public static function isServerActive($strToFind)
    {
        $userInfo = posix_getpwuid(posix_geteuid());
        $userName = 'fortrader';
        if (isset($userInfo) && isset($userInfo['name']) && $userInfo['name']) {
            $userName = $userInfo['name'];
        }
        exec("ps -u {$userName} -o pid,command", $rez);

        $pids = array();
        foreach ($rez as $procInfo) {
            if (strpos($procInfo, $strToFind)) {
                $procInfo = preg_replace('/\s+/', ' ', $procInfo);
                $arr = explode(' ', $procInfo);
                $pids[] = $arr[0];
            }
        }
        if (count($pids) == 1) {
            return false;
        }
        return true;
    }
    public static function getPidByScriptName($strToFind)
    {
        $userInfo = posix_getpwuid(posix_geteuid());
        $userName = 'fortrader';
        if (isset($userInfo) && isset($userInfo['name']) && $userInfo['name']) {
            $userName = $userInfo['name'];
        }
        exec("ps -u {$userName} -o pid,command", $rez);
    
        foreach ($rez as $procInfo) {
            if (strpos($procInfo, $strToFind)) {
                $procInfo = preg_replace('/\s+/', ' ', trim($procInfo));
                $arr = explode(' ', $procInfo);
                return $arr[0];
            }
        }
        return false;
    }
}
