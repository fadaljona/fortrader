<?php
namespace Quotes;

use \ZMQ;
use \ZMQContext;

class BitzQuotesServer
{
    protected $delay;
    protected $quotesSource = 'https://api.bit-z.com/api_v1/tickerall';
    
    protected $socketConnect;
    protected $timezone = 'Europe/Moscow';

    protected $quotes;

    public function __construct($delay, $socketConnect, $configFromYii)
    {
        $this->delay = $delay;
        $this->socketConnect = $socketConnect;
        date_default_timezone_set($this->timezone);
        $this->quotes = $configFromYii->quotes;
        echo '[' . date('Y-m-d H:i:s', time()) . '] constructor done ' . PHP_EOL;
    }
    public function run()
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
        $socket->connect($this->socketConnect);

        $oldQuotes = $this->parseQuotes();

        echo '[' . date('Y-m-d H:i:s', time()) . '] QuotesServer started ' . PHP_EOL;

        while (true) {
            $quotesArr = $this->parseQuotes();

            $changedQuotes = array();
            foreach ($quotesArr as $quote => $val) {
                if (isset($oldQuotes[$quote]) && $oldQuotes[$quote]['compare'] != $val['compare']) {
                    $changedQuotes[$quote] = $val['toSend'];
                }
            }
            if (count($changedQuotes)) {
                $socket->send(json_encode(array( 'key' => 'ALL', 'data' => $changedQuotes )));
            }
            $oldQuotes = $quotesArr;
            unset($changedQuotes);
                
            sleep($this->delay);
        }
    }
    protected function parseQuotes()
    {
        $json = $this->curlGetQuotes($this->quotesSource);
        if (!$json) {
            $this->sendAlert("No data from bit-z");
            return false;
        }
        $json = json_decode($json);
        if ($json->code || !isset($json->data)) {
            $this->sendAlert("No json data from bit-z");
            return false;
        }
        $jsonData = $json->data;
        $quotesArr = array();

        foreach ($this->quotes as $id => $quote) {
            $key = $quote->bitzSymbol;
            if (isset($jsonData->{$key})) {
                if ($quote->toUsd) {
                    $usdKey = $quote->to . '_usdt';
                    if (!isset($jsonData->{$usdKey})) {
                        continue;
                    }
                    $bid = \CommonLib::roundQuotesData($jsonData->{$key}->buy * $jsonData->{$usdKey}->buy);
                    $ask = \CommonLib::roundQuotesData($jsonData->{$key}->sell * $jsonData->{$usdKey}->sell);
                    $quotesArr[$quote->name] = array(
                        'toSend' => json_encode(array(
                            'bid' => $bid,
                            'ask' => $ask,
                            'time' => date('H:i:s', $jsonData->{$key}->date - 60*60*1),
                        )),
                        'compare' => $bid . '__' . $ask,
                    );
                } else {
                    $quotesArr[$quote->name] = array(
                        'toSend' => json_encode(array(
                            'bid' => $jsonData->{$key}->buy,
                            'ask' => $jsonData->{$key}->sell,
                            'time' => date('H:i:s', $jsonData->{$key}->date),
                        )),
                        'compare' => $jsonData->{$key}->buy . '__' . $jsonData->{$key}->sell
                    );
                }
            }
        }

        return $quotesArr;
    }

    protected function curlGetQuotes($url)
    {
        $header[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; 
        $header[] = "Cache-Control: max-age=0"; 
        $header[] = "Connection: keep-alive"; ; 
        $header[] = "Accept-Language: ru-RU,ru;q=0.5"; 
        $header[] = "Pragma: ";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
        return curl_exec($ch);
    }
    protected function sendAlert($msg)
    {
        echo '[' . date('Y-m-d H:i:s', time()) . '] WARNING. ' . $msg . PHP_EOL;
    }
}
