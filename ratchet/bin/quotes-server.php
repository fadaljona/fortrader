<?php

$baseDir = dirname(__FILE__);

$strToFind = 'quotes-server.php';

ini_set('error_log',$baseDir.'/log/quotes-server-error.log');
fclose(STDIN);
fclose(STDOUT);
fclose(STDERR);
$STDIN = fopen('/dev/null', 'r');
$STDOUT = fopen($baseDir.'/log/quotes-server-application.log', 'ab');
$STDERR = fopen($baseDir.'/log/quotes-server-application.log', 'ab');

use Quotes\QuotesServer;
use Quotes\Helper;

require dirname(__DIR__) . '/vendor/autoload.php';

if( !Helper::isServerActive($strToFind) ){
	
	$delay = 100000; // micro_seconds
	$socketConnect = "tcp://localhost:8899";
	
	$configFromYii = unserialize( file_get_contents($baseDir.'/QuotesServerConfig.txt') );
	
	echo '[' . date('Y-m-d H:i:s', time()) . '] config from yii readed ' . PHP_EOL;
	
    $server = new QuotesServer($delay, $socketConnect, $configFromYii);
	
	$server->run();

}else{	
	echo '[' . date('Y-m-d H:i:s', time()) . '] server is already running ' . PHP_EOL;
	exit;
}