<?php

$baseDir = dirname(__FILE__);

$strToFind = 'quotes-bitz-server.php';

ini_set('error_log', $baseDir.'/log/quotes-bitz-server-error.log');
fclose(STDIN);
fclose(STDOUT);
fclose(STDERR);
$STDIN = fopen('/dev/null', 'r');
$STDOUT = fopen($baseDir.'/log/quotes-bitz-server-application.log', 'ab');
$STDERR = fopen($baseDir.'/log/quotes-bitz-server-application.log', 'ab');

use Quotes\BitzQuotesServer;
use Quotes\Helper;

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(dirname(__DIR__)) . '/services/protected/libs/CommonLib.php';

if (!Helper::isServerActive($strToFind)) {
    
    $delay = 10; // seconds
    $socketConnect = "tcp://localhost:8899";
    
    $configFromYii = unserialize(file_get_contents($baseDir.'/BitzQuotesServerConfig.txt'));
    
    echo '[' . date('Y-m-d H:i:s', time()) . '] config from yii readed ' . PHP_EOL;
    
    $server = new BitzQuotesServer($delay, $socketConnect, $configFromYii);
    
    $server->run();
} else {
    echo '[' . date('Y-m-d H:i:s', time()) . '] server is already running ' . PHP_EOL;
    exit;
}
