<?php

$baseDir = dirname(__FILE__);

$strToFind = 'push-server.php';

ini_set('error_log',$baseDir.'/log/push-server-error.log');
fclose(STDIN);
fclose(STDOUT);
fclose(STDERR);
$STDIN = fopen('/dev/null', 'r');
$STDOUT = fopen($baseDir.'/log/push-server-application.log', 'ab');
$STDERR = fopen($baseDir.'/log/push-server-application.log', 'ab');

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\Http\OriginCheck;
use React\EventLoop\Factory;
use Quotes\QuotesWsServer;
use React\ZMQ\Context;
use React\Socket\Server;
use Ratchet\Wamp\WampServer;
use Ratchet\WebSocket\WsServer;

use Quotes\Pusher;
use Quotes\Helper;

require dirname(__DIR__) . '/vendor/autoload.php';

if( !Helper::isServerActive($strToFind) ){
	
    $loop   = Factory::create();
    $pusher = new Pusher;

    // Listen for the web server to make a ZeroMQ push after an ajax request
    $context = new Context($loop);
    $pull = $context->getSocket(ZMQ::SOCKET_PULL);
    $pull->bind('tcp://127.0.0.1:8899'); // Binding to 127.0.0.1 means the only client that can connect is itself
    $pull->on('message', array($pusher, 'onQuotes'));

    // Set up our WebSocket server for clients wanting real-time updates
	$allowedOrigins = array('dev.fortrader.ru', 'devv.fortrader.ru', 'fortrader.org');
    $webSock = new Server($loop);
    $webSock->listen(8889, '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
    $webServer = new IoServer(
        new HttpServer(
			//new OriginCheck(
				new WsServer(
					new WampServer(
						$pusher
					)
				)/*,
				$allowedOrigins
			)*/
        ),
        $webSock
    );
	
	echo '[' . date('Y-m-d H:i:s', time()) . '] push server configured - run ' . PHP_EOL;

    $loop->run();
	
}else{	
	echo '[' . date('Y-m-d H:i:s', time()) . '] server is already running ' . PHP_EOL;
	exit;
}