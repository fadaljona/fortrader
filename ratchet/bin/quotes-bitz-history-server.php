<?php

$baseDir = dirname(__FILE__);

$strToFind = 'quotes-bitz-history-server.php';

ini_set('error_log', $baseDir.'/log/quotes-bitz-history-server-error.log');
fclose(STDIN);
fclose(STDOUT);
fclose(STDERR);
$STDIN = fopen('/dev/null', 'r');
$STDOUT = fopen($baseDir.'/log/quotes-bitz-history-server-application.log', 'ab');
$STDERR = fopen($baseDir.'/log/quotes-bitz-history-server-application.log', 'ab');

use Quotes\BitzQuotesHistoryServer;
use Quotes\Helper;

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(dirname(__DIR__)) . '/services/protected/libs/CommonLib.php';

if (!Helper::isServerActive($strToFind)) {
    $socketConnect = "tcp://localhost:8899";
    
    $configFromYii = unserialize(file_get_contents($baseDir.'/BitzQuotesHistoryServerConfig.txt'));
    
    echo '[' . date('Y-m-d H:i:s', time()) . '] config from yii readed ' . PHP_EOL;

    $server = new BitzQuotesHistoryServer($socketConnect, $configFromYii);
    
    $server->run();
} else {
    echo '[' . date('Y-m-d H:i:s', time()) . '] server is already running ' . PHP_EOL;
    exit;
}
