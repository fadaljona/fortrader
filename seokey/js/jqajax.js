jQuery(document).ready(function($) {


	$('a.catts').click(function(e) {
	
		$('a.catts').css({
			color:"#333",
			"background-color":"#fff",
			"border-color":"#ccc"
		});
		$(this).css({
			color:"#333",
			"background-color":"#ebebeb",
			"border-color":"#adadad"
		});
	
	});

	$('a#send').click(function(e) {
		e.preventDefault();
		
		var element = $(this);
		var obj_data = {};
		$(this.attributes).each(function () {
			if ( this.name.indexOf("data") ==0 ){
				obj_data[ this.name ] = this.value;
			}
		});
		
		console.log(obj_data);
		
		$.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/seokey/inc/ajax.php',
			async:false,
            data: obj_data,
            success: function(data){
				switch(data.action) {
					case 'install_sql':
						$('#install-marker').text(data.message);
						$('#install-marker').css({display:'inline-block'});
						element.css({display:'none'});
						break;
					case 'show_cat':
						$('#count').text(data.count);
						if(data.count == 0){
							$('.panel-default').hide();
						}else{
							$('#post-content').text(data.post_content);
							$('.panel-title').text(data.post_title);
							$('.panel-default').show();
							$('#ready').attr({'data-post-id':data.post_id});
							$('#ready').attr({'data-row-cat-id':data.cat_id});
							$('.fill-later').attr({'data-post-id':data.post_id});
							$('.fill-later').attr({'data-row-cat-id':data.cat_id});
						}
						break;
					case 'fill_later':
						//alert(data);
						$('#form-message').text(data.message);
						$( ".catts[data-row-cat-id='"+obj_data[ 'data-row-cat-id' ]+"']" ).trigger( 'click' );
						$('#form-message').text('Ключ');
						break;
					case 'show_post_by_key':
						//console.log(data);
						//$('#form-message').text(data.post_title);
						$('#replace-key-article').html('<hr>'+data.post_title+'<br />'+data.post_content);
						$('#editkeytext').show();
						$('#editkeytext').val('');
						$('#replace-key-article').show();
						$('#change-key').attr({'data-post-id':data.post_id});

						break;
					case 'show_keys_by_cat':

						//$('#table-container').html(data.table);
						
						

						break;
				} 
            }
        });
	});

	
	$('#ready').click(function(e) {
		e.preventDefault();
		var element = $(this);
		var obj_data = {};
		
		obj_data[ 'data-post-id' ] = $(this).attr('data-post-id');
		obj_data[ 'data-row-cat-id' ] = $(this).attr('data-row-cat-id');
		if( $('#keytext').val() == '' ){
			$('#keytext').css({border:"2px solid red"});
		}else{
			$('#keytext').css({border:"1px solid #ccc"});
			obj_data[ 'keytext' ] = $('#keytext').val();
			obj_data[ 'data-action' ] = 'submit-key';
			
			
			//console.log(obj_data);

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/seokey/inc/ajax.php',
				async:false,
				data: obj_data,
				success: function(data){
					switch(data.action) {
						case 'submit_key':
							if( data.error == 1 ){
								$('#form-message').text(data.message);
							}else if(data.error == 2){
								//$('#form-message').text(data.message);
								$('.alert-danger').show();
								$('.show-text').attr({'data-keytext':data.keyfrase});
								
							}else{
								$('#form-message').text(data.message);
								$( ".catts[data-row-cat-id='"+obj_data[ 'data-row-cat-id' ]+"']" ).trigger( 'click' );
								$('#form-message').text('Ключ');
								$('.alert-danger').hide();
								$('#keytext').val('');
								$('.alert-danger').hide();
								$('#editkeytextmessage').text('Такой ключ занят!');
								$('#replace-key-article').html('');
								$('#editkeytext').val('');
							}
							break;
						case 'show_cat':
							
							break;
					} 
				}
			});
		}
	});
	
	
		$('#change-key').click(function(e) {
		e.preventDefault();
		var element = $(this);
		var obj_data = {};
		$('#editkeytext').show();
		obj_data[ 'data-post-id' ] = $(this).attr('data-post-id');
		if( $('#editkeytext').val() == '' ){
			$('#editkeytext').css({border:"2px solid red"});
		}else{
			$('#editkeytext').css({border:"1px solid #ccc"});
			obj_data[ 'keytext' ] = $('#editkeytext').val();
			obj_data[ 'data-action' ] = 'submit-key';
			
			
			console.log(obj_data);

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/seokey/inc/ajax.php',
				async:false,
				data: obj_data,
				success: function(data){
					switch(data.action) {
						case 'submit_key':
							if( data.error == 1 ){
								alert(data.message);
							}else if(data.error == 2){
								//$('#form-message').text(data.message);
								alert('введите другой текст');
								
							}else{
								$('#editkeytextmessage').text(data.message);
								$('.alert-danger').hide();
								$('#editkeytextmessage').text('Такой ключ занят!');
								$('#replace-key-article').html('');
								$('#editkeytext').val('');
							}
							break;
					} 
				}
			});
		}
	});
	
	
	
	$('a#sendd').click(function(e) {
		e.preventDefault();
		
		var element = $(this);
		var obj_data = {};
		$(this.attributes).each(function () {
			if ( this.name.indexOf("data") ==0 ){
				obj_data[ this.name ] = this.value;
			}
		});
		
		console.log(obj_data);
		
		$.ajax({
            type: 'POST',
            dataType: 'html',
            url: '/seokey/inc/ajax.php',
			async:false,
            data: obj_data,
            success: function(data){
			

						$('#table-container').html(data);
						$('#export').attr({'href':'export.php?cat='+obj_data[ 'data-row-cat-id' ]});
						
							
						
							$('a#send').click(function(e) {
								e.preventDefault();
								
								var element = $(this);
								var obj_data = {};
								$(this.attributes).each(function () {
									if ( this.name.indexOf("data") ==0 ){
										obj_data[ this.name ] = this.value;
									}
								});
								
								obj_data[ 'data-keytext' ] = $('#editkeytext-'+obj_data[ 'data-post-id' ]).val();
								
								if( obj_data[ 'data-keytext' ] == $('#editkeytext-'+obj_data[ 'data-post-id' ]).attr('data-old') ) return false;
								
								console.log(obj_data);
								
								$.ajax({
									type: 'POST',
									dataType: 'json',
									url: '/seokey/inc/ajax.php',
									async:false,
									data: obj_data,
									success: function(data){
										switch(data.action) {
											case 'edit_from_keypage':
												if( data.error >0 ){
													alert(data.message);
												}else{
													//element.text('сохранено');
													$('td#keyvalue[data-id="'+obj_data[ 'data-post-id' ]+'"]').html(data.keytext);
													$('td#keyvalue[data-id="'+obj_data[ 'data-post-id' ]+'"]').one( "click",function(e) {
														e.preventDefault();
														var key_str = $(this).text();
														var data_id = $(this).attr('data-id');
														$(this).html('<input type="text" id="editkeytext-'+data_id+'" class="form-control" value="'+key_str+'">');

													});
												}
												
												break;
										
										} 
									}
								});
							});
							
							
							
							$('td#keyvalue').one( "click",function(e) {
								e.preventDefault();
								var key_str = $(this).html();
								var data_id = $(this).attr('data-id');
								$(this).html('<input type="text" id="editkeytext-'+data_id+'" class="form-control" data-old="'+key_str+'" value="'+key_str+'">');

							});
							


			} 
        });
	});
	
	
	
	
	
	$('.defaultcat').trigger( 'click' );

});