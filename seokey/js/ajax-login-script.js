jQuery(document).ready(function($) {


    // Perform AJAX login on form submit
   $('#log_in').on('click', function(e){
		e.preventDefault();
		
        $.ajax({
            type: 'POST',
            dataType: 'json',
			async:false,
            url: ajax_login_object.ajaxurl,
            data: { 
                'username': $('form#login #username').val(), 
                'password': $('form#login #password').val(), 
                'security': $('form#login #security').val() },
            success: function(data){
				
                if ( data.loggedin == false ){
					$('form#login #username').css({border:"2px solid red"});
					$('form#login #password').css({border:"2px solid red"});
                }else{
					$('form#login #username').css({border:"0px"});
					$('form#login #password').css({border:"0px"});
					document.location.href = ajax_login_object.redirecturl;
				}
            }
        });
        e.preventDefault();
    });

});