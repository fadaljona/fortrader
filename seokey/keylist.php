<?php 
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');?>
<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>
      Jumbotron Template for Bootstrap
    </title>
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"
    rel="stylesheet">
  
    <link href="css/screen-1af48a2b58.css" rel="stylesheet">
</head>
  
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">SEO KEY MANAGER</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li>
              <a href="./">Home</a>
            </li>
            <li class="active">
              <a href="./keylist.php">Keylist</a>
            </li>
            <li>
            </li>
            <li class="dropdown">
              <ul class="dropdown-menu">
                <li>
                  <a href="#">Action</a>
                </li>
                <li>
                  <a href="#">Another action</a>
                </li>
                <li>
                  <a href="#">Something else here</a>
                </li>
                <li class="divider">
                </li>
                <li class="dropdown-header">
                  Nav header
                </li>
                <li>
                  <a href="#">Separated link</a>
                </li>
                <li>
                  <a href="#">One more separated link</a>
                </li>
              </ul>
            </li>
          </ul>
<?php 
if (is_user_logged_in()) { 	
?>
<form class="navbar-form navbar-right" id="login" action="./" method="post">

<?php
$user = wp_get_current_user(); 
global $wpdb;
$results = $wpdb->query( "SHOW TABLES LIKE 'seokey'" );
if( !$results ){
?>
<div class="btn btn-success" id="install-marker" style="display:none;"></div>
<a id="send" data-action="install-sql" class="btn btn-success" href="./">Install Seo Key Table</a>
<?php }?>
<a class="btn btn-success" href="<?php echo get_edit_user_link(); ?>"><?php echo $user->user_login;?></a>  <a class="btn btn-success" href="<?php echo wp_logout_url( '/seokey/' ); ?>">Logout</a>
	</form>
<?php } else { ?>
			 
		  
          <form class="navbar-form navbar-right" id="login" action="./" method="post">
            <div class="form-group">
              <input type="text" name="username" id="username" placeholder="User login" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" name="password" id="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success" id="log_in">
              Sign in
            </button>
			<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
          </form>
<?php }?>
        </div>
        <!--/.navbar-collapse -->
      </div>
    </div>
    <!-- Main jumbotron for a primary marketing message or call to action
    -->
<?php 
if (is_user_logged_in()) {	
?>
    <div class="jumbotron">
      <div class="container">
      </div>
      <div class="btn-group">
        <a id="sendd" href="#" data-row-cat-id="0" data-action="show-keys" class="defaultcat btn btn-default catts">Все статьи</a>
		<?php $categories = get_categories( $args ); 
			//print_r($categories);
			foreach ( $categories as $category ){
				echo '<a id="sendd" href="#" data-action="show-keys" data-row-cat-id="'.$category->cat_ID.'" class="btn btn-default catts">'.$category->cat_name.'</a>';
			}
		?>
      </div>
    </div>
<?php
if ( current_user_can('edit_posts') ) {
?>
    <div class="container">

		<div id='table-container'></div>

      <a href="" class="btn btn-default" target="_blank" id="export">Экспорт</a>
      <hr>
      <footer>
        <p>
          © SEO KEY MANAGER 2015
        </p>
      </footer>
    </div>
<?php
}
?>
    <!-- /container -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"
    >
    </script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"
    >
    </script>
<?php
}
?>
<?php if (!is_user_logged_in()) {?>
<script type='text/javascript'>
/* <![CDATA[ */
var ajax_login_object = {"ajaxurl":"\/seokey\/inc\/login.php","redirecturl":".\/","loadingmessage":"Sending user info, please wait..."};
/* ]]> */
</script>
<script type='text/javascript' src='/seokey/js/ajax-login-script.js'></script>
<?php }else{?>
<script type='text/javascript' src='/seokey/js/jqajax.js'></script>
<?php
}
?>
  </body>

</html>