<?php
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

function install_sql(){
	global $wpdb;
	$result1 = $wpdb->query(
		"CREATE TABLE IF NOT EXISTS `seokey` (
		  `id` bigint(20) unsigned NOT NULL,
		  `keyfrase` varchar(255) NOT NULL DEFAULT '',
		  `done` tinyint(1) NOT NULL DEFAULT '0',
		  `queue` int(11) NOT NULL DEFAULT '0',
		  `cat` int(11) NOT NULL,
		  `cat_name` varchar(200) NOT NULL DEFAULT '',
		  `permalink` varchar(255) NOT NULL DEFAULT '',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
	);
	if( !$result1 ){
		return json_encode(array('message'=>__('seokey table not created'), 'action' => 'install_sql'  ));
	}

	$result2 = $wpdb->query(
		"INSERT INTO seokey (id, cat, cat_name)
		SELECT ID, terms.term_id, terms.name
		FROM $wpdb->posts AS post
		JOIN $wpdb->term_relationships AS rel ON post.ID = rel.object_ID
		JOIN $wpdb->term_taxonomy AS ttax ON rel.term_taxonomy_id = ttax.term_taxonomy_id
		JOIN $wpdb->terms AS terms ON ttax.term_id = terms.term_id
		WHERE ( 1 =1
		AND post_type = 'post'
		AND post_status = 'publish'
		AND taxonomy = 'category' )
		GROUP BY ID
		ORDER BY ID ASC;"
	);
	if( !$result2 ){
		return json_encode(array('message'=>__('some problems with INSERT INTO seokey'), 'action' => 'install_sql' ));
	}
	return json_encode(array('message'=>__('Installed'), 'action' => 'install_sql' ));
}

function show_cat($id){
	global $wpdb;
	$sql="SELECT count(id) FROM seokey WHERE done=0";
	if($id){
		$sql.=" and cat=$id";	
		$sql2=" and cat=$id";
	}
	
	$count = $wpdb->get_var( $sql, 0, 0 );
	$out_array['action'] = 'show_cat';
	$out_array['count'] = $count;
	
	if ( $out_array['count'] ){
		$sql="SELECT id FROM seokey WHERE done=0$sql2 ORDER BY queue ASC limit 0,1;";
		$post_id = $wpdb->get_var( $sql, 0, 0 );
		$post = get_post($post_id);
		$out_array['post_id'] = $post_id;
		$out_array['cat_id'] = $id;
		$out_array['post_title'] = $post->post_title;
		$out_array['post_content'] = mb_substr ( strip_tags( $post->post_content), 0, 500);
	}
	
	return json_encode( $out_array );
}

function submit_key($post_id, $keytext){
	global $wpdb;
	$out_array['action'] = 'submit_key';
	$category=get_the_category( $post_id );
	$cat_id=$category[0]->cat_ID;
	
	
	
	
	
	$sql1="select id from seokey where `keyfrase`=%s";
	$result1 = $wpdb->query(
		$wpdb->prepare(
			$sql1, $keytext
		)
	);
	if( $result1 ){
		$out_array['message'] = 'had double';
		$out_array['error'] = '2';
		$out_array['keyfrase'] = $keytext;
		return json_encode( $out_array );
	}
	
	
	
	
	
	$sql="update seokey set `keyfrase`=%s, done=1, permalink='".get_permalink( $post_id )."' where id=%d";
	$result = $wpdb->query(
		$wpdb->prepare(
			$sql, $keytext, $post_id
		)
	);
	

	if( !$result ){
		$out_array['error'] = '1';
		$out_array['message'] = 'error when updaying key';
		return json_encode( $out_array );
	}
	$sql="SELECT queue FROM seokey where id=$post_id";
	$curent_queue = $wpdb->get_var( $sql, 0, 0 );
	if( $curent_queue ){
	
		$sql2="update seokey set queue=queue-1 where queue>0 and cat=$cat_id";
		$result = $wpdb->query($sql2);

	}
	
	$out_array['error'] = '0';
	$out_array['message'] = 'key saved';
	
	return json_encode( $out_array );
}
function get_max_queue($cat_id){
	global $wpdb;
	$sql="SELECT MAX(queue) FROM seokey where cat=".$cat_id;
	$queue = $wpdb->get_var( $sql, 0, 0 );
	return $queue;
}

function fill_later($post_id){
	global $wpdb;
	$out_array['action'] = 'fill_later';
	$category=get_the_category( $post_id );
	$cat_id=$category[0]->cat_ID;
	$max_queue=get_max_queue($cat_id);
	
	$sql="SELECT id FROM seokey where queue=$max_queue";

	$max_queue_id = $wpdb->get_var( $sql, 0, 0 );
	
	$sql="SELECT queue FROM seokey where id=$post_id";
	$curent_queue = $wpdb->get_var( $sql, 0, 0 );
	
	if( $curent_queue ){
	
		$sql2="update seokey set queue=queue+1 where queue>0 and queue < $max_queue and cat=$cat_id";
		$result = $wpdb->query($sql2);
		if( !$result ){
			$out_array['error'] = '1';
			$out_array['message'] = $sql;
			return json_encode( $out_array );
		}
	
		$sql1="update seokey set queue=1 where id=".$max_queue_id." and cat=$cat_id";
		$result = $wpdb->query($sql1);
		if( !$result ){
			$out_array['error'] = '1';
			$out_array['message'] = $sql;
			//'error when sending post to queue';
			return json_encode( $out_array );
		}
		$out_array['message'] = 'queue updated';
		return json_encode( $out_array );
	}else{
			
		$out_array['queue'] = $max_queue+1;
		$sql="update seokey set queue=".$out_array['queue']." where cat=$cat_id and id=$post_id";
		$result = $wpdb->query( $sql );
		
		if( !$result ){
			$out_array['error'] = '1';
			$out_array['message'] = 'error when sending post to queue';
			return json_encode( $out_array );
		}
		$out_array['error'] = '0';
		$out_array['message'] = 'queue updated';
		
		return json_encode( $out_array );
	}
}

function show_post_by_key($keytext){
	global $wpdb;
	$out_array['action'] = 'show_post_by_key';
	
	$sql="SELECT id FROM seokey where `keyfrase`='$keytext'";
	
	$post_id_by_key = $wpdb->get_var( $sql, 0, 0 );
	
	if( !$post_id_by_key ){
		$out_array['error'] = '1';
		$out_array['message'] = 'error when selecting post id';
		$out_array['sql'] = $sql;
		return json_encode( $out_array );
	}
	
	$post = get_post($post_id_by_key);
	$out_array['post_id'] = $post_id_by_key;
	$out_array['post_title'] = $post->post_title;
	$out_array['post_content'] = mb_substr ( strip_tags( $post->post_content), 0, 500);
	
	
	
	return json_encode( $out_array );
}

function show_keys_by_cat($cat_id){
	global $wpdb;

	
	if($cat_id){
		$sql="SELECT id, keyfrase, cat_name FROM seokey where cat=$cat_id and done=1";
	}else{
		$sql="SELECT id, keyfrase, cat_name FROM seokey where done=1";
	}
	$table = $wpdb->get_results($sql);
	$out_table='<table class="table table-striped"><tbody>';

	foreach($table as $table_row){
		$out_table.='<tr><td id="keyvalue" data-id="'.$table_row->id.'">'.$table_row->keyfrase.'</td><td><a href="/?p='.$table_row->id.'" target="_blank">Перейти</a></td><td>'.$table_row->cat_name.'</td><td id="editinterface"><a href="#" id="send" data-action="edit-from-keypage" data-post-id="'.$table_row->id.'" class="btn btn-xs btn-primary">Редактировать</a></td></tr>';
	}
	$out_table.='</tbody><thead><tr><th><div class="well"><h3>Ключей: '.count($table).'<span id="count"></span>&nbsp;</h3></div>Ключ</th><th>Линк</th><th>Категория</th><th>Действие</th></tr></thead></table>';
	
	$out_array['table'] = $out_table;
	
	return $out_table;
}

function edit_from_keypage($post_id, $keytext){
	global $wpdb;
	$out_array['action'] = 'edit_from_keypage';
	$out_array['keytext'] = $keytext;
	if(!$keytext){
		$out_array['error'] = 1;
		$out_array['message'] = "заполните поле ввода - поля с ключом редактируемые";
		return json_encode( $out_array );
	}
	
	$category=get_the_category( $post_id );
	$cat_id=$category[0]->cat_ID;
	
	
	$sql1="select id from seokey where `keyfrase`=%s";
	$result1 = $wpdb->query(
		$wpdb->prepare(
			$sql1, $keytext
		)
	);
	if( $result1 ){
		$out_array['message'] = 'had double';
		$out_array['error'] = '2';
		return json_encode( $out_array );
	}
	
	
	$sql="update seokey set `keyfrase`=%s, done=1 where id=%d";
	$result = $wpdb->query(
		$wpdb->prepare(
			$sql, $keytext, $post_id
		)
	);
	

	if( !$result ){
		$out_array['error'] = '2';
		$out_array['message'] = 'error when updaying key';
		return json_encode( $out_array );
	}

	
	$out_array['error'] = '0';
	$out_array['message'] = 'key saved';
	
	return json_encode( $out_array );
	

}


































?>