<?php 
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
require($_SERVER['DOCUMENT_ROOT'].'/seokey/inc/func.php');
if (is_user_logged_in()) {
global $wpdb;

	switch ($_POST['data-action']) {
		case 'install-sql':
			echo install_sql();
			break;
		case 'show-cat':
			echo show_cat($_POST['data-row-cat-id']);
			break;
		case 'submit-key':
			echo submit_key($_POST['data-post-id'], $_POST['keytext']);
			break;
		case 'fill-later':
			echo fill_later($_POST['data-post-id']);
			break;
		case 'show-post':
			echo show_post_by_key($_POST['data-keytext']);
			break;
		case 'show-keys':
			echo show_keys_by_cat($_POST['data-row-cat-id']);
			break;
		case 'edit-from-keypage':
			echo edit_from_keypage($_POST['data-post-id'], $_POST['data-keytext']);
			break;
	}
		
		

		//echo json_encode(array('loggedin'=>true, 'message'=> test($_POST['data-action']) ));
}else echo json_encode(array('message'=>__('please login...')));
?>