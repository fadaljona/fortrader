<?php

include dirname(__FILE__) . '/wp-load.php';
include ABSPATH /*. '/'*/ . PLUGINDIR . '/exlines/exlines.php';

$ExLines = new ExLines();
$line = $ExLines->getLine(urldecode($_GET['line']));

if ($line) {
    if (!isset($_GET['nostat']))
        $ExLines->updateLineAccess($line->uid);

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-type: image/png');
	// ABSPATH . PLUGINDIR . "/exlines/lines/cache/" . $line->uid
    exit(file_get_contents(
          "/var/www/fortrader/domains/files.fortrader.org/exlines_cache/" . $line->uid
    ));
}


if (
        isset($_POST['text'], $_POST['template'], $_POST['date']) &&
        !empty($_POST['text']) && !empty($_POST['template']) &&
        !empty($_POST['date'])
) {
    include ABSPATH . '/' . PLUGINDIR . '/exlines/creator.php';
    $creator = new ExLineCreator();

    $date = date_parse($_POST['date']);
    $time = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
    $date = new DateTime('@' . $time);
    $result = $creator->createImg($_POST['template'], array(date_format(
                        $date, 'U'
                ), time(), 0), $_POST['text']);

    if ($result) {
        $ExLines->insertLine($result);
        echo json_encode(array('url' => $result['name']));
    } else {
        echo json_encode(array('error' => 1));
    }
    exit();
}

header("HTTP/1.0 404 Not Found");
?>
