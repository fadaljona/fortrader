<?
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
		
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;

	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	define( 'YII_LOG_ERRORS', @$log_errors );
	
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	require_once "{$dir}/protected/components/sys/WebApplication.php";
	$config = "{$dir}/protected/config/default.php";
	$app = new WebApplication( $config );
	
	$sql = "UPDATE  `{{calendar_event2_i18n}}` set `hasNews` = 0;";
	$result = Yii::App()->db->createCommand( $sql )->query();
	
	echo "{$result->rowCount} setted hasNews to 0\n";

	$sql = "UPDATE  `{{calendar_event2_i18n}}` `ruI18N`
		LEFT JOIN `wp_postmeta` AS `index` ON `index`.`meta_value` = `ruI18N`.`indicator_name`
		LEFT JOIN `wp_postmeta` AS `country` ON `country`.`meta_value` = `ruI18N`.`country` AND `index`.`post_id` = `country`.`post_id`
		LEFT JOIN `wp_posts` AS `posts` ON `posts`.`ID` = `index`.`post_id`
		SET `ruI18N`.`hasNews` = 1
		WHERE `ruI18N`.`idLanguage` = 1 AND `ruI18N`.`indicator_name` IS NOT NULL AND `ruI18N`.`country` IS NOT NULL AND `index`.`meta_key` = 'index' and `country`.`meta_key` = 'country' AND `posts`.`post_status` = 'publish'";
	$result = Yii::App()->db->createCommand( $sql )->query();
	
	echo "{$result->rowCount} setted hasNews to 1\n";
?>