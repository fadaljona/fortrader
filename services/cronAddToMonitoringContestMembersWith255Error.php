<?php

	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
		
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;

	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	define( 'YII_LOG_ERRORS', @$log_errors );
	
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$_SERVER['SCRIPT_FILENAME'] = 'index.php';
	$_SERVER['SCRIPT_NAME'] =  '/index.php';
	$_SERVER['REQUEST_URI'] = 'index.php';
    $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../';
    
	$config = "{$dir}/protected/config/default.php";
	
	require_once(dirname(__FILE__).'/protected/components/sys/WebApplication.php');
	$app = new WebApplication( $config );
	
	$members = ContestMemberModel::model()->findAll(array(
		'with' => array( 'server', 'mt5AccountInfo', 'contest' ),
		'condition' => "
			`contest`.`stopImport` = 0 
			AND `contest`.`stopDeleteMMonitoring` = 0 
			AND `contest`.`completedMembers` = 0 
			AND `t`.`stopMonitor` = 0 
			AND `mt5AccountInfo`.`last_error_code` = -255 AND `mt5AccountInfo`.`sameErrorCount` > 4
		",
	));
	
	echo count($members);
	
	foreach( $members as $member ){
		$member->mt5AccountInfo->sameErrorCount = 0;
		$member->mt5AccountInfo->save();
		
		$url = "http://159.69.219.218:18466/master.api?cmd=add_acc&aid={$member->id}&login={$member->accountNumber}&pass={$member->investorPassword}&srvmt4=" . rawurlencode($member->server->url) . "&__pr=2";
		$addResult = CommonLib::downloadFileFromWww( $url );
			
		if( !$addResult ) $this->throwI18NException( "Empty add result, please contact admin"  );
				
		preg_match_all('/aid\:\s*'.$member->id.'\s*login\:\s*'.$member->accountNumber.'\s*server\:\s*'.$member->server->url.'\s*Ok/',
			$addResult,
			$found,
			PREG_PATTERN_ORDER
		);
			
		if( !isset($found[0][0]) || !$found[0][0] ) $this->throwI18NException( "Wrong add response, please contact admin" );
	}