<?php

error_reporting( E_ALL );
ini_set( 'display_errors', 1 );
		
$dir = dirname(__FILE__);
chdir( $dir );
	
$boot = "{$dir}/boot-dist.php";
if( is_file( $boot )) require_once $boot;
	
$boot = "{$dir}/boot-local.php";
if( is_file( $boot )) require_once $boot;
	
if( !empty( $timezone )) date_default_timezone_set( $timezone );
if( !empty( $debug )) define( 'YII_DEBUG', $debug );
if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	
require_once $pathYii;
	
$dir = dirname(__FILE__);
chdir( $dir );
	
$config = "{$dir}/protected/config/default.php";
$app = Yii::createWebApplication( $config );



$quotes = QuotesSymbolsModel::model()->findAll(Array(
	'select' => " id, name, tickName ",
	'condition' => " `t`.`sourceType` = 'own' AND `t`.`tickName` <> '' ",
));
$quotesArrq = array();
foreach( $quotes as $quote ){
	$quotesArrq[$quote->tickName] = $quote->name;
}

$quotesSettings = QuotesSettingsModel::getInstance();

$sources = $quotesSettings->quotesTickSource;
$resultFile = Yii::App()->params['documentRoot'] . '/quotes_list/quotes.txt';

$arraysTomerge = array();
foreach( $sources as $source ){
	$arraysTomerge[] = parseOneFile( $source, $quotesArrq );
}

$summArr = call_user_func_array('array_merge', $arraysTomerge);


if( !count( $summArr ) ) return false;

$outStr = implode("\r\n", $summArr);

file_put_contents( $resultFile, $outStr );

function parseOneFile( $url, $quotesArrq ){
	$quotesStr = trim( curlGetQuotes($url) );
	$quotes = explode("\r\n", $quotesStr);

	$quotes3 = array();
	foreach( $quotes as $quote ){
		$delimerPos = strpos( $quote, ';' );
		$symbol = substr( $quote, 0, $delimerPos );
		if( !isset($quotesArrq[$symbol]) ) continue;
		$quotes3[$symbol] = $quote;
	}
	return $quotes3;
}

function curlGetQuotes( $url ){
	$header[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; 
	$header[] = "Cache-Control: max-age=0"; 
	$header[] = "Connection: keep-alive"; ; 
	$header[] = "Accept-Language: ru-RU,ru;q=0.5"; 
	$header[] = "Pragma: ";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
	return curl_exec($ch);
}
?>
