<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require dirname(__DIR__) . '/ratchet/vendor/autoload.php';


use Quotes\Helper;


$dir = dirname(__FILE__);
chdir($dir);
    
$boot = "{$dir}/boot-dist.php";
if (is_file($boot)) {
    require_once $boot;
}
    
$boot = "{$dir}/boot-local.php";
if (is_file($boot)) {
    require_once $boot;
}
    
if (!empty($timezone)) {
    date_default_timezone_set($timezone);
}
if (!empty($debug)) {
    define('YII_DEBUG', $debug);
}
if (!empty($mb_encoding)) {
    mb_internal_encoding($mb_encoding);
}
if (!empty($profiling_action)) {
    define('PROFILING_ACTION', true);
}
    
require_once $pathYii;
    
$dir = dirname(__FILE__);
    
$config = "{$dir}/protected/config/default.php";
$app = Yii::createWebApplication($config);

$quotesSettings = QuotesSettingsModel::getInstance();

$autoStart = $quotesSettings->autoStartDataTransfer;
$path = $dir .'/..'. $quotesSettings->phpServersPath;

$phpPath = Yii::App()->params['consolePhpPath'];

if ($autoStart) {
    $pid = Helper::getPidByScriptName('quotes-server.php');
    if (!$pid) {
        exec("nohup {$phpPath} {$path}/quotes-server.php &");
    }
    
    $pid = Helper::getPidByScriptName('quotes-history-server.php');
    if (!$pid) {
        exec("nohup {$phpPath} {$path}/quotes-history-server.php &");
    }
}

if ($quotesSettings->autoStartWsPusher) {
    $pid = Helper::getPidByScriptName('push-server.php');
    if (!$pid) {
        exec("nohup {$phpPath} {$path}/push-server.php &");
    }
}

if ($quotesSettings->autoStartBitfinex) {
    $pid = Helper::getPidByScriptName('quotes-bitfinex-server.php');
    if (!$pid) {
        exec("nohup {$phpPath} {$path}/quotes-bitfinex-server.php &");
    }

    $pid = Helper::getPidByScriptName('quotes-bitfinex-history-server.php');
    if (!$pid) {
        exec("nohup {$phpPath} {$path}/quotes-bitfinex-history-server.php &");
    }
}


$zipLog = $path . "/log/logs.zip";
clearstatcache();

if ((file_exists($zipLog) && (time() - filemtime($zipLog)) > 60*60*24*2) || !file_exists($zipLog)) {
    if (file_exists($zipLog)) {
        $add_path = date('Y-m-d_H-i-s', filemtime($zipLog)) . '___' . date('Y-m-d_H-i-s') . '/';
    } else {
        $add_path = date('Y-m-d_H-i-s') . '/';
    }

    $zip = new ZipArchive();

    if ($zip->open($zipLog, ZipArchive::CREATE) !== true) {
        exit("unable open <$zipLog>\n");
    }
    
    $options = array('add_path' => $add_path, 'remove_all_path' => true);
    $zip->addGlob($path . '/log/*.log', GLOB_BRACE, $options);

    $zip->close();
    
    foreach (glob($path . '/log/*.log') as $filename) {
        file_put_contents($filename, '');
    }
}
