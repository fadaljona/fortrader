<?
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
	set_time_limit(0);
	ini_set('memory_limit', '1024M');
		
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local1.php";
	if( is_file( $boot )) require_once $boot;
	
	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$config = "{$dir}/protected/config/default.php";
	$app = Yii::createWebApplication( $config );
	
	Yii::import( 'components.ShortAccountDataComponent' );
	$component = new ShortAccountDataComponent();
	$component->run();
	
?>
