<?php

	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;
	
	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	require_once "{$dir}/protected/components/sys/WebApplication.php";
	$config = "{$dir}/protected/config/default.php";
	$app = new WebApplication( $config );

	$sitemapModule = Yii::app()->getModule('sitemap');
	$types = $sitemapModule->oneSiteMapTypeToController;
	$types['other'] = '';
	
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
	
	$sitemaps = '';
	foreach( $types as $type => $route ){
		echo $type . "\n";
		$sitemaps .= '<sitemap>' . "\n";
		$sitemaps .= '<loc>'. CommonLib::httpsProtocol( Yii::app()->request->hostInfo ) . '/services/sitemap-' . $type . '.xml</loc>' . "\n";
		$sitemaps .= '<lastmod>'.date('Y-m-d\TH:i:s+00:00', time()).'</lastmod>' . "\n";
		$sitemaps .= '</sitemap>' . "\n";
		
		$urlsCount = $sitemapModule->getAllUrls( $type, 0, false );
		$pagesCount = ($urlsCount - ($urlsCount % $sitemapModule->urlsPerMap)) / $sitemapModule->urlsPerMap + 1;
		
		for( $i=2; $i<=$pagesCount; $i++ ){
			$sitemaps .= '<sitemap>' . "\n";
			$sitemaps .= '<loc>'. CommonLib::httpsProtocol( Yii::app()->request->hostInfo ) . '/services/sitemap-' . $type . $i . '.xml</loc>' . "\n";
			$sitemaps .= '<lastmod>'.date('Y-m-d\TH:i:s+00:00', time()).'</lastmod>' . "\n";
			$sitemaps .= '</sitemap>' . "\n";
		}
	}

	Yii::app()->cacheSiteMap->set('servicesSitemaps', $sitemaps);
