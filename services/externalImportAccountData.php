<?
	/*
		Скрипт - точка входа в проект.
		Импорт данных из внешнего мониторинга.
	*/
	
		// включаем отображение ошибок
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
		
	$dir = dirname(__FILE__);
	chdir( $dir );
	
		// поключаем файл с настройками
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;
	
		// определяем некоторые константы
	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	define( 'YII_LOG_ERRORS', @$log_errors );
	
		// подключаем фреймворк
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
		// создаём приложение, но не запускаем его.
	$config = "{$dir}/protected/config/default.php";
	$app = Yii::createWebApplication( $config );
	
		// подгружаем компонент импорта
	Yii::import( 'components.AccountDataImporterComponent' );
		
		// запускаем компонент.
	$importer = new AccountDataImporterComponent;
	$importer->import();
		
?>
