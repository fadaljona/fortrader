<?php

class PageCacheFilter extends COutputCache
{
	const CACHE_KEY_PREFIX='Yii.PageCache.';
	private $_key;

	protected function getBaseCacheKey()
	{
		return self::CACHE_KEY_PREFIX;
	}
	protected function getCacheKey()
	{
		if($this->_key!==null)
			return $this->_key;
		else
		{
			$key=$this->getBaseCacheKey().'.';
			
			$controller=$this->getController();
			$key.=$controller->getUniqueId().'/';
			if(($action=$controller->getAction())!==null)
				$key.=$action->getId();
		
			$key.='.';

			if($this->varyByParam)
			{
				$param = Yii::app()->request->getQuery( $this->varyByParam );
				if( $param ){
					$key.=$param;
				}else{
					$key.='NO.Param';
				}
			}
			$key.='.';
			return $this->_key=$key;
		}
	}
}
