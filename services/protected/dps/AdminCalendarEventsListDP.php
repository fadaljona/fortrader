<?

	class AdminCalendarEventsListDP extends CActiveDataProvider{
		public $idCountry;
		public $filterModel;
		public $group;
		protected function calculateTotalItemCount(){
			$command = Yii::App()->db->createCommand();
			
			if( $this->group ) {
				$command->select( " COUNT( DISTINCT `t`.`indicator_id`, `cLI18NCalendarEvent`.`idCountry` ) " );
			}
			else{
				$command->select( " COUNT( `t`.`id` ) " );
			}
			$command->from( "{{calendar_event}} t" );
			
			$command->join( '{{calendar_event_i18n}} cLI18NCalendarEvent', " `cLI18NCalendarEvent`.`idEvent` = `t`.`id` AND `cLI18NCalendarEvent`.`idLanguage` = :idLanguage ", Array(
				':idLanguage' => LanguageModel::getCurrentLanguageID(),
			));
			
			if( $this->idCountry ) {
				$command->andWhere( " `cLI18NCalendarEvent`.`idCountry` = :idCountry ", Array(
					':idCountry' => $this->idCountry,
				));
			}
			
			if( $this->filterModel ) {
				if( strlen( $this->filterModel->dateForSearch )) {
					$command->andWhere( " DATE( `t`.`dt` ) = :date ", Array(
						':date' => $this->filterModel->dateForSearch,
					));
				}
				if( strlen( $this->filterModel->indicatorIDForSearch )) {
					$indicatorID = (int)$this->filterModel->indicatorIDForSearch;
					$command->andWhere( " `t`.`indicator_id` = :indicatorID ", Array(
						':indicatorID' => $indicatorID,
					));
				}
				if( strlen( $this->filterModel->indicatorForSearch )) {
					$indicator = CommonLib::escapeLike( $this->filterModel->indicatorForSearch );
					$indicator = CommonLib::filterSearch( $indicator );
					$command->andWhere( " `cLI18NCalendarEvent`.`indicator` LIKE :indicator	", Array(
						':indicator' => $indicator,
					));
				}
			}
			
			$count = $command->queryScalar();
			return $count;
		}
	}

?>