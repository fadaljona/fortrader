<?php
class m141022_170639_add_values_to_message extends DbMigration
{
  public function up()
  {
      $this->insert('ft_message', array(
          'key' => 'You will be forwarded to the account page after !t! seconds. If you are still on this page, click <a href="!link!">here</a>',
          'ns' => '*',
          'createdDT' => date('Y-m-d H:i:s'),
          'updatedDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 1,
              'value' => 'Вы будете переправлены на страницу счета через !t! сек. Если вы все еще находитесь на этой странице, щелкните на эту <a href="!link!">ссылку</a>'
      ));

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 0,
              'value' => 'You will be forwarded to the account page after !t! seconds. If you are still on this page, click <a href="!link!">here</a>'
      ));
  }

  public function down()
  {
    echo "m141022_170639_add_values_to_message does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
