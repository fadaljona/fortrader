<?php
class m180526_070313_bitzToUsdToconvertToUsd extends DbMigration
{
  public function up()
  {
    $this->renameColumn('ft_quotes_symbols', 'bitzToUsd', 'convertToUsd');
  }

  public function down()
  {
    echo "m180526_070313_bitzToUsdToconvertToUsd does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
