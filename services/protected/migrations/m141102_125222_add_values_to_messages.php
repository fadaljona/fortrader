<?php
class m141102_125222_add_values_to_messages extends DbMigration
{
  public function up()
  {
      $this->insert('ft_message', array(
              'key' => 'You are registered successfully',
              'ns' => '*',
              'createdDT' => date('Y-m-d H:i:s'),
              'updatedDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 1,
              'value' => 'Вы успешно зарегистрированы'
      ));

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 0,
              'value' => 'You are registered successfully'
      ));
  }

  public function down()
  {
    echo "m141102_125222_add_values_to_messages does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
