<?php
class m141028_233231_alter_table_quotes_symbols extends DbMigration
{
  public function up()
  {
	  $this->addColumn("ft_quotes_symbols","panel","set('Yes','No') NOT NULL");
	  $this->createIndex("_panel","ft_quotes_symbols","panel",false);
  }

  public function down()
  {
    echo "m141028_233231_alter_table_quotes_symbols does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
