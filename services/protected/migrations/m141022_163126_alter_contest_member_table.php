<?php
class m141022_163126_alter_contest_member_table extends DbMigration
{
  public function up()
  {
      $this->alterColumn('ft_contest_member', 'revision', 'INT(11) NULL');
  }

  public function down()
  {
    echo "m141022_163126_alter_contest_member_table does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
