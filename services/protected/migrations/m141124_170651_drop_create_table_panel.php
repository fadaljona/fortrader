<?php
class m141124_170651_drop_create_table_panel extends DbMigration
{
  public function up()
  {
	  $this->dropTable("ft_user_profile_ext");
	  $this->createTable("ft_quotes_panel",array(
		  "id" => "pk",
		  "idUser"=> "int(11) NOT NULL",
		  "ids"=>"varchar(255) DEFAULT NULL",
		  "time_update_panel" =>"int(4) DEFAULT NULL"
	  ),'ENGINE=InnoDB');
	  $this->createIndex("_idUser","ft_quotes_panel","idUser",false);
  }

  public function down()
  {
    echo "m141124_170651_drop_create_table_panel does not support migration down.\n";
    return false;
  }

}
