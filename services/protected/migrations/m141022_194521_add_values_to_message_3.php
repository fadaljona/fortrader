<?php
class m141022_194521_add_values_to_message_3 extends DbMigration
{
  public function up()
  {
      $this->insert('ft_message', array(
              'key' => 'You are registered',
              'ns' => '*',
              'createdDT' => date('Y-m-d H:i:s'),
              'updatedDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 1,
              'value' => 'Вы зарегистрированы'
      ));

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 0,
              'value' => 'You are registered'
      ));
  }

  public function down()
  {
    echo "m141022_194521_add_values_to_message_3 does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
