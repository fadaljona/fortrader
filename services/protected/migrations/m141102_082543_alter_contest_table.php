<?php
class m141102_082543_alter_contest_table extends DbMigration
{
  public function up()
  {
      $this->alterColumn('ft_contest', 'accountOpening', 'ENUM(\'MetaTrader API\',\'internal mail\',\'manually trader\', \'list of accounts\') NOT NULL');
  }

  public function down()
  {
    echo "m141102_082543_alter_contest_table does not support migration down.\n";
    return false;
  }
}
