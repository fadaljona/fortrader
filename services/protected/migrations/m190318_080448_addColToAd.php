<?php
class m190318_080448_addColToAd extends DbMigration
{
  public function up()
  {
    $this->addColumn('ft_advertisement', 'isDirectAdLink', 'boolean');
  }

  public function down()
  {
    $this->dropColumn('ft_advertisement', 'isDirectAdLink');
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
