<?php
class m141028_233452_add_user_profile_ext extends DbMigration
{
  public function up()
  {
	 $this->createTable("ft_user_profile_ext",array(
		 "id" => "pk",
		 "idUser"=> "int(11) NOT NULL",
		 "idModule"=>"varchar(32) NOT NULL",
		 "values" =>"text"
	 ),'ENGINE=InnoDB');
	  $this->createIndex("_idUser","ft_user_profile_ext","idUser",false);
	  $this->createIndex("_idModule","ft_user_profile_ext","idModule",false);
  }

  public function down()
  {
    echo "m141028_233452_add_user_profile_ext does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
