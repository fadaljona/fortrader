<?php
class m141102_172317_add_new_mail_template extends DbMigration
{
  public function up()
  {
      $this->insert('ft_mail_tpl', array(
              'key' => 'Account is created',
              'createdDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_mail_tpl_i18n', array(
          'idTpl' => $insertedId,
          'idLanguage' => 1,
          'subject' => 'Создан новый счет для конкурса',
          'message' => 'Уважаемый(ая) %username%,

Для вас был зарегистрирован счет в конкурсе "%contest_name%"

Номер аккаунта: %account_number%
Пароль: %password%
Инвесторский пароль счета: %inv_password%
Имя сервера: %server_name%
IP сервера: %server_ip%

С наилучшими пожеланиями,
ForTrader.Ru

Это электронное письмо было создано и послано автоматически.
fortrader.ru'
      ));

      $this->insert('ft_mail_tpl_i18n', array(
          'idTpl' => $insertedId,
          'idLanguage' => 0,
          'subject' => 'Account of contest was created',
          'message' => 'Dear %username%,

For you was created account in contest "%contest_name%".

Account: %account_number%
Password: %password%
Investor Password: %inv_password%
Server Name %server_name%
Server IP: %server_ip%

We are glad you joined us, and we hope that you will enjoy here.

With best regards,
ForTrader.Ru
'
      ));
  }

  public function down()
  {
    echo "m141102_172317_add_new_mail_template does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
