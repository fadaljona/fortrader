<?php
class m180824_071859_addTitleAndMetatileToConverts extends DbMigration
{
  public function up()
  {
    $this->addColumn('ft_currency_rates_convert_i18n', 'title', 'string');
    $this->addColumn('ft_currency_rates_convert_i18n', 'metaTitle', 'string');
  }

  public function down()
  {
    $this->dropColumn('ft_currency_rates_convert_i18n', 'title');
    $this->dropColumn('ft_currency_rates_convert_i18n', 'metaTitle');
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
