<?php
class m140917_112043_ea_redesign extends DbMigration
{
  public function up()
  {
    $transaction=$this->getDbConnection()->beginTransaction();
    try {
      $this->createTable('ft_ea_version_download', array(
        'id' => 'pk',
        'idEAVersion' => 'INT NOT NULL',
        'idUser' => 'INT NOT NULL',
        'downloadedDT' => 'DATETIME',
      ));
      $this->createIndex('idEAVersion', 'ft_ea_version_download', 'idEAVersion');
      $this->createIndex('idUser', 'ft_ea_version_download', 'idUser');
      $this->createIndex('downloadedDT', 'ft_ea_version_download', 'downloadedDT');
      
      $lang = LanguageModel::getByAlias('ru');
      $messages = array(
        '*' => array(
          'EA' => 'Советники',
          'EA Catalog' => 'Каталог советников',
          'EA Monitoring' => 'Мониторинг советников',
          'EA all' => 'Все',
          'EA last' => 'Новые советники',
          'EA popular' => 'Популярные советники',
          'EA views' => 'просмотров',
          'EA views1' => 'просмотр',
          'EA views2' => 'просмотра',
        ),
        'EA' => array(
          'EA last column title' => 'Новые советники',
          'EA last column desc' => 'Недавно добавили',
          'EA comment column title' => 'Обсуждают',
          'EA comment column desc' => 'Сейчас обсуждают',
          'EA rating column title' => 'Рейтинговые',
          'EA rating column desc' => 'Лучшие по оценкам пользователей',
          'EA popular column title' => 'Популярные',
          'EA popular column desc' => 'Самые скачиваемые за неделю',
        ),
      );
      foreach ($messages as $ns => $nsList) {
        foreach ($nsList as $key => $translation) {
          $this->addMessage($ns, $lang->id, $key, $translation);
        }
      }
      
      $transaction->commit();
    } catch(Exception $e) {
      echo "Exception: ".$e->getMessage()."\n";

      $this->dropTable('ft_ea_version_download');
      $transaction->rollback();
      return false;
    }
    
  }

  public function down()
  {
    echo "m140917_112043_ea_redesign does not support migration down.\n";
    return false;
  }

  protected function addMessage($ns, $idLang, $key, $translation) {
    $mess = MessageModel::model()->find("`t`.`ns` = '{$ns}' AND `t`.`key` = '{$key}'");
    if (!$mess) {
      $mess = new MessageModel();
      $mess->ns = $ns;
      $mess->key = $key;
      $mess->type = 'message';
      $mess->createdDT = date('Y-m-d H:i:s');
      $mess->save();
    }
    
    $messI18N = MessageI18NModel::model()->find("idMessage = '{$mess->id}' AND idLanguage = '{$idLang}'");
    if (!$messI18N) {
      $messI18N = new MessageI18NModel();
      $messI18N->idMessage = $mess->id;
      $messI18N->idLanguage = $idLang;
    }

    $messI18N->value = $translation;
    $messI18N->save();
  }
}
