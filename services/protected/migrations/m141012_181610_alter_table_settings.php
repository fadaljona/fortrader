<?php
class m141012_181610_alter_table_settings extends DbMigration
{
  public function up()
  {
      $this->addColumn('ft_settings', 'common_numberOfNotifications', 'INT(10) DEFAULT 10 NOT NULL');
  }

  public function down()
  {
    echo "m141012_181610_alter_table_settings does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
