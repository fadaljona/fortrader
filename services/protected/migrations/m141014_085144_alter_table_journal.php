<?php
class m141014_085144_alter_table_journal extends DbMigration
{
  public function up()
  {
      $this->addColumn('ft_journal', 'hide_when_created', 'TINYINT(1) DEFAULT 0 NULL');
  }

  public function down()
  {
    echo "m141014_085144_alter_table_journal does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
