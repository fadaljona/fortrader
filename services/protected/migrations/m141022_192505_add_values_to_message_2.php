<?php
class m141022_192505_add_values_to_message_2 extends DbMigration
{
  public function up()
  {
      $this->insert('ft_message', array(
              'key' => 'Contest is completed',
              'ns' => '*',
              'createdDT' => date('Y-m-d H:i:s'),
              'updatedDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 1,
              'value' => 'Конкурс завершен'
      ));

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 0,
              'value' => 'Contest is completed'
      ));
  }

  public function down()
  {
    echo "m141022_192505_add_values_to_message_2 does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
