<?php
class m141202_174513_add_messages_to_i18n extends DbMigration
{
  public function up()
  {
      $this->insert('ft_message', array(
              'key' => '% of earnings',
              'ns' => '*',
              'createdDT' => date('Y-m-d H:i:s'),
              'updatedDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 1,
              'value' => '% заработка'
      ));

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 0,
              'value' => '% of earnings'
      ));
  }

  public function down()
  {
    echo "m141202_174513_add_messages_to_i18n does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
