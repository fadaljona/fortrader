<?php
class m180526_071722_updateSourceType extends DbMigration
{
  public function up()
  {
    $this->execute("ALTER TABLE `ft_quotes_symbols` CHANGE `sourceType` `sourceType` ENUM('own','yahoo','bitz','bitfinex','okex','binance') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'own'");
  }

  public function down()
  {
    echo "m180526_071722_updateSourceType does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
