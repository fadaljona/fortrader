<?php
class m141102_085830_create_table_ft_list_of_accounts extends DbMigration
{
  public function up()
  {
      $this->createTable('ft_list_of_accounts', array(
          'id' => 'pk',
          'login' => 'string NOT NULL',
          'password' => 'string NOT NULL',
          'inv_password' => 'string NOT NULL',
          'contest_id' => 'int NOT NULL',
      ));
  }

  public function down()
  {
    echo "m141102_085830_create_table_ft_list_of_accounts does not support migration down.\n";
    return false;
  }
}
