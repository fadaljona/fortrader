<?php
class m141102_170256_add_messages extends DbMigration
{
  public function up()
  {
      $this->insert('ft_message', array(
              'key' => 'Number of you account',
              'ns' => '*',
              'createdDT' => date('Y-m-d H:i:s'),
              'updatedDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 1,
              'value' => 'Номер счета'
      ));

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 0,
              'value' => 'Number of you account'
      ));

      $this->insert('ft_message', array(
              'key' => 'Password of a investor',
              'ns' => '*',
              'createdDT' => date('Y-m-d H:i:s'),
              'updatedDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 1,
              'value' => 'Инвесторский пароль счета'
      ));

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 0,
              'value' => 'Password of a investor'
      ));

      $this->insert('ft_message', array(
              'key' => 'Server name',
              'ns' => '*',
              'createdDT' => date('Y-m-d H:i:s'),
              'updatedDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 1,
              'value' => 'Имя сервера'
      ));

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 0,
              'value' => 'Server name'
      ));

      $this->insert('ft_message', array(
              'key' => 'Server IP',
              'ns' => '*',
              'createdDT' => date('Y-m-d H:i:s'),
              'updatedDT' => date('Y-m-d H:i:s')
      ));

      $insertedId = Yii::app()->db->getLastInsertId();

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 1,
              'value' => 'IP сервера'
      ));

      $this->insert('ft_message_i18n', array(
              'idMessage' => $insertedId,
              'idLanguage' => 0,
              'value' => 'Server IP'
      ));
  }

  public function down()
  {
    echo "m141102_170256_add_messages does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
