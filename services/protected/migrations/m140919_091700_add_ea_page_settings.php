<?php
class m140919_091700_add_ea_page_settings extends DbMigration
{
  public function up()
  {
    $transaction=$this->getDbConnection()->beginTransaction();
    try {
      $this->addColumn('ft_settings', 'common_eaPerPage', 'INT NOT NULL DEFAULT 20');
      $transaction->commit();
    } catch(Exception $e) {
      echo "Exception: ".$e->getMessage()."\n";

      $transaction->rollback();
      return false;
    }
  }

  public function down()
  {
    echo "m140919_091700_add_ea_page_settins does not support migration down.\n";
    return false;
  }
}
