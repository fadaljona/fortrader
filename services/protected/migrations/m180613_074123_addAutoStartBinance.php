<?php
class m180613_074123_addAutoStartBinance extends DbMigration
{
  public function up()
  {
    $this->addColumn('ft_quotes_settings', 'autoStartBinance', 'tinyint(1) default 0');
  }

  public function down()
  {
    $this->dropColumn('ft_quotes_settings', 'autoStartBinance');
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
