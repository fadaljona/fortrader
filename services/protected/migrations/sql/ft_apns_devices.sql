
SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `ft_apns_devices`
-- ----------------------------
DROP TABLE IF EXISTS `ft_apns_devices`;
CREATE TABLE `ft_apns_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `createdDT` datetime NOT NULL,
  `updatedDT` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`token`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
