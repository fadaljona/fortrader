<?php
class m141008_071637_alter_table_journal extends DbMigration
{
  public function up()
  {
      $this->addColumn('ft_journal', 'status', 'enum("0","1") DEFAULT "0"');
      $this->addColumn('ft_journal', 'push_notification', 'enum("0","1") DEFAULT "0"');
  }

  public function down()
  {
    echo "m141008_071637_alter_table_journal does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
