<?php
class m141102_161125_add_field_to_ft_contest_member extends DbMigration
{
  public function up()
  {
      $this->addColumn('ft_contest_member', 'password', 'string NOT NULL DEFAULT ""');
  }

  public function down()
  {
    echo "m141102_161125_add_field_to_ft_contest_member does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
