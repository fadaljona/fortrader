<?php
class m141202_081354_alter_table_quotes_symbols extends DbMigration
{
  public function up()
  {
	  $this->addColumn("ft_quotes_symbols","nalias","VARCHAR(255) DEFAULT NULL");
	  $this->createIndex("_nalias","ft_quotes_symbols","nalias",false);
  }

  public function down()
  {
    echo "m141202_081354_alter_table_quotes_symbols does not support migration down.\n";
    return false;
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
