<?php
class m180623_110539_updateFaq extends DbMigration
{
  public function up()
  {
    $this->addColumn('ft_faq_item', 'order', 'integer');
    $this->addColumn('ft_faq_item', 'replaceItems', 'text');
  }

  public function down()
  {
    $this->dropColumn('ft_faq_item', 'order');
    $this->dropColumn('ft_faq_item', 'replaceItems');
  }

  /*
  // Use safeUp/safeDown to do migration with transaction
  public function safeUp()
  {
  }

  public function safeDown()
  {
  }
  */
}
