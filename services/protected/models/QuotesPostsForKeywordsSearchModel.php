<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesPostsForKeywordsSearchModel extends ModelBase {
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		function rules() {
			return Array(
				Array( 'id', 'unique'),
			);
		}
		public function tableName(){
			return '{{quotes_posts_for_keywords_search}}';
		}
	}
?>