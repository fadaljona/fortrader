<?php

require_once Yii::getPathOfAlias('application') . '/vendor/autoload.php';

trait ReplaceItemsTrait
{
    public function validateReplaces($field, $params)
    {
        $uniques = [];

        foreach ($this->$field as $index => $item) {
            if (!$item['modelClass']) {
                continue;
            }
            if (!$item['object']) {
                $this->addError("{$field}[{$index}]['object']", Yii::t('*', 'replace object can not be empty.'));
            }
            if (!$item['objectName']) {
                $this->addError("{$field}[{$index}]['objectName']", Yii::t('*', 'replace objectName can not be empty.'));
            }
            if (!$item['whereToSearch']) {
                $this->addError("{$field}[{$index}]['whereToSearch']", Yii::t('*', 'replace whereToSearch can not be empty.'));
            }
            if ($item['lang'] != 0 && $item['lang'] != 1) {
                $this->addError("{$field}[{$index}]['lang']", Yii::t('*', 'replace lang can not be empty.'));
            }

            if (!in_array($item['modelClass'] . $item['objectName'] . $item['whereToSearch'] . $item['lang'], $uniques)) {
                $uniques[] = $item['modelClass'] . $item['objectName'] . $item['whereToSearch'] . $item['lang'];
            } else {
                $this->addError("{$field}[{$index}]", Yii::t('*', 'such row already exists. modelClass objectName whereToSearch lang'));
            }
        }
    }

    /**
     * get Property With Replace
     *
     * @param string $property
     * @return string
     */
    public function getPropertyWithReplace($property)
    {
        if (!$property) {
            return '';
        }

        $calledClass = get_called_class();

        $cacheKey = "cache.twig.replace.property.$calledClass.object.{$this->id}.$property."  . Yii::app()->language;

        if ($content = Yii::app()->cache->get($cacheKey)) {
            return $content;
        } else {
            if (!property_exists($calledClass, $property) && !method_exists($calledClass, 'get' . ucfirst($property))) {
                return '';
            }
    
            $langId = LanguageModel::getCurrentLanguageID();
            $params = [];
    
            foreach ($this->replaceItems as $item) {
                if ($item['lang'] != $langId || $item['whereToSearch'] != $property || !class_exists($item['modelClass'])) {
                    continue;
                }
                $modelClass = $item['modelClass'];
    
                $model = $modelClass::model()->findByPk($item['object']);
    
                if (!$model) {
                    continue;
                }
    
                $params[$item['objectName']] = $model;
            }
    
            $loader = new Twig_Loader_Array([
                $property => $this->$property,
            ]);
            $twig = new Twig_Environment($loader);
            
            $content = $twig->render($property, $params);

            Yii::app()->cache->set($cacheKey, $content, self::REPLACED_PROPERTY_CACHE_TIME);

            return $content;
        }
    }
}
