<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAStatementModel extends ModelBase {
		const PATHUploadDir = 'uploads/eaStatements';
		const PATHHoldStatementDir = 'uploads/eaStatements/hold';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_statement}}";
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'EAStatementI18NModel', 'idStatement', 'alias' => 'i18nsEAStatement' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'EAStatementI18NModel', 'idStatement', 
					'alias' => 'cLI18NEAStatement',
					'on' => '`cLI18NEAStatement`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'version' => Array( self::BELONGS_TO, 'EAVersionModel', 'idVersion' ),
				'broker' => Array( self::BELONGS_TO, 'BrokerModel', 'idBroker' ),
				'server' => Array( self::BELONGS_TO, 'ServerModel', 'idServer' ),
				'countMessages' => Array( self::STAT, 'UserMessageModel', 'idLinkedObj', 'condition' => " `instance` = 'eaStatement/single' " ),
				'EATradeAccounts' => Array( self::HAS_MANY, 'EATradeAccountModel', 'idVersion' ),
			 );
		}
		static function getAddURL( $idEA, $idVersion = null ) {
			$params = Array();
			$params[ 'idEA' ] = $idEA;
			if( $idVersion ) {
				$params[ 'idVersion' ] = $idVersion;
			}
			return Yii::App()->createURL( 'eaStatement/add', $params );
		}
		static function getEditURL( $id ) {
			return Yii::App()->createURL( 'eaStatement/edit', Array( 'id' => $id ));
		}
		static function getModelSingleURL( $id ) {
			return Yii::App()->createURL( 'eaStatement/single', Array( 'id' => $id ));
		}
		static function getModelDownloadSetURL( $id ) {
			return Yii::App()->createURL( 'eaStatement/downloadSet', Array( 'id' => $id ));
		}
		static function getModelViewStatementURL( $id ) {
			return Yii::App()->createURL( 'eaStatement/viewStatement', Array( 'id' => $id ));
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->title )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->title )) return $i18n;
			}
		}
		function getTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->title : '';
		}
		function getDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->desc : '';
		}
		function getAlt() {
			return "Statment #{$this->id} {$this->symbol} {$this->period} by {$this->user->user_login}";
		}
		function getShortAlt() {
			return "Statment #{$this->id}";
		}
		
		function getSingleURL() {
			return self::getModelSingleURL( $this->id );
		}
		function getDownloadSetURL() {
			return self::getModelDownloadSetURL( $this->id );
		}	
		function getViewStatementURL() {
			return self::getModelViewStatementURL( $this->id );
		}
		function viewed() {
			$this->views++;
			$this->update( Array( 'views' ));
		}
		function downloaded() {
			$this->downloads++;
			$this->update( Array( 'downloads' ));
		}
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->title ) or strlen( $obj->desc )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = EAStatementI18NModel::model()->findByAttributes( Array( 'idStatement' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = EAStatementI18NModel::instance( $this->id, $idLanguage, $obj->title, $obj->desc );
						$i18n->save();
					}
					else{
						$i18n->title = $obj->title;
						$i18n->desc = $obj->desc;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			
			# set
		function getPathFileSet() {
			return strlen($this->nameFileSet) ? self::PATHUploadDir."/{$this->nameFileSet}" : '';
		}
		function getSrcFileSet() {
			return $this->pathFileSet ? Yii::app()->baseUrl."/{$this->pathFileSet}" : '';
		}
		function deleteFileSet() {
			if( strlen( $this->nameFileSet )) {
				if( is_file( $this->pathFileSet ) and !@unlink( $this->pathFileSet )) throw new Exception( "Can't delete {$this->pathFileSet}" );
				$dir = dirname( $this->pathFileSet );
				if( $dir != self::PATHUploadDir and is_dir( $dir ) and !@rmdir( $dir )) throw new Exception( "Can't delete {$dir}" );
				$this->nameFileSet = null;
			}
		}
		function setFileSet( $name ) {
			$this->deleteFileSet();
			$this->nameFileSet = $name;
		}
		function uploadFileSet( $uploadedFile ) {
			$dirName = CommonLib::getFreeFileName( self::PATHUploadDir );
			$filePath = self::PATHUploadDir."/{$dirName}";
			is_dir( $filePath ) or mkdir( $filePath );
			
			$fileName = $uploadedFile->getName();
			$fileRelPath = "{$dirName}/{$fileName}";
			$fileFullPath = "{$filePath}/{$fileName}";
			
			if( !@$uploadedFile->saveAs( $fileFullPath )) throw new Exception( "Can't write to ".self::PATHUploadDir );
			$this->setFileSet( $fileRelPath );
		}
			
			# statement
		static function holdFileStatement( $uploadedFile ) {
			CommonLib::cleanDir( self::PATHHoldStatementDir, 86400 );
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHHoldStatementDir, $fileEx ));
			
			$filePath = self::PATHHoldStatementDir."/{$fileName}.{$fileEx}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHHoldStatementDir );
			
			return $filePath;
		}
		function getPathFileStatement() {
			return strlen($this->nameFileStatement) ? self::PATHUploadDir."/{$this->nameFileStatement}" : '';
		}
		function getSrcFileStatement() {
			return $this->pathFileStatement ? Yii::app()->baseUrl."/{$this->pathFileStatement}" : '';
		}
		function deleteFileStatement() {
			if( strlen( $this->nameFileStatement )) {
				if( is_file( $this->pathFileStatement ) and !@unlink( $this->pathFileStatement )) throw new Exception( "Can't delete {$this->pathFileStatement}" );
				$dir = dirname( $this->pathFileStatement );
				if( $dir != self::PATHUploadDir and is_dir( $dir ) and !@rmdir( $dir )) throw new Exception( "Can't delete {$dir}" );
				$this->nameFileStatement = null;
			}
		}
		function setFileStatement( $name ) {
			$this->deleteFileStatement();
			$this->nameFileStatement = $name;
		}
		function clearFileStatement( $path ) {
			$content = file_get_contents( $path );
			$content = preg_replace( '#<script .*(</script>|$)#Uis', '', $content );
			$content = preg_replace( '#<a .*(</a>|$)#Uis', '', $content );
			file_put_contents( $path, $content );
		}
		function uploadFileStatement( $uploadedFile ) {
			$dirName = CommonLib::getFreeFileName( self::PATHUploadDir );
			$filePath = self::PATHUploadDir."/{$dirName}";
			is_dir( $filePath ) or mkdir( $filePath );
			
			$fileName = $uploadedFile->getName();
			$fileRelPath = "{$dirName}/{$fileName}";
			$fileFullPath = "{$filePath}/{$fileName}";
			
			if( !@$uploadedFile->saveAs( $fileFullPath )) throw new Exception( "Can't write to ".self::PATHUploadDir );
			$this->clearFileStatement( $fileFullPath );
			$this->setFileStatement( $fileRelPath );
		}
		
			# graph
		function getPathFileGraph() {
			return strlen($this->nameFileGraph) ? self::PATHUploadDir."/{$this->nameFileGraph}" : '';
		}
		function getSrcFileGraph() {
			return $this->pathFileGraph ? Yii::app()->baseUrl."/{$this->pathFileGraph}" : '';
		}
		function deleteFileGraph() {
			if( strlen( $this->nameFileGraph )) {
				if( is_file( $this->pathFileGraph ) and !@unlink( $this->pathFileGraph )) throw new Exception( "Can't delete {$this->pathFileGraph}" );
				$dir = dirname( $this->pathFileGraph );
				if( $dir != self::PATHUploadDir and is_dir( $dir ) and !@rmdir( $dir )) throw new Exception( "Can't delete {$dir}" );
				$this->nameFileGraph = null;
			}
		}
		function setFileGraph( $name ) {
			$this->deleteFileGraph();
			$this->nameFileGraph = $name;
		}
		function uploadFileGraph( $uploadedFile ) {
			$dirName = CommonLib::getFreeFileName( self::PATHUploadDir );
			$filePath = self::PATHUploadDir."/{$dirName}";
			is_dir( $filePath ) or mkdir( $filePath );
			
			$fileName = $uploadedFile->getName();
			$fileRelPath = "{$dirName}/{$fileName}";
			$fileFullPath = "{$filePath}/{$fileName}";
			
			if( !@$uploadedFile->saveAs( $fileFullPath )) throw new Exception( "Can't write to ".self::PATHUploadDir );
			$this->setFileGraph( $fileRelPath );
		}
		function getFileGraph( $options = Array() ) {
			$options[ 'src' ] = $this->srcFileGraph;
			return $this->nameFileGraph ? CHtml::tag( 'img', $options ) : '';
		}
			
			# EATradeAccounts
		function deleteEATradeAccounts() {
			foreach( $this->EATradeAccounts as $statsment ) {
				$statsment->delete();
			}
		}
		
			# events
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA_STATEMENT, Array( 'idEAStatement' => $this->id, 'idUser' => $this->idUser ));
			}
		}		
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			$this->deleteFileSet();
			$this->deleteFileStatement();
			$this->deleteFileGraph();
			$this->deleteEATradeAccounts();
		}
	}

?>