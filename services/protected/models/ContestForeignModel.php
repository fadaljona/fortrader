<?
	Yii::import( 'models.base.ModelBase' );
	Yii::import( 'components.MailerComponent' );
	
	final class ContestForeignModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'ContestForeignI18nModel', 'idContest', 'alias' => 'i18nsContestForeign' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'ContestForeignI18nModel', 'idContest', 
					'alias' => 'cLI18NContestForeignModel',
					'on' => '`cLI18NContestForeignModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->title )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->title )) return $i18n;
			}
		}
		function getTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->title : '';
		}
		function getSponsor() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->sponsor : '';
		}
		function getLink() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->link : '';
		}
		
			# dp
		
		static function getListDp( $currentPage = 0 ){
			$DP = new CActiveDataProvider( 'ContestForeignModel', Array(
				'criteria' => Array(
					'condition' => " `t`.`approved` = 1 ",
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`begin` DESC ',
				),
				'pagination' => array(
				  'pageSize' => 10,
				  'pageVar' => 'foreignContestPage',
				  'currentPage' => $currentPage,
				)
			));
			return $DP;
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->title )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = ContestForeignI18nModel::model()->findByAttributes( Array( 'idContest' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = ContestForeignI18nModel::instance( $this->id, $idLanguage, $obj->title, $obj->sponsor, $obj->link );
						$i18n->save();
					}else{
						$i18n->title = $obj->title;
						$i18n->sponsor = $obj->sponsor;
						$i18n->link = $obj->link;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
			if( $this->beenNewRecord && !Yii::app()->user->checkAccess('contestControl') ) {
				$this->sendNotice();
			}
		}
		function sendNotice(){
			$factoryMailer = new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			
			$mailer->ClearReplyTos();
			//$mailer->AddReplyTo( $this->email, $this->user );
			
			$key = 'New foreign contest';
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			if( !$mailTpl ) self::throwI18NException( "Can't load mail template! ({key})", Array( '{key}' => $key ));
			
			$mailTplI18n = $mailTpl->getI18N( 'ru' );
			
			$i18n = $this->getI18N();
			
			$message = strtr( $mailTplI18n->message, Array(	
				'{title}' => $i18n->title,
				'{sponsor}' => $i18n->sponsor,
				'{link}' => $i18n->link,
				'{prize}' => $this->prize,
				'{begin}' => $this->begin,
			));
			
			$contacts = ContactModel::model()->findAll( Array(
				'condition' => " `t`.`hidden` = 0 ",
			));
			
			if( !$contacts ) return;
			$emails = CommonLib::slice( $contacts, 'email' );
			
			foreach( $emails as $email ) {
				$mailer->addAddress( $email );
			}
			
			$mailer->Subject = $mailTplI18n->subject;
			if( substr_count( $message, '<' )) {
				$message = CommonLib::nl2br( $message, true );
				$mailer->MsgHTML( $message );
			}
			else{
				$mailer->Body = $message;
			}
			$mailer->send();
		}
			
		
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>