<?
	Yii::import( 'models.base.ModelBase' );

	final class BrokerSmDataModel extends ModelBase {
		public $rating;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'broker' => Array( self::HAS_ONE, 'BrokerModel', array( 'id' => 'idBroker' ) ),
			);
		}
		static function instance( $idBroker ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker' ));
		}
		static function getNewRating( $type ){
			$models = self::model()->findAll(Array(
				'select' => array( " `t`.`idBroker` ", " `t`.`vk` + `t`.`fb` as rating " ),
				'with' => array( 'broker' ),
				'order' => " rating DESC ",
				'condition' => " `broker`.`showInRating` = 1 AND `broker`.`type` = :type ",
				'params' => array( ':type' => $type )
			));
			print_r( count( $models ) );
			if( !count( $models ) ) return false;
			$outArr = array();
			$i=1;
			$prevRating = $models[0]->rating;
			foreach( $models as $model ){
				if( $prevRating == $model->rating ){
					$outArr[$model->idBroker] = $i;
				}else{
					$prevRating = $model->rating;
					$outArr[$model->idBroker] = $i+1;
					$i++;
				}
			}
			return $outArr;
		}

	}

?>