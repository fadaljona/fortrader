<?
	Yii::import( 'models.base.ModelBase' );
	
	final class JournalUserActivityModel extends ModelBase {
		const TYPE_EVENT_CREATE_COMMENT = 1;
		const TYPE_EVENT_DOWNLOAD = 2;
		const TYPE_EVENT_VIEW = 3;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'JournalUserActivityI18NModel', 'idActivity', 'alias' => 'i18nsJournalUserActivity' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'JournalUserActivityI18NModel', 'idActivity', 
					'alias' => 'cLI18NJournalUserActivity',
					'on' => '`cLI18NJournalUserActivity`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'journal' => Array( self::BELONGS_TO, 'JournalModel', 'idJournal' ),
			 );
		}
		static function instance( $idUser, $idJournal ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser', 'idJournal' ));
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->message )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->message )) return $i18n;
			}
		}
		function getMessage() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->message : '';
		}
		function getHTMLMessage( $maxLen = null ) {
			$message = $this->message;
			//$message = htmlspecialchars( $this->message );
			//$message = CommonLib::nl2br( $message );
			if( $maxLen ) $message = CommonLib::shorten( $message, $maxLen );
			return $message;
		}
			
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->message )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = JournalUserActivityI18NModel::model()->findByAttributes( Array( 'idActivity' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = JournalUserActivityI18NModel::instance( $this->id, $idLanguage, $obj->message );
						$i18n->save();
					}
					else{
						$i18n->message = $obj->message;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		function setI18NsFromMessage( $key, $ns = '*', $params = Array(), $paramsI18Ns = Array() ) {
			$message = MessageModel::model()->find(Array(
				'with' => 'i18ns',
				'condition' => "
					`t`.`key` = :key AND `t`.`ns` = :ns
				",
				'params' => Array(
					':key' => $key,
					':ns' => $ns,
				),
			));
			if( $message ) {
				$i18ns = Array();
				foreach( $message->i18ns as $i18n ) {
					$message = $i18n->value;
					if( $params ) $message = strtr( $message, $params );
					if( $paramsI18Ns and isset( $paramsI18Ns[ $i18n->idLanguage ])) $message = strtr( $message, $paramsI18Ns[ $i18n->idLanguage ]);
					$i18ns[ $i18n->idLanguage ] = (object)Array(
						'message' => $message,
					);
				}
				$this->setI18Ns( $i18ns );
			}
		}
			
			# events
		static function deleteExists( $idUser, $idJournal, $message ) {
			self::model()->deleteAll(Array(
				'condition' => " 
					`idUser` = :idUser
					AND `idJournal` = :idJournal
					AND EXISTS (
						SELECT 		1 
						FROM		`{{journal_user_activity_i18n}}`
						WHERE		`idActivity` = `id`
						AND			`message` LIKE :message
						LIMIT 		1
					)
				",
				'params' => Array(
					':idUser' => $idUser,
					':idJournal' => $idJournal,
					':message' => $message,
				),
			));
		}
		static function event( $type, $params ) {
			switch( $type ) {
				case self::TYPE_EVENT_CREATE_COMMENT:{
					if( $journal = JournalModel::model()->findByPk( $params[ 'idJournal' ])) {
						$activity = self::instance( $params[ 'idUser' ], $params[ 'idJournal' ]);
						$activity->save();
						
						$url = $journal->getSingleURL();
						$_params = Array();
						$_params[ '{link}' ] = CHtml::link( $journal->number, $url );
						$activity->setI18NsFromMessage( 'Add new comment to journal issie № {link}', '*', $_params );
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_JOURNAL_COMMENT, $params );
					}
					break;
				}
				case self::TYPE_EVENT_DOWNLOAD:{
					if( $journal = JournalModel::model()->findByPk( $params[ 'idJournal' ])) {
						$activity = self::instance( $params[ 'idUser' ], $params[ 'idJournal' ]);
						$activity->save();
						
						$url = $journal->getSingleURL();
						$_params = Array();
						$_params[ '{link}' ] = CHtml::link( $journal->number, $url );
						$activity->setI18NsFromMessage( 'Downloaded issie № {link}', '*', $_params );
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_DOWNLOAD_JOURNAL, $params );
					}
					break;
				}
				case self::TYPE_EVENT_VIEW:{
					if( $journal = JournalModel::model()->findByPk( $params[ 'idJournal' ])) {
						$activity = self::instance( $params[ 'idUser' ], $params[ 'idJournal' ]);
						$activity->save();
						
						$url = $journal->getSingleURL();
						$_params = Array();
						$_params[ '{link}' ] = CHtml::link( $journal->number, $url );
						$activity->setI18NsFromMessage( 'Readed journal issie № {link}', '*', $_params );
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_VIEW_JOURNAL, $params );
					}
					break;
				}
			}
			return true;
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>