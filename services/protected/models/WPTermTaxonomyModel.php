<?
	Yii::import( 'models.base.ModelBase' );
	
	final class WPTermTaxonomyModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "wp_term_taxonomy";
		}
		function relations() {
			return Array(
				'term' => Array( self::BELONGS_TO, 'WPTermModel', 'term_id' ),
			);
		}
	}

?>