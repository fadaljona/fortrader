<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MailingTypeI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{mailing_type_i18n}}";
		}
		static function instance( $idType, $idLanguage, $name ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idType', 'idLanguage', 'name' ));
		}
	}

?>