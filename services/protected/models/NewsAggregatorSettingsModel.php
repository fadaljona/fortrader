<?
	Yii::import( 'models.base.SettingsModelBase' );
	
	final class NewsAggregatorSettingsModel extends SettingsModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) {
					$model = new self();
					$model->id = 1;
				}
			}
			return $model;
		}
		public function geLangStemConstByLangId( $id ){
			if( !$this->messages->i18ns[$id]->value ) return false;
			$decoded = json_decode( $this->messages->i18ns[$id]->value );
			if( !$decoded->stemmerLangConstant ) return false;
			return $decoded->stemmerLangConstant;
		}
		public function getNewsMetaTitle(){
			if( !$this->texts || !$this->texts->newsMetaTitle ) return false;
			return $this->texts->newsMetaTitle;
		}
		public function getNewsTitle(){
			if( !$this->texts || !$this->texts->newsTitle ) return false;
			return $this->texts->newsTitle;
		}
		public function getNewsMetaDesc(){
			if( !$this->texts || !$this->texts->newsMetaDesc ) return false;
			return $this->texts->newsMetaDesc;
		}
		public function getNewsMetaKeys(){
			if( !$this->texts || !$this->texts->newsMetaKeys ) return false;
			return $this->texts->newsMetaKeys;
		}
		public function getNewsShortDesc(){
			if( !$this->texts || !$this->texts->newsShortDesc ) return false;
			return $this->texts->newsShortDesc;
		}
		public function getNewsFullDesc(){
			if( !$this->texts || !$this->texts->newsFullDesc ) return false;
			return $this->texts->newsFullDesc;
		}
		public function getSourceTitle(){
			if( !$this->texts || !$this->texts->sourceTitle ) return false;
			return $this->texts->sourceTitle;
		}
		public function getSourceMetaTitle(){
			if( !$this->texts || !$this->texts->sourceMetaTitle ) return false;
			return $this->texts->sourceMetaTitle;
		}
		public function getSourceMetaDesc(){
			if( !$this->texts || !$this->texts->sourceMetaDesc ) return false;
			return $this->texts->sourceMetaDesc;
		}
		public function getSourceMetaKeys(){
			if( !$this->texts || !$this->texts->sourceMetaKeys ) return false;
			return $this->texts->sourceMetaKeys;
		}
		public function getSourceShortDesc(){
			if( !$this->texts || !$this->texts->sourceShortDesc ) return false;
			return $this->texts->sourceShortDesc;
		}
		public function getSourceFullDesc(){
			if( !$this->texts || !$this->texts->sourceFullDesc ) return false;
			return $this->texts->sourceFullDesc;
		}
	}
?>