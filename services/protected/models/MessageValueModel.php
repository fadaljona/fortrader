<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MessageValueModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idKey, $text, $idLanguage ) {
			$model = new self();
			$model->idKey = $idKey;
			$model->value = $text;
			$model->idLanguage = $idLanguage;
			return $model;
		}
	}

?>