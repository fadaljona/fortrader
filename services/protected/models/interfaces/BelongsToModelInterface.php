<?

	class BelongsToModelInterface extends ModelInterfaceBase{
			// add relations
		static function addRelation( $model, &$relations, $relationName, $relationClass, $relationForeignKey, $parameters = Array() ) {
			$relations[ $relationName ] = Array(
				CActiveRecord::BELONGS_TO,
				$relationClass,
				$relationForeignKey
			);
			$properties = array_merge( $parameters, compact( 'relationName', 'relationClass', 'relationForeignKey' ));
			CComponent::addInterface( get_class( $model ), __CLASS__, $properties );
		}
			// constructor
		function __construct( $class, $properties = Array()) {
			parent::__construct( $class, $properties );
			extract( $this->properties );
			
			$this->callInterfaces = Array(
				'getBelongsToField' => 'getBelongsToField',
				'changeBelongsToRecord' => 'changeBelongsToRecord',
			);
			
			$this->getColumnInterface = 'getBelongsToField';
			$this->setColumnInterface = 'changeBelongsToRecord';
			
			if( isset( $controlFields )) {
				foreach( $controlFields as $controlField ) {
					if( is_array( $controlField )) {
						list( $outerName ) = array_keys( $controlField );
						list( $innerName ) = array_values( $controlField );
						$this->columns[ $innerName ] = $outerName;
					}
					else{
						$this->columns[ $controlFields ] = $controlFields;
					}
				}
			}
		}
			// metods
		function getBelongsToField( $object, $column ) {
			extract( $this->properties );
			
			if( !$object->$relationForeignKey ) 
				return null;
			
			$related = $object->$relationName;
			if( $related ) {
				$innerName = array_search( $column, $this->columns );
				return $related->$innerName;
			}
		}
		function changeBelongsToRecord( $object, $column, $value ) {
			extract( $this->properties );
			$innerName = array_search( $column, $this->columns );
			
			$related = CActiveRecord::model( $relationClass )->findByAttributes( Array(
				$innerName => $value,
			));
			if( $related ) {
				$object->$relationForeignKey = $related->getPrimaryKey();
			}
		}
			// call
		function canCall( $object, $func, $parameters ) {
			switch( $func ) {
				case $this->getColumnInterface: {
					list( $field ) = $parameters;
					return $this->canGetColumn( $object, $field );
				}
				case $this->setColumnInterface: {
					list( $field, $value ) = $parameters;
					return $this->canSetColumn( $object, $field, $value );
				}
				default:{
					return false;
				}
			}
		}
	}

?>