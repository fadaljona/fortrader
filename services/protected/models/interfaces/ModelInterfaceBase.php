<?

	class ModelInterfaceBase{
		protected $properties = Array();
		protected $getInterfaces = Array();
		protected $setInterfaces = Array();
		protected $callInterfaces = Array();
		protected $columns = Array();
		protected $getColumnInterface;
		protected $setColumnInterface;
		
		function __construct( $class, $properties = Array()) {
			$this->properties = $properties;
		}
			// exists	
		function exists( $object, $property ) {
			if( in_array( $property, $this->getInterfaces )) 
				return true;
			
			if( in_array( $property, $this->setInterfaces )) 
				return true;
				
			if( in_array( $property, $this->columns )) 
				return true;
			
			return false;
		}
			// get
		protected function canGetColumn( $object, $property ) {
			if( in_array( $property, $this->columns ) && $this->getColumnInterface ) 
				return true;
			
			return false;
		}
		function canGet( $object, $property ) {
			if( in_array( $property, $this->getInterfaces )) 
				return true;
			
			if( $this->canGetColumn( $object, $property )) 
				return true;
			
			return false;
		}
		function get( $object, $property ) {
			foreach( $this->getInterfaces as $key=>$value ) 
				if( $property === $value ) 
					return $object->{$this->callInterfaces[ $key ]}();
			
			if( $this->canGetColumn( $object, $property ))
				return $this->{$this->getColumnInterface}( $object, $property );
			
			return null;
		}
			// set
		protected function canSetColumn( $object, $property, $value ) {
			if( in_array( $property, $this->columns ) && $this->setColumnInterface ) 
				return true;
			
			return false;
		}
		function canSet( $object, $property, $value ) {
			if( in_array( $property, $this->setInterfaces )) 
				return true;
			
			if( $this->canSetColumn( $object, $property, $value )) 
				return true;
			
			return false;
		}
		function set( $object, $property, $value ) {
			foreach( $this->setInterfaces as $key=>$val ) 
				if( $property === $val ) 
					return $object->{$this->callInterfaces[ $key ]}( $value );
				
			
			if( $this->canSetColumn( $object, $property, $value ))
				return $this->{$this->setColumnInterface}( $object, $property, $value );
			
			return null;
		}
			// call
		function canCall( $object, $func, $parameters ) {
			if( in_array( $func, $this->callInterfaces )) 
				return true;
				
			return false;
		}
		function call( $object, $func, $parameters = Array() ) {
			foreach( $this->callInterfaces as $key=>$value ) 
				if( $func === $value ) {
					return $this->$key( $object, $parameters );
				}
		
			return null;
		}
	}

?>