<?php

interface ModelForTextReplaceInterface
{
    /**
     * properties for search with descriptions
     *
     * @return array
     */
    public static function getPropertiesForSearch();

    /**
     * array of id => model name
     *
     * @return array
     */
    public static function getModelsListForUseInReplace();
}
