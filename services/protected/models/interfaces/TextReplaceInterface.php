<?php

interface TextReplaceInterface
{
    const REPLACED_PROPERTY_CACHE_TIME = 60;

    /**
     * properties where replace availiable
     *
     * @return array
     */
    public static function getPropertiesForReplace();
}
