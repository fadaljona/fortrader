<?

	class I18NModelInterface extends ModelInterfaceBase{
		const defName = 'I18N';
		const patternI18NsRelation = '{interface}s';
		const patternCLI18NRelation = 'currentLanguage{interface}';
		const patternI18NInterface = '{interface}';
		const patternI18NsAssocInterface = '{interface}sAssoc';
		const patternI18NGetInterface = 'get{interface}';
		const patternI18NsGetAssocInterface = 'get{interface}sAssoc';
		const patternI18NsSetAssocInterface = 'set{interface}sAssoc';
		const patternI18NGetFieldInterface = 'get{interface}Field';
		const keyLanguage = 'idLanguage';
		private $holdsAssocs = Array();
		
			// static common
		static function getI18NModelClass( $modelName, $interfaceName = self::defName ) {
			return ModelExBase::_getModelClass( "{$modelName}{$interfaceName}" );
		}
		static function getI18NsRelationName( $interfaceName = self::defName ) {
			$relationName = strtr( self::patternI18NsRelation, Array( '{interface}' => $interfaceName ));
			return strtolower( $relationName );
		}
		static function getCLI18NRelationName( $interfaceName = self::defName ) {
			return strtr( self::patternCLI18NRelation, Array( '{interface}' => $interfaceName ));
		}
		static function getI18NsAlias( $modelName, $interfaceName = self::defName ) {
			$relationName = self::getI18NsRelationName( $interfaceName );
			return "{$relationName}{$modelName}";
		}
		static function getCLI18NAlias( $modelName, $interfaceName = self::defName ) {
			$relationName = self::getCLI18NRelationName( $interfaceName );
			return "{$relationName}{$modelName}";
		}
		static function getI18NInterfaceName( $interfaceName = self::defName ) {
			return strtr( self::patternI18NInterface, Array( '{interface}' => $interfaceName ));
		}
		static function getI18NsAssocInterfaceName( $interfaceName = self::defName ) {
			return strtr( self::patternI18NsAssocInterface, Array( '{interface}' => $interfaceName ));
		}
		static function getI18NGetInterfaceName( $interfaceName = self::defName ) {
			return strtr( self::patternI18NGetInterface, Array( '{interface}' => $interfaceName ));
		}
		static function getI18NsGetAssocInterfaceName( $interfaceName = self::defName ) {
			return strtr( self::patternI18NsGetAssocInterface, Array( '{interface}' => $interfaceName ));
		}
		static function getI18NsSetAssocInterfaceName( $interfaceName = self::defName ) {
			return strtr( self::patternI18NsSetAssocInterface, Array( '{interface}' => $interfaceName ));
		}
		static function getI18NGetFieldInterfaceName( $interfaceName = self::defName ) {
			return strtr( self::patternI18NGetFieldInterface, Array( '{interface}' => $interfaceName ));
		}
			// add relations
		static function addRelations( $model, &$relations, $parameters = Array()) {
			extract( $parameters );
			
				// interfaceName
			if( empty( $interfaceName )) 
				$interfaceName = self::defName;
				
				// modelName
			$modelName = $model->_getName();
			
				// I18NModelClass
			if( empty( $I18NModelClass )) 
				$I18NModelClass = self::getI18NModelClass( $modelName, $interfaceName );
				
				// I18NsRelation
			if( empty( $I18NsRelationName )) {
				$I18NsRelationName = self::getI18NsRelationName( $interfaceName );	
				if( empty( $I18NsAlias )) 
					$I18NsAlias = "{$I18NsRelationName}{$modelName}";
			}
			else{
				if( empty( $I18NsAlias )) 
					$I18NsAlias = self::getI18NsAlias( $modelName, $interfaceName );	
			}
							
				// CLI18NRelation
			if( empty( $CLI18NRelationName )) {
				$CLI18NRelationName = self::getCLI18NRelationName( $interfaceName );	
				if( empty( $CLI18NAlias )) 
					$CLI18NAlias = "{$CLI18NRelationName}{$modelName}";
			}
			else{
				if( empty( $CLI18NAlias )) 
					$CLI18NAlias = self::getCLI18NAlias( $modelName, $interfaceName );	
			}
				
				// keyLanguage
			if( empty( $keyLanguage )) 
				$keyLanguage = self::keyLanguage;
				
				// foreignKey
			if( empty( $foreignKey )) 
				$foreignKey = CActiveRecord::model( $I18NModelClass )->_getFirstPkName();
			
				// add relations
			$relations[ $I18NsRelationName ] = Array( 
				CActiveRecord::HAS_MANY, 
				$I18NModelClass, 
				$foreignKey,
				'alias' => $I18NsAlias,
			);
			
			$relations[ $CLI18NRelationName ] = Array( 
				CActiveRecord::HAS_ONE, 
				$I18NModelClass, 
				$foreignKey, 
				'alias' => $CLI18NAlias,
				'on' => " `{$CLI18NAlias}`.`{$keyLanguage}` = @idCurrentLanguage ",
			);
			
			$properties = array_merge( $parameters, compact( 'interfaceName', 'I18NModelClass', 'keyLanguage', 'foreignKey', 'I18NsRelationName', 'CLI18NRelationName' ));
			CComponent::addInterface( get_class( $model ), __CLASS__, $properties );
		}
		static function hasModelRelations( $modelName, $parameters = Array()) {
			extract( $parameters );
			
				// interfaceName
			if( empty( $interfaceName )) 
				$interfaceName = self::defName;
				
				// I18NsRelation
			if( empty( $I18NsRelationName )) 
				$I18NsRelationName = self::getI18NsRelationName( $interfaceName );	
							
				// CLI18NRelation
			if( empty( $CLI18NRelationName )) 
				$CLI18NRelationName = self::getCLI18NRelationName( $interfaceName );	
			
			$modelClass = ModelExBase::_getModelClass( $modelName );
			$relations = CActiveRecord::model( $modelClass )->_getRelations();
			
			$has = array_key_exists( $I18NsRelationName, $relations ) 
				   && array_key_exists( $CLI18NRelationName, $relations );
				   
			return $has;
		}
			// constructor
		function __construct( $class, $properties = Array()) {
			parent::__construct( $class, $properties );
			extract( $this->properties );
			
			$this->getInterfaces = Array(
				'getI18N' => self::getI18NInterfaceName( $interfaceName ),
				'getI18NsAssoc' => self::getI18NsAssocInterfaceName( $interfaceName ),
			);

			$this->setInterfaces = Array(
				'setI18NsAssoc' => $this->getInterfaces[ 'getI18NsAssoc' ],
			);

			$this->callInterfaces = Array(
				'getI18N' => self::getI18NGetInterfaceName( $interfaceName ),
				'getI18NsAssoc' => self::getI18NsGetAssocInterfaceName( $interfaceName ),
				'setI18NsAssoc' => self::getI18NsSetAssocInterfaceName( $interfaceName ),
			);
			
			$this->getColumnInterface = 'getI18NField';
			
				// det columns
			$I18NModel = CActiveRecord::model( $I18NModelClass );
			$columns = $I18NModel->_getColumnNames();
			$pks = $I18NModel->_getPkName();
			
			$columns = array_diff( $columns, $pks );
			$this->columns = $columns;
		}
			// private metods
		private static function transpositionI18N( $I18N ) {
			$transpositionI18N = Array();
			foreach( $I18N as $field=>$values ) {
				foreach( $values as $idLanguage=>$value ) {
					$transpositionI18N[ $idLanguage ][ $field ] = $value;
				}
			}
			return $transpositionI18N;
		}
		private function addHoldAssoc( $object, $value ) {
			foreach( $this->holdsAssocs as $i=>$holdI18NsAssoc ) {
				if( $holdI18NsAssoc->object == $object ) {
					$this->holdsAssocs[ $i ]->value = $value;
					return;
				}
			}
			$this->holdsAssocs[] = (object)Array(
				'object' => $object,
				'value' => $value,
			);
		}
		private function getHoldAssoc( $object ) {
			foreach( $this->holdsAssocs as $i=>$holdI18NsAssoc ) {
				if( $holdI18NsAssoc->object == $object ) {
					$value = $holdI18NsAssoc->value;
					unset( $this->holdsAssocs[ $i ]);
					return $value;
				}
			}
			return null;
		}
		private function saveAssoc( $object, $value ) {
			extract( $this->properties );
			
			$assoc = self::transpositionI18N( $value );
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $assoc as $idLanguage=>$obj ) {
				if( !in_array( $idLanguage, $idsLanguages )) continue;
				
				$pkName = $object->_getFirstPkName();
				$I18N = CActiveRecord::model( $I18NModelClass )->findByAttributes( Array( $foreignKey => $object->{$pkName}, $keyLanguage => $idLanguage ));
				
				if( !CommonLib::isEmptyObj( $obj )) {
					if( !$I18N ) {
						$I18N = new $I18NModelClass();
						$I18N->$foreignKey = $object->$pkName;
						$I18N->$keyLanguage = $idLanguage;
					}
					foreach( $obj as $key=>$value ) {
						$I18N->$key = $value;
					}
					$I18N->save();
				}
				elseif( $I18N ){
					$I18N->delete();
				}
			}
		}
			// events 
		function onAfterSaveModel( $event ) {
			$object = $event->sender;
			if( !$object->getPrimaryKey()) return;
			$value = $this->getHoldAssoc( $object );
			if( $value ) {
				$this->saveAssoc( $object, $value );
			}
		}
			// metods
		function getI18N( $object, $parameters = Array() ) {
			extract( $this->properties );
			
			if( $object->$CLI18NRelationName ) return $object->$CLI18NRelationName;
			foreach( $object->$I18NsRelationName as $I18N ) {
				if( $I18N->$keyLanguage == 0 ) {
					return $I18N;
				}
			}
			foreach( $object->$I18NsRelationName as $I18N ) {
				return $I18N;
			}
			
			return null;
		}
		function getI18NsAssoc( $object, $parameters = Array() ) {
			extract( $this->properties );
			$assoc = Array();
					
			foreach( $object->$I18NsRelationName as $I18N ) {
				$attributes = $I18N->getAttributes();
				$pks = $I18N->_getPkName();
				$attributes = array_diff_key( $attributes, array_combine( $pks, $pks ));
				$assoc[ $I18N->$keyLanguage ] = $attributes;
			}
			
			$assoc = self::transpositionI18N( $assoc );
			return $assoc;
		}
		function setI18NsAssoc( $object, $parameters = Array()) {
			list($value) = $parameters;
			$this->addHoldAssoc( $object, $value );
			$object->detachEventHandler( 'onAfterSave', Array( $this, 'onAfterSaveModel' ));
			$object->attachEventHandler( 'onAfterSave', Array( $this, 'onAfterSaveModel' ));
		}
		function getI18NField( $object, $column ) {
			extract( $this->properties );
			
			$I18N = $object->$interfaceName;
			return $I18N ? $I18N->$column : "";
		}
	}

?>