<?

	class ManyToManyModelInterface extends ModelInterfaceBase{
		const keyPrimaryModel = 0;
		const keyLinkedModel = 1;
		private $holdsLinksArrays = Array();
			// linksTo		
		static function getLinksRelationName( $relationName ) {
			$relationName = ucfirst( $relationName );
			return "linksTo{$relationName}";
		}
		static function getLinksAlias( $modelName, $relationName ) {
			$relationName = ucfirst( $relationName );
			return "linksTo{$relationName}{$modelName}";
		}
		static function getRelationAlias( $modelName, $relationName ) {
			return "{$relationName}{$modelName}";
		}
			// linksToArray
		static function getLinksArrayInterfaceName( $relationName ) {
			$relationName = ucfirst( $relationName );
			return "linksTo{$relationName}Array";
		}
			// linksToString
		static function getLinksStringInterfaceName( $relationName ) {
			$relationName = ucfirst( $relationName );
			return "linksTo{$relationName}String";
		}
			// getLinksToArray
		static function getLinksArrayGetInterfaceName( $relationName ) {
			$relationName = ucfirst( $relationName );
			return "getLinksTo{$relationName}Array";
		}
			// setLinksToArray
		static function getLinksArraySetInterfaceName( $relationName ) {
			$relationName = ucfirst( $relationName );
			return "setLinksTo{$relationName}Array";
		}
			// getLinksToString
		static function getLinksStringGetInterfaceName( $relationName ) {
			$relationName = ucfirst( $relationName );
			return "getLinksTo{$relationName}String";
		}
			// setLinksToString
		static function getLinksStringSetInterfaceName( $relationName ) {
			$relationName = ucfirst( $relationName );
			return "setLinksTo{$relationName}String";
		}
			// deleteLinksTo
		static function getLinksDeleteInterfaceName( $relationName ) {
			$relationName = ucfirst( $relationName );
			return "deleteLinksTo{$relationName}";
		}
			// addLinksTo
		static function getLinksAddInterfaceName( $relationName ) {
			$relationName = ucfirst( $relationName );
			return "addLinksTo{$relationName}";
		}
			// add relations
		static function addRelations( $model, &$relations, $relationName, $relationClass, $linkClass, $parameters = Array() ) {
			extract( $parameters );				
				
				// modelName
			$modelName = $model->_getName();
			
				// foreignKeys
			if( empty( $foreignKeys ))
				$foreignKeys = CActiveRecord::model( $linkClass )->_getPkName();
			
			$pkName = CActiveRecord::model( $relationClass )->_getFirstPkName();
						
			$linksRelationName = self::getLinksRelationName( $relationName );
			$linksAlias = self::getLinksAlias( $modelName, $relationName );
			
			$relationAlias = self::getRelationAlias( $modelName, $relationName );
			
			$relations[ $linksRelationName ] = Array(
				CActiveRecord::HAS_MANY,
				$linkClass,
				$foreignKeys[self::keyPrimaryModel],
				'alias' => $linksAlias,
			);
			
			$relations[ $relationName ] = Array(
				CActiveRecord::HAS_MANY,
				$relationClass,
				Array( $foreignKeys[self::keyLinkedModel] => $pkName ),
				'alias' => $relationAlias,
				'through' => $linksRelationName,
			);
			
			$properties = array_merge( $parameters, compact( 'relationName', 'linksRelationName', 'relationClass', 'linkClass', 'foreignKeys' ));
			CComponent::addInterface( get_class( $model ), __CLASS__, $properties );
		}
			// constructor
		function __construct( $class, $properties = Array()) {
			parent::__construct( $class, $properties );
			extract( $this->properties );
			
			$this->getInterfaces = Array(
				'getLinksArray' => self::getLinksArrayInterfaceName( $relationName ),
				'getLinksString' => self::getLinksStringInterfaceName( $relationName ),
			);
			
			$this->setInterfaces = Array(
				'setLinksArray' => $this->getInterfaces[ 'getLinksArray' ],
				'setLinksString' => $this->getInterfaces[ 'getLinksString' ],
			);
			
				// call interfaces
			$this->callInterfaces = Array(
					// linksArray
				'getLinksArray' => self::getLinksArrayGetInterfaceName( $relationName ),
				'setLinksArray' => self::getLinksArraySetInterfaceName( $relationName ),
					// linksString
				'getLinksString' => self::getLinksStringGetInterfaceName( $relationName ),
				'setLinksString' => self::getLinksStringSetInterfaceName( $relationName ),
					// links
				'deleteLinks' => self::getLinksDeleteInterfaceName( $relationName ),
				'addLinks' => self::getLinksAddInterfaceName( $relationName ),
			);
		}
			// private metods
		private function addHoldLinksArray( $object, $value ) {
			foreach( $this->holdsLinksArrays as $i=>$holdLinksArray ) {
				if( $holdLinksArray->object == $object ) {
					$this->holdsLinksArrays[ $i ]->value = $value;
					return;
				}
			}
			$this->holdsLinksArrays[] = (object)Array(
				'object' => $object,
				'value' => $value,
			);
		}
		private function getHoldLinksArray( $object ) {
			foreach( $this->holdsLinksArrays as $i=>$holdLinksArray ) {
				if( $holdLinksArray->object == $object ) {
					$value = $holdLinksArray->value;
					unset( $this->holdsLinksArrays[ $i ]);
					return $value;
				}
			}
			return null;
		}
		private function saveLinksArray( $object, $value ) {
			extract( $this->properties );
			
			$ids = $value;
			foreach( $ids as $id ) {
				$link = CActiveRecord::model( $linkClass )->findByAttributes( Array( $foreignKeys[self::keyPrimaryModel] => $object->getPrimaryKey(), $foreignKeys[self::keyLinkedModel] => $id ));
				if( !$link ) {
					$link = new $linkClass;
					$link->{$foreignKeys[self::keyPrimaryModel]} = $object->primaryKey;
					$link->{$foreignKeys[self::keyLinkedModel]} = $id;
					$link->save();
				}
			}
		}
			// events 
		function onAfterSaveModel( $event ) {
			$object = $event->sender;
			if( !$object->getPrimaryKey()) return;
			$value = $this->getHoldLinksArray( $object );
			if( $value ) {
				$this->saveLinksArray( $object, $value );
			}
		}
			// metods
		function getLinksArray( $object, $parameters = Array()) {
			extract( $this->properties );
			$ids = CommonLib::slice( $object->$linksRelationName, $foreignKeys[self::keyLinkedModel] );
			return $ids;
		}
		function setLinksArray( $object, $parameters = Array()) {
			list( $ids ) = $parameters;
			$object->{$this->callInterfaces[ 'deleteLinks' ]}();
			$object->{$this->callInterfaces[ 'addLinks' ]}( $ids );
		}
		function getLinksString( $object, $parameters = Array()) {
			$ids = $object->{$this->callInterfaces['getLinksArray']}();
			$ids = implode( ',', $ids );
			return $ids;
		}
		function setLinksString( $object, $parameters = Array()) {
			list( $ids ) = $parameters;
			if( is_string( $ids )) $ids = CommonLib::explode( $ids );
			$object->{$this->callInterfaces['setLinksArray']}( $ids );			
		}
		function deleteLinks( $object, $parameters = Array()) {
			extract( $this->properties );
			foreach( $object->$linksRelationName as $link ) {
				$link->delete();
			}
		}
		function addLinks( $object, $parameters = Array()) {
			list( $ids ) = $parameters;
			
			$this->addHoldLinksArray( $object, $ids );
			$object->detachEventHandler( 'onAfterSave', Array( $this, 'onAfterSaveModel' ));
			$object->attachEventHandler( 'onAfterSave', Array( $this, 'onAfterSaveModel' ));
		}
	}

?>