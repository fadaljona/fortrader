<?php
	Yii::import( 'models.base.ModelBase' );
	
	final class CurrencyRatesConvertHistoryModel extends ModelBase {
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'convert' => Array( self::HAS_ONE, 'CurrencyRatesConvertModel', array( 'id' => 'idConvert' ) ),
			);

		}
		function rules() {
			return Array(
				Array( 'idConvert', 'numerical', 'integerOnly'=>true , 'min' => 1),
			);
		}
		static function instance( $idConvert ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idConvert' ));
		}
		static function saveInHistory( $idConvert ){
			$model = self::instance( $idConvert );
			$model->ip = Yii::app()->request->userHostAddress;
			if( $model->validate() ) $model->save();
		}
		static function getLastLogs( $type ){
			return self::model()->findAll(array(
				'with' => array('convert'),
				'condition' => ' `convert`.`type` = :type ',
				'order' => ' `t`.`createdDT` DESC ',
				'limit' => 10,
				'params' => array( ':type' => $type )
			));
		}

	}
?>