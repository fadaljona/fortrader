<?
	Yii::import( 'models.base.SettingsModelBase' );
	
	final class CalendarEventSettingsModel extends SettingsModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) {
					$model = new self();
					$model->id = 1;
				}
			}
			return $model;
		}
		
		public function getTitle(){ return $this->getTextSetting('title'); }
		public function getBrokerId(){ return $this->getTextSetting('brokerId'); }
		public function getMetaTitle(){ return $this->getTextSetting('metaTitle'); }
		public function getMetaDesc(){ return $this->getTextSetting('metaDesc'); }
		public function getMetaKeys(){ return $this->getTextSetting('metaKeys'); }
		public function getFirstDesc(){ return $this->getTextSetting('firstDesc'); }
		public function getSecondDesc(){ return $this->getTextSetting('secondDesc'); }
		
		public function getOgImageSrc(){
			return CommonLib::getCommonImageSrc($this->ogImage);
		}

	}
?>