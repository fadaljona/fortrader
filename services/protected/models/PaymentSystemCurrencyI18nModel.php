<?php

Yii::import('models.base.ModelBase');

final class PaymentSystemCurrencyI18nModel extends ModelBase
{
    static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    function tableName()
    {
        return "{{payment_system_currency_i18n}}";
    }

    static function instance($idCurrency, $idLanguage, $title)
    {
        return self::modelFromAssoc(__CLASS__, compact('idCurrency', 'idLanguage', 'title'));
    }
}
