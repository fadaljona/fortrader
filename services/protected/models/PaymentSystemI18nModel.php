<?php

Yii::import('models.base.ModelBase');

final class PaymentSystemI18nModel extends ModelBase
{
    static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    function tableName()
    {
        return "{{payment_system_i18n}}";
    }

    static function instance($idSystem, $idLanguage, $title, $metaTitle, $metaDesc, $metaKeys, $shortDesc, $fullDesc)
    {
        return self::modelFromAssoc(__CLASS__, compact('idSystem', 'idLanguage', 'title', 'metaTitle', 'metaDesc', 'metaKeys', 'shortDesc', 'fullDesc'));
    }
}
