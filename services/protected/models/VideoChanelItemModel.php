<?php
Yii::import('models.base.ModelBase');

final class VideoChanelItemModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function relations()
    {
        return array(
            'i18ns' => array( self::HAS_MANY, 'VideoChanelItemI18NModel', 'idItem', 'alias' => 'i18nsVideoChanelItem'),
            'chanel' => array( self::HAS_ONE, 'VideoChanelModel', array('id' => 'idChanel')),
            'currentLanguageI18N' => array( self::HAS_ONE, 'VideoChanelItemI18NModel', 'idItem',
                'alias' => 'cLI18NVideoChanelItem',
                'on' => '`cLI18NVideoChanelItem`.`idLanguage` = :idLanguage',
                'params' => array(
                    ':idLanguage' => LanguageModel::getCurrentLanguageID(),
                ),
            ),
        );
    }

    public static function getListForFilter()
    {
        $models = self::model()->findAll(array( ));
        $returnArr = array();
        foreach ($models as $model) {
            $returnArr[$model->id] = $model->title;
        }
        return $returnArr;
    }

    public static function getAdminListDp($filterModel = false)
    {
        $condition = '';
        $params = array();
        if (Yii::app()->request->getParam('idChanel')) {
            $condition = " `t`.`idChanel` = :idChanel ";
            $params['idChanel'] = Yii::app()->request->getParam('idChanel');
        }
        $DP = new CActiveDataProvider(get_called_class(), array(
            'criteria' => array(
                'with' => array( 'currentLanguageI18N', 'i18ns' ),
                'order' => ' `t`.`id` DESC ',
                'condition' => $condition,
                'params' => $params
            ),
        ));
        return $DP;
    }

    public function getI18N()
    {
        if ($this->currentLanguageI18N) {
            return $this->currentLanguageI18N;
        }
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == 0) {
                if (strlen($i18n->title)) {
                    return $i18n;
                }
                break;
            }
        }
        foreach ($this->i18ns as $i18n) {
            if (strlen($i18n->title)) {
                return $i18n;
            }
        }
    }
        
    public function getTitle()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->title : '';
    }

    public function getUrl()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->url : '';
    }
        
        
            # i18ns
    public function deleteI18Ns()
    {
        foreach ($this->i18ns as $i18n) {
            $i18n->delete();
        }
    }
        
    public function addI18Ns($i18ns)
    {
        $idsLanguages = LanguageModel::getExistsIDS();
        foreach ($i18ns as $idLanguage => $obj) {
            if ((strlen($obj->title) || strlen($obj->url)) && in_array($idLanguage, $idsLanguages)) {
                $i18n = VideoChanelItemI18NModel::model()->findByAttributes(array( 'idItem' => $this->id, 'idLanguage' => $idLanguage ));
                if (!$i18n) {
                    $i18n = VideoChanelItemI18NModel::instance($this->id, $idLanguage, $obj->title, $obj->url);
                    $i18n->save();
                } else {
                    $i18n->title = $obj->title;
                    $i18n->url = $obj->url;
                    $i18n->save();
                }
            }
        }
    }
    public function setI18Ns($i18ns)
    {
        $this->deleteI18Ns();
        $this->addI18Ns($i18ns);
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        $this->deleteI18Ns();
    }
}
