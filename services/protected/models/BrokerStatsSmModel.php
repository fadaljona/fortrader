<?
	Yii::import( 'models.base.ModelBase' );

	final class BrokerStatsSmModel extends ModelBase {
		public $placeBusyDays;
		public $originPlace;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idBroker, $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker' ));
		}
		static function getModel( $id ){
			$model = self::model()->findByAttributes( Array( 'idBroker' => $id ));
			if( !$model ) $model = self::instance( $id );
			return $model;
		}
		static function saveNewRating( $newRating ){
			if( !$newRating || !count( $newRating ) ) return false;
			
			foreach( $newRating as $id => $place ){
				$model = self::getModel( $id );
				if( $model->place != $place ){
					$model->oldPlace = $model->place;
					$model->place = $place;
					if( !$model->oldPlace ) $model->oldPlace = $model->place;
					$model->changePlaceDT = date( 'Y-m-d H:i:s', time() );
					if( $model->validate() ) $model->save();
				}
			}
		}

	}

?>