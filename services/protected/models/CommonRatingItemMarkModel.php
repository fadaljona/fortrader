<?php
	Yii::import( 'models.base.ModelBase' );
	
	final class CommonRatingItemMarkModel extends ModelBase {
	
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idModel, $modelName, $idUser, $value ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idModel', 'modelName', 'idUser', 'value' ));
		}
		
	}

?>