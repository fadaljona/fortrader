<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CategoryRatingItemDataModel extends ModelBase {
	
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function instance( $idPost, $title, $inRating ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idPost', 'title', 'inRating' ));
		}
		
	}

?>