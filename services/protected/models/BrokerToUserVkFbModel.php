<?
	Yii::import( 'models.base.ModelBase' );

	final class BrokerToUserVkFbModel extends ModelBase {
		public $rating;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idBroker, $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker', 'idUser' ));
		}
		static function getModel( $idBroker, $idUser ){
			$model = self::model()->findByAttributes( Array( 'idBroker' => $idBroker, 'idUser' => $idUser ));
			if( !$model ) $model = self::instance( $idBroker, $idUser );
			return $model;
		}
		static function setupAllBrokersForFirstUser(){
			$brokers = BrokerModel::model()->findAll(Array(
				'select' => array( " `t`.`id` " ),
				'condition' => " `t`.`showInRating` = 1 ",
			));
			foreach( $brokers as $broker ){
				$model = self::getModel( $broker->id, 1 );
				if( $model->validate() ) $model->save();
			}
		}
		static function getNewRating( $type ){
			$models = self::model()->findAll(Array(
				'select' => array( " `t`.`idBroker` ", " sum( `t`.`vk` ) + sum( `t`.`fb` ) as rating " ),
				'group' => " `t`.idBroker ",
				'with' => array( 'broker' ),
				'order' => " rating DESC ",
				'condition' => " `broker`.`showInRating` = 1 AND `broker`.`type` = :type ",
				'params' => array( ':type' => $type )
			));
			if( !count( $models ) ) return false;
			$outArr = array();
			$i=1;
			$prevRating = $models[0]->rating;
			foreach( $models as $model ){
				if( $prevRating == $model->rating ){
					$outArr[$model->idBroker] = $i;
				}else{
					$prevRating = $model->rating;
					$outArr[$model->idBroker] = $i+1;
					$i++;
				}
			}
			return $outArr;
		}
		function relations() {
			return Array(
				'broker' => Array( self::HAS_ONE, 'BrokerModel', array( 'id' => 'idBroker' ) ),
			);
		}
	}

?>