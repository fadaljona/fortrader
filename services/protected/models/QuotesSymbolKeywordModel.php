<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesSymbolKeywordModel extends ModelBase {
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function setFormKeywords( $symbolId, $keywords ){
			
			$keysArr = array();
			$oldKeys = Yii::app()->db->createCommand()
				->select('keyword, idLanguage')
				->from( '{{quotes_symbol_keyword}}' )
				->where( 'symbolId = :symbolId', array(':symbolId' => $symbolId) )
				->queryAll(); 
			foreach($oldKeys as $oldKey){
				$keysArr[$oldKey['idLanguage']][] = $oldKey['keyword'];
			}
			
			foreach( $keywords as $langId => $keywordsLang ){
				
				if( !isset( $keysArr[$langId] ) ) $keysArr[$langId] = array();
				
				$keysToCreate = array_diff( $keywords[$langId], $keysArr[$langId] );
				$keysToDelete = array_diff( $keysArr[$langId], $keywords[$langId] );
				
				if( count($keysToCreate) ){
					foreach( $keysToCreate as $key ){
						if( $key ){
							$model = new QuotesSymbolKeywordModel;
							$model->symbolId = $symbolId;
							$model->keyword = $key;
							$model->idLanguage = $langId;
							if( $model->validate() )$model->save();
						}
					}
				}
				
				if( count($keysToDelete) ){
					foreach( $keysToDelete as $key ){
						if( $key ){
							
							self::model()->find(Array( 
								'condition' => " `t`.`symbolId` = :id AND `t`.`keyword` = :keyword AND `t`.`idLanguage` = :idLanguage ", 
								'params' => array(':id' => $symbolId, 'keyword' => $key, ':idLanguage' => $langId ) )
							)->delete();
							
						}
					}
				}
			}
			
		}
		static function getFormKeywords( $symbolId ){
			if( !$symbolId ) return '';
			$models = self::model()->findAll(Array(
				'select' => array('keyword', 'idLanguage'),
				'condition' => " `t`.`symbolId` = :id ",
				'params' => array(':id' => $symbolId ),
			));
			$returnArr = array();

			foreach( $models as $model ){
				if( isset( $returnArr[$model->idLanguage] ) && $returnArr[$model->idLanguage] ){
					$returnArr[$model->idLanguage] .= ',' . $model->keyword;
				}else{
					$returnArr[$model->idLanguage] = $model->keyword;
				}	
			}
			return $returnArr;
		}
		protected function beforeDelete() {
			$sql = "
				SELECT `TMP`.`postId` FROM (
					SELECT `QKP`.`keywordId` `keywordId`, `QKP`.`postId` `postId` FROM `{{quotes_keyword_posts}}` `QKP` 
					LEFT JOIN `{{quotes_symbol_keyword}}` `QSK` ON `QSK`.`id` = `QKP`.`keywordId`
					WHERE `QSK`.`symbolId` = :symbolId
					GROUP BY `QKP`.`postId`
					HAVING COUNT(`QKP`.`id`) = 1
					) `TMP`
				WHERE `TMP`.`keywordId` = :keywordId
			";
			
			$command = Yii::App()->db->createCommand( $sql );
			$command->bindValue( ':symbolId', $this->symbolId, PDO::PARAM_INT);
			$command->bindValue( ':keywordId', $this->id, PDO::PARAM_INT);
			$data = $command->queryAll();
			
			$symbol = QuotesSymbolsModel::model()->findByPk($this->symbolId);
			
			CommonLib::loadWp();
			
			$term_info = Yii::app()->db->createCommand()
				->select('term_taxonomy_id')
				->from('wp_term_taxonomy')
				->where( ' term_id = :term_id AND taxonomy = "quotes" ', array( ':term_id' => $symbol->wpTerm ) )
				->queryRow();

			$tt_ids[] = $term_info['term_taxonomy_id'];
			
			foreach( $data as $postId ){
				Yii::app()->db->createCommand()->delete(
					'wp_term_relationships', 
					'object_id = :object_id AND term_taxonomy_id = :taxId', 
					array(':object_id' => $postId['postId'], ':taxId' => $term_info['term_taxonomy_id'])
				);
			}
			
			wp_update_term_count( $tt_ids, 'quotes' );
				
			QuotesKeywordPostsModel::model()->deleteAll(
				" keywordId = :id ", 
				array(':id' => $this->id ) 
			);
			
			return parent::beforeDelete();
		}
		
		function rules() {
			return Array(
				Array( 'symbolId, keyword', 'required' ),
				Array( 'symbolId', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'firstSearch', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 1 ),
				Array( 'symbolId', 'unique', 'criteria'=>array(
					'condition'=>'`keyword`=:secondKey',
					'params'=>array(
						':secondKey'=>$this->keyword
					)
				)),
				Array( 'keyword', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		public function relations(){
			return array(
				'symbol' => array( self::HAS_ONE, 'QuotesSymbolsModel', array('id'=>'symbolId') ),
			);
		}
		public function tableName(){
			return '{{quotes_symbol_keyword}}';
		}
	}

?>