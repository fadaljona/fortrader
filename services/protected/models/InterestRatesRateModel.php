<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InterestRatesRateModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'InterestRatesRateI18nModel', 'idRate', 'alias' => 'i18nsInterestRatesRate' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'InterestRatesRateI18nModel', 'idRate', 
					'alias' => 'cLI18NInterestRatesRateModel',
					'on' => '`cLI18NInterestRatesRateModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'bank' => Array( self::HAS_ONE, 'InterestRatesBankModel', array( 'id' => 'idBank' ) ),
			);
		}

		function getLastValue(){
			$cacheKey = 'InterestRatesValueLastVal' . $this->id;
			if( Yii::app()->cache->get($cacheKey) ) return Yii::app()->cache->get($cacheKey);
			
			$model = InterestRatesValueModel::model()->find(array(
				'condition' => " `t`.`idRate` = :idRate ",
				'order' => '`t`.`modDate` DESC', 
				'params' => array( ':idRate' => $this->id )
			));
			
			Yii::app()->cache->set($cacheKey, $model, 60*60*24);
			
			return $model;
		}
		function getChartValues(){
			$cacheKey = 'InterestRatesValueChartValues' . $this->id;
			if( Yii::app()->cache->get($cacheKey) ) return Yii::app()->cache->get($cacheKey);
			
			$settings = InterestRatesSettingsModel::getModel();
			
			$models = InterestRatesValueModel::model()->findAll(array(
				'condition' => " `t`.`idRate` = :idRate AND `t`.`modDate` >= :modDate",
				'order' => '`t`.`modDate` DESC', 
				'params' => array( ':idRate' => $this->id, ':modDate' => date('Y-m-d', time() - 60*60*24*365* $settings->lastYearsToShowOnChart ) )
			));
			
			Yii::app()->cache->set($cacheKey, $models, 60*60*24);
			
			return $models;
		}
		static function getAdminListDp( $filterModel = false ){
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns', 'bank' ),
					'order' => ' `bank`.`order` DESC ',
				),
			));
			return $DP;
		}
		static function getRatesList(){
			$models = self::model()->findAll(array(
				'with' => array('bank'),
			));
			$retArr = array();
			//$retArr[ "" ] = Yii::t( $NSi18n, 'Select...' );
			foreach( $models as $model ){
				$retArr[$model->id] = $model->title . ' - ' . $model->bank->title . ' - ' . Yii::t( CountryModel::getModelNSi18n("CountryModel"), $model->bank->country->name); 
			}
			return $retArr;
		}

		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->title )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->title )) return $i18n;
			}
		}
		function getTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->title : '';
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
		
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->title )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = InterestRatesRateI18nModel::model()->findByAttributes( Array( 'idRate' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = InterestRatesRateI18nModel::instance( $this->id, $idLanguage, $obj->title );
					}else{
						$i18n->title = $obj->title;
					}
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			
			$cacheKey = 'InterestRatesValueLastVal' . $this->id;
			Yii::app()->cache->delete( $cacheKey );
			
			$cacheKey = 'InterestRatesValueChartValues' . $this->id;
			Yii::app()->cache->delete( $cacheKey );
			
		}
	}

?>