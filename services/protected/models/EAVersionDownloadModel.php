<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAVersionDownloadModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
    
		function tableName() {
			return "{{ea_version_download}}";
		}
    
		function relations() {
			return Array(
				'EA' => Array( self::BELONGS_TO, 'EAVersion', 'idEAVersion' ),
			);
		}

    static function downloaded($idEaVersion, $idUser) {
			$model = self::model();
      $model->idEAVersion = $idEaVersion;
      $model->idUser = $idUser;
      $model->downloadedDT = date('Y-m-d H:i:s');
      $model->save();
		}
    
    static function getDownloadCount($idEa, $from = null, $to = null) {
      return (int)Yii::app()->db->createCommand()
        ->select('COUNT(*)')
        ->from('{{ea_version_download}} ead')
        ->join('{{ea_version}} eav', 'eav.id = ead.idEAVersion')
        ->join('{{ea}} ea', 'ea.id = eav.idEA')
        ->where("ea.id = {$idEa} " . (!empty($from) && !empty($to) ? " AND `ead`.`downloadedDT` BETWEEN '{$from}' AND '{$to}'" : ''))
        ->group('ead.id')
        ->queryScalar();
    }
			
		
	}

?>