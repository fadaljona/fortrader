<?php

Yii::import('models.base.ModelBase');

final class PaymentSystemCurrencyAliasModel extends ModelBase
{
    const ALL_ALIASES_ARR = 'PaymentSystemCurrencyAliasModel.ALL_ALIASES_ARR';

    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function relations()
    {
        return array(
            'currency' => array( self::HAS_ONE, 'PaymentSystemCurrencyModel', array('id' => 'idCurrency'))
        );
    }

    public static function getAllAliases()
    {
        if ($outArr = Yii::app()->cache->get(self::ALL_ALIASES_ARR)) {
            return $outArr;
        }
        $outArr = array();
        $models = self::model()->findAll(array(
            'with' => array('currency'),
            'condition' => " `currency`.`id` IS NOT NULL "
        ));
        foreach ($models as $model) {
            $outArr[$model->alias] = $model->idCurrency;
        }
        Yii::app()->cache->set(self::ALL_ALIASES_ARR, $outArr, 60*1);
        return $outArr;
    }

    public static function getFormAliases($currencyId)
    {
        if (!$currencyId) {
            return false;
        }
        $models = self::model()->findAll(array(
            'select' => array('alias'),
            'condition' => " `t`.`idCurrency` = :id ",
            'params' => array(':id' => $currencyId ),
        ));
        $returnStr = '';
        $i=0;
        foreach ($models as $model) {
            if ($i) {
                $returnStr .= ',';
            }
            $returnStr .= $model->alias;
            $i++;
        }
        return $returnStr;
    }

    public static function setFormAliases($currencyId, $aliases)
    {
        if (!$aliases) {
            $aliases = array();
        }
        
        $aliasArr = array();
        $oldAliases = Yii::app()->db->createCommand()
            ->select('alias')
            ->from('{{payment_system_currency_alias}}')
            ->where('idCurrency = :idCurrency', array(':idCurrency' => $currencyId))
            ->queryAll();

        foreach ($oldAliases as $alias) {
            $aliasArr[] = $alias['alias'];
        }
        
        $aliasesToCreate = array_diff($aliases, $aliasArr);
        $aliasesToDelete = array_diff($aliasArr, $aliases);
        
        if (count($aliasesToCreate)) {
            foreach ($aliasesToCreate as $alias) {
                if ($alias) {
                    $model = new self;
                    $model->idCurrency = $currencyId;
                    $model->alias = trim(mb_strtolower($alias));
                    if ($model->validate()) {
                        $model->save();
                    }
                }
            }
        }
        
        if (count($aliasesToDelete)) {
            foreach ($aliasesToDelete as $alias) {
                if ($alias) {
                    self::model()->find(array(
                        'condition' => " `t`.`idCurrency` = :id AND `t`.`alias` = :alias ",
                        'params' => array(':id' => $currencyId, 'alias' => $alias )
                    ))->delete();
                }
            }
        }
        
    }
}
