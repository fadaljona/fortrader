<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesSymbolsI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'symbol' => Array( self::HAS_ONE, 'QuotesSymbolsModel', array( 'id' => 'idSymbol' ) ),
			);
		}
		function tableName() {
			return "{{quotes_symbols_i18n}}";
		}
		static function instance( $idSymbol, $idLanguage, $nalias, $desc, $default, $visible, $toChartList, $title, $metaTitle, $metaDescription, $tooltip, $preview, $toInformer, $chartTitle, $wpPostsTitle, $metaKeywords, $chartLabel, $searchPhrases, $weightInCat, $informerName ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idSymbol', 'idLanguage', 'nalias', 'desc', 'default', 'visible', 'toChartList', 'title', 'metaTitle', 'metaDescription', 'tooltip', 'preview', 'toInformer', 'chartTitle', 'wpPostsTitle', 'metaKeywords', 'chartLabel', 'searchPhrases', 'weightInCat', 'informerName' ));
		}
	}
?>
