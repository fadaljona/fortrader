<?php
Yii::import('models.base.ModelBase');

final class FaqModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function relations()
    {
        return array(
            'i18ns' => array( self::HAS_MANY, 'FaqI18NModel', 'idFaq', 'alias' => 'i18nsFaq' ),
            'currentLanguageI18N' => array( self::HAS_ONE, 'FaqI18NModel', 'idFaq', 
                'alias' => 'cLI18NFaq',
                'on' => '`cLI18NFaq`.`idLanguage` = :idLanguage',
                'params' => array(
                    ':idLanguage' => LanguageModel::getCurrentLanguageID(),
                ),
            ),
        );
    }

    public static function getListForFilter()
    {
        $models = self::model()->findAll(array( ));
        $returnArr = array();
        foreach ($models as $model) {
            $returnArr[$model->id] = $model->title;
        }
        return $returnArr;
    }

    public static function getAdminListDp($filterModel = false)
    {
        $DP = new CActiveDataProvider(get_called_class(), array(
            'criteria' => array(
                'order' => ' `t`.`id` DESC ',
            ),
        ));
        return $DP;
    }

    public function getI18N()
    {
        if ($this->currentLanguageI18N) {
            return $this->currentLanguageI18N;
        }
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == 0) {
                if (strlen($i18n->title)) {
                    return $i18n;
                }
                break;
            }
        }
        foreach ($this->i18ns as $i18n) {
            if (strlen($i18n->title)) {
                return $i18n;
            }
        }
    }
        
    public function getTitle()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->title : '';
    }
        
        
            # i18ns
    public function deleteI18Ns()
    {
        foreach ($this->i18ns as $i18n) {
            $i18n->delete();
        }
    }
        
    public function addI18Ns($i18ns)
    {
        $idsLanguages = LanguageModel::getExistsIDS();
        foreach ($i18ns as $idLanguage => $obj) {
            if (strlen($obj->title) && in_array($idLanguage, $idsLanguages)) {
                $i18n = FaqI18NModel::model()->findByAttributes(array( 'idFaq' => $this->id, 'idLanguage' => $idLanguage ));
                if (!$i18n) {
                    $i18n = FaqI18NModel::instance($this->id, $idLanguage, $obj->title);
                    $i18n->save();
                } else {
                    $i18n->title = $obj->title;
                    $i18n->save();
                }
            }
        }
    }
    public function setI18Ns($i18ns)
    {
        $this->deleteI18Ns();
        $this->addI18Ns($i18ns);
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        $this->deleteI18Ns();
    }
}
