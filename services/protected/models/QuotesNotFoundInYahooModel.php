<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesNotFoundInYahooModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getSimbolsArray(){
			$models = self::model()->findAll(array(
				'select' => array( " `t`.`symbol` " ),
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = $model->symbol;
			}
			return $outArr;
		}
		static function saveNotFoundSymbol( $symbol ){
			$model = self::model()->find(array(
				'condition' => " `t`.`symbol` = :symbol ",
				'params' => array( ':symbol' => $symbol )
			));
			if( $model ) return false;
			
			$model = new self;
			$model->symbol = $symbol;
			$model->createdDate = date( 'Y-m-d H:i:s' );
			if( $model->validate() )
				if( $model->save() ) return true;
			return false;
		}
		function rules() {
			return Array(
				Array( 'createdDate', 'date', 'format'=>'yyyy-M-d H:m:s'),
				Array( 'symbol', 'required' ),
				Array( 'symbol', 'length', 'max' => 255),
			);
		}
	}
?>