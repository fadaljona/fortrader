<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserMessageModel extends ModelBase {
		const idAdmin = 1;
		const TTLControl = 600;
		const ID_QUESTION_LIST_CONTESTS = 1;
		const ID_QUESTION_LIST_BROKERS = 2;
		const ID_QUESTION_LIST_EA = 3;
		const ID_QUESTION_LIST_JOURNALS = 4;
		const ID_QUESTION_LIST_CALENDAR_EVENTS = 5;
		private $idsUsersNoticed = Array();
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'messageQuery' => Array( self::BELONGS_TO, 'UserMessageModel', 'idMessageQuery' ),
			);
		}
		function checkAccess() {
			if( Yii::App()->user->isGuest ) return false;
			if( $this->isNewRecord ) return true;
			if( Yii::App()->user->checkAccess( 'userMessageControl' )) return true;
			if( Yii::App()->user->id == $this->idUser ) {
				$now = time();
				$created = strtotime( $this->createdDT );
				if( $now - $created <= self::TTLControl ) {
					return true;
				}
			}
			return false;
		}
		function isAnswer() {
			if( Yii::App()->user->isGuest ) return false;
			if( !$this->idMessageQuery ) return false;
			if( $this->messageQuery->idUser == Yii::App()->user->id ) return true;
		}
		function getHTMLText( $maxLen = null ) {
			$text = htmlspecialchars( $this->text );
			$text = CommonLib::nl2br( $text );
			if( $maxLen ) $text = CommonLib::shorten( $text, $maxLen );
			return $text;
		}
		function getInstanceURL() {
			switch( $this->instance ) {
				
				case 'ea/list':              
					return EAModel::getListURL();
				case 'ea/laboratory':        
					return EAModel::getListLaboratoryURL();
				case 'ea/single':            
					return EAModel::getModelSingleURL( $this->idLinkedObj );
				case 'eaVersion/single':     
					return EAVersionModel::getModelSingleURL( $this->idLinkedObj );
				case 'eaStatement/single':   
					return EAStatementModel::getModelSingleURL( $this->idLinkedObj );
				case 'eaTradeAccount/single':
					return EATradeAccountModel::getModelSingleURL( $this->idLinkedObj );
				case 'journal/single':       
					return JournalModel::getModelSingleURL( $this->idLinkedObj );
				case 'question/list':        
					return $this->idLinkedObj ? Yii::App()->createURL( 'question/list', Array( 'id' => $this->idLinkedObj )) : Yii::App()->createURL( 'question/list' );
				case 'calendarEvent/list':   
					return CalendarEventModel::getListURL();
				case 'calendarEvent/single': 
					return CalendarEventModel::getModelSingleURL( $this->idLinkedObj );
				case 'user/profile':         
					return UserModel::getModelProfileURL( $this->idLinkedObj );
			}
		}
		private function stackNotice( $idUser ) {
			if( in_array( $idUser, $this->idsUsersNoticed )) return false;
			$this->idsUsersNoticed[] = $idUser;
			return true;
		}
		private function noticeAdmin() {
			if( $this->instance != 'question/list' ) return false;
			if( !$this->stackNotice( self::idAdmin )) return false;
			
			UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_CREATE_USER_MESSAGE, Array(
				'idUser' => self::idAdmin,
				'idMessage' => $this->id,
				'link' => $this->getInstanceURL(),
				'type' => 'question',
			));
		}
		private function noticeContestMember() {
			if( $this->instance != 'contestMember/single' ) return false;
			
			$member = ContestMemberModel::model()->findByPk( $this->idLinkedObj );
			if( !$member ) return false;
			$idUser = $member->idUser;
			if( $idUser == $this->idUser ) return false;
			
			if( !$this->stackNotice( $idUser )) return false;
			
			UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_CREATE_USER_MESSAGE, Array(
				'idUser' => $idUser,
				'idMessage' => $this->id,
				'link' => $this->getInstanceURL(),
				'type' => 'comment',
			));
		}
		private function noticeUser() {
			if( $this->instance != 'user/profile' ) return false;
			if( $this->idLinkedObj == $this->idUser ) return false;
			
			$user = UserModel::model()->findByPk( $this->idLinkedObj );
			if( !$this->stackNotice( $user->id )) return false;
			
			UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_CREATE_USER_MESSAGE, Array(
				'idUser' => $user->id,
				'idMessage' => $this->id,
				'link' => $this->getInstanceURL(),
				'type' => 'public_message',
			));
		}
		private function noticeQueryUser() {
			if( !$this->idMessageQuery ) return false;
			
			$messageQuery = UserMessageModel::model()->findByPk( $this->idMessageQuery );
			if( !$messageQuery ) return false;
			$idUser = $messageQuery->idUser;
			if( $idUser == $this->idUser ) return false;
			
			$user = UserModel::model()->findByPk( $idUser );
			$settings = $user->getSettings();
			if( !$settings->receiveNoticeAnswerMessage ) return false;
			
			if( !$this->stackNotice( $idUser )) return false;
			
			UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_CREATE_USER_MESSAGE, Array(
				'idUser' => $idUser,
				'idMessage' => $this->id,
				'link' => $this->getInstanceURL(),
				'type' => 'answer',
			));
		}
		private function noticeDialogUsers() {
			$messages = UserMessageModel::model()->findAll( Array(
				'select' => " idUser ",
				'with' => Array(
					'user' => Array(
						'select' => " ID ",
						'with' => Array(
							'settings' => Array(),
						),
					),
				),
				'condition' => " 
					`t`.`instance` = :instance 
					AND IF( :idLinkedObj IS NULL, `t`.`idLinkedObj` IS NULL, `t`.`idLinkedObj` = :idLinkedObj )
				",
				'params' => Array(
					':instance' => $this->instance,
					':idLinkedObj' => $this->idLinkedObj,
				),
				'group' => " `t`.`idUser` ",
			));
			foreach( $messages as $message ) {
				if( $message->idUser == $this->idUser ) continue;
				$settings = $message->user->getSettings();
				if( !$settings->receiveNoticeMessage ) continue;
				
				if( !$this->stackNotice( $message->idUser )) return false;
				
				UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_CREATE_USER_MESSAGE, Array(
					'idUser' => $message->idUser,
					'idMessage' => $this->id,
					'link' => $this->getInstanceURL(),
					'type' => 'comment',
				));
			}
		}
		
			# events
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				$this->noticeAdmin();
				$this->noticeContestMember();
				$this->noticeUser();
				$this->noticeQueryUser();
				$this->noticeDialogUsers();
				
				if( $this->instance == 'ea/single' ) {
					UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA_REVIEW, Array( 'idUser' => $this->idUser, 'idEA' => $this->idLinkedObj ));
				}
			
			}
		}
		protected function afterDelete() {
			parent::afterDelete();
			if( $this->instance == 'broker/single' ) {
				BrokerStats2Model::dec( $this->idLinkedObj, "countReviews" );
			}
		}
	}

?>