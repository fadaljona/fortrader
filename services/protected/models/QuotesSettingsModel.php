<?php
Yii::import( 'models.base.SettingsModelBase' );

final class QuotesSettingsModel extends SettingsModelBase{
    
    protected static $instance;
    
    static function model( $class = __CLASS__ ) {
        return parent::model( $class );
    }
    static function getModel() {
        static $model;
        if( !$model ) {
            $model = self::model()->find();
            if( !$model ) {
                $model = new self();
                $model->id = 1;
            }
        }
        return $model;
    }
    
    
    public static function getInstance() {
        if ( is_null(self::$instance) ) {

            self::$instance = self::model()->find();
            if( !self::$instance ) {
                self::$instance = new self();
                self::$instance->id = 1;
            }

        }
        return self::$instance;
    }
    
    static function parseSetting(&$setting){
        $result=array();
        if($setting){
            foreach($setting as $column_name){
                $name=explode("=",$column_name);
                $result[mb_strtolower($name[0],"UTF-8")]=$name[1];
            }
        }

        return $result;
    }
    
    
    public function getColumns_name(){
        if( !$this->texts || !$this->texts->columns_name ) return false;
        return $this->texts->columns_name;
    }
    public function getColumnsShort(){
        if( !$this->texts || !$this->texts->columnsShort ) return false;
        return $this->texts->columnsShort;
    }
    public function getColumnsShort_name(){
        if( !$this->texts || !$this->texts->columnsShort_name ) return false;
        return $this->texts->columnsShort_name;
    }
    public function getQuotesListTitle(){
        if( !$this->texts || !$this->texts->quotesListTitle ) return false;
        return $this->texts->quotesListTitle;
    }
    public function getQuotesListMetaTitle(){
        if( !$this->texts || !$this->texts->quotesListMetaTitle ) return false;
        return $this->texts->quotesListMetaTitle;
    }
    public function getQuotesListDesc(){
        if( !$this->texts || !$this->texts->quotesListDesc ) return false;
        return $this->texts->quotesListDesc;
    }
    public function getQuotesListMetaDesc(){
        if( !$this->texts || !$this->texts->quotesListMetaDesc ) return false;
        return $this->texts->quotesListMetaDesc;
    }
    public function getChartsListTitle(){
        if( !$this->texts || !$this->texts->chartsListTitle ) return false;
        return $this->texts->chartsListTitle;
    }
    public function getChartsListMetaTitle(){
        if( !$this->texts || !$this->texts->chartsListMetaTitle ) return false;
        return $this->texts->chartsListMetaTitle;
    }
    public function getChartsListDesc(){
        if( !$this->texts || !$this->texts->chartsListDesc ) return false;
        return $this->texts->chartsListDesc;
    }
    public function getChartsListMetaDesc(){
        if( !$this->texts || !$this->texts->chartsListMetaDesc ) return false;
        return $this->texts->chartsListMetaDesc;
    }
    public function getChartsListMetaKeys(){
        if( !$this->texts || !$this->texts->chartsListMetaKeys ) return false;
        return $this->texts->chartsListMetaKeys;
    }
    public function getQuotesListMetaKeys(){
        if( !$this->texts || !$this->texts->quotesListMetaKeys ) return false;
        return $this->texts->quotesListMetaKeys;
    }
    public function getQuotesInformerTitle(){
        if( !$this->texts || !$this->texts->quotesInformerTitle ) return false;
        return $this->texts->quotesInformerTitle;
    }
    public function getQuotesInformerMetaTitle(){
        if( !$this->texts || !$this->texts->quotesInformerMetaTitle ) return false;
        return $this->texts->quotesInformerMetaTitle;
    }
    public function getQuotesInformerMetaDesc(){
        if( !$this->texts || !$this->texts->quotesInformerMetaDesc ) return false;
        return $this->texts->quotesInformerMetaDesc;
    }
    public function getQuotesInformerMetaKeys(){
        if( !$this->texts || !$this->texts->quotesInformerMetaKeys ) return false;
        return $this->texts->quotesInformerMetaKeys;
    }
    public function getOtherQuotesListTitle(){
        if( !$this->texts || !$this->texts->otherQuotesListTitle ) return false;
        return $this->texts->otherQuotesListTitle;
    }
    public function getOtherQuotesListMetaTitle(){
        if( !$this->texts || !$this->texts->otherQuotesListMetaTitle ) return false;
        return $this->texts->otherQuotesListMetaTitle;
    }
    public function getOtherQuotesListDesc(){
        if( !$this->texts || !$this->texts->otherQuotesListDesc ) return false;
        return $this->texts->otherQuotesListDesc;
    }
    public function getOtherQuotesListMetaDesc(){
        if( !$this->texts || !$this->texts->otherQuotesListMetaDesc ) return false;
        return $this->texts->otherQuotesListMetaDesc;
    }
    public function getOtherQuotesListMetaKeys(){
        if( !$this->texts || !$this->texts->otherQuotesListMetaKeys ) return false;
        return $this->texts->otherQuotesListMetaKeys;
    }
    public function getQuotesListShortDesc(){
        if( !$this->texts || !$this->texts->quotesListShortDesc ) return false;
        return $this->texts->quotesListShortDesc;
    }
    public function getChartsListShortDesc(){
        if( !$this->texts || !$this->texts->chartsListShortDesc ) return false;
        return $this->texts->chartsListShortDesc;
    }
    public function getTitleTemplate(){
        if( !$this->texts || !$this->texts->titleTemplate ) return false;
        return $this->texts->titleTemplate;
    }
    public function getMetaTitleTemplate(){
        if( !$this->texts || !$this->texts->metaTitleTemplate ) return false;
        return $this->texts->metaTitleTemplate;
    }
    public function getChartTitleTemplate(){
        if( !$this->texts || !$this->texts->chartTitleTemplate ) return false;
        return $this->texts->chartTitleTemplate;
    }
    public function getChartLabelTemplate(){
        if( !$this->texts || !$this->texts->chartLabelTemplate ) return false;
        return $this->texts->chartLabelTemplate;
    }
    public function getWpPostsTitleTemplate(){
        if( !$this->texts || !$this->texts->wpPostsTitleTemplate ) return false;
        return $this->texts->wpPostsTitleTemplate;
    }
    public function getPreviewTemplate(){
        if( !$this->texts || !$this->texts->previewTemplate ) return false;
        return $this->texts->previewTemplate;
    }
    public function getMetaDescriptionTemplate(){
        if( !$this->texts || !$this->texts->metaDescriptionTemplate ) return false;
        return $this->texts->metaDescriptionTemplate;
    }
    public function getMetaKeywordsTemplate(){
        if( !$this->texts || !$this->texts->metaKeywordsTemplate ) return false;
        return $this->texts->metaKeywordsTemplate;
    }
}
