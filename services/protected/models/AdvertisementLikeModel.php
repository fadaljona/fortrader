<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementLikeModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
			);
		}
		static function instance( $idAd, $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idAd', 'idUser' ));
		}
	}

?>