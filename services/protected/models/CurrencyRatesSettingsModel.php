<?
	Yii::import( 'models.base.SettingsModelBase' );
	
	final class CurrencyRatesSettingsModel extends SettingsModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) {
					$model = new self();
					$model->id = 1;
				}
			}
			return $model;
		}
		
		public function getTitle(){ return $this->getTextSetting( 'title' ); }
		public function getDescription(){ return $this->getTextSetting( 'description' ); }
		public function getMetaTitle(){ return $this->getTextSetting( 'metaTitle' ); }
		public function getMetaDesc(){ return $this->getTextSetting( 'metaDesc' ); }
		public function getMetaKeys(){ return $this->getTextSetting( 'metaKeys' ); }
		public function getFullDesc(){ return $this->getTextSetting( 'fullDesc' ); }
		
		public function getEcbTitle(){ return $this->getTextSetting( 'ecbTitle' ); }
		public function getEcbDescription(){ return $this->getTextSetting( 'ecbDescription' ); }
		public function getEcbMetaTitle(){ return $this->getTextSetting( 'ecbMetaTitle' ); }
		public function getEcbMetaDesc(){ return $this->getTextSetting( 'ecbMetaDesc' ); }
		public function getEcbMetaKeys(){ return $this->getTextSetting( 'ecbMetaKeys' ); }
		public function getEcbFullDesc(){ return $this->getTextSetting( 'ecbFullDesc' ); }
		
		public function getTomorrowTitle(){ return $this->getTextSetting( 'tomorrowTitle' ); }
		public function getTomorrowDescription(){ return $this->getTextSetting( 'tomorrowDescription' ); }
		public function getTomorrowMetaTitle(){ return $this->getTextSetting( 'tomorrowMetaTitle' ); }
		public function getTomorrowMetaDesc(){ return $this->getTextSetting( 'tomorrowMetaDesc' ); }
		public function getTomorrowMetaKeys(){ return $this->getTextSetting( 'tomorrowMetaKeys' ); }
		public function getTomorrowFullDesc(){ return $this->getTextSetting( 'tomorrowFullDesc' ); }
		
		public function getConverterTitle(){ return $this->getTextSetting( 'converterTitle' ); }
		public function getConverterDescription(){ return $this->getTextSetting( 'converterDescription' ); }
		public function getConverterMetaTitle(){ return $this->getTextSetting( 'converterMetaTitle' ); }
		public function getConverterMetaDesc(){ return $this->getTextSetting( 'converterMetaDesc' ); }
		public function getConverterMetaKeys(){ return $this->getTextSetting( 'converterMetaKeys' ); }
		public function getConverterFullDesc(){ return $this->getTextSetting( 'converterFullDesc' ); }
		public function getConverterSubTitle1(){ return $this->getTextSetting( 'converterSubTitle1' ); }
		public function getConverterSubTitle2(){ return $this->getTextSetting( 'converterSubTitle2' ); }
		
		public function getEcbConverterTitle(){ return $this->getTextSetting( 'ecbConverterTitle' ); }
		public function getEcbConverterDescription(){ return $this->getTextSetting( 'ecbConverterDescription' ); }
		public function getEcbConverterMetaTitle(){ return $this->getTextSetting( 'ecbConverterMetaTitle' ); }
		public function getEcbConverterMetaDesc(){ return $this->getTextSetting( 'ecbConverterMetaDesc' ); }
		public function getEcbConverterMetaKeys(){ return $this->getTextSetting( 'ecbConverterMetaKeys' ); }
		public function getEcbConverterFullDesc(){ return $this->getTextSetting( 'ecbConverterFullDesc' ); }
		public function getEcbConverterSubTitle1(){ return $this->getTextSetting( 'ecbConverterSubTitle1' ); }
		public function getEcbConverterSubTitle2(){ return $this->getTextSetting( 'ecbConverterSubTitle2' ); }
		
		
		private function replaceParamsAndDate($inpStr, $dateStr, $model){
			$str = str_replace( array( '{$code}', '{$name}' ), array( $model->code, $model->name ), $inpStr );
			return CommonLib::replaceDates( $str, $dateStr );
		}
		private function getSettingNameByDataType($settingName, $type = 'cbr'){
			if( $type == 'cbr' ) return $settingName;
			if( $type == 'ecb' ) return $type . ucfirst($settingName);
			return false;
		}
		private function getTextSettingByDataType( $settingName, $type = 'cbr' ){
			if( !$this->texts ) return false;
			$settingNameByDataType = $this->getSettingNameByDataType($settingName, $type);
			if( !$settingNameByDataType ) return false;
			if( !$this->texts->{$settingNameByDataType} ) return false;
			return $this->texts->{$settingNameByDataType};
		}
		private function getTextSettingByDataTypeAndReplaces( $settingName, $dateStr, $type = 'cbr' ){
			if( !$this->texts ) return false;
			$settingNameByDataType = $this->getSettingNameByDataType($settingName, $type);
			if( !$this->texts->{$settingNameByDataType} ) return false;
			return CommonLib::replaceDates( $this->texts->{$settingNameByDataType}, $dateStr );
		}
		private function getDefaultSingleTextSettingByDataTypeAndReplaces( $settingName, $dateStr, $model, $type = 'cbr' ){
			if( !$this->texts ) return false;
			$settingNameByDataType = $this->getSettingNameByDataType($settingName, $type);
			if( !$this->texts->{$settingNameByDataType} ) return false;
			return $this->replaceParamsAndDate($this->texts->{$settingNameByDataType}, $dateStr, $model);
		}
		
		public function getArchiveTitle($type = 'cbr'){ return $this->getTextSettingByDataType('archiveTitle', $type); }
		public function getArchiveMetaTitle($type = 'cbr'){ return $this->getTextSettingByDataType('archiveMetaTitle', $type); }
		public function getArchiveMetaDesc($type = 'cbr'){ return $this->getTextSettingByDataType('archiveMetaDesc', $type); }
		public function getArchiveMetaKeys($type = 'cbr'){ return $this->getTextSettingByDataType('archiveMetaKeys', $type); }
		public function getArchiveDescription($type = 'cbr'){ return $this->getTextSettingByDataType('archiveDescription', $type); }
		public function getArchiveFullDesc($type = 'cbr'){ return $this->getTextSettingByDataType('archiveFullDesc', $type); }
		
		public function getArchiveYearTitle($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearTitle', $dateStr, $type ); }
		public function getArchiveYearMoTitle($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoTitle', $dateStr, $type ); }
		public function getArchiveYearMoDayTitle($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoDayTitle', $dateStr, $type ); }	
		
		public function getArchiveYearMetaTitle($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMetaTitle', $dateStr, $type ); }
		public function getArchiveYearMoMetaTitle($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoMetaTitle', $dateStr, $type ); }
		public function getArchiveYearMoDayMetaTitle($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoDayMetaTitle', $dateStr, $type ); }
		public function getArchiveYearMetaDesc($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMetaDesc', $dateStr, $type ); }
		public function getArchiveYearMoMetaDesc($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoMetaDesc', $dateStr, $type ); }
		public function getArchiveYearMoDayMetaDesc($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoDayMetaDesc', $dateStr, $type ); }
		public function getArchiveYearMetaKeys($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMetaKeys', $dateStr, $type ); }
		public function getArchiveYearMoMetaKeys($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoMetaKeys', $dateStr, $type ); }
		public function getArchiveYearMoDayMetaKeys($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoDayMetaKeys', $dateStr, $type ); }
		public function getArchiveYearDescription($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearDescription', $dateStr, $type ); }
		public function getArchiveYearMoDescription($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoDescription', $dateStr, $type ); }
		public function getArchiveYearMoDayDescription($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoDayDescription', $dateStr, $type ); }
		public function getArchiveYearFullDesc($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearFullDesc', $dateStr, $type ); }
		public function getArchiveYearMoFullDesc($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoFullDesc', $dateStr, $type ); }
		public function getArchiveYearMoDayFullDesc($dateStr, $type = 'cbr'){ return $this->getTextSettingByDataTypeAndReplaces( 'archiveYearMoDayFullDesc', $dateStr, $type ); }

		public function getDefaultSingleArchiveYearTitle($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearTitle', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMetaTitle($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMetaTitle', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMetaDesc($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMetaDesc', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMetaKeys($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMetaKeys', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearDescription($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearDescription', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearFullDesc($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearFullDesc', $dateStr, $model, $type );
		}	
		public function getDefaultSingleArchiveYearMoTitle($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoTitle', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoMetaTitle($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoMetaTitle', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoMetaDesc($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoMetaDesc', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoMetaKeys($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoMetaKeys', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoDescription($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoDescription', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoFullDesc($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoFullDesc', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoDayTitle($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoDayTitle', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoDayMetaTitle($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoDayMetaTitle', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoDayMetaDesc($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoDayMetaDesc', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoDayMetaKeys($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoDayMetaKeys', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoDayDescription($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoDayDescription', $dateStr, $model, $type );
		}
		public function getDefaultSingleArchiveYearMoDayFullDesc($dateStr, $model, $type = 'cbr'){
			return $this->getDefaultSingleTextSettingByDataTypeAndReplaces( 'defaultSingleArchiveYearMoDayFullDesc', $dateStr, $model, $type );
		}
	}

?>