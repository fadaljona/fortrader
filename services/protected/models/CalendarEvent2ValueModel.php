<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CalendarEvent2ValueModel extends ModelBase {

		public $hasNews;
		public $hasDesc;
		public $indicatorName;
		public $beforeValueName = false;

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{calendar_event2_value}}";
		}
		
		static function instance( $idEvent, $idLanguage, $indicator ) {
			return self::modelFromAssoc( __CLASS__, compact( 'id', 'idLanguage', 'indicator' ));
		}

		function relations() {
			return Array(
				'event' => Array( self::HAS_ONE, 'CalendarEvent2Model', Array( 'indicator_id' => 'indicator_id' )),
			);
		}

		function getRowStatus(){
			$time = strtotime( $this->dt );
			$time -= time();
			if( $time > 0 ) {
				return 'needUpdate';
			}
			return false;
		}
		function getTimeLeftInSeconds(){
			$time = strtotime( $this->dt );
			$time -= time();
			if( $time > 0 ) {
				return $time;
			}
			return 0;
		}
		static function getStaticTimeLeft( $dt ) {
			$time = strtotime( $dt );
			$time -= time();
			
			if( $time < 0 ) {
				return Yii::t( '*', 'Done' );
			}
			
			list( $seconds, $minets, $hours, $days, $months, $years ) = CommonLib::explodeTimeDiff( $time );
			
			if( $seconds == 0 ) {
				return Yii::t( '*', 'Done' );
			}
			elseif( $minets == 0 ) {
				$_sec = Yii::t( '*', 'sec' );
				return "{$seconds} {$_sec}";
			}
			elseif( $hours == 0 ) {
				$_min = Yii::t( '*', 'min' );
				return "{$minets} {$_min}";
			}
			elseif( $days == 0 ) {
				$_hours = Yii::t( '*', 'h' );
				$_min = Yii::t( '*', 'min' );
				$_minets = $minets % 60;
				return "{$hours} {$_hours} {$_minets} {$_min}";
			}
			elseif( $months == 0 ) {
				$_days = Yii::t( '*', 'day|days', $days );
				return "{$days} {$_days}";
			}
			elseif( $years == 0 ) {
				$_months = Yii::t( '*', 'month|months', $months );
				return "{$months} {$_months}";
			}
			else{
				return "{$years} years";
			}
		}
		function getTimeLeft() {
			return self::getStaticTimeLeft( $this->dt );
		}



		function getLinkedCountry() {
			if( !$this->event ) return '';
			if( !$this->event->country ) return '';
			return $this->event->country;
		}


		function getSingleURL() {
			return self::getModelSingleURL( $this->id );
		}

		static function getModelSingleURL( $id ) {
			return Yii::App()->createURL( 'calendarEvent/single', Array( 'id' => $id ));
		}

		function getTitleSerious() {
			return self::getModelTitleSerious( $this->serious );
		}

		static function getModelTitleSerious( $serious ) {
			$titles = self::getTitlesSerious();
			return @$titles[ $serious ];
		}

		static function getTitlesSerious() {
			return Array(
				0 => 'not',
				1 => 'low',
				2 => 'medium',
				3 => 'high',
			);
		}

		public function getBeforeValueHtml() {
			
			if( $this->beforeValueName === false )
				$name = $this->i18ns->before_value_name;
			else
				$name = $this->beforeValueName;
			
			if ( $this->getBeforeClass() ) {
				return '<div>'.$this->before_value.$name.$this->getBeforeTooltip().'</div>';
			}else{
				return '<div>'.$this->before_value.$name.'</div>';
			}
		}

		function getBeforeClass()
		{
			$mb = $this->mb_value;
			$befor = $this->before_value;
			if ( $mb != '' and $befor != '') {
				if ( $befor > $mb) {
					return ' green_bg2 ';
				}elseif($befor < $mb){
					return ' red_bg2 ';
				}
			}
			return false;
		}

		function getBeforeTooltip( ) {
			$sign = '';
			$mb = $this->mb_value;
			$befor = $this->before_value;
			$delta = $befor - $mb;
			$delta1 = number_format( $delta, CommonLib::getDecimalPlaces( $delta ), '.', '' );
			if ( $delta > 0 ) {
				$sign = '+';
			}
			return '<div>('.$sign.$delta1.')</div>';
		}

		function getNumValue($value)
		{
			$value = preg_replace( "#[^0-9\-\.]#", "", $value );
			return $value;
		}

		function getNameValue($value)
		{
			$value = str_replace( $this->getNumValue($value), "", $value );
			return $value;
		}

		function getMbValue() {
			return $this->mb_value;
		}

		function getMbValueSel() {
			return $this->mb_value_sec;
		}

		function getBeforeValue() {
			return $this->before_value;
		}

		function getBeforeValueSel() {
			return $this->before_value_sec;
		}
		
		function getFactValue() {
			return $this->fact_value;
		}
		function getFactValueSel() {
			return $this->fact_value_sec;
		}
		function getFactValueHtml() {
			if( $this->beforeValueName === false )
				$name = $this->i18ns->before_value_name;
			else
				$name = $this->beforeValueName;
			if ( $this->getFactClass() ) {
				return $this->fact_value != '' ? '<div>'.$this->fact_value.$name.$this->getFactTooltip().'</div>' : '';
			}else{
				return $this->fact_value != '' ? '<div>'.$this->fact_value.$name.'</div>' : '';
			}
		}

		function getFactClass()
		{
			$mb = $this->getMbValue();
			$fact = $this->getFactValue();
			if ( $mb != '' and $fact != '') {
				if ( $fact > $mb ) {
					return ' green_bg2 ';
				}elseif($fact < $mb){
					return ' red_bg2 ';
				}
			}
			return false;
		}

		function getFactTooltip( ) {
			$sign = '';
			$mb = $this->getMbValue();
			$fact = $this->getFactValue();
			$delta = $fact - $mb;
			$delta1 = number_format( $delta, CommonLib::getDecimalPlaces( $delta ), '.', '' );;
			if ( $delta > 0 ) {
				$sign = '+';
			}
			return '<div>('.$sign.$delta1.')</div>';
		}

		function getAdminURL() {
			return self::getModelAdminURL( $this->id );
		}

		static function getModelAdminURL( $id ) {
			return Yii::App()->createURL( 'admin/calendarEvent/list', Array( 'idEdit' => $id ));
		}

		function isHaveFirst() {
			$criteria = new CDbCriteria();
			$criteria->condition = 'indicator_id = '.$this->indicator_id;
			$criteria->order = 'dt';
			$data = CalendarEvent2ValueModel::model()->findAll($criteria);

			$s = '';
			
			foreach ($data as $one) {
				$s .= $one->fact_value;
			}
			if ( $s != '' ) {
				return true;
			}
			return false;
		}

		function getTitleFirst()
		{
			$res = $this->i18ns->fact_value_name;
			if ( $res != '' ) {
				return $res;
			}else{
				return $this->getTitleComplFirst();
			}
		}

		function getTitleComplFirst( ) {
			$criteria = new CDbCriteria();
			$criteria->condition = 'indicator_id = '.$this->indicator_id;
			$criteria->order = 'dt';
			$data = CalendarEvent2ValueModel::model()->findAll($criteria);
			foreach ($data as $one) {
				$res = $one->i18ns->fact_value_name;
				if ( $res != '' ) {
					return $res;
				}
			}
			return false;
		}

		function isHaveSel() {
			$criteria = new CDbCriteria();
			$criteria->condition = 'indicator_id = '.$this->indicator_id;
			$criteria->order = 'dt';
			$data = CalendarEvent2ValueModel::model()->findAll($criteria);

			$s1 = '';

			foreach ($data as $one) {
				$s1 .= $one->fact_value_sec;
			}
			if ( $s1 != '' ) {
				return true;
			}
			return false;
		}

		function getTitleSel()
		{
			$res = $this->i18ns->fact_value_sec_name;
			if ( $res != '' ) {
				return $res;
			}else{
				return $this->getTitleComplSel();
			}
		}

		function getTitleComplSel( ) {
			$criteria = new CDbCriteria();
			$criteria->condition = 'indicator_id = '.$this->indicator_id;
			$criteria->order = 'dt';
			$data = CalendarEvent2ValueModel::model()->findAll($criteria);
			foreach ($data as $one) {
				$res = $one->i18ns->fact_value_sec_name;
				if ( $res != '' ) {
					return $res;
				}
			}
			return false;
		}

		function getPeriod() {
			$time = strtotime( $this->dt );
			return date('M-d', $time);
		}

		function getTitleFactValue() {
			$value = $this->getFactValue();
			$len = mb_strlen( $value );
			
			$fint = false;
			for( $i = 0; $i < $len; $i++ ) {
				$char = mb_substr( $value, $i, 1 );
				if( preg_match( "#^[0-9]$#", $char )) {
					$fint = true;
					continue;
				}
				if( $fint and !in_array( $char, Array( " ", ".", "-" ))) {
					return $char;
				}
			}
		}

		function getTimeLeftToBtn() {
			$time = strtotime( $this->dt );
			$time -= time();
			
			if( $time < 0 ) {
				return Yii::t( '*', 'Done' );
			}
			
			list( $seconds, $minets, $hours, $days, $months, $years ) = CommonLib::explodeTimeDiff( $time );
			
			if( $seconds == 0 ) {
				return Yii::t( '*', 'Done' );
			}
			elseif( $minets == 0 ) {
				$_sec = $seconds % 60;
				return "0:0:{$_sec}";
			}
			elseif( $hours == 0 ) {
				$_min = Yii::t( '*', 'min' );
				$_minets = $minets % 60;
				$_sec = $seconds % 60;
				return "0:{$_minets}:{$_sec}";
			}
			elseif( $days == 0 ) {
				$_hours = Yii::t( '*', 'h' );
				$_min = Yii::t( '*', 'min' );
				$_minets = $minets % 60;
				$_sec = $seconds % 60;
				return "{$hours}:{$_minets}:{$_sec}";
			}
			elseif( $months == 0 ) {
				$_days = Yii::t( '*', 'day|days', $days );
				return "{$days} {$_days}";
			}
			elseif( $years == 0 ) {
				$_months = Yii::t( '*', 'month|months', $months );
				return "{$months} {$_months}";
			}
			else{
				return "{$years} years";
			}
		}
	}

?>
