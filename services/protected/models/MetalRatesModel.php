<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MetalRatesModel extends ModelBase {

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'MetalRatesI18nModel', 'idRate', 'alias' => 'i18nsMetalRates' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'MetalRatesI18nModel', 'idRate', 
					'alias' => 'cLI18NMetalRatesModel',
					'on' => '`cLI18NMetalRatesModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				
			);
		}
		
		static function getItemsForInformerByLang($langId){
		
			$models = self::model()->findAll(array(
				'with' => array( 'currentLanguageI18N' ),
				'condition' => " `cLI18NMetalRatesModel`.`title` IS NOT NULL AND `cLI18NMetalRatesModel`.`title` <> '' ",
				'order' => " `t`.`id` ASC ",
			));
			$outArr = array( 
				'type' => 'multipleCheckbox',
			);
			foreach( $models as $model ){
				$outArr['options'][] = array(
					'value' => $model->id,
					'setted' => $model->toInformer,
					'label' => $model->getTitleByLang($langId)
				);
			}
			return $outArr;
		}
		static function getItemsForInformer(){
			$models = self::model()->findAll(array(
				'with' => array( 'currentLanguageI18N' ),
				'condition' => " `cLI18NMetalRatesModel`.`title` IS NOT NULL AND `cLI18NMetalRatesModel`.`title` <> '' ",
				'order' => " `t`.`id` ASC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->id] = array(
					'toInformer' => $model->toInformer,
					'name' => $model->title,
				);
			}
			return $outArr;
		}
		static function getItemsForInformerByIds( $ids ){
			
			$criteria = new CDbCriteria();
			$criteria->with = array( 'currentLanguageI18N' );
			$criteria->condition = " `cLI18NMetalRatesModel`.`title` IS NOT NULL AND `cLI18NMetalRatesModel`.`title` <> '' ";
			$criteria->addInCondition('id', explode(',', $ids ));
			$criteria->order = " `t`.`id` ASC ";
			
			return self::model()->findAll( $criteria );
		}
		public function getName(){
			return $this->code;
		}
		public function getInformerDiff(){
			if( $this->todayCourse && $this->prevDayCourse ){
				return $this->todayCourse - $this->prevDayCourse;
			}
			return 0;
		}
		public function getInformerTooltip(){
			return $this->title;
		}
		public function getInformerItemName(){
			return $this->title;
		}
		public function getInformerShortItemName(){
			return $this->code;
		}
		public function getFaSymbol(){
			return CHtml::tag('span', array('class' => 'symbolCode'), $this->code);
		}
		public function getMetalCourse(){
			return $this->todayCourse;
		}
		public function getFlagSrc(){
			return false;
		} 
		public function getTitleByLang($langId){
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == $langId ) {
					if( strlen( $i18n->title )) return $i18n->title;
					return $this->code;
				}
			}
		}
		
		
		public function getAbsoluteSingleURL(){
			return Yii::app()->createAbsoluteUrl('informers/index');
		}
		public function getPrevDayCourse(){
			if( !$this->prevDayData ) return false;
			return number_format($this->prevDayData->valueBuy, CommonLib::getDecimalPlaces( $this->prevDayData->valueBuy, 4 ), '.', '');
		}
		public function getTodayCourse(){
			if( !$this->todayData ) return false;
			return number_format($this->todayData->valueBuy, CommonLib::getDecimalPlaces( $this->todayData->valueBuy, 4 ), '.', '');
		}
		public function getTodayData(){
			return MetalRatesHistoryModel::model()->find(array(
				'order' => 'date DESC', 
				'condition' => " date <= :date AND idRate = :id  ",
				'params' => array( ':date' => date('Y-m-d'), ':id' => $this->id )
			));
			
		}
		public function getPrevDayData(){
			$models = MetalRatesHistoryModel::model()->findAll(array(
				'order' => 'date DESC', 
				'condition' => " date <= :date AND idRate = :id  ",
				'params' => array( ':date' => date('Y-m-d'), ':id' => $this->id ),
				'limit' => 2
			));
			if( $models && $models[1] ) return $models[1];
			return false;
		}
		static function getAdminListDp( $filterModel = false ){
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`id` ASC ',
				),
			));
			return $DP;
		}	

		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->title )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->title )) return $i18n;
			}
		}

		function getTitle() { $i18n = $this->getI18N(); return $i18n ? $i18n->title : '';}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
		
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->title )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = MetalRatesI18nModel::model()->findByAttributes( Array( 'idRate' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = MetalRatesI18nModel::instance( $this->id, $idLanguage, $obj->title );
					}else{
						$i18n->title = $obj->title;
					}
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			
		}
	}

?>