<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAModel extends ModelBase {
    public $downloaded;
    
		const PATHUploadDir = 'uploads/ea';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea}}";
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'EAI18NModel', 'idEA', 'alias' => 'i18nsEA' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'EAI18NModel', 'idEA', 
					'alias' => 'cLI18NEA',
					'on' => '`cLI18NEA`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'EATradeAccounts' => Array( self::HAS_MANY, 'EATradeAccountModel', 'idEA' ),
				'EAVersions' => Array( self::HAS_MANY, 'EAVersionModel', 'idEA' ),
				'EAStatements' => Array( self::HAS_MANY, 'EAStatementModel', 'idEA' ),
				'tradePlatforms' => Array( self::MANY_MANY, 'TradePlatformModel', '{{ea_to_trade_platform}}(idEA,idPlatform)' ),
				'linksToPlatforms' => Array( self::HAS_MANY, 'EAToTradePlatformModel', 'idEA' ),
				'countMessages' => Array( self::STAT, 'UserMessageModel', 'idLinkedObj', 'condition' => " `instance` = 'ea/single' " ),
				'countVersions' => Array( self::STAT, 'EAVersionModel', 'idEA' ),
				'countDownloads' => Array( self::STAT, 'EAVersionModel', 'idEA', 'select' => " SUM( `downloads` ) " ),
				'stats' => Array( self::HAS_ONE, 'EAStatsModel', 'idEA' ),
			);
		}
		static function getListURL() {
			return Yii::App()->createURL( 'ea/list' );
		}
		static function getListLaboratoryURL() {
			return Yii::App()->createURL( 'ea/laboratory' );
		}
		static function getAddURL() {
			return Yii::App()->createURL( 'ea/add' );
		}
		static function getEditURL( $id ) {
			return Yii::App()->createURL( 'ea/edit', Array( 'id' => $id ));
		}
		static function getModelSingleURL( $id ) {
			return Yii::App()->createURL( 'ea/single', Array( 'id' => $id ));
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->about )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->about )) return $i18n;
			}
		}
		function getAbout() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->about : '';
		}
		
		function viewed() {
			$this->views++;
			$this->update( Array( 'views' ));
		}
		static function updateStats() {
			
			$exists = Yii::App()->db->createCommand("
				SELECT 		1 
				FROM 		`{{ea_stats}}` 
				WHERE 		`updatedDT` IS NULL OR DATE(`updatedDT`) < CURDATE() 
				LIMIT		1
			")->queryScalar();
			
			if( $exists ) {
				$command = file_get_contents( "protected/data/updateEAStats.sql" );
				Yii::App()->db->createCommand( $command )->query();
			}

		}
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->about )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = EAI18NModel::model()->findByAttributes( Array( 'idEA' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = EAI18NModel::instance( $this->id, $idLanguage, $obj->about );
						$i18n->save();
					}
					else{
						$i18n->about = $obj->about;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		
		function getSingleURL() {
			return self::getModelSingleURL( $this->id );
		}
		function getUserMark() {
			$mark = EAMarkModel::model()->findByAttributes( Array( 'idEA' => $this->id, 'idUser' => Yii::App()->user->id ));
			if( !$mark ) $mark = EAMarkModel::instance( $this->id, Yii::App()->user->id );
			return $mark;
		}
			# image
		function getPathImage() {
			return strlen($this->nameImage) ? self::PATHUploadDir."/{$this->nameImage}" : '';
		}
		function getSrcImage() {
			return $this->pathImage ? Yii::app()->baseUrl."/{$this->pathImage}" : '';
		}
		function deleteImage() {
			if( strlen( $this->nameImage )) {
				if( is_file( $this->pathImage ) and !@unlink( $this->pathImage )) throw new Exception( "Can't delete {$this->pathImage}" );
				$this->nameImage = null;
			}
		}
		function setImage( $name ) {
			$this->deleteImage();
			$this->nameImage = $name;
		}
		function uploadImage( $uploadedFile ) {
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );

			$this->setImage( $fileFullName );
		}
		function getImage( $options = Array() ) {
			$options[ 'src' ] = $this->srcImage;
			$options[ 'title' ] = $this->name;
			return $this->nameImage ? CHtml::tag( 'img', $options ) : '';
		}
		
			# trade platforms
		function getIDsTradePlatforms() {
			return CommonLib::slice( $this->tradePlatforms, 'id' );
		}
		function unlinkFromTradePlatforms() {
			foreach( $this->linksToPlatforms as $link ) {
				$link->delete();
			}
		}
		function linkToTradePlatforms( $ids ) {
			foreach( $ids as $idPlatform ) {
				$link = EAToTradePlatformModel::model()->findByAttributes( Array( 'idEA' => $this->id, 'idPlatform' => $idPlatform ));
				if( !$link ) {
					$link = EAToTradePlatformModel::instance( $this->id, $idPlatform );
					$link->save();
				}
			}
		}
		function setTradePlatforms( $ids ) {
			$oldIDs = $this->getIDsTradePlatforms();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromTradePlatforms();
				$this->linkToTradePlatforms( $ids );
			}
		}
		
			# EATradeAccounts
		function deleteEATradeAccounts() {
			foreach( $this->EATradeAccounts as $account ) {
				$account->delete();
			}
		}
		
			# EAVersions
		function deleteEAVersions() {
			foreach( $this->EAVersions as $version ) {
				$version->delete();
			}
		}
		
			# EAStatements
		function deleteEAStatements() {
			foreach( $this->EAStatements as $statsment ) {
				$statsment->delete();
			}
		}
		
			# stats
		function deleteStats() {
			if( $this->stats ) $this->stats->delete();
		}

    static public function getEaPlatforms() {
      $criteria = new CDbCriteria();
      $criteria->join = 'JOIN ft_ea_to_trade_platform eat ON eat.idPlatform = t.id';

      return TradePlatformModel::getPlatforms($criteria);
    }
    
			# events
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				Yii::App()->db->createCommand( " 
					REPLACE INTO 		`{{ea_stats}}` ( `idEA` )
					VALUES 				( :id );
				" )->query( Array(
					':id' => $this->id,
				));
				UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA, Array( 'idEA' => $this->id, 'idUser' => Yii::App()->user->id ));
			}
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			$this->deleteImage();
			$this->unlinkFromTradePlatforms();
			$this->deleteEATradeAccounts();
			$this->deleteEAVersions();
			$this->deleteEAStatements();
			$this->deleteStats();
		}
	}

?>