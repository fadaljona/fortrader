<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesSearchStatsModel extends ModelBase {
		public $count;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function saveKey( $keyword ){
			$stat = new self;
			$stat->searchDate = date('Y-m-d H:i:s', time());
			$stat->keyword = $keyword;
			if( $stat->validate() ) $stat->save();
		}
		
		function rules() {
			return Array(
				Array( 'id', 'unique'),
				Array( 'searchDate', 'date', 'format'=>'yyyy-M-d H:m:s'),
				Array( 'keyword', 'length', 'max' => 255),
			);
		}
		public function tableName(){
			return '{{quotes_search_stats}}';
		}
	}
?>