<?
	Yii::import( 'models.base.SettingsModelBase' );
	
	final class JournalSettingsModel extends SettingsModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) $model = new self();
			}
			return $model;
		}
		public function getTitle(){
			if( !$this->texts || !$this->texts->listTitle ) return false;
			return $this->texts->listTitle;
		}
		public function getDescription(){
			if( !$this->texts || !$this->texts->listDescription ) return false;
			return $this->texts->listDescription;
		}
		public function getFullDescription(){
			if( !$this->texts || !$this->texts->listFullDescription ) return false;
			return $this->texts->listFullDescription;
		}
		public function getMetaTitle(){
			if( !$this->texts || !$this->texts->listMetaTitle ) return false;
			return $this->texts->listMetaTitle;
		}
		public function getMetaDescription(){
			if( !$this->texts || !$this->texts->listMetaDescription ) return false;
			return $this->texts->listMetaDescription;
		}
		public function getMetaKeywords(){
			if( !$this->texts || !$this->texts->listMetaKeywords ) return false;
			return $this->texts->listMetaKeywords;
		}
		public function getOgImgUrl(){
			if( !$this->texts || !$this->texts->listOgImgUrl ) return false;
			return $this->texts->listOgImgUrl;
		}
		
	}

?>