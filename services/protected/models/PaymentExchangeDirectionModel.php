<?php

Yii::import('models.base.ModelBase');

final class PaymentExchangeDirectionModel extends ModelBase
{
    const PATHUploadDir = 'uploads/paymentSystems';

    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function relations()
    {
        return array(
            'i18ns' => array( self::HAS_MANY, 'PaymentExchangeDirectionI18nModel', 'idDirection', 'alias' => 'i18nsPaymentExchangeDirection' ),
            'currentLanguageI18N' => array( self::HAS_ONE, 'PaymentExchangeDirectionI18nModel', 'idDirection',
                'alias' => 'cLI18NPaymentExchangeDirectionModel',
                'on' => '`cLI18NPaymentExchangeDirectionModel`.`idLanguage` = :idLanguage',
                'params' => array(
                    ':idLanguage' => LanguageModel::getCurrentLanguageID(),
                ),
            ),
            'fromCurrency' => array( self::HAS_ONE, 'PaymentSystemCurrencyModel', array('id' => 'from')),
            'toCurrency' => array( self::HAS_ONE, 'PaymentSystemCurrencyModel', array('id' => 'to')),
        );
    }

    public static function getSavedDirections()
    {
        $outArr = array();
        $models = self::model()->findAll();
        foreach ($models as $model) {
            $outArr[] = $model->from . '_' . $model->to;
        }
        return $outArr;
    }
    public static function getAdminListDp($filterModel = false)
    {
        $DP = new CActiveDataProvider(get_called_class(), array(
            'criteria' => array(
                'with' => array( 'currentLanguageI18N', 'i18ns', 'fromCurrency', 'toCurrency' ),
                'order' => ' `t`.`id` DESC ',
            ),
        ));
        return $DP;
    }
    static function findBySlugsWithLang($from, $to)
    {
        if (!$from || !$to) {
            return false;
        }
        $fromModel = PaymentSystemCurrencyModel::findBySlugWithLang($from);
        if (!$fromModel) {
            return false;
        }
        $toModel = PaymentSystemCurrencyModel::findBySlugWithLang($to);
        if (!$toModel) {
            return false;
        }

        return self::model()->find(array(
            'with' => array( 'currentLanguageI18N' ),
            'condition' => " `t`.`from` = :from AND `t`.`to` = :to ",
            'params' => array( ':from' => $fromModel->id, ':to' => $toModel->id ),
        ));
    }
    public function getAvailableLangs()
    {
        $settings = PaymentExchangerSettingsModel::getModel();
        $langsArr = array();
        foreach ($this->i18ns as $i18n) {
            if ($i18n->title) {
                $langsArr[$i18n->idLanguage] = LanguageModel::getLangFromLangsFileByID($i18n->idLanguage);
            }
        }
        foreach ($settings->messages->i18ns as $i18n) {
            if (json_decode($i18n->value)->defaultExchangeDirectionTitle) {
                $langsArr[$i18n->idLanguage] = LanguageModel::getLangFromLangsFileByID($i18n->idLanguage);
            }
        }
        return $langsArr;
    }
    public function getDirectionTitle()
    {
        if ($this->fromCurrency) {
            $fromCurrency = $this->fromCurrency->title;
        } else {
            $fromCurrency = Yii::t('*', 'All');
        }

        if ($this->toCurrency) {
            $toCurrency = $this->toCurrency->title;
        } else {
            $toCurrency = Yii::t('*', 'All');
        }

        return "$fromCurrency -> $toCurrency";
    }



    public function getPathOgImage()
    {
        return strlen($this->ogImage) ? self::PATHUploadDir."/{$this->ogImage}" : '';
    }
    public function getSrcOgImage()
    {
        return $this->pathOgImage ? Yii::app()->baseUrl."/{$this->pathOgImage}" : '';
    }
    public function getAbsoluteSrcOgImage()
    {
        if ($this->pathOgImage) {
            return Yii::app()->createAbsoluteUrl($this->pathOgImage);
        }
        $settings = PaymentExchangerSettingsModel::getModel();
        if (!$settings->paymentExchangeDirectionPageOgImage) {
            return false;
        }
        return CommonLib::getCommonImageSrc($settings->paymentExchangeDirectionPageOgImage);
    }
    public function uploadOgImage($uploadedFile)
    {
        $fileEx = strtolower($uploadedFile->getExtensionName());
        list( $fileName, $fileEx ) = explode(".", CommonLib::getFreeFileName(self::PATHUploadDir, $fileEx));

        $fileFullName = "{$fileName}.{$fileEx}";
        $filePath = self::PATHUploadDir."/{$fileFullName}";
        if (!@$uploadedFile->saveAs($filePath)) {
            throw new Exception("Can't write to ".self::PATHUploadDir);
        }

        $this->setOgImage($fileFullName);
    }
    public function setOgImage($name)
    {
        $this->deleteOgImage();
        $this->ogImage = $name;
    }
    public function deleteOgImage()
    {
        if (strlen($this->ogImage)) {
            if (is_file($this->pathOgImage) and !@unlink($this->pathOgImage)) {
                throw new Exception("Can't delete {$this->pathOgImage}");
            }
            $this->ogImage = null;
        }
    }
    public function getSingleURL()
    {
        //return Yii::App()->createURL( 'informers/single', Array( 'slug' => $this->slug ));
    }

    public function getI18N()
    {
        if ($this->currentLanguageI18N) {
            return $this->currentLanguageI18N;
        }
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == 0) {
                if (strlen($i18n->title)) {
                    return $i18n;
                }
                break;
            }
        }
        foreach ($this->i18ns as $i18n) {
            if (strlen($i18n->title)) {
                return $i18n;
            }
        }
    }
    public function getTitleByLang($langId)
    {
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == $langId) {
                if (strlen($i18n->title)) {
                    return $i18n->title;
                }
                return false;
            }
        }
    }

    public function getModelTitle()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->title : '';
    }
    private function getTextSetting($modelField, $defaultField)
    {
        $i18n = $this->getI18N();
        if ($i18n && $i18n->{$modelField}) {
            return $i18n->{$modelField};
        }
        $settings = PaymentExchangerSettingsModel::getModel();
        if (!$settings->{$defaultField}) {
            return false;
        }
        return str_replace(
            array('{$from}', '{$to}'),
            array($this->fromCurrency->title, $this->toCurrency->title),
            $settings->{$defaultField}
        );
    }
    public function getTitle()
    {
        return $this->getTextSetting('title', 'defaultExchangeDirectionTitle');
    }
    public function getMetaTitle()
    {
        return $this->getTextSetting('metaTitle', 'defaultExchangeDirectionMetaTitle');
    }
    public function getMetaDesc()
    {
        return $this->getTextSetting('metaDesc', 'defaultExchangeDirectionMetaDesc');
    }
    public function getMetaKeys()
    {
        return $this->getTextSetting('metaKeys', 'defaultExchangeDirectionMetaKeys');
    }
    public function getShortDesc()
    {
        return $this->getTextSetting('shortDesc', 'defaultExchangeDirectionShortDesc');
    }
    public function getFullDescTitle()
    {
        return $this->getTextSetting('fullDescTitle', 'defaultExchangeDirectionFullDescTitle');
    }
    public function getFullDesc()
    {
        return $this->getTextSetting('fullDesc', 'defaultExchangeDirectionFullDesc');
    }
    public function getShortDescWithFormat()
    {
        if (!$this->shortDesc) {
            return false;
        }
        $str = $this->shortDesc;
        if (strpos($str, PHP_EOL)) {
            $str = str_replace(PHP_EOL, '</div><div class="fx_text">', $str);
        }
        return CHtml::tag('div', array( 'class' => 'fx_text' ), $str);
    }
    public function getFullDescWithFormat()
    {
        if (!$this->fullDesc) {
            return false;
        }
        $str = $this->fullDesc;
        if (strpos($str, PHP_EOL)) {
            $str = str_replace(PHP_EOL, '</div><div class="fx_text">', $str);
        }
        return CHtml::tag('div', array( 'class' => 'fx_text' ), $str);
    }



        # i18ns
    public function deleteI18Ns()
    {
        foreach ($this->i18ns as $i18n) {
            $i18n->delete();
        }
    }
    public function addI18Ns($i18ns)
    {
        $idsLanguages = LanguageModel::getExistsIDS();
        foreach ($i18ns as $idLanguage => $obj) {
            $i18n = PaymentExchangeDirectionI18nModel::model()->findByAttributes(array( 'idDirection' => $this->id, 'idLanguage' => $idLanguage ));
            if (!$i18n) {
                $i18n = PaymentExchangeDirectionI18nModel::instance(
                    $this->id,
                    $idLanguage,
                    $obj->title,
                    $obj->metaTitle,
                    $obj->metaDesc,
                    $obj->metaKeys,
                    $obj->shortDesc,
                    $obj->fullDescTitle,
                    $obj->fullDesc
                );
            } else {
                $i18n->title = $obj->title;
                $i18n->metaTitle = $obj->metaTitle;
                $i18n->metaDesc = $obj->metaDesc;
                $i18n->metaKeys = $obj->metaKeys;
                $i18n->shortDesc = $obj->shortDesc;
                $i18n->fullDescTitle = $obj->fullDescTitle;
                $i18n->fullDesc = $obj->fullDesc;
            }
            $i18n->save();
        }
    }
    public function setI18Ns($i18ns)
    {
        $this->addI18Ns($i18ns);
    }
        # events
    protected function afterDelete()
    {
        parent::afterDelete();
        $this->deleteI18Ns();
    }
}
