<?
	Yii::import( 'models.base.ModelBase' );

	final class MT5AccountHistoryDealsModel extends ModelBase {
		public $timestamp;
		public $profit;
		public $commission;
		public $swap;
		
		public $SUM_DEAL_VOLUME, $SUM_DEAL_SWAP, $SUM_DEAL_PROFIT, $DEALS_COUNT;
		
		public $closePrice, $lots;
		
		public $OrderCloseDate, $countRows;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "mt5_account_history_deals";
		}
		function getDealTypeTitle(){
			return $this->DEAL_TYPE;
		}
	}

?>