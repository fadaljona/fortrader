<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CurrencyRatesConvertModel extends ModelBase {
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'CurrencyRatesConvertI18NModel', array( 'idPair' => 'idPair' ), 'alias' => 'i18nsCurrencyRatesConvert' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'CurrencyRatesConvertI18NModel', array( 'idPair' => 'idPair' ), 
					'alias' => 'cLI18NCurrencyRatesConvertModel',
					'on' => '`cLI18NCurrencyRatesConvertModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getByAlias( Yii::app()->language )->id,
					),
				),	
				'rateFrom' => Array( self::HAS_ONE, 'CurrencyRatesModel', array( 'id' => 'idCurrencyFrom' ) ),
				'rateTo' => Array( self::HAS_ONE, 'CurrencyRatesModel', array( 'id' => 'idCurrencyTo' ) ),
				'views' => Array( self::HAS_ONE, 'CurrencyRatesConvertViewsModel', 'idConvert' ),
			);

		}
		static function findByParams( $value, $currencyFrom, $currencyTo, $type ){
			if( $type == 'cbr' ){
				$condition = " `value` = :value AND `rateFrom`.`code` = :currencyFrom AND `rateTo`.`code` = :currencyTo AND `rateFrom`.`noData` = 0 AND `rateTo`.`noData` = 0 AND `t`.`type` = 'cbr' ";
			}elseif( $type == 'ecb' ){
				$condition = " `value` = :value AND `rateFrom`.`code` = :currencyFrom AND `rateTo`.`code` = :currencyTo AND `rateFrom`.`ecb` = 1 AND `rateTo`.`ecb` = 1 AND `t`.`type` = 'ecb' ";
			}
			return self::model()->find(array(
				'with' => Array( 'currentLanguageI18N', 'i18ns', 'rateFrom', 'rateTo' ),
				'condition' => $condition,
				'params' => array( ':value' => $value, ':currencyFrom' => strtoupper($currencyFrom), ':currencyTo' => strtoupper($currencyTo) ),
			));
		}
		static function defineIdPair( $idCurrencyFrom, $idCurrencyTo, $type ){
			$model = self::model()->find(array(
				'condition' => ' (`t`.`idCurrencyFrom` = :idCurrencyFrom AND `t`.`idCurrencyTo` = :idCurrencyTo AND `t`.`type` = :type) OR (`t`.`idCurrencyFrom` = :idCurrencyTo AND `t`.`idCurrencyTo` = :idCurrencyFrom AND `t`.`type` = :type) ',
				'params' => array( ':idCurrencyFrom' => $idCurrencyFrom, ':idCurrencyTo' => $idCurrencyTo, ':type' => $type )
			));
			if( $model ){
				return $model->idPair;
			}
			$max = Yii::App()->db->createCommand(" 
				SELECT 	MAX( `idPair` )
				FROM	`{{currency_rates_convert}}`;
			")->queryScalar();
			return $max+1;
		}
		static function getAdminListDp( $filterModel = false ){
            $condition = '';
            $params = array();
            if (Yii::app()->request->getParam('idCurrency')) {
                $condition = " `t`.`idCurrencyFrom` = :idCurrency OR `t`.`idCurrencyTo` = :idCurrency ";
                $params[':idCurrency'] = Yii::app()->request->getParam('idCurrency');
            }

			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns', 'rateFrom' ),
					'order' => ' `t`.`idCurrencyFrom` DESC ',
                    'group' => ' `t`.`idPair` ',
                    'condition' => $condition,
                    'params' => $params
				),
			));
			return $DP;
		}
		public function getSimilars(){
			return self::model()->findAll(array(
				'with' => array('views'),
				'condition' => " `t`.`idCurrencyFrom` = :idCurrencyFrom AND `t`.`idCurrencyTo` = :idCurrencyTo AND `t`.`id` <> :id AND `t`.`type` = :type ",
				'order' => ' `views`.`views` DESC ',
				'params' => array( ':idCurrencyFrom' => $this->idCurrencyFrom, ':idCurrencyTo' => $this->idCurrencyTo, ':id' => $this->id, ':type' => $this->type ),
				'limit' => 10
			));
		}
		public function getInOtherCurrencies(){
			return self::model()->findAll(array(
				'with' => array('views'),
				'condition' => " `t`.`idCurrencyFrom` = :idCurrencyFrom AND `t`.`value` = :value AND `t`.`id` <> :id AND `t`.`type` = :type ",
				'order' => ' `views`.`views` DESC ',
				'params' => array( ':idCurrencyFrom' => $this->idCurrencyFrom, ':value' => $this->value, ':id' => $this->id, ':type' => $this->type ),
				'limit' => 10
			));
		}
		static function getPopulars( $type ){
			return self::model()->findAll(array(
				'with' => array('views'),
				'condition' => ' `t`.`type` = :type ',
				'order' => ' `views`.`views` DESC ',
				'limit' => 10,
				'params' => array( ':type' => $type )
			));
		}
		static function sendPushData( $id ){
			$model = self::model()->findByPk($id);
			if( !$model ) return false;
			$dataToSend = CHtml::tag('li', array(), CHtml::link( $model->modelTitle, $model->singleUrl ) );
			
			$context = new ZMQContext();
			$socket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
			$socket->connect("tcp://localhost:8899");	
			
			$socket->send(json_encode( array( 'key' => 'currencyRatesConverterLog', 'data' => $dataToSend ) ));
			
			unset($socket);
			unset($context);
		}
		public function getModelTitle(){
			return $this->rateFrom->getNamePlurals( $this->value ) . ' ' . Yii::t('*', 'fromToCur') . ' ' . $this->rateTo->toName;
		}
		public function getPairTitle(){
			return $this->rateFrom->name . ' <--> ' . $this->rateTo->name;
		}
		public function getSingleUrlParams(){
			if( $this->type != 'cbr' )
				return array( 'value' => floatval($this->value), 'currencyFrom' => strtolower($this->rateFrom->code), 'currencyTo' => strtolower($this->rateTo->code), 'type' => $this->type );
			return array( 'value' => floatval($this->value), 'currencyFrom' => strtolower($this->rateFrom->code), 'currencyTo' => strtolower($this->rateTo->code) );
		}
		public function getSingleUrl(){
			return Yii::app()->createUrl('currencyRates/exchangePage', $this->singleUrlParams );
		}
		public function getAbsoluteSingleURL(){
			return Yii::app()->createAbsoluteUrl('currencyRates/exchangePage', $this->singleUrlParams );
		}
		public function getFullDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->fullDesc : '';
		}
		public function getMetaKeys() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaKeys : '';
		}
		public function getMetaDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaDesc : '';
        }
        
        public function getTitle() {
			$i18n = $this->getI18N();
			return !empty($i18n->title) ? $i18n->title : $this->modelTitle;
        }
        
        public function getMetaTitle() {
			$i18n = $this->getI18N();
			return !empty($i18n->metaTitle) ? $i18n->metaTitle : $this->modelTitle;
		}

		public function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->fullDesc )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->fullDesc )) return $i18n;
			}
		}
		
		public function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		public function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if( ( strlen( $obj->fullDesc ) || strlen( $obj->metaKeys ) || strlen( $obj->metaDesc ) || strlen( $obj->title ) || strlen( $obj->metaTitle ) ) && in_array( $idLanguage, $idsLanguages )) {
					$i18n = CurrencyRatesConvertI18NModel::model()->findByAttributes( Array( 'idPair' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = CurrencyRatesConvertI18NModel::instance( $this->idPair, $idLanguage, $obj->fullDesc, $obj->metaKeys, $obj->metaDesc, $obj->title, $obj->metaTitle );
						$i18n->save();
					}
					else{
						$i18n->fullDesc = $obj->fullDesc;
						$i18n->metaKeys = $obj->metaKeys;
                        $i18n->metaDesc = $obj->metaDesc;
                        $i18n->title = $obj->title;
                        $i18n->metaTitle = $obj->metaTitle;
						$i18n->save();
					}
				}
			}
		}

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
		
		protected function beforeDelete() {
			CurrencyRatesConvertHistoryModel::model()->deleteAll(
				" idConvert = :id ", 
				array(':id' => $this->id ) 
			);
			CurrencyRatesConvertViewsModel::model()->deleteAll(
				" idConvert = :id ", 
				array(':id' => $this->id ) 
			);

			return parent::beforeDelete();
		}
	}

?>