<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerStats2Model extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function flush() {
			Yii::App()->db->createCommand("
				UPDATE		`{{broker_stats2}}`
				SET			`dayCountLinks` = 0,
							`dayCountMarks` = 0,
							`dayCountReviews` = 0,
							`dayCountRedirects` = 0,
							`flushD` = CURDATE()
				WHERE		`flushD` IS NULL
					OR		`flushD` != CURDATE();
			")->query();
		}
		static function add( $idBroker, $filed, $value ) {
			$stats = self::model()->findByPk( $idBroker );
			if( $stats ) {
				$dayFiled = "day".ucfirst( $filed );
				$stats->$dayFiled = max( $stats->$dayFiled + $value, 0 );
				$stats->$filed = max( $stats->$filed + $value, 0 );
				$stats->updatedDT = new CDbExpression( "NEW()" );
				$stats->update( Array( $dayFiled, $filed, 'updatedDT' ));
			}
		}
		static function inc( $idBroker, $filed ) {
			self::add( $idBroker, $filed, 1 );
		}
		static function dec( $idBroker, $filed ) {
			self::add( $idBroker, $filed, -1 );
		}
	}

?>