<?
	Yii::import( 'models.base.ModelBase' );

	final class BrokerToBrokerIOModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{broker_to_broker_io}}";
		}
		static function instance( $idBroker, $idIO ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker', 'idIO' ));
		}
	}

?>