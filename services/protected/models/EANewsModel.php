<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EANewsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_news}}";
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'EANewsI18NModel', 'idNews', 'alias' => 'i18nsEANews' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'EANewsI18NModel', 'idNews', 
					'alias' => 'cLI18NEANews',
					'on' => '`cLI18NEANews`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			 );
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->news )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->news )) return $i18n;
			}
		}
		function getNews() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->news : '';
		}
					
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->news )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = EANewsI18NModel::model()->findByAttributes( Array( 'idNews' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = EANewsI18NModel::instance( $this->id, $idLanguage, $obj->news );
						$i18n->save();
					}
					else{
						$i18n->news = $obj->news;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			
		protected function afterDelete() {
			$this->deleteI18Ns();
			parent::afterDelete();
		}
	}

?>