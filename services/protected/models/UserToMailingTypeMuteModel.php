<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserToMailingTypeMuteModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
			);
		}
		static function instance( $idUser, $idType ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser', 'idType' ));
		}
	}

?>