<?
	Yii::import( 'models.base.ModelBase' );
	
	final class TradeAccountModel extends ModelBase {
		public $countOrders;
		public $countFilledOrders;
		public $countCanceledOrders;
		public $countRowsMT4AccountHistory;
		public $maxOrderCloseTimeMT4AccountHistory;
		public $maxOrderTicketMT4AccountHistory;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'stats' => Array( self::HAS_ONE, 'TradeAccountStatsModel', 'idAccount' ),
				'server' => Array( self::BELONGS_TO, 'ServerModel', 'idServer' ),
				'EATradeAccounts' => Array( self::HAS_MANY, 'EATradeAccountModel', 'idTradeAccount' ),
			);
		}
		static function updateStats() {
			
			$exists = Yii::App()->db->createCommand("
				SELECT 		1 
				FROM 		`{{trade_account_stats}}` 
				WHERE 		`{{trade_account_stats}}`.`DT` IS NULL OR DATE_ADD( `{{trade_account_stats}}`.`DT`, INTERVAL 5 MINUTE ) < NOW()
				LIMIT		1
			")->queryScalar();
			
			if( $exists ) {
				$command = file_get_contents( "protected/data/updateTradeAccountStats.sql" );
				Yii::App()->db->createCommand( $command )->query();
			}
						
		}
			# stats
		function deleteStats() {
			if( $this->stats ) $this->stats->delete();
		}
		
			# EATradeAccounts
		function deleteEATradeAccounts() {
			foreach( $this->EATradeAccounts as $account ) {
				$account->delete();
			}
		}
		
			# events
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				Yii::App()->db->createCommand( " 
					REPLACE INTO 		`{{trade_account_stats}}` ( `idAccount` )
					VALUES 				( :id )
				" )->query( Array(
					':id' => $this->id,
				));
			}
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteStats();
			$this->deleteEATradeAccounts();
		}
	}

?>