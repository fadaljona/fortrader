<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserNoticeModel extends ModelBase {
		const TYPE_NOTICE_END_ADVERTISEMENT_CAMPAIGN = 1;
		const TYPE_NOTICE_CREATE_CONTEST_MEMBER = 2;
		const TYPE_NOTICE_CREATE_JOURNAL = 3;
		const TYPE_NOTICE_CREATE_USER_MESSAGE = 4;
		const TYPE_NOTICE_LOW_USER_BALANCE = 5;
		const TYPE_NOTICE_INVITE_FRIEND = 6;
		const TYPE_NOTICE_ACCEPT_INVITE_FRIEND = 7;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'i18ns' => Array( self::HAS_MANY, 'UserNoticeI18NModel', 'idNotice', 'alias' => 'i18nsUserNotice' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'UserNoticeI18NModel', 'idNotice', 
					'alias' => 'cLI18NUserNotice',
					'on' => '`cLI18NUserNotice`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		static function instance( $type, $idUser, $idObject = null, $link = null ) {
			return self::modelFromAssoc( __CLASS__, compact( 'type', 'idUser', 'idObject', 'link' ));
		}
		static function getAvailableTypes() {
			return Array( 'end_advertisement_campaign', 'create_contest_member', 'create_journal', 'create_user_message', 'low_user_balance' );
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->message )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->message )) return $i18n;
			}
		}
		function getHTMLMessage( $maxLen = null ) {
			$i18n = $this->getI18N();
			$message = $i18n ? $i18n->message : '';
			$message = htmlspecialchars( $message );
			$message = CommonLib::nl2br( $message );
			if( $maxLen ) $message = CommonLib::shorten( $message, $maxLen );
			return $message;
		}
		function getIcon() {
			$class = "";
			$pre = "btn btn-mini no-hover";
			switch( $this->type ) {
				case 'end_advertisement_campaign':{
					$class = "{$pre} btn-success icon-shopping-cart"; break;
				}
				case 'create_contest_member':{
					$class = "{$pre} btn-primary icon-signal"; break;
				}
				case 'create_journal':{
					$class = "{$pre} btn-info icon-twitter"; break;
				}
				case 'create_user_message':{
					$class = "{$pre} btn-pink icon-comment"; break;
				}
				case 'low_user_balance':{
					$class = "{$pre} btn-info icon-twitter"; break;
				}
				case 'invite_friend':{
					$class = "{$pre} icon-user"; break;
				}
				case 'accept_invite_friend':{
					$class = "{$pre} icon-user"; break;
				}
			}
			$tag = CHtml::openTag( 'i', Array( 'class' => $class ));
			$tag .= CHtml::closeTag( 'i' );
			
			return $tag;
		}
		static function viewAll( $idUser ) {
			Yii::App()->db->createCommand("
				UPDATE		`{{user_notice}}`
				SET			`viewed` = 1
				WHERE		`idUser` = :idUser
			")->query( Array( 
				':idUser' => $idUser
			));
		}
		static function deleteOld( $type, $idUser ) {
			Yii::App()->db->createCommand("
				DELETE
				FROM		`{{user_notice}}`
				WHERE		`type` = :type
					AND		`idUser` = :idUser
			")->query( Array( 
				':type' => $type,
				':idUser' => $idUser,
			));
		}
		function getLink() {
			$link = "";
			
			if( $this->link ) {
				$link = $this->link;
			}
			else{
				switch( $this->type ) {
					case 'create_contest_member':{
						$link = ContestMemberModel::getModelSingleURL( $this->idObject );
						break;
					}
					case 'create_journal':{
						$link = JournalModel::getModelSingleURL( $this->idObject );
						break;
					}
					case 'low_user_balance':{
						$link = Yii::App()->createURL( '/admin/userBalance/list', Array( 'idEdit' => $this->idObject ));
						break;
					}
					case 'invite_friend':{
						$link = UserModel::getModelProfileURL( $this->idObject );
						break;
					}
					case 'accept_invite_friend':{
						$link = UserModel::getModelProfileURL( $this->idObject );
						break;
					}
				}
			}
			
			return $link;
		}
			
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->message )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = UserNoticeI18NModel::model()->findByAttributes( Array( 'idNotice' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = UserNoticeI18NModel::instance( $this->id, $idLanguage, $obj->message );
						$i18n->save();
					}
					else{
						$i18n->message = $obj->message;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		function setI18NsFromMessage( $key, $ns = '*', $params = Array(), $paramsI18Ns = Array() ) {
			$message = MessageModel::model()->find(Array(
				'with' => 'i18ns',
				'condition' => "
					`t`.`key` = :key AND `t`.`ns` = :ns
				",
				'params' => Array(
					':key' => $key,
					':ns' => $ns,
				),
			));
			if( $message ) {
				$i18ns = Array();
				foreach( $message->i18ns as $i18n ) {
					$message = $i18n->value;
					if( $params ) $message = strtr( $message, $params );
					if( $paramsI18Ns and isset( $paramsI18Ns[ $i18n->idLanguage ])) $message = strtr( $message, $paramsI18Ns[ $i18n->idLanguage ]);
					$i18ns[ $i18n->idLanguage ] = (object)Array(
						'message' => $message,
					);
				}
				$this->setI18Ns( $i18ns );
			}
		}			
			
			# events
		static function deleteNotices( $type, $idUser, $idObject ) {
			$events = UserNoticeModel::model()->findAllByAttributes(Array(
				'type' => $type, 
				'idUser' => $idUser, 
				'idObject' => $idObject,
			));
			foreach( $events as $event ) {
				$event->delete();
			}
		}
		private static function noticeEndAdvertisementCampaign( $params ) {
			$model = AdvertisementCampaignModel::model()->findByPk( $params[ 'idCampaign' ]);
			if( !$model ) return;
			
			$url = Yii::App()->createURL( '/admin/advertisementCampaign/list', Array( 'idBroker' => $model->owner->idBroker , 'idEdit' => $model->id ));
			$notice = self::instance( 'end_advertisement_campaign', $params[ 'idUser' ], $params[ 'idCampaign' ], $url );
			$notice->save();
			
			$notice->setI18NsFromMessage( 'End advertisement campaign: {name}', '*', Array(
				'{name}' => $model->name,
			));
		}
		private static function noticeCreateContestMember( $params ) {
			$notice = self::instance( 'create_contest_member', $params[ 'idUser' ], $params[ 'idMember' ] );
			$notice->save();
			
			if( @$params[ 'self' ] ) {
				$notice->setI18NsFromMessage( 'Congrats! Your registered in contest!' );
			}
			else{
				$notice->setI18NsFromMessage( 'For you open the trading account to participate in the contest.' );
			}
		}
		private static function noticeCreateJournal( $params ) {
			$notice = self::instance( 'create_journal', $params[ 'idUser' ], $params[ 'idJournal' ] );
			$notice->save();
			
			$notice->setI18NsFromMessage( 'We inform you about the release of a new issue of the magazine' );
		}
		private static function noticeCreateUserMessage( $params ) {
			$notice = self::instance( 'create_user_message', $params[ 'idUser' ], $params[ 'idMessage' ], $params[ 'link' ]);
			$notice->save();
			
			switch( @$params[ 'type' ]) {
				case 'question':{
					$notice->setI18NsFromMessage( 'New question' );
					break;
				}
				case 'comment':{
					$notice->setI18NsFromMessage( 'New comment' );
					break;
				}
				case 'answer':{
					$notice->setI18NsFromMessage( 'New answer' );
					break;
				}
				case 'public_message':{
					$notice->setI18NsFromMessage( 'Public message' );
					break;
				}
			}
		}
		private static function noticeLowUserBalance( $params ) {
			$user = UserModel::model()->findByPk( $params[ 'idUserTarget' ]);
			if( !$user ) return;
			
			$notice = self::instance( 'low_user_balance', $params[ 'idUser' ], $params[ 'idUserTarget' ]);
			$notice->save();
			
			$notice->setI18NsFromMessage( 'End balance: {username}', '*', Array( '{username}' => $user->user_login ));
		}
		private static function noticeInviteFriend( $params ) {
			$user = UserModel::model()->findByPk( $params[ 'idFriend' ]);
			if( !$user ) return;
			
			$notice = self::instance( 'invite_friend', $params[ 'idUser' ], $params[ 'idFriend' ]);
			$notice->save();
			
			$notice->setI18NsFromMessage( '{username} offers you friendship', '*', Array( '{username}' => $user->user_login ));
		}
		private static function noticeAcceptInviteFriend( $params ) {
			$user = UserModel::model()->findByPk( $params[ 'idFriend' ]);
			if( !$user ) return;
			
			$notice = self::instance( 'accept_invite_friend', $params[ 'idUser' ], $params[ 'idFriend' ]);
			$notice->save();
			
			$notice->setI18NsFromMessage( '{username} accepted your offer of friendship', '*', Array( '{username}' => $user->user_login ));
		}
		static function notice( $type, $params ) {
			switch( $type ) {
				case self::TYPE_NOTICE_END_ADVERTISEMENT_CAMPAIGN:{ 
					self::noticeEndAdvertisementCampaign( $params ); break;
				}
				case self::TYPE_NOTICE_CREATE_CONTEST_MEMBER:{ 
					self::noticeCreateContestMember( $params ); break;
				}
				case self::TYPE_NOTICE_CREATE_JOURNAL:{ 
					self::noticeCreateJournal( $params ); break;
				}
				case self::TYPE_NOTICE_CREATE_USER_MESSAGE:{ 
					self::noticeCreateUserMessage( $params ); break;
				}
				case self::TYPE_NOTICE_LOW_USER_BALANCE:{ 
					self::noticeLowUserBalance( $params ); break;
				}
				case self::TYPE_NOTICE_INVITE_FRIEND:{ 
					self::noticeInviteFriend( $params ); break;
				}
				case self::TYPE_NOTICE_ACCEPT_INVITE_FRIEND:{ 
					self::noticeAcceptInviteFriend( $params ); break;
				}
			}
			return true;
		}
	}

?>