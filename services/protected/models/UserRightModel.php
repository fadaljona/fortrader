<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserRightModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $name, $title ) {
			$model = new self();
			$model->name = $name;
			$model->title = $title;
			return $model;
		}
		static function fromAssoc( $assoc ) {
			return self::modelFromAssoc( __CLASS__, $assoc );
		}
		# fields
		function getType() {
			if( preg_match( '#Control$#', $this->name )) return 'Control';
			if( preg_match( '#Ban$#', $this->name )) return 'Ban';
			return 'Other';
		}
	}

?>