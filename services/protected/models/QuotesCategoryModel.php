<?php

//ManageTextContent
Yii::import( 'components.behaviors.ManageTextContentBehavior' );
Yii::import('models.base.ModelBase');

class QuotesCategoryModel extends ModelBase{
	const CACHE_KEY_PREFIX = 'QuotesCategoryModel.';
	protected $parentname;
	
	public $countRows; //ManageTextContent
	
	public function relations(){
		return array(
		
			//ManageTextContent
				'i18nsManageTextContent' => Array( self::HAS_MANY, 'QuotesCategoryI18nModel', array('idCat' => 'id') ), 
				'ruLanguageI18NManageTextContent' => Array( self::HAS_ONE, 'QuotesCategoryI18nModel', array('idCat' => 'id'), 'on' => '`ruLanguageI18NManageTextContent`.`idLanguage` = 1'),
				'enLanguageI18NManageTextContent' => Array( self::HAS_ONE, 'QuotesCategoryI18nModel', array('idCat' => 'id'), 'on' => '`enLanguageI18NManageTextContent`.`idLanguage` = 0'),
			//ManageTextContent
		
			'symbols' => array( self::HAS_MANY,'QuotesSymbolsModel', Array( "category" => "id" ) ),
			'i18ns' => Array( self::HAS_MANY, 'QuotesCategoryI18nModel', 'idCat', 'alias' => 'i18nsQuotesCategory' ),
			'currentLanguageI18N' => Array( self::HAS_ONE, 'QuotesCategoryI18nModel', 'idCat', 
				'alias' => 'cLI18NQuotesCategoryModel',
				'on' => '`cLI18NQuotesCategoryModel`.`idLanguage` = :idLanguage',
				'params' => Array(
					':idLanguage' => LanguageModel::getCurrentLanguageID(),
				),
			),
		);
	}
	public function tableName(){
		return '{{quotes_category}}';
	}
	
	//ManageTextContent
	public function behaviors(){
		return array(
			'manageTextContentBehavior' => array(
				'class' => 'ManageTextContentBehavior',
				'filterCondition' => ' `t`.`is_def` = 0 '
			),
		);
    }
	
	function getI18N() {
		if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
		foreach( $this->i18ns as $i18n ) {
			if( $i18n->idLanguage == 0 ) {
				if( strlen( $i18n->name )) return $i18n;
				break;
			}
		}
		foreach( $this->i18ns as $i18n ) {
			if( strlen( $i18n->name )) return $i18n;
		}
	}
	
	
	
	
	
	
	
	function getName() {
		$i18n = $this->getI18N();
		return $i18n ? $i18n->name : '';
	}
	function getDesc() {
		$i18n = $this->getI18N();
		return $i18n ? $i18n->desc : '';
	}
	function getFullDesc() {
		$i18n = $this->getI18N();
		return $i18n ? $i18n->fullDesc : '';
	}
	function getTitle() {
		$i18n = $this->getI18N();
		return $i18n ? $i18n->title : '';
	}
	function getMetaTitle() {
		$i18n = $this->getI18N();
		return $i18n ? $i18n->metaTitle : '';
	}
	function getMetaDescription() {
		$i18n = $this->getI18N();
		return $i18n ? $i18n->metaDescription : '';
	}
	function getMetaKeywords() {
		$i18n = $this->getI18N();
		return $i18n ? $i18n->metaKeywords : '';
	}
	function getShortDesc() {
		$i18n = $this->getI18N();
		return $i18n ? $i18n->shortDesc : '';
	}
	function getLOrder() {
		$i18n = $this->getI18N();
		return $i18n ? $i18n->order : 0;
	}


	
	
	
	

	static function getModel(){
		static $model;
		if(!$model){
			$model = self::model()->find();
			if(!$model) $model = new self();
		}
		return $model;
	}
	
	/*informers*/
	static function getLinkForInformerHeader($id){
		$model = self::model()->findByPk($id);
		if( !$model ) return false;
		return $model->absoluteSingleURL;
	}
	

	public static function getAll(){
		return QuotesCategoryModel::model()->findAll(array(
			'with' => array( 'currentLanguageI18N' ),
			'order'=>'`t`.`order`'
		));
	}
	static function getCatsIdsWithNames(){
		$models = self::model()->findAll(array(
			'with' => array( 'currentLanguageI18N' ),
			'condition' => ' `t`.`is_def` = 0 AND `cLI18NQuotesCategoryModel`.`name` IS NOT NULL AND `cLI18NQuotesCategoryModel`.`name` <> "" ',
			'order'=>'`t`.`order`'
		));
		$outArr = array(
			0 => Yii::t('*', 'All'),
		);
		foreach( $models as $model ){
			$outArr[$model->id] = $model->name;
		}
		return $outArr;
	}
	
	public static function findBySlug( $slug ){
		return self::model()->find(array(
			'condition' => " `slug` = :slug ",
			'params' => array( ':slug' => $slug ),
		));
	}
	public function getAvailableLangs(){
		$langsArr = array();
		foreach( $this->i18ns as $i18n ){
			if( $i18n->name ){
				$langsArr[] = LanguageModel::getLangFromLangsFileByID( $i18n->idLanguage );
			}
		}
		return $langsArr;
	}
	static function findBySlugWithLang( $slug ){
		return self::model()->find(array(
			'with' => Array( 'currentLanguageI18N' ),
			'condition' => " `slug` = :slug AND `cLI18NQuotesCategoryModel`.`name` IS NOT NULL AND `cLI18NQuotesCategoryModel`.`name` <> '' ",
			'params' => array( ':slug' => $slug ),
		));
	}
	
	//ManageTextContent
	function getSingleURL() {
		return Yii::App()->createURL( 'quotesNew/category', Array( 'slug' => $this->slug ));
	}
	function getAbsoluteSingleURL() {
		return Yii::App()->createAbsoluteUrl( 'quotesNew/category', Array( 'slug' => $this->slug ));
	}
	public static function getNameById($id){
		if($id){
			$cat=self::model()->find('id='.(int)$id);
			if($cat && $cat->name)
			return $cat->name;
		}
		return '';
	}

	public function getParentname(){
		$id = $this->getAttribute('parent');
		if($id == 0) return 'Основная';else{
			$cat = self::model()->find('id=' . $id);
			return trim($cat->name);
		}
	}
	
	public static function getIdCategory($catname){
		$cat=self::model()->find('`name` LIKE :catname', array(':catname'=>$catname));
		if($cat){
			return $cat->id;
		}else return 0;
	}

	public static function model($className = __CLASS__){
		return parent::model($className);
	}
	
	
	function deleteI18Ns() {
		foreach( $this->i18ns as $i18n ) {
			$i18n->delete();
		}
	}
	function addI18Ns( $i18ns ) {
		$idsLanguages = LanguageModel::getExistsIDS();
		foreach( $i18ns as $idLanguage=>$obj ) {
			if(( strlen( $obj->name )) and in_array( $idLanguage, $idsLanguages )) {
				$i18n = QuotesCategoryI18nModel::model()->findByAttributes( Array( 'idCat' => $this->id, 'idLanguage' => $idLanguage ));
				if( !$i18n ) {
					$i18n = QuotesCategoryI18nModel::instance( $this->id, $idLanguage, $obj->name, $obj->desc, $obj->fullDesc, $obj->title, $obj->metaTitle, $obj->metaDescription, $obj->metaKeywords, $obj->shortDesc, $obj->order );
				}else{
					$i18n->name = $obj->name;
					$i18n->desc = $obj->desc; 
					$i18n->fullDesc = $obj->fullDesc;
					$i18n->title = $obj->title;
					$i18n->metaTitle = $obj->metaTitle; 
					$i18n->metaDescription = $obj->metaDescription; 
					$i18n->metaKeywords = $obj->metaKeywords;
					$i18n->shortDesc = $obj->shortDesc;
					$i18n->order = $obj->order;
				}
			$i18n->save();
			}
		}
	}
	function setI18Ns( $i18ns ) {
		$this->deleteI18Ns();
		$this->addI18Ns( $i18ns );
	}
	protected function afterDelete() {
		parent::afterDelete();
		$this->deleteI18Ns();
	}
}
