<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestMemberLogModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $message ) {
			return self::modelFromAssoc( __CLASS__, compact( 'message' ));
		}
	}

?>