<?
	Yii::import( 'models.base.SettingsModelBase' );
	
	final class ContestSettingsModel extends SettingsModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) $model = new self();
			}
			return $model;
		}
		
		public function getTitle(){
			if( !$this->texts || !$this->texts->title ) return false;
			return $this->texts->title;
		}
		public function getMetaTitle(){
			if( !$this->texts || !$this->texts->metaTitle ) return false;
			return $this->texts->metaTitle;
		}
		public function getMetaDesc(){
			if( !$this->texts || !$this->texts->metaDesc ) return false;
			return $this->texts->metaDesc;
		}
		public function getMetaKeys(){
			if( !$this->texts || !$this->texts->metaKeys ) return false;
			return $this->texts->metaKeys;
		}
		public function getOgImageUrl(){
			if( !$this->texts || !$this->texts->ogImageUrl ) return false;
			return $this->texts->ogImageUrl;
		}
		public function getShortDesc(){
			if( !$this->texts || !$this->texts->shortDesc ) return false;
			return $this->texts->shortDesc;
		}
		public function getFullDesc(){
			if( !$this->texts || !$this->texts->fullDesc ) return false;
			return $this->texts->fullDesc;
		}
		
		public function getAllTitle(){
			if( !$this->texts || !$this->texts->allTitle ) return false;
			return $this->texts->allTitle;
		}
		public function getAllMetaTitle(){
			if( !$this->texts || !$this->texts->allMetaTitle ) return false;
			return $this->texts->allMetaTitle;
		}
		public function getAllMetaDesc(){
			if( !$this->texts || !$this->texts->allMetaDesc ) return false;
			return $this->texts->allMetaDesc;
		}
		public function getAllMetaKeys(){
			if( !$this->texts || !$this->texts->allMetaKeys ) return false;
			return $this->texts->allMetaKeys;
		}
		public function getAllOgImageUrl(){
			if( !$this->texts || !$this->texts->allOgImageUrl ) return false;
			return $this->texts->allOgImageUrl;
		}
		public function getAllShortDesc(){
			if( !$this->texts || !$this->texts->allShortDesc ) return false;
			return $this->texts->allShortDesc;
		}
		public function getAllFullDesc(){
			if( !$this->texts || !$this->texts->allFullDesc ) return false;
			return $this->texts->allFullDesc;
		}
		
		
		
		
	}

?>