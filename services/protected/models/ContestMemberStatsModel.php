<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestMemberStatsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'member' => Array( self::BELONGS_TO, 'ContestMemberModel', 'idMember' ),
				'error' => Array( self::HAS_ONE, 'ContestMonitoringErrorsModel', array( 'code' => 'last_error_code' ) ),
			);
		}
		function getStatus() {
            $status = false;
			
			if( $this->error && $this->error->title ){
				return $this->error->title;
			}
			
            return $status;
		}
	}
	
?>