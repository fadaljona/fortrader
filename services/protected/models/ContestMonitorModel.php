<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestMonitorModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idContest ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idContest' ));
		}
	}

?>