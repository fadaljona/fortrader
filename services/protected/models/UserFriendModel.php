<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserFriendModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				//'servers' => Array( self::HAS_MANY, 'ServerModel', 'idTradePlatform' ),
				//'linksToBrokers' => Array( self::HAS_MANY, 'BrokerToTradePlatformModel', 'idPlatform' ),
			);
		}
		static function instance( $idUser, $idFriend, $status ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser', 'idFriend', 'status' ));
		}
		static function invite( $idUser, $idFriend ) {
			$model = self::model()->findByAttributes(Array(
				'idUser' => $idUser,
				'idFriend' => $idFriend,
			));
			if( $model ) return;
			
			$model = self::instance( $idUser, $idFriend, 'invite' );
			$model->save();
			
			UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_INVITE_FRIEND, Array(
				'idUser' => $idFriend,
				'idFriend' => $idUser,
			));
		}
		static function reject( $idUser, $idFriend ) {
			$model = self::model()->findByAttributes(Array(
				'idUser' => $idFriend,
				'idFriend' => $idUser,
			));
			if( !$model ) return;
			
			$model->status = 'reject';
			$model->save();
			
			UserNoticeModel::deleteNotices( 'invite_friend', $idUser, $idFriend );
		}
		static function accept( $idUser, $idFriend ) {
			$model = self::model()->findByAttributes(Array(
				'idUser' => $idFriend,
				'idFriend' => $idUser,
			));
			if( !$model ) return;
			
			$model->status = 'accept';
			$model->save();
			
			$model = self::instance( $idUser, $idFriend, 'accept' );
			$model->save();
			
			UserNoticeModel::deleteNotices( 'invite_friend', $idUser, $idFriend );
			UserNoticeModel::deleteNotices( 'accept_invite_friend', $idFriend, $idUser );
			UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_ACCEPT_INVITE_FRIEND, Array(
				'idUser' => $idFriend,
				'idFriend' => $idUser,
			));
		}
	}

?>