<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ServerModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'broker' => Array( self::BELONGS_TO, 'BrokerModel', 'idBroker' ),
				'tradePlatform' => Array( self::BELONGS_TO, 'TradePlatformModel', 'idTradePlatform' ),
				'linksToContest' => Array( self::HAS_MANY, 'ServerToContestModel', 'idServer' ),
				'contestsMembers' => Array( self::HAS_MANY, 'ContestMemberModel', 'idServer' ),
				'EAStatements' => Array( self::HAS_MANY, 'EAStatementModel', 'idVersion' ),
			);
		}
		function unlinkFromContests() {
			foreach( $this->linksToContest as $link ) {
				$link->delete();
			}
		}
			# contestsMembers
		function deleteContestsMembers() {
			foreach( $this->contestsMembers as $member ) {
				$member->delete();
			}
		}
		
			# EAStatements
		function deleteEAStatements() {
			foreach( $this->EAStatements as $statsment ) {
				$statsment->delete();
			}
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->unlinkFromContests();
			$this->deleteContestsMembers();
			$this->deleteEAStatements();
		}
	}

?>