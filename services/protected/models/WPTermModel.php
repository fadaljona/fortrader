<?
	Yii::import( 'models.base.ModelBase' );
	
	final class WPTermModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "wp_terms";
		}
		function relations() {
			return Array(
				//'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				//'answer' => Array( self::HAS_ONE, 'BrokerQuestionModel', 'idQuestion' ),
			);
		}
		function getCategoryURL() {
			return Yii::app()->baseUrl."/../{$this->slug}/";
		}
	}

?>