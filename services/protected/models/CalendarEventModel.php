<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CalendarEventModel extends ModelBase {
		const PATHUploadDir = 'uploads/calendar';
		const WIDTHMiddle = 230;
		const HEIGHTMiddle = 'auto';
		const WIDTHThumb = 46;
		const HEIGHTThumb = 'auto';
		public $image;
		public $nameImage;
		public $nameMiddleImage;
		public $nameThumbImage;
		public $dateForSearch;
		public $indicatorIDForSearch;
		public $indicatorForSearch;
		public $hasNews;
		public $hasDesc;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getTitlesSerious() {
			return Array(
				0 => 'not',
				1 => 'low',
				2 => 'medium',
				3 => 'high',
			);
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'CalendarEventI18NModel', 'idEvent', 'alias' => 'i18nsCalendarEvent' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'CalendarEventI18NModel', 'idEvent', 
					'alias' => 'cLI18NCalendarEvent',
					'on' => '`cLI18NCalendarEvent`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'descriptions' => Array( self::HAS_MANY, 'CalendarEventDescModel', Array( 'idIndicator' => 'indicator_id' )),
				'currentLanguageDescription' => Array( self::HAS_ONE, 'CalendarEventDescModel', Array( 'idIndicator' => 'indicator_id' ),
					'on' => '`currentLanguageDescription`.`idLanguage` = :idLanguage2',
					'params' => Array(
						':idLanguage2' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		static function genID() {
			$query = "
				SELECT 	LEAST( MIN( `id` ), 0 )
				FROM	`{{calendar_event}}`
			";
			$min = (int)Yii::App()->db->createCommand( $query )->queryScalar();
			return $min - 1;
		}
		static function getListURL() {
			return Yii::App()->createURL( 'calendarEvent/list' );
		}
		static function getModelSingleURL( $id ) {
			return Yii::App()->createURL( 'calendarEvent/single', Array( 'id' => $id ));
		}
		static function getModelAdminURL( $id ) {
			return Yii::App()->createURL( 'admin/calendarEvent/list', Array( 'idEdit' => $id ));
		}
		static function getModelTitleSerious( $serious ) {
			$titles = self::getTitlesSerious();
			return @$titles[ $serious ];
		}
		static function getModelNumSerious( $title ) {
			$titles = self::getTitlesSerious();
			return array_search( $title, $titles );
		}
		static function getCountries() {
			$query = "
				SELECT
					`{{calendar_event_i18n}}`.`idCountry`,
					`{{calendar_event_i18n}}`.`country`
				FROM
					`{{calendar_event_i18n}}`
				WHERE
					`{{calendar_event_i18n}}`.`idLanguage` = :idLanguage
					AND `{{calendar_event_i18n}}`.`idCountry` IS NOT NULL
				GROUP BY
					`{{calendar_event_i18n}}`.`idCountry`
				ORDER BY
					`{{calendar_event_i18n}}`.`country`
			";
			$countries = Yii::App()->db->createCommand( $query )->setFetchMode( PDO::FETCH_OBJ )->queryAll( true, Array(
				':idLanguage' => LanguageModel::getCurrentLanguageID(),
			));
			$countries = CommonLib::toAssoc( $countries, 'idCountry', 'country' );
			return $countries;
		}
		function onChangeIndicatorID( $old_indicator_id ) {
			Yii::App()->db->createCommand("
				UPDATE
					`{{calendar_event}}`
				SET
					`{{calendar_event}}`.`indicator_id` = :indicator_id
				WHERE
					`{{calendar_event}}`.`indicator_id` = :old_indicator_id
					AND EXISTS(
						SELECT
							1
						FROM
							`{{calendar_event_i18n}}`
						WHERE
							`{{calendar_event_i18n}}`.`idEvent` = `{{calendar_event}}`.`id`
							AND `{{calendar_event_i18n}}`.`idLanguage` = :idLanguage
							AND `{{calendar_event_i18n}}`.`indicator` = :indicator
						LIMIT
							1
					)
			")->query( Array(
				':indicator_id' => $this->indicator_id,
				':old_indicator_id' => $old_indicator_id,
				':idLanguage' => LanguageModel::getCurrentLanguageID(),
				':indicator' => $this->indicator,
			));
		}
		function getSingleURL() {
			return self::getModelSingleURL( $this->id );
		}
		function getAdminURL() {
			return self::getModelAdminURL( $this->id );
		}
		function getI18N( $idLanguage = null ) {
			if( $idLanguage !== null ) {
				foreach( $this->i18ns as $i18n ) {
					if( $i18n->idLanguage == $idLanguage ) {
						return $i18n;
					}
				}
			}
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->indicator )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->indicator )) return $i18n;
			}
		}
		function getIndicator() {
			$i18n = $this->getI18N();
			$res = $i18n ? $i18n->indicator : '';
			return $this->getPropName($res);
		}
		function getPropName($res) {
			if ($res != '' and Yii::app()->language == 'ru') {
				if ($this->isCirilic($res)) {
					return $res;
				}else{
					$bmodel = self::getBeforeDescModel($this->id, $this->indicator_id);
					if ( $bmodel != null ) {
						return $bmodel->getIndicator();
					}
				}
			}
			if ($res != '' and Yii::app()->language == 'en_us') {
				if (!$this->isCirilic($res)) {
					return $res;
				}else{
					$bmodel = self::getBeforeDescModel($this->id, $this->indicator_id);
					if ( $bmodel != null ) {
						return $bmodel->getIndicator();
					}
				}
			}
			return $res;
		}
		function isCirilic( $str ) {
			if (preg_match("/[а-я]/i", $str)) {
				return true;
			}
			return false;
		}
		function getLinkedCountry() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->linkedCountry : '';
		}
		function getCountry() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->country : '';
		}
		/*function getBeforeValue() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->before_value : '';
		}*/
		function getBeforeValue() {
			$bmodel = self::getBeforeDescModel($this->id, $this->indicator_id);
			if ($bmodel and $bmodel != null ) {
				return $bmodel->getFactValue();
			}
			return false;
		}
		/*function getBeforeValueSel() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->before_value_sec : '';
		}*/
		function getBeforeValueSel() {
			$bmodel = self::getBeforeDescModel($this->id, $this->indicator_id);
			if ($bmodel and $bmodel != null ) {
				return $bmodel->getFactValueSel();
			}
			return false;
		}
		public static function getBeforeDescModel($id, $indicator_id) {
			if ( $id != '' and $indicator_id !='' ) {
				$query = "
					SELECT 	 MAX( `id` )
					FROM	`{{calendar_event}}`
					WHERE indicator_id = ".$indicator_id." and id < ".$id."
				";
				$id = (int)Yii::App()->db->createCommand( $query )->queryScalar();
				$model = self::model()->findByPk($id);
				if ( $model != null ) {
					return $model;
				}
			}
			return false;
		}
		function getBeforeValueHtml() {
			$i18n = $this->getI18N();
			if ( $this->getBeforeClass() ) {
				return $i18n ? '<div class="'.$this->getBeforeClass().'">'.$i18n->before_value.$this->getBeforeTooltip().'</div>' : '';
			}else{
				return $i18n ? '<div>'.$i18n->before_value.'</div>' : '';
			}
		}
		function getMbValue() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->mb_value : '';
		}
		function getMbValueSel() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->mb_value_sec : '';
		}
		function getFactValue() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->fact_value : '';
		}
		function getFactValueSel() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->fact_value_sec : '';
		}
		function getFactValueHtml() {
			$i18n = $this->getI18N();
			if ( $this->getFactClass() ) {
				return $i18n ? '<div class="'.$this->getFactClass().'">'.$i18n->fact_value.$this->getFactTooltip().'</div>' : '';
			}else{
				return $i18n ? '<div>'.$i18n->fact_value.'</div>' : '';
			}
		}
		function getFactClass()
		{
			$mb = $this->getNumValue($this->getMbValue());
			$fact = $this->getNumValue($this->getFactValue());
			if ( $mb != '' and $fact != '') {
				if ( $fact > $mb ) {
					return ' better_news_par ';
				}elseif($fact < $mb){
					return ' worse_news_par ';
				}
			}
			return false;
		}
		function getFactTooltip( ) {
			$sign = '';
			$mb = $this->getNumValue($this->getMbValue());
			$fact = $this->getNumValue($this->getFactValue());
			$delta = $fact - $mb;
			$delta1 = str_replace($this->getNumValue($this->getFactValue()), $delta, $this->getFactValue());
			if ( $delta > 0 ) {
				$sign = '+';
				if (Yii::app()->language == 'ru') {
					return '<div class="tbl_tt"><b>Лучше,</b><br/>показатель вышел лучше предыдущего значения на '.$sign.$delta1.'</div>';
				}elseif(Yii::app()->language == 'en_us'){
					return '<div class="tbl_tt"><b>Better,</b><br/>indicator better than the previous value of '.$sign.$delta1.'</div>';
				}
			}else{
				if (Yii::app()->language == 'ru') {
					return '<div class="tbl_tt"><b>Хуже,</b><br/>показатель вышел хуже предыдущего значения на '.$sign.$delta1.'</div>';
				}elseif(Yii::app()->language == 'en_us'){
					return '<div class="tbl_tt"><b>Worse,</b><br/>the rate worse than the previous value of '.$sign.$delta1.'</div>';
				}
			}
		}
		function getBeforeTooltip( ) {
			$sign = '';
			$mb = $this->getNumValue($this->getMbValue());
			$befor = $this->getNumValue($this->getBeforeValue());
			$delta = $befor - $mb;
			$delta1 = str_replace($this->getNumValue($this->getBeforeValue()), $delta, $this->getBeforeValue());
			if ( $delta > 0 ) {
				$sign = '+';
				if (Yii::app()->language == 'ru') {
					return '<div class="tbl_tt"><b>Лучше,</b><br/>показатель вышел лучше предыдущего значения на '.$sign.$delta1.'</div>';
				}elseif(Yii::app()->language == 'en_us'){
					return '<div class="tbl_tt"><b>Better,</b><br/>indicator better than the previous value of '.$sign.$delta1.'</div>';
				}
			}else{
				if (Yii::app()->language == 'ru') {
					return '<div class="tbl_tt"><b>Хуже,</b><br/>показатель вышел хуже предыдущего значения на '.$sign.$delta1.'</div>';
				}elseif(Yii::app()->language == 'en_us'){
					return '<div class="tbl_tt"><b>Worse,</b><br/>the rate worse than the previous value of '.$sign.$delta1.'</div>';
				}
			}
		}
		function getBeforeClass()
		{
			$mb = $this->getNumValue($this->getMbValue());
			$befor = $this->getNumValue($this->getBeforeValue());
			if ( $mb != '' and $befor != '') {
				if ( $befor > $mb) {
					return ' better_news_par ';
				}elseif($befor < $mb){
					return ' worse_news_par ';
				}
			}
			return false;
		}
		function getNumValue($value)
		{
			$value = preg_replace( "#[^0-9\-\.]#", "", $value );
			return $value;
		}
		function getTitle($value)
		{
			$res = str_replace($this->getNumValue($value), '', $value);
			return $res;
		}

		function getTitleFirst()
		{
			$res = str_replace($this->getNumValue($this->getFactValue()), '', $this->getFactValue());
			if ( $res != '' ) {
				return $res;
			}else{
				return $this->getTitleComplFirst();
			}
		}
		function getTitleSel()
		{
			$res = str_replace($this->getNumValue($this->getFactValueSel()), '', $this->getFactValueSel());
			if ( $res != '' ) {
				return $res;
			}else{
				return $this->getTitleComplSel();
			}
		}
		function getTitleComplFirst( ) {
			$criteria = new CDbCriteria();
			$criteria->condition = 'indicator_id = '.$this->indicator_id;
			$criteria->order = 'dt';
			$data = CalendarEventModel::model()->findAll($criteria);
			foreach ($data as $one) {
				$res = str_replace($one->getNumValue($one->getFactValue()), '', $one->getFactValue());
				if ( $res != '' ) {
					return $res;
				}
			}
			return false;
		}
		function getTitleComplSel( ) {
			$criteria = new CDbCriteria();
			$criteria->condition = 'indicator_id = '.$this->indicator_id;
			$criteria->order = 'dt';
			$data = CalendarEventModel::model()->findAll($criteria);
			foreach ($data as $one) {
				$res = str_replace($one->getNumValue($one->getFactValueSel()), '', $one->getFactValueSel());
				if ( $res != '' ) {
					return $res;
				}
			}
			return false;
		}
		function isHaveFirst() {
			$criteria = new CDbCriteria();
			$criteria->condition = 'indicator_id = '.$this->indicator_id;
			$criteria->order = 'dt';
			$data = CalendarEventModel::model()->findAll($criteria);

			$s = '';
			
			foreach ($data as $one) {
				$s .= $one->getFactValue();
			}
			if ( $s != '' ) {
				return true;
			}
			return false;
		}
		function isHaveSel() {
			$criteria = new CDbCriteria();
			$criteria->condition = 'indicator_id = '.$this->indicator_id;
			$criteria->order = 'dt';
			$data = CalendarEventModel::model()->findAll($criteria);

			$s1 = '';

			foreach ($data as $one) {
				$s1 .= $one->getFactValueSel();
			}
			if ( $s1 != '' ) {
				return true;
			}
			return false;
		}
		function filterFactValue() {
			$value = $this->getFactValue();
			$value = preg_replace( "#[^0-9\-\.]#", "", $value );
			return $value;
		}
		function getTitleFactValue() {
			$value = $this->getFactValue();
			$len = mb_strlen( $value );
			
			$fint = false;
			for( $i = 0; $i < $len; $i++ ) {
				$char = mb_substr( $value, $i, 1 );
				if( preg_match( "#^[0-9]$#", $char )) {
					$fint = true;
					continue;
				}
				if( $fint and !in_array( $char, Array( " ", ".", "-" ))) {
					return $char;
				}
			}
		}
		function getTimeLeft() {
			$time = strtotime( $this->dt );
			$time -= time();
			
			if( $time < 0 ) {
				return Yii::t( '*', 'Done' );
			}
			
			list( $seconds, $minets, $hours, $days, $months, $years ) = CommonLib::explodeTimeDiff( $time );
			
			if( $seconds == 0 ) {
				return Yii::t( '*', 'Done' );
			}
			elseif( $minets == 0 ) {
				$_sec = Yii::t( '*', 'sec' );
				return "{$seconds} {$_sec}";
			}
			elseif( $hours == 0 ) {
				$_min = Yii::t( '*', 'min' );
				return "{$minets} {$_min}";
			}
			elseif( $days == 0 ) {
				$_hours = Yii::t( '*', 'h' );
				$_min = Yii::t( '*', 'min' );
				$_minets = $minets % 60;
				return "{$hours} {$_hours} {$_minets} {$_min}";
			}
			elseif( $months == 0 ) {
				$_days = Yii::t( '*', 'day|days', $days );
				return "{$days} {$_days}";
			}
			elseif( $years == 0 ) {
				$_months = Yii::t( '*', 'month|months', $months );
				return "{$months} {$_months}";
			}
			else{
				return "{$years} years";
			}
		}
		function getTimeLeftToBtn() {
			$time = strtotime( $this->dt );
			$time -= time();
			
			if( $time < 0 ) {
				return Yii::t( '*', 'Done' );
			}
			
			list( $seconds, $minets, $hours, $days, $months, $years ) = CommonLib::explodeTimeDiff( $time );
			
			if( $seconds == 0 ) {
				return Yii::t( '*', 'Done' );
			}
			elseif( $minets == 0 ) {
				$_sec = $seconds % 60;
				return "0:0:{$_sec}";
			}
			elseif( $hours == 0 ) {
				$_min = Yii::t( '*', 'min' );
				$_minets = $minets % 60;
				$_sec = $seconds % 60;
				return "0:{$_minets}:{$_sec}";
			}
			elseif( $days == 0 ) {
				$_hours = Yii::t( '*', 'h' );
				$_min = Yii::t( '*', 'min' );
				$_minets = $minets % 60;
				$_sec = $seconds % 60;
				return "{$hours}:{$_minets}:{$_sec}";
			}
			elseif( $months == 0 ) {
				$_days = Yii::t( '*', 'day|days', $days );
				return "{$days} {$_days}";
			}
			elseif( $years == 0 ) {
				$_months = Yii::t( '*', 'month|months', $months );
				return "{$months} {$_months}";
			}
			else{
				return "{$years} years";
			}
		}
		function getPeriod() {
			$time = strtotime( $this->dt );
			return date('M-d', $time);
		}
		function getDescription() {
			return @$this->currentLanguageDescription->description;
		}
		function getNameImage() {
			return @$this->currentLanguageDescription->nameImage;
		}
		function getNameMiddleImage() {
			return @$this->currentLanguageDescription->nameMiddleImage;
		}
		function getNameThumbImage() {
			return @$this->currentLanguageDescription->nameThumbImage;
		}
		function getTitleSerious() {
			return self::getModelTitleSerious( $this->serious );
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				$i18n = CalendarEventI18NModel::model()->findByAttributes( Array( 'idEvent' => $this->id, 'idLanguage' => $idLanguage ));
				if( !$i18n ) {
					$i18n = CalendarEventI18NModel::instance( $this->id, $idLanguage, $obj->indicator );
				}
				$i18n->indicator = $obj->indicator;
				$i18n->country = $obj->country;
				$i18n->period = $obj->period;
				$i18n->before_value = $obj->before_value;
				$i18n->before_value_sec = $obj->before_value_sec;
				$i18n->mb_value = $obj->mb_value;
				$i18n->mb_value_sec = $obj->mb_value_sec;
				$i18n->fact_value = $obj->fact_value;
				$i18n->fact_value_sec = $obj->fact_value_sec;
				
				$i18n->save();
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}

			# descriptions
		function deleteDescriptions() {
			foreach( $this->descriptions as $description ) {
				$description->delete();
			}
		}
		function addDescriptions( $descriptions, $nameImage='', $nameMiddleImage='', $nameThumbImage='' ) {
			foreach( $descriptions as $idLanguage=>$description ) {
				if( !strlen( $description )) continue;
				
				$model = CalendarEventDescModel::model()->findByAttributes( Array( 'idIndicator' => $this->indicator_id, 'idLanguage' => $idLanguage ));
				if( !$model ) {
					$model = CalendarEventDescModel::instance( $this->indicator_id, $idLanguage );
				}
				$model->description = $description;
				
				$model->save();
			}

			$model = CalendarEventDescModel::model()->findAll( 'idIndicator='.$this->indicator_id);
			
			foreach ($model as $one) {
				$one->nameImage = $nameImage;
				$one->nameMiddleImage = $nameMiddleImage;
				$one->nameThumbImage = $nameThumbImage;

				$one->save();
			}
		}
		function setDescriptions( $descriptions, $nameImage='', $nameMiddleImage='', $nameThumbImage=''  ) {
			$this->deleteDescriptions();
			$this->addDescriptions( $descriptions, $nameImage, $nameMiddleImage, $nameThumbImage );
		}
			
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( !$this->id ) $this->id = self::genID();
			}
			return $result;
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			$this->deleteDescriptions();
		}

		function formatName() {
			$out = "";
			
			if( $this->linkedCountry ) {
				$out .= $this->linkedCountry->getIcon();
			}
			elseif( strlen( $this->country )){
				$out .= "({$this->country}) ";
			}
			
			$inner = CHtml::encode( $this->indicator );
			
			if( $this->hasNews ) {
				$inner .= CHtml::tag( 'i', Array( 'class' => "iI i44", 'title' => Yii::t( $this->NSi18n, "Has news" )), '', true );
			}
			if( $this->hasDesc ) {
				$inner .= CHtml::tag( 'i', Array( 'class' => "iI i45", 'title' => Yii::t( $this->NSi18n, "Has description" )), '', true );
			}
			
			$out .= CHtml::link( $inner, $this->getSingleURL() );
			
			return $out;
		}
			# image
		function getPathImage() {
			return strlen($this->nameImage) ? self::PATHUploadDir."/{$this->nameImage}" : '';
		}
		function getSrcImage() {
			return $this->pathImage ? Yii::app()->baseUrl."/{$this->pathImage}" : '';
		}
		function getPathMiddleImage() {
			return strlen($this->nameMiddleImage) ? self::PATHUploadDir."/{$this->nameMiddleImage}" : '';
		}
		function getSrcMiddleImage() {
			return $this->pathMiddleImage ? Yii::app()->baseUrl."/{$this->pathMiddleImage}" : '';
		}
		function getPathThumbImage() {
			return strlen($this->nameThumbImage) ? self::PATHUploadDir."/{$this->nameThumbImage}" : '';
		}
		function getSrcThumbImage() {
			return $this->pathThumbImage ? Yii::app()->baseUrl."/{$this->pathThumbImage}" : '';
		}
		function deleteImage() {
			if( strlen( $this->nameImage )) {
				if( is_file( $this->pathImage ) and !@unlink( $this->pathImage )) throw new Exception( "Can't delete {$this->pathImage}" );
				$this->nameImage = null;
			}
			if( strlen( $this->nameMiddleImage )) {
				if( is_file( $this->pathMiddleImage ) and !@unlink( $this->pathMiddleImage )) throw new Exception( "Can't delete {$this->pathMiddleImage}" );
				$this->nameMiddleImage = null;
			}
			if( strlen( $this->nameThumbImage )) {
				if( is_file( $this->pathThumbImage ) and !@unlink( $this->pathThumbImage )) throw new Exception( "Can't delete {$this->pathThumbImage}" );
				$this->nameThumbImage = null;
			}
		}
		function setImage( $name, $nameMiddle, $nameThumb ) {
			$this->deleteImage();
			$this->nameImage = $name;
			$this->nameMiddleImage = $nameMiddle;
			$this->nameThumbImage = $nameThumb;
		}
		function uploadImage( $uploadedFile ) {
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );

			$middleFileFullName = "{$fileName}-middle.{$fileEx}";
			$middleFilePath = self::PATHUploadDir."/{$middleFileFullName}";
			ImgResizeLib::resize( $filePath, $middleFilePath, self::WIDTHMiddle, self::HEIGHTMiddle );
			
			$thumbFileFullName = "{$fileName}-thumb.{$fileEx}";
			$thumbFilePath = self::PATHUploadDir."/{$thumbFileFullName}";
			ImgResizeLib::resize( $filePath, $thumbFilePath, self::WIDTHThumb, self::HEIGHTThumb );
			
			$this->setImage( $fileFullName, $middleFileFullName, $thumbFileFullName );
		}
		function getMiddleImage( $options = Array() ) {
			$options[ 'src' ] = $this->srcMiddleImage;
			$options[ 'title' ] = $this->brandName;
			return $this->nameMiddleImage ? CHtml::tag( 'img', $options ) : '';
		}
		function getThumbImage( $options = Array() ) {
			$options[ 'src' ] = $this->srcThumbImage;
			if( empty( $options[ 'title' ])) $options[ 'title' ] = $this->brandName;
			return $this->nameThumbImage ? CHtml::tag( 'img', $options ) : '';
		}
	}

?>
