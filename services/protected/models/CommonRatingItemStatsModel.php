<?php
	Yii::import( 'models.base.ModelBase' );
	
	final class CommonRatingItemStatsModel extends ModelBase {
	
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
        
        public static function updateStats(){

			/*$exists = Yii::App()->db->createCommand("
				SELECT 		1
				FROM 		`{{common_rating_item_stats}}`
				WHERE 		`updatedDT` IS NULL OR DATE_ADD(`updatedDT`, INTERVAL 5 MINUTE) < NOW()
				LIMIT		1
			")->queryScalar();
			
        
			if( $exists ) {*/
                
				$command = file_get_contents( "protected/data/updateCommonRatingItemStats.sql" );
				Yii::App()->db->createCommand( $command )->query();
			//}
        }
	}

?>