<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CompanyModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'servers' => Array( self::HAS_MANY, 'ServerModel', 'idCompany', 'order' => '`servers`.`name`' ),
			);
		}
		function deleteServers() {
			foreach( $this->servers as $model ) {
				$model->delete();
			}
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteServers();
		}
	}

?>
