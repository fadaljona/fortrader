<?php

class QuotesPanelModel extends ModelBase{
	static function model( $class = __CLASS__ ) {
		return parent::model( $class );
	}

	public function tableName(){
		return '{{quotes_panel}}';
	}
	
	public static function setState(){
		$idUser=Yii::app()->user-id;
		if(!$idUser)
			return false;

		$setting=self::model()->find(array(
			'select'=>'`ids`,`time_update_panel`',
			'condition'=>'`idUser`=:idUser',
			'params'=>array(
				':idUser'=>$idUser
			)
		));
		if($setting->ids) {
			$idsPanel=unserialize($setting->ids);
			$timer=$setting->time_update_panel;
		}else{
			$idsPanel=QuotesSymbolsModel::getIdsPanelDefault();
			
			$quotesSettings = QuotesSettingsModel::getInstance();
			
			$timer=$quotesSettings->time_update_panel;
		}
		
		Yii::app()->user->setState( "panel.ids", $idsPanel);
		Yii::app()->user->setState( "panel.time_update_panel", $timer);
	}
	
	public static function clearState(){
		Yii::app()->user->setState( "panel.ids", null);
		Yii::app()->user->setState( "panel.time_update_panel", null);
		Yii::app()->user->setState( "panel.default", null);
	}
	
	public static function saveTimer(){
		if($idUser=Yii::app()->user-id){
			if($panel=self::model()->find('idUser='.$idUser)){
				if($timer=Yii::app()->user->getState("panel.time_update_panel"))
					$panel->time_update_panel=$timer;
				$panel->update();
			}else{
				$panel=new QuotesPanelModel();
				if($timer=Yii::app()->user->getState("panel.time_update_panel"))
					$panel->time_update_panel=$timer;
				$panel->idUser=$idUser;
				$panel->save();
			}
		}
	}
	
	public static function saveIds(){
		if($idUser=Yii::app()->user-id){
			if($panel=self::model()->find('idUser='.$idUser)){
				if($timer=Yii::app()->user->getState("panel.ids"))
					$panel->ids=serialize(Yii::app()->user->getState("panel.ids"));
				$panel->update();
			}else{
				$panel=new QuotesPanelModel();
				if($timer=Yii::app()->user->getState("panel.ids"))
					$panel->ids=serialize(Yii::app()->user->getState("panel.ids"));
				$panel->idUser=$idUser;
				$panel->save();
			}
		}
	}

} 