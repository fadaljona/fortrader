<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UnsubscribeCommentModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'mailingType' => Array( self::BELONGS_TO, 'MailingTypeModel', 'idMailingType' ),
			);
		}
	}

?>