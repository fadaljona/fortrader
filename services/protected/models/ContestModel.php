<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestModel extends ModelBase {

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'servers' => Array( self::MANY_MANY, 'ServerModel', '{{server_to_contest}}(idContest,idServer)', 'order' => '`servers`.`name`' ),
				'linksToServers' => Array( self::HAS_MANY, 'ServerToContestModel', 'idContest' ),

				'members' => Array( self::HAS_MANY, 'ContestMemberModel', 'idContest' ),
				'countMembers' => Array( self::STAT, 'ContestMemberModel', 'idContest' ),
				'countMembersToday' => Array( self::STAT, 'ContestMemberModel', 'idContest', 'condition' => " DATE( `createdDT` ) = CURDATE() " ),
				'winers' => Array( self::HAS_MANY, 'ContestWinerModel', 'idContest', 'order' => '`winers`.`place`' ),
				'i18ns' => Array( self::HAS_MANY, 'ContestI18NModel', 'idContest', 'alias' => 'i18nsContest' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'ContestI18NModel', 'idContest', 
					'alias' => 'cLI18NContest',
					'on' => '`cLI18NContest`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		public static function findBySlug( $slug ){
			return self::model()->find(array(
				'condition' => " `slug` = :slug ",
				'params' => array( ':slug' => $slug ),
			));
		}

		static function getListDp($page = 0) {
			$DP = new CActiveDataProvider( 'ContestModel', array(
				'criteria' => array(
					//'condition' => " `t`.`approved` = 1 ",
					'with' => array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`begin` DESC, `cLI18NContest`.`name` ASC'
				),
				'pagination' => array(
				  'pageSize' => 6,
				  'pageVar' => 'contestTablePage',
				  'currentPage' => $page,
				)
			));
			return $DP;
		}

		/**
		 * Returns status HTML string
		 */
		public function getStatusSpan() {
			//$i18n = $this->getI18N();
			$class = '';
			if ($this->status == 'completed') {
				$class = 'contest-status_done';
			} elseif ($this->status == 'started') {
				$class = 'contest-status_passes';
			} elseif ($this->status == 'registration') {
				$class = 'contest-status_checkin';
			}
			return CHtml::openTag('span', array('class' => 'contest-status '.$class)) . Yii::t($NSi18n, $this->status) . CHtml::closeTag('span');
		}
		
		static function getSlugById( $id ){
			$model = self::model()->findByPk( $id );
			if( !$model ) return false;
			return $model->slug;
		}
		static function getListURL() {
			return Yii::App()->createURL( 'contest/list' );
		}
		static function getRegisterMemberURL( $idContest ) {
			return Yii::App()->createURL( 'contest/registerMember', Array( 'idContest' => $idContest ));
		}
		static function getModelAbsoluteSingleURL( $slug ) {
			return Yii::App()->createAbsoluteURL( 'contest/single', Array( 'slug' => $slug ));
		}
		static function getModelSingleURL( $slug ) {
			return Yii::App()->createURL( 'contest/single', Array( 'slug' => $slug ));
		}
			# fields
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->name )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->name )) return $i18n;
			}
		}
		function getMembersCount(){
			return Yii::App()->db->createCommand(" 
				SELECT 	count( `id` )
				FROM	`{{contest_member}}`
				WHERE `idContest` = :idContest
				;
			")->queryScalar( array( ':idContest' => $this->id ) );
		}
		function getName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->name : '';
		}
		function getUrlRules() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->urlRules : '';
		}
		function getCountPrizes() {
			$count = strlen( $this->prizes ) ? substr_count( $this->prizes, "," ) + 1 : 0;
			return $count;
		}
		function getFirstPrize() {
			$prizes = strlen( $this->prizes ) ? explode( ',', $this->prizes ) : Array();
			return $prizes ? $prizes[0] : null;
		}
		function getSumPrizes() {
			$prizes = strlen( $this->prizes ) ? explode( ',', $this->prizes ) : Array();
			return array_sum( $prizes );
		}
		function getAbsoluteSingleURL() {
			return self::getModelAbsoluteSingleURL( $this->slug );
		}
		function getSingleURL() {
			return self::getModelSingleURL( $this->slug );
		}
		function getImage( $params = Array() ) {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->getImage( $params ) : '';
		}
		function getSrcImage( $params = Array() ) {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->getSrcImage( $params ) : '';
		}
		function getSizedSrcImage( $width, $height ) {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->getSizedSrcImage( $width, $height ) : '';
		}
		function getTitle(){
			$i18n = $this->getI18N();
			return $i18n ? $i18n->pageTitle : '';
		}
		function getMetaTitle(){
			$i18n = $this->getI18N();
			return $i18n ? $i18n->pageSeoTitle : '';
		}
		function getMetaDesc(){
			$i18n = $this->getI18N();
			return $i18n ? $i18n->seoDesc : '';
		}
		function getMetaKeys(){
			$i18n = $this->getI18N();
			return $i18n ? $i18n->seoKeys : '';
		}
		function getRules(){
			$i18n = $this->getI18N();
			return $i18n ? json_decode( $i18n->rules ) : '';
		}
		function getDescription(){
			$i18n = $this->getI18N();
			if( !$i18n ) return $this->name;
			if( !$i18n->description ) return $this->name;
			return $i18n->description;
		}

        /**
         * @return mixed|null|void
         */
        function getStatus () {
            return $this->status;
        }

        /**
         * Get winners of the contest
         *
         * @return mixed|null|void
         */
        function getWinners () {
            return $this->winers;
        }

		function viewed() {
			$this->views++;
			$this->update( Array( 'views' ));
		}
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $model ) {
				$model->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if( !in_array( $idLanguage, $idsLanguages )) continue;

				$i18n = ContestI18NModel::model()->findByAttributes( Array( 'idContest' => $this->id, 'idLanguage' => $idLanguage ));
				if( !$i18n ) {
					$i18n = ContestI18NModel::instance( $this->id, $idLanguage, $obj->name, $obj->description, $obj->urlRules, $obj->fullDesc, $obj->rules, $obj->pageTitle, $obj->pageSeoTitle, $obj->seoKeys, $obj->seoDesc );
				}
				else {
					$i18n->name = $obj->name;
					$i18n->description = $obj->description;
					$i18n->urlRules = $obj->urlRules;
					
					$i18n->fullDesc = $obj->fullDesc;
					$i18n->rules = $obj->rules;
					$i18n->pageTitle = $obj->pageTitle;
					$i18n->pageSeoTitle = $obj->pageSeoTitle;
					$i18n->seoKeys = $obj->seoKeys;
					$i18n->seoDesc = $obj->seoDesc;
				}
				if( $obj->image ) $i18n->uploadImage( $obj->image );
				$i18n->save();
			}
		}
		function setI18Ns( $i18ns ) {
			$this->addI18Ns( $i18ns );
		}

			# servers
		function getIDsServers() {
			$ids = CommonLib::slice( $this->servers, 'id' );
			return $ids;
		}
		function unlinkFromServers() {
			foreach( $this->linksToServers as $link ) {
				$link->delete();
			}
		}
		function linkToServers( $ids ) {
			foreach( $ids as $idServer ) {
				$link = ServerToContestModel::model()->findByAttributes( Array( 'idServer' => $idServer, 'idContest' => $this->id ));
				if( !$link ) {
					$link = ServerToContestModel::instance( $idServer, $this->id );
					$link->save();
				}
			}
		}
		function setServers( $ids ) {
			$oldIDs = $this->getIDsServers();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromServers();
				$this->linkToServers( $ids );
				
				if( $ids ) {
					$impIds = implode( ',', $ids );
					Yii::App()->db->createCommand("
						UPDATE	`{{contest_member}}`
						SET		`idServer` = :idServer
						WHERE	`idContest` = :idContest
							AND `idServer` NOT IN ( {$impIds} )
					")->query( Array(
						':idServer' => $ids[0],
						':idContest' => $this->id,
					));
				}
			}
		}
			# controll members
		function completeMembers() {
			Yii::App()->db->createCommand("
				UPDATE		`{{contest_member}}`
				SET			`status` = 'completed'
				WHERE		`idContest` = :idContest
					AND 	`status` = 'participates'
			")->query( Array( 
				':idContest' => $this->id
			));
		}
		function uncompleteMembers() {
			Yii::App()->db->createCommand("
				UPDATE		`{{contest_member}}`
				SET			`status` = 'participates'
				WHERE		`idContest` = :idContest
					AND 	`status` = 'completed'
			")->query( Array( 
				':idContest' => $this->id
			));
		}

			# members
		static function getContestMemberByContestId ($id) {
            $contestMember = ContestMemberModel::model()->findByAttributes( Array( 'idContest' => $id, 'idUser' => Yii::app()->user->id));
            return $contestMember ? $contestMember : false;
        }
		function deleteMembers() {
			foreach( $this->members as $member ) {
				$member->delete();
			}
		}
			# winers
		function deleteWiners() {
			foreach( $this->winers as $winer ) {
				$winer->delete();
			}
		}
		function getBrokers() {
			$brokers = Array();
			
			foreach( $this->servers as $server ) {
				if( !in_array( $server->broker, $brokers )) $brokers[] = $server->broker;
			}
			
			return $brokers;
		}
		
		public function getAvailableLangs(){
			$langsArr = array();
			foreach( $this->i18ns as $i18n ){
				if( $i18n->name ){
					$langsArr[] = LanguageModel::getLangFromLangsFileByID( $i18n->idLanguage );
				}
			}
			return $langsArr;
		}
		
			# events
		protected function beforeSave() {
			if( $this->stopDeleteMMonitoring ){
				MT4AccountMonitorModel::clearData( $this->id );
			}
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.contest/single.'.$this->id.'.'  );
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.contest/list.NO.Param.' );
            return parent::beforeSave();
        }
		protected function afterDelete() {
			parent::afterDelete();
			$this->unlinkFromServers();
			$this->deleteI18Ns();

			$this->deleteMembers();
			$this->deleteWiners();
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.contest/single.'.$this->id.'.'  );
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.contest/list.NO.Param.' );
		}
	}

?>