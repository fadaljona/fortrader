<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerUserActivityModel extends ModelBase {
		const TYPE_EVENT_CREATE_REVIEW = 1;
		const TYPE_EVENT_CREATE_MARK = 2;
		const TYPE_EVENT_CREATE_LINK = 3;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'BrokerUserActivityI18NModel', 'idActivity', 'alias' => 'i18nsBrokerUserActivity' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'BrokerUserActivityI18NModel', 'idActivity', 
					'alias' => 'cLI18NBrokerUserActivity',
					'on' => '`cLI18NBrokerUserActivity`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'broker' => Array( self::BELONGS_TO, 'BrokerModel', 'idBroker' ),
			 );
		}
		static function instance( $idUser, $idBroker ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser', 'idBroker' ));
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->message )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->message )) return $i18n;
			}
		}
		function getMessage() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->message : '';
		}
		function getHTMLMessage( $maxLen = null ) {
			$message = $this->message;
			//$message = htmlspecialchars( $this->message );
			//$message = CommonLib::nl2br( $message );
			if( $maxLen ) $message = CommonLib::shorten( $message, $maxLen );
			return $message;
		}
			
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->message )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = BrokerUserActivityI18NModel::model()->findByAttributes( Array( 'idActivity' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = BrokerUserActivityI18NModel::instance( $this->id, $idLanguage, $obj->message );
						$i18n->save();
					}
					else{
						$i18n->message = $obj->message;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		function setI18NsFromMessage( $key, $ns = '*', $params = Array(), $paramsI18Ns = Array() ) {
			$message = MessageModel::model()->find(Array(
				'with' => 'i18ns',
				'condition' => "
					`t`.`key` = :key AND `t`.`ns` = :ns
				",
				'params' => Array(
					':key' => $key,
					':ns' => $ns,
				),
			));
			if( $message ) {
				$i18ns = Array();
				foreach( $message->i18ns as $i18n ) {
					$message = $i18n->value;
					if( $params ) $message = strtr( $message, $params );
					if( $paramsI18Ns and isset( $paramsI18Ns[ $i18n->idLanguage ])) $message = strtr( $message, $paramsI18Ns[ $i18n->idLanguage ]);
					$i18ns[ $i18n->idLanguage ] = (object)Array(
						'message' => $message,
					);
				}
				$this->setI18Ns( $i18ns );
			}
		}
			
			# events
		static function deleteExists( $idUser, $idBroker, $message ) {
			self::model()->deleteAll(Array(
				'condition' => " 
					`idUser` = :idUser
					AND `idBroker` = :idBroker
					AND EXISTS (
						SELECT 		1 
						FROM		`{{broker_user_activity_i18n}}`
						WHERE		`idActivity` = `id`
						AND			`message` LIKE :message
						LIMIT 		1
					)
				",
				'params' => Array(
					':idUser' => $idUser,
					':idBroker' => $idBroker,
					':message' => $message,
				),
			));
		}
		static function event( $type, $params ) {
			$settings = UserActivitySettingsModel::getModel();
			switch( $type ) {
				case self::TYPE_EVENT_CREATE_REVIEW:{
					if( $broker = BrokerModel::model()->findByPk( $params[ 'idBroker' ])) {
						$activity = self::instance( $params[ 'idUser' ], $params[ 'idBroker' ]);
						$activity->save();
						$url = $broker->getSingleURL();
						$_params = Array();
						$_params[ '{link}' ] = CHtml::link( $broker->shortName, $url );
						$activity->setI18NsFromMessage( 'Wrote a new review about the company {link}', '*', $_params );
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_BROKER_REVIEW, $params );
					}
					break;
				}
				case self::TYPE_EVENT_CREATE_MARK:{
					if( $broker = BrokerModel::model()->findByPk( $params[ 'idBroker' ])) {
						$activity = self::instance( $params[ 'idUser' ], $params[ 'idBroker' ]);
						$activity->save();
						$url = $broker->getSingleURL();
						$_params = Array();
						$_params[ '{link}' ] = CHtml::link( $broker->shortName, $url );
						$_params[ '{trust}' ] = "{$params['trust']}%";
						$activity->setI18NsFromMessage( 'Appraise the conditions {link} by {trust}', '*', $_params );
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_BROKER_MARK, $params );
					}
					break;
				}
				case self::TYPE_EVENT_CREATE_LINK:{
					if( $broker = BrokerModel::model()->findByPk( $params[ 'idBroker' ])) {
						self::deleteExists( $params[ 'idUser' ], $params[ 'idBroker' ], "Marked as a client company %" );
						$activity = self::instance( $params[ 'idUser' ], $params[ 'idBroker' ]);
						$activity->save();
						$url = $broker->getSingleURL();
						$_params = Array();
						$_params[ '{link}' ] = CHtml::link( $broker->shortName, $url );
						$activity->setI18NsFromMessage( 'Marked as a client company {link}', '*', $_params );
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_BROKER_LINK, $params );
					}
					break;
				}
			}
			return true;
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>