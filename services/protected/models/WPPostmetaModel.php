<?
	Yii::import( 'models.base.ModelBase' );
	
	final class WPPostmetaModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "wp_postmeta";
		}
		function relations() {
			return Array(
				//'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				//'answer' => Array( self::HAS_ONE, 'BrokerQuestionModel', 'idQuestion' ),
			);
		}
	}

?>