<?
	Yii::import( 'models.base.ModelBase' );
	
	final class TradeAccountStatsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'tradeAccount' => Array( self::BELONGS_TO, 'TradeAccountModel', 'idAccount' ),
			);
		}
	}
	
?>