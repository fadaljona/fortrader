<?
	Yii::import( 'models.base.ModelBase' );

	final class BrokerToTradePlatformModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idBroker, $idPlatform ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker', 'idPlatform' ));
		}
	}

?>