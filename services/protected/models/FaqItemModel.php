<?php
Yii::import('models.base.ModelBase');
Yii::import('models.interfaces.TextReplaceInterface');
Yii::import('models.traits.ReplaceItemsTrait');

final class FaqItemModel extends ModelBase implements TextReplaceInterface
{
    use ReplaceItemsTrait;
    
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function relations()
    {
        return array(
            'i18ns' => array( self::HAS_MANY, 'FaqItemI18NModel', 'idItem', 'alias' => 'i18nsFaqItem'),
            'faq' => array( self::HAS_ONE, 'FaqModel', array('id' => 'idFaq')),
            'currentLanguageI18N' => array( self::HAS_ONE, 'FaqItemI18NModel', 'idItem',
                'alias' => 'cLI18NFaqItem',
                'on' => '`cLI18NFaqItem`.`idLanguage` = :idLanguage',
                'params' => array(
                    ':idLanguage' => LanguageModel::getCurrentLanguageID(),
                ),
            ),
        );
    }

    public static function getListForFilter()
    {
        $models = self::model()->findAll(array( ));
        $returnArr = array();
        foreach ($models as $model) {
            $returnArr[$model->id] = $model->title;
        }
        return $returnArr;
    }

    public static function getAdminListDp($filterModel = false)
    {
        $condition = '';
        $params = array();
        if (Yii::app()->request->getParam('idFaq')) {
            $condition = " `t`.`idFaq` = :idFaq ";
            $params['idFaq'] = Yii::app()->request->getParam('idFaq');
        }
        $DP = new CActiveDataProvider(get_called_class(), array(
            'criteria' => array(
                'with' => array( 'currentLanguageI18N', 'i18ns' ),
                'order' => ' `t`.`order` DESC ',
                'condition' => $condition,
                'params' => $params
            ),
        ));
        return $DP;
    }

    public function getI18N()
    {
        if ($this->currentLanguageI18N) {
            return $this->currentLanguageI18N;
        }
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == 0) {
                if (strlen($i18n->question)) {
                    return $i18n;
                }
                break;
            }
        }
        foreach ($this->i18ns as $i18n) {
            if (strlen($i18n->question)) {
                return $i18n;
            }
        }
    }
        
    public function getQuestion()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->question : '';
    }

    public function getAnswer()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->answer : '';
    }
        
        
            # i18ns
    public function deleteI18Ns()
    {
        foreach ($this->i18ns as $i18n) {
            $i18n->delete();
        }
    }
        
    public function addI18Ns($i18ns)
    {
        $idsLanguages = LanguageModel::getExistsIDS();
        foreach ($i18ns as $idLanguage => $obj) {
            if ((strlen($obj->question) || strlen($obj->answer)) && in_array($idLanguage, $idsLanguages)) {
                $i18n = FaqItemI18NModel::model()->findByAttributes(array( 'idItem' => $this->id, 'idLanguage' => $idLanguage ));
                if (!$i18n) {
                    $i18n = FaqItemI18NModel::instance($this->id, $idLanguage, $obj->question, $obj->answer);
                    $i18n->save();
                } else {
                    $i18n->question = $obj->question;
                    $i18n->answer = $obj->answer;
                    $i18n->save();
                }
            }
        }
    }
    public function setI18Ns($i18ns)
    {
        $this->deleteI18Ns();
        $this->addI18Ns($i18ns);
    }

    protected function afterDelete()
    {
        parent::afterDelete();
        $this->deleteI18Ns();
    }

    protected function afterFind()
    {
        $this->replaceItems = unserialize($this->replaceItems);

        parent::afterFind();
    }

    protected function beforeSave()
    {
        $this->replaceItems = serialize($this->replaceItems);
        return parent::beforeSave();
    }

    //TextReplaceInterface

    public static function getPropertiesForReplace()
    {
        return [
            'answer',
            'question'
        ];
    }
}
