<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerModel extends ModelBase {
		const PATHUploadDir = 'uploads/brokers';
		const WIDTHMiddle = 230;
		const HEIGHTMiddle = 'auto';
		const WIDTHThumb = 46;
		const HEIGHTThumb = 'auto';
		const HEIGHTSponsor = 60;
		
		public $trueUpdatedDate;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'BrokerI18NModel', 'idBroker', 'alias' => 'i18nsBroker' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'BrokerI18NModel', 'idBroker', 
					'alias' => 'cLI18NBroker',
					'on' => '`cLI18NBroker`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getByAlias( 'ru' )->id,
					),
				),
				'servers' => Array( self::HAS_MANY, 'ServerModel', 'idBroker', 'order' => '`servers`.`name`' ),
				'users' => Array( self::HAS_MANY, 'UserModel', 'idBroker' ),
				'linksToUsers' => Array( self::HAS_MANY, 'BrokerToUserModel', 'idBroker' ),
				'smData' => Array( self::HAS_ONE, 'BrokerSmDataModel', 'idBroker' ),
				'country' => Array( self::BELONGS_TO, 'CountryModel', 'idCountry' ),
				'regulators' => Array( self::MANY_MANY, 'BrokerRegulatorModel', '{{broker_to_broker_regulator}}(idBroker,idRegulator)' ),
				'linksToRegulators' => Array( self::HAS_MANY, 'BrokerToBrokerRegulatorModel', 'idBroker' ),
				'tradePlatforms' => Array( self::MANY_MANY, 'TradePlatformModel', '{{broker_to_trade_platform}}(idBroker,idPlatform)' ),
				'linksToPlatforms' => Array( self::HAS_MANY, 'BrokerToTradePlatformModel', 'idBroker' ),
				'IOs' => Array( self::MANY_MANY, 'BrokerIOModel', '{{broker_to_broker_io}}(idBroker,idIO)' ),
				'linksToIOs' => Array( self::HAS_MANY, 'BrokerToBrokerIOModel', 'idBroker' ),
				'instruments' => Array( self::MANY_MANY, 'BrokerInstrumentModel', '{{broker_to_broker_instrument}}(idBroker,idInstrument)' ),
				'linksToInstruments' => Array( self::HAS_MANY, 'BrokerToBrokerInstrumentModel', 'idBroker' ),
				'binarytypes' => Array( self::MANY_MANY, 'BrokerBinaryOptionModel', '{{broker_to_broker_binary_option}}(idBroker,idBinaryOption)' ),
				'linksToBinarytypes' => Array( self::HAS_MANY, 'BrokerToBrokerBinaryOptionModel', 'idBroker' ),
				'binaryassets' => Array( self::MANY_MANY, 'BrokerBinaryAssetsModel', '{{broker_to_broker_binary_assets}}(idBroker,idBinaryOption)' ),
				'linksToBinaryAssets' => Array( self::HAS_MANY, 'BrokerToBrokerBinaryAssetsModel', 'idBroker' ),
				//'stats' => Array( self::HAS_ONE, 'BrokerStatsModel', 'idBroker' ),
				//'stats2' => Array( self::HAS_ONE, 'BrokerStats2Model', 'idBroker' ),
				'stats' => Array( self::HAS_ONE, 'BrokerStatsSmModel', 'idBroker' ),
				'marks' => Array( self::HAS_MANY, 'BrokerMarkModel', 'idBroker' ),
				'supportUser' => Array( self::BELONGS_TO, 'UserModel', 'idSupportUser' ),
				'EAStatements' => Array( self::HAS_MANY, 'EAStatementModel', 'idVersion' ),
			);
		}
		
		static function setupWpPosts(){
			$brokers = self::model()->findAll(array(
				'condition' => " `t`.`slug` <> '' "
			));
			foreach( $brokers as $broker ){
				$likeSql = '';
				$params = array();
				
				$i18n = $broker->getI18N();
				$namesArr = array( strtolower($broker->slug), mb_strtolower( trim( $i18n->officialName ) ), mb_strtolower( trim( $i18n->brandName ) ) ,  mb_strtolower( trim( $i18n->shortName ) ) );
				$namesArr = array_unique( $namesArr );
				
				usort( $namesArr, function($a,$b){return strlen($a)-strlen($b);});
				
				foreach( $namesArr as $bName ){
					foreach( $namesArr as $arrKey => $arrVal ){
						if( $bName == $arrVal ) continue;
						if( strpos( $arrVal, $bName ) !== false ) unset( $namesArr[$arrKey] );
					}
				}
				
				foreach( $namesArr as $key => $bName ){
					if( $bName ){
						if( $likeSql == '' ){
							$likeSql = " `t`.`post_title` LIKE :name$key OR `t`.`post_content` LIKE :name$key ";
						}else{
							$likeSql .= " OR `t`.`post_title` LIKE :name$key OR `t`.`post_content` LIKE :name$key ";
						}
						$params[':name' . $key] = '%'.$bName.'%' ;
					}
				}
				
				
				$relatedPosts = WPPostModel::model()->findAll(Array(
					'select' => array( " `t`.`ID` " ),
					'condition' => " ( $likeSql ) AND `t`.`post_type` = 'post' AND `t`.`post_status` = 'publish' AND `t`.`post_title` NOT LIKE '%DO NOT DELETE%' ",
					'order' => " `t`.`post_date` DESC ",
					'limit' => 4,
					'params' => $params,
				));
				$postsIds = '';
				$i=0;
				foreach( $relatedPosts as $post ){
					if($i) $postsIds .= ',';
					$postsIds .= $post->ID;
					$i++;
				}
				
				$i18n->wpPosts = $postsIds;
				$i18n->save();
				echo $postsIds . "\n";
				echo $broker->slug . "\n";
			}
		}
		function getWpPosts() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->wpPosts : '';
		}
		
		static function getAllBrokersForSelect(){
			$outArr = array();
			foreach( LanguageModel::getAll() as $lang ){
				$models = self::model()->findAll(array(
					'with' => array( 'i18ns' ),
					'condition' => " `i18nsBroker`.`idLanguage` = :idLanguage AND ( `i18nsBroker`.`officialName` IS NOT NULL AND `i18nsBroker`.`officialName` <> '' OR `i18nsBroker`.`brandName` IS NOT NULL AND `i18nsBroker`.`brandName` <> '' OR `i18nsBroker`.`shortName` IS NOT NULL AND `i18nsBroker`.`shortName` <> '' ) ",
					'order' => " `t`.`type` ASC ",
					'params' => array( ':idLanguage' => $lang->id ),
				));
				$outArr[$lang->id][0] = Yii::t('*', 'None');
				foreach( $models as $model ){
					$outArr[$lang->id][$model->id] = $model->type . ' ' . $model->singleTitle;
				}
			}
			return $outArr;
		}
		static function getListURL() {
			return Yii::App()->createURL( 'broker/list' );
		}
		static function getModelSingleURL( $id ) {
			return Yii::App()->createURL( 'broker/single', Array( 'id' => $id ));
		}
		static function getModelSingleURLbySlug( $slug, $type ) {
			return Yii::App()->createURL( $type . '/single', Array( 'slug' => $slug ));
		}
		static function getAbsoluteModelSingleURLbySlug( $slug, $type ) {
			return Yii::App()->createAbsoluteURL( $type . '/single', Array( 'slug' => $slug ));
		}
		static function getModelAdminURL( $id ) {
			return Yii::App()->createURL( 'admin/broker/list', Array( 'idEdit' => $id ));
		}
		static function getModelRedirectURL( $id, $type, $broker_ads ) {
			return Yii::App()->createURL( 'broker/redirect', Array( 'id' => $id, 'type' => $type, 'ads' => $broker_ads ));
		}
		static function getSlugById( $id ){
			$model = self::model()->findByPk( $id );
			if( !$model ) return false;
			return $model->slug;
		}
		static function updateStats() {
			/*$settings = BrokerSettingsModel::getModel();
			
			$exists = Yii::App()->db->createCommand("
				SELECT 		1 
				FROM 		`{{broker_stats}}` 
				WHERE 		`updatedDT` IS NULL OR DATE(`updatedDT`) < CURDATE() 
				LIMIT		1
			")->queryScalar();
			
			if( $exists ) {
				$command = file_get_contents( "protected/data/updateBrokerStats.sql" );
				Yii::App()->db->createCommand( $command )->query(Array(
					':daysToWeightReductionReview' => $settings->daysToWeightReductionReview,
					':minTrust' => $settings->minTrustMark,
					':maxTrust' => $settings->maxTrustMark,
				));
			}*/

		}
		static function getMaxPlace() {
			return Yii::App()->db->createCommand(" 
				SELECT 	MAX( `place` )
				FROM	`{{broker_stats_sm}}`;
			")->queryScalar();
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->officialName )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->officialName )) return $i18n;
			}
		}
		function getOfficialName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->officialName : '';
		}
		function getBrandName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->brandName ? $i18n->brandName : $this->officialName : '';
		}
		function getShortName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->shortName ? $i18n->shortName : $this->officialName : '';
		}
		
		function getMetaTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaTitle ? $i18n->metaTitle : '' : '';
		}
		function getMetaDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaDesc ? $i18n->metaDesc : '' : '';
		}
		function getMetaKeys() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaKeys ? $i18n->metaKeys : '' : '';
		}
		function getMaxProfit() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->maxProfit ? $i18n->maxProfit : '' : '';
		}
		function getShortDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->shortDesc ? $i18n->shortDesc : '' : '';
		}
		function getFullDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->fullDesc ? $i18n->fullDesc : '' : '';
		}
		
		public function getSingleTitle(){
			if( $this->shortName ){
				$title = $this->shortName;
			}elseif( $this->brandName ){
				$title = $this->brandName;
			}else{
				$title = $this->officialName;
			}
			return $title;
		}
		
		function getAM() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->AM : '';
		}
		function getDemoAccount() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->demoAccount : '';
		}
		
		function getSingleURL() {
			return self::getModelSingleURLbySlug( $this->slug, $this->type );
		}
		function getAbsoluteSingleURL() {
			return self::getAbsoluteModelSingleURLbySlug( $this->slug, $this->type );
		}
		
		function getAllLikesCount(){
			$row = Yii::App()->db->createCommand(" 
				SELECT SUM(`vk`) `vkSum`, SUM(`fb`) `fbSum` FROM `{{broker_sm_data}}` `smData`
				LEFT JOIN `{{broker}}` `broker` ON `broker`.`id` = `smData`.`idBroker`
				WHERE `broker`.`showInRating` = 1
			")->queryRow( true, $params );
			return $row['vkSum'] + $row['fbSum'];
		}
		function getBestRating(){
			return 10;
		}
		function getRating(){
			return round( ($this->bestRating / $this->allLikesCount) * $this->likesCount, 2 );
		}
		function getLikesCount(){
			return $this->vkCount + $this->fbCount;
		}
		
		function getVkCount(){
			if( !$this->smData ) return 0;
			return $this->smData->vk;
		}
		function getFbCount(){
			if( !$this->smData ) return 0;
			return $this->smData->fb;
		}
		function getAdminURL() {
			return self::getModelAdminURL( $this->id );
		}
		function getRedirectUrl( $type ) {
			if(  $this->stats->place ===0 ){ 
				return self::getModelRedirectURL( $this->id, $type, 1 );
			}else{
				return self::getModelRedirectURL( $this->id, $type, 0 );
			}
		}
		function getUserMark() {
			$mark = BrokerMarkModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idUser' => Yii::App()->user->id ));
			if( !$mark ) $mark = BrokerMarkModel::instance( $this->id, Yii::App()->user->id );
			return $mark;
		}
		function viewed() {
			$this->countViews++;
			$this->update( Array( 'countViews' ));
		}
			
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->officialName ) or strlen( $obj->brandName ) or strlen( $obj->shortName ) or strlen( $obj->AM ) or strlen( $obj->demoAccount ) or strlen( $obj->metaTitle ) or strlen( $obj->metaDesc ) or strlen( $obj->metaKeys ) or strlen( $obj->maxProfit )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = BrokerI18NModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = BrokerI18NModel::instance( $this->id, $idLanguage, $obj->officialName, $obj->brandName, $obj->shortName, $obj->AM, $obj->demoAccount, $obj->metaTitle, $obj->metaDesc, $obj->metaKeys, $obj->maxProfit, $obj->shortDesc, $obj->fullDesc );
						$i18n->save();
					}
					else{
						$i18n->officialName = $obj->officialName;
						$i18n->brandName = $obj->brandName;
						$i18n->shortName = $obj->shortName;
						$i18n->AM = $obj->AM;
						$i18n->demoAccount = $obj->demoAccount;
						
						$i18n->metaTitle = $obj->metaTitle;
						$i18n->metaDesc = $obj->metaDesc;
						$i18n->metaKeys = $obj->metaKeys;
						
						$i18n->shortDesc = $obj->shortDesc;
						$i18n->fullDesc = $obj->fullDesc;
						
						$i18n->maxProfit = $obj->maxProfit;
						
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			
			# servers
		function deleteServers() {
			foreach( $this->servers as $model ) {
				$model->delete();
			}
		}
			
			#users
		function deleteUsers() {
			foreach( $this->users as $user ) {
				$user->idBroker = null;
				$user->save();
			}
		}
		
		function linkToUser() {
			if( $idUser = Yii::App()->user->id ) {
				$link = BrokerToUserModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idUser' => $idUser ));
				if( !$link ) {
					$link = BrokerToUserModel::instance( $this->id, $idUser );
					$link->save();
					UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_BROKER_LINK, Array( 'idUser' => $idUser, 'idBroker' => $this->id ));
					BrokerStats2Model::inc( $this->id, "countLinks" );
				}
			}
		}
		function unlinkFromUser() {
			if( $idUser = Yii::App()->user->id ) {
				$link = BrokerToUserModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idUser' => $idUser ));
				if( $link ) {
					$link->delete();
					BrokerStats2Model::dec( $this->id, "countLinks" );
				}
			}
		}
		function unlinkFromUsers() {
			foreach( $this->linksToUsers as $model ) {
				$model->delete();
			}
		}
		function linkedUser() {
			if( $idUser = Yii::App()->user->id ) {
				$link = BrokerToUserModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idUser' => $idUser ));
				return !!$link;
			}
			return false;
		}
		function getCountLinkedUsers() {
			return BrokerToUserModel::model()->countByAttributes( Array(
				'idBroker' => $this->id
			));
		}
		/*function saveSm( $dataToSave ){
			if( $idUser = Yii::App()->user->id ) {
				$model = BrokerToUserVkFbModel::getModel( $this->id, $idUser );
				foreach( $dataToSave as $key => $val ){
					$model->$key = $val;
				}
				$model->save();
			}
		}*/
					
			# image
		function getPathImage() {
			return strlen($this->nameImage) ? self::PATHUploadDir."/{$this->nameImage}" : '';
		}
		function getPathHomeImage() {
			return strlen($this->homeImage) ? self::PATHUploadDir."/{$this->homeImage}" : '';
		}
		function getSrcHomeImage() {
			if( $this->pathHomeImage ){
				return Yii::app()->baseUrl."/{$this->pathHomeImage}";
			}elseif( $this->pathImage ){
				return Yii::app()->baseUrl."/{$this->pathImage}";
			}else{
				return '';
			}
		}
		function getSrcImage() {
			return $this->pathImage ? Yii::app()->baseUrl."/{$this->pathImage}" : '';
		}
		function getPathMiddleImage() {
			return strlen($this->nameMiddleImage) ? self::PATHUploadDir."/{$this->nameMiddleImage}" : '';
		}
		function getSrcMiddleImage() {
			return $this->pathMiddleImage ? Yii::app()->baseUrl."/{$this->pathMiddleImage}" : '';
		}
		function getPathThumbImage() {
			return strlen($this->nameThumbImage) ? self::PATHUploadDir."/{$this->nameThumbImage}" : '';
		}
		function getSrcThumbImage() {
			return $this->pathThumbImage ? Yii::app()->baseUrl."/{$this->pathThumbImage}" : '';
		}
		function deleteImage() {
			if( strlen( $this->nameImage )) {
				if( is_file( $this->pathImage ) and !@unlink( $this->pathImage )) throw new Exception( "Can't delete {$this->pathImage}" );
				$this->nameImage = null;
			}
			if( strlen( $this->nameMiddleImage )) {
				if( is_file( $this->pathMiddleImage ) and !@unlink( $this->pathMiddleImage )) throw new Exception( "Can't delete {$this->pathMiddleImage}" );
				$this->nameMiddleImage = null;
			}
			if( strlen( $this->nameThumbImage )) {
				if( is_file( $this->pathThumbImage ) and !@unlink( $this->pathThumbImage )) throw new Exception( "Can't delete {$this->pathThumbImage}" );
				$this->nameThumbImage = null;
			}
		}
		function setImage( $name, $nameMiddle, $nameThumb ) {
			$this->deleteImage();
			$this->nameImage = $name;
			$this->nameMiddleImage = $nameMiddle;
			$this->nameThumbImage = $nameThumb;
		}
		function uploadImage( $uploadedFile ) {
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );

			$middleFileFullName = "{$fileName}-middle.{$fileEx}";
			$middleFilePath = self::PATHUploadDir."/{$middleFileFullName}";
			ImgResizeLib::resize( $filePath, $middleFilePath, self::WIDTHMiddle, self::HEIGHTMiddle );
			
			$thumbFileFullName = "{$fileName}-thumb.{$fileEx}";
			$thumbFilePath = self::PATHUploadDir."/{$thumbFileFullName}";
			ImgResizeLib::resize( $filePath, $thumbFilePath, self::WIDTHThumb, self::HEIGHTThumb );
			
			$this->setImage( $fileFullName, $middleFileFullName, $thumbFileFullName );
		}
		function uploadHomeImage( $uploadedFile ) {
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );
			
			if( strlen( $this->homeImage )) {
				if( is_file( $this->pathHomeImage ) and !@unlink( $this->pathHomeImage )) throw new Exception( "Can't delete {$this->pathHomeImage}" );
				$this->homeImage = null;
			}
			$this->homeImage = $fileFullName;
		}
		function getMiddleImage( $options = Array() ) {
			$options[ 'src' ] = $this->srcMiddleImage;
			$options[ 'title' ] = $this->brandName;
			$options[ 'alt' ] = $this->brandName;
			return $this->nameMiddleImage ? CHtml::tag( 'img', $options ) : '';
		}
		function getThumbImage( $options = Array() ) {
			$options[ 'src' ] = $this->srcThumbImage;
			if( empty( $options[ 'title' ])) $options[ 'title' ] = $this->brandName;
			if( empty( $options[ 'alt' ])) $options[ 'alt' ] = $this->brandName;
			return $this->nameThumbImage ? CHtml::tag( 'img', $options ) : '';
		}
		function getContestSponsorImage( $options = Array() ) {
			
			$fullImgPath = Yii::getPathOfAlias('application') . '/..' . $this->srcImage ;
			
			$fullImgPathParts = pathinfo($fullImgPath);
			$sponsorFileName = $fullImgPathParts['filename'] . '-sponsor.' . $fullImgPathParts['extension'];
			$sponsorPath = $fullImgPathParts['dirname'] . '/' . $sponsorFileName ;
			
			if( !file_exists( $sponsorPath ) ){
				ImgResizeLib::resize( $fullImgPath, $sponsorPath, 'auto', self::HEIGHTSponsor );
			}
	
			$options[ 'src' ] = Yii::app()->baseUrl . '/' . self::PATHUploadDir . '/' . $sponsorFileName;
			$options[ 'title' ] = $this->brandName;
			$options[ 'alt' ] = $this->brandName;
			return $this->nameMiddleImage ? CHtml::tag( 'img', $options ) : '';
		}
		
			# regulators
		function getIDsRegulators() {
			return CommonLib::slice( $this->regulators, 'id' );
		}
		function unlinkFromRegulators() {
			foreach( $this->linksToRegulators as $link ) {
				$link->delete();
			}
		}
		function linkToRegulators( $ids ) {
			foreach( $ids as $idRegulator ) {
				$link = BrokerToBrokerRegulatorModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idRegulator' => $idRegulator ));
				if( !$link ) {
					$link = BrokerToBrokerRegulatorModel::instance( $this->id, $idRegulator );
					$link->save();
				}
			}
		}
		function setRegulators( $ids ) {
			$oldIDs = $this->getIDsRegulators();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromRegulators();
				$this->linkToRegulators( $ids );
			}
		}
		
			# trade platforms
		function getIDsTradePlatforms() {
			return CommonLib::slice( $this->tradePlatforms, 'id' );
		}
		function unlinkFromTradePlatforms() {
			foreach( $this->linksToPlatforms as $link ) {
				$link->delete();
			}
		}
		function linkToTradePlatforms( $ids ) {
			foreach( $ids as $idPlatform ) {
				$link = BrokerToTradePlatformModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idPlatform' => $idPlatform ));
				if( !$link ) {
					$link = BrokerToTradePlatformModel::instance( $this->id, $idPlatform );
					$link->save();
				}
			}
		}
		function setTradePlatforms( $ids ) {
			$oldIDs = $this->getIDsTradePlatforms();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromTradePlatforms();
				$this->linkToTradePlatforms( $ids );
			}
		}
		
			# IOs
		function getIDsIOs() {
			return CommonLib::slice( $this->IOs, 'id' );
		}
		function unlinkFromIOs() {
			foreach( $this->linksToIOs as $link ) {
				$link->delete();
			}
		}
		function linkToIOs( $ids ) {
			foreach( $ids as $idIO ) {
				$link = BrokerToBrokerIOModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idIO' => $idIO ));
				if( !$link ) {
					$link = BrokerToBrokerIOModel::instance( $this->id, $idIO );
					$link->save();
				}
			}
		}
		function setIOs( $ids ) {
			$oldIDs = $this->getIDsIOs();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromIOs();
				$this->linkToIOs( $ids );
			}
		}
		
			# instruments
		function getIDsInstruments() {
			return CommonLib::slice( $this->instruments, 'id' );
		}
		function unlinkFromInstruments() {
			foreach( $this->linksToInstruments as $link ) {
				$link->delete();
			}
		}
		function linkToInstruments( $ids ) {
			foreach( $ids as $idInstrument ) {
				$link = BrokerToBrokerInstrumentModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idInstrument' => $idInstrument ));
				if( !$link ) {
					$link = BrokerToBrokerInstrumentModel::instance( $this->id, $idInstrument );
					$link->save();
				}
			}
		}
		function setInstruments( $ids ) {
			$oldIDs = $this->getIDsInstruments();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromInstruments();
				$this->linkToInstruments( $ids );
			}
		}
			#binary
		function getIDsBinaryTypes() {
			return CommonLib::slice( $this->binarytypes, 'id' );
		}
		function unlinkFromBinaryTypes() {
			foreach( $this->linksToBinarytypes as $link ) {
				$link->delete();
			}
		}
		function linkToBinaryTypes( $ids ) {
			foreach( $ids as $idBinaryOption ) {
				$link = BrokerToBrokerBinaryOptionModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idBinaryOption' => $idBinaryOption ));
				if( !$link ) {
					$link = BrokerToBrokerBinaryOptionModel::instance( $this->id, $idBinaryOption );
					$link->save();
				}
			}
		}
		function setBinaryTypes( $ids ) {
			$oldIDs = $this->getIDsBinaryTypes();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromBinaryTypes();
				$this->linkToBinaryTypes( $ids );
			}
		}
		
		function getIDsBinaryAssets() {
			return CommonLib::slice( $this->binaryassets, 'id' );
		}
		function unlinkFromBinaryAssets() {
			foreach( $this->linksToBinaryAssets as $link ) {
				$link->delete();
			}
		}
		function linkToBinaryAssets( $ids ) {
			foreach( $ids as $idBinaryOption ) {
				$link = BrokerToBrokerBinaryAssetsModel::model()->findByAttributes( Array( 'idBroker' => $this->id, 'idBinaryOption' => $idBinaryOption ));
				if( !$link ) {
					$link = BrokerToBrokerBinaryAssetsModel::instance( $this->id, $idBinaryOption );
					$link->save();
				}
			}
		}
		function setBinaryAssets( $ids ) {
			$oldIDs = $this->getIDsBinaryAssets();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromBinaryAssets();
				$this->linkToBinaryAssets( $ids );
			}
		}
			# stats
		function deleteStats() {
			if( $this->stats ) $this->stats->delete();
		}
		/*function deleteSmClicks(){
			BrokerToUserVkFbModel::model()->deleteAll(
				" `idBroker` = :id ", 
				array(':id' => $this->id ) 
			);
		}*/
		
			# marks
		function deleteMarks() {
			foreach( $this->marks as $model ) {
				$model->delete();
			}
		}
		
			# EAStatements
		function deleteEAStatements() {
			foreach( $this->EAStatements as $statsment ) {
				$statsment->delete();
			}
		}
		
			# events
		protected function beforeSave() {
			$this->trueUpdatedDate = $this->updatedDT;
			return parent::beforeSave();
		}
		protected function afterSave() {
			parent::afterSave();

			if( !$this->showInRating ){
				$this->deleteStats();
			}/*else{
				$model = BrokerToUserVkFbModel::getModel( $this->id, 1 );
				if( $model->validate() ) $model->save();
			}*/
			
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteServers();
			$this->deleteUsers();
			$this->unlinkFromUsers();
			$this->deleteI18Ns();
			$this->deleteImage();
			$this->unlinkFromRegulators();
			$this->unlinkFromTradePlatforms();
			$this->unlinkFromIOs();
			$this->unlinkFromInstruments();
			
			$this->unlinkFromBinaryTypes();
			$this->unlinkFromBinaryAssets();
			
			$this->deleteStats();
			$this->deleteSmClicks();
			//$this->deleteMarks();
			$this->deleteEAStatements();
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.broker/single.'.$id.'.'  );
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.broker/list.NO.Param.' );
		}
	}

?>
