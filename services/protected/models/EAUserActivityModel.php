<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAUserActivityModel extends ModelBase {
		const TYPE_EVENT_CREATE_REVIEW = 1;
		const TYPE_EVENT_CREATE_MARK = 2;
		const TYPE_EVENT_DOWNLOAD_VERSION = 3;
		const TYPE_EVENT_CREATE_EA = 4;
		const TYPE_EVENT_CREATE_EA_VERSION = 5;
		const TYPE_EVENT_CREATE_EA_STATEMENT = 6;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_user_activity}}";
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'EAUserActivityI18NModel', 'idActivity', 'alias' => 'i18nsEAUserActivity' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'EAUserActivityI18NModel', 'idActivity', 
					'alias' => 'cLI18NEAUserActivity',
					'on' => '`cLI18NEAUserActivity`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'EA' => Array( self::BELONGS_TO, 'EAModel', 'idEA' ),
			 );
		}
		static function instance( $idUser, $idEA ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser', 'idEA' ));
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->message )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->message )) return $i18n;
			}
		}
		function getMessage() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->message : '';
		}
		function getHTMLMessage( $maxLen = null ) {
			$message = $this->message;
			//$message = htmlspecialchars( $this->message );
			//$message = CommonLib::nl2br( $message );
			if( $maxLen ) $message = CommonLib::shorten( $message, $maxLen );
			return $message;
		}
			
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->message )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = EAUserActivityI18NModel::model()->findByAttributes( Array( 'idActivity' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = EAUserActivityI18NModel::instance( $this->id, $idLanguage, $obj->message );
						$i18n->save();
					}
					else{
						$i18n->message = $obj->message;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		function setI18NsFromMessage( $key, $ns = '*', $params = Array(), $paramsI18Ns = Array() ) {
			$message = MessageModel::model()->find(Array(
				'with' => 'i18ns',
				'condition' => "
					`t`.`key` = :key AND `t`.`ns` = :ns
				",
				'params' => Array(
					':key' => $key,
					':ns' => $ns,
				),
			));
			if( $message ) {
				$i18ns = Array();
				foreach( $message->i18ns as $i18n ) {
					$message = $i18n->value;
					if( $params ) $message = strtr( $message, $params );
					if( $paramsI18Ns and isset( $paramsI18Ns[ $i18n->idLanguage ])) $message = strtr( $message, $paramsI18Ns[ $i18n->idLanguage ]);
					$i18ns[ $i18n->idLanguage ] = (object)Array(
						'message' => $message,
					);
				}
				$this->setI18Ns( $i18ns );
			}
		}
			
			# events
		static function event( $type, $params ) {
			$settings = UserActivitySettingsModel::getModel();
			switch( $type ) {
				case self::TYPE_EVENT_CREATE_REVIEW:{
					if( $EA = EAModel::model()->findByPk( $params[ 'idEA' ])) {
						$activity = self::instance( $params[ 'idUser' ], $params[ 'idEA' ]);
						$activity->save();
						$url = $EA->getSingleURL();
						$_params = Array();
						$_params[ '{link}' ] = CHtml::link( CHtml::encode( $EA->name ), $url );
						$activity->setI18NsFromMessage( 'Commented EA {link}', '*', $_params );
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA_REVIEW, $params );
					}
					break;
				}
				case self::TYPE_EVENT_CREATE_MARK:{
					if( $EA = EAModel::model()->findByPk( $params[ 'idEA' ])) {
						$activity = self::instance( $params[ 'idUser' ], $params[ 'idEA' ]);
						$activity->save();
						$url = $EA->getSingleURL();
						$_params = Array();
						$_params[ '{link}' ] = CHtml::link( CHtml::encode( $EA->name ), $url );
						$_params[ '{average}' ] = "{$params['average']}%";
						$activity->setI18NsFromMessage( 'Rated EA {link} by {average}', '*', $_params );
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA_MARK, $params );
					}
					break;
				}
				case self::TYPE_EVENT_DOWNLOAD_VERSION:{
					if( $EAVersion = EAVersionModel::model()->findByPk( $params[ 'idEAVersion' ])) {
						if( $EA = EAModel::model()->findByPk( $EAVersion->idEA )) {
							$activity = self::instance( $params[ 'idUser' ], $EA->id );
							$activity->save();
							
							$urlEA = $EA->getSingleURL();
							$urlEAVersion = $EAVersion->getSingleURL();
							$_params = Array();
							$_params[ '{linkEA}' ] = CHtml::link( CHtml::encode( $EA->name ), $urlEA );
							$_params[ '{linkEAVersion}' ] = CHtml::link( CHtml::encode( $EAVersion->version ), $urlEAVersion );
							$activity->setI18NsFromMessage( 'Downloaded EA {linkEA} {linkEAVersion}', '*', $_params );
							UserActivityModel::event( UserActivityModel::TYPE_EVENT_DOWNLOAD_EA_VERSION, $params );
						}
					}
					break;
				}
				case self::TYPE_EVENT_CREATE_EA:{
					if( $EA = EAModel::model()->findByPk( $params[ 'idEA' ])) {
						$activity = self::instance( $params[ 'idUser' ], $params[ 'idEA' ]);
						$activity->save();
						$url = $EA->getSingleURL();
						$_params = Array();
						$_params[ '{link}' ] = CHtml::link( CHtml::encode( $EA->name ), $url );
						$activity->setI18NsFromMessage( 'Add new EA page {link}', '*', $_params );
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA, $params );
					}
					break;
				}
				case self::TYPE_EVENT_CREATE_EA_VERSION:{
					if( $EAVersion = EAVersionModel::model()->findByPk( $params[ 'idEAVersion' ])) {
						if( $EA = EAModel::model()->findByPk( $EAVersion->idEA )) {
							$activity = self::instance( $params[ 'idUser' ], $EA->id );
							$activity->save();
														
							$urlEA = $EA->getSingleURL();
							$urlEAVersion = $EAVersion->getSingleURL();
							$_params = Array();
							$_params[ '{linkEA}' ] = CHtml::link( CHtml::encode( $EA->name ), $urlEA );
							$_params[ '{linkEAVersion}' ] = CHtml::link( CHtml::encode( $EAVersion->version ), $urlEAVersion );
							$activity->setI18NsFromMessage( 'Add new EA version {linkEA} {linkEAVersion}', '*', $_params );
							UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA_VERSION, $params );
						}
					}
					break;
				}
				case self::TYPE_EVENT_CREATE_EA_STATEMENT:{
					if( $EAStatement = EAStatementModel::model()->findByPk( $params[ 'idEAStatement' ])) {
						$EA = EAModel::model()->findByPk( $EAStatement->idEA );
						$EAVersion = EAVersionModel::model()->findByPk( $EAStatement->idVersion );
						if( $EA and $EAVersion ) {
							$activity = self::instance( $params[ 'idUser' ], $EA->id );
							$activity->save();
							
							$urlEA = $EA->getSingleURL();
							$urlEAVersion = $EAVersion->getSingleURL();
							$_params = Array();
							$_params[ '{linkEA}' ] = CHtml::link( CHtml::encode( $EA->name ), $urlEA );
							$_params[ '{linkEAVersion}' ] = CHtml::link( CHtml::encode( $EAVersion->version ), $urlEAVersion );
							$activity->setI18NsFromMessage( 'Add new EA statement {linkEA} {linkEAVersion}', '*', $_params );
							UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA_STATEMENT, $params );
						}
					}
					break;
				}
			}
			return true;
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>