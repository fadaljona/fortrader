<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CalendarEventDescModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idIndicator, $idLanguage ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idIndicator', 'idLanguage' ));
		}
	}

?>
