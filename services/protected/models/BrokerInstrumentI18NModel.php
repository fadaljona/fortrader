<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerInstrumentI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{broker_instrument_i18n}}";
		}
		static function instance( $idInstrument, $idLanguage, $name ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idInstrument', 'idLanguage', 'name' ));
		}
	}

?>