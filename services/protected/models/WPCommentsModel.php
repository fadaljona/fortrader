<?php

	Yii::import( 'models.base.ModelBase' );
	
	final class WPCommentsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "wp_comments";
		}
		function relations() {
			return Array(
				'meta' => Array( self::HAS_MANY, 'WPCommentmetaModel', Array( "comment_id" => "comment_ID" ) ),
			);
		}
		
	}
?>