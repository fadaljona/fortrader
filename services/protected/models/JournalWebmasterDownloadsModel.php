<?php
Yii::import( 'models.base.ModelBase' );
class JournalWebmasterDownloadsModel extends ModelBase
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return JournalWebmasterDownloads the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{journal_webmaster_downloads}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idWebmaster, ip, createdDT, idJournal', 'required'),
			array('idWebmaster, idJournal', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idWebmaster, ip, createdDT, idJournal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'j' => Array( self::BELONGS_TO, 'JournalModel', 'idJournal', 'joinType' => 'INNER JOIN' ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idWebmaster' => 'Id Webmaster',
			'ip' => 'Ip',
			'createdDT' => 'Created Dt',
			'idJournal' => 'Id Journal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idWebmaster',$this->idWebmaster);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('createdDT',$this->createdDT,true);
		$criteria->compare('idJournal',$this->idJournal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}