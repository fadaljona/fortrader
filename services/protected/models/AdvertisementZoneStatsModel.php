<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementZoneStatsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'advertisementZone' => Array( self::BELONGS_TO, 'AdvertisementZoneModel', 'idZone' ),
				'stats' => Array( self::HAS_MANY, 'AdvertisementZoneStatsModel', 'idZone' ),
			);
		}
			# stats
		function deleteStats() {
			foreach( $this->stats as $model ) {
				$model->delete();
			}
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteStats();
		}
	}

?>