<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerLinksStatsModel extends ModelBase {
		public $lastDate;
		public $openAccountClicks;
		public $openDemoAccountClicks;
		public $siteClicks;
		public $conditionsClicks;
		public $termsClicks;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function saveClick( $type, $brokerId ){
			$stat = new self;
			$stat->clickDate = date('Y-m-d H:i:s', time());
			$stat->brokerId = $brokerId;
			$stat->type = $type;
			$stat->ip = Yii::app()->request->userHostAddress;
			if( $stat->validate() ) $stat->save();
		}
		
		function rules() {
			return Array(
				Array( 'brokerId', 'numerical', 'integerOnly' => true, 'min' => 0),
				Array( 'clickDate', 'date', 'format'=>'yyyy-M-d H:m:s'),
			);
		}
		function relations() {
			return Array(
				'currentLanguageI18N' => Array( self::HAS_ONE, 'BrokerI18NModel', array( 'idBroker' => 'brokerId' ), 
					'alias' => 'cLI18NBroker',
					'on' => '`cLI18NBroker`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getByAlias( 'ru' )->id,
					),
				),
			);
		}
		public function tableName(){
			return '{{broker_links_stats}}';
		}
	}
?>