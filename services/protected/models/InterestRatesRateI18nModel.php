<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InterestRatesRateI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}

		function tableName() {
			return "{{interest_rates_rate_i18n}}";
		}
		static function instance( $idRate, $idLanguage, $title ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idRate', 'idLanguage', 'title' ));
		}
	}
?>