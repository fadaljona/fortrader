<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserReputationModel extends ModelBase {
		const TYPE_EVENT_CREATE_CONTEST_MEMBER = 1;
		const TYPE_EVENT_DELETE_CONTEST_MEMBER = 2;
		const TYPE_EVENT_CREATE_CONTEST_WINER = 3;
		const TYPE_EVENT_DELETE_CONTEST_WINER = 4;
		const TYPE_EVENT_CREATE_THANK_EA_VERSION = 5;
		const TYPE_EVENT_CREATE_THANK_EA_STATEMENT = 6;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'UserReputationI18NModel', 'idReputation', 'alias' => 'i18nsUserReputation' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'UserReputationI18NModel', 'idReputation', 
					'alias' => 'cLI18NUserReputation',
					'on' => '`cLI18NUserReputation`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'from' => Array( self::BELONGS_TO, 'UserModel', 'idFrom' ),
				'to' => Array( self::BELONGS_TO, 'UserModel', 'idTo' ),
			 );
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->message )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->message )) return $i18n;
			}
		}
		function getMessage() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->message : '';
		}
		function getHTMLMessage( $maxLen = null ) {
			$message = htmlspecialchars( $this->message );
			$message = CommonLib::nl2br( $message );
			if( $maxLen ) $message = CommonLib::shorten( $message, $maxLen );
			return $message;
		}
			
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->message )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = UserReputationI18NModel::model()->findByAttributes( Array( 'idReputation' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = UserReputationI18NModel::instance( $this->id, $idLanguage, $obj->message );
						$i18n->save();
					}
					else{
						$i18n->message = $obj->message;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			
			# events
		static function event( $type, $params ) {
			$user = UserModel::model()->findByPk( $params[ 'idUser' ]);
			if( !$user ) return false;
			
			$settings = UserReputationSettingsModel::getModel();
			switch( $type ) {
				case self::TYPE_EVENT_CREATE_CONTEST_MEMBER:{
					$user->countReputation += $settings->count_contestMember;
					break;
				}
				case self::TYPE_EVENT_DELETE_CONTEST_MEMBER:{
					$user->countReputation -= $settings->count_contestMember;
					break;
				}
				case self::TYPE_EVENT_CREATE_CONTEST_WINER:{
					$user->countReputation += $settings->count_contestWinner;
					break;
				}
				case self::TYPE_EVENT_DELETE_CONTEST_WINER:{
					$user->countReputation -= $settings->count_contestWinner;
					break;
				}
				case self::TYPE_EVENT_CREATE_THANK_EA_VERSION:{
					$user->countReputation += $settings->count_eaVersionThank;
					break;
				}
				case self::TYPE_EVENT_CREATE_THANK_EA_STATEMENT:{
					$user->countReputation += $settings->count_eaStatmentThank;
					break;
				}
			}
			
			//$user->countReputation = max( $user->countReputation, 0 );
			
			$user->update( Array( 'countReputation' ));
			return true;
		}
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( !$this->isNewRecord ) {
					if( $to = UserModel::model()->findByPk( $this->idTo ) and $old = self::model()->findByPk( $this->id )) {
						$to->countReputation -= $old->count;
						$to->update( Array( 'countReputation' ));
					}
				}
			}
			return $result;
		}
		protected function afterSave() {
			if( $this->count and $to = UserModel::model()->findByPk( $this->idTo )) {
				$to->countReputation += $this->count;
				$to->update( Array( 'countReputation' ));
			}
			parent::afterSave();
		}
		protected function afterDelete() {
			if( $this->count and $to = UserModel::model()->findByPk( $this->idTo )) {
				$to->countReputation -= $this->count;
				$to->update( Array( 'countReputation' ));
			}
			$this->deleteI18Ns();
			parent::afterDelete();
		}
	}

?>