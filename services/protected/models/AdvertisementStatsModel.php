<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementStatsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'advertisement' => Array( self::BELONGS_TO, 'AdvertisementModel', 'idAd' ),
			);
		}
	}

?>