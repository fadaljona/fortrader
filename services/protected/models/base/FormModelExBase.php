<?

	abstract class FormModelExBase extends CFormModel {
		protected $_class;
		protected $_AR;
		protected $_modelName;
		protected $_modelClass;
		protected $_pkName;
		protected $_attributeLabels;
		protected $_fieldsVariants;
		protected $_NSi18n;
			// factory
		static protected function getClassByModelName( $modelName ) {
			return "Admin{$modelName}FormModel";
		}
		static protected function getAliasByClass( $class ) {
			return "models.forms.admin.{$class}";
		}
		static function instanceByModelName( $modelName, $_class = __CLASS__ ) {
			$class = call_user_func( Array( $_class, 'getClassByModelName' ), $modelName );
			$alias = call_user_func( Array( $_class, 'getAliasByClass' ), $class );
			Yii::import( $alias );
			return new $class();
		}
			// dets
		function det_class() {
			return get_class( $this );
		}
		function det_modelName() {
			$_class = $this->get_class();
			return preg_replace( "#FormModel$#", "", $_class );
		}
		function det_modelClass() {
			$_modelName = $this->get_modelName();
			return ModelExBase::_getModelClass( $_modelName );
		}
		function det_pkName() {
			$_modelClass = $this->get_modelClass();
			return CActiveRecord::model( $_modelClass )->_getFirstPkName();
		}
		function det_attributeLabels( $additionalLabels = Array() ) {
			return $this->translateR( $additionalLabels );
		}
		function det_fieldsVariants() {
			return Array();
		}
		function det_NSi18n() {
			$_class = $this->get_class();
			$_modelClass = $this->get_modelClass();
			$_model = CActiveRecord::model( $_modelClass );
			return array_merge( Array( "models/forms/{$_class}" ), (array)$_model->_getNSi18n());
		}
			// for CFormModel
		function attributeLabels() {
			return $this->get_attributeLabels();
		}
		function generateAttributeLabel( $name ) {
			if( $name == 'id' ) 
				$name = 'ID';
			else
				$name = ucfirst( $name );
				
			$label = $this->translateR( $name );
			return $label;
		}
			// common
		function translateR( $array ) {
			$_NSi18n = $this->get_NSi18n();
			return $array = Yii::t( $_NSi18n, $array );
		}
		function extendLabels( &$labels, $sourceKey, $extendKeys ) {
			foreach( $labels[$sourceKey] as $field=>$label ) {
				foreach( $extendKeys as $extendKey ) {
					$labels["{$sourceKey}[{$field}][{$extendKey}]"] = $label;
				}
			}
			unset( $labels[$sourceKey] );
		}
		function addI18NError( $attribute, $error, $params = Array() ) {
			$_NSi18n = $this->get_NSi18n();
			parent::addError( $attribute, Yii::t( $_NSi18n, $error, $params ));
		}	
		function getFirstError() {
			$field = null;
			$error = null;
			
			$errors = $this->getErrors();
			foreach( $errors as $key=>$keyErrors ) {
				$field = CHtml::resolveName( $this, $key );
				$error = array_shift( $keyErrors );
				break;
			}
			
			return Array( $field, $error );
		}
		function getFirstErrors() {
			$errors = Array();
			foreach( $this->attributeNames() as $key ) {
				if( $this->hasErrors( $key )) {
					$field = CHtml::resolveName( $this, $key );
					$errorsKey = $this->getErrors( $key );
					$error = array_shift( $errorsKey );
					$errors[] = compact( 'field', 'error' );
				}
			}
			return $errors;
		}
		function isPostRequest() {
			return Yii::App()->request->getIsPostRequest();
		}
		function resolveName( $field ) {
			return CHtml::resolveName( $this, $field );
		}
		function resolveID( $field ) {
			$name = $this->resolveName( $field );
			return CHtml::getIdByName( $name );
		}
		function unresolveName( $field ) {
			$_class = $this->get_class();
			if( strpos( $field, $_class ) === 0 ) {
				$field = substr( $field, strlen( $_class ));
				if( strpos( $field, '[' ) === 0 ) {
					if( $p = strpos( $field, ']' )) {
						$field = substr( $field, 1, $p - 1 );
					}
				}
			}
			return $field;
		}
		function getFieldVariants( $filed, $parameters = Array() ) {
			$filed = $this->unresolveName( $filed );
			$_fieldsVariants = $this->get_fieldsVariants();
			if( isset( $_fieldsVariants[ $filed ] )) 
				return $_fieldsVariants[ $filed ];
			
			$_modelClass = $this->get_modelClass();
			$variants = CActiveRecord::model( $_modelClass )->_getFieldVariants( $filed, $parameters );
			//if( $variants ) 
			//	$variants = $this->translateR( $variants );
			
			return $variants;
		}
		protected function throwI18NException( $message, $params = Array()) {
			$_NSi18n = $this->get_NSi18n();
			throw new Exception( Yii::t( $_NSi18n, $message, $params ));
		}	
			// work
		function getPostLink() {
			$link = &$_POST[ get_class( $this )];
			return $link;
		}
		function emptyPost() {
			$postLink = $this->getPostLink();
			return empty( $postLink );
		}
		function loadFromPost() {
			if( $this->isPostRequest() ) {
				$postLink = $this->getPostLink();
				foreach( $this->attributeNames() as $key ) {
					if( !isset( $postLink[$key])) continue;
					$this->$key = CommonLib::trimR( $postLink[ $key ]);
					$this->$key = CommonLib::nullIfEmptyR( $this->$key );
				}
				return true;
			}
			return false;
		}
		function loadFromFiles( $loadFields ) {
			foreach( $loadFields as $key ) {
				$file = CUploadedFile::getInstance( $this, $key );
				if( $file )	$this->$key = $file;
			}
		}
		function loadAR( $pk = 0 ) {
			$_modelClass = $this->get_modelClass();
			$AR = $pk ? CActiveRecord::model( $_modelClass )->findByPk( $pk ) : new $_modelClass();
			if( !$AR ) return false;
			$this->set_AR( $AR );
			return true;
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array() ) {
			if( !$AR ) $AR = $this->get_AR();
			if( !$AR ) return false;
			
			$fields = $loadFields ? $loadFields : $this->attributeNames();
			if( $exceptFields ) $fields = array_diff( $fields, $exceptFields );
			
			foreach( $fields as $key ) {
				if( !isset( $AR->$key )) continue;
				$this->$key = $AR->$key;
			}
			return true;
		}
		function loadFromObject( $obj, $loadFields = Array(), $exceptFields = Array() ) {
			$fields = $loadFields ? $loadFields : $this->attributeNames();
			if( $exceptFields ) $fields = array_diff( $fields, $exceptFields );
			
			foreach( $fields as $key ) {
				$this->$key = @$obj->$key;
			}
		}
		function load( $pk = 0, $throw = false ) {
			$post = $this->getPostLink();
			$pkName = $this->get_pkName();
			$postPK = (int)@$post[$pkName];
			if( $postPK ) $pk = $postPK;
			
			if( $this->loadAR( $pk )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			
			if( $throw ) {
				$_modelName = $this->get_modelName();
				$this->throwI18NException( "Can't find {$_modelName}!" );
			}
			
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array() ) {
			if( !$AR ) $AR = $this->get_AR();
			if( !$AR ) return false;
			
			$exceptFields[] = $AR->_getPkName();
			
			$fields = $saveFields ? $saveFields : $this->attributeNames();
			if( $exceptFields ) $fields = array_diff( $fields, $exceptFields );
			
			foreach( $fields as $key ) {
				if( !isset( $AR->$key )) continue;
				$AR->$key = $this->$key;
			}
			return true;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			$AR = $this->get_AR();
			if( !$AR ) return false;
			
			return $AR->save( $runValidation, $attributes );
		}
		function saveToObject( $obj = null, $saveFields = Array(), $exceptFields = Array() ) {
			$object = $obj ? $obj : new StdClass();
						
			$fields = $saveFields ? $saveFields : $this->attributeNames();
			if( $exceptFields ) $fields = array_diff( $fields, $exceptFields );
			
			foreach( $fields as $key ) {
				$object->$key = $this->$key;
			}
			return $object;
		}
		function save() {
			$AR = $this->get_AR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
		function deleteAR() {
			$AR = $this->get_AR();
			if( !$AR ) 
				return false;
			
			if( $AR->isNewRecord ) 
				return false;
				
			return $AR->delete();
		}
			# validators
		function uniqueField( $field, $params = Array() ) {
			if( !$this->hasErrors()) {
				if( !strlen( $this->$field )) return;
				
				$AR = $this->get_AR();
				if( !$AR ) return;
							
				$pkName = $AR->_getPkName();
				
				$c = new CDbCriteria();
				$c->params = Array();
				
				$c->addCondition( "`t`.`{$field}` = :field" );
				$c->params[ ':field' ] = $this->$field;
				
				if( !$AR->isNewRecord ) {
					$c->addCondition( "`t`.`{$pk}` != :pk" );
					$c->params[ ':pk' ] = $AR->$pk;
				}
				
				$exists = $AR->exists( $c );
				if( $exists ) {
					if( !empty( $params[ 'message' ])) {
						$this->addI18NError( $field, $params[ 'message' ]);
					}
					else{
						$this->addError( $field, Yii::t( 'yii', '{attribute} "{value}" has already been taken.', Array(
							'{attribute}' => $this->getAttributeLabel( $field ),
							'{value}' => CHtml::encode( $this->$field ),
						)));
					}
				}
			}
		}
		function requiredIfNewRecord( $field, $params = Array() ) {
			$AR = $this->get_AR();
			if( !$AR ) return;
			
			if( !strlen( $this->$field ) and $AR->isNewRecord ) {
				if( !empty( $params[ 'message' ])) {
					$this->addI18NError( $field, $params[ 'message' ]);
				}
				else{
					$this->addError( $field, Yii::t( 'yii', '{attribute} cannot be blank.', Array(
						'{attribute}' => $this->getAttributeLabel( $field ),
					)));
				}
			}
		}
		function existsIDModel( $field, $params = Array() ) {
			if( !strlen( $this->$field )) return;
			if( !$this->hasErrors()) {
				$model = CActiveRecord::model( $params[ 'model' ]);
				if( !$model->existsByPk( $this->$field )) {
					$this->addI18NError( $field, 'Wrong {attribute}!', Array(
						'{attribute}' => $this->getAttributeLabel( $field ),
					));
				}
			}
		}
		function existsValueFieldModel( $field, $params = Array() ) {
			if( !strlen( $this->$field )) return;
			if( !$this->hasErrors()) {
				$model = CActiveRecord::model( $params[ 'model' ]);
				
				$c = new CDbCriteria();
				$column = empty( $params[ 'field' ]) ? $field : $params[ 'field' ];
				$c->addColumnCondition( Array( 
					$column => $this->$field 
				));
				
				if( !$model->exists( $c )) {
					$this->addI18NError( $field, 'Wrong {attribute}!', Array(
						'{attribute}' => $this->getAttributeLabel( $field ),
					));
				}
			}
		}
		function inFieldVariants( $field, $params = Array() ) {
			if( !strlen( $this->$field )) return;
			$_modelClass = $this->get_modelClass();
			$variants = CActiveRecord::model( $_modelClass )->_getFieldVariants( $field );
			if( !array_key_exists( $this->$field, $variants )) {
				$this->addI18NError( $field, 'Wrong {attribute}!', Array(
					'{attribute}' => $this->getAttributeLabel( $field ),
				));
			}
		}
		function requiredEx( $field, $params = Array() ) {
			foreach( (array)$this->$field as $id=>$value ) {
				if( mb_strlen( $value ) > 0 ) return;
			}
			
			if( is_array( $this->$field )) {
				$key = LanguageModel::getCurrentLanguageID();
				$f = "{$field}[{$key}]";
			}
			else{
				$f = $field;
			}
			$this->addError( $f, Yii::t( 'yii','{attribute} cannot be blank.', Array( 
				'{attribute}' => $this->getAttributeLabel( $f ),
			)));
		}
		function lengthEx( $field, $params = Array() ) {
			foreach( (array)$this->$field as $id=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$f = is_array( $this->$field ) ? "{$field}[{$id}]" : $field;
					$this->addError( $f, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( $f ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
	}

?>