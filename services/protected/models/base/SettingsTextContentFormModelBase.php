<?php

Yii::import( 'models.base.SettingsFormModelBase' );

abstract class SettingsTextContentFormModelBase extends SettingsFormModelBase {
	
	function saveTextFields() {
		$AR = $this->getAR();
		$i18ns = Array();
		$languages = LanguageModel::getAll();
		foreach( $languages as $language ) {
			$i18ns[ $language->id ] = array();
			foreach( $this->textFields as $key => $val ){
				$i18ns[ $language->id ][$key] = trim( $this->{$key}[ $language->id ] );
			}
			$i18ns[ $language->id ] = (object)$i18ns[ $language->id ];
		}
		if( !$AR->createMessage() ) return false;
		$AR->updateI18NsTextContent( $i18ns );
		return true;
	}
	
}