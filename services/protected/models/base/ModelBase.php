<?

	abstract class ModelBase extends CActiveRecord {
		public $dbName;
		public $beenNewRecord = false;
		protected function resolveClassName( $class ) {
			return $class ? $class : get_class( $this );
		}
		static function getModelCleanClassName( $class ) {
			$class = preg_replace( "#Model$#", "", $class );
			$class[0] = strtolower($class[0]);
			return $class;
		}
		function getCleanClassName( $class = null ) {
			$class = $this->resolveClassName( $class );
			$class = self::getModelCleanClassName( $class );
			return $class;
		}
		function getNSi18n( $class = null ) {
			$class = $this->getCleanClassName( $class );
			return "models/{$class}";
		}
		static function getModelNSi18n( $class ) {
			$class = self::getModelCleanClassName( $class );
			return "models/{$class}";
		}
		function getTabName() {
			$class = $this->getCleanClassName();
			$class = preg_replace( "#[A-Z]#", '_$0', $class );
			$class = strtolower( $class );
			return $class;
		}
		function tableName() {
			$name = $this->getTabName();
			if( $this->dbName ) return "[{$this->dbName}].{$name}";
			return "{{{$name}}}";
		}
		static function getModelColumns( $name ) {
			$AR = CActiveRecord::model( $name );
			$table = $AR->getMetaData()->tableSchema;
			$fields = $table->getColumnNames();
			return $fields;
		}
		static function modelFromAssoc( $class, $assoc ) {
			$model = new $class();
			$table = $model->getMetaData()->tableSchema;
			
			foreach( $table->getColumnNames() as $key ) {
				if( isset( $assoc[ $key ])) $model->$key = $assoc[ $key ];
			}
			return $model;
		}
		function toAssoc( $saveFields = Array(), $exceptFields = Array() ) {
			$assoc = Array();
			$table = $this->getMetaData()->tableSchema;
			
			$fields = $saveFields ? $saveFields : $table->getColumnNames();
			if( $exceptFields ) $fields = array_diff( $fields, $exceptFields );
			
			foreach( $fields as $key ) {
				$assoc[ $key ] = $this->$key;
			}
			return $assoc;
		}
		function existsByPk( $pk ) {
			$table = $this->getMetaData()->tableSchema;
			return $this->exists( Array(
				'condition' => "`t`.`{$table->primaryKey}` = :pk",
				'params' => Array(
					':pk' => $pk,
				),
			));
		}
		function update( $attributes = null ) {
			return $this->getIsNewRecord() ? $this->insert( $attributes ) : parent::update( $attributes );
		}
			# crypt
		static function crypt( $data ) {
			return CommonLib::crypt( $data, Yii::App()->params['keyCrypt']);
		}
		static function decrypt( $data ) {
			return CommonLib::decrypt( $data, Yii::App()->params['keyCrypt']);
		}
		function setCrypt( $filed, $value ) {
			$value = self::crypt( $value );
			$this->$filed = $value;
		}
		function getCrypt( $filed ) {
			$value = $this->$filed;
			$value = self::decrypt( $value );
			return $value;
		}
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( $this->isNewRecord ) {
					$this->beenNewRecord = true;
					if( isset( $this->getMetaData()->columns[ 'createdDT' ]) and !$this->createdDT ) {
						$this->createdDT = date( "Y-m-d H:i:s" );
					}
				}
				if( isset( $this->getMetaData()->columns[ 'updatedDT' ])) {
					$this->updatedDT = date( "Y-m-d H:i:s" );
				}
			}
			return $result;
		}
			# extends
		function _getColumnNames() {
			return $this->getMetaData()->tableSchema->getColumnNames();
        }
	}

?>
