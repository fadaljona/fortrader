<?

	abstract class FormModelBase extends CFormModel {
		protected $AR;
		protected function resolveClassName( $class ) {
			return $class ? $class : get_class( $this );
		}
		function getCleanClassName( $class = null ) {
			$class = $this->resolveClassName( $class );
			$class = preg_replace( "#FormModel$#", "", $class );
			$class[0] = strtolower($class[0]);
			return $class;
		}
		function getNSi18n( $class = null ) {
			$class = $this->getCleanClassName( $class );
			return "models/forms/{$class}";
		}
		protected function getSourceAttributeLabels() {
			return Array();
		}
		function getARClassName() {
			throw new CException( Yii::t( 'yii', '{class}::getARClassName() must be implemented.', Array( '{class}' => get_class( $this ))));
		}
		function attributeLabels() {
			$NSi18n = $this->getNSi18n();
			$labels = $this->getSourceAttributeLabels();
			foreach( $labels as $key=>&$label ) {
				$label = Yii::t( $NSi18n, $label );
			}
			return $labels;
		}
		function addI18NError( $attribute, $error, $params = Array(), $class = null ) {
			$NSi18n = $this->getNSi18n( $class );
			parent::addError( $attribute, Yii::t( $NSi18n, $error, $params ));
		}
		function getPostLink() {
			$postLink = &$_POST[ get_class( $this )];
			return $postLink;
		}
		function emptyPost() {
			$postLink = $this->getPostLink();
			return empty( $postLink );
		}
		function loadFromPost() {
			if( $this->isPostRequest() ) {
				$postLink = $this->getPostLink();
				foreach( $this->attributeNames() as $key ) {
					if( !isset( $postLink[$key])) continue;
					if( is_string( $postLink[$key])) {
						$this->$key = trim($postLink[$key]);
					}
					else{
						$this->$key = $postLink[$key];
					}
				}
				return true;
			}
			return false;
		}
		function loadFromFiles( $loadFields = array() ) {
			foreach( $loadFields as $key ) {
				$file = CUploadedFile::getInstance( $this, $key );
				if( $file )	$this->$key = $file;
			}
		}
		function loadAR( $pk = 0 ) {
			$ARClassName = $this->getARClassName();
			$AR = $pk ? CActiveRecord::model( $ARClassName )->findByPk( $pk ) : new $ARClassName();
			if( !$AR ) return false;
			$this->setAR( $AR );
			return true;
		}
		function setAR( $AR ) {
			$this->AR = $AR;
		}
		function getAR() {
			return $this->AR;
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array() ) {
			if( !$AR ) $AR = $this->getAR();
			if( !$AR ) return false;
			
			$fields = $loadFields ? $loadFields : $this->attributeNames();
			if( $exceptFields ) $fields = array_diff( $fields, $exceptFields );
			
			foreach( $fields as $key ) {
				if( !isset( $AR->$key )) continue;
				$this->$key = $AR->$key;
			}
			return true;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array() ) {
			if( !$AR ) $AR = $this->getAR();
			if( !$AR ) return false;
			
			$table = $AR->getMetaData()->tableSchema;
			$pk = $table->primaryKey;
			$exceptFields[] = $pk;
			
			$fields = $saveFields ? $saveFields : $table->getColumnNames();
			if( $exceptFields ) $fields = array_diff( $fields, $exceptFields );
			
			foreach( $fields as $key ) {
				if( !property_exists( $this, $key )) continue;
				$AR->$key = $this->$key;
			}
			return true;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			$AR = $this->getAR();
			if( !$AR ) return false;

			return $AR->save( $runValidation, $attributes );
		}
		function load( $pk = 0 ) {
			if( $this->loadAR( $pk )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			$this->saveAR();
			return true;
		}
		function saveToObject( $obj = null, $saveFields = Array(), $exceptFields = Array() ) {
			$object = $obj ? $obj : new StdClass();
						
			$fields = $saveFields ? $saveFields : $this->attributeNames();
			if( $exceptFields ) $fields = array_diff( $fields, $exceptFields );
			
			foreach( $fields as $key ) {
				$object->$key = $this->$key;
			}
			return $object;
		}
		function loadFromObject( $obj, $loadFields = Array(), $exceptFields = Array() ) {
			$fields = $loadFields ? $loadFields : $this->attributeNames();
			if( $exceptFields ) $fields = array_diff( $fields, $exceptFields );
			
			foreach( $fields as $key ) {
				$this->$key = @$obj->$key;
			}
		}
		function getFirstError() {
			$field = null;
			$error = null;
			
			$errors = $this->getErrors();
			foreach( $errors as $key=>$keyErrors ) {
				$field = CHtml::resolveName( $this, $key );
				$error = array_shift( $keyErrors );
				break;
			}
			
			return Array( $field, $error );
		}
		function getFirstErrors() {
			$errors = Array();
			foreach( $this->attributeNames() as $key ) {
				if( $this->hasErrors( $key )) {
					$field = CHtml::resolveName( $this, $key );
					$errorsKey = $this->getErrors( $key );
					$error = array_shift( $errorsKey );
					$errors[] = compact( 'field', 'error' );
				}
			}
			return $errors;
		}
		function isPostRequest() {
			return Yii::App()->request->getIsPostRequest();
		}
		function resolveName( $field ) {
			return CHtml::resolveName( $this, $field );
		}
		function resolveID( $field ) {
			$name = $this->resolveName( $field );
			return CHtml::getIdByName( $name );
		}
		protected function throwI18NException( $message, $params = Array(), $class = null ) {
			$NSi18n = $this->getNSi18n( $class );
			throw new Exception( Yii::t( $NSi18n, $message, $params ));
		}			
			# validators
		function uniqueField( $field, $params = Array() ) {
			if( !$this->hasErrors()) {
				if( !strlen( $this->$field )) return;
				
				$AR = $this->getAR();
				if( !$AR ) return;
							
				$table = $AR->getMetaData()->tableSchema;
				$pk = $table->primaryKey;
				
				$c = new CDbCriteria();
				$c->params = Array();
				
				$c->addCondition( "`t`.`{$field}` = :field" );
				$c->params[ ':field' ] = $this->$field;
				
				if( !$AR->isNewRecord ) {
					$c->addCondition( "`t`.`{$pk}` != :pk" );
					$c->params[ ':pk' ] = $AR->$pk;
				}
				
				$finder = CActiveRecord::model( get_class( $AR ));
				$exists = $finder->exists( $c );
				if( $exists ) {
					if( !empty( $params[ 'message' ])) {
						$this->addI18NError( $field, $params[ 'message' ]);
					}
					else{
						$this->addError( $field, Yii::t( 'yii', '{attribute} "{value}" has already been taken.', Array(
							'{attribute}' => $this->getAttributeLabel( $field ),
							'{value}' => CHtml::encode( $this->$field ),
						)));
					}
				}
			}
		}
		function requiredIfNewRecord( $field, $params = Array() ) {
			$AR = $this->getAR();
			if( !$AR ) return;
			
			if( !strlen( $this->$field ) and $AR->isNewRecord ) {
				if( !empty( $params[ 'message' ])) {
					$this->addI18NError( $field, $params[ 'message' ]);
				}
				else{
					$this->addError( $field, Yii::t( 'yii', '{attribute} cannot be blank.', Array(
						'{attribute}' => $this->getAttributeLabel( $field ),
					)));
				}
			}
		}
		function existsIDModel( $field, $params = Array() ) {
			if( !strlen( $this->$field )) return;
			if( !$this->hasErrors()) {
				$model = CActiveRecord::model( $params[ 'model' ]);
				if( !$model->existsByPk( $this->$field )) {
					$this->addI18NError( $field, 'Wrong {attribute}!', Array(
						'{attribute}' => $this->getAttributeLabel( $field ),
					));
				}
			}
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
	}

?>