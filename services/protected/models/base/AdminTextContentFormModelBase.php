<?
	Yii::import( 'models.base.FormModelBase' );
	
	abstract class AdminTextContentFormModelBase extends FormModelBase {
		public $textFields = array();
		
		protected $textModelName;
		protected $idLanguageFieldName;
		protected $idTextModelFieldName;
		protected $idArModelFieldName;
		
		protected function getSourceAttributeLabels() {
			$languages = LanguageModel::getAll();
			$labels = array();
			foreach( $languages as $language ){
				foreach( $this->textFields as $key => $val ){
					$labels[ "{$key}[{$language->id}]" ] = Yii::t('*', str_replace('_', ' ', join( preg_split('/(?<=[a-z])(?=[A-Z])/x', ucfirst($key)), " " )));
				}
			}
			return $labels;
		}
		
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();

				foreach( $this->textFields as $field => $type ) {
					foreach( $this->$field as $idLanguage=>$value ) {
						if( empty( $value )) $this->{$field}[$idLanguage] = null;
					}
				}
			}
		}
		
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array()) {
			$exceptFields = array_keys( $this->textFields );
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {	
				$AR = $this->getAR();
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					foreach( $this->textFields as $field => $type ) {
						$this->{$field}[ $i18n->idLanguage ] = $i18n->{$field};
					}
				}
				return true;
			}
			return false;
		}
		
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				
				$textModelName = $this->textModelName;
				$idLanguageFieldName = $this->idLanguageFieldName;
				$idTextModelFieldName = $this->idTextModelFieldName;
				$idArModelFieldName = $this->idArModelFieldName;
				
				foreach( $languages as $language ) {
					
					$model = $textModelName::model()->findByAttributes( Array( 
						$idTextModelFieldName => $AR->{$idArModelFieldName}, 
						$idLanguageFieldName => $language->id 
					));

					if ( !model ) {
						$model = new $textModelName;
						$model->{$idTextModelFieldName} = $AR->{$idArModelFieldName};
						$model->{$idLanguageFieldName} = $language->id;
					}

					$histModelsArr = array();
					foreach( $this->textFields as $field => $type ){
						
						$oldVal = trim( str_replace( array( "\n", "\r" ), ' ', mb_strtolower($model->{$field}) ) );
						$newVal = trim( str_replace( array( "\n", "\r" ), ' ', mb_strtolower($this->{$field}[ $language->id ]) ) );
						
						if( $oldVal != $newVal ){
							$histModelsArr[] = ManageTextContentHistoryModel::instance( Yii::app()->user->id, $model->{$idTextModelFieldName}, $textModelName, $field, $language->id, $oldVal, $newVal );
						}
						
						$model->{$field} = $this->{$field}[ $language->id ];
					}
						
					if (!$model->save()) {
						throw new Exception( "Can't save text model" );
					}else{
						foreach( $histModelsArr as $histModel ){
							$histModel->modelId = $model->{$idTextModelFieldName};
							$histModel->save();
						}
					}
					
				}

				return true;
			}
			throw new Exception( "Can't save main model" );
			return false;
		}
		
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		
	}

?>