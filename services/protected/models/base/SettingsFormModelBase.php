<?
	Yii::import( 'models.base.FormModelBase' );
	
	abstract class SettingsFormModelBase extends FormModelBase {
		public $textFields = array();
		
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array()) {
			$exceptFields = array_keys( $this->textFields );
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {	
				$this->loadTextFields();
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {		
				$this->saveTextFields();
				return true;
			}
			return false;
		}
		function loadTextFields() {	
			$AR = $this->getAR();
			if( !$i18ns = $AR->messages ) return true;
			$i18ns = $AR->messages->i18ns;
			foreach( $i18ns as $i18n ) {		
				$texts = json_decode($i18n->value);
				foreach( $this->textFields as $key => $val ){
					$this->{$key}[ $i18n->idLanguage ] = $texts->{$key};
				}
			}
			return true;
		}
		function saveTextFields() {
			$AR = $this->getAR();
			$i18ns = Array();
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ) {
				$i18ns[ $language->id ] = array();
				foreach( $this->textFields as $key => $val ){
					$i18ns[ $language->id ][$key] = trim( $this->{$key}[ $language->id ] );
				}
				$i18ns[ $language->id ] = (object)$i18ns[ $language->id ];
			}
			if( !$AR->createMessage() ) return false;
			$AR->setI18Ns( $i18ns );
			return true;
		}
		function setTextLabels( $labels ) {
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				foreach( $this->textFields as $key => $val ){
					$labels[ "{$key}[{$language->id}]" ] = join( preg_split('/(?<=[a-z])(?=[A-Z])/x', ucfirst($key)), " " );
				}
			}
			return $labels;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array( ));

			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
		function load( $id = 1 ) {
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR( null, Array(), Array( ));
				$this->loadFromPost();
				return true;
			}
			return false;
		}
	}

?>