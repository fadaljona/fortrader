<?
	Yii::import( 'models.base.ModelBase' );
	
	abstract class SettingsModelBase extends ModelBase {

		function relations() {
			return Array(
				'messages' => Array( self::HAS_ONE, 'MessageModel', array( 'id' => 'idMessage') ),
			);
		}
		function getTexts(){
			if( !$this->messages->currentLanguageI18N ) return false;
			return json_decode($this->messages->currentLanguageI18N->value);
		}
		public function getTextSetting( $settingName ){
			if( !$this->texts || !$this->texts->{$settingName} ) return false;
			return $this->texts->{$settingName};
		}
		function createMessage(){
			if( $this->messages ) return true;
			if( !$this->messages ){
				$this->messages = MessageModel::instance( $this->getCleanClassName() . $this->id , $this->getCleanClassName() );
				$this->messages->type = 'model';
				if( $this->messages->save() ){
					$this->idMessage = $this->messages->id;
					$this->save();
					return true;
				}else{
					return false;
				}		
			}
		}
		function deleteI18Ns( $removeMessage = false ) {
			if( $this->messages ){
				foreach( $this->messages->i18ns as $i18n ) {
					$i18n->delete();
				}
			}
			if( $this->messages && $removeMessage ) $this->messages->delete();
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			
			foreach( $i18ns as $idLanguage=>$value ) {
				$summText = '';
				foreach( $value as $textVal ) $summText .= $textVal;
				
				if( $summText && in_array( $idLanguage, $idsLanguages )) {
					$i18n = MessageI18NModel::model()->findByAttributes( Array( 'idMessage' => $this->messages->id, 'idLanguage' => $idLanguage ));
					$jsonVal = json_encode($value);
					if( !$i18n ) {
						$i18n = MessageI18NModel::instance( $this->messages->id, $idLanguage, $jsonVal );
						$i18n->save();
					}
					elseif( $i18n->value != $jsonVal ){
						$i18n->value = $jsonVal;
						$i18n->save();
					}
				}
			}
		}
		function updateI18NsTextContent( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			
			foreach( $i18ns as $idLanguage=>$obj ) {
				$summText = '';
				
				foreach( $obj as $textVal ) $summText .= $textVal;
				
				if( $summText && in_array( $idLanguage, $idsLanguages )) {
					$i18n = MessageI18NModel::model()->findByAttributes( Array( 'idMessage' => $this->messages->id, 'idLanguage' => $idLanguage ));
					$jsonVal = json_encode($obj);
					if( !$i18n ) {
						$i18n = MessageI18NModel::instance( $this->messages->id, $idLanguage, $jsonVal );
						$i18n->save();
						
						foreach( $obj as $key => $value ){
							
							$oldVal = '';
							$newVal = trim( str_replace( array( "\n", "\r" ), ' ', mb_strtolower($obj->{$key}) ) );
								
							$histModel = ManageTextContentHistoryModel::instance( Yii::app()->user->id, 1, get_class($this), $key, $idLanguage, $oldVal, $newVal );
							$histModel->save();
							
						}
					}
					elseif( $i18n->value != $jsonVal ){
						
						$oldObj = json_decode($i18n->value);
						foreach( $oldObj as $key => $value ){
							if( property_exists($obj, $key) && $obj->{$key} != $oldObj->{$key} ){
								
								
								$oldVal = trim( str_replace( array( "\n", "\r" ), ' ', mb_strtolower($oldObj->{$key}) ) );
								$newVal = trim( str_replace( array( "\n", "\r" ), ' ', mb_strtolower($obj->{$key}) ) );
								
								$histModel = ManageTextContentHistoryModel::instance( Yii::app()->user->id, 1, get_class($this), $key, $idLanguage, $oldVal, $newVal );
								$histModel->save();
								
								$oldObj->{$key} = $obj->{$key};
							}
						}
						$i18n->value = json_encode($oldObj);
						$i18n->save();
						
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns( true );
		}
	}

?>