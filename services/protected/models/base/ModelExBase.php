<?

	abstract class ModelExBase extends CActiveRecord {
		public $_dbName;
		public $beenNewRecord = false;
			// static common
		static function _getModelClass( $name ) {
			return "{$name}Model";
		}
		static function _getModelName( $class ) {
			$class = preg_replace( "#Model$#", "", $class );
			return $class;
		}
		static function _getModelTableName( $modelName ) {
			$tabName = $modelName;
			$tabName = preg_replace( "#I18N$#", "I18n", $tabName );
			$tabName = lcfirst( $tabName );
			$tabName = preg_replace( "#[A-Z]#", '_$0', $tabName );
			$tabName = strtolower( $tabName );
			return $tabName;
		}
		static function _getModelListURL( $class = __CLASS__ ) {
			return CActiveRecord::model( $class )->_getListURL();
		}
		static function _getModelColumnVariants( $column, $parameters = Array()) {
			$dbType = $column->dbType;
			$patternArray = '#^enum\(|^set\(#';
			if( preg_match( $patternArray, $column->dbType )) {
				$dbType = preg_replace( $patternArray, "return Array(", $dbType );
				$dbType = preg_replace( '#\)$#', ");", $dbType );
				$variants = eval( $dbType );
				$variants = Commonlib::makeAssoc( $variants );
				return $variants;
			}
			return Array();
		}
			// access
		function __isset( $name ) {
			if( parent::__isset( $name ))
				return true;
			
			if( method_exists( $this, "get{$name}" ))
				return true;
				
			if( method_exists( $this, "set{$name}" ))
				return true;
			
			if( isset( $this->getMetaData()->columns[ $name ])) 
				return true;
				
			return false;
		}
			// coommon
		function _getName() {
			$class = get_class( $this );
			return self::_getModelName( $class );
		}
		function _getTabName() {
			$modelName = $this->_getName();
			return self::_getModelTableName( $modelName );
		}
		function _getPkName() {
			return $this->getMetaData()->tableSchema->primaryKey;
		}
		function _getFirstPkName() {
			$pks = $this->_getPkName();
			return is_array( $pks ) ? $pks[0] : $pks;
		}
		function _getColumn( $name ) {
			return @$this->getMetaData()->columns[ $name ];
		}
		function _getColumns() {
			return $this->getMetaData()->columns;
		}
		function _getNotPkColumns() {
			$pksNames = (array)$this->_getPkName();
			$notPkColumns = Array();
			foreach( $this->getMetaData()->columns as $column ) {
				if( !in_array( $column->name, $pksNames )) {
					$notPkColumns[] = $column;
				}
			}
			return $notPkColumns;
		}
		function _getColumnNames() {
			return $this->getMetaData()->tableSchema->getColumnNames();
		}
		function _getNotPkColumnNames() {
			$notPkColumns = $this->_getNotPkColumns();
			return CommonLib::slice( $notPkColumns, 'name' );
		}
		function _getFirstColumnName() {
			$columns = $this->_getColumnNames();
			return $columns[0];
		}
		function _getFirstNotPkColumnName() {
			$columns = $this->_getNotPkColumnNames();
			return $columns[0];
		}
		function _issetColumn( $column ) {
			return isset( $this->getMetaData()->columns[ $column ]);
		}
		function _getRelation( $name ) {
			return @$this->getMetaData()->relations[ $name ];
		}
		function _getRelations() {
			return $this->getMetaData()->relations;
		}
		function _getNSi18n() {
			$class = get_class( $this );
			return "models/{$class}";
		}
		function _translateR( $array ) {
			$_NSi18n = $this->_getNSi18n();
			return $array = Yii::t( $_NSi18n, $array );
		}
		function _getListURL() {
			$name = $this->_getName();
			$name = lcfirst( $name );
			return Yii::App()->createURL( "{$name}/list" );
		}
		function _getFieldVariants( $filed, $parameters = Array()) {
			if( $column = $this->_getColumn(  $filed )) {
				return $this->_getColumnVariants( $column, $parameters );
			}
			return Array();
		}
		function _getColumnVariants( $column, $parameters = Array()) {
			$variants = self::_getModelColumnVariants( $column, $parameters = Array());
			$variants = $this->_translateR( $variants );
			return $variants;
		}
		function _getLightListCriteria( $cDef = null ) {
			$modelName = $this->_getName();
			$c = new CDbCriteria();
				
				// select
			$c->select = Array( $this->_getFirstPkName() );
			
				// add I18N
			if( I18NModelInterface::hasModelRelations( $modelName )) {
				$I18NModelClass = I18NModelInterface::getI18NModelClass( $modelName );
				$CLI18NRelationName = I18NModelInterface::getCLI18NRelationName();
				$I18NColumns = CActiveRecord::model( $I18NModelClass )->_getNotPkColumnNames();
				
					// relation
				$c->with[ $CLI18NRelationName ] = Array(
					'select' => $I18NColumns[0],
				);
				
				$CLI18NAlias = I18NModelInterface::getCLI18NAlias( $modelName );
				if( !$cDef or !$cDef->order )
					$c->order = " `{$CLI18NAlias}`.`{$I18NColumns[0]}` ASC ";
			}
			
			if( $cDef ) {
				$c->mergeWith( $cDef );
			}
			
			return $c;
		}
		function _getLightList() {
			$modelName = $this->_getName();
			$pkName = $this->_getFirstPkName();
			
			$list = $this->findAll( $this->_getLightListCriteria() );
			if( I18NModelInterface::hasModelRelations( $modelName )) {
				$I18NModelClass = I18NModelInterface::getI18NModelClass( $modelName );
				$firstNotPkColumnName = CActiveRecord::model( $I18NModelClass )->_getFirstNotPkColumnName();
			}
			else{
				$firstNotPkColumnName = $this->_getFirstNotPkColumnName();
			}
			$list = CommonLib::toAssoc( $list, $pkName, $firstNotPkColumnName );
			return $list;
		}
			// for CActiveRecord
		function tableName() {
			$name = $this->_getTabName();
			if( $this->_dbName ) return "[{$this->_dbName}].{$name}";
			return "{{{$name}}}";
		}
		function existsByPk( $pk ) {
			$pkName = $this->_getPkName();
			$c = new CDbCriteria();
			$c->addColumnCondition( Array(
				$pkName => $pk
			));
			return $this->exists( $c );
		}
		function update( $attributes = null ) {
			return $this->getIsNewRecord() ? $this->insert( $attributes ) : parent::update( $attributes );
		}
			# crypt
		function _crypt( $data ) {
			return CommonLib::crypt( $data, Yii::App()->params['keyCrypt']);
		}
		function _decrypt( $data ) {
			return CommonLib::decrypt( $data, Yii::App()->params['keyCrypt']);
		}
		function _setCrypt( $filed, $value ) {
			$value = $this->_crypt( $value );
			$this->$filed = $value;
		}
		function _getCrypt( $filed ) {
			$value = $this->$filed;
			$value = $this->_decrypt( $value );
			return $value;
		}
		function _deleteRelationModels( $exceptRelations = Array()) {
			foreach( $this->_getRelations() as $relation ) {
				if( in_array( $relation->name, $exceptRelations )) continue;
				if( $relation instanceOf CHasManyRelation or $relation instanceOf CHasOneRelation ) {
					foreach( (array)$this->{$relation->name} as $model ) {
						$model->delete();
					}
				}
			}
		}
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( $this->isNewRecord ) {
					$this->beenNewRecord = true;
					if( isset( $this->getMetaData()->columns[ 'createdDT' ]) and !$this->createdDT ) {
						$this->createdDT = date( "Y-m-d H:i:s" );
					}
				}
				if( isset( $this->getMetaData()->columns[ 'updatedDT' ])) {
					$this->updatedDT = date( "Y-m-d H:i:s" );
				}
			}
			return $result;
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->_deleteRelationModels();
		}
	}

?>