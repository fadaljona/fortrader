<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_i18n}}";
		}
		static function instance( $idEA, $idLanguage, $about ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idEA', 'idLanguage', 'about' ));
		}
	}

?>