<?php
Yii::import('models.base.ModelBase');

final class FaqI18NModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return "{{faq_i18n}}";
    }

    public static function instance($idFaq, $idLanguage, $title)
    {
        return self::modelFromAssoc(__CLASS__, compact('idFaq', 'idLanguage', 'title'));
    }
}
