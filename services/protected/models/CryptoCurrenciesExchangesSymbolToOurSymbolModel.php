<?php
Yii::import('models.base.ModelBase');

final class CryptoCurrenciesExchangesSymbolToOurSymbolModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public static function instance($idExchange, $symbol)
    {
        return self::modelFromAssoc(__CLASS__, compact('idExchange', 'symbol'));
    }

    public function relations()
    {
        return array(
            'ourSymbol' => array(self::HAS_ONE, 'QuotesSymbolsModel', array('id' => 'symbolId')),
            'exchange' => array(self::HAS_ONE, 'CryptoCurrenciesExchangesModel', array('id' => 'idExchange')),
            'stats' => array( self::HAS_ONE, 'CryptoCurrenciesExchangesSymbolStatsModel', 'idSymbolToSymbol',
                'on' => ' `stats`.`date` = :date ',
                'params' => array(':date' => date('Y-m-d'))
            ),

        );
    }

    public static function getAdminListDp($filterModel = false)
    {
        $condition = '';
        $params = array();
        if (Yii::app()->request->getParam('idExchange')) {
            $condition = " `t`.`idExchange` = :idExchange ";
            $params['idExchange'] = Yii::app()->request->getParam('idExchange');
        }
        $DP = new CActiveDataProvider(get_called_class(), array(
            'criteria' => array(
                'with' => array('ourSymbol'),
                'order' => ' `ourSymbol`.`id` DESC ',
                'condition' => $condition,
                'params' => $params
            ),
        ));
        return $DP;
    }
}
