<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ConversationWatchModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idObj ) {
			
			self::model()->deleteAll('lastRequest < :TwoMinutes', array(':TwoMinutes'=> date("Y-m-d H:i:s", time() - 120 ) ));
			
			if( Yii::App()->user->id ){
			
				$model = self::model()->find('idObj=:idObj and idUser=:idUser', array(':idObj'=>$idObj, ':idUser'=>Yii::App()->user->id ));
				if($model){
					$model->lastRequest = date("Y-m-d H:i:s");
					return $model;
				}else{
					$model = new self();
					$model->idObj = $idObj;
					$model->idUser = Yii::App()->user->id;
					$model->lastRequest = date("Y-m-d H:i:s");
					return $model;
				}
			}
		}
		function rules() {
			return array(
				array('idObj', 'unique', 'criteria'=>array(
					'condition'=>'`idUser`=:secondKey',
					'params'=>array(
						':secondKey'=>$this->idUser
					)
				)),
			);
		}
		public function relations(){

			return array(
				'user'=>array(self::HAS_ONE, 'UserModel', array('ID'=>'idUser') ),
			);
		}
		
	}

?>
