<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestWinerModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'contest' => Array( self::BELONGS_TO, 'ContestModel', 'idContest' ),
				'member' => Array( self::BELONGS_TO, 'ContestMemberModel', 'idMember' ),
			);
		}
		static function instance( $idContest, $idMember, $place, $prize ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idContest', 'idMember', 'place', 'prize' ));
		}
		function getIcon() {
			$numClass = min( $this->place, 4);
			$class = "iI i14 i15_{$numClass}";
			$title = "{$this->place} ".Yii::t( 'lower', 'place' )." - {$this->contest->name} ";
			
			$params = Array(
				'class' => $class,
				'title' => $title,
			);
			return CHtml::tag( 'i', $params, '', true );
		}
		protected function afterSave() {
			if( 
				$this->beenNewRecord
				and $member = ContestMemberModel::model()->findByPk( $this->idMember ) 
			) {
				UserReputationModel::event( UserReputationModel::TYPE_EVENT_CREATE_CONTEST_WINER, Array( 'idUser' => $member->idUser ));
				UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_CONTEST_WINER, Array( 'idUser' => $member->idUser, 'idContest' => $member->idContest ));
			}
			parent::afterSave();
		}
		protected function afterDelete() {
			if( 
				$this->place == '1' 
				and $member = ContestMemberModel::model()->findByPk( $this->idMember ) 
			) {
				UserReputationModel::event( UserReputationModel::TYPE_EVENT_DELETE_CONTEST_WINER, Array( 'idUser' => $member->idUser ));
			}
			parent::afterDelete();
		}
	}

?>