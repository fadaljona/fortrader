<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CalendarEvent2I18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{calendar_event2_i18n}}";
		}


		static function instance( $idEvent, $idLanguage, $indicator ) {
			return self::modelFromAssoc( __CLASS__, compact( 'id', 'idLanguage', 'indicator' ));
		}

			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			return $result;
		}
	}

?>
