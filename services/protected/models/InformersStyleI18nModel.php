<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InformersStyleI18nModel extends ModelBase {

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}

		function tableName() {
			return "{{informers_style_i18n}}";
		}
		
		static function instance( $idStyle, $idLanguage, $title ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idStyle', 'idLanguage', 'title' ));
		}

	}
?>