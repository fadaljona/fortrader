<?
	Yii::import( 'models.base.ModelBase' );
	
	final class NewsAggregatorSourceModel extends ModelBase {
		const PATHUploadDir = 'uploads/rss';
		const WIDTHThumb = 17;
		const HEIGHTThumb = 'auto';
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'NewsAggregatorSourceI18nModel', 'idSource', 'alias' => 'i18nsNewsAggregatorSource' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'NewsAggregatorSourceI18nModel', 'idSource', 
					'alias' => 'cLI18NNewsAggregatorSourceModel',
					'on' => '`cLI18NNewsAggregatorSourceModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		static function getListDp( $searchKey ){
			$c = new CDbCriteria();
			
			if( $searchKey ){
				$c->with['currentLanguageI18N'] = array();
				$c->addCondition( " `cLI18NNewsAggregatorSourceModel`.`title` like :searchKey OR `t`.`url` like :searchKey " );
				$c->params[':searchKey'] = '%' . $searchKey . '%';
			}
		
			$c->addCondition( " `t`.`enabled` = 1 " );
			$c->order = " `t`.`order` DESC ";
			
			$DP = new CActiveDataProvider( 'NewsAggregatorSourceModel', Array(
				'criteria' => $c,
			));
			return $DP;
		}
		static function getAdminListDp( $filterModel = false ){
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`order` ASC ',
				),
			));
			return $DP;
		}
		function uploadImage( $uploadedFile ) {	
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";

			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );
			
			$thumbFileFullName = "{$fileName}-thumb.{$fileEx}";
			$thumbFilePath = self::PATHUploadDir."/{$thumbFileFullName}";
			ImgResizeLib::resize( $filePath, $thumbFilePath, self::WIDTHThumb, self::HEIGHTThumb );
			
			$this->setImage( $fileFullName, $thumbFileFullName );
		}
		function getPathLogo() {
			return strlen($this->logo) ? self::PATHUploadDir."/{$this->logo}" : '';
		}
		function getPathTmbLogo() {
			return strlen($this->tmbLogo) ? self::PATHUploadDir."/{$this->tmbLogo}" : '';
		}
		function getSrcTmbLogo() {
			if( $this->pathTmbLogo ){
				return Yii::app()->baseUrl."/{$this->pathTmbLogo}";
			}elseif( $this->pathLogo ){
				return Yii::app()->baseUrl."/{$this->pathLogo}";
			}else{
				return '';
			}
		}
		function deleteImage() {
			if( strlen( $this->logo )) {
				if( is_file( $this->pathLogo ) and !@unlink( $this->pathLogo )) throw new Exception( "Can't delete {$this->pathLogo}" );
				$this->logo = null;
			}
			if( strlen( $this->tmbLogo )) {
				if( is_file( $this->pathTmbLogo ) and !@unlink( $this->pathTmbLogo )) throw new Exception( "Can't delete {$this->pathTmbLogo}" );
				$this->tmbLogo = null;
			}
		}
		function setImage( $name, $nameThumb ) {
			$this->deleteImage();
			$this->logo = $name;
			$this->tmbLogo = $nameThumb;
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->name )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->name )) return $i18n;
			}
		}
		function getTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->title : '';
		}
		
		function getSingleUrl() {
			return Yii::app()->createUrl('newsAggregator/sourceLink', array('id' => $this->id) );
		}

			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$settings = NewsAggregatorSettingsModel::getModel();
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->title )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = NewsAggregatorSourceI18nModel::model()->findByAttributes( Array( 'idSource' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = NewsAggregatorSourceI18nModel::instance( $this->id, $idLanguage, $obj->title );
					}else{
						$i18n->title = $obj->title;
					}
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>