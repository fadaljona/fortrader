<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MailTplI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{mail_tpl_i18n}}";
		}
		static function instance( $idTpl, $idLanguage, $subject, $message ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idTpl', 'idLanguage', 'subject', 'message' ));
		}
	}

?>