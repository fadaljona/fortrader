<?
	Yii::import( 'models.base.ModelBase' );

	final class UserBalanceChangeModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idUser, $value ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser', 'value' ));
		}
	}

?>