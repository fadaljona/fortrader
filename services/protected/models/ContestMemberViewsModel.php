<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestMemberViewsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function rules() {
			return Array(
				Array( 'idMember', 'unique'),
				Array( 'idMember', 'numerical', 'integerOnly'=>true , 'min' => 1),
				Array( 'views', 'numerical', 'integerOnly'=>true , 'min' => 0),
			);
		}
		static function instance( $idMember, $views ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idMember', 'views' ));
		}
		static function saveViews( $idMember ){
			$model = self::model()->findByAttributes( Array( 'idMember' => $idMember ));
			if( !$model ) $model = self::instance( $idMember, 0 );
			$model->views += 1 ;
			if( $model->validate() ) $model->save();
		}
		
	}

?>