<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MessageKeyModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'values' => Array( self::HAS_MANY, 'MessageValueModel', 'idKey' ),
				'currentLanguageValue' => Array( self::HAS_ONE, 'MessageValueModel', 'idKey', 
					'on' => '`currentLanguageValue`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		static function instance( $text, $ns ) {
			$model = new self();
			$model->key = $text;
			$model->ns = $ns;
			return $model;
		}
		function getCurrentLanguageValue() {
			return $this->currentLanguageValue ? $this->currentLanguageValue->value : $this->key;
		}
		function deleteValues() {
			foreach( $this->values as $value ) {
				$value->delete();
			}
		}
		function addValues( $values ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $values as $idLanguage=>$text ) {
				if( strlen( $text ) and in_array( $idLanguage, $idsLanguages )) {
					$value = MessageValueModel::model()->findByAttributes( Array( 'idKey' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$value ) {
						$value = MessageValueModel::instance( $this->id, $text, $idLanguage );
					}
					else{
						$value->value = $text;
					}
					$value->save();
				}
			}
		}
		function setValues( $values ) {
			$this->deleteValues();
			$this->addValues( $values );
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteValues();
		}
	}

?>