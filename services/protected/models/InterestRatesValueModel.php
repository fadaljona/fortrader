<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InterestRatesValueModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'InterestRatesValueI18nModel', 'idValue', 'alias' => 'i18nsInterestRatesValue' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'InterestRatesValueI18nModel', 'idValue', 
					'alias' => 'cLI18NInterestRatesValueModel',
					'on' => '`cLI18NInterestRatesValueModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		
		static function getAdminListDp( $filterModel = false ){
			$idRate = @$_GET['idRate'] ? @$_GET['idRate'] : 0;
			if( !$idRate ) return false;
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'condition' => " `t`.`idRate` = :idRate ",
					'order' => ' `t`.`modDate` DESC ',
					'params' => array( ':idRate' => $idRate )
				),
			));
			return $DP;
		}
		static function getTotalReviews( ){
			return Yii::App()->db->createCommand("
				SELECT		COUNT( `val`.`id` )
				FROM 		`{{interest_rates_value}}` `val` 
				LEFT JOIN  `{{interest_rates_value_i18n}}` `i18n` ON `val`.`id` = `i18n`.`idValue` AND `i18n`.`idLanguage` = :idLanguage
				WHERE 		`i18n`.`postId` <> 0 AND `i18n`.`postId` IS NOT NULL
			")->queryScalar( Array( ':idLanguage' => LanguageModel::getCurrentLanguageID() ) );
		}
		static function getTotalReviewsByParams( $lastDate, $lastId ){
			return Yii::App()->db->createCommand("
				SELECT		COUNT( `val`.`id` )
				FROM 		`{{interest_rates_value}}` `val` 
				LEFT JOIN  `{{interest_rates_value_i18n}}` `i18n` ON `val`.`id` = `i18n`.`idValue` AND `i18n`.`idLanguage` = :idLanguage
				WHERE 		`i18n`.`postId` <> 0 AND `i18n`.`postId` IS NOT NULL AND `val`.`modDate` <= :lastDate AND `val`.`id` <> :lastId
				ORDER BY	`val`.`modDate` DESC, `val`.`id` ASC
			")->queryScalar( Array( ':idLanguage' => LanguageModel::getCurrentLanguageID(), ':lastDate' => $lastDate, ':lastId' => $lastId ) );
		}
		
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->link )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->link )) return $i18n;
			}
		}
		function getLink() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->link : '';
		}
		function getPostId() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->postId : '';
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
		
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->link )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = InterestRatesValueI18nModel::model()->findByAttributes( Array( 'idValue' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = InterestRatesValueI18nModel::instance( $this->id, $idLanguage, $obj->link, $obj->postId );
					}else{
						$i18n->link = $obj->link;
						$i18n->postId = $obj->postId;
					}
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}

		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				$prevModel = self::model()->find(array(
					'select' => array('modDate', 'value' ),
					'condition' => " `t`.`idRate` = :idRate AND `t`.`modDate` < :modDate AND `t`.`value` <> :value ",
					'params' => array( ':idRate' => $this->idRate, ':modDate' => $this->modDate, ':value' => $this->value ),
					'order' => ' `t`.`modDate` DESC ',
				));
				if( $prevModel ){
					$this->difValue = $this->value - $prevModel->value;
					$changedModel = self::model()->find(array(
						'select' => array('modDate'),
						'condition' => " `t`.`idRate` = :idRate AND `t`.`modDate` > :modDate ",
						'params' => array( ':idRate' => $this->idRate, ':modDate' => $prevModel->modDate ),
						'order' => ' `t`.`modDate` ASC ',
					));
					$this->dateChanged = $changedModel->modDate;
				}else{
					$this->difValue = 0;
					$this->dateChanged = $this->modDate;
				}
				
				unset( $prevModel );
				$prevModel = self::model()->find(array(
					'select' => array('modDate', 'value' ),
					'condition' => " `t`.`idRate` = :idRate AND `t`.`modDate` < :modDate ",
					'params' => array( ':idRate' => $this->idRate, ':modDate' => $this->modDate),
					'order' => ' `t`.`modDate` DESC ',
				));
				if( $prevModel ){
					$this->difValueFromPrev = $this->value - $prevModel->value;
				}else{
					$this->difValueFromPrev = 0;
				}
				
				$cacheKey = 'InterestRatesValueLastVal' . $this->idRate;
				Yii::app()->cache->delete( $cacheKey );
				
				$cacheKey = 'InterestRatesValueChartValues' . $this->idRate;
				Yii::app()->cache->delete( $cacheKey );
			}
			return $result;
		}
		
	}

?>