<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerRegulatorI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{broker_regulator_i18n}}";
		}
		static function instance( $idRegulator, $idLanguage, $name, $link ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idRegulator', 'idLanguage', 'name', 'link' ));
		}
	}

?>