<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AgentModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $value ) {
			return self::modelFromAssoc( __CLASS__, compact( 'value' ));
		}
		static function addAgent( $value ) {
			$agent = AgentModel::model()->findByAttributes( Array(
				'value' => $value ? $value : '',
			));
			
			if( !$agent ) {
				$agent = self::instance( $value );
				$agent->save();
			}
			
			return $agent;
		}
		static function getCurrentIDAgent() {
			$agent = self::addAgent( $_SERVER[ 'HTTP_USER_AGENT' ]);
			return $agent->id;
		}
	}

?>