<?php

Yii::import('models.base.ModelBase');

final class PaymentExchangerI18nModel extends ModelBase
{
    static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    function tableName()
    {
        return "{{payment_exchanger_i18n}}";
    }

    static function instance($idExchanger, $idLanguage, $title)
    {
        return self::modelFromAssoc(__CLASS__, compact('idExchanger', 'idLanguage', 'title'));
    }
}
