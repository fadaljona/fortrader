<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserSettingsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser' ));
		}
		
		protected function afterSave() {
			parent::afterSave();
			if( !$this->beenNewRecord ) {
				
				$user = UserModel::model()->findByPk( $this->idUser );
				$likeEmail = CommonLib::escapeLike( $user->user_email );
				$likeEmail = CommonLib::filterSearch( $likeEmail );
					
				if( !$this->receiveCommentsOnMyArticles ){
					$myPosts = WPPostModel::model()->findAll(array(
						'with' => array( 'decomSubscribers' ),
						'condition' => " `t`.`post_author` = :idUser AND `decomSubscribers`.`meta_value` <> '' AND `decomSubscribers`.`meta_value` IS NOT NULL AND `decomSubscribers`.`meta_value` LIKE :email ",
						'params' => array( ':idUser' => $this->idUser, ':email' => $likeEmail )
					));
					foreach( $myPosts as $post ){
						DecommentsPostsModel::removeEmailFromMeta($post->decomSubscribers, $user->user_email);
					}
				}
				if( !$this->receiveCommentsOnMyProfile ){
					$postKey = 'user.single.user_' . $this->idUser;
					$cat = 'singleUserProfile';
					$postId = DecommentsPostsModel::getPostId( $postKey, $cat, true );
					
					$myPost = WPPostModel::model()->find(array(
						'with' => array( 'decomSubscribers' ),
						'condition' => " `t`.`ID` = :postId AND `decomSubscribers`.`meta_value` <> '' AND `decomSubscribers`.`meta_value` IS NOT NULL AND `decomSubscribers`.`meta_value` LIKE :email ",
						'params' => array( ':postId' => $postId, ':email' => $likeEmail )
					));
					if( $myPost ) DecommentsPostsModel::removeEmailFromMeta($myPost->decomSubscribers, $user->user_email);
				}
				if( !$this->receiveCommentsOnMyMonitoringAccounts ){
					$myMembers = ContestMemberModel::model()->findAll(array(
						'condition' => " `t`.`idUser` = :idUser ",
						'params' => array( ':idUser' => $this->idUser )
					));
					
					$cat = 'singleAccountMonitoring' . DecommentsPostsModel::getCatLangSuffix();
					$postKey = 'monitoring.single.monitoring_';
					
					foreach( $myMembers as $member ){
						$decommentsPost = DecommentsPostsModel::model()->find(array(
							'condition' => " `t`.`cat` = :cat AND `t`.`postKey` = :postKey ",
							'params' => array( ':cat' => $cat, ':postKey' => $postKey . $member->id )
						));
						if( $decommentsPost ){
							$myPost = WPPostModel::model()->find(array(
								'with' => array( 'decomSubscribers' ),
								'condition' => " `t`.`ID` = :postId AND `decomSubscribers`.`meta_value` <> '' AND `decomSubscribers`.`meta_value` IS NOT NULL AND `decomSubscribers`.`meta_value` LIKE :email ",
								'params' => array( ':postId' => $decommentsPost->id, ':email' => $likeEmail )
							));
							if( $myPost ) DecommentsPostsModel::removeEmailFromMeta($myPost->decomSubscribers, $user->user_email);
						}
					}
				}
			}
		}
	}

?>