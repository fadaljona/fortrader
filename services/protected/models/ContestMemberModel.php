<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestMemberModel extends ModelBase {
		public $countOrders;
		public $countFilledOrders;
		public $countCanceledOrders;
		public $sumContestPrizes = 0;
		public $countRowsMT4AccountHistory;
		public $maxOrderCloseTimeMT4AccountHistory;
		public $maxOrderTicketMT4AccountHistory;
		public $revision;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'contest' => Array( self::BELONGS_TO, 'ContestModel', 'idContest' ),
				'server' => Array( self::BELONGS_TO, 'ServerModel', 'idServer' ),
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'winer' => Array( self::HAS_ONE, 'ContestWinerModel', 'idMember' ),
				'stats' => Array( self::HAS_ONE, 'ContestMemberStatsModel', 'idMember' ),
				'monitoringStats' => Array( self::HAS_ONE, 'ContestMemberMonitoringStatsModel', 'idMember' ),
				'views' => Array( self::HAS_ONE, 'ContestMemberViewsModel', 'idMember' ),
				'monitoringDecommentsPosts' => Array( self::HAS_ONE, 'DecommentsPostsModel', '', 'on' => " `monitoringDecommentsPosts`.`cat` = 'singleAccountMonitoring".DecommentsPostsModel::getCatLangSuffix()."' AND `monitoringDecommentsPosts`.`postKey` = CONCAT('monitoring.single.monitoring_', `t`.`id`) " ),
				//'mt5AccountData' => Array( self::HAS_ONE, 'MT5AccountDataModel', '', 'on' => " `mt5AccountData`.`id` = ( SELECT MAX( `id` ) FROM `mt5_account_data` WHERE `ACCOUNT_NUMBER` = `t`.`accountNumber` ) " ),
				'mt5AccountInfo' => Array( self::HAS_ONE, 'MT5AccountInfoModel', '', 'on' => " `mt5AccountInfo`.`ACCOUNT_NUMBER` = `t`.`accountNumber` AND `mt5AccountInfo`.`ServerUrl` = `server`.`url` " ),
				'report' => Array( self::HAS_MANY, 'ContestMemberReportModel', 'idMember' ),
				'commonRatingStats' => Array( self::HAS_ONE, 'CommonRatingItemStatsModel', '', 'on' => " `t`.`id` = `commonRatingStats`.`idModel` AND `commonRatingStats`.`modelName` = 'ContestMember' " ),
			);
		}
		public function rules(){

			return array(
				array('accountNumber', 'unique', 'criteria'=>array(
					'condition'=>'`idServer`=:secondKey',
					'params'=>array(
					':secondKey'=>$this->idServer
					)
				)),
			);
		}
		function getAbsoluteSingleURL() {
			return Yii::App()->createAbsoluteURL( 'contestMember/single', Array( 'id' => $this->id ));
		}
		function getAbsoluteMonitoringSingleURL() {
			return Yii::App()->createAbsoluteURL( 'monitoring/single', Array( 'id' => $this->id ));
		}
		function getMemberMt5AccountInfo(){
			return MT5AccountInfoModel::model()->find(array(
				'condition' => " `t`.`ACCOUNT_NUMBER` = :accountNumber AND `t`.`ServerUrl` = :serverUurl ",
				'params' => array( ':accountNumber' => $this->accountNumber, ':serverUurl' => $this->server->url )
			));
		}
		function getAccountType(){
			$accountInfo = $this->memberMt5AccountInfo;
			if( !$accountInfo ) return '-';
			if( $accountInfo->IsDemo ) return 'demo';
			return 'live';
		}
		
		public function importReportData( $symbol, $data ){
			
			$query = " INSERT IGNORE INTO `mt4_account_history` (`AccountNumber`, `AccountServerId`, `OrderTicket`, `OrderType`, `OrderOpenTime`, `OrderCloseTime`, `OrderLots`, `OrderSymbol`, `OrderOpenPrice`, `OrderStopLoss`, `OrderTakeProfit`, `OrderClosePrice`, `OrderProfit`) VALUES ";
			
			$params = array( ':AccountNumber' => $this->accountNumber, ':AccountServerId' => $this->idServer, ':OrderSymbol' => $symbol );
			
			$i=0;
			$endOrderKey = 1;
			
			foreach( $data as $orderId => $orderData ){
				if( $i ) $query .= ',';
				

				$query .= "( :AccountNumber, :AccountServerId, :OrderTicket$i, :OrderType$i, :OrderOpenTime$i, :OrderCloseTime$i, :OrderLots$i, :OrderSymbol, :OrderOpenPrice$i, :OrderStopLoss$i, :OrderTakeProfit$i, :OrderClosePrice$i, :OrderProfit$i )";
				
				$params[":OrderTicket$i"] = $orderId;
				$params[":OrderType$i"] = $orderData['type'] == 'buy' ? 0 : 1;
				$params[":OrderOpenTime$i"] = strtotime( str_replace('.', '-', $orderData['openTime']) . ':00' );
				$params[":OrderCloseTime$i"] = strtotime( str_replace('.', '-', $orderData['closeTime']) . ':00' );
				$params[":OrderLots$i"] = $orderData['lots'];
				$params[":OrderOpenPrice$i"] = $orderData['openPrice'];
				$params[":OrderStopLoss$i"] = $orderData['stopLoss'];
				$params[":OrderTakeProfit$i"] = $orderData['takeProfit'];
				$params[":OrderClosePrice$i"] = $orderData['closePrice'];
				$params[":OrderProfit$i"] = $orderData['profit'];
				$endOrderKey = $orderId;
				$i++;		
			}
			
			if( count($params) != 3 ){
				$insertResult = Yii::App()->db->createCommand( $query )->query( $params );
			}
			
			
			
			$query = " INSERT IGNORE INTO `mt5_account_info` (`ACCOUNT_NUMBER`, `ServerUrl`, `ACCOUNT_UPDATE`, `last_error_code`, `IsDemo`) VALUES ( :ACCOUNT_NUMBER, :ServerUrl, :ACCOUNT_UPDATE, :last_error_code, 1 )";
			
			unset( $params );
			
			$params = array(
				':ACCOUNT_NUMBER' => $this->accountNumber, 
				':ServerUrl' => $this->server->url, 
				':ACCOUNT_UPDATE' => time(), 
				':last_error_code' => 0
			);
			
			$insertResult = Yii::App()->db->createCommand( $query )->query( $params );
			
			
			$query = " INSERT IGNORE INTO `mt5_account_data` (`ACCOUNT_NUMBER`, `AccountServerId`, `ACCOUNT_BALANCE`) VALUES ( :ACCOUNT_NUMBER, :AccountServerId, :ACCOUNT_BALANCE )";
			
			unset( $params );
			
			$endOrder = end( $data );
			
			$params = array(
				':ACCOUNT_NUMBER' => $this->accountNumber, 
				':AccountServerId' => $this->server->id, 
				':ACCOUNT_BALANCE' => $endOrder['balance'], 
			);
			
			$insertResult = Yii::App()->db->createCommand( $query )->query( $params );
			
			
			
			$query = " INSERT IGNORE INTO `mt4_account_history` (`AccountNumber`, `AccountServerId`, `OrderTicket`, `OrderType`, `OrderOpenTime`, `OrderCloseTime`, `OrderLots`, `OrderSymbol`, `OrderOpenPrice`, `OrderStopLoss`, `OrderTakeProfit`, `OrderClosePrice`, `OrderProfit`) VALUES ( :AccountNumber, :AccountServerId, :OrderTicket, :OrderType, :OrderOpenTime, :OrderCloseTime, :OrderLots, :OrderSymbol, :OrderOpenPrice, :OrderStopLoss, :OrderTakeProfit, :OrderClosePrice, :OrderProfit )";
			
			unset( $params );
			
			$firstOrder = reset( $data );
			
			$params = array( ':AccountNumber' => $this->accountNumber, ':AccountServerId' => $this->idServer, ':OrderSymbol' => $symbol );
			
			$params[":OrderTicket"] = $endOrderKey + 1;
			$params[":OrderType"] = 6;
			$params[":OrderOpenTime"] = strtotime( str_replace('.', '-', $firstOrder['openTime']) . ':00' );
			$params[":OrderCloseTime"] = strtotime( str_replace('.', '-', $firstOrder['closeTime']) . ':00' );
			$params[":OrderLots"] = $firstOrder['lots'];
			$params[":OrderOpenPrice"] = $firstOrder['openPrice'];
			$params[":OrderStopLoss"] = $firstOrder['stopLoss'];
			$params[":OrderTakeProfit"] = $firstOrder['takeProfit'];
			$params[":OrderClosePrice"] = $firstOrder['closePrice'];
			$params[":OrderProfit"] = $firstOrder['balance'] - $firstOrder['profit'];
				
			$insertResult = Yii::App()->db->createCommand( $query )->query( $params );
			
			
			
			
			$this->updateReportStats();
			
		}
		
		public function getAvailableHistorySymbols(){
			$symbols = Yii::app()->db->createCommand()
				->select('OrderSymbol')
				->from('mt4_account_history')  //Your Table name
				->where( 'OrderSymbol IS NOT NULL AND AccountNumber=:accNum AND (OrderType=1 OR OrderType=0) AND AccountServerId=:serverId ', array( ':accNum'=>$this->accountNumber, ':serverId'=>$this->idServer ) )
				->order('OrderSymbol DESC')
				->group('OrderSymbol')
				->queryAll();
				
			if( !$symbols ) return false;
			
			$quotesSettings = QuotesSettingsModel::getInstance();
			$replacementStr = $quotesSettings->histAliasReplacement;
			
			eval("\$replacementArr = $replacementStr;");
			
			$symbolsArr = array();
			
			foreach( $symbols as $symbol ){
				
				$symbolId = QuotesSymbolsModel::getIdByHistNameAndAlias( $replacementArr, $symbol['OrderSymbol'] );
				
				if( $symbolId ){
					$symbolsArr[$symbol['OrderSymbol']] = $symbolId;
				}
				
			}
			
			return $symbolsArr;
		}
		public function getAccNameShort(){
			if( $this->accountTitle ){
				return $this->accountTitle;
			}else{
				return $this->accountNumber;
			}
		}
		public function getAccName(){
			if( $this->server->forReports ){
				return $this->accountTitle;
			}else{
				return $this->accountNumber;
			}
		}
		
		static public function getAccountNumber($id)
		{
			$data = self::model()->findByPk($id, array('select'=>'id, accountNumber'));
			if ( $data != null ) {
				return $data->accountNumber;
			}
			return false;
		}
		static function instance( $idContest, $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idContest', 'idUser' ));
		}
		static function instanceInfoDataObject( $idContest, $idUser ) {
			return new ContestMemberModelInfoDataObject( $idContest, $idUser );
		}
		static function instanceAccountDataObject( $idContest, $idUser ) {
			return new ContestMemberModelAccountDataObject( $idContest, $idUser );
		}
		public function updateReportStats() {
			$command = file_get_contents( "protected/data/updateContestMemberReportStats.sql" );
			Yii::App()->db->createCommand( $command )->query(Array(
				':idMember' => $this->id,
			));
		}
		static function updateStats( $idContest ) {

			$exists = Yii::App()->db->createCommand("
				SELECT 		1 
				FROM 		`{{contest_member_stats}}` 
				INNER JOIN  `{{contest_member}}` ON `{{contest_member}}`.`id` = `{{contest_member_stats}}`.`idMember`
				WHERE 		`{{contest_member}}`.`idContest` = :idContest
					 AND	( `{{contest_member_stats}}`.`DT` IS NULL OR DATE_ADD( `{{contest_member_stats}}`.`DT`, INTERVAL 5 MINUTE ) < NOW() )
				LIMIT		1
			")->queryScalar(Array(
				':idContest' => $idContest,
			));
			
			if( $exists ) {
				$command = file_get_contents( "protected/data/updateContestMemberStats.sql" );
				Yii::App()->db->createCommand( $command )->query(Array(
					':idContest' => $idContest,
				));
			}
		
		}
		static function updateMonitoringStats() {
			$command = file_get_contents( "protected/data/updateContestMemberMonitoringStats.sql" );
			Yii::App()->db->createCommand( $command )->query();
		}
		
		public function getOpenDealsCount(){
			$model = MT5AccountDataModel::model()->find( Array(
				'select' => ' ACCOUNT_ORDERS_TOTAL ',
				'condition' => "
					`t`.`ACCOUNT_NUMBER` = :accountNumber and `t`.`AccountServerId` = :accountServerId
				",
				'order'=>'`t`.`timestamp` DESC',
				'params' => Array(
					':accountNumber' => $this->accountNumber,
					':accountServerId' => $this->idServer,
				),
			));
			if( !$model ) return 0;
			if( !$model->ACCOUNT_ORDERS_TOTAL ) return 0;
			return $model->ACCOUNT_ORDERS_TOTAL;
		}
		
			# fields
		static function getModelSingleURL( $id ) {
			return Yii::App()->createURL( 'contestMember/single', Array( 'id' => $id ));
		}
		function getSingleURL() {
			if( $this->idContest ) return self::getModelSingleURL( $this->id );
			return Yii::app()->createUrl('monitoring/single', Array( 'id' => $this->id ));
		}
		function getLabelStatus() {
			$status = $this->status;
			
			$statsStatus = $this->stats->getStatus();
			if( $statsStatus ) $status = $statsStatus;
			
			$classes = Array(
				'participates' => 'label label-success',
				'disqualified' => 'label label-important',
				'quitted' => 'label label-important',
				'completed' => 'label label-yellow',
				'invalid account' => 'label label-warning',
			);
			$class = @$classes[ $status ];
			$title = ucfirst( $status );
			$title = Yii::t( $this->NSi18n, $title );
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		
		public function getImgInformerTitle(){
			if( $this->server->forReports ) return $this->accountTitle;
			if( $this->accountTitle ) return $this->accountTitle . ' (' . $this->accountNumber . ')';
			return $this->accountNumber;
		}
		
			# winer
		function deleteWiner() {
			if( $this->winer ) $this->winer->delete();
		}
		function deleteStats() {
			if( $this->stats ) $this->stats->delete();
		}
		
			# notice
		private function noticeRegister() {
			UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_CREATE_CONTEST_MEMBER, Array(
				'idUser' => $this->idUser,
				'idMember' => $this->id,
				'self' => true,
			));
		}
		
			# master API
		function sendMasterAPICmd( $params ) {
			$url = "http://159.69.219.218:18466/master.api?".http_build_query( $params );
			$out = @file_get_contents( $url );
			@file_put_contents( 'protected/runtime/master.log', "{$url}\r\n", FILE_APPEND );
			@file_put_contents( 'protected/runtime/master.log', "{$out}\r\n\r\n", FILE_APPEND );
		}
		function sendMasterAPIDelCmd() {
			$server = ServerModel::model()->findByPk( $this->idServer );
			$params = Array(
				'cmd' => 'del',
				'aid' => $this->id,
				'login' => $this->accountNumber,
				'pass' => $this->investorPassword,
				'Srvmt4' => $server->url,
			);
			$this->sendMasterAPICmd( $params );
		}
		function sendMasterAPIAddCmd() {
			$server = ServerModel::model()->findByPk( $this->idServer );
			$params = Array(
				'cmd' => 'add_acc',
				'aid' => $this->id,
				'login' => $this->accountNumber,
				'pass' => $this->investorPassword,
				'Srvmt4' => $server->url,
			);
			$this->sendMasterAPICmd( $params );
		}
			
			# events
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				Yii::App()->db->createCommand( " 
					REPLACE INTO 		`{{contest_member_stats}}` ( `idMember` )
					VALUES 				( :id )
				" )->query( Array(
					':id' => $this->id,
				));
				$this->noticeRegister();
				if( $this->status == "participates" ) {
					$this->sendMasterAPIAddCmd();
				}
				else{
					$this->sendMasterAPIDelCmd();
				}
				
				UserReputationModel::event( UserReputationModel::TYPE_EVENT_CREATE_CONTEST_MEMBER, Array( 'idUser' => $this->idUser ));
				UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_CONTEST_MEMBER, Array( 'idUser' => $this->idUser, 'idContest' => $this->idContest ));
			}
			if( $this->status == 'completed' ) {
				//UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_CONTEST_COMPLETED, Array( 'idUser' => $this->idUser, 'idContest' => $this->idContest ));
			}
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteWiner();
			$this->deleteStats();
			
			MT5AccountTradesModel::model()->deleteAll(
				" ACCOUNT_NUMBER = :accountNumber AND AccountServerId = :serverId ", 
				array(':accountNumber' => $this->accountNumber, ':serverId' => $this->idServer ) 
			);
			MT5AccountInfoModel::model()->deleteAll(
				" ACCOUNT_NUMBER = :accountNumber AND ServerUrl = :serverId ", 
				array(':accountNumber' => $this->accountNumber, ':serverId' => $this->server->url ) 
			);
			MT5AccountHistoryOrdersModel::model()->deleteAll(
				" ACCOUNT_NUMBER = :accountNumber AND AccountServerId = :serverId ", 
				array(':accountNumber' => $this->accountNumber, ':serverId' => $this->idServer ) 
			);
			MT5AccountHistoryDealsModel::model()->deleteAll(
				" ACCOUNT_NUMBER = :accountNumber AND AccountServerId = :serverId ", 
				array(':accountNumber' => $this->accountNumber, ':serverId' => $this->idServer ) 
			);
			MT5AccountDataModel::model()->deleteAll(
				" ACCOUNT_NUMBER = :accountNumber AND AccountServerId = :serverId ", 
				array(':accountNumber' => $this->accountNumber, ':serverId' => $this->idServer ) 
			);
			MT4AccountMonitorModel::model()->deleteAll(
				" accountNumber = :accountNumber AND accountServerId = :serverId ", 
				array(':accountNumber' => $this->accountNumber, ':serverId' => $this->idServer ) 
			);
			MT4AccountHistoryModel::model()->deleteAll(
				" AccountNumber = :accountNumber AND AccountServerId = :serverId ", 
				array(':accountNumber' => $this->accountNumber, ':serverId' => $this->idServer ) 
			);
			ContestMemberReportModel::model()->deleteAll(
				" idMember = :idMember ", 
				array(':idMember' => $this->id ) 
			);
			
			UserReputationModel::event( UserReputationModel::TYPE_EVENT_DELETE_CONTEST_MEMBER, Array( 'idUser' => $this->idUser ));
		}

		static function lastRevisionByAN($an)
		{
			$data = self::model()
				->find('accountNumber='.$an);
			if ( count($data) > 0 ) {
				return $data->revision;
			}
			return 0;
		}
	}

	abstract class ContestMemberModelDataObject{
		const PATHData = 'protected/temp/contest/registerMember';
		protected $idContest;
		protected $idUser;
		public $data;
		function __construct( $idContest, $idUser ) {
			$this->idContest = $idContest;
			$this->idUser = $idUser;
			$this->data = new StdClass();
		}
		abstract protected function getPath();
		function save() {
			$path = $this->getPath();
			$dir = dirname( $path );
			CommonLib::cleanDir( $dir, CommonLib::secInDay );
			@file_put_contents( $path, serialize( $this->data ));
		}
		function load() {
			$path = $this->getPath();
			$contents = @file_get_contents( $path );
			if( strlen( $contents )) {
				$this->data = unserialize( $contents );
				return true;
			}
			return false;
		}
		function unlink() {
			$path = $this->getPath();
			@unlink( $path );
		}
	}
	
	final class ContestMemberModelInfoDataObject extends ContestMemberModelDataObject {
		protected function getPath() {
			return self::PATHData."/{$this->idContest}_{$this->idUser}_info.dat";
		}
	}
	
	final class ContestMemberModelAccountDataObject extends ContestMemberModelDataObject {
		protected function getPath() {
			return self::PATHData."/{$this->idContest}_{$this->idUser}_account.dat";
		}
	}

	
?>
