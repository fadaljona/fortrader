<?
	Yii::import( 'models.base.ModelBase' );

	final class AdvertisementZoneToAdvertisementModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idZone, $idAd ) {
			$model = self::modelFromAssoc( __CLASS__, compact( 'idZone', 'idAd' ));
			return $model;
		}
	}

?>