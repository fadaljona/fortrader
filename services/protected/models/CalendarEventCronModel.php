<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CalendarEventCronModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $date ) {
			return self::modelFromAssoc( __CLASS__, compact( 'date' ));
		}
	}

?>