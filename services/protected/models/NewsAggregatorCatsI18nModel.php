<?
	Yii::import( 'models.base.ModelBase' );
	
	final class NewsAggregatorCatsI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'category' => Array( self::HAS_ONE, 'NewsAggregatorCatsModel', array( 'id' => 'idCat' ) ),
			);
		}
		function tableName() {
			return "{{news_aggregator_cats_i18n}}";
		}
		static function instance( $idCat, $idLanguage, $title, $keys, $pageTitle, $metaTitle, $metaKeys, $metaDesc, $shortDesc, $fullDesc ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idCat', 'idLanguage', 'title', 'keys', 'pageTitle', 'metaTitle', 'metaKeys', 'metaDesc', 'shortDesc', 'fullDesc' ));
		}
	}
?>