<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesForecastsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getMood( $symbolId ){
			$date = date( 'Y-m-d', time() );
			$date .= ' 00:00:00';
			$moodModels = self::model()->findAll(Array(
				'condition' => " `t`.`symbolId` = :symbolId AND `t`.`status` = 2 AND `t`.`startDate` = :startDate ",
				'params' => array(':symbolId' => $symbolId, ':startDate' => $date),
			));
			
			if( !$moodModels ) return false;
				
			$absSumm = $bullSumm = $bearSumm = 0;
			foreach( $moodModels as $moodModel ){
				$absSumm ++;
				if( $moodModel->value == 1 ){
					$bullSumm ++;
				}elseif( $moodModel->value == -1 ){
					$bearSumm ++;
				}
			}
			$outArray = array(
				'bull' => 0,
				'bear' => 0
			);
			if( $bullSumm ) $outArray['bull'] = number_format( $bullSumm / $absSumm * 100, 2, '.', ' ');
			if( $bearSumm ) $outArray['bear'] = number_format( $bearSumm / $absSumm * 100, 2, '.', ' ');
			return $outArray;
		}
		static function getMoodForUser( $symbolId ){
			if( !Yii::App()->user->id ) return 0;
			$date = date( 'Y-m-d', time() );
			$date .= ' 00:00:00';
			$moodModel = self::model()->find(Array(
				'condition' => " `t`.`symbolId` = :symbolId AND `t`.`status` = 2 AND `t`.`startDate` = :startDate AND `t`.`userId` = :userId ",
				'params' => array(':symbolId' => $symbolId, ':startDate' => $date, ':userId' => Yii::App()->user->id ),
			));
			if( $moodModel ) return $moodModel->value;
			return 0;
		}
		static function getCountForUser( ){
			if( !Yii::App()->user->id ) return 0;
			$count = Yii::app()->db->createCommand()
				->select('COUNT( id ) as count')
				->from('{{quotes_forecasts}}')
				->where( ' userId = :userId AND status <> 2 ', array( ':userId' => Yii::App()->user->id ) )
				->queryRow();
			return $count['count'];
		}
		public function relations(){
			return array(
				'symbol' => array(self::HAS_ONE,'QuotesSymbolsModel', array('id' => 'symbolId')),
			);
		}
	}

?>