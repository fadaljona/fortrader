<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserKarmaModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function load( $id ){
			$model = self::model()->findByPk( $id );
			if( $model ) return $model;
			$model = new self();
			$model->id = $id;
			return $model;
		}
		static function updateKarma($id){
			$model = self::load( $id );
			$summ = Yii::app()->db->createCommand()
				->select('SUM( status ) as karma')
				->from('{{quotes_forecasts}}')
				->where( ' userId = :userId AND status <> 2 ', array( ':userId' => $id ) )
				->queryRow();
			
			$model->karma = $summ['karma'];
			if( $model->validate() ){
				$model->save();
			}
		}
		function rules() {
			return Array(
				Array( 'id', 'required' ),
				Array( 'id, karma', 'numerical', 'integerOnly' => true ),
			);
		}
	}

?>