<?
	Yii::import( 'models.base.ModelBase' );
	
	final class WPPostModel extends ModelBase {
		const thumbnailRealPath = '/var/www/fortrader/domains/files.fortrader.org/thumbnails';
		const thumbnailCopyPath = 'uploads/wpposts/thumbnails';
		const widthThumbnail = 55;
		const heightThumbnail = 55;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "wp_posts";
		}
		function relations() {
			return Array(
				'termRelationships' => Array( self::HAS_MANY, 'WPTermRelationshipModel', "object_id" ),
				'termTaxonomy' => Array( self::HAS_MANY, 'WPTermTaxonomyModel', Array( "term_taxonomy_id" => "term_taxonomy_id" ), 'through' => 'termRelationships' ),
				'terms' => Array( self::HAS_MANY, 'WPTermModel', Array( "term_id" => "term_id" ), 'through' => 'termTaxonomy' ),
				'categories' => Array( self::HAS_MANY, 'WPTermModel', Array( "term_id" => "term_id" ), 'through' => 'termTaxonomy', 'condition' => " `termTaxonomy`.`taxonomy` = 'category' " ),
				'important' => Array( self::HAS_ONE, 'WPPostmetaModel', "post_id", 'on' => " `important`.`meta_key` = 'important' " ),
				'thumbnail' => Array( self::HAS_ONE, 'WPPostmetaModel', "post_id", 'on' => " `thumbnail`.`meta_key` = '_p75_thumbnail' " ),
				'daylife' => Array( self::HAS_ONE, 'WPPostmetaModel', "post_id", 'on' => " `daylife`.`meta_key` = 'daylife' " ),
				'index' => Array( self::HAS_ONE, 'WPPostmetaModel', "post_id", 'on' => " `index`.`meta_key` = 'index' " ),
				'country' => Array( self::HAS_ONE, 'WPPostmetaModel', "post_id", 'on' => " `country`.`meta_key` = 'country' " ),
				'author' => Array( self::BELONGS_TO, 'UserModel', "post_author" ),
				'decomSubscribers' => Array( self::HAS_ONE, 'WPPostmetaModel', "post_id", 'on' => " `decomSubscribers`.`meta_key` = '_decom_subscribers' " ),
			);
		}
		function getThumbnailPath() {
			if( !$this->thumbnail ) return '';
			
			$pathReal = self::thumbnailRealPath."/{$this->thumbnail->meta_value}";
			$md = substr( md5( $pathReal ), 0, 8 );
			$ex = CommonLib::getExtension( $pathReal );
			$pathCopy = self::thumbnailCopyPath."/{$this->ID}_{$md}.{$ex}";
			
			if( !is_file( $pathCopy )) {
				try {
					ImgResizeLib::resize( $pathReal, $pathCopy, self::widthThumbnail, self::heightThumbnail );
				}
				catch( Exception $e  ) {
				}
			}
			
			return $pathCopy;
		}
		function getThumbnailSrc() {
			$path = $this->getThumbnailPath();
			return $path ? Yii::app()->baseUrl."/{$path}" : '';
		}
		function getThumbnail( $options = Array() ) {
			if( !$this->thumbnail ) return '';
			
			$options[ 'src' ] = $this->getThumbnailSrc();
			return CHtml::tag( 'img', $options );
		}
		function getShortUrl() {
			return Yii::app()->baseUrl."/../?p={$this->ID}";
		}
		function filterContent() {
			$content = $this->post_content;
			
			$content = preg_replace( "#\r?\n#", " ", $content );
			$content = preg_replace( "#\[caption.*\[\/caption\]#Usi", "", $content );
			$content = strip_tags( $content );
			$content = preg_replace( "# {2,}#", " ", $content );
			
			$maxLen = 250;
			$len = mb_strlen( $content );
			if( $len > $maxLen ) {
				for( $i=$maxLen; $i<$len; $i++ ) {
					$char = mb_substr( $content, $i, 1 );
					$char_next = mb_substr( $content, $i+1, 1 );
					
					if( $char == '.' and $char_next == ' ' ) {
						$content = mb_substr( $content, 0, $i+1 );
						break;
					}
				}
			}
			
			return $content;
		}
	}

?>