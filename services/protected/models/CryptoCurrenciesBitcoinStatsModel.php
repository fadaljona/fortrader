<?php
Yii::import('models.base.ModelBase');

final class CryptoCurrenciesBitcoinStatsModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function getLastStats()
    {
        return self::model()->find(array(
            'order' => '`t`.`date` DESC'
        ));
    }
}
