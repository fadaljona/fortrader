<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementCampaignModel extends ModelBase {
		public $countShows;
		public $countClicks;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'advertisements' => Array( self::HAS_MANY, 'AdvertisementModel', 'idCampaign' ),
				'owner' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
			);
		}
		static function updateStatusesByLimit() {
			Yii::App()->db->createCommand("
				UPDATE		`{{advertisement_campaign}}`
				SET			`status` = IF(
								IF(
									`dayLimitShows` IS NULL,
									FALSE,
									(
										SELECT			SUM( `countShows` )
										FROM			`{{advertisement_stats}}`
										INNER JOIN		`{{advertisement}}` ON ( `id` = `idAd` )
										WHERE			`idCampaign` = `{{advertisement_campaign}}`.`id`
											AND			`date` = CURRENT_DATE()
									) >= `dayLimitShows`
								)
								OR IF(
									`dayLimitClicks` IS NULL,
									FALSE,
									(
										SELECT			SUM( `countClicks` )
										FROM			`{{advertisement_stats}}`
										INNER JOIN		`{{advertisement}}` ON ( `id` = `idAd` )
										WHERE			`idCampaign` = `{{advertisement_campaign}}`.`id`
											AND			`date` = CURRENT_DATE()
									) >= `dayLimitClicks`
								)
								OR IF(
									`totalLimitShows` IS NULL,
									FALSE,
									(
										SELECT			SUM( `countShows` )
										FROM			`{{advertisement_stats}}`
										INNER JOIN		`{{advertisement}}` ON ( `id` = `idAd` )
										WHERE			`idCampaign` = `{{advertisement_campaign}}`.`id`
									) >= `totalLimitShows`
								),
								'Limit is settled',
								'Active'
							)
				WHERE		`status` IN ( 'Active', 'Limit is settled' )
			")->query();
		}
		function clearStats() {
			Yii::App()->db->createCommand( "
				DELETE
				FROM			`{{advertisement_stats}}`
				WHERE			EXISTS (
									SELECT		1
									FROM		`{{advertisement}}`
									WHERE		`{{advertisement}}`.`id` = `{{advertisement_stats}}`.`idAd`
												AND `{{advertisement}}`.`idCampaign` = :idCampaign
									LIMIT		1
								)
			" )->query(Array(
				':idCampaign' => $this->id,
			));
			AdvertisementClickModel::clearCampaignClicks( $this->id );
		}
		function checkAccess() {
			if( Yii::App()->user->checkAccess( 'advertisementControl' )) return true;
			if( $this->isNewRecord || $this->idUser == Yii::App()->user->id ) {
				return Yii::App()->user->checkAccess( 'ownAdvertisementControl' );
			}
			return false;
		}
			# fields
		function getStatus() {
			$curDate = date( "Y-m-d" );
			if( $curDate < $this->begin ) {
				return 'Not begun';
			}
			if( $curDate > $this->end ) {
				return 'Ended';
			}
			if( $this->owner->balance <= 0 ) {
				return 'End balance';
			}
			return $this->status;
		}
		function getCTR() {
			return $this->countShows ? $this->countClicks / $this->countShows * 100 : null;
		}
			# advertisements
		function deleteAdvertisements() {
			foreach( $this->advertisements as $model ) {
				$model->delete();
			}
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteAdvertisements();
		}
		protected function afterSave() {
			$result = parent::afterSave();
			if( $this->type == 'Image' ){
				
				$ads = AdvertisementModel::model()->findAll(Array(
					'condition' => " `t`.`idCampaign` = :idCampaign and `t`.`dontShow` =0 ",
					'params' => array( ':idCampaign' => $this->id )
				));
				$bannersCount = count($ads);
				if( $bannersCount ){
					foreach( $ads as $ad ){
						$ad->weight = $this->weight / $bannersCount;
						$ad->save();
					}
				}
			}
			return $result;
		}
	}

?>