<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MessageModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'MessageI18NModel', 'idMessage' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'MessageI18NModel', 'idMessage', 
					'on' => '`currentLanguageI18N`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		static function instance( $key, $ns ) {
			return self::modelFromAssoc( __CLASS__, compact( 'key', 'ns' ));
		}
		function getCurrentLanguageValue() {
			return $this->currentLanguageI18N ? $this->currentLanguageI18N->value : $this->key;
		}
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$value ) {
				if( strlen( $value ) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = MessageI18NModel::model()->findByAttributes( Array( 'idMessage' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = MessageI18NModel::instance( $this->id, $idLanguage, $value );
						$i18n->save();
					}
					elseif( $i18n->value != $value ){
						$i18n->value = $value;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>