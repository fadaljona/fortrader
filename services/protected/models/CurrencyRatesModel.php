<?
	Yii::import( 'models.base.ModelBase' );
	
    final class CurrencyRatesModel extends ModelBase  implements ModelForTextReplaceInterface
    {
		const PATHUploadDir = 'uploads/currencyRates';
		const cacheTime = 5; //5mins
		const currenciesForChangePageCacheKey = 'currenciesForChangePageCacheKey';
		const currenciesForConverterCacheKey = 'currenciesForConverterCacheKey';
		const ecbCurrenciesForConverterCacheKey = 'ecbCurrenciesForConverterCacheKey';
		const popularCurrenciesCacheKey = 'popularCurrenciesCacheKey';
		const firstDataDateCacheKey = 'firstDataDateCacheKey';
		const yearsAvailableCacheKey = 'yearsAvailableCacheKey';
		const monthsAvailableByYearCacheKey = 'monthsAvailableByYearCacheKey';
		const monthsAvailableByYearForSiteMapCacheKey = 'monthsAvailableByYearForSiteMapCacheKey';
		const lastDayAvailableByYearMoCacheKey  = 'lastDayAvailableByYearMoCacheKey';
		const firstDayAvailableByYearMoCacheKey = 'firstDayAvailableByYearMoCacheKey';
		const dataForPeriodCacheKey = 'dataForPeriodCacheKey';
		
		public $firstGetDataRange = 5;//years
		public $dataType = 'cbr';
		public static $dataTypes = array( 'cbr', 'ecb' );
		
		private $settings;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'CurrencyRatesI18NModel', 'idCurrency', 'alias' => 'i18nsCurrencyRates' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'CurrencyRatesI18NModel', 'idCurrency', 
					'alias' => 'cLI18NCurrencyRatesModel',
					'on' => '`cLI18NCurrencyRatesModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getByAlias( Yii::app()->language )->id,
					),
				),
				'tomorrowOneData' => array(
					self::HAS_ONE, 
					'CurrencyRatesHistoryModel', 
					'idCurrency', 
					'order' => '`tomorrowOneData`.`date` DESC', 
					'condition' => " `tomorrowOneData`.`date` = :date  ",
					'params' => array( ':date' => date('Y-m-d', time() + 60*60*24 ) ),
				),
				'history' => array( self::HAS_MANY, 'CurrencyRatesHistoryModel', 'idCurrency'),
				'country' => Array( self::BELONGS_TO, 'CountryModel', 'idCountry' ),
				'viewsTable' => array( self::HAS_ONE, 'CurrencyRatesViewsModel', 'idCurrency'),
					
			);

		}
		function rules() {
			return Array(
				Array( 'code, cbrId', 'length', 'max' => 10 ),
				Array( 'symbol', 'length', 'max' => 50 ),
				Array( 'code, cbrId', 'unique', 'message' => 'Code is busy. Please choose another.' ),
				Array( 'code, cbrId', 'required' ),
				Array( 'noData, popular, topInSelect', 'boolean'),
				Array( 'idCountry', 'numerical', 'integerOnly'=>true, 'min' => -1 ),
				Array( 'currencyOrder, cbrDigitCode', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
			);
		}
		static function getListDp( $type = 'today' ){
			
			$c = new CDbCriteria();
			$c->with[ 'country' ] = Array();
			$c->with[ 'viewsTable' ] = Array();
			$c->with[ 'currentLanguageI18N' ] = Array();
			$c->with[ 'i18ns' ] = Array();
			
			$weekDay = date('w');
			if( $type == 'tomorrow' && $weekDay != 6 && $weekDay != 7 ){
				$c->with[ 'tomorrowOneData' ] = Array();
			}

			$c->condition = " `t`.`noData` = 0 AND `t`.`defaultCurrency` = 0 ";
			$c->order = " `t`.`currencyOrder` ASC, `viewsTable`.`views` DESC, `t`.`code` ASC ";
			
			$DP = new CActiveDataProvider( 'CurrencyRatesModel', Array(
				'criteria' => $c,
				'pagination' => false
			));
			return $DP;
		}
		static function getEcbListDp(){
			
			$c = new CDbCriteria();
			$c->with[ 'country' ] = Array();
			$c->with[ 'viewsTable' ] = Array();
			$c->with[ 'currentLanguageI18N' ] = Array();
			$c->with[ 'i18ns' ] = Array();
			
			$c->condition = " `t`.`ecb` = 1 AND `t`.`defaultCurrencyEcb` = 0 ";
			$c->order = " `t`.`currencyOrder` ASC, `viewsTable`.`views` DESC, `t`.`code` ASC ";
			
			$DP = new CActiveDataProvider( 'CurrencyRatesModel', Array(
				'criteria' => $c,
				'pagination' => false
			));
			return $DP;
		}
		
		
		
		
		/*informers*/
		static function getConverterFromToSettingsByLang($langId, $type = 'from', $idInType = 0){
			$informerSettings = InformersSettingsModel::getModel();
			
			if( $idInType == 0 ){
				$condition = " `t`.`noData` = 0 AND `i18nsCurrencyRates`.`idLanguage` = :lang AND `i18nsCurrencyRates`.`name` IS NOT NULL AND `i18nsCurrencyRates`.`name` <> '' ";
			}elseif( $idInType == 1 ){
				$condition = " `t`.`ecb` = 1 AND `i18nsCurrencyRates`.`idLanguage` = :lang AND `i18nsCurrencyRates`.`name` IS NOT NULL AND `i18nsCurrencyRates`.`name` <> '' ";
			}
			$models = self::model()->findAll(array(
				'with' => array( 'i18ns', 'viewsTable' ),
				'condition' => $condition,
				'order' => " `t`.`currencyOrder` ASC, `viewsTable`.`views` DESC, `t`.`code` ASC ",
				'params' => array( ':lang' => $langId )
			));
			if( $type == 'from' ){
				$setted = $informerSettings->convertFrom;
			}else{
				$setted = $informerSettings->convertTo;
			}
			if( $idInType == 1 ){
				$setted = 21;
			}
			$outArr = array( 
				'type' => 'select',
			);
			foreach( $models as $model ){
				$settedVal = 0;
				if( $model->id == $setted ) $settedVal = 1;
				$outArr['options'][] = array(
					'value' => $model->id,
					'setted' => $settedVal,
					'label' => $model->getNameByLang($langId)
				);
			}
			return $outArr;
		}
		static function getItemsForInformer(){
			$models = self::model()->findAll(array(
				'with' => array( 'currentLanguageI18N', 'viewsTable' ),
				'condition' => " `t`.`noData` = 0 AND `cLI18NCurrencyRatesModel`.`name` IS NOT NULL AND `cLI18NCurrencyRatesModel`.`name` <> '' ",
				'order' => " `t`.`currencyOrder` ASC, `viewsTable`.`views` DESC, `t`.`code` ASC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->id] = array(
					'toInformer' => $model->toInformer,
					'name' => $model->name,
					'country' => $model->country->alias
				);
			}
			return $outArr;
		}
		static function getItemsForEcbInformer(){
			$models = self::model()->findAll(array(
				'with' => array( 'currentLanguageI18N', 'viewsTable' ),
				'condition' => " `t`.`ecb` = 1 AND `cLI18NCurrencyRatesModel`.`name` IS NOT NULL AND `cLI18NCurrencyRatesModel`.`name` <> '' ",
				'order' => " `t`.`currencyOrder` ASC, `viewsTable`.`views` DESC, `t`.`code` ASC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->id] = array(
					'toInformer' => $model->toInformer,
					'name' => $model->name,
					'country' => $model->country->alias
				);
			}
			return $outArr;
		}
		static function getItemsForInformerByLang($langId, $idInType = 0){
		
			if( $idInType == 0 ){
				$condition = " `t`.`noData` = 0 AND `t`.`defaultCurrency` = 0 AND `i18nsCurrencyRates`.`idLanguage` = :lang AND `i18nsCurrencyRates`.`name` IS NOT NULL AND `i18nsCurrencyRates`.`name` <> '' ";
			}elseif( $idInType == 1 ){
				$condition = " `t`.`ecb` = 1 AND `t`.`defaultCurrencyEcb` = 0 AND `i18nsCurrencyRates`.`idLanguage` = :lang AND `i18nsCurrencyRates`.`name` IS NOT NULL AND `i18nsCurrencyRates`.`name` <> '' ";
			}
			$models = self::model()->findAll(array(
				'with' => array( 'i18ns', 'viewsTable' ),
				'condition' => $condition,
				'order' => " `t`.`currencyOrder` ASC, `viewsTable`.`views` DESC, `t`.`code` ASC ",
				'params' => array( ':lang' => $langId )
			));
			$outArr = array( 
				'type' => 'multipleCheckbox',
			);
			foreach( $models as $model ){
				$outArr['options'][] = array(
					'value' => $model->id,
					'setted' => $model->toInformer,
					'label' => $model->getNameByLang($langId)
				);
			}
			return $outArr;
		}
		static function getCurrenciesForInformer(){
			$models = self::model()->findAll(array(
				'with' => array( 'currentLanguageI18N' ),
				'condition' => " `t`.`noData` = 0 AND `t`.`defaultCurrency` = 0 AND `cLI18NCurrencyRatesModel`.`name` IS NOT NULL AND `cLI18NCurrencyRatesModel`.`name` <> '' ",
				'order' => " `t`.`currencyOrder` ASC, `t`.`code` ASC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->id] = $model->name;
			}
			$default = CurrencyRatesModel::getDefaultCurrency();
			$outArr[$default->id] = $default->name;
			return $outArr;
		}
		static function getItemsForInformerByIds( $ids ){
			
			$criteria = new CDbCriteria();
			$criteria->with = array( 'currentLanguageI18N', 'viewsTable' );
			$criteria->condition = " `t`.`noData` = 0 AND `cLI18NCurrencyRatesModel`.`name` IS NOT NULL AND `cLI18NCurrencyRatesModel`.`name` <> '' ";
			$criteria->addInCondition('id', explode(',', $ids ));
			$criteria->order = " `t`.`currencyOrder` ASC, `viewsTable`.`views` DESC, `t`.`code` ASC ";
			
			return self::model()->findAll( $criteria );
		}
		static function getItemsForEcbInformerByIds( $ids ){
			
			$criteria = new CDbCriteria();
			$criteria->with = array( 'currentLanguageI18N', 'viewsTable' );
			$criteria->condition = " `t`.`ecb` = 1 AND `cLI18NCurrencyRatesModel`.`name` IS NOT NULL AND `cLI18NCurrencyRatesModel`.`name` <> '' ";
			$criteria->addInCondition('id', explode(',', $ids ));
			$criteria->order = " `t`.`currencyOrder` ASC, `viewsTable`.`views` DESC, `t`.`code` ASC ";
			
			$models = self::model()->findAll( $criteria );
			foreach( $models as &$model ){
				$model->dataType = 'ecb';
			}
			return $models;
		}
		
		public function getInformerItemName(){
			$codes = isset( $_GET['codes'] ) ? $_GET['codes'] : 0;
			$codes = intval( $codes );
			
			if( $this->dataType == 'cbr' ){
				if( $this->informerName ) return $this->informerName;
				
				if( $codes ) return $this->code;
				return $this->name;
			}elseif( $this->dataType == 'ecb' ){
				if( isset($_GET) && isset( $_GET['columns'] ) ){
					if( $_GET['columns'] == 'direct' || $_GET['columns'] == 'reverse' )
						$column = $_GET['columns'];
					else
						$column = 'direct';
					
					$toModel = self::getToCourseInformerModel();
					
					if( $codes ){
						if( $column == 'direct' ){
							return $toModel->code . ' / ' . $this->code;
						}else{
							return $this->code . ' / ' . $toModel->code;
						}
					}else{
						if( $column == 'direct' ){
							return $toModel->name . ' / ' . $this->name;
						}else{
							return $this->name . ' / ' . $toModel->name;
						}
					}
					
					
				}
				return $this->code;
			}
			
		}
		public function getInformerItemNameByLang($langId){
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == $langId ) {
					if( strlen( $i18n->informerName )) return $i18n->informerName;
					if( strlen( $i18n->name )) return $i18n->name;
					return $this->code;
				}
			}
		}
		public function getNameByLang($langId){
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == $langId ) {
					if( strlen( $i18n->name )) return $i18n->name;
					return $this->code;
				}
			}
		}
		public function getInformerShortItemName(){
			return $this->code;
		}
		public function getInformerDiff( $catId = false ){
			if( !$catId ) $catId = $_GET['cat'];
			if( !isset( $catId ) || !$catId ) return 0;
			$cat = InformersCategoryModel::model()->findByPk( $catId );
			if( !$cat || $cat->type != 'currencies' ) return 0;
			
			if( $cat->idInType == 0 ){
				if( !$this->todayCourse ) return 0;
				if( !$this->prevDayCourse ) return 0;
				return $this->todayCourse - $this->prevDayCourse;
			}elseif( $cat->idInType == 1 ){
				
				if( !$this->todayEcbData ) return 0;
				if( !$this->prevEcbDayData ) return 0;
				
				return $floatValue = 1 / floatval($this->todayEcbData->value) - 1 / floatval($this->prevEcbDayData->value);
				
			}
			
		}
		static function getToCourseInformerModel( $toCurId = false ){
			if( !$toCurId ) $toCurId = $_GET['toCur'];
			if( !isset( $toCurId ) || !$toCurId ) return self::getDefaultCbrCurrency();
			$toModel = CurrencyRatesModel::model()->findByPk( $toCurId );
			if( !$toModel ) return self::getDefaultCbrCurrency();
			return $toModel;
		}
		public function getReverse( $toCurId = false ){
			if( !$this->todayEcbData ) return Yii::t( '*', 'n/a' );
			$toModel = self::getToCourseInformerModel( $toCurId );
			if( !$toModel->todayEcbData ) return Yii::t( '*', 'n/a' );
			
			$floatValue = floatval( $toModel->todayEcbData->value ) / floatval( $this->todayEcbData->value ) ;
			
			return number_format($floatValue, CommonLib::getDecimalPlaces( $floatValue, 4 ), '.', '');
		}
		public function getDirect( $toCurId = false ){
			if( !$this->todayEcbData ) return Yii::t( '*', 'n/a' );
			$toModel = self::getToCourseInformerModel( $toCurId );
			if( !$toModel->todayEcbData ) return Yii::t( '*', 'n/a' );
			
			$floatValue =  floatval( $this->todayEcbData->value ) / floatval( $toModel->todayEcbData->value ) ;
			
			return number_format($floatValue, CommonLib::getDecimalPlaces( $floatValue, 4 ), '.', '');
		}
		public function getTodayCourse(){
			if( !$this->todayData ) return Yii::t( '*', 'n/a' );
			$toModel = self::getToCourseInformerModel();
			if( !$toModel->todayData ) return Yii::t( '*', 'n/a' );
			
			$floatValue = ( floatval($this->todayData->value)  / $this->todayData->nominal ) / (  floatval( $toModel->todayData->value ) / $toModel->todayData->nominal  );
			return number_format($floatValue, CommonLib::getDecimalPlaces( $floatValue, 4 ), '.', '');
		}
		public function getPrevDayCourse(){
			if( !$this->prevDayData ) return Yii::t( '*', 'n/a' );
			$toModel = self::getToCourseInformerModel();
			if( !$toModel->prevDayData ) return Yii::t( '*', 'n/a' );
			
			$floatValue = ( floatval($this->prevDayData->value)  / $this->prevDayData->nominal ) / (  floatval( $toModel->prevDayData->value ) / $toModel->prevDayData->nominal  );
			return number_format($floatValue, CommonLib::getDecimalPlaces( $floatValue, 4 ), '.', '');
		}
		public function getTomorrowCourse(){
			if( !$this->tomorrowData ) return '<span class="tooltipWrapper">--<span class="tooltip">'.Yii::t('*', 'Waiting for data').'</span></span>';
			$toModel = self::getToCourseInformerModel();
			if( !$toModel->tomorrowData ) return '<span class="tooltipWrapper">--<span class="tooltip">'.Yii::t('*', 'Waiting for data').'</span></span>';
			
			$floatValue = ( floatval($this->tomorrowData->value)  / $this->tomorrowData->nominal ) / (  floatval( $toModel->tomorrowData->value ) / $toModel->tomorrowData->nominal  );
			return number_format($floatValue, CommonLib::getDecimalPlaces( $floatValue, 4 ), '.', '');
		}
		public function getFlagSrc(){
			if( !$this->country ) return false;
			return $this->country->getSrcFlag('shiny', 16);
		} 
		public function getFlagSrcBig(){
			if( !$this->country ) return false;
			return $this->country->getSrcFlag('shiny', 48);
		} 
		public function getFaSymbol(){
			if( !$this->symbol ) return ' ';
			return CHtml::tag('span', array('class' => 'symbolCode'), $this->symbol);
		}
		static function getLinkForInformerHeader(){
			return Yii::app()->createAbsoluteUrl('currencyRates/index');
		}
		public function getInformerTooltip(){
			return $this->name;
		}
		
		
		
		static function getDefaultCbrCurrency(){
			return CurrencyRatesModel::model()->find(array( 'condition' => ' `t`.`defaultCurrency` = 1 ' ));
		}
		static function getDefaultEcbCurrency(){
			return CurrencyRatesModel::model()->find(array( 'condition' => ' `t`.`defaultCurrencyEcb` = 1 ' ));
		}
		static function getArchiveUrl( $array = array() ){
			if( isset( $array['type'] ) && $array['type'] == 'ecb' ) return Yii::app()->createUrl( 'currencyRates/ecbArchive', $array );
			unset( $array['type'] );
			return Yii::app()->createUrl( 'currencyRates/archive', $array );
		}
		public function getMinMaxDataModelForPeriod( $year, $mo, $type ){
			if( !$year ) return false;
			
			if( !$mo ){
				$fromDate = $year . '-01-01';
				$toDate = $year . '-12-31';
			}else{
				$strMo = $mo < 10 ? '0' . $mo : $mo;
				$fromDate = $year . "-$strMo-01";
				$toDate = $year . "-$strMo-" . cal_days_in_month(CAL_GREGORIAN, $mo, $year);
			}
			
			if( $this->dataType == 'cbr' ){
				if( $type == 'max' ){ $sqlFunc = 'MAX'; }else{ $sqlFunc = 'MIN'; }
				
				$sql = "
					SELECT 
						`h2`.`value`, 
						`h2`.`date`, 
						`h2`.`nominal` 
					FROM `{{currency_rates_history}}` `h2`
					INNER JOIN(
						SELECT 
							{$sqlFunc}(`value`) `minVal`, 
							`idCurrency` 
						FROM `ft_currency_rates_history`  
						WHERE `date` <= :toDate AND `date` >= :fromDate AND `idCurrency` = :id
					) `h1`
					ON `h2`.`idCurrency` = `h1`.`idCurrency` AND `h1`.`minVal` = `h2`.`value`
					WHERE `h2`.`date` <= :toDate AND `h2`.`date` >= :fromDate
				";
			}elseif( $this->dataType == 'ecb' ){
				if( $type == 'max' ){ $sqlFunc = 'MIN'; }else{ $sqlFunc = 'MAX'; }
				
				$sql = "
					SELECT 
						`h2`.`value`, 
						`h2`.`date`
					FROM `{{currency_rates_history_ecb}}` `h2`
					INNER JOIN(
						SELECT 
							{$sqlFunc}(`value`) `minVal`, 
							`idCurrency` 
						FROM `{{currency_rates_history_ecb}}`  
						WHERE `date` <= :toDate AND `date` >= :fromDate AND `idCurrency` = :id
					) `h1`
					ON `h2`.`idCurrency` = `h1`.`idCurrency` AND `h1`.`minVal` = `h2`.`value`
					WHERE `h2`.`date` <= :toDate AND `h2`.`date` >= :fromDate
				";
			}
			
			
			$params = array( ':fromDate' => $fromDate, ':toDate' => $toDate, ':id' => $this->id );
			return Yii::App()->db->createCommand( $sql )->queryRow(true, $params );
		}
		public function getDayData( $year, $mo, $day ){
			$strMo = $mo < 10 ? '0' . $mo : $mo;
			$strDay = $day < 10 ? '0' . $day : $day;
			$dayDate = $year . "-$strMo-$strDay";
			if( $this->dataType == 'cbr' ){
				return CurrencyRatesHistoryModel::model()->find(array(
					'order' => 'date DESC', 
					'condition' => " date <= :date AND idCurrency = :id  ",
					'params' => array( ':date' => $dayDate, ':id' => $this->id )
				));
			}elseif( $this->dataType == 'ecb' ){
				return CurrencyRatesHistoryEcbModel::model()->find(array(
				'order' => 'date DESC', 
					'condition' => " date <= :date AND idCurrency = :id  ",
					'params' => array( ':date' => $dayDate, ':id' => $this->id )
				));
			}
			
		}
		
		
		static function getArchiveListDp( $year, $mo, $day, $dataType = 'cbr' ){
			if( $dataType == 'cbr' ) return self::getArchiveCbrListDp( $year, $mo, $day );
			if( $dataType == 'ecb' ) return self::getArchiveEcbListDp( $year, $mo, $day );
		}
		static function getArchiveEcbListDp( $year, $mo, $day ){
			if( !$year ) return false;
			
			if( $year && $mo && $day ){
				$strMo = $mo < 10 ? '0' . $mo : $mo;
				$strDay = $day < 10 ? '0' . $day : $day;
				$oneDayDate = $year . '-' . $strMo . '-' . $strDay;
				
				$params = array( ':idLanguage' => LanguageModel::getCurrentLanguageID(), ':date' => $oneDayDate );
				
				$dataSQL = "
					SELECT 
						`rates`.`id`,
						`rates`.`code`, 
						`country`.`alias` as `cAlias`, 
						`i18n`.`name` as `currencyName`, 
						`histData`.`value` as `dataValue`,
						`histData`.`date` as `dataDate`
					FROM `{{currency_rates}}` `rates`
					LEFT JOIN `{{currency_rates_views}}` `views` ON `views`.`idCurrency` = `rates`.`id` 
					LEFT JOIN `{{currency_rates_i18n}}` `i18n` ON `i18n`.`idCurrency` = `rates`.`id` AND `i18n`.`idLanguage` = :idLanguage
					LEFT JOIN `{{country}}` `country` ON `country`.`id` = `rates`.`idCountry`
					LEFT JOIN (
						SELECT `h1`.`idCurrency`, `h1`.`date`, `h1`.`value`, `h1`.`createdTime`
						FROM `{{currency_rates_history_ecb}}`  `h1`
						INNER JOIN (
							SELECT `idCurrency`, MAX(`date`) as `mDate` 
							FROM `{{currency_rates_history_ecb}}` 
							WHERE `date` <= :date 
							GROUP BY `idCurrency`
						) `h2`
						ON `h1`.`idCurrency` = `h2`.`idCurrency` AND `h1`.`date` = `h2`.`mDate`
					) `histData` ON `histData`.`idCurrency` = `rates`.`id` 
					WHERE `rates`.`ecb` = 1 AND `rates`.`defaultCurrencyEcb` = 0 AND `histData`.`value` <> '' AND `histData`.`value` IS NOT NULL
					ORDER BY `rates`.`currencyOrder` ASC, `views`.`views` DESC, `rates`.`code` ASC
				";
			}else{
				if( !$mo ){
					$fromDate = $year . '-01-01';
					$toDate = $year . '-12-31';
				}elseif( $mo && !$day ){
					$strMo = $mo < 10 ? '0' . $mo : $mo;
					$fromDate = $year . "-$strMo-01";
					$toDate = $year . "-$strMo-" . cal_days_in_month(CAL_GREGORIAN, $mo, $year);
				}
				
				$params = array( ':idLanguage' => LanguageModel::getCurrentLanguageID(), ':fromDate' => $fromDate, ':toDate' => $toDate );
				
				$dataSQL = "
					SELECT 
						`rates`.`id`,
						`rates`.`code`, 
						`country`.`alias` as `cAlias`, 
						`histData`.`maxValDate`, 
						`histData`.`minValDate`, 
						`histData`.`maxVal`, 
						`histData`.`minVal`, 
						`i18n`.`name` as `currencyName`
					FROM `{{currency_rates}}` `rates`
					LEFT JOIN `{{currency_rates_views}}` `views` ON `views`.`idCurrency` = `rates`.`id` 
					LEFT JOIN `{{currency_rates_i18n}}` `i18n` ON `i18n`.`idCurrency` = `rates`.`id` AND `i18n`.`idLanguage` = :idLanguage
					LEFT JOIN `{{country}}` `country` ON `country`.`id` = `rates`.`idCountry`
					LEFT JOIN (
						SELECT 
							`h1`.`idCurrency`, 
							`h1`.`date` as maxValDate, 
							`h1`.`value` as `maxVal`, 
							`h3`.`date` as minValDate, 
							`h3`.`value` as `minVal`
						FROM `{{currency_rates_history_ecb}}`  `h1`
						INNER JOIN (
							SELECT `idCurrency`, MIN(`value`) as `maxVal`, MAX(`value`) as `minVal`
							FROM `{{currency_rates_history_ecb}}` 
							WHERE `date` <= :toDate AND `date` >= :fromDate 
							GROUP BY `idCurrency`
						) `h2`
						ON `h1`.`idCurrency` = `h2`.`idCurrency` AND `h1`.`value` = `h2`.`maxVal`
						LEFT JOIN `{{currency_rates_history_ecb}}` `h3` ON `h3`.`idCurrency` = `h2`.`idCurrency` AND `h3`.`value` = `h2`.`minVal`
						WHERE `h1`.`date` <= :toDate AND `h1`.`date` >= :fromDate AND `h3`.`date` <= :toDate AND `h3`.`date` >= :fromDate 
						GROUP BY `h1`.`idCurrency` 
					) `histData` ON `histData`.`idCurrency` = `rates`.`id` 
					WHERE `rates`.`ecb` = 1 AND `histData`.`maxValDate` IS NOT NULL AND `rates`.`defaultCurrencyEcb` = 0 
					ORDER BY `rates`.`currencyOrder` ASC, `views`.`views` DESC, `rates`.`code` ASC
				";

				
			}

			$countSql = " SELECT COUNT(`id`) FROM `{{currency_rates}}` `rates` WHERE `rates`.`noData` = 0 ";
			
			
			$count=Yii::app()->db->createCommand( $countSql  )->queryScalar();
			
			$DP = new CSqlDataProvider( $dataSQL  , array(
				'totalItemCount'=>$count,
				'pagination' => false,
				'params' => $params
			));

			return $DP;
		}
		static function getArchiveCbrListDp( $year, $mo, $day ){
			if( !$year ) return false;
			
			if( $year && $mo && $day ){
				$strMo = $mo < 10 ? '0' . $mo : $mo;
				$strDay = $day < 10 ? '0' . $day : $day;
				$oneDayDate = $year . '-' . $strMo . '-' . $strDay;
				
				$params = array( ':idLanguage' => LanguageModel::getCurrentLanguageID(), ':date' => $oneDayDate );
				
				$dataSQL = "
					SELECT 
						`rates`.`id`,
						`rates`.`code`, 
						`country`.`alias` as `cAlias`, 
						`histData`.`nominal`, 
						`i18n`.`name` as `currencyName`, 
						`histData`.`value` as `dataValue`,
						`histData`.`date` as `dataDate`,
						`histData`.`diffValue` as `dataDiffValue`
					FROM `{{currency_rates}}` `rates`
					LEFT JOIN `{{currency_rates_views}}` `views` ON `views`.`idCurrency` = `rates`.`id` 
					LEFT JOIN `{{currency_rates_i18n}}` `i18n` ON `i18n`.`idCurrency` = `rates`.`id` AND `i18n`.`idLanguage` = :idLanguage
					LEFT JOIN `{{country}}` `country` ON `country`.`id` = `rates`.`idCountry`
					LEFT JOIN (
						SELECT `h1`.`idCurrency`, `h1`.`date`, `h1`.`value`, `h1`.`nominal`, `h1`.`diffValue`, `h1`.`createdTime`
						FROM `ft_currency_rates_history`  `h1`
						INNER JOIN (
							SELECT `idCurrency`, MAX(`date`) as `mDate` 
							FROM `{{currency_rates_history}}` 
							WHERE `date` <= :date 
							GROUP BY `idCurrency`
						) `h2`
						ON `h1`.`idCurrency` = `h2`.`idCurrency` AND `h1`.`date` = `h2`.`mDate`
					) `histData` ON `histData`.`idCurrency` = `rates`.`id` 
					WHERE `rates`.`noData` = 0 AND `rates`.`defaultCurrency` = 0 
					ORDER BY `rates`.`currencyOrder` ASC, `views`.`views` DESC, `rates`.`code` ASC
				";
			}else{
				if( !$mo ){
					$fromDate = $year . '-01-01';
					$toDate = $year . '-12-31';
				}elseif( $mo && !$day ){
					$strMo = $mo < 10 ? '0' . $mo : $mo;
					$fromDate = $year . "-$strMo-01";
					$toDate = $year . "-$strMo-" . cal_days_in_month(CAL_GREGORIAN, $mo, $year);
				}
				
				$params = array( ':idLanguage' => LanguageModel::getCurrentLanguageID(), ':fromDate' => $fromDate, ':toDate' => $toDate );
				
				$dataSQL = "
					SELECT 
						`rates`.`id`,
						`rates`.`code`, 
						`country`.`alias` as `cAlias`, 
						`histData`.`maxValDate`, 
						`histData`.`minValDate`, 
						`histData`.`maxVal`, 
						`histData`.`minVal`, 
						`histData`.`maxNominal`, 
						`histData`.`minNominal`, 
						`i18n`.`name` as `currencyName`
					FROM `{{currency_rates}}` `rates`
					LEFT JOIN `{{currency_rates_views}}` `views` ON `views`.`idCurrency` = `rates`.`id` 
					LEFT JOIN `{{currency_rates_i18n}}` `i18n` ON `i18n`.`idCurrency` = `rates`.`id` AND `i18n`.`idLanguage` = :idLanguage
					LEFT JOIN `{{country}}` `country` ON `country`.`id` = `rates`.`idCountry`
					LEFT JOIN (
						SELECT 
							`h1`.`idCurrency`, 
							`h1`.`date` as maxValDate, 
							`h1`.`value` as `maxVal`, 
							`h1`.`nominal` as `minNominal`, 
							`h3`.`date` as minValDate, 
							`h3`.`value` as `minVal`, 
							`h3`.`nominal` as `maxNominal`
						FROM `ft_currency_rates_history`  `h1`
						INNER JOIN (
							SELECT `idCurrency`, MAX(`value` / `nominal`) as `maxVal`, MIN(`value` / `nominal`) as `minVal`
							FROM `{{currency_rates_history}}` 
							WHERE `date` <= :toDate AND `date` >= :fromDate 
							GROUP BY `idCurrency`
						) `h2`
						ON `h1`.`idCurrency` = `h2`.`idCurrency` AND `h1`.`value` / `h1`.`nominal` = `h2`.`maxVal`
						LEFT JOIN `{{currency_rates_history}}` `h3` ON `h3`.`idCurrency` = `h2`.`idCurrency` AND `h3`.`value` / `h3`.`nominal` = `h2`.`minVal`
						WHERE `h1`.`date` <= :toDate AND `h1`.`date` >= :fromDate AND `h3`.`date` <= :toDate AND `h3`.`date` >= :fromDate 
						GROUP BY `h1`.`idCurrency` 
					) `histData` ON `histData`.`idCurrency` = `rates`.`id` 
					WHERE `rates`.`noData` = 0 AND `histData`.`maxValDate` IS NOT NULL AND `rates`.`defaultCurrency` = 0 
					ORDER BY `rates`.`currencyOrder` ASC, `views`.`views` DESC, `rates`.`code` ASC
				";

				
			}

			$countSql = " SELECT COUNT(`id`) FROM `{{currency_rates}}` `rates` WHERE `rates`.`noData` = 0 ";
			
			
			$count=Yii::app()->db->createCommand( $countSql  )->queryScalar();
			
			$DP = new CSqlDataProvider( $dataSQL  , array(
				'totalItemCount'=>$count,
				'pagination' => false,
				'params' => $params
			));

			return $DP;
		}
		
		static function getAvailibleDaysFromDb( $year, $mo ){
			$moStr = $mo < 10 ? '0'.$mo : $mo;
			$firstDay = $year . '-' . $moStr . '-01';
			
			$hasPrevData = Yii::App()->db->createCommand("SELECT `idCurrency` FROM `{{currency_rates_history}}` WHERE `date` < :firstDay LIMIT 1;")->queryScalar( array( ':firstDay' => $firstDay ) );

			if( $hasPrevData ) return 1;
			
			$firstData = Yii::App()->db->createCommand("SELECT `date` FROM `{{currency_rates_history}}` ORDER BY `date` ASC LIMIT 1;")->queryScalar();
			
			return date('j', strtotime($firstData));
			
		}
		static function getAvailibleMosFromDb( $year ){
			$mos =  Yii::App()->db->createCommand(" 
				SELECT MONTH(`date`) AS `mo` FROM `{{currency_rates_history}}` WHERE YEAR(`date`) = :year GROUP BY `mo`;
			")->queryAll(true, array( ':year' => $year ) );
			$outArr = array();
			foreach( $mos as $mo ){
				$outArr[] = $mo['mo'];
			}
			sort($outArr);
			return $outArr;
		}
		static function hasDataByYearMo( $year, $mo = false, $dataType = 'cbr' ){
			
			$tableName = self::getHistTableNameByDataType( $dataType );
			
			if( $mo ){
				$sql = "SELECT `idCurrency` FROM `{{" . $tableName . "}}` WHERE YEAR(`date`) = :year and MONTH(`date`) = :mo limit 1;";
				$params = array( ':year' => $year, ':mo' => $mo );
			}else{
				$sql = "SELECT `idCurrency` FROM `{{" . $tableName . "}}` WHERE YEAR(`date`) = :year limit 1;";
				$params = array( ':year' => $year );
			}
			
			return Yii::App()->db->createCommand( $sql )->queryScalar( $params );
			
		}
		public function hasDataByYearMoDay( $year, $mo = false, $day = false, $dataType = 'cbr' ){
			
			$tableName = self::getHistTableNameByDataType( $dataType );
			
			if( !$mo && !$day ){
				$sql = "SELECT `idCurrency` FROM `{{".$tableName."}}` WHERE YEAR(`date`) = :year and `idCurrency` = :id limit 1;";
				$params = array( ':year' => $year, ':id' => $this->id );
			}elseif( $mo && !$day ){
				$sql = "SELECT `idCurrency` FROM `{{".$tableName."}}` WHERE YEAR(`date`) = :year and MONTH(`date`) = :mo and `idCurrency` = :id limit 1;";
				$params = array( ':year' => $year, ':mo' => $mo, ':id' => $this->id );
			}else{
				$sql = "SELECT `idCurrency` FROM `{{".$tableName."}}` WHERE `idCurrency` = :id limit 1;";
				$params = array( ':id' => $this->id );
			}
			
			return Yii::App()->db->createCommand( $sql )->queryScalar( $params );
			
		}
		static function getHistTableNameByDataType( $dataType = 'cbr' ){
			if( $dataType == 'cbr' ){
				return 'currency_rates_history';
			}elseif( $dataType == 'ecb' ){
				return 'currency_rates_history_ecb';
			}
		}
		static function getHistModelByDataType( $dataType = 'cbr' ){
			if( $dataType == 'cbr' ){
				return 'CurrencyRatesHistoryModel';
			}elseif( $dataType == 'ecb' ){
				return 'CurrencyRatesHistoryEcbModel';
			}
		}
		static function getDataForRadius( $radius, $idCurrency, $leftDate, $rightDate, $dataType ){
			$cachKey = self::dataForPeriodCacheKey . '.radius.' . $radius . '.currency.' . $idCurrency . 'leftDate' . $leftDate . 'rightDate' . $rightDate . '.dataType.' . $dataType;
			
			if( Yii::app()->cache->get( $cachKey ) ) return Yii::app()->cache->get( $cachKey );
			
			$outData = '';
			
			if( $dataType == 'cbr' ){
				$models = CurrencyRatesHistoryModel::model()->findAll(array(
					'condition' => " `t`.`idCurrency` = :id AND `t`.`date` >= :fromDate AND `t`.`date` <= :toDate ",
					'order' => " `t`.`date` DESC ",
					'params' => array( ':id' => $idCurrency, ':fromDate' => date('Y-m-d', strtotime($leftDate) - $radius * 60*60*24 ), ':toDate' => date('Y-m-d', strtotime($rightDate) + $radius * 60*60*24 ) ),
				));
				$lastNominal = 1;
				if( $models ){
					$lastNominal = $models[0]->nominal;
				}
				foreach( $models as $model ){
					$value = ($model->value / $model->nominal) * $lastNominal;
					$outData = "{$model->date};{$value}\n" . $outData;
				}
			}elseif( $dataType == 'ecb' ){
				$models = CurrencyRatesHistoryEcbModel::model()->findAll(array(
					'condition' => " `t`.`idCurrency` = :id AND `t`.`date` >= :fromDate AND `t`.`date` <= :toDate ",
					'order' => " `t`.`date` DESC ",
					'params' => array( ':id' => $idCurrency, ':fromDate' => date('Y-m-d', strtotime($leftDate) - $radius * 60*60*24 ), ':toDate' => date('Y-m-d', strtotime($rightDate) + $radius * 60*60*24 ) ),
				));
				foreach( $models as $model ){
					$value = $model->value;
					$outData = "{$model->date};{$value}\n" . $outData;
				}
			}
			
			$outData = trim($outData) ;
			Yii::app()->cache->set( $cachKey, $outData, self::cacheTime * 60 );
			return $outData;
		}
		static function checkDataType( $dataType ){
			if( !in_array( $dataType, self::$dataTypes ) ) return false;
			return true;
		}
		public function checkTypeAndSettings(){
			if( $this->dataType == 'cbr' && ($this->noData != 0 || $this->defaultCurrency == 1 ) ) return false;
			if( $this->dataType == 'ecb' && ($this->ecb != 1 || $this->defaultCurrencyEcb == 1 ) ) return false;
			return true;
		}
		static function getYearsAvailable( $dataType = 'cbr' ){
			$cacheKey = self::yearsAvailableCacheKey . '.type.' . $dataType;
			if( $data = Yii::app()->cache->get( $cacheKey ) ) return $data;
			
			$tableName = self::getHistTableNameByDataType( $dataType );
			
			$years = Yii::App()->db->createCommand(" 
				SELECT YEAR(`date`) `year` FROM `{{" . $tableName . "}}` group by `year`;
			")->queryAll();
			Yii::app()->cache->set( $cacheKey, $years, 60*60*24 );
			
			return $years;
		}
		static function getFirstDataDate( $dataType ){
			$tableName = self::getHistTableNameByDataType( $dataType );
			if( $dataType == 'cbr' ){
				$cond = '`noData` = 0';
			}elseif( $dataType == 'ecb' ){
				$cond = '`ecb` = 1';
			}
			
			$cacheKey = self::firstDataDateCacheKey . '.type.' . $dataType;
			
			if( $data = Yii::app()->cache->get( $cacheKey ) ) return $data;
			$sql = "
				SELECT `history`.`date` FROM `{{" . $tableName . "}}` `history`
				LEFT JOIN `{{currency_rates}}` `rates` ON `rates`.`id` = `history`.`idCurrency` AND `rates`." .$cond. "
				ORDER BY `history`.`date` ASC
				LIMIT 1
			";
			$date = Yii::app()->db->createCommand( $sql )->queryScalar();
			$dateArr = array(
				'date' => $date,
				'time' => strtotime($date),
				'year' => date( 'Y', strtotime($date) ),
				'mo' => date( 'n', strtotime($date) ),
				'day' => date( 'j', strtotime($date) ),
			);
			Yii::app()->cache->set( $cacheKey, $dateArr, 60*60*24 );
			return $dateArr;
		}
		static function getMonthsAvailableByYear( $year, $dataType ){
			$cacheKey = self::monthsAvailableByYearCacheKey . $year . '.type.' . $dataType . '.lang.' . Yii::app()->language;
			if( $data = Yii::app()->cache->get( $cacheKey ) ) return $data;
			
			$firstDate = self::getFirstDataDate($dataType);
			if( $year != date('Y') && $year > $firstDate['year'] ){
				$returnArr = Yii::app()->locale->getMonthNames('wide', true);
			}elseif( $year != date('Y') && $year == $firstDate['year'] ){
				$returnArr = Yii::app()->locale->getMonthNames('wide', true);
				for( $i=1; $i<$firstDate['mo']; $i++ ) unset($returnArr[$i]);
			}else{
				$months = Yii::app()->locale->getMonthNames('wide', true);
				$returnArr = array();
				$currentMo = date('n');
				foreach( $months as $num => $name ){
					$returnArr[$num] = $name;
					if( $num == $currentMo ) break;
				}
			}
			
			Yii::app()->cache->set( $cacheKey, $returnArr, 60*60*24 );
			return $returnArr;
		}
		static function getMonthsAvailableByYearForSiteMap( $year, $type = 'cbr' ){
			$cacheKey = self::monthsAvailableByYearForSiteMapCacheKey . $year . '.type.' . $type;
			if( Yii::app()->cache->get( $cacheKey ) ) return Yii::app()->cache->get( $cacheKey );
			
			$firstDate = self::getFirstDataDate($type);
			if( $year != date('Y') && $year > $firstDate['year'] ){
				$returnArr = array(1,2,3,4,5,6,7,8,9,10,11,12);
			}elseif( $year != date('Y') && $year == $firstDate['year'] ){
				$returnArr= array();
				for( $i=$firstDate['mo']; $i<=12; $i++ ) $returnArr[]=$i;
			}else{
				$months = array(1,2,3,4,5,6,7,8,9,10,11,12);
				$returnArr = array();
				$currentMo = date('n');
				foreach( $months as $num ){
					$returnArr[] = $num;
					if( $num == $currentMo ) break;
				}
			}
			
			Yii::app()->cache->set( $cacheKey, $returnArr, 60*60*24 );
			return $returnArr;
		}
		static function getLastDayAvailableByYearMo( $year, $month ){
			$cacheKey = self::lastDayAvailableByYearMoCacheKey . 'year'. $year . 'mo' . $month;
			if( Yii::app()->cache->get( $cacheKey ) ) return Yii::app()->cache->get( $cacheKey );
			
			$currentYear = date('Y');
			$currentMo = date('n');
			
			if( ($year != $currentYear) || ($year == $currentYear && $month != $currentMo) ){
				$returnDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			}else{
				$returnDay = date('j');
			}
			
			Yii::app()->cache->set( $cacheKey, $returnDay, 60*60*24 );
			return $returnDay;
		}
		static function getFirstDayAvailableByYearMo( $year, $month, $dataType = 'cbr' ){
			$cacheKey = self::firstDayAvailableByYearMoCacheKey . 'year'. $year . 'mo' . $month . '.type.' . $dataType;
			
			if( Yii::app()->cache->get( $cacheKey ) ) return Yii::app()->cache->get( $cacheKey );
			
			$firstDate = self::getFirstDataDate($dataType);
			if( $firstDate['year'] == $year &&  $firstDate['mo'] == $month ){
				$returnDay = $firstDate['day'];
			}else{
				$returnDay = 1;
			}
			Yii::app()->cache->set( $cacheKey, $returnDay, 60*60*24 );
			return $returnDay;
		}
		
		public function getImgData( $data ){
			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);
			return base64_decode($data);
		}
		public function mkpath( $path ){
			if( file_exists( $path ) || @mkdir( $path ) ) return true;
			return ( $this->mkpath ( dirname( $path ) ) and mkdir( $path ) );
		}
		public function saveBase64Img( $img ){
			$img = $this->getImgData( $img );
			$path = Yii::app()->basePath . '/../' . self::PATHUploadDir;
			if( $this->mkpath( $path ) ){
				file_put_contents( $path . '/' . $this->code . '.jpg' , $img);
			}
		}
		public function needToUpdateImg(){
			$path = Yii::app()->basePath . '/../' . self::PATHUploadDir;
			$file = $path . '/' . $this->code . '.jpg';
			clearstatcache(); 
			if( !file_exists($file) ) return true;
			if( time() - filemtime($file) > 60*60*24 ) return true;
			return false;
		}
		public function getOgImage(){
			$path = Yii::app()->basePath . '/../' . self::PATHUploadDir;
			if( file_exists( $path . '/' . $this->code . '.jpg' ) ) return Yii::App()->request->getHostInfo() . '/' . self::PATHUploadDir . '/' . $this->code . '.jpg';
			return '';
		}
		
		
		
		public function getSrtDigitalCode(){
			if( $this->cbrDigitCode < 100 ) return '0' . $this->cbrDigitCode;
			return $this->cbrDigitCode;
		}
		public function getTomorrowData(){
			if( $this->defaultCurrency ){
				$m = new CurrencyRatesHistoryModel;
				$m->value = 1;
				$m->nominal = 1;
				return $m;
			}else{
				$weekDay = date('w');
				if( $weekDay == 6 || $weekDay == 7 ) return $this->todayData;
				return $this->tomorrowOneData;
			}
		}
		public function getTodayData(){
			if( $this->defaultCurrency ){
				$m = new CurrencyRatesHistoryModel;
				$m->value = 1;
				$m->nominal = 1;
				return $m;
			}else{
				return CurrencyRatesHistoryModel::model()->find(array(
					'order' => 'date DESC', 
					'condition' => " date <= :date AND idCurrency = :id  ",
					'params' => array( ':date' => date('Y-m-d'), ':id' => $this->id )
				));
			}
		}
		public function getTodayEcbData(){
			if( $this->defaultCurrencyEcb ){
				$m = new CurrencyRatesHistoryModel;
				$m->value = 1;
				return $m;
			}else{
				return CurrencyRatesHistoryEcbModel::model()->find(array(
					'order' => 'date DESC', 
					'condition' => " date <= :date AND idCurrency = :id  ",
					'params' => array( ':date' => date('Y-m-d'), ':id' => $this->id )
				));
			}
		}
		public function getPrevDayData(){
			if( $this->defaultCurrency ){
				$m = new CurrencyRatesHistoryModel;
				$m->value = 1;
				$m->nominal = 1;
				return $m;
			}else{
				$models = CurrencyRatesHistoryModel::model()->findAll(array(
					'order' => 'date DESC', 
					'condition' => " date <= :date AND idCurrency = :id  ",
					'params' => array( ':date' => date('Y-m-d'), ':id' => $this->id ),
					'limit' => 2
				));
				if( $models && $models[1] ) return $models[1];
				return false;
			}
		}
		public function getPrevEcbDayData(){
			if( $this->defaultCurrencyEcb ){
				$m = new CurrencyRatesHistoryModel;
				$m->value = 1;
				return $m;
			}else{
				$models = CurrencyRatesHistoryEcbModel::model()->findAll(array(
					'order' => 'date DESC', 
					'condition' => " date <= :date AND idCurrency = :id  ",
					'params' => array( ':date' => date('Y-m-d'), ':id' => $this->id ),
					'limit' => 2
				));
				if( $models && $models[1] ) return $models[1];
				return false;
			}
		}
		
		static function findBySlug( $slug ){
			return self::model()->find(array(
				'condition' => " `code` = :slug AND `noData` = 0 AND `defaultCurrency` = 0 ",
				'params' => array( ':slug' => strtoupper($slug) ),
			));
		}
		public function getAvailableLangs(){
			$langsArr = array();
			foreach( $this->i18ns as $i18n ){
				if( $i18n->name ){
					$langsArr[] = LanguageModel::getLangFromLangsFileByID( $i18n->idLanguage );
				}
			}
			return $langsArr;
		}
		static function findBySlugWithLang( $slug ){
			return self::model()->find(array(
				'with' => Array( 'currentLanguageI18N' ),
				'condition' => " `code` = :slug AND `cLI18NCurrencyRatesModel`.`name` IS NOT NULL AND `cLI18NCurrencyRatesModel`.`name` <> '' ",
				'params' => array( ':slug' => strtoupper($slug) ),
			));
		}
		
		static function getPopularCurrencies(){
			if( Yii::app()->cache->get( self::popularCurrenciesCacheKey ) ) return Yii::app()->cache->get( self::popularCurrenciesCacheKey );
			$models = self::model()->findAll(array(
				'with' => array( 'country' ),
				'condition' => " `noData` = 0 AND `popular` = 1 AND `t`.`defaultCurrency` = 0 ",
				'order' => " `currencyOrder` ASC ",
			));
			if( !$models ) return false;
			$outArr = array();
			Yii::app()->cache->set( self::popularCurrenciesCacheKey, $models, self::cacheTime * 60 );
			return $models;
		}
		static function getDefaultCurrency( $type = 'cbr' ){
			if( $type == 'cbr' ){
				return self::model()->find(array(
					'condition' => " `t`.`defaultCurrency` = 1 ",
				));
			}elseif( $type == 'ecb' ){
				return self::model()->find(array(
					'condition' => " `t`.`defaultCurrencyEcb` = 1 ",
				));
			}
		}
		static function getCurrenciesForConverter(){
			if( Yii::app()->cache->get( self::currenciesForConverterCacheKey ) ) return Yii::app()->cache->get( self::currenciesForConverterCacheKey );
			$models = self::model()->findAll(array(
				'condition' => " `noData` = 0 AND `t`.`defaultCurrency` = 0 ",
				'order' => " `topInSelect` DESC, `popular` DESC, `currencyOrder` ASC ",
			));
			if( !$models ) return false;
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->code] = array(
					'id' => $model->id,
					'name' => $model->name,
					'nominal' => $model->todayData->nominal,
					'value' => $model->todayData->value,
					'country' => $model->country->alias
				);
			}
			Yii::app()->cache->set( self::currenciesForConverterCacheKey, $outArr, self::cacheTime * 60 );
			return $outArr;
		}
		static function getEcbCurrenciesForConverter(){
			$cacheKey = self::ecbCurrenciesForConverterCacheKey . '_lang_' . Yii::app()->language;
			if( Yii::app()->cache->get( $cacheKey ) ) return Yii::app()->cache->get( $cacheKey );
			$models = self::model()->findAll(array(
				'condition' => " `ecb` = 1 AND `t`.`defaultCurrencyEcb` = 0 ",
				'order' => " `topInSelect` DESC, `popular` DESC, `currencyOrder` ASC ",
			));
			if( !$models ) return false;
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->code] = array(
					'id' => $model->id,
					'name' => $model->name,
					'value' => $model->todayEcbData->value,
					'country' => $model->country->alias
				);
			}
			Yii::app()->cache->set( $cacheKey, $outArr, self::cacheTime * 60 );
			return $outArr;
		}
		static function getCurrenciesForChangePage( $type = 'cbr' ){
			$cachKey = self::currenciesForChangePageCacheKey . '.type.' . $type;
			if( $data = Yii::app()->cache->get( $cachKey ) ) return $data;
			
			if( $type == 'cbr' ){
				$models = self::model()->findAll(array(
					'condition' => " `noData` = 0 AND `t`.`defaultCurrency` = 0 ",
					'order' => " `topInSelect` DESC, `popular` DESC, `currencyOrder` ASC ",
				));
			}elseif( $type == 'ecb' ){
				$models = self::model()->findAll(array(
					'condition' => " `ecb` = 1 AND `t`.`defaultCurrencyEcb` = 0 ",
					'order' => " `topInSelect` DESC, `popular` DESC, `currencyOrder` ASC ",
				));
			}
			
			if( !$models ) return false;
			$outArr = array();
			foreach( $models as $model ){
				$model->dataType = $type;
				$outArr[$model->code] = array(
					'name' => $model->name,
					'link' => $model->singleURL,
				);
			}
			Yii::app()->cache->set( $cachKey, $outArr, self::cacheTime * 60 );
			return $outArr;
		}
		public function getChartTitle() {
			$i18n = $this->getI18N();
			if( $this->dataType == 'cbr' ){
				return $i18n ? $i18n->chartTitle ? $i18n->chartTitle : $this->name.', '.Yii::t( "*", 'NameFor1Code' ).' '.$this->lastData->nominal.' '.$this->code : $this->name.', '.Yii::t( "*", 'NameFor1Code' ).' '.$this->lastData->nominal.' '.$this->code;
			}elseif( $this->dataType == 'ecb' ){
				return $i18n ? $i18n->ecbChartTitle ? $i18n->ecbChartTitle : $this->name.', '.Yii::t( "*", 'NameFor1Code' ).' 1 EUR' : $this->name.', '.Yii::t( "*", 'NameFor1Code' ).' 1 EUR';
			}
		}
		public function getRightCode(){
			if( $this->dataType == 'ecb' ) return 'EUR / ' . $this->code;
			return $this->code;
		}
		public function getEcbCode(){
			return 'EUR / ' . $this->code;
		}
		public function getConverterTitle() {
			$i18n = $this->getI18N();
			if( $this->dataType == 'cbr' ){
				return $i18n ? $i18n->converterTitle ? $i18n->converterTitle : Yii::t( "*", 'Cbr converter for today' ) : Yii::t( "*", 'Cbr converter for today' );
			}elseif( $this->dataType == 'ecb' ){
				return $i18n ? $i18n->ecbConverterTitle ? $i18n->ecbConverterTitle : Yii::t( "*", 'Ecb converter for today' ) : Yii::t( "*", 'Ecb converter for today' );
			}
		}
		public function getShortDesc() {
			$i18n = $this->getI18N();
			if( $this->dataType == 'cbr' ){
				return $i18n ? $i18n->shortDesc ? $i18n->shortDesc : '' : '';
			}elseif( $this->dataType == 'ecb' ){
				return $i18n ? $i18n->ecbShortDesc ? $i18n->ecbShortDesc : '' : '';
			}
		}
		public function getFullDesc() {
			$i18n = $this->getI18N();
			if( $this->dataType == 'cbr' ){
				return $i18n ? $i18n->fullDesc ? $i18n->fullDesc : '' : '';
			}elseif( $this->dataType == 'ecb' ){
				return $i18n ? $i18n->ecbFullDesc ? $i18n->ecbFullDesc : '' : '';
			}
		}
		public function getTitle() {
			$i18n = $this->getI18N();
			if( $this->dataType == 'cbr' ){
				return $i18n ? $i18n->title ? $i18n->title : '' : '';
			}elseif( $this->dataType == 'ecb' ){
				return $i18n ? $i18n->ecbTitle ? $i18n->ecbTitle : '' : '';
			}
		}
		public function getMetaTitle() {
			$i18n = $this->getI18N();
			if( $this->dataType == 'cbr' ){
				return $i18n ? $i18n->metaTitle ? $i18n->metaTitle : '' : '';
			}elseif( $this->dataType == 'ecb' ){
				return $i18n ? $i18n->ecbMetaTitle ? $i18n->ecbMetaTitle : '' : '';
			}
		}
		public function getMetaDesc() {
			$i18n = $this->getI18N();
			if( $this->dataType == 'cbr' ){
				return $i18n ? $i18n->metaDesc ? $i18n->metaDesc : '' : '';
			}elseif( $this->dataType == 'ecb' ){
				return $i18n ? $i18n->ecbMetaDesc ? $i18n->ecbMetaDesc : '' : '';
			}
		}
		public function getToName() {
			$i18n = $this->getI18N();
			if( !$i18n ) return '';
			if( !$i18n->toName ) return $this->name;
			return $i18n->toName;
		}
		public function getInformerName() {
			$i18n = $this->getI18N();
			if( !$i18n ) return '';
			return $i18n->informerName;
		}
		public function getMetaKeys() {
			$i18n = $this->getI18N();
			if( $this->dataType == 'cbr' ){
				return $i18n ? $i18n->metaKeys ? $i18n->metaKeys : '' : '';
			}elseif( $this->dataType == 'ecb' ){
				return $i18n ? $i18n->ecbMetaKeys ? $i18n->ecbMetaKeys : '' : '';
			}
		}
		
		
		
		
		
		private function getSettings(){
			if( !$this->settings ) $this->settings = CurrencyRatesSettingsModel::getModel();
			return $this->settings;
		}
		private function getSettingNameByDataType($settingName, $type = 'cbr'){
			if( $type == 'cbr' ) return $settingName;
			if( $type == 'ecb' ) return $type . ucfirst($settingName);
			return false;
		}
		private function getTextSettingWithReplacesAndDefaults( $settingName, $dateStr, $type = 'cbr' ){
			$textSettingName = $this->getSettingNameByDataType($settingName, $type);
			$defaultGetter = 'getDefault' . ucfirst($settingName);
			$i18n = $this->getI18N(); 
			if( !$i18n || !$i18n->{$textSettingName} ){
				$settings = $this->getSettings();
				return $settings->{$defaultGetter}($dateStr, $this, $type);
			} 
			return CommonLib::replaceDates( $i18n->{$textSettingName}, $dateStr );
		}
		
		public function getSingleArchiveYearTitle( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMetaTitle( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMetaDesc( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMetaKeys( $dateStr, $type = 'cbr' ) {
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoTitle( $dateStr, $type = 'cbr' ) {
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoMetaTitle( $dateStr, $type = 'cbr' ) {
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoMetaDesc( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoMetaKeys( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoDayTitle( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoDayMetaTitle( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoDayMetaDesc( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoDayMetaKeys( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearDescription( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearFullDesc( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoDescription( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoFullDesc( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoDayDescription( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		public function getSingleArchiveYearMoDayFullDesc( $dateStr, $type = 'cbr' ) { 
			return $this->getTextSettingWithReplacesAndDefaults( lcfirst(substr(__FUNCTION__, 3)), $dateStr, $type );
		}
		
		
		
		
		
		
		
		
		
		public function getSlug(){
			return strtolower( $this->code );
		}
		function getSingleURL() {
			if( $this->dataType == 'cbr' ) return Yii::App()->createURL( 'currencyRates/single', Array( 'slug' => $this->slug ));
			if( $this->dataType == 'ecb' ) return Yii::App()->createURL( 'currencyRates/single', Array( 'slug' => 'eur' . $this->slug, 'type' => 'ecb' ));
		}
		function getIndexURL() {
			if( $this->dataType == 'cbr' ) return Yii::app()->createUrl( 'currencyRates/index' );
			if( $this->dataType == 'ecb' ) return Yii::app()->createUrl( 'currencyRates/ecbIndex' );
		}
		static function getArchiveIndexURL( $dataType ) {
			if( $dataType == 'cbr' ) return Yii::app()->createUrl( 'currencyRates/archive' );
			if( $dataType == 'ecb' ) return Yii::app()->createUrl( 'currencyRates/ecbArchive' );
		}
		static function getSingleURLByCode( $code ) {
			return Yii::App()->createURL( 'currencyRates/single', Array( 'slug' => strtolower($code) ));
		}
		static function getArchiveSingleURLByCode( $code, $year, $mo = false, $day = false, $type = 'cbr' ) {
			if( !$year && !$mo && !$day ) return false;
			$paramsArr = Array();
			
			if( $year && !$mo && !$day ){
				$paramsArr['year'] = $year;
			}elseif( $year && $mo && !$day ){
				$paramsArr['year'] = $year;
				$paramsArr['mo'] = $mo<10 ? '0'.$mo : $mo ;
			}else{
				$paramsArr['year'] = $year;
				$paramsArr['mo'] = $mo<10 ? '0'.$mo : $mo ;
				$paramsArr['day'] = $day<10 ? '0'.$day : $day ;
			}
			
			$paramsArr['slug'] = strtolower($code);
			
			if( $type == 'ecb' ){
				$paramsArr['type'] = $type;
				$paramsArr['slug'] = 'eur' . $paramsArr['slug'];
			} 
			
			return Yii::App()->createURL( 'currencyRates/archiveSingle', $paramsArr );
		}
		function getAbsoluteSingleURL() {
			if( $this->dataType == 'cbr' ) return Yii::App()->createAbsoluteURL( 'currencyRates/single', Array( 'slug' => $this->slug ));
			if( $this->dataType == 'ecb' ) return Yii::App()->createAbsoluteURL( 'currencyRates/single', Array( 'slug' => 'eur' . $this->slug, 'type' => 'ecb' ));
		}
		public function getCountryName(){
			if( !$this->idCountry ) return false;
			return Yii::t( '*', $this->country->name );
		}
		public function getName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->name : '';
		}
		public function getHasNamePlurals(){
			$i18n = $this->getI18N();
			if( !$i18n ) return Yii::t('*','No');
			if( !$i18n->namePlurals ) return Yii::t('*','No');
			return Yii::t('*','Yes');
		}
		public function getNamePlurals( $n = false ) {
			$i18n = $this->getI18N();
			if( $i18n ){
				if( $i18n->namePlurals ){
					$message = $i18n->namePlurals;
					if($n===false)
						return $message;

					$n = floatval($n);
					if(strpos($message,'|')!==false){
						
						if(strpos($message,'#')===false){
							
							$chunks=explode('|',$message);
							$expressions=Yii::app()->getLocale(null)->getPluralRules();
							
							if($num=min(count($chunks),count($expressions))){
								
								for($i=0;$i<$num;$i++)
									$chunks[$i]=$expressions[$i].'#'.$chunks[$i];

								$message=implode('|',$chunks);
								
							}
							
						}
						
						$message=CChoiceFormat::format($message,$n);
						
					}
					
					$params['{n}']=$n;
					
					return strtr($message,$params);
					
				}else{
					$n = floatval($n);
					return $n !== false ? $n . ' ' . $this->name : $this->name;
				}
			}
			return '';
		}
		public function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->name )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->name )) return $i18n;
			}
		}
		public function getLastData(){
			return CurrencyRatesHistoryModel::model()->find(array(
				'order' => 'date DESC', 
				'condition' => " idCurrency = :id  ",
				'params' => array( ':id' => $this->id )
			));
		}
		public function getLastFormatedDataDate(){
			if( !$this->lastData ){
				return date( 'd/m/Y', time() - 60*60*24*365 * $this->firstGetDataRange );
			}
			return date( 'd/m/Y', strtotime( $this->lastData->date ) );
		}
		public function getLastDataDateForImport(){
			if( !$this->lastData ){
				return date( 'Y-m-d', time() - 60*60*24*365 * $this->firstGetDataRange );
			}
			return $this->lastData->date;
		}
		public function getLastDataDate(){
			if( !$this->lastData ) return false;
			return $this->lastData->date;
		}
		public function getLastDataVal(){
			if( !$this->lastData ) return false;
			return $this->lastData->value;
		}
		public function getLastDataNominal(){
			if( !$this->lastData ) return false;
			return $this->lastData->nominal;
		}
		static function getIdByCbrId( $cbrId ){
			$model = self::model()->findByAttributes( Array( 'cbrId' => $cbrId ));
			if( !$model ) return false;
			return $model->id;
		}
		static function findByCbrId( $cbrId ){
			return self::model()->find(array(
				'condition' => " `cbrId` = :cbrId ",
				'params' => array( ':cbrId' => $cbrId ),
			));
		}
		public function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		public function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if( ( strlen( $obj->name ) || strlen( $obj->title ) || strlen( $obj->metaTitle ) || strlen( $obj->metaDesc ) || strlen( $obj->metaKeys ) || strlen( $obj->shortDesc ) || strlen( $obj->fullDesc ) || strlen( $obj->chartTitle ) || strlen( $obj->converterTitle ) || strlen( $obj->namePlurals) || strlen( $obj->toName) || strlen( $obj->informerName) ) && in_array( $idLanguage, $idsLanguages )) {
					$i18n = CurrencyRatesI18NModel::model()->findByAttributes( Array( 'idCurrency' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = CurrencyRatesI18NModel::instance( 
							$this->id, 
							$idLanguage, 
							$obj->name, 
							$obj->title, 
							$obj->metaTitle, 
							$obj->metaDesc, 
							$obj->metaKeys, 
							$obj->shortDesc, 
							$obj->fullDesc, 
							$obj->chartTitle, 
							$obj->converterTitle, 
							$obj->namePlurals, 
							$obj->toName, 
							$obj->informerName, 
							$obj->singleArchiveYearTitle, 
							$obj->singleArchiveYearMetaTitle, 
							$obj->singleArchiveYearMetaDesc, 
							$obj->singleArchiveYearMetaKeys, 
							$obj->singleArchiveYearMoTitle, 
							$obj->singleArchiveYearMoMetaTitle, 
							$obj->singleArchiveYearMoMetaDesc, 
							$obj->singleArchiveYearMoMetaKeys, 
							$obj->singleArchiveYearMoDayTitle, 
							$obj->singleArchiveYearMoDayMetaTitle, 
							$obj->singleArchiveYearMoDayMetaDesc, 
							$obj->singleArchiveYearMoDayMetaKeys, 
							$obj->singleArchiveYearDescription, 
							$obj->singleArchiveYearFullDesc, 
							$obj->singleArchiveYearMoDescription, 
							$obj->singleArchiveYearMoFullDesc, 
							$obj->singleArchiveYearMoDayDescription, 
							$obj->singleArchiveYearMoDayFullDesc,
							$obj->ecbTitle,
							$obj->ecbMetaTitle,
							$obj->ecbMetaDesc,
							$obj->ecbMetaKeys,
							$obj->ecbShortDesc,
							$obj->ecbFullDesc,
							$obj->ecbChartTitle,
							$obj->ecbConverterTitle,
							$obj->ecbSingleArchiveYearTitle,
							$obj->ecbSingleArchiveYearMetaTitle,
							$obj->ecbSingleArchiveYearMetaDesc,
							$obj->ecbSingleArchiveYearMetaKeys,
							$obj->ecbSingleArchiveYearMoTitle,
							$obj->ecbSingleArchiveYearMoMetaTitle,
							$obj->ecbSingleArchiveYearMoMetaDesc,
							$obj->ecbSingleArchiveYearMoMetaKeys,
							$obj->ecbSingleArchiveYearMoDayTitle,
							$obj->ecbSingleArchiveYearMoDayMetaTitle,
							$obj->ecbSingleArchiveYearMoDayMetaDesc,
							$obj->ecbSingleArchiveYearMoDayMetaKeys,
							$obj->ecbSingleArchiveYearDescription,
							$obj->ecbSingleArchiveYearFullDesc,
							$obj->ecbSingleArchiveYearMoDescription,
							$obj->ecbSingleArchiveYearMoFullDesc,
							$obj->ecbSingleArchiveYearMoDayDescription,
							$obj->ecbSingleArchiveYearMoDayFullDesc
						);
						$i18n->save();
					}
					else{
						$i18n->name = $obj->name;
						$i18n->title = $obj->title;
						$i18n->metaTitle = $obj->metaTitle;
						$i18n->metaDesc = $obj->metaDesc;
						$i18n->metaKeys = $obj->metaKeys;
						$i18n->shortDesc = $obj->shortDesc;
						$i18n->fullDesc = $obj->fullDesc;
						
						$i18n->chartTitle = $obj->chartTitle;
						$i18n->converterTitle = $obj->converterTitle;
						
						$i18n->namePlurals = $obj->namePlurals;
						
						$i18n->toName = $obj->toName;
						
						$i18n->informerName = $obj->informerName;
						
						$i18n->singleArchiveYearTitle = $obj->singleArchiveYearTitle;
						$i18n->singleArchiveYearMetaTitle = $obj->singleArchiveYearMetaTitle;
						$i18n->singleArchiveYearMetaDesc = $obj->singleArchiveYearMetaDesc;
						$i18n->singleArchiveYearMetaKeys = $obj->singleArchiveYearMetaKeys;
						$i18n->singleArchiveYearMoTitle = $obj->singleArchiveYearMoTitle;
						$i18n->singleArchiveYearMoMetaTitle = $obj->singleArchiveYearMoMetaTitle;
						$i18n->singleArchiveYearMoMetaDesc = $obj->singleArchiveYearMoMetaDesc;
						$i18n->singleArchiveYearMoMetaKeys = $obj->singleArchiveYearMoMetaKeys;
						$i18n->singleArchiveYearMoDayTitle = $obj->singleArchiveYearMoDayTitle;
						$i18n->singleArchiveYearMoDayMetaTitle = $obj->singleArchiveYearMoDayMetaTitle;
						$i18n->singleArchiveYearMoDayMetaDesc = $obj->singleArchiveYearMoDayMetaDesc;
						$i18n->singleArchiveYearMoDayMetaKeys = $obj->singleArchiveYearMoDayMetaKeys;
						$i18n->singleArchiveYearDescription = $obj->singleArchiveYearDescription;
						$i18n->singleArchiveYearFullDesc = $obj->singleArchiveYearFullDesc;
						$i18n->singleArchiveYearMoDescription = $obj->singleArchiveYearMoDescription;
						$i18n->singleArchiveYearMoFullDesc = $obj->singleArchiveYearMoFullDesc;
						$i18n->singleArchiveYearMoDayDescription = $obj->singleArchiveYearMoDayDescription;
						$i18n->singleArchiveYearMoDayFullDesc = $obj->singleArchiveYearMoDayFullDesc;
						
						$i18n->ecbTitle = $obj->ecbTitle;
						$i18n->ecbMetaTitle = $obj->ecbMetaTitle;
						$i18n->ecbMetaDesc = $obj->ecbMetaDesc;
						$i18n->ecbMetaKeys = $obj->ecbMetaKeys;
						$i18n->ecbShortDesc = $obj->ecbShortDesc;
						$i18n->ecbFullDesc = $obj->ecbFullDesc;
						$i18n->ecbChartTitle = $obj->ecbChartTitle;
						$i18n->ecbConverterTitle = $obj->ecbConverterTitle;
						$i18n->ecbSingleArchiveYearTitle = $obj->ecbSingleArchiveYearTitle;
						$i18n->ecbSingleArchiveYearMetaTitle = $obj->ecbSingleArchiveYearMetaTitle;
						$i18n->ecbSingleArchiveYearMetaDesc = $obj->ecbSingleArchiveYearMetaDesc;
						$i18n->ecbSingleArchiveYearMetaKeys = $obj->ecbSingleArchiveYearMetaKeys;
						$i18n->ecbSingleArchiveYearMoTitle = $obj->ecbSingleArchiveYearMoTitle;
						$i18n->ecbSingleArchiveYearMoMetaTitle = $obj->ecbSingleArchiveYearMoMetaTitle;
						$i18n->ecbSingleArchiveYearMoMetaDesc = $obj->ecbSingleArchiveYearMoMetaDesc;
						$i18n->ecbSingleArchiveYearMoMetaKeys = $obj->ecbSingleArchiveYearMoMetaKeys;
						$i18n->ecbSingleArchiveYearMoDayTitle = $obj->ecbSingleArchiveYearMoDayTitle;
						$i18n->ecbSingleArchiveYearMoDayMetaTitle = $obj->ecbSingleArchiveYearMoDayMetaTitle;
						$i18n->ecbSingleArchiveYearMoDayMetaDesc = $obj->ecbSingleArchiveYearMoDayMetaDesc;
						$i18n->ecbSingleArchiveYearMoDayMetaKeys = $obj->ecbSingleArchiveYearMoDayMetaKeys;
						$i18n->ecbSingleArchiveYearDescription = $obj->ecbSingleArchiveYearDescription;
						$i18n->ecbSingleArchiveYearFullDesc = $obj->ecbSingleArchiveYearFullDesc;
						$i18n->ecbSingleArchiveYearMoDescription = $obj->ecbSingleArchiveYearMoDescription;
						$i18n->ecbSingleArchiveYearMoFullDesc = $obj->ecbSingleArchiveYearMoFullDesc;
						$i18n->ecbSingleArchiveYearMoDayDescription = $obj->ecbSingleArchiveYearMoDayDescription;
						$i18n->ecbSingleArchiveYearMoDayFullDesc = $obj->ecbSingleArchiveYearMoDayFullDesc;
						
						$i18n->save();
					}
				}
			}
		}
		
		protected function afterSave() {
			parent::afterSave();
			Yii::app()->cache->delete( self::currenciesForChangePageCacheKey );
			Yii::app()->cache->delete( self::currenciesForConverterCacheKey );
			Yii::app()->cache->delete( self::ecbCurrenciesForConverterCacheKey );
			Yii::app()->cache->delete( self::popularCurrenciesCacheKey );
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
		protected function beforeDelete() {
			$models = CurrencyRatesConvertModel::model()->findAll(array(
				'condition' => " `idCurrencyFrom` = :id OR `idCurrencyTo` = :id ", 
				'params' => array(':id' => $this->id ) 
			));
			foreach( $models as $model ) $model->delete();
			
			CurrencyRatesViewsModel::model()->deleteAll(
				" idCurrency = :id ", 
				array(':id' => $this->id ) 
			);
			CurrencyRatesHistoryModel::model()->deleteAll(
				" idCurrency = :id ", 
				array(':id' => $this->id ) 
			);
			
			return parent::beforeDelete();
        }
        
        //ModelForTextReplaceInterface

        public static function getPropertiesForSearch()
        {
            return [
                'todayCourse' => 'Today course',
                'prevDayCourse' => 'Prev day course',
                'tomorrowCourse' => 'Tomorrow course',
                'shortDesc' => 'Short desc',
                'title' => 'title',
                'slug' => 'slug',
                'singleURL' => 'page url',
                'indexURL' => 'index page url',
                'absoluteSingleURL' => 'absolute page url',
                'name' => 'name'
            ];
        }

        public static function getModelsListForUseInReplace()
        {
            $outArr = [];
            $models = self::model()->findAll(array(
                'with' => array( 'currentLanguageI18N'),
            ));

            foreach ($models as $model) {
                $outArr[$model->id] = $model->name;
            }

            return $outArr;
        }

        public static function getListForFilterForConvertPairs()
        {
            $models = self::model()->findAll(array('with' => array( 'currentLanguageI18N')));
            $returnArr = array();
            foreach ($models as $model) {
                $returnArr[$model->id] = $model->code . ' - ' . $model->name;
            }
            return $returnArr;
        }
	}

?>