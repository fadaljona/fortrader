<?php

class UserProfileExtModel extends ModelBase{
	private static $prefix='eprofile.';
	static function model( $class = __CLASS__ ) {
		return parent::model( $class );
	}

	public function tableName(){
		return '{{user_profile_ext}}';
	}
	
	public static function setExtSetting($idModule){
		$idUser=Yii::app()->user-id;
		if(!$idModule||!$idUser)
			return false;
		
		$setting=self::model()->find(array(
			'select'=>'`values`,`idModule`,`idUser`',
			'condition'=>'`idModule`=:idModule AND `idUser`=:idUser',
			'params'=>array(
				':idUser'=>$idUser,
				':idModule'=>$idModule
			)
		));
		if($setting)
			Yii::app()->user->setState(self::$prefix.$idModule,json_decode($setting->values));
		else return false;
	}
	
	public static function setExtSettings($idUser){
		if(!$idUser)
			return array();
		$settings=self::model()->findAll(array(
			'select'=>'`values`,`idModule`,`idUser`',
			'condition'=>'`idUser`=:idUser',
			'group'=>'`idModule`',
			'params'=>array(
				':idUser'=>$idUser
			)
		));
		if($settings){
			foreach($settings as $setting){
				Yii::app()->user->setState(self::$prefix.$setting->idModule,json_decode($setting->values));
			}
		}else return false;
		
	}

	protected function beforeSave(){
		$result=parent::beforeSave();

		return $result;
	}
} 