<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CurrencyRatesConvertViewsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function rules() {
			return Array(
				Array( 'idConvert', 'unique'),
				Array( 'idConvert', 'numerical', 'integerOnly'=>true , 'min' => 1),
				Array( 'views', 'numerical', 'integerOnly'=>true , 'min' => 0),
			);
		}
		static function instance( $idConvert, $views ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idConvert', 'views' ));
		}
		static function saveViews( $idConvert ){
			$model = self::model()->findByAttributes( Array( 'idConvert' => $idConvert ));
			if( !$model ) $model = self::instance( $idConvert, 0 );
			$model->views += 1 ;
			if( $model->validate() ) $model->save();
		}
		
	}

?>