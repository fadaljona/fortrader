<?
	Yii::import( 'models.base.ModelBase' );

	final class UserGroupToUserRightModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idUserGroup, $idUserRight ) {
			$model = new self();
			$model->idUserGroup = $idUserGroup;
			$model->idUserRight = $idUserRight;
			return $model;
		}
	}

?>