<?php

Yii::import('models.base.ModelBase');

final class PaymentSystemModel extends ModelBase
{
    const PATHUploadDir = 'uploads/paymentSystems';

    static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    function relations()
    {
        return array(
            'i18ns' => array( self::HAS_MANY, 'PaymentSystemI18nModel', 'idSystem', 'alias' => 'i18nsPaymentSystem' ),
            'currentLanguageI18N' => array( self::HAS_ONE, 'PaymentSystemI18nModel', 'idSystem',
                'alias' => 'cLI18NPaymentSystemModel',
                    'on' => '`cLI18NPaymentSystemModel`.`idLanguage` = :idLanguage',
                    'params' => array(
                        ':idLanguage' => LanguageModel::getCurrentLanguageID(),
                    ),
                ),
        );
    }

    public static function getListForFilter()
    {
        $models = self::model()->findAll(array(
            'with' => ('currentLanguageI18N'),
            'condition' => " `cLI18NPaymentSystemModel`.`title` IS NOT NULL AND `cLI18NPaymentSystemModel`.`title` <> '' "
        ));
        $returnArr = array();
        foreach ($models as $model) {
            $returnArr[$model->id] = $model->title;
        }
        return $returnArr;
    }

    static function getAdminListDp($filterModel = false)
    {
        $DP = new CActiveDataProvider(get_called_class(), array(
            'criteria' => array(
                'with' => array( 'currentLanguageI18N', 'i18ns' ),
                'order' => ' `t`.`id` DESC ',
            ),
        ));
        return $DP;
    }
    static function findBySlug($slug)
    {
        return self::model()->find(array(
            'condition' => " `slug` = :slug ",
            'params' => array( ':slug' => $slug ),
        ));
    }
    public function getAvailableLangs()
    {
        $langsArr = array();
        foreach ($this->i18ns as $i18n) {
            if ($i18n->title) {
                $langsArr[] = LanguageModel::getLangFromLangsFileByID($i18n->idLanguage);
            }
        }
        return $langsArr;
    }
    static function findBySlugWithLang($slug)
    {
        return self::model()->find(array(
            'with' => array('currentLanguageI18N'),
            'condition' => " `slug` = :slug AND `cLI18NPaymentSystemModel`.`title` IS NOT NULL AND `cLI18NPaymentSystemModel`.`title` <> '' ",
            'params' => array( ':slug' => $slug ),
        ));
    }





    function getPathOgImage()
    {
        return strlen($this->ogImage) ? self::PATHUploadDir."/{$this->ogImage}" : '';
    }
    function getSrcOgImage()
    {
        return $this->pathOgImage ? Yii::app()->baseUrl."/{$this->pathOgImage}" : '';
    }
    function getAbsoluteSrcOgImage()
    {
        return $this->pathOgImage ? Yii::app()->createAbsoluteUrl($this->pathOgImage) : '';
    }
    function uploadOgImage($uploadedFile)
    {
        $fileEx = strtolower($uploadedFile->getExtensionName());
        list( $fileName, $fileEx ) = explode(".", CommonLib::getFreeFileName(self::PATHUploadDir, $fileEx));

        $fileFullName = "{$fileName}.{$fileEx}";
        $filePath = self::PATHUploadDir."/{$fileFullName}";
        if (!@$uploadedFile->saveAs($filePath)) {
            throw new Exception("Can't write to ".self::PATHUploadDir);
        }

        $this->setOgImage($fileFullName);
    }
    function setOgImage($name)
    {
        $this->deleteOgImage();
        $this->ogImage = $name;
    }
    function deleteOgImage()
    {
        if (strlen($this->ogImage)) {
            if (is_file($this->pathOgImage) and !@unlink($this->pathOgImage)) {
                throw new Exception("Can't delete {$this->pathOgImage}");
            }
            $this->ogImage = null;
        }
    }
    function getSingleURL()
    {
        //return Yii::App()->createURL( 'informers/single', Array( 'slug' => $this->slug ));
    }

    function getI18N()
    {
        if ($this->currentLanguageI18N) {
            return $this->currentLanguageI18N;
        }
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == 0) {
                if (strlen($i18n->title)) {
                    return $i18n;
                }
                break;
            }
        }
        foreach ($this->i18ns as $i18n) {
            if (strlen($i18n->title)) {
                return $i18n;
            }
        }
    }
    function getTitleByLang($langId)
    {
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == $langId) {
                if (strlen($i18n->title)) {
                    return $i18n->title;
                }
                return false;
            }
        }
    }

    function getTitle()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->title : '';
    }
    function getMetaTitle()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->metaTitle : '';
    }
    function getMetaDesc()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->metaDesc : '';
    }
    function getMetaKeys()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->metaKeys : '';
    }
    function getShortDesc()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->shortDesc : '';
    }
    function getFullDesc()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->fullDesc : '';
    }




        # i18ns
    function deleteI18Ns()
    {
        foreach ($this->i18ns as $i18n) {
            $i18n->delete();
        }
    }
    function addI18Ns($i18ns)
    {
        $idsLanguages = LanguageModel::getExistsIDS();
        foreach ($i18ns as $idLanguage => $obj) {
            $i18n = PaymentSystemI18nModel::model()->findByAttributes(array( 'idSystem' => $this->id, 'idLanguage' => $idLanguage ));
            if (!$i18n) {
                $i18n = PaymentSystemI18nModel::instance($this->id, $idLanguage, $obj->title, $obj->metaTitle, $obj->metaDesc, $obj->metaKeys, $obj->shortDesc, $obj->fullDesc);
            } else {
                $i18n->title = $obj->title;
                $i18n->metaTitle = $obj->metaTitle;
                $i18n->metaDesc = $obj->metaDesc;
                $i18n->metaKeys = $obj->metaKeys;
                $i18n->shortDesc = $obj->shortDesc;
                $i18n->fullDesc = $obj->fullDesc;
            }
            $i18n->save();
        }
    }
    function setI18Ns($i18ns)
    {
        $this->addI18Ns( $i18ns );
    }
        # events
    protected function afterDelete()
    {
        parent::afterDelete();
        $this->deleteI18Ns();
    }
}
