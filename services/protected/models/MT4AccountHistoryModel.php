<?
	Yii::import( 'models.base.ModelBase' );

	final class MT4AccountHistoryModel extends ModelBase {
		public $revision;
		
		public $timestamp;
		public $profit;
		public $commission;
		public $swap;
		
		public $SYMBOL, $DEAL_TYPE, $SUM_DEAL_VOLUME, $SUM_DEAL_SWAP, $SUM_DEAL_PROFIT, $DEALS_COUNT;
		
		public $closePrice, $lots;
		
		public $OrderCloseDate, $countRows;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "mt4_account_history";
		}
		function getDealTypeTitle(){
			return $this->getOrderTypeTitle();
		}
		function getOrderTypeTitle() {
			switch( $this->OrderType ) {
				case 0:{
					return 'buy';
				}
				case 1:{
					return 'sell';
				}
				case 2:{
					return 'buylimit';
				}
				case 3:{
					return 'selllimit';
				}
				case 4:{
					return 'buystop';
				}
				case 5:{
					return 'sellstop';
				}
				case 6:{
					return 'balance';
				}
			}
		}
	}

?>
