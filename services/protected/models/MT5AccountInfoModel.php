<?
	Yii::import( 'models.base.ModelBase' );

	final class MT5AccountInfoModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "mt5_account_info";
		}
		function relations() {
			return Array(
				'member' => Array( self::BELONGS_TO, 'ContestMemberModel', 'idMember' ),
				'error' => Array( self::HAS_ONE, 'ContestMonitoringErrorsModel', array( 'code' => 'last_error_code' ) ),
			);
		}
		static function getGroupedErrorsDp( ){
			$DP = new CActiveDataProvider( MT5AccountInfoModel, Array(
				'criteria' => Array(
					'group' => ' `t`.`last_error_code` ',
					'order' => ' `t`.`last_error_code` ASC ',
					'condition' => ' `t`.`last_error_code` IS NOT NULL ',
				),
			));
			return $DP;
		}
	}

?>