<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestForeignI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{contest_foreign_i18n}}";
		}
		static function instance( $idContest, $idLanguage, $title, $sponsor, $link ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idContest', 'idLanguage', 'title', 'sponsor', 'link' ));
		}
	}
?>