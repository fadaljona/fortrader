<?php

Yii::import('models.base.ModelBase');

final class PaymentExchangerCourseModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function relations()
    {
        return array(
            'exchanger' => array( self::BELONGS_TO, 'PaymentExchangerModel', 'idExchanger'),
            'fromCurrencyModel' => array( self::HAS_ONE, 'PaymentSystemCurrencyModel', array('id' => 'fromCurrency')),
            'toCurrencyModel' => array( self::HAS_ONE, 'PaymentSystemCurrencyModel', array('id' => 'toCurrency')),
            'courseExchangerReserve' => array(
                self::HAS_ONE,
                'PaymentExchangerReserveModel',
                array(
                    'idCurrency' => 'toCurrency',
                    'idExchanger' => 'idExchanger',
                    'date' => 'date'
                )
            ),
        );
    }
}
