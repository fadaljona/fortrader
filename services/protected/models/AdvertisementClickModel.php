<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementClickModel extends ModelBase {
		public $blockIP;
		public $blockAgent;
		public $count;
		public $clickDate;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'advertisement' => Array( self::BELONGS_TO, 'AdvertisementModel', 'idAd' ),
			);
		}
		static function instance( $idAd ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idAd' ));
		}
		static function clearAdvertisementClicks( $idAd ) {
			Yii::App()->db->createCommand( "
				DELETE
				FROM			{{advertisement_click}} 
				WHERE			`idAd` = :idAd
			" )->query(Array(
				':idAd' => $idAd,
			));
		}
		static function clearCampaignClicks( $idCampaign ) {
			Yii::App()->db->createCommand( "
				DELETE
				FROM			{{advertisement_click}} 
				WHERE			EXISTS (
									SELECT		1
									FROM		`{{advertisement}}`
									WHERE		`{{advertisement}}`.`id` = `{{advertisement_click}}`.`idAd`
												AND `{{advertisement}}`.`idCampaign` = :idCampaign
									LIMIT		1
								)
			" )->query(Array(
				':idCampaign' => $idCampaign,
			));
		}
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( $this->isNewRecord ) {
					$this->ip = sprintf( "%u", ip2long( $_SERVER[ 'REMOTE_ADDR' ]));
					$this->agent = $_SERVER[ 'HTTP_USER_AGENT' ];
				}
			}
			return $result;
		}
	}

?>