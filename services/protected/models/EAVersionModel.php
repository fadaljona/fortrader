<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAVersionModel extends ModelBase {
		const PATHUploadDir = 'uploads/eaVersions';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_version}}";
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'EAVersionI18NModel', 'idVersion', 'alias' => 'i18nsEAVersion' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'EAVersionI18NModel', 'idVersion', 
					'alias' => 'cLI18NEAVersion',
					'on' => '`cLI18NEAVersion`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'EA' => Array( self::BELONGS_TO, 'EAModel', 'idEA' ),
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'EAStatements' => Array( self::HAS_MANY, 'EAStatementModel', 'idVersion' ),
				'countMessages' => Array( self::STAT, 'UserMessageModel', 'idLinkedObj', 'condition' => " `instance` = 'eaVersion/single' " ),
			);
		}
		static function getAddURL( $idEA ) {
			return Yii::App()->createURL( 'eaVersion/add', Array( 'idEA' => $idEA ));
		}
		static function getEditURL( $id ) {
			return Yii::App()->createURL( 'eaVersion/edit', Array( 'id' => $id ));
		}
		static function getModelSingleURL( $id ) {
			return Yii::App()->createURL( 'eaVersion/single', Array( 'id' => $id ));
		}
		static function getModelDownloadURL( $id ) {
			return Yii::App()->createURL( 'eaVersion/download', Array( 'id' => $id ));
		}
		static function getModelViewURL( $id ) {
			return Yii::App()->createURL( 'eaVersion/view', Array( 'id' => $id ));
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->desc )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->desc )) return $i18n;
			}
		}
		function getDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->desc : '';
		}
		
		function getSingleURL() {
			return self::getModelSingleURL( $this->id );
		}
		function getDownloadURL() {
			return self::getModelDownloadURL( $this->id );
		}
		function getViewURL() {
			return self::getModelViewURL( $this->id );
		}
		function viewed() {
			$this->views++;
			$this->update( Array( 'views' ));
		}
		function downloaded() {
			$this->downloads++;
			$this->update( Array( 'downloads' ));
			if( !Yii::App()->user->isGuest ) {
				UserActivityModel::event( UserActivityModel::TYPE_EVENT_DOWNLOAD_EA_VERSION, Array( 'idEAVersion' => $this->id, 'idUser' => Yii::App()->user->id ));
			}
		}
			
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->desc )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = EAVersionI18NModel::model()->findByAttributes( Array( 'idVersion' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = EAVersionI18NModel::instance( $this->id, $idLanguage, $obj->desc );
						$i18n->save();
					}
					else{
						$i18n->desc = $obj->desc;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			
			# file
		function getPathFile() {
			return strlen($this->nameFile) ? self::PATHUploadDir."/{$this->nameFile}" : '';
		}
		function getSrcFile() {
			return $this->pathFile ? Yii::app()->baseUrl."/{$this->pathFile}" : '';
		}
		function deleteFile() {
			if( strlen( $this->nameFile )) {
				if( is_file( $this->pathFile ) and !@unlink( $this->pathFile )) throw new Exception( "Can't delete {$this->pathFile}" );
				$dir = dirname( $this->pathFile );
				if( $dir != self::PATHUploadDir and is_dir( $dir ) and !@rmdir( $dir )) throw new Exception( "Can't delete {$dir}" );
				$this->nameFile = null;
			}
		}
		function setFile( $name ) {
			$this->deleteFile();
			$this->nameFile = $name;
		}
		function uploadFile( $uploadedFile ) {
			$dirName = CommonLib::getFreeFileName( self::PATHUploadDir );
			$filePath = self::PATHUploadDir."/{$dirName}";
			is_dir( $filePath ) or mkdir( $filePath );
			
			$fileName = $uploadedFile->getName();
			$fileRelPath = "{$dirName}/{$fileName}";
			$fileFullPath = "{$filePath}/{$fileName}";
			
			if( !@$uploadedFile->saveAs( $fileFullPath )) throw new Exception( "Can't write to ".self::PATHUploadDir );
			$this->setFile( $fileRelPath );
		}
			
			# EAStatements
		function deleteEAStatements() {
			foreach( $this->EAStatements as $statsment ) {
				$statsment->delete();
			}
		}
		
			# events
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA_VERSION, Array( 'idEAVersion' => $this->id, 'idUser' => $this->idUser ));
			}
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			$this->deleteFile();
			$this->deleteEAStatements();
		}
	}

?>