<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CurrencyRatesHistoryModel extends ModelBase {
		const cacheTime = 5; //5mins
		const dataForPeriodCacheKey = 'dataForPeriodCacheKey';
		const dataForConverterPeriodCacheKey = 'dataForConverterPeriodCacheKey';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function rules() {
			return Array(
				Array( 'value, diffValue, nominal', 'numerical' ),
			);
		}
		public function getId(){
			return $this->idCurrency;
		}
		function relations() {
			return Array(
				'currency' => array( self::HAS_ONE, 'CurrencyRatesModel', array( 'id' => 'idCurrency' ) ),	
			);

		}
		static function getIdsToSetUpDiffValues(){
			$models = self::model()->findAll(array(
				'select' => array( " `t`.`idCurrency` " ),
				'condition' => " `t`.`diffValue` IS NULL ",
				'group' => " `t`.`idCurrency` "
			));
			if( !$models ) return false;
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = $model->idCurrency;
			}
			return $outArr;
		}
		static function getDataForConverterChart( $period, $modelId ){
			
			$model = CurrencyRatesConvertModel::model()->findByPk($modelId);
			if( !$model ) return false;
			
			$idCurrencyFrom = $model->idCurrencyFrom;
			$idCurrencyTo = $model->idCurrencyTo;
			$value = $model->value;
			
			$cachKey = self::dataForConverterPeriodCacheKey . '.period.' . $period . '.idCurrencyFrom.' . $idCurrencyFrom . '.idCurrencyTo.' . $idCurrencyTo . '.value.' . $value;
			if( Yii::app()->cache->get( $cachKey ) ) return Yii::app()->cache->get( $cachKey );
			
			$modelFrom = $model->rateFrom;
			$modelTo = $model->rateTo;
			
			if( !$modelFrom || !$modelTo ) return false;
			
			if( !$modelFrom->defaultCurrency && !$modelTo->defaultCurrency ){
				$rows =  Yii::App()->db->createCommand(" 
					SELECT `from`.`date` AS `commonDate`, `from`.`value` AS `fromValue`, `from`.`nominal` AS `fromNominal`, `to`.`value` AS `toValue`, `to`.`nominal` AS `toNominal` 
					FROM `{{currency_rates_history}}` `from`
					LEFT JOIN `{{currency_rates_history}}` `to` ON `to`.`date` = `from`.`date` AND `to`.`idCurrency` = :idCurrencyTo
					WHERE `from`.`idCurrency` = :idCurrencyFrom AND `to`.`idCurrency` IS NOT NULL AND `from`.`date` >= :date
					ORDER BY `from`.`date` DESC
				")->queryAll(true, array( ':idCurrencyFrom' => $idCurrencyFrom, ':idCurrencyTo' => $idCurrencyTo, ':date' => date('Y-m-d', time() - $period * 60*60*24 ) ) );

				
				$outData = '';
				foreach( $rows as $row ){
					$val = $value * ( $row['fromValue'] / $row['fromNominal'] ) / ( $row['toValue'] / $row['toNominal'] ) ;
					$outData = "{$row['commonDate']};{$val}\n" . $outData;
				}
				
			}else{
				$idCur = $modelFrom->defaultCurrency ? $idCurrencyTo : $idCurrencyFrom;
				
				$rows =  Yii::App()->db->createCommand(" 
					SELECT `from`.`date`, `from`.`value`, `from`.`nominal`
					FROM `{{currency_rates_history}}` `from`
					WHERE `from`.`idCurrency` = :idCurrencyFrom AND `from`.`date` >= :date
					ORDER BY `from`.`date` DESC
				")->queryAll(true, array( ':idCurrencyFrom' => $idCur, ':date' => date('Y-m-d', time() - $period * 60*60*24 ) ) );
				
				if( $modelFrom->defaultCurrency ){
					$outData = '';
					foreach( $rows as $row ){
						$val = $value / ( $row['value'] / $row['nominal'] ) ;
						$outData = "{$row['date']};{$val}\n" . $outData;
					}
					
				}else{
					$outData = '';
					foreach( $rows as $row ){
						$val = $value * ( $row['value'] / $row['nominal'] ) ;
						$outData = "{$row['date']};{$val}\n" . $outData;
					}
					
				}
			}
			
			$outData = trim($outData) ;
			Yii::app()->cache->set( $cachKey, $outData, self::cacheTime * 60 );
			return $outData;
		}
		static function getDataForPeriod( $period, $idCurrency ){
			$cachKey = self::dataForPeriodCacheKey . '.period.' . $period . '.currency.' . $idCurrency;
			if( Yii::app()->cache->get( $cachKey ) ) return Yii::app()->cache->get( $cachKey );
			$models = self::model()->findAll(array(
				'condition' => " `t`.`idCurrency` = :id AND `t`.`date` >= :date ",
				'order' => " `t`.`date` DESC ",
				'params' => array( ':id' => $idCurrency, ':date' => date('Y-m-d', time() - $period * 60*60*24 ) ),
			));

			$lastNominal = 1;
			if( $models ){
				$lastNominal = $models[0]->nominal;
			}
			
			$outData = '';
			foreach( $models as $model ){
				$value = ($model->value / $model->nominal) * $lastNominal;
				$outData = "{$model->date};{$value}\n" . $outData;
			}
			$outData = trim($outData) ;
			Yii::app()->cache->set( $cachKey, $outData, self::cacheTime * 60 );
			return $outData;
		}
		static function getLastValWithDiff( $id ){
			$model = self::model()->find(array(
				'select' => array( " `value` " ),
				'condition' => " `diffValue` IS NOT NULL AND `idCurrency` = :id ",
				'order' => " `date` DESC ",
				'params' => array( ':id' => $id ),
			));
			if( !$model ) return 0;
			return $model->value;
		}
		static function setupDiffValuesForOne( $id ){
			$lastVal = self::getLastValWithDiff( $id );
			$models = self::getModelsToSetupDiffValuesForCurrency( $id );
			if( !$models ) return false;
			foreach( $models as $model ){
				if( !$lastVal ){
					$model->diffValue = 0;
				}else{
					$model->diffValue = $model->value - $lastVal;
				}
				if( $model->validate() ){
					$model->save();
					$lastVal = $model->value;
				}else{
					throw new CException("Can't validate history model " );
				}
			}
		}
		static function getModelsToSetupDiffValuesForCurrency( $id ){
			return self::model()->findAll(array(
				'condition' => " `t`.`diffValue` IS NULL AND `idCurrency` = :id ",
				'order' => " `t`.`date` ASC ",
				'params' => array( ':id' => $id ),
			));
		}
		static function getDaySecondsToNewData(){
			$avgDayTime = Yii::App()->db->createCommand(" 
				SELECT CEILING( SUM( MOD( `createdTime`, 86400 ) ) / count( `idCurrency` ) ) as `avgDayTime` 
				FROM `{{currency_rates_history}}` 
				WHERE `createdTime` <> 0 AND WEEKDAY( FROM_UNIXTIME(`createdTime`) ) = :weekDay
				ORDER BY `date` DESC 
				LIMIT 200
			")->queryScalar( array(':weekDay' => date('N') -1 ) );
			
			if( !$avgDayTime ) return false;
			$nowDayTime = time() % 86400;
			if( $nowDayTime >= $avgDayTime ) return false;
			return  $avgDayTime - $nowDayTime;
		}
		
		
		static function setUpDiffValues(){
			$ids = self::getIdsToSetUpDiffValues();
			if( !$ids ) return false;
			foreach( $ids as $id ){
				self::setupDiffValuesForOne( $id );
				echo "diff val setuped for model {$id}\n";
			}
		}
	}

?>