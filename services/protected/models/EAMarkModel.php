<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAMarkModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idEA, $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idEA', 'idUser' ));
		}
		function tableName() {
			return "{{ea_mark}}";
		}
		protected function afterSave() {
			parent::afterSave();
			UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_EA_MARK, Array( 'idUser' => $this->idUser, 'idEA' => $this->idEA, 'average' => $this->average ));
		}
	}

?>