<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CurrencyRatesConvertI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{currency_rates_convert_i18n}}";
		}
		static function instance( $idPair, $idLanguage, $fullDesc, $metaKeys, $metaDesc, $title, $metaTitle ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idPair', 'idLanguage', 'fullDesc', 'metaKeys', 'metaDesc', 'title', 'metaTitle' ));
		}
	}

?>