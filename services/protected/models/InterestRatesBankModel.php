<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InterestRatesBankModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'InterestRatesBankI18nModel', 'idBank', 'alias' => 'i18nsInterestRatesBank' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'InterestRatesBankI18nModel', 'idBank', 
					'alias' => 'cLI18NInterestRatesBankModel',
					'on' => '`cLI18NInterestRatesBankModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'country' => Array( self::BELONGS_TO, 'CountryModel', 'idCountry' ),
			);
		}


		static function getAdminListDp( $filterModel = false ){
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns', 'country' ),
					'order' => ' `t`.`order` DESC ',
				),
			));
			return $DP;
		}

		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->title )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->title )) return $i18n;
			}
		}
		function getTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->title : '';
		}
		function getShortTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->shortTitle : '';
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
		
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->title )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = InterestRatesBankI18nModel::model()->findByAttributes( Array( 'idBank' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = InterestRatesBankI18nModel::instance( $this->id, $idLanguage, $obj->title, $obj->shortTitle );
					}else{
						$i18n->title = $obj->title;
						$i18n->shortTitle = $obj->shortTitle;
					}
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>