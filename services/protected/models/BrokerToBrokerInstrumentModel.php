<?
	Yii::import( 'models.base.ModelBase' );

	final class BrokerToBrokerInstrumentModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idBroker, $idInstrument ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker', 'idInstrument' ));
		}
	}

?>