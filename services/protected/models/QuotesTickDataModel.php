<?php
Yii::import('models.base.ModelBase');

final class QuotesTickDataModel extends ModelBase
{
    static public $prevTickDataFile = '/data/prevQuotesTickData.txt';

    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }
    public static function updateData()
    {
        $quotes = self::getAllQuotes();
            
        if (!$quotes) {
            return false;
        }
            
        foreach ($quotes as $key => $quote) {
            $deleteSql = " DELETE FROM `{{quotes_tick_data}}` WHERE `symbol` = :symbol ";
            $deleteParams = array( ':symbol' => $key );
            Yii::App()->db->createCommand($deleteSql)->query($deleteParams);
        }
            
        $insertSql = 'INSERT IGNORE INTO `{{quotes_tick_data}}` (`symbol`, `bid`, `ask`, `time`, `spread`, `trend`, `precision`, `pcp`, `updatedDT`) VALUES ';
        $insertParams = array();
            
        $i=0;
        foreach ($quotes as $quote) {
            if ($i) {
                $insertSql .= ',';
            }
                
            $tmpArr = explode(';', $quote);
            $name = @$tmpArr[0];
            if (!$name) {
                continue;
            }
                
            $insertSql .= "(:symbol$i, :bid$i, :ask$i, :time$i, :spread$i, :trend$i, :precision$i, :pcp$i, :updatedDT$i)";
            $insertParams[":symbol$i"] = $name;
            $insertParams[":bid$i"] = str_replace(',', '.', @$tmpArr[1]);
            $insertParams[":ask$i"] = str_replace(',', '.', @$tmpArr[2]);
            $insertParams[":time$i"] = @$tmpArr[3];
            $insertParams[":spread$i"] = str_replace(',', '.', @$tmpArr[4]);
            $insertParams[":trend$i"] = @$tmpArr[5];
            $insertParams[":precision$i"] = @$tmpArr[8];
            $insertParams[":pcp$i"] = str_replace(',', '.', @$tmpArr[9]);
            $insertParams[":updatedDT$i"] = date("Y-m-d H:i:s");
                
            $i++;
        }
            
        if (Yii::App()->db->createCommand($insertSql)->query($insertParams)) {
                echo "tick data inserted \n";
        }

        self::updateBitzTickData();
        self::updateBitfinexTickData();
        self::updateOkexTickData();
    }
    public static function updateOkexTickData()
    {
        $dataUrl = 'https://www.okex.com/api/v1/ticker.do?symbol=';
        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'select' => " okexSymbol ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`okexSymbol` <> '' AND `t`.`sourceType` = 'okex' ",
        ));
        foreach ($quotes as $quote) {
            $json = CommonLib::downloadFileFromWww($dataUrl . $quote->okexSymbol);
            if (!$json) {
                echo "no data from okex\n";
                continue;
            }
            $json = json_decode($json);
            if (!$json) {
                echo "no data from okex\n";
                continue;
            }
            $deleteSql = " DELETE FROM `{{quotes_tick_data}}` WHERE `symbol` = :symbol ";
            $deleteParams = array( ':symbol' => $quote->okexSymbol );
            Yii::App()->db->createCommand($deleteSql)->query($deleteParams);
        
            $insertSql = 'INSERT IGNORE INTO `{{quotes_tick_data}}` (`symbol`, `bid`, `ask`, `time`, `updatedDT`) VALUES ';
            $insertParams = array();

            $insertSql .= "(:symbol, :bid, :ask, :time, :updatedDT)";
            $insertParams[":symbol"] = $quote->okexSymbol;
            $insertParams[":bid"] = $json->ticker->buy;
            $insertParams[":ask"] = $json->ticker->sell;
            $insertParams[":time"] = date('H:i:s', $json->date - 60*60*1);
            $insertParams[":updatedDT"] = date("Y-m-d H:i:s");

            if (Yii::App()->db->createCommand($insertSql)->query($insertParams)) {
                echo "okex tick data inserted \n";
            }
        }
    }
    public static function updateBitzTickData()
    {
        $json = CommonLib::downloadFileFromWww('https://www.bit-z.com/api_v1/tickerall');
        if (!$json) {
            return false;
        }
        $json = json_decode($json);
        if ($json->code || !$json->data) {
            return false;
        }
        $dataToInsert = $json->data;
            

        $bitzSymbols = array(
            'btc_usdt', 'eth_usdt', 'bch_btc', 'eth_btc', 'ltc_btc', 'etc_btc', 'zec_btc', 'mzc_btc', 'gxs_btc', 'fct_btc', 'btx_btc', 'lsk_btc', 'dash_btc', 'omg_btc', 'nuls_btc', 'hsr_btc', 'pay_btc', 'eos_btc', 'game_btc', 'xas_btc', 'ppc_btc', 'voise_btc', 'ybct_btc', 'blk_btc', 'xpm_btc', 'qtum_btc', 'viu_btc', 'sss_btc', 'bcd_btc', 'doge_btc', 'dgb_btc', 'btg_btc', 'ark_btc', 'part_btc', 'otn_btc', 'leo_btc', 'gxs_eth', 'zsc_eth', 'doge_eth'
        );
        foreach ($bitzSymbols as $symbol) {
            $deleteSql = " DELETE FROM `{{quotes_tick_data}}` WHERE `symbol` = :symbol ";
            $deleteParams = array( ':symbol' => $symbol );
            Yii::App()->db->createCommand($deleteSql)->query($deleteParams);
        }

        $insertSql = 'INSERT IGNORE INTO `{{quotes_tick_data}}` (`symbol`, `bid`, `ask`, `time`, `updatedDT`) VALUES ';

        $insertParams = array();
            
        $i=0;
        foreach ($bitzSymbols as $symbol) {
            if (!isset($dataToInsert->{$symbol})) {
                continue;
            }
            if ($i) {
                $insertSql .= ',';
            }

            $insertSql .= "(:symbol$i, :bid$i, :ask$i, :time$i, :updatedDT$i)";
            $insertParams[":symbol$i"] = $symbol;
            $insertParams[":bid$i"] = $dataToInsert->{$symbol}->buy;
            $insertParams[":ask$i"] = $dataToInsert->{$symbol}->sell;
            $insertParams[":time$i"] = date('H:i:s', time());
            $insertParams[":updatedDT$i"] = date("Y-m-d H:i:s", time()-60*60*1);
                
            $i++;
        }
            
        if (Yii::App()->db->createCommand($insertSql)->query($insertParams)) {
            echo "bitz tick data inserted \n";
        }
    }
    public static function updateBitfinexTickData()
    {
        $dataUrl = 'https://api.bitfinex.com/v2/tickers?symbols=';
        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'select' => " tickName ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`tickName` <> '' AND `t`.`sourceType` = 'bitfinex' ",
        ));
        if (!$quotes) {
            return false;
        }
        $i=0;
        foreach ($quotes as $quote) {
            if ($i) {
                $dataUrl .= ',';
            }
            $dataUrl .= $quote->tickName;
            if (!$i) {
                $i=1;
            }
        }
        $quotesDataStr = CommonLib::downloadFileFromWww($dataUrl);
        
        if (!$quotesDataStr) {
            echo "No data from bitfinex" . PHP_EOL;
            return false;
        }
        
        $quotesDataArr = json_decode($quotesDataStr);

        if (isset($quotesDataArr[0]) && $quotesDataArr[0] == 'error') {
            echo implode(' ', $quotesDataArr) . PHP_EOL;
            return false;
        }
        
        foreach ($quotesDataArr as $quoteData) {
            $deleteSql = " DELETE FROM `{{quotes_tick_data}}` WHERE `symbol` = :symbol ";
            $deleteParams = array( ':symbol' => $quoteData[0] );
            Yii::App()->db->createCommand($deleteSql)->query($deleteParams);
        }


        $insertSql = 'INSERT IGNORE INTO `{{quotes_tick_data}}` (`symbol`, `bid`, `ask`, `time`, `updatedDT`) VALUES ';

        $insertParams = array();
            
        $i=0;
        foreach ($quotesDataArr as $quoteData) {
            if ($i) {
                $insertSql .= ',';
            }

            $insertSql .= "(:symbol$i, :bid$i, :ask$i, :time$i, :updatedDT$i)";
            $insertParams[":symbol$i"] = $quoteData[0];
            $insertParams[":bid$i"] = $quoteData[1];
            $insertParams[":ask$i"] = $quoteData[3];
            $insertParams[":time$i"] = date('H:i:s', time() - 60*60*1);
            $insertParams[":updatedDT$i"] = date("Y-m-d H:i:s");
                
            $i++;
        }
            
        if (Yii::App()->db->createCommand($insertSql)->query($insertParams)) {
            echo "bitfinex tick data inserted \n";
        }

    }
    public static function getAllQuotes()
    {
        $filePrev = Yii::getPathOfAlias('application') . self::$prevTickDataFile;
            
        $summArr = array();
            
        $quotesSettings = QuotesSettingsModel::getInstance();
            
        $sources = explode('||', $quotesSettings->quotesTickSource);
        $prevData = array();
        $hoursCorrection = array();

        foreach ($sources as &$source) {
            $sourceArr = explode('|', $source);
            if (isset($sourceArr[1])) {
                $hoursCorrection[$sourceArr[0]] = $sourceArr[1];
                $source = $sourceArr[0];
            }
        }
        unset($source);
            
        if (!file_exists($filePrev)) {
            foreach ($sources as $source) {
                $quotesStr = trim(CommonLib::downloadFileFromWww($source));
                $quotes = explode(PHP_EOL, $quotesStr);
                foreach ($quotes as $quote) {
                    $delimerPos = strpos($quote, ';');
                    $symbol = substr($quote, 0, $delimerPos);

                    $timeStartPos = strpos($quote, ';', strpos($quote, ';', strpos($quote, ';')+1)+1)+1;
                    $timeEndPos = strpos($quote, ';', $timeStartPos);
            
                    if ($timeStartPos == $timeEndPos) {
                        $quote = substr($quote, 0, $timeStartPos) . date('H:i:s') . substr($quote, $timeEndPos);
                    }

                    if (isset($hoursCorrection[$source])) {
                        $timeVal = substr($quote, $timeStartPos, 8);
                        $quote = str_replace($timeVal, self::correctTime($timeVal, $hoursCorrection[$source]), $quote);
                    }

                    $summArr[$symbol] = $quote;
                    $prevData[$source][$symbol] = $quote;
                }
            }
            file_put_contents($filePrev, serialize($prevData));
            return $summArr;
        }
            
        $prevCompareData = unserialize(file_get_contents($filePrev));
            
        $sources = array_reverse($sources);
        foreach ($sources as $source) {
            $quotesStr = trim(CommonLib::downloadFileFromWww($source));
            $quotes = explode(PHP_EOL, $quotesStr);
            foreach ($quotes as $quote) {
                $delimerPos = strpos($quote, ';');
                $symbol = substr($quote, 0, $delimerPos);

                $timeStartPos = strpos($quote, ';', strpos($quote, ';', strpos($quote, ';')+1)+1)+1;
                $timeEndPos = strpos($quote, ';', $timeStartPos);

                if ($timeStartPos == $timeEndPos) {
                    $quote = substr($quote, 0, $timeStartPos) . date('H:i:s') . substr($quote, $timeEndPos);
                }

                if (isset($hoursCorrection[$source])) {
                    $timeVal = substr($quote, $timeStartPos, 8);
                    $quote = str_replace($timeVal, self::correctTime($timeVal, $hoursCorrection[$source]), $quote);
                }

                $prevData[$source][$symbol] = $quote;

                if (isset($summArr[$symbol])) {
                    continue;
                }
                if ($quote == $prevCompareData[$source][$symbol]) {
                    continue;
                }
                $summArr[$symbol] = $quote;
            }
        }
        file_put_contents($filePrev, serialize($prevData));
        return $summArr;
    }
    public static function correctTime($timeStr, $offset = 0)
    {
        if (!$timeStr) {
            return false;
        }
        $time = strtotime($timeStr) + 60*60*24 + $offset * 60 * 60;
        return date('H:i:s', $time);
    }
}
