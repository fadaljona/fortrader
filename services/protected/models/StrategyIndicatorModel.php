<?
	
	final class StrategyIndicatorModel extends ModelExBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			$relations = parent::relations();
			I18NModelInterface::addRelations( $this, $relations );
			return $relations;
		}
	}

?>