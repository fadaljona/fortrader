<?
	Yii::import( 'models.base.ModelBase' );

	final class UserToUserGroupModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idUser, $idUserGroup ) {
			$model = new self();
			$model->idUser = $idUser;
			$model->idUserGroup = $idUserGroup;
			return $model;
		}
	}

?>