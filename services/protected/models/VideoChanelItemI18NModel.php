<?php
Yii::import('models.base.ModelBase');

final class VideoChanelItemI18NModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return "{{video_chanel_item_i18n}}";
    }

    public static function instance($idItem, $idLanguage, $title, $url)
    {
        return self::modelFromAssoc(__CLASS__, compact('idItem', 'idLanguage', 'title', 'url'));
    }
}
