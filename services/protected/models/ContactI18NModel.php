<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContactI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{contact_i18n}}";
		}
		static function instance( $idContact, $idLanguage ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idContact', 'idLanguage' ));
		}
	}

?>