<?
	Yii::import( 'models.base.ModelBase' );

	final class ServerToContestModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idServer, $idContest ) {
			$model = self::modelFromAssoc( __CLASS__, compact( 'idServer', 'idContest' ));
			return $model;
		}
	}

?>