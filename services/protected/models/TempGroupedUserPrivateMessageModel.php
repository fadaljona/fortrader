<?
	Yii::import( 'models.base.ModelBase' );
	
	final class TempGroupedUserPrivateMessageModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "temp_grouped_user_private_messages";
		}
		function relations() {
			return Array(
				'message' => Array( self::BELONGS_TO, 'UserPrivateMessageModel', 'id' ),
			);
		}
		static function createTable( $idUser, $idLastMessage, $limit ) {
			Yii::App()->db->createCommand("
				DROP TEMPORARY TABLE IF EXISTS `temp_grouped_user_private_messages`;
				CREATE TEMPORARY TABLE `temp_grouped_user_private_messages` (
					`id` INT(10) UNSIGNED NOT NULL,
					`idWith` INT(10) UNSIGNED NOT NULL,
					`count` INT(10) UNSIGNED NOT NULL,
					PRIMARY KEY (`id`)
				)
				COLLATE='utf8_general_ci'
				ENGINE=Memory;

				INSERT INTO `temp_grouped_user_private_messages` ( `id`, `idWith`, `count` )
				SELECT 		MAX( `id` ) AS `id`, 
							IF( `idFrom` = :idUser, `idTo`, `idFrom` ) AS `idWith`, 
							COUNT( * ) AS `count`
				FROM 		`{{user_private_message}}`
				WHERE		`idFrom` = :idUser
					OR		`idTo` = :idUser
				GROUP BY 	`idWith`
				HAVING		:idLastMessage = 0 OR `id` < :idLastMessage
				ORDER BY 	`id` DESC
				LIMIT		{$limit}
			")->query(Array(
				':idUser' => $idUser,
				':idLastMessage' => (int)$idLastMessage,
			));
		}
	}

?>