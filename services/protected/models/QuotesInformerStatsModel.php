<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesInformerStatsModel extends ModelBase {
		public $count;
		public $lastDate;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function saveStats( $referer ){
			$parsedUrl = parse_url($referer);
			if( isset( $parsedUrl[host] ) ){
				$stat = new self;
				$stat->accessDate = date('Y-m-d H:i:s', time());
				$stat->domain = $parsedUrl[host];
				$stat->url = $referer;
				if( $stat->validate() ) $stat->save();
			}
		}
		
		function rules() {
			return Array(
				Array( 'accessDate', 'date', 'format'=>'yyyy-M-d H:m:s'),
				Array( 'domain, url', 'length', 'max' => 255),
			);
		}
		public function tableName(){
			return '{{quotes_informer_stats}}';
		}
	}
?>