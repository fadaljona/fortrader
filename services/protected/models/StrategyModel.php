<?
	
	final class StrategyModel extends ModelExBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function _getModelListURL( $modelClass = __CLASS__ ) {
			return parent::_getModelListURL( $modelClass );
		}
		function relations() {
			$relations = parent::relations();
			I18NModelInterface::addRelations( $this, $relations );
			BelongsToModelInterface::addRelation( $this, $relations, 'user', 'UserModel', 'idUser', Array( 'controlFields' => Array( Array( 'userLogin' => 'user_login' ))));
			BelongsToModelInterface::addRelation( $this, $relations, 'ea', 'EAModel', 'idEA' );
			ManyToManyModelInterface::addRelations( $this, $relations, 'indicators', 'StrategyIndicatorModel', 'IndicatorToStrategyModel' );
			ManyToManyModelInterface::addRelations( $this, $relations, 'instruments', 'StrategyInstrumentModel', 'InstrumentToStrategyModel' );
			return $relations;
		}
		function _getFieldVariants( $filed, $parameters = Array()) {
			switch( $filed ) {
				case 'idEA':{
					$EAs = EAModel::model()->findAll(Array(
						'select' => " id, name ",
						'order' => ' `name` ',
					));
					$EAs = CommonLib::toAssoc( $EAs, 'id', 'name' );
					$EAs = CommonLib::mergeAssocs( Array( '' => '' ), $EAs );
					return $EAs;
				}
				default: 
					return parent::_getFieldVariants( $filed, $parameters );
			}
		}
	}

?>