<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MailingModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'MailingI18NModel', 'idMailing' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'MailingI18NModel', 'idMailing', 
					'on' => '`currentLanguageI18N`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if( strlen( $obj->message ) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = MailingI18NModel::model()->findByAttributes( Array( 'idMailing' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = MailingI18NModel::instance( $this->id, $idLanguage, $obj->name, $obj->message );
					}
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		function setProgressData( $data ) {
			if( !is_object( $data )) return;
			$this->progressData = serialize( $data );
		}
		function getProgressData() {
			return strlen( $this->progressData ) ? unserialize( $this->progressData ) : (object)Array();
		}
		
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>