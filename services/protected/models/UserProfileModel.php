<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserProfileModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser' ));
		}
		function getIsTrader() {
			return strlen($this->trader_market)
					|| strlen($this->trader_tradingPlatforms)
					|| strlen($this->trader_brokers)
					|| strlen($this->trader_brokers)
					|| strlen($this->trader_financeInstruments)
					|| strlen($this->trader_tradingExperience)
					|| strlen($this->trader_analyseMethod)
					|| strlen($this->trader_tradingType)
					|| strlen($this->trader_useEA)
					|| strlen($this->trader_commercialProduct)
					|| strlen($this->trader_aboutTrading);
		}
		function getIsManager() {
			return strlen($this->manager_availability)
					|| strlen($this->manager_trustManagementType)
					|| strlen($this->manager_agreement)
					|| strlen($this->manager_managementExperience)
					|| strlen($this->manager_investmentRisk)
					|| strlen($this->manager_minimumInvestmentAmount)
					|| strlen($this->manager_additionally);
		}
		function getIsInvestor() {
			return strlen($this->investor_availability)
					|| strlen($this->investor_investmentAmount)
					|| strlen($this->investor_investmentPeriod)
					|| strlen($this->investor_expectedAnnualRateOfReturn)
					|| strlen($this->investor_maximumRiskOfInvestment)
					|| strlen($this->investor_agreement)
					|| strlen($this->investor_additionally);
		}
		function getIsProgrammer() {
			return strlen($this->programmer_availability)
					|| strlen($this->programmer_programmingExperience)
					|| strlen($this->programmer_programmingLangauge)
					|| strlen($this->programmer_typeOfPrograms)
					|| strlen($this->programmer_hourlyRate)
					|| strlen($this->programmer_minimalOrder)
					|| strlen($this->programmer_mql4Profile)
					|| strlen($this->programmer_mql5Profile)
					|| strlen($this->programmer_additionally);
		}
			
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				$this->isTrader = $this->getIsTrader();
				$this->isManager = $this->getIsManager();
				$this->isInvestor = $this->getIsInvestor();
				$this->isProgrammer = $this->getIsProgrammer();
			}
			return $result;
		}

		static function getTimezone( $prior = 'db' ) {
			if ( Yii::app()->user->isGuest ) {
				if ( Yii::app()->request->cookies['utc'] ) {
					$value = Yii::app()->request->cookies['utc']->value;
				}else{
					$value = 0;
				}
			}else{
				if( $prior == 'cookie' ){
					if ( Yii::app()->request->cookies['utc'] ) {
						$value = Yii::app()->request->cookies['utc']->value;
					}else{
						$value = 0;
					}
				}
				if( !isset($value) || $prior == 'db' ){
					$sql = "
						SELECT utc FROM ft_user_profile
						WHERE idUser = ".Yii::app()->user->id."
					";
					$connection=Yii::app()->db; 
					$command=$connection->createCommand($sql);
					$value=$command->queryScalar();
				}
			}
			return $value;
		}
	}

?>
