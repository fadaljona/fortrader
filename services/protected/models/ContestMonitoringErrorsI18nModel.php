<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestMonitoringErrorsI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'category' => Array( self::HAS_ONE, 'ContestMonitoringErrorsModel', array( 'id' => 'idError' ) ),
			);
		}
		function tableName() {
			return "{{contest_monitoring_errors_i18n}}";
		}
		static function instance( $idError, $idLanguage, $title, $description ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idError', 'idLanguage', 'title', 'description' ));
		}
	}
?>