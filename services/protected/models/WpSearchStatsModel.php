<?
	Yii::import( 'models.base.ModelBase' );
	
	final class WpSearchStatsModel extends ModelBase {
		public $count;
		public $lastDate;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function saveKey( $keyword ){
			$stat = new self;
			$stat->searchDate = date('Y-m-d H:i:s', time());
			$stat->keyword = strip_tags($keyword);
			$stat->ip = Yii::app()->request->userHostAddress;
			if( $stat->validate() ) $stat->save();
		}
		
		function rules() {
			return Array(
				Array( 'searchDate', 'date', 'format'=>'yyyy-M-d H:m:s'),
				Array( 'keyword, ip', 'length', 'max' => 255),
			);
		}
		public function tableName(){
			return '{{wp_search_stats}}';
		}
	}
?>