<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CategoryRatingI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}

		function tableName() {
			return "{{category_rating_i18n}}";
		}
		
		static function instance( $idRating, $idLanguage, $title, $metaTitle, $metaDesc, $metaKeys, $shortDesc, $fullDesc ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idRating', 'idLanguage', 'title', 'metaTitle', 'metaDesc', 'metaKeys', 'shortDesc', 'fullDesc' ));
		}

	}
?>