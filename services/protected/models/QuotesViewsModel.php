<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesViewsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function rules() {
			return Array(
				Array( 'symbolId', 'unique'),
				Array( 'symbolId', 'numerical', 'integerOnly'=>true , 'min' => 1),
				Array( 'views', 'numerical', 'integerOnly'=>true , 'min' => 0),
			);
		}
		static function instance( $symbolId, $views ) {
			return self::modelFromAssoc( __CLASS__, compact( 'symbolId', 'views' ));
		}
		static function saveViews( $symbolId ){
			$model = self::model()->findByAttributes( Array( 'symbolId' => $symbolId ));
			if( !$model ) $model = self::instance( $symbolId, 0 );
			$model->views += 1 ;
			if( $model->validate() ) $model->save();
		}
		
	}

?>