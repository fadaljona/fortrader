<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CalendarEventI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{calendar_event_i18n}}";
		}
		function relations() {
			return Array(
				'linkedCountry' => Array( self::BELONGS_TO, 'CountryModel', 'idCountry' ),
			);
		}
		
		static function instance( $idEvent, $idLanguage, $indicator ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idEvent', 'idLanguage', 'indicator' ));
		}
		static function filterCounty( $country, $idLanguage ) {
			switch( $idLanguage ) {
				case 0:{
					$country = strtr( $country, Array(
						'Czech' => 'Czech Republic',
						'Great Britain' => 'United Kingdom',
						'Nederlands' => 'Netherlands',
						'USA' => 'United States',
					));
					break;
				}
				case 1:{
					$country = strtr( $country, Array(
						'Британия' => 'Великобритания',
						'Нидерланды' => 'Нидерланды (Голландия)',
					));
					break;
				}
			}
			return $country;
		}
		static function detIDCounty( $country, $idLanguage ) {
			if( !strlen( $country )) return null;
			$country = self::filterCounty( $country, $idLanguage );
			
			if( $idLanguage == 0 ) {
				$county = CountryModel::model()->findByAttributes( Array(
					'name' => $country,
				));
				if( !$county ) return null;
				
				return $county->id;
			}
			else{
				$query = "
					SELECT		`{{message_i18n}}`.`idMessage`
					FROM		`{{message_i18n}}`
					WHERE		`{{message_i18n}}`.`value` = :country
					LIMIT		1
				";
				$idMessage = Yii::App()->db->createCommand( $query )->queryScalar( Array(
					':country' => $country,
				));
				if( !$idMessage ) return null;
				
				$message = MessageModel::model()->findByPk( $idMessage );
				if( !$message ) return null;
				
				$county = CountryModel::model()->findByAttributes( Array(
					'name' => $message->key,
				));
				if( !$county ) return null;
				
				return $county->id;
			}
		}
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				$this->idCountry = self::detIDCounty( $this->country, $this->idLanguage );
			}
			return $result;
		}
	}

?>