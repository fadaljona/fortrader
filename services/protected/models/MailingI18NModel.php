<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MailingI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{mailing_i18n}}";
		}
		static function instance( $idMailing, $idLanguage, $name, $message ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idMailing', 'idLanguage', 'name', 'message' ));
		}
	}

?>