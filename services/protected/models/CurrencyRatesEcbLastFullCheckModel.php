<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CurrencyRatesEcbLastFullCheckModel extends ModelBase {

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function getAllCount(){
			$countSql = " SELECT COUNT(`idCurrency`) FROM `{{currency_rates_ecb_last_full_check}}` ";
			return Yii::app()->db->createCommand( $countSql  )->queryScalar();
		}
	
	}

?>