<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CurrencyRatesHistoryEcbModel extends ModelBase {
		
		const cacheTime = 5; //5mins
		const dataForEcbConverterPeriodCacheKey = 'dataForEcbConverterPeriodCacheKey';
		const dataForEcbPeriodCacheKey = 'dataForEcbPeriodCacheKey';

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		function relations() {
			return Array(
				'currency' => array( self::HAS_ONE, 'CurrencyRatesModel', array( 'id' => 'idCurrency' ) ),	
			);
		}
		public function getId(){
			return $this->idCurrency;
		}
		public function getDiffValue(){
			$model = self::model()->find(array(
				'select' => array( 'value' ),
				'condition' => " `t`.`idCurrency` = :idCurrency AND `t`.`date` < :date ",
				'order' => " `t`.`date` DESC ",
				'params' => array( ':idCurrency' => $this->idCurrency, ':date' => $this->date )
			));
			return $this->value - $model->value;
		}
		
		static function getDataForPeriod( $period, $idCurrency ){
			$cachKey = self::dataForEcbPeriodCacheKey . '.period.' . $period . '.currency.' . $idCurrency;
			if( Yii::app()->cache->get( $cachKey ) ) return Yii::app()->cache->get( $cachKey );
			$models = self::model()->findAll(array(
				'condition' => " `t`.`idCurrency` = :id AND `t`.`date` >= :date ",
				'order' => " `t`.`date` DESC ",
				'params' => array( ':id' => $idCurrency, ':date' => date('Y-m-d', time() - $period * 60*60*24 ) ),
			));
			
			$outData = '';
			foreach( $models as $model ){
				$value = $model->value;
				$outData = "{$model->date};{$value}\n" . $outData;
			}
			$outData = trim($outData) ;
			Yii::app()->cache->set( $cachKey, $outData, self::cacheTime * 60 );
			return $outData;
		}
		
		static function getDataForConverterChart( $period, $modelId ){

			$model = CurrencyRatesConvertModel::model()->findByPk($modelId);
			if( !$model ) return false;
			
			$idCurrencyFrom = $model->idCurrencyFrom;
			$idCurrencyTo = $model->idCurrencyTo;
			$value = $model->value;
			
			$cachKey = self::dataForEcbConverterPeriodCacheKey . '.period.' . $period . '.idCurrencyFrom.' . $idCurrencyFrom . '.idCurrencyTo.' . $idCurrencyTo . '.value.' . $value;
			if( Yii::app()->cache->get( $cachKey ) ) return Yii::app()->cache->get( $cachKey );
			
			$modelFrom = $model->rateFrom;
			$modelTo = $model->rateTo;
			
			if( !$modelFrom || !$modelTo ) return false;
			
			if( !$modelFrom->defaultCurrencyEcb && !$modelTo->defaultCurrencyEcb ){
				$rows =  Yii::App()->db->createCommand(" 
					SELECT `from`.`date` AS `commonDate`, `from`.`value` AS `fromValue`, `to`.`value` AS `toValue`
					FROM `{{currency_rates_history_ecb}}` `from`
					LEFT JOIN `{{currency_rates_history_ecb}}` `to` ON `to`.`date` = `from`.`date` AND `to`.`idCurrency` = :idCurrencyTo
					WHERE `from`.`idCurrency` = :idCurrencyFrom AND `to`.`idCurrency` IS NOT NULL AND `from`.`date` >= :date
					ORDER BY `from`.`date` DESC
				")->queryAll(true, array( ':idCurrencyFrom' => $idCurrencyFrom, ':idCurrencyTo' => $idCurrencyTo, ':date' => date('Y-m-d', time() - $period * 60*60*24 ) ) );

				
				$outData = '';
				foreach( $rows as $row ){
					$val = $value * $row['toValue'] / $row['fromValue'];
					$outData = "{$row['commonDate']};{$val}\n" . $outData;
				}
				
			}else{
				$idCur = $modelFrom->defaultCurrencyEcb ? $idCurrencyTo : $idCurrencyFrom;
				
				$rows =  Yii::App()->db->createCommand(" 
					SELECT `from`.`date`, `from`.`value`
					FROM `{{currency_rates_history_ecb}}` `from`
					WHERE `from`.`idCurrency` = :idCurrencyFrom AND `from`.`date` >= :date
					ORDER BY `from`.`date` DESC
				")->queryAll(true, array( ':idCurrencyFrom' => $idCur, ':date' => date('Y-m-d', time() - $period * 60*60*24 ) ) );
				
				if( $modelFrom->defaultCurrencyEcb ){
					$outData = '';
					foreach( $rows as $row ){
						$val = $value * $row['value'] ;
						$outData = "{$row['date']};{$val}\n" . $outData;
					}
					
				}else{
					$outData = '';
					foreach( $rows as $row ){
						$val = $value / $row['value']  ;
						$outData = "{$row['date']};{$val}\n" . $outData;
					}
				}
			}
			
			$outData = trim($outData) ;
			Yii::app()->cache->set( $cachKey, $outData, self::cacheTime * 60 );
			return $outData;
		}
	
	}

?>