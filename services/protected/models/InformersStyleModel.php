<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InformersStyleModel extends ModelBase {
		const PATHStylesDir = 'protected/components/widgets/views/informer/styles';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'InformersStyleI18nModel', 'idStyle', 'alias' => 'i18nsInformersStyle' ),
				'categoryTypes' => Array( self::HAS_MANY, 'InformersStyleToTypeModel', 'idStyle' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'InformersStyleI18nModel', 'idStyle', 
					'alias' => 'cLI18NInformersStyleModel',
					'on' => '`cLI18NInformersStyleModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				
			);
		}

		static function getAdminListDp( $filterModel = false ){
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`id` ASC ',
				),
			));
			return $DP;
		}	

		static function getDefault(){
			return self::model()->findAll(array(
				'condition' => ' `t`.`defaultStyle` = 1 ',
			));
		}
		static function getAllAvailiableColorsTranslates(){
			$models = self::model()->findAll();
			$settingsArray = array();
			foreach( $models as $model ){
				if( $model->colors ) $settingsArray = array_merge($settingsArray, $model->colors );
			}
			$colors = array();
			
			foreach( $settingsArray as $colorName => $val ){
				if( strpos( $colorName, 'Color' ) !== false ) $colors[] = $colorName;
			}
			
			$translates = array();
			foreach( Yii::app()->params['langs'] as $lang => $settings ){
				foreach( $colors as $color ){
					$translates[$lang][$color] = Yii::app()->messages->translate('informerColors', $color, $settings['langAlias']);
				}
			}
			
			return $translates;
		}
		
		static function getViewFiles(){
			$cats = scandir( self::PATHStylesDir );
			
			$outArr = array('0' => '--' . Yii::t('*', 'Select style'));
			
			foreach( $cats as $cat ){
				if( $cat == '.' || $cat == '..' ) continue;
				$styles = scandir( self::PATHStylesDir . '/' . $cat );
				
				foreach( $styles as $style ){
					if( $style == '.' || $style == '..' ) continue;
					$outArr[$cat][$style] = $style;
				}
			}
			return $outArr;
		}
		function getViewFilesDir(){
			$type = $this->categoryTypes[0]->type;
			if( $type == 'converter' ) return 'converter';
			if( $type == 'quotes' ) return 'quotes,currencies,cbrfMetals';
			if( $type == 'currencies' ) return 'quotes,currencies,cbrfMetals';
			if( $type == 'cbrfMetals' ) return 'quotes,currencies,cbrfMetals';
			if( $type == 'monitoring' ) return 'monitoring';
			if( $type == 'monitoringForForum' ) return 'monitoringForForum';
			if( $type == 'contestForForum' ) return 'contestForForum';
			if( $type == 'news' ) return 'news';
			if( $type == 'calendar' ) return 'calendar';
		}
		public function setCategoryTypes( $categoryTypes ){
			$this->deleteCategoryTypes();
			$this->addCategoryTypes($categoryTypes);
		}
		public function deleteCategoryTypes(){
			foreach( $this->categoryTypes as $categoryType ) {
				$categoryType->delete();
			}
		}
		public function addCategoryTypes($types){
			foreach( $types as $type ){
				$model = InformersStyleToTypeModel::instance( $this->id, $type );
				if( $model->validate() ) $model->save();
			}
		}
		public function getCategoryTypes(){
			$outArray = array();
			foreach( $this->categoryTypes as $categoryType ) {
				$outArray[] = $categoryType->type;
			}
			return $outArray;
		}
		public function getStrCategoryTypes(){
			$outArray = array();
			foreach( $this->categoryTypes as $categoryType ) {
				$outArray[] = $categoryType->type;
			}
			return implode(',', $outArray);
		}
		
		
		public function setSettings( $settings ){
			$rows = explode("\n", $settings);
			$arr = array();
			foreach( $rows as $row ){
				$sets = explode("=", trim($row));
				$arr[$sets[0]] = $sets[1];
			}
			$this->settings = serialize($arr);
		}
		public function getColors(){
			return unserialize($this->settings);
		}
		public function getCustomSettings(){
			$settings = unserialize($this->settings);
			foreach( $settings as $key => $value ){
				if( strpos( $key, 'Color' ) ) unset( $settings[$key] );
			}
			return $settings;
		}
		public function getLIstSettings(){
			$s = unserialize($this->settings);
			$outStr = '';
			foreach( $s as $key => $val ){
				$outStr .= "$key=$val\n";
			}
			return $outStr;
		}


		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->title )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->title )) return $i18n;
			}
		}
		function getTitleByLang( $langId ) { 
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == $langId ) {
					if( strlen( $i18n->title )) return $i18n->title;
					return false;
				}
			}
		}
		function getTitle() { $i18n = $this->getI18N(); return $i18n ? $i18n->title : '';}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
		
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->title )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = InformersStyleI18nModel::model()->findByAttributes( Array( 'idStyle' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = InformersStyleI18nModel::instance( $this->id, $idLanguage, $obj->title );
					}else{
						$i18n->title = $obj->title;
					}
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			
		}
	}

?>