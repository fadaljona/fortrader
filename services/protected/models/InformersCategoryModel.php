<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InformersCategoryModel extends ModelBase {
		const PATHUploadDir = 'uploads/informers';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'InformersCategoryI18nModel', 'idCategory', 'alias' => 'i18nsInformersCategory' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'InformersCategoryI18nModel', 'idCategory', 
					'alias' => 'cLI18NInformersCategoryModel',
					'on' => '`cLI18NInformersCategoryModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				
			);
		}

		static function getAdminListDp( $filterModel = false ){
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`order` DESC, `t`.`id` DESC ',
				),
			));
			return $DP;
		}
		static function findBySlug( $slug ){
			return self::model()->find(array(
				'condition' => " `slug` = :slug ",
				'params' => array( ':slug' => $slug ),
			));
		}
		public function getAvailableLangs(){
			$langsArr = array();
			foreach( $this->i18ns as $i18n ){
				if( $i18n->title ){
					$langsArr[] = LanguageModel::getLangFromLangsFileByID( $i18n->idLanguage );
				}
			}
			return $langsArr;
		}
		static function findBySlugWithLang( $slug ){
			return self::model()->find(array(
				'with' => Array( 'currentLanguageI18N' ),
				'condition' => " `slug` = :slug AND `cLI18NInformersCategoryModel`.`title` IS NOT NULL AND `cLI18NInformersCategoryModel`.`title` <> '' ",
				'params' => array( ':slug' => $slug ),
			));
		}
		
		
		static function getAvailableTypes(){
			return Array( 'quotes','currencies','converter', 'cbrfMetals', 'monitoring', 'monitoringForForum', 'contestForForum', 'news', 'calendar' );
		}
		static function getAvailableTypesWithTranslate(){
			return Array(
				'currencies' => Yii::t('*', 'currencies'),
				'quotes' => Yii::t('*', 'quotes'),
				'converter' => Yii::t('*', 'converter'),
				'cbrfMetals' => Yii::t('*', 'CBRF Metals'),
				'monitoring' => Yii::t('*', 'monitoring'),
				'monitoringForForum' => Yii::t('*', 'monitoringForForum'),
				'contestForForum' => Yii::t('*', 'contestForForum'),
				'news' => Yii::t('*', 'News'),
				'calendar' => Yii::t('*', 'Calendar'),
			);
		}
		static function getTypesForStyleForm(){
			/*return Array(
				'currencies' => Yii::t('*', 'currencies'),
				'quotes' => Yii::t('*', 'quotes'),
				'quotes,currencies,cbrfMetals' => Yii::t('*', 'quotes') . ' + ' . Yii::t('*', 'currencies') . ' + ' . Yii::t('*', 'CBRF Metals'),
				'converter' => Yii::t('*', 'converter'),
				'cbrfMetals' => Yii::t('*', 'CBRF Metals'),
			);*/
			return Array(
				'currencies' => Yii::t('*', 'currencies'),
				'quotes' => Yii::t('*', 'quotes'),
				'cbrfMetals' => Yii::t('*', 'CBRF Metals'),
				'converter' => Yii::t('*', 'converter'),
				'monitoring' => Yii::t('*', 'monitoring'),
				'monitoringForForum' => Yii::t('*', 'monitoringForForum'),
				'contestForForum' => Yii::t('*', 'contestForForum'),
				'news' => Yii::t('*', 'News'),
				'calendar' => Yii::t('*', 'Calendar'),
			);
		}
		static function getSettingForWpPlugin(){
			
			$outArray = array();
			$informerSettings = InformersSettingsModel::getModel();
			$informerColorsTranslates = InformersStyleModel::getAllAvailiableColorsTranslates();
			
			foreach( Yii::app()->params['langs'] as $lang => $settings ){
				$outArray['langs'][$lang] = array(
					'baseUrl' => $settings['baseUrl'],
				);
				
				$langId = LanguageModel::getByAlias( $settings['langAlias'] )->id;
				
/*				$cat = self::model()->find(array(
					'with' => array( 'i18ns' ),
					'condition' => " `t`.`id` = 8 AND `i18nsInformersCategory`.`idLanguage` = :lang AND `i18nsInformersCategory`.`title` <> '' AND `i18nsInformersCategory`.`title` IS NOT NULL ",
					'params' => array( ':lang' => $langId )
				));
				if( !$cat ) continue;
*/
				
				$cats = self::model()->findAll(array(
					'with' => array( 'i18ns' ),
					'condition' => " `i18nsInformersCategory`.`idLanguage` = :lang AND `i18nsInformersCategory`.`title` <> '' AND `i18nsInformersCategory`.`title` IS NOT NULL AND `t`.`hidden` = 0 ",
					'order' => ' `t`.`order` DESC ',
					'params' => array( ':lang' => $langId )
				));
				foreach( $cats as $cat ){
					$outArray['langs'][$lang]['cats'][$cat->id] = array(
						'title' => $cat->getTitleByLang($langId),
					);
					
					if( !isset( $outArray['catsSettings'][$cat->id]['langs'][$lang] ) ){
						if( $cat->type == 'quotes' || $cat->type == 'currencies'  ){
							
							$outArray['catsSettings'][$cat->id]['langs'][$lang]['items'] = $cat->getItemsByLang($langId);
							$outArray['catsSettings'][$cat->id]['langs'][$lang]['columns'] = $cat->getColumnsByLang($langId);
						}
						if( $cat->type == 'cbrfMetals' ){
							$outArray['catsSettings'][$cat->id]['langs'][$lang]['items'] = $cat->getItemsByLang($langId);
						}
						if( $cat->type == 'currencies' ){
							$outArray['catsSettings'][$cat->id]['langs'][$lang]['toCur'] = CurrencyRatesModel::getConverterFromToSettingsByLang($langId, 'to', $cat->idInType);
						}
						if( $cat->type == 'converter' ){
							$outArray['catsSettings'][$cat->id]['langs'][$lang]['from'] = CurrencyRatesModel::getConverterFromToSettingsByLang($langId, 'from');
							$outArray['catsSettings'][$cat->id]['langs'][$lang]['to'] = CurrencyRatesModel::getConverterFromToSettingsByLang($langId, 'to');
						}
					}
					if( !isset( $outArray['catsSettings'][$cat->id]['mult'] ) ){
						if( $cat->type == 'quotes' || $cat->type == 'currencies' || $cat->type == 'cbrfMetals' ){
							$outArray['catsSettings'][$cat->id]['mult'] = array(
								'type' => 'slider',
								'value' => 1,
								'minValue' => 0.6,
								'maxValue' => 1.5,
								'step' => 0.01
							);
						}
					}
					if( $cat->type == 'currencies' ){
						if( !isset( $outArray['catsSettings'][$cat->id]['codes'] ) ){
							$outArray['catsSettings'][$cat->id]['codes'] = array(
								'type' => 'singleCheckbox',
								'value' => 1,
								'setted' => 1
							);
						}
					}
					if( $cat->type == 'quotes' ){
						if( !isset( $outArray['catsSettings'][$cat->id]['disableRealTime'] ) ){
							$outArray['catsSettings'][$cat->id]['disableRealTime'] = array(
								'type' => 'singleCheckbox',
								'value' => 1,
								'setted' => 0
							);
						}
					}
					if( !isset( $outArray['catsSettings'][$cat->id]['showGetBtn'] ) ){
						$outArray['catsSettings'][$cat->id]['showGetBtn'] = array(
							'type' => 'singleCheckbox',
							'value' => 1,
							'setted' => 0
						);
					}
					if( $cat->type == 'converter' && !isset( $outArray['catsSettings'][$cat->id]['amount'] ) ){
						$outArray['catsSettings'][$cat->id]['amount'] = array(
							'type' => 'text',
							'value' => $informerSettings->convertFromVal,
						);
					}
					
					
					
					
					
					$styles = InformersStyleModel::model()->findAll(array(
						'with' => array( 'i18ns', 'categoryTypes' ),
						'condition' => ' `i18nsInformersStyle`.`idLanguage` = :lang AND `i18nsInformersStyle`.`title` IS NOT NULL AND `i18nsInformersStyle`.`title` <> "" AND `categoryTypes`.`type` = :type ',
						'order' => ' `t`.`defaultStyle` DESC, `t`.`id` ASC ',
						'params' => array( ':type' => $cat->type, ':lang' => $langId ),
					));
					
					foreach( $styles as $style ){
						$outArray['langs'][$lang]['cats'][$cat->id]['styles'][] = $style->id;
						
						if( !isset( $outArray[$lang]['styles'][$style->id] ) ){
							$outArray['langs'][$lang]['stylesTitles'][$style->id] = $style->getTitleByLang($langId);
						}
						
						if( !isset( $outArray['stylesSettings'][$style->id] ) ){
							if( $cat->type == 'quotes' || $cat->type == 'currencies' ){
								if( $style->hasDiffValue ){
									$outArray['stylesSettings'][$style->id]['hideDiff'] = array(
										'type' => 'singleCheckbox',
										'value' => 1,
										'setted' => 0
									);
								}
								if( $style->oneColumn ){
									$outArray['stylesSettings'][$style->id]['actions'][] = array(
										'type' => 'hide',
										'target' => 'columns',
									);
								}else{
									$outArray['stylesSettings'][$style->id]['actions'][] = array(
										'type' => 'show',
										'target' => 'columns',
									);
								}
							}
							if( $style->colors ){
								$outArray['stylesSettings'][$style->id]['colors'] = array(
									'type' => 'colorPicker'
								);
								foreach( $style->colors as $colorName => $colorVal ){
									if( strpos($colorName, 'Color') === false ) continue;
									$outArray['stylesSettings'][$style->id]['colors']['options'][] = array(
										'name' => $colorName,
										'value' => $colorVal
									);
								}
								
							}
							$outArray['stylesSettings'][$style->id]['w'] = array(
								'type' => 'text',
								'value' => $style->width,
							);
						}
						
					}
					if( !isset( $outArray['langs'][$lang]['stylesColorsTitles'] ) ){
						$outArray['langs'][$lang]['stylesColorsTitles'] = $informerColorsTranslates[$lang];
					}
					
				}
			}
			
			return $outArray;
			

		}
		static function getIdInTypes(){
			return array(
				'currencies' => array(
					0 => Yii::t('*', 'Cbr'),
					1 => Yii::t('*', 'Ecb'),
				),
				'quotes' => QuotesCategoryModel::getCatsIdsWithNames(),
				'converter' => array(
					0 => Yii::t('*', 'All Currencies'),
				),
				'cbrfMetals' => array(
					0 => Yii::t('*', 'All metals'),
				),
				'monitoring' => array(
					0 => Yii::t('*', 'monitoring'),
				),
				'monitoringForForum' => array(
					0 => Yii::t('*', 'monitoringForForum'),
				),
				'contestForForum' => array(
					0 => Yii::t('*', 'contestForForum'),
				),
				'news' => array(
					0 => Yii::t('*', 'news'),
				),
				'calendar' => array(
					0 => Yii::t('*', 'Calendar'),
				),
			);
		}
		public function getColumnsByLang($langId){
			
			if( $this->type == 'currencies' ){
				$langAlias = LanguageModel::getByID($langId)->alias;
				if( $this->idInType == 0 ){
					return array(
						'type' => 'multipleCheckbox',
						'options' => array(
							array(
								'value' => 'todayCourse',
								'setted' => 1,
								'label' => Yii::app()->messages->translate('informerColumnNames', 'todayCourse', $langAlias)
							),
							array(
								'value' => 'tomorrowCourse',
								'setted' => 0,
								'label' => Yii::app()->messages->translate('informerColumnNames', 'tomorrowCourse', $langAlias)
							)
						)
					);
				}elseif( $this->idInType == 1 ){
					return array(
						'type' => 'select',
						'options' => array(
							array(
								'value' => 'direct',
								'setted' => 1,
								'label' => Yii::app()->messages->translate('informerColumnNames', 'direct', $langAlias)
							),
							array(
								'value' => 'reverse',
								'setted' => 0,
								'label' => Yii::app()->messages->translate('informerColumnNames', 'reverse', $langAlias)
							)
						)
					);
				}
				
			}elseif( $this->type == 'quotes' ){
				$langAlias = LanguageModel::getByID($langId)->alias;
				return array(
					'type' => 'multipleCheckbox',
					'options' => array(
						array(
							'value' => 'bid',
							'setted' => 1,
							'label' => Yii::app()->messages->translate('informerColumnNames', 'bid', $langAlias)
						),
						array(
							'value' => 'ask',
							'setted' => 0,
							'label' => Yii::app()->messages->translate('informerColumnNames', 'ask', $langAlias)
						),
						array(
							'value' => 'openQuote',
							'setted' => 0,
							'label' => Yii::app()->messages->translate('informerColumnNames', 'openQuote', $langAlias)
						),
						array(
							'value' => 'highQuote',
							'setted' => 0,
							'label' => Yii::app()->messages->translate('informerColumnNames', 'highQuote', $langAlias)
						),
						array(
							'value' => 'lowQuote',
							'setted' => 0,
							'label' => Yii::app()->messages->translate('informerColumnNames', 'lowQuote', $langAlias)
						),
						array(
							'value' => 'chg',
							'setted' => 0,
							'label' => Yii::app()->messages->translate('informerColumnNames', 'chg', $langAlias)
						),
						array(
							'value' => 'chgPer',
							'setted' => 0,
							'label' => Yii::app()->messages->translate('informerColumnNames', 'chgPer', $langAlias)
						),
						array(
							'value' => 'time',
							'setted' => 0,
							'label' => Yii::app()->messages->translate('informerColumnNames', 'time', $langAlias)
						)
					)
				);
			}elseif( $this->type == 'cbrfMetals' ){
				return array(
						'type' => 'select',
						'options' => array(
							array(
								'value' => 'course',
								'setted' => 1,
								'label' => Yii::app()->messages->translate('informerColumnNames', 'course', $langAlias)
							)
						)
					);
			}
			return false;
		}
		public function getColumns(){
			if( $this->type == 'currencies' && $this->idInType == 0 ){
				return array(
					'todayCourse',
					'tomorrowCourse',
				);
			}elseif( $this->type == 'currencies' && $this->idInType == 1 ){
				return array(
					'direct',
					'reverse',
				);
			}elseif( $this->type == 'quotes' ){
				return array(
					'bid',
					'ask',
					'openQuote',
					'highQuote',
					'lowQuote',
					'chg',
					'chgPer',
					'time',
				);
			}elseif( $this->type == 'cbrfMetals' ){
				return array(
					'metalCourse',
				);
			}
			return false;
		}
		public function getLinkForHeader(){
			if( $this->type == 'currencies' && $this->idInType == 0 ){
				return CurrencyRatesModel::getLinkForInformerHeader();
			}elseif( $this->type == 'currencies' && $this->idInType == 1 ){
				return Yii::app()->createAbsoluteUrl('currencyRates/ecbIndex');
			}elseif( $this->type == 'converter' ){
				return Yii::app()->createAbsoluteUrl('currencyRates/converterPage');
			}elseif( $this->type == 'quotes' ){
				return QuotesCategoryModel::getLinkForInformerHeader($this->idInType);
			}
			return false;
		}
		public function getColumnsByGet( $columns, $style ){

			if( $style->oneColumn || !isset( $columns ) || !$columns ) return array($this->columns[0]);
			$outArr = array();
			foreach( explode(',', $columns) as $column ){
				if( in_array( $column, $this->columns ) ) $outArr[] = $column;
			}
			if( !count($outArr) ) return false;
			if( count($outArr) > 1 && $this->type == 'currencies' && $this->idInType == 1 ){
				$outArr2 = array();
				$outArr2[] = $outArr[0];
				unset($outArr);
				$outArr = array();
				$outArr = $outArr2;
			}
			return $outArr;
			
		}
		public function getItems(){
			if( $this->type == 'currencies' && $this->idInType == 0 ){
				return CurrencyRatesModel::getItemsForInformer();
			}elseif( $this->type == 'currencies' && $this->idInType == 1 ){
				return CurrencyRatesModel::getItemsForEcbInformer();
			}elseif( $this->type == 'quotes' ){
				return QuotesSymbolsModel::getItemsForInformer($this->idInType);
			}elseif( $this->type == 'cbrfMetals' ){
				return MetalRatesModel::getItemsForInformer();
			}elseif( $this->type == 'news' ){
				return $this->getWpCatsForNews();
			}
			return false;
		}
		public function getWpCatsForNews(){
			$cats = unserialize($this->rawSettings);
			if( $cats ){
				$langKey = CommonLib::getCurrentLangKey();
				if( !$langKey ) return false;
				$sql = " 
					SELECT terms.term_id, terms.name FROM wp_terms terms 
					LEFT JOIN wp_term_relationships relationships ON relationships.object_id = terms.term_id 
					LEFT JOIN wp_term_taxonomy taxonomy ON taxonomy.term_taxonomy_id = relationships.term_taxonomy_id AND taxonomy.taxonomy = 'term_language' 
					LEFT JOIN wp_terms termsL ON termsL.term_id = taxonomy.term_id AND termsL.slug = 'pll_" . $langKey . "'
					WHERE terms.term_id IN (" . implode(',',$cats) . ") AND taxonomy.term_taxonomy_id IS NOT NULL AND termsL.slug IS NOT NULL
				";
				$catRows =  Yii::App()->db->createCommand($sql)->queryAll();
				
				$outArr = array();
				if( $catRows ){
					foreach( $catRows as $cat ){
						$outArr[$cat['term_id']] = array(
							'toInformer' => $cat['name'],
							'name' => $cat['name']
						);
					}
				}
				return $outArr;
			}
			return false;
		}
		public function getItemsByLang( $langId ){
			if( $this->type == 'currencies' ){
				return CurrencyRatesModel::getItemsForInformerByLang($langId, $this->idInType);
			}elseif( $this->type == 'quotes' ){
				return QuotesSymbolsModel::getItemsForInformerByLang($this->idInType, $langId);
			}elseif( $this->type == 'cbrfMetals' ){
				return MetalRatesModel::getItemsForInformerByLang($langId);
			}
			return false;
		}
		public function getItemsByIds( $ids ){
			if( $this->type == 'currencies' && $this->idInType == 0 ){
				return CurrencyRatesModel::getItemsForInformerByIds( $ids );
			}elseif( $this->type == 'currencies' && $this->idInType == 1 ){
				return CurrencyRatesModel::getItemsForEcbInformerByIds( $ids );
			}elseif( $this->type == 'quotes' ){
				return QuotesSymbolsModel::getItemsForInformerByIds($this->idInType, $ids);
			}elseif( $this->type == 'cbrfMetals' ){
				return MetalRatesModel::getItemsForInformerByIds($ids);
			}elseif( $this->type == 'news' ){
				CommonLib::loadWp();
				return get_posts(array(
					'category' => $ids,
					'numberposts' => 10,
					'orderby' => 'date',
					'order' => 'DESC',
					'post_type' => 'post',
				));
			}
			return false;
		}
		public function getColumnName( $column ){
            if( !$column ) return false;

            if (isset($_GET['texts']) && $_GET['texts']) {
                $texts = str_replace('\"', '"', $_GET['texts']);
                $texts = json_decode($texts);
                if ($texts && isset($texts->{$column}) && $texts->{$column}) {
                    $p = new CHtmlPurifier();
                    $p->options = array('URI.AllowedSchemes'=>array(
                    'http' => true,
                    'https' => true,
                    ));
                    return $p->purify($texts->{$column});
                }
            }

			if( $column == 'todayCourse' && $this->type == 'currencies' && $this->idInType == 0 ){
				$toModel = CurrencyRatesModel::getToCourseInformerModel();
				return $toModel->code;
			}
			return Yii::t('informerColumnNames', $column);
		}
		
		
		
		
		
		
		
		function getPathOgImage() {
			return strlen($this->ogImage) ? self::PATHUploadDir."/{$this->ogImage}" : '';
		}
		function getSrcOgImage() {
			return $this->pathOgImage ? Yii::app()->baseUrl."/{$this->pathOgImage}" : '';
		}
		function getAbsoluteSrcOgImage() {
			return $this->pathOgImage ? Yii::app()->createAbsoluteUrl($this->pathOgImage) : '';
		}
		function uploadOgImage( $uploadedFile ) {
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );

			$this->setOgImage( $fileFullName );
		}
		function setOgImage( $name ) {
			$this->deleteOgImage();
			$this->ogImage = $name;
		}
		function deleteOgImage() {
			if( strlen( $this->ogImage )) {
				if( is_file( $this->pathOgImage ) and !@unlink( $this->pathOgImage )) throw new Exception( "Can't delete {$this->pathOgImage}" );
				$this->ogImage = null;
			}
		}
		
		function getAbsoluteSingleURL() {
			return Yii::App()->createAbsoluteURL( 'informers/single', Array( 'slug' => $this->slug ));
		}
		function getSingleURL() {
			return Yii::App()->createURL( 'informers/single', Array( 'slug' => $this->slug ));
		}

		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->title )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->title )) return $i18n;
			}
		}
		function getTitleByLang( $langId ) { 
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == $langId ) {
					if( strlen( $i18n->title )) return $i18n->title;
					return false;
				}
			}
		}
		
		function getTitle() { $i18n = $this->getI18N(); return $i18n ? $i18n->title : '';}
		function getDescription() { $i18n = $this->getI18N(); return $i18n ? $i18n->description : '';}
		function getImgSrc() { $i18n = $this->getI18N(); return $i18n ? $i18n->srcImage : ''; }
		
		function getPageTitle() { $i18n = $this->getI18N(); return $i18n ? $i18n->pageTitle : ''; }
		function getMetaTitle() { $i18n = $this->getI18N(); return $i18n ? $i18n->metaTitle : ''; }
		function getMetaDesc() { $i18n = $this->getI18N(); return $i18n ? $i18n->metaDesc : ''; }
		function getMetaKeys() { $i18n = $this->getI18N(); return $i18n ? $i18n->metaKeys : ''; }
		function getShortDesc() { $i18n = $this->getI18N(); return $i18n ? $i18n->shortDesc : ''; }
		function getFullDesc() { $i18n = $this->getI18N(); return $i18n ? $i18n->fullDesc : ''; }
		function getInformerTitle() {
			if(isset($_GET['title']) && $_GET['title'] != 'undefined'){
				$p = new CHtmlPurifier();
				$p->options = array('URI.AllowedSchemes'=>array(
				  'http' => true,
				  'https' => true,
				));
				return $p->purify($_GET['title']);
			}

			$i18n = $this->getI18N(); 
			if( !$i18n ) return '';
			if( !$i18n->informerTitle ) return $i18n->title;
			return $i18n->informerTitle;
		}
		function getToolTitle(){
            if (isset($_GET['texts']) && $_GET['texts']) {
                $texts = str_replace('\"', '"', $_GET['texts']);
                $texts = json_decode($texts);
                if ($texts && isset($texts->toolTitle)) {
                    $p = new CHtmlPurifier();
                    $p->options = array('URI.AllowedSchemes'=>array(
                    'http' => true,
                    'https' => true,
                    ));
                    return $p->purify($texts->toolTitle);
                }
            }
            
			$i18n = $this->getI18N();
			if( $i18n && $i18n->toolTitle ) return $i18n->toolTitle;
			return Yii::t('informerColumnNames', 'Symbol');
		}
		
		
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
		
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {

					$i18n = InformersCategoryI18nModel::model()->findByAttributes( Array( 'idCategory' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = InformersCategoryI18nModel::instance( $this->id, $idLanguage, $obj->title, $obj->description, $obj->pageTitle, $obj->metaTitle, $obj->metaDesc, $obj->metaKeys, $obj->shortDesc, $obj->fullDesc, $obj->informerTitle, $obj->toolTitle );
					}else{
						$i18n->title = $obj->title;
						$i18n->description = $obj->description;
						
						$i18n->pageTitle = $obj->pageTitle;
						$i18n->metaTitle = $obj->metaTitle;
						$i18n->metaDesc = $obj->metaDesc;
						$i18n->metaKeys = $obj->metaKeys;
						$i18n->shortDesc = $obj->shortDesc;
						$i18n->fullDesc = $obj->fullDesc;
						$i18n->informerTitle = $obj->informerTitle;
						$i18n->toolTitle = $obj->toolTitle;
						
					}
					if( $obj->nameImages ) $i18n->uploadImage( $obj->nameImages );
					$i18n->save();
				
			}
		}
		function setI18Ns( $i18ns ) {
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			
		}
	}

?>