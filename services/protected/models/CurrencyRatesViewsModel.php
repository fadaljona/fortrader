<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CurrencyRatesViewsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function rules() {
			return Array(
				Array( 'idCurrency', 'unique'),
				Array( 'idCurrency', 'numerical', 'integerOnly'=>true , 'min' => 1),
				Array( 'views', 'numerical', 'integerOnly'=>true , 'min' => 0),
			);
		}
		static function instance( $idCurrency, $views ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idCurrency', 'views' ));
		}
		static function saveViews( $idCurrency ){
			$model = self::model()->findByAttributes( Array( 'idCurrency' => $idCurrency ));
			if( !$model ) $model = self::instance( $idCurrency, 0 );
			$model->views += 1 ;
			if( $model->validate() ) $model->save();
		}
		
	}

?>