<?
	Yii::import( 'models.base.ModelBase' );
	Yii::import( 'components.MailerComponent' );
	
	final class UserPrivateMessageModel extends ModelBase {
		const TTLControl = 600;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'to' => Array( self::BELONGS_TO, 'UserModel', 'idTo' ),
				'from' => Array( self::BELONGS_TO, 'UserModel', 'idFrom' ),
			);
		}
		static function instance( $idTo, $idFrom, $text ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idTo', 'idFrom', 'text' ));
		}
		static function viewAll( $idTo ) {
			Yii::App()->db->createCommand("
				UPDATE		`{{user_private_message}}`
				SET			`viewed` = 1
				WHERE		`idTo` = :idTo
					AND		NOT `viewed`
			")->query( Array( 
				':idTo' => $idTo
			));
		}
		static function open( $ids ) {
			$ids = implode( ",", $ids );
			Yii::App()->db->createCommand("
				UPDATE		`{{user_private_message}}`
				SET			`opened` = 1, `viewed` = 1
				WHERE		`id` IN ($ids)
			")->query();
		}
		function checkAccess() {
			if( Yii::App()->user->isGuest ) return false;
			if( $this->isNewRecord ) return true;
			if( Yii::App()->user->checkAccess( 'userPrivateMessageControl' )) return true;
			if( Yii::App()->user->id == $this->idFrom ) {
				$now = time();
				$created = strtotime( $this->createdDT );
				if( $now - $created <= self::TTLControl ) {
					return true;
				}
			}
			return false;
		}
		function getHTMLText( $maxLen = null ) {
			$text = htmlspecialchars( $this->text );
			$text = CommonLib::nl2br( $text );
			if( $maxLen ) $text = CommonLib::shorten( $text, $maxLen );
			return $text;
		}
		private function notice() {
			$factoryMailer =  new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			
			$mailTpl = MailTplModel::findByKey( 'New private message' );
			if( !$mailTpl ) return false;
			
			$from = UserModel::model()->findByPk( $this->idFrom );
			$to = UserModel::model()->findByPk( $this->idTo );
			if( !$from or !$to ) return false;
			
			if( $to->isLongOnline()) return false;
			
			$settingsTo = $to->getSettings();
			if( !$settingsTo->receiveEmailNoticeAnswerPrivateMessage ) return false;
			
			$alias = $to->language;
			$message = $mailTpl->getMessage( $alias );
			$subject = $mailTpl->getSubject( $alias );
			
			if( !strlen( $message ) or !strlen( $subject )) return false;
			$urlDialog = Yii::App()->createAbsoluteURL( '/userPrivateMessage/dialog', Array( 'idWith' => $this->idFrom ));
			
			$message = strtr( $message, Array(
				'%username%' => $to->user_login,
				'%sender_username%' => $from->user_login,
				'%link%' => $urlDialog,
			));
			
			if( !strlen( $to->user_email )) return false;
			$mailer->addAddress( $to->user_email, $to->showName );
			
			if( !strlen( $subject )) return false;
			$mailer->Subject = $subject;
						
			if( substr_count( $message, '<' )) {
				$message = CommonLib::nl2br( $message, true );
				$mailer->MsgHTML( $message );
			}
			else{
				$mailer->Body = $message;
			}
			
			$result = $mailer->send();
			$error = $result ? null : $mailer->ErrorInfo;
			if( $error ) throw new Exception( $error );
		}
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( $this->isNewRecord ) {
					$this->notice();
				}
			}
			return $result;
		}
	}

?>