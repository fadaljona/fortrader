<?
	Yii::import( 'models.base.ModelBase' );

	final class MT5AccountHistoryOrdersModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "mt5_account_history_orders";
		}
	}

?>