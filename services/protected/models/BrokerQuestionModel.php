<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerQuestionModel extends ModelBase {
		const TTLControl = 600;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
				'answer' => Array( self::HAS_ONE, 'BrokerQuestionModel', 'idQuestion' ),
			);
		}
		function checkAccess() {
			if( Yii::App()->user->isGuest ) return false;
			if( $this->isNewRecord ) return true;
			if( Yii::App()->user->checkAccess( 'userMessageControl' )) return true;
			if( Yii::App()->user->id == $this->idUser ) {
				$now = time();
				$created = strtotime( $this->createdDT );
				if( $now - $created <= self::TTLControl ) {
					return true;
				}
			}
			return false;
		}
		function getHTMLText( $maxLen = null ) {
			$text = htmlspecialchars( $this->text );
			$text = CommonLib::nl2br( $text );
			if( $maxLen ) $text = CommonLib::shorten( $text, $maxLen );
			return $text;
		}
				
			# events
		/*
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_MESSAGE, Array( 'idUser' => $this->idUser, 'instance' => $this->instance, 'idLinkedObj' => $this->idLinkedObj ));
				if( $this->instance == 'broker/single' ) {
					BrokerUserActivityModel::event( BrokerUserActivityModel::TYPE_EVENT_CREATE_REVIEW, Array( 'idUser' => $this->idUser, 'idBroker' => $this->idLinkedObj ));
					BrokerStats2Model::inc( $this->idLinkedObj, "countReviews" );
				}
				if( $this->instance == 'ea/single' ) {
					EAUserActivityModel::event( EAUserActivityModel::TYPE_EVENT_CREATE_REVIEW, Array( 'idUser' => $this->idUser, 'idEA' => $this->idLinkedObj ));
				}
				if( $this->instance == 'journal/single' ) {
					JournalUserActivityModel::event( JournalUserActivityModel::TYPE_EVENT_CREATE_COMMENT, Array( 'idUser' => $this->idUser, 'idJournal' => $this->idLinkedObj ));
				}
			}
		}
		*/
	}

?>