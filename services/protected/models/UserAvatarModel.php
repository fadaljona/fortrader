<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserAvatarModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idUser, $path, $thumbPath, $middlePath ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser', 'path', 'thumbPath', 'middlePath' ));
		}
		protected function unlinkFiles() {
			@unlink( $this->path );
			@unlink( $this->middlePath );
			@unlink( $this->thumbPath );
			
			$wpCachePath = Yii::app()->basePath."/../../cachethumb/";
			foreach ( glob( $wpCachePath."user_{$this->idUser}_*") as $file ) {
				if( $file != "." && $file != "..") @unlink( $file );
			}
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->unlinkFiles();
		}
	}

?>