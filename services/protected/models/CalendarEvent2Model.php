<?
	Yii::import( 'models.base.ModelBase' );
	Yii::import( 'components.behaviors.ManageTextContentBehavior' );

	final class CalendarEvent2Model extends ModelBase {
		const PATHUploadDir = 'uploads/calendar';
		const WIDTHMiddle = 230;
		const HEIGHTMiddle = 'auto';
		const WIDTHThumb = 46;
		const HEIGHTThumb = 'auto';
		public $image;
		public $nameImage;
		public $nameMiddleImage;
		public $nameThumbImage;
		public $dateForSearch;
		public $indicatorIDForSearch;
		public $indicatorForSearch;
		public $hasNews;
		public $hasDesc;
		
		public $countRows; //ManageTextContent
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getTitlesSerious() {
			return Array(
				0 => 'not',
				1 => 'low',
				2 => 'medium',
				3 => 'high',
			);
		}
		function relations() {
			return Array(
			
			//ManageTextContent
				'i18nsManageTextContent' => Array( self::HAS_MANY, 'CalendarEvent2I18NModel', array('indicator_id' => 'indicator_id') ), 
				'ruLanguageI18NManageTextContent' => Array( self::HAS_ONE, 'CalendarEvent2I18NModel', array('indicator_id' => 'indicator_id'), 'on' => '`ruLanguageI18NManageTextContent`.`idLanguage` = 1'),
				'enLanguageI18NManageTextContent' => Array( self::HAS_ONE, 'CalendarEvent2I18NModel', array('indicator_id' => 'indicator_id'), 'on' => '`enLanguageI18NManageTextContent`.`idLanguage` = 0'),
			//ManageTextContent
				
				'i18ns' => Array( self::HAS_MANY, 'CalendarEvent2I18NModel', array('indicator_id' => 'indicator_id'), 'alias' => 'i18nsCalendarEvent' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'CalendarEvent2I18NModel', array('indicator_id' => 'indicator_id'), 
					'alias' => 'cLI18NCalendarEvent',
					'on' => '`cLI18NCalendarEvent`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'country' => Array( self::HAS_ONE, 'CountryModel', Array( 'alias' => 'countrycode' )),
				'currencyModel' => Array( self::HAS_ONE, 'CurrencyRatesModel', Array( 'code' => 'currency' )),
			);
		}
		public function getAvailableLangs(){
			$langsArr = array();
			foreach( $this->i18ns as $i18n ){
				if( $i18n->indicator_name ){
					$langsArr[] = LanguageModel::getLangFromLangsFileByID( $i18n->idLanguage );
				}
			}
			return $langsArr;
		}
		public static function findBySlug( $slug ){
			return self::model()->find(array(
				'condition' => " `slug` = :slug ",
				'params' => array( ':slug' => $slug ),
			));
		}
		static function findBySlugWithLang( $slug ){
			return self::model()->find(array(
				'with' => Array( 'currentLanguageI18N' ),
				'condition' => " `slug` = :slug AND `cLI18NCalendarEvent`.`indicator_name` IS NOT NULL AND `cLI18NCalendarEvent`.`indicator_name` <> '' ",
				'params' => array( ':slug' => $slug ),
			));
		}
		function getNextValue(){
			$model = CalendarEvent2ValueModel::model()->find(array(
				'condition' => " `t`.`indicator_id` = :indicatorId AND `t`.`dt` > :dt ",
				'order' => " `t`.`dt` ASC ",
				'params' => array( ':indicatorId' => $this->indicator_id, ':dt' => date('Y-m-d H:i:s') ),
			));
			
			if( !$model ){
				$model = CalendarEvent2ValueModel::model()->find(array(
					'condition' => " `t`.`indicator_id` = :indicatorId ",
					'order' => " `t`.`dt` DESC ",
					'params' => array( ':indicatorId' => $this->indicator_id ),
				));
			}
			return $model;
		}
		function getPrevValue(){
			return CalendarEvent2ValueModel::model()->find(array(
				'condition' => " `t`.`indicator_id` = :indicatorId AND `t`.`dt` <= :dt AND `t`.`fact_value` <> '' AND `t`.`fact_value` IS NOT NULL ",
				'order' => " `t`.`dt` DESC ",
				'params' => array( ':indicatorId' => $this->indicator_id, ':dt' => date('Y-m-d H:i:s') ),
			));
		}
		static function genID() {
			$query = "
				SELECT 	LEAST( MIN( `id` ), 0 )
				FROM	`{{calendar_event}}`
			";
			$min = (int)Yii::App()->db->createCommand( $query )->queryScalar();
			return $min - 1;
		}
		static function getListURL() {
			return Yii::App()->createURL( 'calendarEvent/list' );
		}
		static function getModelSingleURL( $slug ) {
			return Yii::App()->createURL( 'calendarEvent/single', Array( 'slug' => $slug ));
		}
		static function getModelAdminURL( $id ) {
			return Yii::App()->createURL( 'admin/calendarEvent/list', Array( 'idEdit' => $id ));
		}
		static function getCountries() {
			$query = "
				SELECT `country`.`id`, `country`.`name` FROM `{{calendar_event2}}` `event2`
				LEFT JOIN `{{country}}` `country` ON `country`.`alias` = `event2`.`countrycode`
				WHERE `country`.`id` IS NOT NULL
				GROUP BY `country`.`id`
			";
			$results = Yii::App()->db->createCommand( $query )->queryAll();
			$countries = array();
			foreach( $results as $result ){
				$countries[$result['id']] = Yii::t( '*', $result['name'] );
			}
			return $countries;
		}
		static function getAllCurrencies(){
			$models = self::model()->findAll(array(
				'with' => array( 'currencyModel' => array( 'with' => 'currentLanguageI18N' ) ),
				'condition' => " `t`.`currency` != 'All' ",
				'group' => ' `t`.`currency` ',
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = array(
					'code' => $model->currency,
					'name' => $model->currencyModel->name
				);
			}
			return $outArr;
		}
		
		function getAbsoluteSingleURL() {
			return Yii::App()->createAbsoluteURL( 'calendarEvent/single', Array( 'slug' => $this->slug ));
		}
		
		//ManageTextContent
		function getSingleURL() {
			return self::getModelSingleURL( $this->slug );
		}
		function getAdminURL() {
			return self::getModelAdminURL( $this->id );
		}
		
		
		
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;

			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->indicator_name )) return $i18n;
			}
		}
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		
		
		function getDescription() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->indicator_description : '';
		}
		function getIndicatorName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->indicator_name : '';
		}
		function getPageTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->pageTitle : '';
		}
		function getMetaTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaTitle : '';
		}
		function getMetaDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaDesc : '';
		}
		function getMetaKeys() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaKeys : '';
		}
		function getFullDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->fullDesc : '';
		}
		function getChartTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->chartTitle ? $i18n->chartTitle : $i18n->indicator_name : '';
		}
		function getChartSubTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->chartSubTitle : '';
		}
		
		
		//ManageTextContent
		public function behaviors(){
			return array(
				'manageTextContentBehavior' => array(
					'class' => 'ManageTextContentBehavior',
				),
			);
		}
		

			
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			return $result;
		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
		
	}

?>
