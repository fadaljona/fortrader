<?php

Yii::import('models.base.ModelBase');

final class PaymentSystemCurrencyModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function relations()
    {
        return array(
            'i18ns' => array( self::HAS_MANY, 'PaymentSystemCurrencyI18nModel', 'idCurrency', 'alias' => 'i18nsPaymentSystemCurrency' ),
            'currentLanguageI18N' => array( self::HAS_ONE, 'PaymentSystemCurrencyI18nModel', 'idCurrency',
                'alias' => 'cLI18NPaymentSystemCurrencyModel',
                'on' => '`cLI18NPaymentSystemCurrencyModel`.`idLanguage` = :idLanguage',
                'params' => array(
                    ':idLanguage' => LanguageModel::getCurrentLanguageID(),
                ),
            ),
            'toCurrentLanguageI18N' => array( self::HAS_ONE, 'PaymentSystemCurrencyI18nModel', 'idCurrency',
                'alias' => 'toCLI18NPaymentSystemCurrencyModel',
                'on' => '`toCLI18NPaymentSystemCurrencyModel`.`idLanguage` = :idLanguage',
                'params' => array(
                    ':idLanguage' => LanguageModel::getCurrentLanguageID(),
                ),
            ),
        );
    }

    public static function getExchangeDirectionUrl($from, $to)
    {
        return Yii::app()->createUrl('exchangeECurrency/exchangeDirection', array('from' => $from, 'to' => $to));
    }
    public static function findBySlugWithLang($slug)
    {
        return self::model()->find(array(
            'with' => array( 'currentLanguageI18N' ),
            'condition' => " `slug` = :slug AND `cLI18NPaymentSystemCurrencyModel`.`title` IS NOT NULL AND `cLI18NPaymentSystemCurrencyModel`.`title` <> '' ",
            'params' => array( ':slug' => $slug ),
        ));
    }

    public static function getAdminListDp($filterModel = false)
    {
        $condition = '';
        $params = array();
        if (Yii::app()->request->getParam('idSystem')) {
            $condition = " `t`.`idSystem` = :idSystem ";
            $params['idSystem'] = Yii::app()->request->getParam('idSystem');
        }
        $DP = new CActiveDataProvider(get_called_class(), array(
            'criteria' => array(
                'with' => array( 'currentLanguageI18N', 'i18ns' ),
                'order' => ' `t`.`id` DESC ',
                'condition' => $condition,
                'params' => $params
            ),
        ));
        return $DP;
    }
    public function getAvailableLangs()
    {
        $langsArr = array();
        foreach ($this->i18ns as $i18n) {
            if ($i18n->title) {
                $langsArr[] = LanguageModel::getLangFromLangsFileByID($i18n->idLanguage);
            }
        }
        return $langsArr;
    }

    public static function getCurrenciesForFilter()
    {
        $currencies = self::model()->findAll(array(
            'with' => array( 'currentLanguageI18N', 'i18ns' ),
            'condition' => " `cLI18NPaymentSystemCurrencyModel`.`title` <> '' AND `cLI18NPaymentSystemCurrencyModel`.`title` IS NOT NULL "
        ));
        $outArr = array();
        foreach ($currencies as $currency) {
            $outArr[$currency->id] = array(
                'name' => $currency->title,
                'hasFrom' => $currency->getLastCourseDate('from') == date('Y-m-d') ? 1 : 0,
                'hasTo' => $currency->getLastCourseDate('to') == date('Y-m-d') ? true : false,
            );
        }
        return $outArr;
    }
    public function getLastCourseDate($direction)
    {
        if ($direction == 'to') {
            $directionField = 'toCurrency';
        } elseif ($direction == 'from') {
            $directionField = 'fromCurrency';
        } else {
            return false;
        }

        $course = PaymentExchangerCourseModel::model()->find(array(
            'condition' => " `t`.`$directionField` = :id ",
            'order' => ' `t`.`date` DESC ',
            'params' => array(':id' => $this->id)
        ));
        if (!$course) {
            return false;
        }
        return $course->date;
    }
    public function getSingleURL()
    {
        //return Yii::App()->createURL( 'informers/single', Array( 'slug' => $this->slug ));
    }

    public function getI18N()
    {
        if ($this->currentLanguageI18N) {
            return $this->currentLanguageI18N;
        }
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == 0) {
                if (strlen($i18n->title)) {
                    return $i18n;
                }
                break;
            }
        }
        foreach ($this->i18ns as $i18n) {
            if (strlen($i18n->title)) {
                return $i18n;
            }
        }
    }
    public function getTitleByLang($langId)
    {
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == $langId) {
                if (strlen($i18n->title)) {
                    return $i18n->title;
                }
                return false;
            }
        }
    }

    public function getTitle()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->title : '';
    }


        # i18ns
        public function deleteI18Ns()
    {
        foreach ($this->i18ns as $i18n) {
            $i18n->delete();
        }
    }
    public function addI18Ns($i18ns)
    {
        $idsLanguages = LanguageModel::getExistsIDS();
        foreach ($i18ns as $idLanguage => $obj) {
            $i18n = PaymentSystemCurrencyI18nModel::model()->findByAttributes(array( 'idCurrency' => $this->id, 'idLanguage' => $idLanguage ));
            if (!$i18n) {
                $i18n = PaymentSystemCurrencyI18nModel::instance($this->id, $idLanguage, $obj->title);
            } else {
                $i18n->title = $obj->title;
            }
            $i18n->save();
        }
    }
    public function setI18Ns($i18ns)
    {
        $this->addI18Ns($i18ns);
    }
        # events
    protected function afterDelete()
    {
        parent::afterDelete();
        $this->deleteI18Ns();
    }
}
