<?php
	Yii::import( 'models.base.ModelBase' );

	final class CalendarGroupSubscriptionUserToCountryModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
        static function instance( $idUser, $countryCode ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser', 'countryCode' ));
		}
	
	}

?>
