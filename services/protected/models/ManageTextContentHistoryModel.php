<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ManageTextContentHistoryModel extends ModelBase {
		public $dayLength;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		function relations() {
			return Array(
				'user' => Array( self::HAS_ONE, 'UserModel', Array( 'ID' => 'idUser' )),
			);
		}
		
		static function instance( $idUser, $modelId, $modelName, $fieldName, $lang, $oldVal, $newVal ) {
			if( $modelId ){
				$model = self::model()->find(array(
					'condition' => ' `idUser` = :idUser AND `modelId` = :modelId AND `modelName` = :modelName AND `fieldName` = :fieldName AND `lang` = :lang AND `dayDate` = :dayDate ',
					'params' => array( ':idUser' => $idUser, ':modelId' => $modelId, ':modelName' => $modelName, ':fieldName' => $fieldName, ':lang' => $lang, ':dayDate' => date('Y-m-d') )
				));
				if( $model ){
					$model->newVal = $newVal;
					return $model;
				} 
			}
			
			
			
			$model = self::modelFromAssoc( __CLASS__, compact( 'idUser', 'modelId', 'modelName', 'fieldName', 'lang', 'oldVal', 'newVal' ));
			$model->dayDate = date('Y-m-d');
			return $model;
		}
		
	}