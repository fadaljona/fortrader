<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestStatsModel extends ModelBase {
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function updateStats( $idContest ) {
			
			$command = file_get_contents( "protected/data/updateContestStats.sql" );
			Yii::App()->db->createCommand( $command )->query(Array(
				':idContest' => $idContest,
			));
		}
		
		static function checkStats( $idContest ) {
			$statsModel = self::model()->findByPk($idContest);
			if( $statsModel && $statsModel->totalProfit > 0 )
				return true;
			else
				return false;
		}
		
		public function relations(){

			return array(
				'wleaderMember'=>array(self::HAS_ONE, 'ContestMemberModel', array('id'=>'leaderMember') ),
				'wbestProfitRiskMember'=>array(self::HAS_ONE, 'ContestMemberModel', array('id'=>'bestProfitRiskMember') ),
				'contest' => Array( self::HAS_ONE, 'ContestModel', array('id'=>'idContest') ),
			);
		}
		
	}

?>