<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestI18NModel extends ModelBase {
		const PATHUploadDir = 'uploads/contests';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{contest_i18n}}";
		}
		static function instance( $idContest, $idLanguage, $name, $description, $urlRules, $fullDesc, $rules, $pageTitle, $pageSeoTitle, $seoKeys, $seoDesc ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idContest', 'idLanguage', 'name', 'description', 'urlRules', 'fullDesc', 'rules', 'pageTitle', 'pageSeoTitle', 'seoKeys', 'seoDesc' ));
		}
		function getHTMLDescription() {
			$description = htmlspecialchars( $this->description );
			$description = CommonLib::nl2br( $description );
			return $description;
		}
			
			# image
		function getPathImage() {
			return strlen($this->nameImage) ? self::PATHUploadDir."/{$this->nameImage}" : '';
		}
		function getSrcImage() {
			return $this->pathImage ? Yii::app()->baseUrl."/{$this->pathImage}" : '';
		}
		function getSizedSrcImage( $width, $height ) {
			if( !$this->pathImage ) return '';
			
			$fullImgPath = Yii::getPathOfAlias('webroot') . '/' . $this->pathImage ;
			$fullImgPathParts = pathinfo($fullImgPath);
			
			$fileName = $fullImgPathParts['filename'] . $width . $height . '.' . $fullImgPathParts['extension'];
			$fileNamePath = $fullImgPathParts['dirname'] . '/' . $fileName ;
			
			if( !file_exists( $fileNamePath ) ){
				ImgResizeLib::resize( $fullImgPath, $fileNamePath, $width, $height );
			}
	
			return Yii::app()->baseUrl . '/' . self::PATHUploadDir . '/' . $fileName;
		}
		function deleteImage() {
			if( strlen( $this->nameImage )) {
				if( is_file( $this->pathImage ) and !@unlink( $this->pathImage )) throw new Exception( "Can't delete {$this->pathImage}" );
				$this->nameImage = null;
			}
		}
		function setImage( $name ) {
			$this->deleteImage();
			$this->nameImage = $name;
		}
		function uploadImage( $uploadedFile ) {
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );

			$this->setImage( $fileFullName );
		}
		function getImage( $options = Array() ) {
			$options[ 'src' ] = $this->srcImage;
			$options[ 'title' ] = $this->name;
			return $this->nameImage ? CHtml::tag( 'img', $options ) : '';
		}
		
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteImage();
		}
	}

?>