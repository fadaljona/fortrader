<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserRequestModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
			);
		}
		static function getRequestInstance() {
			return Yii::App()->controller->id.'/'.Yii::App()->controller->action->id;
		}
		static function getRequestIDObject() {
			$idObject = @$_GET['id'];
			return $idObject ? $idObject : null;
		}
		static function getRequestIDUser() {
			$idUser = Yii::App()->user->id;
			return $idUser ? $idUser : null;
		}
		static function getRequestIP() {
			return sprintf( "%u", ip2long( $_SERVER[ 'REMOTE_ADDR' ]));
		}
		static function getRequestIDAgent() {
			return AgentModel::getCurrentIDAgent();
		}
		static function getRequestSession() {
			return session_id();
		}
		static function getRequestDT() {
			static $DT;
			if( !$DT ) $DT = date( "Y-m-d H:i:s" );
			return $DT;
		}
		static function validateRequestInstance() {
			$instance = self::getRequestInstance();
			return in_array( $instance, Array( 'calendarEvent/list','calendarEvent/single' ));
		}
		static function validateRequestPk() {
			$request = UserRequestModel::model()->findByAttributes( Array(
				'createdDT' => self::getRequestDT(),
				'session' => self::getRequestSession(),
			));
			return !$request;
		}
		static function addRequest() {
			if( !self::validateRequestInstance()) return;
			if( !self::validateRequestPk()) return;
			
			$request = new UserRequestModel();
			$request->instance = self::getRequestInstance();
			$request->idObject = self::getRequestIDObject();
			$request->idUser = self::getRequestIDUser();
			$request->ip = self::getRequestIP();
			$request->idAgent = self::getRequestIDAgent();
			$request->session = self::getRequestSession();
			$request->createdDT = self::getRequestDT();
			
			$request->save();
			self::clean();
		}
		static function getCountSessions( $conditions = Array() ) {
			$command = Yii::App()->db->createCommand();
			$command->select = " COUNT( DISTINCT `session` )  ";
			$command->from = "{{user_request}}";
			
			$sqlCondition = "";
			foreach( $conditions as $condition ) {
				$sqlPart = "";
				if( isset( $condition[ 'instance' ])) {
					$impInstances = implode( "','", (array)$condition[ 'instance' ] );
					CommonLib::addSQL( $sqlPart, " `instance` IN ( '{$impInstances}' ) ", "AND" );
				}
				if( isset( $condition[ 'idObject' ])) {
					if( is_array( $condition[ 'idObject' ])) {
						$impIDsObjects = implode( ",", $condition[ 'idObject' ]);
						CommonLib::addSQL( $sqlPart, " `idObject` IN ( '{$impIDsObjects}' ) ", "AND" );
					}
					elseif( $condition[ 'idObject' ] ){
						CommonLib::addSQL( $sqlPart, " `idObject` = {$condition[ 'idObject' ]} ", "AND" );
					}
					else{
						CommonLib::addSQL( $sqlPart, " `idObject` = {$condition[ 'idObject' ]} ", "AND" );
					}
				}
				if( strlen( $sqlPart )) {
					CommonLib::addSQL( $sqlCondition, $sqlPart, "OR" );
				}
			}
			
			if( strlen( $sqlCondition )) {
				$command->where = $sqlCondition;
			}
			
			$count = $command->queryScalar();
			return $count;
		}
		static function clean() {
			Yii::App()->db->createCommand("
				DELETE
				FROM
					`{{user_request}}`
				WHERE `createdDT` < :tenMinutes
			")->query( array( ':tenMinutes' => date( 'Y-m-d H:i:s', time() - 60*10 ) ) );
		}
	}

?>