<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesCategoryI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'category' => Array( self::HAS_ONE, 'QuotesCategoryModel', array( 'id' => 'idCat' ) ),
			);
		}
		function tableName() {
			return "{{quotes_category_i18n}}";
		}
		static function instance( $idCat, $idLanguage, $name, $desc, $fullDesc, $title, $metaTitle, $metaDescription, $metaKeywords, $shortDesc, $order ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idCat', 'idLanguage', 'name', 'desc', 'fullDesc', 'title', 'metaTitle', 'metaDescription', 'metaKeywords', 'shortDesc', 'order' ));
		}
	}
?>