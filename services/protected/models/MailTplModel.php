<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MailTplModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'MailTplI18NModel', 'idTpl', 'order' => '`i18ns`.`idLanguage`' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'MailTplI18NModel', 'idTpl', 
					'on' => '`currentLanguageI18N`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		static function findByKey( $key ) {
			return self::model()->findByAttributes( Array( 'key' => $key ));
		}
		function getI18N( $alias = null ) {
			if( $alias ) {
				$lang = LanguageModel::getByAlias( $alias );
				if( $lang ) {
					foreach( $this->i18ns as $i18n ) {
						if( $i18n->idLanguage == $lang->id ) {
							if( strlen( $i18n->message )) return $i18n;
							break;
						}
					}
				}
			}
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->message )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->message )) return $i18n;
			}
		}
		function getSubject( $alias = null ) {
			$i18n = $this->getI18N( $alias );
			return $i18n ? $i18n->subject : '';
		}
		function getMessage( $alias = null ) {
			$i18n = $this->getI18N( $alias );
			return $i18n ? $i18n->message : '';
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if( strlen( $obj->subject ) and strlen( $obj->message ) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = MailTplI18NModel::model()->findByAttributes( Array( 'idTpl' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = MailTplI18NModel::instance( $this->id, $idLanguage, $obj->subject, $obj->message );
					}
					else{
						$i18n->subject = $obj->subject;
						$i18n->message = $obj->message;
					}
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>