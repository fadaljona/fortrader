<?
	Yii::import( 'models.base.ModelBase' );
	
	final class NewsAggregatorCatsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'NewsAggregatorCatsI18nModel', 'idCat', 'alias' => 'i18nsNewsAggregatorCats' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'NewsAggregatorCatsI18nModel', 'idCat', 
					'alias' => 'cLI18NNewsAggregatorCatsModel',
					'on' => '`cLI18NNewsAggregatorCatsModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		static function findBySlug( $slug ){
			return self::model()->find(array(
				'condition' => " `slug` = :slug ",
				'params' => array( ':slug' => strtolower($slug) ),
			));
		}
		static function getAllNewsUrl(){
			return Yii::app()->createUrl('newsAggregator/index');
		}
		static function getItemsForInformer(){
			$models = self::model()->findAll(array(
				'with' => array( 'currentLanguageI18N' ),
				'condition' => " `cLI18NNewsAggregatorCatsModel`.`title` IS NOT NULL AND `cLI18NNewsAggregatorCatsModel`.`title` <> '' AND `t`.`enabled` = 1 ",
				'order' => " `t`.`id` ASC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->id] = array(
					'toInformer' => $model->title,
					'name' => $model->title,
				);
			}
			return $outArr;
		}
		static function getAdminListDp( $filterModel = false ){
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`order` ASC ',
				),
			));
			return $DP;
		}
		static function defineCategory( $stemmedText, $idLang ){
			$settings = NewsAggregatorSettingsModel::getModel();
			$i18ns = NewsAggregatorCatsI18nModel::model()->findAll(array(
				'condition' => " `t`.`idLanguage` =:idLanguage AND `t`.`stemedKeys` <> '' AND `t`.`stemedKeys` <> '[]' ",
				'params' => array( ':idLanguage' => $idLang ),
			));
			$resultArr = array();
			foreach( $i18ns as $i18n ){
				$keys = json_decode($i18n->stemedKeys);
				$resultArr[$i18n->idCat]['keysCount'] = count($keys);
				$resultArr[$i18n->idCat]['foundKeysCount'] = 0;
				$resultArr[$i18n->idCat]['foundKeys'] = '';
				$resultArr[$i18n->idCat]['title'] = $i18n->category->title;
				$resultArr[$i18n->idCat]['order'] = $i18n->category->order;
				foreach( $keys as $key ){
					if( strpos( ' ' . $stemmedText . ' ', ' ' . $key . ' ' ) !== false ){
						$resultArr[$i18n->idCat]['foundKeysCount'] +=1;
						$resultArr[$i18n->idCat]['foundKeys'] .= $key . ', ';
					}
				}
			}
			$maxRelation = 0;
			$maxRelationIdCat = -1;
			$order = 0;
			foreach( $resultArr as $idCat => $result ){
				if( $result['foundKeysCount'] / $result['keysCount'] > $maxRelation ){
					$maxRelation = $result['foundKeysCount'] / $result['keysCount'];
					$maxRelationIdCat = $idCat;
					$order = $result['order'];
				}elseif( $result['foundKeysCount'] / $result['keysCount'] = $maxRelation && $result['order'] > $order ){
					$order = $result['order'];
					$maxRelationIdCat = $idCat;
				}
			}
			$returnArr = array();
			if( $maxRelation == 0 ){
				$returnArr['cat'] = $settings->defaultCat;
				$returnArr['summary'] = 'DEFAULT CATEGORY';
			}else{
				$returnArr['cat'] = $maxRelationIdCat;
				$returnArr['summary'] = $resultArr[$maxRelationIdCat]['title'] . ' keysCount: ' . $resultArr[$maxRelationIdCat]['keysCount'] . ' foundKeysCount: ' . $resultArr[$maxRelationIdCat]['foundKeysCount'] . ' foundKeys: ' . $resultArr[$maxRelationIdCat]['foundKeys'];
			}
			return $returnArr;
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->name )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->name )) return $i18n;
			}
		}
		function getTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->title : '';
		}
		function getKyesStr() {
			$i18n = $this->getI18N();
			if( !$i18n ) return '';
			if( !$i18n->keys ) return '';
			return implode(', ', json_decode($i18n->keys));
		}
		
		function getPageTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->pageTitle : '';
		}
		function getMetaTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaTitle : '';
		}
		function getMetaKeys() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaKeys : '';
		}
		function getMetaDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaDesc : '';
		}
		function getShortDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->shortDesc : '';
		}
		function getFullDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->fullDesc : '';
		}
		
		
		function getSingleURL() {
			return Yii::app()->createUrl( 'newsAggregator/category', array( 'slug' => $this->slug ));
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$settings = NewsAggregatorSettingsModel::getModel();
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->title )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = NewsAggregatorCatsI18nModel::model()->findByAttributes( Array( 'idCat' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = NewsAggregatorCatsI18nModel::instance( $this->id, $idLanguage, $obj->title, $obj->keys, $obj->pageTitle, $obj->metaTitle, $obj->metaKeys, $obj->metaDesc, $obj->shortDesc, $obj->fullDesc );
					}else{
						$i18n->title = $obj->title;
						$i18n->keys = $obj->keys;
						
						$i18n->pageTitle = $obj->pageTitle;
						$i18n->metaTitle = $obj->metaTitle;
						$i18n->metaKeys = $obj->metaKeys;
						$i18n->metaDesc = $obj->metaDesc;
						$i18n->shortDesc = $obj->shortDesc;
						$i18n->fullDesc = $obj->fullDesc;
					}
					
					
					$langConstant = $settings->geLangStemConstByLangId( $idLanguage );
					if( $obj->keys && $langConstant ){
						$keysArr = json_decode($obj->keys);
						$stemedKeysArr = array();
						foreach( $keysArr as $key ){
							$stemedKey = CommonLib::stemedText( $key, $langConstant );
							if( $stemedKey ) $stemedKeysArr[] = $stemedKey;
						}
						$stemedKeysArr = array_unique($stemedKeysArr);
						$stemedKeysStr = implode('","', $stemedKeysArr);
						if( $stemedKeysStr ){
							$stemedKeysStr = '["' . implode('","', $stemedKeysArr) . '"]';
						}else{
							$stemedKeysStr = '[]';
						}
						$i18n->stemedKeys = $stemedKeysStr;	
					}
					
					
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>