<?
	Yii::import( 'models.base.SettingsModelBase' );
	
	final class BrokerSettingsModel extends SettingsModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) $model = new self();
			}
			return $model;
		}
		
		public function getBrokersListMetaTitle(){
			if( !$this->texts || !$this->texts->brokersListMetaTitle ) return false;
			return $this->texts->brokersListMetaTitle;
		}
		public function getBrokersListMetaDesc(){
			if( !$this->texts || !$this->texts->brokersListMetaDesc ) return false;
			return $this->texts->brokersListMetaDesc;
		}
		public function getBrokersListMetaKeys(){
			if( !$this->texts || !$this->texts->brokersListMetaKeys ) return false;
			return $this->texts->brokersListMetaKeys;
		}
		public function getBrokersListOgImage(){
			if( !$this->texts || !$this->texts->brokersListOgImage ) return false;
			return $this->texts->brokersListOgImage;
		}
		public function getBinaryOptionsListMetaTitle(){
			if( !$this->texts || !$this->texts->binaryOptionsListMetaTitle ) return false;
			return $this->texts->binaryOptionsListMetaTitle;
		}
		public function getBinaryOptionsListMetaDesc(){
			if( !$this->texts || !$this->texts->binaryOptionsListMetaDesc ) return false;
			return $this->texts->binaryOptionsListMetaDesc;
		}
		public function getBinaryOptionsListMetaKeys(){
			if( !$this->texts || !$this->texts->binaryOptionsListMetaKeys ) return false;
			return $this->texts->binaryOptionsListMetaKeys;
		}
		public function getBinaryOptionsListOgImage(){
			if( !$this->texts || !$this->texts->binaryOptionsListOgImage ) return false;
			return $this->texts->binaryOptionsListOgImage;
		}
		
		public function getBrokersListShortDesc(){
			if( !$this->texts || !$this->texts->brokersListShortDesc ) return false;
			return $this->texts->brokersListShortDesc;
		}
		public function getBrokersListFullDesc(){
			if( !$this->texts || !$this->texts->brokersListFullDesc ) return false;
			return $this->texts->brokersListFullDesc;
		}
		public function getBinaryOptionsListShortDesc(){
			if( !$this->texts || !$this->texts->binaryOptionsListShortDesc ) return false;
			return $this->texts->binaryOptionsListShortDesc;
		}
		public function getBinaryOptionsListFullDesc(){
			if( !$this->texts || !$this->texts->binaryOptionsListFullDesc ) return false;
			return $this->texts->binaryOptionsListFullDesc;
		}
	}

?>