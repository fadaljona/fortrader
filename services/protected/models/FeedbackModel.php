<?
	Yii::import( 'models.base.ModelBase' );
	Yii::import( 'components.MailerComponent' );
	
	final class FeedbackModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'type' => Array( self::BELONGS_TO, 'FeedbackTypeModel', 'idType' ),
			);
		}
		function sendEmailsToContacts() {
			$factoryMailer = new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			
			$mailer->clearReplyTos();
			$mailer->addReplyTo( $this->email, $this->user );
			
			$key = 'Feedback email';
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			if( !$mailTpl ) self::throwI18NException( "Can't load mail template! ({key})", Array( '{key}' => $key ));
			
			$mailTplI18n = $mailTpl->getI18N( 'ru' );
			
			$message = strtr( $mailTplI18n->message, Array(
				'{user}' => $this->user,
				'{email}' => $this->email,
				'{nameType}' => $this->type->name,
				'{message}' => $this->message,
			));
			
			$contacts = ContactModel::model()->findAll( Array(
				'condition' => " `t`.`reciveFeedback` ",
			));
			
			if( !$contacts ) return;
			$emails = CommonLib::slice( $contacts, 'email' );
			
			foreach( $emails as $email ) {
				$mailer->addAddress( $email );
			}
			
			$mailer->Subject = $mailTplI18n->subject;
			if( substr_count( $message, '<' )) {
				$message = CommonLib::nl2br( $message, true );
				$mailer->MsgHTML( $message );
			}
			else{
				$mailer->Body = $message;
			}
			
			$mailer->send();
		}
	}

?>