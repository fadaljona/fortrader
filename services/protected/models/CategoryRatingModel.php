<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CategoryRatingModel extends ModelBase {
		const PATHUploadDir = 'uploads/categoryRating';
		const categoriesCacheTime = 600;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'CategoryRatingI18nModel', 'idRating', 'alias' => 'i18nsCategoryRating' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'CategoryRatingI18nModel', 'idRating', 
					'alias' => 'cLI18NCategoryRatingModel',
					'on' => '`cLI18NCategoryRatingModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				
			);
		}

		static function getAdminListDp( $filterModel = false ){
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`id` DESC ',
				),
			));
			return $DP;
		}
		static function findBySlug( $slug ){
			return self::model()->find(array(
				'condition' => " `slug` = :slug ",
				'params' => array( ':slug' => $slug ),
			));
		}
		public function getAvailableLangs(){
			$langsArr = array();
			foreach( $this->i18ns as $i18n ){
				if( $i18n->title ){
					$langsArr[] = LanguageModel::getLangFromLangsFileByID( $i18n->idLanguage );
				}
			}
			return $langsArr;
		}
		static function findBySlugWithLang( $slug ){
			return self::model()->find(array(
				'with' => Array( 'currentLanguageI18N' ),
				'condition' => " `slug` = :slug AND `cLI18NCategoryRatingModel`.`title` IS NOT NULL AND `cLI18NCategoryRatingModel`.`title` <> '' ",
				'params' => array( ':slug' => $slug ),
			));
		}
		static function getCategoriesTree(){
			$cacheKey = 'cacheKeyCategoriesTree';
			
			if( $data = Yii::app()->cache->get( $cacheKey ) ){
				return $data;
			}
			$sql = "
				SELECT `term`.`term_id`, `term`.`name`, `taxonomy`.`parent` 
				FROM `wp_terms` `term`
				LEFT JOIN `wp_term_taxonomy` `taxonomy` ON `taxonomy`.`term_id` = `term`.`term_id`
				WHERE `taxonomy`.`taxonomy` = 'category'
			";
			$results = Yii::App()->db->createCommand( $sql )->queryAll();
			
			$tree = array(); 
			$sub = array( 0 => &$tree ); 
			foreach ($results as $item) { 
				$id = $item['term_id']; 
				$parent = $item['parent']; 
				$branch = &$sub[$parent]; 
				$branch[$id] = array('name' => $item['name']); 
				$sub[$id] = &$branch[$id]; 
			} 
			
			Yii::app()->cache->set( $cacheKey, $tree, self::categoriesCacheTime);
			return $tree;
		}
		static function getAllCategories(){
			$tree = self::getCategoriesTree();
			return self::getCategoryList($tree);
		}
		static function getCategoryList($arr,$level=0){
			$res = array();
			if(is_array($arr)){
				foreach ($arr as $k => $value) {
					if( $k == 'name' ) continue;
					$res[$k] = str_pad("", $level*3, '---') . $value['name'];
					if(is_array($value))$res = $res + self::getCategoryList($value,$level+1);
				}
			}
			return $res;
		}
		static function getChildsOfCategory( $catId ){
			if( !$catId ) return false;
			
			$cacheKey = 'cacheKeyChildsOfCategory' . $catId;
			
			if( is_array( $data = Yii::app()->cache->get( $cacheKey ) ) ){
				return $data;
			}
			$tree = self::getCategoriesTree();
			$childs = self::findChildsOfCategory($tree, $catId );
			
			Yii::app()->cache->set( $cacheKey, $childs, self::categoriesCacheTime);
			return $childs;
		}
		static function findChildsOfCategory($tree, $catId = false ){
			
			$resultArr = array();
			
			if( $catId ){
				$foundBranch = false;
				if( isset( $tree[$catId] ) ){
					$foundBranch = true;
					$arr = $tree[$catId];
				} 
			}else{
				$foundBranch = true;
				$arr = $tree;
			}
			
			if( $foundBranch ){
				if(is_array($arr)){
					foreach ($arr as $k => $value) {
						if( $k == 'name' ) continue;
						$resultArr[] = $k;
						if(is_array($value)){
							$resultArr = array_merge( $resultArr, self::findChildsOfCategory($value) );
						}
					}
				}
			}else{
				foreach( $tree as $branch ){
					if(is_array($branch)){
						$foundArr = self::findChildsOfCategory($branch, $catId);
						$resultArr = array_merge( $resultArr, $foundArr );
						if( $foundArr ) break;
					} 
				}
			}
			
			return $resultArr;
		}
		static function getRatingsForSelect(){
			$models = self::model()->findAll();
			$outArray = array();
			foreach( $models as $model ){
				$outArray[$model->id] = $model->title;
			}
			return $outArray;
		}
		
		static function updateStats($exists = false) {
			if (!$exists) {
				$exists = Yii::App()->db->createCommand("
					SELECT 		1
					FROM 		`{{category_rating_item_stats}}`
					WHERE 		`updatedDT` IS NULL OR DATE(`updatedDT`) < CURDATE()
					LIMIT		1
				")->queryScalar();
			}

			if( $exists ) {
				$command = file_get_contents( "protected/data/updateCategoryRatingItemStats.sql" );
				Yii::App()->db->createCommand( $command )->query();
			}
		}
		
		
		function getPathOgImage() {
			return strlen($this->ogImage) ? self::PATHUploadDir."/{$this->ogImage}" : '';
		}
		function getSrcOgImage() {
			return $this->pathOgImage ? Yii::app()->baseUrl."/{$this->pathOgImage}" : '';
		}
		function getAbsoluteSrcOgImage() {
			return $this->pathOgImage ? Yii::app()->createAbsoluteUrl($this->pathOgImage) : '';
		}
		function uploadOgImage( $uploadedFile ) {
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );

			$this->setOgImage( $fileFullName );
		}
		function setOgImage( $name ) {
			$this->deleteOgImage();
			$this->ogImage = $name;
		}
		function deleteOgImage() {
			if( strlen( $this->ogImage )) {
				if( is_file( $this->pathOgImage ) and !@unlink( $this->pathOgImage )) throw new Exception( "Can't delete {$this->pathOgImage}" );
				$this->ogImage = null;
			}
		}
		
		
		
		function getSingleURL() {
			return Yii::App()->createURL( 'informers/single', Array( 'slug' => $this->slug ));
		}

		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->title )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->title )) return $i18n;
			}
		}
		function getTitleByLang( $langId ) { 
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == $langId ) {
					if( strlen( $i18n->title )) return $i18n->title;
					return false;
				}
			}
		}
		
		function getTitle() { $i18n = $this->getI18N(); return $i18n ? $i18n->title : '';}
		function getMetaTitle() { $i18n = $this->getI18N(); return $i18n ? $i18n->metaTitle : ''; }
		function getMetaDesc() { $i18n = $this->getI18N(); return $i18n ? $i18n->metaDesc : ''; }
		function getMetaKeys() { $i18n = $this->getI18N(); return $i18n ? $i18n->metaKeys : ''; }
		function getShortDesc() { $i18n = $this->getI18N(); return $i18n ? $i18n->shortDesc : ''; }
		function getFullDesc() { $i18n = $this->getI18N(); return $i18n ? $i18n->fullDesc : ''; }


		
		
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
		
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				$i18n = CategoryRatingI18nModel::model()->findByAttributes( Array( 'idRating' => $this->id, 'idLanguage' => $idLanguage ));
				if( !$i18n ) {
					$i18n = CategoryRatingI18nModel::instance( $this->id, $idLanguage, $obj->title, $obj->metaTitle, $obj->metaDesc, $obj->metaKeys, $obj->shortDesc, $obj->fullDesc );
				}else{
					$i18n->title = $obj->title;
					$i18n->metaTitle = $obj->metaTitle;
					$i18n->metaDesc = $obj->metaDesc;
					$i18n->metaKeys = $obj->metaKeys;
					$i18n->shortDesc = $obj->shortDesc;
					$i18n->fullDesc = $obj->fullDesc;
					
				}
				$i18n->save();
			}
		}
		function setI18Ns( $i18ns ) {
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			
		}
	}

?>