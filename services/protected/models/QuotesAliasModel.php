<?php


class QuotesAliasModel extends ModelBase{
	const CACHE_KEY_PREFIX = 'QuotesAliasModel.';
	protected $symbolname;
	protected $symbol_suggest;

	public function tableName(){
		return '{{quotes_alias}}';
	}

	function getSymbol_suggest(){
		if($this->symbol){
			$symbol=QuotesSymbolsModel::model()->find('`id`='.$this->symbol);
			return trim($symbol->name.'('.$this->symbol.')');
		}else
			return '';
	}

	public function getSymbolname(){
		$id = $this->getAttribute('symbol');
		if(!$id)
			return '';
		else{
			$cat = QuotesSymbolsModel::model()->find('id=' . $id);
			return trim($cat->name." ($id)");
		}
	}

	static function getModel(){
		static $model;
		if(!$model){
			$model = self::model()->find();
			if(!$model) $model = new self();
		}
		return $model;
	}

	public function relations(){
		return array();
	}

	public static function model($className = __CLASS__){
		return parent::model($className);
	}
}
