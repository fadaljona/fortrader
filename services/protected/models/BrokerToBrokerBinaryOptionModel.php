<?
	Yii::import( 'models.base.ModelBase' );

	final class BrokerToBrokerBinaryOptionModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idBroker, $idBinaryOption ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker', 'idBinaryOption' ));
		}
	}

?>