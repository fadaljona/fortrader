<?php
	Yii::import( 'models.base.ModelBase' );
	
	final class InformersToParamsModel extends ModelBase {

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
        }
        
        public static function getInformerId( $paramsArr ){
            if( !$paramsArr ) return false;

            $paramsStr = serialize( $paramsArr );
            $hash = md5( $paramsStr );
            $model = self::model()->find(array(
                'condition' => ' `t`.`hash` = :hash ',
                'params' => array( ':hash' => $hash )
            ));
            if( $model ) return $model->id;

            $model = new self;
            $model->data = $paramsStr;
            $model->hash = $hash;

            if( $model->save() ) return $model->id;
            return false;
        }
        public static function getInformerParams( $id ){
            if( !$id ) return false;
            $model = self::model()->findByPk($id);

            if( !$model ) return false;
            return unserialize( $model->data );

        }
	}
?>