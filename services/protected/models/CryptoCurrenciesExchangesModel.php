<?php
Yii::import('models.base.ModelBase');

final class CryptoCurrenciesExchangesModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function relations()
    {
        return array(
            'symbolsToSymbols' => array(self::HAS_MANY, 'CryptoCurrenciesExchangesSymbolToOurSymbolModel', 'idExchange'),
        );
    }

    public static function getListForFilter()
    {
        $models = self::model()->findAll(array( ));
        $returnArr = array();
        foreach ($models as $model) {
            $returnArr[$model->id] = $model->title;
        }
        return $returnArr;
    }

    public static function getAdminListDp($filterModel = false)
    {
        $DP = new CActiveDataProvider(get_called_class(), array(
            'criteria' => array(
                'order' => ' `t`.`id` DESC ',
            ),
        ));
        return $DP;
    }

    protected function afterSave()
    {
        parent::afterSave();
        $strToParse = CommonLib::downloadFileFromWww($this->statsUrl);

        if (!$strToParse) {
            throw new Exception("Can't get content for {$this->statsUrl}");
        }

        preg_match_all('/>([A-Z]+\/[A-Z]+)</', $strToParse, $symbolsData);

        if (!isset($symbolsData) || !isset($symbolsData[1])) {
            throw new Exception("Can't parse {$this->statsUrl}");
        }

        $existSymbols = array();
        foreach ($this->symbolsToSymbols as $model) {
            $existSymbols[] = $model->symbol;
        }

        foreach ($symbolsData[1] as $symbol) {
            if (in_array($symbol, $existSymbols)) {
                continue;
            }
            $model = CryptoCurrenciesExchangesSymbolToOurSymbolModel::instance($this->id, $symbol);
            if ($model->validate()) {
                $model->save();
            }
        }
    }

    public function getSingleURL()
    {
        return Yii::app()->createUrl('cryptoCurrencies/exchageRedirect', array('id' => $this->id));
    }
}
