<?php

	Yii::import( 'models.base.ModelBase' );
	
	final class WPCommentmetaModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "wp_commentmeta";
		}
		
	}
?>