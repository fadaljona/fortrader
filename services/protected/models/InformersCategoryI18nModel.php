<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InformersCategoryI18nModel extends ModelBase {
		const PATHUploadDir = 'uploads/informers';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}

		function tableName() {
			return "{{informers_category_i18n}}";
		}
		
		static function instance( $idCategory, $idLanguage, $title, $description, $pageTitle, $metaTitle, $metaDesc, $metaKeys, $shortDesc, $fullDesc, $informerTitle, $toolTitle ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idCategory', 'idLanguage', 'title', 'description', 'pageTitle', 'metaTitle', 'metaDesc', 'metaKeys', 'shortDesc', 'fullDesc', 'informerTitle', 'toolTitle' ));
		}
		function getPathImage() {
			return strlen($this->nameImage) ? self::PATHUploadDir."/{$this->nameImage}" : '';
		}
		function getSrcImage() {
			return $this->pathImage ? Yii::app()->baseUrl."/{$this->pathImage}" : '';
		}
		function uploadImage( $uploadedFile ) {
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );

			$this->setImage( $fileFullName );
		}
		function setImage( $name ) {
			$this->deleteImage();
			$this->nameImage = $name;
		}
		function deleteImage() {
			if( strlen( $this->nameImage )) {			
				if( is_file( $this->pathImage ) and !@unlink( $this->pathImage )) throw new Exception( "Can't delete {$this->pathImage}" );
				$this->nameImage = null;
			}
		}
	}
?>