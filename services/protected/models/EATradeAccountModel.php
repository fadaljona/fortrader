<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EATradeAccountModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_trade_account}}";
		}
		function relations() {
			return Array(
				'EA' => Array( self::BELONGS_TO, 'EAModel', 'idEA' ),
				'tradeAccount' => Array( self::BELONGS_TO, 'TradeAccountModel', 'idTradeAccount' ),
				'version' => Array( self::BELONGS_TO, 'EAVersionModel', 'idVersion' ),
				'statement' => Array( self::BELONGS_TO, 'EAStatementModel', 'idStatement' ),
				'broker' => Array( self::BELONGS_TO, 'BrokerModel', 'idBroker' ),
			);
		}
		static function getModelSingleURL( $id ) {
			return Yii::App()->createURL( 'eaTradeAccount/single', Array( 'id' => $id ));
		}
		function getSingleURL() {
			return self::getModelSingleURL( $this->id );
		}
	}

?>