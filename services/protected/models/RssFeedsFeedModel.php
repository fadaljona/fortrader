<?
	Yii::import( 'models.base.ModelBase' );
	
	final class RssFeedsFeedModel extends ModelBase {
		public $childs;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'lang' => Array( self::HAS_ONE, 'LanguageModel', array( 'id' => 'idLanguage' ) ),
				'parentRel' => Array( self::HAS_ONE, 'RssFeedsFeedModel', array( 'id' => 'parent' ) ),
				'group' => Array( self::HAS_ONE, 'RssFeedsGroupModel', array( 'id' => 'idGroup' ) ),
			);
		}
		static function renderFeedTree( $feeds ){
			echo CHtml::openTag('ul');
			foreach( $feeds as $feed ){	
				echo CHtml::openTag('li');
					echo CHtml::link($feed['title'], $feed['link']);
					if( isset( $feed['childs'] ) ) RssFeedsFeedModel::renderFeedTree( $feed['childs'] );
				echo CHtml::closeTag('li');
			}
			echo CHtml::closeTag('ul');
		}
		static function getFeedsByTree(){
			$models = self::model()->findAll(array(
				'with' => array('group'),
				'condition' => " `t`.`enabled` = 1 AND `group`.`enabled` = 1 AND `t`.`idLanguage` = :idLanguage ",
				'order' => " `t`.`parent` ASC, `group`.`order` DESC, `t`.`order` DESC ",
				'params' => Array( ':idLanguage' => LanguageModel::getCurrentLanguageID() ),
			));
			
			$modelsArr = array();
			foreach( $models as $model ){
				$modelsArr[] = array(
					'id' => $model->id,
					'parent' => $model->parent,
					'link' => $model->singleUrl,
					'title' => $model->title,
					'order' => $model->order,
					'groupTitle' => $model->group->title,
					'groupOrder' => $model->group->order,
					'idGroup' => $model->idGroup,
				);
			}
			
			$tree = array(); 
			$sub = array( 0 => &$tree ); 

			foreach ( $modelsArr as $key => $item){ 
				$id = $item['id']; 
				$parent = $item['parent']; 
				$branch = &$sub[$parent]; 
				$branch['childs'][$id] =  $modelsArr[$key]; 
				$sub[$id] = &$branch['childs'][$id]; 
			} 
	
			$outArr = array();
			
			foreach( $tree as $parentFeed ){
				foreach( $parentFeed as $topLevelFeed ){
					$outArr[$topLevelFeed['idGroup']] = array(
						'groupTitle' => $topLevelFeed['groupTitle'],
						'groupOrder' => $topLevelFeed['groupOrder'],
					);
					if( !isset( $outArr[$topLevelFeed['idGroup']]['feeds'] ) ) $outArr[$topLevelFeed['idGroup']]['feeds'] = array();
					$outArr[$topLevelFeed['idGroup']]['feeds'][] = $topLevelFeed;
				}
			}
			return $outArr;

		}
		function getSingleUrl(){
			return Yii::app()->createUrl('feeds/single', array( 'slug' => $this->slug ));
		}
		function getAbsoluteSingleUrl(){
			return Yii::app()->createAbsoluteUrl('feeds/single', array( 'slug' => $this->slug ));
		}
		function getLastPostDate(){
			if( $this->cats == 0 ){
				$excludeArr = array();
				$excludeArr[] = WpOptionsModel::getOptionVal('questions_cat_id');
				$excludeArr[] = WpOptionsModel::getOptionVal('services_cat_id');
				$excludeStr = implode(',', $excludeArr);
				return Yii::App()->db->createCommand(" 
					SELECT posts.post_modified FROM wp_terms terms
					LEFT JOIN wp_term_taxonomy tax ON tax.term_id = terms.term_id
					LEFT JOIN wp_term_relationships rel ON tax.term_taxonomy_id = rel.term_taxonomy_id
					LEFT JOIN wp_posts posts ON posts.ID = rel.object_id
					WHERE posts.post_type = 'post' AND posts.post_status = 'publish' AND terms.term_id NOT IN({$excludeStr})
					ORDER BY posts.post_modified DESC
					LIMIT 1;
				")->queryScalar();
			}else{
				return Yii::App()->db->createCommand(" 
					SELECT posts.post_modified FROM wp_terms terms
					LEFT JOIN wp_term_taxonomy tax ON tax.term_id = terms.term_id
					LEFT JOIN wp_term_relationships rel ON tax.term_taxonomy_id = rel.term_taxonomy_id
					LEFT JOIN wp_posts posts ON posts.ID = rel.object_id
					WHERE posts.post_type = 'post' AND posts.post_status = 'publish' AND terms.term_id IN({$this->cats})
					ORDER BY posts.post_modified DESC
					LIMIT 1;
				")->queryScalar();
			}	
		}
		static function getAllForParent(){
			$models =self::model()->findAll(array(
				'condition' => " `t`.`enabled` = 1 ",
				'order' => " `t`.`order` DESC ",
			));
			$outArr = array( 0 => Yii::t('*', 'No parent') );
			foreach( $models as $model ){
				$outArr[$model->id] = $model->title;
			}
			return $outArr;
		}
		static function getAdminListDp( $filterModel ){
			$c = new CDbCriteria();
			
			$c->order = " `t`.`idGroup` DESC, `t`.`order` DESC ";
			$c->with = array( 'parentRel', 'group' );
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => $c,
			));
			return $DP;
		}
		static function findBySlug( $slug ){
			return self::model()->find(array(
				'condition' => " `slug` = :slug ",
				'params' => array( ':slug' => $slug ),
			));
		}
	}

?>