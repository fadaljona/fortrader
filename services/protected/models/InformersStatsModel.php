<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InformersStatsModel extends ModelBase {
		public $count;
		public $lastDate;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function saveStats( $referer, $st, $cat, $isHtml = 0 ){
			$parsedUrl = parse_url($referer);
			if( !$st || !$cat ) return false;
			if( isset( $parsedUrl['host'] ) ){
				$stat = new self;
				$stat->accessDate = date('Y-m-d H:i:s', time());
				$stat->domain = $parsedUrl['host'];
				$stat->url = $referer;
				$stat->cat = $cat;
				$stat->style = $st;
				$stat->isHtml = $isHtml;
				if( $stat->validate() ) $stat->save();
			}
		}
		
		function rules() {
			return Array(
				Array( 'accessDate', 'date', 'format'=>'yyyy-M-d H:m:s'),
				Array( 'domain, url', 'length', 'max' => 255),
				Array( 'cat, style', 'numerical', 'integerOnly' => true, 'min' => 0 ),
			);
		}
		function relations() {
			return Array(
				'stModel' => Array( self::HAS_ONE, 'InformersStyleModel', array( 'id' => 'style' ) ),
				'catModel' => Array( self::HAS_ONE, 'InformersCategoryModel', array( 'id' => 'cat' ) ),
			);
		}
	}
?>