<?php

//ManageTextContent
Yii::import('components.behaviors.ManageTextContentBehavior');
Yii::import('models.interfaces.ModelForTextReplaceInterface');

class QuotesSymbolsModel extends ModelBase implements ModelForTextReplaceInterface
{
    const PATHUploadQuotesDir = 'uploads/quotes';
    const PATHUploadQuotesIconsDir = 'uploads/quotes/icons';
    const BINANCE_EXCHANGE_INFO = 'https://api.binance.com/api/v1/exchangeInfo';
    
    public $sdefault;
    public $snalias;
    
    public $sdesc;
    public $svisible;
    
    private $todayD1Data = false;
    
    public $countRows; //ManageTextContent
    
    const CACHE_KEY_PREFIX = 'QuotesSymbolsModel.';
    
    
    
    static function getModel(){
        static $model;
        if(!$model){
            $model = self::model()->find();
            if(!$model) $model = new self();
        }
        return $model;
    }
    
    public static function getAvailableBinanceSymbols()
    {
        $exchangeInfo = json_decode(file_get_contents(self::BINANCE_EXCHANGE_INFO));
        if (!empty($exchangeInfo->symbols)) {
            return $exchangeInfo->symbols;
        } else {
            return false;
        }
    }
    
    /*images */

    function uploadIcon( $uploadedFile ) {
        $fileEx = strtolower( $uploadedFile->getExtensionName());
        list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadQuotesIconsDir, $fileEx ));
        
        $fileFullName = "{$fileName}.{$fileEx}";
        $filePath = self::PATHUploadQuotesIconsDir."/{$fileFullName}";
        if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadQuotesIconsDir );

        $fileFullName16 = "{$fileName}-16.{$fileEx}";
        $filePath16 = self::PATHUploadQuotesIconsDir."/{$fileFullName16}";
        ImgResizeLib::resize( $filePath, $filePath16, 16, 16 );
        
        $this->setIcons( $fileFullName, $fileFullName16 );
    }
    function setIcons( $name, $name16 ) {
        $this->deleteIcons();
        $this->nameIcon = $name;
        $this->nameIcon16 = $name16;
    }
    function getPathIcon() {
        return strlen($this->nameIcon) ? self::PATHUploadQuotesIconsDir."/{$this->nameIcon}" : '';
    }
    function getPathIcon16() {
        return strlen($this->nameIcon16) ? self::PATHUploadQuotesIconsDir."/{$this->nameIcon16}" : '';
    }
    function deleteIcons() {
        if( strlen( $this->nameIcon )) {
            if( is_file( $this->pathIcon ) and !@unlink( $this->pathIcon )) throw new Exception( "Can't delete {$this->pathIcon}" );
            $this->nameIcon = null;
        }
        if( strlen( $this->nameIcon16 )) {
            if( is_file( $this->pathIcon16 ) and !@unlink( $this->pathIcon16 )) throw new Exception( "Can't delete {$this->pathIcon16}" );
            $this->nameIcon16 = null;
        }
    }
    function getIcon16Src(){
        return $this->pathIcon16 ? Yii::app()->baseUrl."/{$this->pathIcon16}" : '';
    }
    function getIconSrc(){
        return $this->pathIcon ? Yii::app()->baseUrl."/{$this->pathIcon}" : '';
    }
    function getIcon16Img( $class ){
        if( $this->icon16Src ){
            return CHtml::image($this->icon16Src, '', array('class' => $class));
        }
        return false;
    }
    
    
    /*informers*/
    static function getItemsForInformer( $catId ){
        if( $catId ){
            $models = self::model()->findAll(array(
                'with' => array( 'currentLanguageI18N', 'viewsTable' ),
                'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`sourceType` <> 'yahoo' AND `t`.`category` = :catId ",
                'order' => " `cLI18NQuotesSymbolsModel`.`weightInCat` DESC, `viewsTable`.`views` DESC ",
                'params' => array( ':catId' => $catId ),
            ));
        }else{
            $models = self::model()->findAll(array(
                'with' => array( 'currentLanguageI18N', 'viewsTable', '_category' => array( 'with' => array( 'currentLanguageI18N' ) ) ),
                'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`sourceType` <> 'yahoo' AND `_category`.`is_def` = 0 AND `cLI18NQuotesCategoryModel`.`name` IS NOT NULL AND `cLI18NQuotesCategoryModel`.`name` <> '' ",
                'order' => " `_category`.`order` ASC, `_category`.`id` ASC, `cLI18NQuotesSymbolsModel`.`weightInCat` DESC, `viewsTable`.`views` DESC ",
            ));
        }
        
        $outArr = array();
        foreach( $models as $model ){
            $outArr[$model->id] = array(
                'toInformer' => $model->toInformer,
                'name' => $model->informerItemName
            );
            if( !$catId ){
                $outArr[$model->id]['cat'] = $model->_category->name;
            }
        }
        return $outArr;
    }
    static function getItemsForInformerByLang( $catId, $langId ){
        $models = self::model()->findAll(array(
            'with' => array( 'i18ns', 'viewsTable' ),
            'condition' => " `i18nsQuotesSymbols`.`visible` = 'Visible' AND `t`.`sourceType` = 'own' AND `t`.`category` = :catId AND `i18nsQuotesSymbols`.`idLanguage` = :lang ",
            'order' => "  `viewsTable`.`views` DESC ",
            'params' => array( ':catId' => $catId, ':lang' => $langId ),
        ));
        $outArr = array( 
            'type' => 'multipleCheckbox',
        );
        foreach( $models as $model ){
            $outArr['options'][] = array(
                'value' => $model->id,
                'setted' => $model->getToInformerByLang($langId),
                'label' => $model->getInformerItemNameByLang($langId),
            );
        }
        return $outArr;
    }
    static function getItemsForInformerByIds( $catId, $ids ){    
        $criteria = new CDbCriteria();
        
        if( $catId ){
            $criteria->with = array( 'currentLanguageI18N', 'viewsTable' );
            $criteria->condition = " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`sourceType` <> 'yahoo' AND `t`.`category` = :catId ";
            $criteria->params[':catId'] = $catId ;
            $criteria->order = " `cLI18NQuotesSymbolsModel`.`weightInCat` DESC, `viewsTable`.`views` DESC ";
        }else{
            $criteria->with = array( 'currentLanguageI18N', 'viewsTable', '_category' => array( 'with' => array( 'currentLanguageI18N' ) ) );
            $criteria->condition = " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`sourceType` <> 'yahoo' AND `_category`.`is_def` = 0 AND `cLI18NQuotesCategoryModel`.`name` IS NOT NULL AND `cLI18NQuotesCategoryModel`.`name` <> '' ";
            $criteria->order = " _category.order, `cLI18NQuotesSymbolsModel`.`weightInCat` DESC, `viewsTable`.`views` DESC ";
        }
        
        
        $criteria->addInCondition('`t`.`id`', explode(',', $ids ));
        
        return self::model()->findAll( $criteria );
    }
    function getToInformerByLang($langId) {
        foreach( $this->i18ns as $i18n ) {
            if( $i18n->idLanguage == $langId ) {
                if( $i18n->toInformer ) return 1;
            }
        }
        return 0;
    }
    public function getInformerItemNameByLang($langId) {
        foreach( $this->i18ns as $i18n ) {
            if( $i18n->idLanguage == $langId ) {
                if( $i18n->informerName ) return $i18n->informerName;
            }
        }
        return $this->name;
    }
    public function getInformerItemName(){
        return $this->informerName;
    }
    public function getInformerShortItemName(){
            return $this->informerItemName;
        }
    public function getInformerDiff(){
        if( !$this->tickData ) return 0;
        if( !$this->lastBid ) return 0;
        return $this->tickData->bid - $this->lastBid;
    }
    public function getBid(){
        $bid = $this->tickData->bid;
        if (!$bid) {
            $bid = $this->closeQuote;
        }
        return $bid;
    }
    public function getAsk(){
        if( !$this->tickData ) return Yii::t( '*', 'n/a' );
        return $this->tickData->ask;
    }
    private function setTodayD1Data(){
        if( !$this->todayD1Data ) $this->todayD1Data = QuotesHistoryDataModel::getTodayD1DataForOneSymbol( $this->id );
    }
    public function getOpenQuote(){
        $this->setTodayD1Data();
        if( !$this->todayD1Data ) return false;
        return $this->todayD1Data['open'];
    }
    public function getHighQuote(){
        $this->setTodayD1Data();
        if( !$this->todayD1Data ) return false;
        return $this->todayD1Data['high'];
    }
    public function getLowQuote(){
        $this->setTodayD1Data();
        if( !$this->todayD1Data ) return false;
        return $this->todayD1Data['low'];
    }
    public function getCloseQuote(){
        $this->setTodayD1Data();
        if( !$this->todayD1Data ) return false;
        return $this->todayD1Data['close'];
    }
    public function getBarTimeQuote(){
        $this->setTodayD1Data();
        if( !$this->todayD1Data ) return false;
        return $this->todayD1Data['barTime'];
    }
    public function getChg(){
        if( !$this->tickData ) return Yii::t( '*', 'n/a' );
        if( !$this->lastBid ) return Yii::t( '*', 'n/a' );
        
        $Chg = $this->tickData->bid - $this->lastBid;
        $Chg = number_format($Chg, $this->precision, '.', ' ');
        if( $Chg > 0 ) $Chg = '+'.$Chg;
        return $Chg;
    }
    public function getPrecision(){
        if (!isset($this->tickData->precision)) {
            $precision = strlen($this->bid) - strpos($this->bid, '.') - 1;
        } else {
            $precision = $this->tickData->precision;
        }
        return $precision;
    }
    public function getChgPer(){
        if( !$this->tickData ) return Yii::t( '*', 'n/a' );
        if( !$this->lastBid ) return Yii::t( '*', 'n/a' );
    
        $Chg = $this->tickData->bid - $this->lastBid;
        $ChgPercent = $Chg / $this->lastBid * 100 ;
        $ChgPercent = number_format($ChgPercent, $this->precision, '.', ' ');
        if( $Chg > 0 ) $ChgPercent = '+'.$ChgPercent;
        return $ChgPercent;        
    }
    public function getTime(){
        if( !$this->tickData ) return Yii::t( '*', 'n/a' );
        return $this->tickData->time;
    }
    public function getFlagSrc(){
        return false;
    } 
    public function getFlagSrcBig(){
        return false;
    }
    public function getFaSymbol(){
        return false;
    }
    public function getInformerTooltip(){
        if( $this->tooltip ) return $this->tooltip;
        return $this->name;
    }
    

    
    
    
    
    
    
    
    static function getIdByHistNameAndAlias( $replacementArr, $symbol ){
        
        $cacheKey = 'quoteAliasToSymbolId' . $symbol;
        $symbolId = Yii::app()->cache->get( $cacheKey );
        if( $symbolId !== false ){
            if( !$symbolId ) return false;
            return $symbolId;
        } 
        
        $replacedAlias = preg_replace( array_keys($replacementArr), array_values($replacementArr), $symbol );
                
        $symbolModel = QuotesSymbolsModel::model()->find(array(
            'condition' => " `t`.`histName` = :histName ",
            'params' => array( ':histName' => $replacedAlias )
        ));
                
        $symbolId = 0;
        if( $symbolModel ){
            Yii::app()->cache->set( $cacheKey, $symbolModel->id, 60*10);
            return $symbolModel->id;
                    
        }else{
    
            $aliasModel = QuotesHistAliasModel::model()->find(array(
                'condition' => " `t`.`alias` = :histName ",
                'params' => array( ':histName' => $symbol )
            ));
            if( $aliasModel ){
                Yii::app()->cache->set( $cacheKey, $aliasModel->symbolId, 60*10);
                return $aliasModel->symbolId;
            }  
        }
        
        Yii::app()->cache->set( $cacheKey, 0, 60*10);        
        return false;
        
    }
    static function getHistDataByHistAlias( $alias, $replacementStr, $daysTodisplayGraph ){
        
        $cacheKey = 'contestMemberDealsChartData' . $alias;
        if( Yii::app()->cache->get( $cacheKey ) ) return Yii::app()->cache->get( $cacheKey );
        
        eval("\$replacementArr = $replacementStr;");

        $replacedAlias = preg_replace( array_keys($replacementArr), array_values($replacementArr), $alias );
        
        $symbolModel = self::model()->find(array(
            'condition' => " `t`.`histName` = :histName ",
            'params' => array( ':histName' => $replacedAlias )
        ));
        
        $symbolId = 0;
        if( $symbolModel ){
            $symbolId = $symbolModel->id;
        }else{
            $aliasModel = QuotesHistAliasModel::model()->find(array(
                'condition' => " `t`.`alias` = :histName ",
                'params' => array( ':histName' => $alias )
            ));
            if( $aliasModel )  $symbolId = $aliasModel->symbolId;
        }
        
        if( !$symbolId ) return false;
        
        $chartData = QuotesHistoryDataModel::getDataForContestDealsChart($symbolId, $daysTodisplayGraph);
        
        Yii::app()->cache->set( $cacheKey, $chartData, 60*10);
        
        return $chartData;
    }
    static function createYahooQuote( $symbol, $symbolName, $tickName){
        $existName = self::findBySlug( strtolower($symbol) );
        if( $existName ){
            $name = $symbol . rand(1, 100);
        }else{
            $name = $symbol;
        }
        $model = new self;
        $model->name = $name;
        $model->nalias = $symbolName;
        $model->visible = 'Visible';
        $model->histName = $symbol;
        $model->tickName = $tickName;
        $model->sourceType = 'yahoo';
        if( $model->validate() ) 
            if( $model->save() ) return true;
        return false;
    }
    public static function getIdsPanelDefault(){
        $visible = '`cLI18NQuotesSymbolsModel`.`visible`="Visible"';
        $ids_ = self::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'select'=>'id',
            'condition'=>$visible.' AND `t`.`panel`="Yes"'
        ));
        $ids =array();
        foreach($ids_ as $id){
            $ids[]=$id->id;
        }
        
        return $ids;
    }
    static function getSlugById( $id ){
        $model = self::model()->findByPk( $id );
        if( !$model ) return false;
        return $model->slug;
    }
    public function getSlug(){
        return strtolower( $this->name );
    }
    
    //ManageTextContent
    function getSingleURL() {
        return Yii::App()->createURL( 'quotesNew/single', Array( 'slug' => $this->slug ));
    }
    function getAbsoluteSingleURL() {
        return Yii::App()->createAbsoluteURL( 'quotesNew/single', Array( 'slug' => $this->slug ));
    }
    public function getShortTitle(){
        if( $this->nalias ){
            $title = $this->nalias;
        }else{
            $title = $this->name;
        }
        return $title;
    }
    public function getSingleTitle(){
        if( $this->title ){
            $title = $this->title;
        }elseif( $this->nalias ){
            $title = $this->nalias;
        }else{
            $title = $this->name;
        }
        return $title;
    }
    
    public static function findBySlug( $slug ){
        return self::model()->find(array(
            'condition' => " `name` = :slug ",
            'params' => array( ':slug' => $slug ),
        ));
    }
    public function getAvailableLangs(){
        $langsArr = array();
        foreach( $this->i18ns as $i18n ){
            if( $i18n->nalias ){
                $langsArr[] = LanguageModel::getLangFromLangsFileByID( $i18n->idLanguage );
            }
        }
        return $langsArr;
    }
    static function findBySlugWithLang( $slug ){
        return self::model()->find(array(
            'with' => Array( 'currentLanguageI18N' ),
            'condition' => " `name` = :slug AND ((`cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '') OR (`cLI18NQuotesSymbolsModel`.`desc` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`desc` <> '') OR (`cLI18NQuotesSymbolsModel`.`title` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`title` <> '') OR (`cLI18NQuotesSymbolsModel`.`metaTitle` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`metaTitle` <> '') OR (`cLI18NQuotesSymbolsModel`.`metaDescription` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`metaDescription` <> '') OR (`cLI18NQuotesSymbolsModel`.`tooltip` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`tooltip` <> '') OR (`cLI18NQuotesSymbolsModel`.`preview` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`preview` <> '') OR (`cLI18NQuotesSymbolsModel`.`chartTitle` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`chartTitle` <> '') OR (`cLI18NQuotesSymbolsModel`.`wpPostsTitle` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`wpPostsTitle` <> '') OR (`cLI18NQuotesSymbolsModel`.`metaKeywords` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`metaKeywords` <> '') OR (`cLI18NQuotesSymbolsModel`.`chartLabel` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`chartLabel` <> '') OR (`cLI18NQuotesSymbolsModel`.`informerName` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`informerName` <> '') ) ",
            'params' => array( ':slug' => $slug ),
        ));
    }
    
    public static function findByName( $slug ){
        return self::model()->find(array(
            'condition' => " `name` = :slug ",
            'params' => array( ':slug' => $slug ),
        ));
    }
    public static function getQuotesPanelDefault(){
        $visible = '`visible`="Visible"';
        $quotes = self::model()->findAll(array(
            'select'=>'*',
            'condition'=>$visible.' AND `panel`="Yes"'
        ));
        
        return $quotes;
    }

    public function getCategory(){
        $id = $this->getAttribute('category');
        if($id === 0)
            return 'Main';
        elseif($id){
            $cat = QuotesCategoryModel::model()->find('id=' . (int)$id);
            return trim($cat->name);
        }else return '';
    }
    static function parseQuotes(){
        $quotesSettings = QuotesSettingsModel::getInstance();

        $quotesStr = file_get_contents( $quotesSettings->files );
        $quotesStr = str_replace(',', '.', $quotesStr);
        $quotes = explode("\r\n", $quotesStr);
        if( !$quotes ) return false;
        $quotesArr = array();
        foreach( $quotes as $quote ){
            $tmpArr = explode(';', $quote);
            $name = @$tmpArr[0];
            if(!$name) continue;
            $tmpData = array(
                'bid' => @$tmpArr[1],
                'ask' => @$tmpArr[2],
                'time' => @$tmpArr[3],
                'spread' => @$tmpArr[4],
                'trend' => @$tmpArr[5],
                'precision' => @$tmpArr[8],
                'pcp' => @$tmpArr[9],
            );
            $quotesArr[$name] = $tmpData;
        }
        return $quotesArr;
    }
    public function createTaxonomy( $idLanguage ){
        CommonLib::loadWp();
        
        $existTerm = get_term_by( 'slug', strtolower( $this->name ) . $idLanguage, 'quotes' );

        if( $existTerm ){
            wp_delete_term( $existTerm->term_id, 'quotes' );
        }
        
        $newWpTerm = wp_insert_term( 
            $this->name . $idLanguage, 
            'quotes', 
            array(
                'description' => 'Auto inserted',
                'slug' => strtolower( $this->name ) . $idLanguage,
            ) 
        );
    
        $in = $this->getI18NbyLangId( $idLanguage );
        $in->wpTerm = $newWpTerm['term_id'];
        if( $in->validate() ) $in->save();
    }
    public function getImgData( $data ){
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        return base64_decode($data);
    }
    public function mkpath( $path ){
        if( file_exists( $path ) || @mkdir( $path ) ) return true;
        return ( $this->mkpath ( dirname( $path ) ) and mkdir( $path ) );
    }
    public function saveBase64Img( $img ){
        $img = $this->getImgData( $img );
        $path = Yii::app()->basePath . '/../' . self::PATHUploadQuotesDir;
        if( $this->mkpath( $path ) ){
            file_put_contents( $path . '/' . $this->name . '.jpg' , $img);
        }
    }
    public function needToUpdateImg(){
        $path = Yii::app()->basePath . '/../' . self::PATHUploadQuotesDir;
        $file = $path . '/' . $this->name . '.jpg';
        clearstatcache(); 
        if( !file_exists($file) ) return true;
        if( time() - filemtime($file) > 60*60*24 ) return true;
        return false;
    }
    public function getOgImage(){
        $path = Yii::app()->basePath . '/../' . self::PATHUploadQuotesDir;
        if( file_exists( $path . '/' . $this->name . '.jpg' ) ) return Yii::App()->request->getHostInfo() . '/' . self::PATHUploadQuotesDir . '/' . $this->name . '.jpg';
        return '';
    }
    public function getLastDayData(){
        return $this->lastOneDayData[0];
    }
    public function getLastBid(){
        if( !$this->lastOneDayData[0] ) return false;
        if( $this->sourceType != 'yahoo' ){
            return $this->lastOneDayData[0]->close;
        }else{
            $model = QuotesHistoryDataModel::model()->find(array(
                'order' => 'barTime DESC', 
                'condition' => " resolution = 1440 AND symbolId = :id ",
                'params' => array( ':id' => $this->id ),
                'limit' => 1,
                'offset' => 1
            ));
            if( !$model ) return false;
            return $model->close;
        }
    }
    public function getCurrentD1Bid(){
        $model = QuotesHistoryDataModel::model()->find(array(
            'order' => 'barTime DESC', 
            'condition' => " resolution = 1440 AND symbolId = :id ",
            'params' => array( ':id' => $this->id ),
            'limit' => 1,
        ));
        if( !$model ) return false;
        return $model->close;
    }
    public function getLastBidForYahoo(){
        if($this->sourceType == 'own') return false;
        $model = QuotesHistoryDataModel::model()->find(array(
            'order' => 'barTime DESC', 
            'condition' => " resolution = 1440 AND symbolId = :id ",
            'params' => array( ':id' => $this->id ),
            'limit' => 1,
        ));
        if( !$model ) return false;
        return $model->close;
    }
    public function getViews(){
        if( !$this->viewsTable ) return false;
        return $this->viewsTable->views;
    }
    public function viewed(){
        QuotesViewsModel::saveViews($this->id);
    }
    
    protected function beforeDelete() {
        QuotesHistAliasModel::model()->deleteAll(
            " `symbolId` = :id ", 
            array(':id' => $this->id ) 
        );
        QuotesSymbolKeywordModel::model()->deleteAll(
            " `symbolId` = :id ", 
            array(':id' => $this->id ) 
        );
        QuotesForecastsModel::model()->deleteAll(
            " `symbolId` = :id ", 
            array(':id' => $this->id ) 
        );
        QuotesHistoryDataModel::model()->deleteAll(
            " `symbolId` = :id ", 
            array(':id' => $this->id ) 
        );
        QuotesViewsModel::model()->deleteAll(
            " `symbolId` = :id ", 
            array(':id' => $this->id ) 
        );
        return parent::beforeDelete();
    }
    protected function afterSave() {
        Yii::app()->cache->delete( 'quotesWpPostBlock' . $this->id );
        parent::afterSave();
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'nalias' => 'NAlias',
            'desc' => 'Desc',
            'panel' => 'Panel',
            'category' => 'Category',
        );
    }

    public function tableName(){
        return '{{quotes_symbols}}';
    }
    public function getAliasName(){
        if( $this->nalias ){
            return "{$this->nalias} ({$this->name})";
        }else{
            return $this->name;
        }
    }

    protected function afterFind (){
        
        $quotesSettings = QuotesSettingsModel::getInstance();
        
        if( !$this->currentLanguageI18N->title && $quotesSettings->titleTemplate ) eval( $quotesSettings->titleTemplate );
        if( !$this->currentLanguageI18N->metaTitle && $quotesSettings->metaTitleTemplate ) eval( $quotesSettings->metaTitleTemplate );
        if( !$this->currentLanguageI18N->chartTitle && $quotesSettings->chartTitleTemplate ) eval( $quotesSettings->chartTitleTemplate );
        if( !$this->currentLanguageI18N->chartLabel && $quotesSettings->chartLabelTemplate ) eval( $quotesSettings->chartLabelTemplate );
        if( !$this->currentLanguageI18N->wpPostsTitle && $quotesSettings->wpPostsTitleTemplate ) eval( $quotesSettings->wpPostsTitleTemplate );
        if( !$this->currentLanguageI18N->preview && $quotesSettings->previewTemplate ) eval( $quotesSettings->previewTemplate );
        if( !$this->currentLanguageI18N->metaDescription && $quotesSettings->metaDescriptionTemplate ) eval( $quotesSettings->metaDescriptionTemplate );
        if( !$this->currentLanguageI18N->metaKeywords && $quotesSettings->metaKeywordsTemplate ) eval( $quotesSettings->metaKeywordsTemplate );

        parent::afterFind ();
    }
    
    public function getInformerName() {
        $i18n = $this->getI18N();
        if( !$i18n ) return '';
        if( !$i18n->informerName ) return $this->name;
        return $i18n->informerName;
    }

    public function getCryptoCurrencyTitle() {
        $i18n = $this->getI18N();
        if( !$i18n ) return '';
        if( !$i18n->cryptoCurrencyTitle ) return $this->name;
        return $i18n->cryptoCurrencyTitle;
    }

    public function getCryptoCurrencySubTitle() {
        $i18n = $this->getI18N();
        if( !$i18n ) return '';
        if( !$i18n->cryptoCurrencySubTitle ) return false;
        return $i18n->cryptoCurrencySubTitle;
    }

    function getNalias() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->nalias : '';
    }
    function getDesc() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->desc : '';
    }
    function getDefault() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->default : '';
    }
    function getVisible() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->visible : '';
    }
    function getToChartList() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->toChartList : '';
    }
    function getTitle() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->title : '';
    }
    function getMetaTitle() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->metaTitle : '';
    }
    function getWpTerm() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->wpTerm : '';
    }
    function getMetaDescription() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->metaDescription : '';
    }
    function getTooltip() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->tooltip : '';
    }
    function getPreview() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->preview : '';
    }
    function getToInformer() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->toInformer : '';
    }
    function getChartTitle() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->chartTitle : '';
    }
    function getWpPostsTitle() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->wpPostsTitle : '';
    }
    function getMetaKeywords() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->metaKeywords : '';
    }
    function getChartLabel() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->chartLabel : '';
    }
    function getSearchPhrases() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->searchPhrases : '';
    }
    function getWeightInCat() {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->weightInCat : '';
    }
    public function getTickData(){
        if( $this->sourceType == 'bitz' ){

            if( $this->convertToUsd ){
                $usdModel = QuotesTickDataModel::model()->find(array(
                    'condition' => ' `t`.`symbol` = :symbol ',
                    'params' => array( ':symbol' => substr($this->bitzSymbol, strpos($this->bitzSymbol, '_')+1) . '_usdt' ),
                ));
                if( !$usdModel ) return false;

                $tickModel = QuotesTickDataModel::model()->find(array(
                    'condition' => ' `t`.`symbol` = :symbol ',
                    'params' => array( ':symbol' => $this->bitzSymbol ),
                ));
                if( !$tickModel ) return false;

                $returnModel = $tickModel;
                $returnModel->bid = CommonLib::roundQuotesData($usdModel->bid * $tickModel->bid);
                $returnModel->ask = CommonLib::roundQuotesData($usdModel->ask * $tickModel->ask);

                

                return $returnModel;
            }else{
                return QuotesTickDataModel::model()->find(array(
                    'condition' => ' `t`.`symbol` = :symbol ',
                    'params' => array( ':symbol' => $this->bitzSymbol ),
                ));
            }
        } elseif ($this->sourceType == 'binance') {
            if( $this->convertToUsd ){
                $usdHistName = '';
                $histName = $this->histName;

                if (strpos($histName, 'BTC') && strpos($histName, 'BTC') + 3 == strlen($histName)) {
                    $usdHistName = 'BTCUSDT';
                } elseif (strpos($histName, 'ETH') && strpos($histName, 'ETH') + 3 == strlen($histName)) {
                    $usdHistName = 'ETHUSDT';
                } elseif (strpos($histName, 'BNB') && strpos($histName, 'BNB') + 3 == strlen($histName)) {
                    $usdHistName = 'BNBUSDT';
                }

                if (!$usdHistName) {
                    return false;
                }

                $usdModel = QuotesTickDataModel::model()->find(array(
                    'condition' => ' `t`.`symbol` = :symbol ',
                    'params' => array( ':symbol' => $usdHistName),
                ));
                
                if( !$usdModel ) return false;

                $tickModel = QuotesTickDataModel::model()->find(array(
                    'condition' => ' `t`.`symbol` = :symbol ',
                    'params' => array( ':symbol' => $this->histName ),
                ));
                if( !$tickModel ) return false;

                $returnModel = $tickModel;
                $returnModel->bid = CommonLib::roundQuotesData($usdModel->bid * $tickModel->bid);
                $returnModel->ask = CommonLib::roundQuotesData($usdModel->ask * $tickModel->ask);

                return $returnModel;
            }else{
                return QuotesTickDataModel::model()->find(array(
                    'condition' => ' `t`.`symbol` = :symbol ',
                    'params' => array( ':symbol' => $this->histName ),
                ));
            }
        } else {
            if( !$this->tickName ) return false;
            return QuotesTickDataModel::model()->find(array(
                'condition' => ' `t`.`symbol` = :symbol ',
                'params' => array( ':symbol' => $this->tickName ),
            ));
        }
    }

    function getI18NbyLangId( $langId ) {
        foreach( $this->i18ns as $i18n ) {
            if( $i18n->idLanguage == $langId ) {
                return $i18n;
                break;
            }
        }
    }
    
    function getI18N() {
        if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
        foreach( $this->i18ns as $i18n ) {
            if( $i18n->idLanguage == 0 ) {
                if( strlen( $i18n->name )) return $i18n;
                break;
            }
        }
        foreach( $this->i18ns as $i18n ) {
            if( strlen( $i18n->name )) return $i18n;
        }
    }
    
    function deleteI18Ns() {
        foreach( $this->i18ns as $i18n ) {
            $i18n->delete();
        }
    }
    function addI18Ns( $i18ns ) {
        $idsLanguages = LanguageModel::getExistsIDS();
        foreach( $i18ns as $idLanguage=>$obj ) {
            if( in_array( $idLanguage, $idsLanguages ) ) {
                $i18n = QuotesSymbolsI18NModel::model()->findByAttributes( Array( 'idSymbol' => $this->id, 'idLanguage' => $idLanguage ));
                if( !$i18n ) {
                    $i18n = QuotesSymbolsI18NModel::instance( $this->id, $idLanguage, $obj->nalias, $obj->desc, $obj->default, $obj->visible, $obj->toChartList, $obj->title, $obj->metaTitle, $obj->metaDescription, $obj->tooltip, $obj->preview, $obj->toInformer, $obj->chartTitle, $obj->wpPostsTitle, $obj->metaKeywords, $obj->chartLabel, $obj->searchPhrases, $obj->weightInCat, $obj->informerName, $obj->cryptoCurrencyTitle, $obj->cryptoCurrencySubTitle);
                }else{
                    $i18n->nalias = $obj->nalias;
                    $i18n->desc = $obj->desc;
                    $i18n->default = $obj->default;
                    $i18n->visible = $obj->visible;
                    $i18n->toChartList = $obj->toChartList;
                    $i18n->title = $obj->title;
                    $i18n->metaTitle = $obj->metaTitle;
                    $i18n->metaDescription = $obj->metaDescription;
                    $i18n->tooltip = $obj->tooltip;
                    $i18n->preview = $obj->preview;
                    $i18n->toInformer = $obj->toInformer;
                    $i18n->chartTitle = $obj->chartTitle;
                    $i18n->wpPostsTitle = $obj->wpPostsTitle;
                    $i18n->metaKeywords = $obj->metaKeywords;
                    $i18n->chartLabel = $obj->chartLabel;
                    $i18n->searchPhrases = $obj->searchPhrases;
                    $i18n->weightInCat = $obj->weightInCat;
                    $i18n->informerName = $obj->informerName;
                    $i18n->cryptoCurrencyTitle = $obj->cryptoCurrencyTitle;
                    $i18n->cryptoCurrencySubTitle = $obj->cryptoCurrencySubTitle;
                }
            $i18n->save();
            }
        }
    }
    function setI18Ns( $i18ns ) {
        $this->addI18Ns( $i18ns );
    }
    protected function afterDelete() {
        parent::afterDelete();
        $this->deleteI18Ns();
    }
    
    public function relations(){
        return array(
        
            //ManageTextContent
                'i18nsManageTextContent' => Array( self::HAS_MANY, 'QuotesSymbolsI18NModel', array('idSymbol' => 'id') ), 
                'ruLanguageI18NManageTextContent' => Array( self::HAS_ONE, 'QuotesSymbolsI18NModel', array('idSymbol' => 'id'), 'on' => '`ruLanguageI18NManageTextContent`.`idLanguage` = 1'),
                'enLanguageI18NManageTextContent' => Array( self::HAS_ONE, 'QuotesSymbolsI18NModel', array('idSymbol' => 'id'), 'on' => '`enLanguageI18NManageTextContent`.`idLanguage` = 0'),
            //ManageTextContent
        
            '_category'=>array(self::BELONGS_TO,'QuotesCategoryModel','category'),
            'lastOneDayData' => array( 
                self::HAS_MANY, 
                'QuotesHistoryDataModel', 
                'symbolId', 
                'order' => 'barTime DESC', 
                'condition' => " resolution = 1440 AND  barTime < :time ",
                'params' => array( ':time' => time() - 60*60*24 ),
                'limit' => 1 
            ),
            //'tickData' => array(self::HAS_ONE,'QuotesTickDataModel', array( 'symbol' => 'tickName' )),
            'viewsTable' => array( self::HAS_ONE, 'QuotesViewsModel', 'symbolId'),
            
            'i18ns' => Array( self::HAS_MANY, 'QuotesSymbolsI18NModel', 'idSymbol', 'alias' => 'i18nsQuotesSymbols' ),
            'currentLanguageI18N' => Array( self::HAS_ONE, 'QuotesSymbolsI18NModel', 'idSymbol', 
                'alias' => 'cLI18NQuotesSymbolsModel',
                'on' => '`cLI18NQuotesSymbolsModel`.`idLanguage` = :idLanguage',
                'params' => Array(
                    ':idLanguage' => LanguageModel::getCurrentLanguageID(),
                ),
            ),
            
            'histAliases' => Array( self::HAS_MANY, 'QuotesHistAliasModel', array('symbolId' => 'id') ), 
        );
    }
    
    
    //ManageTextContent
    public function behaviors(){
        return array(
            'manageTextContentBehavior' => array(
                'class' => 'ManageTextContentBehavior',
                'filterCondition' => ' `t`.`sourceType` = "own" '
            ),
        );
    }

        
        

    public static function model($className = __CLASS__){
        return parent::model($className);
    }


    //ModelForTextReplaceInterface

    public static function getPropertiesForSearch()
    {
        return [
            'openQuote' => 'today D1 open',
            'highQuote' => 'today D1 high',
            'lowQuote' => 'today D1 low',
            'closeQuote' => 'today D1 close',
            'chg' => 'chg',
            'chgPer' => 'chg percent',
            'slug' => 'slug',
            'singleURL' => 'page url',
            'absoluteSingleURL' => 'absolute page url',
            'shortTitle' => 'nalias or name',
            'singleTitle' => 'page title',
            'lastBid' => 'last bid',
            'cryptoCurrencyTitle' => 'crypto currency title',
            'cryptoCurrencySubTitle' => 'crypto currency sub title',
            'nalias' => 'nalias',
            'title' => 'title'
        ];
    }

    public static function getModelsListForUseInReplace()
    {
        $outArr = [];
        $models = self::model()->findAll(array(
            'with' => array( 'currentLanguageI18N'),
            'condition' => ' `t`.`category` <> 0 and `t`.`category` IS NOT NULL ',
        ));

        foreach ($models as $model) {
            $outArr[$model->id] = $model->shortTitle;
        }

        return $outArr;
    }
}
