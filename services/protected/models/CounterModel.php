<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CounterModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getCurrentLanguageAlias() {
			$currentLanguageAlias = LanguageModel::getCurrentLanguageAlias();
			
			if( $currentLanguageAlias == 'en_us' ) {
				$currentLanguageAlias = 'en';
			}
			
			return $currentLanguageAlias;
		}
		function getCode() {
			$code = $this->code;
			
			$code = strtr( $code, Array(
				'{currentLanguageAlias}' => self::getCurrentLanguageAlias(),
			));
			
			return $code;
		}
	}

?>