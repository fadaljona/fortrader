<?
	Yii::import( 'models.base.SettingsModelBase' );
	
	final class RssFeedsSettingsModel extends SettingsModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) {
					$model = new self();
					$model->id = 1;
				}
			}
			return $model;
		}
		
		public function getTitle(){
			if( !$this->texts || !$this->texts->title ) return false;
			return $this->texts->title;
		}
		public function getMetaTitle(){
			if( !$this->texts || !$this->texts->metaTitle ) return false;
			return $this->texts->metaTitle;
		}
		public function getMetaDesc(){
			if( !$this->texts || !$this->texts->metaDesc ) return false;
			return $this->texts->metaDesc;
		}
		public function getMetaKeys(){
			if( !$this->texts || !$this->texts->metaKeys ) return false;
			return $this->texts->metaKeys;
		}
		public function getShortDesc(){
			if( !$this->texts || !$this->texts->shortDesc ) return false;
			return $this->texts->shortDesc;
		}
		public function getFullDesc(){
			if( !$this->texts || !$this->texts->fullDesc ) return false;
			return $this->texts->fullDesc;
		}
	}
?>