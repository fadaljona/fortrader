<?
	Yii::import( 'models.base.ModelBase' );

	final class MT5AccountTradesModel extends ModelBase {
		
		public $SUM_DEAL_VOLUME, $SUM_DEAL_SWAP, $SUM_DEAL_PROFIT, $DEALS_COUNT;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "mt5_account_trades";
		}
		function getDealTypeTitle() {
			switch( $this->DEAL_TYPE ) {
				case 0:{
					return 'buy';
				}
				case 1:{
					return 'sell';
				}
				case 2:{
					return 'buylimit';
				}
				case 3:{
					return 'selllimit';
				}
				case 4:{
					return 'buystop';
				}
				case 5:{
					return 'sellstop';
				}
				case 6:{
					return 'balance';
				}
			}
		}
	}

?>
