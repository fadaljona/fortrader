<?
	Yii::import( 'models.base.ModelBase' );
	
	final class WPTermRelationshipModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "wp_term_relationships";
		}
		function relations() {
			return Array(
				'termTaxonomy' => Array( self::BELONGS_TO, 'WPTermTaxonomyModel', 'term_taxonomy_id' ),
				'categoryRatingPost' => Array( self::HAS_ONE, 'CategoryRatingPostModel', array( 'ID' => 'object_id' ) ),
			);
		}
	}

?>