<?
	Yii::import( 'models.base.ModelBase' );
	
	final class JournalHistoryModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function downloaded( $idJournal, $idLanguage ) {
			Yii::App()->db->createCommand("
				INSERT INTO 
					`{{journal_history}}`
				SET
					`idJournal` = :idJournal,
					`idLanguage` = :idLanguage,
					`date` = CURDATE(),
					`countDownloads` = 1
				ON DUPLICATE KEY UPDATE
					`countDownloads` = `countDownloads` + 1
			")->query(Array(
				':idJournal' => $idJournal,
				':idLanguage' => $idLanguage,
			));
		}
		static function findToday( $idJournal, $idLanguage ) {
			return self::model()->findBySql("
				SELECT 
					* 
				FROM 
					`{{journal_history}}`
				WHERE
					`idJournal` = :idJournal
					AND `idLanguage` = :idLanguage
					AND `date` = CURDATE()
				LIMIT
					1
			", Array(
				':idJournal' => $idJournal,
				':idLanguage' => $idLanguage,
			));
		}
	}

?>