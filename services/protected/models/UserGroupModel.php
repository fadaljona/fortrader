<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserGroupModel extends ModelBase {
		const PATHAvailableIconClasses = 'protected/data/availableIconClasses.txt';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $name, $iconClass, $additionalInformation ) {
			$model = new self();
			$model->name = $name;
			$model->iconClass = $iconClass;
			$model->additionalInformation = $additionalInformation;
			return $model;
		}
		static function fromAssoc( $assoc ) {
			return self::modelFromAssoc( __CLASS__, $assoc );
		}
		function relations() {
			return Array(
				'rights' => Array( self::MANY_MANY, 'UserRightModel', '{{user_group_to_user_right}}(idUserGroup,idUserRight)' ),
				'linksToRights' => Array( self::HAS_MANY, 'UserGroupToUserRightModel', 'idUserGroup' ),
				'linksToUsers' => Array( self::HAS_MANY, 'UserToUserGroupModel', 'idUserGroup' ),
			);
		}
		static function getAvailableIconClasses() {
			static $classes;
			if( !$classes ) {
				$classes = file( self::PATHAvailableIconClasses, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES  );
				foreach( $classes as &$class ) {
					$class = trim( $class );
				}
			}
			return $classes;
		}
		function getIcon() {
			$NSi18n = $this->getNSi18n();
			$title = Yii::t( $NSi18n, $this->name );
			
			$tag = CHtml::openTag( 'i', Array( 'class' => $this->iconClass, 'title' => $title ));
			$tag .= CHtml::closeTag( 'i' );
			
			return $tag;
		}
			# rights
		function getIDsRights() {
			$ids = Array();
			foreach( $this->rights as $right ) {
				$ids[] = $right->id;
			}
			return $ids;
		}
		function unlinkFromRights() {
			foreach( $this->linksToRights as $link ) {
				$link->delete();
			}
		}
		function linkToRights( $ids ) {
			foreach( $ids as $idRight ) {
				$link = UserGroupToUserRightModel::model()->findByAttributes( Array( 'idUserGroup' => $this->id, 'idUserRight' => $idRight ));
				if( !$link ) {
					$link = UserGroupToUserRightModel::instance( $this->id, $idRight );
					$link->save();
				}
			}
		}
		function setRights( $ids ) {
			$oldIDs = $this->getIDsRights();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromRights();
				$this->linkToRights( $ids );
			}
		}
			# users
		function unlinkFromUsers() {
			foreach( $this->linksToUsers as $link ) {
				$link->delete();
			}
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->unlinkFromRights();
			$this->unlinkFromUsers();
		}
	}

?>