<?php
Yii::import('models.base.ModelBase');

final class PaymentExchangerItemMarkModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }
    public static function instance($idExchanger, $idUser, $value)
    {
        return self::modelFromAssoc(__CLASS__, compact('idExchanger', 'idUser', 'value'));
    }
}
