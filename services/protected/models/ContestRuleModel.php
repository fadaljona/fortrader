<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContestRuleModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idContest, $idLanguage, $text ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idContest', 'idLanguage', 'text' ));
		}
	}

?>