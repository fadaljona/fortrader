<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserNoticeI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{user_notice_i18n}}";
		}
		static function instance( $idNotice, $idLanguage, $message ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idNotice', 'idLanguage', 'message' ));
		}
	}

?>