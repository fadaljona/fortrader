<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAStatementI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_statement_i18n}}";
		}
		static function instance( $idStatement, $idLanguage, $title, $desc ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idStatement', 'idLanguage', 'title', 'desc' ));
		}
	}

?>