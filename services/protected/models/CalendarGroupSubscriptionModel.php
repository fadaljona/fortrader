<?php
	Yii::import( 'models.base.ModelBase' );

	final class CalendarGroupSubscriptionModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}

		function relations() {
			return Array(
				'countries' => Array( self::HAS_MANY, 'CalendarGroupSubscriptionUserToCountryModel', 'idUser' ),
				'seriouses' => Array( self::HAS_MANY, 'CalendarGroupSubscriptionUserToSeriousModel', 'idUser' ),
			);
		}

		public function setCountries( $countries ){
			$this->unlinkCountries();

			if( $countries ){
				$countries = explode(',', $countries);
				foreach( $countries as $country ) {
					$link = CalendarGroupSubscriptionUserToCountryModel::instance( $this->idUser, $country );
					$link->save();
				}
			}
		}
		public function unlinkCountries(){
			CalendarGroupSubscriptionUserToCountryModel::model()->deleteAll(
				" `idUser` = :id ", 
				array(':id' => $this->idUser ) 
			);
		}
		public function setSerious( $seriouses ){
			$this->unlinkSeriouses();

			if( $seriouses ){
				$seriouses = explode(',', $seriouses);
				foreach( $seriouses as $serious ) {
					$link = CalendarGroupSubscriptionUserToSeriousModel::instance( $this->idUser, $serious );
					$link->save();
				}
			}
		}
		public function unlinkSeriouses(){
			CalendarGroupSubscriptionUserToSeriousModel::model()->deleteAll(
				" `idUser` = :id ", 
				array(':id' => $this->idUser ) 
			);
		}


		protected function afterDelete() {
			parent::afterDelete();
			$this->unlinkSeriouses();
			$this->unlinkCountries();
		}
	}

?>
