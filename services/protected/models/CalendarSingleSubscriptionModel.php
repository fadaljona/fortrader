<?php
	Yii::import( 'models.base.ModelBase' );
	
	final class CalendarSingleSubscriptionModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}

		function relations() {
			return Array(
				'event' => Array( self::HAS_ONE, 'CalendarEvent2Model', array( 'id' => 'idEvent' ) ),
			);
		}
		
		public static function findByIdUserIdEvent( $idUser, $idEvent ){
			return self::model()->find('idEvent = ? AND idUser = ? ', array($idEvent, $idUser));
		}
	}

?>