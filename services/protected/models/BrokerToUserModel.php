<?
	Yii::import( 'models.base.ModelBase' );

	final class BrokerToUserModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idBroker, $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker', 'idUser' ));
		}
	}

?>