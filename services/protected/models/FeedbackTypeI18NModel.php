<?
	Yii::import( 'models.base.ModelBase' );
	
	final class FeedbackTypeI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{feedback_type_i18n}}";
		}
		static function instance( $idType, $idLanguage ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idType', 'idLanguage' ));
		}
	}

?>