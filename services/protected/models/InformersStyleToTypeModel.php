<?php
	Yii::import( 'models.base.ModelBase' );
	
	final class InformersStyleToTypeModel extends ModelBase {

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function instance( $idStyle, $type ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idStyle', 'type' ));
		}
	}
?>