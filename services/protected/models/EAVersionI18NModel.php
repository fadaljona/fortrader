<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAVersionI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_version_i18n}}";
		}
		static function instance( $idVersion, $idLanguage, $desc ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idVersion', 'idLanguage', 'desc' ));
		}
	}

?>