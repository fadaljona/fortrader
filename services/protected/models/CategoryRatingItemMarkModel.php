<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CategoryRatingItemMarkModel extends ModelBase {
	
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idPost, $idUser, $value ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idPost', 'idUser', 'value' ));
		}
		
	}

?>