<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserActivityModel extends ModelBase {
		const TYPE_EVENT_CREATE_USER = 1;
		const TYPE_EVENT_CREATE_CONTEST_MEMBER = 2;
		const TYPE_EVENT_CREATE_CONTEST_COMPLETED = 3;
		const TYPE_EVENT_CREATE_CONTEST_WINER = 4;
		const TYPE_EVENT_CREATE_CONTEST_MESSAGE = 5;
		const TYPE_EVENT_CREATE_BROKER_REVIEW = 6;
		const TYPE_EVENT_CREATE_BROKER_MARK = 7;
		const TYPE_EVENT_CREATE_BROKER_LINK = 8;
		const TYPE_EVENT_CREATE_EA_REVIEW = 9;
		const TYPE_EVENT_CREATE_EA_MARK = 10;
		const TYPE_EVENT_DOWNLOAD_EA_VERSION = 11;
		const TYPE_EVENT_CREATE_EA = 12;
		const TYPE_EVENT_CREATE_EA_VERSION = 13;
		const TYPE_EVENT_CREATE_EA_STATEMENT = 14;
		const TYPE_EVENT_CREATE_JOURNAL_COMMENT = 15;
		const TYPE_EVENT_DOWNLOAD_JOURNAL = 16;
		const TYPE_EVENT_VIEW_JOURNAL = 17;
		const TYPE_EVENT_UPDATE_PROFILE = 18;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'UserActivityI18NModel', 'idActivity', 'alias' => 'i18nsUserActivity' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'UserActivityI18NModel', 'idActivity', 
					'alias' => 'cLI18NUserActivity',
					'on' => '`cLI18NUserActivity`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
			 );
		}
		static function instance( $type, $idUser, $idObject, $count ) {
			return self::modelFromAssoc( __CLASS__, compact( 'type', 'idUser', 'idObject', 'count' ));
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->message )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->message )) return $i18n;
			}
		}
		function getMessage() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->message : '';
		}
		function getIcon() {
			$tag = CHtml::openTag( 'i', Array( 'class' => "iI i09 i10_{$this->type}" ));
			$tag .= CHtml::closeTag( 'i' );
			
			return $tag;
		}
		function getHTMLMessage( $maxLen = null ) {
			$message = $this->message;
			//$message = htmlspecialchars( $this->message );
			//$message = CommonLib::nl2br( $message );
			if( $maxLen ) $message = CommonLib::shorten( $message, $maxLen );
			return $message;
		}
			
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->message )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = UserActivityI18NModel::model()->findByAttributes( Array( 'idActivity' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = UserActivityI18NModel::instance( $this->id, $idLanguage, $obj->message );
						$i18n->save();
					}
					else{
						$i18n->message = $obj->message;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		function setI18NsFromMessage( $key, $ns = '*', $params = Array(), $paramsI18Ns = Array() ) {
			$message = MessageModel::model()->find(Array(
				'with' => 'i18ns',
				'condition' => "
					`t`.`key` = :key AND `t`.`ns` = :ns
				",
				'params' => Array(
					':key' => $key,
					':ns' => $ns,
				),
			));
			if( $message ) {
				$i18ns = Array();
				foreach( $message->i18ns as $i18n ) {
					$message = $i18n->value;
					if( $params ) $message = strtr( $message, $params );
					if( $paramsI18Ns and isset( $paramsI18Ns[ $i18n->idLanguage ])) $message = strtr( $message, $paramsI18Ns[ $i18n->idLanguage ]);
					$i18ns[ $i18n->idLanguage ] = (object)Array(
						'message' => $message,
					);
				}
				$this->setI18Ns( $i18ns );
			}
		}
			
			# events
		private static function deleteEvents( $type, $idUser, $idObject ) {
			$events = UserActivityModel::model()->findAllByAttributes(Array(
				'type' => $type, 
				'idUser' => $idUser, 
				'idObject' => $idObject,
			));
			foreach( $events as $event ) {
				$event->delete();
			}
		}
		private static function eventCreateUser( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$activity = self::instance( 'create_user', $params[ 'idUser' ], null, $settings->count_user );
			$activity->save();
			
			$activity->setI18NsFromMessage( 'Registered on the site' );
		}
		private static function eventCreateContestMember( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$contest = ContestModel::model()->findByPk( $params[ 'idContest' ]);
			if( $contest ) {
				$activity = self::instance( 'create_contest_member', $params[ 'idUser' ], $params[ 'idContest' ], $settings->count_contestMember );
				$activity->save();
				
				$url = $contest->getSingleURL();
				$paramsI18Ns = Array();
				foreach( $contest->i18ns as $i18n ) {
					$paramsI18Ns[ $i18n->idLanguage ][ '{link}' ] = CHtml::link( CHtml::encode( $i18n->name ), $url );
				}
				$activity->setI18NsFromMessage( 'Registered in contest {link}', '*', null, $paramsI18Ns );
			}
		}
		private static function eventCreateContestCompleted( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$contest = ContestModel::model()->findByPk( $params[ 'idContest' ]);
			$member = ContestMemberModel::model()->findByAttributes( Array( 'idContest' => $params[ 'idContest' ], 'idUser' => $params[ 'idUser' ]));
			if( $contest and $member ) {
				$place = $member->stats->place;
				if( $place and $place != 1 ) {
					$count = eval( "return ".$settings->eval_contestMemberPlace.";" );
					$activity = self::instance( 'create_contest_completed', $params[ 'idUser' ], $params[ 'idContest' ], $count );
					$activity->save();
					
					$url = $contest->getSingleURL();
					$params = Array( '{place}' => $place );
					$paramsI18Ns = Array();
					foreach( $contest->i18ns as $i18n ) {
						$paramsI18Ns[ $i18n->idLanguage ][ '{link}' ] = CHtml::link( CHtml::encode( $i18n->name ), $url );
					}
					$activity->setI18NsFromMessage( 'Finished in contest {link} in {place} place', '*', $params, $paramsI18Ns );
				}
			}
		}
		private static function eventCreateContestWiner( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$contest = ContestModel::model()->findByPk( $params[ 'idContest' ]);
			$member = ContestMemberModel::model()->findByAttributes( Array( 'idContest' => $params[ 'idContest' ], 'idUser' => $params[ 'idUser' ]));
			if( $contest and $member ) {
				$activity = self::instance( 'create_contest_winer', $params[ 'idUser' ], $params[ 'idContest' ], $settings->count_contestWiner );
				$activity->save();
				
				$url = $contest->getSingleURL();
				$params = Array();
				$paramsI18Ns = Array();
				foreach( $contest->i18ns as $i18n ) {
					$paramsI18Ns[ $i18n->idLanguage ][ '{link}' ] = CHtml::link( CHtml::encode( $i18n->name ), $url );
				}
				$activity->setI18NsFromMessage( 'Won a prize in the {link}', '*', $params, $paramsI18Ns );
			}
		}
		private static function eventCreateContestMessage( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$contest = ContestModel::model()->findByPk( $params[ 'idContest' ]);
			if( $contest ) {
				$activity = self::instance( 'create_contest_message', $params[ 'idUser' ], $params[ 'idContest' ], $settings->count_message );
				$activity->save();
				
				$url = $contest->getSingleURL();
				$paramsI18Ns = Array();
				foreach( $contest->i18ns as $i18n ) {
					$paramsI18Ns[ $i18n->idLanguage ][ '{link}' ] = CHtml::link( CHtml::encode( $i18n->name ), $url );
				}
				$activity->setI18NsFromMessage( 'Added a comment for the contest {link}', '*', Array(), $paramsI18Ns );
			}
		}
		private static function eventCreateBrokerReview( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$broker = BrokerModel::model()->findByPk( $params[ 'idBroker' ]);
			if( $broker ) {
				$activity = self::instance( 'create_broker_review', $params[ 'idUser' ], $params[ 'idBroker' ], $settings->count_brokerReview );
				$activity->save();
				
				$url = $broker->getSingleURL();
				$_params = Array();
				$_params[ '{link}' ] = CHtml::link( CHtml::encode( $broker->shortName ), $url );
				$activity->setI18NsFromMessage( 'Wrote a new review about the company {link}', '*', $_params );
			}
		}
		private static function eventCreateBrokerMark( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$broker = BrokerModel::model()->findByPk( $params[ 'idBroker' ]);
			if( $broker ) {
				$type = $params['trust'] >= 50 ? 'create_broker_mark' : 'create_broker_mark_down';
				$activity = self::instance( $type, $params[ 'idUser' ], $params[ 'idBroker' ], $settings->count_brokerMark );
				$activity->save();
				
				$url = $broker->getSingleURL();
				$_params = Array();
				$_params[ '{link}' ] = CHtml::link( CHtml::encode( $broker->shortName ), $url );
				$_params[ '{trust}' ] = "{$params['trust']}%";
				$activity->setI18NsFromMessage( 'Appraise the conditions {link} by {trust}', '*', $_params );
			}
		}
		private static function eventCreateBrokerLink( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$broker = BrokerModel::model()->findByPk( $params[ 'idBroker' ]);
			if( $broker ) {
				self::deleteEvents( 'create_broker_link', $params[ 'idUser' ], $params[ 'idBroker' ]);
				$activity = self::instance( 'create_broker_link', $params[ 'idUser' ], $params[ 'idBroker' ], $settings->count_brokerLink );
				$activity->save();
				
				$url = $broker->getSingleURL();
				$_params = Array();
				$_params[ '{link}' ] = CHtml::link( CHtml::encode( $broker->shortName ), $url );
				$activity->setI18NsFromMessage( 'Marked as a client company {link}', '*', $_params );
			}
		}
		private static function eventCreateEAReview( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$EA = EAModel::model()->findByPk( $params[ 'idEA' ]);
			if( $EA ) {
				$activity = self::instance( 'create_ea_review', $params[ 'idUser' ], $params[ 'idEA' ], $settings->count_EAReview );
				$activity->save();
				
				$url = $EA->getSingleURL();
				$_params = Array();
				$_params[ '{link}' ] = CHtml::link( CHtml::encode( $EA->name ), $url );
				$activity->setI18NsFromMessage( 'Commented EA {link}', '*', $_params );
			}
		}
		private static function eventCreateEAMark( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$EA = EAModel::model()->findByPk( $params[ 'idEA' ]);
			if( $EA ) {
				self::deleteEvents( 'create_ea_mark', $params[ 'idUser' ], $params[ 'idEA' ]);
				$activity = self::instance( 'create_ea_mark', $params[ 'idUser' ], $params[ 'idEA' ], $settings->count_EAMark );
				$activity->save();
				
				$url = $EA->getSingleURL();
				$_params = Array();
				$_params[ '{link}' ] = CHtml::link( CHtml::encode( $EA->name ), $url );
				$_params[ '{average}' ] = "{$params['average']}%";
				$activity->setI18NsFromMessage( 'Rated EA {link} by {average}', '*', $_params );
			}
		}
		private static function eventDownloadEAVersion( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$EAVersion = EAVersionModel::model()->findByPk( $params[ 'idEAVersion' ]);
			if( $EAVersion ) {
				$EA = EAModel::model()->findByPk( $EAVersion->idEA );
				if( $EA ) {
					self::deleteEvents( 'download_ea_version', $params[ 'idUser' ], $params[ 'idEAVersion' ]);
					$activity = self::instance( 'download_ea_version', $params[ 'idUser' ], $params[ 'idEAVersion' ], $settings->count_downloadEAVersion );
					$activity->save();
					
					$urlEA = $EA->getSingleURL();
					$urlEAVersion = $EAVersion->getSingleURL();
					$_params = Array();
					//$_params[ '{linkEA}' ] = CHtml::link( CHtml::encode( $EA->name ), $urlEA );
					//$_params[ '{linkEAVersion}' ] = CHtml::link( CHtml::encode( $EAVersion->version ), $urlEAVersion );
					$_params[ '{linkEA}' ] = "";
					$_params[ '{linkEAVersion}' ] = CHtml::link( CHtml::encode( "{$EA->name} {$EAVersion->version}" ), $urlEAVersion );
					$activity->setI18NsFromMessage( 'Downloaded EA {linkEA} {linkEAVersion}', '*', $_params );
				}
			}
		}
		private static function eventCreateEA( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$EA = EAModel::model()->findByPk( $params[ 'idEA' ]);
			if( $EA ) {
				$activity = self::instance( 'create_ea', $params[ 'idUser' ], $params[ 'idEA' ], $settings->count_EA );
				$activity->save();
				
				$url = $EA->getSingleURL();
				$_params = Array();
				$_params[ '{link}' ] = CHtml::link( CHtml::encode( $EA->name ), $url );
				$activity->setI18NsFromMessage( 'Add new EA page {link}', '*', $_params );
			}
		}
		private static function eventCreateEAVersion( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$EAVersion = EAVersionModel::model()->findByPk( $params[ 'idEAVersion' ]);
			if( $EAVersion ) {
				$EA = EAModel::model()->findByPk( $EAVersion->idEA );
				if( $EA ) {
					$activity = self::instance( 'create_ea_version', $params[ 'idUser' ], $params[ 'idEAVersion' ], $settings->count_EAVersion );
					$activity->save();
					
					$urlEA = $EA->getSingleURL();
					$urlEAVersion = $EAVersion->getSingleURL();
					$_params = Array();
					//$_params[ '{linkEA}' ] = CHtml::link( CHtml::encode( $EA->name ), $urlEA );
					//$_params[ '{linkEAVersion}' ] = CHtml::link( CHtml::encode( $EAVersion->version ), $urlEAVersion );
					$_params[ '{linkEA}' ] = "";
					$_params[ '{linkEAVersion}' ] = CHtml::link( CHtml::encode( "{$EA->name} {$EAVersion->version}" ), $urlEAVersion );
					$activity->setI18NsFromMessage( 'Add new EA version {linkEA} {linkEAVersion}', '*', $_params );
				}
			}
		}
		private static function eventCreateEAStatement( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$EAStatement = EAStatementModel::model()->findByPk( $params[ 'idEAStatement' ]);
			if( $EAStatement ) {
				$EA = EAModel::model()->findByPk( $EAStatement->idEA );
				$EAVersion = EAVersionModel::model()->findByPk( $EAStatement->idVersion );
				if( $EA and $EAVersion ) {
					$activity = self::instance( 'create_ea_statement', $params[ 'idUser' ], $params[ 'idEAStatement' ], $settings->count_EAStatement );
					$activity->save();
					
					$urlEA = $EA->getSingleURL();
					$urlEAVersion = $EAVersion->getSingleURL();
					$_params = Array();
					//$_params[ '{linkEA}' ] = CHtml::link( CHtml::encode( $EA->name ), $urlEA );
					//$_params[ '{linkEAVersion}' ] = CHtml::link( CHtml::encode( $EAVersion->version ), $urlEAVersion );
					$_params[ '{linkEA}' ] = "";
					$_params[ '{linkEAVersion}' ] = CHtml::link( CHtml::encode( "{$EA->name} {$EAVersion->version}" ), $urlEAVersion );
					$activity->setI18NsFromMessage( 'Add new EA statement {linkEA} {linkEAVersion}', '*', $_params );
				}
			}
		}
		private static function eventCreateJournalComment( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$journal = JournalModel::model()->findByPk( $params[ 'idJournal' ]);
			if( $journal ) {
				$activity = self::instance( 'create_journal_comment', $params[ 'idUser' ], $params[ 'idJournal' ], $settings->count_JournalComment, null );
				$activity->save();
				
				$url = $journal->getSingleURL();
				$_params = Array();
				$_params[ '{link}' ] = CHtml::link( CHtml::encode( $journal->number ), $url );
				$activity->setI18NsFromMessage( 'Add new comment to journal issie № {link}', '*', $_params );
			}
		}
		private static function eventDownloadJournal( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$journal = JournalModel::model()->findByPk( $params[ 'idJournal' ]);
			if( $journal ) {
				self::deleteEvents( 'download_journal', $params[ 'idUser' ], $params[ 'idJournal' ]);
				$activity = self::instance( 'download_journal', $params[ 'idUser' ], $params[ 'idJournal' ], $settings->count_JournalDownload );
				$activity->save();
				
				$url = $journal->getSingleURL();
				$_params = Array();
				$_params[ '{link}' ] = CHtml::link( CHtml::encode( $journal->number ), $url );
				$activity->setI18NsFromMessage( 'Downloaded issie № {link}', '*', $_params );
			}
		}
		private static function eventViewJournal( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$journal = JournalModel::model()->findByPk( $params[ 'idJournal' ]);
			if( $journal ) {
				self::deleteEvents( 'view_journal', $params[ 'idUser' ], $params[ 'idJournal' ]);
				$activity = self::instance( 'view_journal', $params[ 'idUser' ], $params[ 'idJournal' ], $settings->count_JournalView );
				$activity->save();
				
				$url = $journal->getSingleURL();
				$_params = Array();
				$_params[ '{link}' ] = CHtml::link( CHtml::encode( $journal->number ), $url );
				$activity->setI18NsFromMessage( 'Readed journal issie № {link}', '*', $_params );
			}
		}
		private static function eventUpdateProfile( $params ) {
			$settings = UserActivitySettingsModel::getModel();
			$user = UserModel::model()->findByPk( $params[ 'idUser' ]);
			if( $user ) {
				self::deleteEvents( 'update_profile', $params[ 'idUser' ], null );
				$activity = self::instance( 'update_profile', $params[ 'idUser' ], null, 0 );
				$activity->save();
				
				$url = $user->getProfileURL( true );
				$_params = Array();
				$_params[ '{profile}' ] = CHtml::link( '{profile}', $url );
				
				$paramsI18Ns = Array();
				$message = MessageModel::model()->findByAttributes(Array( 'key' => 'profile', 'ns' => 'lower' ));
				foreach( $message->i18ns as $i18n ) {
					$paramsI18Ns[$i18n->idLanguage]['{profile}'] = $i18n->value;
				}
				
				$activity->setI18NsFromMessage( 'Updated {profile}', '*', $_params, $paramsI18Ns );
			}
		}
		static function event( $type, $params ) {
			switch( $type ) {
				case self::TYPE_EVENT_CREATE_USER:{ 
					self::eventCreateUser( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_CONTEST_MEMBER:{
					self::eventCreateContestMember( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_CONTEST_COMPLETED:{
					//self::eventCreateContestCompleted( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_CONTEST_WINER:{
					self::eventCreateContestWiner( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_CONTEST_MESSAGE:{
					self::eventCreateContestMessage( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_BROKER_REVIEW:{
					self::eventCreateBrokerReview( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_BROKER_MARK:{
					self::eventCreateBrokerMark( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_BROKER_LINK:{
					self::eventCreateBrokerLink( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_EA_REVIEW:{
					self::eventCreateEAReview( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_EA_MARK:{
					self::eventCreateEAMark( $params ); break;
				}
				case self::TYPE_EVENT_DOWNLOAD_EA_VERSION:{
					self::eventDownloadEAVersion( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_EA:{
					self::eventCreateEA( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_EA_VERSION:{
					self::eventCreateEAVersion( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_EA_STATEMENT:{
					self::eventCreateEAStatement( $params ); break;
				}
				case self::TYPE_EVENT_CREATE_JOURNAL_COMMENT:{
					self::eventCreateJournalComment( $params ); break;
				}
				case self::TYPE_EVENT_DOWNLOAD_JOURNAL:{
					self::eventDownloadJournal( $params ); break;
				}
				case self::TYPE_EVENT_VIEW_JOURNAL:{
					self::eventViewJournal( $params ); break;
				}
				case self::TYPE_EVENT_UPDATE_PROFILE:{
					self::eventUpdateProfile( $params ); break;
				}
			}
			return true;
		}
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( !$this->isNewRecord ) {
					if( $user = UserModel::model()->findByPk( $this->idUser ) and $old = self::model()->findByPk( $this->id )) {
						$user->countActivities -= $old->count;
						$user->update( Array( 'countActivities' ));
					}
				}
			}
			return $result;
		}
		protected function afterSave() {
			if( $this->count and $user = UserModel::model()->findByPk( $this->idUser )) {
				$user->countActivities += $this->count;
				$user->update( Array( 'countActivities' ));
			}
			parent::afterSave();
		}
		protected function afterDelete() {
			if( $this->count and $user = UserModel::model()->findByPk( $this->idUser )) {
				$user->countActivities -= $this->count;
				$user->update( Array( 'countActivities' ));
			}
			$this->deleteI18Ns();
			parent::afterDelete();
		}
	}

?>