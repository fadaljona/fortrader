<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EANewsI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_news_i18n}}";
		}
		static function instance( $idNews, $idLanguage, $news ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idNews', 'idLanguage', 'news' ));
		}
	}

?>