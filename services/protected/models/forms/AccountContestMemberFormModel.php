<?
	Yii::import( 'models.base.FormModelBase' );

	final class AccountContestMemberFormModel extends FormModelBase {
		public $id;
		public $accountNumber;
		public $investorPassword;
		public $idServer;
		public $user_id=0;
		public $accountTitle;
		public $accountType = 'account';
		public $reportFile;
		public $monitoringType = 'contest';

		function getARClassName() {
			return "ContestMemberModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'accountType' => 'Type',
				'accountNumber' => 'Account number',
				'investorPassword' => 'Investor password',
				'idServer' => 'Server',
				'accountTitle' => 'Account title',
				'reportFile' => 'Report file'
			);
		}
		function rules() {
			$rules = Array(
				Array( 'accountNumber', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => CommonLib::maxUInt ),
				Array( 'id', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => CommonLib::maxUInt ),
				Array( 'idServer', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'investorPassword, accountTitle', 'length', 'max' => CommonLib::maxByte ),
				Array( 'accountType', 'in', 'range' => Array( 'account','report' )),
			);
			if( $this->accountType == 'account' )$rules[] = Array( 'accountNumber, investorPassword, idServer', 'required' );
			if( $this->monitoringType == 'monitoring' ){
				$rules[] = Array( 'accountTitle', 'required' );
				$rules[] = Array( 'accountTitle', 'uniqueName' );
			}
			if( $this->accountType == 'report' ){
				$rules[] = Array( 'accountTitle', 'required' );
				$rules[] = Array( 'reportFile', 'file', 'allowEmpty' => false, 'types' => Array( 'htm' ));
			}
			return $rules;
		}
		
		function uniqueName() {
			if( !$this->hasErrors()) {
				
				$c = new CDbCriteria();
				$c->params = Array();
				
				$c->addCondition( "`t`.`accountTitle` = :accountTitle" );
				$c->params[ ':accountTitle' ] = $this->accountTitle;
				
				$exists = ContestMemberModel::model()->exists( $c );
				if( $exists ) {
					$this->addI18NError( 'accountTitle', "Account title {name} is already occupied. Please, select another.", Array( '{name}' => $this->accountTitle ));
				}
			}
		}
		
		public function getDataFromFile(){
			$reportContent = file_get_contents( $this->reportFile->tempName );
			if( !$reportContent ) return 1;
			
			$dataToReturn = array();
			
			$reportContent=str_replace("\n"," ",$reportContent);
			$reportContent=str_replace("\r"," ",$reportContent);

			preg_match_all( 
				'/<table width=820 cellspacing=1 cellpadding=3 border=0>\s*<tr align=left>(.*?)<\/table>/',
				$reportContent,
				$table1 
			);
			
			if( !$table1 || !isset( $table1[0][0] ) || !$table1[0][0] ) return 2;

			$dataToHash = preg_replace('/[^a-z0-9-\(\);=\."\'\_]/i', '', strip_tags( $table1[0][0] ) );
			
			$dataToReturn['hash'] = hash('sha256', $dataToHash);
		
			preg_match_all( 
				'/<table width=820 cellspacing=1 cellpadding=3 border=0>\s*<tr align=left>(.*?)<td colspan=4>(.*?)[^<*]</',
				$table1[0][0],
				$symbolMatches 
			);
			
			if( !$symbolMatches || !isset( $symbolMatches[2][0] ) || !$symbolMatches[2][0] ) return 3;

			if( $pos = strpos( $symbolMatches[2][0], '(' ) ){
				$symbol = str_replace(' ', '', substr( $symbolMatches[2][0], 0, $pos ) );
			}else{
				$symbol = $symbolMatches[2][0];
			}

			$dataToReturn['symbol'] = $symbol;


			preg_match_all( 
				'/<table width=820 cellspacing=1 cellpadding=3 border=0>\s*<tr bgcolor="#C0C0C0" align=right>(.*?)<\/tr>(.*)<\/table>/',
				$reportContent,
				$table2 
			);		
			
			if( !$table2 || !isset( $table2[2][0] ) || !$table2[2][0] ) return 4;
	

			$dataRows = str_replace('<td colspan=2></td>', '<td class=mspt></td><td class=mspt></td>', $table2[2][0]);


			preg_match_all( 
				'/<tr[^>]*><td[^>]*>[0-9]+<\/td><td[^>]*>([^<]*)<\/td><td[^>]*>([^<]*)<\/td><td[^>]*>([^<]*)<\/td><td[^>]*>([^<]*)<\/td><td[^>]*>([^<]*)<\/td><td[^>]*>([^<]*)<\/td><td[^>]*>([^<]*)<\/td><td[^>]*>([^<]*)<\/td><td[^>]*>([^<]*)<\/td><\/tr>/',
				$dataRows,
				$data, PREG_SET_ORDER 
			);
			
			if( !$data ) return 5;


			$outDataArr = array();

			foreach( $data as $rowArrData ){
				if( $rowArrData[2] != 't/p' && $rowArrData[2] != 'close' ){
					$outDataArr[ $rowArrData[3] ] = array(
						'openTime' => $rowArrData[1],
						'type' => $rowArrData[2],
						'lots' => $rowArrData[4],
						'openPrice' => $rowArrData[5],
						'stopLoss' => $rowArrData[6],
						'takeProfit' => $rowArrData[7],
					);
				}else{
					$outDataArr[ $rowArrData[3] ]['closeTime'] = $rowArrData[1];
					$outDataArr[ $rowArrData[3] ]['closePrice'] = $rowArrData[5];
					
					$outDataArr[ $rowArrData[3] ]['profit'] = $rowArrData[8];
					$outDataArr[ $rowArrData[3] ]['balance'] = $rowArrData[9];
				}
			}
			
			if( !$outDataArr ) return 6;
			
			$dataToReturn['data'] = $outDataArr;
			
			return $dataToReturn;
		}
	}

?>