<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUserNoticeFormModel extends FormModelBase {
		public $id;
		public $to;
		public $messages = Array();
		public $type;
		public $link;
		protected $userTo;
		function getARClassName() {
			return "UserNoticeModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'to' => 'To',
				'type' => 'Type',
				'link' => 'Link',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "messages[{$language->id}]" ] = "Message";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'to, type', 'required' ),
				Array( 'link', 'length', 'max' => CommonLib::maxByte ),
				Array( 'to', 'validateTo' ),
				Array( 'messages', 'checkLens', 'max' => CommonLib::maxByte ),
			);
		}
		function validateTo() {
			if( !$this->hasErrors()) {
				$this->userTo = UserModel::model()->findByAttributes( Array( 'user_login' => $this->to ));
				if( !$this->userTo ) {
					$this->addI18NError( 'to', "Can't find User!" );
				}
			}
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( !strlen( $this->link )) $this->link = null;
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'to' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( $AR->idUser ) {
					$userTo = UserModel::model()->findByPk( $AR->idUser );
					$this->to = $userTo->user_login;
				}
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->messages[ $i18n->idLanguage ] = $i18n->message;
				}
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'to' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->idUser = $this->userTo->ID;
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'message' => $this->messages[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );				
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>