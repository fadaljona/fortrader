<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUsersImportFormModel extends FormModelBase {
		const MODE_UPLOAD = 1;
		const MODE_SELECT_EXPORT = 2;
		const MODE_HIDDEN = 3;
		protected $mode = self::MODE_UPLOAD;
		public $cancelAction = false;
		public $file;
		public $export = 'users_and_groups';
		protected function getSourceAttributeLabels() {
			return Array(
				'file' => 'File',
				'export' => 'Export',
			);
		}
		function rules() {
			$rules = Array();
			if( $this->inMode( self::MODE_UPLOAD )) {
				$rules[] = Array( 'file', 'file' );
			}
			if( $this->inMode( self::MODE_SELECT_EXPORT )) {
				$rules[] = Array( 'export', 'required' );
			}
			return $rules;
		}
		function validate( $attributes=null, $clearErrors=true ) {
			if( $this->cancelAction ) return true;
			return parent::validate( $attributes, $clearErrors );
		}
		function setMode( $mode ) {
			$this->mode = $mode;
		}
		function getMode() {
			return $this->mode;
		}
		function inMode( $mode ) {
			return $mode == $this->getMode();
		}
		function load( $id = 0 ) {
			$this->loadFromPost();
			if( $this->inMode( self::MODE_UPLOAD )) {
				$this->loadFromFiles( Array( 'file' ));
			}
			return true;
		}
	}

?>