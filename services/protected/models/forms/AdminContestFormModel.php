<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminContestFormModel extends FormModelBase {
		public $id;
		public $names;
		public $descriptions;
		public $images;
		public $status = 'registration';
		public $startDeposit = 10000;
		public $endReg;
		public $begin;
		public $end;
		public $servers;
		public $prizes;
		public $accountOpening;
		public $showGraphLeaders;
		public $stopImport;
		public $stopDeleteMMonitoring;
		public $completedMembers;
		
		public $slug;
		
		public $urlsRules;
		public $modeEA;
		
		public $pageTitle = array();
		public $pageSeoTitle = array();
		public $seoKeys = array();
		public $seoDesc = array();
		public $fullDesc = array();
		public $rules = array();
		
		public $excludeFieldsFromAr = array( 'names', 'descriptions', 'servers', 'rules', 'fullDesc', 'pageTitle', 'pageSeoTitle', 'seoKeys', 'seoDesc' );
		
		
		function getARClassName() {
			return "ContestModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'status' => 'Status',
				'startDeposit' => 'Start Deposit',
				'endReg' => 'End registration',
				'begin' => 'Begin',
				'end' => 'End',
				'servers' => 'Servers',
				'prizes' => 'Prizes',
				'accountOpening' => 'Account opening',
				'completedMembers' => 'Complete members',
				'showGraphLeaders' => 'Show graph leaders',
				'stopImport' => 'Stop import data',
				'stopDeleteMMonitoring' => 'Stop member account monitoring and delete data',
				'modeEA' => 'EA',
				'slug' => 'Contest slug',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "names[{$language->id}]" ] = "Title";
				$labels[ "descriptions[{$language->id}]" ] = "Description";
				$labels[ "images[{$language->id}]" ] = "Image";
				$labels[ "urlsRules[{$language->id}]" ] = "URL Rules";
				
				$labels[ "pageTitle[{$language->id}]" ] = "Page Title";
				$labels[ "pageSeoTitle[{$language->id}]" ] = "Seo Title";
				$labels[ "seoKeys[{$language->id}]" ] = "Seo Keys";
				$labels[ "seoDesc[{$language->id}]" ] = "Seo Desc";
				$labels[ "fullDesc[{$language->id}]" ] = "Full Desc";
				$labels[ "rules[{$language->id}]" ] = "Trading Rules";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'names', 'validateNames' ),
				Array( 'descriptions', 'validateDescriptions' ),
				Array( 'status, endReg, begin, end, servers', 'required' ),
				array( 'startDeposit', 'numerical', 'integerOnly'=>true, 'min'=>0),
				Array( 'endReg, begin, end', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'status', 'in', 'range' => Array( 'registration stoped', 'registration', 'started', 'completed' ), 'allowEmpty' => false ),
				Array( 'accountOpening', 'in', 'range' => Array( 'MetaTrader API', 'internal mail', 'manually trader', 'list of accounts' ), 'allowEmpty' => false ),
				Array( 'showGraphLeaders, completedMembers, stopImport, stopDeleteMMonitoring', 'in', 'range' => Array( '0', '1' ), 'allowEmpty' => false ),
				Array( 'servers', 'validateServers' ),
				
				Array( 'pageTitle, pageSeoTitle, seoKeys, seoDesc', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'fullDesc, rules', 'checkLens', 'max' => CommonLib::maxWord ),
				
				Array( 'slug', 'required' ),
				Array( 'slug', 'uniqueField', 'message' => 'Slug is busy. Please choose another.' ),
				Array( 'slug', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function validateNames() {
			$AR = $this->getAR();
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ) {
				$name = @$this->names[ $language->id ];
				$label = $this->getAttributeLabel( "names[{$language->id}]" );
				if( empty( $name )) {
					$this->addError( 'names', Yii::t( 'yii', '{attribute} cannot be blank.', Array( 
						'{attribute}' => "{$label} ({$language->alias})",
					)));
					continue;
				}
				if( mb_strlen( $name ) > CommonLib::maxByte ) {
					$this->addError( 'names', Yii::t( 'yii', '{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => "{$label} ({$language->alias})",
						'{max}' => CommonLib::maxByte,
					)));
					continue;
				}
				
				$c = new CDbCriteria();
				$c->params = Array();
				
				$c->addCondition( "`t`.`name` = :field" );
				$c->params[ ':field' ] = $name;
				
				$c->addCondition( "`t`.`idLanguage` = :idLanguage" );
				$c->params[ ':idLanguage' ] = $language->id;
				
				if( !$AR->isNewRecord ) {
					$c->addCondition( "`t`.`idContest` != :pk" );
					$c->params[ ':pk' ] = $AR->id;
				}
				
				$exists = ContestI18NModel::model()->exists( $c );
				if( $exists ) {
					$this->addError( $field, Yii::t( 'yii', '{attribute} "{value}" has already been taken.', Array(
						'{attribute}' => "{$label} ({$language->alias})",
						'{value}' => CHtml::encode( $name ),
					)));
				}
			}
		}
		function validateDescriptions() {
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ) {
				$description = @$this->descriptions[ $language->id ];
				$label = $this->getAttributeLabel( "descriptions[{$language->id}]" );
				
				if( mb_strlen( $description ) > CommonLib::maxWord ) {
					$this->addError( 'descriptions', Yii::t( 'yii', '{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => "{$label} ({$language->alias})",
						'{max}' => CommonLib::maxWord,
					)));
				}
			}
		}
		function validateServers() {
			if( !$this->hasErrors()) {
				$servers = $this->servers ? explode( ',', $this->servers ) : Array();
				$servers  = array_filter( $servers, Array( 'CommonLib', 'isID' ));
				foreach( $servers as $id ) {
					if( !ServerModel::model()->existsByPk( $id )) {
						$this->addI18NError( 'servers', 'Wrong {attribute}!', Array(
							'{attribute}' => $this->getAttributeLabel( 'servers' ),
						));
						break;
					}
				}
			}
		}
		function loadFromFiles( $loadFields = Array()) {
			parent::loadFromFiles( $loadFields );
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$file = CUploadedFile::getInstance( $this, "images[{$language->id}]" );
				if( $file )	$this->images[$language->id] = $file;
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				foreach( $AR->i18ns as $i18n ) {
					$this->names[ $i18n->idLanguage ] = $i18n->name;
					$this->descriptions[ $i18n->idLanguage ] = $i18n->description;
					$this->images[ $i18n->idLanguage ] = $i18n->srcImage;
					$this->urlsRules[ $i18n->idLanguage ] = $i18n->urlRules;
					
					$this->fullDesc[ $i18n->idLanguage ] = $i18n->fullDesc;
					$this->rules[ $i18n->idLanguage ] = $i18n->rules;

					$this->pageTitle[ $i18n->idLanguage ] = $i18n->pageTitle;
					$this->pageSeoTitle[ $i18n->idLanguage ] = $i18n->pageSeoTitle;
					$this->seoKeys[ $i18n->idLanguage ] = $i18n->seoKeys;
					$this->seoDesc[ $i18n->idLanguage ] = $i18n->seoDesc;
				}
				
				$this->servers = $AR->getIDsServers();
				$this->servers = implode( ',', $this->servers );
				

			}
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			array_push( $exceptFields, 'prizes' );
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				$this->prizes = $this->prizes ? explode( ',', $this->prizes ) : Array();
				$this->prizes = array_filter( $this->prizes, 'CommonLib::isNum' );
				$this->prizes = array_map( 'CommonLib::toFloat', $this->prizes );
				$this->prizes = array_map( 'abs', $this->prizes );
				sort( $this->prizes );
				$this->prizes = array_reverse( $this->prizes );
				$this->prizes = implode( ',', $this->prizes );
				$AR->prizes = strlen($this->prizes) ? $this->prizes : null;
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( empty( $post[ 'rules' ])) $this->rules = Array();
				$this->rules = (Array)$this->rules;
			}
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'name' => $this->names[ $language->id ],
						'description' => $this->descriptions[ $language->id ],
						'image' => $this->images[ $language->id ],
						'urlRules' => $this->urlsRules[ $language->id ],
						
						'fullDesc' => $this->fullDesc[ $language->id ],
						'rules' => $this->rules[ $language->id ],

						'pageTitle' => $this->pageTitle[ $language->id ],
						'pageSeoTitle' => $this->pageSeoTitle[ $language->id ],
						'seoKeys' => $this->seoKeys[ $language->id ],
						'seoDesc' => $this->seoDesc[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );
			
					# servers
				$this->servers = $this->servers ? explode( ',', $this->servers ) : Array();
				$this->servers  = array_filter( $this->servers, Array( 'CommonLib', 'isID' ));
				$AR->setServers( $this->servers );
					# controll members
				if( $AR->completedMembers ) 
					$AR->completeMembers();
				else 
					$AR->uncompleteMembers();
			
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();

			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>