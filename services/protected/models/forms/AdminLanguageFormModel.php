<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminLanguageFormModel extends FormModelBase {
		public $id;
		public $name;
		public $alias;
		function getARClassName() {
			return "LanguageModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'name' => 'Name',
				'alias' => 'Alias',
			);
		}
		function rules() {
			return Array(
				Array( 'name, alias', 'required' ),
				Array( 'name, alias', 'length', 'max' => CommonLib::maxByte ),
				Array( 'name, alias', 'uniqueField' ),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>