<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUserReputationFormModel extends FormModelBase {
		public $id;
		public $from;
		public $to;
		public $messages = Array();
		public $count;
		protected $userFrom;
		protected $userTo;
		function getARClassName() {
			return "UserReputationModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'from' => 'From',
				'to' => 'To',
				'message' => 'Message',
				'count' => 'Count',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "messages[{$language->id}]" ] = "Message";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'from, to, count', 'required' ),
				Array( 'count', 'numerical' ),
				Array( 'from', 'validateFrom' ),
				Array( 'to', 'validateTo' ),
				Array( 'messages', 'checkLens', 'max' => CommonLib::maxByte ),
			);
		}
		function validateFrom() {
			if( !$this->hasErrors()) {
				$this->userFrom = UserModel::model()->findByAttributes( Array( 'user_login' => $this->from ));
				if( !$this->userFrom ) {
					$this->addI18NError( 'from', "Can't find User!" );
				}
			}
		}
		function validateTo() {
			if( !$this->hasErrors()) {
				$this->userTo = UserModel::model()->findByAttributes( Array( 'user_login' => $this->to ));
				if( !$this->userTo ) {
					$this->addI18NError( 'to', "Can't find User!" );
				}
			}
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'from' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( $AR->idFrom ) {
					$userFrom = UserModel::model()->findByPk( $AR->idFrom );
					$this->from = $userFrom->user_login;
				}
				if( $AR->idTo ) {
					$userTo = UserModel::model()->findByPk( $AR->idTo );
					$this->to = $userTo->user_login;
				}
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->messages[ $i18n->idLanguage ] = $i18n->message;
				}
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'from' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->idFrom = $this->userFrom->ID;
				$AR->idTo = $this->userTo->ID;
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'message' => $this->messages[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );				
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>