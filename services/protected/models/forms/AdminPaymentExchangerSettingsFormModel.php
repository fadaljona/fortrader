<?php
Yii::import('models.base.SettingsFormModelBase');

final class AdminPaymentExchangerSettingsFormModel extends SettingsFormModelBase
{
    public $mainPageOgImage;
    public $exchangersRatingPageOgImage;
    public $paymentSystemsRatingPageOgImage;
    public $paymentExchangeDirectionPageOgImage;

    public $mainTitle = array();
    public $mainMetaTitle = array();
    public $mainMetaDesc = array();
    public $mainMetaKeys = array();
    public $mainShortDesc = array();
    public $mainFullDescTitle = array();
    public $mainFullDesc = array();

    public $defaultExchangeDirectionTitle = array();
    public $defaultExchangeDirectionMetaTitle = array();
    public $defaultExchangeDirectionMetaDesc = array();
    public $defaultExchangeDirectionMetaKeys = array();
    public $defaultExchangeDirectionShortDesc = array();
    public $defaultExchangeDirectionFullDescTitle = array();
    public $defaultExchangeDirectionFullDesc = array();

    public $exchangersRatingTitle = array();
    public $exchangersRatingMetaTitle = array();
    public $exchangersRatingMetaDesc = array();
    public $exchangersRatingMetaKeys = array();
    public $exchangersRatingShortDesc = array();
    public $exchangersRatingFullDescTitle = array();
    public $exchangersRatingFullDesc = array();

    public $textFields = array(
        'mainTitle' => 'textFieldRow',
        'mainMetaTitle' => 'textFieldRow',
        'mainMetaDesc' => 'textAreaRow',
        'mainMetaKeys' => 'textAreaRow',
        'mainShortDesc' => 'textAreaRow',
        'mainFullDescTitle' => 'textFieldRow',
        'mainFullDesc' => 'textAreaRow',

        'defaultExchangeDirectionTitle' => 'textFieldRow',
        'defaultExchangeDirectionMetaTitle' => 'textFieldRow',
        'defaultExchangeDirectionMetaDesc' => 'textAreaRow',
        'defaultExchangeDirectionMetaKeys' => 'textAreaRow',
        'defaultExchangeDirectionShortDesc' => 'textAreaRow',
        'defaultExchangeDirectionFullDescTitle' => 'textFieldRow',
        'defaultExchangeDirectionFullDesc' => 'textAreaRow',

        'exchangersRatingTitle' => 'textFieldRow',
        'exchangersRatingMetaTitle' => 'textFieldRow',
        'exchangersRatingMetaDesc' => 'textAreaRow',
        'exchangersRatingMetaKeys' => 'textAreaRow',
        'exchangersRatingShortDesc' => 'textAreaRow',
        'exchangersRatingFullDescTitle' => 'textFieldRow',
        'exchangersRatingFullDesc' => 'textAreaRow',
    );

    protected function getSourceAttributeLabels()
    {
        $labels = array(
            'mainPageOgImage' => 'Main page Og Image',
            'exchangersRatingPageOgImage' => 'Exchangers rating page Og Image',
            'paymentSystemsRatingPageOgImage' => 'Payment systems rating page Og Image',
            'paymentExchangeDirectionPageOgImage' => 'Default exchange direction page Og Image'
        );
        
        return $this->setTextLabels($labels);
    }
    public function rules()
    {
        return array(
            array(
                'mainPageOgImage, exchangersRatingPageOgImage, paymentSystemsRatingPageOgImage, paymentExchangeDirectionPageOgImage',
                'length',
                'max' => CommonLib::maxByte
            ),
            array(
                'mainTitle, mainMetaTitle, mainFullDescTitle, defaultExchangeDirectionTitle, defaultExchangeDirectionMetaTitle, defaultExchangeDirectionFullDescTitle, exchangersRatingTitle, exchangersRatingMetaTitle, exchangersRatingFullDescTitle',
                'checkLens',
                'max' => CommonLib::maxByte
            ),
            array(
                'mainMetaDesc, mainMetaKeys, mainShortDesc, mainFullDesc, defaultExchangeDirectionMetaDesc, defaultExchangeDirectionMetaKeys, defaultExchangeDirectionShortDesc, defaultExchangeDirectionFullDesc, exchangersRatingMetaDesc, exchangersRatingMetaKeys, exchangersRatingShortDesc, exchangersRatingFullDesc',
                'checkLens',
                'max' => CommonLib::maxWord
            ),
        );
    }
    public function loadAR($pk = 0)
    {
        $AR = PaymentExchangerSettingsModel::getModel();
        $this->setAR($AR);
        return true;
    }
}