<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminAdvertisementBlockFormModel extends FormModelBase {
		public $id;
		public $ip;
		public $agent;
		function getARClassName() {
			return "AdvertisementBlockModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'ip' => 'IP',
				'agent' => 'Agent',
			);
		}
		function rules() {
			return Array(
				Array( 'ip', 'validateIP' ),
				Array( 'agent', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		function validateIP() {
			$AR = $this->getAR();
			if( !$this->ip and !$this->agent ) {
				$this->addError( 'ip', Yii::t( 'yii', '{attribute} cannot be blank.', Array(
					'{attribute}' => $this->getAttributeLabel( "ip" ),
				)));
				return;
			}
			if( $this->ip ) {
				$long = ip2long( $this->ip );
				if( !$long or !preg_match( CommonLib::PATTERN_IP, $this->ip )) {
					$this->addError( 'ip', Yii::t( '*', '{attribute} not valid IP-Address.', Array(
						'{attribute}' => $this->getAttributeLabel( "ip" ),
					)));
					return;
				}
				$long = sprintf( "%u", $long );
				$exists = AdvertisementBlockModel::model()->findByAttributes( Array( 'ip' => $long ));
				if( $exists and ( $AR->isNewRecord or $exists->id != $this->id )) {
					$this->addError( 'ip', Yii::t( 'yii', '{attribute} "{value}" has already been taken.', Array(
						'{attribute}' => $this->getAttributeLabel( "ip" ),
						'{value}' => CHtml::encode( $this->ip ),
					)));
				}
			}
			if( $this->agent ) {
				$exists = AdvertisementBlockModel::model()->findByAttributes( Array( 'agent' => $this->agent ));
				if( $exists and ( $AR->isNewRecord or $exists->id != $this->id )) {
					$this->addError( 'ip', Yii::t( 'yii', '{attribute} "{value}" has already been taken.', Array(
						'{attribute}' => $this->getAttributeLabel( "agent" ),
						'{value}' => CHtml::encode( $this->agent ),
					)));
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array() ) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				$this->ip = $AR->ip !== null ? long2ip( $AR->ip ) : '';
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array() ) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				$AR->ip = strlen( $this->ip ) ? sprintf( "%u", ip2long( $this->ip )) : null;
				return true;
			}
			return false;
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( !$this->ip ) $this->ip = null;
				if( !$this->agent ) $this->agent = null;
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>