<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminContestMemberLogFormModel extends FormModelBase {
		public $id;
		public $message;
		function getARClassName() {
			return "ContestMemberLogModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'message' => 'Message',
			);
		}
		function rules() {
			return Array(
				Array( 'message', 'required' ),
				Array( 'message', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>