<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminSettingsFormModel extends FormModelBase {
			# common
		public $common_mailForMailing;
		public $common_adClickCost;
		public $common_delimiter;
		public $common_importAccountData;
		public $common_logImportAccountData;
		public $common_eaPerPage;
		public $common_numberOfNotifications;
		public $servicesPostsCat;
			# smtp
		public $smtp_host;
		public $smtp_port;
		public $smtp_username;
		public $smtp_password;
			# clickatell
		public $clickatell_api_id;
		public $clickatell_user;
		public $clickatell_password;
			# importMembers
		public $importMembers_loginForMessage;
		public $importMembers_mail_host;
		public $importMembers_mail_login;
		public $importMembers_mail_pass;
			# Social media
		public $vkAppId;
		public $vkAppSecretKey;
		public $fbAppId;
		public $fbAppSecretKey;
		
		
		protected function getSourceAttributeLabels() {
			return Array(
					# common
				'common_mailForMailing' => 'EMail for mailing',
				'common_adClickCost' => 'Ad click cost',
				'common_delimiter' => 'Dilimiter when exporting',
				'common_importAccountData' => 'Import account data',
				'common_logImportAccountData' => 'Log import account data',
				'common_eaPerPage' => 'EA count per page',
				'common_numberOfNotifications' => 'Number of notifications',
				'servicesPostsCat' => 'Id wp cat for services posts',
					# smtp
				'smtp_host' => 'Host',
				'smtp_port' => 'Port',
				'smtp_username' => 'Username',
				'smtp_password' => 'Password',
					# clickatell
				'clickatell_api_id' => 'api_id',
				'clickatell_user' => 'user',
				'clickatell_password' => 'password',
					# importMembers
				'importMembers_loginForMessage' => 'Login for messages',
				'importMembers_mail_host' => 'POP3-Host',
				'importMembers_mail_login' => 'POP3-Login',
				'importMembers_mail_pass' => 'POP3-Password',
					# Social media
				'vkAppId' => 'Vk app id',
				'vkAppSecretKey' => 'Vk app secret key',
				'fbAppId' =>  'Facebook app id',
				'fbAppSecretKey' =>  'Facebook app secret key',
			);
		}
		function rules() {
			return Array(
				Array( 'common_mailForMailing, common_delimiter, vkAppSecretKey, fbAppSecretKey', 'length', 'max' => CommonLib::maxByte ),
				Array( 'servicesPostsCat, vkAppId, fbAppId', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'common_adClickCost', 'numerical', 'min' => 0 ),
				Array( 'common_importAccountData', 'in', 'range' => Array( 'Enabled', 'Disabled' )),
				Array( 'smtp_host, smtp_username, smtp_password', 'length', 'max' => CommonLib::maxByte ),
				Array( 'smtp_port', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'clickatell_api_id, clickatell_user, clickatell_password', 'length', 'max' => CommonLib::maxByte ),
				Array( 'importMembers_loginForMessage, importMembers_mail_host, importMembers_mail_login, importMembers_mail_pass', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		function loadAR( $pk = 0 ) {
			$AR = SettingsModel::getModel();
			$this->setAR( $AR );
			return true;
		}
		function load( $id = 0 ) {
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR( null, Array(), Array( 'smtp_password', 'clickatell_password', 'importMembers_mail_pass' ));
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array( 'smtp_password', 'clickatell_password', 'importMembers_mail_pass' ));
			if( strlen( $this->smtp_password )) $AR->setCrypt( 'smtp_password', $this->smtp_password );
			if( strlen( $this->clickatell_password )) $AR->setCrypt( 'clickatell_password', $this->clickatell_password );
			if( strlen( $this->importMembers_mail_pass )) $AR->setCrypt( 'importMembers_mail_pass', $this->importMembers_mail_pass );
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>