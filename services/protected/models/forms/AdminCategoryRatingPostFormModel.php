<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminCategoryRatingPostFormModel extends FormModelBase {
		public $id;
		public $title;
		public $inRating;
		public $postTitle;
		
		public $excludeFieldsFromAr = array( 'title', 'inRating', 'postTitle' );
		
		
		function getARClassName() {
			return "CategoryRatingPostModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'title' => 'Title',
				'inRating' => 'In Rating?',
			);
			return $labels;
		}
		function rules() {
			$rules = Array(
				Array( 'title', 'length', 'min' => 1, 'max' => CommonLib::maxByte ),
				Array( 'inRating', 'boolean'),
			);
			return $rules;
		}


		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$ratingData = $AR->ratingData;
	
				$this->title = $ratingData->title;
				$this->inRating = $ratingData ? $ratingData->inRating : 1;
				
				$this->postTitle = $AR->post_title;
				
				return true;
			}
			return false;
		}

		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				
				$arrToSave = Array();
				$arrToSave[ 'title' ] = $this->title;
				$arrToSave[ 'inRating' ] = $this->inRating;
				
				$AR->setData( (object)$arrToSave );

				return true;
			}
			return false;
		}
		
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( !$id ) return false;
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				
				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>