<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminMessageFormModel extends FormModelBase {
		public $id;
		public $key;
		public $ns = '*';
		public $type;
		public $values = Array();
		function getARClassName() {
			return "MessageModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'key' => 'Key',
				'ns' => 'Group',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "values[{$language->id}]" ] = $language->name;
			}
						
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'key, ns', 'required' ),
				Array( 'key, ns', 'length', 'max' => 165 ),
				Array( 'type', 'in', 'range' => Array( 'message', 'text' )),
				Array( 'values', 'checkLensValues' ),
				Array( 'key', 'uniqueMessageKey' ),
			);
		}
		function checkLensValues() {
			$NSi18n = $this->getNSi18n();
			$max = CommonLib::maxWord;
			foreach( $this->values as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $max ) {
					$this->addError( "values", Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "values[{$idLanguage}]" ),
						'{max}' => $max,
					)));
				}
			}
		}
		function uniqueMessageKey() {
			$message = MessageModel::model()->findByAttributes( Array( 'key' => $this->key, 'ns' => $this->ns ));
			if( $message ) {
				if( !$message->isNewRecord and $message->id == $this->id ) return;
				$this->addI18NError( 'key', 'Key has already been taken.' );
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR( null, null, Array( 'values' ));
				
				foreach( $AR->i18ns as $i18n ) {
					$this->values[ $i18n->idLanguage ] = $i18n->value;
				}
				
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				$AR->setI18Ns( $this->values );
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>