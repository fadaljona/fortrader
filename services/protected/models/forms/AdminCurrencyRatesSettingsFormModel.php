<?
Yii::import( 'models.base.SettingsFormModelBase' );

final class AdminCurrencyRatesSettingsFormModel extends SettingsFormModelBase{
	
	public $ogListImage;
	public $tomorrowOgListImage;
	public $archiveOgImage;
	public $converterOgImage;

	public $description = Array();
	public $title = Array();
	public $metaTitle = Array();
	public $metaDesc = Array();
	public $metaKeys = Array();
	public $fullDesc = Array();
	
	public $ecbDescription = Array();
	public $ecbTitle = Array();
	public $ecbMetaTitle = Array();
	public $ecbMetaDesc = Array();
	public $ecbMetaKeys = Array();
	public $ecbFullDesc = Array();
	
	public $tomorrowDescription = Array();
	public $tomorrowMetaTitle = Array();
	public $tomorrowMetaDesc = Array();
	public $tomorrowMetaKeys = Array();
	public $tomorrowTitle = Array();
	public $tomorrowFullDesc = Array();
	
	
	public $archiveTitle = Array();
	public $archiveMetaTitle = Array();
	public $archiveMetaDesc = Array();
	public $archiveMetaKeys = Array();
	public $archiveDescription = Array();
	public $archiveFullDesc = Array();
	
	public $converterTitle = Array();
	public $converterMetaTitle = Array();
	public $converterMetaDesc = Array();
	public $converterMetaKeys = Array();
	public $converterDescription = Array();
	public $converterFullDesc = Array();
	public $converterSubTitle1 = Array();
	public $converterSubTitle2 = Array();
	
	public $ecbConverterTitle = Array();
	public $ecbConverterMetaTitle = Array();
	public $ecbConverterMetaDesc = Array();
	public $ecbConverterMetaKeys = Array();
	public $ecbConverterDescription = Array();
	public $ecbConverterFullDesc = Array();
	public $ecbConverterSubTitle1 = Array();
	public $ecbConverterSubTitle2 = Array();
	
	public $archiveYearTitle = Array();
	public $archiveYearMoTitle = Array();
	public $archiveYearMoDayTitle = Array();
	
	public $archiveYearMetaTitle = Array();
	public $archiveYearMoMetaTitle = Array();
	public $archiveYearMoDayMetaTitle = Array();
	
	public $archiveYearMetaDesc = Array();
	public $archiveYearMoMetaDesc = Array();
	public $archiveYearMoDayMetaDesc = Array();
	
	public $archiveYearMetaKeys = Array();
	public $archiveYearMoMetaKeys = Array();
	public $archiveYearMoDayMetaKeys = Array();
	
	public $archiveYearDescription = Array();
	public $archiveYearMoDescription = Array();
	public $archiveYearMoDayDescription = Array();
	
	public $archiveYearFullDesc = Array();
	public $archiveYearMoFullDesc = Array();
	public $archiveYearMoDayFullDesc = Array();
	
	
	
	
	public $defaultSingleArchiveYearTitle = Array();
	public $defaultSingleArchiveYearMoTitle = Array();
	public $defaultSingleArchiveYearMoDayTitle = Array();
	
	public $defaultSingleArchiveYearMetaTitle = Array();
	public $defaultSingleArchiveYearMoMetaTitle = Array();
	public $defaultSingleArchiveYearMoDayMetaTitle = Array();
	
	public $defaultSingleArchiveYearMetaDesc = Array();
	public $defaultSingleArchiveYearMoMetaDesc = Array();
	public $defaultSingleArchiveYearMoDayMetaDesc = Array();
	
	public $defaultSingleArchiveYearMetaKeys = Array();
	public $defaultSingleArchiveYearMoMetaKeys = Array();
	public $defaultSingleArchiveYearMoDayMetaKeys = Array();
	
	public $defaultSingleArchiveYearDescription = Array();
	public $defaultSingleArchiveYearMoDescription = Array();
	public $defaultSingleArchiveYearMoDayDescription = Array();
	
	public $defaultSingleArchiveYearFullDesc = Array();
	public $defaultSingleArchiveYearMoFullDesc = Array();
	public $defaultSingleArchiveYearMoDayFullDesc = Array();
	
	
	
	public $ecbArchiveTitle = Array();
	public $ecbArchiveMetaTitle = Array();
	public $ecbArchiveMetaDesc = Array();
	public $ecbArchiveMetaKeys = Array();
	public $ecbArchiveDescription = Array();
	public $ecbArchiveFullDesc = Array();
	
	public $ecbArchiveYearTitle = Array();
	public $ecbArchiveYearMoTitle = Array();
	public $ecbArchiveYearMoDayTitle = Array();
	
	public $ecbArchiveYearMetaTitle = Array();
	public $ecbArchiveYearMoMetaTitle = Array();
	public $ecbArchiveYearMoDayMetaTitle = Array();
	
	public $ecbArchiveYearMetaDesc = Array();
	public $ecbArchiveYearMoMetaDesc = Array();
	public $ecbArchiveYearMoDayMetaDesc = Array();
	
	public $ecbArchiveYearMetaKeys = Array();
	public $ecbArchiveYearMoMetaKeys = Array();
	public $ecbArchiveYearMoDayMetaKeys = Array();
	
	public $ecbArchiveYearDescription = Array();
	public $ecbArchiveYearMoDescription = Array();
	public $ecbArchiveYearMoDayDescription = Array();
	
	public $ecbArchiveYearFullDesc = Array();
	public $ecbArchiveYearMoFullDesc = Array();
	public $ecbArchiveYearMoDayFullDesc = Array();
	
	public $ecbDefaultSingleArchiveYearTitle = Array();
	public $ecbDefaultSingleArchiveYearMoTitle = Array();
	public $ecbDefaultSingleArchiveYearMoDayTitle = Array();
	
	public $ecbDefaultSingleArchiveYearMetaTitle = Array();
	public $ecbDefaultSingleArchiveYearMoMetaTitle = Array();
	public $ecbDefaultSingleArchiveYearMoDayMetaTitle = Array();
	
	public $ecbDefaultSingleArchiveYearMetaDesc = Array();
	public $ecbDefaultSingleArchiveYearMoMetaDesc = Array();
	public $ecbDefaultSingleArchiveYearMoDayMetaDesc = Array();
	
	public $ecbDefaultSingleArchiveYearMetaKeys = Array();
	public $ecbDefaultSingleArchiveYearMoMetaKeys = Array();
	public $ecbDefaultSingleArchiveYearMoDayMetaKeys = Array();
	
	public $ecbDefaultSingleArchiveYearDescription = Array();
	public $ecbDefaultSingleArchiveYearMoDescription = Array();
	public $ecbDefaultSingleArchiveYearMoDayDescription = Array();
	
	public $ecbDefaultSingleArchiveYearFullDesc = Array();
	public $ecbDefaultSingleArchiveYearMoFullDesc = Array();
	public $ecbDefaultSingleArchiveYearMoDayFullDesc = Array();
	
	
	public $textFields = array(
		'title' => 'textFieldRow',
		'description' => 'textAreaRow',
		'metaTitle' => 'textFieldRow',
		'metaDesc' => 'textFieldRow',
		'metaKeys' => 'textFieldRow',
		'fullDesc' => 'textAreaRow',
		
		'tomorrowTitle' => 'textFieldRow',
		'tomorrowDescription' => 'textAreaRow',
		'tomorrowMetaTitle' => 'textFieldRow',
		'tomorrowMetaDesc' => 'textFieldRow',
		'tomorrowMetaKeys' => 'textFieldRow',
		'tomorrowFullDesc' => 'textAreaRow',
		
		'ecbTitle' => 'textFieldRow',
		'ecbDescription' => 'textAreaRow',
		'ecbMetaTitle' => 'textFieldRow',
		'ecbMetaDesc' => 'textFieldRow',
		'ecbMetaKeys' => 'textFieldRow',
		'ecbFullDesc' => 'textAreaRow',
		
		'archiveTitle' => 'textFieldRow',
		'archiveMetaTitle' => 'textFieldRow',
		'archiveMetaDesc' => 'textFieldRow',
		'archiveMetaKeys' => 'textFieldRow',
		'archiveDescription' => 'textAreaRow',
		'archiveFullDesc' => 'textAreaRow',
		
		
		
		'archiveYearTitle' => 'textFieldRow',
		'archiveYearMetaTitle' => 'textFieldRow',
		'archiveYearMetaDesc' => 'textFieldRow',
		'archiveYearMetaKeys' => 'textFieldRow',
		'archiveYearDescription' => 'textAreaRow',
		'archiveYearFullDesc' => 'textAreaRow',
		
		'archiveYearMoTitle' => 'textFieldRow',
		'archiveYearMoMetaTitle' => 'textFieldRow',
		'archiveYearMoMetaDesc' =>  'textFieldRow',
		'archiveYearMoMetaKeys' => 'textFieldRow',
		'archiveYearMoDescription' => 'textAreaRow',
		'archiveYearMoFullDesc' => 'textAreaRow',
		
		'archiveYearMoDayTitle' => 'textFieldRow',
		'archiveYearMoDayMetaTitle' => 'textFieldRow',
		'archiveYearMoDayMetaDesc' => 'textFieldRow',
		'archiveYearMoDayMetaKeys' => 'textFieldRow',
		'archiveYearMoDayDescription' => 'textAreaRow',
		'archiveYearMoDayFullDesc' => 'textAreaRow',
		
		'defaultSingleArchiveYearTitle' => 'textFieldRow',
		'defaultSingleArchiveYearMetaTitle' => 'textFieldRow',
		'defaultSingleArchiveYearMetaDesc' => 'textFieldRow',
		'defaultSingleArchiveYearMetaKeys' => 'textFieldRow',
		'defaultSingleArchiveYearDescription' => 'textAreaRow',
		'defaultSingleArchiveYearFullDesc' => 'textAreaRow',
		
		'defaultSingleArchiveYearMoTitle' => 'textFieldRow',
		'defaultSingleArchiveYearMoMetaTitle' => 'textFieldRow',
		'defaultSingleArchiveYearMoMetaDesc' =>  'textFieldRow',
		'defaultSingleArchiveYearMoMetaKeys' => 'textFieldRow',
		'defaultSingleArchiveYearMoDescription' => 'textAreaRow',
		'defaultSingleArchiveYearMoFullDesc' => 'textAreaRow',
		
		'defaultSingleArchiveYearMoDayTitle' => 'textFieldRow',
		'defaultSingleArchiveYearMoDayMetaTitle' => 'textFieldRow',
		'defaultSingleArchiveYearMoDayMetaDesc' => 'textFieldRow',
		'defaultSingleArchiveYearMoDayMetaKeys' => 'textFieldRow',
		'defaultSingleArchiveYearMoDayDescription' => 'textAreaRow',
		'defaultSingleArchiveYearMoDayFullDesc' => 'textAreaRow',
		
		
		
		'ecbArchiveTitle' => 'textFieldRow',
		'ecbArchiveMetaTitle' => 'textFieldRow',
		'ecbArchiveMetaDesc' => 'textFieldRow',
		'ecbArchiveMetaKeys' => 'textFieldRow',
		'ecbArchiveDescription' => 'textAreaRow',
		'ecbArchiveFullDesc' => 'textAreaRow',
		
		'ecbArchiveYearTitle' => 'textFieldRow',
		'ecbArchiveYearMetaTitle' => 'textFieldRow',
		'ecbArchiveYearMetaDesc' => 'textFieldRow',
		'ecbArchiveYearMetaKeys' => 'textFieldRow',
		'ecbArchiveYearDescription' => 'textAreaRow',
		'ecbArchiveYearFullDesc' => 'textAreaRow',
		
		'ecbArchiveYearMoTitle' => 'textFieldRow',
		'ecbArchiveYearMoMetaTitle' => 'textFieldRow',
		'ecbArchiveYearMoMetaDesc' =>  'textFieldRow',
		'ecbArchiveYearMoMetaKeys' => 'textFieldRow',
		'ecbArchiveYearMoDescription' => 'textAreaRow',
		'ecbArchiveYearMoFullDesc' => 'textAreaRow',
		
		'ecbArchiveYearMoDayTitle' => 'textFieldRow',
		'ecbArchiveYearMoDayMetaTitle' => 'textFieldRow',
		'ecbArchiveYearMoDayMetaDesc' => 'textFieldRow',
		'ecbArchiveYearMoDayMetaKeys' => 'textFieldRow',
		'ecbArchiveYearMoDayDescription' => 'textAreaRow',
		'ecbArchiveYearMoDayFullDesc' => 'textAreaRow',
		
		'ecbDefaultSingleArchiveYearTitle' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMetaTitle' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMetaDesc' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMetaKeys' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearDescription' => 'textAreaRow',
		'ecbDefaultSingleArchiveYearFullDesc' => 'textAreaRow',
		
		'ecbDefaultSingleArchiveYearMoTitle' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMoMetaTitle' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMoMetaDesc' =>  'textFieldRow',
		'ecbDefaultSingleArchiveYearMoMetaKeys' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMoDescription' => 'textAreaRow',
		'ecbDefaultSingleArchiveYearMoFullDesc' => 'textAreaRow',
		
		'ecbDefaultSingleArchiveYearMoDayTitle' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMoDayMetaTitle' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMoDayMetaDesc' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMoDayMetaKeys' => 'textFieldRow',
		'ecbDefaultSingleArchiveYearMoDayDescription' => 'textAreaRow',
		'ecbDefaultSingleArchiveYearMoDayFullDesc' => 'textAreaRow',
		
		
		
		'converterTitle' => 'textFieldRow',
		'converterMetaTitle' => 'textFieldRow',
		'converterMetaDesc' => 'textFieldRow',
		'converterMetaKeys' => 'textFieldRow',
		'converterDescription' => 'textAreaRow',
		'converterFullDesc' => 'textAreaRow',
		'converterSubTitle1' => 'textFieldRow',
		'converterSubTitle2' => 'textFieldRow',
		
		'ecbConverterTitle' => 'textFieldRow',
		'ecbConverterMetaTitle' => 'textFieldRow',
		'ecbConverterMetaDesc' => 'textFieldRow',
		'ecbConverterMetaKeys' => 'textFieldRow',
		'ecbConverterDescription' => 'textAreaRow',
		'ecbConverterFullDesc' => 'textAreaRow',
		'ecbConverterSubTitle1' => 'textFieldRow',
		'ecbConverterSubTitle2' => 'textFieldRow',
	);

	protected function getSourceAttributeLabels() {
		$labels = Array(
			'ogListImage' => 'og List Image',
			'tomorrowOgListImage' => 'tomorrow Og List Image',
			'archiveOgImage' => 'Archive Og Image',
			'converterOgImage' => 'Converter Og Image',
		);
		return $this->setTextLabels( $labels );
	}
	function rules() {
		return Array(
			Array( 'ogListImage, tomorrowOgListImage, archiveOgImage, converterOgImage', 'length', 'max' => CommonLib::maxByte ),
			
			Array( 'title, metaTitle, metaDesc, metaKeys, tomorrowTitle, tomorrowMetaTitle, tomorrowMetaDesc, tomorrowMetaKeys, archiveTitle, archiveMetaTitle, archiveMetaDesc, archiveMetaKeys, converterTitle, converterMetaTitle, converterMetaDesc, converterMetaKeys, converterSubTitle1, converterSubTitle2, archiveYearTitle, archiveYearMoTitle, archiveYearMoDayTitle, archiveYearMetaTitle, archiveYearMoMetaTitle, archiveYearMoDayMetaTitle, archiveYearMetaDesc, archiveYearMoMetaDesc, archiveYearMoDayMetaDesc, archiveYearMetaKeys, archiveYearMoMetaKeys, archiveYearMoDayMetaKeys, defaultSingleArchiveYearTitle, defaultSingleArchiveYearMetaTitle, defaultSingleArchiveYearMetaDesc, defaultSingleArchiveYearMetaKeys, defaultSingleArchiveYearMoTitle, defaultSingleArchiveYearMoMetaTitle, defaultSingleArchiveYearMoMetaDesc, defaultSingleArchiveYearMoMetaKeys, defaultSingleArchiveYearMoDayTitle, defaultSingleArchiveYearMoDayMetaTitle, defaultSingleArchiveYearMoDayMetaDesc, defaultSingleArchiveYearMoDayMetaKeys, ecbConverterTitle, ecbConverterMetaTitle, ecbConverterMetaDesc, ecbConverterMetaKeys, ecbConverterSubTitle1, ecbConverterSubTitle2, ecbTitle, ecbMetaTitle, ecbMetaDesc, ecbMetaKeys, ecbArchiveYearTitle, ecbArchiveYearMetaTitle, ecbArchiveYearMetaDesc, ecbArchiveYearMetaKeys, ecbArchiveYearMoTitle, ecbArchiveYearMoMetaTitle, ecbArchiveYearMoMetaDesc, ecbArchiveYearMoMetaKeys, ecbArchiveYearMoDayTitle, ecbArchiveYearMoDayMetaTitle, ecbArchiveYearMoDayMetaDesc, ecbArchiveYearMoDayMetaKeys, ecbDefaultSingleArchiveYearTitle, ecbDefaultSingleArchiveYearMetaTitle, ecbDefaultSingleArchiveYearMetaDesc, ecbDefaultSingleArchiveYearMetaKeys, ecbDefaultSingleArchiveYearMoTitle, ecbDefaultSingleArchiveYearMoMetaTitle, ecbDefaultSingleArchiveYearMoMetaDesc, ecbDefaultSingleArchiveYearMoMetaKeys, ecbDefaultSingleArchiveYearMoDayTitle, ecbDefaultSingleArchiveYearMoDayMetaTitle, ecbDefaultSingleArchiveYearMoDayMetaDesc, ecbDefaultSingleArchiveYearMoDayMetaKeys, ecbArchiveTitle, ecbArchiveMetaTitle, ecbArchiveMetaDesc, ecbArchiveMetaKeys', 'checkLens', 'max' => CommonLib::maxByte ),
			
			Array( 'description, tomorrowDescription, fullDesc, tomorrowFullDesc, archiveDescription, archiveFullDesc, converterDescription, converterFullDesc, archiveYearDescription, archiveYearMoDescription, archiveYearMoDayDescription, archiveYearFullDesc, archiveYearMoFullDesc, archiveYearMoDayFullDesc, defaultSingleArchiveYearDescription, defaultSingleArchiveYearFullDesc, defaultSingleArchiveYearMoDescription, defaultSingleArchiveYearMoFullDesc, defaultSingleArchiveYearMoDayDescription, defaultSingleArchiveYearMoDayFullDesc, ecbConverterDescription, ecbConverterFullDesc, ecbDescription, ecbFullDesc, ecbArchiveYearDescription, ecbArchiveYearFullDesc, ecbArchiveYearMoDescription, ecbArchiveYearMoFullDesc, ecbArchiveYearMoDayDescription, ecbArchiveYearMoDayFullDesc, ecbDefaultSingleArchiveYearDescription, ecbDefaultSingleArchiveYearFullDesc, ecbDefaultSingleArchiveYearMoDescription, ecbDefaultSingleArchiveYearMoFullDesc, ecbDefaultSingleArchiveYearMoDayDescription, ecbDefaultSingleArchiveYearMoDayFullDesc, ecbArchiveDescription, ecbArchiveFullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
		);
	}
	function loadAR( $pk = 0 ) {
		$AR = CurrencyRatesSettingsModel::getModel();
		$this->setAR( $AR );
		return true;
	}
}