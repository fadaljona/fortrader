<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminEAMarkFormModel extends FormModelBase {
		public $concept;
		public $result;
		public $stability;
		protected $idEA;
		function setIDEA( $idEA ) {
			$this->idEA = $idEA;
		}
		function getARClassName() {
			return "EAMarkModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'concept' => 'Idea',
				'result' => 'Result',
				'stability' => 'Stability',
			);
		}
		function rules() {
			return Array(
				Array( 'concept, result, stability', 'numerical', 'min' => 0, 'max' => 100, 'integerOnly' => true ),
			);
		}
		function loadAR( $pk = 0 ) {
			$EA = EAModel::model()->findByPk( $this->idEA );
			if( !$EA ) return false;
			$AR = $EA->getUserMark();
			$this->setAR( $AR );
			return true;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array() ) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				$AR->average = round( ( $this->concept + $this->result + $this->stability ) / 3 );
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>