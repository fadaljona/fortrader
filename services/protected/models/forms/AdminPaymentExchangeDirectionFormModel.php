<?php

Yii::import('models.base.FormModelBase');

final class AdminPaymentExchangeDirectionFormModel extends FormModelBase
{
    public $id;
    public $direction;
    public $ogImageFile;

    public $title;
    public $metaTitle;
    public $metaDesc;
    public $metaKeys;
    public $shortDesc;
    public $fullDescTitle;
    public $fullDesc;


    public $excludeFieldsFromAr = array(
        'direction',
        'ogImageFile',
        'title',
        'metaTitle',
        'metaDesc',
        'metaKeys',
        'shortDesc',
        'fullDescTitle',
        'fullDesc',
    );
        
    public static function getTextFields()
    {
        return array(
            'title' => array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'metaTitle' => array( 'modelField' => 'metaTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'metaDesc' => array( 'modelField' => 'metaDesc', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'metaKeys' => array( 'modelField' => 'metaKeys', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'shortDesc' => array( 'modelField' => 'shortDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'fullDescTitle' => array( 'modelField' => 'fullDescTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'fullDesc' => array( 'modelField' => 'fullDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
        );
    }
    public function getARClassName()
    {
        return "PaymentExchangeDirectionModel";
    }
    protected function getSourceAttributeLabels()
    {
        $labels = array(
            'ogImageFile' => 'Og image',
            'direction' => 'Exchange direction'
        );

        $languages = LanguageModel::getAll();
        foreach ($languages as $language) {
            $labels[ "title[{$language->id}]" ] = "Title";

            $labels[ "metaTitle[{$language->id}]" ] = "Meta title";
            $labels[ "metaDesc[{$language->id}]" ] = "Meta description";
            $labels[ "metaKeys[{$language->id}]" ] = "Meta keys";
            $labels[ "shortDesc[{$language->id}]" ] = "Short description";
            $labels[ "fullDescTitle[{$language->id}]" ] = "Full description title";
            $labels[ "fullDesc[{$language->id}]" ] = "Full description";
        }
        return $labels;
    }
    public function rules()
    {
        $rules = array(
            array( 'title', 'notEmptyCurrentLang' ),
            array( 'ogImageFile', 'file', 'allowEmpty' => true, 'types' => array( 'jpg', 'jpeg', 'gif', 'png' )),

            array( 'title, metaTitle, metaDesc, metaKeys, fullDescTitle', 'checkLens', 'max' => CommonLib::maxByte ),
            array( 'shortDesc, fullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
        );
        return $rules;
    }
    public function notEmptyCurrentLang($field, $params)
    {
        $NSi18n = $this->getNSi18n();
        $currentLangId = LanguageModel::getCurrentLanguageID();

        if (mb_strlen($this->{$field}[$currentLangId]) == 0) {
            $this->addError("{$field}[{$currentLangId}]", Yii::t( '*','{attribute} can not be empty.', array(
                '{attribute}' => $this->getAttributeLabel("{$field}[{$currentLangId}]"),
            )));
        }
    }
    public function checkLens($field, $params)
    {
        $NSi18n = $this->getNSi18n();
        foreach ($this->$field as $idLanguage => $value) {
            if (mb_strlen($value) > $params['max']) {
                $this->addError($field, Yii::t('yii','{attribute} is too long (maximum is {max} characters).', array(
                    '{attribute}' => $this->getAttributeLabel("{$field}[{$idLanguage}]"),
                    '{max}' => $params['max'],
                )));
            }
        }
    }


    public function loadFromAR($AR = null, $loadFields = array(), $exceptFields = array())
    {
        $exceptFields = $this->excludeFieldsFromAr;
        if (parent::loadFromAR($AR, $loadFields, $exceptFields)) {
            $AR = $this->getAR();

            $i18ns = $AR->i18ns;
            foreach ($i18ns as $i18n) {
                foreach ($this->textFields as $formFieldName => $settings) {
                    $this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
                }
            }

            $this->direction = $AR->directionTitle;
            
            $this->ogImageFile = $AR->srcOgImage;
            return true;
        }
        return false;
    }

    public function saveAR($runValidation = true, $attributes = null)
    {
        if (parent::saveAR($runValidation, $attributes)) {
            $AR = $this->getAR();
            $i18ns = array();
            $languages = LanguageModel::getAll();
            foreach ($languages as $language) {
                $arrToSave = array();

                foreach ($this->textFields as $formFieldName => $settings) {
                    $arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
                }
                $i18ns[ $language->id ] = (object)$arrToSave;
            }

            $AR->setI18Ns($i18ns);
            return true;
        }
        return false;
    }

    public function load($id = 0)
    {
        $post = $this->getPostLink();
        if ((int)@$post['id']) {
            $id = (int)@$post['id'];
        }
        if ($this->loadAR($id)) {
            $this->loadFromAR();
            $this->loadFromPost();
            $this->loadFromFiles(array( 'ogImageFile' ));
            return true;
        }
        return false;
    }

    public function save()
    {
        $AR = $this->getAR();
        if (!$AR) {
            return false;
        }

        $this->saveToAR(null, array(), array());
        if ($this->ogImageFile) {
            $AR->uploadOgImage($this->ogImageFile);
        }

        if ($this->saveAR(false)) {
            return $AR->getPrimaryKey();
        }
        return false;
    }
}