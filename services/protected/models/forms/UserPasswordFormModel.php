<?
	Yii::import( 'models.base.FormModelBase' );

	final class UserPasswordFormModel extends FormModelBase {
		public $ID;
		public $oldPassword;
		public $password;
		public $repeatPassword;
		function getARClassName() {
			return "UserModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'oldPassword' => 'Old password',
				'password' => 'New password',
				'repeatPassword' => 'Repeat password',
			);
		}
		function rules() {
			$NSi18n = $this->getNSi18n();
			return Array(
				Array( 'oldPassword, password, repeatPassword', 'required' ),
				Array( 'oldPassword', 'validateOldPassword' ),
				Array( 'repeatPassword', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t( $NSi18n, 'Repeat password wrong!' )),
				Array( 'password', 'length', 'min' => UserModel::MINPasswordLen ),
				Array( 'password', 'checkPasswordLen' ),
			);
		}
		function validateOldPassword() {
			$AR = $this->getAR();
			if( !$AR->isPassword( $this->oldPassword )) {
				$this->addI18NError( 'oldPassword', "Old password wrong" );
			}
		}
		function checkPasswordLen() {
			if( !$this->hasErrors()) {
				
				CommonLib::loadWp();
				
				$hash = wp_hash_password( $this->password );
				if( strlen( $hash ) > UserModel::MAXPasswordLen ) {
					$this->addI18NError( 'password', 'Password to long!' );
				}
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( !$id ) $id = (int)@$post['ID'];
			if( !$id ) return false;
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR( null, Array(), Array( 'password' ));
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$AR->setPassword( $this->password );
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>