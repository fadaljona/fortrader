<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminServerFormModel extends FormModelBase {
		public $id;
		public $name;
		public $internalName;
		public $url;
		public $idBroker;
		public $idTradePlatform;
		public $forReports;
		
		function getARClassName() {
			return "ServerModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'name' => 'Title',
				'internalName' => 'Internal title',
				'url' => 'URL',
				'idBroker' => 'Broker',
				'idTradePlatform' => 'Trade platform',
				'forReports' => 'For reports?'
			);
		}
		function rules() {
			return Array(
				Array( 'name, idBroker, idTradePlatform', 'required' ),
				Array( 'name, internalName, url', 'length', 'max' => CommonLib::maxByte ),
				//Array( 'name', 'uniqueField' ),
				Array( 'idBroker, idTradePlatform', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'idBroker', 'existsIDModel', 'model' => 'BrokerModel' ),
				Array( 'idTradePlatform', 'existsIDModel', 'model' => 'TradePlatformModel' ),
				Array( 'forReports', 'boolean'),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR( null, Array(), Array( 'ip' ));
				if( $this->internalName == $this->name ) $this->internalName = '';
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array( 'ip' ));
			if( !strlen( $this->internalName )) $AR->internalName = $this->name;
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>