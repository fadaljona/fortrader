<?
	Yii::import( 'models.base.FormModelBase' );
	Yii::import( 'models.forms.AdminEAStatementFormModel', true );

	final class EAStatementFormModel extends FormModelBase {
		public $id;
		public $idEA;
		public $idVersion;
		public $titles = Array();
		public $desces = Array();
		public $optimization;
		public $optimizationStartDate;
		public $optimizationEndDate;
		public $set;
		public $statement;
		public $graph;
		private $outputParseStatement;
		function getARClassName() {
			return "EAStatementModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'idEA' => 'EA',
				'idVersion' => 'EA version',
				'optimizationStartDate' => 'Start date',
				'optimizationEndDate' => 'End date',
				'set' => 'File set',
				'statement' => 'File statement',
				'graph' => 'Graph statement',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "titles[{$language->id}]" ] = "Title";
				$labels[ "desces[{$language->id}]" ] = "Description";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'idEA, idVersion', 'required' ),
				Array( 'idEA', 'existsIDModel', 'model' => 'EAModel' ),
				Array( 'idVersion', 'existsIDModel', 'model' => 'EAVersionModel' ),
				//Array( 'titles', 'validateTitles' ),
				Array( 'titles,desces', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'optimizationStartDate, optimizationEndDate', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'set', 'file', 'allowEmpty' => true, 'types' => Array( 'set' )),
				Array( 'statement', 'file', 'allowEmpty' => true, 'types' => Array( 'htm', 'html' )),
				Array( 'graph', 'file', 'allowEmpty' => true, 'types' => Array( 'jpg', 'gif' )),
			);
		}
		function validateTitles() {
			foreach( $this->titles as $idLanguage=>$title ) {
				if( strlen( $title )) return;
			}
			$NSi18n = $this->getNSi18n();
			$this->addError( "titles", Yii::t( 'yii','{attribute} cannot be blank.', Array( '{attribute}' => Yii::t( $NSi18n, 'Title' ))));
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( !$this->optimization ) {
					$this->optimizationStartDate = null;
					$this->optimizationEndDate = null;
				}
				else{
					$this->optimizationStartDate = str_replace( ".", "-", $this->optimizationStartDate );
					$this->optimizationEndDate = str_replace( ".", "-", $this->optimizationEndDate );
				}
				return true;
			}
			return false;
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'statement', 'graph' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->titles[ $i18n->idLanguage ] = $i18n->title;
					$this->desces[ $i18n->idLanguage ] = $i18n->desc;
				}
				
				$this->set = $AR->srcFileSet;
				$this->statement = $AR->srcFileStatement;
				$this->graph = $AR->srcFileGraph;
				
				if( $AR->optimizationStartDate or $AR->optimizationEndDate ) {
					$this->optimization = true;
				}
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'file' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( !$AR->idUser ) {
					$AR->idUser = Yii::App()->user->id;
				}
				if( !$AR->date ) {
					$AR->date = date( 'Y-m-d' );
				}
				
				if( !$AR->optimizationStartDate ) {
					$AR->optimizationStartDate = null;
				}
				if( !$AR->optimizationEndDate ) {
					$AR->optimizationEndDate = null;
				}
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'title' => strip_tags($this->titles[ $language->id ]),
						'desc' => strip_tags($this->desces[ $language->id ]),
					);
				}
				$AR->setI18Ns( $i18ns );				
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost(); 
				$this->loadFromFiles( Array( 'set', 'statement', 'graph' ));
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			if( $this->set ) {
				$AR->uploadFileSet( $this->set );
			}
			if( $this->statement ) {
				$AR->uploadFileStatement( $this->statement );
				$this->getStatementInfo();
			}
			
			if( $AR->isNewRecord ) {
				$languages = LanguageModel::getAll();
				if( $this->statement ) {
					$profit = (int)$this->outputParseStatement->profit;
					$drow = (int)$this->outputParseStatement->drow;
					
					$period = "";
					preg_match_all( "#\((.+)\)#U", $this->outputParseStatement->period, $matchs );
					if( count( $matchs[0]) > 0 ) {
						$period = $matchs[1][0];
					}
					
					foreach( $languages as $language ) {
						$this->titles[ $language->id ] = "{$this->outputParseStatement->symbol} ({$period}) {$profit}-{$drow}";
					}
				}
				else{
					foreach( $languages as $language ) {
						$this->titles[ $language->id ] = "";
					}
				}
			}
			else{
				$i18ns = EAStatementI18NModel::model()->findAllByAttributes( Array( 'idStatement' => $AR->id ));
				foreach( $i18ns as $i18ns ) {
					$this->titles[ $i18ns->idLanguage ] = $i18ns->title;
				}
			}
			
			if( $this->graph ) {
				$AR->uploadFileGraph( $this->graph );
			}
						
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
		function getStatementInfo() {
			$AR = $this->getAR();
			
			$statement = file_get_contents( $AR->pathFileStatement );
			$output = new StdClass();
			AdminEAStatementFormModelParseStatementAction::parse( $statement, $output );
			if( preg_match( "#Strategy Tester Report$#i", $output->nameStatement )) {
				$AR->type = "Backtest";
			}
			
				# server, broker
			$output->nameServer = preg_replace( "#\(.*\)#i", "", $output->nameServer );
			$output->nameServer = trim( $output->nameServer );
			$server = ServerModel::model()->findByAttributes( Array(
				'name' => $output->nameServer,
			));
			if( $server ) {
				$AR->idServer = $server->id;
				$AR->idBroker = $server->idBroker;
			}
			
				# symbol
			$output->symbol = preg_replace( "#\(.*\)#i", "", $output->symbol );
			$output->symbol = trim( $output->symbol );
			$AR->symbol = $output->symbol;
			
				# period
			preg_match_all( "#\((.+)\)#U", $output->period, $matchs );
			if( count( $matchs[0]) > 0 ) {
				$AR->period = $matchs[1][0];
				@list( $start, $end ) = explode( "-", $matchs[1][1] );
				if( $start and $end ) {
					$AR->testStartDate = trim( str_replace( '.', '-', $start ));
					$AR->testEndDate = trim( str_replace( '.', '-', $end ));
				}
			}
			
				# gain, drow
			$AR->gain = $output->deposit ? round( $output->profit / $output->deposit * 100, 2 ) : null;
			$drow = -(double)$output->drow;
			$AR->drow = $output->deposit ? round( $drow / $output->deposit * 100, 2 ) : null;
			
			$AR->trades = $output->trades;
			
			$this->outputParseStatement = $output;
		}
	}

?>