<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUserReputationSettingsFormModel extends FormModelBase {
		public $count_contestWinner;
		public $count_contestMember;
		public $count_eaVersionThank;
		public $count_eaStatmentThank;
		
		protected function getSourceAttributeLabels() {
			return Array(
					# common
				'count_contestWinner' => 'Contest winer',
				'count_contestMember' => 'Registration in contest',
				'count_eaVersionThank' => 'Thanked for ea version',
				'count_eaStatmentThank' => 'Thanked for statement',
			);
		}
		function rules() {
			return Array(
				Array( 'count_contestWinner, count_contestMember', 'numerical' ),
				Array( 'count_eaVersionThank, count_eaStatmentThank', 'numerical' ),
			);
		}
		function loadAR( $pk = 0 ) {
			$AR = UserReputationSettingsModel::getModel();
			$this->setAR( $AR );
			return true;
		}
		function load( $id = 0 ) {
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>