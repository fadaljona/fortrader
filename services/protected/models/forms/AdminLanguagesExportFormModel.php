<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminLanguagesExportFormModel extends FormModelBase {
		public $languages = Array();
		protected function getSourceAttributeLabels() {
			return Array(
				'languages' => 'Languages',
			);
		}
		function rules() {
			return Array(
				Array( 'languages', 'required' ),
			);
		}
		private function loadDefault() {
			$this->languages = LanguageModel::getExistsIDS();
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( empty( $post[ 'languages' ])) $this->languages = Array();
				$this->languages  = array_filter( (array)$this->languages, Array( 'CommonLib', 'isIDorZero' ));
			}
		}
		function load( $id = 0 ) {
			$this->loadDefault();
			$this->loadFromPost();
			return true;
		}
	}

?>