<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminAdvertisementSettingsFormModel extends FormModelBase {
		public $endBanalce;
		public $endDate;
		public $sendEmail;
		public $emails;
		public $ad_prefix;
		public $popupDelay;
		protected function getSourceAttributeLabels() {
			return Array(
				'endBanalce' => 'End balance',
				'endDate' => 'Campaign end date',
				'sendEmail' => 'Send mails',
				'emails' => 'Emails',
				'ad_prefix' => 'ad prefix',
				'popupDelay' => 'Popup ads delay',
			);
		}
		function rules() {
			return Array(
				Array( 'endBanalce, endDate, sendEmail', 'in', 'range' => Array( '0', '1' )),
				Array( 'emails', 'length', 'max' => CommonLib::maxByte ),
				Array( 'popupDelay', 'numerical', 'integerOnly' => true, 'min' => 0 ),
			);
		}
		function loadAR( $pk = 0 ) {
			$AR = AdvertisementSettingsModel::getModel();
			$this->setAR( $AR );
			return true;
		}
		function load( $id = 0 ) {
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>