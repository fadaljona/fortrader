<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminTradeAccountFormModel extends FormModelBase {
		public $id;
		public $name;
		public $accountNumber;
		public $password;
		public $idServer;
		public $status;
		public $type;
		public $groups;
		function getARClassName() {
			return "TradeAccountModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'name' => 'Title',
				'accountNumber' => 'Account number',
				'password' => 'Password',
				'idServer' => 'Trade server',
				'status' => 'Status',
				'type' => 'Type',
				'groups' => 'Groups',
			);
		}
		function rules() {
			return Array(
				Array( 'name, accountNumber, password, idServer, status, type, groups', 'required' ),
				Array( 'name, password', 'length', 'max' => CommonLib::maxByte ),
				Array( 'idServer', 'existsIDModel', 'model' => 'ServerModel' ),
				Array( 'status', 'in', 'range' => Array( 'On', 'Off' )),
				Array( 'type', 'in', 'range' => Array( 'Demo', 'Real' )),
			);
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'groups' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->groups = implode( ',', $this->groups );
				return true;
			}
			return false;
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'groups' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$this->groups = explode( ',', $AR->groups );
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>