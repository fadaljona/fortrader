<?
	Yii::import( 'models.base.FormModelBase' );

	final class QuoteForecastFormModel extends FormModelBase {
		public $symbolId;
		public $userId;
		public $startDate;
		public $endDate;
		public $value;
		public $status;
		
		function getARClassName() {
			return "QuotesForecastsModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'startDate' => 'Select Period',
				'value' => 'Goal'
			);
		}
		function rules() {
			return Array(
				Array( 'symbolId, userId, startDate, endDate, value', 'required' ),
				Array( 'symbolId, userId', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'status', 'numerical', 'integerOnly' => true, 'min' => -1, 'max' => 2 ),
				Array( 'value', 'numerical', 'integerOnly' => false, 'min' => -1 ),
				Array( 'startDate, endDate', 'date', 'format' => 'yyyy-MM-dd HH:mm:ss' ),
				Array( 'symbolId, userId, startDate, endDate', 'checkExist' ),
			);
		}
		public function checkExist($attribute,$params){
			$model = QuotesForecastsModel::model()->find(Array(
				'condition' => " `t`.`userId` = :userId AND `t`.`symbolId` = :symbolId AND `t`.`startDate` = :startDate AND `t`.`endDate` = :endDate ",
				'params' => array(':userId' => $this->userId, ':symbolId' => $this->symbolId, ':startDate' => $this->startDate, ':endDate' => $this->endDate ),
			));
			if( $model ){
				$this->addError('value','Forecast already exist');
			}
		}
		function load( $id = 0 ) {
			if( $this->loadAR()) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>