<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminCounterFormModel extends FormModelBase {
		public $id;
		public $name;
		public $code;
		public $enabled;
		public $common;
		function getARClassName() {
			return "CounterModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'name' => 'Title',
				'code' => 'Code',
				'enabled' => 'Enabled',
				'common' => 'Show in a common block',
			);
		}
		function rules() {
			return Array(
				Array( 'name, code, enabled, common', 'required' ),
				Array( 'name', 'length', 'max' => CommonLib::maxByte ),
				Array( 'code', 'length', 'max' => CommonLib::maxWord ),
				Array( 'enabled, common', 'in', 'range' => Array( '0', '1' )),
				Array( 'name', 'uniqueField' ),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>