<?
	Yii::import( 'models.base.FormModelBase' );

	final class UserMessageFormModel extends FormModelBase {
		public $id;
		public $instance;
		public $idLinkedObj;
		public $idMessageQuery;
		public $text;
		function getARClassName() {
			return "UserMessageModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'text' => 'Text',
			);
		}
		function rules() {
			return Array(
				Array( 'instance, text', 'required' ),
				Array( 'instance', 'in', 'range' => Array( 
					'ea/list', 'ea/single', 'eaVersion/single', 'eaStatement/single', 
					'eaTradeAccount/single', 'question/list', 'ea/laboratory', 'calendarEvent/list', 'calendarEvent/single', 'user/profile',
					'strategy/list'
				)),
				Array( 'idLinkedObj, idMessageQuery', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'text', 'length', 'max' => CommonLib::maxWord ),
			);
		}
		function loadFromPost() {
			if( parent::loadFromPost() ) {
				if( !$this->idLinkedObj ) $this->idLinkedObj = null;
				if( !$this->idMessageQuery ) $this->idMessageQuery = null;
			}
		}
		function loadAR( $pk = 0 ) {
			if( parent::loadAR( $pk )) {
				$AR = $this->getAR();
				if( $AR->isNewRecord ) {
					$AR->idUser = Yii::App()->user->id;
				}
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>