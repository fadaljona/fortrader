<?
	Yii::import( 'models.base.SettingsFormModelBase' );

	final class AdminJournalSettingsFormModel extends SettingsFormModelBase {
		public $buyLink;
		
		public $listTitle = Array();
		public $listDescription = Array();
		public $listMetaTitle = Array();
		public $listMetaDescription = Array();
		public $listMetaKeywords = Array();
		public $listOgImgUrl = Array();
		public $listFullDescription = Array();
		
		public $textFields = array(
			'listTitle' => 'textFieldRow',
			'listDescription' => 'textAreaRow',
			'listMetaTitle' => 'textFieldRow',
			'listMetaDescription' => 'textFieldRow',
			'listMetaKeywords' => 'textFieldRow',
			'listOgImgUrl' => 'textFieldRow',
			'listFullDescription' => 'textAreaRow',
		);
		
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'buyLink' => 'Buy journal link',
			);
			return $this->setTextLabels( $labels );
		}
		function rules() {
			return Array(
				Array( 'buyLink', 'length', 'max' => CommonLib::maxByte ),
				Array( 'listTitle, listMetaTitle, listMetaDescription, listMetaKeywords, listOgImgUrl', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'listDescription, listFullDescription', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
		
		function loadAR( $pk = 0 ) {
			$AR = JournalSettingsModel::getModel();
			$this->setAR( $AR );
			return true;
		}
	}
?>