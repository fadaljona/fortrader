<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminInterestRatesValueFormModel extends FormModelBase {
		public $id;
		public $idRate;
		public $modDate;
		public $futDate;
		public $value;
		
		public $link;
		public $postId;
		
		public $excludeFieldsFromAr = array( 'link', 'postId' );
		
		function getARClassName() {
			return "InterestRatesValueModel";
		}
		
		public static function getTextFields(){
			return array(
				'link' => Array( 'modelField' => 'link', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'postId' => Array( 'modelField' => 'postId', 'type' => 'hiddenField', 'class' => '', 'disabled' => false ),
			);
		}

		protected function getSourceAttributeLabels() {
			$labels = Array(
				'modDate' => 'Meeting date',
				'futDate' => 'Future date',
				'value' => 'Value',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){			
				$labels[ "link[{$language->id}]" ] = "Review fortrader link";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'idRate', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
				Array( 'value', 'numerical', 'integerOnly'=>false ),
				Array( 'modDate, futDate', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'link', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'link', 'checkPosts' ),
			);
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function checkPosts( $field, $params ){
			foreach( $this->$field as $idLanguage=>$value ) {
				if( $value ){
					if( !$this->postId[$idLanguage] )
						$this->addError( $field, Yii::t( 'yii','{attribute} NOT '.Yii::app()->request->hostInfo.' post link.', Array( 
							'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( !$post['futDate'] ) $this->futDate = NULL;
				foreach( $this->link as $idLanguage=>$value ) {
					if( $value ){
						if( strpos($value, '.html') === false ){
							$this->postId[$idLanguage] = 0;
							continue;
						} 
						if( strpos($value, Yii::app()->request->hostInfo) === false ){
							$this->postId[$idLanguage] = 0;
							continue;
						} 
						
						$url = str_replace( '.html', '', $value );
						$slashPos = strrpos( $url, '/' );
						$slug = substr( $url, $slashPos+1 );
						if( !$slug ){
							$this->postId[$idLanguage] = 0;
							continue;
						} 
						
						CommonLib::loadWp();
						$posts = get_posts( array( 'name' => $slug, 'post_type' => 'post', 'post_status' => 'publish', 'numberposts' => 1) );
						
						if( !$posts ){
							$this->postId[$idLanguage] = 0;
							continue;
						} 
						
						$this->postId[$idLanguage] = $posts[0]->ID;
					}else{
						$this->postId[$idLanguage] = 0;
					}
				}
			}
		}
		
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
					}
				}
								
				return true;
			}
			return false;
		}

		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					
					$arrToSave = Array();
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
					}
					$i18ns[ $language->id ] = (object)$arrToSave;
				}
				
				$AR->setI18Ns( $i18ns );
				return true;
			}
			return false;
		}
	
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				
				
				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>