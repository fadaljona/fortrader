<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminAdvertisementAddBlockFormModel extends FormModelBase {
		public $ip;
		public $agent;
		function getARClassName() {
			return "AdvertisementBlockModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'ip' => 'IP',
				'agent' => 'Agent',
			);
		}
		function rules() {
			return Array(
				Array( 'ip', 'validateIP' ),
				Array( 'agent', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		function validateIP() {
			if( !$this->ip and !$this->agent ) {
				$this->addError( 'ip', Yii::t( 'yii', '{attribute} cannot be blank.', Array(
					'{attribute}' => $this->getAttributeLabel( "ip" ),
				)));
				return;
			}
			if( $this->ip ) {
				$long = ip2long( $this->ip );
				if( !$long or !preg_match( CommonLib::PATTERN_IP, $this->ip )) {
					$this->addError( 'ip', Yii::t( '*', '{attribute} not valid IP-Address.', Array(
						'{attribute}' => $this->getAttributeLabel( "ip" ),
					)));
					return;
				}
				$long = sprintf( "%u", $long );
				$exists = AdvertisementBlockModel::model()->findByAttributes( Array( 'ip' => $long ));
				if( $exists ) {
					$this->addError( 'ip', Yii::t( 'yii', '{attribute} "{value}" has already been taken.', Array(
						'{attribute}' => $this->getAttributeLabel( "ip" ),
						'{value}' => CHtml::encode( $this->ip ),
					)));
				}
			}
			if( $this->agent ) {
				$exists = AdvertisementBlockModel::model()->findByAttributes( Array( 'agent' => $this->agent ));
				if( $exists ) {
					$this->addError( 'ip', Yii::t( 'yii', '{attribute} "{value}" has already been taken.', Array(
						'{attribute}' => $this->getAttributeLabel( "agent" ),
						'{value}' => CHtml::encode( $this->agent ),
					)));
				}
			}
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array() ) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				$AR->ip = strlen( $this->ip ) ? sprintf( "%u", ip2long( $this->ip )) : null;
				return true;
			}
			return false;
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( !$this->ip ) $this->ip = null;
				if( !$this->agent ) $this->agent = null;
			}
		}
		function load( $id = 0 ) {
			if( $this->loadAR( $id )) {
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>