<?
	Yii::import( 'models.base.FormModelBase' );
	Yii::import( 'extensions.ParserHTML' );

	final class AdminEAStatementFormModel extends FormModelBase {
		public $id;
		public $titles = Array();
		public $desces = Array();
		public $idEA;
		public $idVersion;
		public $nameUser;
		public $symbol;
		public $period;
		public $type;
		public $idBroker;
		public $idServer;
		public $date;
		public $set;
		public $gain;
		public $drow;
		public $trades;
		public $statement;
		public $graph;
		public $optimizationStartDate;
		public $optimizationEndDate;
		public $testStartDate;
		public $testEndDate;
		protected $user;
		function getARClassName() {
			return "EAStatementModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'idEA' => 'EA',
				'idVersion' => 'Version',
				'nameUser' => 'Added',
				'symbol' => 'Symbol',
				'period' => 'Period',
				'type' => 'Type',
				'idBroker' => 'Broker',
				'idServer' => 'Server',
				'date' => 'Date',
				'set' => 'Set file',
				'gain' => 'Gain',
				'drow' => 'Drow',
				'trades' => 'Trades',
				'statement' => 'Statement',
				'graph' => 'Balance Grafic',
				'optimizationStartDate' => 'Start date',
				'optimizationEndDate' => 'End date',
				'testStartDate' => 'Start date',
				'testEndDate' => 'End date',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "titles[{$language->id}]" ] = "Title";
				$labels[ "desces[{$language->id}]" ] = "Description";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'idEA, idVersion, nameUser, idBroker, idServer', 'required' ),
				Array( 'titles', 'validateTitles' ),
				Array( 'titles', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'desces', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'idEA', 'existsIDModel', 'model' => 'EAModel' ),
				Array( 'idVersion', 'existsIDModel', 'model' => 'EAVersionModel' ),
				Array( 'nameUser', 'validateUser' ),
				Array( 'symbol', 'length', 'max' => CommonLib::maxByte ),
				Array( 'period', 'in', 'range' => Array( 'M1', 'M5', 'M15', 'M30', 'H1', 'H4', 'D', 'M', 'W' )),
				Array( 'type', 'in', 'range' => Array( 'Demo', 'Real', 'Backtest' )),
				Array( 'idBroker', 'existsIDModel', 'model' => 'BrokerModel' ),
				Array( 'idServer', 'existsIDModel', 'model' => 'ServerModel' ),
				Array( 'date', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'set', 'file', 'allowEmpty' => true, 'types' => Array( 'set' )),
				Array( 'gain, drow', 'numerical' ),
				Array( 'trades', 'numerical', 'integerOnly' => true ),
				Array( 'statement', 'file', 'allowEmpty' => true, 'types' => Array( 'htm', 'html' )),
				Array( 'graph', 'file', 'allowEmpty' => true, 'types' => Array( 'jpg', 'gif' )),
				Array( 'optimizationStartDate, optimizationEndDate, testStartDate, testEndDate', 'date', 'format' => 'yyyy-mm-dd' ),
			);
		}
		function validateTitles() {
			foreach( $this->titles as $idLanguage=>$title ) {
				if( strlen( $title )) return;
			}
			$NSi18n = $this->getNSi18n();
			$this->addError( "titles", Yii::t( 'yii','{attribute} cannot be blank.', Array( '{attribute}' => Yii::t( $NSi18n, 'Title' ))));
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function validateUser() {
			if( !$this->hasErrors()) {
				$this->user = UserModel::model()->findByAttributes( Array( 'user_login' => $this->nameUser ));
				if( !$this->user ) {
					$this->addI18NError( 'nameUser', "Can't find User!" );
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( !$this->gain ) $this->gain = null;
				if( !$this->drow ) $this->drow = null;
				if( !$this->trades ) $this->trades = null;
				if( !$this->optimizationStartDate ) $this->optimizationStartDate = null;
				if( !$this->optimizationEndDate ) $this->optimizationEndDate = null;
				if( !$this->testStartDate ) $this->testStartDate = null;
				if( !$this->testEndDate ) $this->testEndDate = null;
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'statement', 'graph' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->titles[ $i18n->idLanguage ] = $i18n->title;
					$this->desces[ $i18n->idLanguage ] = $i18n->desc;
				}
				
				if( $AR->idUser ) {
					$user = UserModel::model()->findByPk( $AR->idUser );
					$this->nameUser = $user->user_login;
				}
				
				$this->set = $AR->srcFileSet;
				$this->statement = $AR->srcFileStatement;
				$this->graph = $AR->srcFileGraph;
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'statement', 'graph' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->idUser = $this->user->ID;
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'title' => $this->titles[ $language->id ],
						'desc' => $this->desces[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );				
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles( Array( 'statement', 'graph', 'set' ));
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			if( $this->set ) {
				$AR->uploadFileSet( $this->set );
			}
			if( $this->statement ) {
				$AR->uploadFileStatement( $this->statement );
			}
			if( $this->graph ) {
				$AR->uploadFileGraph( $this->graph );
			}
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
		function getStatementInfo( $output ) {
			$path = EAStatementModel::holdFileStatement( $this->statement );
			
			//$this->statement->getName();
						
			$statement = file_get_contents( $path );
			AdminEAStatementFormModelParseStatementAction::parse( $statement, $output );
			
			if( preg_match( "#Strategy Tester Report$#i", $output->nameStatement )) {
				$output->type = "Backtest";
			}
			
				# EA, EAVersion
			$p = strrpos( $output->nameFileEA, '_' );
			$nameEA = substr( $output->nameFileEA, 0, $p );
			$nameEAVersion = substr( $output->nameFileEA, $p + 1 );
			$nameEAVersion = preg_replace( "#v#i", "", $nameEAVersion );
			
			$EA = EAModel::model()->findByAttributes( Array(
				'name' => $nameEA,
			));
			if( $EA ) {
				$output->idEA = $EA->id;
				
				$EAVersion = EAVersionModel::model()->findByAttributes( Array(
					'idEA' => $EA->id,
					'version' => $nameEAVersion,
				));
				if( $EAVersion ) {
					$output->idVersion = $EAVersion->id;
				}
			}
			
				# server, broker
			$output->nameServer = preg_replace( "#\(.*\)#i", "", $output->nameServer );
			$output->nameServer = trim( $output->nameServer );
			$server = ServerModel::model()->findByAttributes( Array(
				'name' => $output->nameServer,
			));
			if( $server ) {
				$output->idServer = $server->id;
				$output->idBroker = $server->idBroker;
			}
			
				# symbol
			$output->symbol = preg_replace( "#\(.*\)#i", "", $output->symbol );
			$output->symbol = trim( $output->symbol );
			
				# period
			preg_match_all( "#\((.+)\)#U", $output->period, $matchs );
			if( count( $matchs[0]) > 0 ) {
				$output->period = $matchs[1][0];
				@list( $start, $end ) = explode( "-", $matchs[1][1] );
				$output->testStartDate = trim( str_replace( '.', '-', $start ));
				$output->testEndDate = trim( str_replace( '.', '-', $end ));
			}
			
				# gain, drow
			$output->gain = $output->deposit ? round( $output->profit / $output->deposit * 100, 2 ) : null;
			$output->drow = -(double)$output->drow;
			$output->drow = $output->deposit ? round( $output->drow / $output->deposit * 100, 2 ) : null;
						
			unlink( $path );
		}
	}

	
	abstract class AdminEAStatementFormModelParseStatementAction {
		static function parse( $statement, $output ) {
			$XML = ParserHTML::createXML( "windows-1251", $statement );
			$XPath = new DOMXPath( $XML );
			
			$output->nameStatement = $XPath->evaluate( 'string(//div[@style="font: 20pt Times New Roman"]/b/.)' );
			$output->nameFileEA = $XPath->evaluate( 'string(//div[@style="font: 16pt Times New Roman"]/b/.)' );
			$output->nameServer = $XPath->evaluate( 'string(//div[@style="font: 10pt Times New Roman"]/b/.)' );
			$output->symbol = $XPath->evaluate( 'string(//table[1]/tr[1]/td[2]/.)' );
			$output->period = $XPath->evaluate( 'string(//table[1]/tr[2]/td[2]/.)' );
			$output->deposit = $XPath->evaluate( 'number(//table[1]/tr[5]/td/tr[4]/td[2]/.)' );
			$output->profit = $XPath->evaluate( 'number(//table[1]/tr[5]/td/tr[5]/td[2]/.)' );
			$output->drow = $XPath->evaluate( 'string(//table[1]/tr[5]/td/tr[7]/td[4]/.)' );
			$output->trades = $XPath->evaluate( 'number(//table[1]/tr[5]/td/tr[9]/td[2]/.)' );
		}
	}
?>