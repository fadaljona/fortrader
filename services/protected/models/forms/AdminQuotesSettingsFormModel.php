<?php
Yii::import('models.base.SettingsFormModelBase');

final class AdminQuotesSettingsFormModel extends SettingsFormModelBase
{
    
    public $files;
    public $columns;
    public $timer;
    public $timer_default;
    public $time_update_panel;
    public $periodGetQuotesControl;
    public $realQuotesDataTimeDifference;
    public $autoStartDataTransfer;
    public $phpServersPath;
    public $statsDataTimeZone;
    public $statsUrlData;
    public $symbolGetQuotesControl;
    public $wsServerUrl;
    public $quotesOgImage;
    public $chartsOgImage;
    public $quotesTickSource;
    public $prefixToRemove;
    public $symbolsToStrip;
    public $otherQuotesOgImage;
    public $notificationEmails;
    public $histAliasReplacement;

    public $autoStartWsPusher;
    public $autoStartBitfinex;
    public $autoStartBitz;
    public $autoStartOkex;
    public $autoStartBinance;
    
    public $columns_name = array();
    public $columnsShort = array();
    public $columnsShort_name = array();
    public $quotesListTitle = array();
    public $quotesListMetaTitle = array();
    public $quotesListDesc = array();
    public $quotesListMetaDesc = array();
    public $chartsListTitle = array();
    public $chartsListMetaTitle = array();
    public $chartsListDesc = array();
    public $chartsListMetaDesc = array();
    public $chartsListMetaKeys = array();
    public $quotesListMetaKeys = array();
    public $quotesInformerTitle = array();
    public $quotesInformerMetaTitle = array();
    public $quotesInformerMetaDesc = array();
    public $quotesInformerMetaKeys = array();
    public $otherQuotesListTitle = array();
    public $otherQuotesListMetaTitle = array();
    public $otherQuotesListDesc = array();
    public $otherQuotesListMetaDesc = array();
    public $otherQuotesListMetaKeys = array();
    public $quotesListShortDesc = array();
    public $chartsListShortDesc = array();
    
    public $titleTemplate = array();
    public $metaTitleTemplate = array();
    public $chartTitleTemplate = array();
    public $chartLabelTemplate = array();
    public $wpPostsTitleTemplate = array();
    public $previewTemplate = array();
    public $metaDescriptionTemplate = array();
    public $metaKeywordsTemplate = array();
    
    public $textFields = array(
        'columns_name' => 'textFieldRow',
        'columnsShort' => 'textFieldRow',
        'columnsShort_name' => 'textFieldRow',
        'quotesListTitle' => 'textFieldRow',
        'quotesListMetaTitle' => 'textFieldRow',
        'quotesListDesc' => 'textAreaRow',
        'quotesListMetaDesc' => 'textFieldRow',
        'chartsListTitle' => 'textFieldRow',
        'chartsListMetaTitle' => 'textFieldRow',
        'chartsListDesc' => 'textAreaRow',
        'chartsListMetaDesc' => 'textFieldRow',
        'chartsListMetaKeys' => 'textFieldRow',
        'quotesListMetaKeys' => 'textFieldRow',
        'quotesInformerTitle' => 'textFieldRow',
        'quotesInformerMetaTitle' => 'textFieldRow',
        'quotesInformerMetaDesc' => 'textFieldRow',
        'quotesInformerMetaKeys' => 'textFieldRow',
        'otherQuotesListTitle' => 'textFieldRow',
        'otherQuotesListMetaTitle' => 'textFieldRow',
        'otherQuotesListDesc' => 'textAreaRow',
        'otherQuotesListMetaDesc' => 'textFieldRow',
        'otherQuotesListMetaKeys' => 'textFieldRow',
        'quotesListShortDesc' => 'textAreaRow',
        'chartsListShortDesc' => 'textAreaRow',
        'titleTemplate' => 'textFieldRow',
        'metaTitleTemplate' => 'textFieldRow',
        'chartTitleTemplate' => 'textFieldRow',
        'chartLabelTemplate' => 'textFieldRow',
        'wpPostsTitleTemplate' => 'textFieldRow',
        'previewTemplate' => 'textAreaRow',
        'metaDescriptionTemplate' => 'textAreaRow',
        'metaKeywordsTemplate' => 'textAreaRow',
    );

    protected function getSourceAttributeLabels() {
        $labels = array(
            'files' => 'files___ Перечень файлов для парсинга котировок разделитель - две вертикальные черты ||',
            'columns' => 'columns___ Набор столбцов, которые будут выводиться в таблице, можно создавать несколько наборов для разных виджетов, в настройках виджета следует указать опцию \'columns\'=>\'имя набора\'(по умолчанию будут выводиться все доступные колонки). name-имя инструмента bid-цена покупки ask-цена продажи tim-время обновления time_up-время обновления spred-спред arrow-направление изменения цены start_time-начало торгов end_time-конец торгов',
            'timer' => 'timer___ Список интервалов для обновления таблицы',
            'timer_default' => 'timer_default___ Время обновления таблицы на сайте по умолчанию',
            'time_update_panel' => 'time_update_panel___ Значение по умолчанию для обновления панели котировок',
            'periodGetQuotesControl' => 'periodGetQuotesControl___ период контроля получения котировок в минутах',
            'realQuotesDataTimeDifference' => 'realQuotesDataTimeDifference___ разница времени сервера и потока котировок',
            'autoStartDataTransfer' => 'Auto start receiving data from mt4',
        );
        return $this->setTextLabels($labels);
    }
    public function rules()
    {
        return array(
            array( 'files, columns, timer, phpServersPath, statsDataTimeZone, statsUrlData, symbolGetQuotesControl, wsServerUrl, quotesOgImage, chartsOgImage, quotesTickSource, prefixToRemove, symbolsToStrip, otherQuotesOgImage, notificationEmails, histAliasReplacement', 'length', 'max' => CommonLib::maxByte ),
            array( 'timer_default, time_update_panel, periodGetQuotesControl, realQuotesDataTimeDifference', 'numerical', 'integerOnly' => true, 'min' => 1 ),
            array( 'autoStartDataTransfer, autoStartWsPusher, autoStartBitfinex, autoStartBitz, autoStartOkex, autoStartBinance', 'boolean'),
            

            array( 'columns_name, columnsShort, columnsShort_name, quotesListTitle, quotesListMetaTitle, quotesListDesc, quotesListMetaDesc, chartsListTitle, chartsListMetaTitle, chartsListDesc, chartsListMetaDesc, chartsListMetaKeys, quotesListMetaKeys, quotesInformerTitle, quotesInformerMetaTitle, quotesInformerMetaDesc, quotesInformerMetaKeys, otherQuotesListTitle, otherQuotesListMetaTitle, otherQuotesListDesc, otherQuotesListMetaDesc, otherQuotesListMetaKeys, quotesListShortDesc, chartsListShortDesc, titleTemplate, metaTitleTemplate, chartTitleTemplate, chartLabelTemplate, wpPostsTitleTemplate, previewTemplate, metaDescriptionTemplate, metaKeywordsTemplate', 'checkLens', 'max' => CommonLib::maxWord ),
            
            
        );
    }
    public function loadAR($pk = 0)
    {
        $AR = QuotesSettingsModel::getModel();
        $this->setAR($AR);
        return true;
    }
}
