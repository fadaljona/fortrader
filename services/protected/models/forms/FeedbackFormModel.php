<?
	Yii::import( 'models.base.FormModelBase' );

	final class FeedbackFormModel extends FormModelBase {
		public $user;
		public $email;
		public $idType;
		public $message;
		function getARClassName() {
			return "FeedbackModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'user' => 'Name author',
				'email' => 'E-mail',
				'idType' => 'Type messsage',
				'message' => 'Text',
			);
		}
		function rules() {
			return Array(
				Array( 'user, email, idType, message', 'required' ),
				Array( 'email', 'email' ),
				Array( 'user, email', 'length', 'max' => CommonLib::maxByte ),
				Array( 'message', 'length', 'max' => CommonLib::maxWord ),
				Array( 'idType', 'existsIDModel', 'model' => 'FeedbackTypeModel' ),
			);
		}
		function load( $id = 0 ) {
			if( $this->loadAR()) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>