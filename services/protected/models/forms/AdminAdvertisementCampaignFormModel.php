<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminAdvertisementCampaignFormModel extends FormModelBase {
		public $id;
		public $name;
		public $type;
		public $begin;
		public $end;
		public $clickCost;
		public $marketClickCost;
		public $status;
		public $idUser;
		public $dayLimitShows;
		public $totalLimitShows;
		public $dayLimitClicks;
		public $weight;
		public $minPopup;
		public $hideOnMobiles;
		public $oneAd;
		public $clearStats;
		
		public $startTime;
		public $endTime;
		
		protected $mode;
		function __construct() {
			parent::__construct();
			$this->mode = Yii::App()->user->checkAccess( 'advertisementControl' ) ? 'Admin' : 'User';
		}
		function getARClassName() {
			return "AdvertisementCampaignModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'name' => 'Title',
				'type' => 'Type',
				'begin' => 'Begin',
				'end' => 'End',
				'clickCost' => 'Cost of click',
				'marketClickCost' => 'Market cost of click',
				'status' => 'Status',
				'idUser' => 'Advertiser',
				'dayLimitShows' => 'Day limit shows',
				'totalLimitShows' => 'Total limit shows',
				'dayLimitClicks' => 'Day limit clicks',
				'weight' => 'Campaign weight',
				'minPopup' => 'Show Popup in every (45 min) for 1 user',
				'hideOnMobiles' => 'Hide Popup on mobile devices',
				'oneAd' => 'One ad in one zone',
				'clearStats' => 'Clear stats',
				
				'startTime' => 'Start time ( format hh:mm:ss )',
				'endTime' => 'End time ( format hh:mm:ss )',
			);
		}
		function rules() {
			$rules = Array(
				Array( 'name, type, begin, end, weight', 'required' ),
				Array( 'name', 'length', 'max' => CommonLib::maxByte ),
				Array( 'begin, end', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'name', 'uniqueName' ),
				Array( 'startTime, endTime', 'validateStartEndTime' ),
			);
			if( $this->mode == 'Admin' ) {
				$rules[] = Array( 'clickCost', 'numerical', 'min' => 0, 'allowEmpty' => false );
				$rules[] = Array( 'marketClickCost, oneAd', 'in', 'range' => Array( '0', '1' ), 'allowEmpty' => false );
				$rules[] = Array( 'status', 'in', 'range' => Array( 'Active', 'Pause', 'Block' ));
			}
			$rules[] = Array( 'dayLimitShows, dayLimitClicks, minPopup, hideOnMobiles, totalLimitShows, weight', 'numerical', 'min' => 0 );
			return $rules;
		}
		function validateStartEndTime( $field, $params ){
			$NSi18n = $this->getNSi18n();
			if( !$this->$field ){
				$this->addError( $field, Yii::t( $NSi18n,"{attribute} can't be empty.", Array( '{attribute}' => $this->getAttributeLabel( $field ) )));
			}
			if( ! preg_match('/[0-9]{2}\:[0-9]{2}\:[0-9]{2}/', $this->$field, $matches, PREG_OFFSET_CAPTURE) ){
				$this->addError( $field, Yii::t( $NSi18n,"{attribute} wrong format.", Array( '{attribute}' => $this->getAttributeLabel( $field ) )));
			}
		}
		function uniqueName() {
			if( !$this->hasErrors()) {
				$AR = $this->getAR();
				if( !$AR ) return;
				
				$c = new CDbCriteria();
				$c->params = Array();
				
				$c->addCondition( "`t`.`idUser` = :idUser" );
				$c->params[ ':idUser' ] = $AR->idUser;
				
				$c->addCondition( "`t`.`name` = :name" );
				$c->params[ ':name' ] = $this->name;
								
				if( !$AR->isNewRecord ) {
					$c->addCondition( "`t`.`id` != :pk" );
					$c->params[ ':pk' ] = $AR->id;
				}
				
				$exists = AdvertisementCampaignModel::model()->exists( $c );
				if( $exists ) {
					$this->addI18NError( 'name', "Campaign {name} for this user is already occupied. Please, select another.", Array( '{name}' => $this->name ));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( !$this->dayLimitShows ) $this->dayLimitShows = null;
				if( !$this->dayLimitClicks ) $this->dayLimitClicks = null;
				if( !$this->totalLimitShows ) $this->totalLimitShows = null;
				if( !$this->weight ) $this->weight = 1;
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$exc = $this->mode == 'Admin' ? Array() : Array( 'clickCost', 'marketClickCost', 'status' );
				$this->loadFromAR( null, Array(), $exc );
				$AR = $this->getAR();
				$this->type = explode( ',', $AR->type );
				
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		
		function creatAdForRatingBrokers( $type, $id ){
		
			if( in_array('Rating Brokers', $type ) ){
				$rating_ads = AdvertisementModel::model()->findByAttributes( array( 'idCampaign' => $id ) );

				if( ! isset( $rating_ads ) ){
					
					$rating_ads = new AdvertisementModel();
					$rating_ads->type = 'Rating Brokers';
					$rating_ads->idCampaign = $id;
					$rating_ads->header = 'Rating Brokers';
					$rating_ads->url = 'Rating Brokers';
					$rating_ads->dontShow = 0;
					$rating_ads->showEverywhere = 0;
					
					$rating_ads->save();
					
				}
			}
		}
		
		protected function beforeDelete(){

			if( in_array('Rating Brokers', $this->type ) ){
				$rating_ads = AdvertisementModel::model()->findByAttributes( array( 'idCampaign' => $this->id ) );

				$rating_ads->delete();
			}

			parent::beforeDelete();

		}
		
		
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			if( $this->mode != 'Admin' ) {
				$this->idUser = Yii::App()->user->id;
			}
			
			$exc = $this->mode == 'Admin' ? Array( 'type' ) : Array( 'clickCost', 'marketClickCost', 'status', 'type' );
			$this->saveToAR( null, Array(), $exc );
			$AR->type = implode( ',', $this->type );
			
			if( $this->saveAR( false )) {
				if( $this->mode == 'Admin' and $this->clearStats ){
					$AR->clearStats();
				}
				
				$this->creatAdForRatingBrokers( $this->type, $AR->getPrimaryKey() );
			
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>