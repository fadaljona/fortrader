<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUserGroupFormModel extends FormModelBase {
		public $id;
		public $name;
		public $iconClass = 'icon-adjust';
		public $additionalInformation;
		public $rights = Array();
		function getARClassName() {
			return "UserGroupModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'name' => 'Name',
				'iconClass' => 'Group icon',
				'additionalInformation' => 'Additional information',
			);
		}
		function rules() {
			return Array(
				Array( 'name', 'required' ),
				Array( 'name', 'length', 'max' => CommonLib::maxByte ),
				Array( 'additionalInformation', 'length', 'max' => CommonLib::maxWord ),
				Array( 'name', 'uniqueField', 'message' => 'Name is busy. Please choose another.' ),
			);
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( empty( $post[ 'rights' ])) $this->rights = Array();
				$this->rights  = array_filter( (array)$this->rights, Array( 'CommonLib', 'isID' ));
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR( null, Array(), Array( 'rights' ));
				$this->rights = $AR->getIDsRights();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array( 'rights' ));
						
			if( $this->saveAR( false )) {
				$AR->setRights( $this->rights );
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>