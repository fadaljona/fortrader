<?
	Yii::import( 'models.base.FormModelBase' );

	final class UserPrivateMessageFormModel extends FormModelBase {
		public $id;
		public $idTo;
		public $text;
		function getARClassName() {
			return "UserPrivateMessageModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'text' => 'Text',
			);
		}
		function rules() {
			return Array(
				Array( 'idTo, text', 'required' ),
				Array( 'idTo', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'text', 'length', 'max' => CommonLib::maxWord ),
			);
		}
		function loadAR( $pk = 0 ) {
			if( parent::loadAR( $pk )) {
				$AR = $this->getAR();
				if( $AR->isNewRecord ) {
					$AR->idFrom = Yii::App()->user->id;
				}
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>