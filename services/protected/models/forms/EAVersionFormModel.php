<?
	Yii::import( 'models.base.FormModelBase' );

	final class EAVersionFormModel extends FormModelBase {
		public $id;
		public $idEA;
		public $version;
		public $desces = Array();
		public $file;
		function getARClassName() {
			return "EAVersionModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'version' => 'Number version',
				'desc' => 'Description',
				'file' => 'File',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "desces[{$language->id}]" ] = "Description";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'idEA, version', 'required' ),
				Array( 'version', 'length', 'max' => CommonLib::maxByte ),
				Array( 'idEA', 'existsIDModel', 'model' => 'EAModel' ),
				Array( 'desces', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'file', 'file', 'allowEmpty' => true, 'types' => Array( 'ex4', 'mq4' )),
			);
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$this->version = strip_tags( $this->version );
				return true;
			}
			return false;
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'file' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->desces[ $i18n->idLanguage ] = $i18n->desc;
				}
				
				$this->file = $AR->srcFile;
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'file' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( !$AR->idUser ) {
					$AR->idUser = Yii::App()->user->id;
				}
				if( !$AR->release ) {
					$AR->release = date( 'Y-m-d' );
				}
				if( !$AR->status ) {
					$AR->status = 'Finish';
				}
								
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$desc = strip_tags($this->desces[ $language->id ], "<br>");
					$desc = preg_replace( "#(\r?\n)+#", "<br>", $desc );
					$i18ns[ $language->id ] = (object)Array(
						'desc' => $desc,
					);
				}
				$AR->setI18Ns( $i18ns );				
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost(); 
				$this->loadFromFiles( Array( 'file' ));
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			if( $this->file ) {
				$AR->uploadFile( $this->file );
			}
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>