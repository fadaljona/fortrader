<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUserMessageFormModel extends FormModelBase {
		public $id;
		public $from;
		public $text;
		public $instance;
		public $idLinkedObj;
		protected $userFrom;
		function getARClassName() {
			return "UserMessageModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'from' => 'From',
				'text' => 'Text',
				'instance' => 'Instance module',
				'idLinkedObj' => 'Id linked object',
			);
		}
		function rules() {
			return Array(
				Array( 'from, text, instance', 'required' ),
				Array( 'text', 'length', 'max' => CommonLib::maxWord ),
				Array( 'instance', 'in', 'range' => Array( 
					'ea/list', 'ea/single', 'eaVersion/single', 'eaStatement/single', 
					'eaTradeAccount/single', 'question/list','ea/laboratory','calendarEvent/list','calendarEvent/single'
				)),
				Array( 'idLinkedObj', 'numerical', 'min' => 1 ),
				Array( 'from', 'validateFrom' ),
				Array( 'idLinkedObj', 'validateIDLinkedObj' ),
			);
		}
		function validateIDLinkedObj() {
			if( !$this->hasErrors()) {
				
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( empty( $this->idLinkedObj )) $this->idLinkedObj = null;
			}
		}
		function validateFrom() {
			if( !$this->hasErrors()) {
				$this->userFrom = UserModel::model()->findByAttributes( Array( 'user_login' => $this->from ));
				if( !$this->userFrom ) {
					$this->addI18NError( 'from', "Can't find User!" );
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'from' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( $AR->idUser ) {
					$userFrom = UserModel::model()->findByPk( $AR->idUser );
					$this->from = $userFrom->user_login;
				}
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'from' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->idUser = $this->userFrom->ID;
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>