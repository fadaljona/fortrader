<?php

Yii::import('models.base.FormModelBase');

final class AdminCryptoCurrenciesExchangesSymbolToOurSymbolFormModel extends FormModelBase
{
    public $id;
    public $idExchange;
    public $symbol;

    public $symbolId;

    public function getARClassName()
    {
        return "CryptoCurrenciesExchangesSymbolToOurSymbolModel";
    }
    protected function getSourceAttributeLabels()
    {
        $labels = array(
            'symbolId' => 'Our symbol'
        );
        return $labels;
    }
    public function rules()
    {
        $rules = array(
            array( 'symbol, idExchange, id', 'required' ),
        );
        return $rules;
    }

    public function load($id = 0)
    {
        $post = $this->getPostLink();
        if ((int)@$post['id']) {
            $id = (int)@$post['id'];
        }
        if ($this->loadAR($id)) {
            $this->loadFromAR();
            $this->loadFromPost();
            return true;
        }
        return false;
    }

    public function save()
    {
        $AR = $this->getAR();
        if (!$AR) {
            return false;
        }

        $this->saveToAR(null, array(), array());

        if ($this->saveAR(false)) {
            if ($id=$AR->getPrimaryKey()) {
                return $id;
            }
        }
        return false;
    }
}