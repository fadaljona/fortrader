<?
	Yii::import( 'models.base.FormModelBase' );

	final class UserProfileFormModel extends FormModelBase {
		public $ID;
		public $user_login;
		public $display_name;
		public $user_email;
		public $language;
		public $city;
		public $user_url;
		public $icq;
		public $skype;
		public $description;
		public $firstName;
		public $lastName;
		public $phone;
		public $idCountry;
		public $sex;
		public $utc;
		
		public $profile_trader_market;
		public $profile_trader_tradingPlatforms;
		public $profile_trader_brokers;
		public $profile_trader_financeInstruments;
		public $profile_trader_tradingExperience;
		public $profile_trader_analyseMethod;
		public $profile_trader_tradingType;
		public $profile_trader_useEA;
		public $profile_trader_commercialProduct;
		public $profile_trader_aboutTrading;
		
		public $profile_manager_availability;
		public $profile_manager_trustManagementType;
		public $profile_manager_agreement;
		public $profile_manager_managementExperience;
		public $profile_manager_investmentRisk;
		public $profile_manager_minimumInvestmentAmount;
		public $profile_manager_additionally;
		
		public $profile_investor_availability;
		public $profile_investor_investmentAmount;
		public $profile_investor_investmentPeriod;
		public $profile_investor_expectedAnnualRateOfReturn;
		public $profile_investor_maximumRiskOfInvestment;
		public $profile_investor_agreement;
		public $profile_investor_additionally;
		
		public $profile_programmer_availability;
		public $profile_programmer_programmingExperience;
		public $profile_programmer_programmingLangauge;
		public $profile_programmer_typeOfPrograms;
		public $profile_programmer_hourlyRate;
		public $profile_programmer_minimalOrder;
		public $profile_programmer_mql4Profile;
		public $profile_programmer_mql5Profile;
		public $profile_programmer_additionally;
				
		function getARClassName() {
			return "UserModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'user_login' => 'Login',
				'display_name' => 'Nicename',
				'user_email' => 'E-mail',
				'language' => 'Language',
				'city' => 'City',
				'user_url' => 'Website',
				'icq' => 'ICQ',
				'skype' => 'Skype',
				'description' => 'About',
				'firstName' => 'First name',
				'lastName' => 'Last name',
				'phone' => 'Phone',
				'idCountry' => 'Country',
				'sex' => 'Sex',
				'utc' => 'Часовой пояс',
				
				'profile_trader_market' => 'Market',
				'profile_trader_tradingPlatforms' => 'Trading platforms',
				'profile_trader_brokers' => 'Brokers',
				'profile_trader_financeInstruments' => 'Finance instruments',
				'profile_trader_tradingExperience' => 'Trading experience',
				'profile_trader_analyseMethod' => 'Analyse method',
				'profile_trader_tradingType' => 'Trading type',
				'profile_trader_useEA' => 'Use EA',
				'profile_trader_commercialProduct' => 'Commercial product',
				'profile_trader_aboutTrading' => 'About your trading',
				
				'profile_manager_availability' => 'Availability',
				'profile_manager_trustManagementType' => 'Trust management type',
				'profile_manager_agreement' => 'Agreement',
				'profile_manager_managementExperience' => 'Management experience',
				'profile_manager_investmentRisk' => 'Investment risk',
				'profile_manager_minimumInvestmentAmount' => 'Minimum investment amount',
				'profile_manager_additionally' => 'Additionally',
				
				'profile_investor_availability' => 'Availability',
				'profile_investor_investmentAmount' => 'Investment amount',
				'profile_investor_investmentPeriod' => 'Investment period',
				'profile_investor_expectedAnnualRateOfReturn' => 'Expected annual rate of return',
				'profile_investor_maximumRiskOfInvestment' => 'Maximum risk of investment',
				'profile_investor_agreement' => 'Agreement',
				'profile_investor_additionally' => 'Additionally',
				
				'profile_programmer_availability' => 'Availability',
				'profile_programmer_programmingExperience' => 'Programming experience',
				'profile_programmer_programmingLangauge' => 'Programming langauge',
				'profile_programmer_typeOfPrograms' => 'Type of programs',
				'profile_programmer_hourlyRate' => 'Hourly rate',
				'profile_programmer_minimalOrder' => 'Minimal order',
				'profile_programmer_mql4Profile' => 'mql4 profile',
				'profile_programmer_mql5Profile' => 'mql5 profile',
				'profile_programmer_additionally' => 'Additionally',
			);
		}
		function rules() {
			return Array(
				Array( 'user_login, user_email', 'required' ),
				Array( 'user_login', 'length', 'max' => UserModel::MAXLoginLen ),
				Array( 'display_name', 'length', 'max' => UserModel::MAXDisplayNameLen ),
				Array( 'user_email', 'email' ),
				Array( 'user_email', 'length', 'max' => UserModel::MAXEMailLen ),
				Array( 'user_url', 'length', 'max' => UserModel::MAXURLLen ),
				Array( 'city, icq, skype, firstName, lastName', 'length', 'max' => CommonLib::maxByte ),
				Array( 'description', 'length', 'max' => CommonLib::maxWord ),
				Array( 'user_login', 'uniqueField', 'message' => 'Login is busy. Please choose another.' ),
				Array( 'display_name', 'uniqueField', 'message' => 'Nickname is busy. Please choose another.' ),
				Array( 'user_email', 'uniqueField', 'message' => 'E-mail is busy. Please choose another.' ),
				Array( 'icq', 'numerical', 'min' => 0 ),
				Array( 'idCountry', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'utc', 'numerical', 'integerOnly' => true),
				Array( 'sex', 'in', 'range' => Array( 'Male', 'Female' )),
				Array( 'user_login', 'validateLogin' ),
				Array( 'display_name', 'validateDisplayName' ),
			);
		}
		function validateLogin() {
			if( !validate_username( $this->user_login )) {
				$this->addI18NError( 'user_login', '{attribute} uses illegal characters!', Array( '{attribute}' => $this->getAttributeLabel( 'user_login' )));
			}
		}
		function validateDisplayName() {
			$origin_display_name = $this->display_name;
			$filtered_display_name = apply_filters( 'pre_user_display_name', $origin_display_name );
			if( $origin_display_name != $filtered_display_name ){
				$this->addI18NError( 'display_name', '{attribute} uses illegal characters!', Array( '{attribute}' => $this->getAttributeLabel( 'display_name' )));
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( !strlen( $this->idCountry )) $this->idCountry = null;
				if( !strlen( $this->sex )) $this->sex = null;
			}
		}
		function loadAR( $pk = 0 ) {
			if( parent::loadAR( $pk )) {
				$AR = $this->getAR();
				$AR->getProfileToForm( $this );
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( !$id ) $id = (int)@$post['ID'];
			if( !$id ) return false;
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$AR->setProfileFromForm( $this );
				$AR->setDescription( $this->description );
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>
