<?
	Yii::import( 'models.base.FormModelBase' );

	final class CalendarSingleSubscriptionFormModel extends FormModelBase {
		public $id;
		public $idUser;
		public $idEvent;
		public $timeBeforeEvent;
		public $idLanguage;
		
		function getARClassName() {
			return "CalendarSingleSubscriptionModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'idEvent' => 'Select event',
				'timeBeforeEvent' => 'Time to start event'
			);
		}
		function rules() {
			return Array(
				Array( 'idEvent, timeBeforeEvent', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'idEvent, timeBeforeEvent', 'required' ),
				Array( 'idEvent', 'checkUniqueness' ),
			);
		}
		
		function checkUniqueness($attribute,$params){
			$ARClassName = $this->getARClassName();
            $model = $ARClassName::model()->findByIdUserIdEvent($this->idEvent, $this->idUser);
            if($model != null )
                $this->addError('idEvent', Yii::t( '*','You already sudscribed to this event') );
         
		}
		function loadByIdUserIdEvent( $idUser, $idEvent ) {
			if( !$idUser || !$idEvent ) return false;
			if( $this->loadARByIdUserIdEvent( $idUser, $idEvent )) {
				$this->loadFromAR();
				return true;
			}
			return false;
		}
		function loadARByIdUserIdEvent( $idUser, $idEvent ) {
			$ARClassName = $this->getARClassName();
			$AR = CActiveRecord::model( $ARClassName )->findByIdUserIdEvent( $idUser, $idEvent );
			if( !$AR ) $AR = new $ARClassName();
			if( !$AR ) return false;
			$this->setAR( $AR );
			return true;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>