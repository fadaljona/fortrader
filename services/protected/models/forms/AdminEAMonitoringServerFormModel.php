<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminEAMonitoringServerFormModel extends FormModelBase {
		public $id;
		public $name;
		function getARClassName() {
			return "EAMonitoringServerModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'name' => 'Title',
			);
		}
		function rules() {
			return Array(
				Array( 'name', 'required' ),
				Array( 'name', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>