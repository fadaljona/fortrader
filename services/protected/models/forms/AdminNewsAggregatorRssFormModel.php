<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminNewsAggregatorRssFormModel extends FormModelBase {
		public $id;
		public $idLanguage;
		public $enabled;
		public $link;
		public $idSource;
		public $title;
		public $lastBuildDate;
		public $regExp;
		public $idCat;
		public $forceCheck;

		function getARClassName() {
			return "NewsAggregatorRssModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'idLanguage' => 'Language',
				'enabled' => 'Enabled?',
				'link' => 'Rss link',
				'idSource' => 'Source',
				'title' => 'Title(for admin)',
				'lastBuildDate' => 'Last Build Date',
				'regExp' => 'Regular expression to remove from news description',
				'idCat' => 'Category',
				'forceCheck' => 'Force check new news in feed',
			);
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'idLanguage, idSource, idCat', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
				Array( 'enabled, forceCheck', 'boolean' ),
				Array( 'link, title, regExp', 'length', 'max' => CommonLib::maxByte ),
				Array( 'link', 'required' ),
			);
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( $this->forceCheck ){
					$this->lastBuildDate = 0;
				}
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				
				
				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array('saveToAR'));
			
			if( $this->saveAR( false )) {
				NewsAggregatorRssModel::generateTxtList();
				return $AR->getPrimaryKey();
			}
			NewsAggregatorRssModel::generateTxtList();
			return false;
		}
	}

?>