<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUsersExportFormModel extends FormModelBase {
		public $export = 'users_and_groups';
		protected function getSourceAttributeLabels() {
			return Array(
				'export' => 'Export',
			);
		}
		function rules() {
			return Array(
				Array( 'export', 'required' ),
			);
		}
		function load( $id = 0 ) {
			$this->loadFromPost();
			return true;
		}
	}

?>