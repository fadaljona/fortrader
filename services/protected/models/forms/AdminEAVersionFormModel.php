<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminEAVersionFormModel extends FormModelBase {
		public $id;
		public $idEA;
		public $version;
		public $nameUser;
		public $status;
		public $release;
		public $desces = Array();
		public $file;
		protected $user;
		function getARClassName() {
			return "EAVersionModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'idEA' => 'EA',
				'version' => 'Version',
				'nameUser' => 'Added',
				'status' => 'Status',
				'release' => 'Release',
				'desc' => 'Description',
				'file' => 'File',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "desces[{$language->id}]" ] = "Description";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'idEA, version, nameUser, status', 'required' ),
				Array( 'version', 'length', 'max' => CommonLib::maxByte ),
				Array( 'idEA', 'existsIDModel', 'model' => 'EAModel' ),
				Array( 'nameUser', 'validateUser' ),
				Array( 'release', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'desces', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'file', 'file', 'allowEmpty' => true, 'types' => Array( 'ex4', 'mq4' )),
			);
		}
		function validateUser() {
			if( !$this->hasErrors()) {
				$this->user = UserModel::model()->findByAttributes( Array( 'user_login' => $this->nameUser ));
				if( !$this->user ) {
					$this->addI18NError( 'nameUser', "Can't find User!" );
				}
			}
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'file' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( $AR->idUser ) {
					$user = UserModel::model()->findByPk( $AR->idUser );
					$this->nameUser = $user->user_login;
				}
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->desces[ $i18n->idLanguage ] = $i18n->desc;
				}
				
				$this->file = $AR->srcFile;
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'file' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->idUser = $this->user->ID;
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'desc' => $this->desces[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );				
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost(); 
				$this->loadFromFiles( Array( 'file' ));
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			if( $this->file ) {
				$AR->uploadFile( $this->file );
			}
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>