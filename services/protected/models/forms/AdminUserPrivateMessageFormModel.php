<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUserPrivateMessageFormModel extends FormModelBase {
		public $id;
		public $from;
		public $to;
		public $text;
		protected $userFrom, $userTo;
		function getARClassName() {
			return "UserPrivateMessageModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'to' => 'To',
				'from' => 'From',
				'text' => 'Text',
			);
		}
		function rules() {
			return Array(
				Array( 'to, from, text', 'required' ),
				Array( 'text', 'length', 'max' => CommonLib::maxWord ),
				Array( 'to', 'validateTo' ),
				Array( 'from', 'validateFrom' ),
			);
		}
		function validateTo() {
			if( !$this->hasErrors()) {
				$this->userTo = UserModel::model()->findByAttributes( Array( 'user_login' => $this->to ));
				if( !$this->userTo ) {
					$this->addI18NError( 'to', "Can't find User!" );
				}
			}
		}
		function validateFrom() {
			if( !$this->hasErrors()) {
				$this->userFrom = UserModel::model()->findByAttributes( Array( 'user_login' => $this->from ));
				if( !$this->userFrom ) {
					$this->addI18NError( 'from', "Can't find User!" );
				}
			}
		}
		function loadAR( $pk = 0 ) {
			if( parent::loadAR( $pk )) {
				$AR = $this->getAR();
				if( $AR->isNewRecord ) {
					$AR->idFrom = Yii::App()->user->id;
				}
				return true;
			}
			return false;
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'to', 'from' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( $AR->idTo ) {
					$userTo = UserModel::model()->findByPk( $AR->idTo );
					$this->to = $userTo->user_login;
				}
				if( $AR->idFrom ) {
					$userFrom = UserModel::model()->findByPk( $AR->idFrom );
					$this->from = $userFrom->user_login;
				}
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'to', 'from' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->idTo = $this->userTo->ID;
				$AR->idFrom = $this->userFrom->ID;
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>