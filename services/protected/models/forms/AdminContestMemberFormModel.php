<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminContestMemberFormModel extends FormModelBase {
		public $id;
		public $idContest;
		public $login;
		public $status;
		public $accountNumber;
		public $investorPassword;
		public $idServer;
		protected $idUser;
		function getARClassName() {
			return "ContestMemberModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'login' => 'User login',
				'status' => 'Status',
				'accountNumber' => 'Account number',
				'investorPassword' => 'Investor password',
				'idServer' => 'Server',
			);
		}
		function rules() {
			return Array(
				Array( 'idContest, status, login, accountNumber, investorPassword, idServer', 'required' ),
				Array( 'idContest, idServer', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'accountNumber', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => CommonLib::maxUInt ),
				Array( 'status', 'in', 'range' => Array( 'participates', 'disqualified', 'quitted', 'completed' )),
				Array( 'login', 'validateLogin' ),
				Array( 'idContest', 'existsIDModel', 'model' => 'ContestModel' ),
				Array( 'idServer', 'existsIDModel', 'model' => 'ServerModel' ),
			);
		}
		function uniqueUser() {
			$AR = $this->getAR();
			if( !$AR ) return;
			
			$c = new CDbCriteria();
			$c->params = Array();
			
			$c->addCondition( "`t`.`idContest` = :idContest" );
			$c->params[ ':idContest' ] = $this->idContest;
			
			$c->addCondition( "`t`.`idUser` = :idUser" );
			$c->params[ ':idUser' ] = $this->idUser;
							
			if( !$AR->isNewRecord ) {
				$c->addCondition( "`t`.`id` != :pk" );
				$c->params[ ':pk' ] = $AR->id;
			}
			
			$exists = ContestMemberModel::model()->exists( $c );
			if( $exists ) {
				$this->addI18NError( 'login', "User {login} for this contest is already occupied. Please, select another.", Array( '{login}' => $this->login ));
			}
		}
		function validateLogin() {
			if( !$this->hasErrors()) {
				$user = UserModel::model()->findByAttributes( Array( 'user_login' => $this->login ));
				if( !$user ) {
					$this->addI18NError( 'login', 'Wrong login!' );
				}
				else{
					$this->idUser = $user->id;
					$this->uniqueUser();
				}
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR( null, Array(), Array( 'login' ));
				if( $AR->idUser ) {
					$user = UserModel::model()->findByPk( $AR->idUser );
					$this->login = $user->user_login;
				}
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array( 'login' ));
			$AR->idUser = $this->idUser;
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>