<?
Yii::import( 'models.base.SettingsTextContentFormModelBase' );

final class AdminCalendarEventSettingsTextContentFormModel extends SettingsTextContentFormModelBase{
	
	public $title = Array();
	public $metaTitle = Array();
	public $metaDesc = Array();
	public $metaKeys = Array();
	public $firstDesc = Array();
	public $secondDesc = Array();
	
	public $textFields = array(
		'title' => 'textFieldRow',
		'metaTitle' => 'textFieldRow',
		'metaDesc' => 'textAreaRow',
		'metaKeys' => 'textAreaRow',
		'firstDesc' => 'textAreaRow',
		'secondDesc' => 'textAreaRow',
	);

	protected function getSourceAttributeLabels() {
		return $this->setTextLabels( Array() );
	}
	function rules() {
		return Array(
			Array( 'title, metaTitle, metaDesc, metaKeys', 'checkLens', 'max' => CommonLib::maxByte ),
			Array( 'firstDesc, secondDesc', 'checkLens', 'max' => CommonLib::maxWord ),
		);
	}
	function loadAR( $pk = 0 ) {
		$AR = CalendarEventSettingsModel::getModel();
		$this->setAR( $AR );
		return true;
	}
	public function getSingleUrl(){
		return Yii::app()->createUrl('calendarEvent/list');
	}
	
	
}