<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminRssFeedsFeedFormModel extends FormModelBase {
		public $id;
		public $idLanguage;
		public $enabled;
		public $order;
		public $title;
		public $chanelTitle;
		public $parent;
		public $newsCount;
		public $slug;
		public $idGroup;
		public $cats;
		public $description;

		function getARClassName() {
			return "RssFeedsFeedModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'idLanguage' => 'Language',
				'enabled' => 'Enabled?',
				'order' => 'Order',
				'title' => 'Title',
				'chanelTitle' => 'Chanel title',
				'description' => 'Description',
				'parent' => 'Parent',
				'newsCount' => 'News count',
				'slug' => 'slug',
				'idGroup' => 'Group',
				'cats' => 'WP Categories'
			);
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'idLanguage, idGroup, order, parent, newsCount', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
				Array( 'enabled', 'boolean' ),
				Array( 'title, chanelTitle, slug, cats, description', 'length', 'max' => CommonLib::maxByte ),
				Array( 'title, chanelTitle, slug, description', 'required' ),
				Array( 'slug', 'uniqueField' ),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				return true;
			}
			return false;
		}
		function loadFromPost() {
			if( parent::loadFromPost() ) {
				if( !$this->newsCount ) $this->newsCount = 20;
				if( !$this->cats ) $this->cats = array(0=>0);
				$this->cats = implode( ',', $this->cats );
			}
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>