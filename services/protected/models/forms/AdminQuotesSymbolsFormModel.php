<?
Yii::import('models.base.FormModelBase');

final class AdminQuotesSymbolsFormModel extends FormModelBase
{
    public $id;
    public $name;
    public $histName;
    public $tickName;
    public $yahooHistName;
    public $category;
    public $panel;
    public $keywords;
    public $sourceType;
    public $bitzSymbol;
    public $okexSymbol;
    public $convertToUsd;
    public $histAliases;

    public $toCryptoCurrenciesPage;
    
    public $nalias;
    public $desc;
    public $default;
    public $visible;
    public $toChartList;
    public $title;
    public $metaTitle;
    public $metaDescription;
    public $tooltip;
    public $preview;
    public $toInformer;
    public $chartTitle;
    public $wpPostsTitle;
    public $metaKeywords;
    public $chartLabel;
    public $searchPhrases;
    public $weightInCat;
    public $informerName;

    public $cryptoCurrencyTitle;
    public $cryptoCurrencySubTitle;

    public $nameIcon;
    
    public $excludeFieldsFromAr = array( 'nalias', 'desc', 'default', 'visible', 'toChartList', 'title', 'metaTitle', 'metaDescription', 'tooltip', 'preview', 'toInformer', 'chartTitle', 'wpPostsTitle', 'metaKeywords', 'chartLabel', 'searchPhrases', 'weightInCat', 'keywords', 'histAliases', 'informerName', 'cryptoCurrencyTitle', 'cryptoCurrencySubTitle' );
    
    public static function getTextFields(){
        return array(
            'default' => array( 'modelField' => 'default', 'type' => 'dropDownListRow', 'class' => '', 'disabled' => false ),
            'visible' => array( 'modelField' => 'visible', 'type' => 'dropDownListRow', 'class' => '', 'disabled' => false ),
            'toChartList' => array( 'modelField' => 'toChartList', 'type' => 'checkBox', 'class' => '', 'disabled' => false ),
            'toInformer' => array( 'modelField' => 'toInformer', 'type' => 'checkBox', 'class' => '', 'disabled' => false ),
            
            'nalias' => array( 'modelField' => 'nalias', 'type' => 'textFieldRow', 'class' => '', 'disabled' => false ),
            'desc' => array( 'modelField' => 'desc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'title' => array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'cryptoCurrencyTitle' => array( 'modelField' => 'cryptoCurrencyTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'cryptoCurrencySubTitle' => array( 'modelField' => 'cryptoCurrencySubTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'informerName' => array( 'modelField' => 'informerName', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'metaTitle' => array( 'modelField' => 'metaTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'metaDescription' => array( 'modelField' => 'metaDescription', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'tooltip' => array( 'modelField' => 'tooltip', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'preview' => array( 'modelField' => 'preview', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'chartTitle' => array( 'modelField' => 'chartTitle', 'type' => 'textFieldRow', 'class' => '', 'disabled' => false ),
            'wpPostsTitle' => array( 'modelField' => 'wpPostsTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'metaKeywords' => array( 'modelField' => 'metaKeywords', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'chartLabel' => array( 'modelField' => 'chartLabel', 'type' => 'textFieldRow', 'class' => '', 'disabled' => false ),
            'searchPhrases' => array( 'modelField' => 'searchPhrases', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'weightInCat' => array( 'modelField' => 'weightInCat', 'type' => 'textFieldRow', 'class' => '', 'disabled' => false ),
        );
    }


    function getARClassName() {
        return "QuotesSymbolsModel";
    }

    protected function getSourceAttributeLabels() {
        $labels = array(
            'id' => 'ID',
            'name' => 'Имя инструмента',
            'histName' => 'Имя для запроса истории или binance символ',
            'yahooHistName' => 'Yahoo history name',
            'tickName' => 'Имя для потока котировок',
            'metaDescription' => 'Meta Description',
            'category' => 'Категория',
            'panel' => 'На панель',
            'keywords' => 'Keywords',
            'sourceType' => 'Source',
            'bitzSymbol' => 'Bitz symbol',
            'histAliases' => 'History Aliases',
            'nameIcon' => 'Upload icon',
            'convertToUsd' => 'To usd?',
            'okexSymbol' => 'Okex symbol',
            'toCryptoCurrenciesPage' => 'To crypto currencies page',
        );
        $languages = LanguageModel::getAll();
        foreach( $languages as $language ){            
            $labels[ "nalias[{$language->id}]" ] = "Имя алиас";
            $labels[ "desc[{$language->id}]" ] = "Описание";
            $labels[ "default[{$language->id}]" ] = "По умолчанию";
            $labels[ "visible[{$language->id}]" ] = "Видимость";
            $labels[ "toChartList[{$language->id}]" ] = "В листинг графиков?";
            $labels[ "title[{$language->id}]" ] = "Тайтл";
            $labels[ "metaTitle[{$language->id}]" ] = "Метатайтл";
            $labels[ "metaDescription[{$language->id}]" ] = "Meta Description";
            $labels[ "tooltip[{$language->id}]" ] = "Подсказка";
            $labels[ "preview[{$language->id}]" ] = 'Превью. Доступные переменные - {$high}, {$low}, {$open}, {$close}, {$ch}, {$ch%}, {$price}, {$yesterdayClose}';
            $labels[ "toInformer[{$language->id}]" ] = 'В информер по дефолту';
            $labels[ "chartTitle[{$language->id}]" ] = 'Заголовок графика';
            $labels[ "wpPostsTitle[{$language->id}]" ] = 'Заголовок блока постов';
            $labels[ "metaKeywords[{$language->id}]" ] = 'Meta Keywords';
            $labels[ "chartLabel[{$language->id}]" ] = 'Надпись на графике';
            $labels[ "searchPhrases[{$language->id}]" ] = 'Список ключевых фраз котировки для поиска';
            $labels[ "weightInCat[{$language->id}]" ] = 'Weight in category';
            $labels[ "informerName[{$language->id}]" ] = 'Informer name';
            $labels[ "cryptoCurrencyTitle[{$language->id}]" ] = 'Crypto currency title';
            $labels[ "cryptoCurrencySubTitle[{$language->id}]" ] = 'Crypto currency sub title';
        }    
        return $labels;
    }
    function rules() {
        $rules = array(
            array( 'nalias, title, metaTitle, metaDescription, tooltip, chartTitle, wpPostsTitle, metaKeywords, chartLabel, searchPhrases, informerName, cryptoCurrencyTitle, cryptoCurrencySubTitle', 'checkLens', 'max' => CommonLib::maxByte ),
            array( 'desc, preview', 'checkLens', 'max' => CommonLib::maxWord ),
            array( 'name, histName, yahooHistName, tickName, bitzSymbol, okexSymbol', 'length', 'max' => 255 ),
            array( 'name, yahooHistName, tickName', 'uniqueField' ),
            array( 'histName', 'uniqueHistName' ),
            array( 'sourceType', 'in', 'range' => array( 'own','yahoo','bitz', 'bitfinex', 'okex', 'binance'), 'allowEmpty' => false ),
            array( 'nameIcon', 'file', 'allowEmpty' => true, 'types' => array( 'jpg', 'jpeg', 'png', 'gif' )),
            array( 'convertToUsd, toCryptoCurrenciesPage', 'boolean' ),
        );
        
        $languages = LanguageModel::getAll();
        foreach( $languages as $language ){            
            $rules[] = array( "toChartList[{$language->id}]", 'boolean');
            $rules[] = array( "toInformer[{$language->id}]", 'boolean');
            $rules[] = array( "weightInCat[{$language->id}]", 'numerical', 'integerOnly' => true, 'min' => 0 );
        }    
        
        return $rules;
    }
    public function uniqueHistName($field, $params)
    {
        if (!$this->histName) {
            return ;
        }

        $arName = $this->getARClassName();

        if ($this->sourceType != 'binance') {
            if ($this->id) {
                $model = $arName::model()->find([
                    'condition' => '`t`.`histName` = :histName AND `t`.`id` <> :id',
                    'params' => [':histName' => $this->histName, ':id' => $this->id]
                ]);
            } else {
                $model = $arName::model()->find([
                    'condition' => '`t`.`histName` = :histName',
                    'params' => [':histName' => $this->histName]
                ]);
            }
            $msg = '{attribute} already in use';
        } else {
            if ($this->id) {
                $model = $arName::model()->find([
                    'condition' => '`t`.`histName` = :histName AND `t`.`id` <> :id AND `t`.`convertToUsd` = :convertToUsd AND `t`.`sourceType` = :sourceType',
                    'params' => [':histName' => $this->histName, ':id' => $this->id, ':convertToUsd' => $this->convertToUsd, ':sourceType' => $this->sourceType]
                ]);
            } else {
                $model = $arName::model()->find([
                    'condition' => '`t`.`histName` = :histName AND `t`.`convertToUsd` = :convertToUsd AND `t`.`sourceType` = :sourceType',
                    'params' => [':histName' => $this->histName, ':convertToUsd' => $this->convertToUsd, ':sourceType' => $this->sourceType]
                ]);
            }

            $msg = '{attribute} with such convertToUsd already in use';
        }

        if ($model) {
            $this->addError($field, Yii::t('yii', $msg, array(
                '{attribute}' => $this->getAttributeLabel("{$field}"),
            )));
        }
    }
    function checkLens( $field, $params ) {
        $NSi18n = $this->getNSi18n();
        foreach( $this->$field as $idLanguage=>$value ) {
            if( mb_strlen( $value ) > $params['max'] ) {
                $this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', array( 
                    '{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
                    '{max}' => $params['max'],
                )));
            }
        }
    }
    function loadFromAR( $AR = null, $loadFields = array(), $exceptFields = array() ) {
        $exceptFields = $this->excludeFieldsFromAr;
        if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
            $AR = $this->getAR();
            
            $i18ns = $AR->i18ns;
            foreach( $i18ns as $i18n ) {
                
                foreach( $this->textFields as $formFieldName => $settings ){
                    $this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
                }
            }
                    
            $this->keywords = QuotesSymbolKeywordModel::getFormKeywords( $AR->id );
            $this->histAliases = QuotesHistAliasModel::getFormAliases( $AR->id );                
            return true;
        }
        return false;
    }
    
    function saveAR( $runValidation = true, $attributes = null ) {
        if( parent::saveAR( $runValidation, $attributes )) {
            $AR = $this->getAR();
            $i18ns = array();
            $languages = LanguageModel::getAll();
            foreach( $languages as $language ) {
                    
                $arrToSave = array();
                
                foreach( $this->textFields as $formFieldName => $settings ){
                    $arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
                }
                $i18ns[ $language->id ] = (object)$arrToSave;
            }
            
            $AR->setI18Ns( $i18ns );
            return true;
        }
        return false;
    }
    

    function load($id = 0){
        $post = $this->getPostLink();
        if((int)@$post['id']) $id = (int)@$post['id'];
        if($this->loadAR($id)){
            $this->loadFromAR(null,null,array('values'));
            $this->loadFromPost();
            $this->loadFromFiles( array( 'nameIcon' ));
            return true;
        }
        return false;
    }

    function save(){
        $AR = $this->getAR();
        if(!$AR) return false;
        $this->saveToAR(null, array(), array('nameIcon'));

        if( $this->nameIcon ) {
            $AR->uploadIcon( $this->nameIcon );
        }

        if($this->saveAR(false)){
            if($id=$AR->getPrimaryKey()){
                
                
                foreach( $this->keywords as &$langKeys ){
                    $langKeys = $langKeys ? explode( ',', $langKeys ) : array( $this->name );
                }
                QuotesSymbolKeywordModel::setFormKeywords( $id, $this->keywords );
                
                $this->histAliases = $this->histAliases ? explode( ',', $this->histAliases ) : '';
                QuotesHistAliasModel::setFormAliases( $id, $this->histAliases );
                
                return $id;
            }
            return Yii::app()->db->getLastInsertID();
        }
        return false;
    }
}

