<?
	Yii::import( 'models.base.FormModelBase' );

	final class BrokerQuestionFormModel extends FormModelBase {
		public $id;
		public $idBroker;
		public $idQuestion;
		public $text;
		function getARClassName() {
			return "BrokerQuestionModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'text' => 'Text',
			);
		}
		function rules() {
			return Array(
				Array( 'text, idBroker', 'required' ),
				Array( 'idBroker, idQuestion', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'text', 'length', 'max' => CommonLib::maxWord ),
			);
		}
		function loadFromPost() {
			if( parent::loadFromPost() ) {
				if( !$this->idQuestion ) $this->idQuestion = null;
			}
		}
		function loadAR( $pk = 0 ) {
			if( parent::loadAR( $pk )) {
				$AR = $this->getAR();
				if( $AR->isNewRecord ) {
					$AR->idUser = Yii::App()->user->id;
				}
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>