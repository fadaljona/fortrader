<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminContestWinerFormModel extends FormModelBase {
		public $id;
		public $idContest;
		public $accountNumber;
		public $place;
		public $prize;
		protected $idMember;
		function getARClassName() {
			return "ContestWinerModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'idContest' => 'Contest',
				'accountNumber' => 'Account number',
				'place' => 'Place',
				'prize' => 'Prize',
			);
		}
		function rules() {
			return Array(
				Array( 'idContest, accountNumber, place', 'required' ),
				Array( 'idContest, place', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'prize', 'numerical', 'min' => 0 ),
				Array( 'accountNumber', 'validateAccountNumber' ),
				Array( 'idContest', 'existsIDModel', 'model' => 'ContestModel' ),
			);
		}
		function uniqueMember() {
			$AR = $this->getAR();
			if( !$AR ) return;
			
			$c = new CDbCriteria();
			$c->params = Array();
			
			$c->addCondition( "`t`.`idContest` = :idContest" );
			$c->params[ ':idContest' ] = $this->idContest;
			
			$c->addCondition( "`t`.`idMember` = :idMember" );
			$c->params[ ':idMember' ] = $this->idMember;
							
			if( !$AR->isNewRecord ) {
				$c->addCondition( "`t`.`id` != :pk" );
				$c->params[ ':pk' ] = $AR->id;
			}
			
			$exists = ContestWinerModel::model()->exists( $c );
			if( $exists ) {
				$this->addI18NError( 'accountNumber', "Account number {account} for this contest is already occupied. Please, select another.", Array( '{account}' => $this->accountNumber ));
			}
		}
		function validateAccountNumber() {
			if( !$this->hasErrors()) {
				$member = ContestMemberModel::model()->findByAttributes( Array( 'idContest' => $this->idContest, 'accountNumber' => $this->accountNumber ));
				if( !$member ) {
					$this->addI18NError( 'accountNumber', 'Wrong account number!' );
				}
				else{
					$this->idMember = $member->id;
					$this->uniqueMember();
				}
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR( null, Array(), Array( 'accountNumber' ));
				if( $AR->idMember ) {
					$member = ContestMemberModel::model()->findByPk( $AR->idMember );
					$this->accountNumber = $member->accountNumber;
				}
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array( 'accountNumber' ));
			$AR->idMember = $this->idMember;
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>