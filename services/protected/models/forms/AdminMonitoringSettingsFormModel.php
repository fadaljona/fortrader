<?
Yii::import( 'models.base.SettingsFormModelBase' );

final class AdminMonitoringSettingsFormModel extends SettingsFormModelBase{
	
	public $ogImage;
	public $inactiveDays;
	
	public $title = Array();
	public $metaTitle = Array();
	public $metaDesc = Array();
	public $metaKeys = Array();
	public $shortDesc = Array();
	public $fullDesc = Array();
	
	public $textFields = array(
		'title' => 'textFieldRow',
		'metaTitle' => 'textFieldRow',
		'metaDesc' => 'textAreaRow',
		'metaKeys' => 'textAreaRow',
		'shortDesc' => 'textAreaRow',
		'fullDesc' => 'textAreaRow',
	);

	protected function getSourceAttributeLabels() {
		$labels = Array(
			'ogImage' => 'List Og Image',
			'inactiveDays' => 'Inactive days'
		);
		return $this->setTextLabels( $labels );
	}
	function rules() {
		return Array(
			Array( 'ogImage', 'length', 'max' => CommonLib::maxByte ),
			Array( 'inactiveDays', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
			Array( 'title, metaTitle, metaDesc, metaKeys', 'checkLens', 'max' => CommonLib::maxByte ),
			Array( 'shortDesc, fullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
		);
	}
	function loadAR( $pk = 0 ) {
		$AR = MonitoringSettingsModel::getModel();
		$this->setAR( $AR );
		return true;
	}
}