<?php
Yii::import('models.base.SettingsFormModelBase');

final class AdminCryptoCurrenciesSettingsFormModel extends SettingsFormModelBase
{
    public $ogImage;
    public $bitcoin;
    public $videoChanel;
    public $faq;

    public $title = array();
    public $metaTitle = array();
    public $metaDesc = array();
    public $metaKeys = array();

    public $textFields = array(
        'title' => 'textFieldRow',
        'metaTitle' => 'textFieldRow',
        'metaDesc' => 'textAreaRow',
        'metaKeys' => 'textAreaRow',
    );

    protected function getSourceAttributeLabels()
    {
        $labels = array(
            'ogImage' => 'Og Image',
        );
        
        return $this->setTextLabels($labels);
    }
    public function rules()
    {
        return array(
            array(
                'ogImage',
                'length',
                'max' => CommonLib::maxByte
            ),
            array(
                'title, metaTitle',
                'checkLens',
                'max' => CommonLib::maxByte
            ),
            array(
                'metaDesc, metaKeys',
                'checkLens',
                'max' => CommonLib::maxWord
            ),
            array(
                'bitcoin, videoChanel, faq',
                'numerical',
                'integerOnly' => true
            ),
        );
    }
    public function loadAR($pk = 0)
    {
        $AR = CryptoCurrenciesSettingsModel::getModel();
        $this->setAR($AR);
        return true;
    }
}