<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminInformersCategoryFormModel extends FormModelBase {
		public $id;
		public $slug;
		public $type;
		public $idInType;
		public $order;
		public $ogImages;
		public $hidden;
		public $rawSettings;
		
		public $title;
		public $description;
		public $nameImages;
		
		public $pageTitle;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		public $shortDesc;
		public $fullDesc;
		public $informerTitle;
		
		public $toolTitle;
		
		public $excludeFieldsFromAr = array( 'title', 'description', 'nameImages', 'ogImages', 'pageTitle', 'metaTitle', 'metaDesc', 'metaKeys', 'shortDesc', 'fullDesc', 'informerTitle', 'toolTitle' );
		
		public static function getTextFields(){
			return array(
				'title' => Array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'description' => Array( 'modelField' => 'description', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'toolTitle' => Array( 'modelField' => 'toolTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				
				'informerTitle' => Array( 'modelField' => 'informerTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'pageTitle' => Array( 'modelField' => 'pageTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'metaTitle' => Array( 'modelField' => 'metaTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'metaDesc' => Array( 'modelField' => 'metaDesc', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'metaKeys' => Array( 'modelField' => 'metaKeys', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'shortDesc' => Array( 'modelField' => 'shortDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'fullDesc' => Array( 'modelField' => 'fullDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
			);
		}
		function getARClassName() {
			return "InformersCategoryModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'slug' => 'Slug',
				'type' => 'Type',
				'idInType' => 'id In Type',
				'ogImages' => 'Facebook ogImages',
				'hidden' => 'Hidden?',
				'rawSettings' => 'Additional settings',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){			
				$labels[ "title[{$language->id}]" ] = "Title - for list";
				$labels[ "description[{$language->id}]" ] = "Description - for list";
				$labels[ "nameImages[{$language->id}]" ] = "Image - for list";
				$labels[ "pageTitle[{$language->id}]" ] = "Page title";
				$labels[ "metaTitle[{$language->id}]" ] = "Meta title";
				$labels[ "metaDesc[{$language->id}]" ] = "Meta desc";
				$labels[ "metaKeys[{$language->id}]" ] = "Meta keys";
				$labels[ "shortDesc[{$language->id}]" ] = "Short desc";
				$labels[ "fullDesc[{$language->id}]" ] = "Full desc";
				$labels[ "informerTitle[{$language->id}]" ] = "Informer Title";
				$labels[ "toolTitle[{$language->id}]" ] = "Informer tool title";
			}
			
			return $labels;
		}
		function rules() {
			$rules = Array(
				Array( 'slug', 'length', 'allowEmpty' => false, 'min' => 1, 'max' => CommonLib::maxByte ),
				Array( 'slug', 'uniqueField', 'message' => 'Slug is busy. Please choose another.' ),
				Array( 'slug, type, idInType', 'required' ),
				Array( 'type', 'in', 'range' => InformersCategoryModel::getAvailableTypes(), 'allowEmpty' => false ),
				Array( 'title, pageTitle, metaTitle, metaDesc, metaKeys, informerTitle, toolTitle', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'description, shortDesc, fullDesc, rawSettings', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'idInType, order', 'numerical', 'integerOnly' => true, 'min' => 0 ),
				Array('type', 'checkUniqueness'),
				Array( 'title', 'notEmptyCurrentLang' ),
				Array( 'ogImages', 'file', 'allowEmpty' => true, 'types' => Array( 'jpg', 'jpeg', 'png', 'gif' )),
				Array( 'hidden', 'boolean' ),
			);

			
			/*$currentLangId = LanguageModel::getCurrentLanguageID();		
			$rules[] = Array( "nameImages[$currentLangId]", 'file', 'types' => Array( 'jpg', 'jpeg', 'png', 'gif' ), 'allowEmpty' => true );*/
			
			return $rules;
		}
		function notEmptyCurrentLang( $field, $params ) {
			$NSi18n = $this->getNSi18n();	
			$currentLangId = LanguageModel::getCurrentLanguageID();
			
			if( mb_strlen( $this->{$field}[$currentLangId] ) == 0 ) {
				$this->addError( "{$field}[{$currentLangId}]", Yii::t( '*','{attribute} can not be empty.', Array( 
					'{attribute}' => $this->getAttributeLabel( "{$field}[{$currentLangId}]" ),
				)));
			}
		}
		function checkUniqueness($attribute,$params){
			$ARClassName = $this->getARClassName();
            $model = $ARClassName::model()->find('type = ? AND idInType = ? ', array($this->type, $this->idInType));
            if($model != null && $model->id != $this->id)
                $this->addError('type', Yii::t( '*','This type and idInType already exist') );
         
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}

		function loadFromFiles( $loadFields = Array()) {
			parent::loadFromFiles( $loadFields );
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$file = CUploadedFile::getInstance( $this, "nameImages[{$language->id}]" );
				if( $file )	$this->nameImages[$language->id] = $file;
			}
			
			$ogFile = CUploadedFile::getInstance( $this, "ogImages" );
			if( $ogFile ) $this->ogImages = $ogFile;
		}
		
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
					}
					$this->nameImages[ $i18n->idLanguage ] = $i18n->srcImage;
				}
				
				if( $this->rawSettings ) $this->rawSettings = unserialize($this->rawSettings);
				
				$this->ogImages = $AR->srcOgImage;
								
				return true;
			}
			return false;
		}

		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'sizeBlock' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				if( $this->ogImages ) $AR->uploadOgImage( $this->ogImages );
			}
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					
					$arrToSave = Array();
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
					}
					$arrToSave[ 'nameImages' ] = $this->nameImages[ $language->id ];

					$i18ns[ $language->id ] = (object)$arrToSave;
				}
				
				$AR->setI18Ns( $i18ns );

				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				
				
				return true;
			}
			return false;
		}
		function loadFromPost() {
			if( parent::loadFromPost() ) {
				if( $this->rawSettings ) $this->rawSettings = serialize($this->rawSettings);
			}
		}
		function loadBySlug( $slug ) {
			if( !$slug ) return false;
			if( $this->loadARBySlug( $slug )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				return true;
			}
			return false;
		}
		function loadARBySlug( $slug ) {
			if( !$slug ) return false;
			$ARClassName = $this->getARClassName();
			$AR = CActiveRecord::model( $ARClassName )->find( array(
				'condition' => ' `t`.`slug` = :slug ',
				'params' => array( ':slug' => $slug )
			) );
			if( !$AR ) return false;
			$this->setAR( $AR );
			return true;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>