<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminAdvertisementFormModel extends FormModelBase {
		public $id;
		public $idCampaign;
		public $header;
		public $text;
		public $image;
		public $holdedImage;
		public $width;
		public $height;
		public $minScreenWidth;
		public $maxScreenWidth;
		public $url;
		public $countLikes;
		public $showEverywhere;
		public $zones;
		public $dontShow;
		public $htmlBaner;
		public $clearStats;
		
		public $needUploadHtmlFile;
		public $iframeWidth;
		public $iframeHeight;
        public $iframeFile;
        public $isDirectAdLink;
		
		protected $mode;
		protected $type = 'Text';
		function __construct() {
			parent::__construct();
			$this->mode = Yii::App()->user->checkAccess( 'advertisementControl' ) ? 'Admin' : 'User';
		}
		function setType( $type ) {
			$this->type = $type;
		}
		function getARClassName() {
			return "AdvertisementModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'idCampaign' => 'Campaign',
				'header' => 'Header',
				'text' => 'Text',
				'image' => 'Image',
				'url' => 'Link to website',
				'showEverywhere' => 'Show everywhere',
				'zones' => 'Zones',
				'dontShow' => 'Dont show',
				'htmlBaner' => 'Html?',
				'clearStats' => 'Clear stats',
				'countLikes' => 'Likes',
                'needUploadHtmlFile' => 'Upload Html File?',
                'isDirectAdLink' => 'Is direct ad link?'
			);
		}
		function rules() {
			$rules = Array();
			
			if( in_array( $this->type, Array( 'Text', 'Image', 'Button', 'Offer' ))) {
				if( ( $this->type=='Image' || $this->type=='Button' ) && $this->htmlBaner ){
					$rules[] = Array( 'idCampaign, header', 'required' );
					if( !$this->needUploadHtmlFile ){
						$rules[] = Array( 'text', 'required' );
						$rules[] = Array( 'text', 'length', 'max' => AdvertisementModel::MAXTextLen );
					}
				}else{
					//$rules[] = Array( 'idCampaign, header, url', 'required' );
				}
				
				$rules[] = Array( 'header', 'length', 'max' => AdvertisementModel::MAXHeaderLen );
				$rules[] = Array( 'url', 'length', 'max' => CommonLib::maxByte );
				$rules[] = Array( 'showEverywhere', 'in', 'range' => Array( '0', '1' ), 'allowEmpty' => false );
			}
			
			if( in_array( $this->type, Array( 'Text', 'Offer' ))) {
				$rules[] = Array( 'text', 'required' );
				$rules[] = Array( 'text', 'length', 'max' => AdvertisementModel::MAXTextLen );
			}
			if( ( ( $this->type=='Image' || $this->type=='Button' ) && !$this->htmlBaner) || $this->type == 'Offer' ) {
				$rules[] = Array( 'image', 'validateImage' );
				$rules[] = Array( 'width, height', 'required' );
				$rules[] = Array( 'width, height', 'numerical', 'integerOnly' => true, 'min' => 1 );
			}
			if( in_array( $this->type, Array( 'Offer' ))) {
				$rules[] = Array( 'countLikes', 'numerical', 'integerOnly' => true, 'min' => 0 );
			}
			
			if(  $this->type=='Image' || $this->type=='Button'  ) {
				$rules[] = Array( 'htmlBaner', 'in', 'range' => Array( '0', '1' ), 'allowEmpty' => false );
			}
			
			if( in_array( $this->type, Array( 'Text', 'Image', 'Button', 'Offer' ))) {
				$rules[] = Array( 'dontShow', 'in', 'range' => Array( '0', '1' ), 'allowEmpty' => false );
				$rules[] = Array( 'idCampaign', 'existsIDModel', 'model' => 'AdvertisementCampaignModel' );
			}
			
			if( $this->type == 'UploadImage' ) {
				$rules[] = Array( 'image', 'file', 'types' => Array( 'jpg', 'jpeg', 'png', 'gif', 'swf' ));
			}
			
			$rules[] = Array( 'minScreenWidth, maxScreenWidth', 'required' );
			$rules[] = Array( 'minScreenWidth, maxScreenWidth', 'numerical', 'integerOnly' => true, 'min' => 1 );
			
            $rules[] = Array( 'needUploadHtmlFile', 'boolean');

            $rules[] = Array( 'isDirectAdLink', 'boolean');
            
			
			$AR = $this->getAR();

			if( $this->htmlBaner && $this->needUploadHtmlFile && !$AR->iframe ){
				$rules[] = array( 'iframeWidth, iframeHeight', 'numerical', 'integerOnly'=>true, 'min'=>1, 'allowEmpty' => false);
				$rules[] = Array( 'iframeFile', 'file', 'allowEmpty' => false, 'types' => Array( 'html', 'htm' ));
			}
			
			
			return $rules;
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( empty( $post[ 'countLikes' ])) $this->countLikes = 0;
				if( !$this->minScreenWidth ) $this->minScreenWidth = 1;
				if( !$this->maxScreenWidth ) $this->maxScreenWidth = 10000;
			}
		}
		function validateImage() {
			$AR = $this->getAR();
			if( $AR->isNewRecord ) {
				if( !$this->holdedImage ) {
					$this->addError( 'image', Yii::t( 'yii', '{attribute} cannot be blank.', Array( '{attribute}' => $this->getAttributeLabel( 'image' ))));
				}
			}
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'holdedImage', 'image', 'width', 'height', 'zones', 'needUploadHtmlFile' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				$AR->type = $this->type;
				
				if( $this->holdedImage and ( 
					$AR->srcImage != $this->holdedImage 
					or $this->width != $AR->widthImage
					or $this->height != $AR->heightImage
				)) {
					if( $AR->srcImage != $this->holdedImage ) {
						if( AdvertisementModel::validatePathImage( $this->holdedImage )) {
							$AR->setImageFromHold( $this->holdedImage, $this->width, $this->height );
						}
					}
					else{
						$AR->resizeImage( $this->width, $this->height );
					}
				}
				
				if( $this->htmlBaner && !$this->needUploadHtmlFile ){
					$AR->deleteIframeFile();
				}
				
				if( $this->iframeFile ){
					$AR->uploadIframeFile( $this->iframeFile );
				}
				if( $this->iframeWidth && $this->iframeHeight && $this->needUploadHtmlFile ){
					$AR->setIframeText( $this->iframeWidth, $this->iframeHeight );
				}
				
				return true;
			}
			return false;
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'holdedImage', 'image', 'width', 'height', 'zones', 'needUploadHtmlFile' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				$this->holdedImage = $AR->srcImage;
				$this->image = $AR->resizedImage;
				$this->width = $AR->widthImage;
				$this->height = $AR->heightImage;
				
				$this->zones = $AR->getIDsZones();
				$this->zones = implode( ',', $this->zones );
				$this->iframeFile = $AR->absoluteSrcIframeFile;
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles( Array( 'image', 'iframeFile' ));
				
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
					# zones
				$this->zones = $this->zones ? explode( ',', $this->zones ) : Array();
				$this->zones  = array_filter( $this->zones, Array( 'CommonLib', 'isID' ));
				$AR->setZones( $this->zones );
				
				if( $this->mode == "Admin" and $this->clearStats ){
					$AR->clearStats();
				}
				
				return $AR->getPrimaryKey();
			}
			return false;
		}
		function saveImage() {
			return AdvertisementModel::holdImage( $this->image );
		}
	}

?>