<?php

Yii::import('models.base.FormModelBase');

final class AdminVideoChanelItemFormModel extends FormModelBase
{
    public $id;
    public $idChanel;

    public $title;
    public $url;


    public $excludeFieldsFromAr = array('title', 'url' );
        
    public static function getTextFields()
    {
        return array(
            'title' => array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'url' => array( 'modelField' => 'url', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
        );
    }
    public function getARClassName()
    {
        return "VideoChanelItemModel";
    }
    protected function getSourceAttributeLabels()
    {
        $labels = array();

        $languages = LanguageModel::getAll();
        foreach ($languages as $language) {
            $labels[ "title[{$language->id}]" ] = "Title";
            $labels[ "url[{$language->id}]" ] = "Video url";
        }
        return $labels;
    }
    public function rules()
    {
        $rules = array(
            array( 'title, url', 'checkLens', 'max' => CommonLib::maxByte ),
            array( 'title, url', 'notEmptyCurrentLang' ),
        );
        return $rules;
    }
    public function notEmptyCurrentLang($field, $params)
    {
        $NSi18n = $this->getNSi18n();
        $currentLangId = LanguageModel::getCurrentLanguageID();

        if (mb_strlen($this->{$field}[$currentLangId]) == 0) {
            $this->addError("{$field}[{$currentLangId}]", Yii::t('*', '{attribute} can not be empty.', array(
                '{attribute}' => $this->getAttributeLabel("{$field}[{$currentLangId}]"),
            )));
        }
    }
    public function checkLens($field, $params)
    {
        $NSi18n = $this->getNSi18n();
        foreach ($this->$field as $idLanguage => $value) {
            if (mb_strlen($value) > $params['max']) {
                $this->addError($field, Yii::t('yii', '{attribute} is too long (maximum is {max} characters).', array(
                    '{attribute}' => $this->getAttributeLabel("{$field}[{$idLanguage}]"),
                    '{max}' => $params['max'],
                )));
            }
        }
    }


    public function loadFromAR($AR = null, $loadFields = array(), $exceptFields = array())
    {
        $exceptFields = $this->excludeFieldsFromAr;
        if (parent::loadFromAR($AR, $loadFields, $exceptFields)) {
            $AR = $this->getAR();

            $i18ns = $AR->i18ns;
            foreach ($i18ns as $i18n) {
                foreach ($this->textFields as $formFieldName => $settings) {
                    $this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
                }
            }
            return true;
        }
        return false;
    }

    public function saveAR($runValidation = true, $attributes = null)
    {
        if (parent::saveAR($runValidation, $attributes)) {
            $AR = $this->getAR();
            $i18ns = array();
            $languages = LanguageModel::getAll();
            foreach ($languages as $language) {
                $arrToSave = array();

                foreach ($this->textFields as $formFieldName => $settings) {
                    $arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
                }
                $i18ns[ $language->id ] = (object)$arrToSave;
            }

            $AR->setI18Ns($i18ns);
            return true;
        }
        return false;
    }

    public function load($id = 0)
    {
        $post = $this->getPostLink();
        if ((int)@$post['id']) {
            $id = (int)@$post['id'];
        }
        if ($this->loadAR($id)) {
            $this->loadFromAR();
            $this->loadFromPost();
            return true;
        }
        return false;
    }

    public function save()
    {
        $AR = $this->getAR();
        if (!$AR) {
            return false;
        }

        $this->saveToAR(null, array(), array());

        if ($this->saveAR(false)) {
            return $AR->getPrimaryKey();
        }
        return false;
    }
}