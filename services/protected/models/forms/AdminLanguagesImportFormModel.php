<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminLanguagesImportFormModel extends FormModelBase {
		const MODE_UPLOAD = 1;
		const MODE_SELECT_LANGUAGES = 2;
		const MODE_HIDDEN = 3;
		protected $mode = self::MODE_UPLOAD;
		protected $defaultLanguages = Array();
		public $cancelAction = false;
		public $file;
		public $languages = Array();
		protected function getSourceAttributeLabels() {
			return Array(
				'file' => 'File',
				'languages' => 'Languages',
			);
		}
		function rules() {
			$rules = Array();
			if( $this->inMode( self::MODE_UPLOAD )) {
				$rules[] = Array( 'file', 'file' );
			}
			if( $this->inMode( self::MODE_SELECT_LANGUAGES )) {
				$rules[] = Array( 'languages', 'required' );
			}
			return $rules;
		}
		function loadDefauls() {
			if( $this->inMode( self::MODE_SELECT_LANGUAGES )) {
				$this->languages = $this->getDefaultLanguages();
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$this->cancelAction = is_string( $this->cancelAction ) ? (bool)strlen( $this->cancelAction ) : false;
				if( $this->inMode( self::MODE_SELECT_LANGUAGES )) {
					$post = $this->getPostLink();
					if( empty( $post[ 'languages' ])) $this->languages = Array();
					$this->languages  = array_filter( (array)$this->languages, Array( 'CommonLib', 'isIDorZero' ));
				}
			}
		}
		function validate( $attributes=null, $clearErrors=true ) {
			if( $this->cancelAction ) return true;
			return parent::validate( $attributes, $clearErrors );
		}
		function setDefaultLanguages( $ids ) {
			$this->defaultLanguages = $ids;
		}
		function getDefaultLanguages() {
			return $this->defaultLanguages;
		}
		function setMode( $mode ) {
			$this->mode = $mode;
		}
		function getMode() {
			return $this->mode;
		}
		function inMode( $mode ) {
			return $mode == $this->getMode();
		}
		function load( $id = 0 ) {
			$this->loadDefauls();
			$this->loadFromPost();
			if( $this->inMode( self::MODE_UPLOAD )) {
				$this->loadFromFiles( Array( 'file' ));
			}
			return true;
		}
	}

?>