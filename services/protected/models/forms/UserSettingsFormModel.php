<?
	Yii::import( 'models.base.FormModelBase' );

	final class UserSettingsFormModel extends FormModelBase {
		public $idUser;
		public $receiveNoticeAnswerMessage;
		public $receiveEmailNoticeAnswerPrivateMessage;
		public $receiveNoticeMessage;
		public $reciveNoticeNewJournal;
		public $reciveEmailNoticeNewJournal;
		public $mailingTypesAccept;
		
		public $receiveCommentsOnMyArticles;
		public $receiveCommentsOnMyProfile;
		public $receiveCommentsOnMyMonitoringAccounts;
		
		protected $user;
		function getARClassName() {
			return "UserSettingsModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'receiveNoticeAnswerMessage' => 'Receive notification for my answer',
				'receiveEmailNoticeAnswerPrivateMessage' => 'Receive notices to email about private messages',
				'receiveNoticeMessage' => 'Receive notification about new comments',
				'reciveNoticeNewJournal' => 'Receive notification of new issue',
				'reciveEmailNoticeNewJournal' => 'Send me e-mail about new magazine issue',
				'receiveCommentsOnMyArticles' => 'Send me e-mail about new comments on my posts',
				'receiveCommentsOnMyProfile' => 'Send me e-mail about new comments on my profile',
				'receiveCommentsOnMyMonitoringAccounts' => 'Send me e-mail about new comments on my monitoring accounts',
			);
		}
		function rules() {
			return Array(
				Array( 'receiveNoticeAnswerMessage, receiveEmailNoticeAnswerPrivateMessage, receiveNoticeMessage, reciveNoticeNewJournal, reciveEmailNoticeNewJournal, receiveCommentsOnMyArticles, receiveCommentsOnMyProfile, receiveCommentsOnMyMonitoringAccounts', 'required' ),
				Array( 'receiveNoticeAnswerMessage, receiveEmailNoticeAnswerPrivateMessage, receiveNoticeMessage, reciveNoticeNewJournal, reciveEmailNoticeNewJournal, receiveCommentsOnMyArticles, receiveCommentsOnMyProfile, receiveCommentsOnMyMonitoringAccounts', 'in', 'range' => Array( '0', '1' )),
			);
		}
		function loadAR( $pk = 0 ) {
			$this->user = UserModel::model()->findByPk( $pk );
			if( !$this->user ) return false;
			$AR = $this->user->getSettings();
			$this->setAR( $AR );
			return true;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				
				$mailingTypes = MailingTypeModel::model()->findAll();
				$mailingTypesAccept = is_array( $this->mailingTypesAccept ) ? $this->mailingTypesAccept : Array();
				$mailingTypesMute = Array();
				
				foreach( $mailingTypes as $mailingType ) {
					if( !in_array( $mailingType->id, $mailingTypesAccept )) {
						$mailingTypesMute[] = $mailingType->id;
					}
				}
				
				$this->user->setMailingTypesMute( $mailingTypesMute );
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( !$id ) $id = (int)@$post['idUser'];
			if( !$id ) return false;
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>