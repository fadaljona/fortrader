<?php

Yii::import('models.base.FormModelBase');

final class AdminPaymentSystemCurrencyFormModel extends FormModelBase
{
    public $id;
    public $idSystem;
    public $slug;

    public $title;
    public $aliases;


    public $excludeFieldsFromAr = array('title', 'aliases');
        
    public static function getTextFields()
    {
        return array(
            'title' => array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false )
        );
    }
    function getARClassName()
    {
        return "PaymentSystemCurrencyModel";
    }
    protected function getSourceAttributeLabels()
    {
        $labels = array(
            
        );

        $languages = LanguageModel::getAll();
        foreach ($languages as $language) {
            $labels[ "title[{$language->id}]" ] = "Title";
        }
        return $labels;
    }
    function rules()
    {
        $rules = array(
            array( 'title', 'checkLens', 'max' => CommonLib::maxByte ),
            array( 'title', 'notEmptyCurrentLang' ),
            array( 'aliases', 'validateAliases' ),
            array( 'slug', 'length', 'allowEmpty' => false, 'min' => 1, 'max' => 20 ),
            array( 'slug', 'uniqueField', 'message' => 'Slug is busy. Please choose another.' ),
            array( 'slug', 'required' ),
        );
        return $rules;
    }
    function validateAliases($field, $params)
    {
        if ($this->aliases) {
            $aliases = explode(',', $this->aliases);
            $aliasModels = PaymentSystemCurrencyAliasModel::model()->findAllByAttributes(array('alias' => $aliases));
            $existsAliases = array();
            if (!$this->id && $aliasModels) {
                foreach ($aliasModels as $alias) {
                    $existsAliases[] = $alias->alias;
                }
            } else {
                foreach ($aliasModels as $alias) {
                    if ($alias->idCurrency != $this->id) {
                        $existsAliases[] = $alias->alias;
                    }
                }
            }

            if ($existsAliases) {
                $this->addError($field, Yii::t('*', '{attribute} already exists. {values}', array(
                    '{attribute}' => $this->getAttributeLabel($field),
                    '{values}' => implode(', ', $existsAliases)
                )));
            }
        }
    }
    function notEmptyCurrentLang($field, $params)
    {
        $NSi18n = $this->getNSi18n();
        $currentLangId = LanguageModel::getCurrentLanguageID();

        if (mb_strlen($this->{$field}[$currentLangId]) == 0) {
            $this->addError("{$field}[{$currentLangId}]", Yii::t( '*','{attribute} can not be empty.', array(
                '{attribute}' => $this->getAttributeLabel("{$field}[{$currentLangId}]"),
            )));
        }
    }
    function checkLens($field, $params)
    {
        $NSi18n = $this->getNSi18n();
        foreach ($this->$field as $idLanguage=>$value) {
            if (mb_strlen($value) > $params['max']) {
                $this->addError($field, Yii::t('yii','{attribute} is too long (maximum is {max} characters).', array(
                    '{attribute}' => $this->getAttributeLabel("{$field}[{$idLanguage}]"),
                    '{max}' => $params['max'],
                )));
            }
        }
    }


    function loadFromAR($AR = null, $loadFields = array(), $exceptFields = array())
    {
        $exceptFields = $this->excludeFieldsFromAr;
        if (parent::loadFromAR($AR, $loadFields, $exceptFields)) {
            $AR = $this->getAR();

            $i18ns = $AR->i18ns;
            foreach ($i18ns as $i18n) {
                foreach ($this->textFields as $formFieldName => $settings) {
                    $this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
                }
            }
            $this->aliases = PaymentSystemCurrencyAliasModel::getFormAliases( $AR->id );
            return true;
        }
        return false;
    }

    function saveAR($runValidation = true, $attributes = null)
    {
        if (parent::saveAR($runValidation, $attributes)) {
            $AR = $this->getAR();
            $i18ns = array();
            $languages = LanguageModel::getAll();
            foreach ($languages as $language) {
                $arrToSave = array();

                foreach ($this->textFields as $formFieldName => $settings) {
                    $arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
                }
                $i18ns[ $language->id ] = (object)$arrToSave;
            }

            $AR->setI18Ns($i18ns);
            return true;
        }
        return false;
    }

    function load($id = 0)
    {
        $post = $this->getPostLink();
        if ((int)@$post['id']) {
            $id = (int)@$post['id'];
        }
        if ($this->loadAR($id)) {
            $this->loadFromAR();
            $this->loadFromPost();
            return true;
        }
        return false;
    }

    function save()
    {
        $AR = $this->getAR();
        if (!$AR) {
            return false;
        }

        $this->saveToAR(null, array(), array());

        if ($this->saveAR(false)) {
            if ($id=$AR->getPrimaryKey()) {
                $this->aliases = $this->aliases ? explode(',', $this->aliases) : false;
                PaymentSystemCurrencyAliasModel::setFormAliases($id, $this->aliases);
                return $id;
            }
        }
        return false;
    }
}