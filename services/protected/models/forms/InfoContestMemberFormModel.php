<?
	Yii::import( 'models.base.FormModelBase' );

	final class InfoContestMemberFormModel extends FormModelBase {
		public $firstName;
		public $lastName;
		public $phone;
		public $idCountry;
		function getARClassName() {
			return "UserModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'firstName' => 'First name',
				'lastName' => 'Last name',
				'phone' => 'Phone',
				'idCountry' => 'Country',
			);
		}
		function rules() {
			return Array(
				Array( 'firstName, lastName, phone, idCountry', 'required' ),
				Array( 'firstName, lastName, phone', 'length', 'max' => CommonLib::maxByte ),
				Array( 'idCountry', 'numerical', 'integerOnly' => true, 'min' => 1 ),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( !$id ) $id = (int)@$post['ID'];
			if( !$id ) return false;
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>