<?php
Yii::import('models.base.FormModelBase');

final class AdminCryptoCurrenciesExchangesFormModel extends FormModelBase
{
    public $id;
    public $url;
    public $statsUrl;
    public $title;

    public function getARClassName()
    {
        return "CryptoCurrenciesExchangesModel";
    }
    protected function getSourceAttributeLabels()
    {
        $labels = array(
            'url' => 'Url',
            'statsUrl' => 'Coinmarketcap stats url',
            'title' => 'title'
        );

        return $labels;
    }
    public function rules()
    {
        $rules = array(
            array( 'url, statsUrl, title', 'length', 'allowEmpty' => false, 'min' => 1, 'max' => CommonLib::maxByte ),
            array( 'url, statsUrl, title', 'uniqueField'),
        );
        return $rules;
    }

    public function load($id = 0)
    {
        $post = $this->getPostLink();
        if ((int)@$post['id']) {
            $id = (int)@$post['id'];
        }
        
        if ($this->loadAR($id)) {
            $AR = $this->getAR();
            $this->loadFromAR();
            $this->loadFromPost();
            return true;
        }
        return false;
    }
    public function save()
    {
        $AR = $this->getAR();
        if (!$AR) {
            return false;
        }

        $this->saveToAR(null, array(), array());

        if ($this->saveAR(false)) {
            return $AR->getPrimaryKey();
        }
        return false;
    }
}
