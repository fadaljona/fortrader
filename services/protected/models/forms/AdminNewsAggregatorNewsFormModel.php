<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminNewsAggregatorNewsFormModel extends FormModelBase {
		public $id;
		public $idLanguage;
		public $title;
		public $description;
		public $gmtTime;
		public $link;
		public $idRss;
		public $idCat;
		public $stemedNewsText;
		public $categoryKeys;


		function getARClassName() {
			return "NewsAggregatorNewsModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'idLanguage' => 'Language',
				'title' => 'Title',
				'description' => 'Description',
				'gmtTime' => 'News date',
				'link' => 'Link',
				'idRss' => 'Rss',
				'idCat' => 'Category',
				'stemedNewsText' => 'stemedNewsText',
				'categoryKeys' => 'Keys summary'
			);
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'idCat', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
				Array( 'idLanguage, idRss, gmtTime', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
				Array( 'link, title', 'length', 'max' => CommonLib::maxByte ),
				Array( 'stemedNewsText', 'length', 'max' => CommonLib::maxWord ),
				Array( 'idLanguage, title, description, gmtTime, link, idRss, idCat', 'required' ),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				
				
				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>