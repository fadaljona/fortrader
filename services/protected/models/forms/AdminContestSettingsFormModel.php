<?
	Yii::import( 'models.base.SettingsFormModelBase' );

	final class AdminContestSettingsFormModel extends SettingsFormModelBase {
		public $sendNotices;
		public $timeToSend;
		public $emails;
		

		public $title = Array();
		public $metaTitle = Array();
		public $metaDesc = Array();
		public $metaKeys = Array();
		public $ogImageUrl = Array();
		public $shortDesc = Array();
		public $fullDesc = Array();
		
		public $allTitle = Array();
		public $allMetaTitle = Array();
		public $allMetaDesc = Array();
		public $allMetaKeys = Array();
		public $allOgImageUrl = Array();
		public $allShortDesc = Array();
		public $allFullDesc = Array();

		
		public $textFields = array(
			'title' => 'textFieldRow',
			'metaTitle' => 'textFieldRow',
			'metaDesc' => 'textFieldRow',
			'metaKeys' => 'textFieldRow',
			'ogImageUrl' => 'textFieldRow',
			'shortDesc' => 'textAreaRow',
			'fullDesc' => 'textAreaRow',
			
			'allTitle' => 'textFieldRow',
			'allMetaTitle' => 'textFieldRow',
			'allMetaDesc' => 'textFieldRow',
			'allMetaKeys' => 'textFieldRow',
			'allOgImageUrl' => 'textFieldRow',
			'allShortDesc' => 'textAreaRow',
			'allFullDesc' => 'textAreaRow',
		);
		
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'sendNotices' => 'Send notices if downtime',
				'timeToSend' => 'Downtime (minutes)',
				'emails' => 'Emails',
			);
			return $this->setTextLabels( $labels );
		}
		function rules() {
			return Array(
				Array( 'sendNotices', 'in', 'range' => Array( '0', '1' )),
				Array( 'timeToSend', 'numerical', 'min' => 1, 'integerOnly' => true ),
				Array( 'emails', 'length', 'max' => CommonLib::maxByte ),
				Array( 'title, metaTitle, metaKeys, ogImageUrl, allTitle, allMetaTitle, allMetaKeys, allOgImageUrl', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'shortDesc, fullDesc, metaDesc, allShortDesc, allFullDesc, allMetaDesc', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
		function loadAR( $pk = 0 ) {
			$AR = ContestSettingsModel::getModel();
			$this->setAR( $AR );
			return true;
		}
	}

?>