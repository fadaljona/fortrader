<?
	Yii::import( 'models.base.FormModelBase' );

	final class EAFormModel extends FormModelBase {
		public $id;
		public $name;
		public $abouts = Array();
		public $creator;
		public $license;
		public $type;
		public $status;
		public $image;
		public $date;
		public $desc;
		public $params;
		public $idsTradePlatforms;
		function getARClassName() {
			return "EAModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'name' => 'Title',
				'creator' => 'Creator',
				'license' => 'License',
				'type' => 'Type',
				'status' => 'Status',
				'image' => 'Image',
				'date' => 'Date',
				'desc' => 'Description',
				'params' => 'Params',
				'idsTradePlatforms' => 'Trade platforms',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "abouts[{$language->id}]" ] = "About";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'name', 'required' ),
				Array( 'name, creator, license', 'length', 'max' => CommonLib::maxByte ),
				Array( 'abouts', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'type', 'in', 'range' => Array( 'Commercial', 'Free' )),
				Array( 'status', 'in', 'range' => Array( 'Running', 'Discontinued' )),
				Array( 'image', 'file', 'allowEmpty' => true, 'types' => Array( 'jpg', 'jpeg', 'gif', 'png' )),
				Array( 'date', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'desc', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'image' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->abouts[ $i18n->idLanguage ] = $i18n->about;
				}
				
				$this->image = $AR->srcImage;
				$this->idsTradePlatforms = $AR->getIDsTradePlatforms();
				
				return true;
			}
			return false;
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( empty( $post[ 'idsTradePlatforms' ])) $this->idsTradePlatforms = Array();
				$this->idsTradePlatforms  = array_filter( (array)$this->idsTradePlatforms, Array( 'CommonLib', 'isID' ));	
			}
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array()) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( !$AR->idUser ) {
					$AR->idUser = Yii::App()->user->id;
				}
				
				if( !$AR->date ) {
					$AR->date = date( 'Y-m-d' );
				}
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'about' => $this->abouts[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );
				$AR->setTradePlatforms( $this->idsTradePlatforms );				
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				
				$AR = $this->getAR();
				$this->params = explode( ',', $AR->params );
				
				$this->loadFromPost(); 
				$this->loadFromFiles( Array( 'image' ));
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			if( $this->image ) {
				$AR->uploadImage( $this->image );
			}
			
			$AR->params = $this->params ? implode( ',', $this->params ) : null;
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>