<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminChangeUserBalanceFormModel extends FormModelBase {
		public $id;
		public $user_login;
		public $direction;
		public $value;
		function getARClassName() {
			return "UserModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'user_login' => 'User',
				'value' => 'Sum',
			);
		}
		function rules() {
			return Array(
				Array( 'id, value', 'required' ),
				Array( 'value', 'numerical', 'min' => 0 ),
				Array( 'direction', 'in', 'range' => Array( 'up', 'down' )),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( !$id ) return false;
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$value = $this->direction == 'up' ? $this->value : -$this->value;
						
			$ARChange = UserBalanceChangeModel::instance( $this->id, $value );
			$ARChange->save();
			
			$AR->balance += $value;
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>