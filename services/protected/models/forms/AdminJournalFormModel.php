<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminJournalFormModel extends FormModelBase {
		public $id;
		public $number;
		public $date;
		public $slug;
		public $covers = Array();
		
		public $journals = Array();
		public $inner = Array();
		public $status;
		public $push_notification;
		public $hide_when_created;
		public $frees = Array();
		
		public $jname = Array();
		public $digest = Array();
		public $countPages = Array();
		public $countDownloads = Array();
		public $price = Array();
		public $description = Array();
		
		public $title = Array();
		public $metaTitle = Array();
		public $metaDescription = Array();
		public $metaKeywords = Array();
		public $shortDescription = Array();
		
		public $isDigest;
		
		public $excludeFieldsFromAr = array( 'covers', 'name', 'digest', 'countPages', 'countDownloads', 'frees', 'description', 'inner', 'title', 'metaTitle', 'metaDescription', 'metaKeywords', 'shortDescription', 'price' );
		
		public static function getTextFields(){
			return array(
				'jname' => Array( 'modelField' => 'name', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
				'digest' => Array( 'modelField' => 'digest', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
				'countPages' => Array( 'modelField' => 'countPages', 'type' => 'textFieldRow' ),
				'countDownloads' => Array( 'modelField' => 'countDownloads', 'type' => 'textFieldRow' ),
				'price' => Array( 'modelField' => 'price', 'type' => 'textFieldRow' ),
				'description' => Array( 'modelField' => 'description', 'type' => 'textAreaRow', 'class' => 'wFullWidth' ),
				'title' => Array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
				'metaTitle' => Array( 'modelField' => 'metaTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
				'metaDescription' => Array( 'modelField' => 'metaDescription', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
				'metaKeywords'  => Array( 'modelField' => 'metaKeywords', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
				'shortDescription' => Array( 'modelField' => 'shortDescription', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
			);
		}
		function getARClassName() {
			return "JournalModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'number' => 'Number',
				'date' => 'Date',
				'status' => 'Hide Issue',
				'push_notification' => 'Push Notification',
				'hide_when_created' => 'Hide when created',
				'isDigest' => 'Digest?',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "covers[{$language->id}]" ] = "Cover";
				$labels[ "jname[{$language->id}]" ] = "Name";
				$labels[ "digest[{$language->id}]" ] = "Digest";
				$labels[ "countPages[{$language->id}]" ] = "Pages";
				$labels[ "countDownloads[{$language->id}]" ] = "Downloads";
				$labels[ "journals[{$language->id}]" ] = "Journal";
				$labels[ "frees[{$language->id}]" ] = "Free";
				$labels[ "price[{$language->id}]" ] = "Price";
				$labels[ "description[{$language->id}]" ] = "Description";
				
				$labels[ "title[{$language->id}]" ] = "Page Title";
				$labels[ "metaTitle[{$language->id}]" ] = "Meta Title";
				$labels[ "metaDescription[{$language->id}]" ] = "Meta Description";
				$labels[ "metaKeywords[{$language->id}]" ] = "Meta Keywords";
				$labels[ "shortDescription[{$language->id}]" ] = "Short Description";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'number, date, slug', 'required' ),
				Array( 'date', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'covers', 'validateCovers' ),
				Array( 'countPages', 'validateCountPageses' ),
				Array( 'countDownloads', 'validateDownloadses' ),
				//Array( 'journals', 'validateJournals' ),
				Array( 'slug, jname, digest, title, metaTitle, metaDescription, metaKeywords, shortDescription', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'description, inner', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'slug', 'uniqueField' ),
				Array( 'isDigest', 'boolean' ),
			);
		}
		function validateCovers() {
			$AR = $this->getAR();
			$langId = LanguageModel::getCurrentLanguageID();
			if( !$AR->isNewRecord && empty($this->covers[ $langId ]) ) return;
			
			if( !empty($this->covers[ $langId ]) ) {
				if( in_array( $this->covers[ $langId ]->type, array('image/jpeg', 'image/jpg', 'image/png', 'image/gif') ) ) return;
				
				$this->addError( "covers", Yii::t( 'yii','{attribute} wrong file type, avalible jpeg jpg png gif', Array( 
					'{attribute}' => $this->getAttributeLabel( "covers[{$langId}]" ),
				)));
			}
			
			$this->addError( "covers", Yii::t( 'yii','{attribute} cannot be blank.', Array( 
				'{attribute}' => $this->getAttributeLabel( "covers[0]" ),
			)));
		}
		function validateCountPageses() {
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$this->countPages[$language->id] = $this->countPages[$language->id] ? abs((int)$this->countPages[$language->id]) : null;
			}
		}
		function validateDownloadses() {
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$this->countDownloads[$language->id] = $this->countDownloads[$language->id] ? abs((int)$this->countDownloads[$language->id]) : null;
			}
		}
		function validateJournals() {			
			$AR = $this->getAR();
			$langId = LanguageModel::getCurrentLanguageID();
			if( !$AR->isNewRecord && empty($this->journals[ $langId ]) ) return;
			
			if( !empty($this->journals[ $langId ]) ) {
				if( in_array( $this->journals[ $langId ]->type, array('application/pdf') ) ) return;
				
				$this->addError( "journals", Yii::t( 'yii','{attribute} wrong file type, avalible pdf', Array( 
					'{attribute}' => $this->getAttributeLabel( "journals[{$langId}]" ),
				)));
			}
			
			$this->addError( "journals", Yii::t( 'yii','{attribute} cannot be blank.', Array( 
				'{attribute}' => $this->getAttributeLabel( "journals[0]" ),
			)));
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();

				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					if( empty( $post[ 'price' ][ $language->id ])) $this->price[ $language->id ] = null;
				}
				
			}
		}
		function loadFromFiles( $loadFields = Array()) {
			parent::loadFromFiles( $loadFields );
			
			$keys = Array( 'covers', 'journals' );
			$languages = LanguageModel::getAll();
			foreach( $keys as $key ) {
				foreach( $languages as $language ){
					$file = CUploadedFile::getInstance( $this, "{$key}[{$language->id}]" );
					if( $file )	$this->{$key}[$language->id] = $file;
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					
					$this->covers[ $i18n->idLanguage ] = $i18n->srcCover;
					$this->inner[ $i18n->idLanguage ] = unserialize( $i18n->inner );
					$this->journals[ $i18n->idLanguage ] = $i18n->srcJournal;
					$this->frees[ $i18n->idLanguage ] = $i18n->free;
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
					}
				}
								
				return true;
			}
			return false;
		}

		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					
					$arrToSave = Array(
						'cover' => @$this->covers[ $language->id ],
						'journal' => @$this->journals[ $language->id ],
						'free' => @$this->frees[ $language->id ],
						'inner' => serialize( json_decode( ( stripslashes( @$this->inner[ $language->id ] ) ), true) ),
					);
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
					}
					$arrToSave['countDownloads'] = $this->countDownloads[ $language->id ] ?: 0;
					$i18ns[ $language->id ] = (object)$arrToSave;
					
				}
				
				$AR->setI18Ns( $i18ns );
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				
				
				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>
