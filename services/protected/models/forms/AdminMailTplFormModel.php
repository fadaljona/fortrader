<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminMailTplFormModel extends FormModelBase {
		public $id;
		public $key;
		public $subjects = Array();
		public $messages = Array();
		function getARClassName() {
			return "MailTplModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'key' => 'Key',
			);
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "subjects[{$language->id}]" ] = "Subject";
				$labels[ "messages[{$language->id}]" ] = "Message";
			}
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'key', 'required' ),
				Array( 'key', 'length', 'max' => CommonLib::maxByte ),
				Array( 'subjects', 'checkLensSubjects' ),
				Array( 'messages', 'checkLensMessages' ),
				Array( 'key', 'uniqueField' ),
			);
		}
		function checkLensSubjects() {
			$NSi18n = $this->getNSi18n();
			$max = CommonLib::maxWord;
			foreach( $this->subjects as $idLanguage=>$subject ) {
				if( mb_strlen( $subject ) > $max ) {
					$this->addError( "subjects", Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => Yii::t( $NSi18n, 'Subject' ),
						'{max}' => $max,
					)));
				}
			}
		}
		function checkLensMessages() {
			$NSi18n = $this->getNSi18n();
			$max = CommonLib::maxWord;
			foreach( $this->messages as $idLanguage=>$message ) {
				if( mb_strlen( $message ) > $max ) {
					$this->addError( "messages", Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => Yii::t( $NSi18n, 'Message' ),
						'{max}' => $max,
					)));
				}
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR( null, null, Array( 'values' ));
				
				foreach( $AR->i18ns as $i18n ) {
					$this->subjects[ $i18n->idLanguage ] = $i18n->subject;
					$this->messages[ $i18n->idLanguage ] = $i18n->message;
				}
				
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'subject' => $this->subjects[ $language->id ],
						'message' => $this->messages[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>