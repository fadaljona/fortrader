<?
	Yii::import( 'models.base.FormModelBase' );

	final class ResetUserPasswordReguestFormModel extends FormModelBase {
		public $email;
		protected function getSourceAttributeLabels() {
			return Array(
				'email' => 'Email or login',
			);
		}
		function rules() {
			return Array(
				Array( 'email', 'required' ),
				//Array( 'email', 'email' ),
				Array( 'email', 'validateEmail' ),
			);
		}
		function validateEmail() {
			if( !$this->hasErrors()) {
				$exists = UserModel::model()->exists( Array(
					'condition' => " 
						`t`.`user_email` = :email
						OR `t`.`user_login` = :email
					",
					'params' => Array(
						':email' => $this->email
					),
				));
				if( !$exists ) {
					$this->addI18NError( 'email', 'Wrong {attribute}!', Array( '{attribute}' => $this->getAttributeLabel( 'email' )));
				}
			}
		}
		function load( $id = 0 ) {
			$this->loadFromPost();
			return true;
		}
	}

?>