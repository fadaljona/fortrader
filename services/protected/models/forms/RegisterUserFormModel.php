<?
	Yii::import( 'models.base.FormModelBase' );

	final class RegisterUserFormModel extends FormModelBase {
		public $user_login;
		public $user_email;
		public $user_pass;
		public $user_pass_repeat;
		public $agree;
		public $sex;
		function getARClassName() {
			return "UserModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'user_login' => 'Login',
				'user_email' => 'E-mail',
				'user_pass' => 'Password',
				'user_pass_repeat' => 'Repeat password',
				'sex' => 'Sex',
			);
		}
		function rules() {
			$NSi18n = $this->getNSi18n();
			return Array(
				Array( 'user_login, user_email, user_pass, user_pass_repeat, sex', 'required' ),
				Array( 'user_login', 'length', 'max' => UserModel::MAXLoginLen ),
				Array( 'user_email', 'email' ),
				Array( 'user_email', 'length', 'max' => UserModel::MAXEMailLen ),
				Array( 'user_pass', 'length', 'min' => UserModel::MINPasswordLen ),
				Array( 'user_login', 'uniqueField', 'message' => 'Login is busy. Please choose another.' ),
				Array( 'user_email', 'uniqueField', 'message' => 'E-mail is busy. Please choose another.' ),
				Array( 'user_pass_repeat', 'compare', 'compareAttribute' => 'user_pass', 'message' => Yii::t( $NSi18n, 'Repeat password wrong!' )),
				Array( 'user_pass', 'checkPasswordLen' ),
				//Array( 'agree', 'compare', 'compareValue' => '1', 'message' => Yii::t( $NSi18n, 'You must accept the user agreement!' )),
				Array( 'user_login', 'validateLogin' ),
				Array( 'sex', 'in', 'range' => Array( 'Male', 'Female' )),
			);
		}
		function checkPasswordLen() {
			if( !$this->hasErrors()) {
				
				CommonLib::loadWp();
				
				$hash = wp_hash_password( $this->user_pass );
				if( strlen( $hash ) > UserModel::MAXPasswordLen ) {
					$this->addI18NError( 'user_pass', 'Password to long!' );
				}
			}
		}
		function validateLogin() {
			CommonLib::loadWp();
			if( !validate_username( $this->user_login )) {
				$this->addI18NError( 'user_login', '{attribute} uses illegal characters!', Array( '{attribute}' => $this->getAttributeLabel( 'user_login' )));
			}
		}
		function load( $id = 0 ) {
			if( $this->loadAR( $id )) {
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->user_login = sanitize_user( $this->user_login, true );
			$this->saveToAR( null, Array(), Array( 'user_pass', 'user_pass_repeat', 'agree' ));
			$AR->setPassword( $this->user_pass );
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>