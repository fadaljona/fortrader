<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUnsubscribeCommentFormModel extends FormModelBase {
		public $id;
		public $login;
		public $idMailingType;
		public $comment;
		public $status;
		protected $user;
		function getARClassName() {
			return "UnsubscribeCommentModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'login' => 'User',
				'idMailingType' => 'Type mailing',
				'comment' => 'Comment',
				'status' => 'Status',
			);
		}
		function rules() {
			return Array(
				Array( 'login, comment', 'required' ),
				Array( 'idMailingType, comment', 'required' ),
				Array( 'comment', 'length', 'max' => CommonLib::maxWord ),
				Array( 'idMailingType', 'existsIDModel', 'model' => 'MailingTypeModel' ),
				Array( 'login', 'validatelogin' ),
			);
		}
		function validatelogin() {
			if( !$this->hasErrors()) {
				$this->user = UserModel::model()->findByAttributes( Array( 'user_login' => $this->login ));
				if( !$this->user ) {
					$this->addI18NError( 'from', "Can't find User!" );
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'to', 'from' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( $AR->idUser ) {
					$user = UserModel::model()->findByPk( $AR->idUser );
					$this->login = $user->user_login;
				}
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'to', 'from' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->idUser = $this->user->ID;
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>