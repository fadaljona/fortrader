<?
Yii::import( 'models.base.SettingsFormModelBase' );

final class AdminRssFeedsSettingsFormModel extends SettingsFormModelBase{
	
	public $ogImage;
	
	public $title = Array();
	public $metaTitle = Array();
	public $metaDesc = Array();
	public $metaKeys = Array();
	public $shortDesc = Array();
	public $fullDesc = Array();
	
	public $textFields = array(

		'title' => 'textFieldRow',
		'metaTitle' => 'textFieldRow',
		'metaDesc' => 'textAreaRow',
		'metaKeys' => 'textAreaRow',
		'shortDesc' => 'textAreaRow',
		'fullDesc' => 'textAreaRow',

	);

	protected function getSourceAttributeLabels() {
		$labels = Array(
			'ogImage' => 'og Image',
		);
		return $this->setTextLabels( $labels );
	}
	function rules() {
		return Array(
			Array( 'ogImage', 'length', 'max' => CommonLib::maxByte ),
			Array( 'title, metaTitle, metaDesc, metaKeys', 'checkLens', 'max' => CommonLib::maxByte ),
			Array( 'shortDesc, fullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
		);
	}
	function loadAR( $pk = 0 ) {
		$AR = RssFeedsSettingsModel::getModel();
		$this->setAR( $AR );
		return true;
	}
}