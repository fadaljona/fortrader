<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminEATradeAccountFormModel extends FormModelBase {
		public $id;
		public $idEA;
		public $idVersion;
		public $idStatement;
		public $idTradeAccount;
		public $idEAMonitoringServer;
		public $planeDrow;
		public $idBroker;
		function getARClassName() {
			return "EATradeAccountModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'idEA' => 'EA',
				'idVersion' => 'Version',
				'idStatement' => 'Config',
				'idTradeAccount' => 'Trade account',
				'idEAMonitoringServer' => 'Monitoring server',
				'planeDrow' => 'Plane drow',
				'idBroker' => 'Sponsor',
			);
		}
		function rules() {
			return Array(
				Array( 'idEA, idVersion, idTradeAccount, idEAMonitoringServer', 'required' ),
				Array( 'idEA', 'existsIDModel', 'model' => 'EAModel' ),
				Array( 'idVersion', 'existsIDModel', 'model' => 'EAVersionModel' ),
				Array( 'idStatement', 'existsIDModel', 'model' => 'EAStatementModel' ),
				Array( 'idTradeAccount', 'existsIDModel', 'model' => 'TradeAccountModel' ),
				Array( 'idEAMonitoringServer', 'existsIDModel', 'model' => 'EAMonitoringServerModel' ),
				Array( 'idBroker', 'existsIDModel', 'model' => 'BrokerModel' ),
				Array( 'planeDrow', 'numerical' ),
			);
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( $this->planeDrow === '' ) $this->planeDrow = null;
				if( !$this->idBroker ) $this->idBroker = null;
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>