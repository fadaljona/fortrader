<?
	Yii::import( 'models.base.FormModelBase' );

	final class ContestMemberFormModel extends FormModelBase {
		public $id;
		public $idContest;
		public $accountNumber;
		public $investorPassword;
		public $idServer;
		public $servers;
		function getARClassName() {
			return "ContestMemberModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'accountNumber' => 'Account number',
				'investorPassword' => 'Investor password',
				'idServer' => 'Server',
			);
		}
		function rules() {
			return Array(
				Array( 'accountNumber, idServer', 'required' ),
				Array( 'idServer', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'accountNumber', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => CommonLib::maxUInt ),
				Array( 'idServer', 'existsIDModel', 'model' => 'ServerModel' ),
				Array( 'investorPassword', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		static function getServers( $idContest ) {
			$servers = Array();
			
			if( $idContest ){
				$models = ServerModel::model()->findAll( Array(
					'condition' => "
						EXISTS (
							SELECT			*
							FROM 			`{{server_to_contest}}`
							WHERE			`idServer` = `t`.`id`
								AND			`idContest` = :idContest
							LIMIT			1
						)
					",
					'params' => Array(
						':idContest' => $idContest,
					),
					'order' => "`t`.`name`",
				));
			}else{
				$models = ServerModel::model()->findAll( Array(
					'order' => "`t`.`name`",
				));
			}
			
			
			foreach( $models as $model ) {
				$servers[ $model->id ] = $model->name;
			}
			
			return $servers;
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'investorPassword', 'servers' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$this->servers = self::getServers( $AR->idContest );
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'idContest', 'investorPassword', 'servers' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( strlen( $this->investorPassword )) {
					$AR->investorPassword = $this->investorPassword;
				}
				
				return true;
			}
			return false;
		}
		
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( !$id ) return false;
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
						
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>