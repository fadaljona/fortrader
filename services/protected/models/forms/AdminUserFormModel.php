<?
	Yii::import( 'models.base.FormModelBase' );
	Yii::import( 'components.MailerComponent' );

	final class AdminUserFormModel extends FormModelBase {
		public $ID;
		public $user_login;
		public $user_nicename;
		public $display_name;
		public $user_email;
		public $user_pass;
		public $language;
		public $idBroker;
		public $groups = Array();
		public $sendNotice = true;
		public $mailingTypesAccept = Array();
		function getARClassName() {
			return "UserModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'user_login' => 'Login',
				'user_nicename' => 'Nicename',
				'display_name' => 'Display name',
				'user_email' => 'E-mail',
				'user_pass' => 'Password',
				'language' => 'Language',
				'idBroker' => 'Representative',
				'sendNotice' => 'Send notice',
			);
		}
		function rules() {
			return Array(
				Array( 'user_login, user_nicename, user_email', 'required' ),
				Array( 'user_login, user_nicename, display_name, user_email, user_pass', 'length', 'max' => CommonLib::maxByte ),
				Array( 'user_email', 'email' ),
				Array( 'user_login', 'uniqueField', 'message' => 'Login is busy. Please choose another.' ),
				Array( 'user_email', 'uniqueField', 'message' => 'E-mail is busy. Please choose another.' ),
				Array( 'user_nicename', 'uniqueField', 'message' => 'Nicename is busy. Please choose another.' ),
				Array( 'display_name', 'uniqueField', 'message' => 'Display name is busy. Please choose another.' ),
				Array( 'user_pass', 'requiredIfNewRecord' ),
				Array( 'idBroker', 'existsIDModel', 'model' => 'BrokerModel' ),
			);
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( empty( $post[ 'groups' ])) $this->groups = Array();
				$this->groups  = array_filter( (array)$this->groups, Array( 'CommonLib', 'isID' ));
				if( !$this->idBroker ) $this->idBroker = null;
				if( empty( $post[ 'mailingTypesAccept' ])) $this->mailingTypesAccept = Array();
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array() ) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$mailingTypes = MailingTypeModel::model()->findAll();
				$mailingTypesMute = CommonLib::slice( $AR->mailingTypesMute, 'idType' );
				
				foreach( $mailingTypes as $mailingType ) {
					if( !in_array( $mailingType->id, $mailingTypesMute )) {
						$this->mailingTypesAccept[] = $mailingType->id;
					}
				}
			}
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				
				$mailingTypes = MailingTypeModel::model()->findAll();
				$mailingTypesAccept = is_array( $this->mailingTypesAccept ) ? $this->mailingTypesAccept : Array();
				$mailingTypesMute = Array();
				
				foreach( $mailingTypes as $mailingType ) {
					if( !in_array( $mailingType->id, $mailingTypesAccept )) {
						$mailingTypesMute[] = $mailingType->id;
					}
				}
				
				
				$AR->setMailingTypesMute( $mailingTypesMute );
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['ID']) $id = (int)@$post['ID'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR( null, Array(), Array( 'user_pass', 'groups' ));
				$this->groups = $AR->getIDsGroups();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array( 'user_pass' ));
			if( strlen( $this->user_pass )) $AR->setPassword( $this->user_pass );
			
			if( $this->saveAR( false )) {
				$AR->setGroups( $this->groups );
				if( $AR->beenNewRecord and $this->sendNotice ) {
					AdminUserFormModelUtils::sendMail( $AR, $this->user_pass );
				}
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}
	
	abstract class AdminUserFormModelUtils {
		private static function getMailer() {
			$factoryMailer =  new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			return $mailer;
		}
		private static function getMailTpl() {
			$key = 'Message to user about manual registration';
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			if( !$mailTpl ) self::throwI18NException( "Can't load mail template! ({key})", Array( '{key}' => $key ));
			return $mailTpl;
		}
		private static function getMailTplI18N( $mailTpl ) {
			$i18n = $mailTpl->currentLanguageI18N;
			if( $i18n and strlen( $i18n->message )) return $i18n;
			foreach( $mailTpl->i18ns as $i18n ) if( strlen( $i18n->message )) return $i18n;

			self::throwI18NException( "Can't load i18n for mail template!" );
		}
		private static function renderMessage( $user, $password, $message ) {
			return strtr( $message, Array(
				'%username%' => $user->user_login,
				'%password%' => $password,
			));
		}
		static function sendMail( $user, $password ) {
			$mailer = self::getMailer();
			$mailTpl = self::getMailTpl();
			$i18n = self::getMailTplI18N( $mailTpl );
						
			$message = self::renderMessage( $user, $password, $i18n->message );
						
			$mailer->addAddress( $user->user_email, $user->showName );
			$mailer->Subject = $i18n->subject;
			if( substr_count( $message, '<' )) {
				$message = CommonLib::nl2br( $message, true );
				$mailer->MsgHTML( $message );
			}
			else{
				$mailer->Body = $message;
			}
			
			$result = $mailer->send();
			$error = $result ? null : $mailer->ErrorInfo;
			if( $error ) throw new Exception( $error );
		}
	}

?>