<?
	Yii::import( 'models.base.FormModelBase' );
	Yii::import( 'components.UpdateBrokerSmStatsComponent' );

	final class AdminBrokerFormModel extends FormModelBase {
		public $id;
		public $showInRating;
		public $showOnHome;
		public $officialNames = Array();
		public $brandNames = Array();
		public $shortNames = Array();
		
		public $metaTitles = Array();
		public $metaDescs = Array();
		public $metaKeyss = Array();
		
		public $shortDesc = array();
		public $fullDesc = array();
		
		public $slug;
		public $type;
		
		public $contract;
		public $earlyClosure;
		public $tradeOnWeekends;
		public $binnaryDemoAccount;
		public $maxProfits = Array();
		
		public $nameImage;
		public $homeImage;
		public $year;
		public $idCountry;
		public $minDeposit;
		public $idsRegulators;
		public $maxLeverage;
		public $AMs = Array();
		public $demoAccounts = Array();
		public $idsTradePlatforms;
		public $idsIOs;
		public $idsInstruments;
		
		public $idsBinaryTypes;
		public $idsBinaryAssets;
		
		public $conditions;
		public $terms;
		public $site;
		public $openAccount;
		public $openDemoAccount;
        public $idSupportUser;
        
        public $toCryptoCurrencies;

		function getARClassName() {
			return "BrokerModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'showInRating' => 'Show in rating',
				'showOnHome' => 'Show on home page',
				'nameImage' => 'Upload logo',
				'homeImage' => 'Image for home page',
				'year' => 'Year',
				'idCountry' => 'Country',
				'minDeposit' => 'Min deposit',
				'idsRegulators' => 'Regulators',
				'maxLeverage' => 'Max leverage',
				'conditions' => 'Conditions',
				'terms' => 'Terms',
				'site' => 'Site',
				'openAccount' => 'Open account',
				'openDemoAccount' => 'Open demo account',
				'idSupportUser' => 'Support user',
				'slug' => 'Broker slug',
				
				'contract' => 'Contract',
				'earlyClosure' => 'Early closure',
				'tradeOnWeekends' => 'Trrade on weekends',
                'binnaryDemoAccount' => 'Binnary demo account',
                
                'toCryptoCurrencies' => 'To crypto currencies page?',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "officialNames[{$language->id}]" ] = "Official name";
				$labels[ "brandNames[{$language->id}]" ] = "Brand name";
				$labels[ "shortNames[{$language->id}]" ] = "Short name";
				$labels[ "AMs[{$language->id}]" ] = "AM";
				$labels[ "demoAccounts[{$language->id}]" ] = "Demo account";
				
				$labels[ "metaTitles[{$language->id}]" ] = "Meta title";
				$labels[ "metaDescs[{$language->id}]" ] = "Meta description";
				$labels[ "metaKeyss[{$language->id}]" ] = "Meta keys";
				
				$labels[ "shortDesc[{$language->id}]" ] = "Short description";
				$labels[ "fullDesc[{$language->id}]" ] = "Full description";
				
				$labels[ "maxProfits[{$language->id}]" ] = "Max profit";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'showInRating, year, idCountry', 'required' ),
				Array( 'showInRating, showOnHome, earlyClosure, tradeOnWeekends, binnaryDemoAccount', 'in', 'range' => Array( 0, 1 )),
				Array( 'type', 'in', 'range' => Array( 'broker','binary' )),
				Array( 'nameImage, homeImage', 'file', 'allowEmpty' => true, 'types' => Array( 'jpg', 'jpeg', 'png', 'gif' )),
				Array( 'year', 'numerical', 'integerOnly' => true, 'min' => 1971 ),
				Array( 'idCountry', 'existsIDModel', 'model' => 'CountryModel' ),
				Array( 'idSupportUser', 'existsIDModel', 'model' => 'UserModel' ),
				Array( 'minDeposit, contract', 'numerical', 'min' => 0 ),
				Array( 'maxLeverage, conditions, terms, site, openAccount, openDemoAccount, slug', 'length', 'max' => CommonLib::maxByte ),
				Array( 'officialNames', 'validateOfficialNames' ),
				Array( 'officialNames, brandNames, shortNames, AMs, demoAccounts', 'checkLens', 'max' => CommonLib::maxByte ),
				//Array( 'officialNames, brandNames, shortNames', 'checkExists' ),
				Array( 'metaTitles, metaDescs, metaKeyss, maxProfits', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'shortDesc, fullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'slug', 'uniqueField', 'message' => 'Slug is busy. Please choose another.' ),
                Array( 'slug', 'required' ),
                array('toCryptoCurrencies', 'boolean'),
			);
		}
		function validateOfficialNames() {
			foreach( $this->officialNames as $idLanguage=>$name ) {
				if( strlen( $name )) return;
			}
			$NSi18n = $this->getNSi18n();
			$this->addError( "officialNames", Yii::t( 'yii','{attribute} cannot be blank.', Array( '{attribute}' => Yii::t( $NSi18n, 'Official name' ))));
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function checkExists( $field ) {
			if( !$this->hasErrors()) {
				$AR = $this->getAR();
				foreach( $this->$field as $idLanguage=>$value ) {
					if( strlen( $value )) {
						$c = new CDbCriteria();
						$keyI18N = preg_replace( "#s$#", '', $field );
						$c->with[ 'i18ns' ] = Array(
							'joinType' => 'INNER JOIN',
							'condition' => " 
								`i18nsBroker`.`{$keyI18N}` = :{$keyI18N}
							",
							'params' => Array(
								":{$keyI18N}" => $value,
							),
						);
						
						if( !$AR->isNewRecord ) {
							$c->addCondition( "`t`.`id` != :pk" );
							$c->params[ ':pk' ] = $AR->id;
						}
						
						$exists = BrokerModel::model()->exists( $c );
						if( $exists ) {
							$this->addError( $field, Yii::t( 'yii', '{attribute} "{value}" has already been taken.', Array(
								'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
								'{value}' => CHtml::encode( $value ),
							)));
						}
					}
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				foreach( Array( 'idsRegulators', 'idsTradePlatforms', 'idsIOs', 'idsInstruments', 'idsBinaryAssets', 'idsBinaryTypes' ) as $field ) {
					if( empty( $post[ $field ])) $this->$field = Array();
					$this->$field  = array_filter( (array)$this->$field, Array( 'CommonLib', 'isID' ));	
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'officialNames', 'brandNames', 'shortNames', 'idsRegulators', 'AMs', 'demoAccounts', 'metaTitles', 'metaDescs', 'metaKeyss', 'maxProfits', 'shortDesc', 'fullDesc' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->officialNames[ $i18n->idLanguage ] = $i18n->officialName;
					$this->brandNames[ $i18n->idLanguage ] = $i18n->brandName;
					$this->shortNames[ $i18n->idLanguage ] = $i18n->shortName;
					$this->AMs[ $i18n->idLanguage ] = $i18n->AM;
					$this->demoAccounts[ $i18n->idLanguage ] = $i18n->demoAccount;
					
					$this->metaTitles[ $i18n->idLanguage ] = $i18n->metaTitle;
					$this->metaDescs[ $i18n->idLanguage ] = $i18n->metaDesc;
					$this->metaKeyss[ $i18n->idLanguage ] = $i18n->metaKeys;
					
					$this->shortDesc[ $i18n->idLanguage ] = $i18n->shortDesc;
					$this->fullDesc[ $i18n->idLanguage ] = $i18n->fullDesc;
					
					$this->maxProfits[ $i18n->idLanguage ] = $i18n->maxProfit;
				}
				
				$this->idsRegulators = $AR->getIDsRegulators();
				$this->idsTradePlatforms = $AR->getIDsTradePlatforms();
				$this->idsIOs = $AR->getIDsIOs();
				$this->idsInstruments = $AR->getIDsInstruments();
			
				$this->idsBinaryTypes = $AR->getIDsBinaryTypes();
				$this->idsBinaryAssets = $AR->getIDsBinaryAssets();
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'officialName' => $this->officialNames[ $language->id ],
						'brandName' => $this->brandNames[ $language->id ],
						'shortName' => $this->shortNames[ $language->id ],
						'AM' => $this->AMs[ $language->id ],
						'demoAccount' => $this->demoAccounts[ $language->id ],
						
						'metaTitle' => $this->metaTitles[ $language->id ],
						'metaDesc' => $this->metaDescs[ $language->id ],
						'metaKeys' => $this->metaKeyss[ $language->id ],
						
						'shortDesc' => $this->shortDesc[ $language->id ],
						'fullDesc' => $this->fullDesc[ $language->id ],
						
						'maxProfit' => $this->maxProfits[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );

				$AR->setRegulators( $this->idsRegulators );
				$AR->setTradePlatforms( $this->idsTradePlatforms );
				$AR->setIOs( $this->idsIOs );
				$AR->setInstruments( $this->idsInstruments );
				
				$AR->setBinaryTypes( $this->idsBinaryTypes );
				$AR->setBinaryAssets( $this->idsBinaryAssets );
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles( Array( 'nameImage', 'homeImage' ));
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array( 'officialNames', 'brandNames', 'shortNames', 'idsRegulators', 'AMs', 'demoAccounts', 'nameImage', 'homeImage', 'metaTitles', 'metaDescs', 'metaKeyss', 'maxProfits', 'shortDesc', 'fullDesc' ));
			if( $this->nameImage ) {
				$AR->uploadImage( $this->nameImage );
			}
			
			if( $this->homeImage ) {
				$AR->uploadHomeImage( $this->homeImage );
			}
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			
			return false;
		}
	}

?>
