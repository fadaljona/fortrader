<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminMetalRatesFormModel extends FormModelBase {
		public $id;
		public $toInformer;
		public $code;
		public $symbol;
		
		public $title;
		
		
		public $excludeFieldsFromAr = array( 'title' );
		
		public static function getTextFields(){
			return array(
				'title' => Array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
			);
		}
		function getARClassName() {
			return "MetalRatesModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'toInformer' => 'To informer by defautl',
				'code' => 'Code',
			);	
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){			
				$labels[ "title[{$language->id}]" ] = "Title";
			}
			
			return $labels;
		}
		function rules() {
			$rules = Array(
				Array( 'title', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'title', 'notEmptyCurrentLang' ),
				Array( 'toInformer', 'boolean'),
				Array( 'code', 'length', 'max' => CommonLib::maxByte ),
			);

			
			return $rules;
		}
		function notEmptyCurrentLang( $field, $params ) {
			$NSi18n = $this->getNSi18n();	
			$currentLangId = LanguageModel::getCurrentLanguageID();
			
			if( mb_strlen( $this->{$field}[$currentLangId] ) == 0 ) {
				$this->addError( "{$field}[{$currentLangId}]", Yii::t( '*','{attribute} can not be empty.', Array( 
					'{attribute}' => $this->getAttributeLabel( "{$field}[{$currentLangId}]" ),
				)));
			}
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}

		
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
					}
				}
				return true;
			}
			return false;
		}
		
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( '' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
			}
		}

		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$arrToSave = Array();
					foreach( $this->textFields as $formFieldName => $settings ){
						$arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
					}
					$i18ns[ $language->id ] = (object)$arrToSave;
				}
				$AR->setI18Ns( $i18ns );
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				
				
				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>