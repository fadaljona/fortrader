<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminMailingTypeFormModel extends FormModelBase {
		public $id;
		public $names;
		function getARClassName() {
			return "MailingTypeModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array();
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "names[{$language->id}]" ] = "Title";
			}
						
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'names', 'validateNames' ),
				Array( 'names', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
		function validateNames() {
			foreach( $this->names as $idLanguage=>$name ) {
				if( strlen( $name )) return;
			}
			$NSi18n = $this->getNSi18n();
			$this->addError( "names", Yii::t( 'yii','{attribute} cannot be blank.', Array( '{attribute}' => Yii::t( $NSi18n, 'Title' ))));
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->names[ $i18n->idLanguage ] = $i18n->name;
				}
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'name' => $this->names[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>