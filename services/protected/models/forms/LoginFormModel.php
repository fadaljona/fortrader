<?
	Yii::import( 'models.base.FormModelBase' );
	Yii::import( 'components.sys.UserIdentity' );
	
	final class LoginFormModel extends FormModelBase {
	    public $login;
		public $password;
		public $remember;
		private $identity;
		
		protected function getSourceAttributeLabels() {
			return Array(
				'login' => 'Login or Email',
				'password' => 'Password',
				'remember' => 'Remember me',
			);
		}
		function rules() {
			return Array(
				Array( 'login, password', 'required' ),
				Array( 'password', 'authenticate' ),
			);
		}
		function detIdentity() {
			$this->identity = new UserIdentity( $this->login, $this->password );
		}
		function setIdentity( $identity ) {
			$this->identity = $identity;
		}
		function getIdentity() {
			return $this->identity;
		}
		function authenticate( $attribute, $params ) {
			if( !$this->hasErrors()) {
				$this->detIdentity();
				$identity = $this->getIdentity();
				if( !$identity->authenticate()) {
					$this->addI18NError( 'password', 'Invalid login or password' );
				}
			}
		}
		function login() {
			$identity = $this->getIdentity();
			if( $identity ) {
				if( $identity->isRoot()) $this->remember = false;
				$authenticateRememberDays = Yii::App()->params[ 'authenticateRememberDays' ];
				$duration = $this->remember ? $authenticateRememberDays * 60*60*24 : 0;
				Yii::app()->user->login( $identity, $duration );
			}
		}
	}

?>