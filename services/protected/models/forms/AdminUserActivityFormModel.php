<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUserActivityFormModel extends FormModelBase {
		public $id;
		public $user;
		public $icon;
		public $messages = Array();
		public $count;
		public $url;
		protected $_user;
		function getARClassName() {
			return "UserActivityModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'user' => 'User',
				'icon' => 'Icon',
				'message' => 'Message',
				'count' => 'Count',
				'url' => 'URL',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "messages[{$language->id}]" ] = "Message";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'user, count', 'required' ),
				Array( 'count', 'numerical' ),
				Array( 'user', 'validateUser' ),
				Array( 'messages', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'icon, url', 'length', 'max' => CommonLib::maxByte ),
			);
		}
		function validateUser() {
			if( !$this->hasErrors()) {
				$this->_user = UserModel::model()->findByAttributes( Array( 'user_login' => $this->user ));
				if( !$this->_user ) {
					$this->addI18NError( 'user', "Can't find User!" );
				}
			}
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( !strlen( $this->icon )) $this->icon = null;
				if( !strlen( $this->url )) $this->url = null;
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'from' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( $AR->idUser ) {
					$user = UserModel::model()->findByPk( $AR->idUser );
					$this->user = $user->user_login;
				}
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->messages[ $i18n->idLanguage ] = $i18n->message;
				}
				
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'from' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->idUser = $this->_user->ID;
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'message' => $this->messages[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );				
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>