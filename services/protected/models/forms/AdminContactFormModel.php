<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminContactFormModel extends FormModelBase {
		public $id;
		public $image;
		public $deleteImage;
		public $email;
		public $skype;
		public $posts = Array();
		public $names = Array();
		public $descs = Array();
		public $position;
		public $reciveFeedback;
		public $hidden;
		function getARClassName() {
			return "ContactModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'image' => 'Foto',
				'deleteImage' => 'Delete foto',
				'email' => 'Email',
				'skype' => 'Skype',
				'position' => 'Position',
				'reciveFeedback' => 'Recive feedback',
				'hidden' => 'Hidden',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "posts[{$language->id}]" ] = "Post";
				$labels[ "names[{$language->id}]" ] = "Name";
				$labels[ "descs[{$language->id}]" ] = "Description";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'image', 'file', 'allowEmpty' => true, 'types' => Array( 'jpg', 'jpeg', 'png', 'gif' )),
				Array( 'email, skype', 'length', 'max' => CommonLib::maxByte ),
				Array( 'posts, names', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'descs', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( $post[ 'position' ] === "" ) $this->position = null;
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'posts', 'names', 'descs' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->posts[ $i18n->idLanguage ] = $i18n->post;
					$this->names[ $i18n->idLanguage ] = $i18n->name;
					$this->descs[ $i18n->idLanguage ] = $i18n->desc;
				}
				
				$this->image = $AR->getSrcImage();
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'post' => $this->posts[ $language->id ],
						'name' => $this->names[ $language->id ],
						'desc' => $this->descs[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );

				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles( Array( 'image' ));
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			if( $this->image ) {
				$AR->uploadImage( $this->image );
			}
			if( $this->deleteImage ) {
				$AR->deleteImage();
			}
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>