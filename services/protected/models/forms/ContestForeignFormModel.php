<?
	Yii::import( 'models.base.FormModelBase' );

	final class ContestForeignFormModel extends FormModelBase {
		public $id;
		public $prize;
		public $begin;
		public $approved;
		
		public $title;
		public $sponsor;
		public $link;
		
		public $excludeFieldsFromAr = array( 'title', 'sponsor', 'link' );
		
		public static function getTextFields(){
			return array(
				'title' => Array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
				'sponsor' => Array( 'modelField' => 'sponsor', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
				'link' => Array( 'modelField' => 'link', 'type' => 'textFieldRow', 'class' => 'wFullWidth' ),
			);
		}
		function getARClassName() {
			return "ContestForeignModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'prize' => 'Prize amount',
				'begin' => 'The date of the beginning',
				'approved' => 'Approve?',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){			
				$labels[ "title[{$language->id}]" ] = "Contest name";
				$labels[ "sponsor[{$language->id}]" ] = "Sponsor";
				$labels[ "link[{$language->id}]" ] = "Link";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'prize, begin, approved, title, sponsor, link', 'required' ),
				Array( 'prize', 'numerical', 'integerOnly'=>true, 'min' => 1 ),
				Array( 'begin', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'approved', 'boolean' ),

				Array( 'title, sponsor, link', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'title, sponsor, link', 'notEmptyCurrentLang' ),
			);
		}
		function notEmptyCurrentLang( $field, $params ) {
			$NSi18n = $this->getNSi18n();	
			$currentLangId = LanguageModel::getCurrentLanguageID();
			
			if( mb_strlen( $this->{$field}[$currentLangId] ) == 0 ) {
				$this->addError( "{$field}[{$currentLangId}]", Yii::t( '*','{attribute} can not be empty.', Array( 
					'{attribute}' => $this->getAttributeLabel( "{$field}[{$currentLangId}]" ),
				)));
			}
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();

				if( !Yii::app()->user->checkAccess('contestControl') ){
					$this->approved = 0;
				}else{
					$this->approved = 1;
				}
				
			}
		}

		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
					}
				}
								
				return true;
			}
			return false;
		}

		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					
					$arrToSave = Array();
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
					}
					$i18ns[ $language->id ] = (object)$arrToSave;
				}
				
				$AR->setI18Ns( $i18ns );
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				
				
				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>