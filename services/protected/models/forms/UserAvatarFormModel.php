<?
	Yii::import( 'models.base.FormModelBase' );

	final class UserAvatarFormModel extends FormModelBase {
		public $ID;
		public $avatar;
		function getARClassName() {
			return "UserModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'avatar' => 'Avatar',
			);
		}
		function rules() {
			return Array(
				Array( 'avatar', 'file', 'types' => Array( 'jpg', 'jpeg', 'png', 'gif' )),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( !$id ) $id = (int)@$post['ID'];
			if( !$id ) return false;
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromFiles( Array( 'avatar' ));
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$AR->uploadAvatar( $this->avatar );
			
			return $AR->getPrimaryKey();
		}
	}

?>