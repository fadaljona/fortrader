<?
Yii::import( 'models.base.SettingsFormModelBase' );

final class AdminInformersSettingsFormModel extends SettingsFormModelBase{
	
	public $ogImage;
	
	public $convertFrom;
	public $convertTo;
	public $convertFromVal;
	

	
	public $title = Array();
	public $catsListTitle = Array();
	public $metaTitle = Array();
	public $metaDesc = Array();
	public $metaKeys = Array();
	public $shortDesc = Array();
	public $fullDesc = Array();
	
	public $infoLinkText = Array();
	public $infoLink = Array();
	
	public $textFields = array(

		'title' => 'textFieldRow',
		'catsListTitle' => 'textFieldRow',
		'metaTitle' => 'textFieldRow',
		'metaDesc' => 'textAreaRow',
		'metaKeys' => 'textAreaRow',
		'shortDesc' => 'textAreaRow',
		'fullDesc' => 'textAreaRow',
		
		'infoLinkText' => 'textFieldRow',
		'infoLink' => 'textFieldRow',

	);

	protected function getSourceAttributeLabels() {
		$labels = Array(
			'ogImage' => 'Category list Og Image',
			'convertFrom' => 'Currency convert from',
			'convertTo' => 'Currency convert to',
			'convertFromVal' => 'Currency convert from value',
		);
		return $this->setTextLabels( $labels );
	}
	function rules() {
		return Array(
			Array( 'ogImage', 'length', 'max' => CommonLib::maxByte ),
			Array( 'title, catsListTitle, metaTitle, metaDesc, metaKeys, infoLinkText, infoLink', 'checkLens', 'max' => CommonLib::maxByte ),
			Array( 'shortDesc, fullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
			Array( 'convertFrom, convertTo', 'numerical', 'min' => 0, 'integerOnly' => true ),
			Array( 'convertFromVal', 'numerical', 'min' => 1, 'integerOnly' => true ),
			Array( 'convertFrom, convertTo, convertFromVal', 'required' ),
			Array('convertFrom', 'checkFromToCurrencies'),
		);
	}
	function checkFromToCurrencies($attribute,$params){
            if( $this->convertFrom == $this->convertTo )
                $this->addError('convertFrom', Yii::t( '*','Choose another convertFrom or convertTo') );
         
	}
	function loadAR( $pk = 0 ) {
		$AR = InformersSettingsModel::getModel();
		$this->setAR( $AR );
		return true;
	}
}