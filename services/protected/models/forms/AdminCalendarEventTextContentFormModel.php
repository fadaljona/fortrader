<?
	Yii::import( 'models.base.AdminTextContentFormModelBase' );

	final class AdminCalendarEventTextContentFormModel extends AdminTextContentFormModelBase {
		
		protected $textModelName = 'CalendarEvent2I18NModel';
		protected $idLanguageFieldName = 'idLanguage';
		protected $idTextModelFieldName = 'indicator_id';
		protected $idArModelFieldName = 'indicator_id';
		
		public $id;
		
		# texts
		public $indicator_name = Array();
		public $pageTitle = Array();
		public $metaTitle = Array();
		public $metaDesc = Array();
		public $metaKeys = Array();
		public $chartTitle = Array();
		public $chartSubTitle = Array();
		
		public $indicator_description = Array();
		public $fullDesc = Array();
		
		public $textFields = array(
			'indicator_name' => 'textFieldRow',
			'pageTitle' => 'textFieldRow',
			'metaTitle' => 'textFieldRow',
			'metaDesc' => 'textFieldRow',
			'metaKeys' => 'textFieldRow',
			'chartTitle' => 'textFieldRow',
			'chartSubTitle' => 'textFieldRow',
			'indicator_description' => 'textAreaRow',
			'fullDesc' => 'textAreaRow',
		);
		
		function getARClassName() {
			return "CalendarEvent2Model";
		}
		
		function rules() {
			return Array(

				Array( 'indicator_name', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'indicator_description', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'pageTitle, metaTitle, metaDesc, metaKeys, chartTitle, chartSubTitle', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'fullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
	}

?>
