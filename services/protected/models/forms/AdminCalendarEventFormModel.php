<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminCalendarEventFormModel extends FormModelBase {
		public $id;
		public $currency;
		
		public $indicators = Array();
		public $descriptions = Array();

		public $indicator_id;
		protected $old_indicator_id;
		
		public $pageTitle = Array();
		public $metaTitle = Array();
		public $metaDesc = Array();
		public $metaKeys = Array();
		public $fullDesc = Array();
		
		public $chartTitle = Array();
		public $chartSubTitle = Array();
		
		public $slug;
		
		function getARClassName() {
			return "CalendarEvent2Model";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'currency' => 'Currency',
				'slug' => 'Slug', 
			);
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "indicators[{$language->id}]" ] = "Title event";
				$labels[ "descriptions[{$language->id}]" ] = "Description";
				
				$labels[ "pageTitle[{$language->id}]" ] = "Page title";
				$labels[ "metaTitle[{$language->id}]" ] = "Meta title";
				$labels[ "metaDesc[{$language->id}]" ] = "Meta desc";
				$labels[ "metaKeys[{$language->id}]" ] = "Meta keys";
				$labels[ "fullDesc[{$language->id}]" ] = "Full desc";
				
				$labels[ "chartTitle[{$language->id}]" ] = "Chart title";
				$labels[ "chartSubTitle[{$language->id}]" ] = "Chart sub title";
			}
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'currency, slug', 'length', 'max' => CommonLib::maxByte ),
				Array( 'slug', 'required' ),
				Array( 'slug', 'uniqueField' ),
				Array( 'indicator_id', 'numerical', 'integerOnly' => true, 'min' => 0 ),
				Array( 'indicators', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'descriptions', 'checkLens', 'max' => CommonLib::maxWord ),
				
				Array( 'pageTitle, metaTitle, metaDesc, metaKeys, chartTitle, chartSubTitle', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'fullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
		function validateIndicators() {
			foreach( $this->indicators as $idLanguage=>$name ) {
				if( !strlen( $name )) {
					$NSi18n = $this->getNSi18n();
					$this->addError( "indicators", Yii::t( 'yii','{attribute} cannot be blank.', Array( '{attribute}' => Yii::t( $NSi18n, 'Title event' ))));
				}
			}
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				
				if( empty( $post[ 'currency' ])) $this->currency = null;
				
				$fields = "indicators, descriptions, pageTitle, metaTitle, metaDesc, metaKeys, fullDesc, chartTitle, chartSubTitle";
				
				$fields = explode( ',', $fields );
				$fields = array_map( 'trim', $fields );
				foreach( $fields as $field ) {
					if( !is_array( $this->$field )) continue;
					foreach( $this->$field as $idLanguage=>$value ) {
						if( empty( $value )) $this->{$field}[$idLanguage] = null;
					}
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'indicators', 'descriptions', 'pageTitle', 'metaTitle', 'metaDesc', 'metaKeys', 'fullDesc', 'chartTitle', 'chartSubTitle' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->indicators[ $i18n->idLanguage ] = $i18n->indicator_name;
					$this->descriptions[ $i18n->idLanguage ] = $i18n->indicator_description;
					
					$this->pageTitle[ $i18n->idLanguage ] = $i18n->pageTitle;
					$this->metaTitle[ $i18n->idLanguage ] = $i18n->metaTitle;
					$this->metaDesc[ $i18n->idLanguage ] = $i18n->metaDesc;
					$this->metaKeys[ $i18n->idLanguage ] = $i18n->metaKeys;
					$this->fullDesc[ $i18n->idLanguage ] = $i18n->fullDesc;
					
					$this->chartTitle[ $i18n->idLanguage ] = $i18n->chartTitle;
					$this->chartSubTitle[ $i18n->idLanguage ] = $i18n->chartSubTitle;
				}
				
				$this->currency = $AR->currency;
				
				$this->old_indicator_id = $AR->indicator_id;
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					
					$model = CalendarEvent2I18NModel::model()->findByAttributes( Array( 
						'indicator_id' => $this->indicator_id, 
						'idLanguage' => $language->id 
					));

					if ( !model ) {
						$model = new CalendarEvent2I18NModel();
						$model->indicator_id = $this->indicator_id;
						$model->idLanguage = $language->id;
					}

					$model->indicator_name = $this->indicators[ $language->id ];
					$model->indicator_name = $this->indicators[ $language->id ];
					$model->indicator_description = $this->descriptions[ $language->id ];
					
					$model->pageTitle = $this->pageTitle[ $language->id ];
					$model->metaTitle = $this->metaTitle[ $language->id ];
					$model->metaDesc = $this->metaDesc[ $language->id ];
					$model->metaKeys = $this->metaKeys[ $language->id ];
					$model->fullDesc = $this->fullDesc[ $language->id ];
					
					$model->chartTitle = $this->chartTitle[ $language->id ];
					$model->chartSubTitle = $this->chartSubTitle[ $language->id ];
					
					
					if (!$model->save()) {
						throw new CHttpException(501, Yii::t('main', 'Error'));
					}
					
				}
				if( !$AR->beenNewRecord ) {
					if( $AR->indicator_id != $this->old_indicator_id ) {
						$AR->onChangeIndicatorID( $this->old_indicator_id );
					}
				}

				return true;
			}
			throw new CHttpException(501, Yii::t('main', 'Error'));
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>
