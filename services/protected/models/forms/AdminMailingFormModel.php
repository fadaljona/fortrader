<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminMailingFormModel extends FormModelBase {
		public $id;
		public $names;
		public $messages;
		public $idType;
		public $usersGroup;
		public $isSMS;
		public $isEmail;
		function getARClassName() {
			return "MailingModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'idType' => 'Type',
				'usersGroup' => 'Users group',
				'isSMS' => 'SMS mailing',
				'isEmail' => 'EMail mailing',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "names[{$language->id}]" ] = "Name";
				$labels[ "messages[{$language->id}]" ] = "Message";
			}
						
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'isSMS', 'validateType' ),
				Array( 'names', 'validateNames' ),
				Array( 'messages', 'validateMessages' ),
				Array( 'names', 'checkLensNames' ),
				Array( 'messages', 'checkLensMessages' ),
			);
		}
		function validateType() {
			if( !$this->isSMS and !$this->isEmail ) {
				$this->addI18NError( "isSMS", "Select SMS/EMail!" );
			}
		}
		function validateNames() {
			foreach( $this->names as $idLanguage=>$name ) {
				if( strlen( $name )) return;
			}
			$NSi18n = $this->getNSi18n();
			$this->addError( "names", Yii::t( 'yii','{attribute} cannot be blank.', Array( '{attribute}' => Yii::t( $NSi18n, 'Name' ))));
		}
		function validateMessages() {
			foreach( $this->messages as $idLanguage=>$message ) {
				if( strlen( $message )) return;
			}
			$NSi18n = $this->getNSi18n();
			$this->addError( "messages", Yii::t( 'yii','{attribute} cannot be blank.', Array( '{attribute}' => Yii::t( $NSi18n, 'Message' ))));
		}
		function checkLensNames() {
			$NSi18n = $this->getNSi18n();
			$max = CommonLib::maxByte;
			foreach( $this->names as $idLanguage=>$name ) {
				if( mb_strlen( $name ) > $max ) {
					$this->addError( "names", Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => Yii::t( $NSi18n, 'Name' ),
						'{max}' => $max,
					)));
				}
			}
		}
		function checkLensMessages() {
			$NSi18n = $this->getNSi18n();
			$max = CommonLib::maxWord;
			foreach( $this->messages as $idLanguage=>$message ) {
				if( mb_strlen( $message ) > $max ) {
					$this->addError( "messages", Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => Yii::t( $NSi18n, 'Message' ),
						'{max}' => $max,
					)));
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
				if( !$this->idType ) $this->idType = null;
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->names[ $i18n->idLanguage ] = $i18n->name;
					$this->messages[ $i18n->idLanguage ] = $i18n->message;
				}
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'name' => $this->names[ $language->id ],
						'message' => $this->messages[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>