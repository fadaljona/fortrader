<?
	
	final class AdminStrategyInstrumentFormModel extends AdminFormModelBase {
		public $id;
		public $I18NsAssoc;
		function det_attributeLabels() {
			$labels = Array(
				'I18NsAssoc' => Array(
					'name' => "Title",
					'desc' => "Description",
				),
			);
			$this->extendLabels( $labels, 'I18NsAssoc', LanguageModel::getExistsIDS());
			return parent::det_attributeLabels( $labels );
		}
		function rules() {
			return Array(
				Array( 'I18NsAssoc[name]', 'requiredEx' ),
				Array( 'I18NsAssoc[name]', 'lengthEx', 'max' => CommonLib::maxByte ),
			);
		}
	}

?>