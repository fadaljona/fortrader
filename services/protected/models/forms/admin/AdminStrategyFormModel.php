<?
	
	final class AdminStrategyFormModel extends AdminFormModelBase {
		public $id;
		public $I18NsAssoc;
		public $userLogin;
		public $type;
		public $timeframes;
		public $linksToIndicatorsString;
		public $linksToInstrumentsString;
		public $idEA;
		public $price;
		function det_attributeLabels() {
			$labels = Array(
				'I18NsAssoc' => Array(
					'name' => "Title",
					'desc' => "Description",
				),
				'userLogin' => "Published",
				'idEA' => "EA",
				'linksToIndicatorsString' => "Indicators",
				'linksToInstrumentsString' => "Instruments",
			);
			$this->extendLabels( $labels, 'I18NsAssoc', LanguageModel::getExistsIDS());
			return parent::det_attributeLabels( $labels );
		}
		function rules() {
			return Array(
				Array( 'I18NsAssoc[name]', 'requiredEx' ),
				Array( 'I18NsAssoc[name], I18NsAssoc[desc]', 'lengthEx', 'max' => CommonLib::maxByte ),
				Array( 'userLogin', 'required' ),
				Array( 'userLogin', 'existsValueFieldModel', 'model' => 'UserModel', 'field' => 'user_login' ),
				Array( 'type', 'inFieldVariants' ),
				Array( 'idEA', 'existsIDModel', 'model' => 'EAModel' ),
				Array( 'price', 'numerical' ),
			);
		}
		function det_fieldsVariants() {
			return $_fieldVariants = Array(
				'linksToIndicatorsString' => StrategyIndicatorModel::model()->_getLightList(),
				'linksToInstrumentsString' => StrategyInstrumentModel::model()->_getLightList(),
			);
		}
	}

?>