<?
Yii::import('models.base.FormModelBase');

final class AdminQuotesAliasFormModel extends FormModelBase{
	public $id;
	public $desc;
	public $symbol;
	public $name;
	public $symbol_suggest;

	function getARClassName() {
		return "QuotesAliasModel";
	}

	protected function getSourceAttributeLabels() {
		return Array(
			'id' => 'ID',
			'name' => 'Имя алиса',
			'desc' => 'Описание',
			'symbol' => 'Инструмент'
		);
	}
	function rules() {
		return Array(
			Array( 'name', 'length', 'max' => 255 ),
			Array( 'name', 'uniqueField' ),
			Array( 'desc', 'length', 'max' => 1000),
			Array( 'symbol', 'numerical', 'min' => 1 )
		);
	}

	function load($id = 0){
		$post = $this->getPostLink();
		if((int)@$post['id']) $id = (int)@$post['id'];
		if($this->loadAR($id)){
			$this->loadFromAR(null,null,Array('values'));
			$this->loadFromPost();
			return true;
		}
		return false;
	}

	function save(){
		$AR = $this->getAR();
		if(!$AR) return false;
		$this->saveToAR();
		if($this->saveAR(false)){
			if($id=$AR->getPrimaryKey())
				return $id;
			return Yii::app()->db->getLastInsertID();
		}
		return false;
	}
}

