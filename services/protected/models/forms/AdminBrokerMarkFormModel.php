<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminBrokerMarkFormModel extends FormModelBase {
		public $conditions;
		public $execution;
		public $support;
		public $IO;
		public $site;
		protected $idBroker;
		function setIDBroker( $idBroker ) {
			$this->idBroker = $idBroker;
		}
		function getARClassName() {
			return "BrokerMarkModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'conditions' => 'Conditions',
				'execution' => 'Execution',
				'support' => 'Support',
				'IO' => 'Input/Output',
				'site' => 'Site',
			);
		}
		function rules() {
			return Array(
				Array( 'conditions, execution, support, IO, site', 'numerical', 'min' => 0, 'max' => 100, 'integerOnly' => true ),
			);
		}
		function loadAR( $pk = 0 ) {
			$broker = BrokerModel::model()->findByPk( $this->idBroker );
			if( !$broker ) return false;
			$AR = $broker->getUserMark();
			$this->setAR( $AR );
			return true;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array() ) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				$AR->trust = round( ( $this->conditions + $this->execution + $this->support + $this->IO + $this->site ) / 5 );
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>