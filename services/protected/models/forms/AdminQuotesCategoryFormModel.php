<?
Yii::import('models.base.FormModelBase');

final class AdminQuotesCategoryFormModel extends FormModelBase{
	public $id;
	public $order;
	public $parent;
	public $is_def;
	public $slug;
	public $ogImageUrl;
	
	public $name;
	public $desc;
	public $fullDesc;
	public $title;
	public $metaTitle;
	public $metaDescription;
	public $metaKeywords;
	public $shortDesc;
	public $orders;
	
	public $excludeFieldsFromAr = array( 'name', 'desc', 'fullDesc', 'title', 'metaTitle', 'metaDescription', 'metaKeywords', 'shortDesc', 'orders' );
	
	public static function getTextFields(){
		return array(
			'orders' => Array( 'modelField' => 'order', 'type' => 'textFieldRow', 'class' => '', 'disabled' => false ),
			'name' => Array( 'modelField' => 'name', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
			'desc' => Array( 'modelField' => 'desc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
			'fullDesc' => Array( 'modelField' => 'fullDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
			'title' => Array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
			'metaTitle' => Array( 'modelField' => 'metaTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
			'metaDescription' => Array( 'modelField' => 'metaDescription', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
			'metaKeywords' => Array( 'modelField' => 'metaKeywords', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
			'shortDesc' => Array( 'modelField' => 'shortDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
		);
	}

	function getARClassName() {
		return "QuotesCategoryModel";
	}

	protected function getSourceAttributeLabels(){
		$labels = Array(
			'id' => 'ID',
			'order' => 'Order',
			'parent' => 'Parent category',
			'is_def' => 'Default',
			'slug' => 'Slug',
			'ogImageUrl' => 'ogImageUrl'
		);
		$languages = LanguageModel::getAll();
		foreach( $languages as $language ){			
			$labels[ "name[{$language->id}]" ] = "Category name";
			$labels[ "orders[{$language->id}]" ] = "Order in language";
			$labels[ "desc[{$language->id}]" ] = "Category description";
			$labels[ "fullDesc[{$language->id}]" ] = "Full category description";
			$labels[ "title[{$language->id}]" ] = "Category title";
			$labels[ "metaTitle[{$language->id}]" ] = "Category Meta Title";
			$labels[ "metaDescription[{$language->id}]" ] = "Category Meta Description";
			$labels[ "metaKeywords[{$language->id}]" ] = "Category Meta Keywords";
			$labels[ "shortDesc[{$language->id}]" ] = "Category Short Desc";
		}	
		return $labels;
	}
	
	function rules(){
		$rules = Array(
			Array( 'slug', 'required' ),
			Array( 'slug', 'uniqueField' ),
			Array( 'slug', 'length', 'max' => CommonLib::maxByte ),
			Array( 'ogImageUrl', 'length', 'max' => CommonLib::maxByte ),
			Array( 'order', 'length', 'max' => 3 ),
			Array( 'is_def', 'in', 'range' => Array( 0, 1 )),
			Array( 'name', 'notEmptyCurrentLang' ),
			Array( 'name, title, metaTitle, metaDescription, metaKeywords', 'checkLens', 'max' => CommonLib::maxByte ),
			Array( 'desc, fullDesc, shortDesc', 'checkLens', 'max' => CommonLib::maxWord ),
		);

		$languages = LanguageModel::getAll();
		foreach( $languages as $language ){			
			$rules[] = Array( "orders[{$language->id}]", 'numerical', 'integerOnly' => true, 'min' => 0 );
		}

		return $rules;
	}
	function notEmptyCurrentLang( $field, $params ) {
		$NSi18n = $this->getNSi18n();	
		$currentLangId = LanguageModel::getCurrentLanguageID();
			
		if( mb_strlen( $this->{$field}[$currentLangId] ) == 0 ) {
			$this->addError( "{$field}[{$currentLangId}]", Yii::t( '*','{attribute} can not be empty.', Array( 
				'{attribute}' => $this->getAttributeLabel( "{$field}[{$currentLangId}]" ),
			)));
		}
	}
	function checkLens( $field, $params ) {
		$NSi18n = $this->getNSi18n();
		foreach( $this->$field as $idLanguage=>$value ) {
			if( mb_strlen( $value ) > $params['max'] ) {
				$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
					'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
					'{max}' => $params['max'],
				)));
			}
		}
	}
	function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
					}
				}
								
				return true;
			}
			return false;
		}

		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					
					$arrToSave = Array();
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
					}
					$i18ns[ $language->id ] = (object)$arrToSave;
				}
				
				$AR->setI18Ns( $i18ns );
				return true;
			}
			return false;
		}
		
	function load($id = 0){
		$post = $this->getPostLink();
		if((int)@$post['id']) $id = (int)@$post['id'];
		if($this->loadAR($id)){
			$this->loadFromAR(null,null,Array('values'));
			$this->loadFromPost();
			return true;
		}
		return false;
	}

	function save(){
		$AR = $this->getAR();
		if(!$AR) return false;
		 $this->saveToAR();
		if($this->saveAR(false)){
			return $AR->getPrimaryKey();
		}
		return false;
	}
}

