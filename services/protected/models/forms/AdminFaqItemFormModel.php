<?php

Yii::import('models.base.FormModelBase');
Yii::import('models.traits.ReplaceItemsTrait');

final class AdminFaqItemFormModel extends FormModelBase
{
    use ReplaceItemsTrait;

    public $id;
    public $idFaq;

    public $question;
    public $answer;
    public $order;

    public $replaceItems;


    public $excludeFieldsFromAr = array('question', 'answer' );
        
    public static function getTextFields()
    {
        return array(
            'question' => array( 'modelField' => 'question', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
            'answer' => array( 'modelField' => 'answer', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
        );
    }
    public function getARClassName()
    {
        return "FaqItemModel";
    }
    protected function getSourceAttributeLabels()
    {
        $labels = array();

        $languages = LanguageModel::getAll();
        foreach ($languages as $language) {
            $labels[ "question[{$language->id}]" ] = "Question";
            $labels[ "answer[{$language->id}]" ] = "Answer";
        }
        return $labels;
    }
    public function rules()
    {
        $rules = array(
            array( 'question', 'checkLens', 'max' => CommonLib::maxByte ),
            array( 'answer', 'checkLens', 'max' => CommonLib::maxWord ),
            array( 'question, answer', 'notEmptyCurrentLang' ),
            array('order', 'numerical', 'integerOnly' => true),
            array('replaceItems', 'validateReplaces'),
        );
        return $rules;
    }
    public function notEmptyCurrentLang($field, $params)
    {
        $NSi18n = $this->getNSi18n();
        $currentLangId = LanguageModel::getCurrentLanguageID();

        if (mb_strlen($this->{$field}[$currentLangId]) == 0) {
            $this->addError("{$field}[{$currentLangId}]", Yii::t('*', '{attribute} can not be empty.', array(
                '{attribute}' => $this->getAttributeLabel("{$field}[{$currentLangId}]"),
            )));
        }
    }
    
    public function checkLens($field, $params)
    {
        $NSi18n = $this->getNSi18n();
        foreach ($this->$field as $idLanguage => $value) {
            if (mb_strlen($value) > $params['max']) {
                $this->addError($field, Yii::t('yii', '{attribute} is too long (maximum is {max} characters).', array(
                    '{attribute}' => $this->getAttributeLabel("{$field}[{$idLanguage}]"),
                    '{max}' => $params['max'],
                )));
            }
        }
    }


    public function loadFromAR($AR = null, $loadFields = array(), $exceptFields = array())
    {
        $exceptFields = $this->excludeFieldsFromAr;
        if (parent::loadFromAR($AR, $loadFields, $exceptFields)) {
            $AR = $this->getAR();

            $i18ns = $AR->i18ns;
            foreach ($i18ns as $i18n) {
                foreach ($this->textFields as $formFieldName => $settings) {
                    $this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
                }
            }
            return true;
        }
        return false;
    }

    public function saveAR($runValidation = true, $attributes = null)
    {
        if (parent::saveAR($runValidation, $attributes)) {
            $AR = $this->getAR();
            $i18ns = array();
            $languages = LanguageModel::getAll();
            foreach ($languages as $language) {
                $arrToSave = array();

                foreach ($this->textFields as $formFieldName => $settings) {
                    $arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
                }
                $i18ns[ $language->id ] = (object)$arrToSave;
            }

            $AR->setI18Ns($i18ns);
            return true;
        }
        return false;
    }

    public function load($id = 0)
    {
        $post = $this->getPostLink();
        if ((int)@$post['id']) {
            $id = (int)@$post['id'];
        }
        if ($this->loadAR($id)) {
            $this->loadFromAR();
            $this->loadFromPost();
            return true;
        }
        return false;
    }

    public function save()
    {
        $AR = $this->getAR();
        if (!$AR) {
            return false;
        }

        $this->saveToAR(null, array(), array());

        if ($this->saveAR(false)) {
            return $AR->getPrimaryKey();
        }
        return false;
    }
}
