<?
	Yii::import( 'models.base.SettingsFormModelBase' );

	final class AdminBrokerSettingsFormModel extends SettingsFormModelBase {
		public $daysToWeightReductionReview;
		public $minTrustMark;
		public $maxTrustMark;
		
		public $brokersListMetaTitle = Array();
		public $brokersListMetaDesc = Array();
		public $brokersListMetaKeys = Array();
		public $brokersListOgImage = Array();
		
		public $brokersListShortDesc = Array();
		public $brokersListFullDesc = Array();

		public $binaryOptionsListMetaTitle = Array();
		public $binaryOptionsListMetaDesc = Array();
		public $binaryOptionsListMetaKeys = Array();
		public $binaryOptionsListOgImage = Array();
		
		public $binaryOptionsListShortDesc = Array();
		public $binaryOptionsListFullDesc = Array();
		

		public $textFields = array(
			'brokersListMetaTitle' => 'textFieldRow',
			'brokersListMetaDesc' => 'textFieldRow',
			'brokersListMetaKeys' => 'textFieldRow',
			'brokersListOgImage' => 'textFieldRow',
			
			'brokersListShortDesc' => 'textAreaRow',
			'brokersListFullDesc' => 'textAreaRow',
			
			'binaryOptionsListMetaTitle' => 'textFieldRow',
			'binaryOptionsListMetaDesc' => 'textFieldRow',
			'binaryOptionsListMetaKeys' => 'textFieldRow',
			'binaryOptionsListOgImage' => 'textFieldRow',
			
			'binaryOptionsListShortDesc' => 'textAreaRow',
			'binaryOptionsListFullDesc' => 'textAreaRow',
		);
		
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'daysToWeightReductionReview' => 'Days to weight reduction review',
				'minTrustMark' => 'Min mark',
				'maxTrustMark' => 'Max mark',
			);
			return $this->setTextLabels( $labels );
		}
		function rules() {
			return Array(
				Array( 'daysToWeightReductionReview', 'numerical', 'integerOnly' => true, 'min' => 0 ),
				Array( 'minTrustMark', 'numerical', 'integerOnly' => true, 'min' => 0 ),
				Array( 'maxTrustMark', 'numerical', 'integerOnly' => true, 'min' => 0 ),
				Array( 'brokersListMetaTitle, brokersListMetaDesc, brokersListMetaKeys, brokersListOgImage, binaryOptionsListMetaTitle, binaryOptionsListMetaDesc, binaryOptionsListMetaKeys, binaryOptionsListOgImage', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'brokersListShortDesc, brokersListFullDesc, binaryOptionsListShortDesc, binaryOptionsListFullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
		function loadAR( $pk = 0 ) {
			$AR = BrokerSettingsModel::getModel();
			$this->setAR( $AR );
			return true;
		}
	}

?>