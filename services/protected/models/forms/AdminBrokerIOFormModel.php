<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminBrokerIOFormModel extends FormModelBase {
		public $id;
		public $names = Array();
		function getARClassName() {
			return "BrokerIOModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array();
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "names[{$language->id}]" ] = "Title";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'names', 'validateNames' ),
				Array( 'names', 'checkLensNames' ),
				Array( 'names', 'checkExists' ),
			);
		}
		function validateNames() {
			foreach( $this->names as $idLanguage=>$name ) {
				if( strlen( $name )) return;
			}
			$NSi18n = $this->getNSi18n();
			$this->addError( "names", Yii::t( 'yii','{attribute} cannot be blank.', Array( '{attribute}' => Yii::t( $NSi18n, 'Title' ))));
		}
		function checkLensNames() {
			$NSi18n = $this->getNSi18n();
			$max = CommonLib::maxByte;
			foreach( $this->names as $idLanguage=>$name ) {
				if( mb_strlen( $name ) > $max ) {
					$this->addError( "names", Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => Yii::t( $NSi18n, 'Title' ),
						'{max}' => $max,
					)));
				}
			}
		}
		function checkExists( $field ) {
			if( !$this->hasErrors()) {
				$AR = $this->getAR();
				foreach( $this->$field as $idLanguage=>$value ) {
					$c = new CDbCriteria();
					$keyI18N = preg_replace( "#s$#", '', $field );
					$c->with[ 'i18ns' ] = Array(
						'joinType' => 'INNER JOIN',
						'condition' => " 
							`i18nsBrokerIO`.`{$keyI18N}` = :{$keyI18N}
						",
						'params' => Array(
							":{$keyI18N}" => $value,
						),
					);
					
					if( !$AR->isNewRecord ) {
						$c->addCondition( "`t`.`id` != :pk" );
						$c->params[ ':pk' ] = $AR->id;
					}
					
					$exists = BrokerIOModel::model()->exists( $c );
					if( $exists ) {
						$this->addError( $field, Yii::t( 'yii', '{attribute} "{value}" has already been taken.', Array(
							'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
							'{value}' => CHtml::encode( $value ),
						)));
					}
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'names' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->names[ $i18n->idLanguage ] = $i18n->name;
				}
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'name' => $this->names[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>