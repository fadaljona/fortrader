<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminCurrencyRatesFormModel extends FormModelBase {
		public $id;
		public $code;
		public $cbrId;
		
		public $noData;
		public $ecb;
		
		public $idCountry;
		public $currencyOrder;
		public $toInformer;
		
		public $defaultCurrency;
		public $defaultCurrencyEcb;
		
		public $popular;
		public $cbrDigitCode;
		public $topInSelect;
		public $symbol;
		
		public $names = Array();
		public $titles = Array();
		public $metaTitles = Array();
		public $metaDescs = Array();
		public $metaKeyss = Array();
		public $shortDescs = Array();
		public $fullDescs = Array();
		
		public $chartTitles = Array();
		public $converterTitles = Array();
		
		public $namePlurals = Array();
		public $toName = Array();
		
		public $informerName = array();
		
		public $singleArchiveYearTitle = array();
		public $singleArchiveYearMetaTitle = array();
		public $singleArchiveYearMetaDesc = array();
		public $singleArchiveYearMetaKeys = array();
		public $singleArchiveYearDescription = array();
		public $singleArchiveYearFullDesc = array();
		
		public $singleArchiveYearMoTitle = array();
		public $singleArchiveYearMoMetaTitle = array();
		public $singleArchiveYearMoMetaDesc = array();
		public $singleArchiveYearMoMetaKeys = array();
		public $singleArchiveYearMoDescription = array();
		public $singleArchiveYearMoFullDesc = array();
		
		public $singleArchiveYearMoDayTitle = array();
		public $singleArchiveYearMoDayMetaTitle = array();
		public $singleArchiveYearMoDayMetaDesc = array();
		public $singleArchiveYearMoDayMetaKeys = array();
		public $singleArchiveYearMoDayDescription = array();
		public $singleArchiveYearMoDayFullDesc = array();
		
		public $ecbTitle = array();
		public $ecbMetaTitle = array();
		public $ecbMetaDesc = array();
		public $ecbMetaKeys = array();
		public $ecbShortDesc = array();
		public $ecbFullDesc = array();
		public $ecbChartTitle = array();
		public $ecbConverterTitle = array();
		public $ecbSingleArchiveYearTitle = array();
		public $ecbSingleArchiveYearMetaTitle = array();
		public $ecbSingleArchiveYearMetaDesc = array();
		public $ecbSingleArchiveYearMetaKeys = array();
		public $ecbSingleArchiveYearMoTitle = array();
		public $ecbSingleArchiveYearMoMetaTitle = array();
		public $ecbSingleArchiveYearMoMetaDesc = array();
		public $ecbSingleArchiveYearMoMetaKeys = array();
		public $ecbSingleArchiveYearMoDayTitle = array();
		public $ecbSingleArchiveYearMoDayMetaTitle = array();
		public $ecbSingleArchiveYearMoDayMetaDesc = array();
		public $ecbSingleArchiveYearMoDayMetaKeys = array();
		public $ecbSingleArchiveYearDescription = array();
		public $ecbSingleArchiveYearFullDesc = array();
		public $ecbSingleArchiveYearMoDescription = array();
		public $ecbSingleArchiveYearMoFullDesc = array();
		public $ecbSingleArchiveYearMoDayDescription = array();
		public $ecbSingleArchiveYearMoDayFullDesc = array();
		
		function getARClassName() {
			return "CurrencyRatesModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'code' => 'Currency code (SLUG)',
				'idCountry' => 'Choose country',
				'currencyOrder' => 'Order in listing - ASC',
				'popular' => 'Popular currency?',
				'cbrDigitCode' => 'cbr digital code',
				'topInSelect' => 'Fixed currencies in selects',
				'symbol' => 'Currency symbol',
				'toInformer' => 'To informer by defautl',
			);	
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "names[{$language->id}]" ] = "Name";
				
				$labels[ "titles[{$language->id}]" ] = "Title";
				$labels[ "metaTitles[{$language->id}]" ] = "Meta title";
				$labels[ "metaDescs[{$language->id}]" ] = "Meta desc";
				$labels[ "metaKeyss[{$language->id}]" ] = "Meta keys";
				$labels[ "shortDescs[{$language->id}]" ] = "Short description";
				$labels[ "fullDescs[{$language->id}]" ] = "Full description";
				
				$labels[ "chartTitles[{$language->id}]" ] = "Chart Title";
				$labels[ "converterTitles[{$language->id}]" ] = "Converter Title";
				$labels[ "namePlurals[{$language->id}]" ] = "Name Plurals";
				$labels[ "toName[{$language->id}]" ] = "To Name";
				
				$labels[ "informerName[{$language->id}]" ] = "Informer name";
				
				
				
				
				$labels[ "singleArchiveYearTitle[{$language->id}]" ] = "Single Archive Year Title";
				$labels[ "singleArchiveYearMetaTitle[{$language->id}]" ] = "Single Archive Year Meta Title";
				$labels[ "singleArchiveYearMetaDesc[{$language->id}]" ] = "Single Archive Year MetaDesc";
				$labels[ "singleArchiveYearMetaKeys[{$language->id}]" ] = "Single Archive Year MetaKeys";
				$labels[ "singleArchiveYearDescription[{$language->id}]" ] = "Single Archive Year Description";
				$labels[ "singleArchiveYearFullDesc[{$language->id}]" ] = "Single Archive Year FullDesc";
						
				$labels[ "singleArchiveYearMoTitle[{$language->id}]" ] = "Single Archive YearMo Title";
				$labels[ "singleArchiveYearMoMetaTitle[{$language->id}]" ] = "Single Archive YearMo MetaTitle";
				$labels[ "singleArchiveYearMoMetaDesc[{$language->id}]" ] = "Single Archive YearMo MetaDesc";
				$labels[ "singleArchiveYearMoMetaKeys[{$language->id}]" ] = "Single Archive YearMo MetaKeys";
				$labels[ "singleArchiveYearMoDescription[{$language->id}]" ] = "Single Archive YearMo Description";
				$labels[ "singleArchiveYearMoFullDesc[{$language->id}]" ] = "Single Archive YearMo FullDesc";
						
				$labels[ "singleArchiveYearMoDayTitle[{$language->id}]" ] = "Single Archive YearMoDay Title";
				$labels[ "singleArchiveYearMoDayMetaTitle[{$language->id}]" ] = "Single Archive YearMoDay MetaTitle";
				$labels[ "singleArchiveYearMoDayMetaDesc[{$language->id}]" ] = "Single Archive YearMoDay MetaDesc";
				$labels[ "singleArchiveYearMoDayMetaKeys[{$language->id}]" ] = "Single Archive YearMoDay MetaKeys";
				$labels[ "singleArchiveYearMoDayDescription[{$language->id}]" ] = "Single Archive YearMoDay Description";
				$labels[ "singleArchiveYearMoDayFullDesc[{$language->id}]" ] = "Single Archive YearMoDay FullDesc";
				
				$labels[ "ecbTitle[{$language->id}]" ] = "Ecb Title";
				$labels[ "ecbMetaTitle[{$language->id}]" ] = "Ecb Meta title";
				$labels[ "ecbMetaDesc[{$language->id}]" ] = "Ecb Meta desc";
				$labels[ "ecbMetaKeys[{$language->id}]" ] = "Ecb Meta keys";
				$labels[ "ecbShortDesc[{$language->id}]" ] = "Ecb Short description";
				$labels[ "ecbFullDesc[{$language->id}]" ] = "Ecb Full description";
				$labels[ "ecbChartTitle[{$language->id}]" ] = "Ecb Chart Title";
				$labels[ "ecbConverterTitle[{$language->id}]" ] = "Ecb Converter Title";
				$labels[ "ecbSingleArchiveYearTitle[{$language->id}]" ] = "Ecb Single Archive Year Title";
				$labels[ "ecbSingleArchiveYearMetaTitle[{$language->id}]" ] = "Ecb Single Archive Year Meta Title";
				$labels[ "ecbSingleArchiveYearMetaDesc[{$language->id}]" ] = "Ecb Single Archive Year MetaDesc";
				$labels[ "ecbSingleArchiveYearMetaKeys[{$language->id}]" ] = "Ecb Single Archive Year MetaKeys";
				$labels[ "ecbSingleArchiveYearMoTitle[{$language->id}]" ] = "Ecb Single Archive YearMo Title";
				$labels[ "ecbSingleArchiveYearMoMetaTitle[{$language->id}]" ] = "Ecb Single Archive YearMo MetaTitle";
				$labels[ "ecbSingleArchiveYearMoMetaDesc[{$language->id}]" ] = "Ecb Single Archive YearMo MetaDesc";
				$labels[ "ecbSingleArchiveYearMoMetaKeys[{$language->id}]" ] = "Ecb Single Archive YearMo MetaKeys";
				$labels[ "ecbSingleArchiveYearMoDayTitle[{$language->id}]" ] = "Ecb Single Archive YearMoDay Title";
				$labels[ "ecbSingleArchiveYearMoDayMetaTitle[{$language->id}]" ] = "Ecb Single Archive YearMoDay MetaTitle";
				$labels[ "ecbSingleArchiveYearMoDayMetaDesc[{$language->id}]" ] = "Ecb Single Archive YearMoDay MetaDesc";
				$labels[ "ecbSingleArchiveYearMoDayMetaKeys[{$language->id}]" ] = "Ecb Single Archive YearMoDay MetaKeys";
				$labels[ "ecbSingleArchiveYearDescription[{$language->id}]" ] = "Ecb Single Archive Year Description";
				$labels[ "ecbSingleArchiveYearFullDesc[{$language->id}]" ] = "Ecb Single Archive Year FullDesc";
				$labels[ "ecbSingleArchiveYearMoDescription[{$language->id}]" ] = "Ecb Single Archive YearMo Description";
				$labels[ "ecbSingleArchiveYearMoFullDesc[{$language->id}]" ] = "Ecb Single Archive YearMo FullDesc";
				$labels[ "ecbSingleArchiveYearMoDayDescription[{$language->id}]" ] = "Ecb Single Archive YearMoDay Description";
				$labels[ "ecbSingleArchiveYearMoDayFullDesc[{$language->id}]" ] = "Ecb Single Archive YearMoDay FullDesc";
				
			}
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'code, cbrId', 'length', 'max' => 10 ),
				Array( 'symbol', 'length', 'max' => 50 ),
				
				Array( 'names, titles, metaTitles, metaDescs, metaKeyss, chartTitles, converterTitles, namePlurals, toName, informerName, singleArchiveYearTitle ,singleArchiveYearMetaTitle ,singleArchiveYearMetaDesc ,singleArchiveYearMetaKeys ,singleArchiveYearMoTitle ,singleArchiveYearMoMetaTitle ,singleArchiveYearMoMetaDesc ,singleArchiveYearMoMetaKeys ,singleArchiveYearMoDayTitle ,singleArchiveYearMoDayMetaTitle ,singleArchiveYearMoDayMetaDesc ,singleArchiveYearMoDayMetaKeys, ecbShortDesc,ecbFullDesc,ecbTitle,ecbMetaTitle,ecbMetaDesc,ecbMetaKeys,ecbChartTitle,ecbConverterTitle,ecbSingleArchiveYearTitle,ecbSingleArchiveYearMetaTitle,ecbSingleArchiveYearMetaDesc,ecbSingleArchiveYearMetaKeys,ecbSingleArchiveYearMoTitle,ecbSingleArchiveYearMoMetaTitle,ecbSingleArchiveYearMoMetaDesc,ecbSingleArchiveYearMoMetaKeys,ecbSingleArchiveYearMoDayTitle,ecbSingleArchiveYearMoDayMetaTitle,ecbSingleArchiveYearMoDayMetaDesc,ecbSingleArchiveYearMoDayMetaKeys', 'checkLens', 'max' => CommonLib::maxByte ),
				
				Array( 'fullDescs, shortDescs, singleArchiveYearDescription ,singleArchiveYearFullDesc ,singleArchiveYearMoDescription ,singleArchiveYearMoFullDesc ,singleArchiveYearMoDayDescription ,singleArchiveYearMoDayFullDesc, ecbSingleArchiveYearDescription,ecbSingleArchiveYearFullDesc,ecbSingleArchiveYearMoDescription,ecbSingleArchiveYearMoFullDesc,ecbSingleArchiveYearMoDayDescription,ecbSingleArchiveYearMoDayFullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
				
				Array( 'code, cbrId', 'uniqueField', 'message' => 'Code is busy. Please choose another.' ),
				Array( 'code, cbrId', 'required' ),
				Array( 'noData, popular, topInSelect, toInformer', 'boolean'),
				Array( 'idCountry', 'numerical', 'integerOnly'=>true, 'min' => -1 ),
				Array( 'currencyOrder, cbrDigitCode', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
			);
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'names', 'titles', 'metaTitles', 'metaDescs', 'metaKeyss', 'shortDescs', 'fullDescs', 'chartTitles', 'converterTitles', 'namePlurals', 'toName', 'informerName', 'singleArchiveYearTitle', 'singleArchiveYearMetaTitle', 'singleArchiveYearMetaDesc', 'singleArchiveYearMetaKeys', 'singleArchiveYearMoTitle', 'singleArchiveYearMoMetaTitle', 'singleArchiveYearMoMetaDesc', 'singleArchiveYearMoMetaKeys', 'singleArchiveYearMoDayTitle', 'singleArchiveYearMoDayMetaTitle', 'singleArchiveYearMoDayMetaDesc', 'singleArchiveYearMoDayMetaKeys', 'singleArchiveYearDescription', 'singleArchiveYearFullDesc', 'singleArchiveYearMoDescription', 'singleArchiveYearMoFullDesc', 'singleArchiveYearMoDayDescription', 'singleArchiveYearMoDayFullDesc', 'ecbTitle','ecbMetaTitle','ecbMetaDesc','ecbMetaKeys','ecbShortDesc','ecbFullDesc','ecbChartTitle','ecbConverterTitle','ecbSingleArchiveYearTitle','ecbSingleArchiveYearMetaTitle','ecbSingleArchiveYearMetaDesc','ecbSingleArchiveYearMetaKeys','ecbSingleArchiveYearMoTitle','ecbSingleArchiveYearMoMetaTitle','ecbSingleArchiveYearMoMetaDesc','ecbSingleArchiveYearMoMetaKeys','ecbSingleArchiveYearMoDayTitle','ecbSingleArchiveYearMoDayMetaTitle','ecbSingleArchiveYearMoDayMetaDesc','ecbSingleArchiveYearMoDayMetaKeys','ecbSingleArchiveYearDescription','ecbSingleArchiveYearFullDesc','ecbSingleArchiveYearMoDescription','ecbSingleArchiveYearMoFullDesc','ecbSingleArchiveYearMoDayDescription','ecbSingleArchiveYearMoDayFullDesc' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->names[ $i18n->idLanguage ] = $i18n->name;
					
					$this->titles[ $i18n->idLanguage ] = $i18n->title;
					$this->metaTitles[ $i18n->idLanguage ] = $i18n->metaTitle;
					$this->metaDescs[ $i18n->idLanguage ] = $i18n->metaDesc;
					$this->metaKeyss[ $i18n->idLanguage ] = $i18n->metaKeys;
					$this->fullDescs[ $i18n->idLanguage ] = $i18n->fullDesc;
					$this->shortDescs[ $i18n->idLanguage ] = $i18n->shortDesc;
					
					$this->chartTitles[ $i18n->idLanguage ] = $i18n->chartTitle;
					$this->converterTitles[ $i18n->idLanguage ] = $i18n->converterTitle;
					
					$this->namePlurals[ $i18n->idLanguage ] = $i18n->namePlurals;
					$this->toName[ $i18n->idLanguage ] = $i18n->toName;
					
					$this->informerName[ $i18n->idLanguage ] = $i18n->informerName;
					
					$this->singleArchiveYearTitle[ $i18n->idLanguage ] = $i18n->singleArchiveYearTitle;
					$this->singleArchiveYearMetaTitle[ $i18n->idLanguage ] = $i18n->singleArchiveYearMetaTitle;
					$this->singleArchiveYearMetaDesc[ $i18n->idLanguage ] = $i18n->singleArchiveYearMetaDesc;
					$this->singleArchiveYearMetaKeys[ $i18n->idLanguage ] = $i18n->singleArchiveYearMetaKeys;
					$this->singleArchiveYearMoTitle[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoTitle;
					$this->singleArchiveYearMoMetaTitle[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoMetaTitle;
					$this->singleArchiveYearMoMetaDesc[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoMetaDesc;
					$this->singleArchiveYearMoMetaKeys[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoMetaKeys;
					$this->singleArchiveYearMoDayTitle[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoDayTitle;
					$this->singleArchiveYearMoDayMetaTitle[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoDayMetaTitle;
					$this->singleArchiveYearMoDayMetaDesc[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoDayMetaDesc;
					$this->singleArchiveYearMoDayMetaKeys[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoDayMetaKeys;
					$this->singleArchiveYearDescription[ $i18n->idLanguage ] = $i18n->singleArchiveYearDescription;
					$this->singleArchiveYearFullDesc[ $i18n->idLanguage ] = $i18n->singleArchiveYearFullDesc;
					$this->singleArchiveYearMoDescription[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoDescription;
					$this->singleArchiveYearMoFullDesc[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoFullDesc;
					$this->singleArchiveYearMoDayDescription[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoDayDescription;
					$this->singleArchiveYearMoDayFullDesc[ $i18n->idLanguage ] = $i18n->singleArchiveYearMoDayFullDesc;
					
					$this->ecbTitle[ $i18n->idLanguage ] = $i18n->ecbTitle;
					$this->ecbMetaTitle[ $i18n->idLanguage ] = $i18n->ecbMetaTitle;
					$this->ecbMetaDesc[ $i18n->idLanguage ] = $i18n->ecbMetaDesc;
					$this->ecbMetaKeys[ $i18n->idLanguage ] = $i18n->ecbMetaKeys;
					$this->ecbShortDesc[ $i18n->idLanguage ] = $i18n->ecbShortDesc;
					$this->ecbFullDesc[ $i18n->idLanguage ] = $i18n->ecbFullDesc;
					$this->ecbChartTitle[ $i18n->idLanguage ] = $i18n->ecbChartTitle;
					$this->ecbConverterTitle[ $i18n->idLanguage ] = $i18n->ecbConverterTitle;
					$this->ecbSingleArchiveYearTitle[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearTitle;
					$this->ecbSingleArchiveYearMetaTitle[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMetaTitle;
					$this->ecbSingleArchiveYearMetaDesc[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMetaDesc;
					$this->ecbSingleArchiveYearMetaKeys[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMetaKeys;
					$this->ecbSingleArchiveYearMoTitle[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoTitle;
					$this->ecbSingleArchiveYearMoMetaTitle[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoMetaTitle;
					$this->ecbSingleArchiveYearMoMetaDesc[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoMetaDesc;
					$this->ecbSingleArchiveYearMoMetaKeys[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoMetaKeys;
					$this->ecbSingleArchiveYearMoDayTitle[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoDayTitle;
					$this->ecbSingleArchiveYearMoDayMetaTitle[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoDayMetaTitle;
					$this->ecbSingleArchiveYearMoDayMetaDesc[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoDayMetaDesc;
					$this->ecbSingleArchiveYearMoDayMetaKeys[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoDayMetaKeys;
					$this->ecbSingleArchiveYearDescription[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearDescription;
					$this->ecbSingleArchiveYearFullDesc[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearFullDesc;
					$this->ecbSingleArchiveYearMoDescription[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoDescription;
					$this->ecbSingleArchiveYearMoFullDesc[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoFullDesc;
					$this->ecbSingleArchiveYearMoDayDescription[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoDayDescription;
					$this->ecbSingleArchiveYearMoDayFullDesc[ $i18n->idLanguage ] = $i18n->ecbSingleArchiveYearMoDayFullDesc;

				}
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();

				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'name' => $this->names[ $language->id ],
						
						'title' => $this->titles[ $language->id ],
						'metaTitle' => $this->metaTitles[ $language->id ],
						'metaDesc' => $this->metaDescs[ $language->id ],
						'metaKeys' => $this->metaKeyss[ $language->id ],
						'shortDesc' => $this->shortDescs[ $language->id ],
						'fullDesc' => $this->fullDescs[ $language->id ],
						
						'chartTitle' => $this->chartTitles[ $language->id ],
						'converterTitle' => $this->converterTitles[ $language->id ],
						
						'namePlurals' => $this->namePlurals[ $language->id ],
						'toName' => $this->toName[ $language->id ],
						
						'informerName' => $this->informerName[ $language->id ],
						
						'singleArchiveYearTitle' => $this->singleArchiveYearTitle[ $language->id ],
						'singleArchiveYearMetaTitle' => $this->singleArchiveYearMetaTitle[ $language->id ],
						'singleArchiveYearMetaDesc' => $this->singleArchiveYearMetaDesc[ $language->id ],
						'singleArchiveYearMetaKeys' => $this->singleArchiveYearMetaKeys[ $language->id ],
						'singleArchiveYearMoTitle' => $this->singleArchiveYearMoTitle[ $language->id ],
						'singleArchiveYearMoMetaTitle' => $this->singleArchiveYearMoMetaTitle[ $language->id ],
						'singleArchiveYearMoMetaDesc' => $this->singleArchiveYearMoMetaDesc[ $language->id ],
						'singleArchiveYearMoMetaKeys' => $this->singleArchiveYearMoMetaKeys[ $language->id ],
						'singleArchiveYearMoDayTitle' => $this->singleArchiveYearMoDayTitle[ $language->id ],
						'singleArchiveYearMoDayMetaTitle' => $this->singleArchiveYearMoDayMetaTitle[ $language->id ],
						'singleArchiveYearMoDayMetaDesc' => $this->singleArchiveYearMoDayMetaDesc[ $language->id ],
						'singleArchiveYearMoDayMetaKeys' => $this->singleArchiveYearMoDayMetaKeys[ $language->id ],
						'singleArchiveYearDescription' => $this->singleArchiveYearDescription[ $language->id ],
						'singleArchiveYearFullDesc' => $this->singleArchiveYearFullDesc[ $language->id ],
						'singleArchiveYearMoDescription' => $this->singleArchiveYearMoDescription[ $language->id ],
						'singleArchiveYearMoFullDesc' => $this->singleArchiveYearMoFullDesc[ $language->id ],
						'singleArchiveYearMoDayDescription' => $this->singleArchiveYearMoDayDescription[ $language->id ],
						'singleArchiveYearMoDayFullDesc' => $this->singleArchiveYearMoDayFullDesc[ $language->id ],
						
						
						'ecbTitle' => $this->ecbTitle[ $language->id ],
						'ecbMetaTitle' => $this->ecbMetaTitle[ $language->id ],
						'ecbMetaDesc' => $this->ecbMetaDesc[ $language->id ],
						'ecbMetaKeys' => $this->ecbMetaKeys[ $language->id ],
						'ecbShortDesc' => $this->ecbShortDesc[ $language->id ],
						'ecbFullDesc' => $this->ecbFullDesc[ $language->id ],
						'ecbChartTitle' => $this->ecbChartTitle[ $language->id ],
						'ecbConverterTitle' => $this->ecbConverterTitle[ $language->id ],
						'ecbSingleArchiveYearTitle' => $this->ecbSingleArchiveYearTitle[ $language->id ],
						'ecbSingleArchiveYearMetaTitle' => $this->ecbSingleArchiveYearMetaTitle[ $language->id ],
						'ecbSingleArchiveYearMetaDesc' => $this->ecbSingleArchiveYearMetaDesc[ $language->id ],
						'ecbSingleArchiveYearMetaKeys' => $this->ecbSingleArchiveYearMetaKeys[ $language->id ],
						'ecbSingleArchiveYearMoTitle' => $this->ecbSingleArchiveYearMoTitle[ $language->id ],
						'ecbSingleArchiveYearMoMetaTitle' => $this->ecbSingleArchiveYearMoMetaTitle[ $language->id ],
						'ecbSingleArchiveYearMoMetaDesc' => $this->ecbSingleArchiveYearMoMetaDesc[ $language->id ],
						'ecbSingleArchiveYearMoMetaKeys' => $this->ecbSingleArchiveYearMoMetaKeys[ $language->id ],
						'ecbSingleArchiveYearMoDayTitle' => $this->ecbSingleArchiveYearMoDayTitle[ $language->id ],
						'ecbSingleArchiveYearMoDayMetaTitle' => $this->ecbSingleArchiveYearMoDayMetaTitle[ $language->id ],
						'ecbSingleArchiveYearMoDayMetaDesc' => $this->ecbSingleArchiveYearMoDayMetaDesc[ $language->id ],
						'ecbSingleArchiveYearMoDayMetaKeys' => $this->ecbSingleArchiveYearMoDayMetaKeys[ $language->id ],
						'ecbSingleArchiveYearDescription' => $this->ecbSingleArchiveYearDescription[ $language->id ],
						'ecbSingleArchiveYearFullDesc' => $this->ecbSingleArchiveYearFullDesc[ $language->id ],
						'ecbSingleArchiveYearMoDescription' => $this->ecbSingleArchiveYearMoDescription[ $language->id ],
						'ecbSingleArchiveYearMoFullDesc' => $this->ecbSingleArchiveYearMoFullDesc[ $language->id ],
						'ecbSingleArchiveYearMoDayDescription' => $this->ecbSingleArchiveYearMoDayDescription[ $language->id ],
						'ecbSingleArchiveYearMoDayFullDesc' => $this->ecbSingleArchiveYearMoDayFullDesc[ $language->id ],
						
					);
				}
				
				$AR->setI18Ns( $i18ns );
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array( 'names', 'titles', 'metaTitles', 'metaDescs', 'metaKeyss', 'shortDescs', 'fullDescs', 'chartTitles', 'converterTitles', 'namePlurals', 'toName', 'informerName', 'singleArchiveYearTitle', 'singleArchiveYearMetaTitle', 'singleArchiveYearMetaDesc', 'singleArchiveYearMetaKeys', 'singleArchiveYearMoTitle', 'singleArchiveYearMoMetaTitle', 'singleArchiveYearMoMetaDesc', 'singleArchiveYearMoMetaKeys', 'singleArchiveYearMoDayTitle', 'singleArchiveYearMoDayMetaTitle', 'singleArchiveYearMoDayMetaDesc', 'singleArchiveYearMoDayMetaKeys', 'singleArchiveYearDescription', 'singleArchiveYearFullDesc', 'singleArchiveYearMoDescription', 'singleArchiveYearMoFullDesc', 'singleArchiveYearMoDayDescription', 'singleArchiveYearMoDayFullDesc', 'ecbTitle','ecbMetaTitle','ecbMetaDesc','ecbMetaKeys','ecbShortDesc','ecbFullDesc','ecbChartTitle','ecbConverterTitle','ecbSingleArchiveYearTitle','ecbSingleArchiveYearMetaTitle','ecbSingleArchiveYearMetaDesc','ecbSingleArchiveYearMetaKeys','ecbSingleArchiveYearMoTitle','ecbSingleArchiveYearMoMetaTitle','ecbSingleArchiveYearMoMetaDesc','ecbSingleArchiveYearMoMetaKeys','ecbSingleArchiveYearMoDayTitle','ecbSingleArchiveYearMoDayMetaTitle','ecbSingleArchiveYearMoDayMetaDesc','ecbSingleArchiveYearMoDayMetaKeys','ecbSingleArchiveYearDescription','ecbSingleArchiveYearFullDesc','ecbSingleArchiveYearMoDescription','ecbSingleArchiveYearMoFullDesc','ecbSingleArchiveYearMoDayDescription','ecbSingleArchiveYearMoDayFullDesc' ));
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}
?>