<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminUserActivitySettingsFormModel extends FormModelBase {
		public $count_user;
		public $count_contestMember;
		public $count_contestWiner;
		public $eval_contestMemberPlace;
		public $count_message;
		public $count_brokerReview;
		public $count_brokerMark;
		public $count_brokerLink;
		public $count_EAReview;
		public $count_EAMark;
		public $count_downloadEAVersion;
		public $count_EA;
		public $count_EAVersion;
		public $count_EAStatement;
		public $count_JournalComment;
		public $count_JournalDownload;
		public $count_JournalView;
		
		protected function getSourceAttributeLabels() {
			return Array(
					# common
				'count_user' => 'Registration user',
				'count_contestMember' => 'Registration in contest',
				'count_contestWiner' => 'Contest winer',
				'eval_contestMemberPlace' => 'Contest place (formula)',
				'count_message' => 'Message',
				'count_brokerReview' => 'Broker review',
				'count_brokerMark' => 'Broker mark',
				'count_brokerLink' => 'Broker client',
				'count_EAReview' => 'EA review',
				'count_EAMark' => 'EA mark',
				'count_downloadEAVersion' => 'Download EA',
				'count_EA' => 'Add new EA',
				'count_EAVersion' => 'Add new EA version',
				'count_EAStatement' => 'Add new EA statement',
				'count_JournalComment' => 'Journal comment',
				'count_JournalDownload' => 'Journal download',
				'count_JournalView' => 'Journal view',
			);
		}
		function rules() {
			return Array(
				Array( 'count_user, count_contestWiner, count_contestMember, count_message', 'numerical' ),
				Array( 'eval_contestMemberPlace', 'length', 'max' => CommonLib::maxByte ),
				Array( 'count_brokerReview, count_brokerMark, count_brokerLink', 'numerical' ),
				Array( 'count_EAReview, count_EAMark, count_downloadEAVersion', 'numerical' ),
				Array( 'count_EA, count_EAVersion, count_EAStatement', 'numerical' ),
				Array( 'count_JournalComment, count_JournalDownload, count_JournalView', 'numerical' ),
			);
		}
		function loadAR( $pk = 0 ) {
			$AR = UserActivitySettingsModel::getModel();
			$this->setAR( $AR );
			return true;
		}
		function load( $id = 0 ) {
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>