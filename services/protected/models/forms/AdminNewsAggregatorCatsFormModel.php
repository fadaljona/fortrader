<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminNewsAggregatorCatsFormModel extends FormModelBase {
		public $id;
		public $order;
		public $enabled;
		public $slug;
		public $ogImage;
		
		public $title;
		public $keys;
		public $stemedKeys;
		
		public $pageTitle;
		public $metaTitle;
		public $metaKeys;
		public $metaDesc;
		public $shortDesc;
		public $fullDesc;
		
		public $excludeFieldsFromAr = array( 'title', 'keys', 'stemedKeys', 'pageTitle', 'metaTitle', 'metaKeys', 'metaDesc', 'shortDesc', 'fullDesc' );
		
		public static function getTextFields(){
			return array(
				'title' => Array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'keys' => Array( 'modelField' => 'keys', 'type' => 'hiddenField', 'class' => 'wFullWidth', 'disabled' => false ),
				'stemedKeys' => Array( 'modelField' => 'stemedKeys', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => true ),
				
				'pageTitle' => Array( 'modelField' => 'pageTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'metaTitle' => Array( 'modelField' => 'metaTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'metaKeys' => Array( 'modelField' => 'metaKeys', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'metaDesc' => Array( 'modelField' => 'metaDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'shortDesc' => Array( 'modelField' => 'shortDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'fullDesc' => Array( 'modelField' => 'fullDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
			);
		}
		function getARClassName() {
			return "NewsAggregatorCatsModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'order' => 'Order',
				'slug' => 'Slug',
				'ogImage' => 'Og Image Link',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){			
				$labels[ "title[{$language->id}]" ] = "Category title";
				$labels[ "keys[{$language->id}]" ] = "Keywords";
				$labels[ "stemedKeys[{$language->id}]" ] = "Stemmed keywords";
				
				$labels[ "pageTitle[{$language->id}]" ] = "Page Title";
				$labels[ "metaTitle[{$language->id}]" ] = "Meta Title";
				$labels[ "metaKeys[{$language->id}]" ] = "Meta Keys";
				$labels[ "metaDesc[{$language->id}]" ] = "Meta Desc";
				$labels[ "shortDesc[{$language->id}]" ] = "Short Desc";
				$labels[ "fullDesc[{$language->id}]" ] = "Full Desc";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'order', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
				Array( 'enabled', 'boolean' ),
				Array( 'title', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'keys', 'checkLens', 'max' => CommonLib::maxWord ),
				Array( 'title', 'notEmptyCurrentLang' ),
				Array( 'slug', 'required' ),
				Array( 'slug, ogImage', 'length', 'max' => CommonLib::maxByte, 'min' => 2 ),
				Array( 'slug', 'uniqueField' ),
			);
		}
		function notEmptyCurrentLang( $field, $params ) {
			$NSi18n = $this->getNSi18n();	
			$currentLangId = LanguageModel::getCurrentLanguageID();
			
			if( mb_strlen( $this->{$field}[$currentLangId] ) == 0 ) {
				$this->addError( "{$field}[{$currentLangId}]", Yii::t( '*','{attribute} can not be empty.', Array( 
					'{attribute}' => $this->getAttributeLabel( "{$field}[{$currentLangId}]" ),
				)));
			}
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}

		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
					}
				}
								
				return true;
			}
			return false;
		}

		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					
					$arrToSave = Array();
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
					}
					$i18ns[ $language->id ] = (object)$arrToSave;
				}
				
				$AR->setI18Ns( $i18ns );
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				
				
				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>