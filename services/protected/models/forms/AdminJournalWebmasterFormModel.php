<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminJournalWebmasterFormModel extends FormModelBase {
		public $idWebmaster;
		public $url;
		public $createdDT;
		public $description;
		public $is_blocked;
		

		function getARClassName() {
			return "JournalWebmastersModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'number' => 'Number',
				'date' => 'Date',
				'status' => 'Hide Issue',
				'push_notification' => 'Push Notification',
			);
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'url', 'required' ),
			);
		}
		
		function loadFromPost() {
			if( parent::loadFromPost()) {
				$post = $this->getPostLink();
                /*
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					if( empty( $post[ 'prices' ][ $language->id ])) $this->prices[ $language->id ] = null;
				}*/
				
			}
		}
		function loadFromFiles( $loadFields = Array()) {
			parent::loadFromFiles( $loadFields );
			
			/*$keys = Array( 'covers', 'journals' );
			$languages = LanguageModel::getAll();
			foreach( $keys as $key ) {
				foreach( $languages as $language ){
					$file = CUploadedFile::getInstance( $this, "{$key}[{$language->id}]" );
					if( $file )	$this->{$key}[$language->id] = $file;
				}
			}*/
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'inner' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();

				$this->idWebmaster = $AR->idWebmaster;
                $this->url = $AR->url;
                $this->createdDT = $AR->createdDT;
                $this->description = $AR->description;
                $this->is_blocked = $AR->is_blocked;
								
				return true;
			}
			return false;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'inner' ) ) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				//$inner = @json_decode( $this->inner, true );
				//$AR->inner = serialize( $inner ? $inner : Array() );
				
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				/*$AR = $this->getAR();
				$i18ns = Array();
				/*$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'cover' => @$this->covers[ $language->id ],
						'name' => $this->names[ $language->id ],
						'digest' => $this->digests[ $language->id ],
						'countPages' => $this->countPageses[ $language->id ],
						'countDownloads' => $this->countDownloadses[ $language->id ] ?: 0,
						'journal' => @$this->journals[ $language->id ],
						'free' => @$this->frees[ $language->id ],
						'price' => @$this->prices[ $language->id ],
						'description' => @$this->descriptions[ $language->id ]
					);
				}*
				
				$AR->setI18Ns( $i18ns );*/
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['idWebmaster']) $id = (int)@$post['idWebmaster'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>