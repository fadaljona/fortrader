<?
Yii::import( 'models.base.SettingsFormModelBase' );

final class AdminCalendarEventSettingsFormModel extends SettingsFormModelBase{
	
	public $ogImage;
	
	public $brokerId = Array();
	public $title = Array();
	public $metaTitle = Array();
	public $metaDesc = Array();
	public $metaKeys = Array();
	public $firstDesc = Array();
	public $secondDesc = Array();
	
	public $textFields = array(
		'brokerId' => 'dropDownListRow',
		'title' => 'textFieldRow',
		'metaTitle' => 'textFieldRow',
		'metaDesc' => 'textAreaRow',
		'metaKeys' => 'textAreaRow',
		'firstDesc' => 'textAreaRow',
		'secondDesc' => 'textAreaRow',
	);

	protected function getSourceAttributeLabels() {
		$labels = Array(
			'ogImage' => 'Category list Og Image',
		);
		return $this->setTextLabels( $labels );
	}
	function rules() {
		return Array(
			Array( 'ogImage', 'length', 'max' => CommonLib::maxByte ),
			
			Array( 'title, metaTitle, metaDesc, metaKeys', 'checkLens', 'max' => CommonLib::maxByte ),
			Array( 'firstDesc, secondDesc', 'checkLens', 'max' => CommonLib::maxWord ),
		);
	}
	function loadAR( $pk = 0 ) {
		$AR = CalendarEventSettingsModel::getModel();
		$this->setAR( $AR );
		return true;
	}
}