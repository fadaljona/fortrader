<?
	Yii::import( 'models.base.FormModelBase' );

	final class CurrencyRatesConvertFormModel extends FormModelBase {
		public $id;
		public $idCurrencyFrom;
		public $idCurrencyTo;
		public $idPair;
		public $value;
		public $type;
        
        public $title;
        public $metaTitle;
		public $metaKeys;
		public $metaDesc;
		public $fullDesc;
		
		public $excludeFieldsFromAr = array( 'fullDesc', 'metaKeys', 'metaDesc', 'title', 'metaTitle' );
		
		function getARClassName() {
			return "CurrencyRatesConvertModel";
		}
		
		public static function getTextFields(){
			return array(
                'title' => Array( 'modelField' => 'title', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
                'metaTitle' => Array( 'modelField' => 'metaTitle', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'metaKeys' => Array( 'modelField' => 'metaKeys', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'metaDesc' => Array( 'modelField' => 'metaDesc', 'type' => 'textFieldRow', 'class' => 'wFullWidth', 'disabled' => false ),
				'fullDesc' => Array( 'modelField' => 'fullDesc', 'type' => 'textAreaRow', 'class' => 'wFullWidth', 'disabled' => false ),
			);
		}

		protected function getSourceAttributeLabels() {
			$labels = Array(
				'idCurrencyFrom' => 'Сurrency 1',
				'idCurrencyTo' => 'Сurrency 2',
				'value' => 'Value',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){	
                $labels[ "title[{$language->id}]" ] = "Title";
                $labels[ "metaTitle[{$language->id}]" ] = "Meta title";
				$labels[ "metaKeys[{$language->id}]" ] = "Meta Keys";
				$labels[ "metaDesc[{$language->id}]" ] = "Meta Description";
				$labels[ "fullDesc[{$language->id}]" ] = "Full Desc";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'id, idPair', 'numerical', 'integerOnly'=>true, 'min' => 0 ),
				Array( 'idCurrencyFrom, idCurrencyTo', 'numerical', 'integerOnly'=>true, 'min' => 1 ),
				Array( 'value', 'numerical', 'integerOnly'=>false ),
				Array( 'metaKeys, metaDesc, title, metaTitle', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'fullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
				Array('idCurrencyFrom', 'checkUniqueness'),
				Array('idCurrencyFrom, idCurrencyTo', 'checkCurrencyExist'),
				Array( 'type', 'in', 'range' => Array( 'cbr','ecb' )),
			);
		}
		function checkCurrencyExist( $field, $params ){
			if( $this->$field != 0 ){
				$model = CurrencyRatesModel::model()->find('id = ? AND noData = 0', array($this->$field));
				if($model == null)
					$this->addError( $field, Yii::t( '*','Currency does not exist') );
			}
		}
		function checkUniqueness($attribute,$params){
			$ARClassName = $this->getARClassName();
            $model = $ARClassName::model()->find('idCurrencyFrom = ? AND idCurrencyTo = ? AND value = ? AND type = ?', array($this->idCurrencyFrom, $this->idCurrencyTo, $this->value, $this->type));
            if($model != null && $model->id != $this->id)
                $this->addError('idCurrencyFrom', Yii::t( '*','This idCurrencyFrom, idCurrencyTo, value and type  already exist') );
         
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}

		
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = array() ) {
			$exceptFields = $this->excludeFieldsFromAr;
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$this->{$formFieldName}[ $i18n->idLanguage ] = $i18n->{$settings['modelField']};
					}
				}
								
				return true;
			}
			return false;
		}

		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					
					$arrToSave = Array();
					
					foreach( $this->textFields as $formFieldName => $settings ){
						$arrToSave[ $settings['modelField'] ] = $this->{$formFieldName}[ $language->id ];
					}
					$i18ns[ $language->id ] = (object)$arrToSave;
				}
				
				$AR->setI18Ns( $i18ns );
				return true;
			}
			return false;
		}
	
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				$this->loadFromFiles();
				
				$this->setIdPair();
				
				return true;
			}
			return false;
		}
		
		function loadARByParams( $idCurrencyFrom, $idCurrencyTo, $value, $type ) {
			$ARClassName = $this->getARClassName();
			
			$AR = CActiveRecord::model( $ARClassName )->find(array( 
				'condition' => ' `idCurrencyFrom` = :idCurrencyFrom AND `t`.`idCurrencyTo` = :idCurrencyTo AND `t`.`value` = :value AND `t`.`type` = :type ',
				'params' => array( ':idCurrencyFrom' => $idCurrencyFrom, ':idCurrencyTo' => $idCurrencyTo, ':value' => $value, ':type' => $type )
			));
			if( !$AR ) $AR = new $ARClassName();
			
			$this->setAR( $AR );
			return true;
		}
		function setIdPair(){
			if( !$this->idPair ) $this->idPair = CurrencyRatesConvertModel::defineIdPair( $this->idCurrencyFrom, $this->idCurrencyTo, $this->type );
		}
		function loadByParams( $idCurrencyFrom, $idCurrencyTo, $value, $type ) {
			
			if( !isset($idCurrencyFrom) || !isset($idCurrencyTo) || !isset($value) || !isset($type) ) return false;

			if( $this->loadARByParams( $idCurrencyFrom, $idCurrencyTo, $value, $type )) {
				$this->loadFromAR();
				
				$this->idCurrencyFrom = $idCurrencyFrom;
				$this->idCurrencyTo = $idCurrencyTo;
				$this->value = $value;
				$this->type = $type;
				
				$this->setIdPair();

				return true;
			}
			return false;
		}

		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR( null, Array(), Array());
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>