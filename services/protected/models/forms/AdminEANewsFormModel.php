<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminEANewsFormModel extends FormModelBase {
		public $id;
		public $name;
		public $news = Array();
		public $date;
		function getARClassName() {
			return "EANewsModel";
		}
		protected function getSourceAttributeLabels() {
			$labels = Array(
				'name' => 'Title',
				'date' => 'Date',
			);
			
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ){
				$labels[ "news[{$language->id}]" ] = "News";
			}
			
			return $labels;
		}
		function rules() {
			return Array(
				Array( 'name, date', 'required' ),
				Array( 'date', 'date', 'format' => 'yyyy-mm-dd' ),
				Array( 'news', 'validateNews' ),
				Array( 'news', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
		function validateNews() {
			foreach( $this->news as $idLanguage=>$name ) {
				if( strlen( $name )) return;
			}
			$NSi18n = $this->getNSi18n();
			$this->addError( "news", Yii::t( 'yii','{attribute} cannot be blank.', Array( '{attribute}' => Yii::t( $NSi18n, 'News' ))));
		}
		function checkLens( $field, $params ) {
			$NSi18n = $this->getNSi18n();
			foreach( $this->$field as $idLanguage=>$value ) {
				if( mb_strlen( $value ) > $params['max'] ) {
					$this->addError( $field, Yii::t( 'yii','{attribute} is too long (maximum is {max} characters).', Array( 
						'{attribute}' => $this->getAttributeLabel( "{$field}[{$idLanguage}]" ),
						'{max}' => $params['max'],
					)));
				}
			}
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'news' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$i18ns = $AR->i18ns;
				foreach( $i18ns as $i18n ) {
					$this->news[ $i18n->idLanguage ] = $i18n->news;
					//var_dump( $i18n->news ); 
				}
				//exit;
				return true;
			}
			return false;
		}
		function saveAR( $runValidation = true, $attributes = null ) {
			if( parent::saveAR( $runValidation, $attributes )) {
				$AR = $this->getAR();
						
				$i18ns = Array();
				$languages = LanguageModel::getAll();
				foreach( $languages as $language ) {
					$i18ns[ $language->id ] = (object)Array(
						'news' => $this->news[ $language->id ],
					);
				}
				$AR->setI18Ns( $i18ns );				
				
				return true;
			}
			return false;
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>