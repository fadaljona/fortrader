<?
	Yii::import( 'models.base.AdminTextContentFormModelBase' );

	final class AdminQuotesSymbolsTextContentFormModel extends AdminTextContentFormModelBase {
		
		protected $textModelName = 'QuotesSymbolsI18NModel';
		protected $idLanguageFieldName = 'idLanguage';
		protected $idTextModelFieldName = 'idSymbol';
		protected $idArModelFieldName = 'id';
		
		public $id;
		
		# texts
		public $nalias = Array();
		public $desc = Array();
		public $title = Array();
		public $informerName = Array();
		public $metaTitle = Array();
		public $metaDescription = Array();
		public $tooltip = Array();
		public $preview = Array();
		public $chartTitle = Array();
		public $wpPostsTitle = Array();
		public $metaKeywords = Array();
		public $chartLabel = Array();
		public $searchPhrases = Array();

		
		public $textFields = array(
			'nalias' => 'textFieldRow',
			'desc' => 'textAreaRow',
			'title' => 'textFieldRow',
			'informerName' => 'textFieldRow',
			'metaTitle' => 'textFieldRow',
			'metaDescription' => 'textAreaRow',
			'tooltip' => 'textFieldRow',
			'preview' => 'textAreaRow',
			'chartTitle' => 'textFieldRow',
			'wpPostsTitle' => 'textFieldRow',
			'metaKeywords' => 'textFieldRow',
			'chartLabel' => 'textFieldRow',
			'searchPhrases' => 'textFieldRow',
		);
		
		function getARClassName() {
			return "QuotesSymbolsModel";
		}
		
		function rules() {
			return Array(

				Array( 'nalias, title, informerName, metaTitle, tooltip, chartTitle, wpPostsTitle, metaKeywords, chartLabel, searchPhrases', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'desc, preview, metaDescription', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
		
		protected function getSourceAttributeLabels() {
			$languages = LanguageModel::getAll();
			$labels = array();
			foreach( $languages as $language ){
				foreach( $this->textFields as $key => $val ){
					if( $key == 'preview' ){
						$labels[ "preview[{$language->id}]" ] = 'Превью. Доступные переменные - {$high}, {$low}, {$open}, {$close}, {$ch}, {$ch%}, {$price}';
					}else{
						$labels[ "{$key}[{$language->id}]" ] = Yii::t('*', str_replace('_', ' ', join( preg_split('/(?<=[a-z])(?=[A-Z])/x', ucfirst($key)), " " )));
					}
				}
			}
			return $labels;
		}
	}

?>
