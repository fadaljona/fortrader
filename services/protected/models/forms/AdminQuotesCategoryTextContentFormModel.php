<?
	Yii::import( 'models.base.AdminTextContentFormModelBase' );

	final class AdminQuotesCategoryTextContentFormModel extends AdminTextContentFormModelBase {
		
		protected $textModelName = 'QuotesCategoryI18nModel';
		protected $idLanguageFieldName = 'idLanguage';
		protected $idTextModelFieldName = 'idCat';
		protected $idArModelFieldName = 'id';
		
		public $id;
		
		# texts
		public $name;
		public $desc;
		public $fullDesc;
		public $title;
		public $metaTitle;
		public $metaDescription; 
		public $metaKeywords;
		public $shortDesc;

		
		public $textFields = array(
			'name' => 'textFieldRow',
			'desc' => 'textAreaRow',
			'fullDesc' => 'textAreaRow',
			'title' => 'textFieldRow',
			'metaTitle' => 'textFieldRow',
			'metaDescription' => 'textFieldRow',
			'metaKeywords' => 'textFieldRow',
			'shortDesc' => 'textAreaRow',
		);
		
		function getARClassName() {
			return "QuotesCategoryModel";
		}
		
		function rules() {
			return Array(

				Array( 'name, title, metaTitle, metaKeywords', 'checkLens', 'max' => CommonLib::maxByte ),
				Array( 'desc, fullDesc, metaDescription, shortDesc', 'checkLens', 'max' => CommonLib::maxWord ),
			);
		}
		
		
	}

?>
