<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminFeedbackFormModel extends FormModelBase {
		public $id;
		public $user;
		public $email;
		public $idType;
		public $message;
		function getARClassName() {
			return "FeedbackModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'user' => 'User',
				'email' => 'Email',
				'idType' => 'Type',
				'message' => 'Message',
			);
		}
		function rules() {
			return Array(
				Array( 'user, email, idType, message', 'required' ),
				Array( 'user, email', 'length', 'max' => CommonLib::maxByte ),
				Array( 'message', 'length', 'max' => CommonLib::maxWord ),
				Array( 'idType', 'existsIDModel', 'model' => 'FeedbackTypeModel' ),
			);
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>