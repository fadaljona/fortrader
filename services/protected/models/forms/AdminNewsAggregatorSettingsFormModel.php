<?
Yii::import( 'models.base.SettingsFormModelBase' );

final class AdminNewsAggregatorSettingsFormModel extends SettingsFormModelBase{
	
	public $storedNewsCount;
	public $ogNewsImage;
	public $ogSourceImage;
	public $defaultCat;

	public $stemmerLangConstant = Array();
	
	public $newsTitle = Array();
	public $newsMetaTitle = Array();
	public $newsMetaDesc = Array();
	public $newsMetaKeys = Array();
	public $newsShortDesc = Array();
	public $newsFullDesc = Array();
		
	public $sourceTitle = Array();
	public $sourceMetaTitle = Array();
	public $sourceMetaDesc = Array();
	public $sourceMetaKeys = Array();
	public $sourceShortDesc = Array();
	public $sourceFullDesc = Array();
	

	
	public $textFields = array(
		'stemmerLangConstant' => 'textFieldRow',
		
		'newsTitle' => 'textFieldRow',
		'newsMetaTitle' => 'textFieldRow',
		'newsMetaDesc' => 'textAreaRow',
		'newsMetaKeys' => 'textAreaRow',
		'newsShortDesc' => 'textAreaRow',
		'newsFullDesc' => 'textAreaRow',
		
		'sourceTitle' => 'textFieldRow',
		'sourceMetaTitle' => 'textFieldRow',
		'sourceMetaDesc' => 'textAreaRow',
		'sourceMetaKeys' => 'textAreaRow',
		'sourceShortDesc' => 'textAreaRow',
		'sourceFullDesc' => 'textAreaRow',
	);

	protected function getSourceAttributeLabels() {
		$labels = Array(
			'storedNewsCount' => 'Stored News Count',
			'ogNewsImage' => 'og News Image',
			'ogSourceImage' => 'og Source Image',
			'defaultCat' => 'Default category',
		);
		return $this->setTextLabels( $labels );
	}
	function rules() {
		return Array(
			Array( 'storedNewsCount', 'numerical', 'integerOnly' => true, 'min' => 1 ),
			Array( 'defaultCat', 'numerical', 'integerOnly' => true, 'min' => 0 ),
			Array( 'ogNewsImage, ogSourceImage', 'length', 'max' => CommonLib::maxByte ),
			Array( 'stemmerLangConstant', 'stemmerLangCheck' ),
			Array( 'newsTitle, newsMetaTitle, newsMetaDesc, newsMetaKeys, sourceTitle, sourceMetaTitle, sourceMetaDesc, sourceMetaKeys, stemmerLangConstant', 'checkLens', 'max' => CommonLib::maxByte ),
			Array( 'newsShortDesc, newsFullDesc, sourceShortDesc, sourceFullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
		);
	}
	function stemmerLangCheck() {
		$stemConstatns = array( 'STEM_DANISH' ,'STEM_DUTCH' ,'STEM_ENGLISH' ,'STEM_FINNISH' ,'STEM_FRENCH' ,'STEM_GERMAN' ,'STEM_HUNGARIAN' ,'STEM_ITALIAN' ,'STEM_NORWEGIAN' ,'STEM_PORTUGUESE' ,'STEM_ROMANIAN' ,'STEM_RUSSIAN' ,'STEM_RUSSIAN_UNICODE' ,'STEM_SPANISH' ,'STEM_SWEDISH' ,'STEM_TURKISH_UNICODE' );

		foreach( $this->stemmerLangConstant as $idLanguage=>$langConstant ) {
			if( !in_array($langConstant, $stemConstatns) ) {
				$this->addError( "stemmerLangConstant", Yii::t( '*','{attribute} must be one of {constString}).', Array( 
					'{attribute}' => $this->getAttributeLabel( "stemmerLangConstant[{$idLanguage}]" ),
					'{constString}' => implode(', ', $stemConstatns),
				)));
			}
		}
	}
	function loadAR( $pk = 0 ) {
		$AR = NewsAggregatorSettingsModel::getModel();
		$this->setAR( $AR );
		return true;
	}
}