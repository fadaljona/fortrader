<?
Yii::import( 'models.base.SettingsFormModelBase' );

final class AdminInterestRatesSettingsFormModel extends SettingsFormModelBase{
	
	public $ogImage;
	public $lastYearsToShowOnChart;
	
	public $title = Array();
	public $metaTitle = Array();
	public $metaDesc = Array();
	public $metaKeys = Array();
	public $shortDesc = Array();
	public $fullDesc = Array();
	
	public $textFields = array(

		'title' => 'textFieldRow',
		'metaTitle' => 'textFieldRow',
		'metaDesc' => 'textAreaRow',
		'metaKeys' => 'textAreaRow',
		'shortDesc' => 'textAreaRow',
		'fullDesc' => 'textAreaRow',

	);

	protected function getSourceAttributeLabels() {
		$labels = Array(
			'ogImage' => 'og Image',
			'lastYearsToShowOnChart' => 'Years To Show On Chart',
		);
		return $this->setTextLabels( $labels );
	}
	function rules() {
		return Array(
			Array( 'ogImage', 'length', 'max' => CommonLib::maxByte ),
			Array( 'title, metaTitle, metaDesc, metaKeys', 'checkLens', 'max' => CommonLib::maxByte ),
			Array( 'shortDesc, fullDesc', 'checkLens', 'max' => CommonLib::maxWord ),
			Array( 'lastYearsToShowOnChart', 'numerical', 'integerOnly'=>true, 'min' => 1 ),
		);
	}
	function loadAR( $pk = 0 ) {
		$AR = InterestRatesSettingsModel::getModel();
		$this->setAR( $AR );
		return true;
	}
}