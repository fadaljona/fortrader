<?
	Yii::import( 'models.base.FormModelBase' );

	final class AdminAdvertisementZoneFormModel extends FormModelBase {
		public $id;
		public $name;
		public $site;
		public $typeAds;
		public $typeBlock;
		public $sizeBlock;
		public $countAds;
		public $typeBorder;
		public $colorBorder;
		public $roundedShapeBlocks;
		public $colorHeader;
		public $colorHoverHeader;
		public $colorBG;
		public $colorText;
		public $colorURL;
		public $colorHoverURL;
		public $font;
		public $sizeHeader;
		public $sizeText;
		public $clearStats;
		public $mixedZone;
		
		const MODE_DEFAULT = 0;
		const MODE_PREVIEW = 1;
		protected $mode = self::MODE_DEFAULT;
		function getARClassName() {
			return "AdvertisementZoneModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'name' => 'Title',
				'site' => 'Site',
				'typeAds' => 'Type ads',
				'typeBlock' => 'Block type',
				'sizeBlock' => 'Block size',
				'countAds' => 'Count ads',
				'typeBorder' => 'Border type',
				'colorBorder' => 'Border color',
				'roundedShapeBlocks' => 'Rounded shape of the blocks',
				'colorHeader' => 'Header color',
				'colorHoverHeader' => 'Hover header color',
				'colorBG' => 'Background color',
				'colorText' => 'Text color',
				'colorURL' => 'Link color',
				'colorHoverURL' => 'Hover link color',
				'font' => 'Font',
				'sizeHeader' => 'Header size',
				'sizeText' => 'Text size',
				'clearStats' => 'Clear stats',
				'mixedZone' => 'Mixed zone?'
			);
		}
		function rules() {
			$rules = Array();
			if( $this->mode == self::MODE_DEFAULT ) {
				$rules[] = Array( 'name', 'required' );
			}
			$rules[] = Array( 'typeBlock, typeBorder, roundedShapeBlocks, font, sizeHeader, sizeText', 'required' );
			$rules[] = Array( 'typeAds', 'in', 'range' => Array( 'All', 'Text', 'Media', 'Buttons', 'Offers' ));
			$rules[] = Array( 'colorBorder, colorHeader, colorHoverHeader, colorBG, colorText, colorURL, colorHoverURL', 'required' );
			$rules[] = Array( 'name, font, site', 'length', 'max' => CommonLib::maxByte );
			$rules[] = Array( 'name', 'uniqueField' );
			$rules[] = Array( 'typeBlock', 'in', 'range' => Array( 'Horizontal', 'Vertical', 'Fixed', 'Popup' ));
			if( $this->typeBlock == 'Fixed' ) $rules[] = Array( 'sizeBlock', 'in', 'range' => self::getSizesBlock() );
			if( $this->typeBlock != 'Fixed' ) $rules[] = Array( 'countAds', 'numerical', 'min' => 1, 'allowEmpty' => false );
			$rules[] = Array( 'typeBorder', 'in', 'range' => Array( 'None', 'All', 'Each' ));
			$rules[] = Array( 'colorBorder, colorHeader, colorHoverHeader, colorBG, colorText, colorURL, colorHoverURL', 'match', 'pattern' => '/^#[0-9a-f]{6}$/' );
			$rules[] = Array( 'roundedShapeBlocks, mixedZone', 'in', 'range' => Array( '0', '1' ));
			$rules[] = Array( 'sizeHeader, sizeText', 'numerical', 'min' => 1, 'max' => 30 );
			
			return $rules;
		}
		static function getSizesBlock() {
			$source = AdvertisementZoneModel::getZoneSizesBlock();
			$sizes = Array();
			foreach( $source as $group=>$array ) {
				$sizes = array_merge( $sizes, $array );
			}
			return $sizes;
		}
		function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'sizeBlock' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				$this->sizeBlock = $AR->getSizeBlock();
			}
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array( 'sizeBlock' )) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				if( $this->typeBlock == 'Fixed' ) {
					$AR->countAds = null;
					$AR->setSizeBlock( $this->sizeBlock );
				}
				else{
					$AR->clearSizeBlock();
				}
			}
		}
		function loadFromPost() {
			if( parent::loadFromPost()) {
				if( !$this->site ) $this->site = 'fortrader';
			}
		}
		function load( $id = 0 ) {
			$post = $this->getPostLink();
			if( (int)@$post['id']) $id = (int)@$post['id'];
			
			if( $this->loadAR( $id )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				if( $this->clearStats ){
					$AR->clearStats();
				}
				return $AR->getPrimaryKey();
			}
			return false;
		}
		function setMode( $mode ) {
			$this->mode = $mode;
		}
	}

?>