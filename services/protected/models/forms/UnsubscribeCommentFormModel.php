<?
	Yii::import( 'models.base.FormModelBase' );

	final class UnsubscribeCommentFormModel extends FormModelBase {
		public $id;
		public $idMailingType;
		public $comment;
		protected $idUser;
		function getARClassName() {
			return "UnsubscribeCommentModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'login' => 'User',
				'idMailingType' => 'Type mailing',
				'comment' => 'Comment',
			);
		}
		function rules() {
			return Array(
				Array( 'idMailingType, comment', 'required' ),
				Array( 'comment', 'length', 'max' => CommonLib::maxWord ),
				Array( 'idMailingType', 'existsIDModel', 'model' => 'MailingTypeModel' ),
			);
		}
		function setIDUser( $idUser ) {
			$this->idUser = $idUser;
		}
		function loadAR( $pk = 0 ) {
			$postLink = $this->getPostLink();
			$AR = UnsubscribeCommentModel::model()->findByAttributes( Array(
				'idUser' => $this->idUser,
				'idMailingType' => $this->idMailingType ? $this->idMailingType : $postLink['idMailingType'],
			));
			if( !$AR ) $AR = new UnsubscribeCommentModel();
			$this->setAR( $AR );
			return true;
		}
		function saveToAR( $AR = null, $saveFields = Array(), $exceptFields = Array() ) {
			if( parent::saveToAR( $AR, $saveFields, $exceptFields )) {
				$AR = $this->getAR();
				
				$AR->idUser = $this->idUser;
			}
		}
		function load( $id = 0 ) {
			if( $this->loadAR()) {
				$AR = $this->getAR();
				$this->loadFromAR();
				$this->loadFromPost(); 
				return true;
			}
			return false;
		}
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
			$this->saveToAR();
			
			if( $this->saveAR( false )) {
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>