<?
	Yii::import( 'models.base.FormModelBase' );

	final class CalendarGroupSubscriptionFormModel extends FormModelBase {

		public $idUser;
        public $timeBeforeEvent;
        public $countriesInp;
		public $serious;
		public $idLanguage;
		
		function getARClassName() {
			return "CalendarGroupSubscriptionModel";
		}
		protected function getSourceAttributeLabels() {
			return Array(
				'timeBeforeEvent' => 'Time to start event',
			);
		}
		function rules() {
			return Array(
				Array( 'timeBeforeEvent', 'numerical', 'integerOnly' => true, 'min' => 1 ),
                Array( 'timeBeforeEvent, serious', 'required' ),
                Array( 'serious', 'length', 'min' => 1 ),
                Array( 'countriesInp', 'validateCountries' ),
			);
		}
        
        function validateCountries($attribute,$params){
			if( $this->{$attribute} ){
                $countriesArr = explode(',', $this->{$attribute});
                if( !$countriesArr ) $this->addError($attribute, Yii::t( '*','Wrong format') );
            }
		}
        function loadAR( $pk ) {
			$ARClassName = $this->getARClassName();
			$AR = CActiveRecord::model( $ARClassName )->findByPk( $pk );
			if( !$AR ){
                $AR = new $ARClassName();
                $AR->idUser = $pk;
            }
			$this->setAR( $AR );
			return true;
		}
        function load( $pk ) {
			if( $this->loadAR( $pk )) {
				$this->loadFromAR();
				$this->loadFromPost();
				return true;
			}
			return false;
        }
        function loadFromAR( $AR = null, $loadFields = Array(), $exceptFields = Array( 'image' )) {
			if( parent::loadFromAR( $AR, $loadFields, $exceptFields )) {
				$AR = $this->getAR();
				
				if( $AR->countries ) {
                    $this->countriesInp = array();
                    foreach( $AR->countries as $country )  $this->countriesInp[] = $country->countryCode;
                    $this->countriesInp = implode(',', $this->countriesInp);
                }
                if( $AR->seriouses ) {
                    $this->serious = array();
                    foreach( $AR->seriouses as $serious )  $this->serious[] = $serious->serious;
                    $this->serious = implode(',', $this->serious);
				}
				return true;
			}
			return false;
		}
		
		function save() {
			$AR = $this->getAR();
			if( !$AR ) return false;
			
            $this->saveToAR( null, Array(), Array('countriesInp', 'serious'));
			
			if( $this->saveAR( false )) {
                $AR->setCountries( $this->countriesInp );
                $AR->setSerious( $this->serious );
				return $AR->getPrimaryKey();
			}
			return false;
		}
	}

?>