<?
	Yii::import( 'models.base.SettingsModelBase' );
	
	final class MonitoringSettingsModel extends SettingsModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) {
					$model = new self();
					$model->id = 1;
				}
			}
			return $model;
		}
		
		public function getTitle(){ return $this->getTextSetting('title'); }
		public function getMetaTitle(){ return $this->getTextSetting('metaTitle'); }
		public function getMetaDesc(){ return $this->getTextSetting('metaDesc'); }
		public function getMetaKeys(){ return $this->getTextSetting('metaKeys'); }
		public function getShortDesc(){ return $this->getTextSetting('shortDesc'); }
		public function getFullDesc(){ return $this->getTextSetting('fullDesc'); }
		
		public function getOgImageSrc(){
			return CommonLib::getCommonImageSrc($this->ogImage);
		}

	}
?>