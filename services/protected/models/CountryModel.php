<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CountryModel extends ModelBase {
		const CACHE_KEY_PREFIX = 'CountryModel.';
		const CACHE_KEY_FOR_CALENDAR_FILTER = 'CACHE_KEY_FOR_CALENDAR_FILTER';
		const PATHUploadDir = 'uploads/country';
		const PATHFilesUploadDir = 'country';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'users' => Array( self::HAS_MANY, 'UserModel', 'idCountry' ),
			);
		}
		function getIcon($class = null) {
			$alias = strtoupper( $this->alias );
			$class = "ficon-{$alias}".$class;
			$name = Yii::t( '*', $this->name );
			$tag = CHtml::openTag( 'i', Array( 'class' => $class, 'title' => $name ));
			$tag .= CHtml::closeTag( 'i' );
			return $tag;
		}

		static public function getIconPic($name, $alias){
			$alias = strtoupper( $alias );
			$class = "ficon-{$alias}";
			$name = Yii::t( '*', $name );
			$tag = CHtml::openTag( 'i', Array( 'class' => $class, 'alt' => $name, 'title' => $name ));
			$tag .= CHtml::closeTag( 'i' );
			return $tag;
		}
		
		static function getAll( $cached = true, $i18n = true, $sorted = true ) {
			$cache = Yii::app()->cache;
			$cacheKey = self::CACHE_KEY_PREFIX."arrayCountries_{$i18n}_{$sorted}";
			if( $cache and $cached ) {
				if(( $data = $cache->get( $cacheKey )) !== false ) {
					return unserialize( $data );
				}
			}
			
			$NSi18n = self::getModelNSi18n( __CLASS__ );
			$countries = Array();
			
			$models = self::model()->findAll();
			foreach( $models as $model ) {
				$countries[ $model->id ] = $i18n ? Yii::t( $NSi18n, $model->name ) : $model->name;
			}
			if( $sorted ) asort( $countries );
			
			foreach( $models as $model ) {
				$countries[ $model->id ] = (object)Array(
					'alias' => $model->alias,
					'name' => $countries[ $model->id ],
				);
			}
			
			if( $cache and $cached ) {
				$dependency = new CDbCacheDependency( " SELECT COUNT(*), MAX( `updatedDT` ) FROM `{{country}}` " );
				$cache->set( $cacheKey, serialize( $countries ), CommonLib::secInDay, $dependency );
			}
			
			return $countries;
		}

		/*static function getAllExist() {
			$NSi18n = self::getModelNSi18n( __CLASS__ );
			$countries = Array();
			
			$models = self::model()->findAll();
			foreach( $models as $model ) {
				$countries[ $model->id ] = $i18n ? Yii::t( $NSi18n, $model->name ) : $model->name;
			}
			if( $sorted ) asort( $countries );
			
			foreach( $models as $model ) {
				if ( CalendarEventI18NModel::model()->count('country="'.$model->name.'"') > 0 ) {
					$countries[ $model->id ] = (object)Array(
						'alias' => $model->alias,
						'name' => $countries[ $model->id ],
					);
				}
			}
			
			return $countries;
		}*/

		static function getAllExist() {
			$cacheKey = self::CACHE_KEY_FOR_CALENDAR_FILTER . '.lang.' . Yii::app()->language;
			if( $countries = Yii::app()->cache->get( $cacheKey ) ) return $countries;
			
			$countries = Array();
			$crit = new CDbCriteria;
			$crit->distinct = true;
			$crit->select = "countrycode";
			$crit->with['country'] = array();

			$rows = CalendarEvent2Model::model()->findAll($crit);

			foreach ($rows as $one) {
	
				if ( $one->country ) {
					$countries[ $one->country->id ] = (object)Array(
						'alias' => $one->countrycode,
						'name' => Yii::t( '*', $one->country->name ),
						'img16' => $one->country->getSrcFlag('shiny', 16)
					);
				}
			}
			Yii::app()->cache->set( $cacheKey, $countries, 60*60);
			return $countries;
		}

		public static function getNameById( $id ) {
			$model = self::model()->findByPk($id, '', $params=array('select'=>'name'));
			if ( $model != null ) {
				return $model->name;
			}
			return false;
		}
		
		/* $type  shiny | flat */
		public function getSrcFlag($type, $size){
			$file = self::PATHUploadDir."/flags/$type/$size/" . strtoupper( $this->alias ) . '.png';
			if( !file_exists( Yii::getPathOfAlias('webroot') . '/' . $file ) ) return false;
			if( !isset( Yii::app()->params['uploadsUrl'] ) ) return Yii::app()->baseUrl . '/' . $file;
			return Yii::app()->params['uploadsUrl'] . '/' . self::PATHFilesUploadDir ."/flags/$type/$size/" . strtoupper( $this->alias ) . '.png';
		}
		public function getImgFlag($type, $size){
			$src = $this->getSrcFlag($type, $size);
			if( !$src ) return '';
			return CHtml::image($src, Yii::t( '*', $this->name ), array('title' => Yii::t( '*', $this->name ) ));
		}
		static function getSrcFlagByAlias($type, $size, $alias){
			$file = self::PATHUploadDir."/flags/$type/$size/" . strtoupper( $alias ) . '.png';
			if( !file_exists( Yii::getPathOfAlias('webroot') . '/' . $file ) ) return false;
			if( !isset( Yii::app()->params['uploadsUrl'] ) ) return Yii::app()->baseUrl . '/' . $file;
			return Yii::app()->params['uploadsUrl'] . '/' . self::PATHFilesUploadDir ."/flags/$type/$size/" . strtoupper( $alias ) . '.png';
		}
		static function getImgFlagByAlias($type, $size, $alias){
			$src = self::getSrcFlagByAlias($type, $size, $alias);
			if( !$src ) return $alias;
			return CHtml::image($src, $alias, array());
		}

		public function getLocalName(){
			return Yii::t( '*', $this->name );
		}
			# users
		function deleteUsers() {
			foreach( $this->users as $model ) {
				$model->delete();
			}
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteUsers();
		}
	}

?>
