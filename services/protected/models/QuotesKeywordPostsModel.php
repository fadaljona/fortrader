<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesKeywordPostsModel extends ModelBase {
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		protected function afterSave() {
			parent::afterSave();
			CommonLib::loadWp();
				
			$settings = SettingsModel::getModel();
			$postCats = wp_get_post_categories( $this->postId );
			if( in_array($settings->servicesPostsCat, $postCats) ) return true;		
	
			$keywordModel = QuotesSymbolKeywordModel::model()->findByPk($this->keywordId);
			if( !$keywordModel->symbol->getI18NbyLangId( $keywordModel->idLanguage )->wpTerm ){
				$keywordModel->symbol->createTaxonomy( $keywordModel->idLanguage );
			}else{
				$wpTerm = get_term( $keywordModel->symbol->getI18NbyLangId( $keywordModel->idLanguage )->wpTerm, 'quotes' );
				if( !$wpTerm || is_wp_error( $wpTerm ) ) {
					$keywordModel->symbol->createTaxonomy( $keywordModel->idLanguage );
				}
			}
			
			wp_set_post_terms( $this->postId, array($keywordModel->symbol->getI18NbyLangId( $keywordModel->idLanguage )->wpTerm), 'quotes', true );
		}

		function rules() {
			return Array(
				Array( 'keywordId, postId', 'required' ),
				Array( 'keywordId, postId', 'numerical', 'integerOnly' => true, 'min' => 1 ),
				Array( 'keywordId', 'unique', 'criteria'=>array(
					'condition' => '`postId`=:secondKey',
					'params' => array(
						':secondKey' => $this->postId
					)
				)),
			);
		}
		public function tableName(){
			return '{{quotes_keyword_posts}}';
		}
	}
?>