<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementZoneClickModel extends ModelBase {
		public $blockIP;
		public $blockAgent;
		public $count;
		public $clickDate;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'advertisement' => Array( self::BELONGS_TO, 'AdvertisementModel', 'idAd' ),
			);
		}
		static function instance( $idZone ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idZone' ));
		}
		static function clearClicks( $idZone ) {
			Yii::App()->db->createCommand( "
				DELETE
				FROM			{{advertisement_zone_click}} 
				WHERE			`idZone` = :idZone
			" )->query(Array(
				':idZone' => $idZone,
			));
		}
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( $this->isNewRecord ) {
					$this->ip = sprintf( "%u", ip2long( $_SERVER[ 'REMOTE_ADDR' ]));
					$this->agent = $_SERVER[ 'HTTP_USER_AGENT' ];
				}
			}
			return $result;
		}
	}

?>