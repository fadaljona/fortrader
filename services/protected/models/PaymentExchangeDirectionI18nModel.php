<?php

Yii::import('models.base.ModelBase');

final class PaymentExchangeDirectionI18nModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return "{{payment_exchange_direction_i18n}}";
    }

    public static function instance($idDirection, $idLanguage, $title, $metaTitle, $metaDesc, $metaKeys, $shortDesc, $fullDescTitle, $fullDesc)
    {
        return self::modelFromAssoc(__CLASS__, compact('idDirection', 'idLanguage', 'title', 'metaTitle', 'metaDesc', 'metaKeys', 'shortDesc', 'fullDescTitle', 'fullDesc'));
    }
}
