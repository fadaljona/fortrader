<?
	Yii::import( 'models.base.ModelBase' );
	
	final class EAMonitoringServerModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_monitoring_server}}";
		}
		function relations() {
			return Array(
				'EATradeAccounts' => Array( self::HAS_MANY, 'EATradeAccountModel', 'idEAMonitoringServer' ),
			);
		}
			# EATradeAccounts
		function deleteEATradeAccounts() {
			foreach( $this->EATradeAccounts as $account ) {
				$account->delete();
			}
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteEATradeAccounts();
		}
	}

?>