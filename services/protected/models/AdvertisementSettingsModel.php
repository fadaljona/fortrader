<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementSettingsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) $model = new self();
			}
			return $model;
		}
	}

?>