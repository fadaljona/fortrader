<?
	Yii::import( 'models.base.ModelBase' );
	
	final class TradePlatformModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'servers' => Array( self::HAS_MANY, 'ServerModel', 'idTradePlatform' ),
				'linksToBrokers' => Array( self::HAS_MANY, 'BrokerToTradePlatformModel', 'idPlatform' ),
			);
		}
			
			# servers
		function deleteServers() {
			foreach( $this->servers as $model ) {
				$model->delete();
			}
		}
		
			# brokers
		function unlinkFromBrokers() {
			foreach( $this->linksToBrokers as $link ) {
				$link->delete();
			}
		}

    static public function getPlatforms($criteria = null) {
      $platforms = array();
      $criteria = $criteria === null ? new CDbCriteria() : $criteria;
      $criteria->order = '`t`.`name`';
      $models = static::model()->findAll($criteria);
      foreach($models as $model) {
        $platforms[ $model->id ] = $model->name;
      }

      return $platforms;
    }

    
		
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteServers();
			$this->unlinkFromBrokers();
		}
	}

?>