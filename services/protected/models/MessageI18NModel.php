<?
	Yii::import( 'models.base.ModelBase' );
	
	final class MessageI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{message_i18n}}";
		}
		static function instance( $idMessage, $idLanguage, $value ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idMessage', 'idLanguage', 'value' ));
		}
	}

?>