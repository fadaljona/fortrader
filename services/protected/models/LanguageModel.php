<?
	Yii::import( 'models.base.ModelBase' );
	
	final class LanguageModel extends ModelBase {
		const englishID = 0;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'messagesI18Ns' => Array( self::HAS_MANY, 'MessageI18NModel', 'idLanguage' ),
			);
		}
		static function instance( $name, $alias ) {
			$model = new self();
			$model->name = $name;
			$model->alias = $alias;
			return $model;
		}
		static function createModelEnglish() {
			$model = new self();
			$model->id = self::englishID;
			$model->name = "English";
			$model->alias = "en_us";
			return $model;
		}
		static function getAll( $refresh = false ) {
			static $languages;
			if( $languages === null or $refresh ) {
				$languages = self::model()->findAll( Array(
					'order' => "`t`.`createdDT` ASC, `t`.`id` ASC",
				));
				array_unshift( $languages, self::createModelEnglish());
			}
			return $languages;
		}
		static function getAliasById( $id ){
			$model = self::getByID( $id );
			if( !$model ) return false;
			return $model->alias;
		}
		static function getByAlias( $alias ) {
			return CommonLib::findSingle( self::getAll(), 'alias', $alias );
		}
		static function getByID( $id ) {
			return CommonLib::findSingle( self::getAll(), 'id', $id );
		}
		static function getLangFromLangsFileByID( $id ) {
			$model = self::getByID( $id );
			foreach( Yii::app()->params['langs'] as $lang ){
				if( $model->alias == $lang['langAlias'] ) return $lang;
			}
		}
		static function getExistsIDS( $withoutEnglish = false ) {
			$ids = Array();
			$languages = self::getAll();
			foreach( $languages as $language ) {
				$ids[] = $language->id;
			}
			if( $withoutEnglish ) array_shift( $ids );
			return $ids;
		}
		static function getExistsAliases() {
			$aliases = Array();
			$languages = self::getAll();
			foreach( $languages as $language ) {
				$aliases[] = $language->alias;
			}
			return $aliases;
		}
		static function getCurrentLanguageID( $refresh = false ) {
			static $id;
			if( $id === null ) {
				$language = self::getByAlias( Yii::App()->language );
				$id = $language ? $language->id : 0;
			}
			return $id;
		}
		static function getCurrentLanguageAlias( $refresh = false ) {
			static $alias;
			if( $alias === null ) {
				$language = self::getByAlias( Yii::App()->language );
				$alias = $language ? $language->alias : "";
			}
			return $alias;
		}
		static function isEnglishID( $id ) {
			return $id == self::englishID;
		}
		static function setIDCurrentLanguage() {
			Yii::App()->db->createCommand("
				SET @idCurrentLanguage := :idCurrentLanguage;
			")->query(Array(
				':idCurrentLanguage' => self::getCurrentLanguageID(),
			));
		}
		static function getLangSlug(){
			foreach( Yii::app()->params['langs'] as $lang => $settings ){
				if( $settings['langAlias'] == Yii::app()->language ) return $lang;
			}
		}
		function isEnglish() {
			return self::isEnglishID( $this->id );
		}
		function deleteMessagesI18Ns() {
			foreach( $this->messagesI18Ns as $i18n ) {
				$i18n->delete();
			}
		}
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteMessagesI18Ns();
			Yii::App()->messages->clearCache( $this->alias );
		}
	}

	define( 'LanguageModelLoaded', true );
?>