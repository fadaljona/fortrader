<?
	Yii::import( 'models.base.ModelBase' );
	
	final class JournalUserActivityI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{journal_user_activity_i18n}}";
		}
		static function instance( $idActivity, $idLanguage, $message ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idActivity', 'idLanguage', 'message' ));
		}
	}

?>