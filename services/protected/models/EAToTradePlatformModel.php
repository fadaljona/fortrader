<?
	Yii::import( 'models.base.ModelBase' );

	final class EAToTradePlatformModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{ea_to_trade_platform}}";
		}
		static function instance( $idEA, $idPlatform ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idEA', 'idPlatform' ));
		}
	}

?>