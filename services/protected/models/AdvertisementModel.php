<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementModel extends ModelBase {
		const MAXHeaderLen = CommonLib::maxByte;
		const MAXTextLen = CommonLib::maxWord;
		const PATHUploadImageDir = 'uploads/a1/images';
		const PATHUploadHtmlDir = 'uploads/a1/html';
		const PATHHoldImageDir = 'uploads/a1/holds';
		public $countShows;
		public $countClicks;
		public $status;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'campaign' => Array( self::BELONGS_TO, 'AdvertisementCampaignModel', 'idCampaign' ),
				'stats' => Array( self::HAS_MANY, 'AdvertisementStatsModel', 'idAd' ),
				'zones' => Array( self::MANY_MANY, 'AdvertisementZoneModel', '{{advertisement_zone_to_advertisement}}(idAd,idZone)', 'order' => '`zones`.`name`' ),
				'linksToZones' => Array( self::HAS_MANY, 'AdvertisementZoneToAdvertisementModel', 'idAd' ),
				'clicks' => Array( self::HAS_MANY, 'AdvertisementClickModel', 'idAd' ),
				'likes' => Array( self::HAS_MANY, 'AdvertisementLikeModel', 'idAd' ),
				'likeCurrentUser' => Array( 
					self::HAS_ONE, 'AdvertisementLikeModel', 'idAd', 
					'on' => " `likeCurrentUser`.`idUser` = :idUser ", 
					'params' => Array( 
						':idUser' => Yii::App()->user->id,
					)
				),
			);
		}
		function checkAccess() {
			return $this->campaign ? $this->campaign->checkAccess() : true;
		}
			
		function uploadIframeFile($uploadedFile){
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadHtmlDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadHtmlDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadHtmlDir );

			$this->setIframeFile( $fileFullName );
		}
		function setIframeFile( $name ) {
			$oldIframe = $this->iframe;

			$this->deleteIframeFile();
			$this->iframe = $name;
			if( $oldIframe && $this->text ){
				$this->text = str_replace( $oldIframe, $this->iframe, $this->text );
			}
		}
		function getPathIframeFile() {
			return strlen($this->iframe) ? self::PATHUploadHtmlDir."/{$this->iframe}" : '';
		}
		function deleteIframeFile() {
			if( strlen( $this->iframe )) {
				if( is_file( $this->pathIframeFile ) and !@unlink( $this->pathIframeFile )) throw new Exception( "Can't delete {$this->pathIframeFile}" );
				$this->iframe = null;
			}
		}
		function getSrcIframeFile() {
			return $this->pathIframeFile ? Yii::app()->baseUrl."/{$this->pathIframeFile}" : '';
		}
		function getAbsoluteSrcIframeFile() {
			return $this->pathIframeFile ? Yii::app()->createAbsoluteUrl($this->pathIframeFile) : '';
		}
		function setIframeText($w, $h){
			if( !$this->absoluteSrcIframeFile ) return false;
			$this->text = '<iframe src="' . CommonLib::removeProtocol( $this->absoluteSrcIframeFile ) . '" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no" width="'.$w.'" height="'.$h.'"></iframe>';
		}

		static function hitShowModels( $models ) {
			$values = $params = Array();
			foreach( (array)$models as $i=>$ad ) {
				$keyID = ":idAd{$i}";
				$values[] = "( {$keyID}, CURDATE(), 1, 0 )";
				$params[ $keyID ] = $ad->id;
			}
			$values = implode( ",", $values );
			Yii::App()->db->createCommand( "
				INSERT INTO		{{advertisement_stats}} ( `idAd`, `date`, `countShows`, `countClicks` )
				VALUES			{$values}
				ON DUPLICATE KEY UPDATE
								`countShows` = `countShows` + 1;
			" )->query( $params );
		}
		function hitClick() {
			Yii::App()->db->createCommand( "
				INSERT INTO		{{advertisement_stats}} ( `idAd`, `date`, `countShows`, `countClicks` )
				VALUES			( :idAd, CURDATE(), 0, 1 )
				ON DUPLICATE KEY UPDATE
								`countClicks` = `countClicks` + 1;
			" )->query(Array(
				':idAd' => $this->id,
			));
			$click = AdvertisementClickModel::instance( $this->id );
			$click->save();
		}
		function clearStats() {
			Yii::App()->db->createCommand( "
				DELETE
				FROM			{{advertisement_stats}} 
				WHERE			`idAd` = :idAd
			" )->query(Array(
				':idAd' => $this->id,
			));
			AdvertisementClickModel::clearAdvertisementClicks( $this->id );
		}
		function like() {
			$idUser = Yii::App()->user->id;
			if( !$idUser ) return false;
			
			$like = AdvertisementLikeModel::model()->findByAttributes(Array( 
				'idAd' => $this->id,
				'idUser' => $idUser,
			));
			if( $like ) {
				$this->countLikes--;
				$this->update( Array( 'countLikes' ));
				$like->delete();
				return false;
			}
			
			$this->countLikes++;
			$this->update( Array( 'countLikes' ));
			
			$like = AdvertisementLikeModel::instance( $this->id, $idUser );
			$like->save();
			
			return true;
		}
			# fields
			# image
		static function validatePathImage( $path ) {
			return strpos( $path, self::PATHHoldImageDir ) === 0 or strpos( $path, self::PATHUploadImageDir ) === 0;
		}
		static function holdImage( $uploadedFile ) {
			CommonLib::cleanDir( self::PATHHoldImageDir, 86400 );
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHHoldImageDir, $fileEx ));
			
			$filePath = self::PATHHoldImageDir."/{$fileName}.{$fileEx}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHHoldImageDir );
			
			return $filePath;
		}
		static function previewImage( $path, $width, $height ) {
			if( !is_file( $path )) return false;
			$filePreviewEx = CommonLib::getExtension( $path );
			list( $filePreviewName, $filePreviewEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHHoldImageDir, $filePreviewEx ));
			$filePreviewPath = self::PATHHoldImageDir."/{$filePreviewName}.{$filePreviewEx}";
			
			if( strtolower( $filePreviewEx ) == 'swf' ){
				copy( $path, $filePreviewPath );
			}else{
				ImgResizeLib::resize( $path, $filePreviewPath, $width, $height );
			}
			
			
			return $filePreviewPath;
		}
		function deleteImage() {
			if( strlen( $this->srcImage )) @unlink( $this->srcImage );
			if( strlen( $this->resizedImage )) @unlink( $this->resizedImage );
			
			$this->widthImage = null;
			$this->heightImage = null;
		}
		function setImageFromHold( $path, $width, $height ) {
			$this->deleteImage();
		
			$fileEx = CommonLib::getExtension( $path );
			list( $srcImageName, $srcImageEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadImageDir, $fileEx ));
			list( $resizedImageName, $resizedImageEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadImageDir, $fileEx ));
			
			$this->srcImage = self::PATHUploadImageDir."/{$srcImageName}.{$srcImageEx}";
			$this->resizedImage = self::PATHUploadImageDir."/{$resizedImageName}.{$resizedImageEx}";
			
			rename( $path, $this->srcImage );
			
			
			list( $srcWidth, $srcHeight, $type, $attr) = getimagesize( $this->srcImage );
			if( ($srcWidth == $width and $srcHeight == $height) || ( strtolower($fileEx) == 'swf' ) ) {
				copy( $this->srcImage, $this->resizedImage );
			}
			else{
				ImgResizeLib::resize( $this->srcImage, $this->resizedImage, $width, $height );
			}
				
			
			$this->widthImage = $width;
			$this->heightImage = $height;
		}
		function resizeImage( $width, $height ) {
			$fileEx = CommonLib::getExtension( $this->resizedImage );
			list( $resizedImageName, $resizedImageEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadImageDir, $fileEx ));
			
			@unlink( $this->resizedImage );
			$this->resizedImage = self::PATHUploadImageDir."/{$resizedImageName}.{$resizedImageEx}";
			
			if( strtolower($fileEx) == 'swf' ){
				copy( $this->srcImage, $this->resizedImage );
			}else{
				ImgResizeLib::resize( $this->srcImage, $this->resizedImage, $width, $height );
			}
			$this->widthImage = $width;
			$this->heightImage = $height;
		}
			
			# zones
		function getIDsZones() {
			$ids = CommonLib::slice( $this->zones, 'id' );
			return $ids;
		}
		function unlinkFromZones() {
			foreach( $this->linksToZones as $link ) {
				$link->delete();
			}
		}
		function linkToZones( $ids ) {
			foreach( $ids as $idZone ) {
				$link = AdvertisementZoneToAdvertisementModel::model()->findByAttributes( Array( 'idZone' => $idZone, 'idAd' => $this->id ));
				if( !$link ) {
					$link = AdvertisementZoneToAdvertisementModel::instance( $idZone, $this->id );
					$link->save();
				}
			}
		}
		function setZones( $ids ) {
			$oldIDs = $this->getIDsZones();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromZones();
				$this->linkToZones( $ids );
			}
		}	
			# stats
		function deleteStats() {
			foreach( $this->stats as $model ) {
				$model->delete();
			}
		}
			# clicks
		function deleteClicks() {
			foreach( $this->clicks as $model ) {
				$model->delete();
			}
		}
			# likes
		function deleteLikes() {
			foreach( $this->likes as $model ) {
				$model->delete();
			}
		}
		
		function isNew() {
			$time = strtotime( $this->createdDT );
			return time() - $time <= 60*60*24*7;
		}
		function getCTR() {
			return $this->countShows ? $this->countClicks / $this->countShows * 100 : null;
		}
		function getUrl() {
			$url = $this->url;
			if( strlen( $url )) {
				if( !preg_match( "#^(https:|http:|ftp:)?//#i", $url )) {
					$url = "http://{$url}";
				}
			}
			return $url;
		}
		function getClickUrl( $zone = null ) {
            if ($this->isDirectAdLink && $this->url) {
                return $this->url;
            }

            $params = Array( 'id' => $this->id );

			if( $zone ) $params[ 'idZone' ] = $zone->id;
			return Yii::App()->controller->createAbsoluteUrl( '/advertisement/redirect', $params );
		}
		function getAubsoluteImageURL() {
			return Yii::App()->controller->createAbsoluteUrl( '/'.$this->resizedImage );
		}
		function getHTMLText() {
			$text = htmlspecialchars( $this->text );
			$text = CommonLib::nl2br( $text );
			return $text;
		}
		function getTitledURL() {
			$url = $this->url;
			$url = preg_replace( '#^(https?:)?//#', '', $url );
			$url = preg_replace( '#\?.*$#', '', $url );
			$url = preg_replace( '#/.*$#', '', $url );
			if( strlen( $url ) > 30 ) {
				$url = substr( $url, 0, 50 ).'...';
			}
			return $url;
		}
		
		static function getModelByBrokerId( $bokerId ) {
			
			$model_id = Yii::app()->db->createCommand()
						->select('advertisement.id')
						->from('ft_advertisement_campaign campaign')  //Your Table name
						->join('wp_users users', 'users.ID = campaign.idUser')
						->join('ft_advertisement advertisement', 'advertisement.idCampaign = campaign.id')
						->where( "campaign.type='Rating Brokers' and campaign.status='Active' and CURDATE() BETWEEN campaign.begin and campaign.end and users.idBroker=".$bokerId )
						->queryRow();
						
			return AdvertisementModel::model()->findByPk($model_id['id']);

			
		}
			
			# events
			
		protected function beforeDelete(){
			$this->deleteIframeFile();
			return parent::beforeDelete();

		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteStats();
			$this->deleteClicks();
			$this->deleteLikes();
			$this->deleteImage();
			$this->unlinkFromZones();
			
			if( $this->type == 'Image' ){
				$adCampaign = AdvertisementCampaignModel::model()->findByPk( $this->idCampaign );
				$bannersCount = Yii::app()->db->createCommand()
					->select('count(`id`) count')
					->from('ft_advertisement')
					->where('dontShow=0 and idCampaign = :idCampaign', array( ':idCampaign' => $adCampaign->id ) )
					->queryRow();
				$bannersCount = $bannersCount['count'];
				
				if( $bannersCount ){
					Yii::app()->db->createCommand()
						->update('ft_advertisement',
						array('weight' => $adCampaign->weight / $bannersCount ),
						'dontShow=0 and idCampaign = :idCampaign',
						array( ':idCampaign' => $adCampaign->id )
						);
				}
			}
			
		}
		protected function beforeSave() {
			if(!$this->htmlBaner)$this->deleteIframeFile();
			if( $this->htmlBaner && $this->iframe ){
				
				$file = file_get_contents($this->pathIframeFile);
				$file = preg_replace('/<\!--startIframeHtmlFtBn--\>.*<\!--endIframeHtmlFtBn--\>/i', '', $file);
				
				if( $this->url ){
					$str="<!--startIframeHtmlFtBn--><style>.winOpenClass{cursor:pointer;}body{position:relative;}</style><script>if (document.body.classList) {document.body.classList.add('winOpenClass');} else {document.body.className += ' winOpenClass';}document.body.addEventListener(\"click\", function(e) {window.open('{$this->url}', '_blank') }, false);</script><!--endIframeHtmlFtBn--></body>";
					$file = preg_replace('/<\s*\/\s*body\s*>/i', $str, $file);
				}
				file_put_contents($this->pathIframeFile, $file);
			}
			return parent::beforeSave();
		}
		protected function afterSave() {
			$result = parent::afterSave();
			
			if( $this->type == 'Image' ){
				$adCampaign = AdvertisementCampaignModel::model()->findByPk( $this->idCampaign );
				$bannersCount = Yii::app()->db->createCommand()
					->select('count(`id`) count')
					->from('ft_advertisement')
					->where('dontShow=0 and idCampaign = :idCampaign', array( ':idCampaign' => $adCampaign->id ) )
					->queryRow();
				$bannersCount = $bannersCount['count'];
				
				if( $bannersCount ){
					Yii::app()->db->createCommand()
						->update('ft_advertisement',
						array('weight' => $adCampaign->weight / $bannersCount ),
						'dontShow=0 and idCampaign = :idCampaign',
						array( ':idCampaign' => $adCampaign->id )
						);
				}
			}
			return $result;
		}
	}

?>