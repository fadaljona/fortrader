<?
	Yii::import( 'models.base.ModelBase' );
	
	final class DecommentsPostsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function rules() {
			return Array(
				Array( 'id, postKey, cat', 'required' ),
				Array( 'id', 'unique' ),
				Array( 'id', 'numerical', 'integerOnly' => true ),
			);
		}
		static function getCatLangSuffix(){
			if( Yii::app()->language != 'ru' ) return Yii::app()->language;
			return '';
		}
		static function getPostKey( $id ){
			$model = self::model()->findByPk( $id );
			if( !$model ) return false;
			return $model->postKey;
		}
		static function getCommentsCount( $paramStr, $cat ){
			$postKey = Yii::app()->controller->id . '.' . Yii::app()->controller->action->id . '.' . $paramStr;
			//echo $postKey . "<br />" . $cat;
			$postId = self::getPostId( $postKey, $cat, true );

			if( !$postId ) return 0;
			
			$decommentsPost = self::model()->findByPk($postId);
			if( isset( $decommentsPost->commentCount ) ) return $decommentsPost->commentCount;
			
			$decommentsPost->commentCount = Yii::app()->db->createCommand()
				->select('count(comment_ID)')
				->from('wp_comments')
				->where( ' `comment_post_ID` = :id AND `comment_approved` = 1 ', array( ':id' => $postId ) )
				->queryScalar();
				
			$decommentsPost->save();
			return $decommentsPost->commentCount;
		}
		static function getPostId( $postKey, $cat, $getCount = false ){
			$cat = $cat . self::getCatLangSuffix();
			$decommentsPost = self::model()->find(array(
				'condition' => " `t`.`postKey` = :postKey AND `t`.`cat` = :cat ",
				'params' => array( ':postKey' => $postKey, ':cat' => $cat )
			));
			
			if( $decommentsPost ){
				$postModel = WPPostModel::model()->find(array(
					'condition' => " `t`.`ID` = :id AND `t`.`post_type` = 'post' AND `t`.`post_status` = 'publish' ",
					'params' => array( ':id' => $decommentsPost->id )
				));
				if( $postModel ){
					return $decommentsPost->id;
				}else{
					if( $getCount ) return false;
					$decommentsPost->delete();
				}
			}
			
			CommonLib::loadWp();
			$settings = SettingsModel::getModel();
			$services_post = array(
				'post_title' => 'DO NOT DELETE__' . $postKey, 
				'post_content' => '', 
				'post_status' => 'publish', 
				'post_author' => 1, 
				'post_category' => array( $settings->servicesPostsCat ) 
			); 
			$post_id = wp_insert_post( $services_post ); 

			if( !$post_id ) return false;

			$postKeyModel = new self;
			$postKeyModel->id = $post_id;
			$postKeyModel->postKey = $postKey;
			$postKeyModel->cat = $cat;
			if( $postKeyModel->validate() ){
				if( $postKeyModel->save() ){
					return $post_id;
				}
			}
			return false;
			
		}
		public static function removeEmailFromMeta($postMeta, $email){
			$semails = unserialize( $postMeta->meta_value );
			foreach( $semails as $i => $semail ){
				if( $semail == $email ) unset( $semails[$i] );
			}

			$postMeta->meta_value = serialize( array_values($semails) );
			$postMeta->save();
		}
		public static function unsubscribeFromPostCommentEmails( $email, $postId, $type ){
			$comment = WPCommentsModel::model()->find(array(
				'condition' => " `t`.`comment_author_email` = :email AND `t`.`comment_post_ID` = :postId ",
				'params' => array( ':email' => $email, ':postId' => $postId ),
			));
			
			if( !$comment ) return false;
			
			$comments = WPCommentsModel::model()->findAll(array(
				'with' => array( 'meta' ),
				'condition' => " `t`.`comment_author_email` = :email AND `t`.`comment_post_ID` = :postId AND `meta`.`meta_key` = '_decom_subscriber' ",
				'params' => array( ':email' => $email, ':postId' => $postId ),
			));
			
			foreach( $comments as $comment ){
				foreach( $comment->meta as $meta ){
					$meta->delete();
				}
			}
			
			$postMeta = WPPostmetaModel::model()->find(array(
				'condition' => ' `t`.`post_id` = :postId AND `t`.`meta_key` = "_decom_subscribers" ',
				'params' => array( ':postId' => $postId ),
			));
			self::removeEmailFromMeta($postMeta, $email);
			
			
			
			if( $type == 'all' ){
				
				WPCommentmetaModel::model()->deleteAll(
					" `meta_key` = '_decom_subscriber' AND `meta_value` = :email ", 
					array( ':email' => $email )
				);
				
				$likeEmail = CommonLib::escapeLike( $email );
				$likeEmail = CommonLib::filterSearch( $likeEmail );
				
				$postMetas = WPPostmetaModel::model()->findAll(array(
					'condition' => ' `t`.`meta_key` = "_decom_subscribers" AND `t`.`meta_value` LIKE :email ',
					'params' => array( ':email' => $likeEmail ),
				));
				foreach( $postMetas as $postMeta ){
					self::removeEmailFromMeta($postMeta, $email);
				}
			}

			return true;
		}
	}

?>