<?
	Yii::import( 'models.base.ModelBase' );
	
	final class NewsAggregatorNewsModel extends ModelBase {
		public $idSource;
		public $extendedSearchFields = array('idSource');
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'lang' => Array( self::HAS_ONE, 'LanguageModel', array( 'id' => 'idLanguage' ) ),
				'category' => Array( self::HAS_ONE, 'NewsAggregatorCatsModel', array( 'id' => 'idCat' ) ),
				'rss' => Array( self::HAS_ONE, 'NewsAggregatorRssModel', array( 'id' => 'idRss' ) ),
			);
		}
		public static function findBySlug( $slug ){
			return self::model()->find(array(
				'condition' => " `slug` = :slug ",
				'params' => array( ':slug' => $slug ),
			));
		}
		function getRedirectSingleURL() {
			return Yii::app()->createUrl( 'newsAggregator/single', array( 'slug' => $this->slug ));
		}
		static function getItemsForInformerByIds( $ids ){
			
			$criteria = new CDbCriteria();
			$criteria->condition = " `t`.`title` IS NOT NULL AND `t`.`title` <> '' AND `t`.`idLanguage` = :idLanguage ";
			$criteria->addInCondition('idCat', explode(',', $ids ));
			$criteria->params[':idLanguage'] = LanguageModel::getCurrentLanguageID();
			$criteria->limit = 10;
			$criteria->order = " `t`.`gmtTime` DESC ";
			
			return self::model()->findAll( $criteria );
		}
		static function getListDp( $lastTime = 0, $cat = 0 ){
			$c = new CDbCriteria();
			
			$cookieObj = Yii::app()->request->cookies['disabledSourcesFT']; 
			$disabledSourcesFT = $cookieObj->value;
			
			$c->with[ 'rss' ] = Array( 'with' => array( 'source' ) );
			$c->with[ 'category' ] = Array();
			
			if( $disabledSourcesFT ){
				if( strpos($disabledSourcesFT, ',') === false ){
					$c->addCondition( " `rss`.`idSource` <> :disabledSourcesFT " );
					$c->params[':disabledSourcesFT'] = $disabledSourcesFT;
				}else{
					$c->addNotInCondition(" `rss`.`idSource` ", explode(',', $disabledSourcesFT));
				}
			}
			
			$c->addCondition( " `rss`.`enabled` = 1 AND `category`.`enabled` = 1 AND `source`.`enabled` = 1 " );

			if( $lastTime ){
				$c->addCondition( " `t`.`gmtTime` < :gmtTime " );
				$c->params[':gmtTime'] = $lastTime;
			}
			if( $cat ){
				$c->addCondition( " `t`.`idCat` = :idCat " );
				$c->params[':idCat'] = $cat;
			}
			$c->order = " `t`.`gmtTime` DESC ";
			
			$DP = new CActiveDataProvider( 'NewsAggregatorNewsModel', Array(
				'criteria' => $c,
				'pagination' => array(
				  'pageSize' => 20,
				  'pageVar' => 'pageRssNews',
				  'currentPage' => 0,
				)
			));
			return $DP;
		}
		static function prepareDescription( $regExp, $description ){
			$description = html_entity_decode($description);
			$description = str_replace( array( "\n", "\r", '<![CDATA[', ']]>' ), ' ', $description);
			$description = preg_replace('/<[^>]+>/ui', ' ', $description);
			$description = preg_replace('/\s+/ui', ' ', $description);
			$description = preg_replace('/'. $regExp .'/ui', '', $description);
			$description = trim( $description );
			return $description;
		}
		static function importNews(){
			$viewFile = YiiBase::getPathOfAlias('application') . '/components/widgets/listViews/views/rssListView/view.php' ;
			
			Yii::import( 'extensions.lastRSS' );
			$settings = NewsAggregatorSettingsModel::getModel();
			
			$rssModels = NewsAggregatorRssModel::model()->findAll(array(
				'with' => array('source'),
				'select' => array(' `t`.`id` ', ' `t`.`idLanguage` ', ' `t`.`regExp` ', ' `t`.`lastBuildDate` ', ' `t`.`idCat` '),
				'condition' => " `t`.`enabled` = 1 AND `source`.`enabled` = 1 ",
				'order' => " `t`.`idSource` ASC ",
			));
			
			$context = new ZMQContext();
			$socket = $context->getSocket(ZMQ::SOCKET_PUSH, '');
			$socket->connect("tcp://localhost:8899");	

			$rssFilesPath = YiiBase::getPathOfAlias('application') . NewsAggregatorRssModel::PathRssStoredTxtFiles;
			foreach( $rssModels as $model ){
				$rssFile = $rssFilesPath . '/' . $model->id . '.txt';
				if( file_exists($rssFile) ){
					
					$langConstant = $settings->geLangStemConstByLangId( $model->idLanguage );

					$rss = new lastRSS;
					$rss->cache_dir = '';
					$rss->cache_time = 0;
					if ($rs = $rss->get($rssFile)) {
		
						
						$lastBuildDate = strtotime($rs['lastBuildDate']);
						if( isset( $rs['lastBuildDate'] ) && $lastBuildDate == $model->lastBuildDate ) continue;
						
						foreach( $rs['items'] as $item ){
							
							$pubDate = strtotime($item['pubDate']);
							$newTitle = str_replace( array( "\n", "\r", '<![CDATA[', ']]>' ), ' ', $item['title']);
							$slug = CommonLib::generateSlug($newTitle) . '-' . $pubDate;
							//echo $slug . "\n";
							$newsModel = self::findBySlug($slug);
							if( $newsModel ) continue;
							unset($newsModel);
							
							$newsModel = new self;
							
							$description = self::prepareDescription( $model->regExp, $item['description'] );
							$stemmedText = CommonLib::stemedText( $item['title'], $langConstant ) . ' ' .CommonLib::stemedText( $description, $langConstant );
							
							if( $model->idCat ){
								$category = array( 'cat' => $model->idCat, 'summary' => Yii::t('*', 'Defined by admin') );
							}else{
								$category = NewsAggregatorCatsModel::defineCategory( $stemmedText, $model->idLanguage );
							}
							
							if( isset( $item['guid'] ) ){
								$newLink = $item['guid'];
							}else{
								$newLink = $item['link'];
							}
							
							$newsModel->idLanguage = $model->idLanguage;
							$newsModel->title = $newTitle;
							$newsModel->description = CommonLib::trimWords($description, 100);
							$newsModel->gmtTime = $pubDate;
							$newsModel->link = $newLink;
							$newsModel->idRss = $model->id;
							$newsModel->idCat = $category['cat'];
							$newsModel->stemedNewsText = $stemmedText;
							$newsModel->categoryKeys = $category['summary'];
							$newsModel->slug = $slug;		
							
							if( $newsModel->validate() ){
								if( $newsModel->save() ){
									echo "news saved {$item['title']}\n";
									
									$data = $newsModel;
									ob_start();
									include $viewFile;
									$renderedModel = ob_get_clean();
									
									$socket->send(json_encode( array( 'key' => 'rssNewsAll', 'data' => $renderedModel ) ));
									$socket->send(json_encode( array( 'key' => 'rssNewsCategory' . $newsModel->idCat, 'data' => $renderedModel ) ));
								}
							} 
						}
						if( $lastBuildDate ){
							$model->lastBuildDate = $lastBuildDate;
						}else{
							$model->lastBuildDate = time();
						}
						
						if( $model->validate() ) $model->save();
					
					}else{
						echo "Error: It's not possible to get $rssFile...";
					}
				}
			}
		}
		static function updateCats(){
			$models = self::model()->findAll(array(
				'with' => array( 'rss' ),
				'condition' => " `t`.`idCat` = 0 ",
				'limit' => 500
			));
			if( !$models ) return;
			$settings = NewsAggregatorSettingsModel::getModel();
			foreach( $models as $model ){
				$langConstant = $settings->geLangStemConstByLangId( $model->idLanguage );
				$stemmedText = CommonLib::stemedText( $model->title, $langConstant ) . ' ' .CommonLib::stemedText( $model->description, $langConstant );
				
				if( $model->rss->idCat ){
					$category = array( 'cat' => $model->rss->idCat, 'summary' => Yii::t('*', 'Defined by admin') );
				}else{
					$category = NewsAggregatorCatsModel::defineCategory( $stemmedText, $model->idLanguage );
				}
				
				$model->idCat = $category['cat'];
				$model->stemedNewsText = $stemmedText;
				$model->categoryKeys = $category['summary'];
				if( $model->validate() ){
					if( $model->save() ){
						echo "news cats updated {$model->title}\n";
					}
				}
			}
		}
		static function resetCats(){
			return self::model()->updateAll( array( 'idCat'=>0, 'categoryKeys' => '', 'stemedNewsText' => '' ) );
		}
		static function deleteOldNews(){
			$settings = NewsAggregatorSettingsModel::getModel();
			$models = self::model()->findAll(array(
				'order' => " `t`.`gmtTime` DESC ",
				'limit' => 50,
				'offset' => $settings->storedNewsCount,
			));
			foreach( $models as $model ){
				$model->delete();
			}
		}
		static function getAdminListDp( $filterModel ){
			$c = new CDbCriteria();
			if( $filterModel ){
				if( strlen( $filterModel->title )) {
					$title = CommonLib::escapeLike( $filterModel->title );
					$title = CommonLib::filterSearch( $title );
					$c->addCondition( " `t`.`title` LIKE :title ");
					$c->params[':title'] = $title;
				}
				if( strlen( $filterModel->idSource )) {
					$c->with = array('rss');
					$c->addCondition( " `rss`.`idSource` = :idSource ");
					$c->params[':idSource'] = $filterModel->idSource;
				}
				if( strlen( $filterModel->idLanguage )) {
					$c->addCondition( " `t`.`idLanguage` = :idLanguage ");
					$c->params[':idLanguage'] = $filterModel->idLanguage;
				}
				if( strlen( $filterModel->idCat )) {
					$c->addCondition( " `t`.`idCat` = :idCat ");
					$c->params[':idCat'] = $filterModel->idCat;
				}
			}
			$c->order = " `t`.`gmtTime` DESC ";
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => $c,
			));
			return $DP;
		}
	}
?>