<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesHistoryDataModel extends ModelBase { 
		public static $resolutionTradingview = array(
			43200 => "M",
			10080 => "W",
			1440 => "D",
			240 => "240",
			60 => "60",
			30 => "30",
			15 => "15",
		/*	5 => "5",
			1 => "1",*/
		);
		public static $resolutionAmcharts = array(
			'MN1' => "M",
			'W1' => "W",
			'D1' => "D",
			'H4' => "240",
			'H1' => "60",
			'30M' => "30",
			'15M' => "15",
		/*	'5M' => "5",
			'1M' => "1",*/
		);
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function hasData( $symbolId, $resolution ){

			$symbol = QuotesSymbolsModel::model()->findByPk( $symbolId );

			$exist = self::model()->find(array(
				'condition' => " `symbolId` = :symbolId AND `resolution` = :resolution ",
				'params' => array( ':symbolId' => $symbolId, ':resolution' => $resolution ),
			));
			if( $exist ) return true;
			return false;
		}
		static function getDataForContestDealsChart( $symbolId, $daysTodisplayGraph ){
			$data = Yii::app()->db->createCommand()
				->select('barTime, close')
				->from('{{quotes_history_data}}')
				->order(' barTime DESC ')
				->where( ' `symbolId` = :symbolId AND `resolution` = 1 AND `barTime` >= :daysTodisplayGraph ', array( ':symbolId' => $symbolId, ':daysTodisplayGraph' => time() - 60*60*24*$daysTodisplayGraph ) )
				->queryAll();
				
			if( !count($data) ) return false;
			$outData = '';
			$data = array_reverse( $data );
			foreach( $data as $row ){
				$outData[] = array(
					"date" => gmdate('Y-m-d H:i', $row['barTime']),
					"close" => $row['close']
				);
			}
			return $outData;
		}
		static function getDataForPeriodChart( $symbolId, $resolution ){
            if ($resolution == 1440) {
                $queryLimit = 2600;
            } else {
                $queryLimit = 100;
            }

			$symbol = QuotesSymbolsModel::model()->findByPk( $symbolId );

			$data = Yii::app()->db->createCommand()
				->select('*')
				->from('{{quotes_history_data}}')
				->order(' barTime DESC ')
				->where( ' `symbolId` = :symbolId AND `resolution` = :resolution ', array( ':symbolId' => $symbolId, ':resolution' => $resolution ) )
				->limit($queryLimit)
				->queryAll();
                
			$outData = '';
			foreach( $data as $row ){
				$outData = "{$row['barTime']};{$row['open']};{$row['high']};{$row['low']};{$row['close']};{$row['volume']}\n" . $outData;
			}
			return trim($outData);
		}
		static function getDataForTradingviewChart( $symbolId, $resolution, $from, $to ){

			$symbol = QuotesSymbolsModel::model()->findByPk( $symbolId );

			$data = Yii::app()->db->createCommand()
				->select('*')
				->from('{{quotes_history_data}}')
				->order(' barTime DESC ')
				->where( ' `symbolId` = :symbolId AND `resolution` = :resolution AND `barTime` >= :from AND `barTime` <= :to ', array( ':symbolId' => $symbolId, ':resolution' => $resolution, ':from' => $from, ':to' => $to+60 ) )
				//->limit( ' 100 ')
				->queryAll();
				
			$barTime = $close = $open = $high = $low = $volume = '';
			
			$i=0;
			foreach( $data as $row ){
				if( $i ) {
					$barTime = ',' . $barTime; $close = ',' . $close; $open = ',' . $open; $high = ',' . $high; $low = ',' . $low; $volume  = ',' . $volume; 
				}
				$barTime = $row['barTime'] . $barTime;
				$close = $row['close'] . $close;
				$open = $row['open'] . $open;
				$high = $row['high'] . $high;
				$low = $row['low'] . $low;
				$volume = $row['volume'] . $volume;
				$i++;
			}
			return '{"s":"ok","t":['.$barTime.'],"c":['.$close.'],"o":['.$open.'],"h":['.$high.'],"l":['.$low.'],"v":['.$volume.']}';
		}
		static function getResolutionsForSymbol( $symbolId ){
			$resolutions = self::model()->findAll(array(
				'select' => array( ' `resolution` ' ),
				'condition' => " `symbolId` = :symbolId ",
				'group' => ' `resolution` ',
				'params' => array( ':symbolId' => $symbolId ),
			));
			$outStr = '';
			$i=0;

			foreach( $resolutions as $resolution ){
				if( isset( self::$resolutionTradingview[$resolution->resolution] ) ){
					if( $i ) $outStr .= ',';
					$outStr .= '"' . self::$resolutionTradingview[$resolution->resolution] . '"';
					$i++;
				}
			}
			return '['.$outStr.']';
		}
		static function getTodayD1Data( $symbolId ){
		
			$symbol = QuotesSymbolsModel::model()->findByPk( $symbolId );

			$lastData = self::model()->find(array(
				'condition' => " `symbolId` = :symbolId AND `resolution` = 1440",
				'order' => ' `barTime` DESC ',
				'params' => array( ':symbolId' => $symbolId ),
			));
			if( !$lastData ) return false;
			
			return array(
				'{$high}' => $lastData->high,
				'{$low}' => $lastData->low,
				'{$open}' => $lastData->open,
				'{$close}' => $lastData->close
			);
		}
		static function getTodayD1DataForList( $symbolIds ){
			if( !count($symbolIds) ) return false;
			$criteria = new CDbCriteria();
			$criteria->condition = " `resolution` = 1440 ";
			$criteria->addInCondition('symbolId', $symbolIds );
			$criteria->order = ' `barTime` DESC ';
			$lastData = self::model()->findAll( $criteria );
			
			if( !$lastData ) return false;
			
			$outData = array();
			foreach( $lastData as $data ){
				$outData[ $data->symbolId ]['high'] = $data->high;
				$outData[ $data->symbolId ]['low'] = $data->low;
				$outData[ $data->symbolId ]['open'] = $data->open;
			}
			return $outData;
		}
		static function getTodayD1DataForOneSymbol( $symbolId ){	
			if( !$symbolId ) return false;
			$criteria = new CDbCriteria();
			$criteria->condition = " `resolution` = 1440 AND `symbolId` = :symbolId ";
			$criteria->params['symbolId'] = $symbolId;
			$criteria->order = ' `barTime` DESC ';
			$lastData = self::model()->find( $criteria );

			if( !$lastData ) return false;
			
			$outData = array();

			return array(
				'high' => $lastData->high,
				'low' => $lastData->low,
                'open' => $lastData->open,
                'close' => $lastData->close,
                'barTime' => $lastData->barTime,
			);
		}
		static function getDataForHistoryHighCharts($callback, $symbolId, $symbolName, $memberId, $start = false, $end = false){
			
			$member = ContestMemberModel::model()->find(array(
				'with' => array( 'server', 'mt5AccountInfo' ),
				'condition' => ' `t`.`id` = :id ',
				'params' => array( ':id' => $memberId )
			));
			if( !$member ) return false;
			
			$dataTimeOffest = 0;
			if( $member->mt5AccountInfo->sGmt ){
				$dataTimeOffest -= 60*60*$member->mt5AccountInfo->sGmt;
			}
			
			$dataMinStart = Yii::app()->db->createCommand()
					->select('barTime')
					->from('{{quotes_history_data}}')
					->order(' barTime ASC ')
					->where( ' `symbolId` = :symbolId  AND `resolution` = 15 ', array( ':symbolId' => $symbolId ) )
					->limit('1')
					->queryAll();
					
			if( !$dataMinStart ) return false;
			$minStart = $dataMinStart[0]['barTime'];
			

			
			if( !$start && !$end ){
				
				$dataMinDeal = Yii::app()->db->createCommand()
						->select('OrderOpenTime')
						->from('mt4_account_history')
						->order(' OrderOpenTime ASC ')
						->where( 'OrderSymbol = :OrderSymbol AND AccountNumber=:accNum AND (OrderType=1 OR OrderType=0) AND AccountServerId=:serverId ', array( ':accNum'=>$member->accountNumber, ':serverId'=>$member->idServer, ':OrderSymbol' => $symbolName ) )
						->limit('1')
						->queryAll();
				
				if( !$dataMinDeal ) return false;	
				$minDealTime = $dataMinDeal[0]['OrderOpenTime'] + $dataTimeOffest - 60*60*24*7;
				
				$start = $minStart;
				if( $minDealTime > $start ){
					$start = $minDealTime;
				}
				
				$dataMaxDeal = Yii::app()->db->createCommand()
						->select('OrderCloseTime')
						->from('mt4_account_history')
						->order(' OrderCloseTime DESC ')
						->where( 'OrderSymbol = :OrderSymbol AND AccountNumber=:accNum AND (OrderType=1 OR OrderType=0) AND AccountServerId=:serverId ', array( ':accNum'=>$member->accountNumber, ':serverId'=>$member->idServer, ':OrderSymbol' => $symbolName ) )
						->limit('1')
						->queryAll();
				

				$end = $dataMaxDeal[0]['OrderCloseTime'] + $dataTimeOffest + 60*60*24*7;
	
				$start = $start*1000;
				$end = $end * 1000;
			}
			
			
				
			$start = floor($start / 1000);
			$end = floor($end / 1000);
			$period = 43200;
				
			if( $start < $minStart ) $start = $minStart;
			if( $end < $start ) return false;
				
			$chartTime = $end - $start;
				
			if( $chartTime < 60*60*13 ){
				$period = 15;
			}elseif( $chartTime < 60*60*25 ){
				$period = 30;
			}elseif( $chartTime < 60*60*24*8 ){
				$period = 240;
			}elseif( $chartTime < 60*60*24*90 ){
				$period = 1440;
			}elseif( $chartTime < 60*60*24*370 ){
				$period = 10080;
			}
		
			$data = Yii::app()->db->createCommand()
				->select('*')
				->from('{{quotes_history_data}}')
				->order(' barTime DESC ')
				->where( ' `symbolId` = :symbolId  AND `resolution` = :resolution  AND `barTime` >= :from AND `barTime` <= :to ', array( ':symbolId' => $symbolId, ':resolution' => $period, ':from' => $start, ':to' => $end ) )
				->queryAll();
				
			
				
			$outStr = '';
			
			$i=0;
			foreach( $data as $row ){
				if( $i ) {
					$outStr = ',' . $outStr; 
				}
				$outStr = '[' .  $row['barTime'] * 1000 . ',' . $row['open'] . ',' . $row['high'] . ',' . $row['low'] . ',' . $row['close'] . ']' . $outStr; 
				
				$i=1;
			}
			
			
			$timeOffsetFromFullDay = self::getTimeOffsetFromFullDay();
			

			$sellDeals = Yii::app()->db->createCommand()
				->select('OrderOpenTime, OrderCloseTime, OrderOpenPrice, OrderClosePrice')
				->from('mt4_account_history')
				->order(' OrderCloseTime DESC ')
				->where( 'OrderSymbol = :OrderSymbol AND AccountNumber=:accNum AND OrderType=1 AND AccountServerId=:serverId AND OrderOpenTime >= :from AND OrderOpenTime <= :to AND OrderCloseTime >= :from AND OrderCloseTime <= :to ', array( ':accNum'=>$member->accountNumber, ':serverId'=>$member->idServer, ':from' => $start - $dataTimeOffest, ':to' => $end - $dataTimeOffest, ':OrderSymbol' => $symbolName ) )
				->queryAll();
				
			$sellStr = '';
			$i=0;
			$prevOrderOpenTime = $prevOrderCloseTime = 0;
			
			foreach( $sellDeals as $row ){
				
				$orderOpenTime = self::getStartPeriodTimeInUtc($timeOffsetFromFullDay, $row['OrderOpenTime'] + $dataTimeOffest, $period) * 1000;
				$orderCloseTime = self::getStartPeriodTimeInUtc($timeOffsetFromFullDay, $row['OrderCloseTime'] + $dataTimeOffest, $period) * 1000;
				
				if( $prevOrderOpenTime == $orderOpenTime && $prevOrderCloseTime == $orderCloseTime && $orderOpenTime == $orderCloseTime ) continue;
				$prevOrderOpenTime = $orderOpenTime;
				$prevOrderCloseTime = $orderCloseTime;
				
				if( $i ) $sellStr = ', ['.$orderCloseTime.',null], ' . $sellStr; 
				
				$sellStr = '[' .  $orderOpenTime . ',' . $row['OrderOpenPrice'] . '], [' .  $orderCloseTime . ',' . $row['OrderClosePrice'] . ']' . $sellStr; 
				
				$i=1;
			}
			
			$buyDeals = Yii::app()->db->createCommand()
				->select('OrderOpenTime, OrderCloseTime, OrderOpenPrice, OrderClosePrice')
				->from('mt4_account_history')
				->order(' OrderCloseTime DESC ')
				->where( 'OrderSymbol = :OrderSymbol AND AccountNumber=:accNum AND OrderType=0 AND AccountServerId=:serverId AND OrderOpenTime >= :from AND OrderOpenTime <= :to AND OrderCloseTime >= :from AND OrderCloseTime <= :to ', array( ':accNum'=>$member->accountNumber, ':serverId'=>$member->idServer, ':from' => $start - $dataTimeOffest, ':to' => $end - $dataTimeOffest, ':OrderSymbol' => $symbolName ) )
				->queryAll();
				
			$buyStr = '';
			$i=0;
			$prevOrderOpenTime = $prevOrderCloseTime = 0;
			
			foreach( $buyDeals as $row ){
				
				$orderOpenTime = self::getStartPeriodTimeInUtc($timeOffsetFromFullDay, $row['OrderOpenTime'] + $dataTimeOffest, $period) * 1000;
				$orderCloseTime = self::getStartPeriodTimeInUtc($timeOffsetFromFullDay, $row['OrderCloseTime'] + $dataTimeOffest, $period) * 1000;
				
				if( $prevOrderOpenTime == $orderOpenTime && $prevOrderCloseTime == $orderCloseTime && $orderOpenTime == $orderCloseTime ) continue;
				$prevOrderOpenTime = $orderOpenTime;
				$prevOrderCloseTime = $orderCloseTime;
				
				if( $i ) $buyStr = ', ['.$orderCloseTime.',null], ' . $buyStr; 
				
				$buyStr = '[' .  $orderOpenTime . ',' . $row['OrderOpenPrice'] . '], [' .  $orderCloseTime . ',' . $row['OrderClosePrice'] . ']' . $buyStr; 
				
				$i=1;
			}
		
			
			$outStr = $callback . '([[' . $outStr . '],['.$sellStr.'],['.$buyStr.']]);';
			
			return $outStr;
		}
		static function getStartPeriodTimeInUtc( $timeOffsetFromFullDay, $time, $period ){
			if( $period < 240 ) $timeOffsetFromFullDay = 0;
			$period = $period * 60;
			return floor( $time / $period ) * $period + $timeOffsetFromFullDay;
		}
		static function getTimeOffsetFromFullDay(){
			$data = Yii::app()->db->createCommand()
				->select('barTime')
				->from('{{quotes_history_data}}')
				->where( ' `resolution` = 1440 ' )
				->limit(1)
				->queryAll();
				
			$time = $data[0]['barTime'];
			return $time - floor( $time / 86400 )  * 86400 - 86400;
        }
        
        public static function getResolutionByTypeOfCryptoCurrenciesChart($type)
        {
            switch ($type) {
                case '1d':
                    $resolution = 60;
                    break;
                case '1m':
                    $resolution = 1440;
                    break;
                case '3m':
                    $resolution = 1440;
                    break;
                case '1y':
                    $resolution = 1440;
                    break;
                case '5y':
                    $resolution = 1440;
                    break;
                case 'my':
                    $resolution = 1440;
                    break;
            }
            return $resolution;
        }

        public static function getTimeFromByTypeOfCryptoCurrenciesChart($type, $lastTime)
        {
            switch ($type) {
                case '1d':
                    return $lastTime - 60*60*24;
                    break;
                case '1m':
                    return $lastTime - 60*60*24*30;
                    break;
                case '3m':
                    return $lastTime - 60*60*24*30*3;
                    break;
                case '1y':
                    return $lastTime - 60*60*24*365;
                    break;
                case '5y':
                    return $lastTime - 60*60*24*365*5;
                    break;
                case 'my':
                    return 0;
                    break;
            }
        }

        public static function getDataForCryptoCurrenciesChart($id, $type)
        {
            if (!in_array($type, array('1d', '1m', '3m', '1y', '5y', 'my'))) {
                throw new Exception("Type not supported");
            }

            $symbol = QuotesSymbolsModel::model()->findByPk($id);
            if (!$symbol) {
                throw new Exception("Can't find symbol");
            }

            $c = new CDbCriteria();
            $c->addCondition('`t`.`symbolId` = :id');
            $c->params[':id'] = $id;

            $c->addCondition('`t`.`resolution` = :resolution');
            $c->params[':resolution'] = self::getResolutionByTypeOfCryptoCurrenciesChart($type);

            $c->order = '`t`.`barTime` DESC';
            $lastTimeModel = self::model()->find($c);

            $c->select = '`t`.`barTime`, `t`.`close`';
            $c->addCondition('`t`.`barTime` >= :time');
            $c->params[':time'] = self::getTimeFromByTypeOfCryptoCurrenciesChart($type, $lastTimeModel->barTime);
            $c->order = '`t`.`barTime` ASC';

            $histModels = self::model()->findAll($c);
            $outArr = array();

            foreach ($histModels as $model) {
                $outArr[] = array(
                    $model->barTime * 1000,
                    $model->close
                );
            }

            return [
                'data' => str_replace('"', '', json_encode($outArr)),
                'name' => $symbol->name
            ];
        }
	}
?>