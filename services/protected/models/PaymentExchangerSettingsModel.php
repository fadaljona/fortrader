<?php

Yii::import('models.base.SettingsModelBase');

final class PaymentExchangerSettingsModel extends SettingsModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }
    public static function getModel()
    {
        static $model;
        if (!$model) {
            $model = self::model()->find();
            if (!$model) {
                $model = new self();
                $model->id = 1;
            }
        }
        return $model;
    }
    public function getMainPageOgImageSrc()
    {
        return CommonLib::getCommonImageSrc($this->mainPageOgImage);
    }
    public function getExchangersRatingPageOgImageSrc()
    {
        return CommonLib::getCommonImageSrc($this->exchangersRatingPageOgImage);
    }
    public function getPaymentSystemsRatingPageOgImageSrc()
    {
        return CommonLib::getCommonImageSrc($this->paymentSystemsRatingPageOgImage);
    }
    public function getMainTitle()
    {
        return $this->getTextSetting('mainTitle');
    }
    public function getMainMetaTitle()
    {
        return $this->getTextSetting('mainMetaTitle');
    }
    public function getMainMetaDesc()
    {
        return $this->getTextSetting('mainMetaDesc');
    }
    public function getMainMetaKeys()
    {
        return $this->getTextSetting('mainMetaKeys');
    }
    public function getMainShortDesc()
    {
        return $this->getTextSetting('mainShortDesc');
    }
    public function getMainShortDescWithFormat()
    {
        return $this->getTextWithFormat('mainShortDesc');
    }
    public function getMainFullDescTitle()
    {
        return $this->getTextSetting('mainFullDescTitle');
    }
    public function getMainFullDesc()
    {
        return $this->getTextSetting('mainFullDesc');
    }
    public function getMainFullDescWithFormat()
    {
        return $this->getTextWithFormat('mainFullDesc');
    }

    private function getTextWithFormat($settingName)
    {
        $str = $this->getTextSetting($settingName);
        if (!$str) {
            return false;
        }
        if (strpos($str, PHP_EOL)) {
            $str = str_replace(PHP_EOL, '</div><div class="fx_text">', $str);
        }
        return CHtml::tag('div', array( 'class' => 'fx_text' ), $str);
    }



    public function getDefaultExchangeDirectionTitle()
    {
        return $this->getTextSetting('defaultExchangeDirectionTitle');
    }
    public function getDefaultExchangeDirectionMetaTitle()
    {
        return $this->getTextSetting('defaultExchangeDirectionMetaTitle');
    }
    public function getDefaultExchangeDirectionMetaDesc()
    {
        return $this->getTextSetting('defaultExchangeDirectionMetaDesc');
    }
    public function getDefaultExchangeDirectionMetaKeys()
    {
        return $this->getTextSetting('defaultExchangeDirectionMetaKeys');
    }
    public function getDefaultExchangeDirectionShortDesc()
    {
        return $this->getTextSetting('defaultExchangeDirectionShortDesc');
    }
    public function getDefaultExchangeDirectionFullDescTitle()
    {
        return $this->getTextSetting('defaultExchangeDirectionFullDescTitle');
    }
    public function getDefaultExchangeDirectionFullDesc()
    {
        return $this->getTextSetting('defaultExchangeDirectionFullDesc');
    }


    public function getExchangersRatingTitle()
    {
        return $this->getTextSetting('exchangersRatingTitle');
    }
    public function getExchangersRatingMetaTitle()
    {
        return $this->getTextSetting('exchangersRatingMetaTitle');
    }
    public function getExchangersRatingMetaDesc()
    {
        return $this->getTextSetting('exchangersRatingMetaDesc');
    }
    public function getExchangersRatingMetaKeys()
    {
        return $this->getTextSetting('exchangersRatingMetaKeys');
    }
    public function getExchangersRatingShortDesc()
    {
        return $this->getTextSetting('exchangersRatingShortDesc');
    }
    public function getExchangersRatingFullDescTitle()
    {
        return $this->getTextSetting('exchangersRatingFullDescTitle');
    }
    public function getExchangersRatingFullDesc()
    {
        return $this->getTextSetting('exchangersRatingFullDesc');
    }
    public function getExchangersRatingShortDescWithFormat()
    {
        return $this->getTextWithFormat('exchangersRatingShortDesc');
    }
    public function getExchangersRatingFullDescWithFormat()
    {
        return $this->getTextWithFormat('exchangersRatingFullDesc');
    }




    public static function getIndexUrl()
    {
        return Yii::app()->createAbsoluteUrl('exchangeECurrency/index');
    }
}
