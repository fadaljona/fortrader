<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ClosedTransactionsStatsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function hasStats( $idContents ) {
			$c = new CDbCriteria();
			$c->select = Array(
				" orderSymbol ",
			);
			$c->addCondition(" `idContest` = :idContest ");
			$c->params[':idContest'] = $idContents;
			$c->limit = 1;
		
			$models = self::model()->findAll( $c );
			if( $models )
				return true;
			else
				return false;
		}
		
	}

?>
