<?
	Yii::import( 'models.base.ModelBase' );
	
	final class FeedbackTypeModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'FeedbackTypeI18NModel', 'idType' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'FeedbackTypeI18NModel', 'idType', 
					'on' => '`currentLanguageI18N`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->name )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->name )) return $i18n;
			}
		}
		function getName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->name : '';
		}
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if( strlen( $obj->name ) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = FeedbackTypeI18NModel::model()->findByAttributes( Array( 'idType' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = FeedbackTypeI18NModel::instance( $this->id, $idLanguage );
					}
					$i18n->name = $obj->name;
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
		
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>