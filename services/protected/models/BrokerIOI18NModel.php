<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerIOI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{broker_io_i18n}}";
		}
		static function instance( $idIO, $idLanguage, $name ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idIO', 'idLanguage', 'name' ));
		}
	}

?>