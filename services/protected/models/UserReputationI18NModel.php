<?
	Yii::import( 'models.base.ModelBase' );
	
	final class UserReputationI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{user_reputation_i18n}}";
		}
		static function instance( $idReputation, $idLanguage, $message ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idReputation', 'idLanguage', 'message' ));
		}
	}

?>