<?
	Yii::import( 'models.base.ModelBase' );
	
	final class RssFeedsGroupI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}

		function tableName() {
			return "{{rss_feeds_group_i18n}}";
		}
		static function instance( $idGroup, $idLanguage, $title ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idGroup', 'idLanguage', 'title' ));
		}
	}
?>