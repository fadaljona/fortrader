<?php
Yii::import('models.base.ModelBase');

final class CryptoCurrenciesExchangesSymbolStatsModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public static function instance($idSymbolToSymbol, $volume, $price, $volumePerCent, $date)
    {
        return self::modelFromAssoc(__CLASS__, compact('idSymbolToSymbol', 'volume', 'price', 'volumePerCent', 'date'));
    }
}
