<?
	Yii::import( 'models.base.ModelBase' );

	final class BrokerToBrokerRegulatorModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idBroker, $idRegulator ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker', 'idRegulator' ));
		}
	}

?>