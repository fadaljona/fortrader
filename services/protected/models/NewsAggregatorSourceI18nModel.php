<?
	Yii::import( 'models.base.ModelBase' );
	
	final class NewsAggregatorSourceI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{news_aggregator_source_i18n}}";
		}
		static function instance( $idSource, $idLanguage, $title ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idSource', 'idLanguage', 'title' ));
		}
	}
?>