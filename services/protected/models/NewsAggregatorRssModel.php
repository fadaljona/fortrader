<?
	Yii::import( 'models.base.ModelBase' );
	
	final class NewsAggregatorRssModel extends ModelBase {
		const Aria2cRssTxtList = '/data/newsRssDownloadList.txt';
		const PathRssTxtFiles = '/data/newsRss/tmp';
		const PathRssStoredTxtFiles = '/data/newsRss';
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'lang' => Array( self::HAS_ONE, 'LanguageModel', array( 'id' => 'idLanguage' ) ),
				'source' => Array( self::HAS_ONE, 'NewsAggregatorSourceModel', array( 'id' => 'idSource' ) ),
				'cat' => Array( self::HAS_ONE, 'NewsAggregatorCatsModel', array( 'id' => 'idCat' ) ),
			);
		}
		static function generateTxtList(){
			$models = self::model()->findAll(array(
				'with' => array('source'),
				'select' => array(' `t`.`id` ', ' `t`.`link` '),
				'condition' => " `t`.`enabled` = 1 AND `source`.`enabled` = 1 ",
				'order' => " `t`.`idSource` ASC ",
			));
			$outStr = '';
			$i=0;
			$downloadPath = YiiBase::getPathOfAlias('application') . self::PathRssTxtFiles;
			foreach( $models as $model ){
				if( $i ) $outStr .= "\n";
				$outStr .= "{$model->link}\n dir=$downloadPath\n out={$model->id}.txt";
				$i++;
			}
			if( $outStr ){
				file_put_contents( YiiBase::getPathOfAlias('application') . self::Aria2cRssTxtList, $outStr );
			}
		}
		static function getAdminListDp( $filterModel ){
			$c = new CDbCriteria();
			if( $filterModel ){
				if( strlen( $filterModel->title )) {
					$title = CommonLib::escapeLike( $filterModel->title );
					$title = CommonLib::filterSearch( $title );
					$c->addCondition( " `t`.`title` LIKE :title ");
					$c->params[':title'] = $title;
				}
				if( strlen( $filterModel->link )) {
					$link = CommonLib::escapeLike( $filterModel->link );
					$link = CommonLib::filterSearch( $link );
					$c->addCondition( " `t`.`link` LIKE :link ");
					$c->params[':link'] = $link;
				}
				if( strlen( $filterModel->enabled )) {
					$c->addCondition( " `t`.`enabled` = :enabled ");
					$c->params[':enabled'] = $filterModel->enabled;
				}
				if( strlen( $filterModel->idLanguage )) {
					$c->addCondition( " `t`.`idLanguage` = :idLanguage ");
					$c->params[':idLanguage'] = $filterModel->idLanguage;
				}
				if( strlen( $filterModel->idSource )) {
					$c->addCondition( " `t`.`idSource` = :idSource ");
					$c->params[':idSource'] = $filterModel->idSource;
				}
			}
			$c->order = " `t`.`idSource` ASC ";
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => $c,
			));
			return $DP;
		}
	}

?>