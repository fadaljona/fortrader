<?php

Yii::import('models.base.SettingsModelBase');

final class CryptoCurrenciesSettingsModel extends SettingsModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public static function getModel()
    {
        static $model;
        if (!$model) {
            $model = self::model()->find();
            if (!$model) {
                $model = new self();
                $model->id = 1;
            }
        }
        return $model;
    }

    public function getOgImageSrc()
    {
        if (!$this->ogImage) {
            return false;
        }
        return CommonLib::getCommonImageSrc($this->ogImage);
    }

    public function getTitle()
    {
        return $this->getTextSetting('title');
    }

    public function getMetaTitle()
    {
        return $this->getTextSetting('metaTitle');
    }

    public function getMetaDesc()
    {
        return $this->getTextSetting('metaDesc');
    }

    public function getMetaKeys()
    {
        return $this->getTextSetting('metaKeys');
    }

    public static function getIndexUrl()
    {
        return Yii::app()->createAbsoluteUrl('cryptoCurrencies/index');
    }

    public function relations()
    {
        $relations = parent::relations();
        $relations['bitcoinSymbol'] = array( self::HAS_ONE, 'QuotesSymbolsModel', array('id' => 'bitcoin'));
        return $relations;
    }
}
