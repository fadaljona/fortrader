<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InterestRatesValueI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}

		function tableName() {
			return "{{interest_rates_value_i18n}}";
		}
		static function instance( $idValue, $idLanguage, $link, $postId ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idValue', 'idLanguage', 'link', 'postId' ));
		}
	}
?>