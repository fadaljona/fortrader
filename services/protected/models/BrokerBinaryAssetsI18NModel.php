<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerBinaryAssetsI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{broker_binary_assets_i18n}}";
		}
		static function instance( $idBinaryOption, $idLanguage, $name ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBinaryOption', 'idLanguage', 'name' ));
		}
	}

?>