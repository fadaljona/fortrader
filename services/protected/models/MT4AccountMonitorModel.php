<?
	Yii::import( 'models.base.ModelBase' );

	final class MT4AccountMonitorModel extends ModelBase {
		const PATHStoreFilesDir = 'uploads/contestMembers';

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "mt4_account_monitor";
		}
		
		function relations() {
			return Array(
				'member' => Array( self::HAS_ONE, 'ContestMemberModel', array( 'accountNumber' => 'accountNumber', 'idServer' => 'accountServerId' ) ),
			);
		}
		
		static function hasData( $accountNumber, $serverId ){
			$checkExistData = self::model()->find( Array(
				'condition' => 'accountNumber=:accNum AND accountServerId=:serverId',
				'params'=>array(':accNum'=>$accountNumber, ':serverId'=>$serverId),
				'order' => ' `t`.`dateAdd` DESC ',
			));
			if($checkExistData) return true;
			return false;
		}
		static function hasArchivedData( $accountNumber, $memberId ){
			$zipFileName = self::PATHStoreFilesDir . "/{$memberId}_{$accountNumber}_monitor.zip";
			if( file_exists($zipFileName) ) return true;
			return false;
		}
		static function linkToArchivedData( $accountNumber, $memberId ){
			$zipFileName = self::PATHStoreFilesDir . "/{$memberId}_{$accountNumber}_monitor.zip";
			if( file_exists($zipFileName) ) return Yii::app()->createUrl($zipFileName);
			return false;
		}
		static function getLastData( $accountNumber, $serverId ){
			return self::model()->find( Array(
				'condition' => 'accountNumber=:accNum AND accountServerId=:serverId',
				'params'=>array(':accNum'=>$accountNumber, ':serverId'=>$serverId),
				'order' => ' `t`.`dateAdd` DESC ',
			));
		}
		static function clearData( $idContest ){
			Yii::app()->db->createCommand(
				"DELETE monitor.* FROM mt4_account_monitor monitor
				LEFT JOIN ft_contest_member member ON member.accountNumber = monitor.accountNumber AND member.idServer = monitor.accountServerId
				WHERE member.idContest = :idContest"
			)->execute( array( 'idContest' => $idContest ) );
		}
	}

?>
