<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ContactModel extends ModelBase {
		const PATHUploadDir = 'uploads/contacts';
		const WIDTHMiddle = 100;
		const HEIGHTMiddle = 100;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'ContactI18NModel', 'idContact', 'alias' => 'i18nsContact' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'ContactI18NModel', 'idContact', 
					'alias' => 'cLI18NContact',
					'on' => '`cLI18NContact`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->post )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->post )) return $i18n;
			}
		}
		function getPost() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->post : '';
		}
		function getName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->name : '';
		}
		function getDesc() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->desc : '';
		}
			
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if( strlen( $obj->post ) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = ContactI18NModel::model()->findByAttributes( Array( 'idContact' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = ContactI18NModel::instance( $this->id, $idLanguage );
					}
					$i18n->post = $obj->post;
					$i18n->name = $obj->name;
					$i18n->desc = $obj->desc;
					
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			
			# image
		function getPathImage() {
			return strlen($this->nameImage) ? self::PATHUploadDir."/{$this->nameImage}" : '';
		}
		function getSrcImage() {
			return $this->pathImage ? Yii::app()->baseUrl."/{$this->pathImage}" : '';
		}
		function getPathMiddleImage() {
			return strlen($this->nameMiddleImage) ? self::PATHUploadDir."/{$this->nameMiddleImage}" : '';
		}
		function getSrcMiddleImage() {
			return $this->pathMiddleImage ? Yii::app()->baseUrl."/{$this->pathMiddleImage}" : '';
		}
		function deleteImage() {
			if( strlen( $this->nameImage )) {
				if( is_file( $this->pathImage ) and !@unlink( $this->pathImage )) throw new Exception( "Can't delete {$this->pathImage}" );
				$this->nameImage = null;
			}
			if( strlen( $this->nameMiddleImage )) {
				if( is_file( $this->pathMiddleImage ) and !@unlink( $this->pathMiddleImage )) throw new Exception( "Can't delete {$this->pathMiddleImage}" );
				$this->nameMiddleImage = null;
			}
		}
		function setImage( $name, $nameMiddle ) {
			$this->deleteImage();
			$this->nameImage = $name;
			$this->nameMiddleImage = $nameMiddle;
		}
		function uploadImage( $uploadedFile ) {
			$fileEx = strtolower( $uploadedFile->getExtensionName());
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( self::PATHUploadDir, $fileEx ));
			
			$fileFullName = "{$fileName}.{$fileEx}";
			$filePath = self::PATHUploadDir."/{$fileFullName}";
			if( !@$uploadedFile->saveAs( $filePath )) throw new Exception( "Can't write to ".self::PATHUploadDir );

			$middleFileFullName = "{$fileName}-middle.{$fileEx}";
			$middleFilePath = self::PATHUploadDir."/{$middleFileFullName}";
			ImgResizeLib::resize( $filePath, $middleFilePath, self::WIDTHMiddle, self::HEIGHTMiddle );
			
			$this->setImage( $fileFullName, $middleFileFullName );
		}
		function getMiddleImage( $options = Array() ) {
			$options[ 'src' ] = $this->srcMiddleImage;
			$options[ 'title' ] = $this->name;
			return $this->nameMiddleImage ? CHtml::tag( 'img', $options ) : '';
		}
		
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			$this->deleteImage();
		}
	}

?>