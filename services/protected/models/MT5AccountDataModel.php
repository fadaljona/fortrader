<?
	Yii::import( 'models.base.ModelBase' );

	final class MT5AccountDataModel extends ModelBase {
		public $accountOrdersHistoryTimeAverage;
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "mt5_account_data";
		}
		static function lastRevision($id)
		{
			$data = self::model()
				->find('ACCOUNT_NUMBER='.ContestMemberModel::getAccountNumber($id));
			if ( count($data) > 0 ) {
				return $data->revision;
			}
			return false;
		}
		public function getStatsSumm(){
			$sumVar = 0;
			foreach( $this->getAttributes() as $key => $value ){
				$sumVar += abs($value);
			}
			return $sumVar;
		}
		
		/*
		function findByAccountNumber( $accountNumber ) {
			return $this->find( Array(
				'condition' => " `t`.`ACCOUNT_NUMBER` = :accountNumber ",
				'params' => Array(
					':accountNumber' => $accountNumber,
				),
				'order' => " `t`.`id` DESC ",
				'limit' => 1,
			));
		}
		*/
	}

?>
