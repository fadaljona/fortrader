<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementBlockModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getCurrentBlock() {
			$ip = sprintf( "%u", ip2long( $_SERVER[ 'REMOTE_ADDR' ]));
			$agent = $_SERVER[ 'HTTP_USER_AGENT' ];
			$exists = self::model()->find(Array(
				'condition' => "
					`t`.`ip` = :ip
					OR :agent LIKE `t`.`agent`
				",
				'params' => Array(
					':ip' => $ip,
					':agent' => $agent,
				),
			));
			return $exists;
		}
	}

?>