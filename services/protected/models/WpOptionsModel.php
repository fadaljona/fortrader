<?php
	Yii::import( 'models.base.ModelBase' );
	
	final class WpOptionsModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getSiteName(){
			$model = self::model()->find(array(
				'select' => array( ' `option_value` ' ),
				'condition' => " `option_name` = 'blogname' ",
			));
			return $model->option_value;
		}
		static function getOptionVal( $option ){
			$model = self::model()->find(array(
				'select' => array( ' `option_value` ' ),
				'condition' => " `option_name` = :option ",
				'params' => array( ':option' => $option )
			));
			return $model->option_value;
		}
		function tableName() {
			return "wp_options";
		}
	}

?>