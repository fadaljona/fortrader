<?php
Yii::import('models.base.ModelBase');

final class VideoChanelI18NModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return "{{video_chanel_i18n}}";
    }

    public static function instance($idChanel, $idLanguage, $title, $subTitle, $url)
    {
        return self::modelFromAssoc(__CLASS__, compact('idChanel', 'idLanguage', 'title', 'subTitle', 'url'));
    }
}
