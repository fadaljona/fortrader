<?
	Yii::import( 'models.base.SettingsModelBase' );
	
	final class InformersSettingsModel extends SettingsModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function getModel() {
			static $model;
			if( !$model ) {
				$model = self::model()->find();
				if( !$model ) {
					$model = new self();
					$model->id = 1;
				}
			}
			return $model;
		}
		
		public function getTitle(){
			if( !$this->texts || !$this->texts->title ) return false;
			return $this->texts->title;
		}
		public function getMetaTitle(){
			if( !$this->texts || !$this->texts->metaTitle ) return false;
			return $this->texts->metaTitle;
		}
		public function getMetaDesc(){
			if( !$this->texts || !$this->texts->metaDesc ) return false;
			return $this->texts->metaDesc;
		}
		public function getMetaKeys(){
			if( !$this->texts || !$this->texts->metaKeys ) return false;
			return $this->texts->metaKeys;
		}
		public function getShortDesc(){
			if( !$this->texts || !$this->texts->shortDesc ) return false;
			return $this->texts->shortDesc;
		}
		public function getFullDesc(){
			if( !$this->texts || !$this->texts->fullDesc ) return false;
			return $this->texts->fullDesc;
		}
		public function getInfoLinkText(){
			if( !$this->texts || !$this->texts->infoLinkText ) return Yii::t('*', 'For what need forex informers and how to get them?');
			return $this->texts->infoLinkText;
		}
		public function getInfoLink(){
			if( !$this->texts || !$this->texts->infoLink ) return false;
			return $this->texts->infoLink;
		}
		public function getCatsListTitle(){
			if( !$this->texts || !$this->texts->catsListTitle ) return false;
			return $this->texts->catsListTitle;
		}
		
		static function getIndexUrl(){
			return Yii::app()->createAbsoluteUrl('informers/index');
		}
		
	}
?>