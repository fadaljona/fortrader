<?
	Yii::import( 'models.base.ModelBase' );
	Yii::import( 'components.MailerComponent' );
	
	final class JournalModel extends ModelBase {
        private $oldValue = null;

		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'JournalI18NModel', 'idJournal', 'alias' => 'i18nsJournal' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'JournalI18NModel', 'idJournal', 
					'alias' => 'cLI18NJournal',
					'on' => '`cLI18NJournal`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'stats' => Array( self::HAS_ONE, 'JournalStatsModel', 'idJournal' ),
			);
		}
		public static function findBySlug( $slug ){
			return self::model()->find(array(
				'condition' => " `slug` = :slug ",
				'params' => array( ':slug' => $slug ),
			));
		}
		static function getSlugById( $id ){
			$model = self::model()->findByPk( $id );
			if( !$model ) return false;
			return $model->slug;
		}
		static function getMaxUpdatedDT() {
			$updatedDT = Yii::App()->db->createCommand(" 
				SELECT MAX( `updatedDT` ) FROM `{{journal}}`;
			")->queryScalar();
			return $updatedDT;
		}
		static function updateStats($exists = false) {

			if (!$exists) {
				$exists = Yii::App()->db->createCommand("
					SELECT 		1
					FROM 		`{{journal_stats}}`
					WHERE 		`updatedDT` IS NULL OR DATE(`updatedDT`) < CURDATE()
					LIMIT		1
				")->queryScalar();
			}

			if( $exists ) {
				$command = file_get_contents( "protected/data/updateJournalStats.sql" );
				Yii::App()->db->createCommand( $command )->query();
			}
			
		}
		static function getModelSingleURL( $id ) {
			return Yii::App()->createURL( 'journal/single', Array( 'id' => $id ));
		}
		static function getModelAbsoluteSingleURL( $id ) {
			return Yii::App()->createAbsoluteURL( 'journal/single', Array( 'id' => $id ));
		}
		static function getModelAdminURL( $id ) {
			return Yii::App()->createURL( 'admin/journal/list', Array( 'idEdit' => $id ));
		}
		static function getModelDownloadJournalURL( $id, $url = null ) {
			return Yii::App()->createURL( 'journal/download', Array( 'id' => $id, 'url'=>$url ));
		}
		static function getModelViewJournalURL( $id, $url = null ) {
			return Yii::App()->createURL( 'journal/download', Array( 'id' => $id, 'mode' => 'view', 'url'=>$url ));
		}
		
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->name )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->name )) return $i18n;
			}
		}
		public function getSingleTitle(){
			if( $this->title ){
				$title = $this->title;
			}elseif( $this->name ){
				$title = $this->name;
			}
			return $title;
		}
		function getName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->name : '';
		}
		function getNameCover() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->nameCover : '';
		}
		function getCover( $params = Array() ) {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->getCover( $params, $this->nameCover ) : '';
		}
		function getCoverUrl( ) {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->getCoverWidth( array(), $this->nameCover ) : '';
		}
		function getNameJournal() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->nameJournal : '';
		}
		function getPathJournal() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->pathJournal : '';
		}
		function getCountDownloads() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->countDownloads : '';
		}
		function getCountDownloadsToday() {
			$i18n = $this->getI18N();
			$history = $i18n ? JournalHistoryModel::findToday( $this->id, $i18n->idLanguage ) : null;
			return $history ? $history->countDownloads : 0;
		}
		function getCountPages() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->countPages : '';
		}
		function getDigest() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->digest : '';
		}
		function getPrice() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->price : '';
		}
		function getFree() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->free : '';
		}
		
		
		function getInner() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->inner : '';
		}
		function getTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->title : '';
		}
		function getMetaTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaTitle : '';
		}
		function getMetaDescription() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaDescription : '';
		}
		function getMetaKeywords () {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->metaKeywords : '';
		}
		function getShortDescription () {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->shortDescription : '';
		}
		function getDescription () {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->description : '';
		}
		
		
		function getDownloadJournalURL($url = null) {
			return self::getModelDownloadJournalURL( $this->id, $url);
		}
		function getViewJournalURL($url = null) {
			return self::getModelViewJournalURL( $this->id, $url);
		}
				
		function getSingleURL() {
			return Yii::App()->createURL( 'journal/single', Array( 'slug' => $this->slug ));
		}
		function getAbsoluteSingleURL() {
			return Yii::App()->createAbsoluteURL( 'journal/single', Array( 'slug' => $this->slug ));
		}
		function getAdminURL() {
			return self::getModelAdminURL( $this->id );
		}
		function getCurrentInner() {
			$inners = unserialize( $this->inner );
			$idCurrentLanguage = LanguageModel::getCurrentLanguageID();
			$return = Array();
			
			foreach( $inners as $i=>$inner ) {
				if( strlen( $inner['headers'] )) {
					$header = $inner['headers'];
				}
				if( strlen( $inner['contents'] )) {
					$content = $inner['contents'];
				}
				if( strlen( $inner['url'] )) {
					$url = $inner['url'];
				}
				$return[] = Array(
					'header' => $header,
					'content' => $content,
					'url' => $url
				);
			}
			
			return $return;
		}
		function getMonth() {
			$time = strtotime( $this->date );
			$month = date( 'F', $time );
			return $month;
		}
		function getYear() {
			$time = strtotime( $this->date );
			$year = date( 'Y', $time );
			return $year;
		}
		function downloaded() {
			$i18n = $this->getI18N();
			if( $i18n ) {
				$i18n->countDownloads++;
				$i18n->update( Array( 'countDownloads' ));
				
				JournalHistoryModel::downloaded( $this->id, $i18n->idLanguage );
			}
		}
		
			# stats
		function deleteStats() {
			if( $this->stats ) $this->stats->delete();
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if( !in_array( $idLanguage, $idsLanguages )) continue;
				$notempty = $obj->cover || strlen( $obj->name ) || $obj->journal;
				if( $notempty ) {
					$i18n = JournalI18NModel::model()->findByAttributes( Array( 'idJournal' => $this->id, 'idLanguage' => $idLanguage ));
					
					$formTextFields = AdminJournalFormModel::getTextFields();
					
					if( !$i18n ) {
						
						$arrToCreate = array(
							'idJournal' => $this->id, 
							'idLanguage' => $idLanguage, 
							'free' => $obj->free,
							'inner' => $obj->inner,
						);
						
						foreach( $formTextFields as $formFieldName => $settings ){
							$arrToCreate[ $settings['modelField'] ] = $obj->{$settings['modelField']}; 
						}
						
						$i18n = JournalI18NModel::instance( $arrToCreate );
					}
					else{
						$i18n->free = $obj->free;
						$i18n->inner = $obj->inner;
						
						foreach( $formTextFields as $formFieldName => $settings ){
							$i18n->{$settings['modelField']} = $obj->{$settings['modelField']}; 
						}
						
					}
					if( $obj->cover ) $i18n->uploadCover( $obj->cover );
					if( $obj->journal ) $i18n->uploadJournal( $obj->journal );
					$i18n->save();
				}
				else{
					$i18n = JournalI18NModel::model()->findByAttributes( Array( 'idJournal' => $this->id, 'idLanguage' => $idLanguage ));
					if( $i18n ) $i18n->delete();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->addI18Ns( $i18ns );
		}
			
			# events
		private function noticeUsers() {
			$factoryMailer =  new MailerComponent();
			
			$mailTpl = MailTplModel::findByKey( 'new issue' );
			if( !$mailTpl ) return false;
			
			$DP = new CActiveDataProvider( 'UserModel', Array(
				'criteria' => Array(
					'select' => " ID, user_login, user_email, language ",
					'with' => Array(
						'settings' => Array(
							'joinType' => "INNER JOIN",
						),
					),
					'condition' => "
						`settings`.`reciveNoticeNewJournal` OR `settings`.`reciveEmailNoticeNewJournal`
					",
				)
			));
			$I = new CDataProviderIterator( $DP, 1000 );
			foreach( $I as $user ) {
				$urlJournal = $this->getAbsoluteSingleURL();
				if( $user->settings->reciveNoticeNewJournal ) {
					UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_CREATE_JOURNAL, Array(
						'idUser' => $user->id,
						'idJournal' => $this->id,
					));
				}
				if( $user->settings->reciveEmailNoticeNewJournal) {
					$alias = $user->language;
					$message = $mailTpl->getMessage( $alias );
					$subject = $mailTpl->getSubject( $alias );
					
					if( !strlen( $message ) or !strlen( $subject )) continue;
					
					$message = strtr( $message, Array(
						'%username%' => $user->user_login,
						'%link%' => $urlJournal,
					));
					
					if( !strlen( $user->user_email )) continue;
					
					$mailer = $factoryMailer->instanceMailer();
					$mailer->addAddress( $user->user_email, $user->user_login );
					$mailer->Subject = $subject;
					if( substr_count( $message, '<' )) {
						$message = CommonLib::nl2br( $message, true );
						$mailer->MsgHTML( $message );
					}
					else{
						$mailer->Body = $message;
					}
					
					$result = $mailer->send();
				}
			}
		}
		function getJournalDate() {
			$out = "";
			$time = strtotime( $this->date );
			
			$m = date( "F", $time );
			$m = Yii::t( "g01", $m );
			$y = date( "Y", $time );
			
			$out .= $m;
			
			$out .= " ";
			$out .= $y;
			
			$out .= " №";
			$out .= $this->number;
			return $out;
		}
		protected function afterSave() {
			parent::afterSave();
			if(($this->beenNewRecord && !$this->hide_when_created) || ($this->oldValue && !$this->beenNewRecord)) {
				Yii::App()->db->createCommand( " 
					REPLACE INTO 		`{{journal_stats}}` ( `idJournal` )
					VALUES 				( :id );
				" )->query( Array(
					':id' => $this->id,
				));
			}
			
			if( isset( $_POST['AdminJournalFormModel']['send_now'] ) && $_POST['AdminJournalFormModel']['send_now'] == 1 ){
				$this->noticeUsers();
			}
		}

        protected function beforeSave() {
            $result = parent::beforeSave();
            if( $result ) {
                $this->oldValue = $this->hide_when_created;
                if ($this->beenNewRecord && $this->status == 1) {
                    $this->hide_when_created = true;
                } else $this->hide_when_created = false;
            }
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.journal/single.'.$this->id.'.'  );
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.journal/last.NO.Param.' );
            return $result;
        }

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			$this->deleteStats();
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.journal/single.'.$this->id.'.'  );
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.journal/last.NO.Param.' );
		}
		
		function getJavascriptCode($twidth) {
			$latestUrl = CommonLib::httpsProtocol( Yii::app()->createAbsoluteURL('journal/latest') );
			$coverUrl = CommonLib::httpsProtocol( Yii::app()->createAbsoluteURL('journal/cover') );
			$title = Yii::t('*', "Forex Magazine for Traders");
			$code =
<<<CODE
			<script type="text/javascript">
				document.write("<a href='{$latestUrl}?url="+encodeURIComponent(document.URL)+"&title="+encodeURIComponent(document.title)+"' target='_blank'><img src='{$coverUrl}?url="+encodeURIComponent(document.URL)+"&width=$twidth' alt='' title='{$title}' width='{$twidth}' /></a>");
			</script>
CODE;
			return htmlentities( str_replace("\t", "", str_replace("\n", "", $code)) );
		}

		static function getCountMark($idJournal) {
			return Yii::App()->db->createCommand()
				->select('COUNT(*)')
				->from('{{journal_mark}}')
				->where("`idJournal` = :idJournal")
				->queryScalar(Array(
					':idJournal' => $idJournal
				));

		}
	}

?>
