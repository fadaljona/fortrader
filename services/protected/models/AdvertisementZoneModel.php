<?
	Yii::import( 'models.base.ModelBase' );
	
	final class AdvertisementZoneModel extends ModelBase {
		const PATHToSizesBlock = 'protected/data/availableSizesAdZones.php';
		public $countShows;
		public $countClicks;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'linksToAds' => Array( self::HAS_MANY, 'AdvertisementZoneToAdvertisementModel', 'idZone' ),
				'clicks' => Array( self::HAS_MANY, 'AdvertisementZoneClickModel', 'idZone' ),
			);
		}
		function unlinkFromAds() {
			foreach( $this->linksToAds as $link ) {
				$link->delete();
			}
		}
			# clicks
		function deleteClicks() {
			foreach( $this->clicks as $model ) {
				$model->delete();
			}
		}
		function hitShow() {
			if( !$this->isNewRecord ) {
				Yii::App()->db->createCommand( "
					INSERT INTO		{{advertisement_zone_stats}} ( `idZone`, `date`, `countShows`, `countClicks` )
					VALUES			( :idZone, CURDATE(), 1, 0 )
					ON DUPLICATE KEY UPDATE
									`countShows` = `countShows` + 1;
				" )->query(Array(
					':idZone' => $this->id,
				));
			}
		}
		function hitClick() {
			Yii::App()->db->createCommand( "
				INSERT INTO		{{advertisement_zone_stats}} ( `idZone`, `date`, `countShows`, `countClicks` )
				VALUES			( :idZone, CURDATE(), 0, 1 )
				ON DUPLICATE KEY UPDATE
								`countClicks` = `countClicks` + 1;
			" )->query(Array(
				':idZone' => $this->id,
			));
			$click = AdvertisementZoneClickModel::instance( $this->id );
			$click->save();
		}
		function clearStats() {
			Yii::App()->db->createCommand( "
				DELETE
				FROM			{{advertisement_zone_stats}} 
				WHERE			`idZone` = :idZone
			" )->query(Array(
				':idZone' => $this->id,
			));
			AdvertisementZoneClickModel::clearClicks( $this->id );
		}
		static function getTplCode() {
			$insertToId = '-insertToId-';
			$url = CommonLib::httpsProtocol( Yii::App()->controller->createAbsoluteUrl( '/advertisementZone/jsRender', Array( 'id' => '-idZone-', 'insertToId' => $insertToId )) );
			$code = "for popup ads \n".'<div id="'.$insertToId.'"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("'.$insertToId.'");s = document.createElement("script");s.src = "'.$url.'&width="+screen.width;s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>';
			
			$code .= "\n\n for other ads\n".'<div id="'.$insertToId.'"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("'.$insertToId.'");s = document.createElement("script");s.src = "'.$url.'";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>';
			$code = htmlspecialchars( $code );
			return $code;
		}
		static function parseSizeBlock( $size ) {
			$ex = array_pad( explode( "x", $size, 2 ), 2, '' );
			$ex = array_map( 'trim', $ex );
			return $ex;
		}
		function getCTR() {
			return $this->countShows ? $this->countClicks / $this->countShows * 100 : null;
		}
		function setSizeBlock( $size ) {
			list( $this->widthBlock, $this->heightBlock ) = self::parseSizeBlock( $size );
		}
		function getSizeBlock() {
			if( $this->typeBlock == 'Fixed' ) {
				return $this->widthBlock && $this->heightBlock ? "{$this->widthBlock} x {$this->heightBlock}" : "";
			}
			else{
				return $this->typeBlock;
			}
		}
		function clearSizeBlock() {
			$this->widthBlock = $this->heightBlock = null;
		}
		static function getZoneSizesBlock() {
			$source = require self::PATHToSizesBlock;
			$sizes = Array();
			foreach( $source as $group => $assoc ) {
				$sizes[ $group ] = Array();
				foreach( $assoc as $size=>$count ) {
					$sizes[ $group ][] = $size;
				}
			}
			return $sizes;
		}
		static function getZoneCountAdsBySizeBlock( $size ) {
			$source = require self::PATHToSizesBlock;
			$counts = Array();
			foreach( $source as $group => $assoc ) {
				foreach( $assoc as $_size=>$count ) {
					$counts[ $_size ] = $count;
				}
			}
			return (int)@$counts[ $size ];
		}
		function getCountAds() {
			if( $this->typeBlock == 'Fixed' ) {
				return self::getZoneCountAdsBySizeBlock( $this->getSizeBlock());
			}
			else{
				return $this->countAds;
			}
		}
		function getStructureAds() {
			if( $this->typeBlock == 'Fixed' ) {
				return $this->widthBlock > $this->heightBlock ? 'Horizontal' : 'Vertical';
			}
			else{
				return $this->typeBlock;
			}
		}
		function getSizesAd() {
			$width = $this->widthBlock;
			$height = $this->heightBlock;
			$count = $this->getCountAds();
			
			$width -= 30;
			$height -= 10;
			
			switch( $this->getStructureAds() ) {
				case 'Horizontal':{
					$width = round($width/$count);
					if( $count > 1 ) {
						$width -= 20;
						$height -= 20;
					}
					break;
				}
				case 'Vertical':{
					$height = round($height/$count);
					$width -= 10;
					$height -= 10;
					break;
				}
			}
			
			if( $this->typeBorder == 'Each' ) {
				$width -= 30;
				$height -= 10;
			}
			
			return Array( $width, $height );
		}
		
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->unlinkFromAds();
			$this->deleteClicks();
		}
	}

?>