<?php
Yii::import('models.base.ModelBase');

final class FaqItemI18NModel extends ModelBase
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return "{{faq_item_i18n}}";
    }

    public static function instance($idItem, $idLanguage, $question, $answer)
    {
        return self::modelFromAssoc(__CLASS__, compact('idItem', 'idLanguage', 'question', 'answer'));
    }
}
