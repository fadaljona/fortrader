<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CurrencyRatesI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{currency_rates_i18n}}";
		}
		static function instance( 
			$idCurrency, 
			$idLanguage, 
			$name, 
			$title, 
			$metaTitle, 
			$metaDesc, 
			$metaKeys, 
			$shortDesc, 
			$fullDesc, 
			$chartTitle, 
			$converterTitle, 
			$namePlurals, 
			$toName, 
			$informerName, 
			$singleArchiveYearTitle, 
			$singleArchiveYearMetaTitle, 
			$singleArchiveYearMetaDesc, 
			$singleArchiveYearMetaKeys, 
			$singleArchiveYearMoTitle, 
			$singleArchiveYearMoMetaTitle, 
			$singleArchiveYearMoMetaDesc, 
			$singleArchiveYearMoMetaKeys, 
			$singleArchiveYearMoDayTitle, 
			$singleArchiveYearMoDayMetaTitle, 
			$singleArchiveYearMoDayMetaDesc, 
			$singleArchiveYearMoDayMetaKeys, 
			$singleArchiveYearDescription, 
			$singleArchiveYearFullDesc, 
			$singleArchiveYearMoDescription, 
			$singleArchiveYearMoFullDesc, 
			$singleArchiveYearMoDayDescription, 
			$singleArchiveYearMoDayFullDesc,
			$ecbTitle,
			$ecbMetaTitle,
			$ecbMetaDesc,
			$ecbMetaKeys,
			$ecbShortDesc,
			$ecbFullDesc,
			$ecbChartTitle,
			$ecbConverterTitle,
			$ecbSingleArchiveYearTitle,
			$ecbSingleArchiveYearMetaTitle,
			$ecbSingleArchiveYearMetaDesc,
			$ecbSingleArchiveYearMetaKeys,
			$ecbSingleArchiveYearMoTitle,
			$ecbSingleArchiveYearMoMetaTitle,
			$ecbSingleArchiveYearMoMetaDesc,
			$ecbSingleArchiveYearMoMetaKeys,
			$ecbSingleArchiveYearMoDayTitle,
			$ecbSingleArchiveYearMoDayMetaTitle,
			$ecbSingleArchiveYearMoDayMetaDesc,
			$ecbSingleArchiveYearMoDayMetaKeys,
			$ecbSingleArchiveYearDescription,
			$ecbSingleArchiveYearFullDesc,
			$ecbSingleArchiveYearMoDescription,
			$ecbSingleArchiveYearMoFullDesc,
			$ecbSingleArchiveYearMoDayDescription,
			$ecbSingleArchiveYearMoDayFullDesc
		) {
			return self::modelFromAssoc( __CLASS__, compact( 
				'idCurrency', 
				'idLanguage', 
				'name', 
				'title', 
				'metaTitle', 
				'metaDesc', 
				'metaKeys', 
				'shortDesc', 
				'fullDesc', 
				'chartTitle', 
				'converterTitle', 
				'namePlurals', 
				'toName', 
				'informerName', 
				'singleArchiveYearTitle', 
				'singleArchiveYearMetaTitle', 
				'singleArchiveYearMetaDesc', 
				'singleArchiveYearMetaKeys', 
				'singleArchiveYearMoTitle', 
				'singleArchiveYearMoMetaTitle', 
				'singleArchiveYearMoMetaDesc', 
				'singleArchiveYearMoMetaKeys', 
				'singleArchiveYearMoDayTitle', 
				'singleArchiveYearMoDayMetaTitle', 
				'singleArchiveYearMoDayMetaDesc', 
				'singleArchiveYearMoDayMetaKeys', 
				'singleArchiveYearDescription', 
				'singleArchiveYearFullDesc', 
				'singleArchiveYearMoDescription', 
				'singleArchiveYearMoFullDesc', 
				'singleArchiveYearMoDayDescription', 
				'singleArchiveYearMoDayFullDesc',
				'ecbTitle',
				'ecbMetaTitle',
				'ecbMetaDesc',
				'ecbMetaKeys',
				'ecbShortDesc',
				'ecbFullDesc',
				'ecbChartTitle',
				'ecbConverterTitle',
				'ecbSingleArchiveYearTitle',
				'ecbSingleArchiveYearMetaTitle',
				'ecbSingleArchiveYearMetaDesc',
				'ecbSingleArchiveYearMetaKeys',
				'ecbSingleArchiveYearMoTitle',
				'ecbSingleArchiveYearMoMetaTitle',
				'ecbSingleArchiveYearMoMetaDesc',
				'ecbSingleArchiveYearMoMetaKeys',
				'ecbSingleArchiveYearMoDayTitle',
				'ecbSingleArchiveYearMoDayMetaTitle',
				'ecbSingleArchiveYearMoDayMetaDesc',
				'ecbSingleArchiveYearMoDayMetaKeys',
				'ecbSingleArchiveYearDescription',
				'ecbSingleArchiveYearFullDesc',
				'ecbSingleArchiveYearMoDescription',
				'ecbSingleArchiveYearMoFullDesc',
				'ecbSingleArchiveYearMoDayDescription',
				'ecbSingleArchiveYearMoDayFullDesc'
			));
		}
	}

?>