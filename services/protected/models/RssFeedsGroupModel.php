<?
	Yii::import( 'models.base.ModelBase' );
	
	final class RssFeedsGroupModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'RssFeedsGroupI18nModel', 'idGroup', 'alias' => 'i18nsRssFeedsGroup' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'RssFeedsGroupI18nModel', 'idGroup', 
					'alias' => 'cLI18NRssFeedsGroupModel',
					'on' => '`cLI18NRssFeedsGroupModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
			);
		}

		static function getAllNewsUrl(){
			return Yii::app()->createUrl('newsAggregator/index');
		}
		static function getAdminListDp( $filterModel = false ){
			$DP = new CActiveDataProvider( get_called_class(), Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`order` DESC ',
				),
			));
			return $DP;
		}

		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->name )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->name )) return $i18n;
			}
		}
		function getTitle() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->title : '';
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
		
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->title )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = RssFeedsGroupI18nModel::model()->findByAttributes( Array( 'idGroup' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = RssFeedsGroupI18nModel::instance( $this->id, $idLanguage, $obj->title );
					}else{
						$i18n->title = $obj->title;
					}
					$i18n->save();
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			# events

		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
		}
	}

?>