<?php
	Yii::import( 'models.base.ModelBase' );

	final class CalendarGroupSubscriptionUserToSeriousModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
        static function instance( $idUser, $serious ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idUser', 'serious' ));
		}
	
	}

?>
