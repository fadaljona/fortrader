<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerBinaryOptionModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'i18ns' => Array( self::HAS_MANY, 'BrokerBinaryOptionI18NModel', 'idBinaryOption', 'alias' => 'i18nsBrokerBinaryOption' ),
				'currentLanguageI18N' => Array( self::HAS_ONE, 'BrokerBinaryOptionI18NModel', 'idBinaryOption', 
					'alias' => 'cLI18NBrokerBinaryOptionModel',
					'on' => '`cLI18NBrokerBinaryOptionModel`.`idLanguage` = :idLanguage',
					'params' => Array(
						':idLanguage' => LanguageModel::getCurrentLanguageID(),
					),
				),
				'linksToBrokers' => Array( self::HAS_MANY, 'BrokerToBrokerBinaryOptionModel', 'idBinaryOption' ),
			);
		}
		function getI18N() {
			if( $this->currentLanguageI18N ) return $this->currentLanguageI18N;
			foreach( $this->i18ns as $i18n ) {
				if( $i18n->idLanguage == 0 ) {
					if( strlen( $i18n->name )) return $i18n;
					break;
				}
			}
			foreach( $this->i18ns as $i18n ) {
				if( strlen( $i18n->name )) return $i18n;
			}
		}
		function getName() {
			$i18n = $this->getI18N();
			return $i18n ? $i18n->name : '';
		}
		
			# i18ns
		function deleteI18Ns() {
			foreach( $this->i18ns as $i18n ) {
				$i18n->delete();
			}
		}
		function addI18Ns( $i18ns ) {
			$idsLanguages = LanguageModel::getExistsIDS();
			foreach( $i18ns as $idLanguage=>$obj ) {
				if(( strlen( $obj->name )) and in_array( $idLanguage, $idsLanguages )) {
					$i18n = BrokerBinaryOptionI18NModel::model()->findByAttributes( Array( 'idBinaryOption' => $this->id, 'idLanguage' => $idLanguage ));
					if( !$i18n ) {
						$i18n = BrokerBinaryOptionI18NModel::instance( $this->id, $idLanguage, $obj->name );
						$i18n->save();
					}
					elseif( $i18n->name != $obj->name ){
						$i18n->name = $obj->name;
						$i18n->save();
					}
				}
			}
		}
		function setI18Ns( $i18ns ) {
			$this->deleteI18Ns();
			$this->addI18Ns( $i18ns );
		}
			
			# brokers
		function unlinkFromBrokers() {
			foreach( $this->linksToBrokers as $link ) {
				$link->delete();
			}
		}
		
			# events
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteI18Ns();
			$this->unlinkFromBrokers();
		}
	}

?>