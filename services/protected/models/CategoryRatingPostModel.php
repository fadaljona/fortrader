<?
	Yii::import( 'models.base.ModelBase' );
	
	final class CategoryRatingPostModel extends ModelBase {
	
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "wp_posts";
		}
		
		function relations() {
			return Array(
				'ratingData' => Array( self::HAS_ONE, 'CategoryRatingItemDataModel', array('idPost' => 'ID' ) ),
				'postsStats' => Array( self::HAS_ONE, 'PostsStatsCommonModel', array('postId' => 'ID' ) ),
				'stats' => Array( self::HAS_ONE, 'CategoryRatingItemStatsModel', array('idPost' => 'ID' ) ),
			);
		}

		static function getAdminListDp( $filterModel = false ){
			if( !isset( $_GET['idRating'] ) ) throw new Exception( "No rating selected" );
			$ratingModel = CategoryRatingModel::model()->findByPk( $_GET['idRating'] );
			if( !$ratingModel ) throw new Exception( "Can't find rating" );
		
			
			
			$categoryChilds = CategoryRatingModel::getChildsOfCategory( $ratingModel->catId );
			$categoryChilds[] = $ratingModel->catId;
			
			
			$c = new CDbCriteria();
			$c->with['termTaxonomy'] = array('with' => 'term');
			$c->with['categoryRatingPost'] = array( 'with' => 'ratingData' );
			$c->addCondition( " `termTaxonomy`.`taxonomy` = 'category' AND `termTaxonomy`.`term_id` IN (".implode(',', $categoryChilds).") " );
			$c->group = " `categoryRatingPost`.`ID` ";
			
			if( $filterModel ){
				if( $filterModel->term_order ){
					$termName = CommonLib::escapeLike( $filterModel->term_order );
					$termName = CommonLib::filterSearch( $termName );
					$c->addCondition( "
						`term`.`name` LIKE :termName
					");
					$c->params[':termName'] = $termName;
				}
				
				if( $filterModel->object_id ){
					$postTitle = CommonLib::escapeLike( $filterModel->object_id );
					$postTitle = CommonLib::filterSearch( $postTitle );
					$c->addCondition( "
						`categoryRatingPost`.`post_title` LIKE :postTitle
					");
					$c->params[':postTitle'] = $postTitle;
				}


				
				if( $filterModel->term_taxonomy_id === '0' || $filterModel->term_taxonomy_id == 1 ){

					if( $filterModel->term_taxonomy_id == 0 ){
						$c->addCondition( "
							`ratingData`.`inRating` = :inRating AND `ratingData`.`inRating` IS NOT NULL
						");
					}else{
						$c->addCondition( "
							`ratingData`.`inRating` = :inRating OR `ratingData`.`inRating` IS NULL
						");
					}
						
					$c->params[':inRating'] = $filterModel->term_taxonomy_id;
				}
				
			}
			
			$DP = new CActiveDataProvider( 'WPTermRelationshipModel', Array(
				'criteria' => $c,
				'sort' => array(
					'sortVar' => 'sort',
					'attributes' => array(
						'categoryRatingPost.ID' => array(
							'default'=>'desc',
						),
					),
					'defaultOrder'=>array(
						'categoryRatingPost.ID'=>CSort::SORT_DESC,
					)
				)
			));
			
			
			return $DP;
		}
		
		public function getPostUrl(){
			
			if( $this->postsStats && $this->postsStats->url ) return $this->postsStats->url;
			
			CommonLib::loadWp();
			
			$url = get_permalink( $this->ID );
			$parsedUrl = parse_url( $url );
	
			if( !$this->postsStats ){
				
				
				
				$model = new PostsStatsCommonModel;
				$model->postId = $this->ID;
				$model->url = $parsedUrl['path'];
				$model->title = $this->post_title;
				$model->postDate = get_the_date( 'Y-m-d H:i:s', $this->ID );
				$model->save();
				return $model->url;
			}
			
			$this->postsStats->url = $parsedUrl['path'];
			$this->postsStats->save();
			return $this->postsStats->url;
		}
		
		public function getId(){ return $this->ID; }
		public function getTitleForList(){
			if( $this->ratingData ){
				if( $this->ratingData->title ) return '<span itemprop="name">' . $this->ratingData->title . '</span> <span class="tooltip" itemprop="description">'. mb_substr( strip_tags($this->post_content), 0, 50 ).'...</span>';
			}
			return '<span itemprop="name">' . $this->post_title . '</span>  <span class="tooltip" itemprop="description">'. mb_substr( strip_tags($this->post_content), 0, 50 ).'...</span>'; 
		}
		public function getTitle(){
			if( $this->ratingData ){
				if( $this->ratingData->title ) return $this->ratingData->title;
			}
			return ''; 
		}

		
		function setData( $data ){
			
			$ratingData = CategoryRatingItemDataModel::model()->findByAttributes( Array( 'idPost' => $this->id ));
			if( !$ratingData ){
				$ratingData = CategoryRatingItemDataModel::instance( $this->id, $data->title, $data->inRating );
			}else{
				$ratingData->title = $data->title;
				$ratingData->inRating = $data->inRating;
			}
			$ratingData->save();
		}
		
		

	}

?>