<?
	Yii::import( 'models.base.ModelBase' );
	
	final class ThankModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
			);
		}
		static function instance( $instance, $idObject, $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'instance', 'idObject', 'idUser' ));
		}
		static function getCount( $instance, $idObject ) {
			return self::model()->count(Array(
				'condition' => "
					`t`.`instance` = :instance
					AND `t`.`idObject` = :idObject
				",
				'params' => Array(
					':instance' => $instance,
					':idObject' => $idObject
				),
			));
		}
		
			# events
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				if( $this->instance == 'eaVersion/single' ) {
					UserReputationModel::event( UserReputationModel::TYPE_EVENT_CREATE_THANK_EA_VERSION, Array( 'idUser' => $this->idUser ));
				}
				if( $this->instance == 'eaStatement/single' ) {
					UserReputationModel::event( UserReputationModel::TYPE_EVENT_CREATE_THANK_EA_STATEMENT, Array( 'idUser' => $this->idUser ));
				}
			}
		}
	}

?>