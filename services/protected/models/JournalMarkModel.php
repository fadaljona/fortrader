<?
	Yii::import( 'models.base.ModelBase' );
	
	final class JournalMarkModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function instance( $idJournal, $idUser, $value ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idJournal', 'idUser', 'value' ));
		}
	}

?>