<?
	Yii::import( 'models.base.ModelBase' );
	
	final class JournalI18NModel extends ModelBase {
		const PATHUploadDir = 'uploads/journals';
		const patternWideID = "ru.fortrader.services.journal.%s.%d";
		const baseUrl = '/services';
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{journal_i18n}}";
		}
		function relations() {
			return Array(
				'language' => Array( self::BELONGS_TO, 'LanguageModel', 'idLanguage' ),
			);
		}
		static function instance( $inpArr ) {
			return self::modelFromAssoc( __CLASS__, $inpArr);
		}
		function getWideID() {
			$language = LanguageModel::getByID( $this->idLanguage );
			$wideID = sprintf( self::patternWideID, $language->alias, $this->idJournal );
			return $wideID;
		}
			# cover
		function getPathCover() {
			return strlen($this->nameCover) ? self::PATHUploadDir."/{$this->nameCover}" : '';
		}
		function getSrcCover( $absolute = false ) {
			return $this->pathCover ? self::baseUrl."/{$this->pathCover}" : '';
		}
		function removeDirectory($dir) {
			if ($objs = glob($dir."/*")) {
				foreach($objs as $obj) {
					is_dir($obj) ? removeDirectory($obj) : unlink($obj);
				}
			}
			rmdir($dir);
		}
		function deleteCover() {
			if( strlen( $this->nameCover )) {
				if( is_file( $this->pathCover ) and !@unlink( $this->pathCover )) throw new Exception( "Can't delete {$this->pathCover}" );
				$dir = dirname( $this->pathCover );
				//if( $dir != self::PATHUploadDir and is_dir( $dir ) and !@rmdir( $dir )) throw new Exception( "Can't delete {$dir}" );
				$this->removeDirectory($dir);
				$this->nameCover = null;
			}
		}
		function setCover( $name ) {
			$this->deleteCover();
			$this->nameCover = $name;
		}
		function uploadCover( $uploadedFile ) {
			$dirName = CommonLib::getFreeFileName( self::PATHUploadDir );
			$filePath = self::PATHUploadDir."/{$dirName}";
			is_dir( $filePath ) or mkdir( $filePath );
			
			$fileName = $uploadedFile->getName();
			$fileRelPath = "{$dirName}/{$fileName}";
			$fileFullPath = "{$filePath}/{$fileName}";
			
			if( !@$uploadedFile->saveAs( $fileFullPath )) throw new Exception( "Can't write to ".self::PATHUploadDir );
			$this->setCover( $fileRelPath );
		}
		
		static function toFilesDomainUrl( $src, $noFilesDomain ){
			if( $noFilesDomain ) return $src;
			if( !isset( Yii::app()->params['uploadsUrl'] ) ) return $src;
			return str_replace( self::baseUrl . '/uploads', Yii::app()->params['uploadsUrl'], $src);
		}
		static function getCoverWidth( $inpOptions, $nameCover, $noFilesDomain = false ) {
		
			$srcCover = strlen($nameCover) ? self::PATHUploadDir."/{$nameCover}" : '';
			$srcCover = $srcCover ? self::baseUrl."/{$srcCover}" : '';
		
			if( isset($inpOptions['style']) ){
				$style = $inpOptions['style'];
				preg_match('/width:([0-9]+).*/',$style,$width_match);
				$width = $width_match[1];
			}
			if( isset($inpOptions['width']) ){
				$width = $inpOptions['width'];
			}
		
			$image_src = $_SERVER['DOCUMENT_ROOT'].$srcCover;
		
			if( !file_exists( $image_src ) ) return self::toFilesDomainUrl( $srcCover, $noFilesDomain );
		
			if( isset($width) ){
				$image_src_ext = substr($srcCover, strrpos($srcCover, '.' ) );
				$image_src_without_ext = substr($srcCover, 0, strrpos($srcCover, '.' ) );
				
				if( file_exists( $_SERVER['DOCUMENT_ROOT'].$image_src_without_ext.'_'.$width.$image_src_ext ) ) 
					return self::toFilesDomainUrl( $image_src_without_ext.'_'.$width.$image_src_ext, $noFilesDomain );
				
				
				$image = new EasyImage( $image_src );
				$image->resize( $width );
				$image->save( $_SERVER['DOCUMENT_ROOT'].$image_src_without_ext.'_'.$width.$image_src_ext );
				
				return self::toFilesDomainUrl( $image_src_without_ext.'_'.$width.$image_src_ext, $noFilesDomain );
			}else
				return self::toFilesDomainUrl( $srcCover, $noFilesDomain );
		}
		
		function getCover( $inpOptions = Array(), $nameCover ) {
		
			$options[ 'src' ] = $this->getCoverWidth( $inpOptions, $nameCover );
			$options[ 'title' ] = $this->name;
			$options[ 'alt' ] = $this->name;
			if( isset($inpOptions['style']) ) $options[ 'style' ] = $inpOptions['style'];
			
			return $nameCover ? CHtml::tag( 'img', $options ) : '';
		}
			
			# journal
		function getPathJournal() {
			return strlen($this->nameJournal) ? self::PATHUploadDir."/{$this->nameJournal}" : '';
		}
		function getSrcJournal( $absolute = false ) {
			return $this->pathJournal ? self::baseUrl."/{$this->pathJournal}" : '';
		}
		function deleteJournal() {
			if( strlen( $this->nameJournal )) {
				if( is_file( $this->pathJournal ) and !@unlink( $this->pathJournal )) throw new Exception( "Can't delete {$this->pathJournal}" );
				$dir = dirname( $this->pathJournal );
				if( $dir != self::PATHUploadDir and is_dir( $dir ) and !@rmdir( $dir )) throw new Exception( "Can't delete {$dir}" );
				$this->nameJournal = null;
			}
		}
		function setJournal( $name ) {
			$this->deleteJournal();
			$this->nameJournal = $name;
		}
		function uploadJournal( $uploadedFile ) {
			$dirName = CommonLib::getFreeFileName( self::PATHUploadDir );
			$filePath = self::PATHUploadDir."/{$dirName}";
			is_dir( $filePath ) or mkdir( $filePath );
			
			$fileName = $uploadedFile->getName();
			$fileRelPath = "{$dirName}/{$fileName}";
			$fileFullPath = "{$filePath}/{$fileName}";
			
			if( !@$uploadedFile->saveAs( $fileFullPath )) throw new Exception( "Can't write to ".$fileFullPath . ' ' . $uploadedFile->getError() );
			$this->setJournal( $fileRelPath );
		}
		
		protected function afterDelete() {
			parent::afterDelete();
			$this->deleteCover();
			$this->deleteJournal();
		}
	}

?>