<?
	Yii::import( 'models.base.ModelBase' );
	
	final class QuotesHistAliasModel extends ModelBase {
		
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		
		static function getFormAliases( $symbolId ){
			if( !$symbolId ) return '';
			$models = self::model()->findAll(Array(
				'select' => array('alias'),
				'condition' => " `t`.`symbolId` = :id ",
				'params' => array(':id' => $symbolId ),
			));
			$returnStr = '';
			$i=0;
			foreach( $models as $model ){
				if($i) $returnStr .= ',';
				$returnStr .= $model->alias;
				$i++;
			}
			return $returnStr;
		}
		
		static function setFormAliases( $symbolId, $aliases ){
			if( !$aliases ) $aliases = array();
			
			$aliasArr = array();
			$oldAliases = Yii::app()->db->createCommand()
				->select('alias')
				->from( '{{quotes_hist_alias}}' )
				->where( 'symbolId = :symbolId', array(':symbolId' => $symbolId) )
				->queryAll(); 
			foreach($oldAliases as $alias){
				$aliasArr[] = $alias['alias'];
			}
			
			$aliasesToCreate = array_diff( $aliases, $aliasArr );
			$aliasesToDelete = array_diff( $aliasArr, $aliases );
			
			if( count($aliasesToCreate) ){
				foreach( $aliasesToCreate as $alias ){
					if( $alias ){
						$model = new self;
						$model->symbolId = $symbolId;
						$model->alias = $alias;
						if( $model->validate() )$model->save();
					}
				}
			}
			
			if( count($aliasesToDelete) ){
				foreach( $aliasesToDelete as $alias ){
					if( $alias ){
						
						self::model()->find(Array( 
							'condition' => " `t`.`symbolId` = :id AND `t`.`alias` = :alias ", 
							'params' => array(':id' => $symbolId, 'alias' => $alias ) )
						)->delete();
						
					}
				}
			}
			
		}
	}

?>