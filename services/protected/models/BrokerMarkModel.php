<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerMarkModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function relations() {
			return Array(
				'user' => Array( self::BELONGS_TO, 'UserModel', 'idUser' ),
			);
		}
		static function instance( $idBroker, $idUser ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker', 'idUser' ));
		}
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( !$this->ip ) {
					$this->ip = sprintf( "%u", ip2long( $_SERVER[ 'REMOTE_ADDR' ]));
				}
			}
			return $result;
		}
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_BROKER_MARK, Array( 'idUser' => $this->idUser, 'idBroker' => $this->idBroker, 'trust' => $this->trust ));
				BrokerStats2Model::inc( $this->idBroker, "countMarks" );
			}
		}
	}

?>