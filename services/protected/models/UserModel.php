<?
	Yii::import( 'models.base.ModelBase' );
	Yii::import( 'components.MailerComponent' );
	
	final class UserModel extends ModelBase {
		const rootID = -1;
		const rootName = 'root';
		const rootLoginPrefix = '@';
		const ttlOnline = 300;
		const ttlLongOnline = CommonLib::secInDay;
		const PATHUploadAvatarDir = 'uploads/users/avatars';
		const PATHRandomAvatarsDir = 'protected/data/avatars';
		const PATHRandomAvatarsDirFemale = 'protected/data/avatarsFemale';
		const WIDTHAvatar = 36;
		const HEIGHTAvatar = 36;
		const WIDTHMiddleAvatar = 180;
		const HEIGHTMiddleAvatar = 200;
		const MAXLoginLen = 60;
		const MAXPasswordLen = 64;
		const MINPasswordLen = 5;
		const MAXNiceNameLen = 50;
		const MAXDisplayNameLen = 250;
		const MAXEMailLen = 100;
		const MAXURLLen = 100;
		public $group;
		public $utc;
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		static function fromAssoc( $assoc ) {
			return self::modelFromAssoc( __CLASS__, $assoc );
		}
		function tableName() {
			return "wp_users";
		}

		function relations() {
			return Array(
				'avatar' => Array( self::HAS_ONE, 'UserAvatarModel', 'idUser' ),
				'settings' => Array( self::HAS_ONE, 'UserSettingsModel', 'idUser' ),
				'groups' => Array( self::MANY_MANY, 'UserGroupModel', '{{user_to_user_group}}(idUser,idUserGroup)' ),
				'linksToGroups' => Array( self::HAS_MANY, 'UserToUserGroupModel', 'idUser' ),
				'memberContests' => Array( self::HAS_MANY, 'ContestMemberModel', 'idUser' ),
				'country' => Array( self::BELONGS_TO, 'CountryModel', 'idCountry' ),
				'balanceChanges' => Array( self::HAS_MANY, 'UserBalanceChangeModel', 'idUser', 'order' => '`balanceChanges`.`createdDT`' ),
				'advertisementCampaigns' => Array( self::HAS_MANY, 'AdvertisementCampaignModel', 'idUser' ),
				'messages' => Array( self::HAS_MANY, 'UserMessageModel', 'idUser' ),
				'broker' => Array( self::BELONGS_TO, 'BrokerModel', 'idBroker' ),
				'linksToBrokers' => Array( self::HAS_MANY, 'BrokerToUserModel', 'idUser' ),
				'brokers' => Array( self::MANY_MANY, 'BrokerModel', '{{broker_to_user}}(idUser,idBroker)' ),
				
				'privateMessagesTo' => Array( self::HAS_MANY, 'UserPrivateMessageModel', 'idTo' ),
				'privateMessagesFrom' => Array( self::HAS_MANY, 'UserPrivateMessageModel', 'idFrom' ),
				'countPrivateMessages' => Array( self::STAT, 'UserPrivateMessageModel', 'idTo' ),
				'countNotViewedPrivateMessages' => Array( self::STAT, 'UserPrivateMessageModel', 'idTo', 'condition' => " NOT `viewed` " ),
				
				'notices' => Array( self::HAS_MANY, 'UserNoticeModel', 'idUser' ),
				'countNotices' => Array( self::STAT, 'UserNoticeModel', 'idUser' ),
				'countNotViewedNotices' => Array( self::STAT, 'UserNoticeModel', 'idUser', 'condition' => " NOT `viewed` " ),
				
				'reputations' => Array( self::HAS_MANY, 'UserReputationModel', 'idTo' ),
				'activities' => Array( self::HAS_MANY, 'UserActivityModel', 'idUser' ),
				'brokerMarks' => Array( self::HAS_MANY, 'BrokerMarkModel', 'idUser' ),
				
				'EAStatements' => Array( self::HAS_MANY, 'EAStatementModel', 'idUser' ),
				'profile' => Array( self::HAS_ONE, 'UserProfileModel', 'idUser' ),
				'countTradingAccounts' => Array( self::STAT, 'ContestMemberModel', 'idUser' ),
				'countEA' => Array( self::STAT, 'EAModel', 'idUser' ),
				
				'mailingTypesMute' => Array( self::HAS_MANY, 'UserToMailingTypeMuteModel', 'idUser' ),
				
				'karma' => Array( self::HAS_ONE, 'UserKarmaModel', 'id' ),
			);
		}
		static function getListURL( $params = Array() ) {
			return Yii::App()->createURL( 'user/list', $params );
		}	
		static function getModelProfileURL( $id, $isCurrentUser = false, $strictParams = false ) {
			$params = $isCurrentUser && !$strictParams ? Array() : Array( 'id' => $id );
			return Yii::App()->createUrl( 'user/profile', $params );
		}
		static function getModelUnsubscribeMailingTypeURL( $idUser, $idType, $isCurrentUser = false ) {
			$params = $isCurrentUser ? Array() : Array( 'idUser' => $idUser );
			$params[ 'idType' ] = $idType;
			return Yii::App()->createUrl( 'user/unsubscribeMailingType', $params );
		}
		static function getModelProfileAbsoluteURL( $id, $isCurrentUser = false, $strictParams = false ) {
			$params = $isCurrentUser && !$strictParams ? Array() : Array( 'id' => $id );
			return Yii::App()->createAbsoluteUrl( 'user/profile', $params );
		}
		static function getModelUnsubscribeMailingTypeAbsoluteURL( $emailUser, $idType ) {
			$key = ModelBase::crypt( $emailUser );
			$params = Array( 
				'key' => $key,
				'idType' => $idType,
			);
			return Yii::App()->createAbsoluteUrl( 'user/unsubscribeMailingType', $params );
		}

		public function getDescription(){
			$metaModel = UserMetaModel::model()->find(array(
				'condition' => ' `t`.`user_id` = :id AND `t`.`meta_key` = "description" ',
				'params' => array( ':id' => $this->id )
			));
			if( $metaModel && $metaModel->meta_value ) return $metaModel->meta_value;
			return $this->about;
        }
        
        public function getWpCapabilities()
        {
            $metaModel = UserMetaModel::model()->find(array(
				'condition' => ' `t`.`user_id` = :id AND `t`.`meta_key` = "wp_capabilities" ',
				'params' => array( ':id' => $this->id )
			));
			if ($metaModel && $metaModel->meta_value) {
                return unserialize($metaModel->meta_value);
            }
			return false;
        }

        public function userHaveWpRoles(array $roles)
        {
            if (!$roles) {
                return false;
            }

            $wpCapabilities = $this->wpCapabilities;

            if (!$wpCapabilities) {
                return false;
            }

            foreach ($roles as $role) {
                if (!empty($wpCapabilities[$role]) && $wpCapabilities[$role]) {
                    return true;
                }
            }

            return false;
        }

		public function setDescription( $val ){
			$metaModel = UserMetaModel::model()->find(array(
				'condition' => ' `t`.`user_id` = :id AND `t`.`meta_key` = "description" ',
				'params' => array( ':id' => $this->id )
			));
			if( !$metaModel ){
				$metaModel = new UserMetaModel;
				$metaModel->user_id = $this->id;
				$metaModel->meta_key = "description";
			}
			$metaModel->meta_value = $val;
			if( $metaModel->validate() ) $metaModel->save();
		}
			# finds
		static protected function findRootForAuthenticate( $login ) {
			if( @$login[0] == self::rootLoginPrefix ) {
				$username = Yii::App()->db->username;
				if( $login == self::rootLoginPrefix.$username ) {
					return self::createRootModel();
				}
			}
			return null;
		}
		function findForAuthenticate( $login ) {
			$user = $this->find( Array( 
				'condition' => "
					`user_login` = :login
					OR (
						`user_email` != ''
						AND `user_email` = :login
					)
				",
				'limit' => 1,
				'params' => Array(
					':login' => strtolower( $login ),
				),
			));
			if( !$user ) $user = self::findRootForAuthenticate( $login );
			return $user;
		}
		function findByPk( $id, $condition = '', $params = Array()) {
			if( self::isRootID( $id )) return self::createRootModel();
			return parent::findByPk( $id, $condition, $params );
		}
		static function findBySlug( $slug ) {
			return self::model()->find(array(
				'condition' => ' `t`.`user_nicename` = :userNicename ',
				'params' => array( ':userNicename' => $slug )
			));
		}
			
			# root
		static protected function createRootModel() {
			$user = new self();
			$user->id = self::rootID;
			$user->user_login = self::rootName;
			return $user;
		}
		static protected function isRootID( $id ) {
			return $id == self::rootID;
		}
		function isRoot() {
			return self::isRootID( $this->id );
		}
			
			# password
		static protected function isRootPassword( $password ) {
			$rootPassword = Yii::App()->db->password;
			return $password == $rootPassword;
		}
		function isPassword( $password ) {
			if( $this->isRoot()) return self::isRootPassword( $password );
			
			CommonLib::loadWp();
			
			return wp_check_password( $password, $this->user_pass );
		}
			
			# fields
		function getShowName() {
			if( $this->isRoot()) return self::rootName;
			if( strlen( $this->display_name )) return $this->display_name;
			if( strlen( $this->user_nicename )) return $this->user_nicename;
			if( strlen( $this->user_login )) return $this->user_login;
			return "#{$this->id}";
		}
		function getWpShowName() {
			if( $this->isRoot()) return self::rootName;
			
			if( strlen( $this->firstName ) && strlen( $this->lastName ) ) return $this->firstName .' '. $this->lastName;
			if( strlen( $this->firstName ) ) return $this->firstName;
			if( strlen( $this->lastName ) ) return $this->lastName;

			if( strlen( $this->display_name )) return $this->display_name;
			if( strlen( $this->user_nicename )) return $this->user_nicename;
			if( strlen( $this->user_login )) return $this->user_login;
			return "#{$this->id}";
		}
		function getProfileURL( $strictParams = false ) {
			return $this->singleURL;
		}
		function getUnsubscribeMailingTypeURL( $idType ) {
			return self::getModelUnsubscribeMailingTypeURL( $this->id, $idType, $this->isCurrentUser());
		}
		function getUnsubscribeMailingTypeAbsoluteURL( $idType ) {
			return self::getModelUnsubscribeMailingTypeAbsoluteURL( $this->user_email, $idType );
		}
		function getRedirectUserUrl() {
			$params = Array( 'id' => $this->id );
			return Yii::App()->createUrl( 'user/redirectURL', $params );
		}
		function setPassword( $password ) {
			
			CommonLib::loadWp();
			
			$hash = wp_hash_password($password);
			$this->user_pass = $hash;
		}
		function isCurrentUser() {
			return $this->id == Yii::App()->user->id; 
		}
		function isOnline() {
			$time = @strtotime( $this->usedDT );
			if( !$time ) return false;
			
			$currentTime = time();
			$diff = $currentTime - $time;
			
			return $diff <= self::ttlOnline;
		}
		function isLongOnline() {
			$time = @strtotime( $this->usedDT );
			if( !$time ) return false;
			
			$currentTime = time();
			$diff = $currentTime - $time;
			
			return $diff <= self::ttlLongOnline;
		}
		function isRegisteredInContest( $idContest ) {
			return ContestMemberModel::model()->exists( Array( 
				'condition' => "
					`t`.`idContest` = :idContest
					AND `t`.`idUser` = :idUser
				",
				'params' => Array(
					':idContest' => $idContest,
					':idUser' => $this->id,
				),
			));
		}
			
			# avatar
		function getImgAvatar( ) {
			$baseUrl = Yii::app()->baseUrl;
			$url = $this->avatar ? "{$baseUrl}/{$this->avatar->path}" : "{$baseUrl}/assets-static/avatars/noavatar-thumb.jpg";
			return Yii::app()->createAbsoluteUrl($url);
		}
		function getHTMLAvatar( $params = Array() ) {
			$baseUrl = Yii::app()->baseUrl;
			$params[ 'src' ] = $this->avatar ? "{$baseUrl}/{$this->avatar->thumbPath}" : "{$baseUrl}/assets-static/avatars/noavatar-thumb.jpg";
			$img = CHtml::tag( 'img', $params );
			return $img;
		}
		function getHTMLMiddleAvatar( $params = Array() ) {
			$baseUrl = Yii::app()->baseUrl;
			$params[ 'src' ] = $this->avatar ? "{$baseUrl}/{$this->avatar->middlePath}" : "{$baseUrl}/assets-static/avatars/noavatar-middle.jpg";
			$img = CHtml::tag( 'img', $params );
			return $img;
		}
		function getSizeAvatar( $width="", $height="") {
			$cache = 'user_'.$this->id.'_'.md5($this->id . $width . $height).'.jpg';
			if ( file_exists($_SERVER['DOCUMENT_ROOT'].'/cachethumb/'.$cache) )  {
				return '/cachethumb/'.$cache;
			}else{
				
				$avatar_src = $this->avatar->path;
				$defaultAva = 0;
				if( !$avatar_src ) {
		
					$avatars = glob( $_SERVER['DOCUMENT_ROOT'] . "/services/protected/data/avatars/*" );
					$i = array_rand( $avatars );
					$avatar_src = $avatars[$i];
					$avatar_src = preg_replace( '/.*services\//', '', $avatar_src );
					
				}
				define("P75_URI_TO_CACHE", '/cachethumb/'); // thumbnail directory				
				require_once $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/simple-post-thumbnails/tmbGenerator.php';
				return generateTmb( array(
					's' => '/services/'.$avatar_src,
					'w' => $width,
					'h' => $height,
					'author' => $this->id
				) );
			}
		}
		function deleteAvatar() {
			if( $this->avatar ) $this->avatar->delete();
		}
		function setAvatar( $path, $thumbPath, $middlePath ) {
			$this->deleteAvatar();
			$avatar = UserAvatarModel::instance( $this->id, $path, $thumbPath, $middlePath );
			$avatar->save();
		}
		function uploadAvatar( $uploadedFile ) {
			$fileEx = $uploadedFile instanceof CUploadedFile ? strtolower( $uploadedFile->getExtensionName()) : CommonLib::getExtension( $uploadedFile );
			list( $fileName, $fileEx ) = explode( ".", CommonLib::getFreeFileName( Yii::app()->basePath . '/../' . self::PATHUploadAvatarDir, $fileEx ));
			
			$filePath=Yii::app()->basePath . '/../' . self::PATHUploadAvatarDir."/{$fileName}.{$fileEx}";
			$filePathForDb=self::PATHUploadAvatarDir."/{$fileName}.{$fileEx}";
		
			if( $uploadedFile instanceof CUploadedFile ) {
				if( !@$uploadedFile->saveAs( $filePath )) return false;//throw new Exception( "Can't write to ".Yii::app()->basePath . '/../' . self::PATHUploadAvatarDir );
			}
			else{
				if( !copy( $uploadedFile, $filePath )) return false;//throw new Exception( "Can't write to ".Yii::app()->basePath . '/../' . self::PATHUploadAvatarDir );
			}

			$thumbFilePath = Yii::app()->basePath . '/../' . self::PATHUploadAvatarDir."/{$fileName}-thumb.{$fileEx}";
			$thumbFilePathForDb = self::PATHUploadAvatarDir."/{$fileName}-thumb.{$fileEx}";
			ImgResizeLib::resize( $filePath, $thumbFilePath, self::WIDTHAvatar, self::HEIGHTAvatar );
		
			$middleFilePath = Yii::app()->basePath . '/../' . self::PATHUploadAvatarDir."/{$fileName}-middle.{$fileEx}";
			$middleFilePathForDb = self::PATHUploadAvatarDir."/{$fileName}-middle.{$fileEx}";
			ImgResizeLib::resize( $filePath, $middleFilePath, self::WIDTHMiddleAvatar, self::HEIGHTMiddleAvatar );
			
			$this->setAvatar( $filePathForDb, $thumbFilePathForDb, $middleFilePathForDb );
	
			$wpCachePath = Yii::app()->basePath."/../../cachethumb/";
			foreach ( glob( $wpCachePath."user_{$this->ID}_*") as $file ) {
				if( $file != "." && $file != "..") unlink( $file );
			}
		}
		function uploadRandomAvatar( $fromWP = false ) {
			$pathToRandDir = '';
			if($fromWP) $pathToRandDir = Yii::app()->basePath . '/../' ;
			switch( $this->sex ) {
				case 'Male':{
					$path = $pathToRandDir . self::PATHRandomAvatarsDir;
					break;
				}
				case 'Female':{
					$path = $pathToRandDir . self::PATHRandomAvatarsDirFemale;
					break;
				}
			}
			$avatars = glob( "{$path}/*" );
			if( $avatars ) {
				$i = array_rand( $avatars );
				$this->uploadAvatar( $avatars[$i] );
			}
		}
			
			# settings
		function getSettings() {
			if( !$this->settings ) $this->settings = UserSettingsModel::instance( $this->id );
			return $this->settings;
		}
		function deleteSettings() {
			if( $this->settings ) $this->settings->delete();
		}
		
			# profile
		function getProfileToForm( $userForm ) {
			if( !$this->profile ) return false;
			$table = $this->profile->getMetaData()->tableSchema;
			$fields = $table->getColumnNames();
			foreach( $fields as $field ) {
				$key = "profile_{$field}";
				if( !property_exists( $userForm, $key )) continue;
				$userForm->$key = $this->profile->$field;
			}
		}
		function setProfileFromForm( $userForm ) {
			$profile = $this->profile ? $this->profile : UserProfileModel::instance( $this->id );
			$table = $profile->getMetaData()->tableSchema;
			$fields = $table->getColumnNames();
			foreach( $fields as $field ) {
				$key = "profile_{$field}";
				if( !property_exists( $userForm, $key )) continue;
				$profile->$field = $userForm->$key;
			}
			$r = $profile->save();
		}
		function deleteProfile() {
			if( $this->profile ) $this->profile->delete();
		}
		public function getSingleURL(){
			return Yii::app()->createUrl('user/single', array('slug' => $this->user_nicename));
		}
		public function getAbsoluteSingleURL(){
			return Yii::app()->createAbsoluteUrl('user/single', array('slug' => $this->user_nicename));
		}
		
			# groups
		function getIDsGroups() {
			$ids = Array();
			foreach( $this->groups as $group ) {
				$ids[] = $group->id;
			}
			return $ids;
		}
		function unlinkFromGroups() {
			foreach( $this->linksToGroups as $link ) {
				$link->delete();
			}
		}
		function linkToGroups( $ids ) {
			foreach( $ids as $idGroup ) {
				$link = UserToUserGroupModel::model()->findByAttributes( Array( 'idUser' => $this->id, 'idUserGroup' => $idGroup ));
				if( !$link ) {
					$link = UserToUserGroupModel::instance( $this->id, $idGroup );
					$link->save();
				}
			}
		}
		function setGroups( $ids ) {
			$oldIDs = $this->getIDsGroups();
			if( array_diff( $ids, $oldIDs ) or array_diff( $oldIDs, $ids )) {
				$this->unlinkFromGroups();
				$this->linkToGroups( $ids );
			}
		}
		
			# memberContests
		function deleteMemberContests() {
			foreach( $this->memberContests as $member ) {
				$member->delete();
			}
		}
		
			# balanceChanges
		function deleteBalanceChanges() {
			foreach( $this->balanceChanges as $model ) {
				$model->delete();
			}
		}
		
			# advertisementCampaigns
		function deleteAdvertisementCampaigns() {
			foreach( $this->advertisementCampaigns as $model ) {
				$model->delete();
			}
		}
		
			# messages
		function deleteMessages() {
			foreach( $this->messages as $model ) {
				$model->delete();
			}
		}
		
		function unlinkFromBrokers() {
			foreach( $this->linksToBrokers as $model ) {
				$model->delete();
			}
		}
			
			# private messages
		function deletePrivateMessages() {
			foreach( $this->privateMessagesTo as $model ) {
				$model->delete();
			}
			foreach( $this->privateMessagesFrom as $model ) {
				$model->delete();
			}
		}
		
			# notices
		function deleteNotices() {
			foreach( $this->notices as $model ) {
				$model->delete();
			}
		}	
	
			# activities
		function deleteActivities() {
			foreach( $this->activities as $model ) {
				$model->delete();
			}
		}	
		
			# reputations
		function deleteReputations() {
			foreach( $this->reputations as $model ) {
				$model->delete();
			}
		}	
		
			# brokerMarks
		function deleteBrokerMarks() {
			foreach( $this->brokerMarks as $model ) {
				$model->delete();
			}
		}
		
			# EAStatements
		function deleteEAStatements() {
			foreach( $this->EAStatements as $statsment ) {
				$statsment->delete();
			}
		}
		
		function subBalance( $value ) {
			if( $this->balance > 0 and $value >= $this->balance ) {
				UserModelNoticeLowBalanceAction::notice( $this->id );
			}
			$this->balance -= $value;
			$this->balance = max( $this->balance, 0 );
			$this->update( Array( 'balance' ));
		}
		
		function deleteMailingTypesMute( $idsTypes = null ) {
			foreach( $this->mailingTypesMute as $model ) {
				if( !$idsTypes or in_array( $model->idType, $idsTypes )) {
					$model->delete();
				}
			}
		}
		function addMailingTypesMute( $idsTypes ) {
			foreach( $idsTypes as $idType ) {
				$mute = UserToMailingTypeMuteModel::model()->findByAttributes( Array( 'idUser' => $this->id, 'idType' => $idType ));
				if( !$mute ) {
					$mute = UserToMailingTypeMuteModel::instance( $this->id, $idType );
					$mute->save();
				}
			}
		}
		function setMailingTypesMute( $idsTypes ) {
			$this->deleteMailingTypesMute();
			$this->addMailingTypesMute( $idsTypes );
		}
		
			# events
		protected function beforeSave() {
			$result = parent::beforeSave();
			if( $result ) {
				if( $this->isNewRecord ) {
					$this->user_registered = date( "Y-m-d H:i:s" );
					$this->user_nicename = $this->user_login;
				}
			}
			return $result;
		}
		protected function afterSave() {
			parent::afterSave();
			if( $this->beenNewRecord ) {
				UserActivityModel::event( UserActivityModel::TYPE_EVENT_CREATE_USER, Array( 'idUser' => $this->id ));
				$this->uploadRandomAvatar();
			}
			
		
			$firstName = UserMetaModel::model()->find(Array(
				'condition' => " `t`.`user_id` = :user_id AND `t`.`meta_key` = 'first_name' ",
				'params' => array( ':user_id' => $this->id ),
			));
			if( !$firstName ){
				$firstName = new UserMetaModel;
				$firstName->user_id = $this->id;
				$firstName->meta_key = 'first_name';
			}
			$firstName->meta_value = $this->firstName;
			if( $firstName->validate() ) $firstName->save();

			$lastName = UserMetaModel::model()->find(Array(
				'condition' => " `t`.`user_id` = :user_id AND `t`.`meta_key` = 'last_name' ",
				'params' => array( ':user_id' => $this->id ),
			));
			if( !$lastName ){
				$lastName = new UserMetaModel;
				$lastName->user_id = $this->id;
				$lastName->meta_key = 'last_name';
			}
			$lastName->meta_value = $this->lastName;
			if( $lastName->validate() ) $lastName->save();

		}
		protected function afterDelete() {
			parent::afterDelete();
			$this->unlinkFromGroups();
			$this->deleteAvatar();
			$this->deleteMemberContests();
			$this->deleteBalanceChanges();
			$this->deleteAdvertisementCampaigns();
			$this->deleteMessages();
			$this->unlinkFromBrokers();
			$this->deletePrivateMessages();
			$this->deleteNotices();
			$this->deleteActivities();
			$this->deleteReputations();
			$this->deleteBrokerMarks();
			$this->deleteEAStatements();
			$this->deleteProfile();
		}
		function save( $runValidation=true, $attributes=null ) {
			if( $this->isRoot()) return true;
			return parent::save( $runValidation, $attributes );
		}
		
			# dumms
		function getID() {
			return $this->ID;
		}
		function setID( $id ) {
			$this->ID = $id;
		}
	}

	
	abstract class UserModelNoticeLowBalanceAction {
		const idAdmin = 1;
		private static function getMailer() {
			$factoryMailer =  new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			return $mailer;
		}
		private static function getMailTpl() {
			$key = 'Adverster balance end';
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			if( !$mailTpl ) self::throwI18NException( "Can't load mail template! ({key})", Array( '{key}' => $key ));
			return $mailTpl;
		}
		private static function getMailTplI18N( $mailTpl ) {
			$i18n = $mailTpl->currentLanguageI18N;
			if( $i18n and strlen( $i18n->message )) return $i18n;
			foreach( $mailTpl->i18ns as $i18n ) if( strlen( $i18n->message )) return $i18n;

			self::throwI18NException( "Can't load i18n for mail template!" );
		}
		private static function renderMessage( $user, $message ) {
			$url = Yii::App()->createAbsoluteURL( '/admin/userBalance/list', Array( 'idEdit' => $user->id ));
			return strtr( $message, Array(
				'%username%' => $user->user_login,
				'%link%' => $url,
			));
		}
		private static function sendMail( $emails, $user ) {
			$mailer = self::getMailer();
			$mailTpl = self::getMailTpl();
			$i18n = self::getMailTplI18N( $mailTpl );
						
			$message = self::renderMessage( $user, $i18n->message );
			
			$emails = explode( ",", $emails );
			$emails = array_map( 'trim', $emails );
			foreach( $emails as $email ) {
				$mailer->AddAddress( $email );
			}
			$mailer->Subject = $i18n->subject;
			if( substr_count( $message, '<' )) {
				$message = CommonLib::nl2br( $message, true );
				$mailer->MsgHTML( $message );
			}
			else{
				$mailer->Body = $message;
			}
			
			$result = $mailer->Send();
			$error = $result ? null : $mailer->ErrorInfo;
			if( $error ) throw new Exception( $error );
		}
		private static function createNotice( $user ) {
			UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_LOW_USER_BALANCE, Array(
				'idUser' => self::idAdmin,
				'idUserTarget' => $user->id,
			));
		}
		static function notice( $idUser ) {
			$user = UserModel::model()->findByPk( $idUser );
			if( !$user ) return false;
			
			$settings = AdvertisementSettingsModel::getModel();
			if( !$settings->endBanalce ) return false;
			
			if( $settings->sendEmail and strlen( $settings->emails )) {
				self::sendMail( $settings->emails, $user );
			}
			self::createNotice( $user );
		}
		
	}
?>
