<?
	Yii::import( 'models.base.ModelBase' );
	
	final class InterestRatesBankI18nModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}

		function tableName() {
			return "{{interest_rates_bank_i18n}}";
		}
		static function instance( $idBank, $idLanguage, $title, $shortTitle, $showInFirstChart, $showInSecondChart ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBank', 'idLanguage', 'title', 'shortTitle' ));
		}
	}
?>