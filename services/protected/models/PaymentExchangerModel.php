<?php

Yii::import('models.base.ModelBase');

final class PaymentExchangerModel extends ModelBase
{
    const PATHUploadDir = 'uploads/paymentSystems';
    const COURSES_COUNT_KEY = 'PaymentExchangerModel.courses_count_key';

    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function relations()
    {
        return array(
            'i18ns' => array( self::HAS_MANY, 'PaymentExchangerI18nModel', 'idExchanger', 'alias' => 'i18nsPaymentExchanger' ),
            'currentLanguageI18N' => array( self::HAS_ONE, 'PaymentExchangerI18nModel', 'idExchanger',
                'alias' => 'cLI18NPaymentExchangerModel',
                'on' => '`cLI18NPaymentExchangerModel`.`idLanguage` = :idLanguage',
                'params' => array(
                    ':idLanguage' => LanguageModel::getCurrentLanguageID(),
                ),
            ),
            'courses' => array( self::HAS_MANY, 'PaymentExchangerCourseModel', 'idExchanger',
                'on' => ' `courses`.`date` = :date ',
                'params' => array(':date' => date('Y-m-d'))
            ),
            'exchangerVoteStats' => array( self::HAS_ONE, 'PaymentExchangerItemStatsModel', 'idExchanger'),
            'country' => array( self::BELONGS_TO, 'CountryModel', 'idCountry' ),
        );
    }

    public static function getAdminListDp($filterModel = false)
    {
        $DP = new CActiveDataProvider(get_called_class(), array(
            'criteria' => array(
                'with' => array( 'currentLanguageI18N', 'i18ns', 'country' ),
                'order' => ' `t`.`id` DESC ',
            ),
        ));
        return $DP;
    }
    public function getAvailableLangs()
    {
        $langsArr = array();
        foreach ($this->i18ns as $i18n) {
            if ($i18n->title) {
                $langsArr[] = LanguageModel::getLangFromLangsFileByID($i18n->idLanguage);
            }
        }
        return $langsArr;
    }
    public function getCoursesCount()
    {
        $cacheKey = self::COURSES_COUNT_KEY . $this->id;
        
        if ($count = Yii::app()->cache->get($cacheKey)) {
            return $count;
        }
        
        $count = Yii::App()->db->createCommand(" 
			SELECT 	count( `idExchanger` )
            FROM	`{{payment_exchanger_course}}`
            WHERE `idExchanger` = :id AND `date` = :date;
		")->queryScalar(array(
            ':id' => $this->id,
            ':date' => date('Y-m-d')
        ));

        Yii::app()->cache->set($cacheKey, $count, 60*5);
        return $count;
    }

    public static function getParserNames()
    {
        $outArr = array();
        foreach (glob(Yii::getPathOfAlias('components.exchangersParsers') . "/*Parser.php") as $filename) {
            $parserName = str_replace('Parser.php', '', basename($filename));
            $outArr[$parserName] = $parserName;
        }
        return $outArr;
    }



    public function getPathLogo()
    {
        return strlen($this->logo) ? self::PATHUploadDir."/{$this->logo}" : '';
    }
    public function getSrcLogo()
    {
        return $this->pathLogo ? Yii::app()->baseUrl."/{$this->pathLogo}" : '';
    }
    public function getAbsoluteSrcLogo()
    {
        return $this->pathLogo ? Yii::app()->createAbsoluteUrl($this->pathLogo) : '';
    }
    public function uploadLogo($uploadedFile)
    {
        $fileEx = strtolower($uploadedFile->getExtensionName());
        list( $fileName, $fileEx ) = explode(".", CommonLib::getFreeFileName(self::PATHUploadDir, $fileEx));

        $fileFullName = "{$fileName}.{$fileEx}";
        $filePath = self::PATHUploadDir."/{$fileFullName}";
        if (!@$uploadedFile->saveAs($filePath)) {
            throw new Exception("Can't write to ".self::PATHUploadDir);
        }

        $this->setLogo($fileFullName);
    }
    public function setLogo($name)
    {
        $this->deleteLogo();
        $this->logo = $name;
    }
    public function deleteLogo()
    {
        if (strlen($this->logo)) {
            if (is_file($this->pathLogo) and !@unlink($this->pathLogo)) {
                throw new Exception("Can't delete {$this->pathLogo}");
            }
            $this->logo = null;
        }
    }
    public function getSingleURL()
    {
        //return Yii::App()->createURL( 'informers/single', Array( 'slug' => $this->slug ));
    }

    public function getI18N()
    {
        if ($this->currentLanguageI18N) {
            return $this->currentLanguageI18N;
        }
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == 0) {
                if (strlen($i18n->title)) {
                    return $i18n;
                }
                break;
            }
        }
        foreach ($this->i18ns as $i18n) {
            if (strlen($i18n->title)) {
                return $i18n;
            }
        }
    }
    public function getTitleByLang($langId)
    {
        foreach ($this->i18ns as $i18n) {
            if ($i18n->idLanguage == $langId) {
                if (strlen($i18n->title)) {
                    return $i18n->title;
                }
                return false;
            }
        }
    }

    public function getTitle()
    {
        $i18n = $this->getI18N();
        return $i18n ? $i18n->title : '';
    }

    public static function updateStats($exists = false)
    {
        if (!$exists) {
            $exists = Yii::App()->db->createCommand("
                SELECT 		1
                FROM 		`{{payment_exchanger_item_stats}}`
                WHERE 		`updatedDT` IS NULL OR DATE(`updatedDT`) < CURDATE()
                LIMIT		1
            ")->queryScalar();
        }

        if ($exists) {
            $command = file_get_contents("protected/data/updatePaymentExchangerItemStats.sql");
            Yii::App()->db->createCommand($command)->query();
        }
    }



        # i18ns
    public function deleteI18Ns()
    {
        foreach ($this->i18ns as $i18n) {
            $i18n->delete();
        }
    }
    public function addI18Ns($i18ns)
    {
        $idsLanguages = LanguageModel::getExistsIDS();
        foreach ($i18ns as $idLanguage => $obj) {
            $i18n = PaymentExchangerI18nModel::model()->findByAttributes(array( 'idExchanger' => $this->id, 'idLanguage' => $idLanguage ));
            if (!$i18n) {
                $i18n = PaymentExchangerI18nModel::instance($this->id, $idLanguage, $obj->title);
            } else {
                $i18n->title = $obj->title;
            }
            $i18n->save();
        }
    }
    public function setI18Ns($i18ns)
    {
        $this->addI18Ns($i18ns);
    }
        # events
    protected function afterDelete()
    {
        parent::afterDelete();
        $this->deleteI18Ns();
    }
}
