<?
	Yii::import( 'models.base.ModelBase' );
	
	final class BrokerI18NModel extends ModelBase {
		static function model( $class = __CLASS__ ) {
			return parent::model( $class );
		}
		function tableName() {
			return "{{broker_i18n}}";
		}
		static function instance( $idBroker, $idLanguage, $officialName, $brandName, $shortName, $AM, $demoAccount, $metaTitle, $metaDesc, $metaKeys, $maxProfit, $shortDesc, $fullDesc ) {
			return self::modelFromAssoc( __CLASS__, compact( 'idBroker', 'idLanguage', 'officialName', 'brandName', 'shortName', 'AM', 'demoAccount', 'metaTitle', 'metaDesc', 'metaKeys', 'maxProfit', 'shortDesc', 'fullDesc' ));
		}
	}

?>