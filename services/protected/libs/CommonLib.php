<?
    abstract class CommonLib {
        const PATTERN_IP = "#^\d+\.\d+\.\d+\.\d+$#";
        const PATTERN_IP_PORT = "#^\d+\.\d+\.\d+\.\d+(\:\d+)?$#";
        const maxByte = 255;
        const maxWord = 65535;
        const maxUInt = 4294967295;
        const secInDay = 86400;
        const kb = 1024;
        const numberPattern = '#^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$#';
        const datePattern = '#^\d{4}-\d{2}-\d{2}$#';
        const PATHImagesUploadDir = 'uploads/common';
        
        static function commonImageUpload($file, $nameToSave){
            $fileEx = strtolower( $file->getExtensionName());
            
            $fileFullName = "{$nameToSave}.{$fileEx}";
            $filePath = self::PATHImagesUploadDir."/{$fileFullName}";
            
            $outArr = array('error' => '');
            
            if( !@$file->saveAs( $filePath )){
                $outArr['error'] = "Can't write to ".self::PATHImagesUploadDir;
            }else{
                $outArr['img'] = $fileFullName;
                $outArr['initialPreview'] = "<img src='".Yii::app()->createAbsoluteUrl(self::PATHImagesUploadDir . '/' . $fileFullName)."' class='file-preview-image' style='width:auto;height:160px;'>";
                $outArr['initialPreviewConfig'] = array(
                    array(
                        'caption' => $fileFullName,
                        'url' => Yii::app()->createUrl('admin/uploadImages/ajaxImageDelete'),
                        'key' => $fileFullName,
                        'size' => $file->size
                    )
                );
            }
            return json_encode( $outArr );
        }
        static function commonImageDelete($nameImage){
            $filePath = self::PATHImagesUploadDir."/{$nameImage}";
            $outArr = array('error' => '');
            if( !file_exists($filePath) ){
                $outArr = array('error' => "file doesn't exist");
            }
            unlink($filePath);
            return json_encode( $outArr );
        }
        static function getInitialPreview($nameImage){
            $filePath = self::PATHImagesUploadDir."/{$nameImage}";
            if( !file_exists($filePath) ) return false;
            return '
            initialPreview: [
                "<img src=\''.Yii::app()->createAbsoluteUrl(self::PATHImagesUploadDir . '/' . $nameImage . '?v=' . filemtime($filePath)).'\' class=\'file-preview-image\' style=\'width:auto;height:160px;\'>",
            ],
            initialPreviewConfig: [{
                caption: "'.$nameImage.'",
                url: "'.Yii::app()->createUrl('admin/uploadImages/ajaxImageDelete').'",
                key: "'.$nameImage.'",
                size: '.filesize($filePath).'
            }]';
        }
        static function getCommonImageSrc($nameImage){
            $filePath = self::PATHImagesUploadDir."/{$nameImage}";
            if( !file_exists($filePath) ) return false;
            return Yii::app()->createAbsoluteUrl(self::PATHImagesUploadDir . '/' . $nameImage . '?v=' . filemtime($filePath));
        }
        
        static function boolToStr( $bool ) {
            return $bool ? 'true' : 'false';
        }
        static function isID( $var ) {
            return intval( $var ) > 0;
        }
        static function isNumNoNegative( $var ) {
            return intval( $var ) >= 0;
        }
        static function isIDorZero( $var ) {
            return intval( $var ) > 0 || $var == 0;
        }
        static function isNum( $var ) {
            return preg_match( self::numberPattern, $var );
        }
        static function isDate( $var ) {
            return preg_match( self::datePattern, $var );
        }
        static function toFloat( $var ) {
            return (float)$var;
        }
        static function enableDBProfiling() {
            Yii::App()->db->enableProfiling = true;
        }
        static function disableDBProfiling() {
            Yii::App()->db->enableProfiling = false;
        }
        static function getDBProfiling() {
            return Yii::App()->db->enableProfiling;
        }
        static function getAppProfileLogRoute() {
            $routes = Yii::App()->log->getRoutes()->toArray();
            foreach( $routes as $route ) {
                if( $route instanceof CProfileLogRoute ) {
                    return $route;
                }
            }
            return null;
        }
        static function disableAppProfileLog() {
            if( $profileLogRoute = self::getAppProfileLogRoute()) {
                $profileLogRoute->enabled = false;
            }
        }
        static function enableAppProfileLog() {
            if( $profileLogRoute = self::getAppProfileLogRoute()) {
                $profileLogRoute->enabled = true;
            }
        }
        static function escapeLike( $text ) {
            return strtr( $text, Array( '%' => '\%', '_' => '\_', '\\' => '\\\\' ));
        }
        static function crypt( $data, $key ) {
            return base64_encode($data);
        }
        static function decrypt( $data, $key ) {
            return base64_decode( $data );
        }
        static function explodeComplexEmail( $cEmail ) {
            $name = $email = "";
            if( preg_match( "#<.+>#", $cEmail )) {
                $name = preg_replace( "#<.+>#", "", $cEmail );
                $email = preg_match( "#<(.+)>#", $cEmail, $match ) ? $match[1] : '';
            }
            elseif( strlen( $cEmail )) {
                $ex = explode( " ", $cEmail );
                $email = array_pop( $ex );
                $name = implode( " ", $ex );
            }
            $name = trim( $name );
            $email = trim( $email );
            return Array( $name, $email );
        }
        static function numberFormat( $value, $decimals = 2, $dec_point = ".", $thousands_sep = " " ) {
            if( !$value ) return 0;
            
            do {
                $result = number_format( $value, $decimals, $dec_point, $thousands_sep );
                $decimals++;
            } while( !(float)$result and $decimals <= 10 );
            
            $result = preg_replace( "#\\{$dec_point}0+$#", '', $result );
            $result = preg_replace( "#\\{$dec_point}([1-9]+)0+$#", "{$dec_point}\$1", $result );
            return $result;
        }
        static function filesizeFormat( $size ) {
            $units = Array( "B", "KB", "MB", "GB", "PB" );
            
            $p = $size > 0 ? floor( log( $size, self::kb )) : 0;
            if( $p ) $size /= pow( self::kb, $p );
            $unit = $units[$p];
            
            $size = self::numberFormat( $size );
            return "{$size} {$unit}";
        }
        static function sliceFunc( $array, $func ) {
            $return = Array();
            foreach( $array as &$element ) {
                $return[] = $element->$func();
            }
            return $return;
        }
        static function slice( $array, $field ) {
            $return = Array();
            foreach( $array as &$element ) {
                $return[] = is_object( $element ) ? $element->$field : $element[ $field ];
            }
            return $return;
        }
        static function sliceSum( $array, $field ) {
            $sum = 0;
            foreach( $array as &$element ) {
                $sum += $element->$field;
            }
            return $sum;
        }
        static function toAssoc( $array, $fieldKey, $fieldVal = null ) {
            $return = Array();
            foreach( $array as &$element ) {
                $return[$element->$fieldKey] = $fieldVal ? $element->$fieldVal : $element;
            }
            return $return;
        }
        static function mergeAssocs() {
            $args = func_get_args();
            $res = array_shift( $args );
            while( !empty( $args )) {
                $next = array_shift( $args );
                foreach( $next as $k => $v ) {
                    $res[$k] = $v;
                }
            }
            return $res;
        }
        static function mergeAssocsR() {
            $args = func_get_args();
            $res = array_shift( $args );
            while( !empty( $args )) {
                $next = array_shift( $args );
                foreach( $next as $k => $v ) {
                    if( isset( $res[$k] ) and is_array( $res[$k])) {
                        $res[$k] = self::mergeAssocsR( $res[$k], $v );
                    }
                    else{
                        $res[$k] = $v;
                    }
                }
            }
            return $res;
        }
        static function mergeObjs() {
            $args = func_get_args();
            $res = array_shift( $args );
            while( !empty( $args )) {
                $next = array_shift( $args );
                foreach( $next as $k => $v ) {
                    $res->$k = $v;
                }
            }
            return $res;
        }
        static function mergeObjsR() {
            $args = func_get_args();
            $res = array_shift( $args );
            while( !empty( $args )) {
                $next = array_shift( $args );
                foreach( $next as $k => $v ) {
                    if( is_object( $v ) and isset( $res->$k ) and is_object( $res->$k )) {
                        mergeObjsR( $res->$k, $v );
                    }
                    else{
                        $res->$k = $v;
                    }
                }
            }
            return $res;
        }
        static function findSingle( $array, $field, $value ) {
            foreach( $array as &$element ) {
                $_value = is_object( $element ) ? $element->$field : $element[ $field ];
                if( $_value == $value ) return $element;
            }
        }
        static function explodeTimeDiff( $seconds ) {
            $minets = floor( $seconds / 60 );
            $hours = floor( $minets / 60 );
            $days = floor( $hours / 24 );
            $months = floor( $days / 30 );
            $years = floor( $months / 12 );
            return Array( $seconds, $minets, $hours, $days, $months, $years );
        }
        static function getFreeFileName( $path, $ex = '', $length = 8 ) {
            if( strlen( $ex )) $ex = ".{$ex}";
            do{
                $name = substr( md5( rand() ), 0, $length ).$ex;
                $isset = is_file( "{$path}/{$name}" ) || is_dir( "{$path}/{$name}" );
            }
            while( $isset );
            return $name;
        }
        static function nl2br( $text, $holdNL = false ) {
            $br = CHtml::tag( 'br' );
            if( $holdNL ) $br .= '$0';
            $text = preg_replace( "#\r?\n#", $br, $text );
            return $text;
        }
        static function cleanDir( $path, $ttl ) {
            $now = time();
            $files = glob( "{$path}/*" );
            if( $files ) {
                foreach( $files as $file ) {
                    if( $now - filemtime( $file ) > $ttl ) {
                        @unlink( $file );
                    }
                }
            }
        }
        static function parseMText( $text ) {
            if( !strlen( $text )) return '';
            $text = preg_split( "#\r?\n#", $text );
            $text = array_map( 'trim', $text );
            $text = array_filter( $text, 'strlen' );
            $text = $text ? "<p>".implode( "</p><p>", $text )."</p>" : '';
            return $text;
        }
        static function randKey( $length = 32, $onlyNumbers = false ) {
            $key = "";
            while( strlen( $key ) < $length ) {
                $partKey = $onlyNumbers ? rand() : md5( rand() );
                $key .= substr( $partKey, 0, $length - strlen( $key ));
            }
            return $key;
        }
        static function getExtension( $pathFile ) {
            $path_parts = pathinfo( $pathFile );
            return @$path_parts['extension'];
        }
        static function minimax( $value, $min, $max ) {
            $value = min( $value, $max );
            $value = max( $value, $min );
            return $value;
        }
        static function shorten( $str, $len ) {
            return mb_strlen( $str ) > $len ? mb_substr( $str, 0, $len - 3 ).'...' : $str;
        }
        static function getUrl( $url ) {
            if( strlen( $url )) {
                if( !preg_match( "#^(http:|https:|ftp:)?//#i", $url ) and !preg_match( "#^/#", $url )) {
                    $url = "http://{$url}";
                }
            }
            return $url;
        }
        static function getMimeType( $path ) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE); 
            $type = finfo_file( $finfo, $path );
            finfo_close($finfo);
            return $type;
        }
        static function createDir( $path ) {
            is_dir( $path ) or mkdir( $path );
        }
        static function addSQL( &$sql, $part, $key ) {
            if( !isset( $sql )) $sql = "";
            $sql = (strlen( $sql ) ? " {$key} " : "").$part;
        }
        static function filterSearch( $key ) {
            if( substr_count( $key, "*" )) {
                $key = str_replace( "*", "%", $key );
            }
            elseif( substr_count( $key, '"' ) ){
                $key = str_replace( '"', "", $key );
            }
            else{
                $key = "%{$key}%";
            }
            return $key;
        }
        static function isEmptyObj( $obj ) {
            foreach( $obj as $key=>$value ) {
                if( strlen( $value )) return false;
            }
            return true;
        }
        static function trimR( $var ) {
            if( is_object( $var )) {
                foreach( $var as $key=>$value ) {
                    $var->$key = self::trimR( $value );
                }
                return $var;
            }
            elseif( is_array( $var )) {
                foreach( $var as $key=>$value ) {
                    $var[$key] = self::trimR( $value );
                }
                return $var;
            }
            else{
                return trim( $var );
            }
        }
        static function explode( $str, $delim = null, $limit = null ) {
            if( !strlen( $str ))
                return Array();
            
            if( !$delim )
                $delim = ',';
            
            $array = $limit ? explode( $delim, $str, $limit ) : explode( $delim, $str );
            $array = array_map( 'trim', $array );
            $array = array_filter( $array );
            return $array;
        }
        static function isAssoc( $array ) {
            return (bool)count( array_filter( array_keys( $array ), 'is_string' ));
        }
        static function makeAssoc( $array ) {
            return array_combine( array_values( $array ), array_values( $array ));
        }
        static function nullIfEmptyR( $var ) {
            if( is_object( $var )) {
                foreach( $var as $key=>$value ) {
                    $var->$key = self::nullIfEmptyR( $value );
                }
                return $var;
            }
            elseif( is_array( $var )) {
                foreach( $var as $key=>$value ) {
                    $var[$key] = self::nullIfEmptyR( $value );
                }
                return $var;
            }
            else{
                return empty( $var ) ? null : $var;
            }
        }
        static function fromCookie( $key ) {
            return @$_COOKIE[ $key ];
        }
        static function toCookie( $key, $value ) {
            $expire = time() + 60*60*24*365;
            setcookie( $key, $value, $expire, '/' );
            $_COOKIE[ $key ] = $value;
        }
        static function addToArray( &$array, $value ) {
            if( !is_array( $array ))
                $array = Array();
            
            for( $i = 1; $i<func_num_args(); $i++ ) {
                $value = func_get_arg( $i );
                if( !in_array( $value, $array ))
                    $array[] = $value;
            }
        }
        static function removeFromArray( &$array, $value ) {
            for( $i = 1; $i<func_num_args(); $i++ ) {
                $value = func_get_arg( $i );
                while( true ) {
                    $index = array_search( $value, $array );
                    if( $index === false )
                        break;
                    array_splice( $array, $index, 1 );
                }
            }
        }
        static function nodeToObj( $node ) {
            $obj = new StdClass();
            
            foreach( $node->attributes as $attribute ) 
                $obj->{$attribute->name} = $attribute->value;
            
            foreach( $node->childNodes as $node ) {
                if( ! $node instanceof DOMElement ) 
                    continue;
                
                if( isset( $obj->{$node->nodeName} )) {
                    if( !is_array( $obj->{$node->nodeName} ))
                        $obj->{$node->nodeName} = Array( $obj->{$node->nodeName} );
                    $obj->{$node->nodeName}[] = self::nodeToObj( $node );
                }
                else    
                    $obj->{$node->nodeName} = self::nodeToObj( $node );
            }
                
            return $obj;
        }
        static function is_utf8($string) { 
            for ($i=0; $i<strlen($string); $i++) { 
                if (ord($string[$i]) < 0x80) continue; 
                elseif ((ord($string[$i]) & 0xE0) == 0xC0) $n=1; 
                elseif ((ord($string[$i]) & 0xF0) == 0xE0) $n=2; 
                elseif ((ord($string[$i]) & 0xF8) == 0xF0) $n=3; 
                elseif ((ord($string[$i]) & 0xFC) == 0xF8) $n=4; 
                elseif ((ord($string[$i]) & 0xFE) == 0xFC) $n=5; 
                else return false; 
                for ($j=0; $j<$n; $j++) { 
                    if ((++$i == strlen($string)) || ((ord($string[$i]) & 0xC0) != 0x80)) 
                        return false; 
                } 
           } 
           return true; 
        } 
        static function autoencode($string, $encoding='utf-8') { 
            if (self::is_utf8($string)) {
                $detect='utf-8'; 
            } else  { 
                  $cp1251=0; 
                  $koi8u=0; 
                  $strlen=strlen($string); 
                  for($i=0;$i<$strlen;$i++) 
                  { 
                      $code=ord($string[$i]); 
                      if (($code>223 and $code<256) or ($code==179) or ($code==180) or ($code==186) or ($code==191)) $cp1251++; // а-я, і, ґ, є, Ї 
                      if (($code>191 and $code<224) or ($code==164) or ($code==166) or ($code==167) or ($code==173)) $koi8u++; // а-я, є, і, ї, ґ 
                  } 
                  if ($cp1251>$koi8u) $detect='windows-1251'; 
                  else $detect='koi8-u'; 
            }
            if ($encoding==$detect) {
                return $string; 
            } else {
                return ($detect === 'windows-1251') ? @iconv($detect, $encoding, $string) : $string; 
            }
        }
        static function downloadFileFromWww( $url, $connectTimeout=10, $timeout=120 ) { 
            $header[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; 
            $header[] = "Cache-Control: max-age=0"; 
            $header[] = "Connection: keep-alive"; ; 
            $header[] = "Accept-Language: ru-RU,ru;q=0.5"; 
            $header[] = "Pragma: ";
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch,CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $connectTimeout);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.8.1) Gecko/20061010 Firefox/2.0");
            curl_setopt($ch, CURLOPT_REFERER, "http://google.com/");
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        }
        static function loadWp() {
            if( defined ( 'WPINC' ) ){
                Yii::app()->params['wpLoaded'] = true;
                return true;
            }
            if( Yii::app()->params['wpLoaded'] ) return true;
            Yii::app()->params['wpBeginLoad'] = true;
            require_once Yii::getPathOfAlias('webroot')."/../wp-load.php";
            Yii::app()->params['wpLoaded'] = true;
        }
        static function hex2rgb($hex) {
            $hex = str_replace("#", "", $hex);
            if(strlen($hex) == 3) {
                $r = hexdec(substr($hex,0,1).substr($hex,0,1));
                $g = hexdec(substr($hex,1,1).substr($hex,1,1));
                $b = hexdec(substr($hex,2,1).substr($hex,2,1));
            } else {
                $r = hexdec(substr($hex,0,2));
                $g = hexdec(substr($hex,2,2));
                $b = hexdec(substr($hex,4,2));
            }
            $rgb = array($r, $g, $b);
            return implode(",", $rgb); 
        }
        static function hex2rgbArr($hex) {
            $hex = str_replace("#", "", $hex);
            if(strlen($hex) == 3) {
                $r = hexdec(substr($hex,0,1).substr($hex,0,1));
                $g = hexdec(substr($hex,1,1).substr($hex,1,1));
                $b = hexdec(substr($hex,2,1).substr($hex,2,1));
            } else {
                $r = hexdec(substr($hex,0,2));
                $g = hexdec(substr($hex,2,2));
                $b = hexdec(substr($hex,4,2));
            }
            $rgb = array( 'R' => $r, 'G' => $g, 'B' => $b);
            return $rgb; 
        }
        static function translitStrToRu($str ){
            $translit = array(
                'ch' => 'ч',    'co' => 'ко',    'ay' => 'эй',    'ph' => 'ф',    'sh' => 'ш',
                'ck' => 'к',    'inc' => 'инк',    'th' => 'c',    'sc' => 'c',    'ie' => 'ае',
                'ce' => 'си',    'cial' => 'циал',    'line' => 'лайн',    'city' => 'сити', 'oo' => 'у',
                'tsch' => 'ч',    'hynix' => 'Хайникс',    'Hyundai' => 'Хёндэ',    'Marine' => 'Марин', 'Insurance' => 'Иншурэнс',
                'Corporation' => 'Корпорэйшн',    'Crown' => 'Краун', 
                
                'a' => 'а',    'b' => 'б',    'c' => 'к',
                'd' => 'д',    'e' => 'е',    'f' => 'ф',
                'g' => 'г',    'h' => 'х',    'i' => 'и',
                'j' => 'й',    'k' => 'к',    'l' => 'л',
                'm' => 'м',    'n' => 'н',    'o' => 'о',
                'p' => 'п',    'q' => 'к',    'r' => 'р',
                's' => 'с',    't' => 'т',    'u' => 'у',
                'v' => 'в',    'w' => 'в',    'x' => 'кс',
                'y' => 'и',    'z' => 'з',    
                    
                'yo' => 'ё',    'zh' => 'ж',    'shh' => 'щ',    '\'' => 'ь',     '\'\'' => 'ъ',
                'e\'' => 'э',    'yu' => 'ю',    'ya' => 'я',

                'CH' => 'Ч',    'Ch' => 'Ч',    'CO' => 'КО',    'Co' => 'Ко',    'Ph' => 'Ф',
                'PH' => 'Ф',    'Sh' => 'Ш',    'SH' => 'Ш',    'Ck' => 'К',    'CK' => 'К',
                'Inc' => 'Инк',    'INc' => 'ИНК',    'INC' => 'ИНК',    'Sc' => 'C',    'SC' => 'C',
                'Trust' => 'Траст',    'Air' => 'Эар',    'Fund' => 'Фанд',    'Ge' => 'Дже',    'John' => 'Джон',
                'JP' => 'ДжэйПи',    'United' => 'Юнайтид',    'Apple' => 'Эпл',    'OO' => 'У',    'TSCH' => 'Ч',
                'Black' => 'Блэк',    'CJ' => 'СиДжэй',    'CHINA' => 'Чайна',    'POWER' => 'ПАУЭР',    'NICE' => 'НАЙС', 
                'SOLUTIONS' => 'СОЛЮШНС',    'PACIFIC' => 'ПАСИФИК', 'HYUNDAI' => 'ХЁНДЭ',
                
                'A' => 'А',    'B' => 'Б',    'C' => 'К',
                'D' => 'Д',    'E' => 'Е',    'F' => 'Ф',
                'G' => 'Г',    'H' => 'Х',    'I' => 'И',
                'J' => 'Й',    'K' => 'К',    'L' => 'Л',
                'M' => 'М',    'N' => 'Н',    'O' => 'О',
                'P' => 'П',    'Q' => 'К',    'R' => 'Р',
                'S' => 'С',    'T' => 'Т',    'U' => 'У',
                'V' => 'В',    'W' => 'В',    'X' => 'Кс',
                'Y' => 'И',    'Z' => 'З',

                'YO' => 'Ё',    'Zh' => 'Ж',    'SHH' => 'Щ',    '\'' => 'Ь',    '\'\'' => 'Ъ',
                'E\'' => 'Э',    'YU' => 'Ю',    'YA' => 'Я',
            );
            return strtr($str, $translit);
        }
        static function rus2translit($string) {
            $converter = array(
                'а' => 'a',   'б' => 'b',   'в' => 'v',
                'г' => 'g',   'д' => 'd',   'е' => 'e',
                'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
                'и' => 'i',   'й' => 'y',   'к' => 'k',
                'л' => 'l',   'м' => 'm',   'н' => 'n',
                'о' => 'o',   'п' => 'p',   'р' => 'r',
                'с' => 's',   'т' => 't',   'у' => 'u',
                'ф' => 'f',   'х' => 'h',   'ц' => 'c',
                'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
                'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
                'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
                
                'А' => 'A',   'Б' => 'B',   'В' => 'V',
                'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
                'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
                'И' => 'I',   'Й' => 'Y',   'К' => 'K',
                'Л' => 'L',   'М' => 'M',   'Н' => 'N',
                'О' => 'O',   'П' => 'P',   'Р' => 'R',
                'С' => 'S',   'Т' => 'T',   'У' => 'U',
                'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
                'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
                'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
                'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            );
            return strtr($string, $converter);
        }
        static function generateSlug($string){
            $string = self::rus2translit($string);
            $string = str_replace(' ', '-', trim(strtolower($string)) );
            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
            return preg_replace('/[\-]+/', '-', $string);
        }
        static function sortLanguages( $a, $b ) {
            $defaultLanguageAlias = Yii::App()->language;
            return $a->alias == $defaultLanguageAlias ? -1 : 1;
        }
        static function getLanguages() {
            $languages = LanguageModel::getAll();
            usort( $languages, Array( 'CommonLib', 'sortLanguages' ));
            return $languages;
        }
        static function stemedText( $inpStr, $langConst ){
            setlocale(LC_ALL, "C");
            $inpStr = mb_strtolower( trim($inpStr) );
            $inpStr = preg_replace("/[^a-zA-ZА-Яа-я0-9-]/ui"," ",$inpStr);
            $inpStr = preg_replace('/\s+/ui', ' ', $inpStr);            
            if( !$inpStr ) return '';
            $wordsArr = explode(' ', $inpStr);        
            $langConst = constant( $langConst );
            foreach( $wordsArr as $index => &$word ){
                if( mb_strlen($word) > 2 ){
                    $word = stem($word, $langConst);
                }else{
                    unset( $wordsArr[$index] );
                }
            }
            setlocale(LC_ALL, NULL);
            return implode(' ', $wordsArr);
        }
        static function trimWords( $text, $num_words = 55, $more = '' ){
            $text = trim($text);
            $words_array = preg_split( "/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY );
            $sep = ' ';
            if ( count( $words_array ) > $num_words ) {
                array_pop( $words_array );
                $text = implode( $sep, $words_array );
                $text = $text . $more;
            } else {
                $text = implode( $sep, $words_array );
            }
            return $text;
        }
        static function replaceDates( $str, $dateStr ){
            if( strpos( $str, '{$date(' ) === false ) return $str;
            $forEval = preg_replace('/\{\$date\(([^\)]+)\)\}/i', '" .Yii::app()->dateFormatter->format( "${1}", strtotime("'.$dateStr.'") ) . "', $str);
            $forEval = '$outStr = "' . $forEval . '";';
            eval( $forEval );
            return $outStr;
        }
        static function getDecimalPlaces( $value, $digits = 2 ){
            $value = abs($value);
            if( $value > 0.1 ) return 2 + $digits - 2;
            if( $value > 0.01 ) return 3 + $digits - 2;
            if( $value > 0.001 ) return 4 + $digits - 2;
            if( $value > 0.0001 ) return 5 + $digits - 2;
            if( $value > 0.00001 ) return 6 + $digits - 2;
            if( $value > 0.000001 ) return 7 + $digits - 2;
            if( $value > 0.0000001 ) return 8 + $digits - 2;
            if( $value > 0.00000001 ) return 9 + $digits - 2;
            if( $value > 0.000000001 ) return 10 + $digits - 2;
            if( $value > 0.0000000001 ) return 11 + $digits - 2;
            if( $value > 0.00000000001 ) return 12 + $digits - 2;
            if( $value > 0.000000000001 ) return 13 + $digits - 2;
            if( $value > 0.0000000000001 ) return 14 + $digits - 2;
            if( $value > 0.00000000000001 ) return 15 + $digits - 2;
            if( $value > 0.000000000000001 ) return 16 + $digits - 2;
            if( $value > 0.0000000000000001 ) return 17 + $digits - 2;
            if( $value > 0.00000000000000001 ) return 17 + $digits - 2;
            if( $value > 0.000000000000000001 ) return 19 + $digits - 2;
            if( $value > 0.0000000000000000001 ) return 20 + $digits - 2;
            if( $value > 0.00000000000000000001 ) return 21 + $digits - 2;
        }
        public static function roundQuotesData($value)
        {
            return number_format($value, self::getDecimalPlaces($value), '.', '');
        }
        static function getTimeZonesWithOffsets(){
            $zonesArray = array('Pacific/Midway', 'Pacific/Honolulu', 'Pacific/Marquesas', 'America/Nome', 'America/Los_Angeles', 'America/Denver', 'America/Chicago', 'America/Detroit', 'America/Barbados', 'America/St_Johns', 'America/Santiago', 'America/Sao_Paulo', 'Atlantic/Cape_Verde', 'UTC', 'Europe/London', 'Europe/Amsterdam', 'Europe/Helsinki', 'Europe/Moscow', 'Asia/Tehran', 'Asia/Dubai', 'Asia/Kabul', 'Asia/Yekaterinburg', 'Asia/Colombo', 'Asia/Kathmandu', 'Asia/Dhaka', 'Asia/Rangoon', 'Asia/Bangkok', 'Asia/Brunei', 'Asia/Pyongyang', 'Australia/Eucla', 'Asia/Seoul', 'Australia/Darwin', 'Asia/Vladivostok', 'Australia/Adelaide', 'Australia/Sydney', 'Asia/Kamchatka', 'Pacific/Auckland', 'Pacific/Chatham', 'Pacific/Apia');
            
            $zonesWithOffset = array();
            foreach( $zonesArray as $zone ){
                $now = new DateTime(null, new DateTimeZone( $zone ));
                $zoneOffset = $now->getOffset() / 60 / 60;
                $zonesWithOffset[ ''.$zoneOffset.'' ] = $zone;
            }
            return $zonesWithOffset;
            
            return array(
                '-11' => 'Pacific/Midway',
                '-10' => 'Pacific/Honolulu',
                '-9.5' => 'Pacific/Marquesas',
                '-9' => 'America/Nome',
                '-8' => 'America/Los_Angeles',
                '-7' => 'America/Denver',
                '-6' => 'America/Chicago',
                '-5' => 'America/Detroit',
                '-4' => 'America/Barbados',
                '-3.5' => 'America/St_Johns',
                '-3' => 'America/Santiago',
                '-2' => 'America/Sao_Paulo',
                '-1' => 'Atlantic/Cape_Verde',
                '0' => 'Europe/London',
                '1' => 'Europe/Amsterdam',
                '2' => 'Europe/Helsinki',
                '3' => 'Europe/Moscow',
                '3.5' => 'Asia/Tehran',
                '4' => 'Asia/Dubai',
                '4.5' => 'Asia/Kabul',
                '5' => 'Asia/Yekaterinburg',
                '5.5' => 'Asia/Colombo',
                '5.75' => 'Asia/Kathmandu',
                '6' => 'Asia/Dhaka',
                '6.5' => 'Asia/Yangon',
                '7' => 'Asia/Bangkok',
                '8' => 'Asia/Brunei',
                '8.5' => 'Asia/Pyongyang',
                '8.75' => 'Australia/Eucla',
                '9' => 'Asia/Seoul',
                '9.5' => 'Australia/Darwin',
                '10' => 'Asia/Vladivostok',
                '10.5' => 'Australia/Adelaide',
                '11' => 'Australia/Sydney',
                '12' => 'Asia/Kamchatka',
                '13' => 'Pacific/Auckland',
                '13.75' => 'Pacific/Chatham',
                '14' => 'Pacific/Apia',
            );
        }
        static function removeProtocol( $src ){
            if( strpos('http', $src) == 0 ){
                $src = str_replace( 'http://', '//', $src);
                $src = str_replace( 'https://', '//', $src);
            }
            return $src;
        }
        static function httpsProtocol( $src ){
            if( strpos($src, 'https') === false ) return str_replace( 'http://', 'https://', $src);
            return $src;
        }
        static function sslRequestMarker(){
            if ( isset( $_SERVER['HTTPS'] ) ) {
                if ( 'on' == strtolower( $_SERVER['HTTPS'] ) ) {
                    return ".HTTPS";
                }
                if ( '1' == $_SERVER['HTTPS'] ) {
                    return ".HTTPS";
                }
            }
            return '';
        }
        static function getOnlyTranslatedTextByGetParam($NSi18n, $key, $param){
            if( isset( $_GET[$param] ) ) return Yii::t($NSi18n, $key);
            if(Yii::t($NSi18n, $key) == $key) return false;
            return Yii::t($NSi18n, $key);
        }
        static function getInfoTextForForumInformer($NSi18n, $param){
            $info = '';
            $forumInformerTitle = CommonLib::getOnlyTranslatedTextByGetParam($NSi18n, 'forumInformer'.$param.'Title', 'showAllTexts');
            $forumInformerTooltip = CommonLib::getOnlyTranslatedTextByGetParam($NSi18n, 'forumInformer'.$param.'Tooltip', 'showAllTexts');
            $forumInformerTooltip2 = CommonLib::getOnlyTranslatedTextByGetParam($NSi18n, 'forumInformer'.$param.'Tooltip2', 'showAllTexts');
            if( $forumInformerTitle || $forumInformerTooltip || $forumInformerTooltip2 ){
                $info = '<span class="info-icon"></span><div class="info-box">';
                if( $forumInformerTitle ) $info .= "<h3>$forumInformerTitle</h3>";
                if( $forumInformerTooltip ) $info .= "<p>$forumInformerTooltip</p>";
                if( $forumInformerTooltip2 ) $info .= "<p class='bold-text'>$forumInformerTooltip2</p>";
                $info .= '</div>';
            }
            return $info;
        }
        static function getCurrentLangKey(){
            foreach( Yii::app()->params['langs'] as $langKey => $langData ){
                if( $langData['langAlias'] == Yii::app()->language ) return $langKey;
            }
            return false;
        }
        static function formatThousands($number)
        {
            if ($number < 1000) {
                return $number;
            }
            $dotPos = strpos($number, '.');
            if (!$dotPos) {
                return number_format($number, 0, '.', ' ');
            }

            $ints = substr($number, 0, $dotPos);
            $decimals = substr($number, $dotPos+1);

            return number_format($ints, 0, '.', ' ') . '.' . $decimals;
        }

        public static function getWpTemplateBlock($cacheKey, $template)
        {
            $cacheKey = $cacheKey . Yii::app()->language;
            if ($content = Yii::app()->cache->get($cacheKey)) {
                return $content;
            } else {
                ob_start();
                self::loadWp();
                require_once Yii::App()->params['wpThemePath'].'/' . $template;
                $blockContent = ob_get_clean();
                Yii::app()->cache->set($cacheKey, $blockContent, 60*60);
                return $blockContent;
            }
        }
    }

?>