<?

	class ImgResizeLib{
		static function resize( $pathSrc, $pathDest, $newWidth, $newHeight ) {
			if(!is_file($pathSrc))
				return false;
			$nameImage = basename( $pathSrc );
			$exImage = preg_match( "#\.([^\.]+)$#", $nameImage, $match ) ? $match[1] : "";
			$exImage = strtolower( $exImage );
			
			$sizes = getimagesize( $pathSrc );
			
			switch( $exImage ) {
				case "jpg":
				case "jpeg": {
					$imageOld = @imagecreatefromjpeg( $pathSrc );
					if( !$imageOld ) throw new Exception( "imagecreatefromjpeg error!" );
					break;
				}
				case "png":{
					$imageOld = @imagecreatefrompng( $pathSrc );
					if( !$imageOld ) throw new Exception( "imagecreatefrompng error!" );
					break;
				}
				case "gif":{
					$imageOld = @imagecreatefromgif( $pathSrc );
					if( !$imageOld ) throw new Exception( "imagecreatefromgif error!" );
					break;
				}
				default:{ return false; break; }
			}
			
			if( $exImage and $sizes and $imageOld ) {
				list( $oldWidth, $oldHeight ) = $sizes;
				
				if( $newWidth != "auto" and $newHeight != "auto" ) {
					$widthScale = $newWidth / $oldWidth;
					$heightScale = $newHeight / $oldHeight;
					$scale = max( $widthScale, $heightScale );
					
					$transWidth = $newWidth / $scale;
					$transHeight = $newHeight / $scale;
					
					$dst_x = 0;
					$dst_y = 0;
					$src_x = $oldWidth / 2 - $transWidth / 2;
					$src_y = $oldHeight / 2 - $transHeight / 2;
					$dst_w = $newWidth;
					$dst_h = $newHeight;
					$src_w = $transWidth;
					$src_h = $transHeight;
				}
				else{
					if( $newHeight == "auto" ) {
						$scale = $newWidth / $oldWidth;
						$newHeight = $oldHeight * $scale;
					}
					else{
						$scale = $newHeight / $oldHeight;
						$newWidth = $oldWidth * $scale;
					}
					
					$dst_x = 0;
					$dst_y = 0;
					$src_x = 0;
					$src_y = 0;
					$dst_w = $newWidth;
					$dst_h = $newHeight;
					$src_w = $oldWidth;
					$src_h = $oldHeight;
				}
				
				$imageNew = imagecreatetruecolor( $newWidth, $newHeight );
				if( $exImage == "png" ) {
					imagealphablending( $imageNew, false );
					imagesavealpha( $imageNew, true );
					
					$white = imagecolorallocatealpha($imageNew, 255, 255, 255, 127);
					imagefilledrectangle($imageNew, 0, 0, $newWidth, $newHeight, $white );
				}
				else{
					$white = imagecolorallocate($imageNew, 255, 255, 255);
					imagefilledrectangle($imageNew, 0, 0, $newWidth, $newHeight, $white );
				}
				
				imagecopyresampled( $imageNew, $imageOld, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h );
				
				switch( $exImage ) {
					case "jpg":
					case "jpeg": {
						imagejpeg( $imageNew, $pathDest );
						break;
					}
					case "png":{
						imagepng( $imageNew, $pathDest );
						break;
					}
					case "gif":{
						imagegif( $imageNew, $pathDest );
						break;
					}
				}
			}
		}
	}

?>