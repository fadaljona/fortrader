<?php

class ManageTextContentLib{
	
	static function getSettings(){
		return array(
			'calendarSingle' => array(
				'type' => 'single',
				'model' => 'CalendarEvent2Model',
				'modelForm' => 'AdminCalendarEventTextContentFormModel',
				'label' => Yii::t('*', 'Calendar events'),
			),
			'calendarSettings' => array(
				'type' => 'settings',
				'model' => 'CalendarEventSettingsModel',
				'modelForm' => 'AdminCalendarEventSettingsTextContentFormModel',
				'label' => Yii::t('*', 'Calendar settings'),
			),
			'quotesSimbolSingle' => array(
				'type' => 'single',
				'model' => 'QuotesSymbolsModel',
				'modelForm' => 'AdminQuotesSymbolsTextContentFormModel',
				'label' => Yii::t('*', 'Quotes Symbols'),
			),
			'quotesCategory' => array(
				'type' => 'single',
				'model' => 'QuotesCategoryModel',
				'modelForm' => 'AdminQuotesCategoryTextContentFormModel',
				'label' => Yii::t('*', 'Quotes categories'),
			),
		);
	}
	static function getSigleRoute(){
		return 'manageTextContent/single';
	}
	static function getCurrentSlug(){
		$settings = self::getSettings();
		if( !Yii::app()->request->getParam('slug') || !array_key_exists(Yii::app()->request->getParam('slug'), $settings) ) return false;
		return Yii::app()->request->getParam('slug');
	}
	static function getSingleUrl(){
		$slug = self::getCurrentSlug();
		if( !$slug ) return false;
		return Yii::app()->createUrl( 'admin/' . self::getSigleRoute(), array( 'slug' => $slug ));
	}
	static function getTypesTopMenuItems(){
		$settings = self::getSettings();
		$returnArr = array(array(
			'label' => Yii::t('*', 'Home'),
			'url' => array('manageTextContent/index'),
			'itemOptions' => array('class' => !Yii::app()->request->getParam('slug') ? 'active' : '')
		));
		foreach( $settings as $slug => $params ){
			$returnArr[] = array(
				'label' => $params['label'],
				'url' => array(self::getSigleRoute(), 'slug' => $slug),
				'itemOptions' => array('class' => Yii::app()->request->getParam('slug') == $slug ? 'active' : '')
			);
		}
		return $returnArr;
	}
	static function getType(){
		$settings = self::getSettings();

		if( !Yii::app()->request->getParam('slug') || !array_key_exists(Yii::app()->request->getParam('slug'), $settings) ) return false;
		
		$itemSettings = $settings[Yii::app()->request->getParam('slug')];
		
		if( $itemSettings['type'] == 'single' || $itemSettings['type'] == 'settings' ) return $itemSettings['type'];
		return false;
	}
	static function getItemSettings(){
		$settings = self::getSettings();
		if( !Yii::app()->request->getParam('slug') || !array_key_exists(Yii::app()->request->getParam('slug'), $settings) ) return false;
		return $settings[Yii::app()->request->getParam('slug')];
	}
}