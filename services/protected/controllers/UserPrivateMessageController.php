<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class UserPrivateMessageController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Private messages";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'deny', 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
	}

?>