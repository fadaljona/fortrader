<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class UserNoticeController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Notices";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'ajaxViewAll' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
	}

?>