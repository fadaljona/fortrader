<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SourcesAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Fortrader News Aggregator Sources';
		public $metaDesc;
		public $metaKeys;
		public $settings;
		
		private function setBreadcrumbs() {
	
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'News Aggregator',
					'url' => Array( '/newsAggregator/index' ),
				),
				(object)Array( 

					'label' => 'Sources',
				)
			);
		}
		private function setMeta() {
			$this->settings = NewsAggregatorSettingsModel::getModel();
			
			$metaTitle = $this->settings->sourceMetaTitle;

			$this->metaDesc = $this->settings->sourceMetaDesc;
			$this->metaKeys = $this->settings->sourceMetaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->sourceTitle;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->ogSourceImage;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
		}
		function run() {
			$this->setMeta();
			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->setLayoutBodyClass('filterPosition');
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings
			));
		}
	}

?>