<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SourceLinkAction extends ActionFrontBase {

		function run( $id ) {
			$model = NewsAggregatorSourceModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Source!" );
			$this->redirect( $model->url );
		}
	}

?>