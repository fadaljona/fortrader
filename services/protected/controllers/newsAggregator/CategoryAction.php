<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class CategoryAction extends ActionFrontBase {
		private $model;

		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs() {
	
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'News Aggregator',
					'url' => Array( '/newsAggregator/index' ),
				),
				(object)Array( 

					'label' => $this->model->title,
				)
			);
		}
		private function getModel( $slug ) {
			$model = NewsAggregatorCatsModel::findBySlug( $slug );
			if( !$model || !$model->enabled ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta() {
			$metaTitle = $this->model->metaTitle;

			$this->metaDesc = $this->model->metaDesc;
			$this->metaKeys = $this->model->metaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = $this->model->title;
			}
			
			$this->title = $this->model->pageTitle;
			if( !$this->title ) $this->title = $this->model->title;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $this->model->ogImage );
		}

		function run( $slug ) {
		
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $slug );	
			
			$this->setMeta();
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			$this->setLayoutBodyClass('filterPosition');

						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model,
			));
		}
	}

?>