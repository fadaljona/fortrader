<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class IndexAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Fortrader News Aggregator';
		public $metaDesc;
		public $metaKeys;
		public $settings;
		
		private $cat = 'indexNewsAggregator';
		private $paramStr = 'news_list_page';
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'News Aggregator',
				)
			);
		}
		private function setMeta() {
			$this->settings = NewsAggregatorSettingsModel::getModel();
			
			$metaTitle = $this->settings->newsMetaTitle;

			$this->metaDesc = $this->settings->newsMetaDesc;
			$this->metaKeys = $this->settings->newsMetaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->newsTitle;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->ogNewsImage;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
		}
		function run() {
			$this->setMeta();
			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->setLayoutBodyClass('filterPosition');
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
				'paramStr' => $this->paramStr,
				'cat' => $this->cat,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>