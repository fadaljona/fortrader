<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {

		function run( $slug ) {
			$model = NewsAggregatorNewsModel::findBySlug( $slug );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			$this->redirect( $model->link );
		}
	}

?>