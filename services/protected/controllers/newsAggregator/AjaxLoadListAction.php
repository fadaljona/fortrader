<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadListAction extends ActionFrontBase {
		private function getDp( $lastTime, $currentCat ){
			$DP = NewsAggregatorNewsModel::getListDp( $lastTime, $currentCat );
			if( !$DP->getTotalItemCount( )) $this->throwI18NException( "Can't find model!" );
			return $DP;
		}
		private function getWidget( $DP ) {
			$widget = $this->controller->createWidget( 'widgets.listViews.RssListViewWidget', Array(
				'dataProvider' => $DP,
				'template' => '{items}',
				'itemsTagName' => 'ul',
				'itemsCssClass' => 'news_big_list_box',
			));
			return $widget;
		}
		private function renderRows( $widget ) {
			ob_start();
			$widget->renderRows();
			return ob_get_clean();
		}
		function run($lastTime, $currentCat) {
			$data = Array();
			try{
				$DP = $this->getDP( $lastTime, $currentCat );
				$widget = $this->getWidget( $DP );
				
				$data[ 'list' ] = $this->renderRows( $widget );
				$data['pagesCount'] = $DP->pagination->getPageCount();

			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>