<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class JournalController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Forex Magazine for Traders";
		}
		function actionIndex() {
			$this->redirect( Array( 'last' ));
		}
		function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'ajaxAddMark', 'ajaxSubscribe', 'ajaxUnsubscribe' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl',
				/*array(
					'PageCacheFilter + single',
					'duration'=> 60*60 * 24 * 30,
					'varyByParam' => 'id',
				),
				array(
					'PageCacheFilter + last',
					'duration'=> 60*60 * 24 ,
					'varyByParam' => 'journalsPage',
				),*/
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$params = array();		
				parse_str( Yii::app()->request->getQueryString(), $params );
				
				$action = '';
				if( Yii::app()->controller->action->id ){
					$action = '/' . Yii::app()->controller->action->id;
				}
				
				$url = Yii::App()->createURL( 'journal' . $action , $params);
				$url = str_replace( '/services', '', $url );
				
				$this->redirect( $url, true, 301 );
			}
			$filterChain->run();			
		}
		public function singleSiteMapArgs(){
			$models = JournalModel::model()->findAll(array(
				'select' => ' `slug` ',
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = array(
					'slug' => $model->slug
				);
			}
			return $outArr;
		}
	}

?>