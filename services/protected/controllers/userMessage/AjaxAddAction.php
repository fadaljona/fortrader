<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.UserMessageFormModel' );
	
	final class AjaxAddAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new UserMessageFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Message!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				if( Yii::App()->user->isRoot()) $this->throwI18NException( "Root can't create messages!" );
				if( Yii::App()->user->checkAccess( 'userMessageBan' )) $this->throwI18NException( "You have negative reputation! Comments are not allowed!" );
				if( Yii::App()->user->getModel()->countReputation < 0 ) $this->throwI18NException( "low reputation for this actions" );
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( !$formModel->getAR()->checkAccess() ) $this->throwI18NException( "Access denied!" );
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>