<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadItemAction extends ActionFrontBase {
		private $message;
		private $widget;
		private function getMessage( $id ) {
			$model = UserMessageModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Message!" );
			return $model;
		}
		private function getWidget() {
			$widget = $this->controller->createWidget( 'widgets.UsersConversationWidget' );
			return $widget;
		}
		private function renderMessage() {
			ob_start();
			$this->widget->renderMessage( $this->message );
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			return $content;
		}
		function run( $id ) {
			$data = Array();
			try{
				$this->message = $this->getMessage( $id );
				$this->widget = $this->getWidget();
				
				$data[ 'item' ] = $this->renderMessage();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>