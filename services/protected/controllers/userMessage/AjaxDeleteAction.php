<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class AjaxDeleteAction extends ActionFrontBase {
		private function getModel( $id ) {
			$model = UserMessageModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Message!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				if( !$model->checkAccess() ) $this->throwI18NException( "Access denied!" );
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>