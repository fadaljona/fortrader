<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.ConversationWatchModel' );
	
	final class AjaxLoadListAction extends ActionFrontBase {
		private $widget;
		private function getWidget( $instance, $idLinkedObj, $idLastMessage, $idFirstMessage ) {
			$widget = $this->controller->createWidget( 'widgets.UsersConversationWidget', Array(
				'instance' => $instance,
				'idLinkedObj' => $idLinkedObj,
				'idLastMessage' => $idLastMessage,
				'idFirstMessage' => $idFirstMessage
			));
			return $widget;
		}
		private function renderMessages() {
			ob_start();
			$this->widget->renderMessages();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$instance = @$_GET['instance'];
				$idLinkedObj = (int)@$_GET['idLinkedObj'];
				$idLastMessage = (int)@$_GET['idLastMessage'];
				$idFirstMessage = (int)@$_GET['idFirstMessage'];
				$this->widget = $this->getWidget( $instance, $idLinkedObj, $idLastMessage, $idFirstMessage );
				
				$data[ 'list' ] = $this->renderMessages();
				$data[ 'more' ] = $this->widget->issetMoreMessages();
				
				$watchModel = ConversationWatchModel::instance((int)@$_GET['idLinkedObj']);
			
				if( $watchModel )
					$watchModel->save();
				
				ob_start();
				$conversationWatchWidget = $this->controller->createWidget( 'widgets.ConversationWatchWidget', Array(
					'idObj' => (int)@$_GET['idLinkedObj'],
				));
				$conversationWatchWidget->run();
				$conversationWatchWidgetContent = ob_get_clean();
				$data[ 'usersOnline' ] = $conversationWatchWidgetContent;
				
			
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			
			
			
			
			
			echo json_encode( $data );
		}
	}

?>