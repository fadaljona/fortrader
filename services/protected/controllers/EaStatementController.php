<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class EaStatementController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "EA Statements";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'add', 'edit', 'iframeAdd', 'ajaxDelete' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
	}

?>