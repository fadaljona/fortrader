<?
	Yii::import( 'controllers.base.ActionFrontBase' );
		
	final class AjaxAddAction extends ActionFrontBase {
		private function thank( $instance, $idObject ) {
			$idUser = Yii::App()->user->id;
			$model = ThankModel::model()->findByAttributes( Array( 
				'instance' => $instance,
				'idObject' => $idObject,
				'idUser' => $idUser,
			));
			if( $model ) return;
			
			$model = ThankModel::instance( $instance, $idObject, $idUser );
			$model->save();
		}
		private function getThanks( $instance, $idObject ) {
			$out = Array();
			$models = ThankModel::model()->findAll( Array( 
				'with' => Array( 'user' ),
				'condition' => "
					`t`.`instance` = :instance
					AND `t`.`idObject` = :idObject
				",
				'order' => " `t`.`createdDT` ",
				'params' => Array(
					':instance' => $instance,
					':idObject' => $idObject,
				),
			));
			foreach( $models as $model ) {
				$date = date( "d.m.Y", strtotime( $model->createdDT ));
				$out[] = CHtml::link( CHtml::encode( $model->user->showName ), $model->user->getProfileURL())." ({$date})";
			}
			return implode( ", ", $out );
		}
		function run( $instance, $idObject ) {
			$data = Array();
			try{
				if( Yii::App()->user->isRoot()) $this->throwI18NException( "Root can't create thanks!" );
							
				$this->thank( $instance, $idObject );
				$data[ 'count' ] = ThankModel::getCount( $instance, $idObject );
				$data[ 'thanks' ] = $this->getThanks( $instance, $idObject );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>