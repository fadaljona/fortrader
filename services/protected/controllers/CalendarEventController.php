<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class CalendarEventController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Economic calendar";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				//Array( 'deny', 'actions' => Array( 'registerMember' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
		public function filters(){
			return array(
				/*array(
					'PageCacheFilter + list',
					'duration'=> 60*2,
					'varyByParam' => 'period',
				),*/
				'servicesRedirect',
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){
				$params = array();		
				parse_str( Yii::app()->request->getQueryString(), $params );
				
				$action = '';
				if( Yii::app()->controller->action->id ){
					$action = '/' . Yii::app()->controller->action->id;
				}
				if( $action == '/index' ) $action = '/list';
				
				if( $action == '/single' ){
					if( isset( $params['id'] ) && $params['id'] ){
						$params['slug'] = $params['id'];
						unset($params['id']);
					}
				}
				
				$url = Yii::App()->createURL( 'calendarEvent' . $action , $params);
				$url = str_replace( '/services', '', $url );
				
				$this->redirect( $url, true, 301 );
			}
			$filterChain->run();			
		}
		public function singleSiteMapArgs(){
			$outArr = array( );
			$sql = "
				SELECT `slug` FROM `{{calendar_event2}}` `event`
				LEFT JOIN `{{calendar_event2_i18n}}` `i18n` ON `event`.`indicator_id` = `i18n`.`indicator_id` AND `i18n`.`idLanguage` = 1
				WHERE `i18n`.`indicator_name` <> '' AND `i18n`.`indicator_name` IS NOT NULL
			";
			$results = Yii::App()->db->createCommand( $sql )->queryAll();
			
			foreach( $results as $result ){
				$outArr[] = array(
					'slug' => $result['slug'],
				);
			}
			return $outArr;
		}
	}

?>