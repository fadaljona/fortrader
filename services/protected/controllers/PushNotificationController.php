<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class PushNotificationController extends ControllerFrontBase {


        public  function  actionApiDeviceToken(){
            if(!Yii::app()->request->isPostRequest){
                 throw new CHttpException(405);
            }

            $token_src = filter_var(Yii::app()->request->getPost('token'), FILTER_SANITIZE_STRING);

            if(empty($token_src)){
             throw new CHttpException(400, "Required parameter 'token' is missing");
            }

            $token = ApnsDevices::model()->find('token=:token', array(':token' => $token_src));

            if(!isset($token)){
             $token = new ApnsDevices();
             $token->token = $token_src;
            }
            $token->save();

            header('HTTP/1.1 201 ');
            Yii::app()->end();
        }
	}
?>