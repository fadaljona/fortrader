<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class CurrencyRatesController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Currency Rates";
		}
		function accessRules(){
			return Array(
				/*array(
					'deny',
					'actions' => array('archive', 'archiveSingle'),
					'expression' => array('CurrencyRatesController','allowOnlyAdmin'),
					'users'=>array('*')
				),*/
			);
		}
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl'
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$url = str_replace( '/services', '', Yii::App()->request->getUrl() );
				
				$this->redirect( strtolower($url), true, 301 );
			}
			$filterChain->run();			
		}
		public static function allowOnlyAdmin(){
			if( Yii::App()->user->checkAccess( 'currencyRatesControl' ) ) return false;
			return true;
		}
		public function singleSiteMapArgs(){
			$outArr = array( );
			$sql = "
				SELECT `code` FROM `{{currency_rates}}`
				WHERE `noData` = 0
			";
			$results = Yii::App()->db->createCommand( $sql )->queryAll();
			
			foreach( $results as $result ){
				$outArr[] = array(
					'slug' => strtolower( $result['code'] ),
				);
			}
			$sql = "
				SELECT `code` FROM `{{currency_rates}}`
				WHERE `ecb` = 1
			";
			$results = Yii::App()->db->createCommand( $sql )->queryAll();

			foreach( $results as $result ){
				$outArr[] = array(
					'slug' => 'eur' . strtolower( $result['code'] ),
					'type' => 'ecb'
				);
			}
			return $outArr;
		}
		public function archiveSingleSiteMapArgs(){
			$outArr = array( );
			
			$sql = "
				SELECT `code` FROM `{{currency_rates}}`
				WHERE `noData` = 0
			";
			$results = Yii::App()->db->createCommand( $sql )->queryAll();

			foreach( $results as $result ){
				
				$slug = strtolower( $result['code'] );
				
				
				$years = CurrencyRatesModel::getYearsAvailable();
				foreach( $years as $year ){
					$outArr[] = array(
						'year' => $year['year'],
						'slug' => $slug,
					);
					$mos = CurrencyRatesModel::getMonthsAvailableByYearForSiteMap( $year['year'] );
					foreach( $mos as $mo ){
						$moStr = $mo < 10 ? '0' . $mo : $mo;
						$outArr[] = array(
							'year' => $year['year'],
							'mo' => $moStr,
							'slug' => $slug,
						);
						$lastDay = CurrencyRatesModel::getLastDayAvailableByYearMo( $year['year'], $mo );
						$firstDay = CurrencyRatesModel::getFirstDayAvailableByYearMo( $year['year'], $mo );
						for( $i = $firstDay; $i <= $lastDay; $i++ ){
							$dayStr = $i < 10 ? '0' . $i : $i;
							$outArr[] = array(
								'year' => $year['year'],
								'mo' => $moStr,
								'day' => $dayStr,
								'slug' => $slug,
							);
						}
					}
				}
			}
			
			
			$sql = "
				SELECT `code` FROM `{{currency_rates}}`
				WHERE `ecb` = 1
			";
			$results = Yii::App()->db->createCommand( $sql )->queryAll();

			foreach( $results as $result ){
	
				$slug = strtolower( $result['code'] );
					
				$years = CurrencyRatesModel::getYearsAvailable('ecb');
				foreach( $years as $year ){
					$outArr[] = array(
						'year' => $year['year'],
						'slug' => 'eur' . $slug,
						'type' => 'ecb',
					);
					$mos = CurrencyRatesModel::getMonthsAvailableByYearForSiteMap( $year['year'], 'ecb' );
					foreach( $mos as $mo ){
						$moStr = $mo < 10 ? '0' . $mo : $mo;
						$outArr[] = array(
							'year' => $year['year'],
							'mo' => $moStr,
							'slug' => 'eur' . $slug,
							'type' => 'ecb',
						);
						$lastDay = CurrencyRatesModel::getLastDayAvailableByYearMo( $year['year'], $mo );
						$firstDay = CurrencyRatesModel::getFirstDayAvailableByYearMo( $year['year'], $mo, 'ecb' );
						for( $i = $firstDay; $i <= $lastDay; $i++ ){
							$dayStr = $i < 10 ? '0' . $i : $i;
							$outArr[] = array(
								'year' => $year['year'],
								'mo' => $moStr,
								'day' => $dayStr,
								'slug' => 'eur' . $slug,
								'type' => 'ecb',
							);
						}
					}
				}
			}
			
			return $outArr;
		}
		
		public function archiveSiteMapArgs(){
			$years = CurrencyRatesModel::getYearsAvailable();
			$outArr = array( array() );
			foreach( $years as $year ){
				$outArr[] = array(
					'year' => $year['year']
				);
				$mos = CurrencyRatesModel::getMonthsAvailableByYearForSiteMap( $year['year'] );
				foreach( $mos as $mo ){
					$moStr = $mo < 10 ? '0' . $mo : $mo;
					$outArr[] = array(
						'year' => $year['year'],
						'mo' => $moStr
					);
					$lastDay = CurrencyRatesModel::getLastDayAvailableByYearMo( $year['year'], $mo );
					$firstDay = CurrencyRatesModel::getFirstDayAvailableByYearMo( $year['year'], $mo );
					for( $i = $firstDay; $i <= $lastDay; $i++ ){
						$dayStr = $i < 10 ? '0' . $i : $i;
						$outArr[] = array(
							'year' => $year['year'],
							'mo' => $moStr,
							'day' => $dayStr
						);
					}
				}
			}
			return $outArr;
		}
		public function ecbArchiveSiteMapArgs(){
			$years = CurrencyRatesModel::getYearsAvailable('ecb');
			$outArr = array( array() );
			foreach( $years as $year ){
				$outArr[] = array(
					'year' => $year['year'],
					'type' => 'ecb',
				);
				$mos = CurrencyRatesModel::getMonthsAvailableByYearForSiteMap( $year['year'], 'ecb' );
				foreach( $mos as $mo ){
					$moStr = $mo < 10 ? '0' . $mo : $mo;
					$outArr[] = array(
						'year' => $year['year'],
						'mo' => $moStr,
						'type' => 'ecb',
					);
					$lastDay = CurrencyRatesModel::getLastDayAvailableByYearMo( $year['year'], $mo );
					$firstDay = CurrencyRatesModel::getFirstDayAvailableByYearMo( $year['year'], $mo, 'ecb' );
					for( $i = $firstDay; $i <= $lastDay; $i++ ){
						$dayStr = $i < 10 ? '0' . $i : $i;
						$outArr[] = array(
							'year' => $year['year'],
							'mo' => $moStr,
							'day' => $dayStr,
							'type' => 'ecb',
						);
					}
				}
			}
			return $outArr;
		}
		public function exchangePageSiteMapArgs(){
			$sql = "
				SELECT `convert`.`value` `value`, `convert`.`type` `type`, `fromRate`.`code` `fromCode`, `toRate`.`code` `toCode` FROM `{{currency_rates_convert}}` `convert` 
				LEFT JOIN `{{currency_rates}}` `fromRate` ON `convert`.`idCurrencyFrom` = `fromRate`.`id`
				LEFT JOIN `{{currency_rates}}` `toRate` ON `convert`.`idCurrencyTo` = `toRate`.`id`
			";
			$results = Yii::App()->db->createCommand( $sql )->queryAll();
			$outArr = array( );
			foreach( $results as $result ){
				if( $result['type'] == 'ecb' ){
					$outArr[] = array(
						'value' => floatval($result['value']), 
						'currencyFrom' => strtolower($result['fromCode']), 
						'currencyTo' => strtolower($result['toCode']),
						'type' => $result['type']
					);
				}else{
					$outArr[] = array(
						'value' => floatval($result['value']), 
						'currencyFrom' => strtolower($result['fromCode']), 
						'currencyTo' => strtolower($result['toCode']),
					);
				}
				
			}
			return $outArr;
		}
	}

?>