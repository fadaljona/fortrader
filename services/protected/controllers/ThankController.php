<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class ThankController extends ControllerFrontBase {
		function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'ajaxAdd' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
	}

?>