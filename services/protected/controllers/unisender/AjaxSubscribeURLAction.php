<?php
Yii::import('controllers.base.ActionFrontBase');
require_once( Yii::app()->params['wpThemePath'] . '/unisender/functions.php');

final class AjaxSubscribeURLAction extends ActionFrontBase
{
    public function run($email, $listId)
    {
        $result = addUserToUnisender($email, $listId);
        $data = array();
        if (!$result) {
            $data[ 'error' ] = Yii::t('*', 'Try other email or contact admin please');
        }
        echo json_encode($data);
    }
}
