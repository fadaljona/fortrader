<?
	Yii::import( 'controllers.base.ControllerFrontBase' );
	Yii::import('components.widgets.quotes.QuotesModel');

	final class QuotesPanelController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "ForTrader.org Quotes Table";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		private function getWidget( $data,$action ) {
			$widget = $this->createWidget( 'widgets.quotes.QuotesWidget', Array(
				'idCat' => (isset($data['idCat'])?$data['idCat']:0),
				'symbols'=> (isset($data['symbols'])?$data['symbols']:array()),
				'ajaxSearchData'=>array(
					'id_quote'=>isset($data['id_quote'])?$data['id_quote']:0,
					'name_quote'=>isset($data['name_quote'])?$data['name_quote']:''
				),
				'action' => $action,
				'ajax' => true
			));
			return $widget;
		}
		
		function actionAjaxGetTable(){
			$idCat=(int)$_POST['idCat'];
			$symbols=is_array($_POST['idsQuotes'])?$_POST['idsQuotes']:array();

			$widget=$this->getWidget(array('idCat'=>$idCat,'symbols'=>$symbols),'getQuotes');

			$widget->run();
		}
		
		function actionAjaxRefreshPanel(){
			if(Yii::app()->user->getState('panel.default')){
				echo json_encode(array("error"=>Yii::t("quotes","Вы уже делали сброс панели")));
			}else{
				$ids=QuotesSymbolsModel::getIdsPanelDefault();
				
				$quotesSettings = QuotesSettingsModel::getInstance();

				$timer=$quotesSettings->time_update_panel;
				
				$db_quotes=QuotesSymbolsModel::getQuotesPanelDefault();
				$quotes=array();
				foreach($db_quotes as $db_quote){
					$quotes[$db_quote->id] = QuotesModel::setQuoteModel($db_quote,'array');
				}
				Yii::app()->user->setState( "panel.ids", $ids);
				Yii::app()->user->setState( "panel.time_update_panel", $timer);
				Yii::app()->user->setState( "panel.default", true);
				
				QuotesPanelModel::saveTimer();
				QuotesPanelModel::saveIds();
				
				echo json_encode(array("ids"=>$ids,"iTimer"=>$timer,"quotes"=>$quotes));
			}
		}
		
		function actionCheckAuth(){
			$id=Yii::app()->user->id;
			return $id?true:false;
		}
		
		function actionAjaxSavePanel(){
			if(!$this->actionCheckAuth())
				echo json_encode(array("error"=>Yii::t("quotesWidget","Нужно авторизоваться")));
			
			$ids=(array)$_POST['ids'];
			$time_update_panel=(int)$_POST['time_update_panel'];
			Yii::app()->user->setState( "panel.default", false);
			
			if(QuotesModel::savePanel($ids,$time_update_panel)){
				echo json_encode(array("msg"=>"Ok"));
			}else{
				echo json_encode(array("error"=>Yii::t("quotesWidget","Ошибка сохранения настроеек панели")));
			}
		}
	}
