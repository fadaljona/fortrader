<?
	Yii::import( 'controllers.base.ControllerFrontBase' );
	Yii::import('components.widgets.quotes.QuotesModel');

	final class QuotesController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "ForTrader.org Quotes Table";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		private function getWidget( $data,$action ) {
			$widget = $this->createWidget( 'widgets.quotes.QuotesWidget', Array(
				'idCat' => (isset($data['idCat'])?$data['idCat']:0),
				'symbols'=> (isset($data['symbols'])?$data['symbols']:array()),
				'ajaxSearchData'=>array(
					'id_quote'=>isset($data['id_quote'])?$data['id_quote']:0,
					'name_quote'=>isset($data['name_quote'])?$data['name_quote']:''
				),
				'action' => $action,
				'ajax' => true
			));
			return $widget;
		}
		public function actionSuggest(){
			if (Yii::app()->request->isAjaxRequest && isset($_GET['term'])) {
				$models = QuotesSymbolsModel::model()->findAll(array(
					'condition'=>'`t`.`desc` LIKE :keyword OR `t`.`name` LIKE :keyword',
					'params'=>array(
						':keyword'=>'%'.$_GET['term'].'%',
					)
				));
				$result = array();
				foreach ($models as $m)
					$result[] = array(
						'label' => $m->desc." (".$m->name.")",
						'id' => $m->id
					);

				echo json_encode($result);
			}
		}
		function actionAjaxGetTable(){
			$idCat=(int)$_POST['idCat'];
			$symbols=is_array($_POST['idsQuotes'])?$_POST['idsQuotes']:array();

			$widget=$this->getWidget(array('idCat'=>$idCat,'symbols'=>$symbols),'getQuotes');

			$widget->run();
		}
		
		function actionAjaxRefreshPanel(){
			if(Yii::app()->user->getState('panel.default')){
				echo json_encode(array("error"=>Yii::t("quotes","Вы уже делали сброс панели")));
			}else{
				$ids=QuotesSymbolsModel::getIdsPanelDefault();
				$quotesSettings = QuotesSettingsModel::getInstance();
				
				$timer=$quotesSettings->time_update_panel;
				$db_quotes=QuotesSymbolsModel::getQuotesPanelDefault();
				$quotes=array();
				foreach($db_quotes as $db_quote){
					$quotes[$db_quote->id] = QuotesModel::setQuoteModel($db_quote,'array');
				}
				Yii::app()->user->setState( "panel.ids", $ids);
				Yii::app()->user->setState( "panel.time_update_panel", $timer);
				Yii::app()->user->setState( "panel.default", true);
				
				QuotesPanelModel::saveTimer();
				QuotesPanelModel::saveIds();
				
				echo json_encode(array("ids"=>$ids,"iTimer"=>$timer,"quotes"=>$quotes));
			}
		}
		
		function actionAjaxGetChecks(){
			$idCat=(int)$_POST['idCat'];
			$widget=$this->getWidget(array('idCat'=>$idCat),'getChecks');
			$widget->run();
		}

		function actionCheckAuth(){
			$id=Yii::app()->user->id;
			return $id?true:false;
		}
		
		function actionAjaxSavePanel(){
			if(!$this->actionCheckAuth())
				echo json_encode(array("error"=>Yii::t("quotesWidget","Нужно авторизоваться")));
			
			$ids=(array)$_POST['ids'];
			$time_update_panel=(int)$_POST['time_update_panel'];
			Yii::app()->user->setState( "panel.default", false);
			
			if(QuotesModel::savePanel($ids,$time_update_panel)){
				echo json_encode(array("msg"=>"Ok"));
			}else{
				echo json_encode(array("error"=>Yii::t("quotesWidget","Ошибка сохранения настроеек панели")));
			}
		}

		function actionAjaxSearchQuote(){
			$idQuote=(int)$_REQUEST['id_quote'];
			$nameQuote=$_REQUEST['name_quote'];
			$widget=$this->getWidget(array('id_quote'=>$idQuote,'name_quote'=>$nameQuote),'ajaxSearch');
			$widget->run();
		}
		function accessRules(){
			return Array(
				Array(
					'allow',
					'roles' => Array('quotesControl')
				),
				Array(
					'deny',
					'users'=>array('*')
				)
			);
		}
		/*function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'registerMember' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}*/
	}
