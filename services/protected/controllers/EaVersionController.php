<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class EaVersionController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "EA versions";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'add', 'edit', 'iframeAdd', 'ajaxDelete' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
	}

?>