<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class NavBarController extends ControllerFrontBase {
		function actionUpdate() {
			$this->widget( "components.widgets.NavbarWidget", Array( 'ns' => 'nsLayoutDefaultIndex' ));
		}
		function accessRules() {
			return Array(
				//Array( 'deny', 'actions' => Array( 'ajaxAddMark', 'ajaxSubscribe', 'ajaxUnsubscribe' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
	}

?>