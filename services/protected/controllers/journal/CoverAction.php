<?php

class CoverAction extends ActionFrontBaseJournal {
	
	function run(  ) {
		$model = $this->getLatestModel();
		$img = Yii::getPathOfAlias('webroot.uploads.journals').DIRECTORY_SEPARATOR.$model->nameCover;
	
		if ( isset( $_GET['width'] ) )
			$img = $_SERVER['DOCUMENT_ROOT'].JournalI18NModel::getCoverWidth( array( 'width' => $_GET['width'] ), $model->nameCover, true );
		else
			$img = $_SERVER['DOCUMENT_ROOT'].JournalI18NModel::getCoverWidth( array( 'width' => 180 ), $model->nameCover, true );
	
		$mimeType = CFileHelper::getMimeType($img);
		
		header("Content-type: {$mimeType}");
		switch($mimeType) {
			case 'image/jpeg':
				$image = imagecreatefromjpeg($img);
				imagejpeg($image);
				break;
			case 'image/png':
				$image = imagecreatefrompng($img);
				imagepng($image);
				break;
			default:
				$image = imagecreate(100, 100);
				break;
		}
		imagedestroy($image);
		Yii::app()->end();
        
	}
}

?>
