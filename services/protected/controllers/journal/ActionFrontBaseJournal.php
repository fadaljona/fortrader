<?php

Yii::import( 'controllers.base.ActionFrontBase' );
class ActionFrontBaseJournal extends ActionFrontBase {
	function normalizeUrl($url) {
		$parts = parse_url($url);
		return $parts['scheme'].'://'.$parts['host'];
	}
	
	protected function getLatestModel() {
		
		$criteria = new CDbCriteria();
		$criteria->order = 'id DESC';
		$criteria->limit = 1;
		$model = JournalModel::model()->find($criteria);
		if( !$model ) $this->throwI18NException( "Can't find EA!" );
		return $model;
	}
}

?>
