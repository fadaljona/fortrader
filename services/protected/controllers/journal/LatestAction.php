<?php

class LatestAction extends ActionFrontBaseJournal {
	
	function run() {
		if(isset($_GET['url'])) {
			$this->addWebmaster($_GET['url']);
		} else {
			throw new CHttpException(404, Yii::t("*", 'Page not found'));
		}
	}
	private function addWebmaster($url) {
		$m = $this->getLatestModel();
		$url = urldecode($url);
		$parsedUrl = parse_url( $url );
		
		$parsedREFERER = parse_url( $_SERVER['HTTP_REFERER'] );
		
		if( $_SERVER['HTTP_REFERER'] && ( $parsedUrl['host'] == $parsedREFERER['host'] ) && $parsedUrl['host'] != 'fortrader.ru' && $parsedUrl['host'] != 'fortrader.org' && $parsedUrl['host'] != 'forexsystems.ru' ) {
			
			$validator = new CUrlValidator();
			if($validator->validateValue($url) !== false) {
				$normalUrl = $this->normalizeUrl($url);

				$criteria = new CDbCriteria();
				$criteria->addCondition('url=:u');
				$criteria->params = array(':u'=>$normalUrl);
				$model = JournalWebmastersModel::model()->find($criteria);
				if( !$model->description ){
					$model->description = isset($_GET['title']) ? urldecode($_GET['title']) : '';
					$model->save();
				}
				if($model === null) {
					$model = new JournalWebmastersModel();
					$model->url = $normalUrl;
					$model->createdDT = new CDbExpression('NOW()');
					$model->description = isset($_GET['title']) ? urldecode($_GET['title']) : '';
					$model->save();
                    
				}
				$this->redirect( $m->absoluteSingleURL . '?url=' . urlencode($parsedUrl['scheme'] . '://' . $parsedUrl['host']) );
			}
		}
		$this->redirect( $m->absoluteSingleURL );
	}
}

?>
