<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadArchiveListAction extends ActionFrontBase {
		private $widget;
		private function getWidget() {
			$widget = $this->controller->createWidget( 'widgets.JournalsArchiveWidget', Array(
			));
			return $widget;
		}
		private function renderList() {
			ob_start();
			$this->widget->renderList();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$this->widget = $this->getWidget();
				
				$data[ 'list' ] = $this->renderList();
				$data[ 'more' ] = $this->widget->issetMore();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>