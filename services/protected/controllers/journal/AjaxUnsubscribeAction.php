<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxUnsubscribeAction extends ActionFrontBase {
		function run() {
			$data = Array();
			try{
				$settings = Yii::App()->user->getModel()->getSettings();
				$settings->reciveNoticeNewJournal = 0;
				$settings->reciveEmailNoticeNewJournal = 0;
				$settings->save();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>