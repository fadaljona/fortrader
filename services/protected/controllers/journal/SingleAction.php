<?
	final class SingleAction extends ActionFrontBaseJournal {
		private $model;
		public $level = 2;
		
		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		private $cat = 'singleJournal';
		private $paramStr;
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Journal',
					'url' => Array( '/journal/last' ),
				),
				(object)Array( 
					'label' => $this->model->name,
				)
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			$item = $navlist->createItemAfter( $navlist->findItemById( "iJournalLast" ));
			$item->setLabel( $this->model->name );
			$item->setActive();
		}
		private function getModel( $slug ) {
			$model = JournalModel::model()->findBySlug( $slug );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta() {
			
			$metaTitle = $this->model->metaTitle;

			$this->metaDesc = $this->model->metaDescription;
			$this->metaKeys = $this->model->metaKeywords;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = $this->model->name;
			}
			
			$this->title = $this->model->title;
			if( !$this->title ) $this->title = '№'.htmlspecialchars( $this->model->number) .' '. $this->model->name . ' - ' . Yii::app()->params['domainInText'];
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			if( $this->model->coverUrl ){
				$this->setLayoutOgSection('');
				$this->setLayoutOgImage( $this->model->coverUrl );
			}
			
			$this->setMainItemscopeItemtype('Periodical');
		}
		private function makeRedirectToSlug(){
			$id = Yii::app()->request->getParam('id');
			if( $id ){
				$slug = JournalModel::getSlugById( $id );
				if( !$slug ) throw new CHttpException(404,'Page Not Found');
				$this->redirect( $this->controller->createUrl( 'journal/single', array( 'slug' => $slug ) ), true, 301 );
			}
		}
		function run( ) {
			$this->makeRedirectToSlug();
			$slug = Yii::app()->request->getParam('slug');
			if( $slug == 'last' || $slug == 'index' || $slug == 'single' ) $this->redirect( $this->controller->createUrl( 'journal/last' ), true, 301 );
			if( !$slug ) throw new CHttpException(404,'Page Not Found');
			
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $slug );
			
			$this->setMeta();
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->setSubitem();	
			$settings = JournalSettingsModel::getModel();
			$this->paramStr = "journal_{$this->model->id}";
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'journal' => $this->model,
				'buyLink' => $settings->buyLink,
				'paramStr' => $this->paramStr,
				'cat' => $this->cat,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>