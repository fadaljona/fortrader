<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'components.JournalApiComponent' );

	final class ApiExportListAction extends ActionFrontBase {
		function run() {
			//header( "ETag: ..." );
			$component = new JournalApiComponent();
			$component->exportList();
		}
	}

?>