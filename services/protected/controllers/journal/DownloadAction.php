<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class DownloadAction extends ActionFrontBaseJournal {
		private $model;
		private function getModel( $id ) {
			$model = JournalModel::model()->find( Array(
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) $this->throwI18NException( "Can't find Version!" );
			return $model;
		}
		private function event( $mode ) {
			if( Yii::App()->user->isGuest ) return;
			
			switch( $mode ) {
				case 'default':{
					UserActivityModel::event( UserActivityModel::TYPE_EVENT_DOWNLOAD_JOURNAL, Array( 'idJournal' => $this->model->id, 'idUser' => Yii::App()->user->id ));
					break;
				}
				case 'view':{
					UserActivityModel::event( UserActivityModel::TYPE_EVENT_VIEW_JOURNAL, Array( 'idJournal' => $this->model->id, 'idUser' => Yii::App()->user->id ));
					break;
				}
			}
		}
		function run( $id, $mode = 'default') {
		
			$this->model = $this->getModel( $id );
			if( !$this->model->free ) throw new CHttpException(404,'Page Not Found');
			CommonLib::disableAppProfileLog();

			$this->model->downloaded();
			
			if(isset($_GET['url']) and $mode == 'default') {
				$this->addWebmasterDownload($_GET['url'], $id);
			}
			
			$this->event( $mode );
			
			$type = CommonLib::getMimeType( $this->model->pathJournal );
			$fileName = basename( $this->model->pathJournal );
			
			header( "Content-type: {$type}" );
			if( $mode == 'default' )
				header( "Content-Disposition: attachment; filename=\"{$fileName}\"" );
			
			readfile( $this->model->pathJournal );
		}
		
		private function addWebmasterDownload($url, $idJournal) {
			$validator = new CUrlValidator();
			if($validator->validateValue($url) !== false) {
				$normalUrl = $this->normalizeUrl($url);
				$criteria = new CDbCriteria();
				$criteria->addCondition('url=:u');
				$criteria->params = array(':u'=>$normalUrl);
				$model = JournalWebmastersModel::model()->find($criteria);
				if($model === null) {
					return false;
				}

				$criteria = new CDbCriteria();
				$criteria->addCondition('ip=:ip AND idJournal=:j AND idWebmaster=:w AND (TO_DAYS(NOW()) - TO_DAYS(createdDT)) < 1');
				$criteria->params = array(':ip'=>$_SERVER['REMOTE_ADDR'], ':j'=>$idJournal, ':w'=>$model->idWebmaster);

				$download = JournalWebmasterDownloadsModel::model()->find($criteria);
				
				if($download === null) {
					$download = new JournalWebmasterDownloadsModel();
					$download->ip = $_SERVER['REMOTE_ADDR'];
					$download->idJournal = $idJournal;
					$download->idWebmaster = $model->idWebmaster;
					$download->createdDT = new CDbExpression('NOW()');
					$download->save();
				}
			}
		}
	}

?>