<?
	final class ScriptsAction extends ActionFrontBaseJournal {
	
		function run( $num ) {
			$model = JournalModel::model()->find( array(
				'condition' => ' `t`.`number` = :number ',
				'params' => array( ':number' => $num )
			) );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			$this->redirect( $model->singleURL, true, 301 );
		}
	}

?>