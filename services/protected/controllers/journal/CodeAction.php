<?php

class CodeAction extends ActionFrontBaseJournal {
	public function run() {
		$width = $_POST['width'];
		$height = $_POST['height'];
		$model = $this->getLatestModel();
		echo CJSON::encode(Array('code'=>$model->getJavascriptCode($width, $height)));
	}
}

?>
