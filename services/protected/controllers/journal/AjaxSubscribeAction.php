<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxSubscribeAction extends ActionFrontBase {
		function run() {
			$data = Array();
			try{
				$settings = Yii::App()->user->getModel()->getSettings();
				$settings->reciveNoticeNewJournal = 1;
				$settings->reciveEmailNoticeNewJournal = 1;
				$settings->save();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>