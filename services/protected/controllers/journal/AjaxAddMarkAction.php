<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxAddMarkAction extends ActionFrontBase {
		private function getModel( $idJournal, $value ) {
			$idUser = Yii::App()->user->id;
			$model = JournalMarkModel::model()->findByAttributes(Array(
				'idJournal' => (int)$idJournal,
				'idUser' => $idUser,
			));
			if( $model ) {
				$model->value = $value;
			}
			else{
				$model = JournalMarkModel::instance( $idJournal, $idUser, $value );
			}
			return $model;
		}
		function run( $idJournal, $value ) {
			$data = Array();
			try{
				if( !is_numeric( $value ) ) {
					$this->throwI18NException( "Wrong value!" );
				}
				
				$model = $this->getModel( $idJournal, $value );
				$model->save();
				JournalModel::updateStats(true);
				$data[ 'votes' ] = JournalModel::getCountMark($idJournal);
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>