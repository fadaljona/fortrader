<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class LastAction extends ActionFrontBase {
		private $model;
		private $settings;
		public $title;
		public $metaTitle = 'Journal';
		public $metaDesc;
		public $metaKeys;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Journal',
				),
			);
		}
		private function setMeta() {
			
			$this->settings = $settings = JournalSettingsModel::getModel();
			$metaTitle = $settings->metaTitle;

			$this->metaDesc = $settings->metaDescription;
			$this->metaKeys = $settings->metaKeywords;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $settings->title;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			if( $settings->ogImgUrl ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settings->ogImgUrl );
			}
		}
		private function getModel() {
            $c = new CDbCriteria();
            $c->condition="`t`.`status` = '0'";
            $c->order = " `t`.`date` DESC, `t`.`id` DESC ";
            $c->limit = 1;

			$model = JournalModel::model()->find($c);

			if( !$model ) $this->throwI18NException( "Can't find Journal!" );
			return $model;
		}
		function run() {
			
			$this->setMeta();
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel();
						
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
									
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'journal' => $this->model,
				'settings' => $this->settings,
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>