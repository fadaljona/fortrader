<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class TradersRatingAction extends ActionFrontBase {
		public $title = 'Rating of traders';
		private function setBreadcrumbs() {
			$this->title = Yii::t( '*', $this->title );
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Quotes',
					'url' => Array( '/quotesNew/list' ),
				),
				(object)Array( 
					'label' => $this->title,
				)
			);
		}
		function run( ) {
			$actionCleanClass = $this->getCleanClassName();
			$this->setLangSwitcher(true);
			$this->setLayoutTitle( $this->title, "{title}{-}{appName}" );
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			$this->controller->render( "{$actionCleanClass}/view", Array(
			));
		}
	}

?>