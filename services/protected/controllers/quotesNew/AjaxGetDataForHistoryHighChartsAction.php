<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxGetDataForHistoryHighChartsAction extends ActionFrontBase {
	
	

		function run( $symbolId, $symbolName, $callback, $memberId, $start = false, $end = false ) {
			
		
			$data = '';
			try{
				
				$callback = $_GET['callback'];
				if (!preg_match('/^[a-zA-Z0-9_]+$/', $callback)) {
					$this->throwI18NException( "Invalid callback name" );
				}
				if( !$symbolName ) $this->throwI18NException( "no symbolName" );
				
				$data = QuotesHistoryDataModel::getDataForHistoryHighCharts($callback, $symbolId, $symbolName, $memberId, $start, $end);
				if( !$data ) $this->throwI18NException( "Can't get data" );
					
			}
			catch( Exception $e ) {
				$data = $e->getMessage();
			}
			echo $data;
		}
	}

?>