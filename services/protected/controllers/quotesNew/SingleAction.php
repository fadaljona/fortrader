<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		public $title;
		private $metaTitle;
		private $metaDescription;
		
		private $cat = 'singleQuote';
		private $paramStr;
		
		static function siteMapArgs(){
			return true;
		}
		private function setBreadcrumbs() {
			$this->controller->commonBag->views = $this->model->views;
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Quotes',
					'url' => Array( '/quotesNew/list' ),
				),
				(object)Array( 
					'label' => $this->title,
				)
			);
		}
		private function setMeta(){
			if( $this->model->title ){
				$this->title = $this->model->title;
			}elseif( $this->model->nalias ){
				$this->title = $this->model->nalias;
			}else{
				$this->title = $this->model->name;
			}
			if( $this->model->metaTitle ) $this->metaTitle = $this->model->metaTitle;
			if( !$this->metaTitle ) $this->metaTitle = $this->title;
			$this->title = Yii::t( '*', $this->title );
			$this->metaTitle = Yii::t( '*', $this->metaTitle );
			
			$this->metaDescription = $this->model->metaDescription ? $this->model->metaDescription : '';
			
			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDescription );
			$this->setLayoutKeywords( $this->model->metaKeywords );
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $this->model->getOgImage() );
		}
		private function setModel( $slug ) {
			$model = QuotesSymbolsModel::model()->findBySlugWithLang( $slug );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			$this->model = $model;
		}
		function run( $slug ) {
			$this->setModel( $slug );
			$this->model->viewed();
			$this->setMeta();
			$this->setLangSwitcher($this->model);
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->paramStr = 'symbol_'.$this->model->id;
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'symbol' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDescription
			));
		}
	}

?>