<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ChartsAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Quotes Charts';
		public $desc;
		public $shortDesc;
		public $metaDesc;
		public $metaKeys;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Quotes',
					'url' => Array( '/quotesNew/list' ),
				),
				(object)Array( 
					'label' => $this->title,
				)
			);
		}
		private function setMeta() {
			$quotesSettings = QuotesSettingsModel::getInstance();
			$this->title = $quotesSettings->chartsListTitle;
			$metaTitle = $quotesSettings->chartsListMetaTitle;
			$this->desc = $quotesSettings->chartsListDesc;
			$this->metaDesc = $quotesSettings->chartsListMetaDesc;
			$this->metaKeys = $quotesSettings->chartsListMetaKeys;
			
			$this->shortDesc = $quotesSettings->chartsListShortDesc;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			if( !$this->title ) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $quotesSettings->chartsOgImage );
			$this->setLangSwitcher(true);
		}
		function run( ) {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setMeta();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>