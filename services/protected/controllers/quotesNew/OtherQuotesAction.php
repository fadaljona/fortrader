<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class OtherQuotesAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Other Quotes';
		public $desc;
		public $metaDesc;
		public $metaKeys;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Quotes',
					'url' => Array( '/quotesNew/list' ),
				),
				(object)Array( 
					'label' => $this->title,
				)
			);
		}
		private function setMeta() {
			$quotesSettings = QuotesSettingsModel::getInstance();

			$this->title = $quotesSettings->otherQuotesListTitle;
			$metaTitle = $quotesSettings->otherQuotesListMetaTitle;
			$this->desc = $quotesSettings->otherQuotesListDesc;
			$this->metaDesc = $quotesSettings->otherQuotesListMetaDesc;
			$this->metaKeys = $quotesSettings->otherQuotesListMetaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			if( !$this->title ) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $quotesSettings->otherQuotesOgImage );
			$this->setLangSwitcher(true);
		}
		function run( ) {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setMeta();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			$this->controller->render( "{$actionCleanClass}/view", Array(
			));
		}
	}

?>