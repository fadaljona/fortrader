<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadPeriodChartDataAction extends ActionAdminBase {
	
		function run( $symbolId, $period ) {
			
			if( !isset($_GET['symbolId']) || !isset($_GET['period']) ){
				echo '';
				return false;
			}
			echo QuotesHistoryDataModel::getDataForPeriodChart( $symbolId, $period );
		}
	}

?>