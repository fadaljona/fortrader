<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import('models.QuotesSymbolsModel');
	Yii::import('models.QuotesSymbolsI18NModel');
	
	final class AjaxSearchAction extends ActionAdminBase {
	
		function run() {
			$data = Array('result' => '');
			
			if( !isset($_POST['search']) ) return false;
			$searchTerm = $_POST['search'];
			
			QuotesSearchStatsModel::saveKey( $searchTerm );
			
			$symbols = QuotesSymbolsModel::model()->findAll(Array(
				'with' => array('currentLanguageI18N'),
				'select' => " `t`.`id`, `t`.`name` ",
				'condition' => " ( `cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '' AND `t`.`name` LIKE :searchTerm OR `cLI18NQuotesSymbolsModel`.`nalias` LIKE :searchTerm OR `cLI18NQuotesSymbolsModel`.`tooltip` LIKE :searchTerm OR `cLI18NQuotesSymbolsModel`.`searchPhrases` LIKE :searchTerm ) AND `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' ",
				'params' => array( ':searchTerm' => '%'.$searchTerm.'%' ),
			));
			if( $symbols ){
				foreach( $symbols as $symbol ){
					$name = $symbol->name;
					if( $symbol->nalias ) $name = $symbol->nalias;
					if( $symbol->tooltip ) $name .= ' - ' . $symbol->tooltip;
					$data['result'] .= CHtml::tag( 
						"li", 
						Array( 'class' => '' ), 
						CHtml::link( 
							CHtml::encode( $name ), 
							$symbol->singleURL
						)
					);
				}
			}else{
				$data['result'] = CHtml::tag( "li", Array( 'class' => '' ), Yii::t( $NSi18n, 'No results found' ) );
			}
			echo json_encode( $data );
		}
	}

?>