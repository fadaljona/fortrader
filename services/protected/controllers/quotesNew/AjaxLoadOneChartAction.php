<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import('models.QuotesSymbolsModel');
	
	final class AjaxLoadOneChartAction extends ActionAdminBase {
	
		function run() {
			$data = Array();
			
			if( !isset($_POST['symbol']) ){
				echo '';
				return false;
			}
			
			$symbol = QuotesSymbolsModel::model()->findByPk( $_POST['symbol'] );
			
			if(!$symbol){
				echo '';
				return false;
			}
			
			$this->controller->widget('widgets.QuoteOneChartWidget',Array( 'model' => $symbol, 'ajax' => true ));
		}
	}

?>