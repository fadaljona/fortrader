<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.AdminQuotesSymbolsFormModel' );

	final class ListAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Quotations of currencies, oil, gold, shares online';
		public $desc;
		public $shortDesc;
		public $metaDesc;
		public $metaKeys;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => "Quotes",
				)
			);
		}
		private function setMeta() {
			$quotesSettings = QuotesSettingsModel::getInstance();

			$this->title = $quotesSettings->quotesListTitle;
			$metaTitle = $quotesSettings->quotesListMetaTitle;
			$this->desc = $quotesSettings->quotesListDesc;
			$this->metaDesc = $quotesSettings->quotesListMetaDesc;
			$this->metaKeys = $quotesSettings->quotesListMetaKeys;
			
			$this->shortDesc = $quotesSettings->quotesListShortDesc;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			if( !$this->title ) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $quotesSettings->quotesOgImage );
			$this->setLangSwitcher(true);
		}
		function run() {
			$this->title = Yii::t( '*', $this->title );
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setMeta();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'metaDesc' => $this->metaDesc
			));
		}
	}
