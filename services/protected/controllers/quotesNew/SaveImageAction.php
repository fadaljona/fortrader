<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class SaveImageAction extends ActionFrontBase {
	
		function run() {

			if( !isset( $_POST['img'] ) || !isset( $_POST['symbolId'] ) ) return true;
			$symbol = QuotesSymbolsModel::model()->findByPk( $_POST['symbolId'] );
			if( !$symbol ) return true;
			
			$symbol->saveBase64Img( $_POST['img'] );
			
		}
	}

?>