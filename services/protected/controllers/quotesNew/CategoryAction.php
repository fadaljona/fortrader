<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class CategoryAction extends ActionFrontBase {
		private $model;
		public $title;
		private $metaTitle;
		private $metaDescription;
		
		static function siteMapArgs(){
			return true;
		}

		private function setBreadcrumbs() {

			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Quotes',
					'url' => Array( '/quotesNew/list' ),
				),
				(object)Array( 
					'label' => $this->title,
				)
			);
		}
		private function setMeta(){
			if( $this->model->title ){
				$this->title = $this->model->title;
			}else{
				$this->title = $this->model->name;
			}
			if( $this->model->metaTitle ) $this->metaTitle = $this->model->metaTitle;
			if( !$this->metaTitle ) $this->metaTitle = $this->title;
			
			$this->metaDescription = $this->model->metaDescription ? $this->model->metaDescription : '';
			
			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDescription );
			$this->setLayoutKeywords( $this->model->metaKeywords );
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $this->model->ogImageUrl );
		}
		private function setModel( $slug ) {
			$model = QuotesCategoryModel::model()->findBySlugWithLang( $slug );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			
			$this->model = $model;
		}
		function run( $slug ) {
			$this->setModel( $slug );
			$this->setMeta();
			
			$this->setLangSwitcher($this->model);
			
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model,
				'metaDesc' => $this->metaDescription
			));
		}
	}

?>