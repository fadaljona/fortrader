<?php
Yii::import('controllers.base.ActionAdminBase');

final class AjaxGetCourseAction extends ActionAdminBase
{

    public function run($id)
    {
        $symbol = QuotesSymbolsModel::model()->findByPk($id);
        if (!$symbol) {
            return false;
        }
        echo $symbol->closeQuote;
    }
}
