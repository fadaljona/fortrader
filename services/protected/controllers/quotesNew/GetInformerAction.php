<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class GetInformerAction extends ActionFrontBase {

		private function saveStats(){
			$referer = $_SERVER['HTTP_REFERER'];
			if( $referer && $this->checkIfNotFortrader() ){
				QuotesInformerStatsModel::saveStats( $referer );
			}
		}
		private function checkIfNotFortrader(){
			$referer = $_SERVER['HTTP_REFERER'];
			if( strpos($referer, Yii::App()->request->getHostInfo()) === false ){
				return true;
			}
			return false;
		}
		private function generateInformer(){
			ob_start();
				$actionCleanClass = $this->getCleanClassName();
				$this->setLayout( "informer/index" );
				$this->controller->render( "{$actionCleanClass}/view", Array());
			return ob_get_clean();
		}
		function run( ) {
			$this->saveStats();
			
			if( $this->checkIfNotFortrader() ){
				$params = $_GET;
				sort($params);
				$params = implode('.', $params);
				
				if( !Yii::app()->cache->get( 'quotesInformer.'.$params ) ){
					$informer = $this->generateInformer();
					Yii::app()->cache->set( 'quotesInformer.'.$params, $informer, 60*5);
				}
				echo Yii::app()->cache->get(  'quotesInformer.'.$params );
			}else{
				echo $this->generateInformer();
			}
		}
	}

?>