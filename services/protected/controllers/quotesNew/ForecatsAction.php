<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ForecatsAction extends ActionFrontBase {
		private $userModel;
		public $title;
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Quotes',
					'url' => Array( '/quotesNew/list' ),
				),
				(object)Array( 
					'label' => 'Your forecasts',
				)
			);
		}
		private function setUserModels( $id ) {
			$model = UserModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find user!" );
			$this->userModel = $model;
			$this->title = Yii::t( '*', 'Forecasts from' ) . ' ' . $this->userModel->getShowName();
		}
		function run( $userId ) {
			$this->setUserModels( $userId );
			$actionCleanClass = $this->getCleanClassName();
			$this->setBreadcrumbs();
			$this->setLangSwitcher(true);
			$this->setLayoutTitle( $this->title, "{title}{-}{appName}" );
			$this->setLayout( "wp/index" );
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'userModel' => $this->userModel,
			));
		}
	}

?>