<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class InformerAction extends ActionFrontBase {

		public $title;
		public $metaTitle = 'Quotes Informer';
		public $metaDesc;
		public $metaKeys;
		
		private function setBreadcrumbs() {
			$this->title = Yii::t( '*', $this->title );
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Quotes',
					'url' => Array( '/quotesNew/list' ),
				),
				(object)Array( 
					'label' => $this->title,
				)
			);
		}
		private function setMeta() {
			$quotesSettings = QuotesSettingsModel::getInstance();

			$this->title = $quotesSettings->quotesInformerTitle;
			$metaTitle = $quotesSettings->quotesInformerMetaTitle;
			$this->metaDesc = $quotesSettings->quotesInformerMetaDesc;
			$this->metaKeys = $quotesSettings->quotesInformerMetaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			if( !$this->title ) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );

		}
		function run( ) {
			
			$this->setMeta();
			
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			$this->controller->render( "{$actionCleanClass}/view", Array(
			));
		}
	}

?>