<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import('models.forms.QuoteForecastFormModel');
	
	final class AjaxAddForecastAction extends ActionAdminBase {
		private $formModel;
	
		private function detFormModel() {
			$this->formModel = new QuoteForecastFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Forecast!" );
			
			$period = intval( $this->formModel->startDate );
			if( $period ){
				$this->formModel->endDate = gmdate( 'Y-m-d H:i:s', time() + $period * 3600 );
				$this->formModel->startDate = gmdate( 'Y-m-d H:i:s', time() );
			}
			if( $this->formModel->status == 2 ){
				$date = date( 'Y-m-d', time() );
				$date .= ' 00:00:00';
				$this->formModel->startDate = $this->formModel->endDate = $date;
			}
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				
				$this->detFormModel();
				$formModel = $this->getFormModel();
				
				if( !Yii::App()->user->id ){
					$currSymbol = QuotesSymbolsModel::model()->findByPk( $formModel->symbolId );
					$this->throwI18NException( 
						Yii::t( '*', 'User must be registered.' ) . " " .  
						CHtml::link(
							Yii::t( '*', 'Please login or register' ), 
							array('default/login', 'returnURL' => Yii::App()->controller->createAbsoluteUrl( '/quotesNew/single', array('slug' => strtolower($currSymbol->name) ) ))
						)
					);
				} 

				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
					if( $formModel->status == 2 ){
						$data[ 'mood' ] = QuotesForecastsModel::getMood( $formModel->symbolId );
						
						if( $formModel->value == 1 ){
							$data[ 'yourMood' ] = Yii::t( '*', 'Bullish' );
						}elseif( $formModel->value == -1 ){
							$data[ 'yourMood' ] = Yii::t( '*', 'Bearish' );
						}
						
					}else{
						$data[ 'count' ] = QuotesForecastsModel::getCountForUser();
					}
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>