<?php
Yii::import('controllers.base.ActionFrontBase');

final class IndexAction extends ActionFrontBase
{
    public $title;
    public $metaTitle = 'Monitoring of exchangers';
    public $metaDesc;
    public $metaKeys;
    public $settings;

    private function setBreadcrumbs()
    {
        $this->controller->commonBag->breadcrumbs = array(
            (object)array(
                'label' => 'Monitoring of exchangers',
            )
        );
    }
    private function setMeta()
    {
        $this->settings = PaymentExchangerSettingsModel::getModel();

        $metaTitle = $this->settings->mainMetaTitle;

        $this->metaDesc = $this->settings->mainMetaDesc;
        $this->metaKeys = $this->settings->mainMetaKeys;

        if ($metaTitle) {
            $this->metaTitle = $metaTitle;
        } else {
            $this->metaTitle = Yii::t('*', $this->metaTitle);
        }
        $this->title = $this->settings->mainTitle;
        if (!$this->title) {
            $this->title = $this->metaTitle;
        }
        if (!$this->metaDesc) {
            $this->metaDesc = $this->metaTitle;
        }

        $this->setLayoutTitle($this->metaTitle, "{title}{-}{appName}");
        $this->setLayoutDescription($this->metaDesc);
        $this->setLayoutKeywords($this->metaKeys);

        if ($settingImg = $this->settings->mainPageOgImageSrc) {
            $this->setLayoutOgSection();
            $this->setLayoutOgImage($settingImg);
        }
        $this->setLangSwitcher(true);
    }
    public function run()
    {
        $this->setMeta();

        $actionCleanClass = $this->getCleanClassName();

        $this->setLayout("wp2/index");
        $this->setBreadcrumbs();

        $this->controller->render("{$actionCleanClass}/view", array(
            'settings' => $this->settings,
            'metaDesc' => $this->metaDesc
        ));
    }
}
