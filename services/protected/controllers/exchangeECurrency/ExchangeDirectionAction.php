<?php
Yii::import('controllers.base.ActionFrontBase');

final class ExchangeDirectionAction extends ActionFrontBase {
    private $model;

    public $title;
    public $metaTitle;
    public $metaDesc;
    public $metaKeys;

    private function setBreadcrumbs()
    {
        if (isset($this->model->pageTitle) && $this->model->pageTitle) {
            $label = $this->model->pageTitle;
        } else {
            $label = $this->model->title;
        }

        $this->controller->commonBag->breadcrumbs = array(
            (object)array(
                'label' => 'Monitoring of exchangers',
                'url' => array( '/exchangeECurrency/index' ),
            ),
            (object)array(
                'label' => $label,
            )
        );
    }
    private function getModel($from, $to)
    {
        $model = PaymentExchangeDirectionModel::findBySlugsWithLang($from, $to);
        if (!$model) {
            throw new CHttpException(404, 'Page Not Found');
        }
        return $model;
    }
    private function setMeta()
    {
        $metaTitle = $this->model->metaTitle;

        $this->metaDesc = $this->model->metaDesc;
        $this->metaKeys = $this->model->metaKeys;

        if ($metaTitle) {
            $this->metaTitle = $metaTitle;
        } else {
            $this->metaTitle = $this->model->title;
        }

        $this->title = $this->model->title;

        if (!$this->metaDesc) {
            $this->metaDesc = $this->metaTitle;
        }

        $this->setLayoutTitle($this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}");
        $this->setLayoutDescription($this->metaDesc);
        $this->setLayoutKeywords($this->metaKeys);

        if ($this->model->absoluteSrcOgImage) {
            $this->setLayoutOgSection();
            $this->setLayoutOgImage($this->model->absoluteSrcOgImage);
        }
    }

    public function run($from, $to)
    {
        $actionCleanClass = $this->getCleanClassName();
        $this->model = $this->getModel($from, $to);
        $this->setMeta();

        $this->setLangSwitcher($this->model);


        $this->setLayout("wp2/index");
        $this->setBreadcrumbs();

        $this->controller->render("{$actionCleanClass}/view", array(
            'model' => $this->model,
            'metaDesc' => $this->metaDesc
        ));
    }
}
