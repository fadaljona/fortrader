<?php
Yii::import('controllers.base.ActionFrontBase');

final class AjaxAddMarkToExchangerAction extends ActionFrontBase
{
    private function getModel($idExchanger, $value)
    {
        $idUser = Yii::App()->user->id;
        $model = PaymentExchangerItemMarkModel::model()->findByAttributes(array(
            'idExchanger' => (int)$idExchanger,
            'idUser' => $idUser,
        ));
        if ($model) {
            $model->value = $value;
        } else {
            $model = PaymentExchangerItemMarkModel::instance($idExchanger, $idUser, $value);
        }
        return $model;
    }
    public function run($idExchanger, $value)
    {
        if (!Yii::app()->user->id) {
            return false;
        }
        $data = array();
        try {
            if (!is_numeric($value)) {
                $this->throwI18NException("Wrong value!");
            }
            $model = $this->getModel($idExchanger, $value);
            $model->save();
            PaymentExchangerModel::updateStats(true);
        } catch (Exception $e) {
            $data[ 'error' ] = $e->getMessage();
        }
        echo json_encode($data);
    }
}
