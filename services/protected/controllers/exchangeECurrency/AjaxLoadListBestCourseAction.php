<?php
Yii::import('controllers.base.ActionFrontBase');

final class AjaxLoadListBestCourseAction extends ActionFrontBase
{
    private function renderList()
    {
        ob_start();

        $fromCurrency = $toCurrency = false;
        if (isset($_GET['fromCurrency']) && $_GET['fromCurrency']) {
            $fromCurrency = $_GET['fromCurrency'];
        }
        if (isset($_GET['toCurrency']) && $_GET['toCurrency']) {
            $toCurrency = $_GET['toCurrency'];
        }

        $this->controller->widget('widgets.lists.ExchangerWithCoursesAndReservesListWidget', array(
            'ns' => 'nsActionView',
            'fromCurrency' => $fromCurrency,
            'toCurrency' => $toCurrency
        ));

        $content = ob_get_clean();
        $content = preg_replace("#[\r\n\t]+#", " ", $content);

        return $content;
    }
    public function run()
    {
        $data = array();
        try {
            $data[ 'list' ] = $this->renderList();
        } catch (Exception $e) {
            $data[ 'error' ] = $e->getMessage();
        }
        echo json_encode($data);
    }
}
