<?php
Yii::import('controllers.base.ActionFrontBase');

final class AjaxLoadListExchangersRatingAction extends ActionFrontBase
{
    private function renderList()
    {
        ob_start();

        $this->controller->widget('widgets.lists.ExchangersRatingListWidget', array(
            'ns' => 'nsActionView',
        ));

        $content = ob_get_clean();
        $content = preg_replace("#[\r\n\t]+#", " ", $content);

        return $content;
    }
    public function run()
    {
        $data = array();
        try {
            $data[ 'list' ] = $this->renderList();
        } catch (Exception $e) {
            $data[ 'error' ] = $e->getMessage();
        }
        echo json_encode($data);
    }
}
