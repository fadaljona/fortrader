<?php
Yii::import('controllers.base.ActionFrontBase');

final class ExchangersRatingAction extends ActionFrontBase
{
    public $title;
    public $metaTitle = 'Exchangers Rating';
    public $metaDesc;
    public $metaKeys;
    public $settings;

    private function setBreadcrumbs()
    {
        $this->controller->commonBag->breadcrumbs = array(
            (object)array(
                'label' => 'Monitoring of exchangers',
                'url' => array( '/exchangeECurrency/index' ),
            ),
            (object)array(
                'label' => $this->title,
            )
        );
    }
    private function setMeta()
    {
        $this->settings = PaymentExchangerSettingsModel::getModel();
        $metaTitle = $this->settings->exchangersRatingMetaTitle;

        $this->metaDesc = $this->settings->exchangersRatingMetaDesc;
        $this->metaKeys = $this->settings->exchangersRatingMetaKeys;

        if ($metaTitle) {
            $this->metaTitle = $metaTitle;
        }

        $this->title = $this->settings->exchangersRatingTitle;

        if (!$this->title) {
            $this->title = $this->metaTitle;
        }

        if (!$this->metaDesc) {
            $this->metaDesc = $this->metaTitle;
        }

        $this->setLayoutTitle($this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}");
        $this->setLayoutDescription($this->metaDesc);
        $this->setLayoutKeywords($this->metaKeys);

        if ($this->settings->paymentSystemsRatingPageOgImageSrc) {
            $this->setLayoutOgSection();
            $this->setLayoutOgImage($this->settings->paymentSystemsRatingPageOgImageSrc);
        }
    }

    public function run()
    {
        $actionCleanClass = $this->getCleanClassName();
        $this->setMeta();

        $this->setLangSwitcher(true);


        $this->setLayout("wp2/index");
        $this->setBreadcrumbs();

        $this->controller->render("{$actionCleanClass}/view", array(
            'settings' => $this->settings,
            'metaDesc' => $this->metaDesc
        ));
    }
}
