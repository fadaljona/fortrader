<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class MonitoringController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Forex account monitoring";
		}
		function accessRules(){
			return Array(
				array(
					/*'deny',
					'actions' => array('index', 'single'),
					'expression' => array('MonitoringController','allowOnlyAdmin'),
					'users'=>array('*')*/
				),
			);
		}
		public function filters(){
			return array(
				'accessControl'
			);
		}
	
		public static function allowOnlyAdmin(){
			if( Yii::App()->user->checkAccess( 'monitoringControl' ) ) return false;
			return true;
		}
	}

?>