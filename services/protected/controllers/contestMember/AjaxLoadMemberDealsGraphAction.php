<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadMemberDealsGraphAction extends ActionFrontBase {
		private function renderGraph( $contestMemberId ) {
			ob_start();
			
			$this->controller->widget( 'widgets.graphs.ContestMemberDealsGraphWidget', Array(
				'ns' => 'nsActionView',
				'contestMemberId' => $contestMemberId,
				'ajax' => true,
			));
			
			$content = ob_get_clean();
			return $content;
		}
		function run( $contestMemberId ) {
			$data = Array();
			try{
				$data[ 'graph' ] = $this->renderGraph( $contestMemberId );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>