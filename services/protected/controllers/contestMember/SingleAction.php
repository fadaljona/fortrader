<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		public $level = 3;
		
		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		private $cat = 'singleContestMember';
		private $paramStr;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Contests',
					'url' => Array( '/contest/list' ),
				),
				(object)Array( 
					'label' => $this->model->contest->currentLanguageI18N->name,
					'url' => $this->model->contest->singleURL,
				),
				(object)Array( 
					'label' => "{$this->model->user->showName}, {$this->model->accountNumber}",
				)
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			
			$item1 = $navlist->createItemAfter( $navlist->findItemById( "iContestsList" ));
			$item1->setLabel( $this->model->contest->currentLanguageI18N->name );
			
			$item2 = $navlist->createItemAfter( $item1 );
			$item2->setLabel( "{$this->model->user->showName}, {$this->model->accountNumber}" );
			$item2->setActive();
		}
		private function getModel( $id ) {
			$model = ContestMemberModel::model()->findByPk( $id );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta() {
			
			$this->metaTitle = "{$this->model->contest->currentLanguageI18N->name}: {$this->model->user->showName}, {$this->model->accountNumber}";

			$this->metaDesc = $this->metaTitle;
			$this->metaKeys = $this->metaTitle;
			
			$this->title = "{$this->model->user->showName} - {$this->model->accountNumber}";

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
		}
		function run( $id, $rev = false ) {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();

			if ( !isset($rev) ) {
				$rev = ContestMemberModel::lastRevision($id);
			}

            $this->model = $this->getModel( $id );

            if($this->model) {
                $accountNumber = ContestMemberModel::getAccountNumber($id);
                $revlist = array();
                $c = new CDbCriteria();
                $c->select = 'revision';
                $c->condition = 'ACCOUNT_NUMBER='.$accountNumber;
                $c->group = 'revision';
                $trades = MT5AccountTradesModel::model()->findAll($c);
                $c->condition = 'AccountNumber='.$accountNumber;
                $c->group = 'revision';
                $history = MT4AccountHistoryModel::model()->findAll($c);
                $res = array_merge($trades, $history);
                foreach ($res as $one) {
                    $revlist[] = $one->revision;
                }
                $revlist = array_unique($revlist);

                if ( count($revlist) > 0  and !isset($_GET['rev'])) {
                    $rev = $revlist[count($revlist)-1];
                }

                $linklist = '';
                if ( count($revlist) > 1 ) {
                    $linklist = '<ul class="nav nav-pills iUl i03">';
                    foreach ($revlist as $one) {
                        $active = '';
                        if ( $rev == $one ) {
                            $active = 'active';
                        }
                        $linklist .= '<li class="'.$active.'">';
                        $linklist .= '
                    <a href="/services/contestMember/single?id='.$id.'&rev='.$one.'">
                    <i class="iI i22 i22_ratings"></i>
                    [Ревизия №'.$one.']</a>';
                        $linklist .= '</li>';
                    }
                    $linklist .= '</ul>';
                }

                

                $this->setBreadcrumbs();
                $this->setSubitem();
            } else {
                $revlist = null;
                $linklist = null;
                $this->model = false;
            }

			$this->setMeta();
			$this->setLayoutBodyClass('participant_page');
			$this->setLayout( "wp/index" );
			
			$this->paramStr = "contestMember_{$this->model->id}";

            $this->controller->render( "{$actionCleanClass}/view", Array(
                    'member' => $this->model,
                    'rev' => $rev,
                    'revlist' => $revlist,
                    'linklist' => $linklist,
					'cat' => $this->cat,
					'paramStr' => $this->paramStr,
            ));

		}
	}

?>
