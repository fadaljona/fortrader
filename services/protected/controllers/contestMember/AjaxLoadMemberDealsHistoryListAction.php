<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadMemberDealsHistoryListAction extends ActionFrontBase {
		private function renderList() {
			ob_start();
			
			$this->controller->widget( 'widgets.lists.ContestMemberDealsHistoryListWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$data[ 'list' ] = $this->renderList();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>