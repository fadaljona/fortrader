<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxAddMarkAction extends ActionFrontBase {
		private function getModel( $idModel, $modelName, $value ) {
			$idUser = Yii::App()->user->id;
			$model = CommonRatingItemMarkModel::model()->findByAttributes(Array(
				'idModel' => $idModel,
				'modelName' => $modelName,
				'idUser' => $idUser,
			));
			if( $model ) {
				$model->value = $value;
			}
			else{
				$model = CommonRatingItemMarkModel::instance( $idModel, $modelName, $idUser, $value );
			}
			return $model;
		}
		function run( $idModel, $modelName, $value ) {
			if( !Yii::app()->user->id ) return false;
			$data = Array();
			try{
				if( !is_numeric( $value ) ) {
					$this->throwI18NException( "Wrong value!" );
				}
				
				$model = $this->getModel( $idModel, $modelName, $value );
				$model->save();
				CommonRatingItemStatsModel::updateStats();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>