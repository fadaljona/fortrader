<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'EA Catalog',
					'url' => Array( '/ea/list' ),
				),
				(object)Array( 
					'label' => $this->model->version->EA->name,
					'url' => $this->model->version->EA->getSingleURL(),
				),
				(object)Array( 
					'label' => Yii::t( '*', 'Version' )." ".$this->model->version->version,
					'url' => $this->model->version->getSingleURL(),
				),
				(object)Array( 
					'label' => $this->model->alt,
				)
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			
			$item1 = $navlist->createItemAfter( $navlist->findItemById( "iEARating" ));
			$item1->setLabel( "{$this->model->version->EA->name}<br>".Yii::t( '*', 'Version' )." {$this->model->version->version}" );
			$item1->setStrictURL( $this->model->version->getSingleURL() );
						
			$item2 = $navlist->createItemAfter( $item1 );
			$item2->setLabel( $this->model->shortAlt );
			$item2->setActive();
		}
		private function getModel( $id ) {
			$model = EAStatementModel::model()->find( Array(
				'with' => Array(
					'version' => Array(
						'with' => Array(
							'EA' => Array(),
						),
					),
					'user' => Array(
						'select' => 'ID, user_login',
					),
					'i18ns' => Array(),
					'currentLanguageI18N' => Array(),
				),
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		function run( $id ) {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $id );
			$this->model->viewed();
			
			$this->setLayoutTitle( $this->model->alt );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'statement' => $this->model,
			));
		}
	}

?>