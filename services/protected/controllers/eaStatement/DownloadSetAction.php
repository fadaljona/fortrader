<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class DownloadSetAction extends ActionFrontBase {
		private $model;
		private function getModel( $id ) {
			$model = EAStatementModel::model()->find( Array(
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) $this->throwI18NException( "Can't find Statement!" );
			return $model;
		}
		function run( $id ) {
			CommonLib::disableAppProfileLog();

			$this->model = $this->getModel( $id );
			$this->model->downloaded();
			
			$fileName = basename( $this->model->nameFileSet );
			header( "Content-type: text/plain" );
			header( "Content-Disposition: attachment; filename=\"{$fileName}\"" );
			
			echo file_get_contents( $this->model->pathFileSet );
		}
	}

?>