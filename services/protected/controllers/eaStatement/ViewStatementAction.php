<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ViewStatementAction extends ActionFrontBase {
		private $model;
		private function getModel( $id ) {
			$model = EAStatementModel::model()->find( Array(
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		function run( $id ) {
			CommonLib::disableAppProfileLog();

			$this->model = $this->getModel( $id );
			//$this->model->downloaded();
			
			header( "Content-Type: text/html; charset=utf-8" );
			
			$content = file_get_contents( $this->model->pathFileStatement );
			$content = @iconv( "windows-1251", "UTF-8", $content );
			
			$src = $this->model->srcFileGraph;
			$content = preg_replace( '#<img src=".*"#U', '<img src="'.$src.'"', $content );
			
			echo $content;
		}
	}

?>