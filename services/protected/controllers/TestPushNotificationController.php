<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

    /*
     * Для корректной работы на сервере должна стоять библиотека openssl и соответстующий модуль php
     *
     * Настройка пушей в файле services/protected/config/default.php
     * ('components' - 'apns')
     * закомментировать  ненужную среду с ненужнем ключом
     * 'sandbox' будет рассылать пуши только для девелоперского билда приложени
     *
     * 'environment' => 'sandbox',
     * 'pemFile' => dirname(__FILE__).'/certificates/apns-dev.pem',
     *
     * !!! максимальная объем PUSHа 255 байт !!! включая сообщение !!!
     *
     *
     * подключение компонета для работы с нотификациями
     */
    Yii::import( 'components.PushNotificationComponent' );

	final class TestPushNotificationController extends ControllerFrontBase {


        /*
         *  функционал для текущей задачи.
         */
        public  function  actionTest(){

           /*
            * для приложения нужен ИД журнала в правильном формате  'ru.fortrader.services.journal.ru.100'
            * сообщение будет выведено пользователю
            */

            $message = 'New journal available';
            $idJournal= 'ru.fortrader.services.journal.ru.100';
            $limit = 500;

            $pn = new PushNotificationComponent();

            // рассылка пушей для обновления с новым журналом, рассылаеться всем зарегестрированным токенам
            // $limit - количество токенов которое берется с база для 1 пакера рассылки
            $pn->massSendUpdatePush($idJournal, $message, $limit);
            // если не задавать будет брать пачками по 100
            // $pn->massSendUpdatePush($idJournal, $message);


            //переодически нужно чистить базу от старых токенов, особенно когда их будет много
            $pn->cleanTokens();
        }

        /*
            Пример отправки обычного пуша
            токены есть в базе (модель ApnsDevices)
        */
        public  function  actionTestCustomPush(){

            $token = '7658006a25f6236f17e69c47ea5cb338e293ea39c995c94819119291482150a9';
            $message = 'We have new journals!';

            $apns = Yii::app()->apns;
            $apns->send($token, $message, array(
                    //'sound' => 'default', // не обязательно
                    //'badge' =>1 // не обязательно
                )
            );
        }
	}
?>