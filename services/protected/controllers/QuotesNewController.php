<?
	Yii::import( 'controllers.base.ControllerFrontBase' );
	Yii::import('components.widgets.quotes.QuotesModel');

	final class QuotesNewController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Quotes";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules(){
			return Array(
				array(
					'deny',
					'actions' => array('forecats'),
					'expression' => array('QuotesNewController','allowOnlyOwner'),
					'users'=>array('*')
				),
				/*array(
					'deny',
					'actions' => array('informer'),
					'expression' => array('QuotesNewController','allowOnlyAdmin'),
					'users'=>array('*')
				),*/
				Array( 'allow', 'users'=>array('*')),
			);
		}
		public function allowOnlyOwner(){
			if( Yii::App()->user->checkAccess( 'quotesControl' ) ) return false;
			$currUserId = Yii::App()->user->id;
			if( !$currUserId ) return true;
			if( $currUserId ){
				if( $currUserId != $_GET["userId"] ){
					return true;
				}
			}
			return false;
		}
		public function allowOnlyAdmin(){
			if( Yii::App()->user->id == 1 ) return false;
			return true;
		}
		public function singleSiteMapArgs(){
			$sql = "
				SELECT `name` FROM `{{quotes_symbols}}` `symbols` 
				LEFT JOIN `{{quotes_symbols_i18n}}` `i18n` ON `i18n`.`idSymbol` = `symbols`.`id` AND `i18n`.`idLanguage` = :langId
				WHERE `i18n`.`visible`='Visible'
			";
			$results = Yii::App()->db->createCommand( $sql )->queryAll(true, array( ':langId' => LanguageModel::getCurrentLanguageID() ) );

			$outArr = array( );
			foreach( $results as $result ){
				$outArr[] = array(
					'slug' => strtolower( $result['name'] ),
				);
			}
			return $outArr;
		}
		public function categorySiteMapArgs(){
			return array(
				array( 'slug' => 'ruble' ),
				array( 'slug' => 'russianstock' ),
				array( 'slug' => 'forex' ),
				array( 'slug' => 'futures' ),
				array( 'slug' => 'americanstock' ),
				array( 'slug' => 'cryptorates' ),
				array( 'slug' => 'rts' ),
				array( 'slug' => 'rts-board' ),
				array( 'slug' => 'commodity-markets' ),
				array( 'slug' => 'indexes' ),
				array( 'slug' => 'metals' ),
			);
		}
	}