<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class ContestController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "ForTrader.Ru Contests History Table";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				//Array( 'deny', 'actions' => Array( 'registerMember' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl',
				/*array(
					'PageCacheFilter + list',
					'duration'=> 60*60 * 24,
					'varyByParam' => 'contestsPage',
				),*/
				/*array(
					'PageCacheFilter + single',
					'duration'=> 60 * 5,
					'varyByParam' => 'id',
				),*/
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$params = array();		
				parse_str( Yii::app()->request->getQueryString(), $params );
				
				$action = '';
				if( Yii::app()->controller->action->id ){
					$action = '/' . Yii::app()->controller->action->id;
				}
				if( $action == '/index' ) $action = '/list';
				
				$url = Yii::App()->createURL( 'contest' . $action , $params);
				$url = str_replace( '/services', '', $url );
				
				$this->redirect( $url, true, 301 );
			}
			$filterChain->run();			
		}
		public function singleSiteMapArgs(){
			$models = ContestModel::model()->findAll(array(
				'select' => ' `slug` ',
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = array(
					'slug' => strtolower( $model->slug )
				);
			}
			return $outArr;
		}
	}