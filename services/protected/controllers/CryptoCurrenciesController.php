<?php

Yii::import('controllers.base.ControllerFrontBase');

final class CryptoCurrenciesController extends ControllerFrontBase
{
    public function detLayoutTitle()
    {
        return "Crypto Currencies";
    }
    public function accessRules()
    {
        return array(
            /*array(
                'deny',
                'expression' => array('CryptoCurrenciesController','allowOnlyAdmin'),
                'users'=>array('*')
            ),*/
        );
    }
    public function filters()
    {
        return array(
            'servicesRedirect',
            'accessControl'
        );
    }
    public function filterServicesRedirect($filterChain)
    {
        if (strpos(Yii::App()->request->getUrl(), '/services') === 0) {
            $url = str_replace('/services', '', Yii::App()->request->getUrl());
            $this->redirect(strtolower($url), true, 301);
        }
        $filterChain->run();
    }
    public static function allowOnlyAdmin()
    {
        if (Yii::App()->user->checkAccess('cryptoCurrenciesControllerControl')) {
            return false;
        }
        return true;
    }
}
