<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class QuestionController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Questions and suggestions";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		/*
		function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'registerMember' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
		*/
		public function filters(){
			return array(
				array(
					'PageCacheFilter',
					'duration'=> 60*10,
					'varyByParam' => 'id',
				),
			);
		}
	}

?>