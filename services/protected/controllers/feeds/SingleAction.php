<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;

		private function getModel( $slug ) {
			$model = RssFeedsFeedModel::findBySlug( $slug );
			if( !$model || !$model->enabled || !$model->group->enabled ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}

		function run( $slug ) {	
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $slug );	
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model,
			));
		}
	}

?>