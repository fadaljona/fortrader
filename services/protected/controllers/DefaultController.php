<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class DefaultController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Services";
		}
		public function filters(){
			return array(
				array(
					'PageCacheFilter + index',
					'duration'=> 60*60 * 24 * 30,
					'varyByParam' => 'id',
				),
			);
		}
	}

?>