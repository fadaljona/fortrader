<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class AjaxLikeAction extends ActionFrontBase {
		private function getModel( $id ) {
			$model = AdvertisementModel::model()->find( Array(
				'select' => " id, countLikes ",
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
				'limit' => 1,
			));
			if( !$model ) $this->throwI18NException( "Can't find Ad!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$data[ 'liked' ] = $model->like();
				$data[ 'likes' ] = CommonLib::numberFormat( $model->countLikes );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>