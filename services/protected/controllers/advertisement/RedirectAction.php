<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class RedirectAction extends ActionFrontBase {
		private function checkBlock() {
			$block = AdvertisementBlockModel::getCurrentBlock();
			if( $block ) {
				$this->throwI18NException( "Can't find Ad!" );
			}
		}
		private function getModel( $id ) {
			$model = AdvertisementModel::model()->find( Array(
				'select' => " id, url ",
				'with' => Array( 
					'campaign' => Array(
						'select' => " id, clickCost ",
					), 
					'campaign.owner' => Array(
						'select' => " ID, balance ",
					), 
				),
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
				'limit' => 1,
			));
			if( !$model ) $this->throwI18NException( "Can't find Ad!" );
			return $model;
		}
		private function getZone( $id ) {
			$zone = AdvertisementZoneModel::model()->find(Array(
				'select' => " id ",
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
				'limit' => 1,
			));
			if( !$zone ) $this->throwI18NException( "Can't find Zone!" );
			return $zone;
		}
		function run( $id, $idZone ) {
			$this->checkBlock();
			$model = $this->getModel( $id );
			$zone = $this->getZone( $idZone );
			
			$model->hitClick();
			$zone->hitClick();
			
			$clickCost = strlen($model->campaign->clickCost) ? $model->campaign->clickCost : SettingsModel::getModel()->common_adClickCost;
			$model->campaign->owner->subBalance( $clickCost );
						
			$redirectUrl = $model->getUrl();
			if( $redirectUrl ) Yii::App()->request->redirect( $model->getUrl() );
		}
	}

?>