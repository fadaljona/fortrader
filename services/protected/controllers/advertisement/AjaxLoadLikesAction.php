<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class AjaxLoadLikesAction extends ActionFrontBase {
		const limit = 10;
		private function getModel( $id ) {
			$model = AdvertisementModel::model()->find( Array(
				'select' => " id, countLikes ",
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
				'limit' => 1,
			));
			if( !$model ) $this->throwI18NException( "Can't find Ad!" );
			return $model;
		}
		private function getLikes( $model ) {
			$links = Array();
			
			$likes = $model->likes( Array( 
				'with' => 'user',
				'order' => ' `likes`.`createdDT` DESC ',
				'limit'=> self::limit
			));
			foreach( $likes as $like ) {
				$links[] = CHtml::link( CHtml::encode( $like->user->showname ), $like->user->getProfileURL() );
			}
			if( $model->countLikes > self::limit ) {
				$links[] = '...';
			}
			return implode( ', ', $links );
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$data[ 'likes' ] = $this->getLikes( $model );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>