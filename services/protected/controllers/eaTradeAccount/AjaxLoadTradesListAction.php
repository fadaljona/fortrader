<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadTradesListAction extends ActionFrontBase {
		private function renderList( $id ) {
			ob_start();
			
			$this->controller->widget( 'widgets.lists.EATradeAccountTradesListWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
				'idAccount' => $id,
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$id = (int)@$_GET['id'];
				$data[ 'list' ] = $this->renderList( $id );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>