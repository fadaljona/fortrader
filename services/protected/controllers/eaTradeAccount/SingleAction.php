<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		public $level = 2;
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'EA Catalog',
					'url' => Array( '/ea/list' ),
				),
				(object)Array( 
					'label' => $this->model->EA->name,
					'url' => $this->model->EA->getSingleURL(),
				),
				(object)Array( 
					'label' => Yii::t( '*', 'Version' )." ".$this->model->version->version,
					'url' => $this->model->version->getSingleURL(),
				),
				(object)Array( 
					'label' => $this->model->tradeAccount->accountNumber,
				),
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			
			$item1 = $navlist->createItemAfter( $navlist->findItemById( "iEARating" ));
			$item1->setLabel( "{$this->model->EA->name}<br>".Yii::t( '*', 'Version' )." {$this->model->version->version}" );
			$item1->setStrictURL( $this->model->version->getSingleURL() );
						
			$item2 = $navlist->createItemAfter( $item1 );
			$item2->setLabel( $this->model->tradeAccount->accountNumber );
			$item2->setActive();
		}
		private function getModel( $id ) {
			$model = EATradeAccountModel::model()->find( Array(
				'with' => Array(
					'tradeAccount' => Array(),
					'EA' => Array(),
					'version' => Array(),
				),
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		function run( $id ) {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $id );
			
			$this->setLayoutTitle( $this->model->tradeAccount->accountNumber );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'account' => $this->model,
			));
		}
	}

?>