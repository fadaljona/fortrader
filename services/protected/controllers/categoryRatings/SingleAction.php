<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		private $cat = 'singleCategoryRatings';
		private $paramStr;

		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		private function setBreadcrumbs( $slug ) {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => $this->title,
					'url' => Array( '/categoryRatings/single', 'slug' => $slug ),
				),
			);
		}
		private function getModel( $slug ) {
			$model = CategoryRatingModel::findBySlugWithLang($slug);
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta(){
			$this->title = $this->model->title;
			
			if( $this->model->metaTitle ) $this->metaTitle = $this->model->metaTitle;
			if( !$this->metaTitle ) $this->metaTitle = $this->title;

			
			$this->metaDesc = $this->model->metaDesc ? $this->model->metaDesc : '';
			
			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->model->metaKeys );
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $this->model->absoluteSrcOgImage );
			
			$this->setMainItemscopeItemtype('CreativeWork');
			$this->setLayoutBodyClass('participant_page');
		}

		function run( $slug ) {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->model = $this->getModel( $slug );	
			
			$this->paramStr = "categoryRating_{$this->model->id}";
			$this->setMeta();
			//$this->setLangSwitcher(true);
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs( $slug );
		
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>