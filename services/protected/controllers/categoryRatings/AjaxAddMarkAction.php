<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxAddMarkAction extends ActionFrontBase {
		private function getModel( $postId, $value ) {
			$idUser = Yii::App()->user->id;
			$model = CategoryRatingItemMarkModel::model()->findByAttributes(Array(
				'idPost' => (int)$postId,
				'idUser' => $idUser,
			));
			if( $model ) {
				$model->value = $value;
			}
			else{
				$model = CategoryRatingItemMarkModel::instance( $postId, $idUser, $value );
			}
			return $model;
		}
		function run( $postId, $value ) {
			if( !Yii::app()->user->id ) return false;
			$data = Array();
			try{
				if( !is_numeric( $value ) ) {
					$this->throwI18NException( "Wrong value!" );
				}
				
				$model = $this->getModel( $postId, $value );
				$model->save();
				CategoryRatingModel::updateStats(true);
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>