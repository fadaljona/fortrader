<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class EaTradeAccountController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Trade accounts";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				//Array( 'deny', 'actions' => Array( 'registerMember' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
		public function filters(){
			return array(
				array(
					'PageCacheFilter + list',
					'duration'=> 60*5,
					'varyByParam' => 'eaAccountTradePage',
				),
				array(
					'PageCacheFilter + single',
					'duration'=> 60,
					'varyByParam' => 'id',
				),
			);
		}
	}

?>