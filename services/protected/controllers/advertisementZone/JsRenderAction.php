<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class JsRenderAction extends ActionFrontBase {
		private function checkBlock() {
			$block = AdvertisementBlockModel::getCurrentBlock();
			if( $block ) exit;
		}
		private function getModel( $id ) {
			$model = AdvertisementZoneModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Zone!" );
			return $model;
		}
		function run() {
			CommonLib::disableAppProfileLog();
			$this->checkBlock();
			ob_start();
			try{
				$controllerCleanClass = $this->controller->getCleanClassName();
				$actionCleanClass = $this->getCleanClassName();
				
				$id = (int)@$_GET['id'];
				$model = $this->getModel( $id );
				
				$this->controller->renderPartial( "{$actionCleanClass}/view", Array(
					'zone' => $model,
				));
			}
			catch( Exception $e ) {
				echo $e->getMessage();
			}
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			if( @$_GET[ 'format' ] == 'html' ) {
				echo $content;
			}elseif( @$_GET[ 'insertToId' ] ){
				$json = json_encode( $content );
				echo "(function(){
					var el = document.getElementById('{$_GET[ 'insertToId' ]}'); 
					el.innerHTML = {$json}; 
					var scripts = el.getElementsByTagName('script'); 

					for (var i = 0; i < scripts.length; i++) { 
						var parentDiv = scripts[i].parentNode;
						var markerEl = document.createElement('div');
						var dateId = new Date(); var markerElId = dateId.getTime();
						markerEl.id = markerElId;
						parentDiv.insertBefore(markerEl, scripts[i]);
								
						var scriptAtts = scripts[i].attributes;
						var scriptForReplace = document.createElement('script');
								
						for (var j = 0; j < scriptAtts.length; j++){
							var attVal = scriptAtts[j].value;
							if( attVal == '' ) attVal = true;
							scriptForReplace.setAttribute(scriptAtts[j].nodeName,attVal);
						}
						if( scripts[i].innerHTML != '' ){ scriptForReplace.innerHTML = scripts[i].innerHTML; }
								
						parentDiv.removeChild(scripts[i]);
						parentDiv.insertBefore(scriptForReplace, markerEl);
						parentDiv.removeChild( document.getElementById(markerElId) );	
					} 
				})(window, document )";
			}else{
				$json = json_encode( $content );
				echo "document.write( {$json} )";
			}
		}
	}

?>