<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class DownloadAction extends ActionFrontBase {
		private $model;
		private function getModel( $id ) {
			$model = EAVersionModel::model()->find( Array(
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) $this->throwI18NException( "Can't find Version!" );
			return $model;
		}
		function run( $id ) {
			CommonLib::disableAppProfileLog();

			$this->model = $this->getModel( $id );
			$this->model->downloaded();
      EAVersionDownloadModel::downloaded($id, (!Yii::App()->user->isGuest ? Yii::App()->user->id : 0));
			
			$fileName = basename( $this->model->nameFile );
			header( "Content-type: text/plain" );
			header( "Content-Disposition: attachment; filename=\"{$fileName}\"" );
			
			echo file_get_contents( $this->model->pathFile );
		}
	}

?>