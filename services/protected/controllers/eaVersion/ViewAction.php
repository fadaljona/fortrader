<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ViewAction extends ActionFrontBase {
		private $model;
		private function getModel( $id ) {
			$model = EAVersionModel::model()->find( Array(
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		function run( $id ) {
			CommonLib::disableAppProfileLog();

			$this->model = $this->getModel( $id );
			//$this->model->downloaded();
			
			header( "Content-Type: text/plain; charset=utf-8" );
			
			$content = file_get_contents( $this->model->pathFile );
			$content = @iconv( "windows-1251", "UTF-8", $content );
			
			echo $content;
		}
	}

?>