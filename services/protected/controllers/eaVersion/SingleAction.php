<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		public $level = 2;
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array();
			
			if( substr_count( $this->model->EA->params, 'LAB' )) {
				$this->controller->commonBag->breadcrumbs[] = (object)Array( 
					'label' => 'EA laboratory',
					'url' => Array( '/ea/laboratory' ),
				);
			}
			else{
				$this->controller->commonBag->breadcrumbs[] = (object)Array( 
					'label' => 'EA Catalog',
					'url' => Array( '/ea/list' ),
				);
			}
			
			$this->controller->commonBag->breadcrumbs[] = (object)Array( 
				'label' => $this->model->EA->name,
				'url' => $this->model->EA->getSingleURL(),
			);
			
			
			$this->controller->commonBag->breadcrumbs[] = (object)Array( 
				'label' => Yii::t( '*', 'Version' )." ".$this->model->version,
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			
			/*if( substr_count( $this->model->EA->params, 'LAB' )) {
				$before = $navlist->findItemById( "iEALaboratory" );
			}
			else{*/
				$before = $navlist->findItemById( "iEARating" );
			//}
			
			$item = $navlist->createItemAfter( $before );
			$item->setLabel( "{$this->model->EA->name}<br>".Yii::t( '*', 'Version' )." {$this->model->version}" );
			$item->setActive();
		}
		private function getModel( $id ) {
			$model = EAVersionModel::model()->find( Array(
				'with' => Array(
					'EA' => Array(),
					'user' => Array(
						'select' => 'ID, user_login',
					),
					'i18ns' => Array(),
					'currentLanguageI18N' => Array(),
				),
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		function run( $id ) {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $id );
			$this->model->viewed();
			
			$this->setLayoutTitle( $this->model->EA->name.". ".Yii::t( '*', 'Version' )." ".$this->model->version );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'version' => $this->model,
			));
		}
	}

?>