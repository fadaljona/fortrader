<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.EAVersionFormModel' );
	
	final class IframeAddAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new EAVersionFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Version!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		private function checkAccess() {
			$AR = $this->formModel->getAR();
			if( $AR->isNewRecord ) return;
			if( $AR->idUser != Yii::App()->user->id ) {
				$this->throwI18NException( "Access denied!" );
			}
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "cleanPage" );
			
			$data = Array();
			try{
				$this->detFormModel();
				$this->checkAccess();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'data' => $data,
			));
		}
	}

?>