<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.EAVersionFormModel' );

	final class EditAction extends ActionFrontBase {
		private $modelEA;
		private $formModel;
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'EA Catalog',
					'url' => Array( '/ea/list' ),
				),
				(object)Array( 
					'label' => $this->modelEA->name,
					'url' => $this->modelEA->getSingleURL(),
				),
				(object)Array( 
					'label' => Yii::t( '*', 'Editing version' ),
				)
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			
			$item1 = $navlist->createItemAfter( $navlist->findItemById( "iEARating" ));
			$item1->setLabel( $this->modelEA->name );
			$item1->setStrictURL( $this->modelEA->getSingleURL() );
						
			$item2 = $navlist->createItemAfter( $item1 );
			$item2->setLabel( "Editing version" );
			$item2->setActive();
			//$this->controller->commonBag->subitemForEA = Array(
			//	(object)Array( 'label' => "Editing version", 'controller' => 'eaVersion', 'action' => 'edit' ),
			//	(object)Array( 'label' => $this->modelEA->name, 'controller' => 'ea', 'action' => 'single' ),
			//);
		}
		private function getModelEA() {
			$model = EAModel::model()->find( Array(
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $this->formModel->idEA,
				),
			));
			if( !$model ) $this->throwI18NException( "Can't find EA!" );
			return $model;
		}
		private function detFormModel( $id ) {
			$this->formModel = new EAVersionFormModel();
			$load = $this->formModel->load( $id );
			if( !$load ) $this->throwI18NException( "Can't find Version!" );
		}
		private function checkAccess() {
			$AR = $this->formModel->getAR();
			if( $AR->idUser != Yii::App()->user->id and !Yii::App()->user->checkAccess( 'eaControl' )) {
				$this->throwI18NException( "Access denied!" );
			}
		}
		function run( $id ) {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$this->detFormModel( $id );
			$this->checkAccess();
			$actionCleanClass = $this->getCleanClassName();
			$this->modelEA = $this->getModelEA();
								
			$this->setLayoutTitle( 'Editing version' );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'EA' => $this->modelEA,
				'formModel' => $this->formModel,
			));
		}
	}

?>