<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.EAVersionFormModel' );

	final class AddAction extends ActionFrontBase {
		private $modelEA;
		private $formModel;
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'EA Catalog',
					'url' => Array( '/ea/list' ),
				),
				(object)Array( 
					'label' => $this->modelEA->name,
					'url' => $this->modelEA->getSingleURL(),
				),
				(object)Array( 
					'label' => Yii::t( '*', 'Adding version' ),
				)
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			
			$item1 = $navlist->createItemAfter( $navlist->findItemById( "iEARating" ));
			$item1->setLabel( $this->modelEA->name );
			$item1->setStrictURL( $this->modelEA->getSingleURL() );
						
			$item2 = $navlist->createItemAfter( $item1 );
			$item2->setLabel( "Adding version" );
			$item2->setActive();
		}
		private function getModelEA( $id ) {
			$model = EAModel::model()->find( Array(
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) $this->throwI18NException( "Can't find EA!" );
			return $model;
		}
		private function detFormModel() {
			$this->formModel = new EAVersionFormModel();
			$this->formModel->idEA = $this->modelEA->id;
			//$load = $this->formModel->load();
			//if( !$load ) $this->throwI18NException( "Can't find Version!" );
		}
		function run( $idEA ) {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			$this->modelEA = $this->getModelEA( $idEA );
			
			$this->detFormModel();
					
			$this->setLayoutTitle( 'Adding version' );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'EA' => $this->modelEA,
				'formModel' => $this->formModel,
			));
		}
	}

?>