<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxSaveImageAction extends ActionFrontBase {
	
		function run() {

			if( !isset( $_POST['img'] ) || !isset( $_POST['id'] ) ) return true;
			$model = CurrencyRatesModel::model()->findByPk( $_POST['id'] );
			if( !$model ) return true;
			
			$model->saveBase64Img( $_POST['img'] );
			
		}
	}

?>