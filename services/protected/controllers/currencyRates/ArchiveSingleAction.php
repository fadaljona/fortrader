<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ArchiveSingleAction extends ActionFrontBase {
		private $model;
		private $cat = 'singleCurrencyRates';
		private $paramStr;

		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		public $settings;
		public $breadcrumbTitle;
		
		private $shortDesc;
		private $fullDesc;
		
		private $dataType = 'cbr';
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs() {
			if( $this->dataType == 'cbr' ){
				$this->controller->commonBag->breadcrumbs = Array(
					(object)Array( 
						'label' => 'Currency Rates',
						'url' => Array( '/currencyRates/index' ),
					),
					(object)Array( 
						'url' => Array( '/currencyRates/archive' ),
						'label' => $this->breadcrumbTitle,
					),
					(object)Array( 

						'label' => $this->title,
					)
				);
			}elseif( $this->dataType == 'ecb' ){
				$this->controller->commonBag->breadcrumbs = Array(
					(object)Array( 
						'label' => 'Ecb Currency Rates',
						'url' => Array( '/currencyRates/ecbIndex' ),
					),
					(object)Array( 
						'url' => Array( '/currencyRates/ecbArchive' ),
						'label' => $this->breadcrumbTitle,
					),
					(object)Array( 

						'label' => $this->title,
					)
				);
			}
			
		}
		private function getModel( $slug ) {
			if( $this->dataType == 'ecb' ) $slug = substr( $slug, 3 );
			$model = CurrencyRatesModel::findBySlugWithLang( $slug );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			$model->dataType = $this->dataType;
			return $model;
		}
		private function setMeta( $year, $mo, $day ) {
			
			$this->settings = CurrencyRatesSettingsModel::getModel();
			$this->breadcrumbTitle = $this->settings->getArchiveTitle($this->dataType) ? $this->settings->getArchiveTitle($this->dataType) : 'Currency Rates Archive';
			
			if( $year && !$mo && !$day ){
				$dateStr = $year . '-01-01';
				$metaTitle = $this->model->getSingleArchiveYearMetaTitle( $dateStr, $this->dataType );
				$this->metaDesc = $this->model->getSingleArchiveYearMetaDesc( $dateStr, $this->dataType );
				$this->metaKeys = $this->model->getSingleArchiveYearMetaKeys( $dateStr, $this->dataType );
				if( $metaTitle ){
					$this->metaTitle = $metaTitle;
				} else{
					$this->metaTitle = $this->model->name;
				}
				$this->title = $this->model->getSingleArchiveYearTitle( $dateStr, $this->dataType );
				if( !$this->title ) $this->title = $this->model->name;
				if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
				$this->shortDesc = $this->model->getSingleArchiveYearDescription( $dateStr, $this->dataType );
				$this->fullDesc = $this->model->getSingleArchiveYearFullDesc( $dateStr, $this->dataType );
			}elseif( $year && $mo && !$day ){
				$dateStr = $year . "-$mo-01";
				$metaTitle = $this->model->getSingleArchiveYearMoMetaTitle( $dateStr, $this->dataType );
				$this->metaDesc = $this->model->getSingleArchiveYearMoMetaDesc( $dateStr, $this->dataType );
				$this->metaKeys = $this->model->getSingleArchiveYearMoMetaKeys( $dateStr, $this->dataType );
				if( $metaTitle ){
					$this->metaTitle = $metaTitle;
				} else{
					$this->metaTitle = $this->model->name;
				}
				$this->title = $this->model->getSingleArchiveYearMoTitle( $dateStr, $this->dataType );
				if( !$this->title ) $this->title = $this->model->name;
				if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
				$this->shortDesc = $this->model->getSingleArchiveYearMoDescription( $dateStr, $this->dataType );
				$this->fullDesc = $this->model->getSingleArchiveYearMoFullDesc( $dateStr, $this->dataType );
			}else{
				$dateStr = $year . "-$mo-$day";
				$metaTitle = $this->model->getSingleArchiveYearMoDayMetaTitle( $dateStr, $this->dataType );
				$this->metaDesc = $this->model->getSingleArchiveYearMoDayMetaDesc( $dateStr, $this->dataType );
				$this->metaKeys = $this->model->getSingleArchiveYearMoDayMetaKeys( $dateStr, $this->dataType );
				if( $metaTitle ){
					$this->metaTitle = $metaTitle;
				} else{
					$this->metaTitle = $this->model->name;
				}
				$this->title = $this->model->getSingleArchiveYearMoDayTitle( $dateStr, $this->dataType );
				if( !$this->title ) $this->title = $this->model->name;
				if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
				$this->shortDesc = $this->model->getSingleArchiveYearMoDayDescription( $dateStr, $this->dataType );
				$this->fullDesc = $this->model->getSingleArchiveYearMoDayFullDesc( $dateStr, $this->dataType );
			}

			if( $this->dataType == 'cbr' ){
				$this->setLayoutTitle( $this->metaTitle, "{title}" );
			}elseif( $this->dataType == 'ecb' ){
				$this->setLayoutTitle( $this->metaTitle, "{title}" );
			}
			
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $this->model->ogImage );
		}

		function run( $year, $mo = false, $day = false, $slug, $type = 'cbr' ) {
			if( !in_array( $type, array( 'cbr', 'ecb' ) ) ) throw new CHttpException(404,'Page Not Found');
			$this->dataType = $type;
			
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $slug );	
			if( !$this->model->hasDataByYearMoDay( $year, $mo, $day, $this->dataType ) ) throw new CHttpException(404,'Page Not Found');
			$this->setLangSwitcher($this->model);
			
			$this->paramStr = $this->dataType . "currency_{$this->model->id}";
			$this->setMeta( $year, $mo, $day );
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();

			$this->cat = $this->dataType . $this->cat;
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'shortDesc' => $this->shortDesc,
				'fullDesc' => $this->fullDesc,
				'year' => intval($year),
				'mo' => intval($mo),
				'day' => intval($day),
				'metaDesc' => $this->metaDesc,
			));
		}
	}

?>