<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadChartDataAction extends ActionFrontBase {
		function run($id, $period, $type) {
			$data = Array();
			try{
				if( $type == 'cbr' ) $histData = CurrencyRatesHistoryModel::getDataForPeriod( $period, $id );
				if( $type == 'ecb' ) $histData = CurrencyRatesHistoryEcbModel::getDataForPeriod( $period, $id );
				
				if( !$histData ) $this->throwI18NException( "No data!" );
				
				$data[ 'data' ] = $histData;
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>