<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxSearchAction extends ActionAdminBase {
	
		function run() {
			$data = Array('result' => '');
			
			if( !isset($_POST['search']) ) return false;
			$searchTerm = $_POST['search'];
			
			if( !isset($_POST['type']) ) return false;
			$type = $_POST['type'];
			
			if( $type == 'cbr' ){
				$whereType = ' AND `t`.`noData` = 0 ';
			}elseif( $type == 'ecb' ){
				$whereType = ' AND `t`.`ecb` = 1 ';
			}else{
				return false;
			}

			$models = CurrencyRatesModel::model()->findAll(Array(
				'with' => array( 'i18ns' ),
				'condition' => " ( `i18nsCurrencyRates`.`name` LIKE :searchTerm OR `i18nsCurrencyRates`.`title` LIKE :searchTerm OR `t`.`code` LIKE :searchTerm ) AND `i18nsCurrencyRates`.`idLanguage` = :idLanguage " . $whereType,
				'params' => array( ':searchTerm' => '%'.$searchTerm.'%', ':idLanguage' => LanguageModel::getCurrentLanguageID() ),
			));
			
			if( $models ){
				foreach( $models as $model ){
					$model->dataType = $type;
					
					if( $model->name ){
						$name = $model->name . ' ' . CHtml::tag('span', array('class' => 'blue_color alignright reg'), $model->code);
					}else{
						$name = $model->code;
					}

					$data['result'] .= CHtml::tag( 
						"li", 
						Array( 'class' => '' ), 
						CHtml::link( 
							$name, 
							$model->singleURL,
							array( 'target' => '_blank' )
						)
					);
				}
			}else{
				$data['result'] = CHtml::tag( "li", Array( 'class' => '' ), Yii::t( '*', 'No results found' ) );
			}
			echo json_encode( $data );
		}
	}
?>