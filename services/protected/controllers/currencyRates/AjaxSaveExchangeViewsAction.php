<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxSaveExchangeViewsAction extends ActionFrontBase {
		function run() {
			if( !isset($_POST['id']) || !$_POST['id'] ) return false;
			$id = intval($_POST['id']);
			if( !$id ) return false;
			if( !CurrencyRatesConvertModel::model()->findByPk( $id ) ) return false;
			CurrencyRatesConvertViewsModel::saveViews( $id );
		}
	}
?>