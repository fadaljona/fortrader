<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ExchangePageAction extends ActionFrontBase {
		private $model;
		private $cat = 'singleCurrencyRatesConvert';
		private $paramStr;

		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs($type) {
			if( $type == 'cbr' ){
				$label = 'Currency Rates Converter';
			}elseif( $type == 'ecb' ){
				$label = 'Ecb Currency Rates Converter';
			}
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => $label,
					'url' => Array( '/currencyRates/converterEcbPage' ),
				),
				(object)Array( 

					'label' => $this->title,
				)
			);
		}
		private function getModel( $value, $currencyFrom, $currencyTo, $type ) {
			$model = CurrencyRatesConvertModel::findByParams( $value, $currencyFrom, $currencyTo, $type );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta( $type ) {
            $this->title = $this->model->title;
			$this->metaTitle = $this->model->metaTitle;

			$this->metaDesc = $this->model->metaDesc;
			$this->metaKeys = $this->model->metaKeys;
			
			if( !$this->metaKeys ) $this->metaKeys = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			if( $type == 'cbr' ){
				$this->setLayoutTitle( $this->metaTitle, "{title}" );
			}elseif( $type == 'ecb' ){
				$this->setLayoutTitle( $this->metaTitle, "{title}" );
			}
			
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			/*$this->setLayoutOgSection();
			$this->setLayoutOgImage( $this->model->ogImage );*/
			$this->setLangSwitcher(true);
		}
		

		function run( $value, $currencyFrom, $currencyTo, $type = 'cbr' ) {	
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $value, $currencyFrom, $currencyTo, $type );	
			
			$this->paramStr = "currencyConvert_{$this->model->idPair}";
			$this->setMeta( $type );

			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs( $type );

			if( $type == 'cbr' ){
				$this->cat = 'singleCurrencyRatesConvert';
			}elseif( $type == 'ecb' ){
				$this->cat = 'singleEcbCurrencyRatesConvert';
            }
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>