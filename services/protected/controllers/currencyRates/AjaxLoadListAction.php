<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadListAction extends ActionFrontBase {
	
		private function renderList( $type, $dataType, $toCurrency ) {
			ob_start();
			
			$this->controller->widget( 'widgets.lists.CurrencyRatesListListWidget', Array(
				'type' => $type,
				'dataType' => $dataType,
				'toCurrency' => $toCurrency
			));
			
			return ob_get_clean();
		}
		function run( $type, $dataType, $toCurrency ) {
			
			if( !in_array( $type, array('today', 'tomorrow') ) || !in_array( $dataType, array('cbr', 'ecb') ) ) return false;
			$toModel = CurrencyRatesModel::model()->findByPk( $toCurrency );
			if( !$toModel ) return false;
			
			$data = Array();
			try{
				$data[ 'content' ] = $this->renderList( $type, $dataType, $toModel );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			
			echo json_encode( $data );
		}
	}

?>