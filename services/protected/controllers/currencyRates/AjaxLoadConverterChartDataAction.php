<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadConverterChartDataAction extends ActionFrontBase {
		function run( $period, $id ) {
			$data = Array();
			try{
				if( !CommonLib::isNum( $id ) || !CommonLib::isNum( $period ) ) $this->throwI18NException( "Wrong params!" );
				$model = CurrencyRatesConvertModel::model()->findByPk( $id );
				if( !$model ) $this->throwI18NException( "Wrong params!" );
				
				if( $model->type == 'cbr' ){
					$histData = CurrencyRatesHistoryModel::getDataForConverterChart( $period, $id );
				}elseif( $model->type == 'ecb' ){
					$histData = CurrencyRatesHistoryEcbModel::getDataForConverterChart( $period, $id );
				}
				
				
				if( !$histData ) $this->throwI18NException( "No data!" );
				
				$data[ 'data' ] = $histData;
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>