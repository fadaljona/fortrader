<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class TomorrowAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Currency Rates for tomorrow';
		public $metaDesc;
		public $metaKeys;
		public $settings;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Currency Rates',
					'url' => Array( '/currencyRates/index' ),
				),
				(object)Array( 

					'label' => 'Currency Rates for tomorrow',
				)
			);
		}
		private function setMeta() {
			$this->settings = CurrencyRatesSettingsModel::getModel();
			$metaTitle = $this->settings->tomorrowMetaTitle;

			$this->metaDesc = $this->settings->tomorrowMetaDesc;
			$this->metaKeys = $this->settings->tomorrowMetaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->tomorrowTitle;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->tomorrowOgListImage;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
			$this->setLangSwitcher(true);
		}
		function run() {
			$this->setMeta();
			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>