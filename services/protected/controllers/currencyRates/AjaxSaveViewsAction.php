<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxSaveViewsAction extends ActionFrontBase {
		function run() {
			if( !isset($_POST['id']) || !$_POST['id'] ) return false;
			CurrencyRatesViewsModel::saveViews( $_POST['id'] );
		}
	}
?>