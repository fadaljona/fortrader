<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class EcbArchiveAction extends ActionFrontBase {
		private $cat = 'archiveCurrencyRatesEcb';
		private $paramStr;

		public $title;
		public $metaTitle = 'Ecb Currency Rates Archive';
		public $metaDesc;
		public $metaKeys;
		
		public $breadcrumbTitle;
		
		public $settings;
		
		private $shortDesc;
		private $fullDesc;
		
		static function siteMapArgs(){
			return true;
		}
	
		
		private function setBreadcrumbs( $year, $mo, $day ) {
			
			if( !$year && !$mo && !$day ){
				$this->controller->commonBag->breadcrumbs = Array(
					(object)Array( 
						'label' => 'Ecb Currency Rates',
						'url' => Array( '/currencyRates/ecbIndex' ),
					),
					(object)Array( 

						'label' => $this->breadcrumbTitle,
					)
				);
			}else{
				$this->controller->commonBag->breadcrumbs = Array(
					(object)Array( 
						'label' => 'Ecb Currency Rates',
						'url' => Array( '/currencyRates/ecbIndex' ),
					),
					(object)Array( 
						'url' => Array( '/currencyRates/ecbArchive' ),
						'label' => $this->breadcrumbTitle,
					),
					(object)Array( 
						'label' => $this->title,
					)
				);
			}
			
		}
		private function setMeta( $year, $mo, $day ) {
			
			$this->settings = CurrencyRatesSettingsModel::getModel();
			
			if( !$year && !$mo && !$day ){
				$metaTitle = $this->settings->getArchiveMetaTitle('ecb');
				$this->metaDesc = $this->settings->getArchiveMetaDesc('ecb');
				$this->metaKeys = $this->settings->getArchiveMetaKeys('ecb');
				if( $metaTitle ){
					$this->metaTitle = $metaTitle;
				} else{
					$this->metaTitle = Yii::t( '*', $this->metaTitle );
				}
				if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
				$this->title = $this->settings->getArchiveTitle('ecb');
				$this->shortDesc = $this->settings->getArchiveDescription('ecb');
				$this->fullDesc = $this->settings->getArchiveFullDesc('ecb');
			}else{
				if( $year && !$mo && !$day ){
					$dateStr = $year . '-01-01';
					$metaTitle = $this->settings->getArchiveYearMetaTitle($dateStr, 'ecb');
					$this->metaDesc = $this->settings->getArchiveYearMetaDesc($dateStr, 'ecb');
					$this->metaKeys = $this->settings->getArchiveYearMetaKeys($dateStr, 'ecb');
					if( $metaTitle ){
						$this->metaTitle = $metaTitle;
					} else{
						$this->metaTitle = Yii::t( '*', $this->metaTitle );
					}
					if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
					$this->title = $this->settings->getArchiveYearTitle($dateStr, 'ecb');
					$this->shortDesc = $this->settings->getArchiveYearDescription($dateStr, 'ecb');
					$this->fullDesc = $this->settings->getArchiveYearFullDesc($dateStr, 'ecb');
				}elseif( $year && $mo && !$day ){
					$dateStr = $year . "-$mo-01";
					$metaTitle = $this->settings->getArchiveYearMoMetaTitle($dateStr, 'ecb');
					$this->metaDesc = $this->settings->getArchiveYearMoMetaDesc($dateStr, 'ecb');
					$this->metaKeys = $this->settings->getArchiveYearMoMetaKeys($dateStr, 'ecb');
					if( $metaTitle ){
						$this->metaTitle = $metaTitle;
					} else{
						$this->metaTitle = Yii::t( '*', $this->metaTitle );
					}
					if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
					$this->title = $this->settings->getArchiveYearMoTitle($dateStr, 'ecb');
					$this->shortDesc = $this->settings->getArchiveYearMoDescription($dateStr, 'ecb');
					$this->fullDesc = $this->settings->getArchiveYearMoFullDesc($dateStr, 'ecb');
				}else{
					$dateStr = $year . "-$mo-$day";
					$metaTitle = $this->settings->getArchiveYearMoDayMetaTitle($dateStr, 'ecb');
					$this->metaDesc = $this->settings->getArchiveYearMoDayMetaDesc($dateStr, 'ecb');
					$this->metaKeys = $this->settings->getArchiveYearMoDayMetaKeys($dateStr, 'ecb');
					if( $metaTitle ){
						$this->metaTitle = $metaTitle;
					} else{
						$this->metaTitle = Yii::t( '*', $this->metaTitle );
					}
					if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
					$this->title = $this->settings->getArchiveYearMoDayTitle($dateStr, 'ecb');
					$this->shortDesc = $this->settings->getArchiveYearMoDayDescription($dateStr, 'ecb');
					$this->fullDesc = $this->settings->getArchiveYearMoDayFullDesc($dateStr, 'ecb');
				}
			}
			
			if(!$this->title) $this->title = $this->metaTitle;
			
			$this->breadcrumbTitle = $this->settings->getArchiveTitle('ecb') ? $this->settings->getArchiveTitle('ecb') : 'Ecb Currency Rates Archive';

			$this->setLayoutTitle( $this->metaTitle, "{title}" );
			
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->archiveOgImage;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
			$this->setLangSwitcher(true);
		}

		function run( $year = false, $mo = false, $day = false, $type = 'ecb' ) {	
			$actionCleanClass = $this->getCleanClassName();
			$this->paramStr = "ecb_currency_archive";
			
			$this->setMeta( $year, $mo, $day );
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs( $year, $mo, $day );

						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
				'year' => intval($year),
				'mo' => intval($mo),
				'day' => intval($day),
				'shortDesc' => $this->shortDesc,
				'fullDesc' => $this->fullDesc,
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>