<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadArchiveListDataAction extends ActionFrontBase {
		function run( ) {
			$data = Array();
			try{
				$year = @$_GET['year'];
				if( !$year ) return false;
				$mo = @$_GET['mo'];
				$day = @$_GET['day'];
				
				if( !CurrencyRatesModel::hasDataByYearMo( $year, $mo ) && $year != date('Y') ) $this->throwI18NException( "No data!" );
				
				if( !$mo ){
					$months = array();
					$moFromDb = CurrencyRatesModel::getAvailibleMosFromDb( $year );
					$allMonths = CurrencyRatesModel::getMonthsAvailableByYear( $year );
					foreach( $moFromDb as $month ){
						$months[$month] = $allMonths[$month];
					}
					
					
					$data['months'] = '';
					foreach( $months as $num => $name ){
						$data['months'] .= Chtml::tag(
							'li',
							array( 'class' => "choose_date_btm_item", 'data-mo' => $num ),
							Chtml::link( $name, 'javascript:;')
						);
					}
				}else{
					$firstDay = CurrencyRatesModel::getAvailibleDaysFromDb( $year, $mo );
					$lastDay = CurrencyRatesModel::getLastDayAvailableByYearMo( $year, $mo );

					$data['days'] = '';
					for( $i=$firstDay; $i<=$lastDay; $i++ ){
						$data['days'] .= Chtml::tag(
							'li',
							array( 'class' => "choose_date_btm_item", 'data-day' => $i ),
							Chtml::link( $i < 10 ? '0'.$i : $i, 'javascript:;')
						);
					}
				}
				
				ob_start();
				$this->controller->widget( 'widgets.lists.CurrencyRatesArchiveListWidget', Array(
					'ns' => 'nsActionView',
					'year' => $year,
					'mo' => $mo,
					'day' => $day
				));
				$data['list'] = ob_get_clean();
				$data['list'] = preg_replace( "#[\r\n\t]+#", " ", $data['list'] );
				
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>