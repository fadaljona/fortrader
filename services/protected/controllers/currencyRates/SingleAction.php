<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		private $cat = 'singleCurrencyRates';
		private $paramStr;

		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs() {
			if( isset($this->model->name) && $this->model->name )
				$label = $this->model->name;
			else
				$label = $this->model->code;
			
			if( $this->model->dataType == 'cbr' ){
				$breadcrumb = Array( 
					'label' => 'Currency Rates',
					'url' => Array( '/currencyRates/index' ),
				);
			}elseif( $this->model->dataType == 'ecb' ){
				$breadcrumb = Array( 
					'label' => 'Ecb Currency Rates',
					'url' => Array( '/currencyRates/ecbIndex' ),
				);
				$label = Yii::t('*', 'EUR to' ) .' '. $this->model->code;
			}
			
			$this->controller->commonBag->breadcrumbs = Array(
				(object)$breadcrumb,
				(object)Array( 

					'label' => $label,
				)
			);
		}
		private function getModel( $slug, $type ) {
			if( $type == 'ecb' ) $slug = substr( $slug, 3 );
			$model = CurrencyRatesModel::findBySlugWithLang( $slug );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta($type) {
			
			$metaTitle = $this->model->metaTitle;

			$this->metaDesc = $this->model->metaDesc;
			$this->metaKeys = $this->model->metaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = $this->model->name;
			}
			
			$this->title = $this->model->title;
			if( !$this->title ) $this->title = $this->model->name;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			if( $this->model->dataType == 'cbr' ){
				$this->setLayoutTitle( $this->metaTitle, "{title}" );
			}elseif( $this->model->dataType == 'ecb' ){
				$this->setLayoutTitle( $this->metaTitle, "{title}" );
			}
			
			if( $type == 'ecb' ) $this->title = '1 EUR = ' . number_format($this->model->todayEcbData->value, CommonLib::getDecimalPlaces( $this->model->todayEcbData->value, 4 ), '.', '') . ' ' . $this->model->code;
			
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $this->model->ogImage );
		}

		function run( $slug, $type = 'cbr' ) {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->model = $this->getModel( $slug, $type );	
			$this->model->dataType = $type;
			if( ! $this->model->checkTypeAndSettings() ) throw new CHttpException(404,'Page Not Found');
			
			$this->paramStr = "currency_{$this->model->id}";
			$this->setMeta($type);
			$this->setLangSwitcher($this->model);
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();

			$this->cat = $type . '.' . $this->cat;			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>