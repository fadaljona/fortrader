<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.CurrencyRatesConvertFormModel' );
	Yii::import( 'models.CurrencyRatesConvertHistoryModel' );
	
	final class AjaxConvertRequestAction extends ActionFrontBase {
		private $startsInMinute = 10;
		
		private $formModel;

		private function detFormModel( $idCurrencyFrom, $idCurrencyTo, $value, $type ) {
			$this->formModel = new CurrencyRatesConvertFormModel;
			$load = $this->formModel->loadByParams( $idCurrencyFrom, $idCurrencyTo, $value, $type );
			if( !$load ) $this->throwI18NException( "Can't find model!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
	
		function run() {
			
			
			if( !Yii::app()->request->isAjaxRequest ) return false;
			if( !Yii::app()->session['canCreateConverterPage'] ) return false;
			
			if( isset( Yii::app()->session['converterCount'] ) ){
				if( time() - Yii::app()->session['converterStartTime'] < 60 ){
					if( Yii::app()->session['converterCount'] < $this->startsInMinute ){
						Yii::app()->session['converterCount'] += 1;
					}else{
						Yii::app()->session['canCreateConverterPage'] = false;
						return false;
					}
				}else{
					Yii::app()->session['converterCount'] = 1;
					Yii::app()->session['converterStartTime'] = time();
				}
			}else{
				Yii::app()->session['converterCount'] = 1;
				Yii::app()->session['converterStartTime'] = time();
			}
			
			if( !isset($_POST['fromCur']) || !isset($_POST['toCur']) || !isset($_POST['fromVal']) || !isset($_POST['type']) || !$_POST['fromCur'] || !$_POST['toCur'] || !$_POST['type'] ) return false;
		
			$fromCur = intval($_POST['fromCur']);
			$toCur = intval($_POST['toCur']);
			$fromVal = floatval($_POST['fromVal']);
			
			if( ($fromCur == $toCur) || $fromCur < 0 || $toCur < 0 ) return false;
			if( $fromVal <= 0 ) return false;

		
			$data = Array();
			try{
				$this->detFormModel($fromCur, $toCur, $fromVal, $_POST['type']);
				$formModel = $this->getFormModel();
				if( !$formModel->id ){
					if( $formModel->validate()) {
						$id = $formModel->save();
						if( !$id ) $this->throwI18NException( "Can't save AR!" );
						$data[ 'id' ] = $id;
					}else{
						list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
					}
				}else{
					$data[ 'id' ] = $formModel->id;
					$data[ 'error' ] = "Model exist";
				}
				if( isset( $data[ 'id' ] ) ){
					CurrencyRatesConvertHistoryModel::saveInHistory( $data[ 'id' ] );
					CurrencyRatesConvertModel::sendPushData( $data[ 'id' ] );
				}

				
			}catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			//echo json_encode( $data );
		}
	}

?>