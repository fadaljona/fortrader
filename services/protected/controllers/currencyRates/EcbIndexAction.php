<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class EcbIndexAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Ecb Currency Rates';
		public $metaDesc;
		public $metaKeys;
		public $settings;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Ecb Currency Rates',
				)
			);
		}
		private function setMeta() {
			$this->settings = CurrencyRatesSettingsModel::getModel();
			$metaTitle = $this->settings->ecbMetaTitle;

			$this->metaDesc = $this->settings->ecbMetaDesc;
			$this->metaKeys = $this->settings->ecbMetaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->ecbTitle;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->ogListImage;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
			$this->setLangSwitcher(true);
		}
		function run() {
			
			$this->setMeta();
			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>