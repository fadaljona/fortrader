<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ConverterPageAction extends ActionFrontBase {
		//private $cat = 'converterCurrencyRates';
		//private $paramStr;

		public $title;
		public $metaTitle = 'Currency Rates Converter';
		public $metaDesc;
		public $metaKeys;
		
		public $settings;
		
		
		private function setBreadcrumbs() {
			
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Currency Rates Converter',
				)
			);
		}
		private function setMeta() {
			$this->settings = CurrencyRatesSettingsModel::getModel();
			$metaTitle = $this->settings->converterMetaTitle;

			$this->metaDesc = $this->settings->converterMetaDesc;
			$this->metaKeys = $this->settings->converterMetaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->converterTitle;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->converterOgImage;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
			$this->setLangSwitcher(true);
		}

		function run( ) {	
			$actionCleanClass = $this->getCleanClassName();
			//$this->paramStr = "currency_converter";
			
			$this->setMeta();
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();

						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>