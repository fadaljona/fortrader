<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadChartDataForArchiveAction extends ActionFrontBase {
		function run( $radius, $id, $left, $right, $type ) {
			if( !CurrencyRatesModel::checkDataType($type) ) return false;
			$data = Array();
			try{
				$histData = CurrencyRatesModel::getDataForRadius( $radius, $id, $left, $right, $type );
				
				if( !$histData ) $this->throwI18NException( "No data!" );
				
				$data[ 'data' ] = $histData;
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>