<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ArchiveAction extends ActionFrontBase {
		private $cat = 'archiveCurrencyRates';
		private $paramStr;

		public $title;
		public $metaTitle = 'Currency Rates Archive';
		public $metaDesc;
		public $metaKeys;
		
		public $breadcrumbTitle;
		
		public $settings;
		
		private $shortDesc;
		private $fullDesc;
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs( $year, $mo, $day ) {
			
			if( !$year && !$mo && !$day ){
				$this->controller->commonBag->breadcrumbs = Array(
					(object)Array( 
						'label' => 'Currency Rates',
						'url' => Array( '/currencyRates/index' ),
					),
					(object)Array( 

						'label' => $this->breadcrumbTitle,
					)
				);
			}else{
				$this->controller->commonBag->breadcrumbs = Array(
					(object)Array( 
						'label' => 'Currency Rates',
						'url' => Array( '/currencyRates/index' ),
					),
					(object)Array( 
						'url' => Array( '/currencyRates/archive' ),
						'label' => $this->breadcrumbTitle,
					),
					(object)Array( 
						'label' => $this->title,
					)
				);
			}
			
		}
		private function setMeta( $year, $mo, $day ) {
			
			$this->settings = CurrencyRatesSettingsModel::getModel();
			
			if( !$year && !$mo && !$day ){
				$metaTitle = $this->settings->archiveMetaTitle;
				$this->metaDesc = $this->settings->archiveMetaDesc;
				$this->metaKeys = $this->settings->archiveMetaKeys;
				if( $metaTitle ){
					$this->metaTitle = $metaTitle;
				} else{
					$this->metaTitle = Yii::t( '*', $this->metaTitle );
				}
				if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
				$this->title = $this->settings->archiveTitle;
				$this->shortDesc = $this->settings->archiveDescription;
				$this->fullDesc = $this->settings->archiveFullDesc;
			}else{
				if( $year && !$mo && !$day ){
					$dateStr = $year . '-01-01';
					$metaTitle = $this->settings->getArchiveYearMetaTitle($dateStr);
					$this->metaDesc = $this->settings->getArchiveYearMetaDesc($dateStr);
					$this->metaKeys = $this->settings->getArchiveYearMetaKeys($dateStr);
					if( $metaTitle ){
						$this->metaTitle = $metaTitle;
					} else{
						$this->metaTitle = Yii::t( '*', $this->metaTitle );
					}
					if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
					$this->title = $this->settings->getArchiveYearTitle($dateStr);
					$this->shortDesc = $this->settings->getArchiveYearDescription($dateStr);
					$this->fullDesc = $this->settings->getArchiveYearFullDesc($dateStr);
				}elseif( $year && $mo && !$day ){
					$dateStr = $year . "-$mo-01";
					$metaTitle = $this->settings->getArchiveYearMoMetaTitle($dateStr);
					$this->metaDesc = $this->settings->getArchiveYearMoMetaDesc($dateStr);
					$this->metaKeys = $this->settings->getArchiveYearMoMetaKeys($dateStr);
					if( $metaTitle ){
						$this->metaTitle = $metaTitle;
					} else{
						$this->metaTitle = Yii::t( '*', $this->metaTitle );
					}
					if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
					$this->title = $this->settings->getArchiveYearMoTitle($dateStr);
					$this->shortDesc = $this->settings->getArchiveYearMoDescription($dateStr);
					$this->fullDesc = $this->settings->getArchiveYearMoFullDesc($dateStr);
				}else{
					$dateStr = $year . "-$mo-$day";
					$metaTitle = $this->settings->getArchiveYearMoDayMetaTitle($dateStr);
					$this->metaDesc = $this->settings->getArchiveYearMoDayMetaDesc($dateStr);
					$this->metaKeys = $this->settings->getArchiveYearMoDayMetaKeys($dateStr);
					if( $metaTitle ){
						$this->metaTitle = $metaTitle;
					} else{
						$this->metaTitle = Yii::t( '*', $this->metaTitle );
					}
					if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
					$this->title = $this->settings->getArchiveYearMoDayTitle($dateStr);
					$this->shortDesc = $this->settings->getArchiveYearMoDayDescription($dateStr);
					$this->fullDesc = $this->settings->getArchiveYearMoDayFullDesc($dateStr);
				}
			}
			
			if(!$this->title) $this->title = $this->metaTitle;
			
			
			$this->breadcrumbTitle = $this->settings->archiveTitle ? $this->settings->archiveTitle : 'Currency Rates Archive';

			
			$this->setLayoutTitle( $this->metaTitle, "{title}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->archiveOgImage;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
			$this->setLangSwitcher(true);
		}

		function run( $year = false, $mo = false, $day = false ) {	
			$actionCleanClass = $this->getCleanClassName();
			$this->paramStr = "currency_archive";
			
			$this->setMeta( $year, $mo, $day );
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs( $year, $mo, $day );

						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
				'year' => intval($year),
				'mo' => intval($mo),
				'day' => intval($day),
				'shortDesc' => $this->shortDesc,
				'fullDesc' => $this->fullDesc,
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>