<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadInterestRatesReviewsAction extends ActionFrontBase {
		private $lastDate;
		private $lastId;
		private $postIds = array();
		private $total;
		
		private function setPostIds( $num, $lastDate, $lastId ) {

			$this->total = InterestRatesValueModel::getTotalReviewsByParams( $lastDate, $lastId );
			
			$models = InterestRatesValueModel::model()->findAll(array(
				'with' => array( 'currentLanguageI18N' ),
				'condition' => ' `cLI18NInterestRatesValueModel`.`postId` <> 0 AND `cLI18NInterestRatesValueModel`.`postId` IS NOT NULL AND `t`.`modDate` <= :lastDate AND `t`.`id` <> :lastId ',
				'order' => ' `t`.`modDate` DESC, `t`.`id` ASC ',
				'limit' => $num,
				'params' => Array( ':lastDate' => $lastDate, ':lastId' => $lastId )
			));
			if( !$models ) $this->throwI18NException( "no reviews" );
			foreach( $models as $model ){
				$this->postIds[] = $model->postId;
			}
			$this->lastDate = $models[$num-1]->modDate;
			$this->lastId = $models[$num-1]->id;
		}
		
		function run( $num, $lastDate, $lastId) {
			$data = Array();

			try{
				$num = intval( $num );
				$lastId = intval( $lastId );
				if( $num < 1 ) $this->throwI18NException( "wrong param" );
				if( $lastId < 1 ) $this->throwI18NException( "wrong param" );
				if( !CommonLib::isDate($lastDate) ) $this->throwI18NException( "wrong param" );
				
				$this->setPostIds( $num, $lastDate, $lastId );
				
				ob_start();
					$postIds = $this->postIds;
					require_once Yii::App()->params['wpThemePath'].'/templates/interestRatesReviews.php'; 
				$blockContent = ob_get_clean();
				
				$data['content'] = $blockContent;
				$data['lastDate'] = $this->lastDate;
				$data['lastId'] = $this->lastId;
				$data['total'] = $this->total;
				
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>