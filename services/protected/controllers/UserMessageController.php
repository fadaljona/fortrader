<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class UserMessageController extends ControllerFrontBase {
		function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'ajaxAdd', 'ajaxDelete' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
	}

?>