<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class NewsAggregatorController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "News Aggregator";
		}
		function accessRules(){
			return Array(
				/*array(
					'deny',
					'actions' => array('category', 'index', 'sources'),
					'expression' => array('NewsAggregatorController','allowOnlyAdmin'),
					'users'=>array('*')
				),*/
			);
		}
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl'
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$url = str_replace( '/services', '', Yii::App()->request->getUrl() );
				
				$this->redirect( strtolower($url), true, 301 );
			}
			$filterChain->run();			
		}
		public static function allowOnlyAdmin(){
			if( Yii::App()->user->id == 1 ) return false;
			return true;
		}
		public function singleSiteMapArgs(){
			$outArr = array( );
	
			return $outArr;
		}
		public function categorySiteMapArgs(){
			$models = NewsAggregatorCatsModel::model()->findAll(array(
				'select' => ' `slug` ',
				'condition' => ' `enabled` = 1 '

			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = array(
					'slug' => $model->slug
				);
			}
			return $outArr;
		}
	}