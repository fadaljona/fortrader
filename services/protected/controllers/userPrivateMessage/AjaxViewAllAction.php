<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxViewAllAction extends ActionFrontBase {
		function run() {
			$data = Array();
			try{
				UserPrivateMessageModel::viewAll( Yii::App()->user->id );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>