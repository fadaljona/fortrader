<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class DialogAction extends ActionFrontBase {
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Private messages',
					'url' => Yii::App()->createURL( '/userPrivateMessage/list' ),
				),
				(object)Array( 
					'label' => 'Dialog',
				)
			);
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Dialog" );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>