<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadListAction extends ActionFrontBase {
		private $widget;
		private function getWidget( $action, $idWith, $idLastMessage ) {
			$widget = $this->controller->createWidget( 'widgets.UsersPrivateMessagesWidget', Array(
				'action' => $action,
				'idWith' => $idWith,
				'idLastMessage' => $idLastMessage,
			));
			return $widget;
		}
		private function renderMessages() {
			ob_start();
			$this->widget->renderMessages();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$action = @$_GET['action'];
				$idWith = (int)@$_GET['idWith'];
				$idLastMessage = (int)@$_GET['idLastMessage'];
				$this->widget = $this->getWidget( $action, $idWith, $idLastMessage );
				
				$data[ 'list' ] = $this->renderMessages();
				$data[ 'more' ] = $this->widget->issetMoreMessages();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>