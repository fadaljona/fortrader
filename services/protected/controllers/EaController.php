<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class EaController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "EA Catalog";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				/*Array( 'deny', 'actions' => Array( 'add', 'edit', 'iframeAdd', 'ajaxDelete', 'ajaxAddMark' ), 'users' => Array( '?' )),
				Array( 'allow' ),*/
				array(
					'deny',
					'expression' => array('EaController','allowOnlyAdmin'),
					'users'=>array('*')
				),
			);
		}
		public static function allowOnlyAdmin(){
			if( Yii::App()->user->id == 1 ) return false;
			return true;
		}
		public function filters(){
			return array(
				'accessControl',
				array(
					'PageCacheFilter',
					'duration'=> 60*60 * 24 * 30,
					'varyByParam' => 'id',
				),
			);
		}
	}

?>