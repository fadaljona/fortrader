<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class BinaryController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Binary options ratings";
		}
		/*function accessRules(){
			return Array(
				array(
					'deny',
					'actions' => array('single', 'index'),
					'expression' => array('BinaryController','allowOnlyAdmin'),
					'users'=>array('*')
				),
			);
		}*/
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl'
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$url = str_replace( '/services', '', Yii::App()->request->getUrl() );
				
				$this->redirect( $url, true, 301 );
			}
			$filterChain->run();			
		}
		public function allowOnlyAdmin(){
			if( Yii::App()->user->id == 1 ) return false;
			return true;
		}
		public function singleSiteMapArgs(){
			$models = BrokerModel::model()->findAll(array(
				'select' => ' `slug` ',
				'condition' => ' `showInRating` = 1 AND `type` = "binary" '

			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = array(
					'slug' => strtolower( $model->slug )
				);
			}
			return $outArr;
		}
	}

?>