<?php
Yii::import('controllers.base.ActionFrontBase');

final class GetInformerAction extends ActionFrontBase
{

    private function saveStats()
    {
        $referer = @$_SERVER['HTTP_REFERER'];
        if ($referer && $this->checkIfNotFortrader()) {
            InformersStatsModel::saveStats($referer, @$_GET['st'], @$_GET['cat']);
        }
    }
    private function checkIfNotFortrader()
    {
        $referer = @$_SERVER['HTTP_REFERER'];
        if (strpos($referer, Yii::App()->request->getHostInfo()) === false) {
            return true;
        }
        return false;
    }
    private function generateInformer($category)
    {
        ob_start();
            
        if ($category->type == 'monitoring') {
            $this->controller->widget('widgets.InformerWidget', array( 'ns' => 'nsActionView'));
        } else {
            $actionCleanClass = $this->getCleanClassName();
            if (!isset($_GET['lType']) || $_GET['lType'] != 'html') {
                $this->setLayout("informer/index");
                $this->controller->render("{$actionCleanClass}/view", array());
            } else {
                $this->controller->widget('widgets.InformerWidget', array( 'ns' => 'nsActionView'));
            }
        }

        return ob_get_clean();
    }
    public function run()
    {
        $this->saveStats();

        if (!isset($_GET['cat']) || !$_GET['cat']) {
            return false;
        }
        $category = InformersCategoryModel::model()->findByPk($_GET['cat']);
            
        $cacheTime = 60*5;
        if ($category->type == 'monitoring') {
            $cacheTime = 60*60*24;
            header('Content-type: image/png');
        }
            
        if ($this->checkIfNotFortrader()) {
            $params = $_GET;
            sort($params);
            $params = implode('.', $params);
                
            $params = $params . '_lang_' . Yii::app()->language;
                
            if (!Yii::app()->cache->get('new.informers.'.$params)) {
                $informer = $this->generateInformer($category);
                Yii::app()->cache->set('new.informers.'.$params, $informer, $cacheTime);
            }
            echo Yii::app()->cache->get('new.informers.'.$params);
        } else {
            echo $this->generateInformer($category);
        }
    }
}
