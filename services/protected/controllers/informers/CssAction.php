<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class CssAction extends ActionFrontBase {

		
		function run( $id ) {
			if( !$_SERVER['HTTP_REFERER'] ) return false;
		
			$params = InformersToParamsModel::getInformerParams( $id );
			if( !$params ) return false;

			header('Content-Type: text/css');
			$this->controller->widget('widgets.InformerWidget',Array( 'getStyles' => true, 'params' => $params ));
			
		}
	}

?>