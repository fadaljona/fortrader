<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class IndexAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Forex Informers';
		public $metaDesc;
		public $metaKeys;
		public $settings;
		
		private $cat = 'indexInformers';
		private $paramStr;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Forex Informers',
				)
			);
		}
		private function setMeta() {
			$this->settings = InformersSettingsModel::getModel();
			
			$metaTitle = $this->settings->metaTitle;

			$this->metaDesc = $this->settings->metaDesc;
			$this->metaKeys = $this->settings->metaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->title;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->ogImage;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
			$this->setLangSwitcher(true);
		}
		function run() {
			$this->setMeta();
			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->paramStr = "informers_index";
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
			//	'cat' => $this->cat,
			//	'paramStr' => $this->paramStr,
			//	'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>