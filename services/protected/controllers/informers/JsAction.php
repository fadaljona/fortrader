<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class JsAction extends ActionFrontBase {

		private function saveStats(){
			$referer = @$_SERVER['HTTP_REFERER'];
			if( $referer && $this->checkIfNotFortrader() ){
				InformersStatsModel::saveStats( $referer, @$_GET['st'], @$_GET['cat'], 1 );
			}
		}
		private function checkIfNotFortrader(){
			$referer = @$_SERVER['HTTP_REFERER'];
			if( strpos($referer, Yii::App()->request->getHostInfo()) === false ){
				return true;
			}
			return false;
		}
		
		function run( $id, $m ) {
			if( !$_SERVER['HTTP_REFERER'] ) return false;

			Yii::app()->request->cookies['ftWssAllow'] = new CHttpCookie('ftWssAllow', 1, array( 'expire' => time() + 60*2 ));
		
			$params = InformersToParamsModel::getInformerParams( $id );
			if( !$params ) return false;

			$_GET = $params;

			$this->saveStats();

			header('Content-Type: application/javascript');
			$this->controller->widget('widgets.InformerWidget',Array( 'getScripts' => true, 'marker' => $m, 'params' => $params ));
			
		}
	}

?>