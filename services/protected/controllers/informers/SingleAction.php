<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		private $cat = 'singleInformers';
		private $paramStr;

		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs() {
			if( isset($this->model->pageTitle) && $this->model->pageTitle )
				$label = $this->model->pageTitle;
			else
				$label = $this->model->title;
			
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Forex Informers',
					'url' => Array( '/informers/index' ),
				),
				(object)Array( 

					'label' => $label,
				)
			);
		}
		private function getModel( $slug ) {
			$model = InformersCategoryModel::findBySlugWithLang( $slug );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta() {


			$metaTitle = $this->model->metaTitle;

			$this->metaDesc = $this->model->metaDesc;
			$this->metaKeys = $this->model->metaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = $this->model->title;
			}
			
			$this->title = $this->model->pageTitle;
			if( !$this->title ) $this->title = $this->model->title;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			if( $this->model->ogImage ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $this->model->absoluteSrcOgImage );
			}
		}

		function run( $slug ) {	
		
		//	if( $slug == 'ecb' && !Yii::App()->user->checkAccess( 'informersControl' ) ) return false;
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $slug );	
			$this->paramStr = "informers_{$this->model->id}";
			$this->setMeta();
			
			$this->setLangSwitcher($this->model);
			
			$this->setLayoutBodyClass('bootstrapColorpickerPlus');
		
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();

						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'catModel' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>