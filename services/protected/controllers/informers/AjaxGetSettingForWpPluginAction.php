<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class AjaxGetSettingForWpPluginAction extends ActionFrontBase {
		const informersJsonSettingsCacheKey = 'informersJsonSettings';
	
		function run() {
	//		$cacheKey = self::informersJsonSettingsCacheKey;
		//	if( $data = Yii::app()->cache->get( $cacheKey ) ){ echo $data; return; }
			
			$data = InformersCategoryModel::getSettingForWpPlugin();
			$data = json_encode( $data );
			//Yii::app()->cache->set( $cacheKey, $data, 60*60*24 );

			echo $data;
			
		//	print_r( $data );

			return;
		}
	}

?>