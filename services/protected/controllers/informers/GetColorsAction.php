<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class GetColorsAction extends ActionFrontBase {

		function run( $st ) {
			$data = array();
			try{
				$model = InformersStyleModel::model()->findByPk($st);
				if( !$model ) $this->throwI18NException( "Can't load style" );
			
				ob_start();
					$this->controller->widget( 'widgets.InformerColorsWidget', Array('model' => $model) );
				$data['data'] = ob_get_clean();
				$data['oneColumn'] = $model->oneColumn;
				$data['width'] = $model->width;
				$data['hasDiffValue'] = $model->hasDiffValue;
			}
			catch( Exception $e ) {
				$data['error'] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>