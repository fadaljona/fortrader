<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.ContestForeignFormModel' );
	
	final class AjaxAddForeignContestAction extends ActionAdminBase {
		private $formModel;
		private $formModelName = 'ContestForeignFormModel';
		private function detFormModel() {
			$this->formModel = new $this->formModelName;
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Contest!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>