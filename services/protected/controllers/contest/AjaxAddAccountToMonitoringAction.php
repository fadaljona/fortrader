<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxAddAccountToMonitoringAction extends ActionFrontBase {
		private $monitoringAddr = 'http://159.69.219.218:18466/';
		
		private function deleteAddAccount( $member ){
			$url = $this->monitoringAddr . "master.api?cmd=del_acc&aid={$member->id}&login={$member->accountNumber}";
			$deleteUrl = $url;
			
			$deleteResult = CommonLib::downloadFileFromWww( $url );
			if( !$deleteResult ) $this->throwI18NException( "Empty delete result, please contact admin" );
			
			preg_match_all('/aid\:\s*'.$member->id.'\s*login\:\s*'.$member->accountNumber.'\s*delete/',
				$deleteResult,
				$found,
				PREG_PATTERN_ORDER
			);
			
			if( !isset($found[0][0]) || !$found[0][0] ) $this->throwI18NException( "Wrong delete response, please contact admin" );
			
			$url = $this->monitoringAddr . "master.api?cmd=add_acc&aid={$member->id}&login={$member->accountNumber}&pass={$member->investorPassword}&srvmt4=" . rawurlencode($member->server->url) . "&__pr=2";
			$addResult = CommonLib::downloadFileFromWww( $url );
			if( !$addResult ) $this->throwI18NException( "Empty add result, please contact admin"  );
				
			preg_match_all('/aid\:\s*'.$member->id.'\s*login\:\s*'.$member->accountNumber.'\s*server\:\s*'.$member->server->url.'\s*Ok/',
				$addResult,
				$found,
				PREG_PATTERN_ORDER
			);
			
			if( !isset($found[0][0]) || !$found[0][0] ) $this->throwI18NException( "Wrong add response, please contact admin" );
			return true;
		}
		
		function run() {
			if( !isset( $_POST['memberId'] ) ) return false;
			$memberId = $_POST['memberId'];
			$model = ContestMemberModel::model()->findByPk( $memberId );
			if( !$model || $model->idUser != Yii::app()->user->id ) return false;
			
			try{
				$this->deleteAddAccount( $model );
				$data[ 'success' ] = true;
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>