<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.InfoContestMemberFormModel' );
	Yii::import( 'models.forms.AccountContestMemberFormModel' );
	
	final class AjaxRegisterMemberAction extends ActionFrontBase {
		private function getUser($step) {
			$user = Yii::App()->user->getModel();
			if(!$user){
				switch($step){
					case 'info':
						$user_id=(int)$_POST['RegisterUserFormModel']['user_id'];
						break;
					case 'account':
						$user_id=(int)$_POST['AccountContestMemberFormModel']['user_id'];
						break;
				}
				$user = UserModel::model()->find('ID=:id',array(':id'=>$user_id));
			}
			if( !$user ) $this->throwI18NException( "Can't find User! Relogin!" );
			return $user;
		}
		private function getContest( $id ) {
			if( $id == 0 ) return false;
			$model = ContestModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Contest!" );
			return $model;
		}
		private function getInfoFormModel() {
			$formModel = new InfoContestMemberFormModel();
			return $formModel;
		}
		private function getAccountFormModel() {
			$formModel = new AccountContestMemberFormModel();
			return $formModel;
		}
		function run( $idContest, $step, $flag = 'no', $act = '' ) {
			$data = Array();

			try{
				$user = $this->getUser($step);
				$contest = $this->getContest( $idContest );
				
				if( $contest && $user->isRegisteredInContest( $contest->id ) && $act != 'edit') $this->throwI18NException( 'User is already registered in this contest!' );
				
				switch( $step ) {
					case 'info':{
				
						if($flag == 'save_account_from_list') {
                            $accountModel = ListOfAccountsModel::model()->find(array(
                                'condition' => 'contest_id=:id',
                                'limit' => 1,
                                'params' => array(':id' => $contest->id),
                            ));
                            if(is_array($accountModel))
                                $accountModel = $accountModel[0];
                            if($accountModel){
                                $contestMember = new ContestMemberModel();
                                $contestMember->idContest = $contest->id;
                                $contestMember->idUser = $user->id;
                                $contestMember->accountNumber = $accountModel->login;
                                $contestMember->password = $accountModel->password;
                                $contestMember->investorPassword = $accountModel->inv_password;
                                $contestMember->idServer = $contest->servers[0]->id;
                                if( $contestMember->validate()) {
                                    if($contestMember->save()) {
                                        $accountModel->delete();

                                        $factoryMailer =  new MailerComponent();
                                        $mailer = $factoryMailer->instanceMailer();
                                        $key = 'Account is created';
                                        $mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
                                        $i18n = $mailTpl->currentLanguageI18N;
                                        if(!$i18n)
                                            foreach( $mailTpl->i18ns as $i18nItem )
                                                if( strlen( $i18nItem->message ))
                                                    $i18n = $i18nItem;

                                        $message = strtr( $i18n->message, Array(
                                                '%username%' => $user->user_login,
                                                '%contest_name%' => $contest->name,
                                                '%account_number%' => $contestMember->accountNumber,
                                                '%password%' => $contestMember->password,
                                                '%inv_password%' => $contestMember->investorPassword,
                                                '%server_name%' => $contest->servers[0]->name,
                                                '%server_ip%' => $contest->servers[0]->url,
                                        ));

                                        $mailer->AddAddress( $user->user_email, $user->showName );
                                        $mailer->Subject = $i18n->subject;
                                        $mailer->Body = $message;
										

                                        try{
                                            $result = $mailer->Send();
                                        } catch( Exception $e ) {
                                            $data[ 'resultOfSending' ] = $e->getMessage();
                                        }

                                    }

                                    $data[ 'contestMemberId' ] = $contestMember->id;
                                    $data[ 'accountNumber' ] = $contestMember->accountNumber;
                                    $data[ 'password' ] = $contestMember->password;
                                    $data[ 'investorPassword' ] = $contestMember->investorPassword;
                                }
                            }
                        }
						else $data[ 'errors' ] = $formModel->getFirstErrors();
						break;
					}
					case 'account':{
						$contestId = $contest ? $contest->id : 0;
						$formModel = $this->getAccountFormModel();
						$formModel->loadFromPost();
						if( $formModel->validate()) {
			
							if( $act == 'edit' ){
								
								if( $contestId ){
									$member = ContestMemberModel::model()->find( array(
										'condition' => " `t`.`idContest` = :idContest and `t`.`idUser` = :idUser ",
										'params' => array(':idContest' => $contestId, ':idUser' => $user->id ),
									) );
								}elseif( $formModel->id && !$contestId ){
									$member = ContestMemberModel::model()->findByPk( $formModel->id );
								}
									
								
								MT5AccountInfoModel::model()->deleteAll(
									" `ACCOUNT_NUMBER` = :accNum AND `AccountServer` = :serverInternalName ",
									array( ':accNum' => $member->accountNumber, ':serverInternalName' => $member->server->internalName )
								);
								
								$member->accountNumber = $formModel->accountNumber;
								$member->investorPassword = $formModel->investorPassword;
								$member->idServer = $formModel->idServer;
								$member->accountTitle = $formModel->accountTitle;
								if( $member->validate() ){
									$member->save();
									$id = $member->id;
								}

							}else{
								$AR = ContestMemberModel::instance( $contestId, $user->id );
								$formModel->saveToAR( $AR );
								$AR->save();
								$id = $AR->getPrimaryKey();
							}
							
											
							$data[ 'existMember' ] = 0;
							if( !$id ){
								
								if( $contestId ){
									$existMemberModel = ContestMemberModel::model()->find(Array(
										'condition' => " `t`.`idContest` = :idContest and `t`.`accountNumber` = :accountNumber ",
										'params' => array(':idContest' => $contestId, ':accountNumber' => $formModel->accountNumber ),
									));
								}else{
									$existMemberModel = ContestMemberModel::model()->find(Array(
										'condition' => " `t`.`idContest` = :idContest and `t`.`accountNumber` = :accountNumber and `t`.`idServer` = :idServer ",
										'params' => array(':idContest' => $contestId, ':accountNumber' => $formModel->accountNumber, ':idServer' => $formModel->idServer ),
									));
								}
								
								
								if( $existMemberModel ){
									$data[ 'existMember' ] = $existMemberModel->id;
									$this->throwI18NException( "Account with this number is already registered in the competition id = ".$existMemberModel->id );
								}else{
									if( $contestId ){
										$this->throwI18NException( "Can't create member, please check if account didn't register in monitoring" );
									}else{
										$this->throwI18NException( "Can't save data, please check if account didn't register in contests" );
									}
								}
							}
							if ( $contest->accountOpening == 'manually trader'  ) {

								$contestMember = ContestMemberModel::model()->findByAttributes(
									array(
										'idUser'=>$user->id,
										'idContest'=>$contestId
									)
								);


								$data[ 'contestMemberId' ] = $contestMember->id;
							}
							if( $contestId == 0 ){
								$data[ 'contestMemberId' ] = $id;
							}
							$data[ 'id' ] = $id;
						}
						else $data[ 'errors' ] = $formModel->getFirstErrors();
						break;
					}
					default:{
						$this->throwI18NException( "Wrong step!" );
					}
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>
