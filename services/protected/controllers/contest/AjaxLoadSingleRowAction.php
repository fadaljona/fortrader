<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadSingleRowAction extends ActionFrontBase {
		private function renderRow() {
			ob_start();
			
			$widget = $this->controller->createWidget( 'widgets.lists.ContestInfoListWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
			));
			
			$widget->renderRow();
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $id ) {
			$data = Array();
			try{
				$data[ 'row' ] = $this->renderRow();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>