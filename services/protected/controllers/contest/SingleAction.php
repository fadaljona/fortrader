<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		public $level = 2;
		
		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		private $cat = 'singleContest';
		private $paramStr;
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Contests',
					'url' => Array( '/contest/list' ),
				),
				(object)Array( 
					'label' => $this->model->currentLanguageI18N->name,
				)
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			$item = $navlist->createItemAfter( $navlist->findItemById( "iContestsList" ));
			$item->setLabel( $this->model->currentLanguageI18N->name );
			$item->setActive();
		}
		private function getModel( $slug ) {
			$model = ContestModel::model()->findBySlug( $slug );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta() {
			
			$metaTitle = $this->model->metaTitle;

			$this->metaDesc = $this->model->metaDesc;
			$this->metaKeys = $this->model->metaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = $this->model->name;
			}
			
			$this->title = $this->model->title;
			if( !$this->title ) $this->title = $this->model->name;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			if( $this->model->getSrcImage() ){
				$this->setLayoutOgSection('');
				$this->setLayoutOgImage( $this->model->getSrcImage() );
			}
		}
		private function makeRedirectToSlug(){
			$id = Yii::app()->request->getParam('id');
			if( $id ){
				$slug = ContestModel::getSlugById( $id );
				if( !$slug ) throw new CHttpException(404,'Page Not Found');
				$this->redirect( $this->controller->createUrl( 'contest/single', array( 'slug' => $slug ) ), true, 301 );
			}
		}
		function run( ) {

			$this->makeRedirectToSlug();
			$slug = Yii::app()->request->getParam('slug');
			if( $slug == 'list' || $slug == 'index' || $slug == 'single' ) $this->redirect( $this->controller->createUrl( 'contest/list' ), true, 301 );
			if( !$slug ) throw new CHttpException(404,'Page Not Found');
			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->model = $this->getModel( $slug );
			$this->model->viewed();
			
			$this->setMeta();	
			$this->setLangSwitcher($this->model);
			$this->setLayoutBodyClass('inner_contest_page');
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
			
			$this->paramStr = "contest_{$this->model->id}";
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'contest' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>
