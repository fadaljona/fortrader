<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxCheckAccountStatusAction extends ActionFrontBase {
		

		function run() {
			if( !isset( $_POST['memberId'] ) ) return false;
			$memberId = $_POST['memberId'];
			$model = ContestMemberModel::model()->findByPk( $memberId );
			if( !$model || $model->idUser != Yii::app()->user->id ) return false;
			
			$accountModel = MT5AccountInfoModel::model()->find( array(
				'condition' => " `t`.`ACCOUNT_NUMBER` = :acNum AND `t`.`AccountServer` = :serverInternalName ",
				'params' => array( ':acNum' => $model->accountNumber, ':serverInternalName' => $model->server->internalName ),
				'order' => " `t`.`ACCOUNT_UPDATE` DESC ",
			) );
			
			if( !$accountModel ){
				echo json_encode( array( 'errorNo' => -123156 ) );
			}else{
				$errorTitle = $errorDesc = '';
				if( $accountModel->error && $accountModel->error->title ){
					$errorTitle = $accountModel->error->title;
					$errorDesc = $accountModel->error->description;
				}
				echo json_encode( array( 'errorNo' => $accountModel->last_error_code, 'errorDesc' => $accountModel->last_error_descr, 'title' => $errorTitle, 'description' => $errorDesc ) );
			}
		}
	}

?>