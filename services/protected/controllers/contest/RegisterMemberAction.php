<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class RegisterMemberAction extends ActionFrontBase {
		private $modelContest;
		private function detModelContest( $idContest ) {
			$this->modelContest = ContestModel::model()->findByPk( $idContest );
			if( !$this->modelContest ) $this->redirect( Array( 'contest/list' ));
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			$item = $navlist->createItemAfter( $navlist->findItemById( "iContestsList" ));
			$item->setLabel( "Registation" );
			$item->setActive();
		}
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array(
					'label' => 'Contests',
					'url' => Array( 'contest/list' ),
				),
				(object)Array(
					'label' => $this->modelContest->currentLanguageI18N->name,
					'url' => $this->modelContest->singleURL,
				),
				(object)Array(
					'label' => 'Registation member',
				)
			);
		}
		function run( $idContest ) {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();

			$this->detModelContest( $idContest );
			
			$this->setLayoutTitle( "Registation member" );
			$this->setLayout( "default/index" );
			$this->setSubitem();
			$this->setBreadcrumbs();

			if(Yii::app()->user->id){
				$UMode='register';
			}else{
				$UMode='guest';
			}
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'modelContest' => $this->modelContest,
				'UMode'=>$UMode
			));
		}
	}