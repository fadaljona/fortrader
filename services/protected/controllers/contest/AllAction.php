<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class AllAction extends ActionFrontBase {
		private $model;
		public $level = 2;
		
		public $title;
		public $metaTitle = 'All Contests';
		public $metaDesc;
		public $metaKeys;
		
		public $settings;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Contests',
					'url' => Array( '/contest/list' ),
				),
				(object)Array( 
					'label' => 'All Contests',
				)
			);
		}

		private function setMeta() {
			$this->settings = ContestSettingsModel::getModel();
			$metaTitle = $this->settings->allMetaTitle;

			$this->metaDesc = $this->settings->allMetaDesc;
			$this->metaKeys = $this->settings->allMetaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->allTitle;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->allOgImageUrl;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
		}

		function run( ) {

			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setMeta();
			$this->setLayoutBodyClass('contest_page');
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings
			));
		}
	}

?>
