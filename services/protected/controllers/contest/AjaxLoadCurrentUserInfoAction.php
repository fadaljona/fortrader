<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadCurrentUserInfoAction extends ActionFrontBase {
		private function getWidgetContent( $idContest ) {
			
			$contest = ContestModel::model()->findByPk($idContest);
			if( !$contest ) $this->throwI18NException( "Can't find contest" );
				
			ob_start();
			
			$this->controller->widget( 'widgets.lists.CurrentUserContestMemberInfoListWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
				'idContest' => $contest->id
			));	
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $idContest ) {
			
			$data = Array();
			try{
				$data[ 'content' ] = $this->getWidgetContent( $idContest );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
			
		}
	}

?>