<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadClosedTransactionsStatsGraphAction extends ActionFrontBase {
		private function renderGraph( $idContest ) {
			ob_start();
			
			$this->controller->widget( 'widgets.graphs.ContestClosedTransactionsStatsGraphWidget', Array(
				'ns' => 'nsActionView',
				'idContest' => $idContest,
				'ajax' => true,
			));
			
			$content = ob_get_clean();
			return $content;
		}
		function run( $idContest ) {
			$data = Array();
			try{
				$data[ 'graph' ] = $this->renderGraph( $idContest );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>