<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class GetStatsAction extends ActionFrontBase {
		public $cacheId = 'cacheGetContestStats';
		public $cacheTime = 600;
		
		function getStats( $idContest ){
			
			return $statsModel = ContestStatsModel::model()->find(Array(
				'with' => Array( 
					'wleaderMember',
				),
				'condition' => " `t`.`idContest` = :idContest ",
				'params' => Array(
					':idContest' => $idContest,
				),
			));
		}
		function prepareStatsData( $idContest ){
			$model = $this->getStats( $idContest );
			$data = Array();
			if( $model ){
				$data[ 'gain' ] = $model->wleaderMember->stats->gain;
				$data[ 'gain100' ] = $model->wleaderMember->stats->gain+1000;
				$data[ 'leader' ] = CHtml::link( CHtml::encode( $model->wleaderMember->user->showName ), $model->wleaderMember->user->getProfileURL(), array('class' => 'lider_statistics_name'));
				$data[ 'totalProfit' ] = round($model->totalProfit,0);
				$data[ 'profit100' ] = round($model->totalProfit,0)+$model->totalTraders*$model->contest->startDeposit;
				$data[ 'totalLoss' ] = abs(round($model->totalLoss,0));
				$data[ 'committedDeals' ] = number_format($model->committedDeals, 0, '.', ' ');
				$data[ 'bestDeal' ] = '+ '.number_format($model->bestDeal, 2, '.', ' ').' '.Yii::t( '*', 'pips' );
				$data[ 'bestProfitRiskMember' ] = CHtml::link( CHtml::encode( $model->wbestProfitRiskMember->user->showName ), $model->wbestProfitRiskMember->user->getProfileURL());
				
				if($model->lastDealOrderSymbol){
							$data[ 'lastDeal' ] = Yii::t( '*', 'SELL' );
							$data[ 'lastDeal' ] .= ' '.$model->lastDealOrderSymbol.' -';
						}else{
							$data[ 'lastDeal' ] = Yii::t( '*', 'BUY' );
							$data[ 'lastDeal' ] .= ' '.$model->lastDealOrderSymbol.' +';
						}  
				$data[ 'lastDeal' ] .= number_format(abs($model->lastDealOrderOpenPrice-$model->lastDealOrderClosePrice), 4, '.', ' ').' '.Yii::t( '*', 'pips' );
				$data[ 'activeTraders' ] = $model->activeTraders;
				$data[ 'profitTraders' ] = $model->profitTraders;
				$data[ 'lossTraders' ] = $model->lossTraders;
				$data[ 'totalTraders' ] = $model->totalTraders;

			}else{
				$data[ 'error' ] = "can't find model";
			}
			return json_encode( $data );
		}
		function setcacheId( $idContest ){
			$this->cacheId .= $idContest;
		}
		
		function run( $idContest ) {
			$this->setcacheId( $idContest );
			$statsData=Yii::app()->cache->get( $this->cacheId );
			if( $statsData===false ){
				$statsData = $this->prepareStatsData( $idContest );
				Yii::app()->cache->set( $this->cacheId, $statsData, $this->cacheTime );
			}
			echo $statsData;
		}
	}

?>