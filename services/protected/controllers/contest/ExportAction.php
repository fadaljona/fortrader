<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ExportAction extends ActionFrontBase {
		
		function run( $id ) {

			$contest = ContestModel::model()->findByPk($id);
			if( !$contest ) return false;
			
			$members = ContestMemberModel::model()->findAll(array(
				'with' => array( 'stats', 'user' ),
				'condition' => " `t`.`idContest` = :idContest AND `stats`.`countTrades` >= 10 ",
				'order' => " `stats`.`place` ASC ",
				'limit' => 100,
				'params' => array( ':idContest' => $contest->id ),
			));
			
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"table.csv\";" );
			header("Content-Transfer-Encoding: binary"); 

			$out = fopen('php://output', 'w');
			
			fputcsv($out, array(
				'place',
				'accountNumber',
				'userName',
				'balance',
				'dealsCount',
				'idUser',
				'idMember'
			), ';');
			
			foreach( $members as $member ){
				fputcsv($out, array(
					$member->stats->place,
					$member->accountNumber,
					$member->user->showName,
					$member->stats->balance,
					$member->stats->countTrades,
					$member->idUser,
					$member->id
				), ';');
			}
			
			

			fclose($out);



		}
	}

?>
