<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadForeignListAction extends ActionFrontBase {
		private function getDP( $page ) {
			$DP = ContestForeignModel::getListDp( $page );
			if( !$DP->getTotalItemCount( )) $this->throwI18NException( "Can't find Contests!" );
			return $DP;
		}
		private function getWidget( $DP ) {
			$widget = $this->controller->createWidget( 'widgets.gridViews.ForeignContestsGridViewWidget', Array(
				'dataProvider' => $DP,
				'returnOnlyTrs' => true
			));
			return $widget;
		}
		private function renderRows( $widget ) {
			ob_start();
			$widget->renderTableBody();
			return ob_get_clean();
		}
		function run( $page ) {
			$data = Array();
			try{
				$DP = $this->getDP( $page );
				$widget = $this->getWidget( $DP );
				
				$data[ 'rows' ] = $this->renderRows( $widget );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>