<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.LoginFormModel' );

	final class ListAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'ForTrader Contests';
		public $metaDesc;
		public $metaKeys;
		public $settings;
		
		private $cat = 'singleContest';
		private $paramStr;
		
		private function getModel() {
			$model = ContestModel::model()->find(Array(
				'condition' => " `t`.`status` = 'registration' or `t`.`status` = 'started' ",
				'order' => " `t`.`createdDT` DESC",
			));
			if( !$model ){
				$model = ContestModel::model()->find(Array(
					'condition' => " `t`.`status` = 'completed' ",
					'order' => " `t`.`end` DESC",
				));
			}
			return $model;
		}
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Contests',
				)
			);
		}
		private function setMeta() {
			$this->settings = ContestSettingsModel::getModel();
			$metaTitle = $this->settings->metaTitle;

			$this->metaDesc = $this->settings->metaDesc;
			$this->metaKeys = $this->settings->metaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->title;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->ogImageUrl;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
		}
		private function setKeywords() {
			$keywords = Yii::t( '*', 'ForTrader.Ru Contests History Table' );
			$this->controller->setLayoutKeywords( $keywords );
		}
		function run() {
			$this->setMeta();
			$actionCleanClass = $this->getCleanClassName();
			

			$this->setLayoutBodyClass( 'contest_page' );
			//$this->setLayoutBodyClass('contest_page inner_contest_page participant_page');
			
			$this->setLayout( "wp2/index" );
			$this->setBreadcrumbs();
			
			$this->paramStr = "contest_list_page";
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
				'model' => $this->getModel(),
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'foreignContestsCount' => ContestForeignModel::model()->countByAttributes(array('approved' => 1)),
				'contestsCount' => ContestModel::model()->count(),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>