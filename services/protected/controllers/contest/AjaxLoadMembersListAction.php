<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadMembersListAction extends ActionFrontBase {
		private function renderList( $idContest ) {
			ob_start();
			
			$this->controller->widget( 'widgets.lists.ContestMembersListWidget', Array(
				'idContest' => $idContest,
				'ns' => 'nsActionView',
				'ajax' => true,
				'showOnEmpty' => true,
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $id ) {
			$data = Array();
			try{
				$data[ 'list' ] = $this->renderList( $id );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>