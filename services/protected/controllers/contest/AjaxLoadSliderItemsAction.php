<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadSliderItemsAction extends ActionFrontBase {
		private function renderContent( $offset, $exclude ) {
			ob_start();
			
			$this->controller->widget( 'widgets.OtherContestsSliderWidget', Array(
				'ns' => 'nsActionView',
				'excludeModel' => $exclude,
				'offset' => $offset,
				'mode' => 'items'
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run() {
			$data = Array();
			
			if( !isset( $_POST['exclude'] ) || !isset( $_POST['offset'] ) ) return false;
			
			
			try{
				$data['content'] = $this->renderContent( $_POST['offset'], $_POST['exclude'] );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>