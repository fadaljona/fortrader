<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class IndexAction extends ActionFrontBase {
		function run() {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Home" );
			$this->setLayout( "{$controllerCleanClass}/{$actionCleanClass}" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>