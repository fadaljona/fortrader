<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ErrorAction extends ActionFrontBase {
		function run() {
		
			$error=Yii::app()->errorHandler->error;

			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( $error['code'].' - '.$error['message'], "{title}{-}{appName}" );
			$this->setLayout( "default/index" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'error' => $error,
			));
		}
	}

?>