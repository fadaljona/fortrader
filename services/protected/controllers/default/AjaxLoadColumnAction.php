<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadColumnAction extends ActionFrontBase {
		private function renderList() {
      $path = 'widgets.lists.' . Yii::app()->request->getQuery('class', 'Column') . 'Widget';
      if (is_file(Yii::getPathOfAlias($path) . '.php')) {
        $content = $this->controller->widget($path, Array(
          'ajax' => true,
          'showOnEmpty' => true,
        ), true);

        $content = preg_replace( "#[\r\n\t]+#", " ", $content );

        return $content;
      } else {
        throw new CException('incorrect query parameters');
      }
		}
    
		function run() {
			$data = array();
			try{
				$data[ 'list' ] = $this->renderList();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
      
			echo json_encode( $data );
		}
	}

?>