<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class LogoutAction extends ActionFrontBase {
		function run() {
			Yii::app()->user->logout();
			$url = @$_GET['returnURL'];
			$url = $url ? $url : $this->controller->createUrl( '/' );
			$url = Yii::App()->user->getReturnUrl( $url );
			$this->redirect( $url );
		}
	}

?>