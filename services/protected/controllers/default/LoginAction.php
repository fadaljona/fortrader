<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.LoginFormModel' );

	final class LoginAction extends ActionFrontBase {
		private $formModel;
		private function getFormModel() {
			if( !$this->formModel ) {
				$this->formModel = new LoginFormModel();	
			}
			return $this->formModel;
		}
		private function processFormModel() {
			$formModel = $this->getFormModel();
			if( !$formModel->emptyPost()) {
				$formModel->loadFromPost();
				if( $formModel->validate()) {
					$formModel->login();

					$currUserModel = UserModel::model()->find(Array(
								'condition' => " `t`.`user_login` = :login ",
								'params' => Array(
									':login' => $formModel->login,
								),
							));
					if( !$currUserModel->city || !$currUserModel->idCountry ){
						include( Yii::app()->basePath."/../../wp-content/geo/SxGeo.php");
						$SxGeo = new SxGeo( Yii::app()->basePath."/../../wp-content/geo/SxGeoCity.dat");
						$ipInfo = $SxGeo->getCityFull($_SERVER['REMOTE_ADDR']);
						
						if( !$currUserModel->city && $ipInfo['city']['name_ru'] ) $currUserModel->city = $ipInfo['city']['name_ru'];
						if( !$currUserModel->idCountry && $ipInfo['country']['iso'] ){
							$countryModel = CountryModel::model()->find(Array(
								'condition' => " `t`.`alias` = :iso ",
								'params' => Array(
									':iso' => strtolower( $ipInfo['country']['iso'] ),
								),
							));
							if( $countryModel ){
								$currUserModel->idCountry = $countryModel->id;
							}
						}
						$currUserModel->save();
					}
					
					$url = @$_GET['returnURL'];
					$url = $url ? $url : $this->controller->createUrl( '/' );
					$url = Yii::App()->user->getReturnUrl( $url );
					$this->redirect( $url );
				}
				$formModel->password = '';
			}
			else{
				$formModel->remember = 1;
			}
		}
		function run() {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->processFormModel();
			$this->setLayoutTitle( "Authorization" );
			$this->setLayoutBodyClass( "login-layout" );
			$this->setLayout( "{$controllerCleanClass}/login" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>