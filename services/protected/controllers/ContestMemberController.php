<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class ContestMemberController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Contests";
		}
		function actionIndex() {
			$this->redirect( Array( 'contest/list' ));
		}
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl',
				/*array(
					'PageCacheFilter + single',
					'duration'=> 60*5,
					'varyByParam' => 'id',
				),*/
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$params = array();		
				parse_str( Yii::app()->request->getQueryString(), $params );
				
				$action = '';
				if( Yii::app()->controller->action->id ){
					$action = '/' . Yii::app()->controller->action->id;
				}
				
				$url = Yii::App()->createURL( 'contestMember' . $action , $params);
				$url = str_replace( '/services', '', $url );
				
				$this->redirect( $url, true, 301 );
			}
			$filterChain->run();			
		}
	}

?>
