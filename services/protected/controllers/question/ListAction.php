<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ListAction extends ActionFrontBase {
		private $obj;
		private function setBreadcrumbs() {
			if( $this->obj->id ) {
				$breadcrumbs = Array(
					(object)Array( 
						'label' => 'Questions and suggestions',
						'url' => Array( 'question/list' ),
					),
					(object)Array(
						'label' => $this->obj->name,
					)
				);
			}
			else{
				$breadcrumbs = Array(
					(object)Array( 
						'label' => 'Questions and suggestions',
					),
				);
			}
			$this->controller->commonBag->breadcrumbs = $breadcrumbs;
		}
		function getObject( $id ) {
			$obj = new StdClass();
			
			$obj->id = $id;
			switch( $id ) {
				case UserMessageModel::ID_QUESTION_LIST_CONTESTS:{
					$obj->name = "Contests";
					break;
				}
				case UserMessageModel::ID_QUESTION_LIST_BROKERS:{
					$obj->name = "Brokers";
					break;
				}
				case UserMessageModel::ID_QUESTION_LIST_EA:{
					$obj->name = "EA";
					break;
				}
				case UserMessageModel::ID_QUESTION_LIST_JOURNALS:{
					$obj->name = "Journals";
					break;
				}
				case UserMessageModel::ID_QUESTION_LIST_CALENDAR_EVENTS:{
					$obj->name = "Calendar events";
					break;
				}
				case 0:{
					break;
				}
				default:{
					$this->throwI18NException( "Can't find Object!" );
				}
			}
			
			return $obj;
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$id = (int)@$_GET['id'];
			$this->obj = $this->getObject( $id );
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'obj' => $this->obj,
			));
		}
	}

?>