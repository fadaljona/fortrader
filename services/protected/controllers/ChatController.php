<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class ChatController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Chat";
		}
		function accessRules() {
			return Array(
				Array( 'deny', 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
	}

?>