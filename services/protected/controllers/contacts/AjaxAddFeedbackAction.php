<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.FeedbackFormModel' );
	
	final class AjaxAddFeedbackAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new FeedbackFormModel();
			$load = $this->formModel->load();
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
					
					$feedback = FeedbackModel::model()->findByPk( $id );
					$feedback->sendEmailsToContacts();
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>