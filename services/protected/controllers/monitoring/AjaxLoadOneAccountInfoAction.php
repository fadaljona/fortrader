<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadOneAccountInfoAction extends ActionFrontBase {
		private function renderRow( $id ) {
			if( !Yii::app()->user->id ) $this->throwI18NException( "User must be loged in" );
			
			$c = new CDbCriteria();
			$c->with[ 'server' ] = Array();
			$c->with[ 'mt5AccountInfo'] = array();

			$c->condition = " `t`.`idContest` = 0 AND `t`.`idUser` = :idUser AND `t`.`id` = :id ";
			$c->params[':idUser'] = Yii::app()->user->id;
			$c->params[':id'] = $id;
			
			$DP = new CActiveDataProvider( 'ContestMemberModel', Array(
				'criteria' => $c,
			));
			if( !$DP->totalItemCount ) $this->throwI18NException( "Can't find account" );
			$widget = $this->controller->createWidget( 'widgets.gridViews.YourMonitoringAccountsGridViewWidget', Array(
				'dataProvider' => $DP,
			));
			
			ob_start();
			
			$widget->renderTableRow(0);
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $id ) {
			$data = Array();
			try{
				$data[ 'row' ] = $this->renderRow( $id );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>