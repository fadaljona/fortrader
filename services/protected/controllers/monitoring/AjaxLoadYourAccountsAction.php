<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadYourAccountsAction extends ActionFrontBase {
		private function renderList( $type ) {
			ob_start();
			
			$this->controller->widget( 'widgets.lists.YourMonitoringAccountsListWidget', Array(
				'ns' => 'nsActionView',
				'type' => $type,
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $yourMonitoringAccountsListPage, $type = 'ft' ) {
			$data = Array();
			try{
				$data[ 'list' ] = $this->renderList( $type );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>