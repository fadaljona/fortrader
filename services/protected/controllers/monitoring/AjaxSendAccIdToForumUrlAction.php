<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxSendAccIdToForumUrlAction extends ActionFrontBase {
		private function sendAccId( $accId, $forumId ) {
			if( !Yii::app()->user->id ) $this->throwI18NException( "User must be loged in" );
			if( !$forumId ) $this->throwI18NException( "no forumId" );
			
			$memberModel = ContestMemberModel::model()->findByPk($accId);
			if( !$memberModel ) $this->throwI18NException( "can't find account" );
			if( $memberModel->idUser != Yii::app()->user->id ) $this->throwI18NException( "it's not your account" );
			
			$forumPostUrl = 'https://forexsystemsru.com/ft/postFromFt.php';
			
			$postdata = http_build_query(
				array(
					'accId' => $accId,
					'forumId' => $forumId,
					'desc' => $memberModel->accountNumber . ' ' . $memberModel->server->name
				)
			);

			$opts = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => $postdata
				)
			);

			$context  = stream_context_create($opts);

			$result = file_get_contents($forumPostUrl, false, $context);
			if( !$result || $result != 1 ) $this->throwI18NException( "wrong answear from forum" );;
			return 1;
		}
		function run( $accId, $forumId ) {
			$data = Array();
			try{
				$data[ 'ok' ] = $this->sendAccId( $accId, $forumId );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>