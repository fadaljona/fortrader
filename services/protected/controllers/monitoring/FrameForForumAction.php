<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class FrameForForumAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Account monitoring';
		public $settings;
		
		private function setMeta() {
			$this->settings = MonitoringSettingsModel::getModel();
			$metaTitle = $this->settings->metaTitle;
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->title;
			if(!$this->title) $this->title = $this->metaTitle;
		}
		function run( $id ) {
			$this->setMeta();

			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayout( "frameForum/index" );

			$this->controller->render( "{$actionCleanClass}/view", Array(
				'forumId' => $id
			));
		}
	}

?>