<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		private $cat = 'singleAccountMonitoring';
		private $paramStr;

		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Forex account monitoring',
					'url' => Array( '/monitoring/index' ),
				),
				(object)Array( 
					'label' => "{$this->model->user->showName}, {$this->model->accName}",
				)
			);
		}
		private function getModel( $id ) {
			$model = ContestMemberModel::model()->find(array(
				'condition' => ' `t`.`id` = :id AND `t`.`idContest` = 0 ',
				'params' => array( ':id' => $id )
			));
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta() {
			
			$this->metaTitle = "{$this->model->user->showName}, {$this->model->accName}";

			$this->metaDesc = $this->metaTitle;
			$this->metaKeys = $this->metaTitle;
			
			$this->title = "{$this->model->user->showName} - {$this->model->accName}";

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
		}

		function run( $id ) {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->model = $this->getModel( $id );	
			
			$this->paramStr = "monitoring_{$this->model->id}";
			$this->setMeta();
			$this->setLangSwitcher(true);
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
		
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>