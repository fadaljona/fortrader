<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadAccountsListAction extends ActionFrontBase {
		private $listWidget;
		private $gridWidget;
		
		private function setWidget( $period, $sortType, $sortTypeOrder, $accountSearchInput, $pagesCount, $negativFilter, $accountType ) {
			$this->listWidget = $this->controller->createWidget( 'widgets.lists.MonitoringListWithFiltersWidget', array(
				'ns' => 'nsActionView',
				'period' => $period, 
				'sortType' => $sortType, 
				'sortTypeOrder' => $sortTypeOrder, 
				'accountSearchInput' => $accountSearchInput,
				'pagesCount' => $pagesCount,
				'negativFilter' => $negativFilter,
				'accountType' => $accountType
			) );
		}
		private function setGridWidget(){
			$this->gridWidget = $this->listWidget->createGridView();
		}
		private function renderList() {
			ob_start();
			$data=$this->gridWidget->dataProvider->getData();
			$n=count($data);

			if($n>0){
				for($row=0;$row<$n;++$row)
					$this->gridWidget->renderTableRow($row);
			}else{
				echo '<tr><td colspan="'.count($this->gridWidget->columns).'" class="empty">';
				$this->gridWidget->renderEmptyText();
				echo "</td></tr>\n";
			}
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $monitoringAccountsListPage, $period, $sortType = 'views', $sortTypeOrder = 'DESC', $accountSearchInput, $pagesCount, $negativFilter, $accountType ) {

			$data = Array();
			try{
				$this->setWidget( $period, $sortType, $sortTypeOrder, $accountSearchInput, $pagesCount, $negativFilter, $accountType );
				$this->setGridWidget();
				$data[ 'list' ] = $this->renderList();
				$data[ 'itemsCount' ] = $this->gridWidget->dataProvider->totalItemCount;
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>