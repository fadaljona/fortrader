<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxSaveViewsAction extends ActionFrontBase {
		function run() {
			if( !isset($_POST['id']) || !$_POST['id'] ) return false;
			ContestMemberViewsModel::saveViews( $_POST['id'] );
		}
	}
?>