<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.AccountContestMemberFormModel' );
	
	final class IframeAddReportAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AccountContestMemberFormModel();
			$this->formModel->loadFromPost();
			$this->formModel->loadFromFiles( Array( 'reportFile' ) );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "//layouts/cleanPage" );
			
			$data = Array();
			try{
				$user = Yii::App()->user->getModel();
				if( !$user ) $this->throwI18NException( "Can't find User!" );
				
				$this->detFormModel();
				
				if( $this->formModel->validate()) {
					
					$fileData = $this->formModel->getDataFromFile();
					if( !is_array( $fileData ) ) $this->throwI18NException( "Wrong file format! error " . $fileData );
					
					$existMember = ContestMemberModel::model()->find(array(
						'with' => array('report'),
						'condition' => " `report`.`reportHash` = :reportHash ",
						'params' => array( ':reportHash' => $fileData['hash'] )
					));
					
					if( $existMember ){
						$this->throwI18NException( 'This report is already uploaded {link}', array( '{link}' => Yii::app()->createAbsoluteUrl('monitoring/single', array( 'id' => $existMember->id )) )  );
					}
					
					$server = ServerModel::model()->find(array(
						'condition' => ' `t`.`forReports` = 1 ',
					));
					
					if( !$server ) $this->throwI18NException( "Can't find server for reports" );
					
					$member = new ContestMemberModel();
					$member->idContest = 0;
					$member->idUser = $user->id;
					$member->idServer = $server->id;
					$member->accountTitle = $this->formModel->accountTitle;
					
					if( $member->validate() ){
						if( $member->save() ){
							
							$report = new ContestMemberReportModel();
							$report->idMember = $member->id;
							$report->reportHash = $fileData['hash'];
							$report->save();
							$member->accountNumber = $report->idAcc;
							$member->save();
							
							$member->importReportData( $fileData['symbol'], $fileData['data'] );
							
							$data[ 'id' ] = $member->id;
							
						}else{
							$this->throwI18NException( "Can't create monitoring member" );
						}
					}else{
						list( $data[ 'errorField' ], $data[ 'error' ]) = $member->getFirstError();
					}
					
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $this->formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'data' => $data,
			));
		}
	}

?>