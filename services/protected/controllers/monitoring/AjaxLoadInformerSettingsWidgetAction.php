<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadInformerSettingsWidgetAction extends ActionFrontBase {
		private function getWidgetContent( $id ) {
			ob_start();
			
			$this->controller->widget( 'widgets.InformerSettingsWidget', Array(
				'ns' => 'nsActionView',
				'category' => InformersCategoryModel::model()->findBySlugWithLang('monitoring'),
				'idMember' => $id,
				'ajax' => true
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $id ) {
			$data = Array();
			try{
				$data[ 'content' ] = $this->getWidgetContent( $id );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>