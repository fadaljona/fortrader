<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class BrokerController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Broker ratings";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'ajaxAddMark', 'ajaxLinkToUser', 'ajaxUnLinkFromUser', 'ajaxSaveSm' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl'
				/*array(
					'PageCacheFilter + single',
					'duration'=> 60*60 * 24 * 30,
					'varyByParam' => 'id',
				),*/
				/*array(
					'PageCacheFilter + list',
					'duration'=> 60*60 * 24,
					'varyByParam' => 'brokersPage',
				),*/
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$params = array();		
				parse_str( Yii::app()->request->getQueryString(), $params );
				
				$action = '';
				if( Yii::app()->controller->action->id ){
					$action = '/' . Yii::app()->controller->action->id;
				}
				
				$url = Yii::App()->createURL( 'brokersrating' . $action , $params);
				$url = str_replace( '/services', '', $url );
				
				$this->redirect( $url, true, 301 );
			}
			$filterChain->run();			
		}
		public function singleSiteMapArgs(){
			$models = BrokerModel::model()->findAll(array(
				'select' => ' `slug` ',
				'condition' => ' `showInRating` = 1 AND `type` = "broker" '

			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = array(
					'slug' => strtolower( $model->slug )
				);
			}
			return $outArr;
		}
	}

?>