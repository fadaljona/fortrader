<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class InterestRatesController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Interest Rates";
		}
		function accessRules(){
			return Array(
				array(
					/*'deny',
					'actions' => array('index'),
					'expression' => array('InterestRatesController','allowOnlyAdmin'),
					'users'=>array('*')*/
				),
			);
		}
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl'
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$url = str_replace( '/services', '', Yii::App()->request->getUrl() );
				
				$this->redirect( strtolower($url), true, 301 );
			}
			$filterChain->run();			
		}
		public static function allowOnlyAdmin(){
			if( Yii::App()->user->id == 1 ) return false;
			return true;
		}
	}