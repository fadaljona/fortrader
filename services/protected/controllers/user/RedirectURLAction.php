<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class RedirectURLAction extends ActionFrontBase {
		public $model;
		private function detModel() {
			$id = abs((int)@$_GET['id']);
			$this->model = UserModel::model()->findByPk( $id );
			if( !$this->model ) $this->throwI18NException( "Can't find User!" );
		}
		function run() {
			$this->detModel();
						
			$url = CommonLib::getURL( $this->model->user_url );
			if( $url ){
				Yii::App()->request->redirect( $url);
			}else{
				throw new CHttpException(404,'Page Not Found');
			}
		}
	}

?>