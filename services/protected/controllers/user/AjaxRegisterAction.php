<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.RegisterUserFormModel' );
	Yii::import( 'components.MailerComponent' );
	Yii::import('application.extensions.*');
	Yii::import( 'models.forms.LoginFormModel' );
	
	final class AjaxRegisterAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new RegisterUserFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			return $this->formModel;
		}
		private function getMailer() {
			$factoryMailer =  new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			return $mailer;
		}
		private function getMailTpl() {
			$key = 'Message to user about his registration';
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			if( !$mailTpl ) $this->throwI18NException( "Can't load mail template! ({key})", Array( '{key}' => $key ));
			return $mailTpl;
		}
		private function getMailTplI18N( $mailTpl ) {
			$i18n = $mailTpl->currentLanguageI18N;
			if( $i18n and strlen( $i18n->message )) return $i18n;
			foreach( $mailTpl->i18ns as $i18n ) if( strlen( $i18n->message )) return $i18n;

			$this->throwI18NException( "Can't load i18n for mail template!" );
		}
		private function renderMessage( $user, $message ) {
			return strtr( $message, Array(
				'%username%' => $user->user_login
			));
		}
		private function sendMail( $user ) {
			$mailer = $this->getMailer();
			$mailTpl = $this->getMailTpl();
			$i18n = $this->getMailTplI18N( $mailTpl );
						
			$message = $this->renderMessage( $user, $i18n->message );
						
			$mailer->AddAddress( $user->user_email, $user->showName );
			$mailer->Subject = $i18n->subject;
			if( substr_count( $message, '<' )) {
				$message = CommonLib::nl2br( $message, true );
				$mailer->MsgHTML( $message );
			}
			else{
				$mailer->Body = $message;
			}
			
			try{
				$result = $mailer->Send();
			}
			catch( Exception $e ) {}
			//$error = $result ? null : $mailer->ErrorInfo;
			//if( $error ) throw new Exception( $error );
		}
		function run() {
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					
					
					require_once( Yii::app()->params['wpThemePath'].'/includes/recaptchalib.php' );
					$secret = "6Lf_WxsUAAAAAL6bs7UCcBCXfsZ-PX3eEL_GgYZk";
					$reCaptcha = new ReCaptcha($secret);
					$response = null;

					$response = $reCaptcha->verifyResponse(
						$_SERVER["REMOTE_ADDR"],
						$_POST['g-recaptcha-response']
					);
					if ($response != null && $response->success){
						$data['ReCaptchaSuccess'] = 1;
					}else{
						$this->throwI18NException( "CAPTCHA validation fails, pass validation. If you can't-refresh the page" );
					}
					
					
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
					$this->sendMail( $formModel->getAR() );
					
					$newLogin = new LoginFormModel();
					$newLogin->login = $formModel->user_login;
					$newLogin->password = $formModel->user_pass;
					$newLogin->remember = 1;
					if( $newLogin->validate()) {
						$newLogin->login();
					}

					//export to mailerlite
			
                    $groupIdModel = WpOptionsModel::model()->find(Array('condition' => " `t`.`option_name` = 'unisender_group_id' "));
                    
                    require_once( Yii::app()->params['wpThemePath'] . '/unisender/functions.php');

                    $result = addUserToUnisender($formModel->user_email, $groupIdModel->option_value);
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
				
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>
