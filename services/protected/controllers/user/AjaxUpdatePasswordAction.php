<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.UserPasswordFormModel' );
	
	final class AjaxUpdatePasswordAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new UserPasswordFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find User!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				if( Yii::App()->user->isRoot()) $this->throwI18NException( "Root can't change password!" );
				$this->detFormModel();
				$formModel = $this->getFormModel();
				$itCurrentModel = $formModel->ID == Yii::App()->user->id;
				if( !$itCurrentModel and !Yii::App()->user->checkAccess( 'userControl' )) {
					$this->throwI18NException( "Access denied!" );
				}
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					foreach( $formModel->attributeNames() as $key ) {
						if( $formModel->hasErrors( $key )) {
							$field = CHtml::resolveName( $formModel, $key );
							$errors = $formModel->getErrors( $key );
							$error = array_shift( $errors );
							$data[ 'errors' ][] = compact( 'field', 'error' );
						}
					}
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>