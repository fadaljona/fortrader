<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadContestMemberRowAction extends ActionAdminBase {
		private function getDP( $id ) {
			$DP = new CActiveDataProvider( "ContestMemberModel", Array(
				'criteria' => Array(
					'condition' => '`t`.`id` = :id',
					'params' => Array(
						':id' => $id,
					),
				),
			));
			if( !$DP->getTotalItemCount( )) $this->throwI18NException( "Can't find Member!" );
			return $DP;
		}
		private function getItCurrentModel( $id ) {
			$member = ContestMemberModel::model()->findByPk( $id );
			return $member and $member->idUser == Yii::App()->user->id;
		}
		private function getWidget( $itCurrentModel, $DP ) {
			$widget = $this->controller->createWidget( 'widgets.gridViews.ContestMembersTAGridViewWidget', Array(
				'dataProvider' => $DP,
				'itCurrentModel' => $itCurrentModel,
			));
			return $widget;
		}
		private function renderRow( $widget ) {
			ob_start();
			$widget->renderTableRow( 0 );
			return ob_get_clean();
		}
		function run() {
			$data = Array();
			try{
				$id = (int)@$_GET[ 'id' ];
				$DP = $this->getDP( $id );
				$itCurrentModel = $this->getItCurrentModel( $id );
				$widget = $this->getWidget( $itCurrentModel, $DP );
				
				$data[ 'row' ] = $this->renderRow( $widget );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>