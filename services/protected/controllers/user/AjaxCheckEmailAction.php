<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.UserModel' );
	
	final class AjaxCheckEmailAction extends ActionFrontBase {
		function run() {
			$data = array('accept'=>0);
			if( strlen( $_POST['email'] ) >= 3 ){
				$user = UserModel::model()->find('user_email=:user_email',
					array(':user_email'=>$_POST['email'])
				);
				if(!$user){
					$data['accept']=1;
				}
			}
			echo json_encode( $data );
		}
	}
