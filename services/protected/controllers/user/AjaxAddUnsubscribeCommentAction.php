<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.UnsubscribeCommentFormModel' );
	
	final class AjaxAddUnsubscribeCommentAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new UnsubscribeCommentFormModel();
			
			$user = $this->getUser();
			$this->formModel->setIDUser( $user->ID );
			
			$load = $this->formModel->load();
		}
		private function getFormModel() {
			return $this->formModel;
		}
		private function getUser() {
			$user = null;
			$key = @$_POST['key'];
			if( strlen( $key )) {
				$email = ModelBase::decrypt( $key );
				$email = trim( $email );
				if( strlen( $email )) {
					$user = UserModel::model()->findByAttributes(Array(
						'user_email' => $email,
					));
				}
			}
			else{
				$user = Yii::App()->user->getModel();
			}
			
			if( !$user ) {
				 $this->throwI18NException( "Can't load User!" );
			}
			return $user;
		}
		function run() {
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>