<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.UserProfileModel' );

	final class setTimezoneAction extends ActionFrontBase {
		function run() {
			$utc = $_GET['utc'];
			if ( Yii::app()->user->isGuest ) {
				if (
					!isset(Yii::app()->request->cookies['utc']) 
					or Yii::app()->request->cookies['utc']->value == 0
				) {
					$cookie = new CHttpCookie('utc', $utc);
					$cookie->expire = time()+60*60*24*180; 
					Yii::app()->request->cookies['utc'] = $cookie;
				}
			}else{
				if ( UserProfileModel::getTimezone() == 0 or $_GET['force'] == 1) {
					$sql = "
						UPDATE ft_user_profile
						SET utc = ".$utc." 
						WHERE idUser = ".Yii::app()->user->id."
					";
					$connection=Yii::app()->db; 
					$command=$connection->createCommand($sql);
					$command->execute();
				}
			}
		}
	}

?>
