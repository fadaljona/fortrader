<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadEAListAction extends ActionFrontBase {
		private function renderList( $idUser ) {
			ob_start();
			
			$this->controller->widget( 'widgets.lists.UserEAListWidget', Array(
				'idUser' => $idUser,
				'ns' => 'nsActionView',
				'ajax' => true,
				'showOnEmpty' => true,
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$idUser = @$_GET[ 'idUser' ];
				$data[ 'list' ] = $this->renderList( $idUser );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>