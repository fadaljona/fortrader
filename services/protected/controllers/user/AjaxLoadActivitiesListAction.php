<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadActivitiesListAction extends ActionFrontBase {
		private $widget;
		private function getWidget( $instance, $idUser, $idObject, $idLastActivity ) {
			$widget = $this->controller->createWidget( 'widgets.UserActivitiesWidget', Array(
				'instance' => $instance,
				'idUser' => $idUser,
				'idObject' => $idObject,
				'idLastActivity' => $idLastActivity,
			));
			return $widget;
		}
		private function renderActivities() {
			ob_start();
			$this->widget->renderActivities();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$instance = (int)@$_GET['instance'];
				$idUser = (int)@$_GET['idUser'];
				$idObject = (int)@$_GET['idObject'];
				$idLastActivity = (int)@$_GET['idLastActivity'];
				$this->widget = $this->getWidget( $instance, $idUser, $idObject, $idLastActivity );
				
				$data[ 'list' ] = $this->renderActivities();
				$data[ 'more' ] = $this->widget->issetMoreActivities();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>