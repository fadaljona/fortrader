<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadContestMembersListAction extends ActionFrontBase {
		private function renderList( $idUser ) {
			ob_start();
			
			$this->controller->widget( 'widgets.lists.ContestMemberAcconutsListWidget', Array(
				'idUser' => $idUser,
				'ns' => 'nsActionView',
				'showOnEmpty' => true,
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $idUser ) {
			$data = Array();
			try{
				$data[ 'list' ] = $this->renderList( $idUser );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>