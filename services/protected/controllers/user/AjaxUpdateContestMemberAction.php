<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.ContestMemberFormModel' );
	
	final class AjaxUpdateContestMemberAction extends ActionFrontBase {
		private $formModel;
		private function getFormModel() {
			if( !$this->formModel ) {
				$this->formModel = new ContestMemberFormModel();
				$load = $this->formModel->load();
				if( !$load ) $this->throwI18NException( "Can't find Member!" );	
			}
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				$formModel = $this->getFormModel();
				$itCurrentModel = $formModel->getAR()->idUser == Yii::App()->user->id;
				if( !$itCurrentModel and !Yii::App()->user->checkAccess( 'contestMemberControl' )) {
					$this->throwI18NException( "Access denied!" );
				}
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
					
					$AR = $formModel->getAR();
					$AR->sendMasterAPIAddCmd();
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>