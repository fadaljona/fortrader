<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'components.MailerComponent' );

	final class ResetPasswordAction extends ActionFrontBase {
		const PasswordLen = UserModel::MINPasswordLen;
		private function getModel( $key ) {
			$key = preg_replace( "#[^0-9a-f]#", "", $key );
			if( strlen( $key ) != 32 ) $this->throwI18NException( "Request error!" );
			$model = UserModel::model()->findByAttributes( Array( 'resetPasswordKey' => $key ));
			if( !$model ) $this->throwI18NException( "Can't find User!" );
			return $model;
		}
		private function getMailer() {
			$factoryMailer =  new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			return $mailer;
		}
		private function getMailTpl() {
			$key = 'User password change message';
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			if( !$mailTpl ) $this->throwI18NException( "Can't load mail template! ({key})", Array( '{key}' => $key ));
			return $mailTpl;
		}
		private function getMailTplI18N( $mailTpl ) {
			$i18n = $mailTpl->currentLanguageI18N;
			if( $i18n and strlen( $i18n->message )) return $i18n;
			foreach( $mailTpl->i18ns as $i18n ) if( strlen( $i18n->message )) return $i18n;

			$this->throwI18NException( "Can't load i18n for mail template!" );
		}
		private function renderMessage( $user, $password, $message ) {
			return strtr( $message, Array(
				'%username%' => $user->user_login,
				'%password%' => $password
			));
		}
		private function sendMail( $user, $password ) {
			$mailer = $this->getMailer();
			$mailTpl = $this->getMailTpl();
			$i18n = $this->getMailTplI18N( $mailTpl );
						
			$message = $this->renderMessage( $user, $password, $i18n->message );
						
			$mailer->addAddress( $user->user_email, $user->showName );
			$mailer->Subject = $i18n->subject;
			if( substr_count( $message, '<' )) {
				$message = CommonLib::nl2br( $message, true );
				$mailer->MsgHTML( $message );
			}
			else{
				$mailer->Body = $message;
			}
			
			$result = $mailer->send();
			$error = $result ? null : $mailer->ErrorInfo;
			if( $error ) throw new Exception( $error );
		}
		private function reset( $user ) {
			$password = CommonLib::randKey( self::PasswordLen, true );
			$user->setPassword( $password );
			$user->resetPasswordKey = null;
			$user->save();
			$this->sendMail( $user, $password );
		}
		function run( $key ) {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Reset password" );
			$this->setLayout( "message" );
			$this->setLayoutBodyClass( "iBody i04" );
			
			try{
				$model = $this->getModel( $key );
				$this->reset( $model );
				$NSi18n = $this->getNSi18n();
				$this->controller->render( "{$actionCleanClass}/message", Array(
					'message' => Yii::t( $NSi18n, 'Password reset. You will be sent a message with further instructions.' ),
				));
			}
			catch( Exception $e ) {
				$this->controller->render( "{$actionCleanClass}/error", Array(
					'error' => $e->getMessage(),
				));
			}
		}
	}

?>