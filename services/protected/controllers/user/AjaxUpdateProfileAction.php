<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.UserProfileFormModel' );
	
	final class AjaxUpdateProfileAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new UserProfileFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find User!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			CommonLib::loadWp();
			$data = Array();
			try{
				if( Yii::App()->user->isRoot()) $this->throwI18NException( "Root can't have profile!" );
				$this->detFormModel();
				$formModel = $this->getFormModel();
				$itCurrentModel = $formModel->ID == Yii::App()->user->id;
				if( !$itCurrentModel and !Yii::App()->user->checkAccess( 'userControl' )) {
					$this->throwI18NException( "Access denied!" );
				}
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
					
					if( $itCurrentModel ) {
						UserActivityModel::event( UserActivityModel::TYPE_EVENT_UPDATE_PROFILE, Array( 'idUser' => $formModel->ID ));
						$user = UserModel::model()->findByPk( Yii::App()->user->id );
						if( $user->profile and $user->profile->isTrader ) {
							$data[ 'isTrader' ] = 'yes';
						}
					}
				}
				else{
					foreach( $formModel->attributeNames() as $key ) {
						if( $formModel->hasErrors( $key )) {
							$field = CHtml::resolveName( $formModel, $key );
							$errors = $formModel->getErrors( $key );
							$error = array_shift( $errors );
							$data[ 'errors' ][] = compact( 'field', 'error' );
						}
					}
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>
