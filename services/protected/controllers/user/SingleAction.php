<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		private $cat = 'singleUserProfile';
		private $paramStr;

		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		private function setBreadcrumbs( $slug ) {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => $this->title,
					'url' => Array( '/user/single', 'slug' => $slug ),
				),
			);
		}
		private function getModel( $slug ) {
			$model = UserModel::findBySlug( $slug );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta(){
			$this->title = Yii::t( '*', 'User profile' ) . ' ' . $this->model->showName;
			
			$this->metaTitle = $this->title;

			$this->metaDesc = $this->title;
			
			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->title );
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $this->model->imgAvatar );
			
			$this->setMainItemscopeItemtype('CreativeWork');
			//$this->setLayoutBodyClass('participant_page');
		}

		private function getItCurrentModel() {
			$model = $this->model;
			$currentModel = Yii::App()->user->getModel();
			$itCurrentModel = ($currentModel and $currentModel->id == $this->model->id);
			return $itCurrentModel;
		}
		private function getCanProfileControl() {
			return $this->getItCurrentModel() || Yii::App()->user->checkAccess( 'userControl' );
		}
		
		function run( $slug ) {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->model = $this->getModel( $slug );	
			
			$this->paramStr = "user_{$this->model->id}";
			$this->setMeta();
			//$this->setLangSwitcher(true);
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs( $slug );
		
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model,
				'canControl' => $this->getCanProfileControl(),
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>