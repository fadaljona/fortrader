<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadRecentActivitiesListAction extends ActionFrontBase {
		private $widget;
		private function getWidget( $idUser, $idLastActivity ) {
			$widget = $this->controller->createWidget( 'widgets.UserRecentActivitiesWidget', Array(
				'idUser' => $idUser,
				'idLastActivity' => $idLastActivity,
			));
			return $widget;
		}
		private function renderActivities() {
			ob_start();
			$this->widget->renderActivities();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$idUser = @$_GET['idUser'];
				$idLastActivity = (int)@$_GET['idLastActivity'];
				$this->widget = $this->getWidget( $idUser, $idLastActivity );
				
				$data[ 'list' ] = $this->renderActivities();
				$data[ 'more' ] = $this->widget->issetMoreActivities();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>