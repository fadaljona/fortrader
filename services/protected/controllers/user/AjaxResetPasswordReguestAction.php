<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.ResetUserPasswordReguestFormModel' );
	Yii::import( 'components.MailerComponent' );
	
	final class AjaxResetPasswordReguestAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new ResetUserPasswordReguestFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			return $this->formModel;
		}
		private function getUser( $email ) {
			return UserModel::model()->find( Array(
				'condition' => " 
					`t`.`user_email` = :email
					OR `t`.`user_login` = :email
				",
				'params' => Array(
					':email' => $email
				),
			));
		}
		private function getMailer() {
			$factoryMailer =  new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			return $mailer;
		}
		private function getMailTpl() {
			$key = 'Reguest to user password change';
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			if( !$mailTpl ) $this->throwI18NException( "Can't load mail template! ({key})", Array( '{key}' => $key ));
			return $mailTpl;
		}
		private function getMailTplI18N( $mailTpl ) {
			$i18n = $mailTpl->currentLanguageI18N;
			if( $i18n and strlen( $i18n->message )) return $i18n;
			foreach( $mailTpl->i18ns as $i18n ) if( strlen( $i18n->message )) return $i18n;

			$this->throwI18NException( "Can't load i18n for mail template!" );
		}
		private function renderMessage( $user, $message ) {
			$link = $this->controller->createAbsoluteUrl( 'user/resetPassword', Array( 'key' => $user->resetPasswordKey ));
			return strtr( $message, Array(
				'%username%' => $user->user_login,
				'%link%' => $link
			));
		}
		private function sendMail( $user ) {
			$mailer = $this->getMailer();
			$mailTpl = $this->getMailTpl();
			$i18n = $this->getMailTplI18N( $mailTpl );
						
			$message = $this->renderMessage( $user, $i18n->message );
						
			$mailer->addAddress( $user->user_email, $user->showName );
			$mailer->Subject = $i18n->subject;
			if( substr_count( $message, '<' )) {
				$message = CommonLib::nl2br( $message, true );
				$mailer->MsgHTML( $message );
			}
			else{
				$mailer->Body = $message;
			}
			
			$result = $mailer->send();
			$error = $result ? null : $mailer->ErrorInfo;
			if( $error ) throw new Exception( $error );
		}
		private function sendRequest( $user ) {
			$key = md5( rand() );
			$user->resetPasswordKey = $key;
			$user->save();
			$this->sendMail( $user );
		}
		function run() {
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$user = $this->getUser( $formModel->email );
					$this->sendRequest( $user );
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>