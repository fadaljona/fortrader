<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.UserProfileFormModel' );

	final class AjaxLoadInfoProfileAction extends ActionFrontBase {
		private function getModel() {
			$id = (int)@$_POST['id'];
			if( !$id ) $this->throwI18NException( "Set Id!" );
			
			$model = UserModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find User!" );
			
			return $model;
		}
		function run() {
			try{
				$model = $this->getModel();
				$this->controller->widget( 'widgets.parts.InfoUserProfileWidget', Array(
					'model' => $model,
				));
			}
			catch( Exception $e ) {
				echo $e->getMessage();
			}
		}
	}

?>