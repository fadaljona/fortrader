<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxDeleteAvatarAction extends ActionFrontBase {
		private function getModel( $id ) {
			$model = UserModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find User!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$itCurrentModel = $model->ID == Yii::App()->user->id;
				if( !$itCurrentModel and !Yii::App()->user->checkAccess( 'userControl' )) {
					$this->throwI18NException( "Access denied!" );
				}
				$model->deleteAvatar();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>