<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxRejectInviteFriendAction extends ActionFrontBase {
		function run( $idFriend ) {
			$data = Array();
			try{
				$idUser = Yii::App()->user->id;
				UserFriendModel::reject( $idUser, $idFriend );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>