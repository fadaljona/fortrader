<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ProfileAction extends ActionFrontBase {
		public $level = 2;
		private function setBreadcrumbs( $model ) {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => Yii::t( '*', 'Profile' )." ".$model->showname,
				)
			);
		}
		private function getModel() {
			$id = abs((int)@$_GET['id']);
			if( $id ) {
				$model = UserModel::model()->findByPk( $id );
			}
			else{
				$model = Yii::App()->user->getModel();
				if( !$model ) {
					Yii::App()->user->loginRequired();
				}
			}
			if( !$model ) {
				Yii::App()->controller->redirect( Array( '/' ));
			}
			
			return $model;
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$model = $this->getModel();
			
			$this->redirect( $model->singleURL, true, 301 );
			
			$this->setLayoutTitle( Yii::t( '*', 'Profile' )." ".CHtml::encode( $model->showname ) );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs( $model );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $model,
			));
		}
	}

?>