<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class AjaxCancelUnsubscribeMailingTypeAction extends ActionFrontBase {
		public $level = 2;
		private function getUser() {
			$user = null;
			$key = @$_GET['key'];
			if( strlen( $key )) {
				$email = ModelBase::decrypt( $key );
				$email = trim( $email );
				if( strlen( $email )) {
					$user = UserModel::model()->findByAttributes(Array(
						'user_email' => $email,
					));
				}
			}
			else{
				$user = Yii::App()->user->getModel();
			}
			
			if( !$user ) {
				 $this->throwI18NException( "Can't load User!" );
			}
			return $user;
		}
		function run() {
			$data = Array();
			try{
				$user = $this->getUser();
				$idType = (int)$_GET[ 'idType' ];
				$user->deleteMailingTypesMute( Array( (int)$idType ));
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>