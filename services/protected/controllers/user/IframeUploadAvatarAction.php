<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.UserAvatarFormModel' );

	final class IframeUploadAvatarAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new UserAvatarFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find User!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "cleanPage" );
			
			$data = new StdClass();
			try{
				if( Yii::App()->user->isRoot()) $this->throwI18NException( "Root can't have profile!" );
				$this->detFormModel();
				$formModel = $this->getFormModel();

				$itCurrentModel = $formModel->ID == Yii::App()->user->id;
				if( !$itCurrentModel and !Yii::App()->user->checkAccess( 'userControl' )) {
					$this->throwI18NException( "Access denied!" );
				}
				
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$model = UserModel::model()->findByPk( $id );
					$data->model = $model;
				}
				else{
					list( $data->errorField, $data->error ) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data->error = $e->getMessage();
			}
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'data' => $data,
			));
		}
	}

?>