<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SettingsAction extends ActionFrontBase {
		private $model;
		public $title;
		
		private function getModel( $id ) {
			$model = UserModel::model()->findByPk( $id );
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'User profile settings',
				)
			);
		}
		private function setMeta(){
			$this->title = Yii::t( '*', 'User profile settings' ) . ' ' . $this->model->showName;
		
			$this->setLayoutTitle( $this->title, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->title );
			$this->setLayoutKeywords( $this->title );

			$this->setMainItemscopeItemtype('CreativeWork');
		}
		private function getItCurrentModel() {
			$model = $this->model;
			$currentModel = Yii::App()->user->getModel();
			$itCurrentModel = ($currentModel and $currentModel->id == $this->model->id);
			return $itCurrentModel;
		}
		private function checkCanProfileControl() {
			$canControl = $this->getItCurrentModel() || Yii::App()->user->checkAccess( 'userControl' );
			if( !$canControl ) $this->throwI18NException( "You can't edit this profile" );
		}
		
		function run( $id ) {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->model = $this->getModel( $id );	
			$this->checkCanProfileControl();
			
			$this->setMeta();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'model' => $this->model
			));
		}
	}

?>
