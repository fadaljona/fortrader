<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadNoticesListAction extends ActionFrontBase {
		private $widget;
		private function getWidget( $idLastNotice ) {
			$widget = $this->controller->createWidget( 'widgets.NavbarWidget', Array(
				'idLastNotice' => $idLastNotice,
			));
			return $widget;
		}
		private function renderMessages() {
			ob_start();
			$this->widget->renderNotices();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$idLastNotice = (int)@$_GET['idLastNotice'];
				$this->widget = $this->getWidget( $idLastNotice );
				
				$data[ 'list' ] = $this->renderMessages();
				$data[ 'more' ] = $this->widget->issetMoreNotices();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>