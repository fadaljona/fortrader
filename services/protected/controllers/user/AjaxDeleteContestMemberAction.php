<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxDeleteContestMemberAction extends ActionAdminBase {
		private function getModel( $id ) {
			$model = ContestMemberModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Member!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$itCurrentModel = $model->idUser == Yii::App()->user->id;
				if( !$itCurrentModel and !Yii::App()->user->checkAccess( 'contestMemberControl' )) {
					$this->throwI18NException( "Access denied!" );
				}
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>
