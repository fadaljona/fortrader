<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.UserModel' );
	
	final class AjaxCheckLoginAction extends ActionFrontBase {
		function run() {
			$data = array('accept'=>0);
			if( strlen( $_POST['login'] ) >= 3 ){
				$user = UserModel::model()->find('user_login=:user_login',
					array(':user_login'=>$_POST['login'])
				);
				if(!$user){
					$data['accept']=1;
				}
			}
			echo json_encode( $data );
		}
	}
