<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.UnsubscribeCommentFormModel' );

	final class UnsubscribeMailingTypeAction extends ActionFrontBase {
		public $level = 2;
		private function setBreadcrumbs( $user ) {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => Yii::t( '*', 'Unsubscribe our mailing list' ),
				),
			);
		}
		private function getUser() {
			$user = null;
			$key = @$_GET['key'];
			if( strlen( $key )) {
				$email = ModelBase::decrypt( $key );
				$email = trim( $email );
				if( strlen( $email )) {
					$user = UserModel::model()->findByAttributes(Array(
						'user_email' => $email,
					));
				}
			}
			else{
				$user = Yii::App()->user->getModel();
			}
			
			if( !$user ) {
				Yii::App()->user->loginRequired();
			}
			return $user;
		}
		private function getMailingType() {
			$idType = abs((int)@$_GET['idType']);
			$type = MailingTypeModel::model()->findByPk( $idType );
			if( !$type ) {
				$this->throwI18NException( "Can't find MailingType!" );
			}
			return $type;
		}
		private function getFormModel( $user, $idType ) {
			$model = new UnsubscribeCommentFormModel();
			$model->setIDUser( $user->ID );
			$model->idMailingType = $idType;
			$model->load();
			return $model;
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$user = $this->getUser();
			$type = $this->getMailingType();
			$formModel = $this->getFormModel( $user, $type->id );
			$user->addMailingTypesMute( Array( $type->id ));
									
			$this->setLayoutTitle( "Unsubscribe our mailing list" );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs( $user );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'user' => $user,
				'type' => $type,
				'formModel' => $formModel,
			));
		}
	}

?>