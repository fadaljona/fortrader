<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.EAFormModel' );

	final class AddAction extends ActionFrontBase {
		private $formModel;
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'EA Catalog',
					'url' => Array( '/ea/list' ),
				),
				(object)Array( 
					'label' => Yii::t( '*', 'Adding EA' ),
				)
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			$item = $navlist->createItemAfter( $navlist->findItemById( "iEARating" ));
			$item->setLabel( "Adding EA" );
			$item->setActive();
		}
		private function detFormModel() {
			$this->formModel = new EAFormModel();
			$this->formModel->creator = Yii::App()->user->getModel()->showName;
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFormModel();
					
			$this->setLayoutTitle( 'Adding EA' );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->formModel,
			));
		}
	}

?>