<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		public $level = 2;
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array();
			
			if( substr_count( $this->model->params, 'LAB' )) {
				$this->controller->commonBag->breadcrumbs[] = (object)Array( 
					'label' => 'EA laboratory',
					'url' => Array( '/ea/laboratory' ),
				);
			}
			else{
				$this->controller->commonBag->breadcrumbs[] = (object)Array( 
					'label' => 'EA Catalog',
					'url' => Array( '/ea/list' ),
				);
			}
			
			$this->controller->commonBag->breadcrumbs[] = (object)Array( 
				'label' => $this->model->name,
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			
			if( substr_count( $this->model->params, 'LAB' )) {
				$before = $navlist->findItemById( "iEALaboratory" );
			}
			else{
				$before = $navlist->findItemById( "iEARating" );
			}
			
			$item = $navlist->createItemAfter( $before );
			
			$item->setLabel( $this->model->name );
			$item->setActive();
		}
		private function getModel( $id ) {
			$model = EAModel::model()->find( Array(
				'with' => Array(
					'tradePlatforms' => Array(
						'together' => false,
					),
				),
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		function run( $id ) {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $id );
			$this->model->viewed();
			
			$this->setLayoutTitle( $this->model->name );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'ea' => $this->model,
			));
		}
	}

?>