<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.AdminEAMarkFormModel' );
	
	final class AjaxAddMarkAction extends ActionFrontBase {
		private $formModel;
		private function getFormModel( $idEA ) {
			if( !$this->formModel ) {
				$this->formModel = new AdminEAMarkFormModel();
				$this->formModel->setIDEA( $idEA );
				$load = $this->formModel->load();
				if( !$load ) $this->throwI18NException( "Can't find Model!" );
			}
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				$idEA = (int)@$_POST[ 'idEA' ];
				$formModel = $this->getFormModel( $idEA );
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>