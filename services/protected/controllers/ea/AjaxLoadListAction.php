<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadListAction extends ActionFrontBase {
		private function renderList( $instance ) {
			ob_start();
			
			if( !$instance ) $instance = 'list';
			$this->controller->widget( 'widgets.lists.EAListWidget', Array(
				'instance' => $instance,
				'ns' => 'nsActionView',
				'ajax' => true,
				'showOnEmpty' => true,
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$instance = @$_GET[ 'instance' ];
				$data[ 'list' ] = $this->renderList( $instance );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>