<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.EAFormModel' );

	final class EditAction extends ActionFrontBase {
		private $formModel;
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'EA Catalog',
					'url' => Array( '/ea/list' ),
				),
				(object)Array( 
					'label' => Yii::t( '*', 'Editing EA' ),
				)
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			$item = $navlist->createItemAfter( $navlist->findItemById( "iEARating" ));
			$item->setLabel( "Editing EA" );
			$item->setActive();
		}
		private function detFormModel( $id ) {
			$this->formModel = new EAFormModel();
			$load = $this->formModel->load( $id );
			if( !$load ) $this->throwI18NException( "Can't find Version!" );
		}
		private function checkAccess() {
			$AR = $this->formModel->getAR();
			if( $AR->idUser != Yii::App()->user->id and !Yii::App()->user->checkAccess( "eaControl" ) ) {
				$this->throwI18NException( "Access denied!" );
			}
		}
		function run( $id ) {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$this->detFormModel( $id );
			$this->checkAccess();
			$actionCleanClass = $this->getCleanClassName();
								
			$this->setLayoutTitle( 'Editing EA' );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->formModel,
			));
		}
	}

?>