<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadStatementsListAction extends ActionFrontBase {
		private $widget;
		private function getWidget( $idEA, $idEAVersion ) {
			$widget = $this->controller->createWidget( 'widgets.EAStatementsWidget', Array(
				'idEA' => $idEA,
				'idEAVersion' => $idEAVersion,
				'limit' => 2,
			));
			return $widget;
		}
		private function renderList() {
			ob_start();
			$this->widget->renderList();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$idEA = (int)@$_GET[ 'idEA' ];
				$idEAVersion = (int)@$_GET[ 'idEAVersion' ];
			
				$this->widget = $this->getWidget( $idEA, $idEAVersion );
				
				$data[ 'list' ] = $this->renderList();
				$data[ 'more' ] = $this->widget->issetMore();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>