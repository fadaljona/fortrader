<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class LaboratoryAction extends ActionFrontBase {
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'EA laboratory',
				)
			);
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "EA laboratory", "{title}{-}{appName}" );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>