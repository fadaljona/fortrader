<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadNewsListAction extends ActionFrontBase {
		private $widget;
		private function getWidget() {
			$widget = $this->controller->createWidget( 'widgets.EANewsWidget', Array(
			));
			return $widget;
		}
		private function renderNews() {
			ob_start();
			$this->widget->renderNews();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$this->widget = $this->getWidget();
				
				$data[ 'list' ] = $this->renderNews();
				$data[ 'more' ] = $this->widget->issetMore();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>