<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadListAccountsAction extends ActionFrontBase {
		private function renderList( $idEA ) {
			ob_start();
			
			$this->controller->widget( 'widgets.lists.EATradeAccountsListWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
				'showOnEmpty' => true,
				'idEA' => $idEA,
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run() {
			$data = Array();
			try{
				$idEA = (int)@$_GET[ 'idEA' ];
				$data[ 'list' ] = $this->renderList( $idEA );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>