<?
	Yii::import( 'controllers.base.ControllerBase' );

	abstract class ControllerAdminBase extends ControllerBase {
		public $layout = '/_layouts/default';
		function getNSi18n( $class = null ) {
			$class = $this->getCleanClassName( $class );
			return "modules/admin/controllers/{$class}";
		}
		function actions() {
			$class = $this->getCleanClassName();
			$alias = "application.modules.admin.controllers.{$class}";
			return $this->getActions( $alias );
		}
		
	}

?>