<?
	Yii::import( 'controllers.base.ActionBase' );
	Yii::import( 'components.NavlistComponent' );
	
	abstract class ActionFrontBase extends ActionBase {
		function getNSi18n( $controllerClass = null, $actionClass = null ) {
			$controllerClass = $this->controller->getCleanClassName( $controllerClass );
			$actionClass = $this->getCleanClassName( $actionClass );
			return "controllers/{$controllerClass}/{$actionClass}";
		}
		function getNavlist() {
			$factory = new NavlistComponent();
			$navlist = $factory->getNavlist();
			return $navlist;
		}
	}

?>