<?

	abstract class ActionBase extends CAction {
		const DPLimit = 10;
		const DPPageVar = 'page';
		const DPPagesCount = 5;
		public $DPLimit = self::DPLimit;
		public $DPPageVar = self::DPPageVar;
		public $delimLayoutTitle = ' - ';
		public $level = 0;
		protected function resolveClassName( $class ) {
			return $class ? $class : get_class( $this );
		}
		static function getActionCleanClassName( $class ) {
			$class = preg_replace( "#Action$#", "", $class );
			$class[0] = strtolower($class[0]);
			return $class;
		}
		function getCleanClassName( $class = null ) {
			$class = $this->resolveClassName( $class );
			$class = self::getActionCleanClassName( $class );
			return $class;
		}
		abstract function getNSi18n( $class = null );
		protected function setLayoutTitle( $titles, $pattern = null ) {
			$NSi18n = $this->getNSi18n();
			
			if( is_string( $titles )) $titles = Array( '{title}' => Yii::t( $NSi18n, $titles ));
			
			if( !$pattern )	{
				if( $this->level >= 2 ) {
					$pattern = "{title}{-}{controllerTitle}{-}{appName}";
				}
				else {
					$pattern = "{controllerTitle}{-}{title}{-}{appName}";
				}
			}
						
			$params = Array(
				'{controllerTitle}' => Yii::t( $NSi18n, $this->controller->detLayoutTitle() ),
				'{appName}' => Yii::t( $NSi18n, Yii::App()->name ),
				'{-}' => $this->delimLayoutTitle,
			);
			$params = array_merge( $params, $titles );
			
			$title = strtr( $pattern, $params );
			$this->controller->setLayoutTitle( $title );
						
			return $title;
		}
		protected function setLayoutOgSection( $str = '' ){
			if( $str ){
				$this->controller->setLayoutOgSection( $str );
				return $str;
			}
			$NSi18n = $this->getNSi18n();
			$str = Yii::t( $NSi18n, $this->controller->detLayoutTitle() );
			$this->controller->setLayoutOgSection( $str );
			return $str;
		}
		protected function setLayoutOgImage( $img ){
			$this->controller->setLayoutOgImage( $img );
			return $img;
		}
		protected function setLangSwitcher( $model ){
			$this->controller->setLangSwitcher( $model );
			return $model;
		}
		protected function setMainItemscopeItemtype( $item = false ){
			$this->controller->setMainItemscopeItemtype( $item );
			return $item;
		}
		protected function setLayoutDescription( $description ) {
			$this->controller->setLayoutDescription( $description );	
			return $description;
		}
		protected function setLayoutKeywords( $keywords ) {
			$this->controller->setLayoutKeywords( $keywords );	
			return $keywords;
		}
		protected function setLayoutBodyClass( $class ) {
			$this->controller->setLayoutBodyClass( $class );
		}
		protected function setLayout( $layout ) {
			$layout = "/_layouts/{$layout}";
			$this->controller->layout = $layout;
		}
		protected function redirect( $url, $terminate = true, $statusCode = 302 ) {
			$this->controller->redirect( $url, $terminate, $statusCode );
		}
		protected function getDPLimit() {
			return $this->DPLimit;
		}
		protected function setDPLimit( $limit ) {
			$this->DPLimit = $limit;
		}
		protected function getDPPageVar() {
			return $this->DPPageVar;
		}
		protected function setDPPageVar( $var ) {
			$this->DPPageVar = $var;
		}
		protected function getDPPagination() {
			return Array(
				'pageSize' => $this->getDPLimit(),
				'pageVar' => $this->getDPPageVar(),
			);
		}
		protected function throwI18NException( $message, $params = Array(), $class = null ) {
			$NSi18n = $this->getNSi18n( $class );
			throw new Exception( Yii::t( $NSi18n, $message, $params ));
		}
		protected function checkAccessArray( $arrayRights, $operator = 'OR' ) {
			$allow = Yii::App()->user->checkAccessArray( $arrayRights, $operator );
			if( $allow ) return true;
			if( Yii::App()->user->getIsGuest()) {
				Yii::App()->user->loginRequired();
			}
			else {
				throw new CHttpException( 403, Yii::t( 'yii', 'You are not authorized to perform this action.' ));
			}
		}
	}

?>