<?
	Yii::import( 'controllers.base.ControllerBase' );
	
	abstract class ControllerFrontBase extends ControllerBase {
		function getNSi18n( $class = null ) {
			$class = $this->getCleanClassName( $class );
			return "controllers/{$class}";
		}
		function actions() {
			$class = $this->getCleanClassName();
			$alias = "application.controllers.{$class}";
			return $this->getActions( $alias );
		}
		protected function beforeAction( $action ) {
			if( parent::beforeAction( $action ))  {
				UserRequestModel::addRequest();
				return true;
			}
			return false;
		}
	}

?>