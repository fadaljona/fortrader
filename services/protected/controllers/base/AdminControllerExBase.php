<?
	abstract class AdminControllerExBase extends ControllerExBase {
		public $editorWidget;
		public $formWidget;
		public $formModel;
			// dets
		function detEditorWidget() {
			$modelName = $this->getModelName();
			return  $editorWidget = AdminEditorWidgetBase::createWidgetByModelName( $modelName );
		}
		function detFormWidget() {
			$editorWidget = $this->getEditorWidget();
			return $formWidget = $editorWidget->getFormWidget();
		}
		function detFormModel() {
			$formWidget = $this->getFormWidget();
			return $formModel = $formWidget->getFormModel();
		}
		function detView() {
			return $view = "_base/{$this->action->id}";
		}
		function detLayout() {
			return $layout = '/_layouts/default';
		}
		function detLayoutBodyClass() {
			return $layoutBodyClass = "iBody i03";
		}
		function accessRules() {
			return $accessRules = Array(
				Array( 'deny' ),
			);
		}
			
			// actions
		function actionAjaxAdd() {
			$data = Array();
			try{
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
		function actionAjaxDelete( $id ) {
			$data = Array();
			try{
				$formModel = $this->getFormModel();
				$ids = CommonLib::explode( $id );
				foreach( $ids as $id ) {
					$formModel->load( $id, $throw = true );
					$formModel->deleteAR();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
		function actionAjaxLoadRow( $id ) {
			$data = Array();
			try{
				$editorWidget = $this->getEditorWidget();
				$data[ 'row' ] = $editorWidget->renderListWidgetRowByPk( $id, $properties = Array(), $return = true );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
		function actionAjaxLoadList() {
			$data = Array();
			try{
				$editorWidget = $this->getEditorWidget();
				$data[ 'list' ] = $editorWidget->renderListWidget( Array(), $captureOutput = true );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
		function actionAjaxLoad( $id ) {
			$data = Array();
			try{
				$formModel = $this->getFormModel();
				$formModel->load( $id, $throw = true );
				$data[ 'model' ] = $formModel->saveToObject();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
		function actionAjaxLoadFieldVariants( $name ) {
			$data = Array();
			try{
				$formWidget = $this->getFormWidget();
				$data[ 'variants' ] = $formWidget->getFieldVariants( $name );
				$data[ 'keysVariants' ] = array_keys( $data[ 'variants' ]);
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>