<?
	Yii::import( 'controllers.base.ActionBase' );

	abstract class ActionAdminBase extends ActionBase {
		public $bodyClass = 'iBody i03';
		function __construct( $controller, $id ) {
			parent::__construct( $controller, $id );
			$this->setLayoutBodyClass( $this->bodyClass );
		}
		function getNSi18n( $controllerClass = null, $actionClass = null ) {
			$controllerClass = $this->controller->getCleanClassName( $controllerClass );
			$actionClass = $this->getCleanClassName( $actionClass );
			return "modules/admin/controllers/{$controllerClass}/{$actionClass}";
		}
		protected function setLayout( $layout ) {
			if( $layout != '//layouts/cleanPage' ) return;
			$layout = str_replace( '/layouts', '/_layouts', $layout );
			$this->controller->layout = $layout;
		}
	}

?>