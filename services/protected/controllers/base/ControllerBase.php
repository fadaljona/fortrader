<?
	Yii::import( 'controllers.base.ActionBase' );
	Yii::import( 'models.LanguageModel' );
	
	abstract class ControllerBase extends CController {
		public $layout = "layout";
		protected $layoutBag;
		public $commonBag;
		function __construct( $id, $module = null ) {
			
			parent::__construct( $id, $module );
			$this->detLanguage();
			$this->createLayoutBag();
			$this->commonBag = new StdClass();
		}

		protected function detLanguage() {
			$availableLanguages = LanguageModel::getExistsAliases();
			$cookieKey = Yii::App()->params[ 'languageCookieVarName' ];
			$cookieLanguage = &$_COOKIE[ $cookieKey ];
			$user = Yii::App()->user->getModel();
			if( !empty( $cookieLanguage )) {
				if( in_array( $cookieLanguage, $availableLanguages )) {
					Yii::app()->setLanguage( $cookieLanguage );
					if( $user and $user->language != $cookieLanguage ) {
						$user->language = $cookieLanguage;
						$user->save();
					}
					return;
				}
			}
			if( $user and $user->language ) {
				if( in_array( $user->language, $availableLanguages )) {
					Yii::app()->setLanguage( $user->language );
					return;
				}
			}
			if( $user and !$user->language ) {
				$preferredLanguages = Yii::App()->request->getPreferredLanguages();
				$preferredLanguages = array_intersect( $preferredLanguages, $availableLanguages );
				if( $preferredLanguages ) {
					$language = array_shift( $preferredLanguages );
					Yii::app()->setLanguage( $language );
					$user->language = $language;
					$user->save();
					return;
				}
			}
			
		}
		protected function createLayoutBag() {
			$this->layoutBag = new StdClass();
			$this->setLayoutTitle( $this->detLayoutTitle());
		}
		function detLayoutTitle() {
			return "";
		}
		function setLayoutTitle( $title ) {
			$this->layoutBag->title = $title;
		}
		function getLayoutTitle() {
			return @$this->layoutBag->title;
		}
		function setLayoutOgSection( $title ) {
			$this->layoutBag->ogSection = $title;
		}
		function getLayoutOgSection() {
			return @$this->layoutBag->ogSection;
		}
		
		function setLayoutOgImage( $img ) {
			$this->layoutBag->ogImage = $img;
		}
		function getLayoutOgImage() {
			return @$this->layoutBag->ogImage;
		}
		
		function setLangSwitcher( $model ) {
			if( $model instanceof ModelBase ){
				
				ob_start();
				$this->widget( 'widgets.LangSwitcherWidget', Array( 'availableLangs' => $model->availableLangs ) );
				$content = ob_get_clean();
				
				$this->layoutBag->langSwitcher = $content;
				
			}elseif( $model === true ){

				ob_start();
				$this->widget( 'widgets.LangSwitcherWidget', Array());
				$content = ob_get_clean();
				
				$this->layoutBag->langSwitcher = $content;
				
			}else{
				$this->layoutBag->langSwitcher = '';
			}
		}
		function getLangSwitcher() {
			if( isset( $this->layoutBag->langSwitcher ) ) return $this->layoutBag->langSwitcher;
			return false;
		}
		
		function setMainItemscopeItemtype( $item = false ) {
			$this->layoutBag->itemscopeItemtype = $item ? $item : 'CreativeWork';
		}
		function getMainItemscopeItemtype() {
			if( isset( $this->layoutBag->itemscopeItemtype ) ) return $this->layoutBag->itemscopeItemtype;
			return 'CreativeWork';
		}
		function setLayoutDescription( $description ) {
			return $this->layoutBag->description = $description;
		}
		function getLayoutDescription() {
			return @$this->layoutBag->description;
		}
		function setLayoutKeywords( $keywords ) {
			return $this->layoutBag->keywords = $keywords;
		}
		function getLayoutKeywords() {
			return @$this->layoutBag->keywords;
		}
		function setLayoutBodyClass( $class ) {
			$this->layoutBag->bodyClass = $class;
		}
		function getLayoutBodyClass() {
			return @$this->layoutBag->bodyClass;
		}
		protected function beforeAction( $action ) {
			if( YII_DEBUG and defined( 'PROFILING_ACTION' )) {
				Yii::beginProfile($this->action->id);
			}
			
			if( Yii::app()->controller->id == 'default' && Yii::app()->controller->action->id == 'error' ) return true;
			
			$moduleName = @Yii::app()->controller->module->id;
			//ksort($_GET);
			$url = Yii::app()->controller->createUrl('', $_GET);
			if (Yii::app()->request->getUrl() != $url && $moduleName!='admin' ) {
				header("HTTP/1.1 301 Moved Permanently");
				header('Location: '.$url);
			}
			
			return true;
		}
		protected function afterAction( $action ) {
			if( YII_DEBUG and defined( 'PROFILING_ACTION' )) {
				Yii::endProfile($this->action->id);
			}
			return true;
		}
		protected function resolveClassName( $class ) {
			return $class ? $class : get_class( $this );
		}
		function getCleanClassName( $class = null ) {
			$class = $this->resolveClassName( $class );
			$class = preg_replace( "#Controller$#", "", $class );
			$class[0] = strtolower($class[0]);
			return $class;
		}
		protected function getNSi18n( $class = null ) {
			$class = $this->getCleanClassName( $class );
			if( $class == "controllerBase" ) {
				return "controllers/base/{$class}";
			}
			return "controllers/{$class}";
		}
		protected function getActions( $alias ) {
			$actions = Array();
			Yii::import( "{$alias}.*" );
			$path = Yii::getPathOfAlias( $alias );
			$files = glob( "{$path}/*Action.php", GLOB_NOSORT );
			if( $files ) {
				foreach( $files as $file ) {
					$fileName = basename( $file );
					$className = preg_replace( "#\.php$#i", "", $fileName );
					$clearClassName = ActionBase::getActionCleanClassName( $className );
					$actions[ $clearClassName ] = $className;
				}
			}
			return $actions;
		}
		public function render($view,$data=null,$return=false){
			Yii::app()->session['canCreate'] = false;
			return parent::render( $view, $data, $return );
		}
		function filters() {
			return Array(
				'accessControl',
			);
		}
		function accessRules() {
			return Array(
				Array( 'allow' ),
			);
		}
	}

?>
