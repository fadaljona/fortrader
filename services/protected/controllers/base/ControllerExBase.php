<?
	abstract class ControllerExBase extends CController {
		public $class;
		public $modelName;
		public $view;
		public $layout;
		public $layoutTitle;
		public $layoutDescription;
		public $layoutKeywords;
		public $layoutBodyClass;
		public $layoutInfo;
		public $NSi18n;
		public $delimLayoutTitle;
		public $actionLevel;
		public $commonBag;
			// constructor
		function __construct( $id, $module = null ) {
			parent::__construct( $id, $module );
			$this->commonBag = new StdClass();
			$this->determLanguage();
			
			$this->getLayout();
		}
			// dets
		function detClass() {
			return $class = get_class( $this );
		}
		function detModelName() {
			$class = $this->getClass();
			return $modelName = preg_replace( "#Controller$#", "", $class );
		}
		function detView() {
			$controller = $this->getClass();
			return $view = "{$controller}/{$this->action->id}";
		}
		function detLayout() {
			return $layout = '/_layouts/default/index';
		}
		function detLayoutTitle() {
			$modelName = $this->getModelName();
			return $layoutTitle = "{$modelName}";
		}
		function detLayoutDescription() {
			return $layoutDescription = "";
		}
		function detLayoutKeywords() {
			return $layoutKeywords = "";
		}
		function detLayoutBodyClass() {
			return "";
		}
		function detLayoutInfo() {
			return $layoutLayoutInfo = "";
		}
		function detNSi18n() {
			$class = $this->getClass();
			return $NSi18n = "modules/admin/controllers/{$class}";
		}
		function detDelimLayoutTitle() {
			return $delimLayoutTitle = ' - ';
		}
		function determLanguage() {
			$availableLanguages = LanguageModel::getExistsAliases();
			$cookieKey = Yii::App()->params[ 'languageCookieVarName' ];
			$cookieLanguage = &$_COOKIE[ $cookieKey ];
			$user = Yii::App()->user->getModel();
			if( !empty( $cookieLanguage )) {
				if( in_array( $cookieLanguage, $availableLanguages )) {
					Yii::app()->setLanguage( $cookieLanguage );
					if( $user and $user->language != $cookieLanguage ) {
						$user->language = $cookieLanguage;
						$user->save();
					}
					return;
				}
			}
			if( $user and $user->language ) {
				if( in_array( $user->language, $availableLanguages )) {
					Yii::app()->setLanguage( $user->language );
					return;
				}
			}
			if( $user and !$user->language ) {
				$preferredLanguages = Yii::App()->request->getPreferredLanguages();
				$preferredLanguages = array_intersect( $preferredLanguages, $availableLanguages );
				if( $preferredLanguages ) {
					$language = array_shift( $preferredLanguages );
					Yii::app()->setLanguage( $language );
					$user->language = $language;
					$user->save();
					return;
				}
			}
		}
			// form CController
		protected function beforeAction( $action ) {
			if( YII_DEBUG and defined( 'PROFILING_ACTION' )) {
				Yii::beginProfile($this->action->id);
			}
			
			$moduleName = Yii::app()->controller->module->id;
			//ksort($_GET);
			$url = Yii::app()->controller->createUrl('', $_GET);
			if (Yii::app()->request->getUrl() != $url && $moduleName!='admin' ) {
				header("HTTP/1.1 301 Moved Permanently");
				header('Location: '.$url);
			}
			
			return parent::beforeAction( $action );
		}
		protected function afterAction( $action ) {
			if( YII_DEBUG and defined( 'PROFILING_ACTION' )) {
				Yii::endProfile($this->action->id);
			}
			return parent::afterAction( $action );
		}
		function getViewPath() {
			if(( $module = $this->getModule()) === null ) $module = Yii::App();
			return $viewPath = $module->getViewPath();
		}
		function filters() {
			return $filters = Array(
				'accessControl',
			);
		}
		function accessRules() {
			return $accessRules = Array(
				Array( 'allow' ),
			);
		}
			// common
		function extendLayoutTitle( $titles, $pattern = null ) {
			$NSi18n = $this->getNSi18n();
			
			if( is_string( $titles )) $titles = Array( '{title}' => Yii::t( $NSi18n, $titles ));
			
			if( !$pattern )	{
				if( $this->getActionLevel() >= 2 ) {
					$pattern = "{title}{-}{controllerTitle}{-}{appName}";
				}
				else {
					$pattern = "{controllerTitle}{-}{title}{-}{appName}";
				}
			}
						
			$params = Array(
				'{controllerTitle}' => Yii::t( $NSi18n, $this->detLayoutTitle() ),
				'{appName}' => Yii::t( $NSi18n, Yii::App()->name ),
				'{-}' => $this->getDelimLayoutTitle(),
			);
			$params = array_merge( $params, $titles );
			
			$title = strtr( $pattern, $params );
			$this->layoutTitle = $title;
						
			return $title;
		}
		function translateR( $array, $params=array() ) {
			$NSi18n = $this->getNSi18n();
			return Yii::t( $NSi18n, $array );	
		}
		function throwI18NException( $message, $params = Array() ) {
			$NSi18n = $this->getNSi18n();
			throw new Exception( Yii::t( $NSi18n, $message, $params ));
		}
			// render
		function render( $view = null, $data = Array(), $return = false ) {
			if( !strlen( $view )) $view = $this->getView();
			return parent::render( $view, $data, $return );
		}
			// actions
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function actionList() {
			$this->extendLayoutTitle( "List" );
			$this->render();
		}
		function actionAjaxLoadList() {
			$data = Array();
			try{
				$listWidget = ListWidgetBase::createWidgetByModelName( $this->getModelName());
				$data[ 'list' ] = $listWidget->run( $view = null, $data = Array(), $captureOutput = true );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>
