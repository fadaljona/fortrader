<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ImgAction extends ActionFrontBase {
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Economic calendar',
				)
			);
		}
		function run() {
			$res = array();
			$model = CalendarEvent2ValueModel::model()->find('indicator_id='.$_GET['id']);
			$res['src'] = $model->getNameImage();
			$res['src_t1'] = $model->getNameMiddleImage();
			$res['src_t2'] = $model->getNameThumbImage();
			$res = json_encode($res);
			echo $res;
		}
	}

?>
