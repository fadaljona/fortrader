<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadShortHistoryAction extends ActionFrontBase {
		private function createWidget( $id, $dt ) {
			$list = $this->controller->createWidget( 'widgets.lists.CalendarEventShortHistoryListWidget', Array(
				'id' => $id,
				'dt' => $dt,
			));
			return $list;
		}
		private function renderList( $widget ) {
			ob_start();
			$widget->renderTableBody();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $id, $dt ) {
			$data = Array();
			try{
				$widget = $this->createWidget( $id, $dt );
				$data[ 'list' ] = $this->renderList( $widget );
				$data[ 'count' ] = $widget->getTotalItemCount();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>