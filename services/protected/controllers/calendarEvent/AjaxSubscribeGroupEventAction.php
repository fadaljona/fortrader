<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.CalendarGroupSubscriptionFormModel' );

	final class AjaxSubscribeGroupEventAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
            $this->formModel = new CalendarGroupSubscriptionFormModel();
			$load = $this->formModel->load(Yii::app()->user->id);
			$this->formModel->idLanguage = LanguageModel::getCurrentLanguageID();
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				if( !Yii::app()->user->id ) $this->throwI18NException( "User need to be loged in" );
				$this->detFormModel();
				$formModel = $this->getFormModel();

				
				if( $formModel->validate()) {
					if( !$formModel->save() ) $this->throwI18NException( "Can't save AR!" );
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
                $data[ 'error' ] = $e->getMessage();
			}
			
			echo json_encode( $data );
		}
	}

?>