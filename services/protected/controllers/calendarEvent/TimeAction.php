<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class TimeAction extends ActionFrontBase {
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Economic calendar',
				)
			);
		}
		function run($id) {
			$model = CalendarEvent2ValueModel::model()->findByPk($id);
			echo $model->getTimeLeftToBtn();
		}
	}

?>
