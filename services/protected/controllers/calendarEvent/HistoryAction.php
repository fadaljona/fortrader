<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class HistoryAction extends ActionFrontBase {
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Economic calendar',
				)
			);
		}
		function run($id, $limit=12, $sel=0) {
			$criteria = new CDbCriteria();
			$criteria->condition = 'indicator_id = '.$id;
			$criteria->order = 'dt DESC';
			$criteria->limit = $limit;
			$criteria->offset = 0;
			$model = CalendarEvent2ValueModel::model()->findAll($criteria);

			$s = '';
			$s1 = '';

			foreach ($model as $one) {
				$s .= $one->getFactValue();
				$s1 .= $one->getFactValueSel();
			}

			if ( $s == '' and $s1 == '' ) {
				return false;
			}

			if ( count($model) > 0 ) {
				$this->controller->render( "/calendarEvent/history/view", Array(
					'model' => $model,
					'limit' => $limit+3, 
					'sel' => $sel, 
					'model0' => $model[0], 
					's' => $s, 
					's1' => $s1
				));
			}else{
				return false;
			}
		}
	}

?>
