<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class ExportAction extends ActionFrontBase {
        private $delimiter = ";";

        private function getModels(){
            return CalendarEvent2ValueModel::model()->findAll(array(
                'with' => array(
                    'event' => array(
                        'with' => array( 'currentLanguageI18N', 'country')
                    ),
                ),
                'condition' => ' `t`.`dt` >= NOW() AND `t`.`dt` <= DATE_ADD(NOW(),INTERVAL 2 WEEK) AND `cLI18NCalendarEvent`.`indicator_name` <> "" AND `cLI18NCalendarEvent`.`indicator_name` IS NOT NULL ',
                'order' => ' `t`.`dt` ASC '
            ));
        }
		private function downloadCsv() {
            $models = $this->getModels();
            if( !$models ) return false;

            $filename='fortrader_economic_events_'.date('Y-m-d') . '__' . date('Y-m-d') .'.csv';

            header('Content-Type: application/csv; charset=UTF-8');
            header('Content-Disposition: attachment; filename="'.$filename.'";');
            echo "\xEF\xBB\xBF";
        
            echo Yii::t('*','Date') .$this->delimiter. Yii::t('*','Event title') .$this->delimiter. Yii::t('*','Country') .$this->delimiter. Yii::t('*','Importance') . PHP_EOL;

            foreach ($models as $model) {
                echo $model->dt .$this->delimiter. $model->event->currentLanguageI18N->indicator_name .$this->delimiter. $model->event->country->localName .$this->delimiter. Yii::t('*', ucfirst($model->titleSerious) ) . PHP_EOL;
            }
        }
        private function downloadXml(){
            $models = $this->getModels();
            if( !$models ) return false;

            $filename='fortrader_economic_events_'.date('Y-m-d') . '__' . date('Y-m-d') .'.xml';

            $xml = new SimpleXMLElement('<xml/>');

            foreach ($models as $model) {
                $event = $xml->addChild('event');
                $event->addChild('date', $model->dt);
                $event->addChild('title', $model->event->currentLanguageI18N->indicator_name);
                $event->addChild('country', $model->event->country->localName);
                $event->addChild('importance', Yii::t('*', ucfirst($model->titleSerious) ));
            }

            Header('Content-type: text/xml; charset=UTF-8');
            header('Content-Disposition: attachment; filename="'.$filename.'";');
            print($xml->asXML());

        }
		function run( $type ) {
            if( $type == 'csv' ){
                $this->downloadCsv();
            }elseif( $type == 'xml' ){
                $this->downloadXml();
            }
           
		}
	}

?>
