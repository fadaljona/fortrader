<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadRowsAction extends ActionFrontBase {
		private function getDP( $ids ) {
			$c = new CDbCriteria();
			
			$c->addInCondition( "`t`.`id`", $ids );
			
			$DP = new CActiveDataProvider( 'CalendarEvent2ValueModel', Array(
				'criteria' => $c,
			));
			if( !$DP->getTotalItemCount( )) $this->throwI18NException( "Can't find Events!" );
			return $DP;
		}
		private function getWidget( $DP ) {
			$widget = $this->controller->createWidget( 'widgets.gridViews.CalendarEventsGridViewWidget', Array(
				'dataProvider' => $DP,
			));
			return $widget;
		}
		private function renderRows( $DP, $widget ) {
			$rows = Array();
			$list = $DP->getData();
			foreach( $list as $i=>$data ) {
				ob_start();
				$widget->renderTableRow( $i );
				$rows[ $data->id ] = ob_get_clean();
			}
			return $rows;
		}
		function run() {
			$data = Array();
			try{
				$ids = @$_POST[ 'ids' ];
				$ids = explode( ',', $ids );
				$ids = array_map( 'intval', $ids );
								
				$DP = $this->getDP( $ids );
				$widget = $this->getWidget( $DP );
				
				$data[ 'rows' ] = $this->renderRows( $DP, $widget );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>