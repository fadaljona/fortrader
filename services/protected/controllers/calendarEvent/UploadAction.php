<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class UploadAction extends ActionFrontBase {
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Economic calendar',
				)
			);
		}
		function run() {
			Yii::import("ext.EAjaxUpload.qqFileUploader");

			$parent_id=md5(Yii::app()->user->id);
			$timestamp = date('mdY',time());
			
			$prefolder = realpath('uploads/');
			$userfolder = $prefolder.'/calendar/';
			$folder = $userfolder.$timestamp.'/';

			if( !is_dir($folder) ){
				if( !is_dir($userfolder) ) {
					if ( !is_dir($prefolder) ) {
						mkdir($prefolder);
					}
					mkdir($userfolder);
				}
				mkdir($folder);
			}

			/*print_r(array(
				'2'=>$folder
			));

			die;*/

			$allowedExtensions = array("jpg","jpeg","gif","png");
			$sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
			$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
			$result = $uploader->handleUpload($folder);

			//$fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
			//$fileName=$result['filename'];//GETTING FILE NAME
			
			$image = Yii::app()->image->load($folder.$result['filename']);
			
			$image->resize(600,600,Image::AUTO)->crop(600, 600, 0, 0)->quality(90);
			$image->save($folder.'t1_'.$result['filename']);
			
			$image->resize(200,200,Image::AUTO)->crop(200, 200, 0, 0)->quality(80)->sharpen(20);
			$image->save($folder.'t2_'.$result['filename']);
			
			$img = getimagesize($folder.$result['filename']);
			$w = $img[0];
			$h = $img[1];
			if ( $h > 720 and $w > 1280 ) {
				$image->resize(1280,720,Image::AUTO)->crop(1280, 720, 0, 0)->quality(90);
				$image->save($folder.'r_'.$result['filename']);
				unlink($folder.$result['filename']);
				$name = 'r_'.$result['filename'];
			}else{
				$name = $result['filename'];
			}

			if ($result){
				$result['src'] = '/services/uploads/calendar/'.$timestamp.'/'.$name;
				$result['src_t1'] = '/services/uploads/calendar/'.$timestamp.'/t1_'.$result['filename'];
				$result['src_t2'] = '/services/uploads/calendar/'.$timestamp.'/t2_'.$result['filename'];
				$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
				echo $return;  //it's array
			}else{
				unlink($folder.$name);
				unlink($folder.'t1_'.$result['filename']);
				unlink($folder.'t2_'.$result['filename']);
			}

			Yii::app()->end();
		}
	}

?>
