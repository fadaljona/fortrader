<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		private $cat = 'singleClendarEvent';
		private $paramStr;

		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs() {
			
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Economic calendar',
					'url' => Array( '/calendarEvent/list' ),
				),
				(object)Array( 
					'label' => $this->model->indicatorName,
				)
			);
		}
		private function getModel( $slug ) {
			$model = CalendarEvent2Model::findBySlugWithLang( $slug );
			if( !$model ){
				if( is_numeric($slug) ){
					$valModel = CalendarEvent2ValueModel::model()->find(array(
						'with' => array( 'event' => array( 'with' => array( 'currentLanguageI18N' ) ) ),
						'condition' => " `t`.`id` = :valId AND `cLI18NCalendarEvent`.`indicator_name` <> '' AND `cLI18NCalendarEvent`.`indicator_name` IS NOT NULL ",
						'params' => array( ':valId' => $slug )
					));
					if( $valModel && $valModel->event ){
						$this->redirect( $valModel->event->singleURL, true, 301 );
					}
				}
				throw new CHttpException(404,'Page Not Found');
			}
			return $model;
		}
		private function setMeta() {

			$metaTitle = $this->model->metaTitle;

			$this->metaDesc = $this->model->metaDesc;
			$this->metaKeys = $this->model->metaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = $this->model->indicatorName;
			}
			
			$this->title = $this->model->pageTitle;
			if( !$this->title ) $this->title = $this->model->indicatorName;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			/*if( $this->model->ogImage ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $this->model->absoluteSrcOgImage );
			}*/
		}

		function run( $slug ) {	

			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $slug );	
			$this->paramStr = "calendarEvent_{$this->model->id}";
			$this->setMeta();
			$this->setLangSwitcher($this->model);
			
			/*$this->setLangSwitcher($this->model);*/
			
			/*$this->setLayoutBodyClass('bootstrapColorpickerPlus');*/
		
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();

						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'event' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				/*'metaDesc' => $this->metaDesc*/
			));
		}
	}

?>
