<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadInformerEventsListAction extends ActionFrontBase {
		private function renderList( ) {
			ob_start();
			
			$this->controller->widget( 'widgets.lists.CalendarInformerEventsListWidget', Array(
				'ns' => 'nsActionView',
				'impact' => $_GET['impact'],
				'symbols' => $_GET['symbols'],
			));
			
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( ) {
			$data = Array();
			try{
				$data[ 'list' ] = $this->renderList( $id );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>