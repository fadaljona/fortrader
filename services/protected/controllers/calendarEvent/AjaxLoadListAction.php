<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadListAction extends ActionFrontBase {
		private function createWidget( $period, $date, $idsSkip ) {
			$list = $this->controller->createWidget( 'widgets.lists.CalendarEventsList2Widget', Array(
				'period' => $period,
				'date' => $date,
				'idsSkip' => $idsSkip,
			));
			return $list;
		}
		private function renderList( $widget ) {
			ob_start();
			$widget->renderTableBody();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $period, $date = null, $idsSkip = null ) {
			$data = Array();
			try{
				if( strlen( $idsSkip )) $idsSkip = explode( ',', $idsSkip );
				$widget = $this->createWidget( $period, $date, $idsSkip );
				$data[ 'list' ] = $this->renderList( $widget );
				
				list( $begin, $end, $more ) = $widget->getRange();
				$data[ 'moreDate' ] = $more;
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>
