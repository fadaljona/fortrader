<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ComingAction extends ActionFrontBase {
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Economic calendar',
				)
			);
		}
		function run($limit=3, $offset=0) {
			$criteria = new CDbCriteria();
			$criteria->condition = 'dt > "'.date( "Y-m-d H:i", time()).'"';
			$criteria->order = 'dt';
			$criteria->limit = $limit;
			$criteria->offset = $offset;
			$model = CalendarEvent2ValueModel::model()->findAll($criteria);

			$this->controller->render( "/calendarEvent/coming/view", Array(
				'model' => $model,
				'limit' => $limit+3
			));
		}
	}

?>
