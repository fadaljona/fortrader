<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'components.CalendarEventApiComponent' );

	final class ApiExportListAction extends ActionFrontBase {
		function run() {
			$component = new CalendarEventApiComponent();
			$component->exportList();
		}
	}

?>