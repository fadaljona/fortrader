<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadCalendarComingEventsAction extends ActionFrontBase {
		private function createWidget( $dt ) {
			$list = $this->controller->createWidget( 'widgets.CalendarComingEventsWidget', Array(
				'fromDt' => $dt,
			));
			return $list;
		}
		private function renderList( $widget ) {
			return $widget->renderList();
		}
		function run( $dt ) {
			$data = Array();
			try{
				$widget = $this->createWidget( $dt );
				$data[ 'list' ] = $this->renderList( $widget );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>