<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class AjaxUnSubscribeSingleEventAction extends ActionFrontBase {
		
		function run( ) {
			$idEvent = $_POST['idEvent'];
			

			$data = Array();
			try{
				if( !Yii::app()->user->id ) $this->throwI18NException( "User need to be loged in" );

				$model = CalendarSingleSubscriptionModel::model()->findByIdUserIdEvent( Yii::app()->user->id, $idEvent );
				if( !$model ) $this->throwI18NException( "Can't find subscription" );
				if( !$model->delete() ) $this->throwI18NException( "Can't delete subscription" );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			
			echo json_encode( $data );
		}
	}

?>