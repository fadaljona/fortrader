<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ListAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Economic calendar';
		public $metaDesc;
		public $metaKeys;
		public $settings;
		
		private $cat = 'indexEconomicCalendar';
		private $paramStr;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Economic calendar',
					'url' => Array( '/calendarEvent/list' ),
				)
			);
		}
		private function setMeta() {
			$this->settings = CalendarEventSettingsModel::getModel();
			
			$metaTitle = $this->settings->metaTitle;

			$this->metaDesc = $this->settings->metaDesc;
			$this->metaKeys = $this->settings->metaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->settings->title;
			if(!$this->title) $this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;
			
			$listWidget = $this->controller->createWidget( 'widgets.lists.CalendarEventsList2Widget', Array());
		
			if( $listWidget->getPeriod() == 'today' ){
				$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			}else{
				list( $begin, $end, $more, $rangeTitle ) = $this->controller->createWidget( 'widgets.lists.CalendarEventsList2Widget', Array())->getRange();
				$this->setLayoutTitle( $this->metaTitle, "{title}{-}{$rangeTitle}{-}{appName}" );
			}
			
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$settingImg = $this->settings->ogImageSrc;
			if( $settingImg ){
				$this->setLayoutOgSection();
				$this->setLayoutOgImage( $settingImg );
			}
			$this->setLangSwitcher(true);
		}
		function run() {
			$this->setMeta();
			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->paramStr = "economic_calendar_index";
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
			//	'cat' => $this->cat,
			//	'paramStr' => $this->paramStr,
			//	'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>