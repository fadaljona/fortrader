<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLoadUpdatedListAction extends ActionFrontBase {
		private function createWidget( $period, $date, $idsSkip ) {
			$list = $this->controller->createWidget( 'widgets.lists.CalendarEventsList2Widget', Array());
			return $list;
		}
		private function renderList( $widget ) {
			ob_start();
			$widget->renderTableBody();
			$content = ob_get_clean();
			$content = preg_replace( "#[\r\n\t]+#", " ", $content );
			
			return $content;
		}
		function run( $period, $d_view = null ) {
			$data = Array();
			try{
				$widget = $this->createWidget( $period, $d_view );
				$data[ 'list' ] = $this->renderList( $widget );
				
				list( $begin, $end, $more ) = $widget->getRange();
				$data[ 'moreDate' ] = $more;
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>
