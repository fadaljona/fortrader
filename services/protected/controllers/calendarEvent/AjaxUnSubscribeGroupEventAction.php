<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class AjaxUnSubscribeGroupEventAction extends ActionFrontBase {
		
		function run( ) {
		
			

			$data = Array();
			try{
                if( !Yii::app()->user->id ) $this->throwI18NException( "User need to be loged in" );
				$model = CalendarGroupSubscriptionModel::model()->find(array(
                    'condition' => ' `t`.`idUser` = :idUser ',
                    'params' => array( ':idUser' => Yii::app()->user->id )
                ));
				if( !$model ) $this->throwI18NException( "Can't find subscription" );
				if( !$model->delete() ) $this->throwI18NException( "Can't delete subscription" );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			
			echo json_encode( $data );
		}
	}

?>