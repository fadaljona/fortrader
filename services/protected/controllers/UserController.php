<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class UserController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Users";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'deny', 'actions' => Array( 'ajaxAddFriend', 'ajaxAcceptInviteFriend', 'ajaxRejectInviteFriend' ), 'users' => Array( '?' )),
				Array( 'allow' ),
			);
		}
		public function filters(){
			return array(
				'accessControl',
				'servicesRedirect',
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$url = str_replace( '/services', '', Yii::App()->request->getUrl() );
				
				$this->redirect( $url, true, 301 );
			}
			$filterChain->run();			
		}
	}

?>