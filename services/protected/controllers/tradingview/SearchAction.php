<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class SearchAction extends ActionAdminBase {
		function run( $limit, $query, $type ) {
			if( !$query && !$type ) {echo json_encode( array() ); return true;};
			
			if( $query && $type ){
				$condition = " `t`.`category` = :category AND ( `t`.`name` LIKE :query OR `cLI18NQuotesSymbolsModel`.`nalias` LIKE :query OR `cLI18NQuotesSymbolsModel`.`tooltip` LIKE :query ) ";
				$params = array( ':category' => $type, ':query' => '%'.$query.'%' );
			}elseif( $query && !$type ){
				$condition = "  `t`.`name` LIKE :query OR `cLI18NQuotesSymbolsModel`.`nalias` LIKE :query OR `cLI18NQuotesSymbolsModel`.`tooltip` LIKE :query  ";
				$params = array( ':query' => '%'.$query.'%' );
			}elseif( !$query && $type ){
				$condition = " `t`.`category` = :category  ";
				$params = array( ':category' => $type );
			}
			
			$models = QuotesSymbolsModel::model()->findAll(array(
				'with' => array( '_category', 'currentLanguageI18N' ),
				'condition' => $condition,
				'params' => $params,
				'limit' => $limit,
			));
			
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = array(
					"symbol" => $model->name,
					"full_name" => $model->name,
					"description" => $model->tooltip,
					"type" => $model->_category->name,
					"exchange" => Yii::t( "*","All Exchanges"),
				);
			}
			echo json_encode( $outArr );
		}
	}
?>