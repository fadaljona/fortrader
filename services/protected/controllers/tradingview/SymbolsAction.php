<?
    Yii::import( 'controllers.base.ActionAdminBase' );
    
    final class SymbolsAction extends ActionAdminBase {
        
        public $zones = array(
            '0' => 'UTC',
            '-300' => 'America/New_York',
            '-480' => 'America/Los_Angeles',
            '-360' => 'America/Chicago',
            '-180' => 'America/Argentina/Buenos_Aires',
            '180' => 'Europe/Moscow',
            '120' => 'Europe/Athens',
            '60' => 'Europe/Berlin',
            '600' => 'Australia/Sydney',
            '420' => 'Asia/Bangkok',
            '540' => 'Asia/Tokyo',
            '480' => 'Asia/Singapore',
            '540' => 'Asia/Seoul',
            '330' => 'Asia/Kolkata',
        );
    
        function run( $symbol ) {
            $model = QuotesSymbolsModel::findByName($symbol);
            if( !$model ) $this->throwI18NException( "Can't find Symbol!" );
            
            if( $model->chartLabel ){
                $label = $model->chartLabel;
            }elseif( $model->tooltip ){
                $label = $model->tooltip;
            }else{
                $label = $model->name;
            }
            
            if( $model->sourceType == 'bitz' ){
                $timeZone = 'Asia/Hong_Kong';
            }elseif( $model->sourceType == 'bitfinex' || $model->sourceType == 'binance' ){
                $timeZone = 'UTC';
            }elseif( $model->sourceType == 'okex' ){
                $timeZone = 'Asia/Hong_Kong';
            }else{
                $quotesSettings = QuotesSettingsModel::getInstance();
                $timeZone = $quotesSettings->statsDataTimeZone;
            }
            
            
            if( $model->tickData ){
                $precision = $model->tickData->precision;
            }else{
                $precision = 2;
            }
            $pricescale = pow(10, $precision);
            
            echo '{"name":"'.$model->name.'","ticker":"'.$model->name.'","description":"'.$label.'","type":"'.$model->category.'","supported_resolutions":'.QuotesHistoryDataModel::getResolutionsForSymbol( $model->id ).',"has_intraday":true,"has_daily":true,"has_weekly_and_monthly":true,"timezone":"' . $timeZone . '", "intraday_multipliers":["1","5","15","30","60","240"],"minmov":1,"minmov2":0,"pricescale":'.$pricescale.' }';
        }
    }

?>