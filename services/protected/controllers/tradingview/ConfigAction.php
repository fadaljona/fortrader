<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class ConfigAction extends ActionAdminBase {
	
		function run() {
			$categories = QuotesCategoryModel::model()->findAll(array(
				'with' => array('symbols'),
				'condition' => " `is_def` = 0 "
			));
			$symbolsTypes = array(
				array(
					"name" => Yii::t( "*",'All types'),
					"value" => ""
				)
			);
			foreach( $categories as $category ){
				if( $category->symbols ){
					$symbolsTypes[] = array(
						"name" => $category->name,
						"value" => $category->id
					);
				}
			}
			$config = array(
				"supports_search" => true,
				"supports_group_request" => false,
				"supports_marks" => false,
				"supports_timescale_marks" => false,
				"supports_time" => true,
				"symbolsTypes" => $symbolsTypes,
				"supportedResolutions" => array(/*"1", "5", "15", "30", */"60", "240", "D", "W", "M"),
			);
			
			echo json_encode($config);
		}
	}

?>