<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class CreateImageAction extends ActionAdminBase {
		private $panesContent;
		private $panesRightAxisContent;
		private $timeAxisContent;
		private $images;

		
		private function setupImages(){

			if( ! isset( $_POST['images'] ) ) $this->throwI18NException( "Can't find image!" );
			$this->images = json_decode($_POST['images']);			

			$timeAxisContent = $this->images->charts[0]->timeAxis->content;
			$timeAxisContentBase64 = $this->getImgData($timeAxisContent);	

			
			$mainImg = $this->getSummImg( $this->images->charts[0]->panes );
			$rightAxisImg = $this->getSummImg( $this->images->charts[0]->panes, true );
			
			$this->panesContent['Im'] = $mainImg;
			$this->panesRightAxisContent['Im'] = $rightAxisImg;
			$this->timeAxisContent['Im'] = imagecreatefromstring( $timeAxisContentBase64 );
			
			if( $this->panesContent['Im'] === false || $this->panesRightAxisContent['Im'] === false || $this->timeAxisContent['Im'] === false ) $this->throwI18NException( "Can't create image!" );
			
			$this->panesContent['width'] = imagesx($mainImg);
			$this->panesContent['height'] = imagesy($mainImg);

			$this->panesRightAxisContent['width'] = imagesx($rightAxisImg);
			$this->panesRightAxisContent['height'] = imagesy($rightAxisImg);
			
			$sizes = getimagesizefromstring( $timeAxisContentBase64 );
			$this->timeAxisContent['width'] = $sizes[0];
			$this->timeAxisContent['height'] = $sizes[1];
		}
		private function getSummImg( $panes, $rightAxis = false ){
			
			$panesContent = $panes[0]->content;
			
			if( $rightAxis ) $panesContent = $panes[0]->rightAxis->content;
			
			$panesContentBase64 = $this->getImgData($panesContent);
			$sizes = getimagesizefromstring( $panesContentBase64 );
			
			$mainImg = imagecreatetruecolor ( 0 , 0 );
			foreach( $panes as $id => $pane ){
				
				$content = $pane->content;
				if( $rightAxis ) $content = $pane->rightAxis->content;
				
				$tmpNewImg = imagecreatefromstring( $this->getImgData($content) );
				
				if( isset( $pane->studies ) && $pane->studies[0] && !$rightAxis ){
					
					$font = Yii::app()->basePath . '/data/arial.ttf';
					$textColor = imagecolorallocate($tmpNewImg, 0, 0, 0);
					$startYPos = 18;
					$iterYPos = 18;
					
					$idStudy = 0;
					foreach( $pane->studies as  $studyText ){
						if( strpos( $studyText, 'Объём' ) !== false ) continue;
						imagettftext($tmpNewImg, 8, 0, 15, $startYPos + $iterYPos*$idStudy , $textColor, $font, $studyText );
						$idStudy ++;
					}
					
				}
				
				$tmpNewImgW = imagesx($tmpNewImg);
				$tmpNewImgH = imagesy($tmpNewImg);
				
				$mainImgW = imagesx($mainImg);
				$mainImgH = imagesy($mainImg);
				
				$tmpMainImg = imagecreatetruecolor ( $sizes[0] , $tmpNewImgH + $mainImgH );

				imagecopymerge ( $tmpMainImg , $mainImg , 0 , 0 , 0 , 0 , $mainImgW , $mainImgH , 100 );
				imagecopymerge ( $tmpMainImg , $tmpNewImg , 0 , $mainImgH , 0 , 0 , $tmpNewImgW , $tmpNewImgH , 100 );
				
				$mainImg = $tmpMainImg;
			}
			return $mainImg;
		}
		private function getImgData( $data ){
			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);
			return $data = base64_decode($data);
		}
		function run() {
			$this->setupImages();
			
			$Im = imagecreatetruecolor( $this->panesContent['width'] + $this->panesRightAxisContent['width'] , $this->panesContent['height'] + $this->timeAxisContent['height'] );
			if( $Im === false ) $this->throwI18NException( "Can't create image!" );
			
			$red = imagecolorallocate( $Im, 255, 255, 255);
			imagefill($Im, 0, 0, $red);

			imagecopymerge($Im, $this->panesContent['Im'], 0, 0, 0, 0, $this->panesContent['width'], $this->panesContent['height'], 100);
			imagecopymerge($Im, $this->panesRightAxisContent['Im'], $this->panesContent['width'], 0, 0, 0, $this->panesRightAxisContent['width'], $this->panesRightAxisContent['height'], 100);
			imagecopymerge($Im, $this->timeAxisContent['Im'], 0, $this->panesContent['height'], 0, 0, $this->timeAxisContent['width'], $this->timeAxisContent['height'], 100);
			
			
			ob_start();
			imagepng($Im);
			$contents =  ob_get_contents();
			ob_end_clean();
			
			echo 'data:image/png;base64,' . base64_encode($contents);
			
			imagedestroy($Im);
			imagedestroy($this->panesContent['Im']);
			imagedestroy($this->panesRightAxisContent['Im']);
			imagedestroy($this->timeAxisContent['Im']);
				

			
		}
	}
?>