<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class HistoryAction extends ActionAdminBase {
		function run( $symbol, $resolution, $from, $to ) {	
		
			$model = QuotesSymbolsModel::findByName($symbol);
			if( !$model ) $this->throwI18NException( "Can't find Symbol!" );
			
			$periods = array_flip( QuotesHistoryDataModel::$resolutionTradingview );
			
			if( !isset( $periods[$resolution] ) ) $this->throwI18NException( "Can't find resolution" );
			
			echo QuotesHistoryDataModel::getDataForTradingviewChart( $model->id, $periods[$resolution], $from, $to );
			

			
		}
	}

?>