<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class IndexAction extends ActionFrontBase {
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Chat',
				),
			);
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			//$this->setLayoutTitle( Array() );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>