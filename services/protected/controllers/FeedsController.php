<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class FeedsController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Feeds";
		}
		function accessRules(){
			return Array(
				/*array(
					'deny',
					'actions' => array('single', 'index'),
					'expression' => array('FeedsController','allowOnlyAdmin'),
					'users'=>array('*')
				),*/
			);
		}
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl'
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$url = str_replace( '/services', '', Yii::App()->request->getUrl() );
				
				$this->redirect( strtolower($url), true, 301 );
			}
			$filterChain->run();			
		}
		public static function allowOnlyAdmin(){
			if( Yii::App()->user->id == 1 || $_SERVER['REMOTE_ADDR'] == '127.0.0.1' ) return false;
			return true;
		}
		/*public function singleSiteMapArgs(){
			$models = RssFeedsFeedModel::model()->findAll(array(
				'select' => ' `slug` ',
				'condition' => ' `enabled` = 1 '

			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = array(
					'slug' => $model->slug
				);
			}
			return $outArr;
		}*/
	}