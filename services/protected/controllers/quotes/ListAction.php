<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.AdminQuotesSymbolsFormModel' );

	final class ListAction extends ActionFrontBase {
		private $symbols_model;
		private function detFormModel() {
			$this->symbols_model = new AdminQuotesSymbolsFormModel();
			$load = $this->symbols_model->load();
			if( !$load ) $this->throwI18NException( Yii::t( "quotesWidget","Не возможно загрузить котировки!") );
		}
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Котировки',
				)
			);
		}
		private function setDescription() {
			$description = Yii::t( '*', '[Text for quotes list]' );
			$description = strip_tags( $description );
			$this->controller->setLayoutDescription( $description );
		}
		private function setKeywords() {
			$keywords = Yii::t( '*', 'Котировки' );
			$this->controller->setLayoutKeywords( $keywords );
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "default/index" );
			$this->setBreadcrumbs();
			$this->setDescription();
			$this->setKeywords();
			$this->detFormModel();
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'symbols_model'=>$this->symbols_model
			));
		}
	}
