<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class IndexAction extends ActionFrontBase {
		private function checkHash( $h, $e, $p ){
			$salt = $this->getSalt();
			if( !$salt ) return false;
			if( $h == md5($salt . $e . $p) ) return true;
			return false;
		}
		private function getSalt(){
			$filePath = Yii::getPathOfAlias('webroot') . '/../wp-content/plugins/decomments/decomments.php';
			$fileStr = file_get_contents($filePath);
			
			preg_match_all(
				'/\'UNSUBSCRIBE_SALT\'\s*,\s*\'([^\']+)\'/',
				$fileStr,
				$matches
			);
							
			if( $matches[1][0] ) return $matches[1][0];
		}
		

		function run( $h, $e, $p, $t ) {
			if( !$this->checkHash( $h, $e, $p ) ) return false;
			
			if( ! DecommentsPostsModel::unsubscribeFromPostCommentEmails( $e, $p, $t ) ) return false;
			
			$title = Yii::t('*', 'Unsubscribe from comment emails');
			$this->setLayoutTitle( $title, "{title}{-}{appName}" );
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "wp/index" );
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'title' => $title
			));
		}
	}

?>