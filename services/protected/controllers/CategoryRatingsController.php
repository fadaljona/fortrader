<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class CategoryRatingsController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Category Ratings";
		}
		function accessRules(){
			return Array(
				array(
					/*'deny',
					'actions' => array('index', 'single'),
					'expression' => array('CategoryRatingsController','allowOnlyAdmin'),
					'users'=>array('*')*/
				),
			);
		}
		public function filters(){
			return array(
				'accessControl'
			);
		}
	
		public static function allowOnlyAdmin(){
			if( Yii::App()->user->checkAccess( 'categoryRatingsControl' ) ) return false;
			return true;
		}
	}

?>