<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class SingleAction extends ActionFrontBase {
		private $model;
		private $cat = 'singleBroker';
		private $paramStr;
		
		public $level = 2;
		
		public $title;
		public $metaTitle;
		public $metaDesc;
		public $metaKeys;
		
		static function siteMapArgs(){
			return true;
		}
		
		private function setBreadcrumbs() {
			
			if( isset($this->model->brandName) && $this->model->brandName )
				$label = $this->model->brandName;
			else
				$label = $this->model->officialName;
			
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Broker ratings',
					'url' => Array( '/broker/list' ),
				),
				(object)Array( 

					'label' => $label,
				)
			);
		}
		private function setSubitem() {
			$navlist = $this->getNavlist();
			$item = $navlist->createItemAfter( $navlist->findItemById( "iBrokersList" ));
			$item->setLabel( $this->model->shortName );
			$item->setActive();
		}
		private function getModel( $slug ) {
			$model = BrokerModel::model()->find( Array(
				'with' => Array(
					'stats' => Array(
						'select' => Array(
							" `stats`.* ",
							" DATEDIFF( NOW(), `stats`.`changePlaceDT` ) as placeBusyDays",
						),
					),
					'regulators' => Array(
						'together' => false,
						'with' => Array(
							'currentLanguageI18N' => Array(),
							'i18ns' => Array(),
						),
					),
					'tradePlatforms' => Array(
						'together' => false,
					),
					'IOs' => Array(
						'together' => false,
						'with' => Array(
							'currentLanguageI18N' => Array(),
							'i18ns' => Array(),
						),
					),
					'instruments' => Array(
						'together' => false,
						'with' => Array(
							'currentLanguageI18N' => Array(),
							'i18ns' => Array(),
						),
					),
				),
				'condition' => " `t`.`slug` = :slug ",
				'params' => Array(
					':slug' => $slug,
				),
			));
			if( !$model ) throw new CHttpException(404,'Page Not Found');
			return $model;
		}
		private function setMeta() {
			
			$metaTitle = $this->model->metaTitle;

			$this->metaDesc = $this->model->metaDesc;
			$this->metaKeys = $this->model->metaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
				$this->title = $metaTitle;
			} else{
				$this->metaTitle = $this->getTitle();
			}
			if( !$this->title ) $this->title = Yii::t( '*', 'Reviews about' ).' '.htmlspecialchars( $this->model->brandName );
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{controllerTitle}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( Yii::App()->request->getHostInfo() . $this->model->srcImage );
		}
		private function getTitle(){
			if( isset($this->model->brandName) && $this->model->brandName )
				$LayoutTitle = $this->model->brandName;
			else
				$LayoutTitle = $this->model->officialName;
			return $LayoutTitle;
		}
		private function makeRedirectToSlug(){
			$id = Yii::app()->request->getParam('id');
			if( $id ){
				$model = BrokerModel::model()->findByPk( $id );
				if( !$model->slug ) throw new CHttpException(404,'Page Not Found');
				$this->redirect( $this->controller->createUrl( $model->type.'/single', array( 'slug' => $model->slug ) ), true, 301 );
			}
		}
		function run() {	
			$this->makeRedirectToSlug();
			$slug = Yii::app()->request->getParam('slug');
			if( $slug == 'list' || $slug == 'index' ) $this->redirect( $this->controller->createUrl( 'broker/list' ), true, 301 );
			if( !$slug ) throw new CHttpException(404,'Page Not Found');

			
			$actionCleanClass = $this->getCleanClassName();
			$this->model = $this->getModel( $slug );
			$this->paramStr = "broker_{$this->model->id}";
			
			$this->setMeta();
			
			$this->model->viewed();
				
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			$this->setSubitem();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'broker' => $this->model,
				'cat' => $this->cat,
				'paramStr' => $this->paramStr,
				'commentsCount' => DecommentsPostsModel::getCommentsCount( $this->paramStr, $this->cat ),
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>