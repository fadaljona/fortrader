<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class RedirectNameAction extends ActionFrontBase {

		function run( $id ) {
			
			$slug = BrokerModel::getSlugById( $id );
			if( !$slug ) $this->throwI18NException( "Can't find Broker!" );
				
			$url = Yii::App()->createURL( 'broker/single', Array( 'slug' => $slug ));
			
            $ads_model = AdvertisementModel::getModelByBrokerId( $id );
            if ($ads_model) {
                $ads_model->hitClick();
            }
			
			Yii::App()->request->redirect( $url );
		}
	}

?>