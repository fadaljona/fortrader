<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxSaveLinksStatsAction extends ActionFrontBase {
		function run( ) {
			if( !isset( $_POST['brokerId'] ) || !isset( $_POST['type'] ) ) return false;
			BrokerLinksStatsModel::saveClick( $_POST['type'], $_POST['brokerId'] );
		}
	}

?>