<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.AdminBrokerMarkFormModel' );
	
	final class AjaxAddMarkAction extends ActionFrontBase {
		private $formModel;
		private function getFormModel( $idBroker ) {
			if( !$this->formModel ) {
				$this->formModel = new AdminBrokerMarkFormModel();
				$this->formModel->setIDBroker( $idBroker );
				$load = $this->formModel->load();
				if( !$load ) $this->throwI18NException( "Can't find Model!" );
			}
			return $this->formModel;
		}
		private function checkKeys( $idBroker ) {
			$idUser = Yii::App()->user->id;
			$mark = BrokerMarkModel::model()->findByAttributes( Array(
				'idUser' => $idUser,
				'idBroker' => $idBroker,
			));
			if( $mark ) return true;
			
			$ip = sprintf( "%u", ip2long( $_SERVER[ 'REMOTE_ADDR' ]));
			$mark = BrokerMarkModel::model()->findByAttributes( Array(
				'idBroker' => $idBroker,
				'ip' => $ip,
			));
			if( $mark ) return false;
			
			if( @$_COOKIE[ "broker_{$idBroker}_mark" ]) return false;
			return true;
		}
		function run() {
			$data = Array();
			try{
				$idBroker = (int)@$_POST[ 'idBroker' ];
				
				Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.broker/single.'.$idBroker.'.'  );
				
				$formModel = $this->getFormModel( $idBroker );
				if( $formModel->validate()) {
					if( $this->checkKeys( $idBroker )) {
						$id = $formModel->save();
						if( !$id ) $this->throwI18NException( "Can't save AR!" );
						$data[ 'id' ] = $id;
					}
					else{
						$data[ 'id' ] = mt_rand();
					}
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>