<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import('models.QuotesSymbolsModel');
	
	final class AjaxSearchAction extends ActionAdminBase {
		public $type = 'broker';
		
		private function getType() {
			if( isset( $_POST[ 'type' ] ) && $_POST[ 'type' ] && in_array( $_POST[ 'type' ], array( 'broker','binary' ) ) ) $this->type = $_POST[ 'type' ];
			return $this->type;
		}
	
		function run() {
			$data = Array('result' => '');
			
			if( !isset($_POST['search']) ) return false;
			$searchTerm = $_POST['search'];

			$models = BrokerModel::model()->findAll(Array(
				'with' => array( 'i18ns' ),
				'condition' => " ( `i18nsBroker`.`officialName` LIKE :searchTerm OR `i18nsBroker`.`brandName` LIKE :searchTerm OR `i18nsBroker`.`shortName` LIKE :searchTerm ) AND `i18nsBroker`.`idLanguage` = :idLanguage AND `t`.`type` = :type ",
				'params' => array( ':searchTerm' => '%'.$searchTerm.'%', ':idLanguage' => LanguageModel::getCurrentLanguageID(), ':type' => $this->getType() ),
			));
			
			if( $models ){
				foreach( $models as $model ){
					if( $model->brandName ) $name = $model->brandName;
					if( $model->officialName ) $name = $model->officialName;
					$name = $model->shortName . ' - ' .  $name;
					$data['result'] .= CHtml::tag( 
						"li", 
						Array( 'class' => '' ), 
						CHtml::link( 
							CHtml::encode( $name ), 
							$model->singleURL,
							array( 'target' => '_blank' )
						)
					);
				}
			}else{
				$data['result'] = CHtml::tag( "li", Array( 'class' => '' ), Yii::t( $NSi18n, 'No results found' ) );
			}
			echo json_encode( $data );
		}
	}
?>