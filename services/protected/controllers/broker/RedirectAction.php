<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class RedirectAction extends ActionFrontBase {
		private function getModel( $id ) {
			$model = BrokerModel::model()->find( Array(
				'select' => " id, openAccount, site, conditions, terms, openDemoAccount ",
				'condition' => " `t`.`id` = :id ",
				'params' => Array(
					':id' => $id,
				),
			));
			if( !$model ) throw new CHttpException(404,"Can't find Broker!");
			return $model;
		}
		function run( $id, $type ) {
			
			$ads = @$_GET['ads'] ? 1 : 0;
			
			$model = $this->getModel( $id );
			
			BrokerStats2Model::inc( $id, "countRedirects" );
			
			$url = '';
			
			switch( $type ) {
				case 'openAccount':{
					$url = CommonLib::getUrl( $model->openAccount );
					break;
				}
				case 'openDemoAccount':{
					$url = CommonLib::getUrl( $model->openDemoAccount );
					break;
				}
				case 'site':{
					$url = CommonLib::getUrl( $model->site );
					break;
				}
				case 'conditions':{
					$url = CommonLib::getUrl( $model->conditions );
					break;
				}
				case 'terms':{
					$url = CommonLib::getUrl( $model->terms );
					break;
				}
			}
	
			if( $ads ){
				$ads_model = AdvertisementModel::getModelByBrokerId( $id );
				if( $ads_model ) $ads_model->hitClick();
			}
			
			if( $url ){
				Yii::App()->request->redirect( $url );
			}else{
				throw new CHttpException(404,'No redirect url in database');
			}
		}
	}

?>