<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxLinkToUserAction extends ActionFrontBase {
		private function getModel( $id ) {
			$model = BrokerModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Broker!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			Yii::app()->cache->delete( PageCacheFilter::CACHE_KEY_PREFIX.'.broker/single.'.$id.'.'  );
			try{
				$broker = $this->getModel( $id );
				$broker->linkToUser();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>