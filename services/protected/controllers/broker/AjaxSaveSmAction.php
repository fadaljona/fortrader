<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	
	final class AjaxSaveSmAction extends ActionFrontBase {
		private function getModel( $id ) {
			$model = BrokerModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Broker!" );
			return $model;
		}
		function run( ) {
			$data = Array();
			try{
				if( !Yii::App()->user->id ) $this->throwI18NException( "User must be logged in" );
				if( !isset($_POST['id']) || !$_POST['id'] || ( !isset($_POST['vk']) && !isset($_POST['fb']) ) ) $this->throwI18NException( "Bad request" );
				$broker = $this->getModel( $_POST['id'] );
				$dataToSave = array();
				if( !isset($_POST['vk']) ) $dataToSave['fb'] = $_POST['fb'];
				if( !isset($_POST['fb']) ) $dataToSave['vk'] = $_POST['vk'];
				$broker->saveSm( $dataToSave );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>