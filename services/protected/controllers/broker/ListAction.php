<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ListAction extends ActionFrontBase {
		public $title;
		public $metaTitle = 'Broker ratings';
		public $metaDesc;
		public $metaKeys;
		public $settings;
		
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Broker ratings',
				)
			);
		}
		private function setMeta() {
			$settings = BrokerSettingsModel::getModel();
			$this->settings = $settings;
			$metaTitle = $settings->brokersListMetaTitle;

			$this->metaDesc = $settings->brokersListMetaDesc;
			$this->metaKeys = $settings->brokersListMetaKeys;
			
			if( $metaTitle ){
				$this->metaTitle = $metaTitle;
			} else{
				$this->metaTitle = Yii::t( '*', $this->metaTitle );
			}
			$this->title = $this->metaTitle;
			if( !$this->metaDesc ) $this->metaDesc = $this->metaTitle;

			$this->setLayoutTitle( $this->metaTitle, "{title}{-}{appName}" );
			$this->setLayoutDescription( $this->metaDesc );
			$this->setLayoutKeywords( $this->metaKeys );
			
			$this->setLayoutOgSection();
			$this->setLayoutOgImage( $settings->brokersListOgImage );
		}
		function run() {
			$this->setMeta();
			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayout( "wp/index" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'settings' => $this->settings,
				'metaDesc' => $this->metaDesc
			));
		}
	}

?>