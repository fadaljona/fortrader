<?php

Yii::import('controllers.base.ActionFrontBase');

final class AjaxLoadTradingWithBrokerCryptoCurrenciesAction extends ActionFrontBase
{
    private function renderList()
    {
        ob_start();
            
        $this->controller->widget('widgets.TradingWithBrokerCryptoCurrenciesWidget', array(
            'ns' => 'nsActionView',
            'ajax' => true,
        ));
            
        $content = ob_get_clean();
        $content = preg_replace("#[\r\n\t]+#", " ", $content);
            
        return $content;
    }
    public function run()
    {
        $data = array();
        try {
            $data[ 'list' ] = $this->renderList();
        } catch (Exception $e) {
            $data[ 'error' ] = $e->getMessage();
        }
        echo json_encode($data);
    }
}
