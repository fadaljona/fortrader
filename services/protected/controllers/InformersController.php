<?
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class InformersController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Forex Informers";
		}
		function accessRules(){
			return Array(
				/*array(
					'deny',
					'expression' => array('InformersController','allowOnlyAdmin'),
					'users'=>array('*')
				),*/
			);
		}
		public function filters(){
			return array(
				'servicesRedirect',
				'accessControl'
			);
		}
		public function filterServicesRedirect($filterChain){
			if( strpos( Yii::App()->request->getUrl(), '/services' ) === 0 ){

				$url = str_replace( '/services', '', Yii::App()->request->getUrl() );
				
				$this->redirect( strtolower($url), true, 301 );
			}
			$filterChain->run();			
		}
		public static function allowOnlyAdmin(){
			if( Yii::App()->user->checkAccess( 'informersControl' ) ) return false;
			return true;
		}
		public function singleSiteMapArgs(){
			$models = InformersCategoryModel::model()->findAll(array(
				'select' => ' `slug` ',
				'condition' => ' `t`.`hidden` = 0 '
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[] = array(
					'slug' => strtolower( $model->slug )
				);
			}
			return $outArr;
		}
	}

?>