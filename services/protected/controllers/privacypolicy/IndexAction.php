<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class IndexAction extends ActionFrontBase {
		private function setBreadcrumbs() {
			$this->controller->commonBag->breadcrumbs = Array(
				(object)Array( 
					'label' => 'Privacy policy',
				)
			);
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Privacy policy", "{title}{-}{appName}" );
			$this->setLayout( "default/{$actionCleanClass}" );
			$this->setBreadcrumbs();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>