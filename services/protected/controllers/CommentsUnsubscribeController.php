<?php
	Yii::import( 'controllers.base.ControllerFrontBase' );

	final class CommentsUnsubscribeController extends ControllerFrontBase {
		function detLayoutTitle() {
			return "Unsubscribe from comment emails";
		}
	}

?>