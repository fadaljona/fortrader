<?php
Yii::import('controllers.base.ActionFrontBase');

final class ExchageRedirectAction extends ActionFrontBase
{
    public function run($id)
    {
        $model = CryptoCurrenciesExchangesModel::model()->findByPk($id);
        if (!$model) {
            $this->throwI18NException("Can't Crypto Currencies Exchange");
        }

        Yii::app()->request->redirect($model->url);
    }
}
