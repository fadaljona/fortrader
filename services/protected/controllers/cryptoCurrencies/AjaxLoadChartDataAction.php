<?php
Yii::import('controllers.base.ActionFrontBase');

final class AjaxLoadChartDataAction extends ActionFrontBase
{
    public function run($id, $type)
    {
        $data = array();
        try {
            $data = QuotesHistoryDataModel::getDataForCryptoCurrenciesChart($id, $type);
        } catch (Exception $e) {
            $data[ 'error' ] = $e->getMessage();
        }
        echo json_encode($data);
    }
}
