<?php

Yii::import('controllers.base.ActionFrontBase');

final class AjaxLoadCryptoExchangeAction extends ActionFrontBase
{
    private function renderList($id)
    {
        ob_start();
            
        $this->controller->widget('widgets.lists.CryptoExchangeListWidget', array(
            'ns' => 'nsActionView',
            'ajax' => true,
            'symbol' => $id
        ));
            
        $content = ob_get_clean();
        $content = preg_replace("#[\r\n\t]+#", " ", $content);
            
        return $content;
    }
    public function run($id)
    {
        $data = array();
        try {
            $data[ 'content' ] = $this->renderList($id);
        } catch (Exception $e) {
            $data[ 'error' ] = $e->getMessage();
        }
        echo json_encode($data);
    }
}
