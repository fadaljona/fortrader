<?php
Yii::import('controllers.base.ActionFrontBase');

final class IndexAction extends ActionFrontBase
{
    public $title;
    public $metaTitle = 'Crypto Currencies';
    public $metaDesc;
    public $metaKeys;
    public $settings;

    private function setBreadcrumbs()
    {
        $this->controller->commonBag->breadcrumbs = array(
            (object)array(
                'label' => 'Crypto Currencies',
            )
        );
    }
    private function setMeta()
    {
        $this->settings = CryptoCurrenciesSettingsModel::getModel();

        $metaTitle = $this->settings->metaTitle;

        $this->metaDesc = $this->settings->metaDesc;
        $this->metaKeys = $this->settings->metaKeys;

        if ($metaTitle) {
            $this->metaTitle = $metaTitle;
        } else {
            $this->metaTitle = Yii::t('*', $this->metaTitle);
        }
        $this->title = $this->settings->title;
        if (!$this->title) {
            $this->title = $this->metaTitle;
        }
        if (!$this->metaDesc) {
            $this->metaDesc = $this->metaTitle;
        }

        $this->setLayoutTitle($this->metaTitle, "{title}{-}{appName}");
        $this->setLayoutDescription($this->metaDesc);
        $this->setLayoutKeywords($this->metaKeys);

        if ($settingImg = $this->settings->ogImageSrc) {
            $this->setLayoutOgSection();
            $this->setLayoutOgImage($settingImg);
        }
        $this->setLangSwitcher(true);
    }
    public function run()
    {
        $this->setMeta();

        $actionCleanClass = $this->getCleanClassName();

        $this->setLayout("wp2/index");
        $this->setBreadcrumbs();

        $this->controller->render("{$actionCleanClass}/view", array(
            'settings' => $this->settings,
            'metaDesc' => $this->metaDesc
        ));
    }
}
