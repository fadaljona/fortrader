<?php
	$baseUrl = '/services';
	$_COOKIE[ 'language' ] = 'ru';
	if( isset( $_SERVER['REQUEST_URI'] ) ){
		if( 
			strpos( $_SERVER['REQUEST_URI'], '/quotes' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/broker' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/binary' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/currencyrates' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/journal' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/contests' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/contest' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/contestMember' ) === 0  || 
			strpos( $_SERVER['REQUEST_URI'], '/news' ) === 0  || 
			strpos( $_SERVER['REQUEST_URI'], '/newsAggregator' ) === 0  || 
			strpos( $_SERVER['REQUEST_URI'], '/feeds' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/interestRates' ) === 0  || 
			strpos( $_SERVER['REQUEST_URI'], '/bankrates' ) === 0  || 
			strpos( $_SERVER['REQUEST_URI'], '/currencyconverter' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/informers' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/economic-calendar' ) === 0  || 
			strpos( $_SERVER['REQUEST_URI'], '/monitoring' ) === 0  || 
			strpos( $_SERVER['REQUEST_URI'], '/categoryRatings' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/earating' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/strategyrating' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/indicatorsrating' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/author' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/user' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/comments-unsubscribe' ) === 0 || 
			strpos( $_SERVER['REQUEST_URI'], '/cryptocurrencies' ) === 0
		){
			$baseUrl = $langs['ru']['baseUrl'];
			$_COOKIE[ 'language' ] = 'ru';
			if( !defined ( 'WPINC' ) )setcookie('pll_language', 'ru', time() + 60*60*24*365, '/');
		}
		if( 
			strpos( $_SERVER['REQUEST_URI'], '/en/' ) === 0 
		){
			$baseUrl = $langs['en']['baseUrl'];
			$_COOKIE[ 'language' ] = $langs['en']['langAlias'];
			if( !defined ( 'WPINC' ) )setcookie('pll_language', 'en', time() + 60*60*24*365, '/');
		}
		if( 
			strpos( $_SERVER['REQUEST_URI'], '/services/' ) === 0 && strpos( $_SERVER['REQUEST_URI'], 'l=en' ) !== false
		){
			$_COOKIE[ 'language' ] = $langs['en']['langAlias'];
		}
	}
?>