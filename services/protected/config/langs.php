<?php
	$langs = array(
		'ru' => array(
			'langAlias' => 'ru',
			'wpLocale' => 'ru_RU',
			'baseUrl' => '',
			'country' => 'ru',
		),
		'en' => array(
			'langAlias' => 'en_us',
			'wpLocale' => 'en_US',
			'baseUrl' => '/en',
			'country' => 'us',
		),
	);
?>