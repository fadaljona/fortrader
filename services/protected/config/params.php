<?php

$langFile = "{$dir}/langs.php";

if (is_file($langFile)) {
    require $langFile;
}

$documentRoot = dirname(__FILE__) . '/../../../';

if (empty($_SERVER['DOCUMENT_ROOT'])) {
    $_SERVER['DOCUMENT_ROOT'] = $documentRoot;
}
    
return [
    'authenticateRememberDays' => 365,
    'languageCookieVarName' => 'language',
    'keyAPI_fxteam.ru' => '128844ebe8db381a7997a5b325b93cbf',
    'keyCrypt' => "097da0f564a12d84665f9107d13f12bd",
    'wpThemePath' => $documentRoot.'/wp-content/themes/ForTraderMaster',
    'wpThemeUrl' => '/wp-content/themes/ForTraderMaster',
    'pathUploadAvatarDir' => $documentRoot.'/services/uploads/users/avatars',
    'pathServicesDir' => $documentRoot.'/services',
    'wpLoaded' => false,
    'facebook' => 'https://www.facebook.com/fortrader',
    'googlePlus' => 'http://google.com/+FortraderRu1',
    'twitter' => 'fortrader',
    'uploadsUrl' => 'https://files.fortrader.org/service_uploads',
    'domainInText' => 'ForTrader.org',
    'langs' => $langs,
    'documentRoot' => $documentRoot,
    'consolePhpPath' => '/opt/plesk/php/7.0/bin/php'
];
