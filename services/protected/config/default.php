<?
	Yii::$enableIncludePath = false;
	if( !defined( 'YII_LOG_ERRORS' )) define( 'YII_LOG_ERRORS', true );
	
	$dir = dirname( __FILE__ );
	$baseDir = realpath( "{$dir}/.." );
	
	$db = "{$dir}/db-dist.php";
	if( is_file( $db )) require $db;
	
	$db = "{$dir}/db-local.php";
	if( is_file( $db )) require $db;

	/*$db = "{$dir}/db-lexeytemp.php";
	if( is_file( $db )) require $db;*/
	
	$langFile = "{$dir}/langs.php";
	if( is_file( $langFile )) require $langFile;
	
	$baseUrlFile = "{$dir}/baseUrl.php";
	if( is_file( $baseUrlFile )) require $baseUrlFile;

	$db = Array(
		'connectionString' => "mysql:host={$HOST};dbname={$DB}",
		'emulatePrepare' => true,
		'username' => $USER,
		'password' => $PASSWORD,
		'charset' => $CHARSET,
		'tablePrefix' => $PREFIX_TABLES,
		'enableProfiling' => $PROFILING,
		'enableParamLogging' => $PROFILING_PARAMS,
		'schemaCachingDuration' => $CACHE_STRUCTURE,
	);

	$hostInfoFile = "{$dir}/hostInfo.php";
    if (is_file( $hostInfoFile )) require $hostInfoFile;


    if (is_file(__DIR__ . '/params-local.php')) {
        $localParams = require(__DIR__ . '/params-local.php');
    } else {
        $localParams = [];
    }

    $params = array_merge(
        require(__DIR__ . '/params.php'),
        $localParams
    );

    
	
	
	Yii::setPathOfAlias( 'dps', "{$baseDir}/dps" );
	Yii::setPathOfAlias( 'models', "{$baseDir}/models" );
	Yii::setPathOfAlias( 'controllers', "{$baseDir}/controllers" );
	Yii::setPathOfAlias( 'components', "{$baseDir}/components" );
	Yii::setPathOfAlias( 'extensions', "{$baseDir}/extensions" );
	Yii::setPathOfAlias( 'bootstrap', "{$baseDir}/extensions/bootstrap" );
	Yii::setPathOfAlias( 'widgets', "{$baseDir}/components/widgets" );
	Yii::setPathOfAlias( 'gridColumns', "{$baseDir}/components/gridColumns" );
	Yii::setPathOfAlias( 'dataColumns', "{$baseDir}/components/dataColumns" );
	
	CHtml::$closeSingleTags = false;
	
	Yii::import( 'components.sys.CMysqlSchema' );
	
	return Array(
		'basePath' => $baseDir,
		'name' => 'ForTrader.org',
		'language' => 'ru',
		'defaultController' => 'default',
		'import' => Array(
			'models.*',
			'models.base.*',
			'models.interfaces.*',
			'controllers.base.*',
			'components.base.*',
			'widgets.base.*',
			'application.libs.*',
			'application.filters.*',
			'ext.easyimage.EasyImage',
			'ext.JsTrans.*'
		),
		'preload' => Array( 'log', 'user' ),
		'modules' => Array(
			'admin' => Array(),
			'sitemap' => array(
				'class' => 'ext.sitemap.SitemapModule',
				'absoluteUrls' => true,
				'protectedControllers' => array('admin', 'advertisementZone', 'quotes', 'tradingview', 'userPrivateMessage', 'strategy', 'CommentsUnsubscribeController', 'exchangeECurrency'),
				'oneSiteMapControllers' => array( 'quotesNew', 'currencyRates' ),
				'oneSiteMapActions' => array( 'currencyRates/archive', 'currencyRates/ecbArchive', 'currencyRates/archiveSingle', 'currencyRates/exchangePage', 'calendarEvent/single' ),
				'oneSiteMapTypeToController' => array(
					'quotes' => 'quotesNew',
					'currencies' => 'currencyRates',
					'action-currency-archive' => 'currencyRates/archive',
					'action-currency-archive-ecb' => 'currencyRates/ecbArchive',
					'action-currency-single-archive' => 'currencyRates/archiveSingle',
					'action-currency-single-convert' => 'currencyRates/exchangePage',
					'action-calendar-event-single' => 'calendarEvent/single'
				),
				'urlsPerMap' => 1000,
				'protectedActions' =>array(
					'default/error', 
					'calendarEvent/apiExportList', 
					'calendarEvent/coming', 
					'calendarEvent/img', 
					'calendarEvent/upload',
					'calendarEvent/export',
					'chat/index',
					'default/login',
					'default/logout',
					'ea/iframeAdd',
					'eaStatement/add',
					'eaStatement/iframeAdd',
					'eaVersion/add',
					'eaVersion/iframeAdd',
					'journal/apiExportList',
					'journal/code',
					'journal/cover',
					'journal/latest',
					'journal/scripts',
					'quotesNew/getInformer',
					'quotesNew/saveImage',
					'user/iframeUploadAvatar',
					'user/profile',
					'user/redirectURL',
					'user/setTimezone',
					'user/settings',
					'user/unsubscribeMailingType',
					'informers/getInformer',
					'informers/getColors',
					'monitoring/iframeAddReport',
				), 
				'priority' => '0.5',
				'changefreq' => 'daily',
				'cacheId' => 'cacheSiteMap',
				'cachingDuration' => 60*60*12,
			),
		),
		'components' => Array(
            'format'=>array(
                'class'=>'application.extensions.timeago.TimeagoFormatter',
            ),
			'db' => $db,
		        'urlManager' => Array(
						'baseUrl' => $baseUrl,
		                'showScriptName' => false,
		                //'useStrictParsing' => true,
		                'urlFormat' => 'path',
		                //'appendParams' => false,
		                'rules' => Array(
		                    '' => 'default/index',
		                    '15135sz/<action:\w+>' => 'advertisementZone/<action>', //  /services/advertisementZone/jsRender?id=25&format=html
		                    '15135at/<action:\w+>' => 'advertisement/<action>', 
							
							'quotes/ajaxAddForecast' => 'quotesNew/ajaxAddForecast',
							'quotes/ajaxLoadOneChart' => 'quotesNew/ajaxLoadOneChart',
							'quotes/ajaxSearch' => 'quotesNew/ajaxSearch',
							'quotes/charts' => 'quotesNew/charts',
							'quotes/forecats' => 'quotesNew/forecats',
							'quotes/' => 'quotesNew/list',
							'quotes' => 'quotesNew/list',
							'quotes/tradersRating' => 'quotesNew/tradersRating',
							'quotes/ajaxLoadPeriodChartData' => 'quotesNew/ajaxLoadPeriodChartData',
							'quotes/informer' => 'quotesNew/informer',
							'quotes/getInformer' => 'quotesNew/getInformer',
							'quotes/saveImage' => 'quotesNew/saveImage',
							'quotes/otherquotes' => 'quotesNew/otherQuotes',
							'quotes/ajaxGetDataForHistoryHighCharts' => 'quotesNew/ajaxGetDataForHistoryHighCharts',
                            'quotes/ajaxGetCourse' => 'quotesNew/ajaxGetCourse',
                            
							'quotes/<slug:ruble>' => 'quotesNew/category',
							'quotes/<slug:russianstock>' => 'quotesNew/category',
							'quotes/<slug:forex>' => 'quotesNew/category',
							'quotes/<slug:futures>' => 'quotesNew/category',
							'quotes/<slug:americanstock>' => 'quotesNew/category',
							'quotes/<slug:cryptorates>' => 'quotesNew/category',
							'quotes/<slug:rts>' => 'quotesNew/category',
							'quotes/<slug:rts-board>' => 'quotesNew/category',
							'quotes/<slug:commodity-markets>' => 'quotesNew/category',
							'quotes/<slug:indexes>' => 'quotesNew/category',
							'quotes/<slug:metals>' => 'quotesNew/category',
							
							'quotes/<slug:[a-zA-Z0-9-.]+>/' => 'quotesNew/single',
							
							'quotesOld/<action:\w+>' => 'quotes/<action>', 
							'quotes' => 'quotesNew', 
							'quotesOld' => 'quotes', 
							
							'brokersrating/ajaxLinkToUser' => 'broker/ajaxLinkToUser',
							'brokersrating/ajaxLoadList' => 'broker/ajaxLoadList',
							'brokersrating/ajaxSaveLinksStats' => 'broker/ajaxSaveLinksStats',
							'brokersrating/ajaxSaveSm' => 'broker/ajaxSaveSm',
							'brokersrating/ajaxSearch' => 'broker/ajaxSearch',
							'brokersrating/ajaxUnLinkFromUser' => 'broker/ajaxUnLinkFromUser',
							'brokersrating/redirect' => 'broker/redirect',
							'brokersrating/redirectName' => 'broker/redirectName',
							'brokersrating/' => 'broker/list',
							'brokersrating' => 'broker/list',
							'brokersrating/<slug:[a-zA-Z0-9-]+>/' => 'broker/single',
							'brokersrating' => 'broker', 
							
							'binaryrating/' => 'binary/index',
							'binaryrating' => 'binary/index',
							'binaryrating/<slug:[a-zA-Z0-9-]+>/' => 'binary/single',
							'binaryrating' => 'binary', 
							
							'currencyrates/ajaxLoadChartData' => 'currencyRates/ajaxLoadChartData',
							'currencyrates/ajaxSaveViews' => 'currencyRates/ajaxSaveViews',
							'currencyrates/ajaxLoadList' => 'currencyRates/ajaxLoadList',
							'currencyrates/ajaxSaveImage' => 'currencyRates/ajaxSaveImage',
							'currencyrates/ajaxSearch' => 'currencyRates/ajaxSearch',
							'currencyrates/tomorrow' => 'currencyRates/tomorrow',
							'currencyrates/ajaxLoadChartDataForArchive' => 'currencyRates/ajaxLoadChartDataForArchive',
							
							'currencyrates/archive/<type:ecb>/<year:[0-9]{4}>/<mo:[0-9]{2}>/<day:[0-9]{2}>/<slug:[a-zA-Z]+>' => 'currencyRates/archiveSingle',
							'currencyrates/archive/<type:ecb>/<year:[0-9]{4}>/<mo:[0-9]{2}>/<slug:[a-zA-Z]+>' => 'currencyRates/archiveSingle',
							'currencyrates/archive/<type:ecb>/<year:[0-9]{4}>/<slug:[a-zA-Z]+>' => 'currencyRates/archiveSingle',
							
							'currencyrates/archive/<year:[0-9]{4}>/<mo:[0-9]{2}>/<day:[0-9]{2}>/<slug:[a-zA-Z]+>' => 'currencyRates/archiveSingle',
							'currencyrates/archive/<year:[0-9]{4}>/<mo:[0-9]{2}>/<slug:[a-zA-Z]+>' => 'currencyRates/archiveSingle',
							'currencyrates/archive/<year:[0-9]{4}>/<slug:[a-zA-Z]+>' => 'currencyRates/archiveSingle',
							
							'currencyrates/archive/<type:ecb>/<year:[0-9]{4}>/<mo:[0-9]{2}>/<day:[0-9]{2}>' => 'currencyRates/ecbArchive',
							'currencyrates/archive/<type:ecb>/<year:[0-9]{4}>/<mo:[0-9]{2}>' => 'currencyRates/ecbArchive',
							'currencyrates/archive/<type:ecb>/<year:[0-9]{4}>' => 'currencyRates/ecbArchive',
							
							'currencyrates/archive/<year:[0-9]{4}>/<mo:[0-9]{2}>/<day:[0-9]{2}>' => 'currencyRates/archive',
							'currencyrates/archive/<year:[0-9]{4}>/<mo:[0-9]{2}>' => 'currencyRates/archive',
							'currencyrates/archive/<year:[0-9]{4}>' => 'currencyRates/archive',

							'currencyrates/archive/ecb' => 'currencyRates/ecbArchive',
							'currencyrates/archive' => 'currencyRates/archive',
							
							'currencyrates/ajaxLoadArchiveListData' => 'currencyRates/ajaxLoadArchiveListData',
							'currencyrates/ajaxConvertRequest' => 'currencyRates/ajaxConvertRequest',
							'currencyrates/ajaxLoadConverterChartData' => 'currencyRates/ajaxLoadConverterChartData',
							'currencyrates/ajaxSaveExchangeViews' => 'currencyRates/ajaxSaveExchangeViews',

							'currencyrates/' => 'currencyRates/index',
							'currencyrates' => 'currencyRates/index',
							
							'currencyconverter/ecb' => 'currencyRates/converterEcbPage',
							'currencyconverter/ecb/' => 'currencyRates/converterEcbPage',
							'currencyconverter/' => 'currencyRates/converterPage',
							'currencyconverter' => 'currencyRates/converterPage',
							'currencyconverter/<type:ecb>/<value:[0-9.]+>-<currencyFrom:[a-zA-Z]+>-to-<currencyTo:[a-zA-Z]+>' => 'currencyRates/exchangePage',
							'currencyconverter/<value:[0-9.]+>-<currencyFrom:[a-zA-Z]+>-to-<currencyTo:[a-zA-Z]+>' => 'currencyRates/exchangePage',
							
							'currencyrates/ecb' => 'currencyRates/ecbIndex', 
							'currencyrates/ecb/' => 'currencyRates/ecbIndex', 
							'currencyrates/<type:ecb>/<slug:[a-zA-Z]+>/' => 'currencyRates/single',
							'currencyrates/<slug:[a-zA-Z]+>/' => 'currencyRates/single',
							'currencyrates' => 'currencyRates', 
							
							
							'journal/ajaxAddMark' => 'journal/ajaxAddMark',
							'journal/ajaxLoadArchiveList' => 'journal/ajaxLoadArchiveList',
							'journal/ajaxLoadList' => 'journal/ajaxLoadList',
							'journal/ajaxLoadRecentActivitiesList' => 'journal/ajaxLoadRecentActivitiesList',
							'journal/ajaxLoadWebmasterList' => 'journal/ajaxLoadWebmasterList',
							'journal/ajaxSubscribe' => 'journal/ajaxSubscribe',
							'journal/ajaxUnsubscribe' => 'journal/ajaxUnsubscribe',
							'journal/apiExportList' => 'journal/apiExportList',
							'journal/code' => 'journal/code',
							'journal/cover' => 'journal/cover',
							'journal/download' => 'journal/download',
							'journal/latest' => 'journal/latest',
							'journal/scripts' => 'journal/scripts',
							'journal/' => 'journal/last',
							'journal' => 'journal/last',
							
							'journal/<slug:[a-zA-Z0-9-.]+>/' => 'journal/single',
							'journal' => 'journal', 
							
							
							'contests/ajaxAddForeignContest' => 'contest/ajaxAddForeignContest',
							'contests/ajaxLoadClosedTransactionsStatsGraph' => 'contest/ajaxLoadClosedTransactionsStatsGraph',
							'contests/ajaxLoadForeignList' => 'contest/ajaxLoadForeignList',
							'contests/ajaxLoadList' => 'contest/ajaxLoadList',
							'contests/ajaxLoadMembersList' => 'contest/ajaxLoadMembersList',
							'contests/ajaxLoadMembersRatingList' => 'contest/ajaxLoadMembersRatingList',
							'contests/ajaxLoadRow' => 'contest/ajaxLoadRow',
							'contests/ajaxLoadSingleRow' => 'contest/ajaxLoadSingleRow',
							'contests/ajaxLoadSliderItems' => 'contest/ajaxLoadSliderItems',
							'contests/ajaxRegisterMember' => 'contest/ajaxRegisterMember',
							'contests/getStats' => 'contest/getStats',
							'contests/registerMember' => 'contest/registerMember',
							'contests/ajaxAddAccountToMonitoring' => 'contest/ajaxAddAccountToMonitoring',
							'contests/ajaxCheckAccountStatus' => 'contest/ajaxCheckAccountStatus',
							'contests/ajaxLoadCurrentUserInfo' => 'contest/ajaxLoadCurrentUserInfo',
							'contests/export' => 'contest/export',
							
							'contests/' => 'contest/list',
							'contests' => 'contest/list',
							
							'contests/<slug:[a-zA-Z0-9-.]+>/' => 'contest/single',
							'contests' => 'contest', 
							
							
							
							'contestMember/ajaxLoadMemberAccountMonitorList' => 'contestMember/ajaxLoadMemberAccountMonitorList',
							'contestMember/ajaxLoadMemberDealsGraph' => 'contestMember/ajaxLoadMemberDealsGraph',
							'contestMember/ajaxLoadMemberDealsHistoryList' => 'contestMember/ajaxLoadMemberDealsHistoryList',
							'contestMember/ajaxLoadMemberOrdersHistoryList' => 'contestMember/ajaxLoadMemberOrdersHistoryList',
							'contestMember/ajaxLoadMemberTradesList' => 'contestMember/ajaxLoadMemberTradesList',
							'contestMember/ajaxLoadMemberTradesSymbolsList' => 'contestMember/ajaxLoadMemberTradesSymbolsList',
							'contestMember/<id:[0-9]+>/' => 'contestMember/single',
							
							
							
							'news/ajaxLoadList' => 'newsAggregator/ajaxLoadList',
							'news/ajaxLoadSourcesList' => 'newsAggregator/ajaxLoadSourcesList',
							'news/source/<id:[0-9]+>/' => 'newsAggregator/sourceLink',
							'news/sources' => 'newsAggregator/sources',
	
							'news/' => 'newsAggregator/index',
							'news' => 'newsAggregator/index',
	
							'news/<slug:[a-zA-Z0-9-]+-[0-9]+>/' => 'newsAggregator/single',
							'news/<slug:[a-zA-Z0-9-]+>/' => 'newsAggregator/category',
								
							'news' => 'newsAggregator', 
							
							
							'feeds/' => 'feeds/index',
							'feeds' => 'feeds/index',
							
							'feeds/<slug:[a-zA-Z0-9-.]+>.rss/' => 'feeds/single',
							'feeds' => 'feeds', 
							
							'bankrates/ajaxLoadInterestRatesList' => 'interestRates/ajaxLoadInterestRatesList',
	
							'bankrates/' => 'interestRates/index',
							'bankrates' => 'interestRates/index',
							
							
							'informers/getColors' => 'informers/getColors',
							'informers/ajaxGetSettingForWpPlugin' => 'informers/ajaxGetSettingForWpPlugin',
							'informers/getInformer' => 'informers/getInformer',
							'informers/css' => 'informers/css',
							'informers/js' => 'informers/js',
							
							'informers/' => 'informers/index',
							'informers' => 'informers/index',
	
							'informers/<slug:[a-zA-Z-]+>/' => 'informers/single',
							'informers' => 'informers', 
							
							'comments-unsubscribe' => 'commentsUnsubscribe/index',
							
							
							'economic-calendar/ajaxLoadList' => 'calendarEvent/ajaxLoadList',
							'economic-calendar/ajaxLoadShortHistory' => 'calendarEvent/ajaxLoadShortHistory',
							'economic-calendar/ajaxLoadCalendarComingEvents' => 'calendarEvent/ajaxLoadCalendarComingEvents',
							'economic-calendar/ajaxLoadRows' => 'calendarEvent/ajaxLoadRows',
							'economic-calendar/apiExportList' => 'calendarEvent/apiExportList',
							'economic-calendar/coming' => 'calendarEvent/coming',
							'economic-calendar/history' => 'calendarEvent/history',
							'economic-calendar/img' => 'calendarEvent/img',
							'economic-calendar/time' => 'calendarEvent/time',
							'economic-calendar/upload' => 'calendarEvent/upload',
							'economic-calendar/ajaxLoadUpdatedList' => 'calendarEvent/ajaxLoadUpdatedList',
							'economic-calendar/ajaxLoadInformerEventsList' => 'calendarEvent/ajaxLoadInformerEventsList',
							'economic-calendar/ajaxLoadInformerRows' => 'calendarEvent/ajaxLoadInformerRows',
							'economic-calendar/ajaxSubscribeSingleEvent' => 'calendarEvent/ajaxSubscribeSingleEvent',
							'economic-calendar/ajaxUnSubscribeSingleEvent' => 'calendarEvent/ajaxUnSubscribeSingleEvent',
							'economic-calendar/ajaxSubscribeGroupEvent' => 'calendarEvent/ajaxSubscribeGroupEvent',
							'economic-calendar/ajaxUnSubscribeGroupEvent' => 'calendarEvent/ajaxUnSubscribeGroupEvent',
							'economic-calendar/export' => 'calendarEvent/export',
							
							'economic-calendar/<period:today>/' => 'calendarEvent/list',
							'economic-calendar/<period:tomorrow>/' => 'calendarEvent/list',
							'economic-calendar/<period:this_week>/' => 'calendarEvent/list',
							'economic-calendar/<period:next_week>/' => 'calendarEvent/list',
							'economic-calendar/<period:date_v>/' => 'calendarEvent/list',
							'economic-calendar/<period:day>/' => 'calendarEvent/list',
							'economic-calendar/<period:soon>/' => 'calendarEvent/list',
							'economic-calendar/<period:soon_more>/' => 'calendarEvent/list',
							'economic-calendar/<period:forthcoming>/' => 'calendarEvent/list',
							'economic-calendar/<period:choice_dates>/' => 'calendarEvent/list',
							'economic-calendar/<period:futureOfEvent>/' => 'calendarEvent/list',
												
							'economic-calendar/<d_view:[0-9]{2}.[0-9]{2}.[0-9]{4}>/' => 'calendarEvent/list',
							
							'economic-calendar/<slug:[a-zA-Z0-9-]+>/' => 'calendarEvent/single',
						
							'economic-calendar/' => 'calendarEvent/list',
							'economic-calendar' => 'calendarEvent/list',
							
							
							'monitoring/ajaxLoadAccountsList' => 'monitoring/ajaxLoadAccountsList',
							'monitoring/ajaxLoadOneAccountInfo' => 'monitoring/ajaxLoadOneAccountInfo',
							'monitoring/ajaxLoadYourAccounts' => 'monitoring/ajaxLoadYourAccounts',
							'monitoring/ajaxSaveViews' => 'monitoring/ajaxSaveViews',
							'monitoring/ajaxLoadInformerSettingsWidget' => 'monitoring/ajaxLoadInformerSettingsWidget',
							'monitoring/frameForForum' => 'monitoring/frameForForum',
							'monitoring/iframeAddReport' => 'monitoring/iframeAddReport',
					
							
							'monitoring/' => 'monitoring/index',
							'monitoring' => 'monitoring/index',
	
							'monitoring/<id:[0-9]+>/' => 'monitoring/single',
							'monitoring' => 'monitoring', 
							
							'categoryRatings/ajaxAddMark' => 'categoryRatings/ajaxAddMark',
							'categoryRatings/ajaxLoadList' => 'categoryRatings/ajaxLoadList',
							'<slug:earating>/' => 'categoryRatings/single',
							'<slug:strategyrating>/' => 'categoryRatings/single',
							'<slug:indicatorsrating>/' => 'categoryRatings/single',
							
							'author/settings' => 'user/settings',
                            'author/<slug:.+>/' => 'user/single',
                            
                            'cryptocurrencies/ajaxLoadChartData' => 'cryptoCurrencies/ajaxLoadChartData',
                            'cryptocurrencies/ajaxLoadCryptoExchange' => 'cryptoCurrencies/ajaxLoadCryptoExchange',
                            'cryptocurrencies/exchageRedirect' => 'cryptoCurrencies/exchageRedirect',
                            'cryptocurrencies/' => 'cryptoCurrencies/index',
							'cryptocurrencies' => 'cryptoCurrencies/index',
							
							
							'sitemap-<type:action-currency-single-convert><page:[0-9]*>.<format:xml>' => 'sitemap/default/index',
							'sitemap-<type:action-currency-single-archive><page:[0-9]*>.<format:xml>' => 'sitemap/default/index',
							'sitemap-<type:action-currency-archive><page:[0-9]*>.<format:xml>' => 'sitemap/default/index',
							'sitemap-<type:action-currency-archive-ecb><page:[0-9]*>.<format:xml>' => 'sitemap/default/index',
							'sitemap-<type:currencies><page:[0-9]*>.<format:xml>' => 'sitemap/default/index',
							'sitemap-<type:quotes><page:[0-9]*>.<format:xml>' => 'sitemap/default/index',
							'sitemap-<type:action-calendar-event-single><page:[0-9]*>.<format:xml>' => 'sitemap/default/index',
							'sitemap-<type:other><page:[0-9]*>.<format:xml>' => 'sitemap/default/index',
							
		                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',  
		                  ),
		            ),
			'assetManager' => Array(
				'class' => 'components.sys.AssetManager',
                'basePath' => $baseDir.'/../assets/',
            ),
			'image'=>array(
				'class'=>'application.extensions.image.CImageComponent',
				// GD or ImageMagick
				'driver'=>'GD',
				// ImageMagick setup path
				'params'=>array('directory'=>'/opt/local/bin'),
			),
			'easyImage' => array(
				'class' => 'application.extensions.easyimage.EasyImage',
				'driver' => 'GD',
				'quality' => 100,
				//'cachePath' => '/assets/easyimage/',
				//'cacheTime' => 2592000,
				//'retinaSupport' => false,
			),
			'log' => Array(
				'class' => 'CLogRouter',
				'routes' => Array(
					Array(
						'class' => 'CProfileLogRoute',
						'levels' => 'profile',
						'enabled' => YII_DEBUG,
					),
					array(
                        'class' => 'CWebLogRoute',
                        'levels'=>'error, warning, trace, profile, info',
                        'enabled' => YII_DEBUG,
                    ),
					array(
						'class'=>'CFileLogRoute',
						'levels'=>'error',
						'except' => 'exception.*,exchangerParser.*',
						'logFile' => 'errors.log',
						'enabled' => YII_LOG_ERRORS,
					),
					array(
						'class'=>'CFileLogRoute',
						'levels'=>'error',
						'except' => 'exception.*,exchangerParser.*',
						'logFile' => 'errors_ex.log',
						'filter' => 'CLogFilter',
						'enabled' => YII_LOG_ERRORS,
                    ),
                   array(
                        'class'=>'CFileLogRoute',
						'logFile' => 'exchanger-parser.log',
                        'categories'=>'exchangerParser.*',
                        'levels'=>'error,info,warning',
                    ),
                    array(
                        'class'=>'CFileLogRoute',
						'logFile' => 'exchanger-parser-not-found-currencies.log',
                        'categories'=>'exchangerParserNotFoundCurrencies.*',
                        'levels'=>'error,info,warning',
                    ),
				),
			),
			'bootstrap' => Array(
				'class' => 'bootstrap.components.Bootstrap',
			),
			'messages' => Array(
				'class' => 'components.sys.DbMessageSource',
				'cachingDuration' => 86400,
			),
			'user' => Array(
				'class' => 'components.sys.WebUser',
				'allowAutoLogin' => true,
				'loginUrl' => Array( 'default/login' ),
				'returnUrl' => '/',
			),
			'cache' => Array(
				'class' => 'system.caching.CFileCache',
			),
			'cache2' => Array(
				'class' => 'system.caching.CFileCache',
				'cachePath' => 'protected/runtime/cache2',
			),
			'cacheSiteMap' => Array(
				'class' => 'system.caching.CFileCache',
				'cachePath' => 'protected/runtime/cacheSiteMap',
			),
			'clientScript' => Array(
				'class' => 'components.sys.ClientScript',
			),
			'errorHandler'=>array(
				'adminInfo'=>'admin3@fortrader.org',
				'errorAction'=>'default/error', 
            ),
            'apns' => array(
                'class' => 'ext.yii-apns-gcm.YiiApns',
                //'environment' => 'production',
                //'pemFile' => dirname(__FILE__).'/certificates/apns-prod.pem',
                'environment' => 'sandbox',
                'pemFile' => dirname(__FILE__).'/certificates/apns-dev.pem',

                'dryRun' => false,
                // 'retryTimes' => 3,
                'options' => array(
                    'sendRetryTimes' => 5
                ),
            ),
			'request' => array(
				'hostInfo' => $hostInfo,
				'baseUrl' => $baseUrl,
			),
		),
		'params' => $params,
	);
	
?>
