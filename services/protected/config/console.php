<?php
// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.

$console = array(
  'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

  // preloading 'log' component
  'preload'=>array('log'),

  'import' => array(
    'application.components.console.*',
  ),

    // application components
  'components'=>array(
    'log'=>array(
      'class'=>'CLogRouter',
      'routes'=>array(
        array(
          'class'=>'CFileLogRoute',
          'levels'=>'error, warning, info',
          'logFile'=>'console.log',
        ),
      ),
    ),
  ),

  'commandMap'=>array(
    'migrate'=>array(
      'class'=>'system.cli.commands.MigrateCommand',
      'templateFile'=>'application.migrations.template',
      'onBeforeAction' => function (CEvent $e) {
          $conn = Yii::app()->db->connectionString;
          echo '      Current DB: _____________________ ' . substr($conn, strpos($conn, 'dbname=')+7) . " _____________________\n\n";
        },
    ),
  ),
);

$main = require(dirname(__FILE__).DIRECTORY_SEPARATOR.'default.php');
$console['components']['db'] = $main['components']['db'];
$console['name'] = $main['name'];
$console['import'] = array_merge($console['import'], $main['import']);

// for mail
$console['params']['log_admins'] = $main['params']['log_admins'];
$console['params']['debug_mailer'] = $main['params']['debug_mailer'];
$console['params']['smtp'] = $main['params']['smtp'];

return $console;