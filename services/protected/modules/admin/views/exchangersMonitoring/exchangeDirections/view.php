<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('exchangersMonitoring/exchangeDirections/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Exchange Directions')?></h3>
        <p><?=Yii::t($NSi18n, 'Manage exchange directions here')?></p>

    </div>
    <?php
        $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
            'ns' => 'nsActionView',
            'addLabel' => false,

            'modelName' => 'PaymentExchangeDirectionModel',
            'formModelName' => 'AdminPaymentExchangeDirectionFormModel',
            'gridViewWidget' => 'AdminPaymentExchangeDirectionGridViewWidget',
            'formWidget' => 'AdminPaymentExchangeDirectionFormWidget',

            'loadRowRoute' => 'admin/exchangersMonitoring/ajaxLoadListRow',
            'loadItemRoute' => 'admin/exchangersMonitoring/ajaxLoadItem',
            'deleteItemRoute' => 'admin/exchangersMonitoring/ajaxDeleteItem',
            'submitItemRoute' => 'admin/exchangersMonitoring/ajaxAddItem',
        ));
    ?>
</div>