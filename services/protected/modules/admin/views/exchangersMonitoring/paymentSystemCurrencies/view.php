<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('exchangersMonitoring/paymentSystemCurrencies/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Payment system currencies')?></h3>
        <p><?=Yii::t($NSi18n, 'Manage payment system currencies here')?></p>

    </div>
    <?php
        $this->widget(
            'widgets.ChooseInTableWidget',
            array(
                'ns' => $ns,
                'param' => 'idSystem',
                'linkRout' => 'admin/' . Yii::app()->controller->id . '/' . Yii::app()->controller->action->id ,
                'options' => PaymentSystemModel::getListForFilter(),
                'header' => Yii::t($NSi18n, 'Select payment system')
            )
        );
       
        if (Yii::app()->request->getParam('idSystem')) {
            $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
                'ns' => 'nsActionView',
                'addLabel' => Yii::t($NSi18n, 'Add currency'),
    
                'modelName' => 'PaymentSystemCurrencyModel',
                'formModelName' => 'AdminPaymentSystemCurrencyFormModel',
                'gridViewWidget' => 'AdminPaymentSystemCurrencyGridViewWidget',
                'formWidget' => 'AdminPaymentSystemCurrencyFormWidget',
    
                'loadRowRoute' => 'admin/exchangersMonitoring/ajaxLoadListRow',
                'loadItemRoute' => 'admin/exchangersMonitoring/ajaxLoadItem',
                'deleteItemRoute' => 'admin/exchangersMonitoring/ajaxDeleteItem',
                'submitItemRoute' => 'admin/exchangersMonitoring/ajaxAddItem',
            ));
        }

    ?>
</div>