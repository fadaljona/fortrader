<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('exchangersMonitoring/paymentSystems/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Payment systems')?></h3>
        <p><?=Yii::t($NSi18n, 'Manage payment systems here')?></p>

    </div>
    <?php
        $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
            'ns' => 'nsActionView',
            'addLabel' => Yii::t($NSi18n, 'Add payment system'),

            'modelName' => 'PaymentSystemModel',
            'formModelName' => 'AdminPaymentSystemFormModel',
            'gridViewWidget' => 'AdminPaymentSystemGridViewWidget',
            'formWidget' => 'AdminPaymentSystemFormWidget',

            'loadRowRoute' => 'admin/exchangersMonitoring/ajaxLoadListRow',
            'loadItemRoute' => 'admin/exchangersMonitoring/ajaxLoadItem',
            'deleteItemRoute' => 'admin/exchangersMonitoring/ajaxDeleteItem',
            'submitItemRoute' => 'admin/exchangersMonitoring/ajaxAddItem',
        ));
    ?>
</div>