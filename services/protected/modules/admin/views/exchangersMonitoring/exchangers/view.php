<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('exchangersMonitoring/exchangers/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Exchangers')?></h3>
        <p><?=Yii::t($NSi18n, 'Manage exchangers here')?></p>

    </div>
    <?php
        $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
            'ns' => 'nsActionView',
            'addLabel' => Yii::t($NSi18n, 'Add exchanger'),

            'modelName' => 'PaymentExchangerModel',
            'formModelName' => 'AdminPaymentExchangerFormModel',
            'gridViewWidget' => 'AdminPaymentExchangerGridViewWidget',
            'formWidget' => 'AdminPaymentExchangerFormWidget',

            'loadRowRoute' => 'admin/exchangersMonitoring/ajaxLoadListRow',
            'loadItemRoute' => 'admin/exchangersMonitoring/ajaxLoadItem',
            'deleteItemRoute' => 'admin/exchangersMonitoring/ajaxDeleteItem',
            'submitItemRoute' => 'admin/exchangersMonitoring/ajaxAddItem',
        ));
    ?>
</div>