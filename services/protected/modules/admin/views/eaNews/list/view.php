<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'eaNews/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'News' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can manage ea monitoring news.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminEANewsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>