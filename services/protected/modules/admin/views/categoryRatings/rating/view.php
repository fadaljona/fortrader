<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'categoryRatings/rating/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Category ratings' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage category ratings here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'addLabel' => Yii::t( $NSi18n, 'Add category rating' ),
			
			'modelName' => 'CategoryRatingModel',
			'formModelName' => 'AdminCategoryRatingFormModel',
			'gridViewWidget' => 'AdminCategoryRatingGridViewWidget',
			'formWidget' => 'AdminCategoryRatingFormWidget',
			
			'loadRowRoute' => 'admin/categoryRatings/ajaxLoadListRow',
			'loadItemRoute' => 'admin/categoryRatings/ajaxLoadItem',
			'deleteItemRoute' => 'admin/categoryRatings/ajaxDeleteItem',
			'submitItemRoute' => 'admin/categoryRatings/ajaxAddItem',
		));
	?>
</div>