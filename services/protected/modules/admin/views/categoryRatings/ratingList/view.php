<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisement/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Items of the rating' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Items of the rating here' )?></p>
	</div>
	<?
		$this->widget( 'widgets.ChooseInTableWidget', Array( 'ns' => $ns, 'param' => 'idRating', 'linkRout' => 'admin/categoryRatings/' . Yii::app()->controller->action->id , 'options' => $ratings, 'header' => Yii::t( $NSi18n, 'Select rating' ) )); 
		

		
		if( isset( $_GET['idRating'] ) ){
			$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
				'ns' => 'nsActionView',
				'addLabel' => false,
				
				'modelName' => 'CategoryRatingPostModel',
				'formModelName' => 'AdminCategoryRatingPostFormModel',
				'gridViewWidget' => 'AdminCategoryRatingPostGridViewWidget',
				'formWidget' => 'AdminCategoryRatingPostFormWidget',
				
				'loadRowRoute' => 'admin/categoryRatings/ajaxLoadPostListRow',
				'loadItemRoute' => 'admin/categoryRatings/ajaxLoadItem',
				'deleteItemRoute' => 'admin/categoryRatings/ajaxDeleteItem',
				'submitItemRoute' => 'admin/categoryRatings/ajaxAddItem',
				
				'enableSorting' => true,
				'filterEnabled' => true,
				'filterModelName' => 'WPTermRelationshipModel',
				
				'filterUrl' => Yii::app()->createUrl( 'admin/categoryRatings/ratingList', array( 'idRating' => $_GET['idRating'] ) ),
			));
		}
		
	
	?>
</div>