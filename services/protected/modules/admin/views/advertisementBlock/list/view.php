<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisementBlock/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Blocks' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of advertisements blocks.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminAdvertisementBlocksEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>