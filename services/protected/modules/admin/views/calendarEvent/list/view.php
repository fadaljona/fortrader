<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'calendarEvent/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Calendar events' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of calendar events.' )?></p>
	</div>

	<?
		$this->widget( 'widgets.editors.AdminCalendarEventsEditorWidget', Array(
			'ns' => 'nsActionView',
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
		));
	?>
</div>
