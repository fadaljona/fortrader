<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'brokerRegulator/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Brokers regulators' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of brokers regulators.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminBrokerRegulatorsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>