<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'feedback/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Feedbacks' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminFeedbacksEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>