<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'contest/errors/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Monitoring errors' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage monitoring errors here' )?></p>
	</div>
	
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => 'ContestMonitoringErrorsModel',
			'formModelName' => 'AdminContestMonitoringErrorsFormModel',
			'addLabel' => Yii::t( $NSi18n, 'Add error' ),
			'gridViewWidget' => 'AdminContestMonitoringGridViewWidget',
			'formWidget' => 'AdminContestMonitoringErrorsFormWidget',
			'loadRowRoute' => 'admin/contest/ajaxLoadListRow',
			'loadItemRoute' => 'admin/contest/ajaxLoadItem',
			'deleteItemRoute' => 'admin/contest/ajaxDeleteItem',
			'submitItemRoute' => 'admin/contest/ajaxAddItem',
		));
	?>
	
	<h3><?=Yii::t( $NSi18n, 'Errors in database' )?></h3>
	
	<?
		$gridview = $this->widget( 'widgets.gridViews.AdminContestMonitoringGroupedErrorsGridViewWidget' , Array(
			'NSi18n' => $NSi18n,
			'ins' => $NSi18n,
			'showTableOnEmpty' => true,
			'dataProvider' => MT5AccountInfoModel::getGroupedErrorsDp(),
			'ajax' => false,
			
		));
	?>
	
</div>