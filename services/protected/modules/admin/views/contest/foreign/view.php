<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'contest/foreign/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Foreign Contests' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage foreign contests here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminForeignContestsEditorWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => 'ContestForeignModel',
			'formModelName' => 'ContestForeignFormModel',
		));
	?>
</div>