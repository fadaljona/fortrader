<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'contest/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Contests' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of contests.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminContestsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>