<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'eaStatement/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'EA Statements' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can manage ea statements.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminEAStatementsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>