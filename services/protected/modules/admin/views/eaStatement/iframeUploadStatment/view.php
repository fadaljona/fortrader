<?
	Yii::app()->clientScript->registerCoreScript( 'jquery' );
?>
<script>
	!function( $ ) {
		<?if( !empty( $data->error )){?>
			var error = <?=json_encode( $data->error )?>;
			parent.nsActionView.wAdminEAStatementFormWidget.uploadStatementError( error );
		<?}else{?>
			var data = <?=@json_encode( $data )?>;
			parent.nsActionView.wAdminEAStatementFormWidget.uploadStatementSuccess( data );
		<?}?>
	}( window.jQuery );
</script>