<?php
Yii::import('controllers.base.ViewAdminBase');
Yii::import('components.widgets.forms.AdminReplaceItemsWidget');
$NSi18n = ViewAdminBase::getNSi18n('helper/modelsForReplace/view');

$classNames = AdminReplaceItemsWidget::$models;
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Models for replace')?></h3>
    </div>

    <?php
    foreach ($classNames as $class) {
        echo '<pre>';
        echo $class . "\n";

        print_r($class::getPropertiesForSearch());

        echo '</pre>';
    }
    ?>

</div> 