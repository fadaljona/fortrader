<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'helper/flushCache/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Flush Cache' )?></h3>
		<?php
			if( $result ){
				echo CHtml::tag( 'p', array(), Yii::t( $NSi18n, 'Cache from runtime/cache removed' ) );
			}else{
				echo CHtml::tag( 'p', array(), Yii::t( $NSi18n, 'CACHE NOT REMOVED' ) );
			}
				
		?>

	</div>

</div>