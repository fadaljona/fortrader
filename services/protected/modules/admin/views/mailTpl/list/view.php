<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'mailTpl/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Mails templates' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of mails templates.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminMailTplsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>