<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'tradeAccount/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Trade accounts' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can manage all trade accounts.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminTradeAccountsEditorWidget', Array(
			'ns' => 'nsActionView',
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
		));
	?>
</div>