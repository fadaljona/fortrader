<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotesSettings/list/view');
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( "quotesWidget",'Управление настройками котировок') ?></h3>
		<p><?= Yii::t( "quotesWidget",'Добавление, редактирование и удаление параметров модуля') ?></p>
	</div>
	<? 
		$this->widget('widgets.forms.AdminQuotesSettingsFormWidget',Array(
			'model' => $formModel,
			'ns' => 'nsActionView',
			'ajax' => true
		)); 
	?>
</div>

