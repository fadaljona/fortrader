<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'user/import/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Import' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can import users and groups from other installations of the project.' )?></p>
	</div>
	<?
		echo CHtml::errorSummary( $formModel, null, null, Array( 
			'class' => 'alert alert-block alert-error'
		));
	?>
	<div class="well pull-left iFormWidget i01">
		<?if( !$formModel->inMode( AdminUsersImportFormModel::MODE_HIDDEN )) {?>
			<?$form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
				<?if( $formModel->inMode( AdminUsersImportFormModel::MODE_UPLOAD )) {?>
					<?=$form->labelEx( $formModel, 'file' )?>
					<?=$form->fileField( $formModel, 'file', Array( 'class' => "" ))?>
				<?}?>
				<?if( $formModel->inMode( AdminUsersImportFormModel::MODE_SELECT_EXPORT )) {?>
					<p class="muted">
						<?=Yii::t( $NSi18n, 'File uploaded' )?>
					</p>
					<?=$form->labelEx( $formModel, 'export' )?>
					<label>
						<?=CHtml::radioButton( $formModel->resolveName( 'export' ), $formModel->export == 'users_and_groups', Array( 'id' => false, 'value' => "users_and_groups" ))?>
						<span class="lbl">
							<?=Yii::t( $NSi18n, "Users and groups" )?>
						</span>
					</label>
					<label>
						<?=CHtml::radioButton( $formModel->resolveName( 'export' ), $formModel->export == 'groups', Array( 'id' => false, 'value' => "groups" ))?>
						<span class="lbl">
							<?=Yii::t( $NSi18n, "Groups" )?>
						</span>
					</label>
				<?}?>
				<div class="iControlBlock i01">
					<?if( $formModel->inMode( AdminUsersImportFormModel::MODE_SELECT_EXPORT )) {?>
						<?
							$this->widget( 'bootstrap.widgets.TbButton', Array( 
								'buttonType' => 'submit',
								'type' => 'danger',
								'label' => Yii::t( $NSi18n, 'Cancel' ),
								'htmlOptions' => Array(
									'class' => "pull-left",
									'name' => $formModel->resolveName( 'cancelAction' ),
								),
							));
						?>
					<?}?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Import' ),
							'htmlOptions' => Array(
								'class' => "pull-right",
							),
						));
					?>
				</div>
			<?$this->endWidget()?>
		<?}else{?>
			<?=$message?>
		<?}?>
	</div>
</div>