<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'user/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Users' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of users.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminUsersEditorWidget', Array(
			'type' => 'Users',
			'ns' => 'nsActionView',
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
		));
	?>
</div>