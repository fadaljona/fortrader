<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'user/export/view' );
	
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Export' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can export users and groups in other installations of the project.' )?></p>
	</div>
	<?
		echo CHtml::errorSummary( $formModel, null, null, Array( 
			'class' => 'alert alert-block alert-error'
		));
	?>
	<div class="well pull-left iFormWidget i01">
		<?$form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm' )?>
			<?=$form->labelEx( $formModel, 'export' )?>
			<label>
				<?=CHtml::radioButton( $formModel->resolveName( 'export' ), $formModel->export == 'users_and_groups', Array( 'id' => false, 'value' => "users_and_groups" ))?>
				<span class="lbl">
					<?=Yii::t( $NSi18n, "Users and groups" )?>
				</span>
			</label>
			<label>
				<?=CHtml::radioButton( $formModel->resolveName( 'export' ), $formModel->export == 'groups', Array( 'id' => false, 'value' => "groups" ))?>
				<span class="lbl">
					<?=Yii::t( $NSi18n, "Groups" )?>
				</span>
			</label>
			<div class="iControlBlock i01">
				<?
					$this->widget( 'bootstrap.widgets.TbButton', Array( 
						'buttonType' => 'submit', 
						'type' => 'success',
						'label' => Yii::t( $NSi18n, 'Export' ),
						'htmlOptions' => Array(
							'class' => "pull-right",
						),
					));
				?>
			</div>
		<?$this->endWidget()?>
	</div>
</div>