<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'interestRates/rate/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Values' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage values here' )?></p>

	</div>
	<?
		$idRate = @$_GET['idRate'] ? @$_GET['idRate'] : 0;
		//echo CHtml::dropDownList( "idRate", $idRate, $ratesList, Array( "class" => "{$ins} wSelectRate", 'onchange' => 'document.location.assign( "'.Yii::app()->createUrl('admin/interestRates/values').'?idRate=" + this.value )', 'style' => 'width:auto;' ));
		
		$this->widget( 'widgets.ChooseInTableWidget', Array( 'ns' => 'nsActionView', 'param' => 'idRate', 'linkRout' => 'admin/interestRates/values' , 'options' => $ratesList, 'header' => Yii::t( $NSi18n, 'Select rate' ) ));
		
		if( $idRate ){
			$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
				'ns' => 'nsActionView',
				'addLabel' => Yii::t( $NSi18n, 'Add value' ),
				
				'modelName' => 'InterestRatesValueModel',
				'formModelName' => 'AdminInterestRatesValueFormModel',
				'gridViewWidget' => 'AdminInterestRatesValueGridViewWidget',
				'formWidget' => 'AdminInterestRatesValueFormWidget',
				
				'loadRowRoute' => 'admin/interestRates/ajaxLoadListRow',
				'loadItemRoute' => 'admin/interestRates/ajaxLoadItem',
				'deleteItemRoute' => 'admin/interestRates/ajaxDeleteItem',
				'submitItemRoute' => 'admin/interestRates/ajaxAddItem',
			));
		}
	?>
</div>