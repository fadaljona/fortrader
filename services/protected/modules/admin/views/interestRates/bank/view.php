<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'interestRates/bank/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Banks' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage banks here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'addLabel' => Yii::t( $NSi18n, 'Add bank' ),
			
			'modelName' => 'InterestRatesBankModel',
			'formModelName' => 'AdminInterestRatesBankFormModel',
			'gridViewWidget' => 'AdminInterestRatesBankGridViewWidget',
			'formWidget' => 'AdminInterestRatesBankFormWidget',
			
			'loadRowRoute' => 'admin/interestRates/ajaxLoadListRow',
			'loadItemRoute' => 'admin/interestRates/ajaxLoadItem',
			'deleteItemRoute' => 'admin/interestRates/ajaxDeleteItem',
			'submitItemRoute' => 'admin/interestRates/ajaxAddItem',
		));
	?>
</div>