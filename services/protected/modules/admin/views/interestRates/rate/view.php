<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'interestRates/rate/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Rates' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage rates here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'addLabel' => Yii::t( $NSi18n, 'Add rate' ),
			
			'modelName' => 'InterestRatesRateModel',
			'formModelName' => 'AdminInterestRatesRateFormModel',
			'gridViewWidget' => 'AdminInterestRatesRateGridViewWidget',
			'formWidget' => 'AdminInterestRatesRateFormWidget',
			
			'loadRowRoute' => 'admin/interestRates/ajaxLoadListRow',
			'loadItemRoute' => 'admin/interestRates/ajaxLoadItem',
			'deleteItemRoute' => 'admin/interestRates/ajaxDeleteItem',
			'submitItemRoute' => 'admin/interestRates/ajaxAddItem',
		));
	?>
</div>