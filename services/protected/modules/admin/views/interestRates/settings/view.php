<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('interestRates/settings/view');
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( $NSi18n, 'Interest Rates settings') ?></h3>
	</div>
	<? 
		$this->widget('widgets.forms.AdminInterestRatesSettingsFormWidget',Array(
			'model' => $formModel,
			'ns' => 'nsActionView',
			'ajax' => true
		)); 
	?>
</div>

