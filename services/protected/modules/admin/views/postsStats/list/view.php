<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'postsStats/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Posts stats' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of common posts stats.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.lists.AdminPostsStatsListWidget', Array(
			'ns' => 'nsActionView',
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
			'startPostDate' => $startPostDate,
			'endPostDate' => $endPostDate,
		));
	?>
</div>