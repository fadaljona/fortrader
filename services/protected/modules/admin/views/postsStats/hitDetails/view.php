<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'postsStats/hitDetails/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Post hits' )?></h3>
		<?php
			if( $postId ){
				echo "<p>".Yii::t( $NSi18n, 'Hits for post' )."<br />$postId<br />{$postsStatsCommonModel->title}<br /><a href='{$postsStatsCommonModel->url}' target='_blank'>{$postsStatsCommonModel->url}</a></p>";
			} 
		?>
		<p></p>
	</div>
	<?
		$this->widget( 'widgets.lists.AdminPostHitsListWidget', Array(
			'ns' => 'nsActionView',
			'postId' => $postId,
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
		));
	?>
</div>