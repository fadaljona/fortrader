<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'postsStats/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Wp Search Log' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.lists.AdminPostsStatsWpSearchLogListWidget', Array(
			'ns' => 'nsActionView',
			'startDate' => $startDate,
			'endDate' => $endDate,
		));
	?>
</div>