<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'journal/settings' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Journals settings ' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can change journals settings.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.forms.AdminJournalSettingsFormWidget', Array(
			'model' => $formModel,
			'ns' => 'nsActionView',
			'ajax' => true,
		));
	?>
</div>