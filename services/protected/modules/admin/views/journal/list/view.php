<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'journal/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Journals' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can manage journal.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminJournalsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>
