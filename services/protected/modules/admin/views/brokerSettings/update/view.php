<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'brokerSettings/update/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Settings' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can change the settings of brokers.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.forms.AdminBrokersSettingsFormWidget', Array(
			'model' => $formModel,
			'ns' => 'nsActionView',
			'ajax' => true,
		));
	?>
</div>