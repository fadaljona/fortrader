<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'company/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Companies' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of companies.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminCompaniesEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>