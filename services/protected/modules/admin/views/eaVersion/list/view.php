<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'eaVersion/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Version EA' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can manage ea version.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminEAVersionsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>