<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'manageTextContent/index/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Text Content manager' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Select content type' )?></p>

	</div>
	
	<?php
		$this->widget(
			'bootstrap.widgets.TbMenu',
			array(
				'type' => 'pills',
				'items' => ManageTextContentLib::getTypesTopMenuItems()
			)
		);
		
		$this->widget( 'widgets.AdminManageTextContentTodayStatsWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
	

</div>