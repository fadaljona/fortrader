<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'manageTextContent/single/view' );

	Yii::import( 'models.forms.' . $modelFormName );

?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">





	
	<?php
	if( $type == 'single' ){
		
		$this->widget( 'widgets.AdminManageTextContentFilterWidget', Array(
			'ns' => 'nsActionView',
		));
		$this->widget( 'widgets.AdminManageTextContentTodayStatsWidget', Array(
			'ns' => 'nsActionView',
		));
		$this->widget( 'widgets.lists.AdminManageTextContentListWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => $modelName,
			'modelFormName' => $modelFormName
		));
	}elseif( $type == 'settings' ){
		$this->widget( 'widgets.AdminManageTextContentTodayStatsWidget', Array(
			'ns' => 'nsActionView',
		));
		$this->widget( 'widgets.forms.AdminManageTextContentSettingsFormWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => $modelName,
			'modelFormName' => $modelFormName
		));
	}
	?>
</div>