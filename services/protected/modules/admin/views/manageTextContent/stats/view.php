<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'manageTextContent/stats/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Text Content manager' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Stats' )?></p>

	</div>
	
	
	<?php
		$this->widget( 'widgets.lists.AdminManageTextContentStatsListWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
	
	
</div>