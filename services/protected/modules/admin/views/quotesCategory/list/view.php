<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotesCategory/list/view');
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( "quotesWidget",'Управление Категориями') ?></h3>
		<p><?= Yii::t( "quotesWidget",'Добавление, редактирование и удаление категорий') ?></p>
	</div>
	<? $this->widget( 'widgets.editors.AdminQuotesCategoryEditorWidget', Array('ns' => 'nsActionView', 'categories'=>$categories)); ?>
</div>

