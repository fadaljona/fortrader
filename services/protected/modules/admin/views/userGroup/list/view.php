<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'userGroup/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Users groups' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page contains the list of groups users.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminUserGroupsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>