<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisement/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Advertisements' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of advertisements.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminAdvertisementsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>