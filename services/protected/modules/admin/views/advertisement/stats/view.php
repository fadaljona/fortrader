<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisement/stats/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Stats' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page provides the opportunity to see the statistics of advertising.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.AdminAdvertisementStatsWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>