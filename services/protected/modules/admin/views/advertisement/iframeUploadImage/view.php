<?
	Yii::app()->clientScript->registerCoreScript( 'jquery' );
?>
<script>
	!function( $ ) {
		<?if( !empty( $data->error )){?>
			var error = <?=json_encode( $data->error )?>;
			parent.nsActionView.wAdminAdvertisementFormWidget.uploadImageError( error );
		<?}else{?>
			var path = <?=json_encode( $data->path )?>;
			var width = <? echo $data->width ? $data->width : 200; ?>;
			var height = <? echo $data->height ? $data->height : 200; ?>;
			parent.nsActionView.wAdminAdvertisementFormWidget.uploadImageSuccess( path, width, height );
		<?}?>
	}( window.jQuery );
</script>