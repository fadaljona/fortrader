<?
	Yii::app()->clientScript->registerCoreScript( 'jquery' );
?>
<script>
	!function( $ ) {
		<?if( !empty( $data['error'] )){?>
			var error = <?=json_encode( $data['error'] )?>;
			var errorField = <?=json_encode( @$data['errorField'] )?>;
			parent.nsActionView.wAdminAdvertisementFormWidget.submitError( error, errorField );
		<?}else{?>
			var id = <?=$data['id']?>;
			parent.nsActionView.wAdminAdvertisementFormWidget.submitSuccess( id );
		<?}?>
	}( window.jQuery );
</script>