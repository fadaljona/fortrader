<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisement/ajaxLoadClicks/view' );
?>
<h4><?=Yii::t( $NSi18n, "Clicks by IP" )?></h4>
<?
	$this->widget( 'widgets.gridViews.AdminAdvertisementClicksGridViewWidget', Array(
		'dataProvider' => $DP,
		'sortField' => $sortField,
		'sortType' => $sortType,
	));
?>
<h4><?=Yii::t( $NSi18n, "Clicks by Day" )?></h4>
<?
	$this->widget( 'widgets.gridViews.AdminAdvertisementDayClicksGridViewWidget', Array(
		'dataProvider' => $dayDP,
		'sortDayField' => $sortDayField,
		'sortDayType' => $sortDayType,
	));
	
?>