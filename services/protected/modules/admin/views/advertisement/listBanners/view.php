<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisement/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Banners' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of banners.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminAdvertisementsEditorWidget', Array(
			'ns' => 'nsActionView',
			'type' => 'Image',
		));
	?>
</div>