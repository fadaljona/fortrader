<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'binaryOptionTypes/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Binary Option Assets' )?></h3>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminBrokerOptionsEditorWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => 'BrokerBinaryAssetsModel',
			'formModelName' => 'AdminBrokerBinaryAssetsFormModel',
		));
	?>
</div>