<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'broker/linksClickStats/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Links click log' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.lists.AdminBrokerLinksClickStatsListWidget', Array(
			'ns' => 'nsActionView',
			'startDate' => $startDate,
			'endDate' => $endDate,
		));
	?>
</div>