<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotes/searchStats/view');
?>

<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( "quotesWidget",'Quotes Search Stats') ?></h3>
	</div>
	<? $this->widget( 'widgets.AdminQuotesSearchStatsWidget', Array()); ?>
</div>