<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'quotes/informerLog/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Informer Log' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.lists.AdminQuotesInformerLogListWidget', Array(
			'ns' => 'nsActionView',
			'startDate' => $startDate,
			'endDate' => $endDate,
		));
	?>
</div>