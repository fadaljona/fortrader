<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotes/list/view');
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( "quotesWidget",'Котировки') ?></h3>
		<p><?= Yii::t( "quotesWidget",'Здесь отображается данные парсинга') ?></p>
	</div>
	<div class="wAdminCountriesListWidget">
	<div id="yw1" class="iGridView i01 grid-view insAdminCountriesListWidget wCountriesGridView">
<table class="items table table-bordered">
<thead>
<tr><th><?= Yii::t( "quotesWidget",'Символ')?></th><th><?= Yii::t( "quotesWidget",'Бид')?></th><th><?= Yii::t( "quotesWidget",'Аск')?></th>
	<th><?= Yii::t( "quotesWidget",'Время')?></th><th><?= Yii::t( "quotesWidget",'Время обновления')?></th><th><?= Yii::t( "quotesWidget",'Спред')?></th>
	<th><?= Yii::t( "quotesWidget",'Рост')?></th><th><?= Yii::t( "quotesWidget",'Старт торгов')?></th><th><?= Yii::t( "quotesWidget",'Окончание торгов')?></th></tr>
</thead>
<tbody>
<?php $i=1; foreach($data as $string):?>
<tr class="<?=($i%2)?'even':'odd'; $i++;?>">
	<td><?=$string['symbol']?></td><td><?=$string['bid']?></td><td><?=$string['ask']?></td>
	<td><?=$string['time']?></td><td><?=$string['time_up']?></td><td><?=$string['spread']?></td>
	<td><?=$string['trend']?></td><td><?=$string['start_trading']?></td><td><?=$string['end_trading']?></td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
	</div>
</div>
</div>