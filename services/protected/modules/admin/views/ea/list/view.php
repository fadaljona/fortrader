<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'ea/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'EA' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can manage ea monitoring.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminEAEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>