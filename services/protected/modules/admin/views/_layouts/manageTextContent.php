<?$this->beginContent( '//_layouts/default' )?>
	<script>
		var nsLayoutDefaultIndex = {};
	</script>
	<?$this->widget( 'widgets.AdminNavbarWidget' )?>
	<div class="container-fluid nsLayoutDefaultIndex">
		<div class="row-fluid">
			<div class="span12">
				<?php
					$this->widget(
						'bootstrap.widgets.TbMenu',
						array(
							'type' => 'pills',
							'items' => ManageTextContentLib::getTypesTopMenuItems()
						)
					);
				?>
				<?=$content?>
			</div>
		</div>
		<hr>
		<footer>
			<div class="pull-left">
				© <?=Yii::App()->name?>
			</div>
			<div class="pull-right">
				<?$this->widget( "components.widgets.ChangeLanguageWidget", Array(
					'ns' => 'nsLayoutDefaultIndex',
				))?>
			</div>
		</footer>
	</div>
<?$this->endContent()?>
<?
	$baseUrl = Yii::app()->baseUrl;
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/assets-static/ckeditor/ckeditor.js" );
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/assets-static/ckeditor/adapters/jquery.js" );
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/js/engine-admin-78rt09gy.js" );
?>