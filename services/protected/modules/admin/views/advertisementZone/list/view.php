<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisementZone/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Zone code and design ads' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page provides the opportunity to design your updates and get the code to be installed on site.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminAdvertisementZonesEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>