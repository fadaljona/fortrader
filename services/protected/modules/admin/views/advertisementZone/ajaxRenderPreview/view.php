<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	Yii::import( 'widgets.AdvertisementZoneWidget' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisementZone/renderPreview/view' );
?>
<h3 style="margin-top:0px;"><?=Yii::t( $NSi18n, 'Preview' )?></h3>
<?
	$this->widget( 'widgets.AdvertisementZoneWidget', Array(
		'mode' => AdvertisementZoneWidget::MODE_PREVIEW,
		'zone' => $zone,
	));
?>
