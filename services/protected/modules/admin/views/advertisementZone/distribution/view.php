<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisementZone/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Zone ads distribution' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page provides the opportunity to view zone ads distribution.' )?></p>
	</div>

	<?
		$this->widget( 'widgets.lists.AdminAdvertisementZonesDistributionListWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>