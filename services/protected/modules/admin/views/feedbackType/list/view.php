<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'feedbackType/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Feedback types' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminFeedbackTypesEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>