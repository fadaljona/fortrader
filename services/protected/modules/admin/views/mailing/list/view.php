<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'mailing/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Mailings' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page provides the ability to send e-mail and SMS messages to your people.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminMailingsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>