<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'brokerIO/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Brokers input/output' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of brokers input/output.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminBrokerIOsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>