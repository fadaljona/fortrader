<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'contact/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Contacts' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminContactsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>