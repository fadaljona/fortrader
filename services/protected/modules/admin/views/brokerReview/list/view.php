<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'brokerReview/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Brokers reviews' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of brokers reviews.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminUserMessagesEditorWidget', Array(
			'ns' => 'nsActionView',
			'instance' => 'broker/single',
		));
	?>
</div>