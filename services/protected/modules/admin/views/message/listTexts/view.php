<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'message/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Texts' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of texts.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminMessagesEditorWidget', Array(
			'ns' => 'nsActionView',
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
			'type' => 'text',
		));
	?>
</div>