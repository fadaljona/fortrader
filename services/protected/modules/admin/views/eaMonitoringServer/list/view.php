<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'eaMonitoringServer/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Monitoring servers' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can manage servers accounts ea monitoring.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminEAMonitoringServersEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>