<?
	$info = $this->detLayoutInfo();
	$NSi18n = $this->getNSi18n();
?>
<div class="well">
	<h3><?=Yii::t( $NSi18n, $this->detLayoutTitle() )?></h3>
	<?if( strlen( $info )){?>
		<p><?=Yii::t( $NSi18n, $info )?></p>
	<?}?>
</div>
<?AdminEditorWidgetBase::widgetByModelName( $this->getModelName() );?>