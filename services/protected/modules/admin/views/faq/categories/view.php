<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('faw/categories/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Faq categories')?></h3>
        <p><?=Yii::t($NSi18n, 'Manage faq categories here')?></p>

    </div>
    <?php
        $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
            'ns' => 'nsActionView',
            'addLabel' => Yii::t($NSi18n, 'Add category'),

            'modelName' => 'FaqModel',
            'formModelName' => 'AdminFaqFormModel',
            'gridViewWidget' => 'AdminFaqGridViewWidget',
            'formWidget' => 'AdminFaqFormWidget',

            'loadRowRoute' => 'admin/faq/ajaxLoadListRow',
            'loadItemRoute' => 'admin/faq/ajaxLoadItem',
            'deleteItemRoute' => 'admin/faq/ajaxDeleteItem',
            'submitItemRoute' => 'admin/faq/ajaxAddItem',
        ));
    ?>
</div>
