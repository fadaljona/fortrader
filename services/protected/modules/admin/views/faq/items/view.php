<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('faq/items/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Items')?></h3>
    </div>
    <?php
        $this->widget(
            'widgets.ChooseInTableWidget',
            array(
                'ns' => $ns,
                'param' => 'idFaq',
                'linkRout' => 'admin/' . Yii::app()->controller->id . '/' . Yii::app()->controller->action->id ,
                'options' => FaqModel::getListForFilter(),
                'header' => Yii::t($NSi18n, 'Select')
            )
        );
       
        if (Yii::app()->request->getParam('idFaq')) {
            $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
                'ns' => 'nsActionView',
                'addLabel' => Yii::t($NSi18n, 'Add item'),
    
                'modelName' => 'FaqItemModel',
                'formModelName' => 'AdminFaqItemFormModel',
                'gridViewWidget' => 'AdminFaqItemGridViewWidget',
                'formWidget' => 'AdminFaqItemFormWidget',
    
                'loadRowRoute' => 'admin/faq/ajaxLoadListRow',
                'loadItemRoute' => 'admin/faq/ajaxLoadItem',
                'deleteItemRoute' => 'admin/faq/ajaxDeleteItem',
                'submitItemRoute' => 'admin/faq/ajaxAddItem',
            ));
        }

    ?>
</div>