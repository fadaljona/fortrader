<?php
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('cryptoCurrencies/settings/view');
?>
<script>var nsActionView = {};</script>
<div class="nsActionView">
    <div class="well">
        <h3><?= Yii::t($NSi18n, 'Settings') ?></h3>
    </div>
    <?php
        $this->widget('widgets.forms.AdminCryptoCurrenciesSettingsFormWidget', array(
            'model' => $formModel,
            'ns' => 'nsActionView',
            'ajax' => true
        ));
    ?>
</div>

