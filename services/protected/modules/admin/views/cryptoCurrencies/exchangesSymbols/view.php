<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('cryptoCurrencies/exchangesSymbols/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Symbol to our symbol')?></h3>

    </div>
    <?php
        $this->widget(
            'widgets.ChooseInTableWidget',
            array(
                'ns' => $ns,
                'param' => 'idExchange',
                'linkRout' => 'admin/' . Yii::app()->controller->id . '/' . Yii::app()->controller->action->id ,
                'options' => CryptoCurrenciesExchangesModel::getListForFilter(),
                'header' => Yii::t($NSi18n, 'Select')
            )
        );
       
        if (Yii::app()->request->getParam('idExchange')) {
            $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
                'ns' => 'nsActionView',
                'addLabel' => Yii::t($NSi18n, ''),
    
                'modelName' => 'CryptoCurrenciesExchangesSymbolToOurSymbolModel',
                'formModelName' => 'AdminCryptoCurrenciesExchangesSymbolToOurSymbolFormModel',
                'gridViewWidget' => 'AdminCryptoCurrenciesExchangesSymbolToOurSymbolGridViewWidget',
                'formWidget' => 'AdminCryptoCurrenciesExchangesSymbolToOurSymbolFormWidget',
    
                'loadRowRoute' => 'admin/cryptoCurrencies/ajaxLoadListRow',
                'loadItemRoute' => 'admin/cryptoCurrencies/ajaxLoadItem',
                'deleteItemRoute' => 'admin/cryptoCurrencies/ajaxDeleteItem',
                'submitItemRoute' => 'admin/cryptoCurrencies/ajaxAddItem',
            ));
        }

    ?>
</div>