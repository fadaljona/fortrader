<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('cryptoCurrencies/exchangesList/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Exchanges')?></h3>
        <p><?=Yii::t($NSi18n, 'Manage exchanges here')?></p>

    </div>
    <?php
        $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
            'ns' => 'nsActionView',
            'addLabel' => Yii::t($NSi18n, 'Add Exchanges'),

            'modelName' => 'CryptoCurrenciesExchangesModel',
            'formModelName' => 'AdminCryptoCurrenciesExchangesFormModel',
            'gridViewWidget' => 'AdminCryptoCurrenciesExchangesGridViewWidget',
            'formWidget' => 'AdminCryptoCurrenciesExchangesFormWidget',

            'loadRowRoute' => 'admin/cryptoCurrencies/ajaxLoadListRow',
            'loadItemRoute' => 'admin/cryptoCurrencies/ajaxLoadItem',
            'deleteItemRoute' => 'admin/cryptoCurrencies/ajaxDeleteItem',
            'submitItemRoute' => 'admin/cryptoCurrencies/ajaxAddItem',
        ));
    ?>
</div>
