<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'eaTradeAccount/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Trade accounts' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can manage trade accounts ea monitoring.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminEATradeAccountsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>