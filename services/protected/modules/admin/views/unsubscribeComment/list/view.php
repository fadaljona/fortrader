<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'mailing/unsubscribeComment/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Unsubscribe comments' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminUnsubscribeCommentsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>