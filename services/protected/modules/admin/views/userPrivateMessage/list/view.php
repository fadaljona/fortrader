<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'userPrivateMessage/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Private messages' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of users private messages.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminUserPrivateMessagesEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>