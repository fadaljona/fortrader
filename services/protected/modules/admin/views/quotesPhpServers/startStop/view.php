<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotesPhpServers/startStop/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?= Yii::t("quotesWidget", 'Stop/Start scripts') ?></h3>
    </div>
    
    <div class="iGridView i01 grid-view">
    <table class="items table table-bordered">
        <thead>
            <tr>
                <th><?= Yii::t("quotesWidget", 'Script') ?></th>
                <th><?= Yii::t("quotesWidget", 'Status') ?></th>
                <th><?= Yii::t("quotesWidget", 'PS') ?></th>
                <th><?= Yii::t("quotesWidget", 'Action') ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?= Yii::t("quotesWidget", 'Own tick quotes') ?></td>
                <td><?=$quotesServer ? Yii::t("quotesWidget", 'Launched') : Yii::t("quotesWidget", 'Stopped') ?></td>
                <td><?=$quotesServer ? $quotesServerRes : ''?></td>
                <td>
                <?php
                    if ($quotesServer) {
                        echo CHtml::link('Stop', array('quotesPhpServers/startStop', 'server' => 'quotesServer', 'act' => 'stop' ));
                    } else {
                        echo CHtml::link('Run', array('quotesPhpServers/startStop', 'server' => 'quotesServer', 'act' => 'run' ));
                    }
                ?>
                </td>
            </tr>
            <tr>
                <td><?= Yii::t("quotesWidget", 'Own history quotes') ?></td>
                <td><?=$quotesHistoryServer ? Yii::t("quotesWidget", 'Launched') : Yii::t("quotesWidget", 'Stopped') ?></td>
                <td><?=$quotesHistoryServer ? $quotesHistoryServerRes : ''?></td>
                <td>
                <?php
                    if ($quotesHistoryServer) {
                        echo CHtml::link('Stop', array('quotesPhpServers/startStop', 'server' => 'quotesHistoryServer', 'act' => 'stop' ));
                    } else {
                        echo CHtml::link('Run', array('quotesPhpServers/startStop', 'server' => 'quotesHistoryServer', 'act' => 'run' ));
                    }
                ?>
                </td>
            </tr>
            <tr>
                <td><?= Yii::t("quotesWidget", 'Bitz tick quotes') ?></td>
                <td><?=$bitzQuotesServer ? Yii::t("quotesWidget", 'Launched') : Yii::t("quotesWidget", 'Stopped') ?></td>
                <td><?=$bitzQuotesServer ? $bitzQuotesServerRes : ''?></td>
                <td>
                <?php
                    if ($bitzQuotesServer) {
                        echo CHtml::link('Stop', array('quotesPhpServers/startStop', 'server' => 'bitzQuotesServer', 'act' => 'stop' ));
                    } else {
                        echo CHtml::link('Run', array('quotesPhpServers/startStop', 'server' => 'bitzQuotesServer', 'act' => 'run' ));
                    }
                ?>
                </td>
            </tr>
            <tr>
                <td><?= Yii::t("quotesWidget", 'Bitz history quotes') ?></td>
                <td><?=$bitzQuotesHistoryServer ? Yii::t("quotesWidget", 'Launched') : Yii::t("quotesWidget", 'Stopped') ?></td>
                <td><?=$bitzQuotesHistoryServer ? $bitzQuotesHistoryServerRes : ''?></td>
                <td>
                <?php
                    if ($bitzQuotesHistoryServer) {
                        echo CHtml::link('Stop', array('quotesPhpServers/startStop', 'server' => 'bitzQuotesHistoryServer', 'act' => 'stop' ));
                    } else {
                        echo CHtml::link('Run', array('quotesPhpServers/startStop', 'server' => 'bitzQuotesHistoryServer', 'act' => 'run' ));
                    }
                ?>
                </td>
            </tr>





            <tr>
                <td><?= Yii::t("quotesWidget", 'bitfinex tick quotes') ?></td>
                <td><?=$bitfinexQuotesServer ? Yii::t("quotesWidget", 'Launched') : Yii::t("quotesWidget", 'Stopped') ?></td>
                <td><?=$bitfinexQuotesServer ? $bitfinexQuotesServerRes : ''?></td>
                <td>
                <?php
                    if ($bitfinexQuotesServer) {
                        echo CHtml::link('Stop', array('quotesPhpServers/startStop', 'server' => 'bitfinexQuotesServer', 'act' => 'stop' ));
                    } else {
                        echo CHtml::link('Run', array('quotesPhpServers/startStop', 'server' => 'bitfinexQuotesServer', 'act' => 'run' ));
                    }
                ?>
                </td>
            </tr>
            <tr>
                <td><?= Yii::t("quotesWidget", 'bitfinex history quotes') ?></td>
                <td><?=$bitfinexQuotesHistoryServer ? Yii::t("quotesWidget", 'Launched') : Yii::t("quotesWidget", 'Stopped') ?></td>
                <td><?=$bitfinexQuotesHistoryServer ? $bitfinexQuotesHistoryServerRes : ''?></td>
                <td>
                <?php
                    if ($bitfinexQuotesHistoryServer) {
                        echo CHtml::link('Stop', array('quotesPhpServers/startStop', 'server' => 'bitfinexQuotesHistoryServer', 'act' => 'stop' ));
                    } else {
                        echo CHtml::link('Run', array('quotesPhpServers/startStop', 'server' => 'bitfinexQuotesHistoryServer', 'act' => 'run' ));
                    }
                ?>
                </td>
            </tr>


            
            <tr>
                <td><?= Yii::t("quotesWidget", 'okex') ?></td>
                <td><?=$okexPid ? Yii::t("quotesWidget", 'Launched') : Yii::t("quotesWidget", 'Stopped') ?></td>
                <td><?=$okexPid ? $okexRes : ''?></td>
                <td>
                <?php
                    if ($okexPid) {
                        echo CHtml::link('Stop', array('quotesPhpServers/startStop', 'server' => 'okex', 'act' => 'stop' ));
                    } else {
                        echo CHtml::link('Run', array('quotesPhpServers/startStop', 'server' => 'okex', 'act' => 'run' ));
                    }
                ?>
                </td>
            </tr>


            <tr>
                <td><?= Yii::t("quotesWidget", 'binance') ?></td>
                <td><?=$binancePid ? Yii::t("quotesWidget", 'Launched') : Yii::t("quotesWidget", 'Stopped') ?></td>
                <td><?=$binancePid ? $binanceRes : ''?></td>
                <td>
                <?php
                    if ($binancePid) {
                        echo CHtml::link('Stop', array('quotesPhpServers/startStop', 'server' => 'binance', 'act' => 'stop' ));
                    } else {
                        echo CHtml::link('Run', array('quotesPhpServers/startStop', 'server' => 'binance', 'act' => 'run' ));
                    }
                ?>
                </td>
            </tr>




            <tr>
                <td><?= Yii::t("quotesWidget", 'Pusher') ?></td>
                <td><?=$pushServer ? Yii::t("quotesWidget", 'Launched') : Yii::t("quotesWidget", 'Stopped') ?></td>
                <td><?=$pushServer ? $pushServerRes : ''?><?php if ($pushServer && $pushServerSubscribersCount ) echo '<br />' . Yii::t("quotesWidget", 'Subscribers count') . ': ' . $pushServerSubscribersCount; ?></td>
                <td>
                <?php
                    if ($pushServer) {
                        echo CHtml::link('Stop', array('quotesPhpServers/startStop', 'server' => 'pushServer', 'act' => 'stop' ));
                    } else {
                        echo CHtml::link('Run', array('quotesPhpServers/startStop', 'server' => 'pushServer', 'act' => 'run' ));
                    }
                ?>
                </td>
            </tr>
        </tbody>
    </table>
    </div>
    
</div>

