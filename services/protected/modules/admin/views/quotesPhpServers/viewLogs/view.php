<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotesPhpServers/viewLogs/view');
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( "quotesWidget",'View Logs') ?></h3>
	</div>
	
	<?=CHtml::link( Yii::t( "quotesWidget",'Clear logs'), array( 'quotesPhpServers/viewLogs', 'act' => 'clear' ));?> | <?=CHtml::link( Yii::t( "quotesWidget",'Download compressed logs'), array( 'quotesPhpServers/viewLogs', 'act' => 'getZipLogs' ));?> - <?=Yii::t( "quotesWidget",'logs are backed up once per 3 day')?>
	
	<h3><?= Yii::t( "quotesWidget",'Quotes Server') ?></h3>
	
	<h4><?= Yii::t( "quotesWidget",'Application Log') ?></h4>
	<textarea rows=15 style="width:100%" readonly>
	<?php
		if( isset($servesLogs['quotesServerAppLog']) ) echo $servesLogs['quotesServerAppLog'];
	?>
	</textarea>
	
	<h4><?= Yii::t( "quotesWidget",'Error Log') ?></h4>
	<textarea rows=15 style="width:100%" readonly>
	<?php
		if( isset($servesLogs['quotesServerErrorLog']) ) echo $servesLogs['quotesServerErrorLog'];
	?>
	</textarea>
	
	
	<h3><?= Yii::t( "quotesWidget",'Quotes History Server') ?></h3>
	<textarea rows=15 style="width:100%" readonly>
	<?php
		if( isset($servesLogs['quotesHistoryServerAppLog']) ) echo $servesLogs['quotesHistoryServerAppLog'];
	?>
	</textarea>
	
	<h4><?= Yii::t( "quotesWidget",'Error Log') ?></h4>
	<textarea rows=15 style="width:100%" readonly>
	<?php
		if( isset($servesLogs['quotesHistoryServerErrorLog']) ) echo $servesLogs['quotesHistoryServerErrorLog'];
	?>
	</textarea>
	
	
	<h3><?= Yii::t( "quotesWidget",'Push Server') ?></h3>
	<textarea rows=15 style="width:100%" readonly>
	<?php
		if( isset($servesLogs['pushServerAppLog']) ) echo $servesLogs['pushServerAppLog'];
	?>
	</textarea>
	
	<h4><?= Yii::t( "quotesWidget",'Error Log') ?></h4>
	<textarea rows=15 style="width:100%" readonly>
	<?php
		if( isset($servesLogs['pushServerErrorLog']) ) echo $servesLogs['pushServerErrorLog'];
	?>
	</textarea>
	
<?/*	<div class="iGridView i01 grid-view">
	<table class="items table table-bordered">
		<thead>
			<tr>
				<th><?= Yii::t( "quotesWidget",'Server') ?></th>
				<th><?= Yii::t( "quotesWidget",'Status') ?></th>
				<th><?= Yii::t( "quotesWidget",'Action') ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?= Yii::t( "quotesWidget",'Quotes Server') ?></td>
				<td><?=$quotesServer ? Yii::t( "quotesWidget",'Launched') : Yii::t( "quotesWidget",'Stopped') ?></td>
				<td>
				<?php
					if( $quotesServer ){
						echo CHtml::link( 'Stop', array( 'quotesPhpServers/startStop', 'server' => 'quotesServer', 'act' => 'stop' ));
					}else{
						echo CHtml::link( 'Run', array( 'quotesPhpServers/startStop', 'server' => 'quotesServer', 'act' => 'run' ));
					}
				?>
				</td>
			</tr>
			<tr>
				<td><?= Yii::t( "quotesWidget",'Quotes History Server') ?></td>
				<td><?=$quotesHistoryServer ? Yii::t( "quotesWidget",'Launched') : Yii::t( "quotesWidget",'Stopped') ?></td>
				<td>
				<?php
					if( $quotesHistoryServer ){
						echo CHtml::link( 'Stop', array( 'quotesPhpServers/startStop', 'server' => 'quotesHistoryServer', 'act' => 'stop' ));
					}else{
						echo CHtml::link( 'Run', array( 'quotesPhpServers/startStop', 'server' => 'quotesHistoryServer', 'act' => 'run' ));
					}
				?>
				</td>
			</tr>
			<tr>
				<td><?= Yii::t( "quotesWidget",'Push Server') ?></td>
				<td><?=$pushServer ? Yii::t( "quotesWidget",'Launched') : Yii::t( "quotesWidget",'Stopped') ?></td>
				<td>
				<?php
					if( $pushServer ){
						echo CHtml::link( 'Stop', array( 'quotesPhpServers/startStop', 'server' => 'pushServer', 'act' => 'stop' ));
					}else{
						echo CHtml::link( 'Run', array( 'quotesPhpServers/startStop', 'server' => 'pushServer', 'act' => 'run' ));
					}
				?>
				</td>
			</tr>
		</tbody>
	</table>
	</div>*/?>
	
</div>

