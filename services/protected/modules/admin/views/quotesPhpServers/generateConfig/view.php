<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotesPhpServers/generateConfig/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?= Yii::t("quotesWidget", 'Generating config') ?></h3>
    </div>

<?php
$tabs = array();
$i = 0;
foreach ($configs as $name => $config) {
    $active = false;
    if (!$i) {
        $active = true;
        $i = 1;
    }
    $tabs[] = array(
        'label' => $name,
        'content' => '<pre>' . print_r($config, true) . '</pre>',
        'active' => $active
    );
}
$this->widget('bootstrap.widgets.TbTabs', array(
    'tabs' => $tabs,
));?>


</div>

