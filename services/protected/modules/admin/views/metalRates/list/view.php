<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'metalRates/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Metals' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage metals here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'addLabel' => false,
			
			'modelName' => 'MetalRatesModel',
			'formModelName' => 'AdminMetalRatesFormModel',
			'gridViewWidget' => 'AdminMetalRatesGridViewWidget',
			'formWidget' => 'AdminMetalRatesFormWidget',
			
			'loadRowRoute' => 'admin/metalRates/ajaxLoadListRow',
			'loadItemRoute' => 'admin/metalRates/ajaxLoadItem',
			'deleteItemRoute' => 'admin/metalRates/ajaxDeleteItem',
			'submitItemRoute' => 'admin/metalRates/ajaxAddItem',
		));
	?>
</div>