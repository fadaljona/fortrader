<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'contestMember/logs/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Logs' )?></h3>
		<p><?//=Yii::t( $NSi18n, 'This page is a list of contests members.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminContestMemberLogsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>