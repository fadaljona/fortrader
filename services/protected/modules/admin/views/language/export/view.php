<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'message/export/view' );
	
	$nameLanguages = 'languages[]';
	$nameLanguages = CHtml::resolveName( $formModel, $nameLanguages );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Export' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can export the messages in tongues and other installations of the project.' )?></p>
	</div>
	<?
		echo CHtml::errorSummary( $formModel, null, null, Array( 
			'class' => 'alert alert-block alert-error'
		));
	?>
	<div class="well pull-left iFormWidget i01">
		<?$form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm' )?>
			<?=$form->labelEx( $formModel, 'languages' )?>
			<?foreach( $languages as $language ){?>
				<label>
					<?=CHtml::checkBox( $nameLanguages, in_array( $language->id, $formModel->languages ), Array( 'id' => '', 'value' => $language->id ))?>
					<span class="lbl">
						<?=Yii::t( $NSi18n, $language->name )?>
					</span>
				</label>
			<?}?>
			<div class="iControlBlock i01">
				<?
					$this->widget( 'bootstrap.widgets.TbButton', Array( 
						'buttonType' => 'submit', 
						'type' => 'success',
						'label' => Yii::t( $NSi18n, 'Export' ),
						'htmlOptions' => Array(
							'class' => "pull-right",
						),
					));
				?>
			</div>
		<?$this->endWidget()?>
	</div>
</div>