<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'language/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Languages' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of languages.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminLanguagesEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>