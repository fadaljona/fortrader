<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'message/import/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Import' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can import the language and messages from other installations of the project.' )?></p>
	</div>
	<?
		echo CHtml::errorSummary( $formModel, null, null, Array( 
			'class' => 'alert alert-block alert-error'
		));
	?>
	<div class="well pull-left iFormWidget i01">
		<?if( !$formModel->inMode( AdminLanguagesImportFormModel::MODE_HIDDEN )) {?>
			<?$form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
				<?if( $formModel->inMode( AdminLanguagesImportFormModel::MODE_UPLOAD )) {?>
					<?=$form->labelEx( $formModel, 'file' )?>
					<?=$form->fileField( $formModel, 'file', Array( 'class' => "" ))?>
				<?}?>
				<?if( $formModel->inMode( AdminLanguagesImportFormModel::MODE_SELECT_LANGUAGES )) {?>
					<p class="muted">
						<?=Yii::t( $NSi18n, 'File uploaded' )?>
					</p>
					<?=$form->labelEx( $formModel, 'languages' )?>
					<label>
						<?=CHtml::checkBox( $formModel->resolveName( 'languages[]' ), true, Array( 'id' => '', 'value' => 0 ))?>
						<span class="lbl">
							<?=Yii::t( $NSi18n, "English" )?>
						</span>
					</label>
					<?foreach( $languages as $id=>$assoc ){?>
						<label>
							<?=CHtml::checkBox( $formModel->resolveName( 'languages[]' ), in_array( $id, $formModel->languages ), Array( 'id' => '', 'value' => $id ))?>
							<span class="lbl">
								<?=Yii::t( $NSi18n, @$assoc['name'] )?>
							</span>
						</label>
					<?}?>
				<?}?>
				<div class="iControlBlock i01">
					<?if( $formModel->inMode( AdminLanguagesImportFormModel::MODE_SELECT_LANGUAGES )) {?>
						<?
							$this->widget( 'bootstrap.widgets.TbButton', Array( 
								'buttonType' => 'submit',
								'type' => 'danger',
								'label' => Yii::t( $NSi18n, 'Cancel' ),
								'htmlOptions' => Array(
									'class' => "pull-left",
									'name' => $formModel->resolveName( 'cancelAction' ),
								),
							));
						?>
					<?}?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Import' ),
							'htmlOptions' => Array(
								'class' => "pull-right",
							),
						));
					?>
				</div>
			<?$this->endWidget()?>
		<?}else{?>
			<?=$message?>
		<?}?>
	</div>
</div>