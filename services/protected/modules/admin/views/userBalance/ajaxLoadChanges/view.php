<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'userBalance/ajaxLoadChanges/view' );
?>
<h4><?=Yii::t( $NSi18n, "History of changes in the user's balance" )?></h4>
<?
	$this->widget( 'widgets.gridViews.AdminUserBalanceChangesGridViewWidget', Array(
		'dataProvider' => $DP,
	));
?>