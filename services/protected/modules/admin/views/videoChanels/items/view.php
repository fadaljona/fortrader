<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('videoChanels/items/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Items')?></h3>

    </div>
    <?php
        $this->widget(
            'widgets.ChooseInTableWidget',
            array(
                'ns' => $ns,
                'param' => 'idChanel',
                'linkRout' => 'admin/' . Yii::app()->controller->id . '/' . Yii::app()->controller->action->id ,
                'options' => VideoChanelModel::getListForFilter(),
                'header' => Yii::t($NSi18n, 'Select')
            )
        );
       
        if (Yii::app()->request->getParam('idChanel')) {
            $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
                'ns' => 'nsActionView',
                'addLabel' => Yii::t($NSi18n, 'Add video'),
    
                'modelName' => 'VideoChanelItemModel',
                'formModelName' => 'AdminVideoChanelItemFormModel',
                'gridViewWidget' => 'AdminVideoChanelItemGridViewWidget',
                'formWidget' => 'AdminVideoChanelItemFormWidget',
    
                'loadRowRoute' => 'admin/videoChanels/ajaxLoadListRow',
                'loadItemRoute' => 'admin/videoChanels/ajaxLoadItem',
                'deleteItemRoute' => 'admin/videoChanels/ajaxDeleteItem',
                'submitItemRoute' => 'admin/videoChanels/ajaxAddItem',
            ));
        }

    ?>
</div>