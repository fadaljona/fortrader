<?php
    Yii::import('controllers.base.ViewAdminBase');
    $NSi18n = ViewAdminBase::getNSi18n('videoChanels/categories/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?=Yii::t($NSi18n, 'Chanels')?></h3>
        <p><?=Yii::t($NSi18n, 'Manage video chanels here')?></p>

    </div>
    <?php
        $this->widget('widgets.editors.AdminCommonListEditorWidget', array(
            'ns' => 'nsActionView',
            'addLabel' => Yii::t($NSi18n, 'Add chanel'),

            'modelName' => 'VideoChanelModel',
            'formModelName' => 'AdminVideoChanelModelFormModel',
            'gridViewWidget' => 'AdminVideoChanelGridViewWidget',
            'formWidget' => 'AdminVideoChanelFormWidget',

            'loadRowRoute' => 'admin/videoChanels/ajaxLoadListRow',
            'loadItemRoute' => 'admin/videoChanels/ajaxLoadItem',
            'deleteItemRoute' => 'admin/videoChanels/ajaxDeleteItem',
            'submitItemRoute' => 'admin/videoChanels/ajaxAddItem',
        ));
    ?>
</div>
