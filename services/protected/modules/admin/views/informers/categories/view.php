<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'informers/categories/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Categories' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage informer categories here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'addLabel' => Yii::t( $NSi18n, 'Add informer category' ),
			
			'modelName' => 'InformersCategoryModel',
			'formModelName' => 'AdminInformersCategoryFormModel',
			'gridViewWidget' => 'AdminInformersCategoryGridViewWidget',
			'formWidget' => 'AdminInformersCategoryFormWidget',
			
			'loadRowRoute' => 'admin/informers/ajaxLoadListRow',
			'loadItemRoute' => 'admin/informers/ajaxLoadItem',
			'deleteItemRoute' => 'admin/informers/ajaxDeleteItem',
			'submitItemRoute' => 'admin/informers/ajaxAddItem',
		));
	?>
</div>