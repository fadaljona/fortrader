<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('informers/settings/view');
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( $NSi18n, 'Informers settings') ?></h3>
	</div>
	<? 
		$this->widget('widgets.forms.AdminInformersSettingsFormWidget',Array(
			'model' => $formModel,
			'ns' => 'nsActionView',
			'ajax' => true
		)); 
	?>
</div>

