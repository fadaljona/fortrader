<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'informers/styles/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Styles' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage informer styles here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'addLabel' => Yii::t( $NSi18n, 'Add style' ),
			
			'modelName' => 'InformersStyleModel',
			'formModelName' => 'AdminInformersStyleFormModel',
			'gridViewWidget' => 'AdminInformersStyleGridViewWidget',
			'formWidget' => 'AdminInformersStyleFormWidget',
			
			'loadRowRoute' => 'admin/informers/ajaxLoadListRow',
			'loadItemRoute' => 'admin/informers/ajaxLoadItem',
			'deleteItemRoute' => 'admin/informers/ajaxDeleteItem',
			'submitItemRoute' => 'admin/informers/ajaxAddItem',
		));
	?>
</div>