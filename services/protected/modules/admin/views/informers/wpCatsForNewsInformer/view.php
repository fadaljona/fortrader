<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'informers/categories/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Wp cats for news informer' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage Wp cats for news informer here' )?></p>

	</div>
	<?
		$this->widget('widgets.forms.AdminInformersCategoryNewRawSettingsFormWidget',Array(
			'model' => $formModel,
			'formModelName' => 'AdminInformersCategoryFormModel',
			'modelName' => 'InformersCategoryModel',
			'ns' => 'nsActionView',
		)); 
	?>
</div>