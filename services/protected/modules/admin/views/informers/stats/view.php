<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'informers/stats/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Informer Stats' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.lists.AdminInformerStatsListWidget', Array(
			'ns' => 'nsActionView',
			'startDate' => $startDate,
			'endDate' => $endDate,
			'groupBy' => $groupBy,
			'informerCat' => $informerCat
		));
	?>
</div>