<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'currencyRates/pairs/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Converter pairs' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage converter pairs here' )?></p>

	</div>
    <?
        $this->widget(
            'widgets.ChooseInTableWidget',
            array(
                'ns' => $ns,
                'param' => 'idCurrency',
                'linkRout' => 'admin/' . Yii::app()->controller->id . '/' . Yii::app()->controller->action->id ,
                'options' => CurrencyRatesModel::getListForFilterForConvertPairs(),
                'header' => Yii::t($NSi18n, 'Select')
            )
        );
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'addLabel' => Yii::t( $NSi18n, '' ),
			
			'modelName' => 'CurrencyRatesConvertModel',
			'formModelName' => 'CurrencyRatesConvertFormModel',
			'gridViewWidget' => 'AdminCurrencyRatesConvertPairsGridViewWidget',
			'formWidget' => 'AdminCurrencyRatesConvertPairsFormWidget',
			
			'loadRowRoute' => 'admin/currencyRates/ajaxLoadListRow',
			'loadItemRoute' => 'admin/currencyRates/ajaxLoadItem',
			'deleteItemRoute' => '',
			'submitItemRoute' => 'admin/currencyRates/ajaxAddItem',
		));
	?>
</div>