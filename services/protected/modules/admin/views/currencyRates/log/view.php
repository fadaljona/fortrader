<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'currencyRates/log/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Converter Log' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.lists.AdminCurrencyRatesConverterlogListWidget', Array(
			'ns' => 'nsActionView',
			'startDate' => $startDate,
			'endDate' => $endDate,
		));
	?>
</div>