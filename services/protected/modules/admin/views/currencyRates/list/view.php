<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'currencyRates/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Currency Rates' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage currency rates here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCurrencyRatesEditorWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => 'CurrencyRatesModel',
			'formModelName' => 'AdminCurrencyRatesFormModel',
		));
	?>
</div>