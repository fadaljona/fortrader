<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisementCampaign/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Advertisements campaigns' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of advertisements campaigns.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminAdvertisementCampaignsEditorWidget', Array(
			'ns' => 'nsActionView',
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
		));
	?>
</div>