<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'server/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Servers' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of servers.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminServersEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>