<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'counter/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Counters' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of counters.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminCountersEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>