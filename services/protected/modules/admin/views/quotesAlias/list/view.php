<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotesAlias/list/view');
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( "quotesWidget",'Управление алисами') ?></h3>
		<p><?= Yii::t( "quotesWidget",'Добавление, редактирование и удаление алисов к инструментам') ?></p>
	</div>
	<? $this->widget( 'widgets.editors.AdminQuotesAliasEditorWidget', Array('ns' => 'nsActionView')); ?>
</div>

