<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'userActivity/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Activity' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of users activities.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminUserActivitiesEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>