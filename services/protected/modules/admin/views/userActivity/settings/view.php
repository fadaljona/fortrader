<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'settings/update/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Settings' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can change the settings of the activity.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.forms.AdminUserActivitySettingsFormWidget', Array(
			'model' => $formModel,
			'ns' => 'nsActionView',
			'ajax' => true,
		));
	?>
</div>