<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'tradePlatform/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Trade platforms' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of trade platforms.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminTradePlatformsEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>