<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'advertisementSettings/update/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Ads settings' )?></h3>
		<p><?=Yii::t( $NSi18n, 'On this page you can change the settings of ads.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.forms.AdminAdvertisementSettingsFormWidget', Array(
			'model' => $formModel,
			'ns' => 'nsActionView',
			'ajax' => true,
		));
	?>
</div>