<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'rssFeeds/groups/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Rss feeds groups' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage rss feeds groups here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'addLabel' => Yii::t( $NSi18n, 'Add group' ),
			
			'modelName' => 'RssFeedsGroupModel',
			'formModelName' => 'AdminRssFeedsGroupFormModel',
			'gridViewWidget' => 'AdminRssFeedsGroupGridViewWidget',
			'formWidget' => 'AdminRssFeedsGroupFormWidget',
			
			'loadRowRoute' => 'admin/rssFeeds/ajaxLoadListRow',
			'loadItemRoute' => 'admin/rssFeeds/ajaxLoadItem',
			'deleteItemRoute' => 'admin/rssFeeds/ajaxDeleteItem',
			'submitItemRoute' => 'admin/rssFeeds/ajaxAddItem',
		));
	?>
</div>