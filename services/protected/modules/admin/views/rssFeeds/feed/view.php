<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'rssFeeds/feed/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Feeds' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage rss feeds here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'addLabel' => Yii::t( $NSi18n, 'Add rss feed' ),
			
			'modelName' => 'RssFeedsFeedModel',
			'formModelName' => 'AdminRssFeedsFeedFormModel',
			'gridViewWidget' => 'AdminRssFeedsFeedGridViewWidget',
			'formWidget' => 'AdminRssFeedsFeedFormWidget',
			
			'loadRowRoute' => 'admin/rssFeeds/ajaxLoadListRow',
			'loadItemRoute' => 'admin/rssFeeds/ajaxLoadItem',
			'deleteItemRoute' => 'admin/rssFeeds/ajaxDeleteItem',
			'submitItemRoute' => 'admin/rssFeeds/ajaxAddItem',
	
		));
	?>
</div>