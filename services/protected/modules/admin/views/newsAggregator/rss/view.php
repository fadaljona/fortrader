<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'newsAggregator/rss/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Rss' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage rss here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => 'NewsAggregatorRssModel',
			'formModelName' => 'AdminNewsAggregatorRssFormModel',
			'addLabel' => Yii::t( $NSi18n, 'Add rss' ),
			'gridViewWidget' => 'AdminNewsAggregatorRssGridViewWidget',
			'formWidget' => 'AdminNewsAggregatorRssFormWidget',
			'loadRowRoute' => 'admin/newsAggregator/ajaxLoadListRow',
			'loadItemRoute' => 'admin/newsAggregator/ajaxLoadItem',
			'deleteItemRoute' => 'admin/newsAggregator/ajaxDeleteItem',
			'submitItemRoute' => 'admin/newsAggregator/ajaxAddItem',
			'filterEnabled' => true,
		));
	?>
</div>