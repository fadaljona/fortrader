<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('newsAggregator/settings/view');
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( $NSi18n, 'News Aggregator settings') ?></h3>
	</div>
	<? 
		$this->widget('widgets.forms.AdminNewsAggregatorSettingsFormWidget',Array(
			'model' => $formModel,
			'ns' => 'nsActionView',
			'ajax' => true
		)); 
	?>
</div>

