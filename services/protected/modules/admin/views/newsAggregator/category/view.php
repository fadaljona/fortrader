<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'newsAggregator/category/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'News categories' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage news categories here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => 'NewsAggregatorCatsModel',
			'formModelName' => 'AdminNewsAggregatorCatsFormModel',
			'addLabel' => Yii::t( $NSi18n, 'Add category' ),
			'gridViewWidget' => 'AdminCommonListGridViewWidget',
			'formWidget' => 'AdminNewsAggregatorCatsFormWidget',
			'loadRowRoute' => 'admin/newsAggregator/ajaxLoadListRow',
			'loadItemRoute' => 'admin/newsAggregator/ajaxLoadItem',
			'deleteItemRoute' => 'admin/newsAggregator/ajaxDeleteItem',
			'submitItemRoute' => 'admin/newsAggregator/ajaxAddItem',
		));
	?>
</div>