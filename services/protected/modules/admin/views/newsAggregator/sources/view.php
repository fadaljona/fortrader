<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'newsAggregator/sources/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'News Sources' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage news sources here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => 'NewsAggregatorSourceModel',
			'formModelName' => 'AdminNewsAggregatorSourceFormModel',
			'addLabel' => Yii::t( $NSi18n, 'Add source' ),
			'gridViewWidget' => 'AdminNewsAggregatorSourcesGridViewWidget',
			'formWidget' => 'AdminNewsAggregatorSourceFormWidget',
			'loadRowRoute' => 'admin/newsAggregator/ajaxLoadListRow',
			'loadItemRoute' => 'admin/newsAggregator/ajaxLoadItem',
			'deleteItemRoute' => 'admin/newsAggregator/ajaxDeleteItem',
			'submitItemRoute' => 'admin/newsAggregator/ajaxAddItem',
		));
	?>
</div>