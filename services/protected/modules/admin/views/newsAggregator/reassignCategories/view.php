<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'newsAggregator/reassignCategories/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Reassign Categories' )?></h3>
		<?php if($count){?>
			<p>
				<?=$count?> <?=Yii::t( $NSi18n, 'news categories reset' )?><br />
				<?=Yii::t( $NSi18n, 'Wait for cron' )?> /services/cronUpdateRssNewsCats.php
			</p><?php }?>

	</div>
</div>