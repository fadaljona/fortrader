<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'newsAggregator/news/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'News' )?></h3>
		<p><?=Yii::t( $NSi18n, 'Manage news here' )?></p>

	</div>
	<?
		$this->widget( 'widgets.editors.AdminCommonListEditorWidget', Array(
			'ns' => 'nsActionView',
			'modelName' => 'NewsAggregatorNewsModel',
			'formModelName' => 'AdminNewsAggregatorNewsFormModel',
			'addLabel' => false,
			'gridViewWidget' => 'AdminNewsAggregatorNewsGridViewWidget',
			'formWidget' => 'AdminNewsAggregatorNewsFormWidget',
			'loadRowRoute' => 'admin/newsAggregator/ajaxLoadListRow',
			'loadItemRoute' => 'admin/newsAggregator/ajaxLoadItem',
			'deleteItemRoute' => 'admin/newsAggregator/ajaxDeleteItem',
			'submitItemRoute' => 'admin/newsAggregator/ajaxAddItem',
			'filterEnabled' => true,
		));
	?>
</div>