<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotesSymbols/binanceSymbols/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?= Yii::t($NSi18n, 'Binance symbols') ?></h3>
    </div>

    <h3>From</h3>
    <?php
    foreach ($getFromTo['from'] as $i => $symbol) {
        if ($i) {
            echo ', ';
        }
        echo CHtml::link($symbol, array('quotesSymbols/binanceSymbols', 'from' => $symbol));
    }
    ?>

    <h3>To</h3>
    <?php
    foreach ($getFromTo['to'] as $i => $symbol) {
        if ($i) {
            echo ', ';
        }
        echo CHtml::link($symbol, array('quotesSymbols/binanceSymbols', 'to' => $symbol));
    }
    ?>

    <?php
        $this->widget('widgets.gridViews.AdminBinanceSymbolsGridViewWidget', array(
            'NSi18n' => $NSi18n,
            'showTableOnEmpty' => true,
            'dataProvider' => $DP,
            'ajax' => false,
        ));
                
    ?>
    
</div>

