<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotesSymbols/list/view');
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?= Yii::t( "quotesWidget",'Управление Котировками') ?></h3>
		<p><?= Yii::t( "quotesWidget",'Добавление, редактирование и удаление торгового инструмента') ?></p>
	</div>
	<? $this->widget( 'widgets.editors.AdminQuotesSymbolsEditorWidget', Array(
		'ns' => 'nsActionView',
		'filterURL' => $filterURL,
		'filterModel' => $filterModel,
		'categories'=>$categories
	)); ?>
</div>

