<?
Yii::import('controllers.base.ViewAdminBase');
$NSi18n = ViewAdminBase::getNSi18n('quotesSymbols/howToSetup/view');
?>
<script>
    var nsActionView = {};
</script>
<div class="nsActionView">
    <div class="well">
        <h3><?= Yii::t($NSi18n, 'How to setup sources for symbols') ?></h3>
    </div>
    
    <div class="iGridView i01 grid-view">
        тут <a href="https://fortrader.org/services/admin/quotesSymbols/list">https://fortrader.org/services/admin/quotesSymbols/list</a> <br>
        выбираем нужный Source<br> 
        <img src="/uploads/forStaticPages/7DsEmX5.png"> <br><br>

        <h2>bitz</h2>
         переделан - сейчас нет привязки, просто выбираете символ и отмечаете To usd? если нужна ковертация<br> 
        <img src="/uploads/forStaticPages/9yxb3ND.png"> <br><br>

        <h2>bitfinex</h2>
        нужно заполнить Имя для потока котировок и Имя для запроса истории - значения ставить одинаковые <br><br>

        список символов <a href="https://api.bitfinex.com/v1/symbols">https://api.bitfinex.com/v1/symbols</a> <br><br>

        переводим в верхний регистр и добавляем t перед ним <br><br>

        пример dshusd <br>
        соответственно в форму нужно вставить tDSHUSD <br><br>

        <h2>binance</h2>
        тут <a href="/services/admin/quotesSymbols/list">/services/admin/quotesSymbols/list</a> <br><br>

        выбираем источник и нужно ли конвертировать значения в usd<br>

        <img src="/uploads/forStaticPages/Screenshot_2018-06-03_13-13-23.png"> <br><br>


        тут указываем символ<br>
        <img src="/uploads/forStaticPages/Screenshot_2018-06-03_13-14-46.png"><br><br>

        список символов binance тут <a href="/services/admin/quotesSymbols/binanceSymbols">/services/admin/quotesSymbols/binanceSymbols</a><br><br>

        после всех манипуляций <a href="https://fortrader.org/services/admin/quotesPhpServers/generateConfig">https://fortrader.org/services/admin/quotesPhpServers/generateConfig</a> <br><br>

        потом рестарт получения данных для нужного источника<br>
        <a href="https://fortrader.org/services/admin/quotesPhpServers/startStop">https://fortrader.org/services/admin/quotesPhpServers/startStop</a><br>
        <img src="/uploads/forStaticPages/JdH4ocb.png">
        <br><br><br><br>



        добавил настройки автостарта <br>
        <a href="https://fortrader.org/services/admin/quotesSettings/list">https://fortrader.org/services/admin/quotesSettings/list</a> <br>
        <img src="/uploads/forStaticPages/HvmRKoq.png"> <br><br>

        проверяются по крону <br><br>

        раз в 5 минут <br>
        Auto start receiving data from mt4 <br>
        Auto Start Ws Pusher <br><br>

        раз в 2 часа <br>
        Auto Start Bitfinex <br>
        Auto Start Bitz <br>
        Auto Start Okex
    </div>
    
</div>

