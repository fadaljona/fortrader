<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'contestWiner/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Contests winers' )?></h3>
		<p><?=Yii::t( $NSi18n, 'This page is a list of contests winers.' )?></p>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminContestWinersEditorWidget', Array(
			'ns' => 'nsActionView',
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
		));
	?>
</div>