<?
	Yii::import( 'controllers.base.ViewAdminBase' );
	$NSi18n = ViewAdminBase::getNSi18n( 'mailingTypes/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="well">
		<h3><?=Yii::t( $NSi18n, 'Mailing types' )?></h3>
	</div>
	<?
		$this->widget( 'widgets.editors.AdminMailingTypesEditorWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>