<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		private $campaign;
		private function detCampaignModel() {
			$idCampaign = (int)@$_GET[ 'idCampaign' ];
			if( $idCampaign ) {
				$this->campaign = AdvertisementCampaignModel::model()->findByPk( $idCampaign );
			}
		}
		private function checkAccess() {
			if( $this->campaign ) {
				if( !$this->campaign->checkAccess() ) $this->throwI18NException( "Access denied!" );
			}
		}
		function run() {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "{$controllerCleanClass}/index" );
			
			$this->detCampaignModel();
			$this->checkAccess();
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>