<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.AdminAdvertisementFormModel' );

	final class IframeUploadImageAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminAdvertisementFormModel();
			$this->formModel->setType( 'UploadImage' );
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Ad!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "//layouts/cleanPage" );
			
			$data = new StdClass();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				
				if( $formModel->validate()) {
					$data->path = $formModel->saveImage();
					list( $data->width, $data->height, $type, $attr) = getimagesize( $data->path );
				}
				else{
					list( $data->errorField, $data->error ) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data->error = $e->getMessage();
			}
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'data' => $data,
			));
		}
	}

?>