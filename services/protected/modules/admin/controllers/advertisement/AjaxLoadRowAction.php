<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadRowAction extends ActionAdminBase {
		private function getDP( $id ) {
			$DP = new CActiveDataProvider( "AdvertisementModel", Array(
				'criteria' => Array(
					'with' => Array(
						'campaign' => Array(
							'select' => " idUser, begin, end, status ",
							'with' => Array(
								'owner' => Array(
									'select' => " balance ",
								),
							),
						),
					),
					'select' => Array(
						" `t`.* ",
						" (
							SELECT			SUM( `countShows` )
							FROM			{{advertisement_stats}}
							WHERE			`idAd` = `t`.`id`
						) AS countShows",
						" (
							SELECT			SUM( `countClicks` )
							FROM			{{advertisement_stats}}
							WHERE			`idAd` = `t`.`id`
						) AS countClicks",
						"
							IF(
								`t`.`dontShow`,
								'Dont show',
								IF(
									CURDATE() < `campaign`.`begin`,
									'Not begun',
									IF(
										CURDATE() > `campaign`.`end`,
										'Ended',
										IF(
											`owner`.`balance` <= 0,
											'End balance',
											`campaign`.`status`
										)
									)
								)
							)
							AS status
						"
					),
					'condition' => '`t`.`id` = :id',
					'params' => Array(
						':id' => $id,
					),
				),
			));
			if( !$DP->getTotalItemCount( )) $this->throwI18NException( "Can't find Ad!" );
			return $DP;
		}
		private function getWidget( $DP ) {
			$widget = $this->controller->createWidget( 'widgets.gridViews.AdminAdvertisementsGridViewWidget', Array(
				'dataProvider' => $DP,
				'typeAd' => $DP->data[0]->type,
			));
			return $widget;
		}
		private function renderRow( $widget ) {
			ob_start();
			$widget->renderTableRow( 0 );
			return ob_get_clean();
		}
		function run() {
			$data = Array();
			try{
				$id = (int)@$_GET[ 'id' ];
				$DP = $this->getDP( $id );
				
				$model = $DP->data[0];
				if( !$model->checkAccess() ) $this->throwI18NException( "Access denied!" );
				
				$widget = $this->getWidget( $DP );
				
				$data[ 'row' ] = $this->renderRow( $widget );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>