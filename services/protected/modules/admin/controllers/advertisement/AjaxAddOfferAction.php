<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminAdvertisementFormModel' );
	
	final class AjaxAddOfferAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminAdvertisementFormModel();
			$this->formModel->setType( 'Offer' );
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Ad!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		private function checkAccess() {
			$formModel = $this->getFormModel();
			$AR = $formModel->getAR();
			$campaign = AdvertisementCampaignModel::model()->findByPk( $formModel->idCampaign );
			if( !$AR->checkAccess() or !$campaign->checkAccess() ) $this->throwI18NException( "Access denied!" );
		}
		function run() {
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$this->checkAccess();
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>