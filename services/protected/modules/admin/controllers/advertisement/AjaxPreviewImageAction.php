<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxPreviewImageAction extends ActionAdminBase {
		function run( $path, $width, $height ) {
			$data = Array();
			try{
				if( !AdvertisementModel::validatePathImage( $path )) $this->throwI18NException( "Access denied!" );
				$width = CommonLib::minimax( (int)$width, 1, 2000 );
				$height = CommonLib::minimax( (int)$height, 1, 2000 );
								
				$data[ 'path' ] = AdvertisementModel::previewImage( $path, $width, $height );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>