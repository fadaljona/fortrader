<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadClicksAction extends ActionAdminBase {
		public $sortField = 'createdDT';
		public $sortDayField = 'clickDate';
		public $sortType = 'ASC';
		public $sortDayType = 'ASC';
		private function getSortField() {
			$sortField = @$_GET[ 'sortField' ];
			if( !in_array( $sortField, Array( 'createdDT', 'ip' ))) $sortField = $this->sortField;
			return $sortField;
		}
		private function getSortDayField() {
			$sortDayField = @$_GET[ 'sortDayField' ];
			if( !in_array( $sortDayField, Array( 'clickDate', 'count' ))) $sortDayField = $this->sortDayField;
			return $sortDayField;
		}
		private function getSortType() {
			$sortType = @$_GET[ 'sortType' ];
			if( !in_array( $sortType, Array( 'ASC', 'DESC' ))) $sortType = $this->sortType;
			return $sortType;
		}
		private function getSortDayType() {
			$sortDayType = @$_GET[ 'sortDayType' ];
			if( !in_array( $sortDayType, Array( 'ASC', 'DESC' ))) $sortDayType = $this->sortDayType;
			return $sortDayType;
		}
		private function getOrderDP() {
			$sortField = $this->getSortField();
			$sortType = $this->getSortType();
			return "`t`.`{$sortField}` {$sortType}";
		}
		private function getOrderDayDP() {
			$sortField = $this->getSortDayField();
			$sortType = $this->getSortDayType();
			return "{$sortField} {$sortType}";
		}
		private function getDP($marker) {
			$id = $this->getIdModel();
			$modelType = $this->getModelType();
			list( $begin, $end ) = $this->getRange();
			
			$c = new CDbCriteria();
			
			if( $marker == 'ip' ){
				$c->select = Array( 
					" `t`.* ", 
					"
						EXISTS (
							SELECT		1
							FROM		`{{advertisement_block}}`
							WHERE		`t`.`ip` = `ip`
							LIMIT		1
						) AS blockIP
					",
					"
						EXISTS (
							SELECT		1
							FROM		`{{advertisement_block}}`
							WHERE		`t`.`agent` LIKE `agent`
							LIMIT		1
						) AS blockAgent
					",
				);
			}elseif( $marker == 'day' ){
				$c->select = Array( " count(`t`.`id`) AS count", " date(`t`.`createdDT`) AS clickDate " );
				$c->group = " clickDate ";
			}
			
			
			$c->addCondition( " DATE(`t`.`createdDT`) BETWEEN :begin AND :end " );
			$c->params[ ':begin' ] = $begin;
			$c->params[ ':end' ] = $end;
			
			if( $marker == 'ip' )$c->order = $this->getOrderDP();
			if( $marker == 'day' )$c->order = $this->getOrderDayDP();
			
			switch( $modelType ) {
				case 'Ad':{
					$modelName = 'AdvertisementClickModel';
					$c->addCondition( " `t`.`idAd` = :idAd " );
					$c->params[ ':idAd' ] = $id;
					break;
				}
				case 'Campaign':{
					$modelName = 'AdvertisementClickModel';
					$c->with['advertisement'] = Array(
						'select' => false,
					);
					$c->addCondition( " `advertisement`.`idCampaign` = :idCampaign " );
					$c->params[ ':idCampaign' ] = $id;
					break;
				}
				case 'Zone':{
					$modelName = 'AdvertisementZoneClickModel';
					$c->addCondition( " `t`.`idZone` = :idZone " );
					$c->params[ ':idZone' ] = $id;
					break;
				}
			}
			
			$DP = new CActiveDataProvider( $modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			
			if( $marker == 'day' ) $DP->pagination->pageVar = 'pageDay';
			$DP->pagination->pageSize = 20;
			//$DP->pagination->pageSize = 2;
			return $DP;
		}
		private function getIdModel() {
			$id = (int)@$_POST[ 'id' ];
			return $id;
		}
		private function getModelType() {
			$model = @$_POST[ 'model' ];
			if( !in_array( $model, Array( 'Ad', 'Campaign', 'Zone' ))) $model = 'Ad';
			return $model;
		}
		private function getRange() {
			$begin = @$_POST[ 'begin' ];
			$end = @$_POST[ 'end' ];
			if( !CommonLib::isDate( $begin )) $begin = date( "Y-m-d" );
			if( !CommonLib::isDate( $end )) $end = date( "Y-m-d" );
			return $end >= $begin ? Array( $begin, $end ) : Array( $end, $begin );
		}
		private function getModel() {
			static $model;
			if( !$model ) {
				$id = $this->getIdModel();
				$modelType = $this->getModelType();
				switch( $modelType ) {
					case 'Ad':{
						$model = AdvertisementModel::model()->findByPk( $id );
						break;
					}
					case 'Campaign':{
						$model = AdvertisementCampaignModel::model()->findByPk( $id );
						break;
					}
					case 'Zone':{
						$model = AdvertisementZoneModel::model()->findByPk( $id );
						break;
					}
				}
				if( !$model ) $this->throwI18NException( "Can't find model!" );
			}
			return $model;
		}
		private function checkAccess() {
			$modelType = $this->getModelType();
			switch( $modelType ) {
				case 'Ad':
				case 'Campaign':{
					$model = $this->getModel();
					return $model->checkAccess();
				}
				case 'Zone':{
					return Yii::App()->user->checkAccess( 'advertisementZoneControl' );
				}
			}
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			if( !$this->checkAccess()) $this->throwI18NException( "Access denied!" );
			
			$this->controller->renderPartial( "{$actionCleanClass}/view", Array(
				'DP' => $this->getDP('ip'),
				'dayDP' => $this->getDP('day'),
				'sortField' => $this->getSortField(),
				'sortDayField' => $this->getSortDayField(),
				'sortType' => $this->getSortType(),
				'sortDayType' => $this->getSortDayType(),
			));
		}
	}

?>