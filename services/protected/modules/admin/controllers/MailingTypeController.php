<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class MailingTypeController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Mailing types';
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'mailingControl' )),
				Array( 'deny' ),
			);
		}
	}

?>