<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class AdvertisementCampaignController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Advertisements campaigns";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'advertisementControl', 'ownAdvertisementControl' )),
				Array( 'deny' ),
			);
		}
	}

?>