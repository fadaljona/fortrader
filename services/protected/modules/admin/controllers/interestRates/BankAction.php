<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class BankAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Banks" );
			$this->setLayout( "interestRates/bank" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>