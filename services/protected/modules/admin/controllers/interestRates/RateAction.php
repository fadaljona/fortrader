<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class RateAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Rates" );
			$this->setLayout( "interestRates/rate" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>