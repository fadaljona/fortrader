<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadItemAction extends ActionAdminBase {
		private $formModel;
		private $formModelName;
		private function detFormModel() {
			$formModelName = $this->formModelName;
			$this->formModel = new $formModelName;
			$id = (int)@$_GET['id'];
			if( !$id ) $this->throwI18NException( "Set id!" );
			$load = $this->formModel->load( $id );
			if( !$load ) $this->throwI18NException( "Can't find model!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			if( !@$_GET['formModelName'] ) return false;
			$this->formModelName = @$_GET['formModelName'];
			Yii::import( 'models.forms.' . $this->formModelName );
			
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				$data[ 'model' ] = $formModel->saveToObject();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>