<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ValuesAction extends ActionAdminBase {
		
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Values" );
			$this->setLayout( "interestRates/values" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'ratesList' => InterestRatesRateModel::getRatesList(),
			));
		}
	}

?>