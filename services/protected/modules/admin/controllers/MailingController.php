<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class MailingController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Mailings';
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'mailingControl' )),
				Array( 'deny' ),
			);
		}
	}

?>