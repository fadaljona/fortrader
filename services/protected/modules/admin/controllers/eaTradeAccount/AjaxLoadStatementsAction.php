<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminIndividualExpenseFormModel' );
	
	final class AjaxLoadStatementsAction extends ActionAdminBase {
		private function getWidget() {
			$widget = $this->controller->createWidget( "widgets.forms.AdminEATradeAccountFormWidget" );
			return $widget;
		}
		function run( $idVersion ) {
			$data = Array();
			try{
				$widget = $this->getWidget();
				$data[ 'statements' ] = $widget->getStatements( $idVersion );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>