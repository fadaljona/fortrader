<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class TradeAccountController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Trade accounts";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'tradeAccountControl' )),
				Array( 'deny' ),
			);
		}
	}

?>