<?php
Yii::import('controllers.base.ActionAdminBase');

final class ItemsAction extends ActionAdminBase
{
    public function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Chanel Items");
        $this->setLayout("videoChanels/items");

        $this->controller->render("{$actionCleanClass}/view", array());
    }
}
