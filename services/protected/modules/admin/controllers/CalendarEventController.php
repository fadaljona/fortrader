<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class CalendarEventController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Calendar";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'calendarEventControl' )),
				Array( 'deny' ),
			);
		}
	}

?>