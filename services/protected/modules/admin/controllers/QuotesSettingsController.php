<?php

Yii::import('controllers.base.ControllerAdminBase');

class QuotesSettingsController extends ControllerAdminBase{
	function detLayoutTitle(){
		return Yii::t( "quotesWidget","Настройки модуля котировок");
	}

	function accessRules(){
		return Array(
			Array('allow','roles' => Array('quotesControl')),
			Array( 'allow', 'actions' => array('list', 'ajaxSettingsUpdate'), 'roles' => Array( 'informersTranslateControl' )),
			Array('deny'),
		);
	}

	public function actionIndex(){
		$this->redirect(Array('list'));
	}

	public function loadModel($id){
		$model = QuotesSettingsModel::model()->findByPk($id);
		if($model === null) throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}
