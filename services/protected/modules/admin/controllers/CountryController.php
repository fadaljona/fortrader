<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class CountryController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Countries";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'countryControl' )),
				Array( 'deny' ),
			);
		}
	}

?>