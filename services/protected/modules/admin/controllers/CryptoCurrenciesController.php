<?php

Yii::import('controllers.base.ControllerAdminBase');

final class CryptoCurrenciesController extends ControllerAdminBase
{
    public function detLayoutTitle()
    {
        return "Crypto currency";
    }

    public function accessRules()
    {
        return array(
            array( 'allow', 'roles' => array( 'cryptoCurrenciesControllerControl' )),
            array( 'deny' ),
        );
    }
}
