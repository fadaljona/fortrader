<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class PostsStatsController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Posts Stats";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'postsStatsControl' )),
				Array( 'allow', 'actions' => array('ajaxSetSmMarker', 'ajaxAddTrafficStats', 'ajaxSearchLog') ),
				Array( 'deny' ),
			);
		}
	}

?>