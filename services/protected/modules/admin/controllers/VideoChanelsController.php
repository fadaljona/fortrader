<?php

Yii::import('controllers.base.ControllerAdminBase');

final class VideoChanelsController extends ControllerAdminBase
{
    public function detLayoutTitle()
    {
        return "Video chanels";
    }

    public function accessRules()
    {
        return array(
            array( 'allow', 'roles' => array( 'videoChanelsControl' )),
            array( 'deny' ),
        );
    }
}
