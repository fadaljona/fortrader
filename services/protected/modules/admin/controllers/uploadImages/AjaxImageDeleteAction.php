<?php
Yii::import( 'controllers.base.ActionAdminBase' );

final class AjaxImageDeleteAction extends ActionAdminBase {

	function run() {

		$nameImage = @$_POST['key'];
		if( !$nameImage ) return false;
		
		echo CommonLib::commonImageDelete($nameImage);
	}
}
