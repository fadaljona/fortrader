<?php
Yii::import( 'controllers.base.ActionAdminBase' );

final class AjaxImageUploadAction extends ActionAdminBase {

	function run() {

		$file = CUploadedFile::getInstanceByName('file_data');
		if( !$file ) return false;
		$nameToSave = @$_POST['nameToSave'];
		if( !$nameToSave ) return false;
		
		echo CommonLib::commonImageUpload($file, $nameToSave);
	}
}
