<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'components.MailingComponent' );
	
	final class AjaxRunAction extends ActionAdminBase {
		private function getMailing( $id ) {
			$mailing = MailingModel::model()->findByPk( $id );
			if( !$mailing ) throw new Exception( "Can't find Mailing!" );
			return $mailing;
		}
		function run( $id, $stage, $offset = 0, $restart = 0 ) {
			//if( mt_rand( 1, 10 ) == 1 ) throw new Exception( "Test error!" );
			$data = compact( 'id', 'stage', 'offset', 'restart' );
			try{
				$mailing = $this->getMailing( $id );
				$component = new MailingComponent();
				$component->run( $mailing, $data );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			unset( $data[ 'restart' ]);
			echo json_encode( $data );
		}
	}

?>