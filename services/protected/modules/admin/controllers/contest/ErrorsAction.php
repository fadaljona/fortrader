<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ErrorsAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Monitoring errors" );
			$this->setLayout( "contest/errors" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>