<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxLoadForeignContestDeleteAction extends ActionAdminBase {
		private function getModel( $id ) {
			$modelName = 'ContestForeignModel';
			$model = $modelName::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Contest!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>