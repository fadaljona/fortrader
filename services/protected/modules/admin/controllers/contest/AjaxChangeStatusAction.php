<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxChangeStatusAction extends ActionAdminBase {
		private function getModel( $id ) {
			$model = ContestModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Contest!" );
			return $model;
		}
		function run( $id, $status ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				if( !in_array( $status, Array( 'registration stoped', 'registration', 'started', 'completed' ))) {
					throw new Exception( Yii::t( 'yii','{attribute} is not in the list.', Array( '{attribute}' => 'Status' )));
				}
				$model->status = $status;
				$r = $model->save();
				if( !$r ) $this->throwI18NException( "Can't save AR!" );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>