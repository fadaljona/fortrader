<?
	Yii::import( 'controllers.base.ActionFrontBase' );

	final class ExportMembersAction extends ActionFrontBase {
		private function getDP( $idContest ) {
			return new CActiveDataProvider( "ContestMemberModel", Array(
				'criteria' => Array(
					'with' => Array(
						'user' => Array(
							'select' => "ID,user_login",
						),
					),
					'condition' => " `t`.`idContest` = :idContest ",
					'order' => " `t`.`idUser` ",
					'params' => Array(
						':idContest' => $idContest,
					),
				),
			));
		}
		private function renderIterator( $idContest, $iterator ) {
			$fileName = "members_{$idContest}.csv";
			$delimiter = SettingsModel::getModel()->common_delimiter;
			header( "Content-type: text/comma-separated-values" );
			header( "Content-Disposition: attachment; filename=\"{$fileName}\"" );
			
			$fileds = explode( ",", "id,idContest,idUser,loginUser,accountNumber,investorPassword,idServer,status,createdDT" );
			
			echo implode( $delimiter, $fileds );
			echo "\r\n";
			
			foreach( $iterator as $member ) {
				$row = Array();
				foreach( $fileds as $filed ) {
					switch( $filed ) {
						case 'loginUser':{
							$row[] = $member->user->user_login;
							break;
						}
						default: {
							$row[] = $member->$filed;		
							break;
						}
					}
					
				}
				echo implode( $delimiter, $row );
				echo "\r\n";
			}
		}
		function run( $id ) {
			CommonLib::disableAppProfileLog();
			$DP = $this->getDP( $id );
			$iterator = new CDataProviderIterator( $DP, 1000 );
			$this->renderIterator( $id, $iterator );
		}
	}

?>