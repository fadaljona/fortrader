<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class EaController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "EA";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'eaControl' )),
				Array( 'deny' ),
			);
		}
	}

?>