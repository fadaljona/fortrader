<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class ServerController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Servers";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'serverControl' )),
				Array( 'deny' ),
			);
		}
	}

?>