<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class BrokerReviewController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Brokers reviews";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'userMessageControl' )),
				Array( 'deny' ),
			);
		}
	}

?>