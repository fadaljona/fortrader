<?
Yii::import('controllers.base.ActionAdminBase');
Yii::import( 'models.forms.AdminQuotesAliasFormModel' );

final class ListAction extends ActionAdminBase{
	private $formModel;
	private function detFormModel() {
		$this->formModel = new AdminQuotesAliasFormModel();
		$load = $this->formModel->load();
		if( !$load ) $this->throwI18NException( Yii::t( "quotesWidget","Алиас не найден!") );
	}

	function run(){
		$this->setLayoutTitle(Yii::t( "quotesWidget","Алиасы котировок"));
		$this->detFormModel();
		$this->controller->render("list/view",Array('formModel' => $this->formModel));
	}
}
