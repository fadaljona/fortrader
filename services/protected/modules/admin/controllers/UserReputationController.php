<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UserReputationController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Reputation';
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'userReputationControl' )),
				Array( 'deny' ),
			);
		}
	}

?>