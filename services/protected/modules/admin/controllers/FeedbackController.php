<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class FeedbackController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Feedbacks';
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'feedbackControl' )),
				Array( 'deny' ),
			);
		}
	}

?>