<?php

Yii::import('controllers.base.ControllerAdminBase');

class QuotesPhpServersController extends ControllerAdminBase{
	function detLayoutTitle(){
		return Yii::t( "quotesWidget","Quotes - PHP Servers");
	}

	function accessRules(){
		return Array(Array('allow','roles' => Array('quotesControl')),Array('deny'),);
	}

	public function actionIndex(){
		$this->redirect(Array('list'));
	}

}
