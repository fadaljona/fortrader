<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class FeedbackTypeController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Feedback types';
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'feedbackControl' )),
				Array( 'deny' ),
			);
		}
	}

?>