<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxDeleteAgentAction extends ActionAdminBase {
		private function getModel( $agent ) {
			$model = AdvertisementBlockModel::model()->findByAttributes( Array( 'agent' => $agent ));
			//if( !$model ) $this->throwI18NException( "Can't find Block!" );
			return $model;
		}
		function run( $agent ) {
			$data = Array();
			try{
				$model = $this->getModel( $agent );
				if( $model ) $model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>