<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxDeleteIPAction extends ActionAdminBase {
		private function getModel( $ip ) {
			$ip = ip2long( $ip );
			$ip = sprintf( "%u", $ip );
			$model = AdvertisementBlockModel::model()->findByAttributes( Array( 'ip' => $ip ));
			//if( !$model ) $this->throwI18NException( "Can't find Block!" );
			return $model;
		}
		function run( $ip ) {
			$data = Array();
			try{
				$model = $this->getModel( $ip );
				if( $model ) $model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>