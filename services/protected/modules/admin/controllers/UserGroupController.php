<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UserGroupController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Users groups";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'userGroupControl' )),
				Array( 'deny' ),
			);
		}
	}

?>