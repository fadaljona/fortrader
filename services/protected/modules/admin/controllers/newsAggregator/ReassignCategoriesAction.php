<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ReassignCategoriesAction extends ActionAdminBase {

		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Reassign Categories" );
			$this->setLayout( "newsAggregator/reassignCategories" );
			
			$count = NewsAggregatorNewsModel::resetCats();
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'count' => $count,
			));
		}
	}

?>