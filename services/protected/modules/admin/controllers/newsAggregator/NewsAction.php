<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class NewsAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "News" );
			$this->setLayout( "newsAggregator/news" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>