<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class RssAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Rss" );
			$this->setLayout( "newsAggregator/rss" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>