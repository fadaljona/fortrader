<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class CategoryAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Categories" );
			$this->setLayout( "newsAggregator/category" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>