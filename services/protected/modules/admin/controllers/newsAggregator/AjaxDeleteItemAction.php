<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxDeleteItemAction extends ActionAdminBase {
		private $modelName;
		private function getModel( $id ) {
			$modelName = $this->modelName;
			$model = $modelName::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find model!" );
			return $model;
		}
		function run( $id ) {
			if( !@$_GET['modelName'] ) return false;
			$this->modelName = @$_GET['modelName'];
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>