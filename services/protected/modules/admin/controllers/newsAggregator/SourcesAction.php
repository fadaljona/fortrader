<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class SourcesAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Sources" );
			$this->setLayout( "newsAggregator/sources" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>