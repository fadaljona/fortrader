<?php

Yii::import( 'controllers.base.ActionAdminBase' );

class WebmasterListAction extends ActionAdminBase {
		function run() {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "{$controllerCleanClass}/index" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
}

?>
