<?php
Yii::import( 'controllers.base.ActionAdminBase' );
class AjaxBlockWebmasterAction extends ActionAdminBase {
		private function getModel( $id ) {
			$model = JournalWebmastersModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Journal!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$model->is_blocked = 1;
                if(!$model->save(false, array('is_blocked'))) {
                    throw new Exception("Can't save model.");
                }
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>
