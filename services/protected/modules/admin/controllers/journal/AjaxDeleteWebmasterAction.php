<?php

Yii::import( 'controllers.base.ActionAdminBase' );
class AjaxDeleteWebmasterAction extends ActionAdminBase {
		private function getModel( $id ) {
			$model = JournalWebmastersModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Journal!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>
