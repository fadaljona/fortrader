<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class NewsAggregatorController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "News Aggregator";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'newsAggregatorControl' )),
				Array( 'deny' ),
			);
		}
	}

?>