<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class EaMonitoringServerController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Monitoring servers";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'eaControl' )),
				Array( 'deny' ),
			);
		}
	}

?>