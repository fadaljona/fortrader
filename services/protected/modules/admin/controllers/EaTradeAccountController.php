<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class EaTradeAccountController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Trade accounts";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'eaControl' )),
				Array( 'deny' ),
			);
		}
	}

?>