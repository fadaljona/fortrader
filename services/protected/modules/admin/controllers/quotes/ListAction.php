<?
Yii::import('controllers.base.ActionAdminBase');
Yii::import('components.widgets.quotes.Parser');

final class ListAction extends ActionAdminBase{
	function run(){
		$this->setLayoutTitle(Yii::t( "quotesWidget","Таблица котировок"));
		
		$quotesSettings = QuotesSettingsModel::getInstance();

		$files=$quotesSettings->files;
		$this->controller->render("list/view",Array('data' => Parser::parse($files,'array'),));
	}
}
