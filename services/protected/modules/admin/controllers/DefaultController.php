<?
	
	final class DefaultController extends AdminControllerExBase {
		function detLayoutTitle() {
			return "Administration";
		}
		function detView() {
			$class = $this->getClass();
			return "{$class}/{$this->action->id}";
		}
		function actionIndex() {
			$this->extendLayoutTitle( '', '{controllerTitle}' );
			$this->render();
		}
		function accessRules() {
			return Array(
				Array( 'deny', 'users' => Array( '?' )),
				Array( 'allow', 'actions' => Array( 'index' )),
				Array( 'deny' ),
			);
		}
	}

?>