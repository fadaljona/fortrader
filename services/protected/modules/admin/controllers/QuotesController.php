<?
Yii::import('controllers.base.ControllerAdminBase');

final class QuotesController extends ControllerAdminBase{
	function detLayoutTitle(){
		return Yii::t( "quotesWidget","Котировки");
	}

	function actionIndex(){
		$this->redirect(Array('list'));
	}

	function accessRules(){
		return Array(
			Array(
				'allow',
				'roles' => Array('quotesControl')
			),
			Array(
				'deny',
				'users'=>array('*')
			)
		);
	}
}