<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UserBalanceController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Finance";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'actions' => Array( 'list', 'ajaxLoadChanges' ), 'roles' => Array( 'ownBalanceView' )),
				Array( 'allow', 'roles' => Array( 'userBalanceControl' )),
				Array( 'deny' ),
			);
		}
	}

?>