<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class MonitoringController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Account monitoring";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'monitoringControl' )),
				Array( 'deny' ),
			);
		}
	}

?>