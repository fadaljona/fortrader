<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminBrokerSettingsFormModel' );

	final class UpdateAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminBrokerSettingsFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Settings!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();

			$this->detFormModel();
			
			$this->setLayoutTitle( "Editor" );
			$this->setLayout( "broker/index" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>