<?php
Yii::import('controllers.base.ActionAdminBase');

final class ModelsForReplaceAction extends ActionAdminBase
{
    public function run()
    {
        $actionCleanClass = $this->getCleanClassName();
        $this->setLayoutTitle("Models for replace");
        $this->setLayout("helper/flushCache");

        $this->controller->render("{$actionCleanClass}/view", array(
            //'result' => Yii::app()->cache->flush(),
        ));
    }
}
