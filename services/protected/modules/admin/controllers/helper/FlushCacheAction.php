<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class FlushCacheAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Flush Cache" );
			$this->setLayout( "helper/flushCache" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'result' => Yii::app()->cache->flush(),
			));
		}
	}

?>