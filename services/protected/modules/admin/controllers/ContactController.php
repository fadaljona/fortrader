<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class ContactController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Contacts';
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'feedbackControl' )),
				Array( 'deny' ),
			);
		}
	}

?>