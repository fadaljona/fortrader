<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class HelperController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Helper";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'helperControl' )),
				Array( 'deny' ),
			);
		}
	}

?>