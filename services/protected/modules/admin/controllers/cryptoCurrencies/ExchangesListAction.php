<?php

Yii::import('controllers.base.ActionAdminBase');

final class ExchangesListAction extends ActionAdminBase
{
    public function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Exchanges list");
        $this->setLayout("cryptoCurrencies/exchangesList");

        $this->controller->render("{$actionCleanClass}/view", array());
    }
}
