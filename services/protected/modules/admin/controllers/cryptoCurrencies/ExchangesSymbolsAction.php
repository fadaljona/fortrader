<?php
Yii::import('controllers.base.ActionAdminBase');

final class ExchangesSymbolsAction extends ActionAdminBase
{
    public function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Exchanges Symbols");
        $this->setLayout("cryptoCurrencies/exchangesSymbols");

        $this->controller->render("{$actionCleanClass}/view", array());
    }
}
