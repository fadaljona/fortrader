<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class LanguageController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Languages';
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'languageControl' )),
				Array( 'deny' ),
			);
		}
	}

?>