<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		
		const KEYStateFilterContest = 'admin.contestWiners.list.filter.contest';
	
		private $filterModel;
		
		private function getFilterURL() {
			return $this->controller->createUrl( 'list' );
		}
		
		private function detFilterModel() {
			$this->filterModel = new ContestWinerModel();
			$post = &$_POST[ get_class( $this->filterModel )];
			if( !empty( $post )) {
				Yii::App()->user->setState( self::KEYStateFilterContest, $post[ 'idContest' ]);
			}

			$this->filterModel->idContest = Yii::App()->user->getState( self::KEYStateFilterContest );

		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFilterModel();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "contest/index" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>