<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class MetalRatesController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Metals";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'informersControl' )),
				Array( 'deny' ),
			);
		}
	}

?>