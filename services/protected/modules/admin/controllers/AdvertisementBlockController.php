<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class AdvertisementBlockController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Blocks";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'advertisementBlockControl' )),
				Array( 'deny' ),
			);
		}
	}

?>