<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class RssFeedsController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Rss Feeds";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'rssFeedsControl' )),
				Array( 'deny' ),
			);
		}
	}

?>