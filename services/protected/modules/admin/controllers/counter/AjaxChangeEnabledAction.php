<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxChangeEnabledAction extends ActionAdminBase {
		private function getModel( $id ) {
			$model = CounterModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Counter!" );
			return $model;
		}
		function run( $id, $enabled ) {
			$data = Array();
			try{
				if( !in_array( $enabled, Array( '0', '1' ))) $this->throwI18NException( "Wrong enabled!" );
				$model = $this->getModel( $id );
				$model->enabled = $enabled;
				$model->update( Array( 'enabled' ));
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>