<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		const KEYStateFilterDate = 'admin.calendarEvent.list.filter.date';
		const KEYStateFilterIndicatorID = 'admin.calendarEvent.list.filter.indicatorID';
		const KEYStateFilterIndicator = 'admin.calendarEvent.list.filter.indicator';
		private $filterModel;
		private function getFilterURL() {
			return $this->controller->createUrl( 'list', Array( 'idCountry' => (int)@$_GET[ 'idCountry' ]));
		}
		private function detFilterModel() {
			$this->filterModel = new CalendarEvent2Model();
			$post = &$_POST[ get_class( $this->filterModel )];
			if( !empty( $post )) {
				Yii::App()->user->setState( self::KEYStateFilterDate, $post[ 'dateForSearch' ]);
				
				$indicatorIDForSearch = $post[ 'indicatorIDForSearch' ];
				$indicatorIDForSearch = preg_replace( "#[^0-9]#", "", $indicatorIDForSearch );
				Yii::App()->user->setState( self::KEYStateFilterIndicatorID, $indicatorIDForSearch );
				
				Yii::App()->user->setState( self::KEYStateFilterIndicator, $post[ 'indicatorForSearch' ]);
			}
			$this->filterModel->dateForSearch = Yii::App()->user->getState( self::KEYStateFilterDate );
			$this->filterModel->indicatorIDForSearch = Yii::App()->user->getState( self::KEYStateFilterIndicatorID );
			$this->filterModel->indicatorForSearch = Yii::App()->user->getState( self::KEYStateFilterIndicator );
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFilterModel();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "default/index" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>
