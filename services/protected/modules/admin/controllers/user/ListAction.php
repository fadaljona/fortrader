<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		const KEYStateFilterLogin = 'admin.user.list.filter.user_login';
		const KEYStateFilterGroup = 'admin.user.list.filter.group';
		private $filterModel;
		private function getFilterURL() {
			return $this->controller->createUrl( 'list' );
		}
		private function detFilterModel() {
			$this->filterModel = new UserModel();
			$post = &$_POST[ get_class( $this->filterModel )];
			if( !empty( $post )) {
				Yii::App()->user->setState( self::KEYStateFilterLogin, $post[ 'user_login' ]);
				Yii::App()->user->setState( self::KEYStateFilterGroup, $post[ 'group' ]);
			}
			$this->filterModel->user_login = Yii::App()->user->getState( self::KEYStateFilterLogin );
			$this->filterModel->group = Yii::App()->user->getState( self::KEYStateFilterGroup );
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFilterModel();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "{$controllerCleanClass}/index" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>