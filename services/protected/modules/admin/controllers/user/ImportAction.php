<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminUsersImportFormModel' );
	Yii::import( 'components.UsersSyncComponent' );
	
	final class ImportAction extends ActionAdminBase {
		const PATHUploadDir = 'uploads/users';
		const TTLTemp = 3600;
		const KEYStateUploadFile = 'admin.user.uploaded';
		private $formModel;
		private $message;
		private function clearUploadDir() {
			$now = time();
			$files = glob( self::PATHUploadDir."/*" );
			if( $files ) {
				foreach( $files as $file ) {
					if( $now - filemtime( $file ) > self::TTLTemp ) {
						@unlink( $file );
					}
				}
			}
		}
		private function getUploadFileFreeName() {
			do{
				$name = substr( md5( rand() ), 0, 8 );
				$isset = is_file( self::PATHUploadDir."/{$name}" );
			}
			while( $isset );
			return $name;
		}
		private function detFormModel() {
			$this->formModel = new AdminUsersImportFormModel();
		}
		private function getFormModel() {
			return $this->formModel;
		}
		private function getMessage() {
			return $this->message;
		}
		private function setMessage( $message ) {
			$this->message = $message;
		}
		private function processForm() {
			$form = $this->getFormModel();
			$mode = AdminUsersImportFormModel::MODE_UPLOAD;
			
			$fileName = Yii::App()->user->getState( self::KEYStateUploadFile );
			if( $fileName ) {
				$file = self::PATHUploadDir."/{$fileName}";
				if( is_file( $file )) {
					$data = @unserialize( file_get_contents( $file ));
					$mode = AdminUsersImportFormModel::MODE_SELECT_EXPORT;
				}
			}
			
			$form->setMode( $mode );
			
			$form->load();
			
			if( $form->isPostRequest()) {
				if( $form->validate()) {
					switch( $form->getMode() ) {
						case AdminUsersImportFormModel::MODE_UPLOAD: {
							$this->clearUploadDir();
							$fileName = $this->getUploadFileFreeName();
							$form->file->saveAs( self::PATHUploadDir."/{$fileName}" );
							Yii::App()->user->setState( self::KEYStateUploadFile, $fileName );
							$this->redirect( Array( 'import' ));
							break;
						}
						case AdminUsersImportFormModel::MODE_SELECT_EXPORT: {
							if( $form->cancelAction ) {
								if( is_file( $file )) unlink( $file );
								$this->redirect( Array( 'import' ));
							}
							
							$sync = new UsersSyncComponent();
							$result = $sync->import( $data, $form->export );
							if( is_file( $file )) unlink( $file );
							$NSi18n = $this->getNSi18n();
							$form->setMode( AdminUsersImportFormModel::MODE_HIDDEN );
							if( $result == UsersSyncComponent::STATUS_OK ) {
								$this->setMessage( Yii::t( $NSi18n, "Import success" ));
							}
							else{
								$this->setMessage( Yii::t( $NSi18n, "Error" ));
							}

							break;
						}
					}
				}
			}
		}
		function run() {
			$this->checkAccessArray( Array( 'userControl', 'userGroupControl' ), 'AND' );
			
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFormModel();
			$this->processForm();
			
			$this->setLayoutTitle( "Import" );
			$this->setLayout( "{$controllerCleanClass}/index" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->getFormModel(),
				'message' => $this->getMessage(),
			));
		}
	}

?>