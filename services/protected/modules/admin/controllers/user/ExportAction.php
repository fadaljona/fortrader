<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminUsersExportFormModel' );
	Yii::import( 'components.UsersSyncComponent' );

	final class ExportAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminUsersExportFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			return $this->formModel;
		}
		private function processForm() {
			$form = $this->getFormModel();
			if( $form->isPostRequest()) {
				if( $form->validate()) {
					$sync = new UsersSyncComponent();
					$sync->export( $form->export, $data );
					//var_dump( $data ); exit;
					Yii::App()->request->sendFile( 'users.dat', serialize( $data ));
				}
			}
		}
		function run() {
			$this->checkAccessArray( Array( 'userControl', 'userGroupControl' ), 'AND' );
			
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFormModel();
			$this->processForm();
			
			$this->setLayoutTitle( "Export" );
			$this->setLayout( "{$controllerCleanClass}/index" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>