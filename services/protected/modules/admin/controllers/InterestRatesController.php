<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class InterestRatesController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Interest Rates";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'interestRatesControl' )),
				Array( 'allow', 'actions' => array('values', 'ajaxLoadItem', 'ajaxAddItem', 'ajaxLoadListRow'), 'roles' => Array( 'interestRatesValuesControl' )),
				Array( 'deny' ),
			);
		}
	}

?>