<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class BrokerIOController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Brokers input/output";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'brokerControl' )),
				Array( 'deny' ),
			);
		}
	}

?>