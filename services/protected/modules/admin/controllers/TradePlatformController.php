<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class TradePlatformController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Trade platforms";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'tradePlatformControl' )),
				Array( 'deny' ),
			);
		}
	}

?>