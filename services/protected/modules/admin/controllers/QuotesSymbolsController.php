<?php

Yii::import('controllers.base.ControllerAdminBase');

class QuotesSymbolsController extends ControllerAdminBase{
	function detLayoutTitle(){
		return Yii::t( "quotesWidget","Котировки");
	}

	public function actionIndex(){
		$this->redirect(Array('list'));
	}

	function accessRules(){
		return Array(
			Array('allow','roles' => Array('quotesControl')),
			Array( 'allow', 'actions' => array('list', 'ajaxLoad', 'ajaxUpdate', 'ajaxLoadRow'), 'roles' => Array( 'informersTranslateControl' )),
			Array('deny'),
		);
	}

	public function loadModel($id){
		$model = QuotesCategoryModel::model()->findByPk($id);
		if($model === null) throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
