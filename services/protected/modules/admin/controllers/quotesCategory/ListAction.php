<?
Yii::import('controllers.base.ActionAdminBase');
Yii::import( 'models.forms.AdminQuotesCategoryFormModel' );

final class ListAction extends ActionAdminBase{
	private $formModel;
	private function detFormModel() {
		$this->formModel = new AdminQuotesCategoryFormModel();
		$load = $this->formModel->load();
		if( !$load ) $this->throwI18NException( Yii::t( "quotesWidget","Невозможно загрузить категории!") );
	}
	private function getCategories() {
		$categories=array();
		$categories[0]='';
		$aCatsFromModel=QuotesCategoryModel::getAll();
		
		if($aCatsFromModel)
		foreach($aCatsFromModel as $oCategory){
			if( !$oCategory->currentLanguageI18N || !$oCategory->currentLanguageI18N->name ) continue;
			$categories[$oCategory->id]=$oCategory->currentLanguageI18N->name;
		}
		
		return $categories;
	}
	function run(){
		$this->setLayoutTitle(Yii::t( "quotesWidget","Категории котировок"));
		$this->detFormModel();
		$this->controller->render("list/view",Array('formModel' => $this->formModel,'categories'=>$this->getCategories()));
	}
}
