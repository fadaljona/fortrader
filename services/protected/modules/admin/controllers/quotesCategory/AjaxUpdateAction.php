<?
Yii::import( 'controllers.base.ActionAdminBase' );
Yii::import( 'models.forms.AdminQuotesCategoryFormModel' );

final class AjaxUpdateAction extends ActionAdminBase {
	private $formModel;
	private function detFormModel() {
		$this->formModel = new AdminQuotesCategoryFormModel();
		$load = $this->formModel->load();
		if( !$load ) $this->throwI18NException( Yii::t( "quotesWidget","Категория не найдена!") );
	}
	private function getFormModel() {
		return $this->formModel;
	}
	function run() {
		$data = Array();
		try{
			$this->detFormModel();
			$formModel = $this->getFormModel();
			if( $formModel->validate()) {
				$id = $formModel->save();
				if( !$id ) $this->throwI18NException( Yii::t( "quotesWidget","Невозможно сохранить в АР!") );
				$data[ 'id' ] = $id;
			}
			else{
				list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
			}
		}
		catch( Exception $e ) {
			$data[ 'error' ] = $e->getMessage();
		}
		echo json_encode( $data );
	}
}
