<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxDeleteAction extends ActionAdminBase {
		private function getModel( $id ) {
			$model = QuotesCategoryModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( Yii::t( "quotesWidget","Категория не найдена!") );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}
