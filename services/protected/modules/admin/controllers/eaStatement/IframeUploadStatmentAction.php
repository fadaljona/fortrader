<?
	Yii::import( 'controllers.base.ActionFrontBase' );
	Yii::import( 'models.forms.AdminEAStatementFormModel' );

	final class IframeUploadStatmentAction extends ActionFrontBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminEAStatementFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Statement!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "//layouts/cleanPage" );
			
			$data = new StdClass();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				
				$formModel->getStatementInfo( $data );
			}
			catch( Exception $e ) {
				$data->error = $e->getMessage();
			}
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'data' => $data,
			));
		}
	}

?>