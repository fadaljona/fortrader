<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminEAStatementFormModel' );
	
	final class IframeAddAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminEAStatementFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find File!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "//layouts/cleanPage" );
			
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'data' => $data,
			));
		}
	}

?>