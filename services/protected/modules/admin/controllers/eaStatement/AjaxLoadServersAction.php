<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminIndividualExpenseFormModel' );
	
	final class AjaxLoadServersAction extends ActionAdminBase {
		private function getWidget() {
			$widget = $this->controller->createWidget( "widgets.forms.AdminEAStatementFormWidget" );
			return $widget;
		}
		function run( $idBroker ) {
			$data = Array();
			try{
				$widget = $this->getWidget();
				$data[ 'servers' ] = $widget->getServers( $idBroker );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>