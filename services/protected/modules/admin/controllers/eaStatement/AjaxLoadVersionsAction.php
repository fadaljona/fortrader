<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminIndividualExpenseFormModel' );
	
	final class AjaxLoadVersionsAction extends ActionAdminBase {
		private function getWidget() {
			$widget = $this->controller->createWidget( "widgets.forms.AdminEAStatementFormWidget" );
			return $widget;
		}
		function run( $idEA ) {
			$data = Array();
			try{
				$widget = $this->getWidget();
				$data[ 'versions' ] = $widget->getVersions( $idEA );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>