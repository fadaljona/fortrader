<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class EaStatementController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "EA Statements";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'eaControl' )),
				Array( 'deny' ),
			);
		}
	}

?>