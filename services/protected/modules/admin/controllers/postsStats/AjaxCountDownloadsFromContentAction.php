<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxCountDownloadsFromContentAction extends ActionAdminBase {
		private function getPostsStatsCommonModel( $id ) {
			$model = PostsStatsCommonModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Stats row!" );
			return $model;
		}

		function run( ) {
			$currentPostId = (int)@$_POST['current_post_id'];
			if( $currentPostId ){
				$referrer = @$_POST['referrer'];
				
				if( strpos($referrer, Yii::app()->request->hostInfo) === false ) return false;
				
				$model = $this->getPostsStatsCommonModel( $currentPostId );
				$model->downloadsFromContent = $model->downloadsFromContent + 1;
				$model->save();
			}
		}
	}
	


?>

