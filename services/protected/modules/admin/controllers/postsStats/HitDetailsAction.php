<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class HitDetailsAction extends ActionAdminBase {
		//const KEYStateFilterHost = 'admin.postshits.list.filter.host';
		const KEYStateFilterKeyword = 'admin.postshits.list.filter.Keyword';
		const KEYStateFilterReferer = 'admin.postshits.list.filter.Referer';

		private $filterModel;
		
		private function getPostsStatsCommonModel( $id ) {
			$model = PostsStatsCommonModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find post!" );
			return $model;
		}
		
		private function getFilterURL($id){
			if( $id ){
				return $this->controller->createUrl( 'hitDetails', array( 'id' => $id ) );
			}else{
				return $this->controller->createUrl( 'hitDetails' );
			}
			
		}
		private function detFilterModel() {
			$this->filterModel = new PostsStatsTrafficModel();
			$post = &$_POST[ get_class( $this->filterModel )];
			if( !empty( $post )) {
			//	Yii::App()->user->setState( self::KEYStateFilterHost, $post[ 'remoteHost' ]);
				Yii::App()->user->setState( self::KEYStateFilterKeyword, $post[ 'keyword' ]);
				Yii::App()->user->setState( self::KEYStateFilterReferer, $post[ 'referer' ]);
			}
			//$this->filterModel->remoteHost = Yii::App()->user->getState( self::KEYStateFilterHost );
			$this->filterModel->keyword = Yii::App()->user->getState( self::KEYStateFilterKeyword );
			$this->filterModel->referer = Yii::App()->user->getState( self::KEYStateFilterReferer );
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		
		function run() {
			$id = @$_GET['id'];
			if( $id ){
				$postsStatsCommonModel = $this->getPostsStatsCommonModel( $id );
			}
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Post hits" );
			$this->setLayout( "default/index" );
			
			$this->detFilterModel();			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'postId' => $id,
				'postsStatsCommonModel' => $postsStatsCommonModel,
				'filterURL' => $this->getFilterURL( $id ),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>