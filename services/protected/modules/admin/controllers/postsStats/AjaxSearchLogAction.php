<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxSearchLogAction extends ActionAdminBase {

		function run( ) {
			if( !isset( $_POST['searchKey'] ) || !$_POST['searchKey'] ) return true;
			WpSearchStatsModel::saveKey( $_POST['searchKey'] );
		}
	}

?>