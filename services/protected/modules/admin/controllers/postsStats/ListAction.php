<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		const KEYStateFilterTitle = 'admin.postsstats.list.filter.title';
		const KEYStateFilterPostId = 'admin.postsstats.list.filter.postid';
		
		const KEYStateFilterStartPostDate = 'admin.postsstats.list.filter.startpostdate';
		const KEYStateFilterEndPostDate = 'admin.postsstats.list.filter.endpostdate';
		
		private $filterModel;
		private $startPostDate = '';
		private $endPostDate = '';
		private function getFilterURL() {
			return $this->controller->createUrl( 'list' );
		}
		private function detFilterModel() {
			$this->filterModel = new PostsStatsCommonModel();
			$post = &$_POST[ get_class( $this->filterModel )];
			if( !empty( $post )) {
				Yii::App()->user->setState( self::KEYStateFilterTitle, $post[ 'title' ]);
				Yii::App()->user->setState( self::KEYStateFilterPostId, $post[ 'postId' ]);
				$startPostDate = $post['startPostDate'];
				$endPostDate = $post['endPostDate'];
				if( $startPostDate && $endPostDate ){
					if( strtotime( $startPostDate ) <= strtotime( $endPostDate ) ){
						Yii::App()->user->setState( self::KEYStateFilterStartPostDate,  date( 'Y-m-d H:i:s' ,strtotime( $startPostDate ) ) );
						Yii::App()->user->setState( self::KEYStateFilterEndPostDate, date( 'Y-m-d H:i:s' ,strtotime( $endPostDate ) ) );
					}
				}
			}
			$this->filterModel->title = Yii::App()->user->getState( self::KEYStateFilterTitle );
			$this->filterModel->postId = Yii::App()->user->getState( self::KEYStateFilterPostId );
			
			$this->startPostDate = Yii::App()->user->getState( self::KEYStateFilterStartPostDate );
			$this->endPostDate = Yii::App()->user->getState( self::KEYStateFilterEndPostDate );
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "default/index" );
			$this->detFilterModel();			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'startPostDate' => $this->startPostDate,
				'endPostDate' => $this->endPostDate
			));
		}
	}

?>