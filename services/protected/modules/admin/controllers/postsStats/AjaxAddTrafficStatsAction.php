<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxAddTrafficStatsAction extends ActionAdminBase {
		private function getPostsStatsCommonModel( $id ) {
			$model = PostsStatsCommonModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Campaign!" );
			return $model;
		}
		private function updateInterest( $prevId, $referer ) {
			if( $prevId && $referer != 'direct' ){
				$parsedReferer = parse_url( $referer );
				if( $parsedReferer['host'] == $_SERVER['HTTP_HOST'] ){
					$model = $this->getPostsStatsCommonModel( $prevId ); 
					$model->interest += 1;
					$model->save();
				}
			}
		}
		function run( ) {
			$prevPostId = (int)@$_POST['prev_post_id'];
			$currentPostId = (int)@$_POST['current_post_id'];
			if( $currentPostId ){
				$referrer = @$_POST['referrer'] ? @$_POST['referrer'] : 'direct';
				$this->updateInterest( $prevPostId, $referrer );
				$hostKeyArr = PostsStatsCommonModel::getHostAndKeyword( $referrer );
				
				$trafficModel = new PostsStatsTrafficModel;
				$trafficModel->postId = $currentPostId;
				$trafficModel->remoteHost = $hostKeyArr['host'];
				$trafficModel->referer = $referrer;
				$trafficModel->keyword = $hostKeyArr['keyword'];
				$trafficModel->visitDate = date( 'Y-m-d H:i:s', time() );
				$trafficModel->save();
			}
		}
	}
	


?>

