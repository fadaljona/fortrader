<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxSetSmMarkerAction extends ActionAdminBase {
		private function getModel( $id ) {
			$model = PostsStatsCommonModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Post!" );
			return $model;
		}
		function run( ) {
			$model = $this->getModel( (int)@$_POST['id'] );
			$model->updateSm = 1;
			$model->save();
		}
	}

?>