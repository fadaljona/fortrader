<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadRowAction extends ActionAdminBase {
		private function getDP( $id ) {
			$DP = new CActiveDataProvider( "AdvertisementCampaignModel", Array(
				'criteria' => Array(
					'select' => Array(
						" `t`.* ",
						" (
							SELECT			SUM( `countShows` )
							FROM			{{advertisement_stats}}
							INNER JOIN		{{advertisement}} ON ( `id` = `idAd` )
							WHERE			`idCampaign` = `t`.`id`
						) AS countShows",
						" (
							SELECT			SUM( `countClicks` )
							FROM			{{advertisement_stats}}
							INNER JOIN		{{advertisement}} ON ( `id` = `idAd` )
							WHERE			`idCampaign` = `t`.`id`
						) AS countClicks",
					),
					'condition' => '`t`.`id` = :id',
					'params' => Array(
						':id' => $id,
					),
				),
			));
			if( !$DP->getTotalItemCount( )) $this->throwI18NException( "Can't find Campaign!" );
			return $DP;
		}
		private function getWidget( $DP ) {
			$mode = Yii::App()->user->checkAccess( 'advertisementControl' ) ? 'Admin' : 'User';
			$widget = $this->controller->createWidget( 'widgets.gridViews.AdminAdvertisementCampaignsGridViewWidget', Array(
				'dataProvider' => $DP,
				'mode' => $mode,
			));
			return $widget;
		}
		private function renderRow( $widget ) {
			ob_start();
			$widget->renderTableRow( 0 );
			return ob_get_clean();
		}
		function run() {
			$data = Array();
			try{
				$id = (int)@$_GET[ 'id' ];
				$DP = $this->getDP( $id );
				
				$model = $DP->data[0];
				if( !$model->checkAccess() ) $this->throwI18NException( "Access denied!" );
				
				$widget = $this->getWidget( $DP );
				
				$data[ 'row' ] = $this->renderRow( $widget );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>