<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminAdvertisementCampaignFormModel' );
	
	final class AjaxAddAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminAdvertisementCampaignFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Campaign!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				if( Yii::App()->user->isRoot()) $this->throwI18NException( "Root can't create campaign!" );
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( !$formModel->getAR()->checkAccess() ) $this->throwI18NException( "Access denied!" );
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>