<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminAdvertisementCampaignFormModel' );
	
	final class AjaxLoadAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminAdvertisementCampaignFormModel();
			$id = (int)@$_GET['id'];
			if( !$id ) $this->throwI18NException( "Set id!" );
			$load = $this->formModel->load( $id );
			if( !$load ) $this->throwI18NException( "Can't find Campaign!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( !$formModel->getAR()->checkAccess() ) $this->throwI18NException( "Access denied!" );
				$data[ 'model' ] = $formModel->saveToObject();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>