<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxDeleteAction extends ActionAdminBase {
		private function getModel( $id ) {
			$model = AdvertisementCampaignModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Campaign!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				if( !$model->checkAccess() ) $this->throwI18NException( "Access denied!" );
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>