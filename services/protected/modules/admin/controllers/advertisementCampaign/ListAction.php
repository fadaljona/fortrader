<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		const KEYStateFilterName = 'admin.advertisementcampaigns.list.filter.name';
		const KEYStateFilterStatus = 'admin.advertisementcampaigns.list.filter.status';
		private $filterModel;
		private function getFilterURL() {
			return $this->controller->createUrl( 'list' );
		}
		private function detFilterModel() {
			$this->filterModel = new AdvertisementCampaignModel();
			$post = &$_POST[ get_class( $this->filterModel )];
			if( !empty( $post )) {
				Yii::App()->user->setState( self::KEYStateFilterName, $post[ 'name' ]);
				Yii::App()->user->setState( self::KEYStateFilterStatus, $post[ 'status' ]);
			}
			$this->filterModel->name = Yii::App()->user->getState( self::KEYStateFilterName );
			$this->filterModel->status = Yii::App()->user->getState( self::KEYStateFilterStatus );
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "advertisement/index" );
			
			$this->detFilterModel();
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>