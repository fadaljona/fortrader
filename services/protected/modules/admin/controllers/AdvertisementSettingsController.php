<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class AdvertisementSettingsController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Ads settings';
		}
		function actionIndex() {
			$this->redirect( Array( 'update' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'adSettingsControl' )),
				Array( 'deny' ),
			);
		}
	}

?>