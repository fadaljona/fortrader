<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class InformersController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Informers";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'informersControl' )),
				Array( 'allow', 'actions' => array('categories', 'settings', 'styles', 'ajaxLoadItem', 'ajaxAddItem', 'ajaxLoadListRow', 'ajaxSubmitCategory', 'ajaxSettingsUpdate'), 'roles' => Array( 'informersTranslateControl' )),
				Array( 'deny' ),
			);
		}
	}

?>