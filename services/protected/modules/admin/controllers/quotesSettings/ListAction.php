<?
Yii::import('controllers.base.ActionAdminBase');
Yii::import( 'models.forms.AdminQuotesSettingsFormModel' );

final class ListAction extends ActionAdminBase{
	private $formModel;
	private function detFormModel() {
		$this->formModel = new AdminQuotesSettingsFormModel();
		$load = $this->formModel->load();
		if( !$load ) $this->throwI18NException( Yii::t( "quotesWidget","Настройки не найдены!") );
	}

	function run(){
		$this->setLayoutTitle(Yii::t( "quotesWidget","Настройки модуля котировок"));
		$this->detFormModel();
		$this->controller->render("list/view",Array('formModel' => $this->formModel));
	}
}
