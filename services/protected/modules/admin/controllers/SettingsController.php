<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class SettingsController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Settings';
		}
		function actionIndex() {
			$this->redirect( Array( 'update' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'settings' )),
				Array( 'deny' ),
			);
		}
	}

?>