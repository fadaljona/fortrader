<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class ContestController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Contests";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'contestControl' )),
				Array( 'deny' ),
			);
		}
	}

?>