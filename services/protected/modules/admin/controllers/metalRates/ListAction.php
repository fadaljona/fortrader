<?php

	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayoutTitle( "Metals" );
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>