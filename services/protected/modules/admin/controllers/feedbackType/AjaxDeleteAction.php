<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxDeleteAction extends ActionAdminBase {
		private function getModel( $id ) {
			$model = FeedbackTypeModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Type!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>