<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "feedback/index" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>