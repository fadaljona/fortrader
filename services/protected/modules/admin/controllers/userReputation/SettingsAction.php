<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminUserReputationSettingsFormModel' );

	final class SettingsAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminUserReputationSettingsFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Settings!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();

			$this->detFormModel();
			
			$this->setLayoutTitle( "Settings" );
			$this->setLayout( "{$controllerCleanClass}/index" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>