<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminInformersCategoryFormModel' );
	
	final class WpCatsForNewsInformerAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminInformersCategoryFormModel();
			$load = $this->formModel->loadBySlug( 'news' );
			if( !$load ) $this->throwI18NException( "Can't load model" );
		}
		
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Wp cats for news informer" );
			$this->detFormModel();	
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->formModel
			));
		}
	}

?>