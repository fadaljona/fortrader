<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminInformersSettingsFormModel' );

	final class SettingsAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminInformersSettingsFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't load model" );
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Settings" );
			$this->setLayout( "informers/settings" );
			$this->detFormModel();
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->formModel
			));
		}
	}

?>