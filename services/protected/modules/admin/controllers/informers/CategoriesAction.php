<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class CategoriesAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Categories" );
			$this->setLayout( "informers/categories" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>