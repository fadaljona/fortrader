<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class StatsAction extends ActionAdminBase {

		const KEYStateFilterStartDate = 'admin.InformerStats.filter.startdate';
		const KEYStateFilterEndDate = 'admin.InformerStats.filter.enddate';
		const KEYStateFilterGroupBy = 'admin.InformerStats.filter.groupBy';
		const KEYStateFilterInformerCat = 'admin.InformerStats.filter.informerCat';
		
		private $startDate = '';
		private $endDate = '';
		private $groupBy = '';
		private $informerCat = 0;

		private function detFilterModel() {
			if( !empty( $_POST )) {
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				if( $startDate && $endDate ){
					if( strtotime( $startDate ) <= strtotime( $endDate ) ){
						Yii::App()->user->setState( self::KEYStateFilterStartDate,  date( 'Y-m-d H:i:s' ,strtotime( $startDate ) ) );
						Yii::App()->user->setState( self::KEYStateFilterEndDate, date( 'Y-m-d H:i:s' ,strtotime( $endDate ) ) );
					}
				}
				$groupBy = $_POST['statsGroupBy'];
				if( $groupBy == 'domain' || $groupBy == 'domain, cat, style' ){
					Yii::App()->user->setState( self::KEYStateFilterGroupBy,  $groupBy );
				}
				

				if( isset( $_POST['informerCat'] ) ){
					Yii::App()->user->setState( self::KEYStateFilterInformerCat,  $_POST['informerCat'] );
				}
				
			}
			$this->startDate = Yii::App()->user->getState( self::KEYStateFilterStartDate );
			$this->endDate = Yii::App()->user->getState( self::KEYStateFilterEndDate );
			$this->groupBy = Yii::App()->user->getState( self::KEYStateFilterGroupBy );
			$this->informerCat = Yii::App()->user->getState( self::KEYStateFilterInformerCat );
			
			if( !$this->startDate ) $this->startDate = date( 'Y-m-d' ) . ' 00:00:00';
			if( !$this->endDate ) $this->endDate = date( 'Y-m-d' ) . ' 23:59:59';
			if( !$this->groupBy ) $this->groupBy = 'domain';
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Informer Stats" );
			$this->setLayout( "default/index" );
			$this->detFilterModel();			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'startDate' => $this->startDate,
				'endDate' => $this->endDate,
				'groupBy' => $this->groupBy,
				'informerCat' => $this->informerCat
			));
		}
	}

?>