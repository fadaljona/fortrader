<?php

	Yii::import( 'controllers.base.ActionAdminBase' );

	final class StylesAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Styles" );
			$this->setLayout( "informers/styles" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>