<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.*' );
	
	final class AjaxOptionLoadAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			if( !isset( $_GET['formModelName'] ) || !$_GET['formModelName'] || !class_exists( $_GET['formModelName'] ) ) $this->throwI18NException( "Can't find model!" );
			$formModelName = $_GET['formModelName'];
			$this->formModel = new $formModelName;
			$id = (int)@$_GET['id'];
			if( !$id ) $this->throwI18NException( "Set id!" );
			$load = $this->formModel->load( $id );
			if( !$load ) $this->throwI18NException( "Can't find Instrument!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				$data[ 'model' ] = $formModel->saveToObject();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>