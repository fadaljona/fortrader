<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class BinaryAction extends ActionAdminBase {
		const KEYStateFilterBrokerName = 'admin.brokers.binary.filter.brokerName';

		private $filterModel;
		private function getFilterURL() {
			return $this->controller->createUrl( 'binary' );
		}
		private function detFilterModel() {
			$this->filterModel = new BrokerI18NModel();
			$post = &$_POST[ get_class( $this->filterModel )];
			if( !empty( $post )) {
				Yii::App()->user->setState( self::KEYStateFilterBrokerName, $post[ 'officialName' ]);
			}
			$this->filterModel->officialName = Yii::App()->user->getState( self::KEYStateFilterBrokerName );
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFilterModel();
			
			$this->setLayoutTitle( "Binary options" );
			$this->setLayout( "{$controllerCleanClass}/index" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(

				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>
