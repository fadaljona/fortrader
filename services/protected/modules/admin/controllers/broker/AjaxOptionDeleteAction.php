<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxOptionDeleteAction extends ActionAdminBase {
		private function getModel( $id ) {
			if( !isset( $_GET['modelName'] ) || !$_GET['modelName'] || !class_exists( $_GET['modelName'] ) ) $this->throwI18NException( "Can't find model!" );
			$modelName = $_GET['modelName'];
			$model = $modelName::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Instrument!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>