<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class LinksClickLogAction extends ActionAdminBase {

		const KEYStateFilterStartDate = 'admin.broker.linksClickLog.filter.startdate';
		const KEYStateFilterEndDate = 'admin.broker.linksClickLog.filter.enddate';
		
		private $startDate = '';
		private $endDate = '';

		private function detFilterModel() {
			if( !empty( $_POST )) {
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				if( $startDate && $endDate ){
					if( strtotime( $startDate ) <= strtotime( $endDate ) ){
						Yii::App()->user->setState( self::KEYStateFilterStartDate,  date( 'Y-m-d H:i:s' ,strtotime( $startDate ) ) );
						Yii::App()->user->setState( self::KEYStateFilterEndDate, date( 'Y-m-d H:i:s' ,strtotime( $endDate ) ) );
					}
				}
			}
			$this->startDate = Yii::App()->user->getState( self::KEYStateFilterStartDate );
			$this->endDate = Yii::App()->user->getState( self::KEYStateFilterEndDate );
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Links click log" );
			$this->setLayout( "default/index" );
			$this->detFilterModel();			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'startDate' => $this->startDate,
				'endDate' => $this->endDate
			));
		}
	}

?>