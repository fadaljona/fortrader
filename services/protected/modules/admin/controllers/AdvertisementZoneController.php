<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class AdvertisementZoneController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Advertisements zones";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'advertisementZoneControl' )),
				Array( 'deny' ),
			);
		}
	}

?>