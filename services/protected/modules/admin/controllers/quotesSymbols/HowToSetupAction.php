<?php
Yii::import('controllers.base.ActionAdminBase');

final class HowToSetupAction extends ActionAdminBase
{
    public function run()
    {
        $this->setLayoutTitle(Yii::t("quotesWidget", "How to setup"));
        $this->controller->render("{$this->getCleanClassName()}/view", array());
    }
}
