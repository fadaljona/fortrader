<?php
Yii::import('controllers.base.ActionAdminBase');
Yii::import('models.forms.FiltersForm');

final class BinanceSymbolsAction extends ActionAdminBase
{
    private $symbols;

    private function getSymbols()
    {
        if (!empty($this->symbols)) {
            return $this->symbols;
        }

        $symbols = QuotesSymbolsModel::getAvailableBinanceSymbols();
        if ($symbols) {
            $this->symbols = $symbols;
        } else {
            return false;
        }

        return $this->symbols;
    }

    private function getFromTo()
    {
        $symbols = $this->getSymbols();
        if (!$symbols) {
            return false;
        }

        $outArr = array();

        foreach ($symbols as $symbol) {
            $outArr['from'][] = $symbol->baseAsset;
            $outArr['to'][] = $symbol->quoteAsset;
        }

        $outArr['from'] = array_unique($outArr['from']);
        $outArr['to'] = array_unique($outArr['to']);

        return $outArr;
    }

    private function filterSymbols($from, $to)
    {
        $symbols = $this->getSymbols();
        if (!$symbols) {
            return false;
        }

        if ($from || $to) {
            foreach ($symbols as $i => $symbol) {
                if ($from && $symbol->baseAsset != $from) {
                    unset($symbols[$i]);
                }
                if ($to && $symbol->quoteAsset != $to) {
                    unset($symbols[$i]);
                }
            }
        }

        return $symbols;
    }

    public function getSymbolsDp($from, $to)
    {
        $symbols = $this->filterSymbols($from, $to);
        if ($symbols) {
            $dp = new CArrayDataProvider($symbols);
            $dp->pagination->pageSize = 50;
            return $dp;
        }
        return false;
    }

    public function run($from = false, $to = false)
    {

        $this->setLayoutTitle(Yii::t("quotesWidget", "Binance symbols"));
        $this->controller->render("{$this->getCleanClassName()}/view", array(
            'DP' => $this->getSymbolsDp($from, $to),
            'getFromTo' => $this->getFromTo()
        ));
    }
}
