<?
Yii::import('controllers.base.ActionAdminBase');
Yii::import( 'models.forms.AdminQuotesSymbolsFormModel' );

final class ListAction extends ActionAdminBase{
	private $formModel;
	const KEYStateFilterId = 'admin.quotesSymbols.list.filter.id';
	const KEYStateFilterDefault = 'admin.quotesSymbols.list.filter.default';
	const KEYStateFilterPanel = 'admin.quotesSymbols.list.filter.panel';
	const KEYStateFilterName = 'admin.quotesSymbols.list.filter.name';
	const KEYStateFilterNAlias = 'admin.quotesSymbols.list.filter.nalias';
	const KEYStateFilterDesc = 'admin.quotesSymbols.list.filter.desc';
	const KEYStateFilterCategory = 'admin.quotesSymbols.list.filter.category';
	const KEYStateFilterVisible = 'admin.quotesSymbols.list.filter.visible';
	const KEYStateFilterSourceType = 'admin.quotesSymbols.list.filter.sourceType';
	
	const KEYStateFilterTickName = 'admin.quotesSymbols.list.filter.tickName';
	const KEYStateFilterHistName = 'admin.quotesSymbols.list.filter.histName';
	
	private $filterModel;
	private function getFilterURL() {
		return $this->controller->createUrl( 'list' );
	}
	private function detFilterModel() {
		$this->filterModel = new QuotesSymbolsModel();
		$post = &$_POST[ get_class( $this->filterModel )];
		if( !empty( $post )) {
			Yii::App()->user->setState( self::KEYStateFilterId, $post[ 'id' ]);
			Yii::App()->user->setState( self::KEYStateFilterDefault, $post[ 'sdefault' ]);
			Yii::App()->user->setState( self::KEYStateFilterPanel, $post[ 'panel' ]);
			Yii::App()->user->setState( self::KEYStateFilterName, $post[ 'name' ]);
			Yii::App()->user->setState( self::KEYStateFilterNAlias, $post[ 'snalias' ]);
			Yii::App()->user->setState( self::KEYStateFilterDesc, $post[ 'desc' ]);
			Yii::App()->user->setState( self::KEYStateFilterCategory, $post[ 'category' ]);
			Yii::App()->user->setState( self::KEYStateFilterVisible, $post[ 'svisible' ]);
			Yii::App()->user->setState( self::KEYStateFilterSourceType, $post[ 'sourceType' ]);
			
			Yii::App()->user->setState( self::KEYStateFilterTickName, $post[ 'tickName' ]);
			Yii::App()->user->setState( self::KEYStateFilterHistName, $post[ 'histName' ]);
		}
		
		$this->filterModel->id = Yii::App()->user->getState( self::KEYStateFilterId );
		$this->filterModel->sdefault = Yii::App()->user->getState( self::KEYStateFilterDefault );
		$this->filterModel->panel = Yii::App()->user->getState( self::KEYStateFilterPanel );
		$this->filterModel->name = Yii::App()->user->getState( self::KEYStateFilterName );
		$this->filterModel->snalias = Yii::App()->user->getState( self::KEYStateFilterNAlias );
		$this->filterModel->sdesc = Yii::App()->user->getState( self::KEYStateFilterDesc );
		$this->filterModel->category = Yii::App()->user->getState( self::KEYStateFilterCategory );
		$this->filterModel->svisible = Yii::App()->user->getState( self::KEYStateFilterVisible );
		$this->filterModel->sourceType = Yii::App()->user->getState( self::KEYStateFilterSourceType );
		
		$this->filterModel->tickName = Yii::App()->user->getState( self::KEYStateFilterTickName );
		$this->filterModel->histName = Yii::App()->user->getState( self::KEYStateFilterHistName );
	}
	private function getFilterModel() {
		return $this->filterModel;
	}
	private function detFormModel() {
		$this->formModel = new AdminQuotesSymbolsFormModel();
		$load = $this->formModel->load();
		if( !$load ) $this->throwI18NException( Yii::t( "quotesWidget","Невозможно загрузить модель котировок!") );
	}

	private function getCategories() {
		$categories=array();
		$categories[0]='';
		$aCatsFromModel=QuotesCategoryModel::getAll();

		if($aCatsFromModel)
			foreach($aCatsFromModel as $oCategory)
				$categories[$oCategory->id]=$oCategory->name;

		return $categories;
	}

	function run(){
		$this->setLayoutTitle(Yii::t( "quotesWidget","Котировки"));
		$this->detFilterModel();
		$this->detFormModel();
		
		
		$this->controller->render("list/view",Array(
			'formModel' => $this->formModel,
			'filterURL' => $this->getFilterURL(),
			'filterModel' => $this->getFilterModel(),
			'categories'=>$this->getCategories()
		));
	}
}
