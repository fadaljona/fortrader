<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class AdvertisementController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Advertisements";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'advertisementControl', 'ownAdvertisementControl' )),
				Array( 'deny' ),
			);
		}
	}

?>