<?
	
	final class StrategyIndicatorController extends AdminControllerExBase {
		function detLayoutTitle() {
			return "Strategy indicators";
		}
		function detLayoutInfo() {
			return "On this page you can manage strategy indicators.";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'strategyControl' )),
				Array( 'deny' ),
			);
		}
	}

?>