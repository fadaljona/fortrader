<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class CategoryRatingsController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Category ratings";
		}
		function actionIndex() {
			$this->redirect( Array( 'rating' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'categoryRatingsControl' )),
				Array( 'deny' ),
			);
		}
	}

?>