<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadRowLogAction extends ActionAdminBase {
		private function getDP( $id ) {
			$DP = new CActiveDataProvider( "ContestMemberLogModel", Array(
				'criteria' => Array(
					'condition' => '`t`.`id` = :id',
					'params' => Array(
						':id' => $id,
					),
				),
			));
			if( !$DP->getTotalItemCount( )) $this->throwI18NException( "Can't find Log!" );
			return $DP;
		}
		private function getWidget( $DP ) {
			$widget = $this->controller->createWidget( 'widgets.gridViews.AdminContestMemberLogsGridViewWidget', Array(
				'dataProvider' => $DP,
			));
			return $widget;
		}
		private function renderRow( $widget ) {
			ob_start();
			$widget->renderTableRow( 0 );
			return ob_get_clean();
		}
		function run() {
			$data = Array();
			try{
				$id = (int)@$_GET[ 'id' ];
				$DP = $this->getDP( $id );
				$widget = $this->getWidget( $DP );
				
				$data[ 'row' ] = $this->renderRow( $widget );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>