<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class AjaxDeleteAction extends ActionAdminBase {
		private $monitoringAddr = 'http://159.69.219.218:18466/';
		private function getModel( $id ) {
			$model = ContestMemberModel::model()->findByPk( $id );
			if( !$model ) $this->throwI18NException( "Can't find Member!" );
			return $model;
		}
		function run( $id ) {
			$data = Array();
			try{
				$model = $this->getModel( $id );
				
				$url = $this->monitoringAddr . "master.api?cmd=del_acc&aid={$model->id}&login={$model->accountNumber}";
				
				$deleteResult = CommonLib::downloadFileFromWww( $url );
				if( !$deleteResult ) $this->throwI18NException( "Empty delete result, please contact admin" );
				
				preg_match_all('/aid\:\s*'.$model->id.'\s*login\:\s*'.$model->accountNumber.'\s*delete/',
					$deleteResult,
					$found,
					PREG_PATTERN_ORDER
				);
				
				if( !isset($found[0][0]) || !$found[0][0] ) $this->throwI18NException( "Wrong delete response, please contact admin" );
				
				$model->delete();
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>