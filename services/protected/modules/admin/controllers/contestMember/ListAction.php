<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		
		const KEYStateFilter = 'admin.contestMember.list.filter';
	
		private $filterModel;
		
		private function detFilterModel() {
			$this->filterModel = new ContestMemberModel();
			$post = &$_POST[ get_class( $this->filterModel )];
			
			$cacheKey = self::KEYStateFilter . 'contest' . $_GET['idContest'];
			
			$accNumKey = $cacheKey . 'accnum';
			$userNameKey = $cacheKey . 'userName';
			
			if( !empty( $post )) {
				Yii::App()->user->setState( $accNumKey, $post[ 'accountNumber' ]);
				Yii::App()->user->setState( $userNameKey, $post[ 'idUser' ]);
			}

			$this->filterModel->accountNumber = Yii::App()->user->getState( $accNumKey );
			$this->filterModel->idUser = Yii::App()->user->getState( $userNameKey );

		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function getFilterURL() {
			return $this->controller->createUrl( 'list', array( 'idContest' => $_GET['idContest'] ) );
		}
		
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFilterModel();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "contest/index" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>