<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class EaVersionController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Version EA";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'eaControl' )),
				Array( 'deny' ),
			);
		}
	}

?>