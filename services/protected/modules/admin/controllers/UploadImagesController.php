<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UploadImagesController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'uploadImagesControl' )),
				Array( 'deny' ),
			);
		}
	}

?>