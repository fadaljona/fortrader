<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminAdvertisementZoneFormModel' );
	
	final class AjaxRenderPreviewAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminAdvertisementZoneFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Zone!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				$formModel->setMode( AdminAdvertisementZoneFormModel::MODE_PREVIEW );
				if( $formModel->validate()) {
					if( in_array( $formModel->typeAds, Array( 'Media', 'Buttons' ))) {
						throw new Exception( Yii::t( '*', 'Preview not possible' ));
					}
					$formModel->saveToAR();
					$this->controller->renderPartial( "{$actionCleanClass}/view", Array(
						'zone' => $formModel->getAR(),
					));
				}
				else{
					list( $errorField, $error ) = $formModel->getFirstError();
					throw new Exception( $error );
				}
			}
			catch( Exception $e ) {
				echo $e->getMessage();
			}
		}
	}

?>