<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class BrokerUserActivityController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Brokers activity";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'brokerControl' )),
				Array( 'deny' ),
			);
		}
	}

?>