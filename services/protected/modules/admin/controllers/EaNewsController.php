<?
	Yii::import( 'controllers.base.ControllerAdminBase' );
	
	final class EaNewsController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "News";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'eaControl' )),
				Array( 'deny' ),
			);
		}
	}

?>