<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadChangesAction extends ActionAdminBase {
		private function getIdUser() {
			$id = (int)@$_POST[ 'id' ];
			return $id;
		}
		private function checkAccess() {
			if( !Yii::App()->user->checkAccess( 'userBalanceControl' )) {
				$id = $this->getIdUser();
				if( Yii::App()->user->id != $id ) {
					$this->throwI18NException( "Access denied!" );
				}
			}
		}
		private function getDP() {
			$id = $this->getIdUser();
			$DP = new CActiveDataProvider( "UserBalanceChangeModel", Array(
				'criteria' => Array(
					'condition' => '`t`.`idUser` = :idUser',
					'order' => '`t`.`createdDT` DESC',
					'params' => Array(
						':idUser' => $id,
					),
				),
				'pagination' => $this->getDPPagination(),
			));
			//$DP->pagination->pageSize = 1;
			return $DP;
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->checkAccess();
			
			$this->controller->renderPartial( "{$actionCleanClass}/view", Array(
				'DP' => $this->getDP(),
			));
		}
	}

?>