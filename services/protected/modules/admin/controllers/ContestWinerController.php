<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class ContestWinerController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Contests winers";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'contestWinerControl' )),
				Array( 'deny' ),
			);
		}
	}

?>