<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminLanguagesImportFormModel' );
	Yii::import( 'components.LanguagesSyncComponent' );
	
	final class ImportAction extends ActionAdminBase {
		const PATHUploadDir = 'uploads/languages';
		const TTLTemp = 3600;
		const KEYStateUploadFile = 'admin.language.uploaded';
		private $formModel;
		private $languages = Array();
		private $message;
		private function clearUploadDir() {
			$now = time();
			$files = glob( self::PATHUploadDir."/*" );
			if( $files ) {
				foreach( $files as $file ) {
					if( $now - filemtime( $file ) > self::TTLTemp ) {
						@unlink( $file );
					}
				}
			}
		}
		private function getUploadFileFreeName() {
			do{
				$name = substr( md5( rand() ), 0, 8 );
				$isset = is_file( self::PATHUploadDir."/{$name}" );
			}
			while( $isset );
			return $name;
		}
		private function detFormModel() {
			$this->formModel = new AdminLanguagesImportFormModel();
		}
		private function getFormModel() {
			return $this->formModel;
		}
		private function setLanguages( $languages ) {
			$this->languages = $languages;
		}
		private function getLanguages() {
			return $this->languages;
		}
		private function getMessage() {
			return $this->message;
		}
		private function setMessage( $message ) {
			$this->message = $message;
		}
		private function processForm() {
			$form = $this->getFormModel();
			$mode = AdminLanguagesImportFormModel::MODE_UPLOAD;
			
			$fileName = Yii::App()->user->getState( self::KEYStateUploadFile );
			if( $fileName ) {
				$file = self::PATHUploadDir."/{$fileName}";
				if( is_file( $file )) {
					$data = @unserialize( file_get_contents( $file ));
					if( $data ) {
						$languages = @$data[ 'languages' ];
						if( $languages ) $this->setLanguages( $languages );
						$mode = AdminLanguagesImportFormModel::MODE_SELECT_LANGUAGES;
					}
				}
			}
			
			$form->setMode( $mode );
			if( $mode == AdminLanguagesImportFormModel::MODE_SELECT_LANGUAGES ) {
				$form->setDefaultLanguages( array_keys( $languages ));
			}
			
			$form->load();
			
			if( $form->isPostRequest()) {
				if( $form->validate()) {
					switch( $form->getMode() ) {
						case AdminLanguagesImportFormModel::MODE_UPLOAD: {
							$this->clearUploadDir();
							$fileName = $this->getUploadFileFreeName();
							$form->file->saveAs( self::PATHUploadDir."/{$fileName}" );
							Yii::App()->user->setState( self::KEYStateUploadFile, $fileName );
							$this->redirect( Array( 'import' ));
							break;
						}
						case AdminLanguagesImportFormModel::MODE_SELECT_LANGUAGES: {
							if( $form->cancelAction ) {
								if( is_file( $file )) unlink( $file );
								$this->redirect( Array( 'import' ));
							}
							
							$sync = new LanguagesSyncComponent();
							$result = $sync->import( $data, $form->languages );
							if( is_file( $file )) unlink( $file );
							$NSi18n = $this->getNSi18n();
							$form->setMode( AdminLanguagesImportFormModel::MODE_HIDDEN );
							if( $result == LanguagesSyncComponent::STATUS_OK ) {
								$this->setMessage( Yii::t( $NSi18n, "Import success" ));
							}
							else{
								$this->setMessage( Yii::t( $NSi18n, "Error" ));
							}
							break;
						}
					}
				}
			}
		}
		function run() {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFormModel();
			$this->processForm();
			
			$this->setLayoutTitle( "Import" );
			$this->setLayout( "{$controllerCleanClass}/index" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->getFormModel(),
				'languages' => $this->getLanguages(),
				'message' => $this->getMessage(),
			));
		}
	}

?>