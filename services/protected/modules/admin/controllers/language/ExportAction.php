<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminLanguagesExportFormModel' );
	Yii::import( 'components.LanguagesSyncComponent' );

	final class ExportAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminLanguagesExportFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			return $this->formModel;
		}
		private function processForm() {
			$form = $this->getFormModel();
			if( $form->isPostRequest()) {
				if( $form->validate()) {
					$sync = new LanguagesSyncComponent();
					$sync->export( $form->languages, $data );
					Yii::App()->request->sendFile( 'languages.dat', serialize( $data ));
				}
			}
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			return $languages;
		}
		function run() {
			$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFormModel();
			$this->processForm();
			
			$this->setLayoutTitle( "Export" );
			$this->setLayout( "{$controllerCleanClass}/index" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->getFormModel(),
				'languages' => $this->getLanguages(),
			));
		}
	}

?>