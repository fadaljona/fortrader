<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		const KEYStateFilterKey = 'admin.language.list.filter.key';
		const KEYStateFilterNS = 'admin.language.list.filter.ns';
		private $filterModel;
		private function getFilterURL() {
			return $this->controller->createUrl( 'list' );
		}
		private function detFilterModel() {
			$this->filterModel = MessageModel::instance( "", "" );
			$post = &$_POST[ get_class( $this->filterModel )];
			if( !empty( $post )) {
				Yii::App()->user->setState( self::KEYStateFilterKey, $post[ 'key' ]);
				Yii::App()->user->setState( self::KEYStateFilterNS, $post[ 'ns' ]);
			}
			$this->filterModel->key = Yii::App()->user->getState( self::KEYStateFilterKey );
			$this->filterModel->ns = Yii::App()->user->getState( self::KEYStateFilterNS );
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFilterModel();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "language/index" );

			$this->controller->render( "{$actionCleanClass}/view", Array(
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>