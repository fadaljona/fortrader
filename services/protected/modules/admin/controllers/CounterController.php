<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class CounterController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Counters";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'counterControl' )),
				Array( 'deny' ),
			);
		}
	}

?>