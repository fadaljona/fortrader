<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UserPrivateMessageController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Private messages';
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'userPrivateMessageControl' )),
				Array( 'deny' ),
			);
		}
	}

?>