<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class BrokerRegulatorController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Brokers regulators";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'brokerControl' )),
				Array( 'deny' ),
			);
		}
	}

?>