<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class ContestSettingsController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Contest settings';
		}
		function actionIndex() {
			$this->redirect( Array( 'update' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'contestControl' )),
				Array( 'deny' ),
			);
		}
	}

?>