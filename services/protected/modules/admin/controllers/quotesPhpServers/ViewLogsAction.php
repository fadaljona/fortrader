<?
Yii::import('controllers.base.ActionAdminBase');

final class ViewLogsAction extends ActionAdminBase{
	protected $path;
	
	protected $quotesServerAppLog;
	protected $quotesServerErrorLog;
	protected $quotesHistoryServerAppLog;
	protected $quotesHistoryServerErrorLog;
	protected $pushServerAppLog;
	protected $pushServerErrorLog;
	
	private function getPhpServersPath(){
		$quotesSettings = QuotesSettingsModel::getInstance();
		if(!$this->path) $this->path = Yii::getPathOfAlias('webroot') .'/..'. $quotesSettings->phpServersPath;
		return $this->path;
	}
	private function setLogFiles(){
		$path = $this->getPhpServersPath();
		
		$this->quotesServerAppLog = $path . '/log/quotes-server-application.log';
		$this->quotesServerErrorLog = $path . '/log/quotes-server-error.log';
		
		$this->quotesHistoryServerAppLog = $path . '/log/quotes-history-server-application.log';
		$this->quotesHistoryServerErrorLog = $path . '/log/quotes-history-server-error.log';
		
		$this->pushServerAppLog = $path . '/log/push-server-application.log';
		$this->pushServerErrorLog = $path . '/log/push-server-error.log';
	}
	private function getServersLogs(){
		$path = $this->getPhpServersPath();
		
		$outArr = array();
		clearstatcache();
		if( file_exists($this->quotesServerAppLog) )
			$outArr['quotesServerAppLog'] = file_get_contents($this->quotesServerAppLog);
		if( file_exists($this->quotesServerErrorLog) )
			$outArr['quotesServerErrorLog'] = file_get_contents($this->quotesServerErrorLog);
		if( file_exists($this->quotesHistoryServerAppLog) )
			$outArr['quotesHistoryServerAppLog'] = file_get_contents($this->quotesHistoryServerAppLog);
		if( file_exists($this->quotesHistoryServerErrorLog) )
			$outArr['quotesHistoryServerErrorLog'] = file_get_contents($this->quotesHistoryServerErrorLog);
		if( file_exists($this->pushServerAppLog) )
			$outArr['pushServerAppLog'] = file_get_contents($this->pushServerAppLog);
		if( file_exists($this->pushServerErrorLog) )
			$outArr['pushServerErrorLog'] = file_get_contents($this->pushServerErrorLog);
		return $outArr;
	}
	private function clearLogs(){
		file_put_contents($this->quotesServerAppLog, '');
		file_put_contents($this->quotesServerErrorLog, '');
		file_put_contents($this->quotesHistoryServerAppLog, '');
		file_put_contents($this->quotesHistoryServerErrorLog, '');
		file_put_contents($this->pushServerAppLog, '');
		file_put_contents($this->pushServerErrorLog, '');
	}

	function run(){
		if( isset($_GET['act']) && $_GET['act'] == 'getZipLogs' ){
			$path = $this->getPhpServersPath();
			$zipLog = $path . '/log/logs.zip';
			$type = CommonLib::getMimeType( $zipLog );
			$fileName = basename( $zipLog );
			
			header( "Content-type: {$type}" );
			header( "Content-Disposition: attachment; filename=\"{$fileName}\"" );
			
			readfile( $zipLog );
			return true;
		}
		
		$this->setLogFiles();
		
		if( isset($_GET['act']) && $_GET['act'] == 'clear' ){
			$this->clearLogs();
			$this->redirect( Array( 'quotesPhpServers/viewLogs' ));
		}

		$this->setLayoutTitle(Yii::t( "quotesWidget","View Logs"));
		$actionCleanClass = $this->getCleanClassName();
		$this->controller->render("{$actionCleanClass}/view", array(
			'servesLogs' => $this->getServersLogs()
		));
	}
}
