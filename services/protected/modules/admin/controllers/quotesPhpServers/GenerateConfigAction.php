<?php

Yii::import('controllers.base.ActionAdminBase');

final class GenerateConfigAction extends ActionAdminBase
{
    private function getPhpServersPath()
    {
        $quotesSettings = QuotesSettingsModel::getInstance();
        return Yii::getPathOfAlias('webroot') .'/..'. $quotesSettings->phpServersPath;
    }
    private function generateConfig()
    {
        $path = $this->getPhpServersPath() ;
        
        $quotesSettings = QuotesSettingsModel::getInstance();
        
        $QuotesHistoryServerConfig = new stdClass;
        $QuotesHistoryServerConfig->documentRoot = Yii::getPathOfAlias('webroot') .'/..';
        $QuotesHistoryServerConfig->statsUrlData = $quotesSettings->statsUrlData;
        $QuotesHistoryServerConfig->symbolGetQuotesControl = $quotesSettings->symbolGetQuotesControl;
        $QuotesHistoryServerConfig->periodGetQuotesControl = $quotesSettings->periodGetQuotesControl;
        $QuotesHistoryServerConfig->notificationEmails = $quotesSettings->notificationEmails;
        $QuotesHistoryServerConfig->statsDataTimeZone = $quotesSettings->statsDataTimeZone;
        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N', 'histAliases'),
            'select' => " id, name, histName ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`histName` <> '' AND `t`.`sourceType` = 'own' ",
        ));
        $quotesArr = array();
        foreach ($quotes as $quote) {
            $histAliases = array();
            if ($quote->histAliases) {
                foreach ($quote->histAliases as $histAlias) {
                    $histAliases[] = $histAlias->alias;
                }
            }
            $histAliases[] = $quote->histName;
            $quotesArr[] = (object) array('id' => $quote->id, 'name' => $quote->name, 'histAliases' => $histAliases);
        }
        $QuotesHistoryServerConfig->quotes = $quotesArr;

        $QuotesHistoryServerConfig->db = array(
            'username' => Yii::app()->db->username,
            'password' => Yii::app()->db->password,
            'connectionString' => Yii::app()->db->connectionString,
        );
        
        file_put_contents($path.'/QuotesHistoryServerConfig.txt', serialize($QuotesHistoryServerConfig));


        $BitzQuotesHistoryServerConfig = new stdClass;
        $BitzQuotesHistoryServerConfig->documentRoot = Yii::getPathOfAlias('webroot') .'/..';
        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'select' => " id, name, bitzSymbol, convertToUsd ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`bitzSymbol` <> '' AND `t`.`sourceType` = 'bitz' ",
        ));
        $quotesArr = array();
        foreach ($quotes as $quote) {
            $quotesArr[$quote->id] = (object) array(
                'id' => $quote->id,
                'name' => $quote->name,
                'bitzSymbol' => $quote->bitzSymbol,
                'toUsd' => $quote->convertToUsd ? true : false
            );
        }
        $BitzQuotesHistoryServerConfig->quotes = $quotesArr;

        $BitzQuotesHistoryServerConfig->db = array(
            'username' => Yii::app()->db->username,
            'password' => Yii::app()->db->password,
            'connectionString' => Yii::app()->db->connectionString,
        );
        
        file_put_contents($path.'/BitzQuotesHistoryServerConfig.txt', serialize($BitzQuotesHistoryServerConfig));
        
        


        $BitfinexQuotesHistoryServerConfig = new stdClass;
        $BitfinexQuotesHistoryServerConfig->documentRoot = Yii::getPathOfAlias('webroot') .'/..';
        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N', 'histAliases'),
            'select' => " id, name, histName ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`histName` <> '' AND `t`.`sourceType` = 'bitfinex' ",
        ));
        $quotesArr = array();
        foreach ($quotes as $quote) {
            $quotesArr[] = (object) array('id' => $quote->id, 'name' => $quote->name, 'histName' => $quote->histName);
        }
        $BitfinexQuotesHistoryServerConfig->quotes = $quotesArr;

        $BitfinexQuotesHistoryServerConfig->db = array(
            'username' => Yii::app()->db->username,
            'password' => Yii::app()->db->password,
            'connectionString' => Yii::app()->db->connectionString,
        );
        
        file_put_contents($path.'/BitfinexQuotesHistoryServerConfig.txt', serialize($BitfinexQuotesHistoryServerConfig));


        
        
        
        $QuotesServerConfig = new stdClass;
        $QuotesServerConfig->documentRoot = Yii::getPathOfAlias('webroot') .'/..';
        $QuotesServerConfig->realQuotesDataTimeDifference = $quotesSettings->realQuotesDataTimeDifference;
        $QuotesServerConfig->periodGetQuotesControl = $quotesSettings->periodGetQuotesControl;
        $QuotesServerConfig->notificationEmails = $quotesSettings->notificationEmails;
        
        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'select' => " id, name, tickName ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`tickName` <> '' AND `t`.`sourceType` = 'own' ",
        ));
        $quotesArrq = array();
        foreach ($quotes as $quote) {
            $quotesArrq[$quote->tickName] = $quote->name;
        }
        $QuotesServerConfig->quotes = $quotesArrq;
        $QuotesServerConfig->quotesTickSource = $quotesSettings->quotesTickSource;
        
        file_put_contents($path.'/QuotesServerConfig.txt', serialize($QuotesServerConfig));




        $BitzQuotesServerConfig = new stdClass;
        $BitzQuotesServerConfig->documentRoot = Yii::getPathOfAlias('webroot') .'/..';

        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'select' => " id, name, bitzSymbol, convertToUsd ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`bitzSymbol` <> '' AND `t`.`sourceType` = 'bitz' ",
        ));
        $quotesArrq = array();
        foreach ($quotes as $quote) {
            $quotesArrq[$quote->id] = (object) array(
                'id' => $quote->id,
                'name' => $quote->name,
                'bitzSymbol' => $quote->bitzSymbol,
                'toUsd' => $quote->convertToUsd ? true : false,
                'from' => substr($quote->bitzSymbol, 0, strpos($quote->bitzSymbol, '_')),
                'to' => substr($quote->bitzSymbol, strpos($quote->bitzSymbol, '_')+1)
            );
        }
        $BitzQuotesServerConfig->quotes = $quotesArrq;
        
        file_put_contents($path.'/BitzQuotesServerConfig.txt', serialize($BitzQuotesServerConfig));





        $BitfinexQuotesServerConfig = new stdClass;
        $BitfinexQuotesServerConfig->documentRoot = Yii::getPathOfAlias('webroot') .'/..';
        
        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'select' => " id, name, tickName ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`tickName` <> '' AND `t`.`sourceType` = 'bitfinex' ",
        ));
        $quotesArrq = array();
        foreach ($quotes as $quote) {
            $quotesArrq[$quote->tickName] = $quote->name;
        }
        $BitfinexQuotesServerConfig->quotes = $quotesArrq;
        
        file_put_contents($path.'/BitfinexQuotesServerConfig.txt', serialize($BitfinexQuotesServerConfig));




        $OkexQuotesServerConfig = new stdClass;
        $OkexQuotesServerConfig->documentRoot = Yii::getPathOfAlias('webroot') .'/..';
        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N', 'histAliases'),
            'select' => " id, name, okexSymbol ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`okexSymbol` <> '' AND `t`.`sourceType` = 'okex' ",
        ));
        $quotesArr = array();
        foreach ($quotes as $quote) {
            $quotesArr[] = (object) array('id' => $quote->id, 'name' => $quote->name, 'histName' => $quote->okexSymbol);
        }
        $OkexQuotesServerConfig->histQuotes = $quotesArr;

        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'select' => " id, name, okexSymbol ",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`okexSymbol` <> '' AND `t`.`sourceType` = 'okex' ",
        ));
        $quotesArrq = array();
        foreach ($quotes as $quote) {
            $quotesArrq[$quote->okexSymbol] = $quote->name;
        }
        $OkexQuotesServerConfig->tickQuotes = $quotesArrq;

        $OkexQuotesServerConfig->db = array(
            'username' => Yii::app()->db->username,
            'password' => Yii::app()->db->password,
            'connectionString' => Yii::app()->db->connectionString,
        );
        file_put_contents($path.'/OkexQuotesServerConfig.txt', serialize($OkexQuotesServerConfig));







        $BinanceQuotesServerConfig = new stdClass;
        $BinanceQuotesServerConfig->documentRoot = Yii::getPathOfAlias('webroot') .'/..';
        $quotes = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'select' => " id, histName, name, convertToUsd",
            'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`histName` <> '' AND `t`.`histName` IS NOT NULL AND `t`.`sourceType` = 'binance' ",
        ));

        $binanceSymbols = QuotesSymbolsModel::getAvailableBinanceSymbols();
        $binanceSymbolToData = [];
        foreach ($binanceSymbols as $symbol) {
            $binanceSymbolToData[$symbol->symbol] = $symbol;
        }

        $quotesArr = array();
        foreach ($quotes as $quote) {
            if (!empty($binanceSymbolToData[$quote->histName])) {
                $quotesArr[] = (object) array(
                    'id' => $quote->id,
                    'histName' => $quote->histName,
                    'toUsd' => $quote->convertToUsd,
                    'name' => $quote->name,
                    'from' => $binanceSymbolToData[$quote->histName]->baseAsset,
                    'to' => $binanceSymbolToData[$quote->histName]->quoteAsset,
                );
            }
        }
        $BinanceQuotesServerConfig->quotes = $quotesArr;
        $BinanceQuotesServerConfig->statsDataTimeZone = $quotesSettings->statsDataTimeZone;

        $BinanceQuotesServerConfig->db = array(
            'username' => Yii::app()->db->username,
            'password' => Yii::app()->db->password,
            'connectionString' => Yii::app()->db->connectionString,
        );
        file_put_contents($path.'/BinanceQuotesServerConfig.txt', serialize($BinanceQuotesServerConfig));

        
        return array(
            'Own history' => $QuotesHistoryServerConfig,
            'Own tick' => $QuotesServerConfig,
            'Bitz history' => $BitzQuotesHistoryServerConfig,
            'Bitz tick' => $BitzQuotesServerConfig,
            'Bitfinex history' => $BitfinexQuotesHistoryServerConfig,
            'Bitfinex tick' => $BitfinexQuotesServerConfig,
            'Okex' => $OkexQuotesServerConfig,
            'Binance' => $BinanceQuotesServerConfig
        );
    }
    public function run()
    {
        $this->setLayoutTitle(Yii::t("quotesWidget", "Generate config"));
        $actionCleanClass = $this->getCleanClassName();
        $this->controller->render("{$actionCleanClass}/view", array(
            'configs' => $this->generateConfig(),
        ));
    }
}
