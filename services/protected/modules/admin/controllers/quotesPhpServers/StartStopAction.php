<?php
Yii::import('controllers.base.ActionAdminBase');

final class StartStopAction extends ActionAdminBase
{

    private function getPhpServersPath()
    {
        $quotesSettings = QuotesSettingsModel::getInstance();
        return Yii::getPathOfAlias('webroot') .'/..'. $quotesSettings->phpServersPath;
    }
    private function getNameWhoExecuteScript()
    {
        $userInfo = posix_getpwuid(posix_geteuid());
        if (isset($userInfo) && isset($userInfo['name']) && $userInfo['name']) {
            return $userInfo['name'];
        }
        return 'fortrader';
    }
    private function isServerActive($strToFind)
    {
        exec("ps -u ". $this->getNameWhoExecuteScript() ." -o pid,command", $rez);
        
        foreach ($rez as $procInfo) {
            if (strpos($procInfo, $strToFind)) {
                $procInfo = preg_replace('/\s+/', ' ', trim($procInfo));
                $arr = explode(' ', $procInfo);
                return $arr[0];
            }
        }
        return false;
    }
    private function startStop($server, $act, $path)
    {
        $phpPath = Yii::App()->params['consolePhpPath'];

        switch ($server) {
            case 'quotesServer':
                $pid = $this->isServerActive('quotes-server.php');
                if ($act == 'run') {
                    if (!$pid) {
                        exec("nohup {$phpPath} {$path}/quotes-server.php &");
                    }
                } elseif ($act == 'stop') {
                    if ($pid) {
                        exec("kill ".$pid);
                    }
                }
                break;
            case 'quotesHistoryServer':
                $pid = $this->isServerActive('quotes-history-server.php');
                if ($act == 'run') {
                    if (!$pid) {
                        exec("nohup {$phpPath} {$path}/quotes-history-server.php &");
                    }
                } elseif ($act == 'stop') {
                    if ($pid) {
                        exec("kill ".$pid);
                    }
                }
                break;
            case 'bitzQuotesServer':
                $pid = $this->isServerActive('quotes-bitz-server.php');
                if ($act == 'run') {
                    if (!$pid) {
                        exec("nohup {$phpPath} {$path}/quotes-bitz-server.php &");
                    }
                } elseif ($act == 'stop') {
                    if ($pid) {
                        exec("kill ".$pid);
                    }
                }
                break;
            case 'bitzQuotesHistoryServer':
                $pid = $this->isServerActive('quotes-bitz-history-server.php');
                if ($act == 'run') {
                    if (!$pid) {
                        exec("nohup {$phpPath} {$path}/quotes-bitz-history-server.php &");
                    }
                } elseif ($act == 'stop') {
                    if ($pid) {
                        exec("kill ".$pid);
                    }
                }
                break;

            case 'bitfinexQuotesServer':
                $pid = $this->isServerActive('quotes-bitfinex-server.php');
                if ($act == 'run') {
                    if (!$pid) {
                        exec("nohup {$phpPath} {$path}/quotes-bitfinex-server.php &");
                    }
                } elseif ($act == 'stop') {
                    if ($pid) {
                        exec("kill ".$pid);
                    }
                }
                break;
            case 'bitfinexQuotesHistoryServer':
                $pid = $this->isServerActive('quotes-bitfinex-history-server.php');
                if ($act == 'run') {
                    if (!$pid) {
                        exec("nohup {$phpPath} {$path}/quotes-bitfinex-history-server.php &");
                    }
                } elseif ($act == 'stop') {
                    if ($pid) {
                        exec("kill ".$pid);
                    }
                }
                break;

            case 'okex':
                $pid = $this->isServerActive('quotes-okex.php');
                if ($act == 'run') {
                    if (!$pid) {
                        exec("nohup {$phpPath} {$path}/quotes-okex.php &");
                    }
                } elseif ($act == 'stop') {
                    if ($pid) {
                        exec("kill ".$pid);
                    }
                }
                break;

            case 'binance':
                $pid = $this->isServerActive('quotes-binance.php');
                if ($act == 'run') {
                    if (!$pid) {
                        exec("nohup {$phpPath} {$path}/quotes-binance.php &");
                    }
                } elseif ($act == 'stop') {
                    if ($pid) {
                        exec("kill ".$pid);
                    }
                }
                break;

            case 'pushServer':
                $pid = $this->isServerActive('push-server.php');
                if ($act == 'run') {
                    if (!$pid) {
                        exec("nohup {$phpPath} {$path}/push-server.php &");
                    }
                } elseif ($act == 'stop') {
                    if ($pid) {
                        exec("kill ".$pid);
                    }
                }
                break;
        }
    }
    private function getResourses($pid)
    {
        if ($pid) {
            exec("ps -p $pid -o %cpu,%mem,rss,command", $rez);
            $rez[1] = preg_replace('/\s+/', ' ', trim($rez[1]));
            $arr = explode(' ', $rez[1]);
            return '%CPU ' . $arr[0] . ' %MEM ' . $arr[1] . ' RSS ' . number_format($arr[2]/1024, 2, ".", ' ') . ' mb<br />PID '.$pid . '<br />CMD ' . $arr[3] . ' '. $arr[4];
        }
        return false;
    }
    private function getPushServerSubscribersCount()
    {
        $path = $this->getPhpServersPath();
        $pushServerApplicationFileName = $path . '/log/push-server-application.log';
        if (!file_exists($pushServerApplicationFileName)) {
            return false;
        }
        
        $fp = fopen($pushServerApplicationFileName, 'r');
    
        fseek($fp, -2, SEEK_END); 
        $pos = ftell($fp);
        $LastLine = "";

        while ((($C = fgetc($fp)) != "\n") && ($pos > 0)) {
            $LastLine = $C.$LastLine;
            fseek($fp, $pos--);
        }
        fclose($fp);
        
        $LastLine = trim(substr($LastLine, 0, strlen($LastLine)-1));
        if (!$LastLine) {
            return false;
        }
        
        $exploded = explode(' ', $LastLine);
        if (!$exploded) {
            return false;
        }
        $count = $exploded[count($exploded)-1];
        if (!$count) {
            return false;
        }
        return $count;
    }

    public function run()
    {
        $path = $this->getPhpServersPath();
        
        if (isset($_GET['server']) && isset($_GET['act'])) {
            $this->startStop($_GET['server'], $_GET['act'], $path);
            $this->redirect(array( 'quotesPhpServers/startStop' ));
        }
        
        $quotesServerPid = $this->isServerActive('quotes-server.php');
        $quotesHistoryServerPid = $this->isServerActive('quotes-history-server.php');
        $pushServerPid = $this->isServerActive('push-server.php');

        $bitzQuotesServerPid = $this->isServerActive('quotes-bitz-server.php');
        $bitzQuotesHistoryServerPid = $this->isServerActive('quotes-bitz-history-server.php');

        $bitfinexQuotesServerPid = $this->isServerActive('quotes-bitfinex-server.php');
        $bitfinexQuotesHistoryServerPid = $this->isServerActive('quotes-bitfinex-history-server.php');

        $okexPid = $this->isServerActive('quotes-okex.php');
        $binancePid = $this->isServerActive('quotes-binance.php');
        
        
        $this->setLayoutTitle(Yii::t("quotesWidget", "Stop/Start"));
        $actionCleanClass = $this->getCleanClassName();
        $this->controller->render("{$actionCleanClass}/view", array(
            'quotesServer' => $quotesServerPid,
            'quotesServerRes' => $this->getResourses($quotesServerPid),
            'quotesHistoryServer' => $quotesHistoryServerPid,
            'quotesHistoryServerRes' => $this->getResourses($quotesHistoryServerPid),
            'pushServer' => $pushServerPid,
            'pushServerRes' => $this->getResourses($pushServerPid),
            'pushServerSubscribersCount' => $this->getPushServerSubscribersCount(),

            'bitzQuotesServer' => $bitzQuotesServerPid,
            'bitzQuotesHistoryServer' => $bitzQuotesHistoryServerPid,
            'bitzQuotesServerRes' => $this->getResourses($bitzQuotesServerPid),
            'bitzQuotesHistoryServerRes' => $this->getResourses($bitzQuotesHistoryServerPid),

            'bitfinexQuotesServer' => $bitfinexQuotesServerPid,
            'bitfinexQuotesHistoryServer' => $bitfinexQuotesHistoryServerPid,
            'bitfinexQuotesServerRes' => $this->getResourses($bitfinexQuotesServerPid),
            'bitfinexQuotesHistoryServerRes' => $this->getResourses($bitfinexQuotesHistoryServerPid),

            'okexPid' => $okexPid,
            'okexRes' => $this->getResourses($okexPid),

            'binancePid' => $binancePid,
            'binanceRes' => $this->getResourses($binancePid),
        ));
    }
}
