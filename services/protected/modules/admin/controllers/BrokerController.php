<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class BrokerController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Brokers";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'brokerControl' )),
				Array( 'deny' ),
			);
		}
	}

?>