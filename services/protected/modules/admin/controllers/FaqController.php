<?php

Yii::import('controllers.base.ControllerAdminBase');

final class FaqController extends ControllerAdminBase
{
    public function detLayoutTitle()
    {
        return "Faq";
    }

    public function accessRules()
    {
        return array(
            array( 'allow', 'roles' => array( 'faqControl' )),
            array( 'deny' ),
        );
    }
}
