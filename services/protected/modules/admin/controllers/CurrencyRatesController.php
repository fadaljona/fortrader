<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class CurrencyRatesController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Currency Rates";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'currencyRatesControl' )),
				Array( 'deny' ),
			);
		}
	}

?>