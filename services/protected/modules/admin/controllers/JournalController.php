<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class JournalController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Journals";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'journalControl' )),
				Array( 'deny' ),
			);
		}
	}

?>