<?
	
	final class StrategyInstrumentController extends AdminControllerExBase {
		function detLayoutTitle() {
			return "Strategy instruments";
		}
		function detLayoutInfo() {
			return "On this page you can manage strategy instruments.";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'strategyControl' )),
				Array( 'deny' ),
			);
		}
	}

?>