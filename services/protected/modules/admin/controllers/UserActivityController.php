<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UserActivityController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Activity';
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'userActivityControl' )),
				Array( 'deny' ),
			);
		}
	}

?>