<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.AdminRssFeedsSettingsFormModel' );

	final class SettingsAction extends ActionAdminBase {
		private $formModel;
		private function detFormModel() {
			$this->formModel = new AdminRssFeedsSettingsFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't load model" );
		}
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Settings" );
			$this->setLayout( "rssFeeds/Settings" );
			$this->detFormModel();
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'formModel' => $this->formModel
			));
		}
	}

?>