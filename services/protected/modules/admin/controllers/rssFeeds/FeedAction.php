<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class FeedAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Feed" );
			$this->setLayout( "rssFeeds/feed" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>