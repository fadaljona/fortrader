<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class GroupsAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Groups" );
			$this->setLayout( "rssFeeds/groups" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>