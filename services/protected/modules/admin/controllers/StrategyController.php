<?
	
	final class StrategyController extends AdminControllerExBase {
		function detLayoutTitle() {
			return "Strategies";
		}
		function detLayoutInfo() {
			return "On this page you can manage trade strategy.";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'strategyControl' )),
				Array( 'deny' ),
			);
		}
	}
		
?>