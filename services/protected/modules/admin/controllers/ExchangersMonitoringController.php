<?php

Yii::import( 'controllers.base.ControllerAdminBase' );

final class ExchangersMonitoringController extends ControllerAdminBase
{
    function detLayoutTitle()
    {
        return "Monitoring of exchangers";
    }
    function actionIndex()
    {
        $this->redirect(array( 'rating' ));
    }
    function accessRules()
    {
        return array(
            array( 'allow', 'roles' => array( 'exchangersMonitoringControl' )),
            array( 'deny' ),
        );
    }
}

?>