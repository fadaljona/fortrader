<?php

Yii::import('controllers.base.ControllerAdminBase');

class QuotesAliasController extends ControllerAdminBase{
	function detLayoutTitle(){
		return Yii::t( "quotesWidget","Алиасы котировок");
	}

	public function actionIndex(){
		$this->redirect(Array('list'));
	}

	public function actionSuggest(){
		if (Yii::app()->request->isAjaxRequest && isset($_GET['term'])) {
			$models = QuotesSymbolsModel::model()->findAll(array(
				'condition'=>'name LIKE :keyword OR id LIKE :keyword',
				'params'=>array(
					':keyword'=>'%'.$_GET['term'].'%',
				)
			));
			$result = array();
			foreach ($models as $m)
				$result[] = array(
					'label' => $m->name." (".$m->id.")",
					'id' => $m->id
				);

			echo json_encode($result);
		}
	}

	function accessRules(){
		return Array(Array('allow','roles' => Array('quotesControl')),Array('deny'),);
	}

	public function loadModel($id){
		$model = QuotesCategoryModel::model()->findByPk($id);
		if($model === null) throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
