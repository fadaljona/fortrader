<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class BrokerInstrumentController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Brokers instruments";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'brokerControl' )),
				Array( 'deny' ),
			);
		}
	}

?>