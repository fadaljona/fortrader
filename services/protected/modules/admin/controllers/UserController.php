<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UserController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Users";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'userControl' )),
				Array( 'deny' ),
			);
		}
	}

?>