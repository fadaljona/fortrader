<?php

Yii::import('controllers.base.ActionAdminBase');

final class CategoriesAction extends ActionAdminBase
{
    public function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Faq categories");
        $this->setLayout("faq/categories");

        $this->controller->render("{$actionCleanClass}/view", array());
    }
}
