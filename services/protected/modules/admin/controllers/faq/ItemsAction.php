<?php
Yii::import('controllers.base.ActionAdminBase');

final class ItemsAction extends ActionAdminBase
{
    public function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Faq Items");
        $this->setLayout("faq/items");

        $this->controller->render("{$actionCleanClass}/view", array());
    }
}
