<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class ManageTextContentController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Manage Text Content";
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'textContentControl' )),
				Array( 'deny' ),
			);
		}
	}

?>