<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		const KEYStateFilterName = 'admin.tradeAccount.list.filter.name';
		const KEYStateFilterAccountNumber = 'admin.tradeAccount.list.filter.accountNumber';
		const KEYStateFilterGroups = 'admin.tradeAccount.list.filter.groups';
		private $filterModel;
		private function getFilterURL() {
			return $this->controller->createUrl( 'list' );
		}
		private function detFilterModel() {
			$this->filterModel = new TradeAccountModel();
			$post = &$_POST[ get_class( $this->filterModel )];
			if( !empty( $post )) {
				Yii::App()->user->setState( self::KEYStateFilterName, $post[ 'name' ]);
				Yii::App()->user->setState( self::KEYStateFilterAccountNumber, $post[ 'accountNumber' ]);
				Yii::App()->user->setState( self::KEYStateFilterGroups, $post[ 'groups' ]);
			}
			$this->filterModel->name = Yii::App()->user->getState( self::KEYStateFilterName );
			$this->filterModel->accountNumber = Yii::App()->user->getState( self::KEYStateFilterAccountNumber );
			$this->filterModel->groups = Yii::App()->user->getState( self::KEYStateFilterGroups );
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			//$controllerCleanClass = $this->controller->getCleanClassName();
			$actionCleanClass = $this->getCleanClassName();
			
			$this->detFilterModel();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "default/index" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>