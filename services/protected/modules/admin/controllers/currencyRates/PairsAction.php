<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class PairsAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Pairs" );
			$this->setLayout( "currencyRates/pairs" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>