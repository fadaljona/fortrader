<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class ListAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "List" );
			$this->setLayout( "currencyRates/list" );
						
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>