<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadOptionRowAction extends ActionAdminBase {
		private function getDP( $id ) {
			if( !isset( $_GET['modelName'] ) || !$_GET['modelName'] || !class_exists( $_GET['modelName'] ) ) $this->throwI18NException( "Can't find model!" );
			$modelName = $_GET['modelName'];
			$DP = new CActiveDataProvider( $modelName, Array(
				'criteria' => Array(
					'condition' => '`t`.`id` = :id',
					'params' => Array(
						':id' => $id,
					),
				),
			));
			if( !$DP->getTotalItemCount( )) $this->throwI18NException( "Can't find Instrument!" );
			return $DP;
		}
		private function getWidget( $DP ) {
			$widget = $this->controller->createWidget( 'widgets.gridViews.AdminCurrencyRatesGridViewWidget', Array(
				'dataProvider' => $DP,
			));
			return $widget;
		}
		private function renderRow( $widget ) {
			ob_start();
			$widget->renderTableRow( 0 );
			return ob_get_clean();
		}
		function run() {
			$data = Array();
			try{
				$id = (int)@$_GET[ 'id' ];
				$DP = $this->getDP( $id );
				$widget = $this->getWidget( $DP );
				
				$data[ 'row' ] = $this->renderRow( $widget );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>