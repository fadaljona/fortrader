<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	Yii::import( 'models.forms.*' );
	
	final class AjaxOptionAddAction extends ActionAdminBase {
		private $formModel;
		private $formModelName;
		private function detFormModel() {
			$this->formModel = new $this->formModelName;
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Instrument!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$formModelName = key($_POST);
			if( !class_exists( $formModelName ) ) $this->throwI18NException( "Can't find model!" );
			$this->formModelName = $formModelName;
			
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>