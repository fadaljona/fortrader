<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UnsubscribeCommentController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Unsubscribe comments';
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'mailingControl' )),
				Array( 'deny' ),
			);
		}
	}

?>