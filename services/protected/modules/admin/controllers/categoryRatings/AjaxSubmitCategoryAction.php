<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxSubmitCategoryAction extends ActionAdminBase {
		private $formModel;
		private $formModelName;
		
		private function detFormModel() {
			$formModelName = $this->formModelName;
			$this->formModel = new $formModelName;
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find Model!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		function run() {
			$post = $_POST;
			reset($post);
			$formModelName = key($post);
			if( !$formModelName ) return false;
			$this->formModelName = $formModelName;
			Yii::import( 'models.forms.' . $this->formModelName );
			
			$actionCleanClass = $this->getCleanClassName();
			$this->setLayout( "//layouts/cleanPage" );
			
			$data = Array();
			try{
				$this->detFormModel();
				$formModel = $this->getFormModel();
				if( $formModel->validate()) {
					$id = $formModel->save();
					if( !$id ) $this->throwI18NException( "Can't save AR!" );
					$data[ 'id' ] = $id;
				}
				else{
					list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
				}
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'data' => $data,
			));
		}
	}

?>