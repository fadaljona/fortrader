<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class RatingListAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Items of the rating" );
			$this->setLayout( "categoryRatings/ratingList" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'ratings' => CategoryRatingModel::getRatingsForSelect()
			));
		}
	}

?>