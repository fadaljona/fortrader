<?
	Yii::import( 'controllers.base.ActionAdminBase' );
	
	final class AjaxLoadPostListRowAction extends ActionAdminBase {
		private function getDP( $id ) {
			
			$DP = new CActiveDataProvider( 'WPTermRelationshipModel', Array(
				'criteria' => Array(
					'with' => Array( 'termTaxonomy', 'categoryRatingPost' ),
					'condition' => " `termTaxonomy`.`taxonomy` = 'category' AND `categoryRatingPost`.`ID` = :id ",
					'group' => " `categoryRatingPost`.`ID` ",
					'params' => Array(
						':id' => $id,
					),
				),
			));
			
			if( !$DP->getTotalItemCount( )) $this->throwI18NException( "Can't find model!" );
			return $DP;
		}
		private function getWidget( $DP ) {
			$gridViewWidget = @$_GET['gridViewWidget'];
			$widget = $this->controller->createWidget( 'widgets.gridViews.'.$gridViewWidget, Array(
				'dataProvider' => $DP,
			));
			return $widget;
		}
		private function renderRow( $widget ) {
			ob_start();
			$widget->renderTableRow( 0 );
			return ob_get_clean();
		}
		function run() {
			if( !@$_GET['modelName'] ) return false;
			if( !@$_GET['gridViewWidget'] ) return false;
			$data = Array();
			try{
				$id = (int)@$_GET[ 'id' ];
				$DP = $this->getDP( $id );
				$widget = $this->getWidget( $DP );
				
				$data[ 'row' ] = $this->renderRow( $widget );
			}
			catch( Exception $e ) {
				$data[ 'error' ] = $e->getMessage();
			}
			echo json_encode( $data );
		}
	}

?>