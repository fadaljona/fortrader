<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class RatingAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Category ratings" );
			$this->setLayout( "categoryRatings/rating" );
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>