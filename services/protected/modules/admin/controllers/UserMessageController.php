<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UserMessageController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Private messages';
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'userMessageControl' )),
				Array( 'deny' ),
			);
		}
	}

?>