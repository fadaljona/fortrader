<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class IndexAction extends ActionAdminBase {
		function run() {
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "Manage Text Content" );
			
			$this->controller->render( "{$actionCleanClass}/view", Array(
				
			));
		}
	}

?>