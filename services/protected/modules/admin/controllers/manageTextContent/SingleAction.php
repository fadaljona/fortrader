<?
	Yii::import( 'controllers.base.ActionAdminBase' );

	final class SingleAction extends ActionAdminBase {
		private $modelName;
		private $modelFormName;
		
		private function setSettings(){
			$settings = ManageTextContentLib::getItemSettings();
			if( !$settings ) throw new Exception( "Can't find itemSettings" );
			if( !isset($settings['model']) || !$settings['model'] || !isset($settings['modelForm']) || !$settings['modelForm'] ) throw new Exception( "Can't find itemSettings" );
			
			$this->modelName = $settings['model'];
			$this->modelFormName = $settings['modelForm'];
		}
		
		function run() {
			if( !isset($_GET['slug']) || !$_GET['slug'] ) $this->redirect( array('manageTextContent/index'), true, 301 );
			
			$actionCleanClass = $this->getCleanClassName();
			
			$this->setLayoutTitle( "" );
			
			$this->controller->layout = '/_layouts/manageTextContent';
			
			$type = ManageTextContentLib::getType();
			if( !$type ) $this->throwI18NException( "Can't type!" );
			
			$this->setSettings();
				
			$this->controller->render( "{$actionCleanClass}/view", Array(
				'type' => $type,
				'modelName' => $this->modelName,
				'modelFormName' => $this->modelFormName
			));
		}
	}

?>