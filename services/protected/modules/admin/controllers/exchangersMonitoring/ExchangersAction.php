<?php

Yii::import( 'controllers.base.ActionAdminBase' );

final class ExchangersAction extends ActionAdminBase
{
    function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Exchangers");
        $this->setLayout("exchangersMonitoring/exchangers");

        $this->controller->render("{$actionCleanClass}/view", array());
    }
}
