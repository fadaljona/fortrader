<?php

Yii::import( 'controllers.base.ActionAdminBase' );

final class ExchangeDirectionsAction extends ActionAdminBase
{
    function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Exchange Directions");
        $this->setLayout("exchangersMonitoring/exchangeDirections");

        $this->controller->render("{$actionCleanClass}/view", array());
    }
}
