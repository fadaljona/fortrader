<?php
Yii::import('controllers.base.ActionAdminBase');
Yii::import('models.forms.AdminPaymentExchangerSettingsFormModel');

final class SettingsAction extends ActionAdminBase
{
    private $formModel;
    private function detFormModel()
    {
        $this->formModel = new AdminPaymentExchangerSettingsFormModel();
        $load = $this->formModel->load();
        if (!$load) {
            $this->throwI18NException("Can't load model");
        }
    }
    public function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Settings");
        $this->setLayout("exchangersMonitoring/settings");
        $this->detFormModel();
        $this->controller->render("{$actionCleanClass}/view", array(
            'formModel' => $this->formModel
        ));
    }
}
