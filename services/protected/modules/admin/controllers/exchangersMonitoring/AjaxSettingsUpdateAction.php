<?php
Yii::import('controllers.base.ActionAdminBase');
Yii::import('models.forms.AdminPaymentExchangerSettingsFormModel');

final class AjaxSettingsUpdateAction extends ActionAdminBase
{
    private $formModel;
    private function detFormModel()
    {
        $this->formModel = new AdminPaymentExchangerSettingsFormModel();
        $load = $this->formModel->load();
        if (!$load) {
            $this->throwI18NException(Yii::t("*", "Can't load model"));
        }
    }
    private function getFormModel()
    {
        return $this->formModel;
    }
    public function run()
    {
        $data = array();
        try {
            $this->detFormModel();
            $formModel = $this->getFormModel();
            if ($formModel->validate()) {
                $id = $formModel->save();
                if (!$id) {
                    $this->throwI18NException(Yii::t("*", "Can't save model"));
                }
                $data[ 'id' ] = $id;
            } else {
                list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
            }
        } catch (Exception $e) {
            $data[ 'error' ] = $e->getMessage();
        }
        echo json_encode($data);
    }
}
