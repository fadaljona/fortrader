<?php

Yii::import( 'controllers.base.ActionAdminBase' );

final class PaymentSystemsAction extends ActionAdminBase
{
    function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Payment Systems");
        $this->setLayout("exchangersMonitoring/paymentSystems");

        $this->controller->render("{$actionCleanClass}/view", array());
    }
}
