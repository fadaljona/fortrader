<?php

Yii::import( 'controllers.base.ActionAdminBase' );

final class PaymentSystemCurrenciesAction extends ActionAdminBase
{
    function run()
    {
        $actionCleanClass = $this->getCleanClassName();

        $this->setLayoutTitle("Currencies of payment system");
        $this->setLayout("exchangersMonitoring/paymentSystemCurrencies");

        $this->controller->render("{$actionCleanClass}/view", array());
    }
}
