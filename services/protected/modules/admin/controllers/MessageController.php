<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class MessageController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Messages';
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'languageControl' )),
				Array( 'deny' ),
			);
		}
	}

?>