<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class MailTplController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Mails templates';
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'languageControl' )),
				Array( 'deny' ),
			);
		}
	}

?>