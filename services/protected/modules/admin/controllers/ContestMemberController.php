<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class ContestMemberController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Contests members";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'contestMemberControl' )),
				array(
					'allow',
					'actions' => array('ajaxDelete'),
					'expression' => array('ContestMemberController','allowOnlyOwner'),
					'users'=>array('*')
				),
				Array( 'deny' ),
			);
		}
		public function allowOnlyOwner(){
			if( Yii::App()->user->checkAccess( 'contestMemberControl' ) ) return true;
			$currUserId = Yii::App()->user->id;
			if( !$currUserId ) return false;
			
			if( !isset( $_GET['id'] ) || !$_GET['id'] ) return false;
			
			$member = ContestMemberModel::model()->findByPk($_GET['id']);
			
			if( !$member ) return false;
			if( $member->idUser != $currUserId ) return false;
			
			return true;
		}
	}
?>