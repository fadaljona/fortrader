<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class UserNoticeController extends ControllerAdminBase {
		function detLayoutTitle() {
			return 'Notices';
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'userNoticeControl' )),
				Array( 'deny' ),
			);
		}
	}

?>