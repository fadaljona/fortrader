<?
	Yii::import( 'controllers.base.ControllerAdminBase' );

	final class CompanyController extends ControllerAdminBase {
		function detLayoutTitle() {
			return "Companies";
		}
		function actionIndex() {
			$this->redirect( Array( 'list' ));
		}
		function accessRules() {
			return Array(
				Array( 'allow', 'roles' => Array( 'companyControl' )),
				Array( 'deny' ),
			);
		}
	}

?>