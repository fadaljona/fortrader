<?php
	class ParserHTML{
		private static function clearHTML( $content ) {
			$content = preg_replace( '#<!DOCTYPE.*>#isU', '', $content );
			$content = preg_replace( '#<script.*>.*</script>#isU', '', $content );
			$content = preg_replace( '#<noscript.*>.*</noscript>#isU', '', $content );
			$content = preg_replace( '#<style.*>.*</style>#isU', '', $content );
			$content = preg_replace( '#<meta.*>#isU', '', $content );
			$content = preg_replace( '#<link.*>#isU', '', $content );
			$content = preg_replace( '#</?noindex.*>#isU', '', $content );
			$content = preg_replace( '#<!--.*-->#isU', '', $content );
			$content = preg_replace( '#\r#', '', $content );
			$content = preg_replace( '#^[ \t]+$#m', '', $content );
			$content = preg_replace( '#\n{2,}#', "\n", $content );
			$content = preg_replace( '#^\n|\n$#', '', $content );
			return $content;
		}
		private static function createTag( $content ) {
			$tag = new stdClass();
			$tag->closeLeft = (bool)preg_match( '#^/#', $content );
			$tag->closeRight = (bool)preg_match( '#/$#', $content );
			$content = preg_replace( '#^/|/$#', '', $content );
			$tag->name = preg_match( '#^(\w+)( |$)#', $content, $match ) ? $match[1] : '';
			$tag->name = strtolower( $tag->name );
			$tag->correct = preg_match( "#^\w+$#", $tag->name );
			if( $tag->correct ) {
				$content = preg_replace( "#^{$tag->name}#", '', $content );
				if( preg_match( '#br|img#i', $tag->name )) $tag->closeRight = true;
				$patternAttributes = '#([a-z:-]+)(?:\s*=\s*(?:"([^"]*)"|\'([^\']*)\'|(\S+)[ |$]))?#i';
				$attributesMatchs = preg_match_all( $patternAttributes, $content, $matchs ) ? $matchs : false;
				$tag->attributes = Array();
				if( $attributesMatchs ) {
					for( $i=0; $i<count( $attributesMatchs[0] ); $i++ ) {
						if( strpos( $attributesMatchs[1][$i], ":" ) !== false ) continue;
						if( preg_match( '#^xmlns$#', $attributesMatchs[1][$i] )) continue;
						if( strlen( $attributesMatchs[4][$i])) $value = $attributesMatchs[4][$i];
						elseif( strlen( $attributesMatchs[3][$i])) $value = $attributesMatchs[3][$i];
						else $value = $attributesMatchs[2][$i];
						if( !preg_match( '#^[a-z:-]+\s*=#', $attributesMatchs[0][$i])) $value = $attributesMatchs[1][$i];
						$tag->attributes[$attributesMatchs[1][$i]] = $value;
					}
				}
			}
			return $tag;
		}
		private static function createNode( $tag, $XML, $encoding ) {
			$node = $XML->createElement( $tag->name );
			foreach( $tag->attributes as $name=>$value ) {
				if( $encoding != "UTF-8" ) $value=iconv( $encoding, "UTF-8", $value );
				$node->setAttribute( $name, $value );
			}
			return $node;
		}
		private static function findClosestNode( $node, $name ) {
			do {
				if ( @$node->tagName == $name ) return $node;
				$node = $node->parentNode;
			} while( $node );
			return false;
		}
		static function createXML( $encoding, $content ) {
			$content = self::clearHTML( $content );
			$XML = new DOMDocument( "1.0", "UTF-8" );
			$nodeCurrent = $XML;
			$posCurrent = $posCurrentFind = 0;
			while( $posCurrent < strlen( $content )) {
				$posLT = strpos( $content, '<', $posCurrentFind );
				if ( $posLT !== false ) {
					$posGT = strpos( $content, '>', $posLT );
					if ( $posGT !== false ) {
						$posCurrentFind = $posGT+1;
						$contentTag = substr( $content, $posLT+1, $posGT-$posLT-1 );
						$tag = self::createTag( $contentTag );
						if( $tag->correct ) {
							if ( $posLT>$posCurrent ) {
								$text = substr( $content, $posCurrent, $posLT-$posCurrent );
								if( $encoding != "UTF-8" ) $text=iconv( $encoding, "UTF-8", $text );
								$nodeCurrent->appendChild( $XML->createTextNode( $text ));
							}
							if( !$tag->closeLeft ) {
								$node = self::createNode( $tag, $XML, $encoding );
								$nodeCurrent->appendChild( $node );
								if( !$tag->closeRight )	$nodeCurrent = $node;
								$posCurrent = $posCurrentFind;
							}
							else {
								$nodeCurrentNew = self::findClosestNode( $nodeCurrent, $tag->name );
								if( $nodeCurrentNew ) {
									$nodeCurrent = $nodeCurrentNew->parentNode;
									$posCurrent = $posCurrentFind;
								}
							}
						}
					}
					else break 1;
				}
				else break 1;
			}
			return $XML;
		}
	}
?>