<?

	final class ClickatellXMLAPI {
		const URLSend = 'http://api.clickatell.com/xml/xml';
		const version = '2.4.1';
		const maxMsgLen = 459;
		const maxUnicodeMsgLen = 198;
		const maxMsgSingleLen = 160;
		const maxUnicodeMsgSingleLen = 70;
		const maxMsgPartleLen = 153;
		const maxUnicodeMsgPartLen = 63;
		protected $curl;
		protected $session_id;
		public $UTF8 = false;
		public $debugRequest = false;
		public $debugResponse = false;
		function __construct()  {
			$this->curl = curl_init();
			curl_setopt( $this->curl, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $this->curl, CURLOPT_SSL_VERIFYHOST, 0 );
			curl_setopt( $this->curl, CURLOPT_SSL_VERIFYPEER, 0 );
			curl_setopt( $this->curl, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $this->curl, CURLOPT_POST, true );
			curl_setopt( $this->curl, CURLOPT_URL, self::URLSend );
		}
		protected function post( $XML ) {
			$data = $XML->saveXML();
			curl_setopt( $this->curl, CURLOPT_POSTFIELDS, http_build_query( Array( 'data' => $data )));
			$response = curl_exec( $this->curl );
			
			$XMLResponse = new ClickatellXML();
			if( !@$XMLResponse->loadXML( $response )) {
				if( $this->debugResponse ) { 
					echo $response; exit;
				}
				throw new Exception( 'Parse response XML error!' );
			}
			return $XMLResponse;
		}
		function multiActions( $array ) {
			if( strlen( $this->session_id )) foreach( $array as &$assoc ) $assoc[ 'params' ][ 'session_id' ] = $this->session_id;
			$XMLRequest = ClickatellXML::instanceFromActions( $array );
			if( $this->debugRequest ) { echo $XMLRequest->saveXML(); exit; }
			$XMLResponse = $this->post( $XMLRequest );
			if( $this->debugResponse ) { echo $XMLResponse->saveXML(); exit; }
			$XMLResponse->throwError();
			return $XMLResponse->getResponses();
		}
		function action( $action, $params = Array() ) {
			$responses = $this->multiActions( Array(
				Array( 'action' => $action, 'params' => $params ),
			));
			return @$responses[0]->params;
		}
		function ping( $session_id ) {
			if( !strlen( $session_id )) return false;
			$response = $this->action( __FUNCTION__, compact( 'session_id' ));
			if( @$response->ok ) {
				$this->session_id = $session_id;
				return true;
			}
			return false;
		}
		function auth( $api_id, $user, $password ) {
			$response = $this->action( __FUNCTION__, compact( 'api_id', 'user', 'password' ));
			if( @$response->session_id ) {
				$this->session_id = $response->session_id;
				return $response->session_id;
			}
			throw new Exception( 'Authentication failed!' );
		}
		function pingOrAuth( $session_id, $api_id, $user, $password ) {
			$this->ping( $session_id ) or ( $session_id = $this->auth( $api_id, $user, $password ));
			return $session_id;
		}
		function getBalance() {
			$response = $this->action( __FUNCTION__ );
			return @$response->ok;
		}
		function sendMsg( $to, $text, $from = null ) {
			$params = compact( 'to' );
			if( strlen( $from )) $params[ 'from' ] = $from;
			$text = $this->truncateMsg( $text );
			$partsCount = $this->getMsgPartsCount( $text );
			if( $partsCount>1 ) $params[ 'concat' ] = $partsCount;
			if( $this->UTF8 and self::needConvertUTF8ToUnicode( $text )) {
				$params[ 'unicode' ] = "1";
				$text = self::UTF8ToUnicodeHex( $text );
			}
			$params[ 'text' ] = $text;
			$response = $this->action( __FUNCTION__, $params );
			if( @$response->fault ) throw new Exception( $response->fault );
			return @$response->apiMsgId;
		}
		function queryMsg( $apiMsgId ) {
			$response = $this->action( __FUNCTION__, compact( 'apiMsgId' ));
			if( @$response->fault ) throw new Exception( $response->fault );
			return @$response->status;
		}
		private function getMsgPartsCount( $text ) {
			$len = $this->UTF8 ? mb_strlen( $text, "UTF-8" ) : strlen( $text );
			$maxSingleLen = $this->UTF8 && self::needConvertUTF8ToUnicode( $text ) ? self::maxUnicodeMsgSingleLen : self::maxMsgSingleLen;
			$maxPartLen = $this->UTF8 && self::needConvertUTF8ToUnicode( $text ) ? self::maxUnicodeMsgPartLen : self::maxMsgPartleLen;
			return $len > $maxSingleLen ? ceil( $len / $maxPartLen ) : 1;
		}
		function checkMsgLen( $text ) {
			$len = $this->UTF8 ? mb_strlen( $text, "UTF-8" ) : strlen( $text );
			return $this->UTF8 && self::needConvertUTF8ToUnicode( $text ) ? $len <= self::maxUnicodeMsgLen : $len <= self::maxMsgLen;
		}
		function truncateMsg( $text ) {
			if( $this->UTF8 ) {
				$maxLen = self::needConvertUTF8ToUnicode( $text ) ? self::maxUnicodeMsgLen : self::maxMsgLen;
				return mb_substr( $text, 0, $maxLen, "UTF-8" );
			}
			else{
				return substr( $text, 0, self::maxMsgLen );
			}
		}
		static function needConvertUTF8ToUnicode( $text ) {
			return strlen( $text ) != mb_strlen( $text, "utf-8" );
		}
		static function utf8ToUnicodeHex( $text ) {
			$text = iconv( "UTF-8", "UTF-16", $text ); # convert to UTF-16
			$text = bin2hex( $text ); # convert to HEX
			if( preg_match( "#^FFFE#i", $text )) { # wrong BO
				$text = preg_replace( "#(..)(..)#", '$2$1', $text ); # convert wrong BO
			}
			$text = preg_replace( "#^FEFF#i", "", $text ); # delete BOM
			return $text;
		}
	}
	
	class ClickatellXML extends DOMDocument{
		const RootElementName = 'clickAPI';
		function __construct( $version = '1.0' , $encoding = 'UTF-8' ) {
			return parent::__construct( $version, $encoding );
		}
		static function instanceFromActions( $array ) {
			$XML = new self();
			$nodeRoot = $XML->createRootElement();
			foreach( $array as $assoc ) {
				$nodeAction = $nodeRoot->appendChild( $XML->createElement( $assoc[ 'action' ]));
				if( @$assoc['params'] ) {
					foreach( $assoc['params'] as $key=>$value ) {
						$value = self::encode( $value );
						$nodeAction->appendChild( $XML->createElement( $key, $value ));		
					}
				}
			}
			return $XML;
		}
		function createRootElement() {
			return $this->appendChild( $this->createElement( self::RootElementName ));
		}
		function getRootElement() {
			$XPath = new DOMXPath( $this );
			$entriesRoot = $XPath->query( '/'.self::RootElementName );
			if( !$entriesRoot->length ) throw new Exception( 'Parse response XML error!' );
			return $entriesRoot->item(0);
		}
		function getResponses() {
			$responses = Array();
			$XPath = new DOMXPath( $this );
			$nodeRoot = $this->getRootElement();
			$entriesResp = $XPath->query( '*', $nodeRoot );
			foreach( $entriesResp as $nodeResp ) {
				$response = new StdClass();
				$response->action = $nodeResp->nodeName;
				$response->params = new StdClass();
				foreach( $nodeResp->childNodes as $childNode ) {
					if( $childNode->nodeType != XML_ELEMENT_NODE ) continue;
					$response->params->{$childNode->nodeName} = $childNode->nodeValue;
				}
				$responses[] = $response;
			}
			return $responses;
		}
		function throwError() {
			$XPath = new DOMXPath( $this );
			$nodeRoot = $this->getRootElement();
			$entriesXMLErrorResp = $XPath->query( 'xmlErrorResp', $nodeRoot );
			if( !$entriesXMLErrorResp->length ) return false;
			
			$nodeXMLErrorResp = $entriesXMLErrorResp->item(0);
			$fault = $XPath->evaluate( 'string(fault)', $nodeXMLErrorResp );
			if( strlen( $fault )) throw new Exception( $fault );
		}
		static function encode( $value ) {
			return str_replace( '&', '&amp;', $value );
		}
	}

?>