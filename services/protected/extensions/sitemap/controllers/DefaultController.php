<?php

class DefaultController extends ControllerFrontBase
{
	/**
	 * Displays sitemap in XML or HTML format,
	 * depending on the value of $format parameter
	 * @param string $format
	 */
	public function actionIndex($format = 'xml', $type = 'other', $page)
	{
		if( $page == '' ) $page = 1;
		
		$urls = $this->getModule()->getAllUrls( $type, $page );
			
		if ($format == 'xml')
		{
			if (!headers_sent())
				header('Content-Type: text/xml');
			$this->renderPartial('xml', array('urls' => $urls));
		}
		else
			$this->render('html', array('urls' => $urls));
	}
}