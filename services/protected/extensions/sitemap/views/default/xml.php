<?php echo '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="' . Yii::App()->request->getHostInfo() . '/main-sitemap.xsl"?>' ?>
 
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php foreach ($urls as $url => $data) : ?>
<url>
	<loc><?php echo htmlspecialchars($url) ?></loc>
<?php if (isset($data['lastmod'])) : ?>
	<lastmod><?php echo $data['lastmod'] ?></lastmod>
<?php endif; ?>
<?php if (isset($data['changefreq'])) : ?>
	<changefreq><?php echo $data['changefreq'] ?></changefreq>
<?php endif; ?>
<?php if (isset($data['priority'])) : ?>
	<priority><?php echo $data['priority'] ?></priority>
<?php endif; ?>
</url>
<?php endforeach; ?>
</urlset> 
