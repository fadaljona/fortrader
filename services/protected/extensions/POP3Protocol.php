<?

	class POP3Protocol{
		const sizeBlock = 1024;
		private $host, $user, $password;
		private $c;
		function __construct( $host, $user, $password ) {
			$this->host = $host;
			$this->user = $user;
			$this->password = $password;
			
			$this->connect();
			$this->auth();
		}
		function __destruct() {
			$this->disconnect();
		}
		protected function connect() {
			$this->c = @fsockopen( 'ssl://' . $this->host, 995, $errno, $errstr, 10 );
			if( !$this->c ) throw new Exception( "Can't connect to {$this->host}" );
			return true;
		}
		protected function disconnect() {
			$this->putSigle( "QUIT" );
			$this->getSigle();
			return fclose( $this->c );
		}
		protected function auth() {
			$this->putSigle( "USER {$this->user}" );
			$result = $this->getSigle();
			if( !self::validateResult( $result )) throw new Exception( "Can't auth on {$this->user}" );
			
			$this->putSigle( "PASS {$this->password}" );
			$result = $this->getSigle();
			if( !self::validateResult( $result )) throw new Exception( "Can't auth on {$this->user}" );
			return true;
		}
		protected function getSigle() {
			return fgets( $this->c, self::sizeBlock );
		}
		protected function getMulti() {
			$data = Array();
			while( !feof( $this->c )) {
				$buffer = $this->getSigle();
				if( trim( $buffer ) == "." ) break;
				$data[] = $buffer;
			}
			return implode($data);
		}
		protected function putSigle( $str ) {
			return fputs( $this->c, "{$str}\r\n" );
		}
		protected static function validateResult( $result ) {
			return strpos( $result, "+OK" ) === 0;
		}
		protected static function parseStat( $str ) {
			list( $ok, $count, $size ) = explode( " ", $str );
			$statictics = (object)Array(
				'count' => $count,
				'size' => $size,
			);
			return $statictics;
		}
		protected static function parseHeaders( $text ) {
			$headers = Array();
			$key = "";
			foreach( explode( "\r\n", $text ) as $line ) {
				if( strpos( $line, "+OK" ) === 0 ) continue;
				if( strpos( $line, "\t" ) === 0 ) {
					$line = trim( $line );
					if( is_array( $headers[ $key ])) {
						$header = array_pop( $headers[ $key ] );
						$header .= " {$line}";
						array_push( $headers[ $key ], $header );
					}
					else{
						$headers[ $key ] .= " {$line}";
					}
				}
				else{
					list($key, $header) = explode( ": ", $line, 2 );
					if( isset( $headers[ $key ])) {
						if( !is_array( $headers[ $key ])) {
							$headers[ $key ] = Array( $headers[ $key ] );
						}
						$headers[ $key ][] = $header;
					}
					else{
						$headers[ $key ] = $header;
					}
				}
			}
			return $headers;
		}
		protected static function parseTop( $text ) {
			list( $headers, $body ) = explode( "\r\n\r\n", $text );
			$headers = self::parseHeaders( $headers );
			$top = (object)Array(
				'headers' => $headers,
				'body' => $body,
			);
			return $top;
		}
		function getStat() {
			$this->putSigle( "STAT" );
			$result = $this->getSigle();
			if( !self::validateResult( $result )) throw new Exception( "Can't get Statistics" );
			return self::parseStat( $result );
		}
		function getTop( $i, $lines = 0 ) {
			$this->putSigle( "TOP {$i} {$lines}" );
			$result = $this->getMulti();
			return self::parseTop($result);
		}
	}

?>