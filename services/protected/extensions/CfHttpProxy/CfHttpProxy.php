<?php
/**
 * get page content protected by cloud flare
 */

Yii::setPathOfAlias('cfhttpproxy', dirname(__FILE__));
Yii::import('cfhttpproxy.libraries.cloudflare');
Yii::import('cfhttpproxy.libraries.httpProxy');

class CfHttpProxy
{
    public static function getRawPageContent($url)
    {
        $httpProxy   = new httpProxy();
        $httpProxyUA = 'proxyFactory';

        $requestPage = json_decode($httpProxy->performRequest($url));

        if ($requestPage->status->http_code == 503) {
            cloudflare::useUserAgent($httpProxyUA);
            if ($clearanceCookie = cloudflare::bypass($url)) {
                $requestPage = $httpProxy->performRequest($url, 'GET', null, array(
                    'cookies' => $clearanceCookie
                ), true);
                return $requestPage;
            } else {
                return false;
            }
        }
        return false;
    }
}