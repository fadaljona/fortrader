<?
	Yii::import( 'components.base.ComponentBase' );
	
	final class CalendarEventsImporterComponentNew extends ComponentBase {
		public $daysRadiusLeft = 30;
		public $daysRadiusRight = 30;
		public $parseNewEvents = 0;
		const APIURL = "http://infos.fxteam.ru/3.1/apiecalend/events2.php?dt={date}&key={key}&lang={lang}";
		const pathLog = 'log/importCalendarEvents';
		public $mode = 'Default';
		private function getCronLogs() {
			$models = CalendarEventCronModel::model()->findAll(Array(
				'condition' => " DATEDIFF( `date`, CURDATE() ) BETWEEN :left AND :right ",
				'order' => " `date` ",
				'params' => Array(
					':left' => -$this->daysRadiusLeft,
					':right' => $this->daysRadiusRight,
				),
			));
			$models = CommonLib::toAssoc( $models, 'date' );
			return $models;
		}
		private static function sortCronLogs( $a, $b ) {
			if( $a->updatedDT == $b->updatedDT ) return 0;
			return $a->updatedDT > $b->updatedDT ? 1 : -1;
		}
		private static function saveCronLog( $date ) {
			$model = CalendarEventCronModel::model()->findByPk( $date );
			if( !$model ) $model = CalendarEventCronModel::instance( $date );
			$model->save();
		}
		private static function getNumValue($value)
		{
			$value = preg_replace( "#[^0-9\-\.]#", "", $value );
			return $value;
		}
		private static function getTitle($value)
		{
			$res = str_replace(self::getNumValue($value), '', $value);
			return $res;
		}
		private static function isCirilic( $str ) {
			if (preg_match("/[а-я]/i", $str)) {
				return true;
			}
			return false;
		}
		private function getWorkDate() {
			if( $this->mode == 'Today' ) {
				return date( "Y-m-d" );
			}
			
			$cronLogs = $this->getCronLogs();
			//echo 'test';
			if( $this->parseNewEvents ){
			
				for( $i = 0; $i <= $this->daysRadiusLeft + $this->daysRadiusRight; $i++ ) {
					$daysDiff = $i - $this->daysRadiusLeft;
					$time = time() + $daysDiff * 86400;
					$date = date( "Y-m-d", $time );
					if( date('Y-m-d',strtotime($cronLogs[$date]->updatedDT) ) != date( "Y-m-d", time() ) ){ return $date;};

				}
			}
			
			for( $i = 0; $i <= $this->daysRadiusLeft + $this->daysRadiusRight; $i++ ) {
				$daysDiff = $i - $this->daysRadiusLeft;
				$time = time() + $daysDiff * 86400;
				$date = date( "Y-m-d", $time );
				if( empty( $cronLogs[ $date ])) return $date;
			}
			
			uasort( $cronLogs, Array( 'CalendarEventsImporterComponent', 'sortCronLogs' ));
			$keys = array_keys( $cronLogs );
			if( $keys ) return $keys[0];
			exit;
		}
		private static function getAPIContent( $date, $lang ) {
			if( $lang == 'en_us' ) {
				 $lang = 'en';
			}
			$url = strtr( self::APIURL, Array(
				'{date}' => $date,
				'{key}' => Yii::App()->params[ 'keyAPI_fxteam.ru' ],
				'{lang}' => $lang,
			));
			$content = CommonLib::downloadFileFromWww( $url );
			self::logAPIContent( $date, $lang, $content );
			return $content;
		}
		private static function logAPIContent( $date, $lang, $content ) {
			$year = substr( $date, 0, strpos( $date, '-' ));
			$dir = self::pathLog."/{$lang}/{$year}";
			if( !is_dir( $dir )) mkdir( $dir );
			$file = "{$dir}/{$date}.xml";
			file_put_contents( $file, $content );
		}
		private static function getEventsFromAPI( $date, $lang ) {
			$content = self::getAPIContent( $date, $lang );
			//$content = file_get_contents( "log/importCalendarEvents/2014-03-09.xml" );
			
			$XML = new DOMDocument( "1.0", "UTF-8" );
			$XML->loadXML( $content );
			$XPath = new DOMXPath( $XML );
			
			$events = Array();
			
			$nodes = $XPath->query( "//event" );
			foreach( $nodes as $nodeEvent ) {
				$event = Array();
				foreach( $nodeEvent->childNodes as $node ) {
					if( $node->nodeType == XML_ELEMENT_NODE ) {
						$event[ $node->nodeName ] = $node->textContent !== "" ? $node->textContent : null;
					}
				}
				if( @$event['id'] and @$event['dt'] ) $events[] = $event;
			}
			/*echo '<pre>';
			print_r($events);
			echo '</pre>';
			die;*/
			return $events;
		}
		private static function updateEvents( $date, $lang ) {
			$fieldsEvent = ModelBase::getModelColumns( 'CalendarEvent2Model' );
			$fieldsEventI18N = ModelBase::getModelColumns( 'CalendarEvent2I18NModel' );
			$fieldsEventValue = ModelBase::getModelColumns( 'CalendarEvent2ValueModel' );

			/*print_r($fieldsEventValue);
			die;*/
			
			$events = self::getEventsFromAPI( $date, $lang->alias );

			echo $date.'<pre>';
			print_r($events);
			echo '</pre>';
						
			foreach( $events as $event ) {
				if ( $event['indicator_id'] != 0 ) {
					$model = CalendarEvent2Model::model()->findByAttributes(Array(
						'indicator_id' => $event['indicator_id'],
					));
					if( !$model ) {
						$model = new CalendarEvent2Model();
						$model->date_add = date('Y-m-d H:i:s', time());
					}
					foreach( $event as $key => $value ) {
						if( in_array( $key, $fieldsEvent )) {
							$model->$key = $value;
						}
					}
					
					$model->date_update = date('Y-m-d H:i:s', time());
					if( !$model->slug ){
						$slug = CommonLib::generateSlug($event['indicator']);
						if( CalendarEvent2Model::findBySlug( $slug ) ){
							$slug = $event['indicator_id'];
						}
						$model->slug = $slug;
					}
					$model->save();

					$modelI18N = CalendarEvent2I18NModel::model()->findByAttributes(Array(
						'indicator_id' => $model->indicator_id,
						'idLanguage' => $lang->id,
					));
					if( !$modelI18N ) {
						$modelI18N = CalendarEvent2I18NModel::instance( $model->id, $lang->id, $event['indicator'] );
					}


					$modelValue = CalendarEvent2ValueModel::model()->findByAttributes(Array(
						'unic_id' => $event['id'],
					));

					if( !$modelValue ) {

						$modelValue = CalendarEvent2ValueModel::model()->findByAttributes(Array(
							'indicator_id' => $model->indicator_id,
							'dt' => $event['dt'],
						));

						if( !$modelValue ) {
							$modelValue = new CalendarEvent2ValueModel();
						}
					}

					foreach( $event as $key => $value ) {
						if( in_array( $key, $fieldsEventValue ) and $key != 'id') {
							$modelValue->$key = self::getNumValue($value);
						}
						if( in_array( $key, $fieldsEventI18N ) and $key != 'id') {
							$modelI18N->$key = $value;
						}
						if( in_array( $key.'_name', $fieldsEventI18N ) and $key != 'id' ) {
							$modelI18N->{$key.'_name'} = self::getTitle($value);
						}
					}
			
					$modelI18N->indicator_name = strip_tags( $event['indicator'] );
					if ( 
						($modelI18N->idLanguage == 0 and !self::isCirilic($modelI18N->indicator_name))
						or
						($modelI18N->idLanguage == 1 and self::isCirilic($modelI18N->indicator_name))
					){
						$modelI18N->save();
					}
					$modelValue->dt = $event['dt'];
					$modelValue->unic_id = $event['id'];
					$modelValue->save();
				}
			}
			self::saveCronLog( $date );
		}

		function import() {
			$date = $this->getWorkDate();
			$langs = LanguageModel::getAll();
			foreach( $langs as $lang ) {
				self::updateEvents( $date, $lang );
			}
		}
	}
	
?>
