<?
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'components.ClickatellComponent' );
	Yii::import( 'components.MailerComponent' );
	
	final class MailingComponent extends ComponentBase {
		public $limitStage = 10;
		public $lowClickatellBalanse = 100;
		private $factoryClickatell;
		public $debug = false;
		public $SMTPDebug = false;
		public $logEmail = false;
		const pathLogEmail = 'log/mailings/email';
		function __construct() {
			$this->factoryClickatell = new ClickatellComponent();
			$this->factoryMailer = new MailerComponent();
			$this->getLanguages();
		}
		private function getDefLanguage() {
			return defined( 'LBP_LANGUAGE' ) ? LBP_LANGUAGE : 'ru';
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ) {
				$this->languages[ $language->alias ] = $language->id;
			}
		}
		private function getMailingI18N( $mailing, $language, $defLanguage ) {
			$idLanguage = $this->languages[ $language ];
			$idDefLanguage = $this->languages[ $defLanguage ];
			foreach( $mailing->i18ns as $i18n ) {
				if( $i18n->idLanguage == $idLanguage ) {
					return $i18n;
				}
			}
			foreach( $mailing->i18ns as $i18n ) {
				if( $i18n->idLanguage == $idDefLanguage ) {
					return $i18n;
				}
			}
			foreach( $mailing->i18ns as $i18n ) {
				if( strlen( $i18n->message )) {
					return $i18n;
				}
			}
		}
		private function getMailingMessage( $mailing, $language, $defLanguage ) {
			$i18n = $this->getMailingI18N( $mailing, $language, $defLanguage );
			return $i18n ? $i18n->message : '';
		}
		private function getMailingName( $mailing, $language, $defLanguage ) {
			$i18n = $this->getMailingI18N( $mailing, $language, $defLanguage );
			return $i18n ? $i18n->name : '';
		}
		private function getTextMessageI18N( $key, $language, $defLanguage ) {
			$idLanguage = $this->languages[ $language ];
			$idDefLanguage = $this->languages[ $defLanguage ];
			$messageI18N = MessageModel::model()->find( Array(
				'with' => Array( "i18ns" ),
				'condition' => " `t`.`key` = :key ",
				'params' => Array(
					':key' => $key,
				),
			));
			
			if( !$messageI18N ) return "";
			
			foreach( $messageI18N->i18ns as $i18n ) {
				if( $i18n->idLanguage == $idLanguage ) {
					return $i18n->value;
				}
			}
			foreach( $messageI18N->i18ns as $i18n ) {
				if( $i18n->idLanguage == $idDefLanguage ) {
					return $i18n->value;
				}
			}
			foreach( $messageI18N->i18ns as $i18n ) {
				if( strlen( $i18n->value )) {
					return $i18n->value;
				}
			}
		}
		private function renderMessageToUser( $mailing, $user, $type = null ) {
			$language = $user->language ? $user->language : $this->getDefLanguage();
			$message = $this->getMailingMessage( $mailing, $language, $this->getDefLanguage() );
			
			$message = strtr( $message, Array(
				'%username%' => $user->user_login,
				'%useremail%' => $user->user_email,
			));
			
			if( $type == 'mail' ) {
				$unsubscribe = $this->getTextMessageI18N( 'unsubscribe_mail_type', $language, $this->getDefLanguage() );
				$urlSettings = Yii::App()->createAbsoluteUrl( 'user/settings', Array( 'tabSettings' => 'yes' ));
				$unsubscribe = strtr( $unsubscribe, Array(
					'{profile_link}' => CHtml::openTag( 'a', Array( 'href' => $urlSettings )),
					'{/profile_link}' => CHtml::closeTag( 'a' ),
					'{unsubscribe_link}' => $mailing->idType ? CHtml::openTag( 'a', Array( 'href' => $user->getUnsubscribeMailingTypeAbsoluteURL( $mailing->idType ))) : '',
					'{/unsubscribe_link}' => $mailing->idType ? CHtml::closeTag( 'a' ) : '',
				));
				$message .= $unsubscribe;
			}
			
			return $message;
		}
		private function logEmailMessage( $user, $mailing, $message ) {
			$date = date( "Y.m.d" );
			$dirDate = self::pathLogEmail."/{$date}";
			CommonLib::createDir( $dirDate );
			
			$dirEmail = "{$dirDate}/{$mailing->id}";
			CommonLib::createDir( $dirEmail );
			
			$pathFile = "{$dirEmail}/{$user->user_email}";
			file_put_contents( $pathFile, $message );
		}
		private function mailToUser( $mailing, $user ) {
			$warnings = Array();
			
			$language = $user->language ? $user->language : $this->getDefLanguage();
			$subject = $this->getMailingName( $mailing, $language, $this->getDefLanguage() );
			if( !strlen( $subject )) throw new Exception( 'Empty subject!' );
			
			$message = $this->renderMessageToUser( $mailing, $user, 'mail' );
			if( !strlen( $message )) throw new Exception( 'Empty message!' );
			
			try {
				$mailer = $this->factoryMailer->instanceMailer();
			}
			catch( Exception $e ) {
				throw new Exception( "Mailer - ".$e->getMessage() );
			}
						
			try {
				if( !strlen( $user->user_email )) throw new Exception( 'Empty user email!' );
				
				$mailer->addAddress( $user->user_email, $user->user_login );
				$mailer->Subject = $subject;
				if( $mailing->idType ) {
					$mailer->addCustomHeader( "List-Unsubscribe: ".$user->getUnsubscribeMailingTypeAbsoluteURL( $mailing->idType ));
				}
								
				if( substr_count( $message, '<' )) {
					$mailer->MsgHTML( $message );
				}
				else{
					$mailer->Body = $message;
				}
				
				if( $this->logEmail ) {
					$this->logEmailMessage( $user, $mailing, $message );
				}
				
				if( !$this->debug ) {
					$mailer->SMTPDebug = $this->SMTPDebug;
					$result = $mailer->send();
					$error = $result ? null : $mailer->ErrorInfo;
					if ( $mailer->Mailer == 'smtp' ) $mailer->smtpClose();
				}
				else{
					$warnings[] = "Debug... EMail to {$user->user_email}";
					$result = true;
					$error = null;
				}
			}
			catch( Exception $e ) {
				$result = false;
				$error = $e->getMessage();
			}
			
			return Array( $result, $error, $warnings );
		}
		private function renderUserPhone( $phone ) {
			$phone = preg_replace( "#^9#", "+79", $phone );
			$phone = preg_replace( "#^8|^7#", "+7", $phone );
			$phone = preg_replace( "# #", "", $phone );
			if( !preg_match( "#^\+\d{11,}$#", $phone )) $phone = null;
			return $phone;
		}
		private function smsToUser( $mailing, $user ) {
			$warnings = Array();
			
			$message = $this->renderMessageToUser( $mailing, $user );
			if( !strlen( $message )) throw new Exception( 'Empty message!' );
			
			try {
				$API = $this->factoryClickatell->instanceAPI();
			}
			catch( Exception $e ) {
				throw new Exception( "Clickatell - ".$e->getMessage() );
			}
			
			if ( !$API->checkMsgLen( $message )) {
				throw new Exception( Yii::t( $NSi18n, "Message to long for SMS! Maximum {d1} chars ({d2} for Unicode)!", Array(
					"{d1}" => $API::maxMsgLen, 
					"{d2}" => $API::maxUnicodeMsgLen
				)));
			}
			
			try {
				$phone = $this->renderUserPhone( $user->phone );
				if( strlen( $phone )) {
					if( !$this->debug ) {
						$apiMsgId = $API->sendMsg( $phone, $message );
						$result = strlen( $apiMsgId );
						$error = $result ? null : "Empty return apiMsgId!";
					}
					else{
						$warnings[] = "Debug... SMS to {$phone}: {$message}";
						$result = true;
						$error = null;
					}
				}
				else{
					$warnings[] = "Can't send SMS to to {$user->phone}";
					$result = true;
					$error = null;
				}
			}
			catch( Exception $e ) {
				$result = false;
				$error = $e->getMessage();
				if( preg_match( "#No credit left#i", $error )) {
					throw new Exception( "Clickatell - {$error}" );
				}
			}
			
			return Array( $result, $error, $warnings );
		}
		private function getDPUsers( $mailing, $stage, $offset = 0 ) {
			$c = new CDbCriteria();
			
			if( $mailing->usersGroup ) {
				$c->join .= " INNER JOIN `{{user_to_user_group}}` ON `{{user_to_user_group}}`.`idUser` = `t`.`id` AND `{{user_to_user_group}}`.`idUserGroup` = {$mailing->usersGroup} ";
			}
			if( $mailing->idType ) {
				$c->addCondition( "
					NOT EXISTS (
						SELECT 			1
						FROM			`{{user_to_mailing_type_mute}}`
						WHERE			`{{user_to_mailing_type_mute}}`.`idUser` = `t`.`id`
							AND			`{{user_to_mailing_type_mute}}`.`idType` = :idType
					)
				" );
				$c->params[ ':idType' ] = $mailing->idType;
			}
			
			switch( $stage ) {
				case 'SMSMailing':{
					$c->addCondition( "LENGTH(`t`.`phone`)" );
					break;
				}
				case 'EmailMailing':{
					$c->addCondition( "LENGTH(`t`.`user_email`)" );
					break;
				}
			}
			
			$c->order = '`t`.`id`';
			$DP = new CActiveDataProvider( 'UserModel', Array(
				'criteria' => $c,
				'pagination' => Array(
					'pageSize' => $this->limitStage,
					'currentPage' => (int)$offset,
				),
			));
			$DP->pagination->setItemCount( $DP->getTotalItemCount( ));
			$DP->pagination->applyLimit( $c );
			return $DP;
		}
		private function check( $mailing, &$data ) {
			$NSi18n = $this->getNSi18n();
			if( $mailing->isSMS ) {
				try {
					$API = $this->factoryClickatell->instanceAPI();
				}
				catch( Exception $e ) {
					throw new Exception( "Clickatell - ".$e->getMessage() );
				}
				$balance = $API->getBalance();
				if( $balance < $this->lowClickatellBalanse ) {
					$data[ 'warnings' ][] = Yii::t( $NSi18n, "Warning: Low Clickatell balance! {balance}", Array(
						"{balance}" => $balance, 
					));
				}
			}
			if( $mailing->isEmail ) {
				$from = SettingsModel::getModel()->common_mailForMailing;
				if( !strlen( $from )) throw new Exception( Yii::t( $NSi18n, 'EMail for mailing not set in Settings project!' ));
				
				list( $fromName, $fromEmail ) = CommonLib::explodeComplexEmail( $from );
				if( !strlen( $fromEmail )) throw new Exception( Yii::t( $NSi18n, 'Wrong EMail for mailing set in Settings project!' ));
			}
		}
		private function runStage( $mailing, $stage, &$data ) {
			switch( $stage ) {
				case 'initialization':{
					$this->check( $mailing, $data );
					
					if( !@$data[ 'restart' ]) {
						$progress = $mailing->getProgressData();
						if( @$progress->stage and $progress->stage != 'finish' ) {
							$this->runStage( $mailing, 'resume', $data );
							return;
						}
					}
					
					$this->runStage( $mailing, 'SMSMailing', $data );
					break;
				}
				case 'resume':{
					$progress = $mailing->getProgressData();
					$data[ "stage" ] = $stage;
					$data[ 'progressData' ] = $progress;
					break;
				}
				case 'SMSMailing':{
					if( $mailing->isSMS ) {
						$data[ "stage" ] = $stage;
						$data[ "offset" ] = "0";
						$DP = $this->getDPUsers( $mailing, $stage );
						$data[ "count" ] = $DP->pagination->getPageCount();
						$data[ "limit" ] = $this->limitStage;
					}
					else{
						$this->runStage( $mailing, 'EmailMailing', $data );
					}
					break;
				}
				case 'EmailMailing':{
					if( $mailing->isEmail ) {
						$data[ "stage" ] = $stage;
						$data[ "offset" ] = "0";
						$DP = $this->getDPUsers( $mailing, $stage );
						$data[ "count" ] = $DP->pagination->getPageCount();
						$data[ "limit" ] = $this->limitStage;
					}
					else{
						$this->runStage( $mailing, 'finish', $data );
					}
					break;
				}
				case 'finish':{
					$data[ "stage" ] = $stage;
					$data[ "offset" ] = "0";
					$data[ "count" ] = "0";
					break;
				}
			}
		}
		private function progressStage( $mailing, &$data ) {
			$NSi18n = $this->getNSi18n();
			switch( @$data[ "stage" ]) {
				case 'SMSMailing':{
					$DP = $this->getDPUsers( $mailing, $data[ "stage" ], $data[ "offset" ]);
					$data[ "count" ] = $DP->pagination->getPageCount();
					$data[ "limit" ] = $this->limitStage;
					if( !$data[ "count" ]) {
						$data[ "warnings" ][] = Yii::t( $NSi18n, 'SMS Mailing - There is no one to send messages ...' );
					}
					if( $data[ "offset" ] < $data[ "count" ]) {
						foreach( $DP->getData() as $user ) {
							list( $result, $error, $warnings ) = $this->smsToUser( $mailing, $user );
							if( !$result ) {
								$data[ "warnings" ][] = Yii::t( $NSi18n, 'Could not send sms to {phone}! {error}', Array( 
									'{phone}' => $user->phone,
									'{error}' => $error,
								));
							}
							if( $warnings ) $data[ "warnings" ] = array_merge( isset($data[ "warnings" ]) ? $data[ "warnings" ] : Array(), $warnings );
						}
						$data[ "offset" ]++;
					}
					if( $data[ "offset" ] >= $data[ "count" ]) {
						$this->runStage( $mailing, 'EmailMailing', $data );
					}
					break;
				}
				case 'EmailMailing':{
					$DP = $this->getDPUsers( $mailing, $data[ "stage" ], $data[ "offset" ]);
					$data[ "count" ] = $DP->pagination->getPageCount();
					$data[ "limit" ] = $this->limitStage;
					if( !$data[ "count" ]) {
						$data[ "warnings" ][] = Yii::t( $NSi18n, 'Email Mailing - There is no one to send messages ...' );
					}
					if( $data[ "offset" ] < $data[ "count" ]) {
						foreach( $DP->getData() as $user ) {
							list( $result, $error, $warnings ) = $this->mailToUser( $mailing, $user );
							if( !$result ) {
								$data[ "warnings" ][] = Yii::t( $NSi18n, 'Could not send mail to {email}! {error}', Array( 
									'{email}' => $user->user_email,
									'{error}' => $error,
								));
							}
							if( $warnings ) $data[ "warnings" ] = array_merge( isset($data[ "warnings" ]) ? $data[ "warnings" ] : Array(), $warnings );
						}
						$data[ "offset" ]++;
					}
					if( $data[ "offset" ] >= $data[ "count" ]) {
						$this->runStage( $mailing, 'finish', $data );
					}
					break;
				}
			}
		}
		private function saveProgress( $mailing, $data ) {
			switch( @$data[ "stage" ]) {
				case 'resume':{
					break;
				}
				case 'initialization': {
					$mailing->setProgressData( (object)Array() );
					$mailing->update( Array( 'progressData' ));
					break;
				}
				case 'finish': {
					$mailing->setProgressData( (object)Array(
						'stage' => $data[ 'stage' ],
					));
					$mailing->update( Array( 'progressData' ));
					break;
				}
				default: {
					$mailing->setProgressData( (object)Array(
						'stage' => $data[ 'stage' ],
						'offset' => $data[ 'offset' ],
						'count' => $data[ 'count' ],
						'limit' => $data[ 'limit' ],
					));
					$mailing->update( Array( 'progressData' ));
					break;
				}
			}
		}
		function run( $mailing, &$data ) {
			$data[ "offset" ] = max( (int)@$data[ "offset" ], 0 );
			switch( @$data[ "stage" ]) {
				case 'initialization':{
					$this->runStage( $mailing, 'initialization', $data );
					break;
				}
				case 'SMSMailing':
				case 'EmailMailing': {
					$this->progressStage( $mailing, $data );
					break;
				}
				default: {
					throw new Exception( "Unknown stage {$data[ "stage" ]}!" );
				}
			}
			$this->saveProgress( $mailing, $data );
		}
	}
	
?>