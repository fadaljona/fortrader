<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class MonitoringAccountInfoWidget extends WidgetBase {
		const modelName = 'ContestMemberModel';
		public $model;
		public $idMember;
		private function getIDMember() {
			return $this->idMember;
		}
		private function loadModel() {
			$idMember = $this->getIDMember();
			$this->model = ContestMemberModel::model()->find( Array(
				'with' => Array( 
					'server' => Array(),
					'mt5AccountInfo' => Array(),
					'server.broker' => Array(),
					'server.broker.currentLanguageI18N' => Array(),
					'stats' => Array(),
				),
				'condition' => "
					`t`.`id` = :idMember
				",
				'params' => Array(
					':idMember' => $idMember,
				),
			));
			return $this->model;
		}
		private function getModel() {
			return $this->model ? $this->model : $this->loadModel();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
			));
		}
	}

?>