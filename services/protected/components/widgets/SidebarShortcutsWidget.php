<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class SidebarShortcutsWidget extends WidgetBase {
		private function getEditURL() {
			if( Yii::App()->user->hasAdminRight()) {
				$controller = $this->controller->id;
				$action = $this->controller->action->id;
				$id = (int)@$_GET[ 'id' ];
				
				switch( "{$controller}/{$action}" ) {
					case 'broker/list':{
						return Yii::App()->createURL( '/admin/broker/list' );
					}
					case 'broker/single':{
						return Yii::App()->createURL( '/admin/broker/list', Array( 'idEdit' => $id ));
					}
					case 'contest/list':{
						return Yii::App()->createURL( '/admin/contest/list' );
					}
					case 'contest/single':{
						return Yii::App()->createURL( '/admin/contest/list', Array( 'idEdit' => $id ));
					}
					case 'contestMember/single':{
						$contestMember = ContestMemberModel::model()->findByPk( $id );
						if( $contestMember ) {
							return Yii::App()->createURL( '/admin/contestMember/list', Array( 'idContest' => $contestMember->idContest, 'idEdit' => $id ));
						}
						break;
					}
					case 'ea/list':{
						return Yii::App()->createURL( '/admin/ea/list' );
					}
					case 'ea/single':{
						return Yii::App()->createURL( '/admin/ea/list', Array( 'idEdit' => $id ));
					}
					case 'eaStatement/single':{
						return Yii::App()->createURL( '/admin/eaStatement/list', Array( 'idEdit' => $id ));
					}
					case 'eaTradeAccount/single':{
						$eaTA = EATradeAccountModel::model()->findByPk( $id );
						return Yii::App()->createURL( '/admin/tradeAccount/list', Array( 'idEdit' => $eaTA->idTradeAccount ));
					}
					case 'eaVersion/single':{
						return Yii::App()->createURL( '/admin/eaVersion/list', Array( 'idEdit' => $id ));
					}
					case 'user/profile':{
						if( $id ) {
							return Yii::App()->createURL( '/admin/user/list', Array( 'idEdit' => $id ));
						}
						break;
					}
					case 'userPrivateMessage/list':{
						return Yii::App()->createURL( '/admin/userPrivateMessage/list' );
					}
				}
			}
			return Yii::App()->createURL( '/user/settings' );
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'editURL' => $this->getEditURL(),
			));
		}
	}

?>