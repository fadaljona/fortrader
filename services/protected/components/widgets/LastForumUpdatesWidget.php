<?php
Yii::import('components.widgets.base.WidgetBase');

final class LastForumUpdatesWidget extends WidgetBase
{
    public $url;
    
    private function parseUrl()
    {
        $cacheKey = 'LastForumUpdates' . $this->url . Yii::app()->language;
        if ($content = Yii::app()->cache->get($cacheKey)) {
            return $content;
        } else {
            $res = CommonLib::downloadFileFromWww($this->url);
            if (!$res) {
                return false;
            }

            preg_match_all('/<a\s+href="([^"]+)"[^>]+><strong>([^<]+)<\/strong><\/a><\/span>/', $res, $matches);

            if (!isset($matches[1]) || !isset($matches[2])) {
                return false;
            }

            $outArr = array();

            foreach ($matches[1] as $id => $url) {
                $outArr[$url] = $matches[2][$id];
            }
            
            Yii::app()->cache->set($cacheKey, $outArr, 60*15);
            return $outArr;
        }
    }
        
    public function run()
    {
        if (!$this->url) {
            return false;
        }

        $posts = $this->parseUrl();

        if (!$posts) {
            return false;
        }

        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'posts' => $posts,
            'forumUrl' => $this->url
        ));
    }
}
