<?
Yii::import('components.widgets.base.WidgetBase');

final class SearchQuotesWidget extends WidgetBase{
	public $minInputChars;

	function run() {
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'minInputChars' => $this->minInputChars,
		));
	}
	
}
?>