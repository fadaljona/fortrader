<?
Yii::import('components.widgets.base.WidgetBase');

final class InformerWidget extends WidgetBase{
    const colorPattern = "/^[0-9,a-z,A-Z,\=,\,]+$/";
    
    public $getStyles = false;
    public $getScripts = false;
    public $params = false;
    public $marker = false;
    private $style;

    private function getParams(){
        if( $this->params ) return $this->params;
        return $_GET;
    }
    private function cIsset( $param ){
        $getParams = $this->getParams();
        if( !isset($getParams[$param]) || !$getParams[$param] ) return false;
        return $getParams[$param];
    }
    private function setStyle(){
        $getParams = $this->getParams();
        $this->style = InformersStyleModel::model()->findByPk( $getParams['st'] );
        if( !$this->style ) $this->style = InformersStyleModel::getDefault();
    }
    private function getStyle(){
        if( !$this->style ) $this->setStyle();
        return $this->style;
    }
    private function getCategory(){
        $getParams = $this->getParams();
        return InformersCategoryModel::model()->findByPk( $getParams['cat'] );
    }
    private function restoreColorsFromGet(){
        $style = $this->getStyle();
        if( !$this->cIsset( 'colors' ) ) return $style->colors;

        $getParams = $this->getParams();
            
        if( !preg_match( self::colorPattern, $getParams['colors'] ) ) return $style->colors;
        
        $getColors = array();
        if( isset($getParams['colors']) && $getParams['colors'] && $getParams['colors'] != 'false' ){
            foreach( explode(',', $getParams['colors']) as $color ){
                $expColor = explode('=', $color);
                $getColors[$expColor[0]] = '#' . $expColor[1];
            }
        }
        
        $informerColors = array();
        foreach( $style->colors as $nameColor => $val ){
            if( isset( $getColors[$nameColor] ) ){
                $informerColors[$nameColor] = $getColors[$nameColor];
            }else{
                $informerColors[$nameColor] = $val;
            }
        }
        
        
        
        return $informerColors;
    }
    private function renderQuotesCurrencies($style, $informerColors){
        $getParams = $this->getParams();

        if( !isset( $getParams['w'] ) ){
            $width = $style->width . 'px';
        }elseif( !$getParams['w'] ){
            $width = '100%';
        }else{
            $width = intval($getParams['w']) . 'px';;
        }


        $hash = '';
        $lType = 'view';
        if( isset( $getParams['lType'] ) && $getParams['lType'] == 'html' ){
            $lType = 'html/view';

            $hash = $getParams;
            sort($hash);
            $hash = implode('.', $hash);
            $hash = md5($hash . '_lang_' . Yii::app()->language);
        }

        $mult = floatval( $getParams['mult'] );
        if( !$mult ) return false;

        
        if( !isset( $getParams['hideDiff'] ) || !$getParams['hideDiff'] ){
            $showDiff = true;
        }else{
            $showDiff = false;
        }
        if( isset( $getParams['demoPreview'] ) && $getParams['demoPreview'] == 1 )$showDiff = false;
        
        if( !$this->cIsset('cat') || !$this->cIsset('mult') || !$this->cIsset('items') ) return false;
        $category = $this->getCategory();
        if( !$category ) return false;
        

        if( $this->getStyles ){
            $this->render( $this->getCleanClassName() . "/styles/{$style->viewFilesDir}/{$style->viewFile}/html/css" , Array(
                'model' => $style,
                'mult' => $mult,
                'informerColors' => $informerColors,
                'showGetBtn' => intval( @$getParams['showGetBtn'] ),
                'width' => $width,
                'hash' => $hash,
                'showDiff' => $showDiff,
            ));
            return false;
        }

        
        $items = $category->getItemsByIds( $getParams['items'] );
        if( !$items ) return false;

        $columns = $category->getColumnsByGet( $getParams['columns'], $style );
        if( !$columns ) return false;

        $defaultColumn = $category->columns[0];

        if( $this->getScripts ){
            $this->render( $this->getCleanClassName() . "/styles/{$style->viewFilesDir}/{$style->viewFile}/html/js" , Array(
                'marker' => $this->marker,
                'hash' => $hash,
                'columns' => $columns,
                'items' => $items,
                'category' => $category,
                'getParams' => $getParams,
                'defaultColumn' => $defaultColumn,
                'showDiff' => $showDiff,
            ));
            return false;
        }
    
        

        
        $this->render( $this->getCleanClassName() . "/styles/{$style->viewFilesDir}/{$style->viewFile}/{$lType}" , Array(
            'model' => $style,
            'category' => $category,
            'mult' => $mult,
            'items' => $items,
            'columns' => $columns,
            'mode' => 'live',
            'defaultColumn' => $defaultColumn,
            'informerColors' => $informerColors,
            'showGetBtn' => intval( @$getParams['showGetBtn'] ),
            'width' => $width,
            'showDiff' => $showDiff,
            'hash' => $hash,
            'hideHeader' => $this->cIsset('hideHeader'),
            'hideDate' => $this->cIsset('hideDate'),
        ));
    }
    private function renderConverter($style, $informerColors){
        $getParams = $this->getParams();

        if( !isset( $getParams['w'] ) ){
            $width = $style->width . 'px';
        }elseif( !$getParams['w'] ){
            $width = '100%';
        }else{
            $width = intval($getParams['w']) . 'px';;
        }
        
        if( !isset( $getParams['from'] ) || !$getParams['from'] ){
            $settings = InformersSettingsModel::getModel();
            $from = $settings->convertFrom;
        }else{
            $from = intval($getParams['from']);;
        }
        if( !isset( $getParams['to'] ) || !$getParams['to'] ){
            $settings = InformersSettingsModel::getModel();
            $to = $settings->convertTo;
        }else{
            $to = intval($getParams['to']);;
        }
        if( !isset( $getParams['amount'] ) || !$getParams['amount'] ){
            $settings = InformersSettingsModel::getModel();
            $amount = $settings->convertFromVal;
        }else{
            $amount = intval($getParams['amount']);;
        }
        
        if( !$this->cIsset('cat') ) return false;
        $category = $this->getCategory();
        if( !$category ) return false;
        
        $this->render( $this->getCleanClassName() . "/styles/{$style->viewFilesDir}/{$style->viewFile}/view" , Array(
            'informerColors' => $informerColors,
            'width' => $width,
            'from' => $from,
            'to' => $to,
            'amount' => $amount,
            'currenciesForConverter' => CurrencyRatesModel::getCurrenciesForConverter(),
            'defaultCurrency' => CurrencyRatesModel::getDefaultCurrency(),
            'showGetBtn' => intval( @$getParams['showGetBtn'] ),
        ));
    }
    public function getMonitoringChartData($member){
        $data = Array();
        $c = new CDbCriteria();
        
        $c->select = Array(
            " UNIX_TIMESTAMP( `timestamp` ) AS timestamp ",
            " ACCOUNT_BALANCE ",
            " ACCOUNT_EQUITY ",
            " revision ",
        );

        $c->addInCondition( 'ACCOUNT_NUMBER', Array( $member->accountNumber ));
        $c->addInCondition( 'AccountServerId', Array( $member->idServer ));
        $c->addCondition(" `timestamp` != '0000-00-00 00:00:00' ");
                
        $c->order = " `timestamp` ";
        $models = MT5AccountDataModel::model()->findAll( $c );
                
        if( count( $models ) > 1 ) {
                    
            $lastIndex = count($models) - 1;
            $lastTimeStamp = $models[ $lastIndex ]->timestamp;
                    
            $firstPoint = true;
            $tmpEquity = false;
            $prevData = false;
                    
            foreach( $models as $index => $model ) {
                $accountBalance = (float)$model->ACCOUNT_BALANCE;
                $accountEquity = (float)$model->ACCOUNT_EQUITY;
                        
                if( $firstPoint ){
                    $data[ $model->timestamp ][ 'balance' ] = $accountBalance;
                    $data[ $model->timestamp ][ 'equity' ] = $accountEquity;
                    $data[ $model->timestamp ][ 'date' ] = date("Y-m-d H:i:s", $model->timestamp);
                    $firstPoint = false;
                }else{
                            
                    $daysToFirstPoint = ($lastTimeStamp - $model->timestamp) / ( 60*60*24 );
                    if( $daysToFirstPoint >= 20 ){
                        $endOfPeriod = strtotime("tomorrow", $model->timestamp) - 1;
                        $periodVal = 60*60*24;
                    }elseif( $daysToFirstPoint >= 8 ){
                        $endOfDay = strtotime("tomorrow", $model->timestamp) - 1;
                        if( $model->timestamp > $endOfDay - 60*60*12 ){
                            $endOfPeriod = $endOfDay;
                        }else{
                            $endOfPeriod = $endOfDay - 60*60*12;
                        }
                        $periodVal = 60*60*12;
                    }else{
                        $endOfDay = strtotime("tomorrow", $model->timestamp) - 1;
                        if( $model->timestamp > $endOfDay - 60*60*6 ){
                            $endOfPeriod = $endOfDay;
                        }elseif( $model->timestamp > $endOfDay - 60*60*12 ){
                            $endOfPeriod = $endOfDay - 60*60*6;
                        }elseif( $model->timestamp > $endOfDay - 60*60*18 ){
                            $endOfPeriod = $endOfDay - 60*60*12;
                        }else{
                            $endOfPeriod = $endOfDay - 60*60*18;
                        }
                        $periodVal = 60*60*6;
                    }
                            
                            
                    if( $tmpEquity ){
                        if( $tmpEquity > $accountEquity ) $tmpEquity = $accountEquity;
                    }else{
                        $tmpEquity = $accountEquity;
                    }
                            
                    if( isset($models[$index+1]) ){
                        if( $models[$index+1]->timestamp > $endOfPeriod ){
                                    
                            if( isset($prevData) && isset($prevData['endOfPeriod' ]) && $prevData['endOfPeriod' ] + $periodVal < $endOfPeriod ){
                                $tmpEndOfPeriod = $prevData['endOfPeriod' ] + $periodVal;
                                while( $tmpEndOfPeriod < $endOfPeriod  ){
                                    $data[ $tmpEndOfPeriod ][ 'balance' ] =  $prevData['balance' ];
                                    $data[ $tmpEndOfPeriod ][ 'equity' ] = $prevData['equity'];
                                    $data[ $tmpEndOfPeriod ][ 'date' ] = date("Y-m-d H:i:s", $tmpEndOfPeriod);
                                    $tmpEndOfPeriod += $periodVal;
                                }
                            }
                                    
                            $prevData = array(
                                'balance' => $accountBalance,
                                'equity' => $tmpEquity,
                                'endOfPeriod' => $endOfPeriod
                            );
                                    
                            $data[ $endOfPeriod ][ 'balance' ] =  $accountBalance;
                            $data[ $endOfPeriod ][ 'equity' ] = $tmpEquity;
                            $data[ $endOfPeriod ][ 'date' ] = date("Y-m-d H:i:s", $endOfPeriod);
                            $tmpEquity = false;    
                        }
                    }else{
                        $data[ $model->timestamp ][ 'balance' ] = $accountBalance;
                        $data[ $model->timestamp ][ 'equity' ] = $tmpEquity;
                        $data[ $model->timestamp ][ 'date' ] = date("Y-m-d H:i:s", $model->timestamp);
                    }
                }
            }
        }
        
        
        $abscissa = array();
        $balance = array();
        $equity = array();
        
        if( $data ){
            $maxY = 0;
            $minY = 100000;
            foreach( $data as $timeStamp => $oneData ){
                if( $oneData['balance'] > $maxY ) $maxY = $oneData['balance'];
                if( $oneData['equity'] > $maxY ) $maxY = $oneData['equity'];
                
                if( $oneData['balance'] < $minY ) $minY = $oneData['balance'];
                if( $oneData['equity'] < $minY ) $minY = $oneData['equity'];
                
                $abscissa[] = $timeStamp ;
                $balance[] = $oneData['balance'];
                $equity[] = $oneData['equity'];
            }
        }else{
            $maxY = 100;
            $minY = 0;
            $abscissa[] = 0 ;
            $balance[] = 0;
            $equity[] = 0;
        }
        return array(
            'abscissa' => $abscissa,
            'balance' => $balance,
            'equity' => $equity,
            'maxY' => $maxY,
            'minY' => $minY
        );
    }
    private function renderMonitoring($style, $informerColors){
        if( ! $accId = $this->cIsset('accId') ) return false;
        if( !$member = ContestMemberModel::model()->findByPk($accId) ) return false;

        $getParams = $this->getParams();
        
        if( !isset( $getParams['w'] ) || !$getParams['w'] ){
            $width = $style->width . 'px';
        }else{
            $width = intval($getParams['w']) . 'px';;
        }
        

        $this->render( $this->getCleanClassName() . "/styles/{$style->viewFilesDir}/{$style->viewFile}/view" , Array(
            'member' => $member,
            'width' => $width,
            'informerColors' => $informerColors,
            'settings' => $style->customSettings,
        ));
    }
    private function renderMonitoringForForum($style, $informerColors){
        if( ! $accId = $this->cIsset('accId') ) return false;
        if( !$member = ContestMemberModel::model()->findByPk($accId) ) return false;
        
        $this->render( $this->getCleanClassName() . "/styles/{$style->viewFilesDir}/{$style->viewFile}/view" , Array(
            'member' => $member,
        ));
        
    }
    private function renderContestForForum($style){
        $contest = ContestModel::model()->find(Array(
            'condition' => " `t`.`status` = 'registration' or `t`.`status` = 'started' ",
            'order' => " `t`.`createdDT` DESC",
        ));
        if( !$contest ) return false;
        $this->render( $this->getCleanClassName() . "/styles/{$style->viewFilesDir}/{$style->viewFile}/view" , Array(
            'contest' => $contest
        ));
        
    }
    private function renderNews($style, $informerColors){
        $getParams = $this->getParams();

        if( !isset( $getParams['w'] ) ){
            $width = $style->width . 'px';
        }elseif( !$getParams['w'] ){
            $width = '100%';
        }else{
            $width = intval($getParams['w']) . 'px';;
        }
        
        if( !$this->cIsset('cat') || !$this->cIsset('mult') || !$this->cIsset('items') ) return false;
        $category = $this->getCategory();
        if( !$category ) return false;
    
        $mult = floatval( $getParams['mult'] );
        if( !$mult ) return false;
    
        $items = $category->getItemsByIds( $getParams['items'] );
        if( !$items ) return false;

        if( !isset($getParams['slts']) ){
            $slts = 1;
        }else{
            $slts = $getParams['slts'];
        }

        $this->render( $this->getCleanClassName() . "/styles/{$style->viewFilesDir}/{$style->viewFile}/view" , Array(
            'model' => $style,
            'category' => $category,
            'mult' => $mult,
            'items' => $items,
            'informerColors' => $informerColors,
            'width' => $width,
            'slts' => $slts,
        ));
    }
    private function renderCalendar($style){
        $getParams = $this->getParams();

        $this->render( $this->getCleanClassName() . "/styles/{$style->viewFilesDir}/{$style->viewFile}/view" , Array(
            'impact' => $getParams['impact'],
            'symbols' => $getParams['symbols'],
            'update' => $getParams['update'],
        ));
    }
    
    function run() {
        if( !$this->cIsset('st') ) return false;
        $style = $this->getStyle();
        if( !$style ) return false;
        
        $informerColors = $this->restoreColorsFromGet();

        if( $style->viewFilesDir == 'quotes,currencies,cbrfMetals' ){
            $this->renderQuotesCurrencies($style, $informerColors);
            return true;
        }
        if( $style->viewFilesDir == 'converter' ){
            $this->renderConverter($style, $informerColors);
            return true;
        }
        if( $style->viewFilesDir == 'monitoring' ){
            $this->renderMonitoring($style, $informerColors);
            return true;
        }
        if( $style->viewFilesDir == 'monitoringForForum' ){
            $this->renderMonitoringForForum($style, $informerColors);
            return true;
        }
        if( $style->viewFilesDir == 'contestForForum' ){
            $this->renderContestForForum($style);
            return true;
        }
        if( $style->viewFilesDir == 'news' ){
            $this->renderNews($style, $informerColors);
            return true;
        }
        if( $style->viewFilesDir == 'calendar' ){
            $this->renderCalendar($style);
            return true;
        }
        
    }
}
?>