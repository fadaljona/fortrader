<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.UserMessageFormModel' );
	
	final class UsersConversationWidget extends WidgetBase {
		public $ajax = false;
		public $instance;
		public $idLinkedObj;
		public $limitMessages = 5;
		public $idLastMessage;
		public $idFirstMessage;
		private function getFormModel() {
			$formModel = new UserMessageFormModel();
			return $formModel;
		}
		private function getInstance() {
			if( $this->instance ) return $this->instance;
			return "{$this->controller->id}/{$this->controller->action->id}";
		}
		private function getIDLinkedObj() {
			if( $this->idLinkedObj ) return $this->idLinkedObj;
			$id = (int)@$_GET['id'];
			return $id ? $id : null;
		}
		private function getIDLastMessage() {
			if( $this->idLastMessage ) return $this->idLastMessage;
			$id = (int)@$_GET['idLastMessage'];
			return $id ? $id : null;
		}
		private function getIDFirstMessage() {
			if( $this->idFirstMessage ) return $this->idFirstMessage;
			$id = (int)@$_GET['idFirstMessage'];
			return $id ? $id : null;
		}
		private function getCriteriaMessages( $forCount = false ) {
			//$userMessageControl = Yii::App()->user->checkAccess( 'userMessageControl' );
			$c = new CDbCriteria();
			
			if( !$forCount ) {
				$c->select = Array( 'id', 'idUser', 'idMessageQuery', 'text', 'createdDT' );
				
				$c->with[ 'user' ] = Array(
					'select' => Array( 'ID', 'user_login', 'user_nicename', 'display_name', 'firstName', 'lastName' ),
				);
				$c->with[ 'user.avatar' ] = Array(
					'select' => Array( 'idUser', 'thumbPath' ),
				);
				
				//if( $userMessageControl ) {
					$c->with[ 'user.groups' ] = Array(
						'select' => Array( 'id', 'name' ),
					);
				//}
				
				if( !Yii::App()->user->isGuest ) {
					$c->with[ 'messageQuery' ] = Array(
						Array( 'id', 'idUser' ),
					);
				}
			}
			
			$instance = $this->getInstance();
			$c->addCondition( " `t`.`instance` = :instance " );
			$c->params[ ':instance' ] = $instance;
			
			$idLinkedObj = $this->getIDLinkedObj();
			if( $instance != 'question/list' or $idLinkedObj ) {
				$c->addCondition( " IF( :idLinkedObj IS NULL, `t`.`idLinkedObj` IS NULL, `t`.`idLinkedObj` = :idLinkedObj ) " );
				$c->params[ ':idLinkedObj' ] = $idLinkedObj;
			}
			
			$idLastMessage = $this->getIDLastMessage();			
			if( $idLastMessage ) {
				$c->addCondition( " `t`.`id` < :idLastMessage " );
				$c->params[ ':idLastMessage' ] = $idLastMessage;
			}
			
			$idFirstMessage = $this->getIDFirstMessage();			
			if( $idFirstMessage ) {
				$c->addCondition( " `t`.`id` > :idFirstMessage " );
				$c->params[ ':idFirstMessage' ] = $idFirstMessage;
			}
			
			if( !$forCount ) {
				$c->order = " `t`.`id` DESC ";
				$c->limit = $this->limitMessages;
			}
			
			return $c;
		}
		private function getMessages() {
			$c = $this->getCriteriaMessages();
			$messages = UserMessageModel::model()->findAll( $c );
			return $messages;
		}
		function run() {
			$watchModel = ConversationWatchModel::instance( $this->getIDLinkedObj() );
			if( $watchModel )
				$watchModel->save();
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->ajax,
				'formModel' => $this->getFormModel(),
				'instance' => $this->getInstance(),
				'idLinkedObj' => $this->getIDLinkedObj(),
			));
		}
		function renderMessages() {
			$messages = $this->getMessages();
			foreach( $messages as $message ){
				$this->renderMessage( $message );
			}
		}
		function renderMessage( $message ) {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/message", Array(
				'message' => $message,
				'instance' => $this->getInstance(),
			));
			$this->idLastMessage = $message->id;
		}
		function issetMoreMessages() {
			$c = $this->getCriteriaMessages( true );
			$exists = UserMessageModel::model()->exists( $c );
			return $exists;
		}
	}

?>