<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesArchiveProfileWidget extends WidgetBase {
		public $model;
		public $shortDesc;
		public $fullDesc;
		
		public $year;
		public $mo;
		public $day;
		
		private $period = false;
		private $minData = false;
		private $maxData = false;
		private $dayDataModel = false;
		private $dateStr = false;
		private $delimiterDate = false;
		
		private $minDatePointer;
		private $maxDatePointer;
		
		private $borderDates;
		
		private function createPartDp( $dateStr, $type, $limit = 3 ){
			if( $type == 'before' ){
				$sign = '<=';
				$order = 'DESC';
			}elseif( $type == 'after' ){
				$sign = '>';
				$order = 'ASC';
			}else{
				return false;
			}
			
			$c = new CDbCriteria();

			$c->addCondition( " `t`.`idCurrency` = :id ");
			$c->addCondition( " `t`.`date` $sign :date ");
			$c->params[':id'] = $this->model->id;
			$c->params[':date'] = $dateStr;
			$c->order = " `date` $order ";
			$c->limit = $limit;
			$c->with = array( 'currency' );
			
			$modelName = CurrencyRatesModel::getHistModelByDataType($this->model->dataType);
			
			$DP = new CActiveDataProvider( $modelName, Array(
				'criteria' => $c,
				'pagination' => false
			));
			if( $type == 'after' ){
				$DP->setData(array_reverse($DP->getData()));
			}
			return $DP;
		}

		private function createHistDP() {
			if( !$this->period ){
				$dp1 = $this->createPartDp( $this->dayDataModel->date, 'after', 3 );
				$dp2 = $this->createPartDp( $this->dayDataModel->date, 'before', 4 );

				$records = array();
				for ($i = 0; $i < $dp1->itemCount; $i++) { array_push($records, $dp1->data[$i] );}
				for ($i = 0; $i < $dp2->itemCount; $i++) { array_push($records, $dp2->data[$i] );}
				return $provAll = new CArrayDataProvider($records, array( 'pagination' => false ) );
			}else{
				$minTime = strtotime( $this->minData['date'] );
				$maxTime = strtotime( $this->maxData['date'] );
				if( $minTime <= $maxTime ){
					$first = 'maxData';
					$second = 'minData';
				}else{
					$first = 'minData';
					$second = 'maxData';
				}
				
				$dp1 = $this->createPartDp( $this->{$first}['date'], 'after', 1 );
				$dp2 = $this->createPartDp( $this->{$first}['date'], 'before', 3 );
				$dp3 = $this->createPartDp( $this->{$second}['date'], 'after', 1 );
				$dp4 = $this->createPartDp( $this->{$second}['date'], 'before', 2 );
				$records = array();
				for ($i = 0; $i < $dp1->itemCount; $i++) { array_push($records, $dp1->data[$i]); }
				for ($i = 0; $i < $dp2->itemCount; $i++) { array_push($records, $dp2->data[$i]); if( $i==2 ) $this->delimiterDate = $dp2->data[$i]->date; }
				for ($i = 0; $i < $dp3->itemCount; $i++) { array_push($records, $dp3->data[$i]); }
				for ($i = 0; $i < $dp4->itemCount; $i++) { array_push($records, $dp4->data[$i]); }
				return $provAll = new CArrayDataProvider($records, array( 'pagination' => false ) );
				
			}
		}
		
		private function setDataModels(){
			if( !$this->mo || !$this->day ){
				$this->period = true;
				$this->minData = $this->model->getMinMaxDataModelForPeriod( $this->year, $this->mo, 'min' );
				$this->maxData = $this->model->getMinMaxDataModelForPeriod( $this->year, $this->mo, 'max' );
				if( !$this->minData || !$this->maxData ) throw new CHttpException(404,'Page Not Found');
			}else{
				$this->dayDataModel = $this->model->getDayData( $this->year, $this->mo, $this->day );
			}
			
			if( $this->mo && $this->day ){
				$strMo = $this->mo < 10 ? '0' . $this->mo : $this->mo;
				$strDay = $this->day < 10 ? '0' . $this->day : $this->day;
				$this->dateStr = $this->year . "-$strMo-$strDay";
			}
		}
		private function detectMinMaxDates(){
			$minTime = strtotime( $this->minData['date'] );
			$maxTime = strtotime( $this->maxData['date'] );
			if( $minTime <= $maxTime ){
				$this->minDatePointer = 'minData';
				$this->maxDatePointer = 'maxData';
			}else{
				$this->minDatePointer = 'maxData';
				$this->maxDatePointer = 'minData';
			}
		}
		private function getChartData(){
			if( !$this->period ){
				return CurrencyRatesModel::getDataForRadius( 30, $this->model->id, $this->dayDataModel->date, $this->dayDataModel->date, $this->model->dataType );
			}else{
				$dates = $this->getBorderDates();
				return CurrencyRatesModel::getDataForRadius( 30, $this->model->id, $dates['left'], $dates['right'], $this->model->dataType );
			}
		}
		private function getEvents(){
			if( !$this->period ){
				return array(
					array(
						'date' => $this->dayDataModel->date,
						'type' => "triangleDown",
						"backgroundColor" => "#00CC00",
						"rollOverColor" => "#00CC00",
						"color" => "#000000",
						"graph" => "g1",
						"description" => Yii::app()->dateFormatter->format( 'd MMMM yyyy',strtotime( $this->dayDataModel->date ) ),
					)
				);
			}else{
				return array(
					array(
						'date' => $this->minData['date'],
						'type' => "triangleDown",
						"backgroundColor" => "#CC0000",
						"rollOverColor" => "#CC0000",
						"color" => "#000000",
						"graph" => "g1",
						"description" => Yii::app()->dateFormatter->format( 'd MMMM yyyy',strtotime( $this->minData['date'] ) ),
					),
					array(
						'date' => $this->maxData['date'],
						'type' => "triangleUp",
						"backgroundColor" => "#00CC00",
						"rollOverColor" => "#00CC00",
						"color" => "#000000",
						"graph" => "g1",
						"description" => Yii::app()->dateFormatter->format( 'd MMMM yyyy',strtotime( $this->maxData['date'] ) ),
					)
				);
			}
		}
		private function getBorderDates( ){
			if( $this->borderDates ) return $this->borderDates;
			if( !$this->period ){
				$this->borderDates = array(
					'left' => $this->dayDataModel->date,
					'right' => $this->dayDataModel->date
				);
			}else{
				if( $this->mo ){
					$strMo = $this->mo < 10 ? '0' . $this->mo : $this->mo;
					$fromDate = $this->year . "-$strMo-01";
					$toDate = $this->year . "-$strMo-" . cal_days_in_month(CAL_GREGORIAN, $this->mo, $this->year);
				}else{
					$fromDate = $this->year . '-01-01';
					$toDate = $this->year . '-12-31';
				}
				if( strtotime( $toDate ) > time() ) $toDate = date('Y-m-d');
				$this->borderDates = array(
					'left' => $fromDate,
					'right' => $toDate,
				);
			}
			return $this->borderDates;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->setDataModels();
			$this->detectMinMaxDates();
			
			$this->render( "{$class}/view", Array(
				'model' => $this->model,
				'currenciesForChangePage' => CurrencyRatesModel::getCurrenciesForChangePage( $this->model->dataType ),
				'popularCurrencies' => CurrencyRatesModel::getPopularCurrencies(),
				'histDp' => $this->createHistDP(),
				'chartData' => $this->getChartData(),
				'shortDesc' => $this->shortDesc,
				'fullDesc' => $this->fullDesc,
				'year' => $this->year,
				'mo' => $this->mo,
				'day' => $this->day,
				'period' => $this->period,
				'minData' => $this->minData,
				'maxData' => $this->maxData,
				'dayDataModel' => $this->dayDataModel,
				'dateStr' => $this->dateStr,
				'delimiterDate' => $this->delimiterDate,
				'chartEvents' => $this->getEvents(),
				'borderDates' => $this->getBorderDates(),
			));
		}
	}

?>