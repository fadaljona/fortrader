<?php
class Parser{
	public static function parse($files='',$format='json'){
		if(!$files)
			return false;
		$aResult=array();

		if(is_array($files)){
			foreach($files as $file)
				array_merge($aResult,self::parseFile($file));
		}else{
			$aResult = self::parseFile($files,$format);
		}

		switch($format){
			case 'json':
				$sResult=json_encode($aResult);
				break;
			case 'array':
				$sResult=$aResult;
				break;
			default:
				$sResult=null;
		}

		return $sResult;
	}

	public static function parseFile($file){
		try{
			if(file_exists($file))
				$aData=@file($file);
			else{
				$sData=@file_get_contents($file);
				$aData=array();
				preg_match_all('/(.+)\r\n/',$sData,$aData);
				$aData=$aData[0];
			}
		}catch (Exception $e){
			return array();
		}
		
		if(!$aData)
			return array();

		$aResult=array();
		
		$default_tz=date_default_timezone_get();
		date_default_timezone_set("UTC");
		$date=date("H:i:s d.m",time());
		date_default_timezone_set($default_tz);
		foreach($aData as $sString){
			$aElements=explode(';',$sString);
			$symbol=trim(str_replace('#','',$aElements[0]));
			$time=($aElements[3])?$aElements[3]:$aElements[4];

			$aResult[$symbol]=array(
				'symbol'=>$symbol,
				'bid'=>$aElements[1],
				'ask'=>$aElements[2],
				'time'=>$time,
				'rate'=>'',
				'time_up'=>$date,
				'spread'=>$aElements[5],
				'trend'=>$aElements[6],
				'start_trading'=>$aElements[7],
				'end_trading'=>$aElements[8]
			);
		}
		
		

		return $aResult;
	}
}