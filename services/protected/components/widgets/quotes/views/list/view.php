<?
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/widgets/wQuotesWidget.js");
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl .'/css/wQuotesWidget.css');
Yii::app()->clientScript->registerCssFile('//fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic');
?>

<div class="wQuotesWidget" id="wQuotesWidgetList">
	<script>var wQuotesWidgetList;</script>
	<div class="custom-select-box" style="float: left">
		<span class="text-select"></span>
		<? if(is_array($cats)){
			$default = $cats['default']; ?>
			<? echo CHtml::dropDownList('categories',$default,$cats['categories']) ?>
		<? } ?>
	</div>
	<div class="wButtonFlop" onclick="if(jQuery('#wSettingsQuotes').is(':visible')){jQuery('#wSettingsQuotes').hide(500)}else{jQuery('#wSettingsQuotes').show(500)}"></div>
<div class="iClear"></div>
	<div id="wSettingsQuotes">
	<div id="wSettingsBlock">
		<ul id="wSubCats">
			<? if(isset($cats['subcategories'][$default]) && is_array($cats['subcategories'][$default]) && !empty($cats['subcategories'][$default])){
				foreach($cats['subcategories'][$default] as $catId => $catName){ ?>
					<li data-id="<?= $catId ?>" class="wSubCats_0" >
						<a href="#"><?= $catName ?></a>
						<? if(isset($cats['subcategories'][$catId])){?>
							<ul class="wSubCats">
								<?foreach($cats['subcategories'][$catId] as $catId_1 => $catName_1){?>
								<li data-id="<?= $catId_1 ?>" class="wSubCats_1">
									<a href="#"><?=Yii::t( "quotesWidget",$catName_1)?></a>
								</li>
								<?}?>
							</ul>
						<?}?>
					</li>
				<? }}elseif($default==20){ 
				foreach($cats['categories'] as $catId => $catName){
					if($catId!=71){?>
				<li data-id="<?= $catId ?>" class="wSubCats_0" >
						<a href="#"><?= $catName ?></a>
					<? if(isset($cats['subcategories'][$catId])){?>
						<ul class="wSubCats">
								<?foreach($cats['subcategories'][$catId] as $catId_1 => $catName_1){?>
									<li data-id="<?= $catId_1 ?>" class="wSubCats_1">
									<a href="#"><?=Yii::t( "quotesWidget",$catName_1)?></a>
										<? if(isset($cats['subcategories'][$catId_1])){?>
										<ul class="wSubCats">
								<?foreach($cats['subcategories'][$catId_1] as $catId_2 => $catName_2){?>
											<li data-id="<?= $catId_2 ?>" class="wSubCats_1">
									<a href="#"><?=Yii::t( "quotesWidget",$catName_2)?></a></li>
								<?}?>
										</ul>
										<?}?>
								</li>
								<?}?>
							</ul>
					<?}?>
				</li>
			<? }}}?>
		</ul>
		<div id="wSettingsBlockBottom">
			<div class="pull-left" style="height: 60px;">
				<div style="height: 33px; margin-bottom: 10px">
					<form action="/" name="wSearchQuote" method="post" onsubmit="wQuotesWidgetList.onSubmitSearchQuote(this); return false;">
						<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
							'model' => $symbols_model,
							'attribute' => 'desc',
							'source' => Yii::app()->createUrl('quotes/suggest'),
							'options' => array(
								'delay' => 200,
								'minLength' => 2,
								'showAnim' => 'fold',
								'multiple' => false,
								'select' => "js:function(event, ui) {
														this.value = ui.item.label;
														$('#AdminQuotesSymbolsFormModel_id').val(ui.item.id);
														return false;}"
							),
							'htmlOptions' => array('style' => 'margin-bottom:0;')));
						?><div class="btn btn-small btn-info no-radius insUsersConversationWidget wSubmit " style="background-color: #3a87ad !important;border-color: #3a87ad !important;height: 22px;width: 72px;line-height: 22px;margin-left: 4px;" onclick="jQuery(this).parent('form').submit()"><?=Yii::t( "quotesWidget","Поиск")?></div>
						<input type="hidden" name="id_quote" id="AdminQuotesSymbolsFormModel_id" value="">
					</form>
				</div>
				<div>
					<span id="selectAllChecks" class="wChecksClick" ><?=Yii::t( "quotesWidget","выбрать все")?></span>&nbsp;
					<span id="defaultChecks" class="wChecksClick" style="margin-left: 23px"><?=Yii::t( "quotesWidget","по умолчанию")?></span>
				</div>
			</div>
			<div class="pull-right">
				<div id="wCheckBoxes">
					<? foreach($checks as $check): ?>
						<label class="checksBoxes iLabel i01">
							<input type="checkbox" id="quoteCheck_<?= $check['id'] ?>" name="quotesChecked" <?= $check['checked'] ?> value="<?= $check['id'] ?>" class="select-on-check">
							<span class="lbl"></span>&nbsp;<span class="wQuoteCheckBox"><?= Yii::t("quotesWidget",$check['name']) ?></span>
						</label>
					<? endforeach; ?>
				</div>
			</div>
		</div>
	</div>

	<div style="min-height: 50px; margin:18px 0px;">
		<div class="pull-right">
			<div style="float: right; height: 30px; line-height: 30px;">
				<span><?=Yii::t( "quotesWidget","Автоматическое обновление через")?></span> 
				<span id="timerLabel" class="timerLabel"><?= ($timer_default) ? $timer_default : 10 ?></span> 
				<span><?=Yii::t( "quotesWidget","секунд")?></span>
			</div>
			<div class="iClear"></div>
			<div style="float: right; height: 30px;">
				<div class="pull-right btn btn-small btn-info no-radius insUsersConversationWidget wSubmit" id="refreshTable"><?=Yii::t( "quotesWidget","Обновить")?></div>
				<div class="pull-right btn btn-small btn-info no-radius insUsersConversationWidget wSubmit" id="setTimer"><?=Yii::t( "quotesWidget","Применить")?></div>
				<div class="pull-right custom-select-box custom-select-box-small">
					<span class="text-select"></span>
					<? echo CHtml::dropDownList('timer',$timer_default,$timer) ?>
				</div>
				<span class="pull-right wTextRefresh"style="line-height: 30px;"><?=Yii::t( "quotesWidget","Обновлять каждые")?> </span>
			</div>
		</div>
	</div>
	</div>

<div class="iClear"></div>
<span class="wQuotesError">
	<span class="text"></span>
	<span class="wClose" onclick="jQuery(this).parent('span').hide()">x</span>
</span>
<?//=$debug_timer?>
<script type="text/javascript">
	<? if(isset($cats['categories'][71])) unset($cats['categories'][71]);?>
		wQuotesWidgetList=wQuotesWidget();
		wQuotesWidgetList.setOptions({
			idContainer: '#wQuotesWidgetList',
			idsCategories: <?=($cats)?json_encode($cats):''?>,
			defaultCheckIds:<?=($checksIds)?$checksIds:json_encode(array()) ?>,
			idCurrentCat: <?=($default)?$default:0 ?>,
			iTimer:<?=($timer_default)?$timer_default:10?>,
			iTimerLabel: ''
		});
		
</script>
<div id="yw1" class="iGridView i01 grid-view insAdminCountriesListWidget wCountriesGridView">
	<div id="wQuotesTableWidget">
<?= $quotes ?>
	</div>
	<script type="text/javascript">
		wQuotesWidgetList.init();
	</script>
</div>
	</div>