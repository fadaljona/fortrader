<table class="items table table-bordered quotesTable">
<thead>
<tr>
	<th width="30px"></th>
	<? $columns_=array(); foreach($columns as $key=>$column): $columns_[]=$key;?>
	<th><?=Yii::t( "quotesWidget",$column)?></th>
	<? endforeach; ?>
</tr>
</thead>
<tbody>
<?php 
$i=$j=1;
$idsQuotes=array();
$Quotes=array();
foreach($quotes as $nameCat=>$quotesByCat){
	if($nameCat!='ids_quotes'){
		$cat=explode('__',$nameCat);
		$rows='';
		$rowsCount=0;
		foreach($quotesByCat as $row){
			if($row->default=="Yes"){
				$idsQuotes[]=$row->id;
				$Quotes[$row->id]=(array)$row;
				$rows.='<tr class="'.(($i % 2) ? 'even' : 'odd').' wLineId'.$row->id.'" data-id="hideShow_'.$j.'" idSymbol="'.$row->id.'">';
				$i++;
				$rowsCount++;
				$rows.='<td onmouseover="jQuery(this).find(\'div.wHideLine\').show()" onmouseout="jQuery(this).find(\'div.wHideLine\').hide()"><div class="wHideLine" data-id="'.$row->id.'"></div></td>';
				foreach($columns as $key => $column){
						if($key == 'name'){
							$rows.='<td class="cell_'.$key.'">
							<div class="'.(((int)$row->trend == 1) ? 'wArrow wArrowGreen' : (((int)$row->trend == -1) ? 'wArrow wArrowRed' : '')).'"></div>
							<span class="wTextCellName" title="'.Yii::t( "quotesWidget",$row->desc?$row->desc:$row->name).'">'.Yii::t( "quotesWidget",$row->nalias?$row->nalias:$row->name).'</span>
							</td>';
						}else{
							$rows.='<td class="cell_'.$key.'"><span class="wTextCell">'.$row->$key.'</span></td>';
						}
					}
				$rows.='</tr>';
			}} ?>
			
		<? if($rows){ ?>
		<tr class="wNameCategory wCategory_<?=$cat[1]?>" data-hide="0" data-iter="<?= $j ?>" data-count="<?= $rowsCount ?>">
			<td colspan="10" style="text-align: center"><div class="wArrow wArrowDown"></div><?=Yii::t( "quotesWidget",$cat[0]) ?></td>
		</tr>
		<? echo $rows;}
		$j++;
	}}?>
</tbody>
</table>
<script type="text/javascript">
wQuotesWidgetList.setOptions({
	idsQuotes: <?=json_encode($idsQuotes)?>,
	columns:<?=json_encode($columns_)?>,
	quotes:<?=json_encode($Quotes)?>
});
</script>
