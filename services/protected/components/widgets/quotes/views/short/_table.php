<table class="items table table-bordered quotesTable" style="width: 420px !important;">
<thead>
<tr>
	<? $columns_=array(); foreach($columns as $key=>$column): $columns_[]=$key;?>
		<th><?=Yii::t( "quotesWidget",$column)?></th>
	<? endforeach; ?>
</tr>
</thead>
<tbody>
<?php
$i=$j=1;
$quotesWithOutCat=array();
$idsQuotes=array();
foreach($quotes as $nameCat=>$quotesByCat){
	if($nameCat!='ids_quotes'){
		$cat=explode('__',$nameCat);
		$rows='';
		foreach($quotesByCat as $row){
			if($row->default=="Yes"){
				$quotesWithOutCat[$row->id] = (array)$row;
				$rows.='<tr class="'.(($i % 2) ? 'even' : 'odd').' wLineId'.$row->id.'" data-id="hideShow_'.$j.'" idSymbol="'.$row->id.'">';
				$i++;
				foreach($columns as $key => $column){
					if($key == 'name'){
						$rows.='<td class="cell_'.$key.'"><div class="'.(((int)$row->trend == 1) ? 'wArrow wArrowGreen' : (((int)$row->trend == -1) ? 'wArrow wArrowRed' : '')).'"></div><span class="wTextCellName">'.$row->name.'</span></td>';
					}else{
						$rows.='<td class="cell_'.$key.'"><span class="wTextCell">'.$row->$key.'</span></td>';
					}
				}
				$rows.='</tr>';
			}} ?>
			<? echo $rows;
		$j++;
	}}?>
</tbody>
</table>
<script type="text/javascript">
wQuotesWidgetShort.setOptions({
	"prevQuotes":<?=json_encode($quotesWithOutCat)?>,
	"idsQuotes": <?=json_encode($idsQuotes)?>,
	"columns":<?=json_encode($columns_)?>
});
</script>
