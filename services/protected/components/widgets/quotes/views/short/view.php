<?
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/widgets/wQuotesWidget.js");
?>
<script type="text/javascript">	
	var wQuotesWidgetShort=wQuotesWidget();
	wQuotesWidgetShort.setOptions({
			idContainer: '#wQuotesWidgetShort',
			idsCategories: <?=($cats)?json_encode($cats):''?>,
			defaultCheckIds:<?=($checksIds)?$checksIds:json_encode(array()) ?>,
			idCurrentCat: <?=($default)?$default:0 ?>,
			iTimer:<?=($timer_default)?$timer_default:10?>,
			iTimerLabel: '',
			idsQuotes: <?=($ids_quotes?$ids_quotes:json_encode(array()))?>
		});
	wQuotesWidgetShort.init();
</script>
<div class="iClear"></div>
<div id="wQuotesWidgetShort">
<div id="yw1" class="iGridView i01 grid-view insAdminCountriesListWidget wCountriesGridView">
	<div id="wQuotesTableWidget">
	<?= $quotes ?>
	</div>
</div>
	</div>
<div class="iClear"></div>