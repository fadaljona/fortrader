<?
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/widgets/wQuotesWidget.js");
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl .'/css/wQuotesWidget.css');
Yii::app()->clientScript->registerCssFile('//fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic');
?>

<div class="wQuotesWidget" id="wQuotesWidgetList">
	<script>var wQuotesWidgetList;</script>
	<div class="custom-select-box" style="float: left">
		<span class="text-select"></span>
		<? if(is_array($cats)){
			$default = $cats['default']; ?>
			<? echo CHtml::dropDownList('categories',$default,$cats['categories']) ?>
		<? } ?>
	</div>
	<div class="wButtonFlop" onclick="if(jQuery('#wSettingsQuotes').is(':visible')){jQuery('#wSettingsQuotes').hide(500)}else{jQuery('#wSettingsQuotes').show(500)}"></div>
	<div id="loaderCategory" style="float: left;margin: 4px 0 0 5px;width: 15px;"></div>
<div class="iClear"></div>
	<div id="wSettingsQuotes">
	<div id="wSettingsBlock">
		<ul id="wSubCats">
				<? foreach($cats['categories'] as $catId => $catName){
					if($catId!=71){?>
						<li data-id="<?= $catId ?>" class="wSubCats_0" >
						<a href="#"><?= $catName ?></a>
							<? if(isset($cats['subcategories'][$catId])){?>
								<ul class="wSubCats">
								<?foreach($cats['subcategories'][$catId] as $catId_1 => $catName_1){?>
									<li data-id="<?= $catId_1 ?>" class="wSubCats_1">
									<a href="#"><?=Yii::t( "quotesWidget",$catName_1)?></a>
										<? if(isset($cats['subcategories'][$catId_1])){?>
											<ul class="wSubCats">
								<?foreach($cats['subcategories'][$catId_1] as $catId_2 => $catName_2){?>
									<li data-id="<?= $catId_2 ?>" class="wSubCats_1">
									<a href="#"><?=Yii::t( "quotesWidget",$catName_2)?></a></li>
								<?}?>
										</ul>
										<?}?>
								</li>
								<?}?>
							</ul>
							<?}?>
				</li>
					<? }}?>
		</ul>
		<div id="wSettingsBlockBottom">
			<div class="pull-left" style="height: 60px;">
				<div style="height: 33px; margin-bottom: 10px">
					<form action="/" name="wSearchQuote" method="post" onsubmit="wQuotesWidgetList.onSubmitSearchQuote(this); return false;">
						<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
							'model' => $symbols_model,
							'attribute' => 'desc',
							'source' => Yii::app()->createUrl('quotes/suggest'),
							'options' => array(
								'delay' => 200,
								'minLength' => 2,
								'showAnim' => 'fold',
								'multiple' => false,
								'select' => "js:function(event, ui) {
														this.value = ui.item.label;
														$('#AdminQuotesSymbolsFormModel_id').val(ui.item.id);
														return false;}"
							),
							'htmlOptions' => array('style' => 'margin-bottom:0;')));
						?><div class="btn btn-small btn-info no-radius insUsersConversationWidget wSubmit " style="background-color: #3a87ad !important;border-color: #3a87ad !important;height: 22px;width: 72px;line-height: 22px;margin-left: 4px;" onclick="jQuery(this).parent('form').submit()"><?=Yii::t( "quotesWidget","Поиск")?></div>
						<input type="hidden" name="id_quote" id="AdminQuotesSymbolsFormModel_id" value="">
					</form>
				</div>
				<div>
					<span id="refreshPanel" class="wChecksClick"><?=Yii::t( "quotesWidget","сброс панели")?></span>
				</div>
				<div>
					<span><?=Yii::t( "quotesWidget","Скорость обновления панели:")?></span>&nbsp;<input type="text" size="4" maxlength="4" name="time_update_panel" style="margin:0;width: 30px" value="<?=$time_update_panel?>">
					&nbsp;<span id="timerPanel" style="width: 15px"></span>&nbsp;<span><?=Yii::t( "quotesWidget","секунд")?></span>
				</div>
			</div>
			<div class="pull-right">
				<div id="wCheckBoxes">
					<? foreach($checks as $check): ?>
						<label class="checksBoxes iLabel i01">
							<input type="checkbox" id="quoteCheck_<?= $check['id'] ?>" name="quotesChecked" <?= $check['checked'] ?> value="<?= $check['id'] ?>" class="select-on-check">
							<span class="lbl"></span>&nbsp;<span class="wQuoteCheckBox"><?=Yii::t("quotesWidget",$check['name']) ?></span>
						</label>
					<? endforeach; ?>
				</div>
			</div>
		</div>
	</div>
	</div>

<div class="iClear"></div>
<span class="wQuotesError">
	<span class="text"></span>
	<span class="wClose" onclick="jQuery(this).parent('span').hide()">x</span>
</span>
	<?//=$debug_timer?>
	<script type="text/javascript">
	<? if(isset($cats['categories'][71])) unset($cats['categories'][71]);?>
	wQuotesWidgetList=wQuotesWidget();
	wQuotesWidgetList.setOptions({
		idContainer: '#wQuotesWidgetList',
		idsCategories: <?=($cats)?json_encode($cats):json_encode(array())?>,
		oPanel:(( typeof QuotesPanel !== undefined )?QuotesPanel:null),
		defaultCheckIds:<?=($checksIds)?$checksIds:json_encode(array()) ?>,
		mode:"editPanel",
		ls:<?=$ls?>,
		idCurrentCat: 71,
		iTimer:<?=($timer_default)?$timer_default:10?>,
		iTimerLabel: ''
	});
		
</script>
<div id="yw1" class="iGridView i01 grid-view insAdminCountriesListWidget wCountriesGridView">
	<div id="wQuotesTableWidget">
<?= $quotes ?>
	</div>
	<script type="text/javascript">
		wQuotesWidgetList.init();
	</script>
</div>
	</div>