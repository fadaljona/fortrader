<?
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/widgets/wQuotesPanel.js");
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl .'/css/wQuotesPanel.css');
?>
<div class="wQuotesPanel" id="wQuotesPanel">
		<div class="wQuotes">
			<? $ids=$Quotes=array();
			foreach($quotes as $quote){ 
				$ids[]=$quote['id'];
				$Quotes[]=(array)$quote;?>
			<div class="wQuoteItem" id="wQuoteItem__<?=$quote['id']?>">
				<span class="wQuoteItemText" title="<?=Yii::t("quotesWidget",$quote['desc'])?>"><span class="wQuoteItemName"><?=Yii::t("quotesWidget",$quote['nalias']?$quote['nalias']:$quote['name'])?></span><span class='wSeparate'>:</span></span>
				<span class="wQuoteItemPrice"><?=$quote['bid']?></span>
				<div class="wQuoteItemDelete" title="<?=Yii::t( "quotesWidget", 'Удалить с панели' )?>" data-id="<?=$quote['id']?>"><div class="wQuoteItemBtnImage"></div></div>
				<div class="wQuoteItemTrend <?=($quote['trend']==1?'wGreen':'wRed')?>"></div>
			</div>
			<?}?>
		</div>
	<div class="wPlusBtn" title="<?=Yii::t( "quotesWidget", 'Добавить' )?>"></div>
</div>
<script>
	var QuotesPanel=wQuotesPanel();
	QuotesPanel.setOptions({
		iTimer:<?=(isset($time_update_panel)?$time_update_panel:10) ?>,
		logged:<?=$logged?1:0?>,
		ls:<?=json_encode($ls)?>,
		quotes:<?=json_encode($Quotes)?>,
		ids:<?=json_encode($ids)?>});
	QuotesPanel.init();
</script>