<?php

class QuoteModel {
	private static $_aliases=array();
	public $id;
	public $name;
	public $nalias;
	public $desc;
	public $default;
	public $category_id;
	public $category_name;
	public $rate;
	public $max;
	public $min;
	public $volume;
	public $spread;
	public $last_trade;
	public $open_trade;
	public $bid;
	public $visible;
	public $ask;
	public $time;
	public $time_up;
	public $start_trading;
	public $end_trading;
	public $trend;
	//Алисы
	public $alias=array();
	
	public function setAlias(){
		if(empty(self::$_aliases)){
			if($this->id&&$this->name){
				$aliases=QuotesAliasModel::model()->findAll();
				foreach($aliases as $alias){
					self::$_aliases[$alias->symbol][]=$alias;
				}
			}
		}
		if( isset(self::$_aliases[$this->id]) && self::$_aliases[$this->id] )
			$this->alias[$this->name]=self::$_aliases[$this->id];
	}
}