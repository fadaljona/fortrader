<?php
Yii::import('components.widgets.base.WidgetBase');
Yii::import('components.widgets.quotes.QuotesModel');

class QuotesPanelWidget extends WidgetBase {
	public $view="panel";
	
	protected function ctrlAccess($admin=false){
		if($id=Yii::app()->user->id)
			$user=UserToUserGroupModel::model()->find('`idUser`='.$id);
		if($admin){
			if($user){
				return intval($user->idUserGroup)==1;
			}else{
				return false;
			}
		}elseif($user){
			return array('id'=>$user->id);
		}else{
			return false;
		}
	}

	public function run(){
		/*if(!$this->ctrlAccess(true))
			return false;*/
		
		$quotes=QuotesModel::getQuotesByPanel();
		
		$this->render($this->view.'/view',array(
			'quotes'=>$quotes,
			'time_update_panel'=>QuotesModel::getTimer(),
			'logged'=>(Yii::app()->user->id?true:false),
			'ls'=>array(
				'lLoginForSave'=>Yii::t( "quotesWidget", 'Для сохранения настроек необходимо войти на сайт или зарегистрироваться' ),
				'deleteItem'=>Yii::t( "quotesWidget", 'Удалить с панели' ),
				'addItem'=>Yii::t( "quotesWidget", 'Добавить' )
			)
		));
	}
} 