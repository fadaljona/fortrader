<?
Yii::import('components.widgets.base.WidgetBase');
Yii::import('components.widgets.quotes.QuotesModel');

final class QuotesWidget extends WidgetBase{
	public $view;
	public $idCat; //по умолчанию id системной категории
	public $columns = 'columns';
	public $symbols = array();
	public $action;
	public $ajaxSearchData;
	public $format = 'array';
	public $timer;
	public $ajax = false;
	public $symbols_model;
	private $_columns;

	private function getQuotes($cats,$only_default = false,$ids = array(),$with_panel = false){
		if(!$cats && !$ids){
			return array();
		}
		$result = array();
		if($cats){
			foreach($cats as $category){
				if($quotes = QuotesModel::getQuotesByCatId($category->id,$only_default,$with_panel)){
					$result[ $category->name."__".$category->id ] = $quotes;
				}
				foreach($quotes as $quote){
					$result['ids_quotes'][] = $quote->id;
				}
			}
			$result['ids_quotes'] = json_encode($result['ids_quotes']);
		}elseif($ids){
			if($quotes = QuotesModel::getQuotesByIds($ids,'object')){
				foreach($quotes as $quote){
					$result[ $quote->category_name."__".$quote->category_id ][] = $quote;
				}
			}
			$result['ids_quotes'] = json_encode($ids);
		}

		return $result;
	}

	private function getQuotesAjax($only_default = true,$with_panel = false){
		$result = array();
		if($this->symbols){
			$result = QuotesModel::getQuotesByIds($this->symbols);
		}elseif($this->idCat){
			$cats   = $this->getCategoryWithChildes($this->idCat);
			$result = $this->getQuotes($cats,$only_default,array(),$with_panel);
		}

		return json_encode($result);
	}

	private function getCategories(){
		$aCats   = QuotesCategoryModel::getAll();
		$aResult = array();

		foreach($aCats as $cat){
			if((int)$cat->parent === 0){
				$aResult['categories'][ $cat->id ] = Yii::t("quotesWidget",$cat->name);
			}else{
				$aResult['subcategories'][ $cat->parent ][ $cat->id ] = Yii::t("quotesWidget",$cat->name);
			}

			if((int)$cat->is_def === 1){
				$aResult['default'] = $cat->id;
			}
		}

		if(!$aResult['default']){
			$aResult['default'] = 20;
		}

		return $aResult;
	}

	private function getCategoryWithChildes($idCat = 0){
		$categories = array();
		if($idCat){
			if((int)$idCat == 20){
				$categories = QuotesCategoryModel::getAll();
			}else{
				$categories = array_merge($categories,QuotesCategoryModel::model()->findAll(array(
					'condition' => '`parent`='.$idCat,
					'order'     => '`order`'
				)));
				$_cats      = array();

				foreach($categories as $category){
					if($result = QuotesCategoryModel::model()->findAll(array(
						'condition' => '`parent`='.(int)$category->id,
						'order'     => '`order`'
					))
					){
						$_cats = array_merge($_cats,$result);
					}
				}
				$categories = array_merge($categories,$_cats);
				array_push($categories,QuotesCategoryModel::model()->find('`id`='.$idCat));
			}
		}

		return $categories;
	}

	private function renderTable(&$quotes,$idsQuotesPanel=null){
		return $this->render($this->view."/_table",
			Array(
			'quotes' => $quotes,
			'idsQuotesPanel'=>$idsQuotesPanel?$idsQuotesPanel:null,
			'columns' => $this->_columns)
			,true
		);
	}

	private function getChecks(&$quotes){
		$checks = array();
		foreach($quotes as $nameCat => $quote){
			foreach($quote as $quoteOne){
				$checks[] = array(
					'id'      => $quoteOne->id,
					'name'    => ($quoteOne->nalias?$quoteOne->nalias:$quoteOne->name),
					'checked' => ($quoteOne->default=="Yes")?'checked':''
				);
			}
		}

		return $checks;
	}

	private function getAjaxChecks(){
		$result = array();
		$checks = array();
		$cats   = $this->getCategoryWithChildes($this->idCat);

		foreach($cats as $cat){
			$checks[ $cat->id ] = QuotesSymbolsModel::model()->findAll(array(
				'select'    => '`id`,`nalias`,`name`,`default`,`category`,`desc`',
				'condition' => '`visible`="Visible" AND `category`='.(int)$cat->id
			));
		}

		if($checks){
			foreach($checks as $cat_id => $checks_){
				if($checks_){
					$result['html'] .= $this->render($this->view."/_checks",Array('checks' => $checks_),true);
					foreach($checks_ as $check){
						if($check->default == "Yes"){
							$result['default'][] = $check->id;
						}
					}
				}
			}
		}

		return $result;
	}

	private function getAllChecks($checkeds = array()){
		$checks = array();
		$quotes = QuotesModel::getAllQuotes();

		if($checkeds && is_array($checkeds)){
			foreach($quotes as $quote){
				$checks[] = array('id'      => $quote->id,
								  'name'    => ($quote->nalias?$quote->nalias:$quote->name),
								  'checked' => (in_array($quote->id,$checkeds))?'checked':''
				);
			}
		}else{
			foreach($quotes as $quote){
				$checks[] = array('id'      => $quote->id,
								  'name'    => ($quote->nalias?$quote->nalias:$quote->name),
								  'checked' => ($quote->default=="Yes")?'checked':''
				);
			}
		}

		return $checks;
	}
	
	private function getDefaultChecks(){
		$ids    = array();
		$checks = QuotesSymbolsModel::model()->findAll('`default`="Yes"');
		foreach($checks as $check){
			if($check->default == "Yes"){
				$ids[] = $check->id;
			}
		}

		return $ids;
	}

	private function getAjaxSearch(){
		$result = array();

		if($this->ajaxSearchData){
			if($this->ajaxSearchData['id_quote']){
				$result['result'] = QuotesModel::getQuotesByIds(array($this->ajaxSearchData['id_quote']));
			}elseif($this->ajaxSearchData['name_quote']){
				$result['result'] = QuotesModel::searchQuotes($this->ajaxSearchData['name_quote']);
			}
		}
		$result['name'] = Yii::t("quotesWidget","Результаты поиска");

		return $result;
	}

	function run(){
		$start = microtime(true);
		if(!$this->view){
			$this->view = "list";
		}
		/*--------START AJAX--*/
		if($this->ajax){
			switch($this->action){
				case 'getChecks':
					$response = $this->getAjaxChecks();
					if($response){
						$response = json_encode(array(
							'html'    => $response['html'],
							'default' => $response['default']
						));
					}else{
						$response = json_encode(array(
							'error' => Yii::t("quotesWidget",'Ничего не найдено в данной категории'),
							'html'  => ''
						));
					}
					break;
				
				case 'getQuotes':
					$response = $this->getQuotesAjax();
					
					if(!$response){
						$response = json_encode(array('error' => Yii::t("quotesWidget",'Котировки не найдены')));
					}
					break;
				
				case 'ajaxSearch':
					$response = $this->getAjaxSearch();
					if($response){
						$response = json_encode($response);
					}else{
						$response = json_encode(array('error' => Yii::t("quotesWidget",'Ничего не найдено')));
					}
					break;
				
				default:
					$response = json_encode(array('error' => Yii::t("quotesWidget",'Неизвестная ошибка')));
					break;
			}
			echo $response;
			exit();
		}
		/*--------END AJAX--*/
		
		$quotesSettings = QuotesSettingsModel::getInstance();
		
		$timer                = '';
		$idsQuotesPanel       = null;
		$cats                 = $this->getCategories();
		$quotes               = array();
		$quotes['ids_quotes'] = array();
		$columns              = $quotesSettings->{$this->columns};
		$colums_name          = QuotesSettingsModel::parseSetting($quotesSettings->{$this->columns.'_name'});

		foreach($columns as $column){
			if(array_key_exists($column,$colums_name)){
				$this->_columns[ $column ] = $colums_name[ $column ];
			}else{
				$this->_columns[ $column ] = $column;
			}
		}

		if(isset($_GET['m']) && $_GET['m'] == "editPanel"){
			$this->view        = "editPanel";
			$this->idCat       = 71;
			$cats['default']   = 71;
			$checks_def        = $this->getDefaultChecks();
			$idsQuotesPanel    = QuotesModel::getIdsPanel();
			//$checksPanel       = $this->getDefaultChecksPanel();
			//$quotes            = $this->getQuotes(array(),false,array_merge($checks_def,$checksPanel));
			$quotes            = $this->getQuotes(array(),false,$checks_def);
			$checks            = $this->getAllChecks();
			$time_update_panel = QuotesModel::getTimer();
		}else{
			if(!$this->timer){
				$this->timer = $quotesSettings->default_timer;
			}

			if(!$this->idCat){
				$this->idCat = (($cats['default'])?$cats['default']:0);
			}
			
			$aCats         = $this->getCategoryWithChildes($this->idCat);
			$quotes        = $this->getQuotes($aCats);
			$checks        = $this->getChecks($quotes);
			$aTimer        = $quotesSettings->timer;
			
			foreach($aTimer as $time){
				$timer[ $time ] = $time.' .сек';
			}
		}

		$checksIds = array();
		foreach($checks as $checkb){
			if($checkb['checked'] == 'checked'){
				$checksIds[] = $checkb['id'];
			}
		}
		$iTimerDefault = $quotesSettings->timer_default;
		$sTable = $this->renderTable($quotes,$idsQuotesPanel);
		$end2   = microtime(true);
		$this->render($this->view."/view",Array(
			'quotes'            => $sTable,
			'timer'             => $timer,
			'timer_default'     => $iTimerDefault,
			'time_update_panel' => $time_update_panel,
			'cats'              => $cats,
			'ls'                => json_encode(
				array(
					"refreshPanel"=>Yii::t("quotesWidget","Вы уверены, что хотите сбросить настройки панели?"),
					"validNumber"=>Yii::t("quotesWidget","Должно быть число больше 0")
				)),
			'checks'            => $checks,
			'checksIds'         => json_encode($checksIds),
			'symbols_model'     => $this->symbols_model,
			'debug_timer'       => ($end2-$start).' s',
			'ids_quotes'        => $quotes['ids_quotes']
		));
	}
}
