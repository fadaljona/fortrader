<?php
Yii::import('components.widgets.quotes.Parser');
Yii::import('components.widgets.quotes.QuoteModel');

class QuotesModel{
	protected static $data_source_files = null;
	public static $format = 'array';
	public static $return_invisible = false;
	public static $no_data = '-';

	private static function setSourceFiles(){
		$quotesSettings = QuotesSettingsModel::getInstance();

		$files = $quotesSettings->files;
		$source = Parser::parse($files,self::$format);
		if($source)
			self::$data_source_files = $source;
		else
			self::$data_source_files = array();
	}
	
	public static function getIdsPanel(){
		$user=Yii::app()->user;
		if($user->id==0){
			$ids=QuotesSymbolsModel::getIdsPanelDefault();
		}
		else{
			$ids=$user->getState("panel.ids");
			if(!$ids)
				QuotesPanelModel::setState('panel');
			$ids=$user->getState("panel.ids");
		}
		return $ids;
	}
	
	public static function getTimer(){
		$quotesSettings = QuotesSettingsModel::getInstance();
		
		if($timer_=Yii::app()->user->getState("panel.time_update_panel")){
		}else{
			$timer_=$quotesSettings->time_update_panel;
			Yii::app()->user->setState("panel.time_update_panel",$timer_);
		}
		$timer=$quotesSettings->time_update_panel;
		return $timer;
	}

	public static function getDataFromFiles(){
		if(is_null(self::$data_source_files)){
			self::setSourceFiles();
		}
		return self::$data_source_files;
	}

	public static function setQuoteModel(&$db_quote=null, $format=null){
		if(is_null(self::$data_source_files)){
			self::setSourceFiles();
		}
		if($db_quote){

			$quote = new QuoteModel();

			$quote->id = $db_quote->id;
			$quote->name = $db_quote->name;
			$quote->nalias = Yii::t("quotesWidget",$db_quote->nalias);
			$quote->category_id = $db_quote->category;
			$quote->category_name = Yii::t("quotesWidget",$db_quote->_category->name);
			$quote->default = $db_quote->default;
			$quote->visible = $db_quote->visible;
			$quote->desc = Yii::t("quotesWidget",$db_quote->desc);
			
			$quote->setAlias();

			$is_object = self::setQuoteObject($quote,$db_quote->name);
			if(!$is_object&&$quote->alias){
				foreach($quote->alias[$quote->name] as $alias){
					if(isset(self::$data_source_files[$alias->name])){
						$is_object=self::setQuoteObject($quote,$alias->name);
						break;
					}
				}
			}
			if(!$is_object)
				self::setQuoteObjectEmpty($quote);
			
			if($format=='array'){
				$quote=(array)$quote;
			}
		}else{
			$quote = null;
		}

		return $quote;
	}
	
	public static function savePanel($ids,$time){
		if($ids&&is_array($ids)){
			Yii::app()->user->setState( "panel.ids", $ids);
			QuotesPanelModel::saveIds();
			return true;
		}elseif($time){
			Yii::app()->user->setState( "panel.time_update_panel", $time);
			QuotesPanelModel::saveTimer();
			return true;
		}else{
			return false;
		}
	}

	public static function  getQuotesByPanel(){
		if(is_null(self::$data_source_files)) self::setSourceFiles();
		//$visible = ((self::$return_invisible) ? '(`visible`="Visible" OR `visible`="Unvisible")' : '`visible`="Visible"');
		
		$ids=self::getIdsPanel();

		if(is_array($ids)&&!empty($ids))
			$db_quotes = QuotesSymbolsModel::model()->findAll('  `id` IN ('.implode(",",$ids).')');

		$quotes = array();

		if($db_quotes){
			foreach($db_quotes as $db_quote){
				$quotes[$db_quote->id] = self::setQuoteModel($db_quote,'array');
			}
			$temp=array();
			foreach($ids as $id){
				$temp[$id]=$quotes[$id];
			}
			$quotes=$temp;
			unset($temp);
		}
		return $quotes;
	}
	
	private static function setQuoteObject(&$quote,$name){
		if($name&&isset(self::$data_source_files[$name])){
			$quote->ask = isset(self::$data_source_files[$name]['ask']) ? self::$data_source_files[$name]['ask'] : self::$no_data;
			$quote->bid = isset(self::$data_source_files[$name]['bid']) ? self::$data_source_files[$name]['bid'] : self::$no_data;
			$quote->end_trading = isset(self::$data_source_files[$name]['end_trading']) ? self::$data_source_files[$name]['end_trading'] : self::$no_data;
			$quote->last_trade = isset(self::$data_source_files[$name]['last_trade']) ? self::$data_source_files[$name]['last_trade'] : self::$no_data;
			$quote->max = isset(self::$data_source_files[$name]['max']) ? self::$data_source_files[$name]['max'] : self::$no_data;
			$quote->min = isset(self::$data_source_files[$name]['min']) ? self::$data_source_files[$name]['min'] : self::$no_data;
			$quote->open_trade = isset(self::$data_source_files[$name]['open_trade']) ? self::$data_source_files[$name]['open_trade'] : self::$no_data;
			$quote->rate = isset(self::$data_source_files[$name]['rate']) ? self::$data_source_files[$name]['rate'] : self::$no_data;
			$quote->spread = isset(self::$data_source_files[$name]['spread']) ? self::$data_source_files[$name]['spread'] : self::$no_data;
			$quote->start_trading = isset(self::$data_source_files[$name]['start_trading']) ? self::$data_source_files[$name]['start_trading'] : self::$no_data;
			$quote->time = isset(self::$data_source_files[$name]['time']) ? self::$data_source_files[$name]['time'] : self::$no_data;
			$quote->time_up = isset(self::$data_source_files[$name]['time_up']) ? self::$data_source_files[$name]['time_up'] : self::$no_data;
			$quote->trend = isset(self::$data_source_files[$name]['trend']) ? self::$data_source_files[$name]['trend'] : self::$no_data;
			$quote->volume = isset(self::$data_source_files[$name]['volume']) ? self::$data_source_files[$name]['volume'] : self::$no_data;
			return true;
		}else return false;
	}
	
	private static function setQuoteObjectEmpty(&$quote){
		if($quote){
			$quote->ask = self::$no_data;
			$quote->bid = self::$no_data;
			$quote->end_trading = self::$no_data;
			$quote->last_trade = self::$no_data;
			$quote->max = self::$no_data;
			$quote->min = self::$no_data;
			$quote->open_trade = self::$no_data;
			$quote->rate = self::$no_data;
			$quote->spread = self::$no_data;
			$quote->start_trading = self::$no_data;
			$quote->time = self::$no_data;
			$quote->time_up = self::$no_data;
			$quote->trend = self::$no_data;
			$quote->volume = self::$no_data;
		}
	}

	public static function getQuote($id_quote = 0){
		if(is_null($id_quote)) return array();
		if(is_null(self::$data_source_files)) self::setSourceFiles();
		$visible = ' AND ' . ((self::$return_invisible) ? '(`visible`="Visible" OR `visible`="Unvisible")' : '`visible`="Visible"');

		$db_quote = QuotesSymbolsModel::model()->with('_category')->find('`t`.`id`=:id' . $visible,array(':id' => $id_quote));
		return self::setQuoteModel($db_quote);
	}
	
	public static function searchQuotes($string=''){
		if(empty($string)) return array();
		if(is_null(self::$data_source_files)) self::setSourceFiles();
		$visible = ' AND ' . ((self::$return_invisible) ? '(`visible`="Visible" OR `visible`="Unvisible")' : '`visible`="Visible"');

		$db_quotes = QuotesSymbolsModel::model()->with('_category')->findAll('`t`.`name` LIKE :string OR `t`.`desc` LIKE :string' . $visible,array(':string' => "%".$string."%"));

		$quotes = array();

		if($db_quotes){
			foreach($db_quotes as $db_quote){
				$quotes[$db_quote->id] = self::setQuoteModel($db_quote,'array');
			}
		}
		return $quotes;
	}

	public static function getQuotesByIds($ids=array(),$format='array'){
		if(empty($ids))
			return array();
		if(is_null(self::$data_source_files)) self::setSourceFiles();
		$ids = array_filter($ids,
			function($el){ return !empty($el);}
		);

		$visible = ' AND ' . ((self::$return_invisible) ? '(`visible`="Visible" OR `visible`="Unvisible")' : '`visible`="Visible"');
		$db_quotes = QuotesSymbolsModel::model()->with('_category')->findAll('`t`.`id` IN ('.implode(',',$ids).')' . $visible);
		$quotes = array();

		if($db_quotes){
			foreach($db_quotes as $db_quote){
				$quotes[$db_quote->id] = self::setQuoteModel($db_quote,$format);
			}
		}
		return $quotes;
	}

	public static function getAllQuotes(){
		if(is_null(self::$data_source_files)) self::setSourceFiles();
		$visible = ' ' . ((self::$return_invisible) ? '(`visible`="Visible" OR `visible`="Unvisible")' : '`visible`="Visible"');
		$db_quotes = QuotesSymbolsModel::model()->with('_category')->findAll($visible);

		$quotes = array();
		if($db_quotes){
			foreach($db_quotes as $db_quote){
				$quotes[$db_quote->id] = self::setQuoteModel($db_quote);
			}
		}

		return $quotes;
	}

	public static function getQuotesByCatId($id_category = null,$only_default=false, $with_panel=false){
		if(is_null($id_category)) return array();
		if(is_null(self::$data_source_files)) self::setSourceFiles();

		$visible = ' AND ' . ((self::$return_invisible) ? '(`visible`="Visible" OR `visible`="Unvisible")' : '`visible`="Visible"');
		if($with_panel){
			$only_def= $only_default?' AND (`default`="Yes" OR `panel`="Yes")':'OR `panel`="Yes"';
		}else{
			$only_def= $only_default?' AND `default`="Yes"':'';
		}
		
		$db_quotes = QuotesSymbolsModel::model()->with('_category')->findAll('`category`=:category' . $visible.$only_def,array(':category' => $id_category));

		$quotes = array();
		if($db_quotes){
			foreach($db_quotes as $db_quote){
				$quotes[$db_quote->id] = self::setQuoteModel($db_quote);
			}
		}

		return $quotes;
	}
}
