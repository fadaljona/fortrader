<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminNavbarWidget extends WidgetBase {
		private function getUserName() {
			$model = Yii::App()->user->getModel();
			return $model ? $model->showName : null;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'userName' => $this->getUserName(),
				'class' => $class,
			));
		}
	}

?>