<?
Yii::import('components.widgets.base.WidgetBase');

final class DeCommentsWidget extends WidgetBase{
	public $paramStr = '';
	public $postId = 0;
	public $chart = 0;
	public $cat = 'default';
	private $postKey;
	private $userModel;
	
	private function getPostKey(){
		if( !$this->postKey ){
			$this->postKey = Yii::app()->controller->id . '.' . Yii::app()->controller->action->id . '.' . $this->paramStr;
		} 
		return $this->postKey;
	}
	private function getDecomPostId(){
		if( !$this->postId ){
			$postId = DecommentsPostsModel::getPostId( $this->getPostKey(), $this->cat );
			if( !$postId ) $this->throwI18NException( "Error occurred - can't get post id for comments" );
			$this->postId = $postId;
		}
		return $this->postId;
	}
	private function getUserModel(){
		if( !Yii::App()->user->id ) return false;
		
		if( !$this->userModel ){
			$currentUser = UserModel::model()->find(array(
				'condition' => " `t`.`id` = :id",
				'with' => array('avatar'),
				'params' => array( ':id' => Yii::App()->user->id )
			));
			if( !$currentUser  ) $this->throwI18NException( "Can't load user" );
			$this->userModel = $currentUser;
		}
		return $this->userModel;
	}
	private function doActionToVar( $wpAction ){
		ob_start();
		do_action( $wpAction );
		return ob_get_clean();
	}
	private function getDecomSettings(){
		$decomSettingsCacheKey = 'decomSettings' . Yii::app()->language;
		$decomSettingsCache = Yii::app()->cache->get($decomSettingsCacheKey);
		if( !$decomSettingsCache ){
			CommonLib::loadWp();
			global $post;
			$post = get_post( $this->getDecomPostId() );
			$settings = array(
				'decom_get_options' => decom_get_options(),
				'option_show_avatars' => get_option( 'show_avatars' ),
				'texts' => array(
					'Add a picture' => __( 'Add a picture', DECOM_LANG_DOMAIN ),
					'Choose file' => __( 'Choose file', DECOM_LANG_DOMAIN ),
					'Submit' => __( 'Submit', DECOM_LANG_DOMAIN ),
					'Cancel' => __( 'Cancel', DECOM_LANG_DOMAIN ),
					'Attention' => __( 'Attention', DECOM_LANG_DOMAIN ),
					'Add a quote' => __( 'Add a quote', DECOM_LANG_DOMAIN ),
					'Subscribe' => __('Subscribe', DECOM_LANG_DOMAIN),
					'decom_comment_single_translate' => ' ' . _n( 'comment', 'comments', 1, DECOM_LANG_DOMAIN ),
					'decom_comment_twice_translate' => ' ' . _n( 'comment', 'comments', 2, DECOM_LANG_DOMAIN ),
					'decom_comment_plural_translate' => ' ' . _n( 'comment', 'comments', 5, DECOM_LANG_DOMAIN ),
					'Name' => __( 'Name', DECOM_LANG_DOMAIN ),
					'decom-name-author-val' => $_COOKIE['decommentsa'],
					'E-mail' => __( 'E-mail', DECOM_LANG_DOMAIN ),
					'Website' => __( 'Website', DECOM_LANG_DOMAIN ),
					'E-mail is already registered' => __( 'E-mail is already registered on the site. Please use the ', DECOM_LANG_DOMAIN ),
					'or' => __( ' or ', DECOM_LANG_DOMAIN ),
					'enter another' => __( 'enter another', DECOM_LANG_DOMAIN ),
					'incorrect username or password' => __( 'You entered an incorrect username or password', DECOM_LANG_DOMAIN ),
					'Password' => __( 'Password', DECOM_LANG_DOMAIN ),
					'Log in' => __( 'Log in', DECOM_LANG_DOMAIN ),
					'login form' => __( 'login form', DECOM_LANG_DOMAIN ),
					'Add comment' => __( 'Add comment', DECOM_LANG_DOMAIN ),
					'Log out' => __( 'Log out', DECOM_LANG_DOMAIN ),
					'Replies to my comments' => __( 'Replies to my comments', DECOM_LANG_DOMAIN ),
					'All comments' => __( 'All comments', DECOM_LANG_DOMAIN ),
					'Sorry, you must be logged in to post a comment.' => __( 'Sorry, you must be logged in to post a comment.', DECOM_LANG_DOMAIN ),
				),
			);
			Yii::app()->cache->set($decomSettingsCacheKey, $settings);
		}
		return Yii::app()->cache->get($decomSettingsCacheKey);
	}
	private function getPostDecomSettings(){
		$postId = $this->getDecomPostId();
		$postDecomSettingsCacheKey = 'postDecomSettings'.$postId . Yii::app()->language;
		if( !Yii::app()->cache->get($postDecomSettingsCacheKey) ){
			CommonLib::loadWp();
			$settings = array(
				//'decom_post_url' => esc_js( get_permalink( $postId ) ),
			);
			Yii::app()->cache->set($postDecomSettingsCacheKey, $settings);
		}
		return Yii::app()->cache->get($postDecomSettingsCacheKey);
	}
	function run() {
		$this->render( $this->getCleanClassName() . "/view", Array(
			'post_id' => $this->getDecomPostId(),
			'decomSettings' => $this->getDecomSettings(),
			'postDecomSettings' => $this->getPostDecomSettings(),
			'userModel' => $this->getUserModel(),
			'chart' => $this->chart,
		));
	}
}
?>