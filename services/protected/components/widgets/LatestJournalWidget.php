<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class LatestJournalWidget extends WidgetBase {
		public $model;

		private function getModel() {
			return $this->model;
		}
		function run() {
			$JournalSettings = JournalSettingsModel::getModel();
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
				'buyLink' => $JournalSettings->buyLink,
			));
		}
	}

?>