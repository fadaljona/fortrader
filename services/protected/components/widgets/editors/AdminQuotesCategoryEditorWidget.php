<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminQuotesCategoryFormModel' );

	final class AdminQuotesCategoryEditorWidget extends WidgetBase {
		const modelName = 'QuotesCategoryModel';
		public $formModel;
		public $categories;
		private function getFormModel() {
			if( !$this->formModel ) {
				$this->formModel = new AdminQuotesCategoryFormModel();
				$this->formModel->load();
				return $this->formModel;
			}
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'order' => '`t`.`order` ASC',
				),
				'pagination' => false,//$this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'categories'=>$this->categories
			));
		}
	}
