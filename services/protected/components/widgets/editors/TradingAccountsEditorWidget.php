<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'components.widgets.lists.ContestMembersTAListWidget' );
	Yii::import( 'models.forms.ContestMemberFormModel' );

	final class TradingAccountsEditorWidget extends WidgetBase {
		public $idUser;
		public $contestMemberFormModel;
		private function getContestMemberFormModel() {
			if( !$this->contestMemberFormModel ) {
				$this->contestMemberFormModel = new ContestMemberFormModel();
				$this->contestMemberFormModel->load();	
			}
			return $this->contestMemberFormModel;
		}
		private function getContestMembersDP() {
			$DP = new CActiveDataProvider( 'ContestMemberModel', Array(
				'criteria' => Array(
					'with' => Array( 'contest', 'contest.currentLanguageI18N', 'contest.i18ns', 'stats' ),
					'condition' => " `t`.`idUser` = :idUser ",
					'params' => Array(
						':idUser' => $this->idUser,
					),
					'order' => ' `t`.`createdDT` DESC, `t`.`id` DESC ',
				),
				'pagination' => $this->getDPPagination(),
			));
			//$DP->pagination->pageSize = 2;
			$DP->pagination->pageVar = 'membersPage';
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'contestMembersDP' => $this->getContestMembersDP(),
				'contestMemberFormModel' => $this->getContestMemberFormModel(),
			));
		}
	}

?>