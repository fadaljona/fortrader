<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminMessageFormModel' );

	final class AdminMessagesEditorWidget extends WidgetBase {
		const modelName = 'MessageModel';
		public $formModel;
		public $filterURL;
		public $filterModel;
		public $type = 'message';
		private $inSearch = false;
		private function detFormModel() {
			$this->formModel = new AdminMessageFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getType() {
			return $this->type;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function getDP() {
			$c = new CDbCriteria();
			
			switch( $this->getType() ) {
				case 'message':{
					$c->addCondition( " `t`.`type` = 'message' " );
					break;
				}
				case 'text':{
					$c->addCondition( " `t`.`type` = 'text' " );
					break;
				}
			}
			
			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				$key = CommonLib::escapeLike( $filterModel->key );
				if( strlen($filterModel->key) or strlen($filterModel->ns)) {
					$key = CommonLib::escapeLike( $filterModel->key );
					$key = CommonLib::filterSearch( $key );
					if( strlen( $key )) {
						$c->addCondition("
							`t`.`key` LIKE :key
							OR EXISTS (
								SELECT 	`id`
								FROM 	{{message_i18n}}
								WHERE	`idMessage` = `t`.`id`
									AND `value` LIKE :key
								LIMIT	1
							)
						");
						$c->params[':key'] = "{$key}";
					}
					
					$ns = CommonLib::escapeLike( $filterModel->ns );
					$ns = CommonLib::filterSearch( $ns );
					if( strlen( $ns )) {
						$c->addSearchCondition( '`ns`', $ns, false );	
					}
										
					$this->setInSearch();
				}
			}
			$c->with['currentLanguageI18N'] = Array(
				'joinType' => 'LEFT JOIN',
				'order' => "`currentLanguageI18N`.`value`,`t`.`key`",
			);
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'type' => $this->getType(),
				'inSearch' => $this->getInSearch(),
			));
		}
	}

?>