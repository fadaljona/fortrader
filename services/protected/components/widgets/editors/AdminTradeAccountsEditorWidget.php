<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminTradeAccountFormModel' );

	final class AdminTradeAccountsEditorWidget extends WidgetBase {
		const modelName = 'TradeAccountModel';
		public $formModel;
		public $filterURL;
		public $filterModel;
		private $inSearch = false;
		private function detFormModel() {
			$this->formModel = new AdminTradeAccountFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function getDP() {
			$c = new CDbCriteria();
			
			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				if( strlen( $filterModel->name )) {
					$name = CommonLib::escapeLike( $filterModel->name );
					$name = CommonLib::filterSearch( $name );
					$c->addCondition( "
						`t`.`name` LIKE :name
					");
					$c->params[':name'] = $name;
					$this->setInSearch();
				}
				if( strlen( $filterModel->accountNumber )) {
					$accountNumber = CommonLib::escapeLike( $filterModel->accountNumber );
					$accountNumber = CommonLib::filterSearch( $accountNumber );
					$c->addCondition( "
						`t`.`accountNumber` LIKE :accountNumber
					");
					$c->params[':accountNumber'] = $accountNumber;
					$this->setInSearch();
				}
				if( $filterModel->groups ) {
					$c->addCondition( "
						FIND_IN_SET( :group, `t`.`groups` )
					");
					$c->params[':group'] = $filterModel->groups;
					$this->setInSearch();
				}
			}
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
			));
		}
	}

?>