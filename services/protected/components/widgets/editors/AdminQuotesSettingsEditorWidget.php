<?
Yii::import( 'components.widgets.base.WidgetBase' );
Yii::import( 'models.forms.AdminQuotesSettingsFormModel' );

final class AdminQuotesSettingsEditorWidget extends WidgetBase {
	const modelName = 'QuotesSettingsModel';
	public $formModel;
	private function getFormModel() {
		if( !$this->formModel ) {
			$this->formModel = new AdminQuotesSettingsFormModel();
			$this->formModel->load();
			return $this->formModel;
		}
	}
	private function getDP() {
		$DP = new CActiveDataProvider( self::modelName, Array(
			'criteria' => Array(
				'order' => '`t`.`name` ASC',
			),
			'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
		));
		return $DP;
	}
	function run() {
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'DP' => $this->getDP(),
			'formModel' => $this->getFormModel()
		));
	}
}
