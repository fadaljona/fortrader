<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminAdvertisementCampaignFormModel' );

	final class AdminAdvertisementCampaignsEditorWidget extends WidgetBase {
		const modelName = 'AdvertisementCampaignModel';
		public $formModel;
		public $mode;
		public $filterURL;
		public $filterModel;
		private $inSearch = false;
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function detFormModel() {
			$this->formModel = new AdminAdvertisementCampaignFormModel();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function detMode() {
			$this->mode = Yii::App()->user->checkAccess( 'advertisementControl' ) ? 'Admin' : 'User';
		}
		private function getMode() {
			if( !$this->mode ) $this->detMode();
			return $this->mode;
		}
		private function getUser() {
			$user = Yii::App()->user->getModel();
			if( !$user ) throw new Exception( "Can't find User! Relogin!" );
			return $user;
		}
		private function getIDUser() {
			$user = $this->getUser();
			return $user->id;
		}
		private function getIDBroker() {
			$idBroker = @$_GET['idBroker'];
			if( !$idBroker ) {
				$idBroker = Yii::App()->user->getModel()->idBroker;
				if( !$idBroker ) {
					$idBroker = 'null';
				}
			}
			return $idBroker;
		}
		private function getDP() {
			
			$mode = $this->getMode();
			$c = new CDbCriteria();
			
			$c->select = Array(
				" `t`.* ",
				" (
					SELECT			SUM( `countShows` )
					FROM			{{advertisement_stats}}
					INNER JOIN		{{advertisement}} ON ( `id` = `idAd` )
					WHERE			`idCampaign` = `t`.`id`
				) AS countShows",
				" (
					SELECT			SUM( `countClicks` )
					FROM			{{advertisement_stats}}
					INNER JOIN		{{advertisement}} ON ( `id` = `idAd` )
					WHERE			`idCampaign` = `t`.`id`
				) AS countClicks"
			);
			
			$c->with['owner'] = Array( 
				'select' => " user_login, balance ",
				'joinType' => 'INNER JOIN'
			);
			if( $mode == 'Admin' ) {
				$idBroker = $this->getIDBroker();
				if( $idBroker == 'null' ) {
					$c->condition = "`owner`.`idBroker` IS NULL";
				}
				elseif( $idBroker ){
					$c->condition = "`owner`.`idBroker` = :idBroker";
					$c->params[ ':idBroker' ] = $idBroker;
				}
			}
			else{
				$idUser = $this->getIDUser();
				$c->condition = "`t`.`idUser` = :idUser";
				$c->params[ ':idUser' ] = $idUser;
			}
			$c->order = '`t`.`name` ASC';
			
			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				if( strlen( $filterModel->name )) {
					$name = CommonLib::escapeLike( $filterModel->name );
					$name = CommonLib::filterSearch( $name );
					$c->addCondition( "
						`t`.`name` LIKE :name
					");
					$c->params[':name'] = $name;
					$this->setInSearch();
				}
				
				if( strlen( $filterModel->status )) {
					$status = $filterModel->status;
					$curDate = date( "Y-m-d" );
					
					if( $status == 'Not begun' ){
						$c->addCondition( "
							`t`.`begin` > :curDate
						");
						$c->params[':curDate'] = $curDate;
					}elseif( $status == 'Ended' ){
						$c->addCondition( "
							`t`.`end` < :curDate
						");
						$c->params[':curDate'] = $curDate;
					}elseif( $status == 'End balance' ){
						$c->addCondition( "
							`owner`.`balance` <= 0
						");
					}elseif( $status == 'Active' ){
						$c->addCondition( "
							`t`.`status` = :status and `t`.`begin` <= :curDate and `t`.`end` >= :curDate and `owner`.`balance` > 0
						");
						$c->params[':status'] = $status;
						$c->params[':curDate'] = $curDate;
					}else{
						$c->addCondition( "
							`t`.`status` = :status
						");
						$c->params[':status'] = $status;
					}

					$this->setInSearch();
				}
				
			}
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		private function getBrokers() {
			$NSi18n = $this->getNSi18n();
			$idBroker = $this->getIDBroker();
			$brokers = Array();
			
			$models = BrokerModel::model()->findAll( Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => '`cLI18NBroker`.`officialName`'
			));
			if( !$idBroker ) {
				$brokers[ "" ] = Yii::t( $NSi18n, 'Select...' );
			}
			$brokers[ "null" ] = Yii::t( $NSi18n, '(Without broker)' );
			foreach( $models as $model ) {
				$brokers[ $model->id ] = $model->officialName;
			}
			
			return $brokers;
		}
		function run() {
			$class = $this->getCleanClassName();
			$mode = $this->getMode();
			$idBroker = $this->getIDBroker();
			$this->render( "{$class}/view", Array(
				'DP' => $mode != "Admin" || $idBroker ? $this->getDP() : null,
				'formModel' => $this->getFormModel(),
				'mode' => $mode,
				'idBroker' => $idBroker,
				'brokers' => $mode == "Admin" ? $this->getBrokers() : Array(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
			));
		}
	}

?>