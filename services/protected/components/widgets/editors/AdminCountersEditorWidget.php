<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminCounterFormModel' );

	final class AdminCountersEditorWidget extends WidgetBase {
		const modelName = 'CounterModel';
		public $formModel;
		private function getFormModel() {
			if( !$this->formModel ) {
				$this->formModel = new AdminCounterFormModel();
				$this->formModel->load();
			}
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'order' => '`t`.`name` ASC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>