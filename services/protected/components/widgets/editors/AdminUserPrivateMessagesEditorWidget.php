<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminUserPrivateMessageFormModel' );

	final class AdminUserPrivateMessagesEditorWidget extends WidgetBase {
		const modelName = 'UserPrivateMessageModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminUserPrivateMessageFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 
						'to' => Array(
							'select' => ' ID, user_login ',
						), 
						'from' => Array(
							'select' => ' ID, user_login ',
						),
					),
					'order' => '`t`.`createdDT` DESC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>