<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminUnsubscribeCommentFormModel' );

	final class AdminUnsubscribeCommentsEditorWidget extends WidgetBase {
		const modelName = 'UnsubscribeCommentModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminUnsubscribeCommentFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array(
						'user' => Array(
							'select' => " user_login, user_nicename, display_name ",
						),
						'mailingType' => Array(
							'with' => Array(
								'currentLanguageI18N' => Array(),
							),
						),
					),
					'order' => '`t`.`createdDT` DESC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>