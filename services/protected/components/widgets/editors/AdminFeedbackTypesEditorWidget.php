<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminFeedbackTypeFormModel' );

	final class AdminFeedbackTypesEditorWidget extends WidgetBase {
		const modelName = 'FeedbackTypeModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminFeedbackTypeFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => 'currentLanguageI18N',
					'order' => '`t`.`position` IS NULL, `t`.`position`',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>