<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminContestWinerFormModel' );

	final class AdminContestWinersEditorWidget extends WidgetBase {
		const modelName = 'ContestWinerModel';
		public $formModel;
		public $filterURL;
		public $filterModel;
		

		private function detFormModel() {
			$this->formModel = new AdminContestWinerFormModel();
			$this->formModel->load();
		}

		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getDP() {
		
			$c = new CDbCriteria();
			
			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				if( $filterModel->idContest ) {

					$c->addCondition( "
						`t`.`idContest` = :idContest
					");
					$c->params[':idContest'] = $filterModel->idContest;
				}
			}
			
			$c->with = Array( 'contest', 'contest.currentLanguageI18N', 'member', 'member.user' );
			$c->order = '`cLI18NContest`.`name` ASC';
		
		
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>