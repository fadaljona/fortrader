<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminContestMemberFormModel' );

	final class AdminContestMembersEditorWidget extends WidgetBase {
		const modelName = 'ContestMemberModel';
		public $formModel;
		public $filterURL;
		public $filterModel;
		
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		
		private function detFormModel() {
			$this->formModel = new AdminContestMemberFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$idCurrentContest = $this->getIDContest();
			if( !$idCurrentContest ) return null;
			if( !ContestModel::model()->existsByPk( $idCurrentContest )) $this->throwI18NException( "Can't find Contest!" );
			
			$c = new CDbCriteria();
			$c->with = Array( 'user' );
			$c->addCondition( " `t`.`idContest` = :idCurrentContest ");
			$c->params[':idCurrentContest'] = $idCurrentContest;
			$c->order = '`user`.`user_login` ASC';
			
			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				if( $filterModel->accountNumber ) {
					
					$accountNumber = CommonLib::escapeLike( $filterModel->accountNumber );
					$accountNumber = CommonLib::filterSearch( $accountNumber );

					$c->addCondition( "
						`t`.`accountNumber` LIKE :accountNumber
					");
					$c->params[':accountNumber'] = $accountNumber;
				}
				if( $filterModel->idUser ) {
					$userName = CommonLib::escapeLike( $filterModel->idUser );
					$userName = CommonLib::filterSearch( $userName );
					
					$c->addCondition( "
						`user`.`display_name` LIKE :userName OR `user`.`user_nicename` LIKE :userName OR `user`.`user_login` LIKE :userName
					");
					$c->params[':userName'] = $userName;
				}
			}
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		private function getIDContest() {
			return (int)@$_GET[ 'idContest' ];
		}
		private function getContests() {
			$contests = Array();
			
			$models = ContestModel::model()->findAll( Array(
				'with' => 'currentLanguageI18N',
				'order' => "`t`.`begin` DESC",
			));
			
			$idCurrentContest = $this->getIDContest();
			/*if( !$idCurrentContest ) {
				$NSi18n = $this->getNSi18n();
				$contests[ "" ] = Yii::t( $NSi18n, 'Select...' );
			}*/
			
			foreach( $models as $model ) {
				$contests[ $model->id ] = $model->currentLanguageI18N->name;
			}
			
			return $contests;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'idContest' => $this->getIDContest(),
				'contests' => $this->getContests(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>