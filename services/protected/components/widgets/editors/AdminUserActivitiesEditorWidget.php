<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminUserActivityFormModel' );

	final class AdminUserActivitiesEditorWidget extends WidgetBase {
		const modelName = 'UserActivityModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminUserActivityFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 
						'i18ns' => Array(),
						'currentLanguageI18N' => Array(),
						'user' => Array(
							'select' => ' ID, user_login ',
						), 
					),
					'order' => '`t`.`createdDT` DESC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>