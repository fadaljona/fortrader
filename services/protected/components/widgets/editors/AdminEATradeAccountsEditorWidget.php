<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminEATradeAccountFormModel' );

	final class AdminEATradeAccountsEditorWidget extends WidgetBase {
		const modelName = 'EATradeAccountModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminEATradeAccountFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 'EA', 'tradeAccount' ),
					'order' => ' `EA`.`name` ASC, `tradeAccount`.`name` ASC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>