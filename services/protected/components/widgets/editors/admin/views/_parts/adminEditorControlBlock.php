<div class="row-fluid">
	<button type="button" class="btn btn-small btn-danger pull-left <?=$ins?> wDelete" style="margin: 0 5px 5px 0;">
		<i class="icon-remove"></i>
		<?=$this->translateR( 'Delete' )?>
	</button>
	<button type="button" class="btn btn-small btn-success pull-right <?=$ins?> wAdd" style="margin: 0 0 5px 5px;">
		<i class="icon-file"></i>
		<?=$this->translateR( $titleAddButton )?>
	</button>
</div>