<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminMailingFormModel' );

	final class AdminMailingsEditorWidget extends WidgetBase {
		const modelName = 'MailingModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminMailingFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => 'currentLanguageI18N',
					'order' => '`t`.`createdDT` DESC, `t`.`id` DESC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>