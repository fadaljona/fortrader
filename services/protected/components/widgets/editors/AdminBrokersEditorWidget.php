<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminBrokerFormModel' );

	final class AdminBrokersEditorWidget extends WidgetBase {
		const modelName = 'BrokerModel';
		public $formModel;
		public $filterURL;
		public $filterModel;
		public $type = 'broker';
		private $inSearch = false;
		private function detFormModel() {
			$this->formModel = new AdminBrokerFormModel();
			$this->formModel->load();
			$this->formModel->type = $this->type;
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function getDP() {

			$c = new CDbCriteria();
			
			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				if( strlen( $filterModel->officialName )) {
					$name = CommonLib::escapeLike( $filterModel->officialName );
					$name = CommonLib::filterSearch( $name );
					$c->addCondition( "
						`cLI18NBroker`.`brandName` LIKE :name or `cLI18NBroker`.`officialName` LIKE :name
					");
					$c->params[':name'] = $name;
					$this->setInSearch();
				}
			}
			
			$c->addCondition( " `t`.`type` = :type ");
			$c->params[':type'] = $this->type;
			
			if( Yii::app()->request->getQuery('id') ){
				$c->addCondition( " `t`.`id` = :id ");
				$c->params[':id'] = Yii::app()->request->getQuery('id');
			}
			
			$c->with = Array( 'currentLanguageI18N', 'i18ns' );
			$c->order = '`cLI18NBroker`.`officialName` ASC';
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,



				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
			));
		}
	}

?>
