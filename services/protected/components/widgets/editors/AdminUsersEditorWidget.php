<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminUserFormModel' );

	final class AdminUsersEditorWidget extends WidgetBase {
		const modelName = 'UserModel';
		public $type;
		public $formModel;
		public $filterURL;
		public $filterModel;
		private $inSearch = false;
		private $formTypes = Array(
			'Users' => 'User',
		);
		private function getType() {
			return $this->type;
		}
		private function getFormType() {
			return $this->formTypes[$this->getType()];
		}
		private function getListType() {
			return $this->getType();
		}
		private function detFormModel() {
			$this->formModel = new AdminUserFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function getDP() {
			$c = new CDbCriteria();
			
			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				if( strlen($filterModel->user_login) or $filterModel->group ) {
					if( strlen( $filterModel->user_login )) {
						$key = CommonLib::escapeLike( $filterModel->user_login );
						$key = CommonLib::filterSearch( $key );
						$c->addCondition( "
							`t`.`user_login` LIKE :key
							OR `t`.`user_nicename` LIKE :key
							OR `t`.`display_name` LIKE :key
						");
						if( substr_count( $key, '@' )) {
							$c->addCondition( " `t`.`user_email` LIKE :key ", 'OR' );
						}
						$c->params[':key'] = $key;
					}
					if( $filterModel->group ) {
						$c->addCondition( "
							EXISTS (
								SELECT 	1
								FROM 	`{{user_to_user_group}}`
								WHERE	`idUser` = `t`.`id`
									AND `idUserGroup` = :idGroup
							)
						");
						$c->params[':idGroup'] = $filterModel->group;
					}
					$this->setInSearch();
				}
			}
			
			$c->with = 'groups';
			$c->order = '`t`.`user_registered` DESC, `t`.`id` DESC';
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'type' => $this->getType(),
				'formType' => $this->getFormType(),
				'formModel' => $this->getFormModel(),
				'listType' => $this->getListType(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
			));
		}
	}

?>