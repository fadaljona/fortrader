<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminAdvertisementBlockFormModel' );

	final class AdminAdvertisementBlocksEditorWidget extends WidgetBase {
		const modelName = 'AdvertisementBlockModel';
		public $DP;
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminAdvertisementBlockFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			if( !$this->DP ) {
				$this->DP = new CActiveDataProvider( self::modelName, Array(
					'criteria' => Array(
						'order' => '`t`.`createdDT` DESC',
					),
					'pagination' => $this->getDPPagination(),
				));
			}
			return $this->DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>