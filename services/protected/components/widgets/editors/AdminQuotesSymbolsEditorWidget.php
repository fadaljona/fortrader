<?
Yii::import( 'components.widgets.base.WidgetBase' );
Yii::import( 'models.forms.AdminQuotesSymbolsFormModel' );

final class AdminQuotesSymbolsEditorWidget extends WidgetBase {
	const modelName = 'QuotesSymbolsModel';
	public $formModel;
	public $categories;
	public $filterURL;
	public $filterModel;
	private $inSearch = false;
	private function getFormModel() {
		if( !$this->formModel ) {
			$this->formModel = new AdminQuotesSymbolsFormModel();
			$this->formModel->load();
			return $this->formModel;
		}
	}
	private function getFilterURL() {
		return $this->filterURL;
	}
	private function getFilterModel() {
		return $this->filterModel;
	}
	private function setInSearch( $inSearch = true ) {
		$this->inSearch = $inSearch;
	}
	private function getInSearch() {
		return $this->inSearch;
	}
	private function getDP() {
		$c = new CDbCriteria();
		$filterModel = $this->getFilterModel();
		$c->with = array( 'currentLanguageI18N' );
		if( $filterModel ) {
			if( $filterModel->id) {
				$c->addCondition( "`id` = ".(int)$filterModel->id);
				$this->setInSearch();
			}
			if( strlen( $filterModel->name )) {
				$name = CommonLib::escapeLike( $filterModel->name );
				$name = CommonLib::filterSearch( $name );
				$c->addCondition( "
						`t`.`name` LIKE :name
					");
				$c->params[':name'] = $name;
				$this->setInSearch();
			}
			if( strlen( $filterModel->tickName )) {
				$tickName = CommonLib::escapeLike( $filterModel->tickName );
				$tickName = CommonLib::filterSearch( $tickName );
				$c->addCondition( "
						`t`.`tickName` LIKE :tickName
					");
				$c->params[':tickName'] = $tickName;
				$this->setInSearch();
			}
			if( strlen( $filterModel->histName )) {
				$histName = CommonLib::escapeLike( $filterModel->histName );
				$histName = CommonLib::filterSearch( $histName );
				$c->addCondition( "
						`t`.`histName` LIKE :histName
					");
				$c->params[':histName'] = $histName;
				$this->setInSearch();
			}
			if( strlen( $filterModel->snalias )) {
				$nalias = CommonLib::escapeLike( $filterModel->snalias );
				$nalias = CommonLib::filterSearch( $nalias );
				$c->addCondition( "
						`cLI18NQuotesSymbolsModel`.`nalias` LIKE :nalias
					");
				$c->params[':nalias'] = $nalias;
				$this->setInSearch();
			}
			if( strlen( $filterModel->sdesc )) {
				$desc = CommonLib::escapeLike( $filterModel->sdesc );
				$desc = CommonLib::filterSearch( $desc );
				$c->addCondition( "
						`cLI18NQuotesSymbolsModel`.`desc` LIKE :desc
					");
				$c->params[':desc'] = $desc;
				$this->setInSearch();
			}
			if( $filterModel->category ) {
				$catid=$filterModel->category;
				$c->addCondition( "`category` = ".(int)$catid);
				$this->setInSearch();
			}
			if( $filterModel->sdefault ) {
				$c->addCondition( "
						FIND_IN_SET( :default, `cLI18NQuotesSymbolsModel`.`default` )
					");
				$c->params[':default'] = $filterModel->sdefault;
				$this->setInSearch();
			}
			if( $filterModel->panel ) {
				$c->addCondition( "
						FIND_IN_SET( :panel, `t`.`panel` )
					");
				$c->params[':panel'] = $filterModel->panel;
				$this->setInSearch();
			}
			if( $filterModel->svisible ) {
				$c->addCondition( "
						FIND_IN_SET( :visible, `cLI18NQuotesSymbolsModel`.`visible` )
					");
				$c->params[':visible'] = $filterModel->svisible;
				$this->setInSearch();
			}
    
            if ($filterModel->sourceType) {
                $c->addCondition( "
					`t`.`sourceType` LIKE :sourceType
				");
                $c->params[':sourceType'] = $filterModel->sourceType;
                $this->setInSearch();
            }
		}
		
		$c->order = " `t`.`category` DESC, `cLI18NQuotesSymbolsModel`.`weightInCat` DESC ";
		
		$DP = new CActiveDataProvider( self::modelName, Array(
			'criteria' => $c,
			'pagination' => $this->getDPPagination(),
		));
		$DP->pagination->pageSize = 50;
		return $DP;
	}
	function run() {
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'DP' => $this->getDP(),
			'formModel' => $this->getFormModel(),
			'filterURL' => $this->getFilterURL(),
			'filterModel' => $this->getFilterModel(),
			'inSearch' => $this->getInSearch(),
			'categories'=>$this->categories
		));
	}
}
