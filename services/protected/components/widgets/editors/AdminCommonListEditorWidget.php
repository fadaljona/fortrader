<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	

	final class AdminCommonListEditorWidget extends WidgetBase {
		public $modelName;
		public $formModelName;
		public $formModel;
		public $addLabel = 'Add';
		public $gridViewWidget;
		public $loadRowRoute;
		public $formWidget;
		public $loadItemRoute;
		public $deleteItemRoute;
		public $submitItemRoute;
		public $filterEnabled = false;
		public $enableSorting = false;
		public $filterModelName = false;
		public $filterUrl = false;
		
		private function detFormModel() {
			$this->formModel = new $this->formModelName;
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		function run() {
			Yii::import( 'models.forms.' . $this->formModelName );
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'formModel' => $this->getFormModel(),
				'formModelName' => $this->formModelName,
				'modelName' => $this->modelName,
				'addLabel' => $this->addLabel,
				'gridViewWidget' => $this->gridViewWidget,
				'loadRowRoute' => $this->loadRowRoute,
				'formWidget' => $this->formWidget,
				'loadItemRoute' => $this->loadItemRoute,
				'deleteItemRoute' => $this->deleteItemRoute,
				'submitItemRoute' => $this->submitItemRoute,
				'filterEnabled' => $this->filterEnabled,
				'enableSorting' => $this->enableSorting,
				'filterModelName' => $this->filterModelName,
				'filterUrl' => $this->filterUrl
			));
		}
	}

?>