<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminUserMessageFormModel' );

	final class AdminUserMessagesEditorWidget extends WidgetBase {
		const modelName = 'UserMessageModel';
		public $formModel;
		public $instance;
		private function detFormModel() {
			$this->formModel = new AdminUserMessageFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getInstance() {
			return $this->instance;
		}
		private function getDP() {
			$c = new CDbCriteria();
			$c->with['user'] = Array(
				'select' => ' ID, user_login ',
			);
			$c->order = '`t`.`createdDT` DESC';
			if( $instance = $this->getInstance()) {
				$c->addCondition( " `t`.`instance` = :instance " );
				$c->params['instance'] = $instance;
			}
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'instance' => $this->getInstance(),
			));
		}
	}

?>