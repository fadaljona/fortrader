<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminAdvertisementFormModel' );

	final class AdminAdvertisementsEditorWidget extends WidgetBase {
		const modelName = 'AdvertisementModel';
		public $formModel;
		public $mode;
		public $type = 'Text';
		public $sortField = 'header';
		public $sortType = 'ASC';
		private function getSortField() {
			$sortField = @$_GET[ 'sortField' ];
			if( !in_array( $sortField, Array( 'header', 'countLikes', 'countShows', 'countClicks', 'CTR', 'status' ))) $sortField = $this->sortField;
			return $sortField;
		}
		private function getSortType() {
			$sortType = @$_GET[ 'sortType' ];
			if( !in_array( $sortType, Array( 'ASC', 'DESC' ))) $sortType = $this->sortType;
			return $sortType;
		}
		private function detFormModel() {
			$this->formModel = new AdminAdvertisementFormModel();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function detMode() {
			$this->mode = Yii::App()->user->checkAccess( 'advertisementControl' ) ? 'Admin' : 'User';
		}
		private function getMode() {
			if( !$this->mode ) $this->detMode();
			return $this->mode;
		}
		private function getType() {
			return $this->type;
		}
		private function getUser() {
			$user = Yii::App()->user->getModel();
			if( !$user ) throw new Exception( "Can't find User! Relogin!" );
			return $user;
		}
		private function getIdUser() {
			$user = $this->getUser();
			return $user->id;
		}
		private function getOrderDP() {
			$sortField = $this->getSortField();
			$sortType = $this->getSortType();
			switch( $sortField ) {
				case 'header': 
					return " `t`.`header` {$sortType} ";
				case 'likes': 
					return " `t`.`countLikes` {$sortType} ";
				case 'countShows': 
					return " `countShows` {$sortType} ";
				case 'countClicks': 
					return " `countClicks` {$sortType} ";
				case 'CTR': 
					return " `countShows` / `countClicks` {$sortType} ";
				case 'status': 
					return " `status` {$sortType} ";
			}
		}
		private function getDP() {
			$idCurrentCampaign = $this->getIDCampaign();
			if( !$idCurrentCampaign ) return null;
			if( !AdvertisementCampaignModel::model()->existsByPk( $idCurrentCampaign )) $this->throwI18NException( "Can't find Campaign!" );
			
			$mode = $this->getMode();
			$c = new CDbCriteria();
			
			$c->select = Array(
				" `t`.* ",
				" (
					SELECT			SUM( `countShows` )
					FROM			{{advertisement_stats}}
					WHERE			`idAd` = `t`.`id`
				) AS countShows",
				" (
					SELECT			SUM( `countClicks` )
					FROM			{{advertisement_stats}}
					WHERE			`idAd` = `t`.`id`
				) AS countClicks",
				"
					IF(
						`t`.`dontShow`,
						'Dont show',
						IF(
							CURDATE() < `campaign`.`begin`,
							'Not begun',
							IF(
								CURDATE() > `campaign`.`end`,
								'Ended',
								IF(
									`owner`.`balance` <= 0,
									'End balance',
									`campaign`.`status`
								)
							)
						)
					)
					AS status
				"
			);
			
			$c->with['campaign'] = Array(
				'select' => " begin, end, status ",
				'with' => Array(
					'owner' => Array(
						'select' => " balance ",
					),
				),
			);
			
			$c->condition = "
				`t`.`idCampaign` = :idCampaign
				AND	`t`.`type` = :type
			";
			$c->params[ ':idCampaign' ] = $idCurrentCampaign;
			$c->params[ ':type' ] = $this->getType();
						
			$c->order = $this->getOrderDP();
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		private function getIDCampaign() {
			return (int)@$_GET[ 'idCampaign' ];
		}
		private function getCampaigns() {
			$campaigns = Array();
			$mode = $this->getMode();
			
			$c = new CDbCriteria();
			
			$type = $this->getType();
			switch( $type ) {
				case 'Text':{ $type = 'Text'; break; }
				case 'Image':{ $type = 'Image'; break; }
				case 'Button':{ $type = 'Buttons'; break; }
				case 'Offer':{ $type = 'Offers'; break; }
			}
			$c->addCondition( " FIND_IN_SET( '{$type}', `t`.`type` ) " );
			
			if( $mode == 'Admin' ) {
				$c->with = Array( 
					'owner' => Array(
						'select' => " display_name, user_nicename, user_login ",
					),
				);
				$c->order = "`owner`.`user_login`, `t`.`name`";
			}
			else{
				$idUser = $this->getIdUser();
				$c->addCondition( "`t`.`idUser` = :idUser" );
				$c->params = Array(
					':idUser' => $idUser,
				);
				$c->order = "`t`.`name`";
			}
			
			
			$models = AdvertisementCampaignModel::model()->findAll( $c );
			if( count( $models )) {
				$idCurrentCampaign = $this->getIDCampaign();
				if( !$idCurrentCampaign ) {
					$NSi18n = $this->getNSi18n();
					$campaigns[ "" ] = Yii::t( $NSi18n, 'Select...' );
				}
				
				foreach( $models as $model ) {
					if( $mode == 'Admin' ) {
						@$campaigns[ $model->id ] = "({$model->owner->user_login}) {$model->name}";
					}
					else{
						$campaigns[ $model->id ] = $model->name;
					}
				}
			}
			return $campaigns;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'mode' => $this->getMode(),
				'idCampaign' => $this->getIDCampaign(),
				'campaigns' => $this->getCampaigns(),
				'type' => $this->getType(),
				'sortField' => $this->getSortField(),
				'sortType' => $this->getSortType(),
			));
		}
	}

?>