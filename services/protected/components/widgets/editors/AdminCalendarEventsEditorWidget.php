<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminCalendarEventFormModel' );
	Yii::import( 'dps.AdminCalendarEventsListDP' );

	final class AdminCalendarEventsEditorWidget extends WidgetBase {
		const modelName = 'CalendarEvent2Model';
		public $formModel;
		public $filterURL;
		public $filterModel;
		private $inSearch = false;
		private function detFormModel() {
			$this->formModel = new AdminCalendarEventFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function getDP() {
			
			$c = new CDbCriteria();
			
		//	$params = array( ':idLanguage' => LanguageModel::getCurrentLanguageID() );

			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				if( strlen( $filterModel->indicatorIDForSearch )) {
					$indicatorID = (int)$filterModel->indicatorIDForSearch;
					$c->addCondition( " `t`.`indicator_id` = :indicatorID " );
					$c->params[':indicatorID'] = $indicatorID;
					$this->setInSearch();
				}
				if( strlen( $filterModel->indicatorForSearch )) {
					$indicator = CommonLib::escapeLike( $filterModel->indicatorForSearch );
					$indicator = CommonLib::filterSearch( $indicator );
					$c->with['currentLanguageI18N'] = array();
					$c->addCondition( " `cLI18NCalendarEvent`.`indicator_name` LIKE :indicatorName " );
					$c->params[':indicatorName'] = $indicator;
					$this->setInSearch();
				}
			}
			
			$idCountry = $this->getIDCountry();
			if( $idCountry ){
				$c->with['country'] = array();
				$c->addCondition( " `country`.`id` LIKE :idCountry " );
				$c->params[':idCountry'] = $idCountry;
			}
			
			$c->order = "`t`.`countrycode` DESC ";
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));

			return $DP;
		}
		private function getIDCountry() {
			return (int)@$_GET[ 'idCountry' ];
		}
		private function getCountries() {
			$countries = CalendarEvent2Model::getCountries();
			$countries = CommonLib::mergeAssocs( Array( '' => '' ), $countries );
			return $countries;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'idCountry' => $this->getIDCountry(),
				'countries' => $this->getCountries(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
			));
		}
	}

?>