<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminChangeUserBalanceFormModel' );

	final class AdminUserBalancesEditorWidget extends WidgetBase {
		const modelName = 'UserModel';
		public $formModel;
		public $filterURL;
		public $filterModel;
		private $inSearch = false;
		public $mode;
		private function getMode() {
			if( !$this->mode ) {
				$this->mode = Yii::App()->user->checkAccess( 'userBalanceControl' ) ? 'Admin' : 'User';
			}
			return $this->mode;
		}
		private function detFormModel() {
			$this->formModel = new AdminChangeUserBalanceFormModel();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function getDP() {
			$mode = $this->getMode();
			$c = new CDbCriteria();
			
			if( $mode == 'Admin' ) {
				$filterModel = $this->getFilterModel();
				if( $filterModel ) {
					if( strlen($filterModel->user_login) or $filterModel->group ) {
						if( strlen( $filterModel->user_login )) {
							$key = CommonLib::escapeLike( $filterModel->user_login );
							$key = CommonLib::filterSearch( $key );
							$c->addCondition( "
								`t`.`user_login` LIKE :key
								OR `t`.`user_nicename` LIKE :key
								OR `t`.`display_name` LIKE :key
							");
							if( substr_count( $key, '@' )) {
								$c->addCondition( " OR `t`.`user_email` LIKE :key " );
							}
							$c->params[':key'] = $key;
						}
						if( $filterModel->group ) {
							$c->addCondition( "
								EXISTS (
									SELECT 	1
									FROM 	`{{user_to_user_group}}`
									WHERE	`idUser` = `t`.`id`
										AND `idUserGroup` = :idGroup
								)
							");
							$c->params[':idGroup'] = $filterModel->group;
						}
						$this->setInSearch();
					}
				}
				$c->with = 'groups';
				$c->order = '`t`.`balance` DESC, `t`.`user_registered` DESC, `t`.`id` DESC';
			}
			else{
				$c->condition = "
					`t`.`ID` LIKE :idUser
				";
				$c->params = Array(
					':idUser' => Yii::App()->user->id,
				);
			}
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
				'mode' => $this->getMode(),
			));
		}
	}

?>