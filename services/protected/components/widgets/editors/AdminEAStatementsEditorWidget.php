<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminEAStatementFormModel' );

	final class AdminEAStatementsEditorWidget extends WidgetBase {
		const modelName = 'EAStatementModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminEAStatementFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 'i18ns', 'currentLanguageI18N', 'user' ),
					'order' => ' `cLI18NEAStatement`.`title` ASC ',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>