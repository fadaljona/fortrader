<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.*' );

	final class AdminBrokerOptionsEditorWidget extends WidgetBase {
		public $modelName;
		public $formModelName;
		public $formModel;
		private function detFormModel() {
			$this->formModel = new $this->formModelName;
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( $this->modelName, Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => '`cLI18N' .$this->modelName. '`.`name` ASC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
				'formModelName' => $this->formModelName,
				'modelName' => $this->modelName
			));
		}
	}

?>