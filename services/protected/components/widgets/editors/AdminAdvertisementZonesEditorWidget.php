<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminAdvertisementZoneFormModel' );

	final class AdminAdvertisementZonesEditorWidget extends WidgetBase {
		const modelName = 'AdvertisementZoneModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminAdvertisementZoneFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$c = new CDbCriteria();
			$c->select = Array(
				" `t`.* ",
				" (
					SELECT			SUM( `countShows` )
					FROM			{{advertisement_zone_stats}}
					WHERE			`idZone` = `t`.`id`
				) AS countShows",
				" (
					SELECT			SUM( `countClicks` )
					FROM			{{advertisement_zone_stats}}
					WHERE			`idZone` = `t`.`id`
				) AS countClicks"
			);
			$c->order = " `t`.`name` ASC ";
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>