<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminEAFormModel' );

	final class AdminEAEditorWidget extends WidgetBase {
		const modelName = 'EAModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminEAFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'order' => '`t`.`name` ASC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>