<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminUserNoticeFormModel' );

	final class AdminUserNoticesEditorWidget extends WidgetBase {
		const modelName = 'UserNoticeModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminUserNoticeFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 
						'user' => Array(
							'select' => ' ID, user_login ',
						), 
					),
					'order' => '`t`.`createdDT` DESC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>