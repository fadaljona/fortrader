<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminContactFormModel' );

	final class AdminContactsEditorWidget extends WidgetBase {
		const modelName = 'ContactModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminContactFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`position` IS NULL, `t`.`position` ',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>