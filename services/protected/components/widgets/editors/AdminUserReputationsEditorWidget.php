<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminUserReputationFormModel' );

	final class AdminUserReputationsEditorWidget extends WidgetBase {
		const modelName = 'UserReputationModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminUserReputationFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 
						'i18ns' => Array(),
						'currentLanguageI18N' => Array(),
						'to' => Array(
							'select' => ' ID, user_login ',
						), 
						'from' => Array(
							'select' => ' ID, user_login ',
						),
					),
					'order' => '`t`.`createdDT` DESC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>