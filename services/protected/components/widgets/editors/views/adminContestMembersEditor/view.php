<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminContestMembersEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminContestMembersEditorWidget">
	<div class="pull-left">

		<?//=CHtml::dropDownList( "idContest", $idContest, $contests, Array( "class" => "{$ins} wSelectContest" ))?>
		
		<?php $this->widget( 'widgets.ChooseInTableWidget', Array( 'ns' => $ns, 'param' => 'idContest', 'linkRout' => 'admin/contestMember/list', 'options' => $contests, 'header' => Yii::t( $NSi18n, 'Select contest' ) )); ?>
		
	</div>
	<?if( $idContest ){?>
		<?
			$this->widget( 'bootstrap.widgets.TbButton', Array(
				'label' => Yii::t( $NSi18n, 'Add member' ),
				'htmlOptions' => Array(
					'class' => "pull-right {$ins} wAdd",
				),
			))
		?>
		<div class="iClear"></div>
		<?
			$this->widget( 'widgets.forms.AdminContestMemberFormWidget', Array(
				'model' => $formModel,
				'ns' => $ns,
				'hidden' => true,
				'ajax' => true,
				'idContest' => $idContest,
			));
		?>
		<div class="iClear"></div>
		<?
			$this->widget( 'widgets.lists.AdminContestMembersListWidget', Array(
				'DP' => $DP,
				'ns' => $ns,
				'ajax' => true,
				'showOnEmpty' => true,
				'filterURL' => $filterURL,
				'filterModel' => $filterModel,
			));
		?>
	<?}?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;
		
		ns.wAdminContestMembersEditorWidget = wAdminContestMembersEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminContestMembersEditorWidget',
		});
		if( idEdit ) {
			ns.wAdminContestMembersEditorWidget.wForm.showEdit( idEdit );
			ns.wAdminContestMembersEditorWidget.wList.setSelection([ idEdit ]);
		}
	}( window.jQuery );
</script>