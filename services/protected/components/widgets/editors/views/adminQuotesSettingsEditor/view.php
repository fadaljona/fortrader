<?
Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/widgets/editors/wAdminQuotesSettingsEditorWidget.js");

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminQuotesSettingsEditorWidget">
	<? $this->widget('bootstrap.widgets.TbButton',Array('label' => Yii::t($NSi18n,"Добавить Параметр"),'htmlOptions' => Array('class' => "pull-right {$ins} wAdd"))) ?>
	<div class="iClear"></div>
	<? $this->widget('widgets.forms.AdminQuotesSettingsFormWidget',Array('model' => $formModel,'ns' => 'nsActionView','ajax' => true)); ?>
	<div class="iClear"></div>
	<? $this->widget('widgets.lists.AdminQuotesSettingsListWidget',Array('DP' => $DP,'ns' => $ns,'ajax' => true,'hidden' => !$DP->getTotalItemCount(),'showOnEmpty' => true)); ?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";

		ns.wAdminQuotesSettingsEditorWidget = wAdminQuotesSettingsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminQuotesSettingsEditorWidget'
		});
	}( window.jQuery );
</script>