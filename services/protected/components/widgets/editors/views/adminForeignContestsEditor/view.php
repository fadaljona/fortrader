<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminForeignContestsEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminForeignContestsEditorWidget">

	<a class="pull-right <?=$ins?> wAdd btn"><?=Yii::t( $NSi18n, 'Add contest' )?></a>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.ContestForeignFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
			'mode' => 'admin'
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminForeignContestsListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => false,
			'showOnEmpty' => true,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wAdminForeignContestsEditorWidget = wAdminForeignContestsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminForeignContestsEditorWidget',
		});
	}( window.jQuery );
</script>