<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminMailingTypesEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
?>
<div class="iEditorWidget i01 wAdminMailingTypesEditorWidget">
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add type' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminMailingTypeFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminMailingTypesListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount(),
			'showOnEmpty' => true,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wAdminMailingTypesEditorWidget = wAdminMailingTypesEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminMailingTypesEditorWidget',
		});
	}( window.jQuery );
</script>