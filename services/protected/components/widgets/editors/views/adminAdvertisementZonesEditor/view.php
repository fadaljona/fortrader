<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminAdvertisementZonesEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminAdvertisementZonesEditorWidget">
	<?if( !$DP->getTotalItemCount()){?>
		<div class="alert <?=$ins?> wAlert">
			<?=Yii::t( $NSi18n, 'It is not yet added any zone. To do this, click on the Add button.' )?>
		</div>
	<?}?>
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add zone' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminAdvertisementZoneFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminAdvertisementZonesListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount(),
			'showOnEmpty' => true,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wAdminAdvertisementZonesEditorWidget = wAdminAdvertisementZonesEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminAdvertisementZonesEditorWidget',
		});
	}( window.jQuery );
</script>