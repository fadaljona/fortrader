<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminCommonListEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="iEditorWidget i01 wAdminCommonListEditorWidget">

	<?php if($addLabel){?><a class="pull-right <?=$ins?> wAdd btn"><?=$addLabel?></a><?php }?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.' . $formWidget , Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
			'addLabel' => $addLabel,
			'formModelName' => $formModelName,
			'loadItemRoute' => $loadItemRoute,
			'modelName' => $modelName,
			'deleteItemRoute' => $deleteItemRoute,
			'submitItemRoute' => $submitItemRoute,
		));
	?>
	
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminCommonListWidget', Array(
			'ns' => $ns,
			'ajax' => true,
			'hidden' => false,
			'showOnEmpty' => true,
			'modelName' => $modelName,
			'gridViewWidget' => $gridViewWidget,
			'loadRowRoute' => $loadRowRoute,
			'filterEnabled' => $filterEnabled,
			'enableSorting' => $enableSorting,
			'filterModelName' => $filterModelName,
			'filterUrl' => $filterUrl
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wAdminCommonListEditorWidget = wAdminCommonListEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminCommonListEditorWidget',
		});
	}( window.jQuery );
</script>