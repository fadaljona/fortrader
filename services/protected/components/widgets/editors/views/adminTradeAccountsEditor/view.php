<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminTradeAccountsEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminTradeAccountsEditorWidget">
	<?if( !$DP->getTotalItemCount() and !$inSearch ){?>
		<div class="alert <?=$ins?> wAlert">
			<?=Yii::t( $NSi18n, 'It is not yet added any account. To do this, click on the Add button.' )?>
		</div>
	<?}?>
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add account' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminTradeAccountFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminTradeAccountsListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount() and !$inSearch,
			'showOnEmpty' => true,
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
			'inSearch' => $inSearch,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;
		
		ns.wAdminTradeAccountsEditorWidget = wAdminTradeAccountsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminTradeAccountsEditorWidget',
		});
		if( idEdit ) {
			ns.wAdminTradeAccountsEditorWidget.wForm.showEdit( idEdit );
			ns.wAdminTradeAccountsEditorWidget.wList.setSelection([ idEdit ]);
		}
	}( window.jQuery );
</script>