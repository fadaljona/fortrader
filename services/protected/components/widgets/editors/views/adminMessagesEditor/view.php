<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminMessagesEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	switch( $type ) {
		case 'message':{
			$alert = 'It is not yet added any message. To do this, click on the Add button.';
			$add = 'Add message';
			$form = 'widgets.forms.AdminMessageFormWidget';
			break;
		}
		case 'text':{
			$alert = 'It is not yet added any text. To do this, click on the Add button.';
			$add = 'Add text';
			$form = 'widgets.forms.AdminTextMessageFormWidget';
			break;
		}
	}
?>
<div class="iEditorWidget i01 wAdminMessagesEditorWidget">
	<?if( !$DP->getTotalItemCount() and !$inSearch ){?>
		<div class="alert <?=$ins?> wAlert">
			<?=Yii::t( $NSi18n, $alert )?>
		</div>
	<?}?>
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, $add ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( $form, Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminMessagesListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount() and !$inSearch,
			'showOnEmpty' => true,
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
			'inSearch' => $inSearch,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wAdminMessagesEditorWidget = wAdminMessagesEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminMessagesEditorWidget',
		});
	}( window.jQuery );
</script>