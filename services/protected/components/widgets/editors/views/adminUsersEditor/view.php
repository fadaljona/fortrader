<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminUsersEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminUsersEditorWidget">
	<?if( !$DP->getTotalItemCount() and !$inSearch ){?>
		<div class="alert <?=$ins?> wAlert">
			<?=Yii::t( $NSi18n, 'It is not yet added any users. To do this, click on the Add button.' )?>
		</div>
	<?}?>
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add user' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminUserFormWidget', Array(
			'type' => $formType,
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminUsersListWidget', Array(
			'type' => $listType,
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount() and !$inSearch,
			'showOnEmpty' => true,
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
			'inSearch' => $inSearch,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var type = "<?=$type?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;
		
		ns.wAdminUsersEditorWidget = wAdminUsersEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminUsersEditorWidget',
			type: type,
		});
		if( idEdit ) {
			ns.wAdminUsersEditorWidget.wForm.showEdit( idEdit );
			ns.wAdminUsersEditorWidget.wList.setSelection([ idEdit ]);
		}
	}( window.jQuery );
</script>