<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminEAVersionsEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminEAVersionsEditorWidget">
	<?if( !$DP->getTotalItemCount()){?>
		<div class="alert <?=$ins?> wAlert">
			<?=Yii::t( $NSi18n, 'It is not yet added any version. To do this, click on the Add button.' )?>
		</div>
	<?}?>
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add version' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminEAVersionFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminEAVersionsListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount(),
			'showOnEmpty' => true,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;
		
		ns.wAdminEAVersionsEditorWidget = wAdminEAVersionsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminEAVersionsEditorWidget',
		});
		if( idEdit ) {
			ns.wAdminEAVersionsEditorWidget.wForm.showEdit( idEdit );
			ns.wAdminEAVersionsEditorWidget.wList.setSelection([ idEdit ]);
		}
	}( window.jQuery );
</script>