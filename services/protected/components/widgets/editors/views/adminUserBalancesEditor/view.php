<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminUserBalancesEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminUserBalancesEditorWidget">
	<div class="iClear"></div>
	<?if( $mode == 'Admin' ){?>
		<?
			$this->widget( 'widgets.forms.AdminChangeUserBalanceFormWidget', Array(
				'model' => $formModel,
				'ns' => $ns,
				'hidden' => true,
				'ajax' => true,
			));
		?>
	<?}?>
	<div class="iClear"></div>
	<div class="wAdminUserBalanceChanges">
		<div class="progress progress-info progress-striped active">
			<div class="bar" style="width: 100%;"></div>
		</div>
	</div>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminUserBalancesListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'showOnEmpty' => true,
			'filterURL' => $filterURL,
			'filterModel' => $mode == 'Admin' ? $filterModel : null,
			'inSearch' => $inSearch,
			'mode' => $mode,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;
		
		ns.wAdminUserBalancesEditorWidget = wAdminUserBalancesEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminUserBalancesEditorWidget',
		});
		<?if( $mode == 'User' ){?>
			ns.wAdminUserBalancesEditorWidget.loadChanges( <?=Yii::App()->user->id?> );
		<?}else{?>
			if( idEdit ) {
				ns.wAdminUserBalancesEditorWidget.wForm.showEdit( idEdit );
				ns.wAdminUserBalancesEditorWidget.wList.setSelection([ idEdit ]);
				ns.wAdminUserBalancesEditorWidget.loadChanges( idEdit );
			}
		<?}?>
	}( window.jQuery );
</script>