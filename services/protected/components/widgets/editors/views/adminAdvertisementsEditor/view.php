<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminAdvertisementsEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	switch( $type ) {
		case 'Text':
		default:{
			$alert = 'It is not yet added any ad. To do this, click on the Add button.';
			$labelButton = 'Add ad';
			break;
		}
		case 'Image':{
			$alert = 'It is not yet added any banner. To do this, click on the Add button.';
			$labelButton = 'Add banner';
			break;
		}
		case 'Button':{
			$alert = 'It is not yet added any button. To do this, click on the Add button.';
			$labelButton = 'Add button';
			break;
		}
	}
?>
<div class="iEditorWidget i01 wAdminAdvertisementsEditorWidget">
	<?if( $campaigns ){?>
		<div class="pull-left">

			<?//=CHtml::dropDownList( "idCampaign", $idCampaign, $campaigns, Array( "class" => "{$ins} wSelectCampaign" ))?>
			<?php $this->widget( 'widgets.ChooseInTableWidget', Array( 'ns' => $ns, 'param' => 'idCampaign', 'linkRout' => 'admin/advertisement/' . Yii::app()->controller->action->id , 'options' => $campaigns, 'header' => Yii::t( $NSi18n, 'Select campaign' ) )); ?>
		</div>
		<div class="iClear"></div>
		<?if( $idCampaign ){?>
			<?if( !$DP->getTotalItemCount()){?>
				<div class="alert <?=$ins?> wAlert">
					<?=Yii::t( $NSi18n, $alert )?>
				</div>
			<?}?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array(
					'label' => Yii::t( $NSi18n, $labelButton ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wAdd",
					),
				))
			?>
			<div class="iClear"></div>
			<?
				$this->widget( 'widgets.forms.AdminAdvertisementFormWidget', Array(
					'model' => $formModel,
					'ns' => $ns,
					'hidden' => true,
					'ajax' => true,
					'mode' => $mode,
					'idCampaign' => $idCampaign,
					'campaigns' => $campaigns,
					'type' => $type,
				));
			?>
			<div class="iClear"></div>
			<?
				$this->widget( 'widgets.lists.AdminAdvertisementsListWidget', Array(
					'DP' => $DP,
					'ns' => $ns,
					'ajax' => true,
					'hidden' => !$DP->getTotalItemCount(),
					'showOnEmpty' => true,
					'mode' => $mode,
					'type' => $type,
					'sortField' => $sortField,
					'sortType' => $sortType,
				));
			?>
		<?}?>
	<?}else{?>
		<div class="alert <?=$ins?> wAlert">
			<?=Yii::t( $NSi18n, 'It is not yet added any campaigns.' )?>
		</div>
	<?}?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var type = "<?=$type?>";
		
		ns.wAdminAdvertisementsEditorWidget = wAdminAdvertisementsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminAdvertisementsEditorWidget',
			type: type,
		});
	}( window.jQuery );
</script>