<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminContestMemberLogsEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminContestMemberLogsEditorWidget">
	<?
		$this->widget( 'widgets.forms.AdminContestMemberLogFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminContestMemberLogsListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount(),
			'showOnEmpty' => true,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wAdminContestMemberLogsEditorWidget = wAdminContestMemberLogsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminContestMemberLogsEditorWidget',
		});
	}( window.jQuery );
</script>