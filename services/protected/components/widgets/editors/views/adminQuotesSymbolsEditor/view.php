<?
Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl . "/js/widgets/editors/wAdminQuotesSymbolsEditorWidget.js" );

$ns     = $this->getNS();
$ins    = $this->getINS();
$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminQuotesSymbolsEditorWidget">
	<? $this->widget( 'bootstrap.widgets.TbButton',Array(
		'label'       => Yii::t( "quotesWidget", "Добавить Инструмент" ),
		'htmlOptions' => Array( 'class' => "pull-right {$ins} wAdd" )
	) );?>
	<div class="iClear"></div>
	<? $this->widget( 'widgets.forms.AdminQuotesSymbolsFormWidget',Array(
		'model'      => $formModel,
		'ns'         => 'nsActionView',
		'ajax'       => true,
		'hidden'     => true,
		'categories' => $categories
	) ); ?>
	<div class="iClear"></div>
	<? $this->widget( 'widgets.lists.AdminQuotesSymbolsListWidget',Array(
		'DP'               => $DP,
		'ns'               => $ns,
		'ajax'             => true,
		'showOnEmpty'      => true,
		'showTableOnEmpty' => true,
		'filterURL'        => $filterURL,
		'filterModel'      => $filterModel,
		'inSearch'         => $inSearch,
		'hidden'           => ! $DP->getTotalItemCount()
	) ); ?>
</div>
<script>
	!function ($) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;

		ns.wAdminQuotesSymbolsEditorWidget = wAdminQuotesSymbolsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText + ' .wAdminQuotesSymbolsEditorWidget'
		});

		if (idEdit) {
			ns.wAdminQuotesSymbolsEditorWidget.wForm.showEdit(idEdit);
			ns.wAdminQuotesSymbolsEditorWidget.wList.setSelection([idEdit]);
		}
	}(window.jQuery);
</script>