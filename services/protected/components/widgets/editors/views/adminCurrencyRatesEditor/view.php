<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminCurrencyRatesEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminCurrencyRatesEditorWidget">

	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminCurrencyRatesFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
			'formModelName' => $formModelName,
			'modelName' => $modelName
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminCurrencyRatesListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount(),
			'showOnEmpty' => true,
			'modelName' => $modelName
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wAdminCurrencyRatesEditorWidget = wAdminCurrencyRatesEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminCurrencyRatesEditorWidget',
		});
	}( window.jQuery );
</script>