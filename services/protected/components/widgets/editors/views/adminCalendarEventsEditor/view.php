<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminCalendarEventsEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminCalendarEventsEditorWidget">
	<div class="pull-left">
		<?=Yii::t( $NSi18n, 'Select country' )?><br>
		<?=CHtml::dropDownList( "idCountry", $idCountry, $countries, Array( "class" => "{$ins} wSelectCountry" ))?>
		
	</div>
	<?
	/*	$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add event' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))*/
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminCalendarEventFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminCalendarEventsListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount() and !$inSearch,
			'showOnEmpty' => true,
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
			'inSearch' => $inSearch,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;
		
		ns.wAdminCalendarEventsEditorWidget = wAdminCalendarEventsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminCalendarEventsEditorWidget',
		});
		if( idEdit ) {
			ns.wAdminCalendarEventsEditorWidget.wForm.showEdit( idEdit );
			ns.wAdminCalendarEventsEditorWidget.wList.setSelection([ idEdit ]);
		}
	}( window.jQuery );
</script>
