<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminJournalsWebmasterEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminJournalsWebmasterEditorWidget">
	<?if( !$DP->getTotalItemCount()){?>
		<div class="alert <?=$ins?> wAlert">
			<?=Yii::t( $NSi18n, 'It is not yet added any journal webmaster.' )?>
		</div>
	<?}?>
	<?
		/*$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add journal' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))*/
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminJournalWebmasterFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminJournalsWebmasterListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount(),
			'showOnEmpty' => true,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;
		
		ns.wAdminJournalsWebmasterEditorWidget = wAdminJournalsWebmasterEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminJournalsWebmasterEditorWidget',
		});
		if( idEdit ) {
			ns.wAdminJournalsWebmasterEditorWidget.wForm.showEdit( idEdit );
			ns.wAdminJournalsWebmasterEditorWidget.wList.setSelection([ idEdit ]);
		}
	}( window.jQuery );
</script>