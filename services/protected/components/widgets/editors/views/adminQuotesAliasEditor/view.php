<?
Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/widgets/editors/wAdminQuotesAliasEditorWidget.js");

$ns = $this->getNS();
$ins = $this->getINS();
?>
<div class="iEditorWidget i01 wAdminQuotesAliasEditorWidget">
	<? $this->widget('bootstrap.widgets.TbButton',Array('label' => Yii::t( "quotesWidget","Добавить Алис"),'htmlOptions' => Array('class' => "pull-right {$ins} wAdd"))) ?>
	<div class="iClear"></div>
	<? $this->widget('widgets.forms.AdminQuotesAliasFormWidget',Array('model' => $formModel,'ns' => 'nsActionView','ajax' => true,'categories' => $categories)); ?>
	<div class="iClear"></div>
	<? $this->widget('widgets.lists.AdminQuotesAliasListWidget',Array('DP' => $DP,'ns' => $ns,'ajax' => true,'hidden' => !$DP->getTotalItemCount(),'showOnEmpty' => true)); ?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";

		ns.wAdminQuotesAliasEditorWidget = wAdminQuotesAliasEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminQuotesAliasEditorWidget'
		});
	}( window.jQuery );
</script>