<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminFeedbackTypesEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminFeedbackTypesEditorWidget">
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add type' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminFeedbackTypeFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminFeedbackTypesListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount(),
			'showOnEmpty' => true,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wAdminFeedbackTypesEditorWidget = wAdminFeedbackTypesEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminFeedbackTypesEditorWidget',
		});
	}( window.jQuery );
</script>