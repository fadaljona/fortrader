<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wTradingAccountsEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$itCurrentModel = $this->idUser == Yii::App()->user->id;
?>
<div class="iEditorWidget i01 wTradingAccountsEditorWidget">
	<?if( $itCurrentModel or Yii::App()->user->checkAccess( 'contestMemberControl' )){?>
		<?
			$this->widget( 'widgets.forms.ContestMemberFormWidget', Array(
				'model' => $contestMemberFormModel,
				'ns' => $ns,
				'hidden' => true,
			));
		?>
		<div class="iClear"></div>
	<?}?>
	<?
		$this->widget( 'widgets.lists.ContestMembersTAListWidget', Array(
			'DP' => $contestMembersDP,
			'ns' => $ns,
			'showOnEmpty' => true,
			'idUser' => $this->idUser,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var ins = ".<?=$ins?>";
		var nsText = ".<?=$ns?>";
		var idEdit = <?=(int)@$_GET['idEditMember']?>;
				
		ns.wTradingAccountsEditorWidget = wTradingAccountsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wTradingAccountsEditorWidget',
		});
		if( idEdit ) {
			ns.wTradingAccountsEditorWidget.wForm.showEdit( idEdit );
			ns.wTradingAccountsEditorWidget.wList.setSelection([ idEdit ]);
		}
	}( window.jQuery );
</script>