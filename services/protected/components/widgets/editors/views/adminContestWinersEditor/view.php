<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminContestWinersEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminContestWinersEditorWidget">
	<?if( !$DP->getTotalItemCount()){?>
		<div class="alert <?=$ins?> wAlert">
			<?=Yii::t( $NSi18n, 'It is not yet added any winer. To do this, click on the Add button.' )?>
		</div>
	<?}?>
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add winer' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminContestWinerFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminContestWinersListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
			'hidden' => !$DP->getTotalItemCount(),
			'showOnEmpty' => true,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wAdminContestWinersEditorWidget = wAdminContestWinersEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminContestWinersEditorWidget',
		});
	}( window.jQuery );
</script>