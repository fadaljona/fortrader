<?
Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/widgets/editors/wAdminQuotesCategoryEditorWidget.js");

$ns = $this->getNS();
$ins = $this->getINS();
?>
<div class="iEditorWidget i01 wAdminQuotesCategoryEditorWidget">
	<? $this->widget('bootstrap.widgets.TbButton',Array('label' => Yii::t( "quotesWidget","Добавить Категорию"),'htmlOptions' => Array('class' => "pull-right {$ins} wAdd"))) ?>
	<div class="iClear"></div>
	<? $this->widget('widgets.forms.AdminQuotesCategoryFormWidget',Array('model' => $formModel,'ns' => 'nsActionView','ajax' => true,'categories' => $categories)); ?>
	<div class="iClear"></div>
	<? $this->widget('widgets.lists.AdminQuotesCategoryListWidget',Array('DP' => $DP,'ns' => $ns,'ajax' => true,'hidden' => !$DP->getTotalItemCount(),'showOnEmpty' => true)); ?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";

		ns.wAdminQuotesCategoryEditorWidget = wAdminQuotesCategoryEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminQuotesCategoryEditorWidget'
		});
	}( window.jQuery );
</script>