<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminMailingsEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lRestartConfirm' => 'Restart mailing?',
		'lRunConfirm' => 'Run mailing?',
		'lStageInitialization' => 'Initialization',
		'lStageFinish' => 'Finished',
		'lError' => 'Error!',
		'lStageSMSMailing' => 'SMS mailing',
		'lStageEmailMailing' => 'EMail mailing',
		'lConnectionError' => 'Connection error! Repeat...',
		'lPause' => 'Pause',
		'lStop' => 'Stop',
		'lRestart' => 'Restart',
		'lStoping' => 'Stoping...',
		'lRestarting' => 'Restarting...',
		'lStoped' => 'Mailing stoped!',
		'lQueryRestart' => 'Mailing progress: (%stage%) %offset% / %count% Resume?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="iEditorWidget i01 wAdminMailingsEditorWidget">
	<div class="alert <?=$ins?> wRunAlert">
		<?=Yii::t( $NSi18n, 'Attention! Delivery is launched.' )?><br>
		<div class="progress progress-info progress-striped active <?=$ins?> wRunProgress">
			<div class="bar" style="width: 0%;"></div>
		</div>
		
		<div class="pull-right" style="margin:5px;">
			<button class="btn btn-success <?=$ins?> wResume"><?=Yii::t( $NSi18n, 'Resume' )?></button>
			<button class="btn btn-warning <?=$ins?> wPause"><?=Yii::t( $NSi18n, 'Pause' )?></button>
			<button class="btn btn-danger <?=$ins?> wStop"><?=Yii::t( $NSi18n, 'Stop' )?></button>
			<button class="btn btn-danger <?=$ins?> wRestart"><?=Yii::t( $NSi18n, 'Restart' )?></button>
		</div>
			
		<?=Yii::t( $NSi18n, 'Stage' )?>:
		<span class="<?=$ins?> wRunStage"></span><br>
		<?=Yii::t( $NSi18n, 'Progress' )?>: 
		<span class="<?=$ins?> wTextProgress"></span>
		
		<div class="iClear"></div>
	</div>
	<div class="alert alert-error <?=$ins?> wRunWarnings">
		<a href="#" class="close" data-dismiss="alert">&times;</a>
		<?=Yii::t( $NSi18n, 'In the process of sending messages there were identified the following mistakes:' )?>
		<ol></ol>
	</div>
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Create mailing' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminMailingFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminMailingsListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount(),
			'showOnEmpty' => true,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminMailingsEditorWidget = wAdminMailingsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminMailingsEditorWidget',
			ls: ls,
		});
	}( window.jQuery );
</script>