<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminBrokersEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 wAdminBrokersEditorWidget">
	<?if( !$DP->getTotalItemCount() and !$inSearch){?>
		<div class="alert <?=$ins?> wAlert">
			<?=Yii::t( $NSi18n, 'It is not yet added any broker. To do this, click on the Add button.' )?>
		</div>
	<?}?>
	<?
		$this->widget( 'bootstrap.widgets.TbButton', Array(
			'label' => Yii::t( $NSi18n, 'Add broker' ),
			'htmlOptions' => Array(
				'class' => "pull-right {$ins} wAdd",
			),
		))
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.forms.AdminBrokerFormWidget', Array(
			'model' => $formModel,
			'ns' => $ns,
			'hidden' => true,
			'ajax' => true,
		));
	?>
	<div class="iClear"></div>
	<?
		$this->widget( 'widgets.lists.AdminBrokersListWidget', Array(
			'DP' => $DP,
			'ns' => $ns,
			'ajax' => true,
			'hidden' => !$DP->getTotalItemCount() and !$inSearch,
			'showOnEmpty' => true,
			'filterURL' => $filterURL,
			'filterModel' => $filterModel,
		));
	?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;
		
		ns.wAdminBrokersEditorWidget = wAdminBrokersEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminBrokersEditorWidget',
		});
		if( idEdit ) {
			ns.wAdminBrokersEditorWidget.wForm.showEdit( idEdit );
			ns.wAdminBrokersEditorWidget.wList.setSelection([ idEdit ]);
		}
	}( window.jQuery );
</script>
