<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/editors/wAdminAdvertisementCampaignsEditorWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$ownAdvertisementControl = Yii::App()->user->checkAccess( 'ownAdvertisementControl' );
?>
<div class="iEditorWidget i01 wAdminAdvertisementCampaignsEditorWidget">
	<?if( $mode == "Admin" ){?>
		<div class="pull-left">

			<?//=CHtml::dropDownList( "idBroker", $idBroker, $brokers, Array( "class" => "{$ins} wSelectBroker" ))?>
			
			<?php $this->widget( 'widgets.ChooseInTableWidget', Array( 'ns' => $ns, 'param' => 'idBroker', 'linkRout' => 'admin/advertisementCampaign/list' , 'options' => $brokers, 'header' => Yii::t( $NSi18n, 'Select broker' ) )); ?>
			
		</div>
		<div class="iClear"></div>
	<?}?>
	<?if( $mode != "Admin" || $idBroker ) {?>
		<?if( !$DP->getTotalItemCount() and !$inSearch ){?>
			<div class="alert <?=$ins?> wAlert">
				<?=Yii::t( $NSi18n, 'It is not yet added any campaign. To do this, click on the Add button.' )?>
			</div>
		<?}?>
		<?
			$this->widget( 'bootstrap.widgets.TbButton', Array(
				'label' => Yii::t( $NSi18n, 'Add campaign' ),
				'htmlOptions' => Array(
					'class' => "pull-right {$ins} wAdd",
				),
			))
		?>
		<div class="iClear"></div>
		<?
			$this->widget( 'widgets.forms.AdminAdvertisementCampaignFormWidget', Array(
				'model' => $formModel,
				'ns' => $ns,
				'hidden' => true,
				'ajax' => true,
				'mode' => $mode,
				'idBroker' => $idBroker,
			));
		?>
		<div class="iClear"></div>
		<?
			$this->widget( 'widgets.lists.AdminAdvertisementCampaignsListWidget', Array(
				'DP' => $DP,
				'ns' => $ns,
				'ajax' => true,
				'hidden' => !$DP->getTotalItemCount() and !$inSearch,
				'showOnEmpty' => true,
				'mode' => $mode,
				'filterURL' => $filterURL,
				'filterModel' => $filterModel,
			));
		?>
	<?}?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEdit = <?=(int)@$_GET['idEdit']?>;
		
		ns.wAdminAdvertisementCampaignsEditorWidget = wAdminAdvertisementCampaignsEditorWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminAdvertisementCampaignsEditorWidget',
		});
		if( idEdit ) {
			ns.wAdminAdvertisementCampaignsEditorWidget.wForm.showEdit( idEdit );
			ns.wAdminAdvertisementCampaignsEditorWidget.wList.setSelection([ idEdit ]);
		}
	}( window.jQuery );
</script>