<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminMailingTypeFormModel' );

	final class AdminMailingTypesEditorWidget extends WidgetBase {
		const modelName = 'MailingTypeModel';
		public $formModel;
		private function detFormModel() {
			$this->formModel = new AdminMailingTypeFormModel();
			$this->formModel->load();
		}
		private function getFormModel() {
			if( !$this->formModel ) $this->detFormModel();
			return $this->formModel;
		}
		private function getDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => 'currentLanguageI18N',
					'order' => '`currentLanguageI18N`.`name`',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>