<?php

Yii::import('components.widgets.base.WidgetBase');

final class NewChatWidget extends WidgetBase
{
    public function run()
    {
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array());
    }
}
