<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EANewsWidget extends WidgetBase {
		public $ajax = true;
		public $limit = 5;
		public $idsAdjacent;
		public $dateLast;
		public $idsSkip;
		private function getDateLast() {
			if( $this->dateLast ) return $this->dateLast;
			$dateLast = @$_GET['dateLast'];
			return $dateLast ? $dateLast : null;
		}
		private function getIDsSkip() {
			if( $this->idsSkip ) return $this->idsSkip;
			$idsSkip = @$_GET['idsSkip'];
			return $idsSkip ? explode( ',', $idsSkip ) : null;
		}
		private function getCriteria( $forCount = false ) {
			$c = new CDbCriteria();
			
			if( !$forCount ) {
				$c->with[ 'currentLanguageI18N' ] = Array();
				$c->with[ 'i18ns' ] = Array();
			}
			
			$dateLast = $this->getDateLast();			
			if( $dateLast ) {
				$c->addCondition( " `t`.`date` <= :dateLast " );
				$c->params[ ':dateLast' ] = $dateLast;
			}
			
			$idsSkip = $this->getIDsSkip();			
			if( $idsSkip ) {
				$c->addNotInCondition( "`t`.`id`", $idsSkip );
			}
			
			if( !$forCount ) {
				$c->order = " `t`.`date` DESC, `t`.`id` DESC ";
				$c->limit = $this->limit;
			}
			
			return $c;
		}
		private function getNews() {
			$c = $this->getCriteria();
			$news = EANewsModel::model()->findAll( $c );
			return $news;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->ajax,
			));
		}
		function renderNews() {
			$news = $this->getNews();
			foreach( $news as $_news ){
				$this->render_news( $_news );
			}
		}
		function render_news( $news ) {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/news", Array(
				'news' => $news,
			));
			$this->dateLast = $news->date;
			
			static $holdData;
			if( !$holdData or $holdData != $news->date ) {
				$holdData = $news->date;
				$this->idsSkip = Array( $news->id );
			}
			else{
				$this->idsSkip[] = $news->id;
			}
		}
		function issetMore() {
			$c = $this->getCriteria( true );
			$exists = EANewsModel::model()->exists( $c );
			return $exists;
		}
	}

?>