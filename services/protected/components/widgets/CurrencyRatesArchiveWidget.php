<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesArchiveWidget extends WidgetBase {
		public $year;
		public $mo;
		public $day;
		public $dataType = 'cbr';

		private function getDatesForFilter(){
			if( !$this->year && !$this->mo && !$this->day ){
				
				$years = CurrencyRatesModel::getYearsAvailable( $this->dataType );
				$this->year = $years[count($years)-1]['year'];
				$months = CurrencyRatesModel::getMonthsAvailableByYear( $this->year, $this->dataType );
				$this->mo = count($months);
				$this->day = CurrencyRatesModel::getLastDayAvailableByYearMo( $this->year, $this->mo );
				$firstDay = CurrencyRatesModel::getFirstDayAvailableByYearMo( $this->year, $this->mo, $this->dataType );
				return array( 'years' => $years, 'months' => $months, 'choosedDay' => $this->day, 'lastDay' => $this->day, 'choosedYear' => $this->year, 'choosedMo' => $this->mo, 'firstDay' => $firstDay );
				
			}elseif( $this->year && !$this->mo && !$this->day ){
				
				if( !CurrencyRatesModel::hasDataByYearMo( $this->year, false, $this->dataType ) ) throw new CHttpException(404,'Page Not Found');
				
				$years = CurrencyRatesModel::getYearsAvailable( $this->dataType );
				$months = CurrencyRatesModel::getMonthsAvailableByYear( $this->year, $this->dataType );
				
				return array( 'years' => $years, 'months' => $months, 'choosedYear' => $this->year, 'choosedMo' => false, 'choosedDay' => false );
				
			}elseif( $this->year && $this->mo && !$this->day ){
				
				if( !CurrencyRatesModel::hasDataByYearMo( $this->year, $this->mo, $this->dataType ) ) throw new CHttpException(404,'Page Not Found');
				
				$years = CurrencyRatesModel::getYearsAvailable( $this->dataType );
				$months = CurrencyRatesModel::getMonthsAvailableByYear( $this->year, $this->dataType );
				
				$lastDay = CurrencyRatesModel::getLastDayAvailableByYearMo( $this->year, $this->mo );
				$firstDay = CurrencyRatesModel::getFirstDayAvailableByYearMo( $this->year, $this->mo, $this->dataType );
				
				return array( 'years' => $years, 'months' => $months, 'choosedDay' => false, 'choosedYear' => $this->year, 'choosedMo' => $this->mo, 'lastDay' => $lastDay, 'firstDay' => $firstDay );
				
			}elseif( $this->year && $this->mo && $this->day ){
				
				if( !CurrencyRatesModel::hasDataByYearMo( $this->year, $this->mo, $this->dataType ) ) throw new CHttpException(404,'Page Not Found');
				
				$years = CurrencyRatesModel::getYearsAvailable( $this->dataType );
				$months = CurrencyRatesModel::getMonthsAvailableByYear( $this->year, $this->dataType );
				
				$lastDay = CurrencyRatesModel::getLastDayAvailableByYearMo( $this->year, $this->mo );
				$firstDay = CurrencyRatesModel::getFirstDayAvailableByYearMo( $this->year, $this->mo, $this->dataType );
				
				if( $this->day == 0 || $this->day > $lastDay ) throw new CHttpException(404,'Page Not Found');
				
				return array( 'years' => $years, 'months' => $months, 'choosedDay' => $this->day, 'choosedYear' => $this->year, 'choosedMo' => $this->mo, 'lastDay' => $lastDay, 'firstDay' => $firstDay );
				
			}else{
				throw new CHttpException(404,'Page Not Found');
			}
			
		}
	
		function run() {
			$class = $this->getCleanClassName();

			$this->render( "{$class}/view", Array(
				'filterData' => $this->getDatesForFilter(),
				'dataType' => $this->dataType,
			));
		}
	}

?>