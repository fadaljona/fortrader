<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ChangeLanguageWidget extends WidgetBase {
		private function getLanguage() {
			return Yii::app()->getLanguage();
		}
		private function getLanguages() {
			$items = Array();
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ) {
				$items[ $language->alias ] = $language->name;
			}
			return $items;
		}
		function run() {
			/*$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				"language" => $this->getLanguage(),
				"languages" => $this->getLanguages(),
			));*/
		}
	}

?>