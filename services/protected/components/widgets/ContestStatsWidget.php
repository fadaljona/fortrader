<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestStatsWidget extends WidgetBase {
		public $idContest;
		
		function getStats(){
			
			return $statsModel = ContestStatsModel::model()->find(Array(
				'with' => Array( 
					'wleaderMember',
				),
				'condition' => " `t`.`idContest` = :idContest ",
				'params' => Array(
					':idContest' => $this->idContest,
				),
			));
			
		}

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getStats(),
			));
		}
	}

?>