<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ProfitabilityOfTheAccountWidget extends WidgetBase {
		public $idMember;
		private $DP;
		private function getIDMember() {
			return $this->idMember;
		}
		private function createDp(){

			$c = new CDbCriteria();
			$c->with[ 'monitoringStats' ] = Array();
			$c->with[ 'stats' ] = Array();

			$c->condition = " `t`.`idContest` = 0 AND `t`.`id` = :idMember ";
			$c->params[':idMember'] = $this->getIDMember();
			
			$DP = new CActiveDataProvider( 'ContestMemberModel', Array(
				'criteria' => $c,
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDp();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
			));
		}
	}

?>