<?
Yii::import('components.widgets.base.WidgetBase');

final class YahooPopularQuotesWidget extends WidgetBase{
	const cacheTime = 5; //5mins
	const yahooPopularQuotesCacheKey = 'yahooPopularQuotesCacheKey';
		
	public $title;
	
	private function getYahooQuotes(){
		return QuotesSymbolsModel::model()->findAll(array(
			'with' => array( 'viewsTable', 'currentLanguageI18N' ),
			'condition' => " `t`.`histName` <> '' AND `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' ",
			'order' => " `viewsTable`.`views` DESC ",
			'limit' => 20,
		));
	}

	function run() {
		$cachKey = self::yahooPopularQuotesCacheKey . Yii::app()->language;
		if( Yii::app()->cache->get( $cachKey ) ){ echo Yii::app()->cache->get( $cachKey ); return true; };
			ob_start();
			$this->render( $this->getCleanClassName() . "/view", Array(
				'yahooQuotes' => $this->getYahooQuotes(),
				'title' => $this->title ? $this->title : Yii::t("*", 'Yahoo quotes'),
			));
			$content = ob_get_clean();
		Yii::app()->cache->set( $cachKey, $content, self::cacheTime * 60 );
		echo $content;
	}
	
}
?>