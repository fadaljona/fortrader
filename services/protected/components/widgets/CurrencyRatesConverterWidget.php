<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesConverterWidget extends WidgetBase {
		public $currentModel;
		public $withoutTitle = false;
		public $headerTitle = false;
		public $type = 'cbr';
		
		private function getModel(){
			if( !$this->currentModel ){
				$this->currentModel = CurrencyRatesModel::model()->findBySlug('usd');
				if( !$this->currentModel ) $this->currentModel = CurrencyRatesModel::model()->find();
			}
			return $this->currentModel;
		}

		function run() {
			$class = $this->getCleanClassName();
			Yii::app()->session['canCreateConverterPage'] = true;
			
			if( $this->type == 'cbr' ){
				$this->render( "{$class}/{$this->type}", Array(
					'currenciesForConverter' => CurrencyRatesModel::getCurrenciesForConverter(),
					'model' => $this->getModel(),
					'defaultCurrency' => CurrencyRatesModel::getDefaultCurrency(),
					'withoutTitle' => $this->withoutTitle,
					'headerTitle' => $this->headerTitle,
					'type' => $this->type
				));
			}elseif( $this->type == 'ecb' ){
				$this->render( "{$class}/{$this->type}", Array(
					'currenciesForConverter' => CurrencyRatesModel::getEcbCurrenciesForConverter(),
					'model' => $this->getModel(),
					'defaultCurrency' => CurrencyRatesModel::getDefaultCurrency( $this->type ),
					'withoutTitle' => $this->withoutTitle,
					'headerTitle' => $this->headerTitle,
					'type' => $this->type
				));
			}
			
			
		}
	}

?>