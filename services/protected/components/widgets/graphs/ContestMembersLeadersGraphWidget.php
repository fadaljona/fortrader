<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMembersLeadersGraphWidget extends WidgetBase {
		const limitMembers = 5;
		const CACHE_KEY_PREFIX = 'ContestMembersLeadersGraphWidget.';
		public $idContest;
		public $cachingDuration = 86400;
		public $cacheID = 'cache';
		public $cache2ID = 'cache2';
		public $colors = Array( '#82AF6F', '#F89406', '#D15B47', '#3A87AD', '#D6487E' );
		public $firstValue;
		private function getContest() {
			return ContestModel::model()->findByPk( $this->idContest );
		}
		private function getMembers() {
			static $members;
			if( $members === null ) {
				$members = ContestMemberModel::model()->findAll(Array(
					'with' => Array( 'user', 'stats' ),
					'condition' => " 
						`t`.`idContest` = :idContest AND `stats`.`place` IS NOT NULL AND ( `t`.`status` = 'participates' OR `t`.`status` = 'completed' ) 
					",
					'order' => " `stats`.`place` ",
					'limit' => self::limitMembers,
					'params' => Array(
						':idContest' => $this->idContest,
					),
				));
			}
			return $members;
		}
		private function getAccounts() {
			static $accounts;
			if( $accounts === null ) {
				$members = $this->getMembers();
				foreach( $members as $member ) {
					$accounts[] = $member->accountNumber;
				}
			}
			return $accounts;
		}
		private function getServers() {
			static $servers;
			if( $servers === null ) {
				$members = $this->getMembers();
				foreach( $members as $member ) {
					$servers[] = $member->idServer;
				}
			}
			return array_unique($servers);
		}
		private function getLogins() {
			static $logins;
			if( $logins === null ) {
				$members = $this->getMembers();
				foreach( $members as $member ) {
					$logins[$member->accountNumber] = $member->user->showName;
				}
			}
			return $logins;
		}
		private function getContestMembers() {
			static $contestMembers;
			if( $contestMembers === null ) {
				$members = $this->getMembers();
				foreach( $members as $member ) {
					$contestMembers[$member->accountNumber] = $member->id;
				}
			}
			return $contestMembers;
		}
		private function getColors() {
			static $colors;
			if( !$colors ) {
				$cache2 = Yii::app()->getComponent( $this->cache2ID );
				$cacheKey = self::CACHE_KEY_PREFIX.$this->idContest.".colors";
				$colors = $cache2->get( $cacheKey );
				$colors = $colors ? unserialize( $colors ) : Array();
				$accounts = $this->getAccounts();
				
				foreach( $colors as $account=>$color ) {
					if( !in_array( $account, $accounts )) {
						unset( $colors[ $account ]);
					}
				}
				
				foreach( $accounts as $account ) {
					if( !array_key_exists( $account, $colors )) {
						foreach( $this->colors as $color ) {
							if( !in_array( $color, $colors )) {
								$colors[ $account ] = $color;
								break;
							}
						}
					}
				}
				
				$cache2->set( $cacheKey, serialize($colors), -1 );
			}
			return $colors;
		}
		
		//функция не используется, оставлена для последующего вывода истории лидеров на отдельном графике
		private function addHistoryData( &$data ) {
		
			$accounts = $this->getAccounts();
			$servers = $this->getServers();
			
	         	/*$timestampBegin = null;
			if( isset( $data ) && count( $data ) > 0 ) {
				$timestamps = array_keys( $data );
				$timestampBegin = (int)min( $timestamps );
			}*/
			
			if( $server->tradePlatform->name == 'MetaTrader 5' ) {
			}
			
			//if( $server->tradePlatform->name == 'MetaTrader 4' ) {
				$c = new CDbCriteria();
				$c->select = Array(" AccountNumber", "OrderCloseTime", "OrderProfit", "OrderSwap", "OrderCommission ",);
				$c->addInCondition( 'AccountNumber',   $accounts );
				$c->addInCondition( 'AccountServerId', $servers );
				/*if( $timestampBegin ) {
					$c->addCondition(" `OrderCloseTime` <= :begin ");
					$c->params[':begin'] = $timestampBegin;
				}*/
				$c->order = " `OrderCloseTime` ";
				$models = MT4AccountHistoryModel::model()->findAll( $c );
				//print_r( $c  );
				
				if( count( $models ) > 0 ) {
				//	print_r("start log models ");
					$balance = null;
					foreach( $models as $model ) {
						if( $balance === null ) {
							$balance = (float)$model->OrderProfit;
						}
						else {
							$balance += (float)$model->OrderProfit;
						}
						$balance += (float)$model->OrderSwap;
						$balance += (float)$model->OrderCommission;
						// if history with this timestamp already exists
						if(isset($data[ $model->OrderCloseTime ]))
						{
							$data[ $model->OrderCloseTime ][ $model->AccountNumber ] = $balance;
							
						//	print_r($data);
						}
						// otherwise prepending history to the beginning of data array
						else
						{
							$data = array($model->OrderCloseTime => array($model->AccountNumber => $balance, 'date' => date("Y-m-d H:i:s", (int)$model->OrderCloseTime))) + $data;
						}
					}
										
					// history line is gray for all graphs
					$timestamps = array_keys( $data );
					foreach($accounts as $account)
					{
						$data[ $timestamps[0] ][ 'lineColor_' . $account ] = '#999999';
					}
					if( isset( $data[ $timestampBegin ] )) {
						// next points after history will be colorful.
						// If 'lineColorField' is empty string graph will use
						// its native 'lineColor'
						foreach($accounts as $account)
						{
							$data[ $timestampBegin ][ 'lineColor_' . $account ] = '';
						}
					}
				}
				//	ksort ($data);
			//}
		}
		private function getData() {
			$data = Array();
			$contest = $this->getContest();
			$accounts = $this->getAccounts();
			$servers = $this->getServers();
						
			$c = new CDbCriteria();
			$c->select = Array(
				" ACCOUNT_NUMBER ",
				" UNIX_TIMESTAMP( `timestamp` ) AS timestamp ",
				" ACCOUNT_EQUITY ",
			);
			
			$c->addInCondition( 'ACCOUNT_NUMBER', $accounts );
			$c->addInCondition( 'AccountServerId', $servers );
			$c->addCondition(" `timestamp` != '0000-00-00 00:00:00' ");
			$c->addCondition(" DATE(`timestamp`) >= DATE( :begin ) ");
			$c->params[':begin'] = $contest->begin;
			$c->order = " `timestamp`, `ACCOUNT_NUMBER`  ";
			//print_r( $c  );
			$models = MT5AccountDataModel::model()->findAll( $c );
			
			$firstValues = array();
			$lastValues = array();
			foreach( $models as $model ) {
				$data[ $model->timestamp ][ $model->ACCOUNT_NUMBER ] = (float)$model->ACCOUNT_EQUITY;
				$data[ $model->timestamp ][ 'date' ] = date("Y-m-d H:i:s", (int)$model->timestamp);
				// remembering the first value (needed for a start point horizontal bar)
				if(!isset($this->firstValue))
				{
					$this->firstValue = (float)$model->ACCOUNT_EQUITY;
				}
				// remembering the last value of each graph
				$lastValues[ $model->ACCOUNT_NUMBER ] = (float)$model->ACCOUNT_EQUITY;
			}
			
			// continuing each graph to the end of the chart.
			// Every graph will have a point at the last category axis value
			end($data);
			$lastTimestamp = key($data); // getting key of the last element in array
			foreach($accounts as $account)
			{
				if(!isset($data[ $lastTimestamp ][ $account ]) && isset($lastValues[ $account ]))
				{
					$data[ $lastTimestamp ][ $account ] = $lastValues[ $account ];
				}
			}
			
		//	$this->addHistoryData( $data );
			
			return $data;
		}
		function renderWidget() {
			$data = $this->getData();
			//---test
			/*$this->firstValue = 20588;
			$data = array(
				'1400077745'=>array(
					'date'=>date("Y-m-d H:i:s",1400077745),
					'256941'=>20588,
					'256877'=>20588,
					'lineColor_256941'=>"#999999",
					'lineColor_256877'=>"#999999",
				),
				'1400087786'=>array(
					'date'=>date("Y-m-d H:i:s",1400087786),
					'256941'=>20910.65,
					'256877'=>12216.4,
					'lineColor_256941'=>'#D15B47',
					'lineColor_256877'=>'#3A87AD',
				),
				'1400129388'=>array(
					'date'=>date("Y-m-d H:i:s",1400129388),
					'256941'=>21057.67,
					'256877'=>12646.4,
				),
				'1400152079'=>array(
					'date'=>date("Y-m-d H:i:s",1400152079),
					'256941'=>22090.17,
					'256877'=>11786.4,
				),
				'1400232079'=>array(
					'date'=>date("Y-m-d H:i:s",1400232079),
					'256941'=>21795.04,
					'256877'=>13076.4,
				),
				'1400288051'=>array(
					'date'=>date("Y-m-d H:i:s",1400288051),
					'256941'=>20910.39,
					'256877'=>16946.4,
				),
				'1400337786'=>array(
					'date'=>date("Y-m-d H:i:s",1400337786),
					'256941'=>20910.65,
					'256877'=>12216.4,
				),
				'1400389388'=>array(
					'date'=>date("Y-m-d H:i:s",1400389388),
					'256941'=>21057.67,
					'256877'=>12646.4,
				),
				'1400432079'=>array(
					'date'=>date("Y-m-d H:i:s",1400432079),
					'256941'=>22090.17,
					'256877'=>11786.4,
				),
				'1400482079'=>array(
					'date'=>date("Y-m-d H:i:s",1400482079),
					'256941'=>21795.04,
					'256877'=>13076.4,
				),
				'1400538051'=>array(
					'date'=>date("Y-m-d H:i:s",1400538051),
					'256941'=>20910.39,
					'256877'=>16946.4,
				),
				'1400608051'=>array(
					'date'=>date("Y-m-d H:i:s",1400608051),
					'256941'=>20910.39,
					'256877'=>16946.4,
				),
			);*/
			//---end test

			$class = $this->getCleanClassName();
			$this->render( "{$class}/viewAmcharts", Array(
				'data' => $data,
				/*'accounts' => array('256941','256877'),
				'logins' => array('256941'=>'Julio', '256877'=>'Emilio'),
				'colors' => array('256941'=>"#D15B47",'256877'=>"#3A87AD"),
				'contestMembers' => array('256941'=>"6743",'256877'=>"6693"),*/
				'accounts' => $this->getAccounts(),
				'logins' => $this->getLogins(),
				'colors' => $this->getColors(),
				'contestMembers' => $this->getContestMembers(),
				'firstValue' => $this->firstValue,
			));
		}
		function run() {	
			$cache = Yii::app()->getComponent( $this->cacheID );
			$cacheKey = self::CACHE_KEY_PREFIX.$this->idContest;
			
			/*if( $this->cachingDuration > 0 && $cache ) {
				if(( $content = $cache->get( $cacheKey )) !== false ) {
					echo $content;
					return;
				}
			}*/
			
			ob_start();
			$this->renderWidget();
			$content = ob_get_clean();
			
			if( $this->cachingDuration > 0 && $cache ) {
				$dependency = new CDbCacheDependency( " 
					SELECT 		`updatedStats` 
					FROM 		`{{contest}}` 
					WHERE		`id` = :idContest
					LIMIT		1
				" );
				$dependency->params = Array(
					':idContest' => $this->idContest,
				);
				$cache->set( $cacheKey, $content, $this->cachingDuration, $dependency );
			}
			
			echo $content;
		}
	}

?>
