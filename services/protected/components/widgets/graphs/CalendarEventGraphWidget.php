<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CalendarEventGraphWidget extends WidgetBase {
		public $event;
		private function getData() {
			$data = Array();
			$title = null;
			
			if( !$this->event->indicator_id ) return Array( $data, $title );
			
			$c = new CDbCriteria();
			
			$c->select = " * ";
		
			$c->condition = "
				`t`.`indicator_id` = :indicator_id
				and fact_value != ''
				and fact_value != ''
			";
			$c->params[ ':indicator_id' ] = $this->event->indicator_id;
			
			$c->order = " `t`.`dt` ASC ";
			
			$models = CalendarEvent2ValueModel::model()->findAll( $c );
			
			if( count( $models ) > 1 ) {
				foreach( $models as $model ) {
					if ( !isset($_GET['y']) ) {
						$value = (float)$model->fact_value;
					}else{
						$value = (float)$model->fact_value_sec;
					}

					$data[ $model->id ][ 'date' ] = $model->dt;
					$data[ $model->id ][ 'history' ] = $value;
					$data[ $model->id ][ 'history_text' ] = $model->getFactValue();

					if( !strlen( $title )) {
						$title = $model->getTitleFactValue();
					}
				}
			}
					
			return Array( $data, $title );
		}
		function run() {
			list( $data, $title ) = $this->getData();
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/viewAmcharts", Array(
				'data' => $data,
				'title' => $title,
				
			));
		}
	}

?>
