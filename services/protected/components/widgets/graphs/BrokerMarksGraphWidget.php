<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class BrokerMarksGraphWidget extends WidgetBase {
		public $idBroker;
		public $countMarks;
		private function getData() {
			$data = Array();
			
			$marks = BrokerMarkModel::model()->findAll(Array(
				'with' => Array( 
					'user' => Array(
						'select' => " ID, user_login, user_nicename, display_name, firstName, lastName ",
					),
				),
				'condition' => " `t`.`idBroker` = :idBroker ",
				'order' => " `t`.`createdDT` ",
				'params' => Array(
					':idBroker' => $this->idBroker,
				),
			));
			$this->countMarks = count( $marks );
			if( $this->countMarks > 1 ) {
				foreach( $marks as $mark ) {
					$timestamp = strtotime( $mark->createdDT );
					$data[ $timestamp ] = Array(
						'showName' => $mark->user->showName,
						'conditions' => (int)$mark->conditions,
						'execution' => (int)$mark->execution,
						'support' => (int)$mark->support,
						'IO' => (int)$mark->IO,
						'site' => (int)$mark->site,
						'trust' => (int)$mark->trust,
					);
				}
			}
			return $data;
		}
		function run() {
			$data = $this->getData();
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'data' => $data,
			));
		}
	}

?>