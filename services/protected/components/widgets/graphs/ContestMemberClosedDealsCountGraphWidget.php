<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberClosedDealsCountGraphWidget extends WidgetBase {
		public $contestMember;
		
		private function getSqlTimeZone(){
			$diffTimeStr = Yii::App()->db->createCommand( 'SELECT TIMEDIFF(NOW(), UTC_TIMESTAMP)' )->queryScalar();
			$diffTimeArr = explode(':', $diffTimeStr);
			if( $diffTimeArr[0] > 0 ) return '+' . $diffTimeArr[0] . ':' . $diffTimeArr[1];
			return $diffTimeArr[0] . ':' . $diffTimeArr[1];
		}
		private function getData(){
			if( !$this->contestMember ) return false;
			$sqlTimeZone = $this->getSqlTimeZone();
			if( $this->contestMember->server->tradePlatform->name == 'MetaTrader 5' ){

				$model = MT5AccountHistoryDealsModel::model()->findAll(array(
					'select' => ' DATE_FORMAT(CONVERT_TZ(from_unixtime( `DEAL_TIME` ) ,"'.$sqlTimeZone.'","+00:00"), "%H:%i") as OrderCloseDate, count(`ACCOUNT_NUMBER`) as countRows ',
					'condition' => "
						`t`.`ACCOUNT_NUMBER` = :accountNumber
						AND `t`.`AccountServerId` = :idServer
					",
					'order' => " `OrderCloseDate` ASC ",
					'group' => ' OrderCloseDate ',
					'params' => Array(
						':accountNumber' => $this->contestMember->accountNumber,
						':idServer' => $this->contestMember->idServer,
					),
				));
				
			}elseif( $this->contestMember->server->tradePlatform->name == 'MetaTrader 4' ){
				
				$models = MT4AccountHistoryModel::model()->findAll(array(
					'select' => ' DATE_FORMAT(CONVERT_TZ(from_unixtime( `OrderCloseTime` ) ,"'.$sqlTimeZone.'","+00:00"), "%H:%i")  as OrderCloseDate, count(`AccountNumber`) as countRows ',
					'condition' => "
						`t`.`AccountNumber` = :accountNumber
						AND `t`.`AccountServerId` = :idServer
					",
					'order' => " `OrderCloseDate` ASC ",
					'group' => ' OrderCloseDate ',
					'params' => Array(
						':accountNumber' => $this->contestMember->accountNumber,
						':idServer' => $this->contestMember->idServer,
					),
				));
			}
			if( !$models ) return false;
			
			$data = array();
			
			for($i=0; $i<15; $i++) $data[] = array('OrderCloseDate' => '00:00');
			
			foreach( $models as $model ){
				
				if( $model->OrderCloseDate == '00:00' ){
					$data[] = array(
						'OrderCloseDate' => $model->OrderCloseDate,
						'countRows' => $model->countRows
					);
				}else{
					if( $data[count($data)-1]['OrderCloseDate'] != date('H:i', strtotime($model->OrderCloseDate) - 60) ){
						for($i= strtotime($data[count($data)-1]['OrderCloseDate']) + 60; $i < strtotime($model->OrderCloseDate); $i=$i+60){
							$data[] = array('OrderCloseDate' => date('H:i', $i));
						}
					}
					$data[] = array(
						'OrderCloseDate' => $model->OrderCloseDate,
						'countRows' => $model->countRows
					);
				}

			}
			
			if( $data[count($data)-1]['OrderCloseDate'] != '23:59' ){
				for($i= strtotime($data[count($data)-1]['OrderCloseDate']) + 60; $i < strtotime('23:59'); $i=$i+60){
					$data[] = array('OrderCloseDate' => date('H:i', $i));
				}
			}

			for($i=0; $i<15; $i++) $data[] = array('OrderCloseDate' => '23:59');
			
			return $data;
		}
		
		function run() {
			$data = $this->getData();
			if( !$data ) return false;
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/viewAmcharts", Array(
				'data' => $data
			));
		}
	}

?>

