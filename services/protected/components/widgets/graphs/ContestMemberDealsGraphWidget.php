<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberDealsGraphWidget extends WidgetBase {
		public $contestMember;
		public $contestMemberId;
		public $dataUrl = 'http://46.38.57.237:18889/getgrapchmt4.api?';
		public $daysTodisplayGraph = 5; // days
		public $regenerateSymbolsListPeriod = 600; // seconds
		public $fileList = '/data/dealsSymbolsList.txt';
		public $dealsOnGraphFilesDir = '/data/dealsOnGraph/';
		public $dealsOnGraphFilesDirTmp = '/data/dealsOnGraphTmp/';
		public $ajax;
		
		private function setMember() {
			$this->contestMember = ContestMemberModel::model()->findByPk( $this->contestMemberId );
		}
		
		private function getData() {
			
			$dataEvents = Yii::app()->db->createCommand()
					->select('OrderType, OrderOpenPrice, OrderOpenTime, OrderClosePrice, OrderCloseTime, OrderSymbol, OrderTicket, OrderLots')
					->from('mt4_account_history')  //Your Table name
					->where( 'AccountNumber=:accNum AND (OrderType=1 OR OrderType=0) AND AccountServerId=:serverId AND OrderOpenTime >= :daysTodisplay AND OrderCloseTime >= :daysTodisplay ', array( ':accNum'=>$this->contestMember->accountNumber, ':serverId'=>$this->contestMember->idServer, ':daysTodisplay'=> time() - 60*60*24*$this->daysTodisplayGraph ) )
					->order('OrderSymbol DESC')
					->queryAll();
			
			$tmpSymbol = '';
			$outData = array();
			foreach( $dataEvents as $dataEvent ){
				if( $dataEvent['OrderSymbol']!=$tmpSymbol ){
					$tmpSymbol = $dataEvent['OrderSymbol'];
				}
				$dataEvent['OrderOpenTime'] = gmdate( 'Y-m-d H:i', $dataEvent['OrderOpenTime'] );
				$dataEvent['OrderCloseTime'] = gmdate( 'Y-m-d H:i', $dataEvent['OrderCloseTime'] );
				
				$outData[$tmpSymbol]['events'][] = $dataEvent;
			}
			
			$quotesSettings = QuotesSettingsModel::getInstance();

			$replacementStr = $quotesSettings->histAliasReplacement;
			
			//print_r( $outData );
			
			foreach( $outData as $key => $value ){
				$chartDataPhp = QuotesSymbolsModel::getHistDataByHistAlias( $key, $replacementStr, $this->daysTodisplayGraph );

				if( $chartDataPhp ){
					$outData[$key]['chartData'] = $chartDataPhp;
				}else{
					unset( $outData[$key] );
				}
			}
			return $outData;
		}
		private function regenerateSymbolsList() {
			clearstatcache();
			if( (time()-filemtime( Yii::app()->basePath.$this->fileList )) > $this->regenerateSymbolsListPeriod ){
				$symbols = Yii::app()->db->createCommand()
						->select('OrderSymbol')
						->from('mt4_account_history')  //Your Table name
						->where( '(OrderType=1 OR OrderType=0) AND RowDateAdd > NOW() - INTERVAL :daysTodisplay DAY', array( ':daysTodisplay'=>$this->daysTodisplayGraph ) )
						->group('OrderSymbol')
						->queryAll();
				$outStr = '';
				foreach( $symbols as $symbol ){
					$outStr .= $this->dataUrl.$symbol['OrderSymbol'].',,,-3 -O '.Yii::app()->basePath.$this->dealsOnGraphFilesDirTmp.$symbol['OrderSymbol'].".txt\n";
				}
				file_put_contents( Yii::app()->basePath.$this->fileList, $outStr );
			}
		}
		private function showGraph() {
			$showGraph = Yii::app()->db->createCommand()
					->select('COUNT( AccountNumber ) as count')
					->from('mt4_account_history')  //Your Table name
					->where( 'AccountNumber=:accNum AND (OrderType=1 OR OrderType=0) AND AccountServerId=:serverId AND RowDateAdd > NOW() - INTERVAL :daysTodisplay DAY', array( ':accNum'=>$this->contestMember->accountNumber, ':serverId'=>$this->contestMember->idServer, ':daysTodisplay'=>$this->daysTodisplayGraph ) )
					->queryRow();
			return $showGraph['count'];
		}
		function run() {
			$this->regenerateSymbolsList();
			$this->setMember();
			if( $this->showGraph() ){
				if( $this->ajax ) $data = $this->getData();
				$class = $this->getCleanClassName();

				$this->render( "{$class}/viewAmcharts", Array(
					'data' => $data,
					'ajax' => $this->ajax,
					'contestMemberId' => $this->contestMemberId,
				));
				
			}
		}
	}

?>
