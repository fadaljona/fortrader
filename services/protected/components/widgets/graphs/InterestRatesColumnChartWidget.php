<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class InterestRatesColumnChartWidget extends WidgetBase {
		public $jsDp;
		
		private function createJsDp() {
			$models = InterestRatesRateModel::model()->findAll(array(
				'with' => array(
					'bank' => array(
						'with' => array( 'currentLanguageI18N' )
					),
					'currentLanguageI18N',
				),
				'order' => " `bank`.`order` DESC ",
				'condition' => " `bank`.`showInFirstChart` = 1 AND `cLI18NInterestRatesRateModel`.`title` <> '' AND `cLI18NInterestRatesRateModel`.`title` IS NOT NULL AND `cLI18NInterestRatesBankModel`.`title` <> '' AND `cLI18NInterestRatesBankModel`.`title` IS NOT NULL  "
			));
			
			$outArray = array();
			
			foreach( $models as $model ){
				$outArray[] = array(
					'bank' => $model->bank->shortTitle ? $model->bank->shortTitle : $model->bank->title,
					'img' => $model->bank->country->getSrcFlag("shiny", 32),
					'value' => $model->lastValue->value,
					'difDate' => date('d.m.Y', strtotime($model->lastValue->modDate)),
					'contryName' => Yii::t( CountryModel::getModelNSi18n("CountryModel"), $model->bank->country->name),
					'flag' => 0,
				);
			}
			return json_encode( $outArray );

		}
		private function getDP() {
			return $this->jsDp ? $this->jsDp : $this->createJsDp();
		}

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'jsDp' => $this->getDP(),
			));
		}
	}

?>