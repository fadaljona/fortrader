<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberDealsHistoryGraphWidget extends WidgetBase {
		public $member;
		


		function run() {
			
			if( $this->member->server->tradePlatform->name != 'MetaTrader 4' ) return false;
			
			$symbolsArr = $this->member->availableHistorySymbols;
			
			if( !$symbolsArr ) return false;
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'member' => $this->member,
				'symbolsArr' => $symbolsArr,
			));
		}
	}

?>