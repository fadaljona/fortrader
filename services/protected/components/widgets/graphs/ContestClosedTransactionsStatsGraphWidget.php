<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestClosedTransactionsStatsGraphWidget extends WidgetBase {
		public $idContest;
		public $colors = Array( '#F0D216', '#76BEDF' );
		public $ajax;

		private function getData() {
			$data = Array();
						
			$c = new CDbCriteria();
			$c->select = Array(
				" orderSymbol ",
				" sumOrderLots ",
				" orderType ",
			);
			

			$c->addCondition(" `idContest` = :idContest ");
			$c->params[':idContest'] = $this->idContest;
			$c->order = " `sumOrderLots` DESC ";
		
			$models = ClosedTransactionsStatsModel::model()->findAll( $c );
			
			$dataOrderType0 = Array();
			$dataOrderType1 = Array();
			
			foreach( $models as $model ) {
				if( $model->sumOrderLots!=0 ){
					if( $model->orderType ){
						$dataOrderType1[] = array(
							'orderSymbol' => $model->orderSymbol,
							'sumOrderLots' => $model->sumOrderLots,
						);
					}else{
						$dataOrderType0[] = array(
							'orderSymbol' => $model->orderSymbol,
							'sumOrderLots' => $model->sumOrderLots,
						);
					}
				}
			}
			
			$outArray = array(
				0 => $dataOrderType0,
				1 => $dataOrderType1,
			);
			return $outArray;
		}
		
		function renderWidget() {
			$dataArr = $this->getData();
			$class = $this->getCleanClassName();
			
			$i=0;
			$titles = array(
				0 => Yii::t( $NSi18n, 'Total Long Positions' ),
				1 => Yii::t( $NSi18n, 'Total Short Positions' ),
			);
			
			foreach( $dataArr as $data ){
			
				$this->render( "{$class}/viewAmcharts", Array(
					'data' => $data,
					'colors' => $this->colors,
					'pieTitle' => $titles[$i],
				));
				$i++;
			}
		}
		function beforWidget() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/beforWidget", Array(
				'ajax' => $this->ajax,
			));
		}
		function afterWidget() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/afterWidget", Array(
				'ajax' => $this->ajax,
				'idContest' => $this->idContest,
			));
		}
		function run() {	
			
			ob_start();
			$this->beforWidget();
			if( $this->ajax ) $this->renderWidget();
			$this->afterWidget();
			$content = ob_get_clean();
			echo $content;
		}
	}

?>
