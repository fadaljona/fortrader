<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EATradeAccountsGraphWidget extends WidgetBase {
		//const limitMembers = 5;
		const CACHE_KEY_PREFIX = 'EATradeAccountsGraphWidget.';
		public $cachingDuration = 86400;
		public $cacheID = 'cache';
		public $cache2ID = 'cache2';
		public $colors = Array( '#82AF6F', '#F89406', '#D15B47', '#3A87AD', '#D6487E' );
		private function getEAAccounts() {
			static $accounts;
			if( $accounts === null ) {
				TradeAccountModel::updateStats();
				$accounts = EATradeAccountModel::model()->findAll(Array(
					'select' => " id ",
					'with' => Array(
						'tradeAccount' => Array(
							'select' => " id, accountNumber, idServer ",
							'with' => Array(
								'stats' => Array(
									'select' => " deposit ",
								),
							),
						),
						'EA' => Array(
							'select' => " id, name ",
						),
						'version' => Array(
							'select' => " id,version ",
						),
					),
					'condition' => " 
						`stats`.`deposit` 
						AND `tradeAccount`.`status` = 'On'
					",
					'order' => " `stats`.`gain` DESC  ",
					//'limit' => self::limitMembers,
				));
			}
			return $accounts;
		}
		private function getAccounts() {
			static $accounts;
			if( $accounts === null ) {
				$EAAccounts = $this->getEAAccounts();
				$accounts = Array();
				foreach( $EAAccounts as $EAAccount ) {
					$accounts[] = $EAAccount->tradeAccount->accountNumber;
				}
			}
			return $accounts;
		}
		private function getEATradeAccounts() {
			static $EATradeAccounts;
			if( $EATradeAccounts === null ) {
				$EAAccounts = $this->getEAAccounts();
				foreach( $EAAccounts as $EAAccount ) {
					$EATradeAccounts[$EAAccount->tradeAccount->accountNumber] = $EAAccount->id;
				}
			}
			return $EATradeAccounts;
		}
		private function getEA() {
			static $EA;
			if( $EA === null ) {
				$EAAccounts = $this->getEAAccounts();
				foreach( $EAAccounts as $EAAccount ) {
					$EA[$EAAccount->tradeAccount->accountNumber] = "{$EAAccount->EA->name} {$EAAccount->version->version}";
				}
			}
			return $EA;
		}
		private function getDeposits() {
			static $deposits;
			if( $deposits === null ) {
				$EAAccounts = $this->getEAAccounts();
				$deposits = Array();
				foreach( $EAAccounts as $EAAccount ) {
					$deposits[$EAAccount->tradeAccount->accountNumber] = $EAAccount->tradeAccount->stats->deposit;
				}
			}
			return $deposits;
		}
		private function getColors() {
			static $colors;
			if( !$colors ) {
				$accounts = $this->getAccounts();
				foreach( $accounts as $i=>$account ) {
					$color = $this->colors[ $i % count( $this->colors ) ];
					$colors[ $account ] = $color;
				}
			}
			return $colors;
		}
		private function getData() {
			$data = Array();
			
			$deposits = $this->getDeposits();
			$accounts = $this->getEAAccounts();
			
			$whereAccounts = Array();
			foreach( $accounts as $account ) {
				$whereAccounts[] = "( {$account->tradeAccount->accountNumber}, {$account->tradeAccount->idServer} )";
			}
			$impWhereAccounts = implode( ",", $whereAccounts );
			
			$query = "
				SELECT 
					`ACCOUNT_NUMBER`, UNIX_TIMESTAMP( `timestamp` ) AS `timestamp`, `ACCOUNT_BALANCE`
				FROM
					`mt5_account_data`
				WHERE
					( `ACCOUNT_NUMBER`, `AccountServerId` ) IN ( {$impWhereAccounts} )
					AND `timestamp` != '0000-00-00 00:00:00'
				ORDER BY
					`ACCOUNT_NUMBER`, `timestamp`
			";
						
			$account = null;
			$balance = null;
			$models = Yii::App()->db->createCommand( $query )->setFetchMode( PDO::FETCH_OBJ )->queryAll();
			foreach( $models as $model ) {
				if( $account == $model->ACCOUNT_NUMBER and $balance === (float)$model->ACCOUNT_BALANCE ) continue;
				$balance = (float)$model->ACCOUNT_BALANCE;
				$account = $model->ACCOUNT_NUMBER;
				$deposit = $deposits[ $model->ACCOUNT_NUMBER ];
				$gain = ($balance - $deposit) / $deposit * 100;
				$gain = round( $gain, 2 );
				$data[ $account ][ $model->timestamp ] = $gain;
			}

			return $data;
		}
		function renderWidget() {
			$data = $this->getData();
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'data' => $data,
				'accounts' => $this->getAccounts(),
				'colors' => $this->getColors(),
				'EATradeAccounts' => $this->getEATradeAccounts(),
				'EA' => $this->getEA(),
			));
		}
		function run() {
			Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/graphs/wEATradeAccountsGraphWidget.js" );
			
			$cache = Yii::app()->getComponent( $this->cacheID );
			$cacheKey = self::CACHE_KEY_PREFIX;
			
			if( $this->cachingDuration > 0 && $cache ) {
				if(( $content = $cache->get( $cacheKey )) !== false ) {
					echo $content;
					return;
				}
			}
			
			ob_start();
			$this->renderWidget();
			$content = ob_get_clean();
			
			if( $this->cachingDuration > 0 && $cache ) {
				$dependency = new CDbCacheDependency( " 
					SELECT 		COUNT(*), MAX(`trueDT`)
					FROM 		`{{trade_account_stats}}` 
				" );
				$cache->set( $cacheKey, $content, $this->cachingDuration, $dependency );
			}
			
			echo $content;
		}
	}

?>