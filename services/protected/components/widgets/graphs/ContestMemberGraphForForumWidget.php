<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberGraphForForumWidget extends WidgetBase {
		public $contestMember;
		public $accountNumber;
		public $idServer;
		public $revision;
		public $firstValue;
		public $type = 'contest';
		public $groupPeriod = 6; //hours;
		private function getAccountNumber() {
			if( $this->contestMember ) return $this->contestMember->accountNumber;
			return $this->accountNumber;
		}
		private function getIDServer() {
			if( $this->contestMember ) return $this->contestMember->idServer;
			return $this->idServer;
		}
		private function getServer() {
			$server = ServerModel::model()->findByPk( $this->getIDServer() );
			return $server;
		}
		private function getLogin() {
			return $this->contestMember->user->user_login;
		}
		private function getDataCacheKey(){
			return 'ContestMemberGraphForForumWidget.' . $this->getAccountNumber() . '.server.' . $this->getIDServer();
		}
		private function getData() {
			$cacheKey = $this->getDataCacheKey();
			if( $data = Yii::app()->cache->get( $cacheKey ) ) return $data;
			
			$data = Array();
	
			if( $this->type == 'monitoring' ){
				
				$cFirstDataForAccount = new CDbCriteria();
				$cFirstDataForAccount->select = Array(
					" UNIX_TIMESTAMP( `timestamp` ) AS timestamp "
				);
				$cFirstDataForAccount->addInCondition( 'ACCOUNT_NUMBER', Array( $this->getAccountNumber() ));
				$cFirstDataForAccount->addInCondition( 'AccountServerId', Array( $this->getIDServer() ));
				$cFirstDataForAccount->addCondition(" `timestamp` != '0000-00-00 00:00:00' ");
				
				$cFirstDataForAccount->order = " `timestamp` ASC ";
				$cFirstDataForAccount->limit = 1;
				$FirstDataForAccountModel = MT5AccountDataModel::model()->find( $cFirstDataForAccount );
				
				if( $this->contestMember->server->tradePlatform->name == 'MetaTrader 5' ){
					
					$cHist = new CDbCriteria();
					$cHist->select = Array(
						" `DEAL_TIME` AS `timestamp` ",
						" `DEAL_PROFIT` AS `profit` ",
						" `DEAL_COMMISSION` AS `commission` ",
						" `DEAL_SWAP` AS `swap` ",
					);
					$cHist->addInCondition( 'ACCOUNT_NUMBER', Array( $this->getAccountNumber() ));
					$cHist->addInCondition( 'AccountServerId', Array( $this->getIDServer() ));
					$cHist->order = " `DEAL_TIME` ";
					if( $FirstDataForAccountModel ){
						$cHist->addCondition( " `DEAL_TIME` < :firstDataTime " );
						$cHist->params[':firstDataTime'] = $FirstDataForAccountModel->timestamp;
					}
					$modelsHist = MT5AccountHistoryDealsModel::model()->findAll( $cHist );
					
				}elseif( $this->contestMember->server->tradePlatform->name == 'MetaTrader 4' ){
					
					$cHist = new CDbCriteria();
					$cHist->select = Array(
						" `OrderCloseTime` AS `timestamp` ",
						" `OrderProfit` AS `profit` ",
						" `OrderCommission` AS `commission` ",
						" `OrderSwap` AS `swap` ",
					);
					$cHist->addInCondition( 'AccountNumber', Array( $this->getAccountNumber() ));
					$cHist->addInCondition( 'AccountServerId', Array( $this->getIDServer() ));
					$cHist->order = " `OrderCloseTime` ";
					if( $FirstDataForAccountModel ){
						$cHist->addCondition( " `OrderCloseTime` < :firstDataTime " );
						$cHist->params[':firstDataTime'] = $FirstDataForAccountModel->timestamp;
					}
					$modelsHist = MT4AccountHistoryModel::model()->findAll( $cHist );
					
				}
				
				$histBalance =  0;
				
				if( $modelsHist ){
					foreach( $modelsHist as $histRow ){
						$histBalance = $histBalance + $histRow->profit + $histRow->commission + $histRow->swap;
						$data[ $histRow->timestamp ][ 'history' ] = $histBalance;
						$data[ $histRow->timestamp ][ 'date' ] = date("Y-m-d H:i:s", $histRow->timestamp);
					}
				}
			}
			
			
			
			
			$c = new CDbCriteria();
			$c->select = Array(
				" UNIX_TIMESTAMP( `timestamp` ) AS timestamp ",
				" ACCOUNT_BALANCE ",
				" ACCOUNT_EQUITY ",
				" revision ",
			);

			$c->addInCondition( 'ACCOUNT_NUMBER', Array( $this->getAccountNumber() ));
			$c->addInCondition( 'AccountServerId', Array( $this->getIDServer() ));
			$c->addCondition(" `timestamp` != '0000-00-00 00:00:00' ");
			
			$c->order = " `timestamp` ";
			$models = MT5AccountDataModel::model()->findAll( $c );
			
			if( count( $models ) > 1 ) {
				
				$lastIndex = count($models) - 1;
				$lastTimeStamp = $models[ $lastIndex ]->timestamp;
				
				$firstPoint = true;
				$tmpEquity = false;
				$prevData = false;
				
				foreach( $models as $index => $model ) {
					$accountBalance = (float)$model->ACCOUNT_BALANCE;
					$accountEquity = (float)$model->ACCOUNT_EQUITY;
					
					if( $firstPoint ){
						
						if( $this->type == 'monitoring' ){
							if( $modelsHist ){
								$data[ $model->timestamp ][ 'history' ] = $accountBalance;
							}
						}
						
						$data[ $model->timestamp ][ 'balance' ] = $accountBalance;
						$data[ $model->timestamp ][ 'equity' ] = $accountEquity;
						$data[ $model->timestamp ][ 'date' ] = date("Y-m-d H:i:s", $model->timestamp);
						$firstPoint = false;
					}else{
						
						$daysToFirstPoint = ($lastTimeStamp - $model->timestamp) / ( 60*60*24 );
						if( $daysToFirstPoint >= 20 ){
							$endOfPeriod = strtotime("tomorrow", $model->timestamp) - 1;
							$periodVal = 60*60*24;
						}elseif( $daysToFirstPoint >= 8 ){
							$endOfDay = strtotime("tomorrow", $model->timestamp) - 1;
							if( $model->timestamp > $endOfDay - 60*60*12 ){
								$endOfPeriod = $endOfDay;
							}else{
								$endOfPeriod = $endOfDay - 60*60*12;
							}
							$periodVal = 60*60*12;
						}else{
							$endOfDay = strtotime("tomorrow", $model->timestamp) - 1;
							if( $model->timestamp > $endOfDay - 60*60*6 ){
								$endOfPeriod = $endOfDay;
							}elseif( $model->timestamp > $endOfDay - 60*60*12 ){
								$endOfPeriod = $endOfDay - 60*60*6;
							}elseif( $model->timestamp > $endOfDay - 60*60*18 ){
								$endOfPeriod = $endOfDay - 60*60*12;
							}else{
								$endOfPeriod = $endOfDay - 60*60*18;
							}
							$periodVal = 60*60*6;
						}
						
						
						if( $tmpEquity ){
							if( $tmpEquity > $accountEquity ) $tmpEquity = $accountEquity;
						}else{
							$tmpEquity = $accountEquity;
						}
						
						if( isset($models[$index+1]) ){
							if( $models[$index+1]->timestamp > $endOfPeriod ){
								
								if( isset($prevData) && isset($prevData['endOfPeriod' ]) && $prevData['endOfPeriod' ] + $periodVal < $endOfPeriod ){
									$tmpEndOfPeriod = $prevData['endOfPeriod' ] + $periodVal;
									while( $tmpEndOfPeriod < $endOfPeriod  ){
										$data[ $tmpEndOfPeriod ][ 'balance' ] =  $prevData['balance' ];
										$data[ $tmpEndOfPeriod ][ 'equity' ] = $prevData['equity' ];
										$data[ $tmpEndOfPeriod ][ 'date' ] = date("Y-m-d H:i:s", $tmpEndOfPeriod);
										$tmpEndOfPeriod += $periodVal;
									}
								}
								
								$prevData = array(
									'balance' => $accountBalance,
									'equity' => $tmpEquity,
									'endOfPeriod' => $endOfPeriod
								);
								
								$data[ $endOfPeriod ][ 'balance' ] =  $accountBalance;
								$data[ $endOfPeriod ][ 'equity' ] = $tmpEquity;
								$data[ $endOfPeriod ][ 'date' ] = date("Y-m-d H:i:s", $endOfPeriod);
								
								$tmpEquity = false;	
							}
						}else{
							$data[ $model->timestamp ][ 'balance' ] = $accountBalance;
							$data[ $model->timestamp ][ 'equity' ] = $tmpEquity;
							$data[ $model->timestamp ][ 'date' ] = date("Y-m-d H:i:s", $model->timestamp);
						}
					}
				}
			}
			Yii::app()->cache->set( $cacheKey, $data, 60*5);
			return $data;
		}
		function run() {
			$data = 	$this->getData();
			$class = $this->getCleanClassName();
			$this->render( "{$class}/viewAmcharts", Array(
				'data' => $data,
				'login' => $this->getLogin(),
				'type' => $this->type,
				'accountNumber' => $this->getAccountNumber(),
				'contestMember' => $this->contestMember
			));
		}
	}

?>
