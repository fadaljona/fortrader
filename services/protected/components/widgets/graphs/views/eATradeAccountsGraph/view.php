<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lAccount' => 'Account',
		'lTime' => 'Time',
		'lGain' => 'Gain',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	$jsls[ 'lEA' ] = Yii::t( 'singular', 'EA' );
?>
<?if( $data ){?>
	<div class="widget-box wEATradeAccountsGraphWidget">
		<div class="widget-header widget-header-small">
			<h5 class="smaller">
				<?=Yii::t( $NSi18n, 'Leaders graph' )?>
			</h5>
		</div>
		<div class="widget-body">
			<div class="<?=$ins?> wGraph"></div>
			<div class="<?=$ins?> wBabl"></div>
		</div>
	</div>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var ins = ".<?=$ins?>";
			var nsText = ".<?=$ns?>";
			var data = <?=json_encode( $data )?>;
			var accounts = <?=json_encode( $accounts )?>;
			var colors = <?=json_encode( $colors )?>;
			var EATradeAccounts = <?=json_encode( $EATradeAccounts )?>;
			var EA = <?=json_encode( $EA )?>;
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wEATradeAccountsGraphWidget = wEATradeAccountsGraphWidgetOpen({
				ins: ins,
				selector: nsText+' .wEATradeAccountsGraphWidget',
				data: data,
				accounts: accounts,
				colors: colors,
				EATradeAccounts: EATradeAccounts,
				EA: EA,
				ls: ls,
			});
		}( window.jQuery );
	</script>
<?}?>