<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( $data ){ ?>
	<script>
		// formats balloon for each chart bullet
		function formatBalloon(data, graph)
		{
			value = graph.login + ', ' + graph.account_id + '<br /><?=Yii::t( $NSi18n, 'Equity' )?>: ' + data.values.value;
			return value;
		}
	</script>
<div class="widget-box amChartsWidget">
	<div class="widget-header">
		<h4 class="lighter smaller">
			<i class="icon-bar-chart blue"></i>
			<?=Yii::t( $NSi18n, 'Leaders graph' )?>	</h4>
	</div>
	<div class="widget-body">
		<div><?php
	$dataProvider = new CArrayDataProvider(
		$data,
		array(
			'keyField' => false,
			'pagination'=>array(
				'pageSize'=>100000,
			),
		)
	);
	
	$graphs = array();
	// making graphs for each leader
	foreach($accounts as $account)
	{
		$graphs[] = array(
			'account_id' => $account,
			'member_id' => $contestMembers[$account],
			'login' => $logins[$account],
			'title' => $account . ' (' . $logins[$account] . ')',
			'valueField' => $account,
			'type' => 'line',
			'lineThickness' => 2,
			'bullet' => 'round',
			'bulletSize'=>5,
			'bulletColor' => "#FFFFFF",
			//'bulletBorderColor' => $colors[$account],
			'bulletBorderThickness' => 2,
			'bulletBorderAlpha' => 1,
			'lineColor' => $colors[$account],
			'balloonColor' => $colors[$account],
			'negativeBase' => $firstValues[$account],
			//'negativeLineColor' => '#637bb6',
			//'legendColor' => $colors[$account],
			'hideBulletsCount' => 50,
			'fillAlphas' => 0,
			'useLineColorForBulletBorder' => true,
			'lineColorField' => 'lineColor_' . $account,
			'balloonFunction' => 'formatBalloon',
		);
	}
	
	$this->widget('ext.amcharts.EAmChartWidget', array(
		'width'=>'100%',
		'height'=>500,
		'options'=>array(
			'type'=>'serial',
			'dataProvider'=>$dataProvider,
			'categoryField'=>'date',
			'language'=>substr(Yii::app()->language, 0, 2),
			'dataDateFormat'=>'YYYY-MM-DD JJ:NN:SS',
			'creditsPosition'=>'bottom-right',
			'autoMargins'=>false,
			'marginLeft'=>20,
			'categoryAxis'=>array(
				//'title'=>'Date',
				'parseDates'=>true,
				'minPeriod'=>'ss',
				'dashLength'=>1,
				'minorGridEnabled'=>true,
				//'twoLineMode'=>true,
				'axisColor'=>'#DADADA',
				'inside'=>true,
				'labelsEnabled'=>false,
				
			),
			'valueAxes'=>array(
				array(
					//'title'=>'Data',
					'axisAlpha'=>0.2,
					//'dashLength'=>1,
					'position'=>'left',
					'inside'=>true,
					'baseValue'=>$firstValue,
				),
			),
			'mouseWheelZoomEnabled'=>true,
			'graphs'=>$graphs,
			/*'legend'=>array(
				'position'=>'right',
				'useGraphSettings'=>true,
			),*/
			'balloon'=>array(
				//'cornerRadius'=>5,
				//'adjustBorderColor'=>false,
				//'fillColor'=>'#0022FF',
				//'borderColor'=>'#0022FF',
			),
			'export'=>array(
				'enabled'=>false,
			),
			'chartCursor'=>array(
				'cursorPosition'=>'mouse',
				'categoryBalloonDateFormat'=>'JJ:NN, MMMM DD',
				/*'pan'=>true,
				'valueLineEnabled'=>true,
				'valueLineBalloonEnabled'=>true,*/
			),
			'chartScrollbar'=>array(
				'autoGridCount'=>true,
				'scrollbarHeight'=>40,
				'backgroundColor'=>'#BBBBBB',
				'selectedBackgroundColor'=>'#D4D4D4',
				'updateOnReleaseOnly'=>true,
			),
			
			'listeners'=>array(
				'clickGraphItem'=>'function(event) {location.href="' . CHtml::normalizeUrl(array('contestMember/single', 'id'=>'')) . '"+event.graph.member_id;}',
			),
		),
	));
?>
		</div>
	</div>
</div><?php 
}
