<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lAccount' => 'Account',
		'lLogin' => 'Login',
		'lTime' => 'Time',
		'lBalance' => 'Balance',
		'lEquity' => 'Equity',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<?if( $data ){?>
	<div class="widget-box wContestMembersLeadersGraphWidget">
		<div class="widget-header widget-header-small">
			<h5 class="smaller">
				<?=Yii::t( $NSi18n, 'Leaders graph' )?>
			</h5>
		</div>
		<div class="widget-body">
			<div class="<?=$ins?> wGraph"></div>
			<div class="<?=$ins?> wBabl"></div>
		</div>
	</div>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var ins = ".<?=$ins?>";
			var nsText = ".<?=$ns?>";
			var data = <?=json_encode( $data )?>;
			var accounts = <?=json_encode( $accounts )?>;
			var logins = <?=json_encode( $logins )?>;
			var colors = <?=json_encode( $colors )?>;
			var contestMembers = <?=json_encode( $contestMembers )?>;
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wContestMembersLeadersGraphWidget = wContestMembersLeadersGraphWidgetOpen({
				ins: ins,
				selector: nsText+' .wContestMembersLeadersGraphWidget',
				data: data,
				accounts: accounts,
				logins: logins,
				colors: colors,
				contestMembers: contestMembers,
				ls: ls,
			});
		}( window.jQuery );
	</script>
<?}?>