<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();

?>
	<div class="span6">
	<div class="amChartsWidget">

	<?php	
		
	$dataProvider = new CArrayDataProvider(
		$data,
		array(
			'keyField' => false,
			'pagination'=>array(
				'pageSize'=>100000,
			),
		)
	);
	$this->widget('ext.amcharts.EAmChartWidget', array(
		'width'=>'100%',
		'height'=>500,
		'options'=>array(
			'type'=>'pie',
			'dataProvider'=>$dataProvider,
			"valueField"=> "sumOrderLots",
			"titleField"=> "orderSymbol",
			"groupPercent"=>1,
			"groupedTitle" => Yii::t( $NSi18n, 'Other order Symbols' ),
			"theme" => "light",
			"labelsEnabled" => false,
			"autoMargins"=> true,
			"legend"=> array(
				"markerType"=> "circle",
				"position"=> "bottom",
				//"marginRight"=> 80,
				"autoMargins"=> true,
				"truncateLabels"=> 25 // custom parameter
			),
			'amExport'=>array(
				'top'=>0,
				'right'=>0,
				'buttonColor'=>'#EFEFEF',
				'buttonRollOverColor'=>'#DDDDDD',
				'exportPNG'=>true,
				'exportJPG'=>true,
				'exportPDF'=>true,
				'exportSVG'=>true,
			),
		),
	));
?>
		
	</div>
	<div class="pie-title"><?=$pieTitle?></div>
	</div>
