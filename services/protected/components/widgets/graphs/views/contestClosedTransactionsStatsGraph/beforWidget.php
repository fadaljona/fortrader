<?php
	if(!$ajax){ 
		Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/graphs/wContestClosedTransactionsStatsGraphWidget.js" );
		$basePath = Yii::getPathOfAlias('ext.amcharts.assets');
		$baseUrl = Yii::app()->getAssetManager()->publish($basePath);
		Yii::App()->clientScript->registerScriptFile($baseUrl.'/amcharts.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/pie.js');
		
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/lang/'.substr(Yii::app()->language, 0, 2).'.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/amexport.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/canvg.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/filesaver.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/jspdf.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/jspdf.plugin.addimage.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/rgbcolor.js');
	}
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<?php if(!$ajax){ ?><div class="widget-box wContestClosedTransactionsStatsGraphWidget"><?php }?>
	<div class="widget-header">
		<h4><i class="icon-bar-chart blue"></i><?=Yii::t( $NSi18n, 'Closed Transactions Stats' )?></h4>

		<div class="widget-toolbar">
			<a href="#" data-action="collapse">
				<i class="icon-chevron-up"></i>
			</a>
		</div>
	</div>

	<div class="widget-body">
		<div class="widget-main">
			<div class="row-fluid">
			<?php if( !$ajax ){?>
				<div class="<?=$ins?> wTplItem" style="display:none;">
					<div class="progress progress-info progress-striped active">
						<div class="bar" style="width: 100%;"></div>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-small btn-yellow no-radius <?=$ins?> wShowGraph">
						<i class="icon-arrow-down"></i>
						<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Show graph' )?></span>
					</button>
				</div>
			<?php }?>