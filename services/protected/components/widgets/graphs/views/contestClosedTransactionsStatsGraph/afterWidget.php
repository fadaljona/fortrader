			</div>
		</div>
	</div>
<?php if(!$ajax){ ?></div><?php }?>
<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<?php if(!$ajax){ ?>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var ins = ".<?=$ins?>";
		var nsText = ".<?=$ns?>";
		var idContest = <?=$idContest?>;
		ns.wContestClosedTransactionsStatsGraphWidget = wContestClosedTransactionsStatsGraphWidgetOpen({
			ins: ins,
			selector: nsText+' .wContestClosedTransactionsStatsGraphWidget',
			idContest: idContest,
		});
	}( window.jQuery );
</script>
<?php } ?>