<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$cs=Yii::app()->getClientScript();

	$cs->registerScriptFile( '//code.jquery.com/ui/1.11.4/jquery-ui.js' );
	$cs->registerScriptFile( '//code.highcharts.com/stock/highstock.js' );
	$cs->registerScriptFile( '//code.highcharts.com/stock/modules/exporting.js' );
	
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.graphs').'/wContestMemberDealsHistoryChartWidget.js' ) );
	
	Yii::app()->clientScript->registerScript('jsStyler', '
		!function( $ ) {
			
			
			var $historySymbolForHighcharts = $(".'.$ins.' .historySymbolForHighcharts");
			if( $historySymbolForHighcharts.length ){
				var historySymbolForHighchartsPrevVal = $historySymbolForHighcharts.val();
				$historySymbolForHighcharts.styler({
					onSelectClosed: function() {
						if( $historySymbolForHighcharts.val() !=  historySymbolForHighchartsPrevVal ){
							historySymbolForHighchartsPrevVal = $historySymbolForHighcharts.val();
							nsActionView.wContestMemberDealsHistoryChartWidget.historySymbolChanged();
						}
					}
				});
			}
			
			
		}( window.jQuery );
	', CClientScript::POS_END);
	

	
?>
<div class="<?=$ins?>">
	<?php
		$symbolsArrFordropDown = array_flip($symbolsArr);
		echo CHtml::tag( 'div', array('class'=>''),  CHtml::dropDownList( 'historySymbolForHighcharts', current($symbolsArrFordropDown), $symbolsArrFordropDown, array('class'=>'select_type1 historySymbolForHighcharts') ) );
	?>
	<div id="highChartsContestMemberDealsHistory" style="height: 400px; min-width: 310px"></div>

</div>

<script>
!function( $ ) {
	var ns = <?=$ns?>;
	var nsText = ".<?=$ns?>";
	var ins = ".<?=$ins?>";

	ns.wContestMemberDealsHistoryChartWidget = wContestMemberDealsHistoryChartWidgetOpen({
		ns: ns,
		ins: ins,
		selector: nsText+' .<?=$ins?>',
		getDataUrl: '<?=Yii::app()->createUrl("quotesNew/ajaxGetDataForHistoryHighCharts" )?>',
		memberId: <?=$member->id?>
		
	});
}( window.jQuery );
</script>