<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<section class="section_offset">
	<h4 class="toggle_btn section_title"><?=Yii::t($NSi18n, 'Closed deals count stats')?></h4>
	<div class="toggle_content">
		<div class="box_graph">
		<?php
			$dataProvider = new CArrayDataProvider(
				$data,
				array(
					'keyField' => false,
					'pagination'=>array(
						'pageSize'=>100000,
					),
				)
			);
			$this->widget('ext.amcharts.EAmChartWidget', array(
				'width'=>'100%',
				'height'=>350,
				'options'=>array(
					'type'=>'serial',
					'dataProvider'=>$dataProvider,
					'categoryField' => 'OrderCloseDate',
					'language'=>substr(Yii::app()->language, 0, 2),
					'dataDateFormat'=>'JJ:NN',
					'creditsPosition'=>'bottom-right',
					'autoMargins'=>false,
					'marginLeft'=>40,
					'marginRight'=>55,
					'marginTop'=>0,
					'marginBottom'=>30,

					'categoryAxis'=>array(
						'parseDates'=>false,
						'labelOffset'=>-10,
						'equalSpacing'=> true,
						'minPeriod'=>'mm',
						'dashLength'=>1,
						'axisColor'=>'#DADADA',
					),
					'valueAxes'=>array(
						array(
							'axisAlpha'=>0.2,
							'position'=>'left',
							'inside'=>false,
						),
					),
					'mouseWheelZoomEnabled'=>true,
					'graphs'=>array(
						array(
							'id' => 'countRows',
							'title' => Yii::t( $NSi18n, 'Count deals' ),
							'balloonText' => Yii::t( $NSi18n, 'Count deals' ) . ': [[value]]',
							'valueField' => 'countRows',
							'type' => 'line',
							'lineThickness' => 0,
							'bullet' => 'round',
							'bulletSize'=>5,
							'bulletColor' => '#FFFFFF',
							'bulletBorderThickness' => 1,
							'bulletBorderAlpha' => 1,
							'lineColor' => '#89bb50',
							'balloonColor' => '#89bb50',
							'hideBulletsCount' => 100000,
							'fillAlphas' => 0,
							'useLineColorForBulletBorder' => true,
						),
					),
					'chartCursor'=>array(
						'valueLineEnabled' => true,
						'valueLineBalloonEnabled' => true,
						'cursorAlpha' => 0.4,
						'valueLineAlpha' => 0.4,
						'cursorPosition' => 'mouse',
						'categoryBalloonDateFormat'=>'JJ:NN',
					),
					'chartScrollbar'=>array(
						'graph'=>'countRows',
						'autoGridCount'=>true,
						'scrollbarHeight'=>20,
						'backgroundColor'=>'#BBBBBB',
						'selectedBackgroundColor'=>'#D4D4D4',
						'updateOnReleaseOnly'=>true,
					),
					'amExport'=>array(
						'top'=>0,
						'right'=>0,
						'buttonColor'=>'#EFEFEF',
						'buttonRollOverColor'=>'#DDDDDD',
						'exportPNG'=>true,
						'exportJPG'=>true,
						'exportPDF'=>true,
						'exportSVG'=>true,
					),
				),
			));
		?>
		</div>
	</div>
</section>