<?php
	if(!$ajax){ 
		Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/graphs/wContestMemberDealsGraphWidget.js" );
		$basePath = Yii::getPathOfAlias('ext.amcharts.assets');
		$baseUrl = Yii::app()->getAssetManager()->publish($basePath);
		Yii::App()->clientScript->registerScriptFile($baseUrl.'/amcharts.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/serial.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/amstock.js');
		
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/lang/'.substr(Yii::app()->language, 0, 2).'.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/amexport.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/canvg.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/filesaver.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/jspdf.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/jspdf.plugin.addimage.js');
		Yii::App()->clientScript->registerScriptFile($baseUrl . '/exporting/rgbcolor.js');
	}
	
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
	<?php if(!$ajax){ ?><div class="widget-box amChartsWidget wContestMemberDealsGraphWidget spinner-margin position-relative"><?php } ?>

		<section class="section_offset paddingBottom30">
			<h5 class="info_title color2"><?=Yii::t( $NSi18n, 'History of transactions on the account' )?><i aria-hidden="true" class="fa fa-line-chart"></i></h3>
		</section>
		
		<div class="tabs_box accordion_tabs tabs_rating">
	
		<?php if($ajax){ ?>
			<div class="clearfix">
				<div class="wrapper_tabs_list">
					<ul class="tabs_list clearfix overview_list PFDregular">
						<?php 
							$i=0;
							foreach( $data as $symbol => $stockData ){ ?>
							<li class="<?php if($i==0){echo 'active';$i=1;}?>">
								<a href="javascript:;"><?=$symbol?></a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		<?php } ?>

		
		<?php if(!$ajax){ ?>
			<div class="section_offset paddingBottom30">
				<div class="clearfix">
					<a href="#" class="load_btn chench_btn <?=$ins?> wShowGraph"><?=Yii::t( $NSi18n, 'Show graph' )?></a>
				</div>
			</div>
		<?php } ?>
		
	<?php if($ajax){ ?>
		<div class="tabs_contant">
		<?php
$i=0;
foreach( $data as $symbol => $stockData ){
	
	if($i==0){
		echo '<div class="active">';
		$i=1;
	}else{
		echo '<div>';
	}
	
	$stockEvents = array();
	foreach( $stockData['events'] as $stockEvent ){
		if( $stockEvent['OrderType'] ){
			$markType = "arrowDown";
			$backgroundColor = "#CC0000";
			$markDescription = Yii::t( $NSi18n, 'SELL' );
		}else{
			$markType = "arrowUp";
			$backgroundColor = "#00CC00";
			$markDescription = Yii::t( $NSi18n, 'BUY' );
		}
		$stockEvents[] = array(
			'date' => $stockEvent['OrderOpenTime'],
			'type' => $markType,
			'backgroundColor' => $backgroundColor,
			'graph' => "g1",
			'description' => '#'.$stockEvent['OrderTicket'].' '.$markDescription.' '.$stockEvent['OrderOpenPrice'].' '.$stockEvent['OrderLots']
		);
	}
	foreach( $stockData['events'] as $stockEvent ){
		if( $stockEvent['OrderType'] ){
			$markDescription = Yii::t( $NSi18n, 'SELL' );
			$typeBullet = 'triangleUp';
			$backgroundColor = "#CC0000";
			$borderColor = '#CC0000';
			$textColor = '#FFFFFF';
		}else{
			$markDescription = Yii::t( $NSi18n, 'BUY' );
			$typeBullet = 'triangleDown';
			$backgroundColor = "#00CC00";
			$borderColor = '#00CC00';
			$textColor = '#000000';	
		}
		$stockEvents[] = array(
			'date' => $stockEvent['OrderCloseTime'],
			'type' => $typeBullet,
			'backgroundColor' => $backgroundColor,
			'rollOverColor' => $backgroundColor,
			'color' => $textColor,
			//'borderColor' => $borderColor,
			//'showOnAxis' => true,
			'graph' => "g1",
			'text' => "X",
			'description' => '#'.$stockEvent['OrderTicket'].' '.Yii::t( $NSi18n, 'CLOSE' ).' '.$markDescription.' '.$stockEvent['OrderClosePrice'].' '.$stockEvent['OrderLots']
		);
	}

	$this->widget('ext.amcharts.EAmChartWidget', array(
		'width'=>'100%',
		'height'=>350,
		'options'=>array(
			'type'=>'stock',
			
			'dataSets' => array(
				array(
					'color' => "#b0de09",
					'fieldMappings' => array(
						array(
							'fromField' => "close",
							'toField' => "close"
						),
					),
					'dataProvider'=> $stockData['chartData'],
					'categoryField' => 'date',
					// EVENTS
					'stockEvents' => $stockEvents,
				),
			),
			
			'dataDateFormat' => "YYYY-MM-DD JJ:NN",
			'categoryAxesSettings' => array(
				'minPeriod' => 'mm',
			),
			
			'panels' => array(
				array(
			
					'title' => Yii::t( $NSi18n, 'Close value' )." ".$symbol,
					'percentHeight' => 70,

					'stockGraphs' => array( 
						array(
							'id' => "g1",
							'valueField' => "close"
						),
					),

					'stockLegend' => array(
						'valueTextRegular' => " ",
						'markerType' => "none"
					),
				)
			),
			
			'chartScrollbarSettings' => array(
				'graph' => "g1",
			),
			
			'chartCursorSettings' => array(
				'valueBalloonsEnabled' => true,
				'graphBulletSize' => 1,
				'valueLineBalloonEnabled' => true,
				'valueLineEnabled' => true,
				'valueLineAlpha' => 0.5
			),
			
			'panelsSettings' => array(
				'usePrefixes' => true
			),
			'language'=>substr(Yii::app()->language, 0, 2),
			'amExport'=>array(
				'top'=>21,
				'right'=>21,
				'buttonColor'=>'#EFEFEF',
				'buttonRollOverColor'=>'#DDDDDD',
				'exportPNG'=>true,
				'exportJPG'=>true,
				'exportPDF'=>true,
				'exportSVG'=>true,
			),
		),
	));
	echo '</div>';
}
?>

	
		
		
	</div>
	<?php } ?>
	</div>
<?php if(!$ajax){ ?></div><?php } ?>
<?php if(!$ajax){ ?>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var ins = ".<?=$ins?>";
		var nsText = ".<?=$ns?>";
		var contestMemberId = <?=$contestMemberId?>;
		ns.wContestMemberDealsGraphWidget = wContestMemberDealsGraphWidgetOpen({
			ins: ins,
			selector: nsText+' .wContestMemberDealsGraphWidget',
			contestMemberId: contestMemberId,
		});
	}( window.jQuery );
</script>
<?php } ?>