<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/graphs/wCalendarEventGraphWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lMonth_01' => 'Jan',
		'lMonth_02' => 'Feb',
		'lMonth_03' => 'Mar',
		'lMonth_04' => 'Apr',
		'lMonth_05' => 'May',
		'lMonth_06' => 'Jun',
		'lMonth_07' => 'Jul',
		'lMonth_08' => 'Aug',
		'lMonth_09' => 'Sep',
		'lMonth_10' => 'Oct',
		'lMonth_11' => 'Nov',
		'lMonth_12' => 'Dec',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<?if( $data ){?>
	<div class="widget-box wCalendarEventGraphWidget">
		<div class="widget-header widget-header-small">
			<h5 class="smaller">
				<?=Yii::t( $NSi18n, 'Graph' )?>
			</h5>
		</div>
		<div class="widget-body">
			<div class="<?=$ins?> wGraph"></div>
			<div class="<?=$ins?> wBabl"></div>
		</div>
	</div>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var ins = ".<?=$ins?>";
			var nsText = ".<?=$ns?>";
			var data = <?=json_encode( $data )?>;
			var title = "<?=$title?>";
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wCalendarEventGraphWidget = wCalendarEventGraphWidgetOpen({
				ins: ins,
				selector: nsText+' .wCalendarEventGraphWidget',
				data: data,
				title: title,
				ls: ls,
			});
		}( window.jQuery );
	</script>
<?}?>