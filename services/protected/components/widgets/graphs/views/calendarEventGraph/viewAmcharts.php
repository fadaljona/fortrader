<?php
if( $data ){ ?>
	<div class="amChartsWidget">
		<div><?php
	$dataProvider = new CArrayDataProvider(
		$data,
		array(
			'keyField' => false,
			'pagination'=>array(
				'pageSize'=>100000,
			),
		)
	);
	
	$this->widget('ext.amcharts.EAmChartWidget', array(
		'width'=>'100%',
		'height'=>350,
		'options'=>array(
			'type'=>'serial',
			'dataProvider'=>$dataProvider,
			'categoryField' => 'date',
			'language'=>substr(Yii::app()->language, 0, 2),
			'dataDateFormat'=>'YYYY-MM-DD JJ:NN:SS',
			'creditsPosition'=>'bottom-right',
			'autoMargins'=>false,
			'marginLeft'=>10,
			'marginRight'=>10,
			'marginTop'=>0,
			'marginBottom'=>25,
			'categoryAxis'=>array(
				'parseDates'=>true,
				'minPeriod'=>'mm',
				'dashLength'=>1,
				'minorGridEnabled'=>false,
				'axisColor'=>'#DADADA',
			),
			'valueAxes'=>array(
				array(
					'axisAlpha'=>0.2,
					//'dashLength'=>1,
					'position'=>'left',
					'inside'=>true,
					'baseValue'=>$firstValue,
				),
			),
			'mouseWheelZoomEnabled'=>true,
			'graphs'=>array(
				array(
					'id' => 'history',
					'title' => Yii::t( $NSi18n, 'History' ),
					'balloonText' => Yii::t( $NSi18n, 'History' ) . ': [[value]]',
					'valueField' => 'history',
					'type' => 'line',
					'lineThickness' => 2,
					'bullet' => 'round',
					'bulletSize'=>5,
					'bulletColor' => '#FFFFFF',
					'bulletBorderThickness' => 2,
					'bulletBorderAlpha' => 1,
					'lineColor' => '#167ac6',
					'balloonColor' => '#167ac6',
					'hideBulletsCount' => 50,
					'fillAlphas' => 0,
					'useLineColorForBulletBorder' => true,
				),
			),
			'chartCursor'=>array(
				'cursorPosition' => 'mouse',
				'categoryBalloonDateFormat'=>'JJ:NN, MMMM DD YYYY',
			),
			'chartScrollbar'=>array(
				'graph'=>'history',
				'autoGridCount'=>true,
				'scrollbarHeight'=>40,
				'backgroundColor'=>'#BBBBBB',
				'selectedBackgroundColor'=>'#D4D4D4',
				'updateOnReleaseOnly'=>true,
			),
			'amExport'=>array(
				'top'=>21,
				'right'=>21,
				'buttonColor'=>'#EFEFEF',
				'buttonRollOverColor'=>'#DDDDDD',
				'exportPNG'=>true,
				'exportJPG'=>true,
				'exportPDF'=>true,
				'exportSVG'=>true,
			),
		),
	));
?>
		</div>
	</div><?php
}
