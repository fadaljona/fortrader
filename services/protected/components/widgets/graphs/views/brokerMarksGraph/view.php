<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/graphs/wBrokerMarksGraphWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<?if( $data ){?>
	<div class="wBrokerMarksGraphWidget">
		<div class="<?=$ins?> wGraph"></div>
		<div class="<?=$ins?> wBabl">
			<div class="wShowName"></div>
			<div class="wDT"></div>
			<div class="wLine"></div>
			<div class="iDiv i17 i18"></div><?=Yii::t( '*', 'Conditions' )?>:&nbsp;&nbsp;<span class="wConditions"></span><br>
			<div class="iDiv i17 i19"></div><?=Yii::t( '*', 'Execution' )?>:&nbsp;&nbsp;<span class="wExecution"></span><br>
			<div class="iDiv i17 i20"></div><?=Yii::t( '*', 'Support' )?>:&nbsp;&nbsp;<span class="wSupport"></span><br>
			<div class="iDiv i17 i21"></div><?=Yii::t( '*', 'Input/Output' )?>:&nbsp;&nbsp;<span class="wIO"></span><br>
			<div class="iDiv i17 i22"></div><?=Yii::t( '*', 'Site' )?>:&nbsp;&nbsp;<span class="wSite"></span><br>
			<div class="wLine"></div>
			<?=Yii::t( '*', 'Trust' )?>:&nbsp;&nbsp;<span class="wTrust"></span>
		</div>
	</div>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var ins = ".<?=$ins?>";
			var nsText = ".<?=$ns?>";
			var data = <?=json_encode( $data )?>;
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wBrokerMarksGraphWidget = wBrokerMarksGraphWidgetOpen({
				ins: ins,
				selector: nsText+' .wBrokerMarksGraphWidget',
				data: data,
				ls: ls,
			});
		}( window.jQuery );
	</script>
<?}?>