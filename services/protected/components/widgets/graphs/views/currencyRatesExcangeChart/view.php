<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$languageAlias = LanguageModel::getCurrentLanguageAlias();
	
	$cs=Yii::app()->getClientScript();
	$amchartsBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.amcharts') );
	
	$cs->registerScriptFile($amchartsBaseUrl.'/amcharts.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/serial.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/plugins/export/export.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/lang/'.$languageAlias.'.js');
	$cs->registerCssFile($amchartsBaseUrl.'/plugins/export/export.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.graphs').'/wCurrencyRatesExcangeChartWidgetWidget.js' ) );
	
	$chartId = 'chartdiv' . time();
?>

<div class="exchange-graph-container <?=$ins?> position-relative spinner-margin">
	<h3><?=Yii::t( $NSi18n, 'Online graph' )?> <?=floatval($model->value)?> <?=$model->rateFrom->code?>/<?=$model->rateTo->code?></h3>
	<p><?=Yii::t( $NSi18n, 'This graph displays the online ratio' )?> <?=floatval($model->value)?> <?=$model->rateFrom->code?>/<?=$model->rateTo->code?> <?=Yii::t( $NSi18n, 'or' )?> (<?=$model->rateFrom->name?> / <?=$model->rateTo->name?>)</p>

	<div class="converterChartHeight" id="<?=$chartId?>"></div>
	<div class="clearfix periodChangeWrap">
		<ul class="quotes_list_sm marginBottom0 changePeriodLinks">                   
			<li><span><?=Yii::t( $NSi18n, 'Period' )?>:</span></li>
			<li><a href="#" data-val="7"><?=Yii::t( $NSi18n, 'Week' )?></a></li>
			<li><a href="#" data-val="30"><?=Yii::t( $NSi18n, 'Month' )?></a></li>
			<li><a href="#" data-val="91"><?=Yii::t( $NSi18n, 'Quarter' )?></a></li>
			<li class="active"><a href="#" data-val="365"><?=Yii::t( $NSi18n, 'Year' )?></a></li>
			<li><a href="#" data-val="1825">5 <?=Yii::t( $NSi18n, 'CurrYears' )?></a></li>
		</ul>
	</div>
	
</div>

<script>
!function( $ ) {
	var ns = <?=$ns?>;
	
				
	ns.wCurrencyRatesExcangeChartWidget = wCurrencyRatesExcangeChartWidgetOpen({
		selector: '.<?=$ns?> .<?=$ins?>',
		idModel: <?=$model->id?>,
		loadChartDataUrl: '<?=Yii::app()->createAbsoluteUrl('currencyRates/ajaxLoadConverterChartData')?>',
		chartData: <?=json_encode($chartData)?>,
		chartSelector: '<?=$chartId?>',
		exportLibs: '<?=Yii::app()->getBaseUrl(true)?>/amcharts/plugins/export/libs/',
		lang: '<?=$languageAlias?>',
		label: '<?=floatval($model->value)?> <?=$model->rateFrom->code?> <?=Yii::t('*', 'fromToCur')?> <?=$model->rateTo->code?>',
		baloonTitle: '<?=floatval($model->value)?> <?=$model->rateFrom->code?>',
		baloonAfterTitle: '<?=$model->rateTo->code?>',
	});
}( window.jQuery );
</script>