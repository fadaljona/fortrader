<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<!-- graph -->
<div class="graph">	
	<div class="heading-box graph-hedading clear">
		<p class="name"><?=$contestMember->imgInformerTitle?></p>
		<p class="text"><?=Yii::t($NSi18n, 'Graph of account') . ' ' . $accountNumber?></p>
	</div>
<?php
	if( $data ){ 
	$dataProvider = new CArrayDataProvider(
		$data,
		array(
			'keyField' => false,
			'pagination'=>array(
				'pageSize'=>100000,
			),
		)
	);
	
	$this->widget('ext.amcharts.EAmChartWidget', array(
		'width'=>'100%',
		'height'=>204,
		'options'=>array(
			'backgroundColor' => '#F2F4F5',
			'backgroundAlpha' => 0.5,
			'type'=>'serial',
			'dataProvider'=>$dataProvider,
			'categoryField' => 'date',
			'language'=>substr(Yii::app()->language, 0, 2),
			'dataDateFormat'=>'YYYY-MM-DD JJ:NN:SS',
			'creditsPosition'=>'bottom-right',
			'autoMargins'=>false,
			'marginLeft'=>50,
			'marginRight'=>0,
			'marginTop'=>0,

			'categoryAxis'=>array(
				'parseDates'=>true,
				
				'labelOffset'=>-10,
				'equalSpacing'=> false,
				/*'gridCount'=>100,
				'autoGridCount'=>false,*/
				
				'minPeriod'=>'ss',
				'dashLength'=>1,
				//'minorGridEnabled'=>true,
				
				'axisColor'=>'#a6cbe8',
				'gridColor' => '#dfe3e6',
				'gridAlpha' => 1,
				'axisAlpha'=>1,
				'color' => '#414649',
	
			),
			'valueAxes'=>array(
				array(
					'axisAlpha'=>1,
					'axisColor'=>'#a6cbe8',
					'position'=>'left',
					'inside'=>false,
					'gridAlpha' => 0,
					'gridColor' => '#dfe3e6',
					'color' => '#5188a6',
				),
			),
			'mouseWheelZoomEnabled'=>true,
			'graphs'=>array(
				array(
					'id' => 'balance',
					'title' => Yii::t( $NSi18n, 'Balance' ),
					'balloonText' => Yii::t( $NSi18n, 'Balance' ) . ': [[value]]',
					'valueField' => 'balance',
					'type' => 'line',
					'lineThickness' => 1,
					'bullet' => 'round',
					'bulletSize'=>5,
					'bulletColor' => '#FFFFFF',
					'bulletBorderThickness' => 1,
					'bulletBorderAlpha' => 1,
					'lineColor' => '#4688ba',
					'balloonColor' => '#4688ba',
					'hideBulletsCount' => 100,
					'fillAlphas' => 0,
					'useLineColorForBulletBorder' => true,
				),
				array(
					'id' => 'equity',
					'title' => Yii::t( $NSi18n, 'Min Equity' ),
					'balloonText' => Yii::t( $NSi18n, 'Min Equity' ) . ': [[value]]',
					'valueField' => 'equity',
					'type' => 'line',
					'lineThickness' => 1,
					'bullet' => 'round',
					'bulletSize'=>5,
					'bulletColor' => '#FFFFFF',
					'bulletBorderThickness' => 1,
					'bulletBorderAlpha' => 1,
					'lineColor' => '#f64819',
					'balloonColor' => '#f64819',
					'hideBulletsCount' => 100,
					'fillAlphas' => 0,
					'useLineColorForBulletBorder' => true,
				),
				array(
					'id' => 'history',
					'title' => Yii::t( $NSi18n, 'History' ),
					'balloonText' => Yii::t( $NSi18n, 'History' ) . ': [[value]]',
					'valueField' => 'history',
					'type' => 'line',
					'lineThickness' => 1,
					'bullet' => 'round',
					'bulletSize'=>5,
					'bulletColor' => '#FFFFFF',
					'bulletBorderThickness' => 1,
					'bulletBorderAlpha' => 1,
					'lineColor' => '#999999',
					'balloonColor' => '#999999',
					'hideBulletsCount' => 100,
					'fillAlphas' => 0,
					'useLineColorForBulletBorder' => true,
				),
			),
			'chartCursor'=>array(
				'cursorPosition' => 'mouse',
				'categoryBalloonDateFormat'=>'JJ:NN, MMMM DD',
				'categoryBalloonColor' => '#f64819',
				'cursorColor' => '#f64819',
			),
			'chartScrollbar'=>array(
				'graph'=>'equity',
				'autoGridCount'=>true,
				'scrollbarHeight'=>20,
				'backgroundColor'=>'#BBBBBB',
				'selectedBackgroundColor'=>'#D4D4D4',
				'updateOnReleaseOnly'=>true,
			),
			'amExport'=>array(
				'top'=>20,
				'right'=>0,
				'buttonColor'=>'#EFEFEF',
				'buttonRollOverColor'=>'#DDDDDD',
				'exportPNG'=>true,
				'exportJPG'=>true,
				'exportPDF'=>true,
				'exportSVG'=>true,
			),
		),
	));

}?>
</div>
<!-- graph -->
