<?php 
	$languageAlias = LanguageModel::getCurrentLanguageAlias();

	$cs=Yii::app()->getClientScript();
	$amchartsBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.amcharts') );
	
	$cs->registerScriptFile($amchartsBaseUrl.'/amcharts.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/serial.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/plugins/export/export.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/lang/'.$languageAlias.'.js');
	$cs->registerCssFile($amchartsBaseUrl.'/plugins/export/export.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.graphs').'/wInterestRatesLineChartWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$chartId = 'InterestRatesLineChart';
?>
<div class="<?=$ins?> wInterestRatesLineChartWidget">
	<div id="<?=$chartId ?>" style="height:410px;"></div>
</div>


<script>

!function( $ ) {
	var ns = ".<?=$ns?>";
	var nsText = ".<?=$ns?>";
	var ins = ".<?=$ins?>";

	ns.wwInterestRatesLineChartWidget = wwInterestRatesLineChartWidgetOpen({
		ns: ns,
		ins: ins,
		selector: nsText+' .<?=$ins?>',
		chartId: '<?=$chartId?>',
		lang: '<?=$languageAlias?>',
		dp: <?=$jsDpAndGraphs['dp']?>,
		graphs: <?=$jsDpAndGraphs['graphs']?>,
	});
}( window.jQuery );

</script>