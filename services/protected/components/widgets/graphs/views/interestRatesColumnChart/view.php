<?php 
	$languageAlias = LanguageModel::getCurrentLanguageAlias();

	$cs=Yii::app()->getClientScript();
	$amchartsBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.amcharts') );
	
	$cs->registerScriptFile($amchartsBaseUrl.'/amcharts.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/serial.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/plugins/export/export.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/lang/'.$languageAlias.'.js');
	$cs->registerCssFile($amchartsBaseUrl.'/plugins/export/export.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.graphs').'/wInterestRatesColumnChartWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$chartId = 'interestRatesColumnChart';
?>
<div class="<?=$ins?> wInterestRatesColumnChartWidget">
	<div id="<?=$chartId ?>" style="height:410px;"></div>
</div>


<script>

!function( $ ) {
	var ns = ".<?=$ns?>";
	var nsText = ".<?=$ns?>";
	var ins = ".<?=$ins?>";

	ns.wInterestRatesColumnChartWidget = wInterestRatesColumnChartWidgetOpen({
		ns: ns,
		ins: ins,
		selector: nsText+' .<?=$ins?>',
		chartId: '<?=$chartId?>',
		lang: '<?=$languageAlias?>',
		dp: <?=$jsDp?>,
	});
}( window.jQuery );

</script>