<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/graphs/wContestMemberGraphWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lAccount' => 'Account',
		'lLogin' => 'Login',
		'lTime' => 'Time',
		'lBalance' => 'Balance',
		'lEquity' => 'Equity',
		'lHistory' => 'History',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<?if( $data ){?>
	<div class="widget-box wContestMemberGraphWidget">
		<div class="widget-header widget-header-small">
			<h5 class="smaller">
				<?=Yii::t( $NSi18n, 'Account graph' )?>
			</h5>
		</div>
		<div class="widget-body">
			<div class="<?=$ins?> wGraph"></div>
			<div class="<?=$ins?> wBabl"></div>
		</div>
	</div>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var ins = ".<?=$ins?>";
			var nsText = ".<?=$ns?>";
			var data = <?=json_encode( $data )?>;
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wContestMemberGraphWidget = wContestMemberGraphWidgetOpen({
				ins: ins,
				selector: nsText+' .wContestMemberGraphWidget',
				data: data,
				ls: ls,
			});
		}( window.jQuery );
	</script>
<?}?>