<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();

	if( $data ){ ?>
	
	<section class="section_offset paddingBottom0">
	<?php
		if( $type == 'monitoring' ){
			echo Chtml::tag('h4', array('class' => 'toggle_btn'), Yii::t($NSi18n, 'Graph of account') . ' ' . $accountNumber );
		}else{
			echo CHtml::tag('h5', array('class' => 'info_title color2'), Yii::t( $NSi18n, 'Graph of member' ). ' ' . $login . '<i class="fa fa-line-chart" aria-hidden="true"></i>');
		}
	?>
	
	<div class="<?php if( $type == 'monitoring' ) echo 'toggle_content'; else echo 'section_offset paddingBottom50';?>">
		<div class="box_graph">
		<?php
	$dataProvider = new CArrayDataProvider(
		$data,
		array(
			'keyField' => false,
			'pagination'=>array(
				'pageSize'=>100000,
			),
		)
	);
	
	$this->widget('ext.amcharts.EAmChartWidget', array(
		'width'=>'100%',
		'height'=>350,
		'options'=>array(
			'type'=>'serial',
			'dataProvider'=>$dataProvider,
			'categoryField' => 'date',
			'language'=>substr(Yii::app()->language, 0, 2),
			'dataDateFormat'=>'YYYY-MM-DD JJ:NN:SS',
			'creditsPosition'=>'bottom-right',
			'autoMargins'=>false,
			'marginLeft'=>10,
			'marginRight'=>55,
			'marginTop'=>0,

			'categoryAxis'=>array(
				'parseDates'=>true,
				
				'labelOffset'=>-10,
				'equalSpacing'=> false,
				/*'gridCount'=>100,
				'autoGridCount'=>false,*/
				
				'minPeriod'=>'ss',
				'dashLength'=>1,
				//'minorGridEnabled'=>true,
				'axisColor'=>'#DADADA',
			),
			'valueAxes'=>array(
				array(
					'axisAlpha'=>0.2,
					'position'=>'left',
					'inside'=>true,
				),
			),
			'mouseWheelZoomEnabled'=>true,
			'graphs'=>array(
				array(
					'id' => 'balance',
					'title' => Yii::t( $NSi18n, 'Balance' ),
					'balloonText' => Yii::t( $NSi18n, 'Balance' ) . ': [[value]]',
					'valueField' => 'balance',
					'type' => 'line',
					'lineThickness' => 2,
					'bullet' => 'round',
					'bulletSize'=>5,
					'bulletColor' => '#FFFFFF',
					'bulletBorderThickness' => 2,
					'bulletBorderAlpha' => 1,
					'lineColor' => '#3A87AD',
					'balloonColor' => '#3A87AD',
					'hideBulletsCount' => 50,
					'fillAlphas' => 0,
					'useLineColorForBulletBorder' => true,
				),
				array(
					'id' => 'equity',
					'title' => Yii::t( $NSi18n, 'Min Equity' ),
					'balloonText' => Yii::t( $NSi18n, 'Min Equity' ) . ': [[value]]',
					'valueField' => 'equity',
					'type' => 'line',
					'lineThickness' => 2,
					'bullet' => 'round',
					'bulletSize'=>5,
					'bulletColor' => '#FFFFFF',
					'bulletBorderThickness' => 2,
					'bulletBorderAlpha' => 1,
					'lineColor' => '#D15B47',
					'balloonColor' => '#D15B47',
					'hideBulletsCount' => 50,
					'fillAlphas' => 0,
					'useLineColorForBulletBorder' => true,
				),
				array(
					'id' => 'history',
					'title' => Yii::t( $NSi18n, 'History' ),
					'balloonText' => Yii::t( $NSi18n, 'History' ) . ': [[value]]',
					'valueField' => 'history',
					'type' => 'line',
					'lineThickness' => 2,
					'bullet' => 'round',
					'bulletSize'=>5,
					'bulletColor' => '#FFFFFF',
					'bulletBorderThickness' => 2,
					'bulletBorderAlpha' => 1,
					'lineColor' => '#999999',
					'balloonColor' => '#999999',
					'hideBulletsCount' => 50,
					'fillAlphas' => 0,
					'useLineColorForBulletBorder' => true,
				),
			),
			'chartCursor'=>array(
				'cursorPosition' => 'mouse',
				'categoryBalloonDateFormat'=>'JJ:NN, MMMM DD',
			),
			'chartScrollbar'=>array(
				'graph'=>'equity',
				'autoGridCount'=>true,
				'scrollbarHeight'=>20,
				'backgroundColor'=>'#BBBBBB',
				'selectedBackgroundColor'=>'#D4D4D4',
				'updateOnReleaseOnly'=>true,
			),
			'amExport'=>array(
				'top'=>0,
				'right'=>0,
				'buttonColor'=>'#EFEFEF',
				'buttonRollOverColor'=>'#DDDDDD',
				'exportPNG'=>true,
				'exportJPG'=>true,
				'exportPDF'=>true,
				'exportSVG'=>true,
			),
		),
	));
?>
		
	</div>
	</div>
	</section>
	<?php
}
