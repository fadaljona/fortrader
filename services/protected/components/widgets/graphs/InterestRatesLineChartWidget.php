<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class InterestRatesLineChartWidget extends WidgetBase {
		public $jsDpAndGraphs;
		
		private function createJsDpAndGraphs() {
			$models = InterestRatesRateModel::model()->findAll(array(
				'with' => array(
					'bank' => array(
						'with' => array( 'currentLanguageI18N' )
					),
					'currentLanguageI18N',
				),
				'order' => " `bank`.`order` DESC ",
				'condition' => " `bank`.`showInSecondChart` = 1  AND `cLI18NInterestRatesRateModel`.`title` <> '' AND `cLI18NInterestRatesRateModel`.`title` IS NOT NULL AND `cLI18NInterestRatesBankModel`.`title` <> '' AND `cLI18NInterestRatesBankModel`.`title` IS NOT NULL "
			));
			
			$graphsArray = array();
			$dpArray = array();
			
			foreach( $models as $model ){
				$bankName = $model->bank->shortTitle ? $model->bank->shortTitle : $model->bank->title;
				$graphsArray[] = array(
					"balloonText" => "<div style='line-height:16px;'><img width='16' src='".$model->bank->country->getSrcFlag("shiny", 32)."'> $bankName: <b>[[value]]%</b></div>",
					"type" => "step",
					"title" => $model->bank->title,
					"bullet" => "round",
					"bulletBorderAlpha" => 1,
					"bulletColor" => "#FFFFFF",
					"useLineColorForBulletBorder" => true,
					"valueField" => $model->id,
					"fillAlphas" => 0,
					"customMarker" => $model->bank->country->getSrcFlag("shiny", 32),
					"lineColor" => $model->bank->secondChartColor ? $model->bank->secondChartColor : '#000000',
					"bulletSize" => 5,
				);
				foreach( $model->chartValues as $val ){
					$dpArray[strtotime($val->modDate)][$model->id] = $val->value;
				}
			}
			
			ksort( $dpArray );
			
			$dpOutArray = array();
			
			foreach( $dpArray as $date => $array ){
				$array['date'] = date('Y-m-d', $date);
				$array['time'] = $date;
				$dpOutArray[]=$array;
			}
			
			return array( 'graphs' => json_encode($graphsArray), 'dp' => json_encode($dpOutArray) );

		}
		private function getJsDpAndGraphs() {
			return $this->jsDpAndGraphs ? $this->jsDpAndGraphs : $this->createJsDpAndGraphs();
		}

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'jsDpAndGraphs' => $this->getJsDpAndGraphs(),
			));
		}
	}

?>