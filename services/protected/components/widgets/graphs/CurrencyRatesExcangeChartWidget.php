<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesExcangeChartWidget extends WidgetBase {
		public $model;	

		function run() {
			$class = $this->getCleanClassName();
	
			if( $this->model->type == 'cbr' ){
				$chartData = CurrencyRatesHistoryModel::getDataForConverterChart( 365, $this->model->id );
			}elseif( $this->model->type == 'ecb' ){
				$chartData = CurrencyRatesHistoryEcbModel::getDataForConverterChart( 365, $this->model->id );
			}
			
			$this->render( "{$class}/view", Array(
				'model' => $this->model,
				'chartData' => $chartData,
			));
		}
	}

?>