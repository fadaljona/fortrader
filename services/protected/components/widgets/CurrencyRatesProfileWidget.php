<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesProfileWidget extends WidgetBase {
		public $model;

		private function createHistDP() {
			$c = new CDbCriteria();

			$c->addCondition( " `t`.`idCurrency` = :id ");
			$c->params[':id'] = $this->model->id;
			$c->order = " `date` DESC ";
			$c->limit = 7;
			$c->with = array( 'currency' );
			
			if( $this->model->dataType == 'cbr' ){
				$tableName = 'CurrencyRatesHistoryModel';
			}elseif( $this->model->dataType == 'ecb' ){
				$tableName = 'CurrencyRatesHistoryEcbModel';
			}
			
			$DP = new CActiveDataProvider( $tableName, Array(
				'criteria' => $c,
				'pagination' => false
			));
			return $DP;
		}
		private function getChartData(){
			if( $this->model->dataType == 'cbr' ){
				return CurrencyRatesHistoryModel::getDataForPeriod( 365, $this->model->id );
			}elseif( $this->model->dataType == 'ecb' ){
				return CurrencyRatesHistoryEcbModel::getDataForPeriod( 365, $this->model->id );
			}
		}
		function run() {
			$class = $this->getCleanClassName();
			
			$this->render( "{$class}/view", Array(
				'model' => $this->model,
				'currenciesForChangePage' => CurrencyRatesModel::getCurrenciesForChangePage( $this->model->dataType ),
				'popularCurrencies' => CurrencyRatesModel::getPopularCurrencies(),
				'histDp' => $this->createHistDP(),
				'chartData' => $this->getChartData(),
			));
		}
	}

?>