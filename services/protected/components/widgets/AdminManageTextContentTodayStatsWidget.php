<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminManageTextContentTodayStatsWidget extends WidgetBase {
		
		private function getTodayStats(){
			
			$c = new CDbCriteria();
			$c->select = array(
				" `t`.* ",
				" SUM( CHAR_LENGTH (replace(`t`.`newVal`, `t`.`oldVal`, '')) ) as dayLength "
			);
			$c->group = " `t`.`lang` ";
			$c->order = " `t`.`lang` DESC ";
			
			if( !Yii::App()->user->checkAccess( 'textContentAdminControl' ) ){
				$c->addCondition('  `t`.`idUser` = :idUser ');
				$c->params[':idUser'] = Yii::app()->user->id;
			}
			
			$c->addCondition('  `t`.`dayDate` = :dayDate ');
			$c->params[':dayDate'] = date('Y-m-d');
			
			$models = ManageTextContentHistoryModel::model()->findAll($c);
	
			return $models;
		}

		function run() {
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'todayStats' => $this->getTodayStats(),
			));
		}
	}

?>