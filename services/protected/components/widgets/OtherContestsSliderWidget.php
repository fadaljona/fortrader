<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class OtherContestsSliderWidget extends WidgetBase {
		public $excludeModel;
		public $offset = 0;
		public $mode = 'full';
		private $models;
		
		private function getModels(){
			if( $this->models ) return $this->models;
			$this->models = ContestModel::model()->findAll( array(
				'condition' => " `t`.`id` <> :id ",
				'with' => Array('currentLanguageI18N' ),
				'order' => ' `t`.`begin` DESC, `cLI18NContest`.`name` ASC',
				'params' => array( ':id' => $this->excludeModel ),
				'offset' => $this->offset,
				'limit' => 3
			) );
			return $this->models;
		}
		private function renderItems(){
			ob_start();
			$class = $this->getCleanClassName();
			$this->render( "{$class}/items", Array(
				'models' => $this->getModels(),
			));
			$content = ob_get_clean();
			return $content;
		}

		function run() {
			if( $this->mode == 'items' ){
				echo $this->renderItems();
				return true;
			}
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'models' => $this->getModels(),
				'excludeId' => $this->excludeModel,
				'items' => $this->renderItems(),
			));
		}
	}

?>