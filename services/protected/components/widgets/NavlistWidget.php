<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'components.NavlistComponent' );
	
	final class NavlistWidget extends WidgetBase {
		private function getNavlist() {
			return NavlistComponent::getNavlist();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'navlist' => $this->getNavlist(),
			));
		}
	}

?>