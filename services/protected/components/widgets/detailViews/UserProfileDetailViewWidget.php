<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class UserProfileDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wUserProfileDetailView';
		public $class = 'iDetailView i01 profile-user-info';
		public $tagName = 'div';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfile/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Username', 'name' => 'user_login', 'valueCssClass' => 'color_blue', ),
				Array( 'label' => 'Location', 'visible' => $this->data->country || strlen( $this->data->city ), 'type' => 'raw', 'value' => $this->formatLocation() ),
				/*Array( 'label' => 'Joined', 'visible' => $this->data->user_registered, 'value' => $this->formatJoined() ),*/
				Array( 'label' => 'Joined since', 'visible' => $this->data->usedDT, 'type' => 'raw', 'value' => $this->formatJoinedSince(), 'valueCssClass' => 'color_orange', ),
				Array( 'label' => 'I trade in', 'visible' => $this->data->brokers, 'type' => 'raw', 'value' => $this->formatBrokers() ),
				Array( 'label' => 'Website', 'visible' => $this->data->user_url, 'type' => 'raw', 'value' => CHtml::link( CHtml::encode( $this->data->user_url ), $this->data->getRedirectUserUrl(), Array( 'target' => '_blank' ))),
				Array( 'label' => 'ICQ', 'visible' => !is_null( $this->data->icq ) && $this->data->icq, 'name' => 'icq' ),
				Array( 'label' => 'Skype', 'visible' => !is_null( $this->data->skype ) && $this->data->skype, 'name' => 'skype' ),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]) . ':'; unset( $attribute );
			return $attributes;
		}
		function formatLocation() {
			return $this->renderPart( dirname( __FILE__ ).'/userProfile/location.php' );
		}
		function formatJoined() {
			return date( "d/m/Y", strtotime( $this->data->user_registered ));
		}
		function formatJoinedSince() {
			return $this->renderWidget( 'widgets.parts.TimeDiffWidget', Array( 'time' => $this->data->user_registered ));
		}
		function formatBrokers() {
			$out = Array();
			foreach( $this->data->brokers as $broker ) {
				$out[] = CHtml::link( CHtml::encode( $broker->shortName ), $broker->getSingleURL() );
			}
			return implode( ", ", $out );
		}
		
		
		
		public function run()
		{
			$formatter=$this->getFormatter();
			if ($this->tagName!==null)
				echo CHtml::openTag($this->tagName,$this->htmlOptions);

			$i=0;
			$n=is_array($this->itemCssClass) ? count($this->itemCssClass) : 0;

			foreach($this->attributes as $attribute)
			{
				if(is_string($attribute))
				{
					if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$attribute,$matches))
						throw new CException(Yii::t('zii','The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
					$attribute=array(
						'name'=>$matches[1],
						'type'=>isset($matches[3]) ? $matches[3] : 'text',
					);
					if(isset($matches[5]))
						$attribute['label']=$matches[5];
				}

				if(isset($attribute['visible']) && !$attribute['visible'])
					continue;

				$tr=array('{label}'=>'', '{class}'=>$n ? $this->itemCssClass[$i%$n] : '');
				if(isset($attribute['cssClass']))
					$tr['{class}']=$attribute['cssClass'].' '.($n ? $tr['{class}'] : '');

				if(isset($attribute['label']))
					$tr['{label}']=$attribute['label'];
				elseif(isset($attribute['name']))
				{
					if($this->data instanceof CModel)
						$tr['{label}']=$this->data->getAttributeLabel($attribute['name']);
					else
						$tr['{label}']=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute['name'])))));
				}
				
				if(isset($attribute['valueCssClass']))
					$tr['{valueCssClass}']=$attribute['valueCssClass'];
				else{
					$tr['{valueCssClass}']='';
				}

				if(!isset($attribute['type']))
					$attribute['type']='text';
				if(isset($attribute['value']))
					$value=is_object($attribute['value']) && get_class($attribute['value']) === 'Closure' ? call_user_func($attribute['value'],$this->data) : $attribute['value'];
				elseif(isset($attribute['name']))
					$value=CHtml::value($this->data,$attribute['name']);
				else
					$value=null;

				$tr['{value}']=$value===null ? $this->nullDisplay : $formatter->format($value,$attribute['type']);

				$this->renderItem($attribute, $tr);

				$i++;
			}

			if ($this->tagName!==null)
				echo CHtml::closeTag($this->tagName);
		}
	}

?>