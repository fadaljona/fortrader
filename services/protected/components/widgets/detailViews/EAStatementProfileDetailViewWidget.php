<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class EAStatementProfileDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wEAStatementProfileDetailView';
		public $class = 'iDetailView i01 profile-user-info profile-user-info-striped';
		public $tagName = 'div';
		public $NSi18n = 'components/widgets/eaStatementProfileDetailView';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfile/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Added', 'type' => 'raw', 'value' => $this->formatAdded() ),
				Array( 'label' => 'Symbol', 'name' => 'symbol', 'visible' => strlen( $this->data->symbol )),
				Array( 'label' => 'Period', 'name' => 'period' ),
				Array( 'label' => 'Optimization', 'value' => $this->formatOptimization(), 'visible' => (bool)$this->data->optimizationStartDate ),
				Array( 'label' => 'Test', 'value' => $this->formatTest(), 'visible' => (bool)$this->data->testStartDate ),
				Array( 'label' => 'Broker', 'type' => 'raw', 'value' => $this->formatBroker(), 'visible' => (bool)$this->data->broker ),
				Array( 'label' => 'Server', 'type' => 'raw', 'value' => $this->formatServer(), 'visible' => (bool)$this->data->server ),
				Array( 'label' => 'Gain', 'type' => 'raw', 'value' => $this->formatGain() ),
				Array( 'label' => 'Drow', 'type' => 'raw', 'value' => $this->formatDrow() ),
				Array( 'label' => 'Trades', 'value' =>  CommonLib::numberFormat( $this->data->trades ) ),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatDate( $dateTime ) {
			$time = strtotime( $dateTime );
			$day = date( "d", $time );
			$month = date( "F", $time );
			$month = strtolower( $month );
			$date = Yii::t( 'components/widgets/eaStatementProfileDetailView', "{d} {$month}", Array( "{d}" => $day ));
			$yearDate = date( "Y", $time );
			$yearNow = date( "Y" );
			//if( $yearDate != $yearNow ) 
			$date .= " {$yearDate}";
			return $date;
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatAdded() {
			return CHtml::link( CHtml::encode( $this->data->user->user_login ), $this->data->user->getProfileURL() );
		}
		function formatOptimization() {
			return $this->formatDate( $this->data->optimizationStartDate )." - ".$this->formatDate( $this->data->optimizationEndDate );
		}
		function formatTest() {
			return $this->formatDate( $this->data->testStartDate )." - ".$this->formatDate( $this->data->testEndDate );
		}
		function formatBroker() {
			return $this->data->broker ? CHtml::link( CHtml::encode( $this->data->broker->officialName ), $this->data->broker->getSingleURL()) : null;
		}
		function formatServer() {
			return $this->data->server ? CHtml::link( CHtml::encode( $this->data->server->name )) : null;
		}
		function formatGain() {
			return $this->formatPercent( $this->data->gain );
		}
		function formatDrow() {
			return $this->formatPercent( $this->data->drow );
		}
	}

?>