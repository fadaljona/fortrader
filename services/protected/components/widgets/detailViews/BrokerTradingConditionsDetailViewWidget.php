<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class BrokerTradingConditionsDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wBrokerTradingConditionsDetailView';
		public $tagName = null;
		public $NSi18n = 'components/widgets/brokerTradingConditionsDetailView';
		function init() {
			$cs=Yii::app()->getClientScript();
			$formHelpersBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.assets-static.formHelpers') );
			$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers-countries.flags.css');
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/brokerProfile/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$brokerControl = Yii::App()->user->checkAccess( 'brokerControl' );
			
			$attributes = Array(
				Array( 
					'label' => 'Edit', 
					'visible' => $brokerControl, 
					'type' => 'raw', 
					'value' => CHtml::link( $this->t( 'Link' ), $this->data->getAdminURL() ) 
				),
				Array( 
					'label' => 'Title', 
					'value' => $this->formatTitle( $this->data ),
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Country', 
					'visible' => $this->data->country, 
					'value' => $this->formatCountry( $this->data->country ),
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Since', 
					'visible' => $this->data->year, 
					'name' => 'year' 
				),
				Array( 
					'label' => 'Regulator', 
					'type' => 'raw', 
					'visible' => $this->data->regulators, 
					'value' => $this->formatRegulator() 
				),
				Array( 
					'label' => 'Place in rating', 
					'visible' => $this->data->stats->place !== null, 
					'value' => $this->formatPlace(),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Changes', 
					'visible' => $this->data->stats->place != $this->data->stats->oldPlace, 
					'value' => $this->formatChanges(),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Platforms', 
					'visible' => $this->data->tradePlatforms, 
					'value' => $this->formatPlatforms(),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Binary Types', 
					'visible' => $this->data->type == 'binary' && $this->data->binarytypes, 
					'value' => $this->formatBinaryTypes(),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Binary Assets', 
					'visible' => $this->data->type == 'binary' && $this->data->binaryassets, 
					'value' => $this->formatBinaryAssets(),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Early closure', 
					'visible' => $this->data->type == 'binary', 
					'value' => $this->data->earlyClosure ? Yii::t( '*', 'Yes' ) : Yii::t( '*', 'No' ),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Trrade on weekends', 
					'visible' => $this->data->type == 'binary', 
					'value' => $this->data->tradeOnWeekends ? Yii::t( '*', 'Yes' ) : Yii::t( '*', 'No' ),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Binnary demo account', 
					'visible' => $this->data->type == 'binary', 
					'value' => $this->data->binnaryDemoAccount ? Yii::t( '*', 'Yes' ) : Yii::t( '*', 'No' ),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Max profit', 
					'visible' => $this->data->type == 'binary' && $this->data->maxProfit, 
					'name' => 'maxProfit',
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Input/Output', 
					'visible' => $this->data->IOs, 
					'value' => $this->formatIOs(),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Instruments', 
					'visible' => $this->data->instruments, 
					'value' => $this->formatInstruments(),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Investing', 
					'visible' => $this->data->AM, 
					'name' => 'AM',
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Demo account', 
					'visible' => strlen( $this->data->demoAccount ), 
					'name' => 'demoAccount',
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Updated', 
					'name' => 'trueUpdatedDate',
					'labelCssClass' => 'inside_col_blue',
				),

			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatTitle( $data ){
			$name = $data->officialName ? "{$data->brandName} ({$data->officialName})" : $data->brandName;
			return CHtml::tag('span', array( 'itemprop' => 'legalName name' ), $name);
		}
		function formatCountry( $country ){
			return $country->icon . Yii::t( '*', $country->name );
		}
		function formatMinDeposit() {
			return CommonLib::numberFormat( $this->data->minDeposit )."$";
		}
		function formatPlatforms() {
			return implode( ', ', CommonLib::slice( $this->data->tradePlatforms, 'name' ));
		}
		function formatIOs() {
			return implode( ', ', CommonLib::slice( $this->data->IOs, 'name' ));
		}
		function formatInstruments() {
			return implode( ', ', CommonLib::slice( $this->data->instruments, 'name' ));
		}
		function formatBinaryTypes() {
			return implode( ', ', CommonLib::slice( $this->data->binarytypes, 'name' ));
		}
		function formatBinaryAssets() {
			return implode( ', ', CommonLib::slice( $this->data->binaryassets, 'name' ));
		}
		function formatRegulator() {
			$out = Array();
			foreach( $this->data->regulators as $regulator ) {
				$out[] = strlen( $regulator->link ) ? CHtml::link( $regulator->name, CommonLib::getURL( $regulator->link ), Array( 'target' => '_blank' )) : $regulator->name;
			}
			return implode( ', ', $out );
		}
		function formatPlace() {
			if( ! $this->data->stats->place )
				return Yii::t( '*', 'Advertising' );
			else
				return Yii::t( '*', '{a} from {b}', Array( '{a}' => $this->data->stats->place, '{b}' => BrokerModel::getMaxPlace()));
		}
		function formatChanges() {
			return $this->renderPart( dirname( __FILE__ ).'/brokerProfile/changes.php' );
		}
		public function run(){
			$formatter=$this->getFormatter();
			if ($this->tagName!==null)
				echo CHtml::openTag($this->tagName,$this->htmlOptions);

			$i=0;
			$n=is_array($this->itemCssClass) ? count($this->itemCssClass) : 0;

			foreach($this->attributes as $attribute)
			{
				if(is_string($attribute))
				{
					if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$attribute,$matches))
						throw new CException(Yii::t('zii','The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
					$attribute=array(
						'name'=>$matches[1],
						'type'=>isset($matches[3]) ? $matches[3] : 'text',
					);
					if(isset($matches[5]))
						$attribute['label']=$matches[5];
				}

				if(isset($attribute['visible']) && !$attribute['visible'])
					continue;

				$tr=array('{label}'=>'', '{class}'=>$n ? $this->itemCssClass[$i%$n] : '');
				if(isset($attribute['cssClass']))
					$tr['{class}']=$attribute['cssClass'].' '.($n ? $tr['{class}'] : '');

				if(isset($attribute['label']))
					$tr['{label}']=$attribute['label'];
				elseif(isset($attribute['name']))
				{
					if($this->data instanceof CModel)
						$tr['{label}']=$this->data->getAttributeLabel($attribute['name']);
					else
						$tr['{label}']=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute['name'])))));
				}
				
				if(isset($attribute['labelCssClass']))
					$tr['{labelCssClass}']=$attribute['labelCssClass'];
				else{
					$tr['{labelCssClass}']='';
				}

				if(!isset($attribute['type']))
					$attribute['type']='text';
				if(isset($attribute['value']))
					$value=$attribute['value'];
				elseif(isset($attribute['name']))
					$value=CHtml::value($this->data,$attribute['name']);
				else
					$value=null;

				$tr['{value}']=$value===null ? $this->nullDisplay : $formatter->format($value,$attribute['type']);

				$this->renderItem($attribute, $tr);

				$i++;
			}

			if ($this->tagName!==null)
				echo CHtml::closeTag($this->tagName);
		}
	}

?>