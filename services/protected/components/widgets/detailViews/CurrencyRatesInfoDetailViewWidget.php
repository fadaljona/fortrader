<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class CurrencyRatesInfoDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wBrokerTradingConditionsDetailView';
		public $tagName = null;
		public $NSi18n = 'components/widgets/currencyRatesInfoDetailView';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = '<tr class="{class}"><td class="inside_col_1">{label}</td><td class="inside_col_2">{value}</td></tr>';
			parent::init();
		}
		protected function getAttributes() {
			$brokerControl = Yii::App()->user->checkAccess( 'brokerControl' );
			
			$attributes = Array(
				Array( 
					'label' => 'Letter code', 
					'visible' => true, 
					'type' => 'raw', 
					'value' => CHtml::tag('strong', array(), $this->data->code)
				),
				Array( 
					'label' => 'Digital code', 
					'visible' => $this->data->dataType == 'cbr', 
					'type' => 'raw', 
					'value' => CHtml::tag('strong', array(), $this->data->srtDigitalCode)
				),
				Array( 
					'label' => 'CurrencySymbol', 
					'visible' => $this->data->symbol, 
					'type' => 'raw', 
					'value' => CHtml::tag('strong', array(),  $this->data->symbol)
				),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
	}
?>