<?=CHtml::link( CHtml::encode( basename( $this->data->nameFile )), $this->data->getDownloadURL() )?>
&nbsp;&nbsp;(<?=CommonLib::filesizeFormat( @filesize( $this->data->pathFile ))?>)
&nbsp;&nbsp;<?=CHtml::link( Yii::t( '*', 'view' ), $this->data->getViewURL(), Array( 'target' => "_blank" ))?>