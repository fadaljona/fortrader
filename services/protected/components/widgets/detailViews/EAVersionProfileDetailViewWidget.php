<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class EAVersionProfileDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wEAVersionProfileDetailView';
		public $class = 'iDetailView i01 profile-user-info profile-user-info-striped';
		public $tagName = 'div';
		public $NSi18n = 'components/widgets/eaVersionProfileDetailView';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfile/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Name', 'value' => $this->formatName() ),
				Array( 'label' => 'Added', 'type' => 'raw', 'value' => $this->formatAdded() ),
				Array( 'label' => 'Released', 'value' => $this->formatReleased() ),
				Array( 'label' => 'Files', 'type' => 'raw', 'value' => $this->formatFiles(), 'visible' => strlen( $this->data->nameFile ) ),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatName() {
			$version = Yii::t( '*', 'Version' );
			return "{$this->data->EA->name}. {$version} {$this->data->version}";
		}
		function formatAdded() {
			return CHtml::link( CHtml::encode( $this->data->user->user_login ), $this->data->user->getProfileURL() );
		}
		function formatReleased() {
			return date( "d/m/Y", strtotime( $this->data->release ));
		}
		function formatFiles() {
			return $this->renderPart( dirname( __FILE__ ).'/eAVersionProfile/files.php' );
		}
	}

?>