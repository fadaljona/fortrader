<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class JournalProfileDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wJournalProfileDetailView';
		public $class = 'iDetailView i01 profile-user-info profile-user-info-striped';
		public $tagName = 'div';
		public $NSi18n = 'components/widgets/journalProfileDetailView';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfile/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Number and release date', 'value' => $this->formatNumber() ),
				Array( 'label' => 'Count pages', 'name' => 'countPages' ),
				//Array( 'label' => 'Size', 'value' => $this->formatSize() ),
				//Array( 'label' => 'Released', 'value' => $this->formatReleased() ),
				Array( 'label' => 'Digest', 'type' => 'raw', 'value' => $this->formatDigest(), 'visible' => strlen( $this->data->digest ) ),
				//Array( 'label' => 'Download', 'type' => 'raw', 'value' => $this->formatLinks() ),
				//Array( 'label' => 'Type document', 'type' => 'raw', 'value' => $this->formatTypeDocument() ),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatNumber() {
			$out = "№{$this->data->number} {$this->data->date}";
			return $out;
		}
		/*
		function formatSize() {
			$size = @filesize( $this->data->pathJournal );
			return CommonLib::filesizeFormat( $size );
		}
		function formatReleased() {
			return date( "d/m/Y", strtotime( $this->data->date ));
		}
		*/
		function formatDigest() {
			$view = Yii::t( '*', 'View' );
			return CHtml::link( $view, CommonLib::getURL( $this->data->digest ));
		}
		/*
		function formatLinks() {
			return $this->renderPart( dirname( __FILE__ ).'/journalProfile/links.php' );
		}
		*/
		function formatTypeDocument() {
			return $this->renderPart( dirname( __FILE__ ).'/journalProfile/typeDocument.php' );
		}
	}

?>
