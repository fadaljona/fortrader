<?
	Yii::import( 'controllers.base.ActionBase' );
	Yii::import( 'bootstrap.widgets.TbDetailView' );
	
	abstract class DetailViewWidgetBase extends TbDetailView {
		public $w;
		public $class;
		public $ins;
		public $NSi18n = '*';
		
		abstract protected function getAttributes();
		
		function init() {
			$this->attributes = $this->getAttributes();
			parent::init();
		}
		function t( $text ) {
			return Yii::t( $this->NSi18n, $text );
		}
		function renderPart( $pathFile ) {
			ob_start();
			require $pathFile;
			return ob_get_clean();
		}
		function renderWidget( $widget, $params ) {
			ob_start();
			$this->controller->widget( $widget, $params );
			return ob_get_clean();
		}
	}

?>