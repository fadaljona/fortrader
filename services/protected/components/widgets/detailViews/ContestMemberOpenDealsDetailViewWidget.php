<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class ContestMemberOpenDealsDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wContestMemberOpenDealsDetailView';
		public $tagName = 'ul';
		public $NSi18n = 'components/widgets/ContestMemberOpenDealsDetailView';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w} conditions_list_green clearfix",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/contestMemberOpenDeals/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			
			$attributes = Array(
			
				Array( 
					'label' => 'Pending Orders', 
					'value' => $this->data->ACCOUNT_ORDERS_PENDING_TOTAL,
					'cssClass' => 'border_white'
				),
				Array( 
					'label' => 'Open Orders', 
					'value' => $this->data->ACCOUNT_ORDERS_TOTAL
				),
				Array( 
					'label' => 'Total Lot Open Orders', 
					'value' => number_format($this->data->ACCOUNT_ORDERS_LOT_SUM, 2, '.', ' '),
					'cssClass' => 'border_white'
				),
				Array( 
					'label' => 'Average Lot Open Orders', 
					'value' => number_format($this->data->ACCOUNT_ORDERS_LOT_AVERAGE, 2, '.', ' '),
				),
			

			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
	}
?>