<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class ContestMemberInfoDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wContestMemberInfoDetailView';
		public $data;

		public $NSi18n = 'components/widgets/ContestMemberInfoDetailViewWidget';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w} inside_table",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/contestMemberInfo/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$canControl = Yii::App()->user->checkAccess( 'contestMemberControl' );
			
			$attributes = Array(
				Array( 
					'label' => 'Account number', 
					'value' => $this->data->accountNumber,
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Broker', 
					'value' => $this->formatBroker( $this->data->contest->brokers ),
					'type' => 'raw', 
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Place', 
					'visible' => $this->data->stats->place, 
					'value' => CommonLib::numberFormat( $this->data->stats->place ),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Balance', 
					'visible' => $this->data->stats->balance !== null, 
					'value' =>  '$ '.CommonLib::numberFormat( $this->data->stats->balance ),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Gain', 
					'visible' => $this->data->stats->gain !== null, 
					'value' =>  $this->formatPercent( $this->data->stats->gain ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Drow', 
					'visible' => $this->data->stats->drowMax !== null, 
					'value' =>  $this->formatPercent( $this->data->stats->drowMax ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Tradese', 
					'value' =>  CommonLib::numberFormat( $this->data->stats->countTrades ),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Open', 
					'value' =>  CommonLib::numberFormat( $this->data->stats->countOpen ),
					'labelCssClass' => 'inside_col_blue',
				),
				Array( 
					'label' => 'Status', 
					'value' =>  $this->data->getLabelStatus() ,
					'labelCssClass' => 'inside_col_yellow',
					'valueCssClass' => 'bg_light_yellow',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Updated', 
					'value' =>  $this->formatUpdate( $this->data ) ,
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Equity', 
					'visible' => $this->data->stats->equity !== null, 
					'value' =>  CommonLib::numberFormat( $this->data->stats->equity ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Profit', 
					'visible' => $this->data->stats->profit !== null, 
					'value' =>  '$ '.CommonLib::numberFormat( $this->data->stats->profit ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Deposits', 
					'visible' => $this->data->stats->deposit !== null, 
					'value' => '$ '.CommonLib::numberFormat( $this->data->stats->deposit ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Withdrawals', 
					'visible' => $this->data->stats->withdrawals !== null, 
					'value' => '$ '.CommonLib::numberFormat( $this->data->stats->withdrawals ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Leverage', 
					'value' => $this->formatLeverage( $this->data->stats ) ,
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Avg. Trade Time', 
					'value' => $this->formatTradeTime( $this->data->stats->avgTradeTimeSec ) ,
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Best Trade', 
					'value' => !$this->data->stats->bestTrade ? Yii::t( '*', 'no data' ) : '$ '.number_format( $this->data->stats->bestTrade, 2, '.', ' ' ) ,
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Worst Trade', 
					'value' => !$this->data->stats->worstTrade ? Yii::t( '*', 'no data' ) : '$ '.number_format( $this->data->stats->worstTrade, 2, '.', ' ' ) ,
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Profitable Trades', 
					'value' => $this->formatProfitableTrades( $this->data->stats ) ,
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Edit', 
					'visible' => $canControl || Yii::app()->user->id == $this->data->id, 
					'value' => '<i aria-hidden="true" class="fa fa-times"></i>',
					'labelCssClass' => 'inside_col_yellow',
					'valueCssClass' => 'bg_light_yellow',
					'type' => 'raw',
				),

			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatProfitableTrades( $stats ) {
			if( !$stats->profitableTrades || !$stats->allTrades ) return Yii::t( '*', 'no data' );
			return number_format( ( $stats->profitableTrades/$stats->allTrades ) * 100, 2, '.', ' ' ).'%';
		}
		function formatTradeTime( $time ) {
			if( !$time ) return Yii::t( '*', 'no data' );
			$d = gmdate("j", $time);
			$h = gmdate("G", $time);
			$m = gmdate("i", $time);
			$outDate = '';
			if( $d-1 ) $outDate .= ($d-1).Yii::t( '*', 'IntervalDays' ).' ';
			if( $h ) $outDate .= $h.Yii::t( '*', 'IntervalHours' ).' ';
			if( $m ) $outDate .= $m.Yii::t( '*', 'IntervalMinutes' ).' ';
			return $outDate;
		}
		function formatLeverage( $stats ) {
			if( $stats->leverage == 0 ) return Yii::t( '*', 'no data' );
			if( $stats->leverageCganged ){
				ob_start();
				$this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $stats->leverageCgangedDate, 'class' => "" ));
				$changeDate = ob_get_clean();
				return '<div class="better_news_par">1:'.$stats->leverage.
					'<div class="tbl_tt"><b>Дата изменения:</b><br />'.$changeDate.'
					<br /><b>Плечо:</b> '.$stats->leverage.'</div></div>';
			}else{
				return '1:'.$stats->leverage;
			}
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'green_color' : 'red_color' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatBroker( $brokers ){
			$outStr = '';
			$i=0;
			foreach( $brokers as $broker ){
				if( $i ) $outStr .= ', ';
				$outStr .= CHtml::link( CHtml::encode( $broker->officialName ), $broker->singleURL, array( 'class' => 'blue_color' ) );
				$i++;
			}
			return $outStr;
		}
		function formatUpdate( $data ) {
			if( $data->stats->trueDT ){
				ob_start();
				$this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->stats->trueDT, 'class' => "iBold" ));
				$content = ob_get_clean();
				return $content;
			} 
			return '';
		}
		
		public function renderTable( $from, $to ){
			if ($this->tagName!==null)
				echo CHtml::openTag($this->tagName, array( 'class' => $this->htmlOptions['class'] ));
			$formatter=$this->getFormatter();
			$i=0;
			$n=is_array($this->itemCssClass) ? count($this->itemCssClass) : 0;
			if( $to > count( $this->attributes ) ) $to = count( $this->attributes );
			for( $j = $from; $j < $to; $j++ ){
				$attribute = $this->attributes[$j];
				if(is_string($attribute))
				{
					if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$attribute,$matches))
						throw new CException(Yii::t('zii','The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
					$attribute=array(
						'name'=>$matches[1],
						'type'=>isset($matches[3]) ? $matches[3] : 'text',
					);
					if(isset($matches[5]))
						$attribute['label']=$matches[5];
				}

				if(isset($attribute['visible']) && !$attribute['visible'])
					continue;

				$tr=array('{label}'=>'', '{class}'=>$n ? $this->itemCssClass[$i%$n] : '');
				if(isset($attribute['cssClass']))
					$tr['{class}']=$attribute['cssClass'].' '.($n ? $tr['{class}'] : '');

				if(isset($attribute['label']))
					$tr['{label}']=$attribute['label'];
				elseif(isset($attribute['name']))
				{
					if($this->data instanceof CModel)
						$tr['{label}']=$this->data->getAttributeLabel($attribute['name']);
					else
						$tr['{label}']=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute['name'])))));
				}
				
				if(isset($attribute['labelCssClass']))
					$tr['{labelCssClass}']=$attribute['labelCssClass'];
				else{
					$tr['{labelCssClass}']='';
				}
				
				if(isset($attribute['valueCssClass']))
					$tr['{valueCssClass}']=$attribute['valueCssClass'];
				else{
					$tr['{valueCssClass}']='';
				}

				if(!isset($attribute['type']))
					$attribute['type']='text';
				if(isset($attribute['value']))
					$value=$attribute['value'];
				elseif(isset($attribute['name']))
					$value=CHtml::value($this->data,$attribute['name']);
				else
					$value=null;

				$tr['{value}']=$value===null ? $this->nullDisplay : $formatter->format($value,$attribute['type']);

				$this->renderItem($attribute, $tr);

				$i++;
			}
			if ($this->tagName!==null)
				echo CHtml::closeTag($this->tagName);
		}

		public function run(){
			
			$countRows = count( $this->attributes );
			$forFirstTable = ($countRows + $countRows % 2) / 2;
			
			$originClass = $this->htmlOptions['class'];
			
			$this->htmlOptions['class'] = $originClass . ' table1';
			$this->renderTable(0, $forFirstTable);
			
			$this->htmlOptions['class'] = $originClass . ' table2';
			$this->renderTable($forFirstTable, $countRows);
			
			$this->htmlOptions['class'] = $originClass;
		
		}
	}

?>