<div class="profile-card__item clearfix {class}">
	<div class="profile-card__key">{label}</div>
	<div class="profile-card__value {valueCssClass}">{value}</div>
</div>