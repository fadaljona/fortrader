<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class UserProfileInvestorLeftDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wUserProfileDetailView';
		public $class = 'iDetailView i02';
		public $tagName = 'div';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfileTrader/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Availability', 'value' => $this->data->profile->investor_availability, 'visible' => (bool)$this->data->profile->investor_availability ),
				Array( 'label' => 'Investment amount', 'value' => $this->data->profile->investor_investmentAmount, 'visible' => (bool)$this->data->profile->investor_investmentAmount ),
				Array( 'label' => 'Investment period', 'value' => $this->data->profile->investor_investmentPeriod, 'visible' => (bool)$this->data->profile->investor_investmentPeriod ),
				Array( 'label' => 'Expected annual rate of return', 'value' => $this->data->profile->investor_expectedAnnualRateOfReturn, 'visible' => (bool)$this->data->profile->investor_expectedAnnualRateOfReturn ),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = Yii::t( 'models/forms/userProfile', $attribute[ 'label' ]).':'; unset( $attribute );
			return $attributes;
		}
	}

?>