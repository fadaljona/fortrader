<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class UserProfileManagerDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wUserProfileDetailView';
		public $class = 'iDetailView i02 i03';
		public $tagName = 'div';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfile/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Availability', 'value' => $this->data->profile->manager_availability, 'visible' => (bool)$this->data->profile->manager_availability ),
				Array( 'label' => 'Trust management type', 'value' => $this->data->profile->manager_trustManagementType, 'visible' => (bool)$this->data->profile->manager_trustManagementType ),
				Array( 'label' => 'Agreement', 'value' => $this->data->profile->manager_agreement, 'visible' => (bool)$this->data->profile->manager_agreement ),
				Array( 'label' => 'Management experience', 'value' => $this->data->profile->manager_managementExperience, 'visible' => (bool)$this->data->profile->manager_managementExperience ),
				Array( 'label' => 'Investment risk', 'value' => $this->data->profile->manager_investmentRisk, 'visible' => (bool)$this->data->profile->manager_investmentRisk ),
				Array( 'label' => 'Minimum investment amount', 'value' => $this->data->profile->manager_minimumInvestmentAmount, 'visible' => (bool)$this->data->profile->manager_minimumInvestmentAmount ),
				Array( 'label' => 'Additionally', 'value' => $this->data->profile->manager_additionally, 'visible' => (bool)$this->data->profile->manager_additionally ),
				
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = Yii::t( 'models/forms/userProfile', $attribute[ 'label' ]).':'; unset( $attribute );
			return $attributes;
		}
	}

?>