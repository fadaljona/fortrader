<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class UserProfileProgrammerDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wUserProfileDetailView';
		public $class = 'iDetailView i02';
		public $tagName = 'div';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfile/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Availability', 'value' => $this->data->profile->programmer_availability, 'visible' => (bool)$this->data->profile->programmer_availability ),
				Array( 'label' => 'Programming experience', 'value' => $this->data->profile->programmer_programmingExperience, 'visible' => (bool)$this->data->profile->programmer_programmingExperience ),
				Array( 'label' => 'Programming langauge', 'value' => $this->data->profile->programmer_programmingLangauge, 'visible' => (bool)$this->data->profile->programmer_programmingLangauge ),
				Array( 'label' => 'Type of programs', 'value' => $this->data->profile->programmer_typeOfPrograms, 'visible' => (bool)$this->data->profile->programmer_typeOfPrograms ),
				Array( 'label' => 'Hourly rate', 'value' => $this->data->profile->programmer_hourlyRate, 'visible' => (bool)$this->data->profile->programmer_hourlyRate ),
				Array( 'label' => 'Minimal order', 'value' => $this->data->profile->programmer_minimalOrder, 'visible' => (bool)$this->data->profile->programmer_minimalOrder ),
				Array( 'label' => 'mql4 profile', 'value' => $this->data->profile->programmer_mql4Profile, 'visible' => (bool)$this->data->profile->programmer_mql4Profile ),
				Array( 'label' => 'mql5 profile', 'value' => $this->data->profile->programmer_mql5Profile, 'visible' => (bool)$this->data->profile->programmer_mql5Profile ),
				Array( 'label' => 'Additionally', 'value' => $this->data->profile->programmer_additionally, 'visible' => (bool)$this->data->profile->programmer_additionally ),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = Yii::t( 'models/forms/userProfile', $attribute[ 'label' ]).':'; unset( $attribute );
			return $attributes;
		}
	}

?>