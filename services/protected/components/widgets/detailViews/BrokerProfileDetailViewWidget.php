<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class BrokerProfileDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wBrokerProfileDetailView';
		public $class = 'iDetailView i01 profile-user-info';
		public $tagName = 'div';
		public $NSi18n = 'components/widgets/brokerProfileDetailView';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfile/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Title', 'name' => 'brandName' ),
				Array( 'label' => 'Since', 'visible' => $this->data->year, 'name' => 'year' ),
				Array( 'label' => 'Regulator', 'type' => 'raw', 'visible' => $this->data->regulators, 'value' => $this->formatRegulator() ),
				Array( 'label' => 'Place in rating', 'visible' => $this->data->stats->place !== null, 'value' => $this->formatPlace() ),
				Array( 'label' => 'Changes', 'visible' => $this->data->stats->place != $this->data->stats->oldPlace, 'value' => $this->formatChanges() ),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatLocation() {
			return $this->renderPart( dirname( __FILE__ ).'/userProfile/location.php' );
		}
		function formatJoined() {
			return date( "d/m/Y", strtotime( $this->data->user_registered ));
		}
		function formatLastOnline() {
			return $this->renderWidget( 'widgets.parts.TimeDiffWidget', Array( 'time' => $this->data->usedDT ));
		}
		function formatRegulator() {
			$out = Array();
			foreach( $this->data->regulators as $regulator ) {
				$out[] = strlen( $regulator->link ) ? CHtml::link( $regulator->name, CommonLib::getURL( $regulator->link ), Array( 'targer' => '_blank' )) : $regulator->name;
			}
			return implode( ', ', $out );
		}
		function formatPlace() {
			if( ! $this->data->stats->place )
				return Yii::t( '*', 'Advertising' );
			else
				return Yii::t( '*', '{a} from {b}', Array( '{a}' => $this->data->stats->place, '{b}' => BrokerModel::getMaxPlace()));
		}
		function formatChanges() {
			return $this->renderPart( dirname( __FILE__ ).'/brokerProfile/changes.php' );
		}
	}

?>
