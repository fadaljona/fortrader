<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class MonitoringAccountInfoDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wMonitoringAccountInfoDetailView';
		public $data;
		public $tagName = 'div';

		public $NSi18n = 'components/widgets/MonitoringAccountInfoDetailViewWidget';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/monitoringAccountInfo/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			
			$attributes = Array(
				Array( 
					'label' => 'Account number', 
					'value' => $this->data->accName,
					'valueCssClass' => 'green_color',
				),
				Array( 
					'label' => 'Account type', 
					'value' => $this->data->mt5AccountInfo->IsDemo == 0 ? Yii::t( '*', 'Real' ) : Yii::t( '*', 'Demo' ) ,
					'valueCssClass' => 'green_color',
				),
				Array( 
					'label' => 'Broker', 
					'value' => $this->formatBroker( $this->data->server->broker ),
					'type' => 'raw', 
					'visible' => !$this->data->server->forReports
				),
				Array( 
					'label' => 'Status', 
					'value' =>  $this->formatStatus( $this->data ) ,
					'valueCssClass' => 'green_color',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Updated', 
					'value' =>  $this->formatUpdate( $this->data ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Deposits', 
					'visible' => $this->data->stats->deposit !== null, 
					'value' => '$ '.CommonLib::numberFormat( $this->data->stats->deposit ),
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Leverage', 
					'value' => $this->formatLeverage( $this->data->stats ) ,
					'type' => 'raw', 
					'visible' => !$this->data->server->forReports
				),
				
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatStatus( $data ) {
			if( !$data->mt5AccountInfo ) return '<span class="label_red">' . Yii::t('*', 'Pending') . '</span>';
			if( !$data->stats->last_error_code )  return '<span class="label_green">' . Yii::t('*', 'Ok') . '</span>';
			if( $data->mt5AccountInfo->error->title ) return '<span class="label_red">' . $data->mt5AccountInfo->error->title . '</span>';
			return '<span class="label_red">' . Yii::t('*', 'Please contact the admin') . '</span>';
		}
		function formatBroker( $broker ){
			return CHtml::link( CHtml::encode( $broker->officialName ), $broker->singleURL, array(  ) );
		}
		function formatUpdate( $data ) {
			if( $data->stats->trueDT ){
				ob_start();
				$this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->stats->trueDT, 'class' => "iBold" ));
				$content = ob_get_clean();
				return $content;
			} 
			return '';
		}
		function formatLeverage( $stats ) {
			if( $stats->leverage == 0 ) return Yii::t( '*', 'no data' );
			if( $stats->leverageCganged ){
				ob_start();
				$this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $stats->leverageCgangedDate, 'class' => "" ));
				$changeDate = ob_get_clean();
				return '<div class="better_news_par">1:'.$stats->leverage.
					'<div class="tbl_tt"><b>Дата изменения:</b><br />'.$changeDate.'</div></div>';
			}else{
				return '1:'.$stats->leverage;
			}
		}
		
		public function run(){
			$formatter=$this->getFormatter();
			if ($this->tagName!==null)
				echo CHtml::openTag($this->tagName,$this->htmlOptions);

			$i=0;
			$n=is_array($this->itemCssClass) ? count($this->itemCssClass) : 0;

			foreach($this->attributes as $attribute)
			{
				if(is_string($attribute))
				{
					if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$attribute,$matches))
						throw new CException(Yii::t('zii','The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
					$attribute=array(
						'name'=>$matches[1],
						'type'=>isset($matches[3]) ? $matches[3] : 'text',
					);
					if(isset($matches[5]))
						$attribute['label']=$matches[5];
				}

				if(isset($attribute['visible']) && !$attribute['visible'])
					continue;

				$tr=array('{label}'=>'', '{class}'=>$n ? $this->itemCssClass[$i%$n] : '');
				if(isset($attribute['cssClass']))
					$tr['{class}']=$attribute['cssClass'].' '.($n ? $tr['{class}'] : '');

				if(isset($attribute['label']))
					$tr['{label}']=$attribute['label'];
				elseif(isset($attribute['name']))
				{
					if($this->data instanceof CModel)
						$tr['{label}']=$this->data->getAttributeLabel($attribute['name']);
					else
						$tr['{label}']=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute['name'])))));
				}
				
				if(isset($attribute['labelCssClass']))
					$tr['{labelCssClass}']=$attribute['labelCssClass'];
				else{
					$tr['{labelCssClass}']='';
				}
				
				if(isset($attribute['valueCssClass']))
					$tr['{valueCssClass}']=$attribute['valueCssClass'];
				else{
					$tr['{valueCssClass}']='';
				}

				if(!isset($attribute['type']))
					$attribute['type']='text';
				if(isset($attribute['value']))
					$value=is_object($attribute['value']) && get_class($attribute['value']) === 'Closure' ? call_user_func($attribute['value'],$this->data) : $attribute['value'];
				elseif(isset($attribute['name']))
					$value=CHtml::value($this->data,$attribute['name']);
				else
					$value=null;

				$tr['{value}']=$value===null ? $this->nullDisplay : $formatter->format($value,$attribute['type']);

				$this->renderItem($attribute, $tr);

				$i++;
			}

			if ($this->tagName!==null)
				echo CHtml::closeTag($this->tagName);
		}

	}

?>