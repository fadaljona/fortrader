<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class UserProfileTraderRightDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wUserProfileDetailView';
		public $class = 'iDetailView i02';
		public $tagName = 'div';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfileTrader/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Analyse method', 'value' => $this->data->profile->trader_analyseMethod, 'visible' => (bool)$this->data->profile->trader_analyseMethod ),
				Array( 'label' => 'Trading type', 'value' => $this->data->profile->trader_tradingType, 'visible' => (bool)$this->data->profile->trader_tradingType ),
				Array( 'label' => 'Use EA', 'value' => $this->data->profile->trader_useEA, 'visible' => (bool)$this->data->profile->trader_useEA ),
				Array( 'label' => 'Commercial product', 'value' => $this->data->profile->trader_commercialProduct, 'visible' => (bool)$this->data->profile->trader_commercialProduct ),
				Array( 'label' => 'About your trading', 'value' => $this->data->profile->trader_aboutTrading, 'visible' => (bool)$this->data->profile->trader_aboutTrading ),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = Yii::t( 'models/forms/userProfile', $attribute[ 'label' ]).':'; unset( $attribute );
			return $attributes;
		}
	}

?>