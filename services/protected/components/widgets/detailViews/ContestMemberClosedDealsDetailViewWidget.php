<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class ContestMemberClosedDealsDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wContestMemberClosedDealsDetailView';
		public $data;

		public $NSi18n = 'components/widgets/ContestMemberClosedDealsDetailViewWidget';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w} inside_table",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/contestMemberClosedDeals/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {

			
			$attributes = Array(
				Array( 
					'label' => 'Total Trades', 
					'value' => $this->data->ACCOUNT_ORDERS_HISTORY_TOTAL,
				),
				Array( 
					'label' => 'Canceled Orders', 
					'value' => $this->data->ACCOUNT_ORDERS_HISTORY_PENDING_TOTAL,
				),
				Array( 
					'label' => 'Average Order Lifetime', 
					'value' => $this->formatTradeTime( round($this->data->accountOrdersHistoryTimeAverage) ),
					'type' => 'raw', 
					'visible' => $this->data->accountOrdersHistoryTimeAverage != ''
				),
				Array( 
					'label' => 'Best Trade', 
					'value' => $this->formatSingMoney( $this->data->ACCOUNT_ORDERS_HISTORY_BEST ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Worse Trade', 
					'value' => $this->formatSingMoney( $this->data->ACCOUNT_ORDERS_HISTORY_WORSE ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Average Profit', 
					'value' => $this->formatSingMoney( $this->data->ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Average Worse', 
					'value' => $this->formatSingMoney( $this->data->ACCOUNT_ORDERS_HISTORY_LOSS_AVERAGE ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Best Trade In Pips', 
					'value' => $this->formatPips( $this->data->ACCOUNT_ORDERS_HISTORY_BEST_PIPS ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Worse Trade in Pips', 
					'value' => $this->formatPips( $this->data->ACCOUNT_ORDERS_HISTORY_WORSE_PIPS ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Profit Trades (% of total)', 
					'value' => CommonLib::numberFormat($this->data->ACCOUNT_ORDERS_HISTORY_PROFIT_PERCENT)."%",
				),
				Array( 
					'label' => 'Profit Factor', 
					'value' => number_format($this->data->ACCOUNT_ORDERS_HISTORY_PROFIT_FACTOR, 2, ".", " "),
				),
				Array( 
					'label' => 'Maximal Lot', 
					'value' => number_format($this->data->ACCOUNT_ORDERS_HISTORY_MAXLOT, 2, ".", " "),
				),
				Array( 
					'label' => 'Minimal Lot', 
					'value' => number_format($this->data->ACCOUNT_ORDERS_HISTORY_MINLOT, 2, ".", " "),
				),
				Array( 
					'label' => 'Average Lot', 
					'value' => number_format($this->data->ACCOUNT_ORDERS_HISTORY_LOT_AVERAGE, 2, ".", " "),
				),
				Array( 
					'label' => 'Total Lot', 
					'value' => number_format($this->data->ACCOUNT_ORDERS_HISTORY_LOT_SUM, 2, ".", " "),
				),
				Array( 
					'label' => 'Orders Average Per Day', 
					'value' => $this->data->ACCOUNT_ORDERS_HISTORY_DAY,
				),
				Array( 
					'label' => 'Total Profit Trades', 
					'value' => $this->data->ACCOUNT_ORDERS_HISTORY_PROFIT_TOTAL,
				),
				Array( 
					'label' => 'Total Loss Trades', 
					'value' => $this->data->ACCOUNT_ORDERS_HISTORY_LOSS_TOTAL,
				),
				Array( 
					'label' => 'Gross Loss', 
					'value' => $this->formatSummUsd( $this->data->ACCOUNT_ORDERS_HISTORY_GROSS_LOSS ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Gross Profit', 
					'value' => $this->formatSummUsd( $this->data->ACCOUNT_ORDERS_HISTORY_GROSS_PROFIT ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Total Net Profit', 
					'value' => $this->formatSummUsd( $this->data->ACCOUNT_ORDERS_HISTORY_PROFIT ) ,
					'type' => 'raw', 
				),

			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatSummUsd( $usd ) {
			if( $usd ){
				return '$'.number_format( abs($usd), 0, ".", " " );
			}else return Yii::t( '*', 'no data' );
		}
		function formatTradeTime( $seconds ) {
			
			$days = floor($seconds / (3600*24));
			$hours = floor($seconds / 3600);
			$mins = floor($seconds / 60 % 60);
			$secs = floor($seconds % 60);
			
			$outStr = '';
			
			if( $days )  $outStr .= ' '. Yii::t('*', '{n} day|{n} days', $days);
			if( $hours )  $outStr .= ' '. Yii::t('*', '{n} hour|{n} hours', $hours);
			if( $mins )  $outStr .= ' '. Yii::t('*', '{n} minute|{n} minutes', $mins);
			$outStr .= ' '. Yii::t('*', '{n} second|{n} seconds', $secs);
			
			return $outStr;
		}
		function formatSingMoney( $money ) {
			if( $money ){
				if( $money > 0 ){
					return '+'.CommonLib::numberFormat( $money ).' $';
				}
				if( $money < 0 ){
					return CommonLib::numberFormat( $money ).' $';
				}
			}else return Yii::t( '*', 'no data' );
		}
		function formatPips( $pips ) {
			if( $pips ){
				if( $pips > 0 ){
					return '+'.number_format( $pips, 4, ".", " " );
				}else{
					return number_format( $pips, 4, ".", " " );
				}
			}else return Yii::t( '*', 'no data' );
		}
		
		public function renderTable( $from, $to ){
			if ($this->tagName!==null)
				echo CHtml::openTag($this->tagName, array( 'class' => $this->htmlOptions['class'] ));
			$formatter=$this->getFormatter();
			$i=0;
			$n=is_array($this->itemCssClass) ? count($this->itemCssClass) : 0;
			if( $to > count( $this->attributes ) ) $to = count( $this->attributes );
			for( $j = $from; $j < $to; $j++ ){
				$attribute = $this->attributes[$j];
				if(is_string($attribute))
				{
					if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$attribute,$matches))
						throw new CException(Yii::t('zii','The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
					$attribute=array(
						'name'=>$matches[1],
						'type'=>isset($matches[3]) ? $matches[3] : 'text',
					);
					if(isset($matches[5]))
						$attribute['label']=$matches[5];
				}

				if(isset($attribute['visible']) && !$attribute['visible'])
					continue;

				$tr=array('{label}'=>'', '{class}'=>$n ? $this->itemCssClass[$i%$n] : '');
				if(isset($attribute['cssClass']))
					$tr['{class}']=$attribute['cssClass'].' '.($n ? $tr['{class}'] : '');

				if(isset($attribute['label']))
					$tr['{label}']=$attribute['label'];
				elseif(isset($attribute['name']))
				{
					if($this->data instanceof CModel)
						$tr['{label}']=$this->data->getAttributeLabel($attribute['name']);
					else
						$tr['{label}']=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute['name'])))));
				}
				
				if(isset($attribute['labelCssClass']))
					$tr['{labelCssClass}']=$attribute['labelCssClass'];
				else{
					$tr['{labelCssClass}']='';
				}
				
				if(isset($attribute['valueCssClass']))
					$tr['{valueCssClass}']=$attribute['valueCssClass'];
				else{
					$tr['{valueCssClass}']='';
				}

				if(!isset($attribute['type']))
					$attribute['type']='text';
				if(isset($attribute['value']))
					$value=$attribute['value'];
				elseif(isset($attribute['name']))
					$value=CHtml::value($this->data,$attribute['name']);
				else
					$value=null;

				$tr['{value}']=$value===null ? $this->nullDisplay : $formatter->format($value,$attribute['type']);

				$this->renderItem($attribute, $tr);

				$i++;
			}
			if ($this->tagName!==null)
				echo CHtml::closeTag($this->tagName);
		}

		public function run(){
			
			$countRows = count( $this->attributes );
			$forFirstTable = ($countRows + $countRows % 2) / 2;
			
			$originClass = $this->htmlOptions['class'];
			
			$this->htmlOptions['class'] = $originClass . ' table1';
			$this->renderTable(0, $forFirstTable);
			
			$this->htmlOptions['class'] = $originClass . ' table2';
			$this->renderTable($forFirstTable, $countRows);
			
			$this->htmlOptions['class'] = $originClass;
		
		}
	}

?>