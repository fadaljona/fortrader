<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class MonitoringAccountIndicatorsDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wMonitoringAccountInfoDetailView';
		public $data;
		public $tagName = 'div';

		public $NSi18n = 'components/widgets/MonitoringAccountIndicatorsDetailViewWidget';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/monitoringAccountInfo/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			
			$attributes = Array(
				Array( 
					'label' => 'Balance', 
					'visible' => $this->data->stats->balance !== null, 
					'value' =>  '$ '.CommonLib::numberFormat( $this->data->stats->balance ),
				),
				Array( 
					'label' => 'Gain', 
					'visible' => $this->data->stats->gain !== null, 
					'value' =>  $this->formatPercent( $this->data->stats->gain ),
					'valueCssClass' => 'green_color',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Drow', 
					'visible' => $this->data->stats->drowMax !== null, 
					'value' =>  $this->formatPercent( $this->data->stats->drowMax ),
					'valueCssClass' => 'green_color',
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Tradese', 
					'value' =>  CommonLib::numberFormat( $this->data->stats->countTrades ),
				),
				Array( 
					'label' => 'Open', 
					'value' =>  CommonLib::numberFormat( $this->data->stats->countOpen ),
					'visible' => !$this->data->server->forReports
				),
				
				Array( 
					'label' => 'Equity', 
					'visible' => $this->data->stats->equity !== null, 
					'value' =>  CommonLib::numberFormat( $this->data->stats->equity ),
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Profit', 
					'visible' => $this->data->stats->profit !== null, 
					'value' =>  '$ '.CommonLib::numberFormat( $this->data->stats->profit ),
					'type' => 'raw', 
				),
				
				Array( 
					'label' => 'Withdrawals', 
					'visible' => $this->data->stats->withdrawals !== null, 
					'value' => '$ '.CommonLib::numberFormat( $this->data->stats->withdrawals ),
					'type' => 'raw', 
				),
				
				Array( 
					'label' => 'Avg. Trade Time', 
					'value' => $this->formatTradeTime( $this->data->stats->avgTradeTimeSec ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Best Trade', 
					'value' => !$this->data->stats->bestTrade ? Yii::t( '*', 'no data' ) : '$ '.number_format( $this->data->stats->bestTrade, 2, '.', ' ' ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Worst Trade', 
					'value' => !$this->data->stats->worstTrade ? Yii::t( '*', 'no data' ) : '$ '.number_format( $this->data->stats->worstTrade, 2, '.', ' ' ) ,
					'type' => 'raw', 
				),
				Array( 
					'label' => 'Profitable Trades', 
					'value' => $this->formatProfitableTrades( $this->data->stats ) ,
					'type' => 'raw', 
				),

			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}

		
		function formatProfitableTrades( $stats ) {
			if( !$stats->profitableTrades || !$stats->allTrades ) return Yii::t( '*', 'no data' );
			return number_format( ( $stats->profitableTrades/$stats->allTrades ) * 100, 2, '.', ' ' ).'%';
		}
		function formatTradeTime( $time ) {
			if( !$time ) return Yii::t( '*', 'no data' );
			$d = gmdate("j", $time);
			$h = gmdate("G", $time);
			$m = gmdate("i", $time);
			$outDate = '';
			if( $d-1 ) $outDate .= ($d-1).Yii::t( '*', 'IntervalDays' ).' ';
			if( $h ) $outDate .= $h.Yii::t( '*', 'IntervalHours' ).' ';
			if( $m ) $outDate .= $m.Yii::t( '*', 'IntervalMinutes' ).' ';
			return $outDate;
		}
		
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'label_green' : 'label_red' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		
		
		public function run(){
			$formatter=$this->getFormatter();
			if ($this->tagName!==null)
				echo CHtml::openTag($this->tagName,$this->htmlOptions);

			$i=0;
			$n=is_array($this->itemCssClass) ? count($this->itemCssClass) : 0;

			foreach($this->attributes as $attribute)
			{
				if(is_string($attribute))
				{
					if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$attribute,$matches))
						throw new CException(Yii::t('zii','The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
					$attribute=array(
						'name'=>$matches[1],
						'type'=>isset($matches[3]) ? $matches[3] : 'text',
					);
					if(isset($matches[5]))
						$attribute['label']=$matches[5];
				}

				if(isset($attribute['visible']) && !$attribute['visible'])
					continue;

				$tr=array('{label}'=>'', '{class}'=>$n ? $this->itemCssClass[$i%$n] : '');
				if(isset($attribute['cssClass']))
					$tr['{class}']=$attribute['cssClass'].' '.($n ? $tr['{class}'] : '');

				if(isset($attribute['label']))
					$tr['{label}']=$attribute['label'];
				elseif(isset($attribute['name']))
				{
					if($this->data instanceof CModel)
						$tr['{label}']=$this->data->getAttributeLabel($attribute['name']);
					else
						$tr['{label}']=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute['name'])))));
				}
				
				if(isset($attribute['labelCssClass']))
					$tr['{labelCssClass}']=$attribute['labelCssClass'];
				else{
					$tr['{labelCssClass}']='';
				}
				
				if(isset($attribute['valueCssClass']))
					$tr['{valueCssClass}']=$attribute['valueCssClass'];
				else{
					$tr['{valueCssClass}']='';
				}

				if(!isset($attribute['type']))
					$attribute['type']='text';
				if(isset($attribute['value']))
					$value=is_object($attribute['value']) && get_class($attribute['value']) === 'Closure' ? call_user_func($attribute['value'],$this->data) : $attribute['value'];
				elseif(isset($attribute['name']))
					$value=CHtml::value($this->data,$attribute['name']);
				else
					$value=null;

				$tr['{value}']=$value===null ? $this->nullDisplay : $formatter->format($value,$attribute['type']);

				$this->renderItem($attribute, $tr);

				$i++;
			}

			if ($this->tagName!==null)
				echo CHtml::closeTag($this->tagName);
		}

	}

?>