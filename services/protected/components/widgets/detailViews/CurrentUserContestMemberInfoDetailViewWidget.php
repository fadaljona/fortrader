<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class CurrentUserContestMemberInfoDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wCurrentUserContestMemberInfoDetailView';
		public $data;
		public $itCurrentModel;

		public $NSi18n = 'components/widgets/CurrentUserContestMemberInfoDetailViewWidget';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w} inside_table",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/currentUserContestMemberInfo/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$canControl = Yii::App()->user->checkAccess( 'contestMemberControl' );
			
			$attributes = Array(
				Array( 
					'label' => 'Account number', 
					'value' => CHtml::link( CHtml::encode( $this->data->accountNumber ), $this->data->singleURL, array( 'target' => '_blank', 'class' => 'blue_color' ) ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw'
				),
				Array( 
					'label' => 'Rank', 
					'value' => $this->data->stats->place ? CommonLib::numberFormat( $this->data->stats->place ) : '',
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw'
				),
				Array( 
					'label' => 'Balance', 
					'value' => $this->data->stats->balance !== null ? '$ '.CommonLib::numberFormat( $this->data->stats->balance ) : '',
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw'
				),
				Array( 
					'label' => 'Gain', 
					'value' => $this->data->stats->gain !== null ? $this->formatPercent( $this->data->stats->gain ) : '',
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw'
				),
				Array( 
					'label' => 'Drow', 
					'value' => $this->data->stats->drowMax !== null ? $this->formatPercent( $this->data->stats->drowMax ) : '',
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw'
				),
				Array( 
					'label' => 'Tradese', 
					'value' => CommonLib::numberFormat( $this->data->stats->countTrades ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw'
				),
				Array( 
					'label' => 'Open', 
					'value' => CommonLib::numberFormat( $this->data->stats->countOpen ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw'
				),
				Array( 
					'label' => 'Status', 
					'value' => $this->data->getLabelStatus(),
					'labelCssClass' => 'inside_col_yellow',
					'valueCssClass' => 'bg_light_yellow',
					'type' => 'raw'
				),
				Array( 
					'label' => 'Updated', 
					'value' => $this->formatUpdate( $this->data ),
					'labelCssClass' => 'inside_col_blue',
					'type' => 'raw'
				),
				Array( 
					'label' => 'Edit', 
					'visible' => $this->itCurrentModel || $canControl,
					'value' => $this->formatEdit( $this->data->contest->status ),
					'labelCssClass' => 'inside_col_yellow',
					'valueCssClass' => 'bg_light_yellow',
					'type' => 'raw',
				),
				
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatEdit( $status ){
			if( $status == 'registration' ) return '<i aria-hidden="true" class="fa fa-pencil-square-o"></i> | <i aria-hidden="true" class="fa fa-times"></i>';
			return '<i aria-hidden="true" class="fa fa-times"></i>';
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatUpdate( $data ) {
			if( $data->stats->trueDT ){
				ob_start();
				$this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->stats->trueDT, 'class' => "iBold" ));
				$content = ob_get_clean();
				return $content;
			} 
			return '';
		}
		
		
		public function renderTable( $from, $to ){
			if ($this->tagName!==null)
				echo CHtml::openTag($this->tagName, array( 'class' => $this->htmlOptions['class'] ));
			$formatter=$this->getFormatter();
			$i=0;
			$n=is_array($this->itemCssClass) ? count($this->itemCssClass) : 0;
			if( $to > count( $this->attributes ) ) $to = count( $this->attributes );
			for( $j = $from; $j < $to; $j++ ){
				$attribute = $this->attributes[$j];
				if(is_string($attribute))
				{
					if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$attribute,$matches))
						throw new CException(Yii::t('zii','The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
					$attribute=array(
						'name'=>$matches[1],
						'type'=>isset($matches[3]) ? $matches[3] : 'text',
					);
					if(isset($matches[5]))
						$attribute['label']=$matches[5];
				}

				if(isset($attribute['visible']) && !$attribute['visible'])
					continue;

				$tr=array('{label}'=>'', '{class}'=>$n ? $this->itemCssClass[$i%$n] : '');
				if(isset($attribute['cssClass']))
					$tr['{class}']=$attribute['cssClass'].' '.($n ? $tr['{class}'] : '');

				if(isset($attribute['label']))
					$tr['{label}']=$attribute['label'];
				elseif(isset($attribute['name']))
				{
					if($this->data instanceof CModel)
						$tr['{label}']=$this->data->getAttributeLabel($attribute['name']);
					else
						$tr['{label}']=ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $attribute['name'])))));
				}
				
				if(isset($attribute['labelCssClass']))
					$tr['{labelCssClass}']=$attribute['labelCssClass'];
				else{
					$tr['{labelCssClass}']='';
				}
				
				if(isset($attribute['valueCssClass']))
					$tr['{valueCssClass}']=$attribute['valueCssClass'];
				else{
					$tr['{valueCssClass}']='';
				}

				if(!isset($attribute['type']))
					$attribute['type']='text';
				if(isset($attribute['value']))
					$value=$attribute['value'];
				elseif(isset($attribute['name']))
					$value=CHtml::value($this->data,$attribute['name']);
				else
					$value=null;

				$tr['{value}']=$value===null ? $this->nullDisplay : $formatter->format($value,$attribute['type']);

				$this->renderItem($attribute, $tr);

				$i++;
			}
			if ($this->tagName!==null)
				echo CHtml::closeTag($this->tagName);
		}

		public function run(){
			
			$countRows = count( $this->attributes );
			$forFirstTable = ($countRows + $countRows % 2) / 2;
			
			$originClass = $this->htmlOptions['class'];
			
			$this->htmlOptions['class'] = $originClass . ' table1';
			$this->renderTable(0, $forFirstTable);
			
			$this->htmlOptions['class'] = $originClass . ' table2';
			$this->renderTable($forFirstTable, $countRows);
			
			$this->htmlOptions['class'] = $originClass;
		
		}
	}

?>