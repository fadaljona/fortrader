<?
	Yii::import( 'widgets.detailViews.base.DetailViewWidgetBase' );
	
	final class EAProfileDetailViewWidget extends DetailViewWidgetBase {
		public $w = 'wEAProfileDetailView';
		public $class = 'iDetailView i01 profile-user-info profile-user-info-striped';
		public $tagName = 'div';
		public $NSi18n = 'components/widgets/eaProfileDetailView';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->itemTemplate = file_get_contents( dirname( __FILE__ ).'/userProfile/itemTemplate.tpl' );
			parent::init();
		}
		protected function getAttributes() {
			$attributes = Array(
				Array( 'label' => 'Name', 'name' => 'name' ),
				Array( 'label' => 'Creator', 'name' => 'creator', 'visible' => strlen( $this->data->creator )),
				Array( 'label' => 'License', 'name' => 'license', 'visible' => strlen( $this->data->license )),
				Array( 'label' => 'Platform', 'value' => $this->formatPlatforms(), 'visible' => $this->data->tradePlatforms ),
				Array( 'label' => 'Released', 'value' => $this->formatReleased() ),
				Array( 'label' => 'Description', 'value' => $this->formatDescription(), 'type' => 'raw', 'visible' => strlen( $this->data->desc ) ),
			);
			foreach( $attributes as &$attribute ) if( isset( $attribute[ 'label' ])) $attribute[ 'label' ] = $this->t( $attribute[ 'label' ]); unset( $attribute );
			return $attributes;
		}
		function formatReleased() {
			return date( "d/m/Y", strtotime( $this->data->date ));
		}
		function formatPlatforms() {
			return implode( ', ', CommonLib::slice( $this->data->tradePlatforms, 'name' ));
		}
		function formatDescription() {
			return CHtml::link( "View", CommonLib::getURL( $this->data->desc ), Array( 'target' => "_blank" ))
				   .' <i class="icon-external-link bigger-10 green"></i>';
		}
	}

?>