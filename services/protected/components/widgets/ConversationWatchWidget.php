<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ConversationWatchWidget extends WidgetBase {
		
		public $idObj;

		function run() {

			$usersOnline = ConversationWatchModel::model()->findAll(Array(
				'with' => Array( 
					'user' => Array(
						'select' => " ID, user_login, user_nicename, display_name, firstName, lastName ",
					),
				),
				'condition' => " `t`.`idObj` = :idObj ",
				'order' => " `t`.`lastRequest` ",
				'params' => Array(
					':idObj' => $this->idObj,
				),
			));

			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'usersOnline' => $usersOnline
			));
		}
	}

?>