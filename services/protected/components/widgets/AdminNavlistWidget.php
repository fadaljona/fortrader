<?
	Yii::import( 'components.NavlistComponent' );

	final class AdminNavlistWidget extends WidgetExBase {
		public $navlist;
		protected function detNavlist() {
			return NavlistComponent::getAdminNavlist();
		}
		function renderSubmenu( $view = null, $data = Array(), $return = false ) {
			$defData = Array(
				'navlist' => $this->detNavlist(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			
			if( !strlen( $view )) $view = "viewSubmenu";
			return CWidget::render( $view, $data, $return );
		}
		function render( $view = null, $data = Array(), $return = false ) {
			$defData = Array(
				'navlist' => $this->detNavlist(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			return parent::render( $view, $data, $return );
		}
	}

?>