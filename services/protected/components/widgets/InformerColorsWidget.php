<?
Yii::import('components.widgets.base.WidgetBase');

final class InformerColorsWidget extends WidgetBase{
	public $model;
	
	function run() {
		if( !$this->model ) return false;
		$this->render( $this->getCleanClassName() . "/view", Array(
			'currentColors' => $this->model->colors,
			'viewFilesDir' => $this->model->viewFilesDir,
		));
	}
	
}
?>