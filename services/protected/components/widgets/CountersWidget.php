<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CountersWidget extends WidgetBase {
		public $counters;
		private function getCounters() {
			$c = new CDbCriteria();
			$c->addCondition( " `t`.`enabled` " );
			if( $this->counters == 'common' ) {
				$c->addCondition( " `t`.`common` " );
			}
			if( is_array( $this->counters ) and $this->counters ) {
				$c->addInCondition( " `t`.`name` ", $this->counters );
			}
			return CounterModel::model()->findAll( $c );
		}
		function run() {
			$counters = $this->getCounters();
			foreach( $counters as $counter ) {
				echo $counter->getCode();
			}
		}
	}

?>