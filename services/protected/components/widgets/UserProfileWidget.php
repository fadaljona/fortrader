<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.UserAvatarFormModel' );
	
	final class UserProfileWidget extends WidgetBase {
		public $model;
		private function getModel() {
			return $this->model;
		}
		private function getItCurrentModel() {
			$model = $this->getModel();
			$currentModel = Yii::App()->user->getModel();
			$itCurrentModel = ($currentModel and $currentModel->id == $this->model->id);
			return $itCurrentModel;
		}
		private function getCanProfileControl() {
			return $this->getItCurrentModel() || Yii::App()->user->checkAccess( 'userControl' );
		}
		private function getAvatarFormModel() {
			$model = new UserAvatarFormModel();
			$model->load( $this->model->id );
			return $model;
		}
		
		private function getCountTradingAccounts() {
			$model = $this->getModel();
			return $model->countTradingAccounts;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
				'avatarFormModel' => $this->getAvatarFormModel(),
				'canControl' => $this->getCanProfileControl(),
				'countTradingAccounts' => $this->getCountTradingAccounts(),
			));
		}
	}

?>