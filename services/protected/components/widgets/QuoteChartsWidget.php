<?
Yii::import('components.widgets.base.WidgetBase');

final class QuoteChartsWidget extends WidgetBase{
    public $model;
    private $periods = array(
        'MN1' => array( 'minPeriod' => "MM", 'categoryBalloonDateFormat' => "MMMM YYYY", 'periodVal' => 43200 ),
        'W1' => array( 'minPeriod' => "7DD", 'categoryBalloonDateFormat' => "MMMM DD YYYY", 'periodVal' => 10080 ),
        'D1' => array( 'minPeriod' => "DD", 'categoryBalloonDateFormat' => "MMMM DD YYYY", 'periodVal' => 1440 ),
        'H4' => array( 'minPeriod' => "hh", 'categoryBalloonDateFormat' => "JJ:NN, MMMM DD YYYY", 'periodVal' => 240 ),
        'H1' => array( 'minPeriod' => "hh", 'categoryBalloonDateFormat' => "JJ:NN, MMMM DD YYYY", 'periodVal' => 60 ),
        /*'30M' => array( 'minPeriod' => "30mm", 'categoryBalloonDateFormat' => "JJ:NN, MMMM DD YYYY", 'periodVal' => 30 ),
        '15M' => array( 'minPeriod' => "15mm", 'categoryBalloonDateFormat' => "JJ:NN, MMMM DD YYYY", 'periodVal' => 15 ),*/
        /*'5M' => array( 'minPeriod' => "5mm", 'categoryBalloonDateFormat' => "JJ:NN, MMMM DD YYYY", 'periodVal' => 5 ),
        '1M' => array( 'minPeriod' => "mm", 'categoryBalloonDateFormat' => "JJ:NN, MMMM DD YYYY", 'periodVal' => 1 ),*/
    );

    private function getHistory(){
        $existHistory = QuotesHistoryDataModel::hasData( $this->model->id, 1440 );
        
        $dataFound = 0;
        if( $existHistory ){
            $this->periods['D1']['data'] = QuotesHistoryDataModel::getDataForPeriodChart( $this->model->id, 1440 );
            $dataFound = 1;
        }
        foreach( $this->periods as $period => $periodData ){
            $existHistory = QuotesHistoryDataModel::hasData( $this->model->id, $periodData['periodVal'] );
            if( !$existHistory ){
                unset( $this->periods[$period] );
            }
            if( $existHistory && !$dataFound ){
                $this->periods[$period]['data'] = QuotesHistoryDataModel::getDataForPeriodChart( $this->model->id, $periodData['periodVal'] );
                $dataFound = 1;
            }
        }
        return $this->periods;
    }
    private function getPreview(){
        if( !$this->model->lastBid ) return false;
        if( !$this->model->preview ) return false;
        $histData = QuotesHistoryDataModel::getTodayD1Data( $this->model->id );
        if( !$histData ) return false;
        
        $precision = max( strlen($histData['{$close}']) - strpos( $histData['{$close}'], '.' ) - 1, strlen($this->model->lastBid) - strpos( $this->model->lastBid, '.' ) - 1 );
        
        $ch = $histData['{$close}'] - $this->model->lastBid;
        $chPercent = $ch / $histData['{$close}'] * 100 ;
        
        $histData['{$ch}'] = number_format( $ch, $precision, '.', ' ');
        $histData['{$ch%}'] = number_format( $chPercent, 2, '.', ' ');
        
        /*if( $this->model->tickData ){
            $histData['{$price}'] = $this->model->tickData->bid;
        }else{
            $histData['{$price}'] = $histData['{$close}'];
        }*/
        $histData['{$price}'] = $histData['{$close}'];

        $histData['{$yesterdayClose}'] = '';
        if( $this->model->lastDayData ){
            $histData['{$yesterdayClose}'] = $this->model->lastDayData->close;
        }
        
        $preview = $this->model->preview;
        foreach( $histData as $key => $val ){
            $preview = str_replace( $key, $val, $preview );
        }
        return $preview;
    }
    public function init(){
        if( $this->model->sourceType == 'bitz' ){
            foreach( $this->periods as $key => $period ){
                if( $key != 'D1' && $key != 'H1' ){
                    unset( $this->periods[$key] );
                }
            }
        }
        if( $this->model->sourceType == 'okex' ){
            foreach( $this->periods as $key => $period ){
                if( $key != 'D1' && $key != 'H1' && $key != 'W1' ){
                    unset( $this->periods[$key] );
                }
            }
        }
    }
    
    function run() {
        $this->render( $this->getCleanClassName() . "/view", Array(
            'model' => $this->model,
            'periods' => $this->getHistory(),
            'preview' => $this->getPreview()
        ));
    }
    
}
?>