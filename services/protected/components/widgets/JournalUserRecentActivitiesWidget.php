<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class JournalUserRecentActivitiesWidget extends WidgetBase {
		public $ajax = true;
		public $limitActivities = 5;
		public $idLastActivity;
		private function getIDLastActivity() {
			if( $this->idLastActivity ) return $this->idLastActivity;
			$id = (int)@$_GET['idLastActivity'];
			return $id ? $id : null;
		}
		private function getCriteriaActivities( $forCount = false ) {
			$c = new CDbCriteria();
			
			if( !$forCount ) {
				$c->with[ 'currentLanguageI18N' ] = Array();
				$c->with[ 'i18ns' ] = Array();
				$c->with[ 'user' ] = Array(
					'select' => Array( 'ID', 'user_login', 'user_nicename', 'display_name', 'firstName', 'lastName' ),
				);
				$c->with[ 'user.avatar' ] = Array(
					'select' => Array( 'idUser', 'thumbPath' ),
				);
			}
			
			$idLastActivity = $this->getIDLastActivity();			
			if( $idLastActivity ) {
				$c->addCondition( " `t`.`id` < :idLastActivity " );
				$c->params[ ':idLastActivity' ] = $idLastActivity;
			}
			
			if( !$forCount ) {
				$c->order = " `t`.`id` DESC ";
				$c->limit = $this->limitActivities;
			}
			
			
			$c->addInCondition( '`t`.`type`', Array( 'create_journal_comment', 'download_journal', 'view_journal' ));
			
			return $c;
		}
		private function getActivities() {
			$c = $this->getCriteriaActivities();
			$activities = UserActivityModel::model()->findAll( $c );
			return $activities;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->ajax,
			));
		}
		function renderActivities() {
			$activities = $this->getActivities();
			foreach( $activities as $activity ){
				$this->renderActivity( $activity );
			}
		}
		function renderActivity( $activity ) {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/activity", Array(
				'activity' => $activity,
			));
			$this->idLastActivity = $activity->id;
		}
		function issetMoreActivities() {
			$c = $this->getCriteriaActivities( true );
			$exists = UserActivityModel::model()->exists( $c );
			return $exists;
		}
	}

?>