<?php
Yii::import('components.widgets.base.WidgetBase');

final class QuoteChartMaxZoomWidget extends WidgetBase
{
    public $symbolId;

    public function run()
    {
        if (!$this->symbolId) {
            return false;
        }

        $model = QuotesSymbolsModel::model()->findByPk($this->symbolId);
        if (!$model) {
            return false;
        }

        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'symbolId' => $this->symbolId
        ));
    }
}
