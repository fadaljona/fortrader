<?
Yii::import('components.widgets.base.WidgetBase');

final class PostInformWidget extends WidgetBase{
	const CACHE_KEY_PREFIX='PostInformWidget_';
	public $cacheID='cache';
	public $cachingDuration=60;

	private function getPosts(){
		$posts=WPPostModel::model()->findAll(Array(
			'select'=>" ID, post_title, post_date ",
			'with'=>Array(
				"categories"=>Array("together"=>false),
				"important"=>Array(),
				"thumbnail"=>Array(),
				"daylife"=>Array(),
			),
			'condition'=>"
					`important`.`meta_value` IS NOT NULL
					AND	(
						DATEDIFF( NOW(), `t`.`post_date` ) <= `daylife`.`meta_value`
					)
				",
			'limit' => 30,
			'order'=>"
					`t`.`post_date` DESC
				",
		));

		return $posts;
	}

	private function renderWidget(){
		$class=$this->getCleanClassName();
		return $this->render("{$class}/view", Array(
			'posts'=>$this->getPosts()
		), true);
	}

	function run(){
		Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/widgets/wPostInformWidget.js");

		$cache=Yii::app()->getComponent($this->cacheID);
		$cacheKey=self::CACHE_KEY_PREFIX."01";

		if($this->cachingDuration > 0 && $cache){
			if(($content=$cache->get($cacheKey)) !== false){
				$html=$content;
			}else{
				$html=$this->renderWidget();
				$cache->set($cacheKey, $html, $this->cachingDuration);
			}
		}else{
			$html=$this->renderWidget();
		}

		echo $html;

	}
}
