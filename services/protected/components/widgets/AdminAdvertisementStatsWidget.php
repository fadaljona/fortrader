<?
	Yii::import( 'components.widgets.base.WidgetBase' );

	final class AdminAdvertisementStatsWidget extends WidgetBase {
		private $campaign;
		private function getMode() {
			return Yii::App()->user->checkAccess( 'advertisementControl' ) ? 'Admin' : 'User';
		}
		private function getUser() {
			$user = Yii::App()->user->getModel();
			if( !$user ) throw new Exception( "Can't find User! Relogin!" );
			return $user;
		}
		private function getIDUser() {
			$user = $this->getUser();
			return $user->id;
		}
		private function getIDCampaign() {
			return (int)@$_GET[ 'idCampaign' ];
		}
		private function getIDBroker() {
			$idBroker = @$_GET['idBroker'];
			if( !$idBroker ) {
				$idBroker = Yii::App()->user->getModel()->idBroker;
				if( !$idBroker ) {
					$idBroker = 'null';
				}
			}
			return $idBroker;
		}
		private function getCampaign() {
			if( $this->campaign === null ) {
				$idCampaign = $this->getIDCampaign();
				if( $idCampaign ) {
					$this->campaign = AdvertisementCampaignModel::model()->findByPk( $idCampaign );
					if( !$this->campaign ) $this->throwI18NException( "Can't find Campaign!" );
					if( !$this->campaign->checkAccess() ) $this->throwI18NException( "Access denied!" );
				}
				else $this->campaign = false;
			}
			return $this->campaign;
		}
		private function getCampaigns() {
			$campaigns = Array();
			$mode = $this->getMode();
			
			$c = new CDbCriteria();
			$c->select = ' id, name ';
			
			if( $mode == 'Admin' ) {
				$c->with = Array(
					'owner' => Array(
						'select' => ' user_login ',
					)
				);
				$c->order = "`owner`.`user_login`, `t`.`name`";
			}
			else{
				$idUser = $this->getIDUser();
				$c->condition = "`t`.`idUser` = :idUser";
				$c->params = Array(
					':idUser' => $idUser,
				);
				$c->order = "`t`.`name`";
			}
			
			
			$models = AdvertisementCampaignModel::model()->findAll( $c );
			if( count( $models )) {
				$idCurrentCampaign = $this->getIDCampaign();
				if( !$idCurrentCampaign ) {
					$NSi18n = $this->getNSi18n();
					$campaigns[ "" ] = Yii::t( $NSi18n, 'Select...' );
				}
				
				foreach( $models as $model ) {
					if( $mode == 'Admin' ) {
						@$campaigns[ $model->id ] = "({$model->owner->user_login}) {$model->name}";
					}
					else{
						$campaigns[ $model->id ] = $model->name;
					}
				}
			}
			return $campaigns;
		}
		private function getBrokers() {
			$NSi18n = $this->getNSi18n();
			$idBroker = $this->getIdBroker();
			$brokers = Array();
			
			$models = BrokerModel::model()->findAll( Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => '`cLI18NBroker`.`officialName`'
			));
			if( !$idBroker ) {
				$brokers[ "" ] = Yii::t( $NSi18n, 'Select...' );
			}
			$brokers[ "null" ] = Yii::t( $NSi18n, '(Without broker)' );
			foreach( $models as $model ) {
				$brokers[ $model->id ] = $model->officialName;
			}
			
			return $brokers;
		}
		private function getModel() {
			$model = @$_GET[ 'model' ];
			if( !$model ) $model = 'Ad';
			return $model;
		}
		private function getRange() {
			$begin = @$_GET[ 'begin' ];
			$end = @$_GET[ 'end' ];
			if( !CommonLib::isDate( $begin )) $begin = date( "Y-m-d" );
			if( !CommonLib::isDate( $end )) $end = date( "Y-m-d" );
			return $end >= $begin ? Array( $begin, $end ) : Array( $end, $begin );
		}
		private function getBegin() {
			list( $begin, $end ) = $this->getRange();
			return $begin;
		}
		private function getEnd() {
			list( $begin, $end ) = $this->getRange();
			return $end;
		}
		private function getCriteriaAdDP() {
			$c = new CDbCriteria();
			
			list( $begin, $end ) = $this->getRange();
			
			$c->select = Array(
				" `t`.* ",
				" (
					SELECT			SUM( `countShows` )
					FROM			{{advertisement_stats}}
					WHERE			`idAd` = `t`.`id`
						AND			`date` BETWEEN '{$begin}' AND '{$end}'
				) AS countShows",
				" (
					SELECT			SUM( `countClicks` )
					FROM			{{advertisement_stats}}
					WHERE			`idAd` = `t`.`id`
						AND			`date` BETWEEN '{$begin}' AND '{$end}'
				) AS countClicks"
			);
			
			$this->getCampaign();
			$idCurrentCampaign = $this->getIDCampaign();
			
			$c->condition = " `t`.`idCampaign` = :idCampaign ";
			$c->params[':idCampaign'] = $idCurrentCampaign;
			$c->order = '`t`.`header` ASC';
			
			return $c;
		}
		private function getCriteriaCampaignDP() {
			$c = new CDbCriteria();
			
			list( $begin, $end ) = $this->getRange();
			
			$c->select = Array(
				" `t`.* ",
				" (
					SELECT			SUM( `countShows` )
					FROM			{{advertisement_stats}}
					INNER JOIN		{{advertisement}} ON ( `id` = `idAd` )
					WHERE			`idCampaign` = `t`.`id`
						AND			`date` BETWEEN '{$begin}' AND '{$end}'
				) AS countShows",
				" (
					SELECT			SUM( `countClicks` )
					FROM			{{advertisement_stats}}
					INNER JOIN		{{advertisement}} ON ( `id` = `idAd` )
					WHERE			`idCampaign` = `t`.`id`
						AND			`date` BETWEEN '{$begin}' AND '{$end}'
				) AS countClicks"
			);
			$c->with['owner'] = Array( 
				'select' => " user_login, balance ",
				'joinType' => 'INNER JOIN'
			);

			if( $this->getMode() == 'Admin' ) {
				$idBroker = $this->getIDBroker();
				if( $idBroker == 'null' ) {
					$c->condition = "`owner`.`idBroker` IS NULL";
				}
				elseif( $idBroker ){
					$c->condition = "`owner`.`idBroker` = :idBroker";
					$c->params[':idBroker'] = $idBroker;
				}
			}
			else{
				$idUser = $this->getIDUser();
				$c->condition = "`t`.`idUser` = :idUser";
				$c->params[':idUser'] = $idUser;
			}

			$c->order = '`t`.`name` ASC';
			
			return $c;
		}
		private function getCriteriaZoneDP() {
			$c = new CDbCriteria();
			
			list( $begin, $end ) = $this->getRange();
			
			$c->select = Array(
				" `t`.* ",
				" (
					SELECT			SUM( `countShows` )
					FROM			{{advertisement_zone_stats}}
					WHERE			`idZone` = `t`.`id`
						AND			`date` BETWEEN '{$begin}' AND '{$end}'
				) AS countShows",
				" (
					SELECT			SUM( `countClicks` )
					FROM			{{advertisement_zone_stats}}
					WHERE			`idZone` = `t`.`id`
						AND			`date` BETWEEN '{$begin}' AND '{$end}'
				) AS countClicks"
			);
			
			$c->order = " `t`.`name` ASC ";
			
			return $c;
		}
		private function getCriteriaDP() {
			switch( $this->getModel() ) {
				case 'Ad':
				default: return $this->getCriteriaAdDP();
				case 'Campaign': return $this->getCriteriaCampaignDP();
				case 'Zone': return $this->getCriteriaZoneDP();
			}
		}
		private function getDP() {
			$namesModels = Array(
				'Ad' => 'AdvertisementModel',
				'Campaign' => 'AdvertisementCampaignModel',
				'Zone' => 'AdvertisementZoneModel',
			);
			$nameModel = @$namesModels[ $this->getModel() ];
			if( !$nameModel ) $nameModel = $namesModels[ 'Ad' ];
			
			$c = $this->getCriteriaDP();
			$DP = new CActiveDataProvider( $nameModel, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
						
			return $DP;
		}
		protected function createGridViewWidget() {
			$NSi18n = $this->getNSi18n();
			$ns = $this->getNS();
			$ins = $this->getINS();
			$DP = $this->getDP();
			
			$params = Array(
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'showTableOnEmpty' => true,
				'dataProvider' => $DP,
				'forStats' => true,
				'selectionChanged' => "{$ns}.wAdminAdvertisementStatsWidget.selectionGridChanged",
			);
			
			switch( $this->getModel() ) {
				case 'Ad':
				default: { 
					$nameWidget = 'widgets.gridViews.AdminAdvertisementsGridViewWidget';
					break;
				}
				case 'Campaign': { 
					$nameWidget = 'widgets.gridViews.AdminAdvertisementCampaignsGridViewWidget';
					$params[ 'mode' ] = $this->getMode();
					break;
				}
				case 'Zone': { 
					$nameWidget = 'widgets.gridViews.AdminAdvertisementZonesGridViewWidget';
					break;
				}
			}
			
			$widget = $this->controller->createWidget( $nameWidget, $params );
			return $widget;
		}
		function run() {
			$class = $this->getCleanClassName();
			
			$model = $this->getModel();
			$mode = $this->getMode();
			
			$params = Array(
				'model' => $model,
				'mode' => $mode,
				'begin' => $this->getBegin(),
				'end' => $this->getEnd(),
			);
			
			switch( $model ) {
				case 'Ad':
				default:{
					$params[ 'idCampaign' ] = $this->getIDCampaign();
					$params[ 'campaigns' ] = $this->getCampaigns();
					break;
				}
				case 'Campaign':{
					if( $mode == "Admin" ) {
						$params[ 'idBroker' ] = $this->getIDBroker();
						$params[ 'brokers' ] =  $this->getBrokers();
					}
					break;
				}
			}
			
			$this->render( "{$class}/view", $params );
		}
	}

?>