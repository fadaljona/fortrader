<?
Yii::import('components.widgets.base.WidgetBase');

final class SearchBrokersWidget extends WidgetBase{
	public $minInputChars;
	public $type;

	function run() {
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'minInputChars' => $this->minInputChars,
			'type' => $this->type,
		));
	}
	
}
?>