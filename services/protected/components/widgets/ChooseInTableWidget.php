<?
Yii::import('components.widgets.base.WidgetBase');

final class ChooseInTableWidget extends WidgetBase{
	public $param;
	public $linkRout;
	public $options;
	public $header;
	
	
	function run() {
		$this->render( $this->getCleanClassName() . "/view", Array(
			'param' => $this->param,
			'linkRout' => $this->linkRout,
			'options' => $this->options,
			'header' => $this->header,
		));
	}
	
}
?>