<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class JournalProfileWidget extends WidgetBase {
		public $mode = 'single';
		public $model;

		private function getModel() {
			return $this->model;
		}
		function run() {
			$JournalSettings = JournalSettingsModel::getModel();
			//JournalModel::updateStats();
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
				'mode' => $this->mode,
				'buyLink' => $JournalSettings->buyLink,

			));
		}
	}

?>