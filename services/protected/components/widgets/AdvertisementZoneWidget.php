<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdvertisementZoneWidget extends WidgetBase {
		const MODE_DEFAULT = 0;
		const MODE_PREVIEW = 1;
		public $mode = self::MODE_DEFAULT;
		public $zone;
		private function selectADs( $count, $idsEx = Array(), $idsExCampaigns = Array(), $onlyNew = false, $order = "RAND()" ) {
			list( $widthAdZone,  $heightAdZone ) = $this->zone->getSizesAd();
			
			$c = new CDbCriteria();
			$c->select = " id, type, header, text, url, resizedImage, widthImage, heightImage, countLikes, createdDT, htmlBaner, isDirectAdLink ";
			
			$c->with[ 'campaign' ] = Array( 'select' => " id, oneAd, minPopup, hideOnMobiles " );
			$c->with[ 'campaign.owner' ] = Array( 'select' => false );
			
			$c->with['likeCurrentUser'] = Array();
			
			$c->addCondition( "  
				NOT `t`.`dontShow`
				AND `campaign`.`status` = 'Active'
				AND CURDATE() BETWEEN `campaign`.`begin` AND `campaign`.`end`
				AND (  
					( `campaign`.`startTime` <= :nowTime AND `campaign`.`endTime` >= :nowTime AND (`campaign`.`endTime` - `campaign`.`startTime`) > 0 ) 
					OR ( (`campaign`.`startTime` <= :nowTime OR `campaign`.`endTime` >= :nowTime) AND (`campaign`.`endTime` - `campaign`.`startTime`) < 0 )  
				)
				AND `owner`.`balance` > 0
				AND (
					`t`.`showEverywhere` AND ( 
						`t`.`type` IN ( 'Text', 'Offer' ) 
						OR ( `t`.`type` IN ('Image', 'Button') AND :typeBlockZone = 'Fixed' AND :widthAdZone >= `t`.`widthImage` AND :heightAdZone >= `t`.`heightImage` )
					)
					OR EXISTS (
						SELECT 		1
						FROM		`{{advertisement_zone_to_advertisement}}`
						WHERE		`idZone` = :idZone
							AND		`idAd` = `t`.`id`
						LIMIT		1
					)
				)
				AND (
					:typeAdsZone = 'Text' AND `t`.`type` = 'Text'
					OR :typeAdsZone = 'Media' AND `t`.`type` = 'Image'
					OR :typeAdsZone = 'Buttons' AND `t`.`type` = 'Button'
					OR :typeAdsZone = 'Offers' AND `t`.`type` = 'Offer'
					OR :typeAdsZone = 'Text' AND `t`.`type` = 'Image' AND :mixedZone = 1
				)
			" );
			
			if( $idsEx ) {
				$c->addNotInCondition( " `t`.`id` ", $idsEx );	
			}
			
			if( $idsExCampaigns ) {
				$c->addNotInCondition( " `campaign`.`id` ", $idsExCampaigns );
			}
			
			if( $onlyNew ) {
				$c->addCondition( " DATE_ADD( `t`.`createdDT`, INTERVAL 1 WEEK ) >= CURDATE() " );
			}
			
			$screenWidth = (int)@$_GET['width'];
			if( $screenWidth ){
				$c->addCondition( " `t`.`minScreenWidth` < :screenWidth AND `t`.`maxScreenWidth` > :screenWidth " );
			}
			
			$c->order = $order;
			
			$c->limit = $count;
			
			if( $screenWidth ){ $c->params[ ':screenWidth' ] = $screenWidth; }
			$c->params[ ':idZone' ] = $this->zone->id;
			$c->params[ ':typeAdsZone' ] = $this->zone->typeAds;
			$c->params[ ':typeBlockZone' ] = $this->zone->typeBlock;
			$c->params[ ':mixedZone' ] = $this->zone->mixedZone;
			$c->params[ ':widthAdZone' ] = $widthAdZone;
			$c->params[ ':heightAdZone' ] = $heightAdZone;
			
			$c->params[ ':nowTime' ] = date('H:i:s');
			
			$ads = AdvertisementModel::model()->findAll( $c );
			return $ads;
		}
		private function getDefaultADs() {
			$count = $this->zone->getCountAds();
			AdvertisementCampaignModel::updateStatusesByLimit();
			
			$ads = Array();
			$idsEx = Array();
			$idsExCampaigns = Array();
			do {
				$typeRequest = null;
				
				if( $this->zone->typeAds == 'Offers' ) {
					$typeRequest = $ads ? 'Offers' : 'FirstOffer';
				}
				if( $this->zone->typeAds == 'Media' ){
					$typeRequest = 'Banners';
				}
				if( $this->zone->typeAds == 'Text' && $this->zone->mixedZone == 1 ){
					$typeRequest = 'MixedZone';
				}
				
				switch( $typeRequest ) {
					case 'FirstOffer':{
						$adsNew = $this->selectADs( 1, $idsEx, $idsExCampaigns, true );
						if( $adsNew ) break;
					}
					case 'Offers':{
						$adsNew = $this->selectADs( $count, $idsEx, $idsExCampaigns, false, " `t`.`countLikes` DESC, RAND() " );
						break;
					}
					case 'Banners':{
						if( ($this->zone->typeBlock=='Popup') && isset($_COOKIE['popup_services_displayed']) && $_COOKIE['popup_services_displayed'] ) $count=0;
						$adsNew = $this->selectADs( $count, $idsEx, $idsExCampaigns, false, " LOG(1.0-RAND())/`t`.`weight` DESC " );
						break;
					}
					case 'MixedZone':{
						$adsNew = $this->selectADs( $count, $idsEx, $idsExCampaigns, false, " `t`.`type` DESC, RAND() " );
						break;
					}
					default:{
						$adsNew = $this->selectADs( $count, $idsEx, $idsExCampaigns );
						break;
					}
				}
				
				if( $adsNew ) {
					foreach( $adsNew as $i=>$adNew ) {
						if( in_array( $adNew->campaign->id, $idsExCampaigns )) {
							unset( $adsNew[ $i ]);
							continue;
						}
						if( $adNew->campaign->oneAd ) {
							$idsExCampaigns[] = $adNew->campaign->id;
						}
						$idsEx[] = $adNew->id;
					}
					$ads = array_merge( $ads, $adsNew );
					$count -= count( $adsNew );
				}
			} while( $adsNew and $count );
			
			if( $ads ) AdvertisementModel::hitShowModels( $ads );
			return $ads;
		}
		private function getPreviewAD() {
			$NSi18n = $this->getNSi18n();
			$ad = new AdvertisementModel();
			$ad->type = $this->zone->typeAds == 'Offers' ? 'Offer' : 'Text';
			$ad->header = Yii::t( $NSi18n, 'Here is an example of your header' );
			$ad->text = Yii::t( $NSi18n, 'This is an example of your ad. The best advertisement for your advertising.' );
			$ad->url = 'youcolorurl.ru';
			$ad->resizedImage = 'uploads/a1/dummy.jpg';
			$ad->widthImage = 80;
			$ad->heightImage = 60;
			$ad->createdDT = date( 'Y-m-d H:i:s', mt_rand( 1, 2 ) == 1 ? time() : time() - 60*60*24*8 );
			return $ad;
		}
		private function getPreviewADs() {
			$ads = Array();
			$count = $this->zone->getCountAds();
			for(;$count--;) {
				$ads[] = $this->getPreviewAD();
			}
			return $ads;
		}
		private function getADs() {
			$ads = $this->mode == self::MODE_DEFAULT ? $this->getDefaultADs() : $this->getPreviewADs();
			return $ads;
		}
		function run() {
			$class = $this->getCleanClassName();
			$ads = $this->getADs();
			$adSettings = AdvertisementSettingsModel::getModel();

			if( $ads ) {
				$this->render( "{$class}/view", Array(
					'ads' => $ads,
					'zone' => $this->zone,
					'prefix' => $adSettings->ad_prefix,
					'popupDelay' => $adSettings->popupDelay,
				));
				$this->zone->hitShow();
			}
		}
	}

?>