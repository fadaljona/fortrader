<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminRssFeedsFeedGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminCommonGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'title'),
				Array( 'name' => 'slug'),
				Array( 'name' => 'newsCount'),
				Array( 'name' => 'order'),
				Array( 'header' => 'Parent', 'value' => ' $data->parent ? $data->parentRel->title : Yii::t("*", "No parent") '),
				Array( 'header' => 'Group', 'value' => ' $data->idGroup ? $data->group->title : Yii::t("*", "No group") '),
				Array( 
					'name' => 'idLanguage',
					'header' => 'Language', 
					'value' => ' $data->idLanguage == 0 ? Yii::t("*", "English") : $data->lang->name ',
					'class' => 'components.dataColumns.LangsDataColumn'
				),
				/*Array( 
					'name' => 'idSource',
					'header' => 'Source', 
					'value' => ' $data->source->title ',
					'class' => 'components.dataColumns.NewsAggregatorRssSourceDataColumn'
				),*/
				Array( 
					'name' => 'enabled',
					'value' => ' $data->enabled == 0 ? Yii::t("*", "No") : Yii::t("*", "Yes") ',
					'class' => 'components.dataColumns.NewsAggregatorRssEnabledDataColumn'
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>