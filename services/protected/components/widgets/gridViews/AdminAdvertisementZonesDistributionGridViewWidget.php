<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminAdvertisementZonesDistributionGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminAdvertisementZonesDistributionGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idZone" => $data->id ) ';
		public $forStats = false;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Zones' ),
				Array( 'value' => '$data["zId"]', 'header' => 'zId' ),
				Array( 'name' => 'site', 'header' => 'Site'),
				Array( 'value' => ' $this->grid->formatCampaignName( $data ) ', 'type' => 'raw', 'header' => 'Campaign'),
				Array( 'value' => '$data["cId"]', 'header' => 'cId' ),
				Array( 'name' => 'campaignWeight', 'header' => 'Campaign weight'),
				Array( 'value' => ' $this->grid->formatAdsName( $data ) ', 'type' => 'raw', 'header' => 'Advertisements'),
				Array( 'value' => ' $data["adsId"] ', 'type' => 'raw', 'header' => 'AdId'),
				Array( 'name' => 'adsWeight', 'header' => 'Ads weight'),
			);

			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatCampaignName( $data ) {
			$link = CHtml::link( 
				$data['campaignName'], 
				Yii::App()->createUrl( '/admin/advertisementCampaign/list', Array( 'idBroker' => $data['idBroker'] )), 
				Array( 'target' => '_blank' )
			);
			return $link;
		}
		function formatAdsName( $data ) {
			return $link = CHtml::link( 
				$data['adsName'], 
				Yii::App()->createUrl( '/admin/advertisement/listBanners', Array( 'idCampaign' => $data['idCampaign'] )), 
				Array( 'target' => '_blank' )
			);
		}
	}

?>