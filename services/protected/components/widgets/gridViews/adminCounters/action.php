<?
	if( $data->enabled ) {
		$class = 'wDisable btn-danger';
		$title = 'Disable';
	}
	else{
		$class = 'wEnable btn-success';
		$title = 'Enable';
	}
?>
<button class="btn btn-small <?=$class?>"><?=Yii::t( '*', $title )?></button>