<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminUnsubscribeCommentsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wUnsubscribeCommentsGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "idComment" => $data->id, "class" => $this->getClassRow( $data )) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		function getClassRow( $data ) {
			switch( $data->status ) {
				case 'New':{
					return 'warning';
					break;
				}
				case 'Done':{
					break;
				}
				case 'Need answer':{
					return 'error';
					break;
				}
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $this->grid->formatDate( $data->createdDT ) ', 'header' => 'Date', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'value' => ' $data->mailingType->name ', 'header' => 'Type', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'value' => ' $data->user->showName ', 'header' => 'User', 'headerHtmlOptions' => Array( 'class' => '' )),
				//Array( 'value' => ' CommonLib::shorten( $data->comment, 200 )', 'header' => 'Comment', 'headerHtmlOptions' => Array( 'class' => '' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $DT ) {
			return date( "Y.m.d", strtotime( $DT ));
		}
	}

?>