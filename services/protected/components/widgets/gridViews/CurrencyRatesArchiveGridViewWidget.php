<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class CurrencyRatesArchiveGridViewWidget extends GridViewWidgetBase {
		public $w = 'wCurrencyRatesArchiveGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data["id"] ) ';
		public $returnOnlyTrs = false;
		
		public $year;
		public $mo;
		public $day;
		

		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='2' class=\"tabl_quotes table_currency\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $this->grid->formatCode( $data ) ', 
					'header' => 'Code' , 
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_1',
					)
				),
				Array( 
					'header' => 'Nominal for period',
					'value' => ' $this->grid->formatNominal( $data ) ',
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'CurrNominal' )
					)
				),
				Array( 
					'value' => ' $this->grid->formatTitle( $data ) ',
					'header' => 'Title' ,
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'Title' )
					)
				),
			);
			if( $this->day ){
				$columns[] = Array(
					'value' => ' $this->grid->formatValue( $data ) ',
					'header' => 'Course cbr',
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'Course cbr' )
					)
				);
			}else{
				$columns[] = Array(
					'value' => ' $this->grid->formatMinMaxCourse( $data, "min" ) ',
					'header' => 'min Course cbr',
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'min Course cbr' )
					)
				);
				$columns[] = Array(
					'value' => ' $this->grid->formatMinMaxCourse( $data, "max" ) ',
					'header' => 'max Course cbr',
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'max Course cbr' )
					)
				);
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatMinMaxCourse( $data, $type = "min" ){
			if( $type == "min" ){
				$classColor = 'green_color';
				$date = date('d.m.Y', strtotime($data['minValDate']));
				$val = floatval($data['minVal']);
			}else{
				$classColor = 'red_color';
				$date = date('d.m.Y', strtotime($data['maxValDate']));
				$val = floatval($data['maxVal']) / $data['minNominal'] * $data['maxNominal'];
			}
			return "<span><span class='$classColor'>$val \ </span>$date</span>";
		}
		function formatNominal( $data ) {
			if( $this->day ) return $data['nominal'];
			return $data['minNominal'];
		}	
		function formatCode( $data ) {

			$code = CHtml::link(
				$data['code'], 
				CurrencyRatesModel::getArchiveSingleURLByCode($data['code'], $this->year, $this->mo, $this->day),
				array()
			);
			if( $data['cAlias'] ){
				return CountryModel::getImgFlagByAlias('shiny', 16, $data['cAlias']) . " $code";
			}else{
				return $code;
			}
		}
		function formatTitle( $data){
			return CHtml::link( 
				CHtml::encode( $data['currencyName'] ), 
				CurrencyRatesModel::getArchiveSingleURLByCode($data['code'], $this->year, $this->mo, $this->day),
				array( )
			);
		}
		function formatValue( $data ){
			$classColor = '';
			
			if( $data['dataDiffValue'] < 0 ) $classColor = 'red_color';
			if( $data['dataDiffValue'] > 0 )$classColor = 'green_color';
			$date = date('d.m.Y', strtotime($data['dataDate']));
			return "<span><span class='$classColor'>" . floatval($data['dataValue']) ." \ </span>$date</span>";
		}
	}

?>