<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminAdvertisementZonesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdvertisementZonesGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idZone" => $data->id ) ';
		public $forStats = false;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Title' ),
				Array( 'value' => ' $data->site ', 'header' => 'Site', 'headerHtmlOptions' => Array( 'class' => 'hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $this->grid->t( $data->sizeBlock ) ', 'header' => 'Block size', 'headerHtmlOptions' => Array( 'class' => 'hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' CommonLib::numberFormat( $data->countShows ) ', 'header' => 'Shows' ),
				Array( 'value' => ' CommonLib::numberFormat( $data->countClicks ) ', 'header' => 'Clicks' ),
				Array( 'value' => ' $this->grid->formatCTR( $data ) ', 'header' => 'CTR' ),
			);
			if( !$this->forStats ) {
				$columns[] = Array( 'value' => ' $this->grid->formatCodeLink( $data ) ', 'type' => 'raw', 'header' => 'Code' );
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatCTR( $data ) {
			if( $data->CTR === null ) return '';
			$value = CommonLib::numberFormat( $data->CTR );
			$value .= '%';
			return $value;
		}
		function formatCodeLink( $zone ) {
			$link = CHtml::link( Yii::t( $this->NSi18n, "Get code" ), '#', Array( 'class' => 'iCodeLink', 'idZone' => $zone->id ));
			return $link;
		}
	}

?>