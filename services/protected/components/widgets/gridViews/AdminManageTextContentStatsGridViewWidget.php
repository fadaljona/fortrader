<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );
	
	final class AdminManageTextContentStatsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminManageTextContentStatsGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-id" => $data->id ) ';
		public $itemsCssClass = '';
		public $langs;
		public $modelFormName;
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'dayDate'),
				Array( 'header' => 'Language', 'value' => 'LanguageModel::getByID( $data->lang )->name'),
				Array( 'header' => 'User', 'value' => ' $data->user->user_login '),
				Array( 'header' => 'Symbols added', 'value' => ' $data->dayLength '),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>
