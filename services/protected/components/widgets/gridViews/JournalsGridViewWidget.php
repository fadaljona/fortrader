<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.JournalActionsGridColumn' );

	final class JournalsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wJournalsGridView';
		public $class = 'iGridView i05 i07';
		public $rowHtmlOptionsExpression = ' Array( "idJournal" => $data->id ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "dataTable items";
		//public $sortField;
		//public $sortType;
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		/*
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		*/
		protected function getColumns() {
			$columns = Array(
				Array( 'class' => 'ACECheckBoxGridColumn',  'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' ),  'htmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' )),
				Array( 'name' => 'number', 'header' => 'Issue', 'headerHtmlOptions' => Array( 'class' => 'c01', 'style' => "width:20px;" )),
				Array( 'value' => ' $this->grid->formatName( $data ) ', 'header' => 'Issue Name', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c02', 'style' => "width:100%;" )),
				Array( 'value' => ' $this->grid->formatLangs( $data ) ', 'header' => 'Languages', 'headerHtmlOptions' => Array( 'class' => 'c03 hidden-480', 'style' => "width:20px;" ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $this->grid->formatDownloads( $data ) ', 'header' => 'Downloads', 'headerHtmlOptions' => Array( 'class' => 'c04 hidden-480', 'style' => "width:20px" ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
			);
			if( Yii::App()->user->checkAccess( 'journalControl' )) {
				$columns[] = Array( 'class' => 'JournalActionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatName( $data ) {
			return CHtml::link( CHtml::encode( $data->name ), $data->getSingleURL() );
		}
		function formatLangs( $data ) {
			$out = Array();
			foreach( $data->i18ns as $i18n ) {
				$out[] = strtoupper( $i18n->language ? $i18n->language->alias : 'en' );
			}
			return implode( ", ", $out );
		}
		function formatDownloads( $data ) {
			return CommonLib::numberFormat( $data->countDownloads );
		}
	}

?>
