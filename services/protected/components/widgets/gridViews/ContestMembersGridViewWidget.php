<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	//Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.ContestMembersActionsGridColumn' );

	final class ContestMembersGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMembersGridView';
		public $class = 'iGridView i05 i06';
		public $rowCssClassExpression = ' $this->getRowClass( $data ) ';
		public $rowHtmlOptionsExpression = ' Array( "data-idMember" => $data->id ) ';
		public $selectableRows = 0;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		public $tabType = false;
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getRowClass( $data ) {
			if( $data->stats->last_error_code == -255 ) {
				return 'iTr i01';
			}
		}
		protected function getColumns() {
			$columns = Array(
				//Array( 'class' => 'ACECheckBoxGridColumn',  'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' ),  'htmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' )),
                Array(
                    'value' => ' $data->stats->place ',
                    'header' => 'Place',
                    'type' => 'raw',
					'htmlOptions' => Array( 'class' => 'name', 'data-title' => Yii::t( $this->NSi18n, 'Place' ) )
                ),
                Array(
					'value' => ' CHtml::link( CHtml::encode( $data->accountNumber ), $data->singleURL ) ', 
					'header' => 'Account', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
					//	'class' => 'c02 '.$this->getSorting( 'accountNumber' ), 
					//	'data-sortField' => 'accountNumber' 
					),
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Account' ) )
				),
				Array( 
					'value' => ' CHtml::link( CHtml::encode( $data->user->showName ), $data->user->profileURL ) ', 
					'header' => 'Trader', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						//'class' => 'c01 '.$this->getSorting( 'user_login' ), 
						//'data-sortField' => 'user_login' 
					),
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Trader' ) )
				),
				/*Array( 
					'value' => ' $data->user->country ? $this->grid->formatCountry( $data->user->country ) : "" ', 
					'header' => 'Country', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'class' => 'c03 hidden-480 '.$this->getSorting( 'country_name' ), 
						'data-sortField' => 'country_name' 
					),
					'htmlOptions' => Array( 
						'class' => 'hidden-480',
					)

				),*/
				Array( 
					'value' => ' $this->grid->formatBalance( $data ) ', 
					'header' => 'Balance', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'class' => 'c05 hidden-480 '.$this->getSorting( 'balance' ),
						'data-sortField' => 'balance',
					), 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Balance' ) ),
					'class' => 'widgets.gridViews.base.SortableDataColumn',
					'sortType' => $this->getSorting( 'balance' ),

				),
				/*Array( 
					'value' => ' $this->grid->formatGain( $data ) ', 
					'header' => 'Gain', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'class' => 'c04 hidden-phone hidden-tablet-contest '.$this->getSorting( 'gain' ),
						'data-sortField' => 'gain' 
					), 
					'htmlOptions' => Array( 
						'class' => 'iNoWrap hidden-phone hidden-tablet-contest',
					),
				),*/
				/*Array( 
					'value' => ' $this->grid->formatDrow( $data ) ', 
					'header' => 'Drow', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'class' => 'c05 hidden-480 '.$this->getSorting( 'drow' ),
						'data-sortField' => 'drow',
					), 
					'htmlOptions' => Array( 
						'class' => 'iNoWrap hidden-480' 
					)

				),*/
				Array( 
					'value' => ' $this->grid->formatEquity( $data ) ', 
					'header' => 'Equity', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'class' => 'c05 hidden-480 hidden-phone hidden-tablet-contest '.$this->getSorting( 'equity' ),
						'data-sortField' => 'equity',
					), 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Equity' ) ),
					'class' => 'widgets.gridViews.base.SortableDataColumn',
					'sortType' => $this->getSorting( 'equity' ),
				),
				Array( 
					'value' => ' $this->grid->formatCountTrades( $data ) ', //CommonLib::numberFormat( $data->stats->countTrades ) ', 
					'header' => 'Tradese', 
					'headerHtmlOptions' => Array( 
						'class' => 'c06 hidden-480 '.$this->getSorting( 'tradese' ),
						'data-sortField' => 'tradese',
					), 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Tradese' ) ),
					'class' => 'widgets.gridViews.base.SortableDataColumn',
					'sortType' => $this->getSorting( 'tradese' ),


				),
				/*Array( 
					'value' => ' CommonLib::numberFormat( $data->stats->countOpen ) ', 
					'header' => 'Open', 
					'headerHtmlOptions' => Array( 
						'class' => 'c06 hidden-480 '.$this->getSorting( 'open' ),
						'data-sortField' => 'open',
					), 
					'htmlOptions' => Array( 
						'class' => 'iNoWrap hidden-480'
					)

				),*/
				/*Array( 
					'value' => ' ContestMemberModel::lastRevisionByAN($data->accountNumber); ', 
					'header' => 'Ревизия', 
					'headerHtmlOptions' => Array( 
						'class' => 'c06 hidden-480 '.$this->getSorting( 'revision' ),
						'data-sortField' => 'revision',
					), 
					'htmlOptions' => Array( 
						'class' => 'iNoWrap hidden-480'
					)

				),*/
				//Array( 'value' => ' "" ', 'header' => 'Trade Type', 'headerHtmlOptions' => Array( 'class' => 'c07 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array(
					'value' => ' $this->grid->formatStatus( $data ) ',
					'header' => 'Status', 
					'type' => 'raw',
					'headerHtmlOptions' => Array(
						//'class' => 'c07 hidden-480 hidden-phone hidden-tablet-contest '.$this->getSorting( 'status' ),
						//'data-sortField' => 'status',
					),
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Status' ) )
				),
				Array( 
					'value' => ' $this->grid->formatUpdate( $data ) ', 
					'header' => 'Updated', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'class' => 'c08 hidden-480 hidden-phone hidden-tablet-contest',
					), 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Updated' ) )
				),
			);
			/*if( Yii::App()->user->checkAccess( 'contestMemberControl' )) {
				$columns[] = Array( 'class' => 'ContestMembersActionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			}*/
			foreach( $columns as $key => $columnVal ){
				
				if( isset( $columns[$key][ 'header' ]) ){
					if( $this->tabType == 'Participate' && $columns[$key][ 'header' ] == 'Status' ){
						unset( $columns[$key] );
						continue;
					}
					if( $this->tabType == 'Error' ){
						if( $columns[$key][ 'header' ] == 'Place' || $columns[$key][ 'header' ] == 'Balance' || $columns[$key][ 'header' ] == 'Equity' || $columns[$key][ 'header' ] == 'Tradese' ){
							unset( $columns[$key] );
							continue;
						}
					}
				}
			
				if( isset( $columns[$key][ 'header' ])){
					$columns[$key][ 'header' ] = Yii::t( $this->NSi18n, $columns[$key][ 'header' ]);
				}  
			}

			return $columns;
		}
		function formatCountry( $country ) {
			//$name = Yii::t( $this->NSi18n, $country->name );
			//return "{$country->icon}&nbsp;{$name}";
			return "{$country->icon}";
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatGain( $data ) {
			return $data->stats->gain !== null ? $this->formatPercent( $data->stats->gain ) : '';
		}
		function formatDrow( $data ) {
			return $data->stats->drowMax !== null ? $this->formatPercent( $data->stats->drowMax ) : '';
		}
		function formatCountTrades( $data ) {
			return $data->stats->countTrades > 0 ? CommonLib::numberFormat( $data->stats->countTrades ) : 0;
		}
		function formatEquity( $data ) {
			return $data->stats->equity !== null ? '$ '.CommonLib::numberFormat( $data->stats->equity ) : '';
		}
		function formatBalance( $data ) {
			return $data->stats->balance !== null ? '$ '.CommonLib::numberFormat( $data->stats->balance ) : '';
		}
		function formatStatus( $data ) {
			return $data->getLabelStatus();
		}
		function formatUpdate( $data ) {
			$data->stats->trueDT && $this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->stats->trueDT, 'class' => "iBold" ));
		}
	}

?>