<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.EATradeAcoountsActionsGridColumn' );

	final class EATradeAccountInfoGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wEATradeAccountInfoGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "idAccount" => $data->idTradeAccount ) ';
		public $selectableRows = 1;
		
		
		
		
		
		
		
		
		
		
		
		public function renderItems()
		{
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
			{
				echo "<table class=\"{$this->itemsCssClass}\">\n";
				//$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}
			else
				$this->renderEmptyText();
		}
		public function renderTableHeader()
		{
			echo "<thead>\n";

			echo "<tr>\n";
			foreach($this->columns as $column)
				if( isset($column->header) && $column->header ) echo '<th>'.$column->header.'</th>';
				else echo '<th></th>';
			echo "</tr>\n";

			echo "</thead>\n";
		}
		public function renderTableBody()
		{
			$data=$this->dataProvider->getData();
			$n=count($data);
			$rows=count($this->columns);
			echo "<tbody>\n";

			if($n>0)
			{
				for($row=0;$row<$rows;++$row)
					$this->renderTableRow($row);
			}
			else
			{
				echo '<tr><td colspan="'.count($n+1).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
			echo "</tbody>\n";
		}
		public function renderTableRow($row)
		{
			$htmlOptions=array();
			$n=count($data);
			echo CHtml::openTag('tr', $htmlOptions)."\n";
			for( $i=0; $i<$n+1; $i++   ){
				if( $i==0 ) echo '<td>'.$this->columns[$row]->header.'</td>';
				$this->columns[$row]->renderDataCell($i);
			}
			echo "</tr>\n";
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array();

			$columns[] = Array( 'value' => ' CommonLib::numberFormat( $data->tradeAccount->stats->countTrades ) ', 'header' => 'Trades', 'headerHtmlOptions' => Array( 'class' => 'c01 ' ), 'htmlOptions' => Array( 'class' => '' ));
			$columns[] = Array( 'value' => ' $this->grid->formatGain( $data ) ', 'header' => 'Gain', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c02' ), 'htmlOptions' => Array( 'class' => 'iNoWrap' ));
			$columns[] = Array( 'value' => ' $this->grid->formatDrow( $data ) ', 'header' => 'Drow', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c03 ' ), 'htmlOptions' => Array( 'class' => ' iNoWrap' ));
			$columns[] = Array( 'value' => ' CommonLib::numberFormat( $data->tradeAccount->stats->equity ) ', 'header' => 'Equity', 'headerHtmlOptions' => Array( 'class' => 'c04' ));
			$columns[] = Array( 'value' => ' CommonLib::numberFormat( $data->tradeAccount->stats->profit ) ', 'header' => 'Profit', 'headerHtmlOptions' => Array( 'class' => 'c05 ' ), 'htmlOptions' => Array( 'class' => '' ));
			$columns[] = Array( 'value' => ' CommonLib::numberFormat( $data->tradeAccount->stats->deposit ) ', 'header' => 'Deposits', 'headerHtmlOptions' => Array( 'class' => 'c06 ' ), 'htmlOptions' => Array( 'class' => '' ));
			$columns[] = Array( 'value' => ' CommonLib::numberFormat( $data->tradeAccount->stats->withdrawals ) ', 'header' => 'Withdrawals', 'headerHtmlOptions' => Array( 'class' => 'c07 ' ), 'htmlOptions' => Array( 'class' => '' ));
			$columns[] = Array( 'value' => ' $this->grid->formatPlaneDrow( $data->planeDrow ) ', 'header' => 'Plane drow', 'headerHtmlOptions' => Array( 'class' => 'c08 ' ), 'htmlOptions' => Array( 'class' => '' ));
			$columns[] = Array( 'value' => ' $this->grid->formatStatus( $data->tradeAccount->status ) ', 'header' => 'Status', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c09 ' ), 'htmlOptions' => Array( 'class' => '' ));
			$columns[] = Array( 'value' => ' $this->grid->formatUpdate( $data ) ', 'header' => 'Updated', 'headerHtmlOptions' => Array( 'class' => 'c10 hidden-phone' ), 'htmlOptions' => Array( 'class' => 'hidden-phone' ));
			if( Yii::App()->user->checkAccess( 'tradeAccountControl' )) {
				$columns[] = Array( 'header' => 'Edit Information', 'class' => 'EATradeAcoountsActionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatGain( $data ) {
			return $data->tradeAccount->stats->gain !== null ? $this->formatPercent( $data->tradeAccount->stats->gain ) : '';
		}
		function formatDrow( $data ) {
			return $data->tradeAccount->stats->drowMax !== null ? $this->formatPercent( $data->tradeAccount->stats->drowMax ) : '';
		}
		function formatPlaneDrow( $planeDrow ) {
			return $planeDrow !== null ? CommonLib::numberFormat( $planeDrow )." %" : '';
		}
		function formatStatus( $status ) {
			$classes = Array(
				'Off' => 'label',
				'On' => 'label label-success',
			);
			$class = @$classes[ $status ];
			$title = Yii::t( $this->NSi18n, $status );
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatUpdate( $data ) {
			$data->tradeAccount->stats->trueDT && $this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->tradeAccount->stats->trueDT, 'class' => "iBold" ));
		}
	}

?>