<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class TradersRatingForecastGridViewWidget extends GridViewWidgetBase {
		public $w = 'wTradersRatingForecastGridView tabl_quotes';
		public $class = 'iGridView';

		public $forStats = false;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value'=>'$data->karma->karma',
					'header' => 'Karma',
				),
				Array( 
					'value'=>'$this->grid->formatUserName( $data )',
					'header' => 'Trader',
					'type' => 'raw', 
				)
				
			);

			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatUserName( $data ) {
			return CHtml::link( CHtml::encode($data->showName), $data->getProfileURL() );
		}
	}

?>