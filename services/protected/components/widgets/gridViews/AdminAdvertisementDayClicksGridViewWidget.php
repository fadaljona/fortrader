<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminAdvertisementDayClicksGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminAdvertisementDayClicksGridView';
		public $class = 'iGridView i05 i06';
		public $itemsCssClass = "dataTable items";
		public $sortDayField;
		public $sortDayType;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view",
			);
			$this->setId( $this->w );
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortDayField ) {
				if( $this->sortDayType == 'ASC' ) {
					return 'sorting_day_asc';
				}
				else{
					return 'sorting_day_desc';
				}
			}
			else{
				return 'sorting_day';
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $this->grid->formatDate( $data->clickDate ) ', 
					'header' => 'Date', 
					'headerHtmlOptions' => Array( 
						'class' => 'c02 '.$this->getSorting( 'clickDate' ),
						'sortDayField' => 'clickDate' 
					)
				),
				Array( 
					'name' => 'count',
					'header' => 'Clicks', 
					'headerHtmlOptions' => Array( 
						'class' => 'c02 '.$this->getSorting( 'count' ),
						'sortDayField' => 'count' 
					)
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$date = date( "d.m.Y", $time );
			return $date;
		}
	}

?>