<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class CategoryRatingGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wCategoryRatingGridView';
		public $class = 'iGridVie';
		public $rowHtmlOptionsExpression = ' Array( "data-id" => $data->categoryRatingPost->ID, "class" => "row-id" . $data->categoryRatingPost->ID, "itemprop" => "isBasedOn", "itemscope" => " ", "itemtype" => "http://schema.org/CreativeWork" ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "wCategoryRatingGridViewTable";
		public $sortField;
		public $sortType;
		public $ratingCatId;
		public $catsCount;
		
		function init() {
			
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='2' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array();
			$columns[] = Array( 
					'value' => ' $this->grid->formatTitle( $data ) ', 
					'header' => '<span>' .  Yii::t( $this->NSi18n, 'Title' ) . ' <span class="table_sort"></span></span>' , 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'title' ),
						'data-sortField' => 'title' 
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_1 ',
					)
				);
		/*	if( $this->catsCount > 1 ){
				$columns[] = Array( 
					'value' => '$this->grid->ratingCatId != $data->termTaxonomy->term->term_id ? $data->termTaxonomy->term->name : "" ', 
					'header' => '<span>' . Yii::t( $this->NSi18n, 'Type' ) . ' </span>' , 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'type' ),
						'data-sortField' => 'type' 
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_2 ',
						'data-title' => Yii::t( $this->NSi18n, 'Type' )
					)
				);
			}*/
			
			$columns[] = Array( 
					'value' => ' $data->categoryRatingPost->postsStats->postViews ', 
					'header' => '<span>' . Yii::t( $this->NSi18n, 'Views' ) . ' <span class="table_sort"></span></span>' , 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'views' ),
						'data-sortField' => 'views' 
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_2 ',
						'data-title' => Yii::t( $this->NSi18n, 'Views' )
					)
				);
			$columns[] = Array( 
					'value' => ' $data->categoryRatingPost->postsStats->commentCount ', 
					'header' => '<span>' . Yii::t( $this->NSi18n, 'Reviews' ) . ' <span class="table_sort"></span></span>' , 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'reviews' ),
						'data-sortField' => 'reviews' 
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_2 ',
						'data-title' => Yii::t( $this->NSi18n, 'Reviews' )
					)
				);
		/*	$columns[] = Array( 
					'value' => ' $data->categoryRatingPost->postsStats->downloadsFromContent ', 
					'header' => '<span>' . Yii::t( $this->NSi18n, 'Downloads' ) . ' <span class="table_sort"></span></span>' , 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'downloads' ),
						'data-sortField' => 'downloads' 
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_2 ',
						'data-title' => Yii::t( $this->NSi18n, 'Downloads' )
					)
				);*/
			$columns[] = Array( 
					'value' => ' $this->grid->formatRating( $data ) ', 
					'header' => '<span>' . Yii::t( $this->NSi18n, 'Vote' ) . '</span>' , 
					'type' => 'raw',
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'vote' ),
						'data-sortField' => 'vote' 
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_2 ',
						'data-title' => Yii::t( $this->NSi18n, 'vote' ),
						'itemprop' => 'aggregateRating', 
						'itemscope' => ' ', 
						'itemtype' => 'http://schema.org/AggregateRating'
					)
				);
				$columns[] = Array( 
					'value' => ' $data->categoryRatingPost->stats ? $data->categoryRatingPost->stats->average : 0 ', 
					'header' => '<span>' . Yii::t( $this->NSi18n, 'Rating' ) . ' <span class="table_sort"></span></span>' , 
					'type' => 'raw',
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'rating' ),
						'data-sortField' => 'rating' 
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_2 ',
						'data-title' => Yii::t( $this->NSi18n, 'Rating' ),
					)
				);
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatTitle( $data ){
			return CHtml::tag(
				'span', 
				array('class' => 'p_rel blue_color'), 
				CHtml::link(
					$data->categoryRatingPost->titleForList,
					$data->categoryRatingPost->postUrl,
					array( 'itemprop' => 'url' )
				)
			);
		}
		function formatRating( $data ){
			$ratingVal = $data->categoryRatingPost->stats ? $data->categoryRatingPost->stats->average : 0;
			$votesCount = $data->categoryRatingPost->stats ? $data->categoryRatingPost->stats->votesCount : 0;
			ob_start();
			$this->widget( 'widgets.WebRatingWidget', Array(
				'ns' => "row-id" . $data->categoryRatingPost->ID,
				'currentValue' => $ratingVal,
				'maxValue' => 10,
				'returnNs' => true,
				'onChangeValue' => "nsActionView.wCategoryRatingWithFilterWidget.onChangeMark",
				'readOnly' => Yii::App()->user->isGuest ? true : false,
				'wrapperStyle' => 'display:inline-block !important;',
			));
			echo '<meta itemprop="ratingCount" content="'.$votesCount.'" />
			<meta itemprop="bestRating" content="10" />
			<meta itemprop="worstRating" content="0" />
			<meta itemprop="ratingValue" content="'.$ratingVal.'" />';
			
			return ob_get_clean();
		}
		

	}

?>