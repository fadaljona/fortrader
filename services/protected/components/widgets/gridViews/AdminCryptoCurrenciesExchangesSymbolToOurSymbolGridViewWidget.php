<?php
    
Yii::import('widgets.gridViews.base.GridViewWidgetBase');

final class AdminCryptoCurrenciesExchangesSymbolToOurSymbolGridViewWidget extends GridViewWidgetBase
{
    public $w = 'wAdminCommonGridView';
    public $class = 'iGridView i02';
    public $rowHtmlOptionsExpression = ' array( "data-idObj" => $data->id ) ';
    
    public function init()
    {
        $this->htmlOptions = array(
            'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
        );
        parent::init();
    }
    protected function getColumns()
    {
        $columns = array(
            array('name' => 'symbol'),
            array(
                'header' => 'Our symbol',
                'value' => '$data->ourSymbol ? $data->ourSymbol->cryptoCurrencyTitle : ""'
            ),
        );
        foreach ($columns as &$column) {
            if (isset($column[ 'header' ])) {
                $column[ 'header' ] = Yii::t($this->NSi18n, $column[ 'header' ]); 
            }
        }
        unset($column);
        return $columns;
    }
}
