<?
	$types = Array(
		'high' => 'High serious',
		'medium' => 'Medium serious',
		'low' => 'Low serious',
		'not' => 'Not serious',
	);
?>
<div class="position-relative">
	<div class="iDiv i65" data-toggle="dropdown">
		<?=Yii::t( '*', 'Filter' )?>
		<i></i>
	</div>
	
	<div class="dropdown-menu iDiv i66 iKeepOpen">
		<div class="text-center">
			<span class="iSpan i23"><?=Yii::t( '*', 'Events' )?>:</span>
		</div>
		<div class="iDiv i67"></div>
		
		<?foreach( $types as $key=>$title ){?>
			<div class="iDiv i68">
				<label>
					<?=CHtml::checkBox( "type_{$key}", false, Array( 'class' => "{$this->ins} wFilterType", 'value' => $key ))?>
					<span class="lbl iSpan i22_<?=$key?>">
						<?=Yii::t( '*', $title )?>
					</span>
				</label>
			</div>
		<?}?>
		<div class="iDiv i67"></div>
		
		<div class="text-center">
			<button class="iBtn i12 <?=$this->ins?> wSaveFilterTypes"><?=Yii::t( '*', "Save" )?></button>
		</div>
	</div>
</div>
