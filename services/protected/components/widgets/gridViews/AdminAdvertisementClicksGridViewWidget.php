<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminAdvertisementClicksGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminAdvertisementClicksGridView';
		public $class = 'iGridView i05 i06';
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view",
			);
			$this->setId( $this->w );
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $this->grid->formatDate( $data->createdDT ) ', 
					'header' => 'Date', 
					'headerHtmlOptions' => Array( 
						'class' => 'c01 '.$this->getSorting( 'createdDT' ),
						'sortField' => 'createdDT' 
					)
				),
				Array( 
					'name' => 'ip',
					'header' => 'IP', 
					'class' => 'dataColumns.ClickIPDataColumn',
					'headerHtmlOptions' => Array( 
						'class' => 'c02 '.$this->getSorting( 'ip' ),
						'sortField' => 'ip' 
					)
				),
				Array( 
					'name' => 'agent',
					'header' => 'Agent', 
					'class' => 'dataColumns.ClickAgentDataColumn',
					'headerHtmlOptions' => Array( 
						'class' => 'c03',
					)
				)
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$date = date( "d.m.Y H:i", $time );
			return $date;
		}
		function formatAgent( $agent ) {
			return CommonLib::shorten( $agent, 50 );
		}
	}

?>