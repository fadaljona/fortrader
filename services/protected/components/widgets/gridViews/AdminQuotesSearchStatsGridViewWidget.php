<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminQuotesSearchStatsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminQuotesSearchStatsGridView';
		public $class = 'iGridView i01';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'keyword', 'header' => 'Keyword', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'count', 'header' => 'Count', 'headerHtmlOptions' => Array( 'class' => '' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( "quotesWidget",$column[ 'header' ]); unset( $column );
			return $columns;
		}
	}
