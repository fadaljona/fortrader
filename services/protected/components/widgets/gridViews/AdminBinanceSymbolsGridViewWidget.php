<?php
Yii::import('widgets.gridViews.base.GridViewWidgetBase');

final class AdminBinanceSymbolsGridViewWidget extends GridViewWidgetBase
{
    public $w = '';
    public $class = 'iGridView';
    public $showTableOnEmpty=true;
    public $rowHtmlOptionsExpression = '';
    
    public function init()
    {
        $this->htmlOptions = array(
            'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
        );
        parent::init();
    }

    protected function getColumns() {
        $columns = array(
            array('name' => 'symbol'),
            array('name' => 'baseAsset', 'header' => 'From symbol'),
            array('name' => 'quoteAsset', 'header' => 'To symbol'),
        );
        foreach ($columns as &$column) {
            if (isset($column[ 'header' ])) {
                $column[ 'header' ] = Yii::t("quotesWidget", $column[ 'header' ]);
            }
        }
        unset($column);
        return $columns;
    }
}
