<tfoot>
	<tr>
		<td colspan="10" style="font-weight:bold">
			<?=Yii::t( $this->NSi18n, "Total" )?>: <?=CommonLib::numberFormat( $this->member->countOrders )?>
			&nbsp;&nbsp;&nbsp;<?=Yii::t( $this->NSi18n, "Filled" )?>: <?=CommonLib::numberFormat( $this->member->countFilledOrders )?>
			&nbsp;&nbsp;&nbsp;<?=Yii::t( $this->NSi18n, "Canceled" )?>: <?=CommonLib::numberFormat( $this->member->countCanceledOrders )?>
		</td>
	</tr>
</tfoot>