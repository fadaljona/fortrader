<span class="ex_information">
    <svg class="svg_icon svg_inform">
        <use xlink:href="#icon-info"></use>
    </svg>
    <div class="ex_tooltip-info">
        <div class="ex-info__header clearfix">
            <div class="ex-info__logo">
            <?php
            if ($exchanger->srcLogo) {
                echo CHtml::image($exchanger->srcLogo, $exchanger->title);
            }
            ?>
            </div>
            <div class="ex-info__name"><?=$exchanger->title?></div>
            <div class="ex-info__rate">
            <?php
            $this->widget('widgets.WebRatingWidget', array(
                'ns' => "data-exchanger" . $exchanger->id,
                'currentValue' => $exchanger->exchangerVoteStats ? $exchanger->exchangerVoteStats->average : 0,
                'maxValue' => 10,
                'starWidth' => 15,
                'returnNs' => true,
                'onChangeValue' => "nsActionView.wChooseExchangerByDirectionWidget.onChangeMark",
                'readOnly' => Yii::App()->user->isGuest ? true : false,
                'wrapperStyle' => 'display:inline-block !important;',
            ));
            ?>
            </div>
        </div>
        <div class="ex-info__body">
            <?php /*<div class="ex-info__item clearfix">
                <div>WebMoney BL:</div>
                <div>-</div>
            </div>
            <div class="ex-info__item clearfix">
                <div>Perfect Money TS:</div>
                <div>-</div>
            </div>*/?>
            <?php 
            if (!Yii::App()->user->isGuest) {
                $voteModel = PaymentExchangerItemMarkModel::model()->findByAttributes(array(
                    'idExchanger' => $exchanger->id,
                    'idUser' => Yii::app()->user->id,
                ));
            ?>
            <div class="ex-info__item clearfix">
                <div><?=Yii::t($this->NSi18n, 'Your vote')?>:</div>
                <div class="exchangerVoteCount"><?=$voteModel ? $voteModel->value : '-';?></div>
            </div>
            <?php } ?>
            <?php /*
            <div class="ex-info__item clearfix">
                <div><?=Yii::t($this->NSi18n, 'Amount of reserves')?>:</div>
                <div>$87 587</div>
            </div>*/?>
            <?php if ($exchanger->dob) {?>
            <div class="ex-info__item clearfix">
                <div><?=Yii::t($this->NSi18n, 'Age')?>:</div>
                <div><?=Yii::app()->format->timeago($exchanger->dob);?></div>
            </div>
            <?php }?>
            <?php if ($exchanger->country) {?>
            <div class="ex-info__item clearfix">
                <div><?=Yii::t($this->NSi18n, 'Country')?>:</div>
                <div><?=Yii::t('*', $exchanger->country->name)?></div>
            </div>
            <?php }?>
        </div>
    </div>
</span>
<span><?=$exchanger->title?></span>