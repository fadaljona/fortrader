<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class InterestRatesGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wInterestRatesGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "data-idMember" => $data->id ) ';
		public $selectableRows = 2;
		function init() {

			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='2' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		protected function getColumns() {
			$columns = Array(
				array(
					'header' => 'Country', 
					'value' => ' $data->bank->country->getImgFlag("shiny", 48) ',
					'type' => 'raw',
					'htmlOptions' => Array( 'class' => 'table_rating_col_1', 'data-title' => Yii::t( $this->NSi18n, 'Country' ) )
				),
				array(
					'header' => 'Central Bank', 
					'type' => 'raw',
					'value' => ' $this->grid->formatBank( $data->bank ) ', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Central Bank' ) )
				),
				array(
					'header' => 'Key interest rate', 
					'type' => 'raw',
					'value' => ' "<span>" . $data->title . "</span>" ',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Key interest rate' ) )
				),
				array(
					'header' => 'True value', 
					'type' => 'raw',
					'value' => ' "<span>" . $data->lastValue->value . "%</span>" ',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'True value' ) )
				),
				array(
					'header' => 'The last meeting', 
					'type' => 'raw',
					'value' => ' $this->grid->lastMod( $data->lastValue ) ', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'The last meeting' ) )
				),
				array(
					'header' => 'The next meeting', 
					'type' => 'raw',
					'value' => ' $this->grid->futData( $data->lastValue ) ',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'The next meeting' ) )
				),
				
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}

		function futData( $val ){
			if( $val->futDate ){
				return "<span>" . date("d.m.Y", strtotime($val->futDate)) . "</span>" ;
			}
			return "";
		}
		function lastMod( $val ){
			$outStr = date('d.m.Y', strtotime($val->modDate));
			if( $val->difValueFromPrev ){
				if( $val->difValueFromPrev > 0 ){
					$color = 'green_color';
				}else{
					$color = 'red_color';
				}
				$outStr .= ' <br><span class="'.$color.'">('.$val->difValueFromPrev.'%)</span>';
			}
			if( $val->link ){
				return CHtml::link( '<span>' . $outStr . '</span>', $val->link );
			}else{
				return '<span>' . $outStr . '</span>';
			}
			
		}
		function formatBank( $bank ){
			$outStr = '';
			if( $bank->shortTitle ){
				$outStr = $bank->shortTitle . '<br>';
			}
			return '<span class="blue_color">'.$outStr.' '.$bank->title.'</span>';
		}
	}

?>