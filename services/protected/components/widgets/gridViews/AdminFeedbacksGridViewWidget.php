<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminFeedbacksGridViewWidget extends GridViewWidgetBase {
		public $w = 'wFeedbacksGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "idFeedback" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $this->grid->formatDate( $data->createdDT ) ', 'header' => 'Date', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'value' => ' $data->type->name ', 'header' => 'Type', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'value' => ' $data->user ', 'header' => 'User', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'value' => ' $data->email ', 'header' => 'EMail', 'headerHtmlOptions' => Array( 'class' => '' )),
				//Array( 'value' => ' CommonLib::shorten( $data->message, 200 )', 'header' => 'Message', 'headerHtmlOptions' => Array( 'class' => '' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $DT ) {
			return date( "Y.m.d", strtotime( $DT ));
		}
	}

?>