<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminPostsStatsWpSearchStatsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminPostsStatsWpSearchStatsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = '  ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				array( 'name' => 'keyword' ),
				array( 'name' => 'count' ),
				array( 'name' => 'lastDate' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>