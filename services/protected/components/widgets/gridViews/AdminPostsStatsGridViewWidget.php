<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminPostsStatsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminPostsStatsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "postId" => $data->postId ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'postId', 'value' => ' $this->grid->formatHitLink( $data ) ', 'type' => 'raw', 'header' => 'ID' ),
				Array( 'value' => ' $this->grid->formatUrl( $data ) ', 'type' => 'raw', 'header' => 'URL' ),
				Array( 'name' => 'title', 'header' => 'Title' ),
				Array( 'name' => 'commentCount', 'header' => 'C', 'filter'=>false ),
				Array( 'name' => 'postViews', 'header' => 'V', 'filter'=>false ),
				Array( 'name' => 'interest', 'header' => 'I', 'filter'=>false ),
				Array( 'name' => 'fbLikes', 'header' => 'FBL', 'filter'=>false ),
				Array( 'name' => 'fbShare', 'header' => 'FBS', 'filter'=>false ),
				Array( 'name' => 'vkShare', 'header' => 'VK', 'filter'=>false ),
				Array( 'name' => 'tw', 'header' => 'TW', 'filter'=>false ),
				Array( 'name' => 'gp', 'header' => 'GP', 'filter'=>false ),
				Array( 
					'value' => ' $this->grid->formatDate( $data ) ', 
					'header' => 'Date',
					'type' => 'raw',
					'sortable' => true,
					'name' => 'postDate',				
					'filter' => false,
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $data ) {
			return date( 'Y-m-d', strtotime( $data->postDate ) );
		}
		function formatUrl( $data ) {
			return "<a href='{$data->url}' target='_blank'>{$data->url}</a>";
		}
		function formatHitLink( $data ) {
			return "<a href='".Yii::App()->createURL( 'admin/postsStats/hitDetails', Array( 'id' => $data->postId ))."' target='_blank'>{$data->postId}</a>";
		}
	}

?>