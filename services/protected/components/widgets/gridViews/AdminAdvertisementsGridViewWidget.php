<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminAdvertisementsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdvertisementsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idAdvertisement" => $data->id ) ';
		public $mode = 'User';
		public $typeAd;
		public $forStats = false;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array();
			$columns[] = Array( 
				'name' => 'header', 
				'header' => 'Header',
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'header' ), 
					'sortField' => 'header' 
				)
			);
			if( $this->typeAd == 'Offer' ) {
				$columns[] = Array( 
					'value' => ' CommonLib::numberFormat( $data->countLikes ) ', 
					'header' => 'Likes',
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'likes' ), 
						'sortField' => 'likes' 
					)
				);
			}
			$columns[] = Array( 
				'value' => ' CommonLib::numberFormat( $data->countShows ) ', 
				'header' => 'Shows',
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'countShows' ), 
					'sortField' => 'countShows' 
				)
			);
			$columns[] = Array( 
				'value' => ' CommonLib::numberFormat( $data->countClicks ) ', 
				'header' => 'Clicks',
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'countClicks' ), 
					'sortField' => 'countClicks' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatCTR( $data ) ', 
				'header' => 'CTR',
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'CTR' ), 
					'sortField' => 'CTR' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->t( $data->status )', 
				'header' => 'Status',
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'status' ), 
					'sortField' => 'status' 
				)
			);
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatCodeLink( $zone ) {
			$link = CHtml::link( Yii::t( $this->NSi18n, "Get code" ), '#', Array( 'class' => 'iCodeLink', 'idZone' => $zone->id ));
			return $link;
		}
		function formatCTR( $data ) {
			if( $data->CTR === null ) return '';
			$value = CommonLib::numberFormat( $data->CTR );
			$value .= '%';
			return $value;
		}
	}

?>