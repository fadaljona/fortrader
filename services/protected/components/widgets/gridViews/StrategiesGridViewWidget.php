<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	//Yii::import( 'gridColumns.EAActionsGridColumn' );

	final class StrategiesGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wStrategiesGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idStrategy" => $data->id ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				//return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array();
			$columns[] = Array( 
				'class' => 'ACECheckBoxGridColumn',  
				'headerHtmlOptions' => Array( 
					'class' => 'checkbox-column hidden-phone' 
				),  
				'htmlOptions' => Array( 
					'class' => 'checkbox-column hidden-phone' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatName( $data ) ', 
				'header' => 'Title', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c01 '.$this->getSorting( 'name' ),
					//'sortField' => 'place' 
				), 
				'htmlOptions' => Array( 
					'class' => 'c01' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatType( $data->type ) ', 
				'header' => 'Type', 
				'headerHtmlOptions' => Array( 
					'class' => 'c01 '.$this->getSorting( 'type' ),
					//'sortField' => 'place' 
				), 
				'htmlOptions' => Array( 
					'class' => 'c01' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatIndicators( $data->indicators ) ', 
				'header' => 'Indicators', 
				'headerHtmlOptions' => Array( 
					'class' => 'c01 ',
				), 
				'htmlOptions' => Array( 
					'class' => 'c01' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatEA( $data ) ', 
				'header' => 'EA', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c01 ',
				), 
				'htmlOptions' => Array( 
					'class' => 'c01' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatInstruments( $data->instruments ) ', 
				'header' => 'Instruments', 
				'headerHtmlOptions' => Array( 
					'class' => 'c01 ',
				), 
				'htmlOptions' => Array( 
					'class' => 'c01' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatTimeframes( $data->timeframes ) ', 
				'header' => 'Timeframes', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c01 ',
				), 
				'htmlOptions' => Array( 
					'class' => 'c01' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatComments( $data ) ', 
				'header' => 'Comments', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c01 '.$this->getSorting( 'comments' ),
				), 
				'htmlOptions' => Array( 
					'class' => 'c01' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatPrice( $data->price ) ', 
				'header' => 'License', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c01 '.$this->getSorting( 'price' ),
				), 
				'htmlOptions' => Array( 
					'class' => 'c01' 
				)
			);
						
			//if( Yii::App()->user->checkAccess( 'eaControl' )) {
			//	$columns[] = Array( 'class' => 'EAActionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			//}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatName( $data ) {
			if( strlen( $data->desc )) {
				return CHtml::link( CHtml::encode( $data->name ), CommonLib::getURL($data->desc), Array( 'target' => '_blank' ));
			}
			else{
				return CHtml::encode( $data->name );
			}
		}
		function formatType( $type ) {
			return Yii::t( 'models/strategy', $type );
		}
		function formatIndicators( $indicators ) {
			require dirname( __FILE__ ).'/strategies/indicators.php';
			return "";
		}
		function formatEA( $data ) {
			if( $data->idEA ) {
				$out = CHtml::tag( 'i', Array( 'class' => "iI i41" ), '', true );
				$out .= Yii::t( 'v01', "Yes" );
			}
			else{
				$out = CHtml::tag( 'i', Array( 'class' => "iI i42" ), '', true );
				$out .= Yii::t( 'v01', "No" );
			}
			return $out;
		}
		function formatInstruments( $instruments ) {
			require dirname( __FILE__ ).'/strategies/instruments.php';
			return "";
		}
		function formatTimeframes( $timeframes ) {
			$timeframes = strlen( $timeframes ) ? explode( ",", $timeframes ) : Array();
			require dirname( __FILE__ ).'/strategies/timeframes.php';
			return "";
		}
		function formatComments( $data ) {
			$inner = CommonLib::numberFormat( 0 );
			$inner .= CHtml::tag( 'i', Array( 'class' => "iI i19" ), '', true );
			$out = CHtml::tag( 'a', Array( 'class' => 'iA i14', 'href' => "#" ), $inner, true );
			return $out;
		}
		function formatPrice( $price ) {
			if( $price > 0 ) {
				$out = "$";
				$out .= CommonLib::numberFormat( $price );
			}
			else{
				$inner = "FREE";
				$out = CHtml::tag( 'span', Array( 'class' => "iSpan i21" ), $inner, true );
			}
			return $out;
		}
	}

?>