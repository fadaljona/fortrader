<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class ForeignContestsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wForeignContestsGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "data-idContest" => $data->id ) ';
		public $returnOnlyTrs = false;
		function init() {

			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		public function renderTableBody(){
			$data=$this->dataProvider->getData();
			$n=count($data);
			if( !$this->returnOnlyTrs )echo "<tbody>\n";

			if($n>0)
			{
				for($row=0;$row<$n;++$row)
					$this->renderTableRow($row);
			}
			else
			{
				echo '<tr><td colspan="'.count($this->columns).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
			if( !$this->returnOnlyTrs )echo "</tbody>\n";
		}
		public function renderTableHeader(){
			if(!$this->hideHeader){
				echo "<thead>\n";

				if($this->filterPosition===self::FILTER_POS_HEADER) $this->renderFilter();
				
				echo '<tr class="competition_tbl_head">
							<th colspan="5" class="clearfix">' . Yii::t( $this->NSi18n, 'Table contests friendly resources' ) .'<i class="fa fa-thumbs-o-up" aria-hidden="true"></i></th>
				</tr>';

				echo "<tr>\n";
				foreach($this->columns as $column) $column->renderHeaderCell();
				echo "</tr>\n";

				if($this->filterPosition===self::FILTER_POS_BODY) $this->renderFilter();

				echo "</thead>\n";
			}elseif($this->filter!==null && ($this->filterPosition===self::FILTER_POS_HEADER || $this->filterPosition===self::FILTER_POS_BODY)){
				echo "<thead>\n";
				$this->renderFilter();
				echo "</thead>\n";
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $data->title ', 
					'header' => 'Contest title', 
					'htmlOptions' => Array( 'class' => 'col_name', 'data-title' => Yii::t( $this->NSi18n, 'Contest title' ) )
				),
				Array( 
					'value' => ' "$" . $data->prize ', 
					'header' => 'Prize amount', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Prize amount' ) )
				),
				Array( 
					'value' => ' Yii::app()->dateFormatter->format( "d MMMM yyyy", strtotime($data->begin) ) ', 
					'header' => 'Begin', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Begin' ) )
				),
				Array( 
					'value' => ' $data->sponsor ', 
					'header' => 'Sponsor', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Sponsor' ) )
				),
				Array( 
					'value' => ' $this->grid->formatLink( $data->link ) ',
					'header' => 'Information', 
					'type' => 'raw',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Information' ) )
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatLink( $link ) {
			return CHtml::link( CHtml::encode( Yii::t( $this->NSi18n, "View" ) ), $link, array( 'target' => '_blank' ) );
		}
	}
?>