<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );

	final class ContestMemberTradesSymbolsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMemberTradesSymbolsGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "data-idTrade" => $data->id ) ';
		public $selectableRows = 2;
		public $member;
		public $byDirection;
		public $sortField;
		public $sortType;
		
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
				'id' => $this->w,
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array();
			
			$columns[] = Array( 
				'value' => ' $data->SYMBOL ', 
				'header' => '<span>' .  Yii::t( $this->NSi18n, 'Symbol' ) . ' <span class="table_sort"></span></span>', 
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'symbol' ),
					'data-sortField' => 'symbol' 
				),
				'htmlOptions' => Array( 
					'class' => 'height', 
					'data-title' => $this->t('Symbol') 
				)
			);
			$columns[] = Array( 
				'value' => ' $data->DEALS_COUNT ', 
				'header' => '<span>' .  Yii::t( $this->NSi18n, 'Deals count' ) . ' <span class="table_sort"></span></span>', 
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'dealsCount' ),
					'data-sortField' => 'dealsCount' 
				),
				'htmlOptions' => Array( 
					'class' => 'height', 
					'data-title' => $this->t('Deals count') 
				)
			);
			if( $this->byDirection ){
				$columns[] = Array( 
					'value' => ' $this->grid->getDealTypeTitle( $data ) ', 
					'header' => '<span>' .  Yii::t( $this->NSi18n, 'Action' ) . ' <span class="table_sort"></span></span>', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'action' ),
						'data-sortField' => 'action' 
					),
					'htmlOptions' => Array( 
						'data-title' => $this->t('Action') 
					)
				);
			}
			$columns[] = Array( 
				'value' => ' number_format( abs($data->SUM_DEAL_VOLUME), 2, ".", "" ) ', 
				'header' => '<span>' .  Yii::t( $this->NSi18n, 'Volume' ) . ' <span class="table_sort"></span></span>', 
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'volume' ),
					'data-sortField' => 'volume' 
				),
				'htmlOptions' => Array( 
					'data-title' => $this->t('Volume') 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatFloat( $data->SUM_DEAL_SWAP ) ', 
				'type' => 'raw', 
				'header' => '<span>' .  Yii::t( $this->NSi18n, 'Swap' ) . ' <span class="table_sort"></span></span>', 
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'swap' ),
					'data-sortField' => 'swap' 
				),
				'htmlOptions' => Array( 
					'data-title' => $this->t('Swap') 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatFloat( $data->SUM_DEAL_PROFIT ) ', 
				'header' => '<span>' .  Yii::t( $this->NSi18n, 'Profit' ) . ' <span class="table_sort"></span></span>', 
				'headerHtmlOptions' => Array( 
					'class' => $this->getSorting( 'profit' ),
					'data-sortField' => 'profit' 
				),
				'htmlOptions' => Array( 
					'data-title' => $this->t('Profit') 
				)
			);
			
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}

		function formatFloat( $val ) {
			return number_format( $val, CommonLib::getDecimalPlaces( $val ), ',', ' ' );
		}
		function getDealTypeTitle($data){
			if( $this->byDirection ) return $data->getDealTypeTitle();
			return '';
		}
	}

?>