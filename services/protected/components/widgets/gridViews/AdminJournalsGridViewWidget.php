<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminJournalsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wJournalsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idJournal" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'number', 'header' => 'Number' ),
				Array( 'name' => 'name', 'header' => 'Name' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>