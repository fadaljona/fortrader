<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );
	

	final class AdminManageTextContentGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminManageTextContentGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-id" => $data->id ) ';
		public $itemsCssClass = '';
		public $langs;
		public $modelFormName;
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'header' => 'Forms', 'value' => ' $this->grid->formatForm( $data ) ', 'type' => 'raw' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatForm( $data ) {
			ob_start();
				require dirname( __FILE__ ).'/adminManageTextContent/form.php';
			return ob_get_clean();
		}
	}

?>
