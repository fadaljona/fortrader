<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminUserActivitiesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wUserActivitiesGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idActivity" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->user->user_login ', 'header' => 'User' ),
				Array( 'value' => ' $this->grid->formatMessage( $data ) ', 'header' => 'Message', 'type' => 'raw' ),
				Array( 'value' => ' CommonLib::numberFormat( $data->count ) ', 'header' => 'Count' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatMessage( $data ) {
			$out = '';
			$out .= $data->getIcon();
			$out .= ( strlen($out) ? ' ' : '' ).$data->getHTMLMessage();
			return $out;
		}
	}

?>