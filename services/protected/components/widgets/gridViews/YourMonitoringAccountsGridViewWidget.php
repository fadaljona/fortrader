<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class YourMonitoringAccountsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMembersGridView';
		public $class = 'iGridView i05 i06';
		public $rowHtmlOptionsExpression = ' Array( "data-id" => $data->id, "data-server" => $data->idServer, "data-account" => $data->accountNumber, "data-accountTitle" => $data->accountTitle ) ';
		public $selectableRows = 0;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		public $tabType = false;
		public $type = 'ft';
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='2' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}


		protected function getColumns() {
			$columns = Array();
			
			if( $this->type != 'frameForForum' ){
				$columns[] = array( 
					'header' => 'Account number',
					'type' => 'raw',
					'value' => ' $this->grid->formatAccountLink( $data ) ', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Account number' ) )
				);
			}else{
				$columns[] = array( 
					'header' => Yii::t( $this->NSi18n, 'Click to insert in thread' ),
					'type' => 'raw',
					'value' => ' $this->grid->formatAccountLink( $data ) ', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Account number' ), 'class' => 'linkForForumFrame' )
				);
			}
			$columns[] = array( 
				'header' => 'Server',
				'value' => ' $data->server->forReports ? Yii::t( $this->grid->NSi18n, "Report Metatrader4" ) : $data->server->name ', 
				'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Server' ) )
			);
			
			if( $this->type != 'frameForForum' ){
				$columns[] = array( 
					'header' => 'Date added',
					'value' => ' $data->createdDT ', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Date added' ) )
				);
				$columns[] = array( 
					'header' => 'Status',
					'value' => ' $this->grid->formatStatus( $data ) ', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Status' ) )
				);
				$columns[] = array( 
					'header' => 'Edit',
					'type' => 'raw',
					'value' => ' $this->grid->formatEdit( $data ) ', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Edit' ) )
				);
			}
			

			
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	
		
		function formatAccountLink( $data ) {
	
			return CHtml::link($data->accName, array('monitoring/single', 'id' => $data->id), array('class' => 'blue_color') );
		}
		function formatStatus( $data ) {
			if( !$data->mt5AccountInfo ) return Yii::t('*', 'Pending');
			if( !$data->mt5AccountInfo->last_error_code )   return Yii::t('*', 'Ok');
			if( $data->mt5AccountInfo->error && $data->mt5AccountInfo->error->title ) return $data->mt5AccountInfo->error->title;
			if( !$data->stats->last_error_code )  return Yii::t('*', 'Ok');
			return Yii::t('*', 'Please contact the admin');
		}
		function formatEdit( $data ){
			if( $data->server->forReports ){
				return '<i style="cursor: pointer;margin-top:2px;" aria-hidden="true" title="'.Yii::t('*', 'Delete').'" class="fa fa-times"></i>';
			}else{
				return '<i style="cursor: pointer;margin-top:3px;" title="'.Yii::t('*', 'Edit').'" aria-hidden="true" class="fa fa-pencil-square-o"></i> | <i style="cursor: pointer;margin-top:2px;" aria-hidden="true" title="'.Yii::t('*', 'Delete').'" class="fa fa-times"></i>';
			}
		}
	}

?>