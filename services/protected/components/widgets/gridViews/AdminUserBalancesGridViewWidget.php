<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminUserBalancesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wUserBalancesGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idUser" => $data->id ) ';
		public $mode = 'User';
		function init() {
			if( $this->mode == 'User' ) {
				$this->class .= ' i03';
			}
			else{
				$this->class .= ' i02';
			}
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array();
			$columns[] = Array( 
				'name' => 'user_login',
				'value' => 'CommonLib::shorten( $data->user_login, 25 ) ',
				'header' => 'Login', 
				'headerHtmlOptions' => Array( 'class' => 'c01' )
			);
			if( $this->mode == 'Admin' ) {
				$columns[] = Array( 
					'name' => 'group',
					'class' => 'dataColumns.UserGroupsDataColumn', 
					'header' => 'Group',
				);
			}
			$columns[] = Array( 
				'value' => ' CommonLib::numberFormat( $data->balance ) ', 
				'header' => 'Balance', 
				'headerHtmlOptions' => Array( 'class' => 'c02' )
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>