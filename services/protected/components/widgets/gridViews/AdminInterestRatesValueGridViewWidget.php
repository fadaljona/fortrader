<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminInterestRatesValueGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminCommonGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'modDate', 'header' => 'Modification date' ),
				Array( 'name' => 'value' ),
				Array( 'name' => 'futDate', 'header' => 'Future date' ),
				Array( 'header' => 'Link', 'value' => ' $data->link ' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>