<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class QuotesForecastsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wQuotesForecastsGridView tabl_quotes';
		public $class = 'iGridView';

		public $forStats = false;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value'=>'$this->grid->formatSymbolName( $data )',
					'header' => 'Symbol',
					'type' => 'raw', 
					'htmlOptions' => Array( 'class' => 'tabl_quotes_tipe' )
				),
				Array( 
					'value'=>'$data->startDate',
					'header' => 'Forecats Start Date',
					'htmlOptions' => Array( 'data-title' => Yii::t( '*', 'Start Date' ), 'class' => 'height80' ),
				),
				Array( 
					'value'=>'$data->endDate',
					'header' => 'Forecats End Date',
					'htmlOptions' => Array( 'data-title' => Yii::t( '*', 'End Date' ), 'class' => 'height80' ),
				),
				Array( 
					'value'=>'$data->value',
					'header' => 'Goal',
					'htmlOptions' => Array( 'data-title' => Yii::t( '*', 'Value' ) ),
				),
				Array( 
					'value'=>'$this->grid->formatStatus( $data->status )',
					'header' => 'Status',
					'type' => 'raw', 
					'htmlOptions' => Array( 'data-title' => Yii::t( '*', 'Status' )),
				),
				
			);

			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatSymbolName( $data ) {
			if( $data->symbol->nalias ){
				$name = $data->symbol->nalias;
			}else{
				$name = $data->symbol->name;
			}
			$descStr = '';
			if( $data->symbol->tooltip ) $descStr = CHtml::tag('span', array('class' => 'tooltip'), $data->symbol->tooltip);
			$link = CHtml::link(
				$name . $descStr, 
				array('quotesNew/single', 'slug' => strtolower($data->symbol->name) ),
				array( 'target' => '_blank' )
			);
			return CHtml::tag('span', array('class' => 'p_rel blue_color'), $link);
		}
		function formatStatus( $status ) {
			if( $status == 0 ) return Yii::t( '*', 'Not verified' );
			if( $status == 1 ) return CHtml::tag('span', array('class' => 'green_color'), Yii::t( '*', 'Confirmed' ));
			if( $status == -1 ) return CHtml::tag('span', array('class' => 'red_color'), Yii::t( '*', 'Not confirmed' ));
		}

	}

?>