<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );

	final class ContestMemberTradesGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMemberTradesGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "data-idTrade" => $data->id ) ';
		public $selectableRows = 2;
		public $member;
		public $sums;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
				'id' => $this->w,
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
				$this->renderSumm();
			}else
				$this->renderEmptyText();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->SYMBOL ', 'header' => 'Symbol', 'htmlOptions' => Array( 'class' => 'height', 'data-title' => $this->t('Symbol') )),
				Array( 'value' => ' $data->DEAL_TICKET ', 'header' => 'Order', 'htmlOptions' => Array( 'data-title' => $this->t('Order') )),
				Array( 'value' => ' $this->grid->formatTime( $data ) ', 'header' => 'Time', 'htmlOptions' => Array( 'data-title' => $this->t('Time') )),
				Array( 'value' => ' $data->getDealTypeTitle() ', 'header' => 'Action', 'htmlOptions' => Array( 'data-title' => $this->t('Action') )),
				Array( 'value' => ' $data->DEAL_VOLUME ', 'header' => 'Volume', 'htmlOptions' => Array( 'data-title' => $this->t('Volume') )),
				Array( 'value' => ' $data->DEAL_SL ', 'header' => 'S/L', 'htmlOptions' => Array( 'data-title' => $this->t('S/L') )),
				Array( 'value' => ' $data->DEAL_TP ', 'header' => 'T/P', 'htmlOptions' => Array( 'data-title' => $this->t('T/P') )),
				Array( 'value' => ' CommonLib::numberFormat( $data->DEAL_PRICE, 4 )', 'header' => 'Price', 'htmlOptions' => Array( 'data-title' => $this->t('Price') )),
				Array( 'value' => ' $this->grid->formatSwap( $data->DEAL_SWAP ) ', 'type' => 'raw', 'header' => 'Swap', 'htmlOptions' => Array( 'data-title' => $this->t('Swap') )),
				Array( 'value' => ' $data->DEAL_PROFIT ', 'header' => 'Profit', 'htmlOptions' => Array( 'data-title' => $this->t('Profit') )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatTime( $data ) {
			$DT = date( "Y.m.d H:i", $data->DEAL_TIME );
			return $DT;
		}
		function formatSwap( $swap ) {
			$class = $swap < 0 ? 'text-error' : '';
			$label = CHtml::tag( "span", Array( 'class' => $class ), $swap );
			return $label;
		}
		function renderSumm() {
			require dirname( __FILE__ ).'/contestMemberTrades/footer.php';
		}
	}

?>