<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminInformersStyleGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminCommonGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'id' ),
				Array( 'header' => 'Title', 'value' => ' $data->title ' ),
				Array( 'name' => 'viewFile' ),
				Array( 'header' => 'Preview', 'type' => 'raw', 'value' => ' CHtml::link("preview", Yii::app()->createUrl("informers/getInformer", array( "st" => $data->id )) ) '  ),
				Array( 'name' => 'oneColumn' ),
				Array( 'name' => 'hasDiffValue' ),
				Array( 'name' => 'defaultStyle' ),
				Array( 'name' => 'hidden' ),
				Array( 'name' => 'borderWidth' ),
				Array( 'name' => 'width' ),
				Array( 'name' => 'order' ),
				Array( 'header' => 'Category types', 'value' => ' $this->grid->formatCategoryTypes( $data->getStrCategoryTypes() ) ' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatCategoryTypes( $categoryTypes ) {
			if( !$categoryTypes ) return '';
			$categoryTypes = explode(',',$categoryTypes);
			$avCatsWithTranslate = InformersCategoryModel::getAvailableTypesWithTranslate();
			
			$outStr = '';
			$i=0;
			foreach( $categoryTypes as $type ){
				if( $i==0  ) $outStr .= $avCatsWithTranslate[$type];
				else $outStr .= ', ' . $avCatsWithTranslate[$type] ;
				$i=2;
			}
			return $outStr;
		}
	}

?>