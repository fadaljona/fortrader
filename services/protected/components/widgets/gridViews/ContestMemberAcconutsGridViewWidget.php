<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );


	final class ContestMemberAcconutsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMembersGridView';
		public $class = 'iGridView i05';
		public $rowCssClassExpression = ' ';
		public $rowHtmlOptionsExpression = ' Array( "data-idMember" => $data->id ) ';
		public $selectableRows = 1;
		public $itemsCssClass = "dataTable items";
	
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
	
		protected function getColumns() {
			$columns = Array();
		
			$columns[] = Array( 
				'value' => ' $data->contest->id ? CHtml::link( CHtml::encode( $data->contest->name ), $data->contest->singleURL ) : CHtml::link( Yii::t( $this->grid->NSi18n, "Monitoring" ), array("monitoring/index") ) ', 
				'header' => 'Contest/Monitoring', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col ' ), 
				'htmlOptions' => Array( 
					'class' => 'calendar_event',
					'data-text' => Yii::t($this->NSi18n, 'Contest/Monitoring')
				)
			);
			
			$columns[] = Array( 
				'value' => ' $data->contest->id ? CHtml::link( CHtml::encode( $data->accountNumber ), $data->singleURL ) : CHtml::link(CHtml::encode( $data->accountNumber ), array("monitoring/single", "id" => $data->id)) ', 
				'header' => 'Account', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col ' ),
				'htmlOptions' => Array( 
					'data-text' => Yii::t($this->NSi18n, 'Account')
				)
			);
			
			$columns[] = Array( 
				'value' => ' $this->grid->formatGain( $data ) ', 
				'header' => 'Gain', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col ' ),
				'htmlOptions' => Array( 
					'data-text' => Yii::t($this->NSi18n, 'Gain')
				)
			);
			
			$columns[] = Array( 
				'value' => ' $this->grid->formatStatus( $data ) ', 
				'header' => 'Status', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col ' ), 
				'htmlOptions' => Array( 
					'class' => 'box_label',
					'data-text' => Yii::t($this->NSi18n, 'Status')
				)
			);
			
			$columns[] = Array( 
				'value' => ' $this->grid->formatUpdate( $data ) ', 
				'header' => 'Updated', 
				'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col' ), 
				'htmlOptions' => Array( 
					'data-text' => Yii::t($this->NSi18n, 'Updated')
				)
			);
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatGain( $data ) {
			return $data->stats && $data->stats->gain !== null ? $this->formatPercent( $data->stats->gain ) : '';
		}
		function formatStatus( $data ) {
			$status = $data->status;
			
			$statsStatus = $data->stats->getStatus();
			if( $statsStatus ) $status = $statsStatus;
			
			$title = ucfirst( $status );
			$title = Yii::t( $this->NSi18n, $title );
			
			if( $status == 'participates' ){
				return CHtml::tag('div', array('class' => 'trade-account_status account-status_accept'), $title);
			}
			return CHtml::tag('div', array('class' => 'trade-account_status'), $title);
		}
		function formatUpdate( $data ) {
			$data->stats && $data->stats->trueDT && $this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->stats->trueDT, 'class' => "iBold" ));
		}
	}

?>