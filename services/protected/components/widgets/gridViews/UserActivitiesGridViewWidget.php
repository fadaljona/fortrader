<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );


	final class UserActivitiesGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wUserActivitiesGridView';
		public $class = '';
		public $rowCssClassExpression = '';
		public $rowHtmlOptionsExpression = ' Array( "data-id" => $data->id ) ';
		public $selectableRows = 1;
		public $itemsCssClass = "dataTable items";
	
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
	
		protected function getColumns() {
			$columns = Array();
			
			
			$columns[] = Array( 
				'value' => ' $data->message ', 
				'header' => 'Message', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col width80percent' ), 
				'htmlOptions' => Array( 
					'class' => 'calendar_event',
					'data-text' => Yii::t($this->NSi18n, 'Message')
				)
			);
			
			$columns[] = Array( 
				'value' => ' $this->grid->formatTime( $data->createdDT ) ', 
				'header' => 'Time', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col ' ), 
				'htmlOptions' => Array( 
					'class' => 'calendar_event',
					'data-text' => Yii::t($this->NSi18n, 'Message')
				)
			);
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatTime( $time ){
			$this->widget( "widgets.parts.TimeDiffWidget", Array( "time" => $time ));
		}
		
	}

?>