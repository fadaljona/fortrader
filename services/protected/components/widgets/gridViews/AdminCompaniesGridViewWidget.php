<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminCompaniesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wCompaniesGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idCompany" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Title' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>