<?php
    
Yii::import('widgets.gridViews.base.GridViewWidgetBase');

final class AdminPaymentExchangeDirectionGridViewWidget extends GridViewWidgetBase
{
    public $w = 'wAdminCommonGridView';
    public $class = 'iGridView i02';
    public $rowHtmlOptionsExpression = ' array( "data-idObj" => $data->id ) ';
    
    public function init()
    {
        $this->htmlOptions = array(
            'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
        );
        parent::init();
    }
    protected function getColumns()
    {
        $columns = array(
            array( 'name' => 'id'),
            array(
                'header' => 'From currency',
                'value' => ' $data->fromCurrency ? $data->fromCurrency->title : Yii::t($this->grid->NSi18n, "All") '
            ),
            array(
                'header' => 'To currency',
                'value' => ' $data->toCurrency ? $data->toCurrency->title : Yii::t($this->grid->NSi18n, "All") '
            ),
            array(
                'header' => 'title',
                'value' => ' $data->modelTitle '
            ),
        );
        foreach ($columns as &$column) {
            if (isset($column[ 'header' ])) {
                $column[ 'header' ] = Yii::t($this->NSi18n, $column[ 'header' ]);
            }
        }
        unset($column);
        return $columns;
    }
}
