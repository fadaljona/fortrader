<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminCurrencyRatesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wBrokerInstrumentsGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'code', 'header' => 'Char code'),
				Array( 'name' => 'name', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'name' => 'lastDataDate', 'header' => 'Last value date'),
				Array( 'name' => 'lastDataVal', 'header' => 'Last value'),
				Array( 'name' => 'lastDataNominal', 'header' => 'Last nominal'),
				Array( 'name' => 'countryName', 'header' => 'Currency Country'),
				Array( 'name' => 'currencyOrder', 'header' => 'Order'),
				Array( 'name' => 'popular' ),
				Array( 'name' => 'topInSelect' ),
				Array( 'name' => 'symbol', 'type' => 'raw' ),
				Array( 'header' => 'Setted plurals', 'value' => ' $data->hasNamePlurals ' ),
				Array( 'header' => 'To name', 'value' => ' $data->toName ' ),
				Array( 'name' => 'toInformer' ),
				Array( 'name' => 'informerName' ),
				Array( 'header' => 'Type', 'value' => ' $this->grid->formatType( $data ) ' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatType( $data ) {
			$outStr = '';
			if( !$data->noData ) $outStr = 'cbr';
			if( $data->ecb ) $outStr = $outStr ? $outStr . ' ecb' : 'ecb';
			return $outStr;
		}
	}

?>