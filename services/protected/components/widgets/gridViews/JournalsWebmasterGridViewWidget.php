<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class JournalsWebmasterGridViewWidget extends GridViewWidgetFrontBase {
		public $journalId = 0;
		public $w = 'wJournalsWebmasterGridView';
		public $class = 'iGridView i05 i07';
		public $rowHtmlOptionsExpression = 'Array("class"=>($data->is_blocked) ? "blocked" : "nonblocked",  "id" => $data->idWebmaster ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "dataTable items";

		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='1' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		protected function getColumns() {
			$columns = Array(

				Array( 
					'header' => 'Site',
					'value'=>'CHtml::tag("div", array(), CHtml::link($data->url, "/go/".preg_replace( "(^https?://)", "", $data->url ), Array( "target" => "_blank", "rel" => "nofollow" )))',
					'type'=>'raw',
					'headerHtmlOptions' => Array( 'class' => 'magazine_table_website'),
					'htmlOptions' => array( 'class' => 'tabl_quotes_tipe magazine_table_website', 'data-title' => Yii::t('*', 'Site') ),
				),
				Array( 
					'header' => 'Description',
					'value'=>'$data->description',
					'type'=>'raw',
					'htmlOptions' => array( 'data-title' => Yii::t('*', 'Description') ),
				),
				Array( 
					'header' => 'Downloads',
					'value'=>'$data->getDownloadCount(null, 30)."/".$data->getDownloadCount(null, 1)',
					'type'=>'raw',
					'headerHtmlOptions' => Array( 'class' => 'magazine_table_download'),
					'htmlOptions' => array( 'data-title' => Yii::t('*', 'Downloads') ),
				),
                
			);

			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>
