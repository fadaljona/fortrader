<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminCurrencyRatesConvertPairsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminCommonGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'idPair'),
                Array( 'header' => 'From code', 'value' => ' $data->rateFrom->code '),
                Array( 'header' => 'To code', 'value' => ' $data->rateTo->code ' ),
                Array( 'header' => 'From name', 'value' => ' $data->rateFrom->name ' ),
                Array( 'header' => 'To name', 'value' => ' $data->rateTo->name ' ),
                Array( 'header' => 'Title', 'value' => ' !empty($data->getI18N()->title) ? substr($data->title, 0, 50). "..." : "" ' ),
                Array( 'header' => 'Meta title', 'value' => ' !empty($data->getI18N()->metaTitle) ? substr($data->metaTitle, 0, 50). "..." : "" ' ),
				Array( 'header' => 'Full description', 'value' => ' $data->fullDesc ? substr($data->fullDesc, 0, 50). "..." : "" ' ),
				Array( 'header' => 'Meta Keys', 'value' => ' $data->metaKeys ? substr($data->metaKeys, 0, 50). "..." : "" ' ),
				Array( 'header' => 'Meta Desc', 'value' => ' $data->metaDesc ? substr($data->metaDesc, 0, 50). "..." : "" ' ),
				Array( 'name' => 'type'),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>