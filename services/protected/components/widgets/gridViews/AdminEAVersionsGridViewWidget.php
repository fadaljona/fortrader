<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminEAVersionsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wEAVersionsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idVersion" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'version', 'header' => 'Version', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $data->EA->name ', 'header' => 'EA', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $data->user->user_login ', 'header' => 'Added', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>