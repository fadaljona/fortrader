<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.ContestActionsGridColumn' );

	final class ContestInfoGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestInfo';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "idContest" => $data->id ) ';
		public $selectableRows = 2;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'class' => 'ACECheckBoxGridColumn',  'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-480' ),  'htmlOptions' => Array( 'class' => 'checkbox-column hidden-480' )),
				Array( 'value' => ' $this->grid->formatBroker( $data ) ', 'type' => 'raw', 'header' => 'Broker', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $this->grid->formatSumPrizes( $data->sumPrizes ) ', 'header' => 'Prize fund', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'name' => 'countMembers', 'header' => 'Members', 'headerHtmlOptions' => Array( 'class' => 'c03 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' ) ),
				Array( 'value' => ' $this->grid->formatDate( $data->begin ) ', 'header' => '<i class="icon-time"></i>&nbsp;'.Yii::t( $this->NSi18n, 'Begin' ), 'headerHtmlOptions' => Array( 'class' => 'c04 hidden-phone' ), 'htmlOptions' => Array( 'class' => 'hidden-phone' )),
				Array( 'value' => ' $this->grid->formatDate( $data->end ) ', 'header' => 'End', 'headerHtmlOptions' => Array( 'class' => 'c05 hidden-phone' ), 'htmlOptions' => Array( 'class' => 'hidden-phone' )),
				Array( 'value' => ' $this->grid->formatStatus( $data ) ', 'header' => 'Status', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c06' )),
				Array( 'value' => ' $this->grid->formatUpdate( $data->updatedStats ) ', 'header' => 'Updated', 'headerHtmlOptions' => Array( 'class' => 'c07 hidden-phone' ), 'htmlOptions' => Array( 'class' => 'hidden-phone' )),
			);
			if( Yii::App()->user->checkAccess( 'contestControl' )) {
				$columns[] = Array( 'class' => 'ContestActionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		private function sortBrokers( $a, $b ) {
			return $a->getShortName() > $b->getShortName() ? 1 : -1;
		}
		function formatBroker( $data ) {
			$brokers = $data->getBrokers();
			usort( $brokers, Array( $this, 'sortBrokers' ));
			$out = Array();
			foreach( $brokers as $broker ) {
				$out[] = CHtml::link( CHtml::encode( $broker->getShortName()), $broker->getSingleURL() );
			}
			return implode( ', ', $out );
		}
		function formatSumPrizes( $sumPrizes ) {
			return "\$ {$sumPrizes}";
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$Y = date( "Y", $time );
			$M = date( "M", $time );
			$d = date( "d", $time );
			$date = "{$M} {$d}";
			
			$yearNow = date( "Y" );
			if( $Y != $yearNow ) $date = "{$Y} {$date}";
			
			return $date;
		}
		function formatUpdate( $DT ) {
			$DT && $this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $DT, 'class' => "iBold" ));
		}
		function isCurrentUserInContest( $idContest ) {
			static $idContests;
			if( $idContests === null ) {
				$idContests = Array();
				$userModel = Yii::App()->user->getModel();
				if( $userModel ) {
					$idContests = CommonLib::slice( $userModel->memberContests, 'idContest' );
				}
			}
			return in_array( $idContest, $idContests );
		}
		function formatStatus( $data ) {
			$status = $data->status;
			$classes = Array(
				'registration stoped' => 'label',
				'registration' => 'label label-success',
				'started' => 'label label-important arrowed-in',
				'completed' => 'label label-warning',
			);
			$class = @$classes[ $status ];
			$title = ucfirst( $status );
			$title = Yii::t( $this->NSi18n, $title );
			switch( $status ) {
				case 'registration':{
					if( $this->isCurrentUserInContest( $data->id )) {
						$title = Yii::t( $this->NSi18n, 'You are registered' );
						$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
					}
					else{
						$href = $this->controller->createUrl( 'contest/registerMember', Array( 'idContest' => $data->id ));
						$label = CHtml::tag( "a", Array( 'class' => $class, 'href' => $href ), $title );
					}
					break;
				}
				default:{
					$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
				}
			}
			
			return $label;
		}
	}

?>