<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminFeedbackTypesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wFeedbackTypesGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idType" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->position ', 'header' => '#' ),
				Array( 'value' => ' $data->currentLanguageI18N ? $data->currentLanguageI18N->name : "" ', 'header' => 'Title' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>