<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	
	final class UsersGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wUsersGridView';
		public $class = 'iGridView i03 ';
		public $rowHtmlOptionsExpression = ' Array( "idUser" => $data->id ) ';
		public $selectableRows = 0;
		public $itemsCssClass = "dataTable items ";
		public $sortField;
		public $sortType;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				//return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $this->grid->formatLink( $data ) ', 
					'header' => 'User', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'class' => ' '.$this->getSorting( 'name' ),
						//'sortField' => 'place' 
					), 
					'htmlOptions' => Array( 'class' => 'iTd i03' )
				),
				Array( 
					'value' => ' CommonLib::numberFormat( $data->countReputation ) ', 
					'header' => 'Reputation',
					'headerHtmlOptions' => Array( 
						'class' => ' '.$this->getSorting( 'reputation' ),
						//'sortField' => 'place' 
					), 
				),
				Array( 
					'value' => ' CommonLib::numberFormat( $data->countActivities ) ', 
					'header' => 'Activity', 
					'headerHtmlOptions' => Array( 'class' => 'hidden-phone '.$this->getSorting( 'activity' ) ), 
					'htmlOptions' => Array( 'class' => 'hidden-phone' )
				),
				Array( 
					'value' => ' $this->grid->formatAwards( $data ) ', 
					'header' => 'Awards', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 'class' => 'hidden-phone '.$this->getSorting( 'awards' )), 
					'htmlOptions' => Array( 'class' => 'iTd i02 hidden-phone' )
				),
				Array( 
					'value' => ' $this->grid->formatProfile( $data ) ', 
					'header' => 'Profile', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 'class' => 'hidden-phone' ), 
					'htmlOptions' => Array( 'class' => 'iTd i01 hidden-phone' )
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatLink( $data ) {
			$content = $data->getHTMLAvatar( Array( 'class' => "iImg i03", ));
			$content .= CHtml::encode( $data->showName );
			return CHtml::link( $content, $data->getProfileURL() );
		}
		function formatAwards( $data ) {
			$output = '';
			foreach( $data->memberContests as $memberContest ) {
				if( $memberContest->winer ) {
					$output .= $memberContest->winer->getIcon();
				}
			}
			return $output;
		}
		function formatProfile( $data ) {
			$output = '';
			if( $data->profile ) {
				if( $data->profile->isTrader) 
					$output .= CHtml::tag( 'i', Array( 'class' => 'iI i11 i12_trader', 'title' => Yii::t( '*', 'Trader' ) ), '', true );
				if( $data->profile->isManager) 
					$output .= CHtml::tag( 'i', Array( 'class' => 'iI i11 i12_manager', 'title' => Yii::t( '*', 'Manager' )), '', true );
				if( $data->profile->isInvestor) 
					$output .= CHtml::tag( 'i', Array( 'class' => 'iI i11 i12_investor', 'title' => Yii::t( '*', 'Investor' )), '', true );
				if( $data->profile->isProgrammer) 
					$output .= CHtml::tag( 'i', Array( 'class' => 'iI i11 i12_programmer', 'title' => Yii::t( '*', 'Programmer' )), '', true );
			}
			return $output;
		}
	}

?>