<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminInformerStatsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminInformerStatsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = '  ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				array( 'header' => 'Domain', 'value' => ' $this->grid->formatDomainLink( $data->domain ) ', 'type' => 'raw' ),
				array( 'name' => 'count' ),
				array( 'name' => 'lastDate' ),
				array( 'header' => 'Style', 'value' => ' $data->stModel->title ' ),
				array( 'header' => 'Category', 'value' => ' $data->catModel->title ' ),
				array( 'name' => 'isHtml' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDomainLink( $domain ) {
			return CHtml::link($domain, '//' . $domain, array( 'target' => '_blank' ));
		}
	}

?>