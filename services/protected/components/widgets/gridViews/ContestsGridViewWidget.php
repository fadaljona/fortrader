<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ContestActionsGridColumn' );

	final class ContestsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestsGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "data-idContest" => $data->id ) ';
		public $selectableRows = 2;
		public $label;
		
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}

		protected function getColumns() {
			$columns = Array(
			
				Array( 
					'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)', 
					'header' => '#', 
					'type' => 'raw', 
					'htmlOptions' => Array( 'data-title' => '#' )
				),
				Array( 
					'value' => ' $this->grid->formatBrokers( $data ) ', 
					'header' => 'Sponsor', 
					'type' => 'raw', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Sponsor' ) )
				),
				Array( 
					'value' => ' $this->grid->formatName( $data ) ', 
					'header' => 'Title', 
					'type' => 'raw', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Title' ) )
				),
				Array( 
					'value' => ' $this->grid->formatSumPrizes( $data->sumPrizes ) ', 
					'header' => 'Prize fund', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Prize fund' ) )
				),
				Array( 
					'name' => 'countMembers', 
					'header' => 'Members', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Members' ) )
				),
				Array( 
					'value' => ' $this->grid->formatDate( $data->begin ) ', 
					'header' => 'Begin', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Begin' ) )
				),
				Array( 
					'value' => ' $this->grid->formatStatus( $data ) ', 
					'header' => 'Status', 
					'type' => 'raw', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Status' ) )
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatBrokers( $data ) {
			$out = Array();
			foreach( $data->brokers as $broker ) {
				$key = Yii::t( '*', "{broker} Forex Contests", Array( '{broker}' => $broker->brandName ));
				$thumbImage = $broker->getThumbImage(Array( 'title' => $key, 'alt' => $key ));
				$title = $thumbImage ? $thumbImage : CHtml::encode( $broker->brandName );
				$out[] = CHtml::link( $title, $broker->getSingleURL() );
			}
			$out = implode( ', ', $out );
			$out = str_replace( '>, <', '> <', $out );
			return $out;
		}
		function formatName( $data ) {
			return CHtml::link( CHtml::encode( $data->currentLanguageI18N->name ), $data->singleURL );
		}
		function formatSumPrizes( $sumPrizes ) {
			return "\${$sumPrizes}";
		}
		function formatDate( $date ) {
			/*$time = strtotime( $date );
			$Y = date( "Y", $time );
			$M = date( "M", $time );
			$d = date( "d", $time );
			$date = "{$M} {$d}";
			
			$yearNow = date( "Y" );
			if( $Y != $yearNow ) $date = "{$Y} {$date}";
			
			return $date;*/
			return Yii::app()->dateFormatter->format( "d MMM yyyy", strtotime($date) );
		}
		function isCurrentUserInContest( $idContest ) {
			static $idContests;
			if( $idContests === null ) {
				$idContests = Array();
				$userModel = Yii::App()->user->getModel();
				if( $userModel ) {
					$idContests = CommonLib::slice( $userModel->memberContests, 'idContest' );
				}
			}
			return in_array( $idContest, $idContests );
		}
		function formatStatus( $data ) {
			$status = $data->status;
			$classes = Array(
				'registration stoped' => 'label',
				'registration' => 'label label-success',
				'started' => 'label label-important arrowed-in',
				'completed' => 'label label-warning',
			);
			$class = @$classes[ $status ];
			$title = ucfirst( $status );
			if( $title == 'Completed' ) {
				$title = 'Contest finished';
			}
			$title = Yii::t( $this->NSi18n, $title );
			switch( $status ) {
				case 'registration':{
					if( $this->isCurrentUserInContest( $data->id )) {
						$title = Yii::t( $this->NSi18n, 'You are registered' );
						$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
					}
					else{
						$href = $data->singleURL . '#contestRegistrationForm';
						$label = CHtml::tag( "a", Array( 'class' => $class, 'href' => $href ), $title );
					}
					break;
				}
				default:{
					$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
				}
			}
			
			return $label;
		}
	}

?>