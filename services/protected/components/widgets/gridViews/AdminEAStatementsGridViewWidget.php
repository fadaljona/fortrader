<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminEAStatementsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wEAStatementsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idStatement" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'title', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $data->user->user_login ', 'header' => 'Added', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'name' => 'symbol', 'header' => 'Symbol', 'headerHtmlOptions' => Array( 'class' => 'c03' )),
				Array( 'name' => 'period', 'header' => 'Period', 'headerHtmlOptions' => Array( 'class' => 'c04' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>