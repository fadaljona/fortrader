<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.EATradeAcoountsActionsGridColumn' );

	final class EATradeAccountsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wEATradeAccountsGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "idAccount" => $data->idTradeAccount ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		public $idEA;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array();
			$columns[] = Array( 
				'class' => 'ACECheckBoxGridColumn',  
				'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' ),  
				'htmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' )
			);
			$colName = Array( 
				'value' => ' $this->grid->formatName( $data ) ', 
				'header' => 'Account name',
				'headerHtmlOptions' => Array( 
					'class' => 'c02 '.$this->getSorting( 'name' ),
					'sortField' => 'name' 
				), 
				'htmlOptions' => Array( 'class' => 'c01' )
			);
			$colAccount = Array( 
				'value' => ' $this->grid->formatNumber( $data ) ', 
				'header' => 'Account number',
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c02 '.$this->getSorting( 'number' ),
					'sortField' => 'number' 
				), 
				'htmlOptions' => Array( 'class' => 'c02' )
			);
			$colVersion = Array( 
				'value' => ' $this->grid->formatVersion( $data ) ', 
				'header' => 'Version', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c02 hidden-1200 '.$this->getSorting( 'version' ),
					'sortField' => 'version' 
				), 
				'htmlOptions' => Array( 'class' => 'c02 hidden-1200' )
			);
			$colStatement = Array( 
				'value' => ' $this->grid->formatStatement( $data ) ', 
				'header' => 'Config', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c02'.$this->getSorting( 'statement' ),
					'sortField' => 'statement', 
				), 
				'htmlOptions' => Array( 'class' => 'c02' )
			);
			$columns[] = $colName;
			$columns[] = $colAccount;
			
			//$columns[] = $colVersion;
			//$columns[] = $colStatement;
			$columns[] = Array( 
				'value' => ' $this->grid->formatType( $data->tradeAccount->type ) ', 
				'header' => 'Type', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c03 hidden-1024 '.$this->getSorting( 'type' ),
					'sortField' => 'type' 
				), 
				'htmlOptions' => Array( 'class' => 'c03 hidden-1024' )
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatGain( $data->tradeAccount->stats->gain ) ', 
				'header' => 'Gain', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c04 '.$this->getSorting( 'gain' ),
					'sortField' => 'gain' 
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap c04' )
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatDrow( $data->tradeAccount->stats->drowMax ) ', 
				'header' => 'Drow', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c05 hidden-1400 '.$this->getSorting( 'drow' ),
					'sortField' => 'drow' 
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap c05 hidden-1400' )
			);
			$columns[] = Array( 
				'value' => ' CommonLib::numberFormat( $data->tradeAccount->stats->countTrades ) ', 
				'header' => 'Tradese', 
				'headerHtmlOptions' => Array( 
					'class' => 'c06 hidden-1600 '.$this->getSorting( 'tradese' ),
					'sortField' => 'tradese',
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap hidden-1600' )
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatUpdate( $data->tradeAccount->stats->trueDT ) ', 
				'header' => '<i class="icon-time bigger-110 hidden-phone"></i>'.Yii::t( $this->NSi18n, 'Updated' ), 
				'headerHtmlOptions' => Array( 
					'class' => 'c06 hidden-1024 '/*.$this->getSorting( 'tradese' )*/,
					//'sortField' => 'tradese',
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap hidden-1024'	)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatStatus( $data->tradeAccount->status ) ', 
				'header' => 'Status', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c07 hidden-640 '/*.$this->getSorting( 'type' )*/,
					//'sortField' => 'type' 
				), 
				'htmlOptions' => Array( 'class' => 'c07 hidden-640' )
			);
			if( Yii::App()->user->checkAccess( 'tradeAccountControl' )) {
				$columns[] = Array( 'class' => 'EATradeAcoountsActionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatName( $data ) {
			return $data->tradeAccount->name;
		}
		function formatNumber( $data ) {
			return CHtml::link( $data->tradeAccount->accountNumber, $data->getSingleURL() );
		}
		function formatVersion( $data ) {
			return CHtml::link( $data->version->version, $data->version->getSingleURL() );
		}
		function formatStatement( $data ) {
			$view = Yii::t( $this->NSi18n, "View" );
			return $data->statement ? CHtml::link( $view, $data->statement->getSingleURL() ) : '';
		}
		function formatType( $type ) {
			$classes = Array(
				'Demo' => 'label',
				'Real' => 'label label-success',
			);
			$class = @$classes[ $type ];
			$title = Yii::t( $this->NSi18n, $type );
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatGain( $gain ) {
			return $gain !== null ? $this->formatPercent( $gain ) : '';
		}
		function formatDrow( $drowMax ) {
			return $drowMax !== null ? $this->formatPercent( $drowMax ) : '';
		}
		function formatUpdate( $trueDT ) {
			$trueDT && $this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $trueDT, 'class' => "iBold" ));
		}
		function formatStatus( $status ) {
			$classes = Array(
				'Off' => 'label',
				'On' => 'label label-success',
			);
			$class = @$classes[ $status ];
			$title = Yii::t( $this->NSi18n, $status );
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
	}

?>