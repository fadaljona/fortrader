<?if( $regulators ){?>
	<?if( count( $regulators ) == 1 ){?>
		<span class="blue_color">
		<?if( strlen( $regulators[0]->link )){?>
			<a href="<?=CommonLib::getURL( $regulators[0]->link )?>" target="_blank">
				<?=$regulators[0]->name?>
			</a>
		<?}else{?>
			<?=$regulators[0]->name?>
		<?}?>
		</span>
	<?}else{?>	
		<div class="table_rating_select select_text_left">
			<div class="newDropDownList PFDregular">
				<div class="active_option open_select">
					<div class="inner"><?=Yii::t('*','List')?></div><i class="fa fa-angle-down"></i>
				</div>
				<ul class="options_list dropDown">
					<?php
						foreach( $regulators as $regulator ) {
							if( strlen( $regulator->link ) ){
								$url = CommonLib::getURL( $regulator->link );
								$htmlOptions = Array( 'target' => '_blank' );
							}else{
								$url = 'javascript:;';
								$htmlOptions = Array( );
							}
							$regulatorStr = CHtml::link( CHtml::encode( $regulator->name ), $url, $htmlOptions);
							echo '<li>' . $regulatorStr . '</li>';
						}
					?>
				</ul>
			</div>
		</div>
	<?}?>
<?}?>

