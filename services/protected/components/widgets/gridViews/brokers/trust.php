<?
	if( $trust === null ) {
		return;
	}
	
	$trust = round( $trust );
	
	if( $trust <= 30 ) {
		$color = "#D15B47";
	}
	elseif( $trust <= 59 ) {
		$color = "#87CEEB";
	}
	else{
		$color = "#87B87F";
	}
?>
<div class="easy-pie-chart percentage" data-percent="<?=$trust?>" data-color="<?=$color?>">
	<span class="percent"><?=$trust?></span> %
</div>