<?php /*<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">*/?>
<?php if( $data->stats->place === 0 ){ ?>

	<span class="table_rating_place"><span><?=Yii::t( '*', 'ADV' )?></span></span>
	<span class="table_rating_ico">
	<?=CHtml::link( $data->getThumbImage() , Yii::App()->createURL( 'broker/redirectName', Array( 'id' => $data->id )) )?>
	</span>
	<span class="blue_color_2 ">
		<?php
			$ads_model = AdvertisementModel::getModelByBrokerId( $data->id );
			AdvertisementModel::hitShowModels( array('0'=>$ads_model) );
			$itemUrl = Yii::App()->createAbsoluteURL( 'broker/redirectName', Array( 'id' => $data->id ));
		?>
		<?=CHtml::link( CHtml::tag('span', array( /*'itemprop' => 'name'*/), CHtml::encode( $data->shortName ) ), $itemUrl/*, array('itemprop' => 'item' )*/ )?>
	</span>
	<div class="clear"></div>
<?php }else{ ?>

	<span class="table_rating_place"><span><?php if( $data->stats->place == NULL ) echo Yii::t( '*', 'NEW' ); else echo $data->stats->place; ?></span></span>
	<span class="table_rating_ico">
	<?=CHtml::link( $data->getThumbImage() , $data->singleURL )?>
	</span>
	<span class="blue_color_2 ">
		<?php $itemUrl = $data->absoluteSingleURL; ?>
		<?=CHtml::link( CHtml::tag('span', array( /*'itemprop' => 'name'*/), CHtml::encode( $data->shortName ) ), $itemUrl/*, array('itemprop' => 'item' )*/ )?>
	</span>
	<div class="clear"></div>
<?php }?>
<?php /*	<meta itemprop="position" content="<?=$data->stats->originPlace?>" />
</div>	*/?>