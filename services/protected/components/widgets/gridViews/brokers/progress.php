<div class="progress <?=$class?> progress-striped">
	<div class="bar" style="width: <?=round( $value )?>%"></div>
</div>