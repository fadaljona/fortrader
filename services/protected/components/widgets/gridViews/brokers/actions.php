<?
if( 
	!strlen( $data->openAccount )
	and !strlen( $data->site )
	and !strlen( $data->conditions )
	and !strlen( $data->terms )
) {
	echo '_';
	return false;
}
	
echo CHtml::openTag('div', array( 'class' => 'table_rating_select select_text_left' ));	
	echo CHtml::openTag('div', array( 'class' => 'newDropDownList PFDregular' ));
		echo CHtml::openTag('div', array( 'class' => 'active_option open_select' ));
			echo CHtml::tag( 'div', array( 'class' => 'inner'), Yii::t( '*', 'Choice' ) );
			echo CHtml::tag('i', array( 'class' => 'fa fa-angle-down' ), '');
		echo CHtml::closeTag('div');
		
		echo CHtml::openTag('ul', array( 'class' => 'options_list dropDown' ));
			if( strlen( $data->openAccount )){
				echo CHtml::openTag('li');
					echo CHtml::link( 
						Yii::t( '*', 'Open account' ), 
						$data->getRedirectURL( 'openAccount' ),
						array(
							'class' => 'saveBrokerLinksStats',
							'data-broker-id' => $data->id,
							'data-link-type' => 'openAccount',
							'target' => '_blank'
						)
					);
				echo CHtml::closeTag('li');
			}
			if( strlen( $data->openDemoAccount )){
				echo CHtml::openTag('li');
					echo CHtml::link( 
						Yii::t( '*', 'Open demo account' ), 
						$data->getRedirectURL( 'openDemoAccount' ),
						array(
							'class' => 'saveBrokerLinksStats',
							'data-broker-id' => $data->id,
							'data-link-type' => 'openDemoAccount',
							'target' => '_blank'
						)
					);
				echo CHtml::closeTag('li');
			}
			if( strlen( $data->site )){
				echo CHtml::openTag('li');
					echo CHtml::link( 
						Yii::t( '*', 'Go to the wesite' ), 
						$data->getRedirectURL( 'site' ),
						array(
							'class' => 'saveBrokerLinksStats',
							'data-broker-id' => $data->id,
							'data-link-type' => 'site',
							'target' => '_blank'
						)
					);
				echo CHtml::closeTag('li');
			}
			if( strlen( $data->conditions )){
				echo CHtml::openTag('li');
					echo CHtml::link( 
						Yii::t( '*', 'Veiew conditions' ), 
						$data->getRedirectURL( 'conditions' ),
						array(
							'class' => 'saveBrokerLinksStats',
							'data-broker-id' => $data->id,
							'data-link-type' => 'conditions',
							'target' => '_blank'
						)
					);
				echo CHtml::closeTag('li');
			}
			if( strlen( $data->terms )){
				echo CHtml::openTag('li');
					echo CHtml::link( 
						Yii::t( '*', 'Veiew reglament' ), 
						$data->getRedirectURL( 'terms' ),
						array(
							'class' => 'saveBrokerLinksStats',
							'data-broker-id' => $data->id,
							'data-link-type' => 'terms',
							'target' => '_blank'
						)
					);
				echo CHtml::closeTag('li');
			}
		echo CHtml::closeTag('ul');
		
	echo CHtml::closeTag('div');
echo CHtml::closeTag('div');
?>