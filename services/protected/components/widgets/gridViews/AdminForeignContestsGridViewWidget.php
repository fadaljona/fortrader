<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminForeignContestsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wBrokerInstrumentsGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'prize'),
				Array( 'name' => 'begin'),
				Array( 'name' => 'approved'),
				Array( 'name' => 'title', 'header' => 'Title'),
				Array( 'name' => 'sponsor'),
				Array( 'name' => 'link'),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>