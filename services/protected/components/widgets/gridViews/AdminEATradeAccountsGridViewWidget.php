<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminEATradeAccountsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wEATradeAccountsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idAccount" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->EA->name ', 'header' => 'EA', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $data->tradeAccount->name ', 'header' => 'Account name', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'value' => ' $data->tradeAccount->accountNumber ', 'header' => 'Account number', 'headerHtmlOptions' => Array( 'class' => 'c03' )),
				Array( 'value' => ' $this->grid->t( $data->tradeAccount->status ) ', 'header' => 'Status', 'headerHtmlOptions' => Array( 'class' => 'c04' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>