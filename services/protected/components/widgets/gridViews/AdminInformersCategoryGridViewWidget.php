<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminInformersCategoryGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminCommonGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'header' => 'Title', 'value' => '$data->title'),
				Array( 'name' => 'order' ),
				Array( 'name' => 'slug' ),
				Array( 'name' => 'hidden' ),
				Array( 'header' => 'Image', 'type' => 'raw', 'value' => ' $data->imgSrc ? "<img width=\"100\" src=\"$data->imgSrc\">" : "" '),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>