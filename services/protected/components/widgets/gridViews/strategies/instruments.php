<?if( $instruments ){?>
	<?if( count( $instruments ) == 1 ){?>
		<?=$instruments[0]->name?>
	<?}else{?>
		<div class="position-relative">
			<a href="#" data-toggle="dropdown"><?=Yii::t('*','List')?></a>
			<div class="dropdown-menu dropdown-caret" style="padding:10px;">
				<?
					$out = Array();
					foreach( $instruments as $instrument ) {
						$out[] = $instrument->name;
					}
					echo implode( ', ', $out );
				?>
			</div>
		</div>
	<?}?>
<?}?>