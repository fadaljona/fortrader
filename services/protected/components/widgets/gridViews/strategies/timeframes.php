<?if( $timeframes ){?>
	<?if( count( $timeframes ) == 1 ){?>
		<?=$timeframes[0]?>
	<?}else{?>
		<div class="position-relative">
			<a href="#" data-toggle="dropdown"><?=Yii::t('*','List')?></a>
			<div class="dropdown-menu dropdown-caret" style="padding:10px;">
				<?
					$out = Array();
					foreach( $timeframes as $timeframe ) {
						$out[] = $timeframe;
					}
					echo implode( ', ', $out );
				?>
			</div>
		</div>
	<?}?>
<?}?>