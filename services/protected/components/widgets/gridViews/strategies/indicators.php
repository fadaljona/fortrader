<?if( $indicators ){?>
	<?if( count( $indicators ) == 1 ){?>
		<?if( strlen( $indicators[0]->desc )){?>
			<a href="<?=CommonLib::getURL( $indicators[0]->desc )?>" target="_blank">
				<?=$indicators[0]->name?>
			</a>
		<?}else{?>
			<?=$indicators[0]->name?>
		<?}?>
	<?}else{?>
		<div class="position-relative">
			<a href="#" data-toggle="dropdown"><?=Yii::t('*','List')?></a>
			<div class="dropdown-menu dropdown-caret" style="padding:10px;">
				<?
					$out = Array();
					foreach( $indicators as $indicator ) {
						$out[] = strlen( $indicator->desc ) ? CHtml::link( CHtml::encode( $indicator->name ), CommonLib::getURL( $indicator->desc ), Array( 'targer' => '_blank' )) : $indicator->name;
					}
					echo implode( ', ', $out );
				?>
			</div>
		</div>
	<?}?>
<?}?>