<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminQuotesSettingsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wQuotesSettingsGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idSetting" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'id', 'header' => 'ID', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'name', 'header' => 'Имя', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'desc', 'header' => 'Описание', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'value', 'header' => 'Значение', 'headerHtmlOptions' => Array( 'class' => '' ))
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( "quotesWidget",$column[ 'header' ]); unset( $column );
			return $columns;
		}
	}
