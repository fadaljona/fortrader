<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class ContestsTableGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestsTableGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = 'array("data-idContest" => $data->id, "class" => "contest-table__row")';
		public $returnOnlyTrs = false;
		function init() {

			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass} contest-table\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		public function renderTableBody(){
			$data=$this->dataProvider->getData();
			$n=count($data);
			if( !$this->returnOnlyTrs )echo "<tbody>\n";

			if ($n>0) {
				for ($row = 0; $row < $n; ++$row) {
					$this->renderTableRow($row);
				}
			}
			else
			{
				echo '<tr><td colspan="'.count($this->columns).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
			if( !$this->returnOnlyTrs )echo "</tbody>\n";
		}
		public function renderTableHeader(){
			if(!$this->hideHeader){
				echo "<thead>\n";

				if($this->filterPosition===self::FILTER_POS_HEADER) $this->renderFilter();
				
				echo "<tr class='contest-table__row'>";
				foreach($this->columns as $n => $column) {
					if (!$n) {
						$head_align = "left";
					} else {
						$head_align = "center";
					}
					$column->headerHtmlOptions = array('class' => 'data_'.($n+1), 'align' => $head_align);
					$column->renderHeaderCell();
				}
				echo "</tr>";

				if($this->filterPosition===self::FILTER_POS_BODY) $this->renderFilter();

				echo "</thead>\n";
			}elseif($this->filter!==null && ($this->filterPosition===self::FILTER_POS_HEADER || $this->filterPosition===self::FILTER_POS_BODY)){
				echo "<thead>\n";
				$this->renderFilter();
				echo "</thead>\n";
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => 'CHtml::image($data->getSizedSrcImage(25, 25), "", array("class" => "img_preview")) . CHtml::link($data->title, $data->singleUrl)', 
					'header' => 'Title',
					'type' => 'raw',
					'htmlOptions' => Array( 'class' => 'data_1', 'data-title' => Yii::t( $this->NSi18n, 'Title' ) )
				),
				Array( 
					'value' => ' Yii::app()->dateFormatter->format( "d MMMM yyyy", strtotime($data->begin) ) ', 
					'header' => 'Begin', 
					'htmlOptions' => Array('class' => 'data_2', 'align' => 'center', 'data-title' => Yii::t( $this->NSi18n, 'Begin' ) )
				),
				Array( 
					'value' => ' $data->getMembersCount() ', 
					'header' => 'Members', 
					'htmlOptions' => Array('class' => 'data_3', 'align' => 'center', 'data-title' => Yii::t( $this->NSi18n, 'Prize amount' ) )
				),
				Array( 
					'value' => ' "$" . $data->getSumPrizes() ', 
					'header' => 'Prize', 
					'htmlOptions' => Array('class' => 'data_4', 'align' => 'center', 'data-title' => Yii::t( $this->NSi18n, 'Prize' ) )
				),
				Array( 
					'value' => '$data->getStatusSpan()',
					'header' => 'Status',
					'type' => 'raw',
					'htmlOptions' => Array('class' => 'data_5', 'align' => 'center', 'data-title' => Yii::t( $this->NSi18n, 'Status' ) )
				)
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatLink( $link ) {
			return CHtml::link( CHtml::encode( Yii::t( $this->NSi18n, "View" ) ), $link, array( 'target' => '_blank' ) );
		}
	}
?>