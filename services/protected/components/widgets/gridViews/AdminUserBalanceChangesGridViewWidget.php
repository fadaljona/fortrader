<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminUserBalanceChangesGridViewWidget extends GridViewWidgetBase {
		public $class = 'iGridView i01 i05 i06';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $this->grid->formatDate( $data->createdDT ) ', 'header' => 'Date', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $this->grid->formatChange( $data->value ) ', 'header' => 'Change', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$Y = date( "Y", $time );
			$M = date( "M", $time );
			$d = date( "d", $time );
			$date = Yii::t( $this->NSi18n, "{$M} {d}", Array( '{d}' => $d ));
			
			$yearNow = date( "Y" );
			if( $Y != $yearNow ) $date = "{$date} {$Y}";
			
			return $date;
		}
		function formatChange( $change ) {
			$str = CommonLib::numberFormat( $change );
			return $change > 0 ? "+{$str}" : $str;
		}
	}

?>