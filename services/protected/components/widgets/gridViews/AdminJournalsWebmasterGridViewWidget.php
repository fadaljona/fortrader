<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminJournalsWebmasterGridViewWidget extends GridViewWidgetBase {
		public $w = 'wJournalsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idWebmaster" => $data->idWebmaster ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'url', 'header' => 'Url' ),
				Array( 'name' => 'description', 'header' => 'description' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>