<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminBrokerLinksClickStatsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminQuotesInformerLogGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = '  ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				array( 'value' => ' $this->grid->formatName( $data ) ', 'type' => 'raw', 'header' => 'brandName' ),
				array( 'name' => 'brokerId' ),
				array( 'name' => 'openAccountClicks' ),
				array( 'name' => 'openDemoAccountClicks' ),
				array( 'name' => 'siteClicks' ),
				array( 'name' => 'conditionsClicks' ),
				array( 'name' => 'termsClicks' ),
				array( 'name' => 'lastDate' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatName( $data ) {
			return print_r( $data->currentLanguageI18N->brandName, true );
		}
	}

?>