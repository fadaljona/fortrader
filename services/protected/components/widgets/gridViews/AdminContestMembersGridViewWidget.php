<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminContestMembersGridViewWidget extends GridViewWidgetBase {
		public $w = 'wContestMembersGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idMember" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'idUser', 'value' => ' $data->user->showName ', 'header' => 'User', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'name' => 'accountNumber', 'header' => 'Account number', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>