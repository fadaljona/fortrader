<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminCountriesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wCountriesGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idCountry" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'name' => 'alias', 'header' => 'Alias', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>