<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.ContestMembersActionsGridColumn' );

	final class CurrentUserContestMemberInfoGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wCurrentUserContestMemberInfo';
		public $class = 'iGridView i05';
		public $rowCssClassExpression = ' $this->getRowClass( $data ) ';
		public $rowHtmlOptionsExpression = ' Array( "idMember" => $data->id ) ';
		public $selectableRows = 1;
		public $itCurrentModel;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getRowClass( $data ) {
			if( $data->stats->last_error_code == -255 ) {
				return 'iTr i01';
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'class' => 'ACECheckBoxGridColumn',  'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-480' ),  'htmlOptions' => Array( 'class' => 'checkbox-column hidden-480' )),
				Array( 'value' => ' $this->grid->formatAccountNumber( $data ) ', 'type' => 'raw', 'header' => 'Account number', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $this->grid->formatPlace( $data ) ', 'header' => 'Rank', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'value' => ' $this->grid->formatBalance( $data ) ', 'header' => 'Balance', 'headerHtmlOptions' => Array( 'class' => 'c03 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $this->grid->formatGain( $data ) ', 'header' => 'Gain', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c04 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $this->grid->formatDrow( $data ) ', 'header' => 'Drow', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c05 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' CommonLib::numberFormat( $data->stats->countTrades ) ', 'header' => 'Tradese', 'headerHtmlOptions' => Array( 'class' => 'c05 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' CommonLib::numberFormat( $data->stats->countOpen ) ', 'header' => 'Open', 'headerHtmlOptions' => Array( 'class' => 'c05 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $this->grid->formatStatus( $data ) ', 'header' => 'Status', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c06 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $this->grid->formatUpdate( $data ) ', 'header' => 'Updated', 'headerHtmlOptions' => Array( 'class' => 'c07 hidden-phone' ), 'htmlOptions' => Array( 'class' => 'hidden-phone' )),
			);
			if( $this->itCurrentModel or Yii::App()->user->checkAccess( 'contestMemberControl' )) {
				$columns[] = Array( 'class' => 'ContestMembersActionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatAccountNumber( $data ) {
			return CHtml::link( CHtml::encode( $data->accountNumber ), $data->singleURL );
		}
		function formatPlace( $data ) {
			return $data->stats->place ? CommonLib::numberFormat( $data->stats->place ) : '';
		}
		function formatBalance( $data ) {
			return $data->stats->balance !== null ? '$ '.CommonLib::numberFormat( $data->stats->balance ) : '';
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatGain( $data ) {
			return $data->stats->gain !== null ? $this->formatPercent( $data->stats->gain ) : '';
		}
		function formatDrow( $data ) {
			return $data->stats->drowMax !== null ? $this->formatPercent( $data->stats->drowMax ) : '';
		}
		function formatStatus( $data ) {
			return $data->getLabelStatus();
		}
		function formatUpdate( $data ) {
			$data->stats->trueDT && $this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->stats->trueDT, 'class' => "iBold" ));
		}
	}

?>