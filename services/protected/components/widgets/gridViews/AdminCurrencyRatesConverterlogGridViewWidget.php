<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminCurrencyRatesConverterlogGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminPostsStatsWpSearchLogGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = '  ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				array( 'name' => 'idConvert' ),
				array( 'name' => 'ip' ),
				Array( 'header' => 'Codes', 'value' => ' $this->grid->formatCodes( $data ) ' ),
				array( 'name' => 'createdDT' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatCodes( $data ){
			$from = $data->convert->rateFrom->code;
			$to = $data->convert->rateTo->code;
			return $from . ' --> ' . $to;
		}
	}

?>