<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminEANewsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wEANewsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idNews" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $this->grid->formatDate( $data->date ) ', 'header' => 'Date', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$date = date( "d.m.Y", $time );
			return $date;
		}
	}

?>