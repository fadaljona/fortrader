<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminCountersGridViewWidget extends GridViewWidgetBase {
		public $w = 'wCountersGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idCounter" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $this->grid->formatAction( $data ) ', 'header' => '', 'headerHtmlOptions' => Array( 'class' => 'c02' ))
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatAction( $data ) {
			require dirname( __FILE__ ).'/adminCounters/action.php';
		}
	}

?>