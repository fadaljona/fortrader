<?php
    
Yii::import('widgets.gridViews.base.GridViewWidgetBase');

final class AdminPaymentSystemGridViewWidget extends GridViewWidgetBase
{
    public $w = 'wAdminCommonGridView';
    public $class = 'iGridView i02';
    public $rowHtmlOptionsExpression = ' array( "data-idObj" => $data->id ) ';
    
    function init()
    {
        $this->htmlOptions = array(
            'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
        );
        parent::init();
    }
    protected function getColumns()
    {
        $columns = array(
            array( 'header' => 'Title', 'value' => '$data->title'),
            array( 'name' => 'slug' ),
        );
        foreach ($columns as &$column) {
            if (isset($column[ 'header' ])) {
                $column[ 'header' ] = Yii::t($this->NSi18n, $column[ 'header' ]); 
            }
        }
        unset($column);
        return $columns;
    }
}
