<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminQuotesCategoryGridViewWidget extends GridViewWidgetBase {
		public $w = 'wQuotesCategoryGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idCategory" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'id', 'header' => 'ID', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'name', 'header' => 'Имя', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'slug'),
				Array( 'name' => 'desc', 'header' => 'Описание', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'parentname', 'header' => 'Родительская категория', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'is_def', 'value'=>'$data->is_def==1?"+":""', 'header' => 'По умолчанию', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'order', 'header' => 'Порядок', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'name' => 'lOrder', 'header' => 'Lang order', 'headerHtmlOptions' => Array( 'class' => '' ))
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( "quotesWidget",$column[ 'header' ]); unset( $column );
			return $columns;
		}
	}
