<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );

	final class ContestMemberStatsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMemberStatsGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "idMember" => $data->id ) ';
		public $selectableRows = 2;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'class' => 'ACECheckBoxGridColumn',  'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-480' ),  'htmlOptions' => Array( 'class' => 'checkbox-column hidden-480' )),
				Array( 'value' => ' $this->grid->formatEquity( $data ) ', 'header' => 'Equity', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $this->grid->formatProfit( $data ) ', 'header' => 'Profit', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'value' => ' $this->grid->formatDeposit( $data ) ', 'header' => 'Deposits', 'headerHtmlOptions' => Array( 'class' => 'c03' )),
				Array( 'value' => ' $this->grid->formatWithdrawals( $data ) ', 'header' => 'Withdrawals', 'headerHtmlOptions' => Array( 'class' => 'c04 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' CommonLib::numberFormat( $data->stats->countTrades ) ', 'header' => 'Trades', 'headerHtmlOptions' => Array( 'class' => 'c05 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatEquity( $data ) {
			return $data->stats->equity !== null  ? CommonLib::numberFormat( $data->stats->equity ) : '';
		}
		function formatProfit( $data ) {
			return $data->stats->profit !== null ? '$ '.CommonLib::numberFormat( $data->stats->profit ) : '';
		}
		function formatDeposit( $data ) {
			return $data->stats->deposit !== null ? '$ '.CommonLib::numberFormat( $data->stats->deposit ) : '';
		}
		function formatWithdrawals( $data ) {
			return $data->stats->withdrawals !== null ? '$ '.CommonLib::numberFormat( $data->stats->withdrawals ) : '';
		}
	}

?>