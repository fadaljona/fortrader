<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class CalendarEventsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wCalendarEventsGridView';
		public $class = 'iGridView i05';
		public $rowCssClassExpression = ' $this->getRowClass( $data ) ';
		public $rowHtmlOptionsExpression = ' Array( "data-idEvent" => $data->id, "data-timeEvent" => strtotime( $data->dt ), "data-seriousEvent" => $data->serious ) ';
		public $selectableRows = 0;
		public $begin;
		public $end;
		public $period;
		public $dataTimeZone = 3;
		public $forthcomingLimit = 20;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function getRowClass( $data ) {
			static $ftime;
			
			$time = strtotime( $data->dt );
			$date = date( "Y-m-d", $time );
			$today = date( "Y-m-d" );
			$now = time();
			
			$rowClass = ' tabl_quotes_item ' . $data->getRowStatus();
			
			if( $date == $today and $time >= $now ) {		
				$datatime = date( "Y-m-d H:i", $time );
				if( $ftime === null or $ftime == $datatime ) {
					$ftime = $datatime;
					switch( $data->serious ) {
						case 1:{ $rowClass = 'green_bg2 ' . $rowClass; break; }
						case 2:{ $rowClass = 'cream_bg ' . $rowClass; break; }
						case 3:{ $rowClass = 'red_bg2 ' . $rowClass; break; }
					}
				}
			}
			return $rowClass;
		}
		
		public function getData() {
			
			$list = $this->dataProvider->getData();
			$data = Array();

			$tz = UserProfileModel::getTimezone('cookie') - $this->dataTimeZone;
			$timeOffset = (60 * 60 * $tz);
			
			$evalBegin = !$this->begin;
			$evalEnd = !$this->end;
			
			foreach( $list as $event ) {
				$time = strtotime( $event->dt ) + $timeOffset;
				$data[ date( "Y-m-d", $time ) ][] = $event;
				if( $evalBegin ) {
					if( !$this->begin or $time < $this->begin ) {
						$this->begin = $time;
					}
				}
				if( $evalEnd ) {
					
					if( !$this->end or $time > $this->end ) {
						$this->end = $time;
					}
				}
			}
			
			return $data;
		}
		
		public function renderItems(){
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
			{
				echo "<div class='turn_content'><div class='tabl_quotes table_rating table_calendar'><table class=\"{$this->itemsCssClass}\" data-item='1'>\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table></div></div>";
			}
			else
				$this->renderEmptyText();
		}
		public function renderTableBody() {
			echo "<tbody>\n";
			$tz = UserProfileModel::getTimezone('cookie') - $this->dataTimeZone;
			$timeOffset = (60 * 60 * $tz);
			
			$data = $this->getData();
			
			$beginTime = $this->begin + $timeOffset;
			$endTime = $this->end + $timeOffset;
			
			if( ($this->period == 'forthcoming' || $this->period == 'futureOfEvent') && ( ($this->end + 1 - $this->begin) % (60*60*24) ) != 0 ){
				$endTime = $endTime + 60*60*24;
			}
			
			$i = 0;
			for( $time = $beginTime; $time <= $endTime; $time += 60*60*24 ) {	
				$date = date( "Y-m-d", $time );		
	
				if( ($this->period == 'forthcoming' || $this->period == 'futureOfEvent') && empty( $data[ $date ]) ){
					
				}else{
					$this->renderDateRow( $date );		
				}	
					
				if( !empty( $data[ $date ])) {
					foreach( $data[ $date ] as $row ) {
						$this->renderTableRow( $i );
						$i++;
					}
				}
				else{
					if( $this->period != 'forthcoming' && $this->period != 'futureOfEvent' ) $this->renderEmptyRow();
				}
			}
			if( !count($data) && ($this->period == 'forthcoming' || $this->period == 'futureOfEvent') ){
				$this->renderEmptyRow();
			}else{
				$this->renderMoreRow($endTime + 60*60*24);
			}
			
			echo "</tbody>\n";
		}
		protected function renderDateRow( $dt ) {
			
			$date = Yii::app()->dateFormatter->format( 'cccc, LLLL dd\yyyy',strtotime( $dt ));
			require dirname( __FILE__ ).'/calendarEvents/date.php';
		}
		protected function renderMoreRow($endTime) {
			if( $this->period == 'forthcoming' ){
				if( $this->dataProvider->totalItemCount < $this->forthcomingLimit ) return false;
				$date = $this->t('Show more');
			}else{
				$date = Yii::app()->dateFormatter->format( 'cccc, LLLL dd\yyyy',$endTime);
			}
			if( $this->period != 'futureOfEvent' ) require dirname( __FILE__ ).'/calendarEvents/more.php';
		}
		protected function renderEmptyRow() {
			require dirname( __FILE__ ).'/calendarEvents/empty.php';
		}
		protected function getColumns() {
			$columns = Array(
			
				Array( 
					'value' => ' $this->grid->formatName( $data ) ', 
					'header' => 'Event', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 'class' => 'table_calendar_title' ), 
					'htmlOptions' => array( 'class' => 'calendar_event', 'data-text' => $this->t('Event') )
				),
				Array( 
					'value' => ' $this->grid->formatDate( $data->dt ) ', 
					'header' => 'Time', 
					'htmlOptions' => Array( 'class' => 'semibold', 'data-text' => $this->t('Time') )
				),
				Array( 
					'value' => ' $this->grid->formatTimeLeft( $data ) ', 
					'header' => 'Time left', 
					'type' => 'raw', 
					'htmlOptions' => Array( 'class' => 'semibold timeLeft', 'data-text' => $this->t('Time left') )
				),
				Array( 
					'value' => ' $this->grid->formatImpact( $data ) ', 
					'header' => 'Impact', 
					'type' => 'raw', 
					'htmlOptions' => Array( 'class' => 'box_label', 'data-text' => $this->t('Impact') )
				),
				Array(
					'value' => ' $data->getBeforeValueHtml() ', 
					'type'=> 'html' , 
					'header' => 'Previous',
					'htmlOptions' => Array( 'class' => 'semibold', 'data-text' => $this->t('Previous') ),
					'cssClassExpression' => ' $data->getBeforeClass() ',
				),
				Array( 
					'value' => ' $data->getMbValue() ',
					'header' => 'MB', 
					'htmlOptions' => Array( 'class' => 'semibold', 'data-text' => $this->t('MB') ),
				),
				Array( 
					'value' => ' $data->getFactValueHtml( ) ',
					'type'=>'html', 
					'header' => 'Fact',
					'htmlOptions' => Array( 'class' => 'semibold', 'data-text' => $this->t('Fact') ),
					'cssClassExpression' => ' $data->getFactClass() ',
				),
			);

			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$tz = UserProfileModel::getTimezone('cookie') - $this->dataTimeZone;
			$time = $time + (60 * 60 * $tz);
			$date = date( "H:i", $time );
			return $date;
		}
		function formatTimeLeft( $data ) {
			return $data->getTimeLeft();
		}
		function formatName( $data ) {
			$out = "";

			$out .= CHtml::openTag('span', array('class' => 'calendar_icon'));
			
			if( $data->hasNews ) {
				$out .= CHtml::image(Yii::app()->params['wpThemeUrl'] . '/images/discuss-icon.png');
			}
			if( $data->hasDesc ) {
				$out .= CHtml::image(Yii::app()->params['wpThemeUrl'] . '/images/description-icon.png');
			}
			
			$out .= CHtml::closeTag('span');
			
			if( $data->event && $data->event->country ) {
				$out .= Chtml::tag('span', array('class' => 'calendar_flag'), $data->event->country->getImgFlag('shiny', 16));
			}
			
			$inner = CHtml::encode( $data->indicatorName );
			$out .= CHtml::link( $inner, $data->event->singleURL );
			
			return $out;
		}
		function formatImpact( $data ) {
			if( !$data->serious ) return '';
			
			$type = $data->getTitleSerious();
			
			$class = '';
			switch( $data->serious ) {
				case 1:{ $class = 'label_green'; break; }
				case 2:{ $class = 'label_orange'; break; }
				case 3:{ $class = 'label_red'; break; }
			}
			
			return CHtml::tag('span', array( 'class' => $class ), Yii::t( '*', ucfirst( $type )));
		}
	}

?>