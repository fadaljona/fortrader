<div class="pull-right">
	<a href="<?=Yii::App()->controller->createURL( '/admin/contest/exportMembers', Array( 'id' => $data->id ))?>" class="btn btn-small wExport">
		<?=Yii::t( '*', 'Export' )?>
	</a>
</div>
<?=$this->formatDate( $data->begin )?>