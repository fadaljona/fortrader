<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminUserReputationsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wUserReputationsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idReputation" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->from->user_login ', 'header' => 'From' ),
				Array( 'value' => ' $data->to->user_login ', 'header' => 'To' ),
				Array( 'value' => ' $data->getHTMLMessage( 100 ) ', 'header' => 'Message', 'type' => 'raw' ),
				Array( 'value' => ' CommonLib::numberFormat( $data->count ) ', 'header' => 'Count' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>