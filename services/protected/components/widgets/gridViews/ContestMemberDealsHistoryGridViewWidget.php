<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );

	final class ContestMemberDealsHistoryGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMemberDealsHistoryGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "data-idDeal" => $data->id ) ';
		public $selectableRows = 2;
		public $member;
		public $sums;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
				'id' => $this->w,
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
				$this->renderSumm();
			}else
				$this->renderEmptyText();
		}
		protected function getColumns() {
			if( $this->member->server->tradePlatform->name == 'MetaTrader 5' ) {
				$columns = Array(
					Array( 'value' => ' $this->grid->formatTime( $data->DEAL_TIME ) ', 'header' => 'Time', 'htmlOptions' => Array( 'class' => 'height', 'data-title' => $this->t('Time') ) ),
					Array( 'value' => ' $data->DEAL_ORDER ', 'header' => 'Deal', 'htmlOptions' => Array( 'data-title' => $this->t('Deal') ) ),
					Array( 'value' => ' $data->DEAL_TICKET ', 'header' => 'Order', 'htmlOptions' => Array( 'data-title' => $this->t('Order') ) ),
					Array( 'value' => ' $data->SYMBOL ', 'header' => 'Symbol', 'htmlOptions' => Array( 'data-title' => $this->t('Symbol') ) ),
					Array( 'value' => ' $data->DEAL_TYPE ', 'header' => 'Action', 'htmlOptions' => Array( 'data-title' => $this->t('Action') ) ),
					Array( 'value' => ' $data->DEAL_ENTRY ', 'header' => 'Entry', 'htmlOptions' => Array( 'data-title' => $this->t('Entry') ) ),
					Array( 'value' => ' $data->DEAL_VOLUME ', 'header' => 'Volume', 'htmlOptions' => Array( 'data-title' => $this->t('Volume') ) ),
					Array( 'value' => ' CommonLib::numberFormat( $data->DEAL_PRICE, 4 ) ', 'header' => 'Price', 'htmlOptions' => Array( 'data-title' => $this->t('Price') ) ),
					Array( 'value' => ' $this->grid->formatSwap( $data->DEAL_SWAP ) ', 'type' => 'raw', 'header' => 'Swap', 'htmlOptions' => Array( 'data-title' => $this->t('Swap') ) ),
					Array( 'value' => ' $data->DEAL_PROFIT ', 'header' => 'Profit', 'htmlOptions' => Array( 'data-title' => $this->t('Profit') ) ),
				);
			}
			if( $this->member->server->tradePlatform->name == 'MetaTrader 4' ) {
				$columns = Array(
					Array( 'value' => ' $data->OrderTicket ', 'header' => 'Order', 'htmlOptions' => Array( 'class' => 'height', 'data-title' => $this->t('Order') ) ),
					Array( 'value' => ' $this->grid->formatTime( $data->OrderOpenTime ) ', 'header' => 'Time', 'htmlOptions' => Array( 'data-title' => $this->t('Time') ) ),
					Array( 'value' => ' $data->getOrderTypeTitle() ', 'header' => 'Type', 'htmlOptions' => Array( 'data-title' => $this->t('Type') ) ),
					Array( 'value' => ' $this->grid->formatOrderLots( $data ) ', 'header' => 'Volume', 'htmlOptions' => Array( 'data-title' => $this->t('Volume') ) ),
					Array( 'value' => ' $data->OrderSymbol ', 'header' => 'Symbol', 'htmlOptions' => Array( 'data-title' => $this->t('Symbol') ) ),
					Array( 'value' => ' $data->OrderStopLoss ', 'header' => 'S/L', 'htmlOptions' => Array( 'data-title' => $this->t('S/L') ) ),
					Array( 'value' => ' $data->OrderTakeProfit ', 'header' => 'T/P', 'htmlOptions' => Array( 'data-title' => $this->t('T/P') ) ),
					Array( 'value' => ' $this->grid->formatTime( $data->OrderCloseTime ) ', 'header' => 'Time', 'htmlOptions' => Array( 'data-title' => $this->t('Time') ) ),
					Array( 'value' => ' $data->OrderClosePrice ', 'header' => 'Price', 'htmlOptions' => Array( 'data-title' => $this->t('Price') ) ),
					Array( 'value' => ' $this->grid->formatSwap( $data->OrderSwap ) ', 'type' => 'raw', 'header' => 'Swap', 'htmlOptions' => Array( 'data-title' => $this->t('Swap') ) ),
					Array( 'value' => ' $data->OrderProfit ', 'header' => 'Profit', 'htmlOptions' => Array( 'data-title' => $this->t('Profit') ) ),
				);
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatTime( $time ) {
			$DT = date( "Y.m.d H:i", $time );
			return $DT;
		}
		function formatOrderLots( $data ) {
			if( $data->OrderType == 6 ) return 0;
			return $data->OrderLots;
		}
		function formatSwap( $swap ) {
			
			$str = strval($swap);
			$explodedStr = explode('.', $str);
			if( isset($explodedStr[1]) && strlen($explodedStr[1]) > 2 ) $explodedStr[1] = substr($explodedStr[1], 0, 2);
			$str = implode('.', $explodedStr);
			$swap = floatval($str);
			
			$class = $swap < 0 ? 'text-error' : '';
			$label = CHtml::tag( "span", Array( 'class' => $class ), $swap );
			return $label;
		}
		function renderSumm() {
			require dirname( __FILE__ ).'/contestMemberDealsHistory/footer.php';
		}
	}

?>