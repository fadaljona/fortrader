<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminCalendarEventsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wCalendarEventsGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "idEvent" => $data->id, "class" => $this->getClassRow( $data )) ';
		function init() {
			Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/assets-static/formHelpers/css/bootstrap-formhelpers.css" );
			Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/assets-static/formHelpers/css/bootstrap-formhelpers-countries.flags.css" );
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'indicatorIDForSearch', 'value' => ' $this->grid->formatIndicatorID( $data ) ', 'header' => 'ID indicator' ),
				Array( 'name' => 'indicatorForSearch', 'value' => ' $this->grid->formatName( $data ) ', 'class' => 'dataColumns.CalendarEventNameDataColumn', 'header' => 'Title', 'type' => 'raw', ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function getClassRow( $data ) {
			if( $data->description ) return "warning";
		}
		function formatIndicatorID( $data ) {
			return CommonLib::numberFormat( $data['indicator_id'] );
		}
		function formatName( $data ) {
			$out = "";
			
			if( $data->country ) {
				$out .= $data->country->getImgFlag('shiny', 16) . ' ';
			}
			
			$out .= $data->indicatorName;
			
			return $out;
		}
	}

?>
