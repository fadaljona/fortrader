<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );

	final class ContestMemberOrdersHistoryGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMemberOrdersHistoryGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "idOrder" => $data->id ) ';
		public $selectableRows = 2;
		public $member;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
				'id' => $this->w,
			);
			$this->setId( $this->w );
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'class' => 'ACECheckBoxGridColumn',  'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-480' ),  'htmlOptions' => Array( 'class' => 'checkbox-column hidden-480' )),
				Array( 'value' => ' $this->grid->formatTime( $data ) ', 'header' => 'Time', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $data->ORDER_TICKET ', 'header' => 'Order', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'value' => ' $data->SYMBOL ', 'header' => 'Symbol', 'headerHtmlOptions' => Array( 'class' => 'c04' )),
				Array( 'value' => ' $data->ORDER_TYPE ', 'header' => 'Type', 'headerHtmlOptions' => Array( 'class' => 'c05 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $data->ORDER_VOLUME ', 'header' => 'Volume', 'headerHtmlOptions' => Array( 'class' => 'c07 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' CommonLib::numberFormat( $data->ORDER_PRICE, 4 ) ', 'header' => 'Price', 'headerHtmlOptions' => Array( 'class' => 'c08 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $data->ORDER_SL ', 'header' => 'S/L', 'headerHtmlOptions' => Array( 'class' => 'c06 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $data->ORDER_TP ', 'header' => 'T/P', 'headerHtmlOptions' => Array( 'class' => 'c07 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $data->ORDER_ENTRY ', 'header' => 'State', 'headerHtmlOptions' => Array( 'class' => 'c06 hidden-480' ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatTime( $data ) {
			$DT = date( "Y.m.d H:i", $data->ORDER_TIME );
			return $DT;
		}
		function formatSwap( $swap ) {
			$class = $swap < 0 ? 'text-error' : '';
			$label = CHtml::tag( "span", Array( 'class' => $class ), $swap );
			return $label;
		}
		function renderTableFooter() {
			require dirname( __FILE__ ).'/contestMemberOrdersHistory/footer.php';
		}
	}

?>