<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminContestsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wContestsGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idContest" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->currentLanguageI18N->name ', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $this->grid->formatBegin( $data ) ', 'header' => 'Begin', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatBegin( $data ) {
			require dirname( __FILE__ ).'/adminContests/begin.php';
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$date = date( "d.m.Y", $time );
			return $date;
		}
	}

?>