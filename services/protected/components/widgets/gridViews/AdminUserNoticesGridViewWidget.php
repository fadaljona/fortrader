<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminUserNoticesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wUserNoticesGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idNotice" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->user->user_login ', 'header' => 'To' ),
				Array( 'value' => ' $data->getIcon()." ".$data->getHTMLMessage( 200 ) ', 'type' => 'raw', 'header' => 'Message' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>