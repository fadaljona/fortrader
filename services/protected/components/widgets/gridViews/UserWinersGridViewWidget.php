<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );


	final class UserWinersGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wUserWinersGridView';
		public $class = '';
		public $rowCssClassExpression = '';
		public $rowHtmlOptionsExpression = ' Array( "data-idMember" => $data->id ) ';
		public $selectableRows = 1;
		public $itemsCssClass = "dataTable items";
	
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
	
		protected function getColumns() {
			$columns = Array();
		
			$columns[] = Array( 
				'value' => ' CHtml::link( CHtml::encode( $data->contest->name ), $data->contest->singleURL ) ', 
				'header' => 'Contest', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col width80percent' ), 
				'htmlOptions' => Array( 
					'class' => 'calendar_event',
					'data-text' => Yii::t($this->NSi18n, 'Contest')
				)
			);
			
			$columns[] = Array( 
				'value' => ' $data->winer->place . " " . Yii::t( "lower", "place" ) ', 
				'header' => 'Place', 
				'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col ' ),
				'htmlOptions' => Array( 
					'class' => 'calendar_event',
					'data-text' => Yii::t($this->NSi18n, 'Place')
				)
			);
			

			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		
		
	}

?>