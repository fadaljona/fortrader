<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminCategoryRatingPostGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminCommonGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->categoryRatingPost->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'header' => 'Term name', 'name' => 'term_order', 'value' => '$data->termTaxonomy->term->name'),
				Array( 'name' => 'categoryRatingPost.ID'),
				Array( 'name' => 'object_id', 'header' => 'Post title', 'value' => '$data->categoryRatingPost->post_title'),
				Array( 'header' => 'Title', 'value' => '$data->categoryRatingPost->title'),
				Array( 'name' => 'term_taxonomy_id', 'header' => 'In rating?', 'value' => '$data->categoryRatingPost->ratingData ? $data->categoryRatingPost->ratingData->inRating : 1'),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>