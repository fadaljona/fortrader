<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.ContestMembersActionsGridColumn' );

	final class ContestMemberInfoVerticalGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMemberInfoVerticalGridView';
		public $class = 'iGridView i05';
		public $rowCssClassExpression = ' $this->getRowClass( $data ) ';
		public $rowHtmlOptionsExpression = ' Array( "idMember" => $data->id ) ';
		public $selectableRows = 1;
		public $itCurrentModel;
		function init() {
			if( !$this->itCurrentModel and !Yii::App()->user->checkAccess( 'contestMemberControl' )){
				$this->selectableRows = 0;
				$this->class .= ' i06';
			}
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getRowClass( $data ) {
			if( $data->stats->last_error_code == -255 ) {
				return 'iTr i01';
			}
		}
		
		

		
		
		
		
		
		public function renderItems()
		{
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
			{
				echo "<table class=\"{$this->itemsCssClass}\">\n";
				//$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}
			else
				$this->renderEmptyText();
		}
		public function renderTableHeader()
		{
			echo "<thead>\n";

			echo "<tr>\n";
			foreach($this->columns as $column)
				if( isset($column->header) && $column->header ) echo '<th>'.$column->header.'</th>';
				else echo '<th></th>';
			echo "</tr>\n";

			echo "</thead>\n";
		}
		public function renderTableBody()
		{
			$data=$this->dataProvider->getData();
			$n=count($data);
			$rows=count($this->columns);
			echo "<tbody>\n";

			if($n>0)
			{
				for($row=0;$row<$rows;++$row)
					$this->renderTableRow($row);
			}
			else
			{
				echo '<tr><td colspan="'.count($n+1).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
			echo "</tbody>\n";
		}
		public function renderTableRow($row)
		{
			$htmlOptions=array();
			$n=count($data);
			echo CHtml::openTag('tr', $htmlOptions)."\n";
			for( $i=0; $i<$n+1; $i++   ){
				if( $i==0 ) echo '<td>'.$this->columns[$row]->header.'</td>';
				$this->columns[$row]->renderDataCell($i);
			}
			echo "</tr>\n";
		}
		
		
		
		
		protected function getColumns() {
			$columns = Array();
			
			$columns[] = Array( 'value' => ' $this->grid->formatName( $data ) ', 'type' => 'raw', 'header' => 'Trader', 'headerHtmlOptions' => Array( 'class' => 'c01' ));
			$columns[] = Array( 'value' => ' $this->grid->formatAccountNumber( $data ) ', 'header' => 'Account number', 'headerHtmlOptions' => Array( 'class' => 'c01' ));
			$columns[] = Array( 'value' => ' $this->grid->formatBroker( $data ) ', 'type' => 'raw', 'header' => 'Broker', 'headerHtmlOptions' => Array( 'class' => 'c01' ));
			$columns[] = Array( 'value' => ' $this->grid->formatPlace( $data ) ', 'header' => 'Rank', 'headerHtmlOptions' => Array( 'class' => 'c02' ), 'htmlOptions' => Array( 'class' => 'iNoWrap' ));
			$columns[] = Array( 'value' => ' $this->grid->formatBalance( $data ) ', 'header' => 'Balance', 'headerHtmlOptions' => Array( 'class' => 'c03' ), 'htmlOptions' => Array( 'class' => ' iNoWrap' ));
			$columns[] = Array( 'value' => ' $this->grid->formatGain( $data ) ', 'header' => 'Gain', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c04 ' ), 'htmlOptions' => Array( 'class' => 'iNoWrap' ));
			$columns[] = Array( 'value' => ' $this->grid->formatDrow( $data ) ', 'header' => 'Drow', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c05 ' ), 'htmlOptions' => Array( 'class' => ' iNoWrap' ));
			$columns[] = Array( 'value' => ' CommonLib::numberFormat( $data->stats->countTrades ) ', 'header' => 'Tradese', 'headerHtmlOptions' => Array( 'class' => 'c05 ' ), 'htmlOptions' => Array( 'class' => ' iNoWrap' ));
			$columns[] = Array( 'value' => ' CommonLib::numberFormat( $data->stats->countOpen ) ', 'header' => 'Open', 'headerHtmlOptions' => Array( 'class' => 'c05 ' ), 'htmlOptions' => Array( 'class' => ' iNoWrap' ));
			$columns[] = Array( 'value' => ' $this->grid->formatStatus( $data ) ', 'header' => 'Status', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c06 ' ), 'htmlOptions' => Array( 'class' => '' ));
			$columns[] = Array( 'value' => ' $this->grid->formatUpdate( $data ) ', 'header' => 'Updated', 'headerHtmlOptions' => Array( 'class' => 'c07 ' ), 'htmlOptions' => Array( 'class' => '' ));
			
			$columns[] = Array( 'value' => ' $this->grid->formatEquity( $data ) ', 'header' => 'Equity', 'headerHtmlOptions' => Array( 'class' => 'c01' ));
			$columns[] = Array( 'value' => ' $this->grid->formatProfit( $data ) ', 'header' => 'Profit', 'headerHtmlOptions' => Array( 'class' => 'c02' ));
			$columns[] = Array( 'value' => ' $this->grid->formatDeposit( $data ) ', 'header' => 'Deposits', 'headerHtmlOptions' => Array( 'class' => 'c03' ));
			$columns[] = Array( 'value' => ' $this->grid->formatWithdrawals( $data ) ', 'header' => 'Withdrawals', 'headerHtmlOptions' => Array( 'class' => 'c04' ), 'htmlOptions' => Array( 'class' => '' ));
			
			$columns[] = Array( 'value' => ' $this->grid->formatLeverage( $data ) ', 'type'=>'html', 'header' => 'Leverage', 'headerHtmlOptions' => Array( 'class' => 'c04' ), 'htmlOptions' => Array( 'class' => '' ));
			
			$columns[] = Array( 'value' => ' $this->grid->formatTradeTime( $data ) ', 'header' => 'Avg. Trade Time', 'headerHtmlOptions' => Array( 'class' => 'c04' ), 'htmlOptions' => Array( 'class' => '' ));
			
			$columns[] = Array( 'value' => ' $this->grid->formatBWTrade( $data->stats->bestTrade ) ', 'header' => 'Best Trade', 'headerHtmlOptions' => Array( 'class' => 'c04' ), 'htmlOptions' => Array( 'class' => '' ));
			
			$columns[] = Array( 'value' => ' $this->grid->formatBWTrade( $data->stats->worstTrade ) ', 'header' => 'Worst Trade', 'headerHtmlOptions' => Array( 'class' => 'c04' ), 'htmlOptions' => Array( 'class' => '' ));
			
			$columns[] = Array( 'value' => ' $this->grid->formatProfitableTrades( $data ) ', 'type'=>'html', 'header' => 'Profitable Trades', 'headerHtmlOptions' => Array( 'class' => 'c04' ), 'htmlOptions' => Array( 'class' => '' ));
			
			if( $this->itCurrentModel or Yii::App()->user->checkAccess( 'contestMemberControl' )) {
				$columns[] = Array( 'header' => 'Edit Information', 'class' => 'ContestMembersActionsGridColumn');
			}
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatLeverage( $data ) {
			if( $data->stats->leverage == 0 ) return Yii::t( '*', 'no data' );
			if( $data->stats->leverageCganged ){
				ob_start();
				$this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->stats->leverageCgangedDate, 'class' => "" ));
				$changeDate = ob_get_clean();
				return '<div class="better_news_par">1:'.$data->stats->leverage.
					'<div class="tbl_tt"><b>Дата изменения:</b><br />'.$changeDate.'
					<br /><b>Плечо:</b> '.$data->stats->leverage.'</div></div>';
			}else{
				return '1:'.$data->stats->leverage;
			}
		}
		function formatTradeTime( $data ) {
			if( !$data->stats->avgTradeTimeSec ) return Yii::t( '*', 'no data' );
			$d = gmdate("j", $data->stats->avgTradeTimeSec);
			$h = gmdate("G", $data->stats->avgTradeTimeSec);
			$m = gmdate("i", $data->stats->avgTradeTimeSec);
			$outDate = '';
			if( $d-1 ) $outDate .= ($d-1).Yii::t( '*', 'IntervalDays' ).' ';
			if( $h ) $outDate .= $h.Yii::t( '*', 'IntervalHours' ).' ';
			if( $m ) $outDate .= $m.Yii::t( '*', 'IntervalMinutes' ).' ';
			return $outDate;
		}
		function formatBWTrade( $tradeSumm ) {
			if( !$tradeSumm ) return Yii::t( '*', 'no data' );
			return '$ '.number_format( $tradeSumm, 2, '.', ' ' );
		}
		function formatProfitableTrades( $data ) {
			if( !$data->stats->profitableTrades || !$data->stats->allTrades ) return Yii::t( '*', 'no data' );
			return number_format( ( $data->stats->profitableTrades/$data->stats->allTrades ) * 100, 2, '.', ' ' ).'%';
		}
		function formatEquity( $data ) {
			return $data->stats->equity !== null  ? CommonLib::numberFormat( $data->stats->equity ) : '';
		}
		function formatProfit( $data ) {
			return $data->stats->profit !== null ? '$ '.CommonLib::numberFormat( $data->stats->profit ) : '';
		}
		function formatDeposit( $data ) {
			return $data->stats->deposit !== null ? '$ '.CommonLib::numberFormat( $data->stats->deposit ) : '';
		}
		function formatWithdrawals( $data ) {
			return $data->stats->withdrawals !== null ? '$ '.CommonLib::numberFormat( $data->stats->withdrawals ) : '';
		}
		
		function formatName( $data ) {
			return CHtml::link( CHtml::encode( $data->user->showName ), $data->user->profileURL );
		}
		function formatAccountNumber( $data ) {
			return $data->accountNumber;
		}
		function formatBroker( $data ) {
			return CHtml::link( CHtml::encode( $data->server->broker->officialName ), $data->server->broker->getSingleURL() );
		}
		function formatPlace( $data ) {
			return $data->stats->place ? CommonLib::numberFormat( $data->stats->place ) : '';
		}
		function formatBalance( $data ) {
			return $data->stats->balance !== null ? '$ '.CommonLib::numberFormat( $data->stats->balance ) : '';
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatGain( $data ) {
			return $data->stats->gain !== null ? $this->formatPercent( $data->stats->gain ) : '';
		}
		function formatDrow( $data ) {
			return $data->stats->drowMax !== null ? $this->formatPercent( $data->stats->drowMax ) : '';
		}
		function formatStatus( $data ) {
			return $data->getLabelStatus();
		}
		function formatUpdate( $data ) {
			$data->stats->trueDT && $this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->stats->trueDT, 'class' => "iBold" ));
		}
	}

?>