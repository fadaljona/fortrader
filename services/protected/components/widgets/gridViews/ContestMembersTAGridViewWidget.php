<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.ContestMembersActionsGridColumn' );

	final class ContestMembersTAGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMembersGridView';
		public $class = 'iGridView i05';
		public $rowCssClassExpression = ' $this->getRowClass( $data ) ';
		public $rowHtmlOptionsExpression = ' Array( "idMember" => $data->id ) ';
		public $selectableRows = 1;
		public $itemsCssClass = "dataTable items";
		public $itCurrentModel;
		function init() {
			if( !$this->itCurrentModel and !Yii::App()->user->checkAccess( 'contestMemberControl' )){
				$this->selectableRows = 0;
				$this->class .= ' i06';
			}
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		protected function getRowClass( $data ) {
			if( $data->stats->last_error_code == -255 ) {
				return 'iTr i01';
			}
		}
		protected function getColumns() {
			$columns = Array();
			if( $this->itCurrentModel or Yii::App()->user->checkAccess( 'contestMemberControl' )) {
				$columns[] = Array( 'class' => 'ACECheckBoxGridColumn',  'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' ),  'htmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' ));
			}
			$columns[] = Array( 'value' => ' $data->contest->id ? CHtml::link( CHtml::encode( $data->contest->name ), $data->contest->singleURL ) : CHtml::link( Yii::t( $this->grid->NSi18n, "Monitoring" ), array("monitoring/index") ) ', 'header' => 'Contest/Monitoring', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c01 ' ));
			$columns[] = Array( 'value' => ' $data->contest->id ? CHtml::link( CHtml::encode( $data->accountNumber ), $data->singleURL ) : CHtml::link(CHtml::encode( $data->accountNumber ), array("monitoring/single", "id" => $data->id)) ', 'header' => 'Account', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c02 ' ));
			$columns[] = Array( 'value' => ' $this->grid->formatGain( $data ) ', 'header' => 'Gain', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c04 ' ), 'htmlOptions' => Array( 'class' => 'iNoWrap' ));
			$columns[] = Array( 'value' => ' $this->grid->formatStatus( $data ) ', 'header' => 'Status', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c07 hidden-480 ' ), 'htmlOptions' => Array( 'class' => 'hidden-480' ));
			$columns[] = Array( 'value' => ' $this->grid->formatUpdate( $data ) ', 'header' => 'Updated', 'headerHtmlOptions' => Array( 'class' => 'c08 hidden-phone' ), 'htmlOptions' => Array( 'class' => 'hidden-phone' ));
			if( $this->itCurrentModel or Yii::App()->user->checkAccess( 'contestMemberControl' )){
				$columns[] = Array( 'class' => 'ContestMembersActionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatGain( $data ) {
			return $data->stats && $data->stats->gain !== null ? $this->formatPercent( $data->stats->gain ) : '';
		}
		function formatStatus( $data ) {
			return $data->getLabelStatus();
		}
		function formatUpdate( $data ) {
			$data->stats && $data->stats->trueDT && $this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $data->stats->trueDT, 'class' => "iBold" ));
		}
	}

?>