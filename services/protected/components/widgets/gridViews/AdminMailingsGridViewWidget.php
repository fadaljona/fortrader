<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminMailingsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wMailingsGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idMailing" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $this->grid->formatDate( $data->createdDT ) ', 'header' => 'Date', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'value' => ' $data->currentLanguageI18N ? $data->currentLanguageI18N->name : "" ', 'header' => 'Name' ),
				Array( 'value' => ' $this->grid->formatProgress( $data->getProgressData() ) ', 'header' => 'Progress', 'type' => 'raw' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $dateTime ) {
			$time = strtotime( $dateTime );
			$day = date( "d", $time );
			$month = date( "F", $time );
			$month = strtolower( $month );
			$date = Yii::t( 'widgets/adminMailingsGridView', "{d} {$month}", Array( "{d}" => $day ));
			$yearDate = date( "Y", $time );
			$yearNow = date( "Y" );
			if( $yearDate != $yearNow ) $date .= " {$yearDate}";
			return $date;
		}
		function formatProgress( $progressData ) {
			if( @$progressData->stage ) {
				$stage = @$progressData->stage;
				$offset = @$progressData->offset * @$progressData->limit;
				$count = @$progressData->count * @$progressData->limit;
				return $count ? "({$stage})<br>{$offset} / {$count}" : "({$stage})";
			}
		}
	}

?>