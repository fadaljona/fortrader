<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.BrokersActionsGridColumn' );

	final class BrokersGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wBrokersGridView';
		public $class = 'iGridVie';
		public $rowHtmlOptionsExpression = ' Array( "data-idBroker" => $data->id ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "wBrokersGridViewTable";
		public $sortField;
		public $sortType;
		function init() {
			$cs=Yii::app()->getClientScript();
			$formHelpersBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.assets-static.formHelpers') );
			$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers-countries.flags.css');
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='2' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $this->grid->formatPlace( $data ) ', 
					'header' => '<span>' . Yii::t( '*', 'Place' ) . ' <span class="table_sort"></span></span>' , 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'style' => 'width: 250px;',
						'class' => $this->getSorting( 'place' ),
						'data-sortField' => 'place' 
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_1 ',
					)
				),
			
				Array( 
					'value' => ' $this->grid->formatActions( $data )', 
					'header' => 'Action', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'style' => 'width: 150px;',
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_2',
					)
				),
				/*Array( 
					'name' => 'year', 
					'header' => '<span>' . Yii::t( '*', 'Year' ) . ' <span class="table_sort"></span></span>', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'year' ),
						'sortField' => 'year' 
					), 
					'htmlOptions' => Array( 
						'class' => '' 
					)
				),*/
				Array( 
					'value' => ' $data->country ? $this->grid->formatCountry( $data->country ) : "" ', 
					'type' => 'raw', 
					'header' => '<span>' . Yii::t( '*', 'Country' ) . ' <span class="table_sort"></span></span>', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'country_name' ),
						'data-sortField' => 'country_name' 
					), 
					'htmlOptions' => Array( 
						'class' => '' 
					)
				),
				Array( 
					'value' => ' $this->grid->formatDollar( $data->minDeposit ) ', 
					'header' => '<span>' . Yii::t( '*', 'Min depo' ) . ' <span class="table_sort"></span></span>', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'minDepo' ),
						'data-sortField' => 'minDepo' 
					), 
					'htmlOptions' => Array( 
						'class' => ''
					)
				),
				Array( 
					'value' => ' $this->grid->formatRegulators( $data->regulators ) ', 
					'type' => 'raw', 
					'header' => 'Regulator', 
					'headerHtmlOptions' => Array( 
						'class' => '' 
					), 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_2' 
					)
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatPlace( $data ) {
			ob_start();
				require dirname( __FILE__ ).'/brokers/place.php';
			return ob_get_clean();
		}
		function formatActions( $data ) {
			ob_start();
				require dirname( __FILE__ ).'/brokers/actions.php';
			return ob_get_clean();
		}
		function formatCountry( $country ) {
			return "{$country->icon}";
		}
		function formatDollar( $value ) {
			return $value !== null ? '$'.CommonLib::numberFormat( $value ) : '';
		}
		function formatRegulators( $regulators ) {
			ob_start();
					require dirname( __FILE__ ).'/brokers/regulators.php';
			return ob_get_clean();
		}
	}

?>