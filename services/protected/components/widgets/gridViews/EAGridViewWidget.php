<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.EAActionsGridColumn' );

	final class EAGridViewWidget extends GridViewWidgetFrontBase {
		public $instance = 'list';
		public $w = 'wEAGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idEA" => $data->id ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				//return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array();
			$columns[] = Array( 
				'class' => 'ACECheckBoxGridColumn',  
				'headerHtmlOptions' => Array( 
					'class' => 'checkbox-column hidden-phone' 
				),  
				'htmlOptions' => Array( 
					'class' => 'checkbox-column hidden-phone' 
				)
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatName( $data ) ', 
				'header' => 'Name EA', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c01 '.$this->getSorting( 'name' ),
					//'sortField' => 'place' 
				), 
				'htmlOptions' => Array( 
					'class' => 'c01' 
				)
			);
			if( $this->instance != 'user' ) {
				$columns[] = Array( 
					'name' => 'creator', 
					'header' => 'Author', 
					'headerHtmlOptions' => Array( 
						'class' => 'c02 '.$this->getSorting( 'author' ),
						//'sortField' => 'place' 
					), 
					'htmlOptions' => Array( 
						'class' => 'c02' 
					)
				);
			}
			if( $this->instance == 'laboratory' ) {
				$columns[] = Array( 
					'value' => ' CommonLib::numberFormat( $data->countVersions ) ', 
					'header' => 'Versions',
					'headerHtmlOptions' => Array( 
						'class' => 'c05 hidden-480 '.$this->getSorting( 'versions' ),
						//'sortField' => 'place' 
					), 
					'htmlOptions' => Array( 
						'class' => 'c05 hidden-480' 
					)
				);
			}
			if( $this->instance == 'list' ) {
				$columns[] = Array( 
					'name' => 'license', 
					'header' => 'License', 
					'headerHtmlOptions' => Array( 
						'class' => 'c03 hidden-480 '/*.$this->getSorting( 'place' )*/,
						//'sortField' => 'place' 
					), 
					'htmlOptions' => Array( 
						'class' => 'c03 hidden-480' 
					)
				);
			}
			$columns[] = Array( 
				'value' => ' $this->grid->formatComments( $data ) ', 
				'type' => 'raw', 
				'header' => 'Comments', 
				'headerHtmlOptions' => Array( 
					'class' => 'c04 hidden-480 '.$this->getSorting( 'comments' ),
					//'sortField' => 'place' 
				), 
				'htmlOptions' => Array( 
					'class' => 'c04 hidden-480' 
				)
			);
			if( $this->instance == 'laboratory' ) {
				$columns[] = Array( 
					'value' => ' CommonLib::numberFormat( $data->countDownloads ) ', 
					'header' => 'Downloads',
					'headerHtmlOptions' => Array( 
						'class' => 'c05 hidden-480 '.$this->getSorting( 'downloads' ),
						//'sortField' => 'place' 
					), 
					'htmlOptions' => Array( 
						'class' => 'c05 hidden-480' 
					)
				);
			}
			$columns[] = Array( 
				'value' => ' CommonLib::numberFormat( $data->views ) ', 
				'header' => 'Popular',
				'headerHtmlOptions' => Array( 
					'class' => 'c05 hidden-480 '.$this->getSorting( 'popular' ),
					//'sortField' => 'place' 
				), 
				'htmlOptions' => Array( 
					'class' => 'c05 hidden-480' 
				)
			);
			if( $this->instance == 'list' ) {
				$columns[] = Array( 
					'value' => ' $this->grid->formatParams( $data->params ) ', 
					'type' => 'raw', 
					'header' => 'Params',
					'headerHtmlOptions' => Array( 
						'class' => 'c06 hidden-480'/*.$this->getSorting( 'place' )*/,
						//'sortField' => 'place' 
					), 
					'htmlOptions' => Array( 
						'class' => 'c06 hidden-480' 
					)
				);
			}
			$columns[] = Array( 
				'value' => ' $this->grid->formatRating( $data->stats->average ) ', 
				'header' => 'Rating',
				'headerHtmlOptions' => Array( 
					'class' => 'c06 hidden-480 '.$this->getSorting( 'rating' ),
					//'sortField' => 'place' 
				), 
				'htmlOptions' => Array( 
					'class' => 'c06 hidden-480' 
				)
			);
			
			//if( Yii::App()->user->checkAccess( 'eaControl' )) {
				$columns[] = Array( 'class' => 'EAActionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			//}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatName( $data ) {
			return CHtml::link( CHtml::encode( $data->name ), $data->getSingleURL() );
		}
		function formatStatus( $status ) {
			$classes = Array(
				'Running' => 'label label-success',
				'Discontinued' => 'label label-warning',
			);
			$class = @$classes[ $status ];
			$title = Yii::t( $this->NSi18n, $status );
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatComments( $data ) {
			$inner = CommonLib::numberFormat( $data->countMessages );
			$inner .= CHtml::tag( 'i', Array( 'class' => "iI i19" ), '', true );
			$out = CHtml::tag( 'a', Array( 'class' => 'iA i14', 'href' => $data->getSingleURL().'#wUsersConversationWidget' ), $inner, true );
			return $out;
		}
		function formatParams( $params ) {
			if( !$params ) return '';
			$out = '';
			foreach( explode( ',', $params ) as $i=>$param ) {
				if( $i > 0 ) {
					$out .= $i % 2 == 0 ? '<br>' : '&nbsp;&nbsp;';
				}
				$inner = CHtml::tag( 'i', Array( 'class' => "iI i18 i18_{$param}" ), '', true )." {$param}";
				$out .= CHtml::tag( 'div', Array( 'class' => 'iDiv i40' ), $inner, true );
			}
			return $out;
		}
		function formatRating( $average ) {
			if( $average === null ) return;
			require dirname( __FILE__ ).'/EA/review.php';
		}
	}

?>