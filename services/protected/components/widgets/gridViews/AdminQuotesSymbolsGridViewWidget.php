<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminQuotesSymbolsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wQuotesSymbolsGridView';
		public $class = 'iGridView';
		public $showTableOnEmpty=true;
		public $rowHtmlOptionsExpression = ' Array( "idSymbol" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'id', 'filter'=>false),
				Array( 'name' => 'name', 'header' => 'Name', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'name' => 'snalias', 'header' => 'NAlias', 'value' => ' $data->currentLanguageI18N->nalias ', 'headerHtmlOptions' => Array( 'class' => 'c03' )),
				Array( 'name' => 'desc', 'value' => ' mb_substr($data->currentLanguageI18N->desc, 0, 15) . "..." ', 'filter'=>false),
				Array( 'name' => 'sdefault', 'value' => ' $this->grid->formatList( $data->currentLanguageI18N->default ) ', 'header' => 'По умолчанию', 'class' => "components.dataColumns.QuotesSymbolsDefaultDataColumn", 'headerHtmlOptions' => Array( 'class' => 'c05' )),
				Array( 'name' => 'svisible', 'value' => ' $this->grid->formatList( $data->currentLanguageI18N->visible ) ', 'header' => 'Видимость', 'class' => "components.dataColumns.QuotesSymbolsVisibleDataColumn", 'headerHtmlOptions' => Array( 'class' => 'c06' )),

				Array( 'name' => 'category', 'value' => ' $this->grid->getNameCategory( $data->category ) ', 'header' => 'Категория', 'class' => "components.dataColumns.QuotesSymbolsCatDataColumn" ),
				
				Array( 'name' => 'sourceType', 'class' => "components.dataColumns.QuotesSymbolsSourceTypeDataColumn" ),
				Array( 'name' => 'tickName'),
				Array( 'name' => 'histName'),
				Array( 'name' => 'weightInCat', 'value' => ' $data->currentLanguageI18N->weightInCat ', 'filter'=>false),
                Array( 'name' => 'toInformer', 'filter'=>false),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( "quotesWidget",$column[ 'header' ]); unset( $column );
			return $columns;
		}

		function formatList( $data ) {
			$data = explode( ',', $data );
			$translate=array(
				'Yes'=>Yii::t( "quotesWidget",'Да'),
				'No'=>Yii::t( "quotesWidget",'Нет'),
				'Visible'=>Yii::t( "quotesWidget",'Показывать'),
				'Unvisible'=>Yii::t( "quotesWidget",'Скрыть')
			);
			foreach( $data as &$group ) $group =  (isset($translate[$group])?$translate[$group]:$group);
			$data = implode( ', ', $data );
			return $data;
		}
		
		function getNameCategory($id){
			return QuotesCategoryModel::getNameById($id);
		}
	}
