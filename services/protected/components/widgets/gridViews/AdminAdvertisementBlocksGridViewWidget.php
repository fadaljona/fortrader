<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminAdvertisementBlocksGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdvertisementBlocksGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idBlock" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $this->grid->foramtIP( $data->ip ) ', 'header' => 'IP' ),
				Array( 'value' => ' CommonLib::shorten( $data->agent, 50 ) ', 'header' => 'Agent' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function foramtIP( $ip ) {
			return $ip ? long2ip( $ip ) : '' ;
		}
	}

?>