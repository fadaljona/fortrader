<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class CurrencyRatesLeftHistArchiveGridViewWidget extends GridViewWidgetBase {
		public $w = 'wCurrencyRatesLeftHistArchiveGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		public $period;
		public $dayDataModel;
		public $delimiterDate;
		public $minData;
		public $maxData;
		public $dataType;
		
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderTableBody(){
			$data=$this->dataProvider->getData();
			$n=count($data);
			echo "<tbody><tr class='table_br'><td></td><td></td></tr>\n";

			if($n>0)
			{
				for($row=0;$row<$n;++$row)
					$this->renderTableRow($row);
			}
			else
			{
				echo '<tr><td colspan="'.count($this->columns).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
			echo "</tbody>\n";
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'header' => 'Date',
					'value' => ' $this->grid->formatDate( $data ) ', 
					'type' => 'raw', 				
				),
				Array( 
					'header' => 'Cbr course',
					'value' => ' $this->grid->formatCourse( $data ) ',
					'type' => 'raw', 					
				),	
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $data ) {
			$date = $data->date;
			$time = strtotime($data->date);
			
			if( (!$this->period && $this->dayDataModel->date == $date) || ($this->minData['date'] == $date || $this->maxData['date'] == $date) ){
				
				return CHtml::link(
					 CHtml::tag('strong', array(), date( 'd.m', $time) ),
					CurrencyRatesModel::getArchiveSingleURLByCode($data->currency->code, date( 'Y', $time ), date( 'n', $time ), date( 'j', $time ), $this->dataType),
					array()
				);
				
			}elseif( $this->period && $this->delimiterDate == $date ){
				return '.........';
			}else{
				
				return CHtml::link(
					date( 'd.m', strtotime($date)),
					CurrencyRatesModel::getArchiveSingleURLByCode($data->currency->code, date( 'Y', $time ), date( 'n', $time ), date( 'j', $time ), $this->dataType),
					array()
				);
				
			}
		}
		function formatCourse( $data ){
			if( $this->period && $this->delimiterDate == $data->date ) return '...........................';
			
			$spanClass = '';
			$diffVal = '';
			$diffVal = number_format( $data->diffValue, CommonLib::getDecimalPlaces( $data->diffValue, 4 ), ',', ' ' );
			/*if( $this->dataType == 'cbr' ){
				$value = $data->value;
			}elseif( $this->dataType == 'ecb' ){
				$value = 1 / $data->value;
			}*/
			$value = $data->value;
			
			$value = number_format( $value, CommonLib::getDecimalPlaces( $value, 4 ), ',', ' ' );
			if( $data->diffValue < 0 ){
				$spanClass = 'red_color';
			}
			if( $data->diffValue > 0 ){
				$spanClass = 'green_color';
				$diffVal = '+' . $diffVal;
			}
			
			
			if( (!$this->period && $this->dayDataModel->date == $data->date) || ($this->minData['date'] == $data->date || $this->maxData['date'] == $data->date) ){
				return CHtml::tag('strong', array(), $value) . ' ' . CHtml::tag('span', array( 'class' => $spanClass ), $diffVal );
			}else{
				return $value . ' ' . CHtml::tag('span', array( 'class' => $spanClass ), $diffVal );
			}
			
			
		}
	}

?>