<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminContactsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wContactsGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "idContact" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->position ', 'header' => '#', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'value' => ' $data->post ', 'header' => 'Post', 'headerHtmlOptions' => Array( 'class' => '' )),
				Array( 'value' => ' $data->name ', 'header' => 'Name', 'headerHtmlOptions' => Array( 'class' => '' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>