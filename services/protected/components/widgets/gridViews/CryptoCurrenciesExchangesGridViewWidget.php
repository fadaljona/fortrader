<?php
Yii::import('widgets.gridViews.base.GridViewWidgetBase');
Yii::import('gridColumns.CryptoCurrenciesExchangesColumn');

final class CryptoCurrenciesExchangesGridViewWidget extends GridViewWidgetBase
{
    public $w = 'wCryptoCurrenciesExchangesGridView';
    public $rowHtmlOptionsExpression = ' array( "data-idObj" => $data->id) ';

    public function init()
    {
        $this->htmlOptions = array(
            'class' => "{$this->class} grid-view {$this->w}",
        );
            
        parent::init();
    }

    public function renderItems()
    {
        if ($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty) {
            echo "<div class=\"crypto-exch__header\"><table class=\"crypto-exch__thead\">\n";
            $this->renderTableHeader();
            echo "</table></div>\n";

            echo "<div class=\"crypto-exch__body wide_scroll js_scrolling\"><table class=\"crypto-exch__tbody\">\n";
            ob_start();
            $this->renderTableBody();
            $body=ob_get_clean();
            $this->renderTableFooter();
            echo $body; // TFOOT must appear before TBODY according to the standard.
            echo "</table></div>";
        } else {
            $this->renderEmptyText();
        }
    }

    protected function getColumns()
    {
        $columns = array(
            array(
                'header' => $this->formatRowNumHeader(),
                'value' => '$this->grid->formatRowNum( $row )',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'exch-col_num'
                ),
                'headerHtmlOptions' => array(
                    'class' => 'exch-col_num'
                )
            ),
            array(
                'header' => $this->formatHeader('Source'),
                'value' => '$this->grid->formatSourceVal( $data )',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'exch-col_from'
                ),
                'headerHtmlOptions' => array(
                    'class' => 'exch-col_from'
                )
            ),
            array(
                'header' => $this->formatHeader('Currency'),
                'value' => '$this->grid->formatCurrencyVal( $data )',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'exch-col_currency'
                ),
                'headerHtmlOptions' => array(
                    'class' => 'exch-col_currency'
                )
            ),
            array(
                'header' => $this->formatHeader('Volume24H'),
                'value' => '$data->stats ?
                    $this->grid->formatData( "$" . number_format($data->stats->volume, 0, ".", ",") ) :
                    ""',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'exch-col_amounth'
                ),
                'headerHtmlOptions' => array(
                    'class' => 'exch-col_amounth'
                )
            ),
            array(
                'header' => $this->formatHeader('Price'),
                'value' => '$data->stats ?
                    $this->grid->formatData( "$" . number_format($data->stats->price, CommonLib::getDecimalPlaces($data->stats->price), ".", ",") ) :
                    ""',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'exch-col_price'
                ),
                'headerHtmlOptions' => array(
                    'class' => 'exch-col_price'
                )
            ),
            array(
                'header' => $this->formatHeader('Volume24HPerCent'),
                'value' => '$data->stats ?
                    $this->grid->formatData(
                        number_format(
                            $data->stats->volumePerCent, CommonLib::getDecimalPlaces($data->stats->volumePerCent),
                            ".",
                            ","
                        ) . "%"
                    ) :
                    ""',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => 'exch-col_amountp'
                ),
                'headerHtmlOptions' => array(
                    'class' => 'exch-col_amountp'
                )
            )
        );

        foreach ($columns as &$column) {
            $column['class'] = 'CryptoCurrenciesExchangesColumn';
            if (isset($column[ 'header' ])) {
                $column[ 'header' ] = Yii::t($this->NSi18n, $column[ 'header' ]);
            }
        }
        
        unset($column);
        return $columns;
    }

    public function formatData($val)
    {
        return CHtml::tag('div', array('class' => 'exch-tbody__data'), $val);
    }

    public function formatCurrencyVal($data)
    {
        return CHtml::link(
            $data->ourSymbol->cryptoCurrencyTitle,
            $data->ourSymbol->singleURL,
            array('class' => 'exch-tbody__link', 'target' => '_blank')
        );
    }

    public function formatSourceVal($data)
    {
        return CHtml::link(
            $data->exchange->title,
            $data->exchange->singleURL,
            array('class' => 'exch-tbody__link', 'target' => '_blank')
        );
    }

    public function formatHeader($header)
    {
        return CHtml::tag('span', array('class' => 'exch-thead_name'), Yii::t($this->NSi18n, $header));
    }

    public function formatRowNum($row)
    {
        return CHtml::tag(
            'div',
            array('class' => 'exch-tbody__data'),
            $this->dataProvider->pagination->currentPage * $this->dataProvider->pagination->pageSize + ($row+1)
        );
    }

    public function formatRowNumHeader()
    {
        return CHtml::tag('span', array('class' => 'exch-thead_name'), '#');
    }
}
