<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class CalendarInformerEventsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wCalendarEventsGridView';
		public $class = 'iGridView i05';
		public $rowCssClassExpression = ' $this->getRowClass( $data ) ';
		public $rowHtmlOptionsExpression = ' Array( "data-idEvent" => $data->id, "data-timeEvent" => strtotime( $data->dt ), "data-seriousEvent" => $data->serious ) ';
		public $selectableRows = 0;

		public $dataTimeZone = 3;

		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function getRowClass( $data ) {
			return $data->getRowStatus();
		}
		
		public function renderItems(){
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
			{
				echo "<table class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}
			else
				$this->renderEmptyText();
		}
		public function renderTableHeader(){
			if(!$this->hideHeader)
			{
				echo "<thead>\n";

				if($this->filterPosition===self::FILTER_POS_HEADER)
					$this->renderFilter();

				echo "<tr class='calendar-table__head'>\n";
				foreach($this->columns as $column)
					$column->renderHeaderCell();
				echo "</tr>\n";

				if($this->filterPosition===self::FILTER_POS_BODY)
					$this->renderFilter();

				echo "</thead>\n";
			}
			elseif($this->filter!==null && ($this->filterPosition===self::FILTER_POS_HEADER || $this->filterPosition===self::FILTER_POS_BODY))
			{
				echo "<thead>\n";
				$this->renderFilter();
				echo "</thead>\n";
			}
		}

		
		protected function getColumns() {
			$columns = Array(
			
				Array( 
					'value' => ' $this->grid->formatName( $data ) ', 
					'header' => 'Event', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col' ), 
					'htmlOptions' => array( 'class' => 'calendar_event', 'data-text' => $this->t('Event') )
				),
				Array( 
					'value' => ' $this->grid->formatDate( $data->dt ) ', 
					'header' => 'Time', 
					'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col' ), 
					'htmlOptions' => Array( 'class' => 'semibold', 'data-text' => $this->t('Time') )
				),
				Array( 
					'value' => ' $this->grid->formatTimeLeft( $data ) ', 
					'header' => 'Time left', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col' ), 
					'htmlOptions' => Array( 'class' => 'semibold timeLeft', 'data-text' => $this->t('Time left') )
				),
				Array( 
					'value' => ' $this->grid->formatImpact( $data ) ', 
					'header' => 'Impact', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col' ), 
					'htmlOptions' => Array( 'class' => 'box_label', 'data-text' => $this->t('Impact') )
				),
				Array(
					'value' => ' $data->getBeforeValueHtml() ', 
					'type'=> 'html' , 
					'header' => 'Previous',
					'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col' ), 
					'htmlOptions' => Array( 'class' => 'semibold', 'data-text' => $this->t('Previous') ),
					'cssClassExpression' => ' $data->getBeforeClass() ',
				),
				Array( 
					'value' => ' $data->getMbValue() ',
					'header' => 'MB', 
					'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col' ), 
					'htmlOptions' => Array( 'class' => 'semibold', 'data-text' => $this->t('MB') ),
				),
				Array( 
					'value' => ' $data->getFactValueHtml( ) ',
					'type'=>'html', 
					'header' => 'Fact',
					'headerHtmlOptions' => Array( 'class' => 'calendar-table__name-col' ), 
					'htmlOptions' => Array( 'class' => 'semibold', 'data-text' => $this->t('Fact') ),
					'cssClassExpression' => ' $data->getFactClass() ',
				),
			);

			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$tz = UserProfileModel::getTimezone('cookie') - $this->dataTimeZone;
			$time = $time + (60 * 60 * $tz);
			$date = date( "H:i", $time );
			return $date;
		}
		function formatTimeLeft( $data ) {
			return $data->getTimeLeft();
		}
		function formatName( $data ) {
			$out = "";
			
			if( $data->event && $data->event->country ) {
				$out .= Chtml::tag('span', array('class' => 'calendar_flag'), $data->event->country->getImgFlag('shiny', 16));
			}
			
			$inner = CHtml::encode( $data->indicatorName );
			$out .= CHtml::link( $inner, $data->event->singleURL );
			
			return $out;
		}
		function formatImpact( $data ) {
			if( !$data->serious ) return '';
			
			$type = $data->getTitleSerious();
			
			$class = '';
			switch( $data->serious ) {
				case 1:{ $class = 'label_green'; break; }
				case 2:{ $class = 'label_orange'; break; }
				case 3:{ $class = 'label_red'; break; }
			}
			
			return CHtml::tag('span', array( 'class' => $class ), Yii::t( '*', ucfirst( $type )));
		}
	}

?>