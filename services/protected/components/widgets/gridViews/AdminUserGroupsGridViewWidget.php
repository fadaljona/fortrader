<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminUserGroupsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wUserGroupsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idUserGroup" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'value' => ' $data->icon."&nbsp;".Yii::t( "'.$this->NSi18n.'", $data->name ) ', 'header' => 'Name', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'class' => 'gridColumns.UserGroupRightsGridColumn', 'header' => 'Rights' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}
	
?>