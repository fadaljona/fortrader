<?php
Yii::import('widgets.gridViews.base.GridViewWidgetBase');

final class CryptoCurrenciesPageGridViewWidget extends GridViewWidgetBase
{
    public $w = 'wCryptoCurrenciesPageGridView';
    public $itemsCssClass = 'crypto__table';
    public $rowHtmlOptionsExpression = ' array( "data-idObj" => $data->id, "class" => "crypto-cur__row" ) ';
    public $hideHeader = true;

    public function init()
    {
        $this->htmlOptions = array(
            'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
        );
            
        parent::init();
    }

    protected function getColumns()
    {
        $columns = array(
            array(
                'value' => ' $this->grid->formatTitle( $data ) ',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => "crypto-cur__column"
                )
            ),
            array(
                'value' => ' $this->grid->formatValue( $data ) ',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => "crypto-cur__column",
                )
            ),
            array(
                'value' => ' $this->grid->formatChange( $data ) ',
                'type' => 'raw',
                'htmlOptions' => array(
                    'class' => "crypto-cur__column"
                )
            )
        );

        foreach ($columns as &$column) {
            if (isset($column[ 'header' ])) {
                $column[ 'header' ] = Yii::t($this->NSi18n, $column[ 'header' ]);
            }
        }
        
        unset($column);
        return $columns;
    }


    public function formatTitle($data)
    {
        $outStr = '';
        $outStr .= CHtml::openTag('div', array('class' => 'crypto-cur__item'));
        if ($data->nameIcon) {
            $outStr .= CHtml::openTag('div', array('class' => 'crypto-cur__logo'));
                $outStr .= CHtml::image($data->iconSrc, $data->name);
            $outStr .= CHtml::closeTag('div');
        }
        
        $outStr .= CHtml::openTag('div', array('class' => 'crypto-cur__ratio'));
        $outStr .= CHtml::tag(
            'div',
            array('class' => 'crypto-cur__title'),
            CHtml::link($data->cryptoCurrencyTitle, $data->singleURL, array('target' => '_blank'))
        );
        if ($data->cryptoCurrencySubTitle) {
            $outStr .= CHtml::tag('div', array('class' => 'crypty-cur__subtitle'), $data->cryptoCurrencySubTitle);
        }
        $outStr .= CHtml::closeTag('div');
        
        $outStr .= CHtml::closeTag('div');
        return  $outStr;
    }

    public function getChangeClass($data)
    {
        $bid = $data->bid;
        if ($data->lastBid) {
            if ($bid - $data->lastBid > 0) {
                return 'rise';
            } elseif ($bid - $data->lastBid < 0) {
                return 'fall';
            }
        }
        return '';
    }

    public function formatValue($data)
    {
        $class = $this->getChangeClass($data);
        if ($class) {
            $class = 'crypto-val_' . $class . '_color';
        }

        return CHtml::tag(
            'div',
            array(
                'class' => 'crypto__val ' . $class . " pid-{$data->name}-bid",
                'data-precision' => $data->precision,
                'data-last-bid' => $data->lastBid
            ),
            $data->bid
        );
    }

    public function formatChange($data)
    {
        $class = $this->getChangeClass($data);
        if ($class) {
            $class = 'crypto-pos_' . $class;
        }

        $bid = $data->bid;

        if ($data->lastBid) {
            $Chg = $bid - $data->lastBid;
            $ChgPercent = $Chg / $data->lastBid * 100 ;

            $ChgPercent = number_format($ChgPercent, 2, '.', ' ');
            if ($Chg > 0) {
                $ChgPercent = '+'.$ChgPercent;
            }
        } else {
            $ChgPercent = $Chg = Yii::t("*", 'n/a');
        }

        return CHtml::tag(
            'div',
            array(
                'class' => $class . " pid-{$data->name}-chg",
            ),
            "{$ChgPercent}%"
        );
    }
}
