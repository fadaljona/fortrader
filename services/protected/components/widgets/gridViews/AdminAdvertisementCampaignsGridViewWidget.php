<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminAdvertisementCampaignsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdvertisementCampaignsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idCampaign" => $data->id ) ';
		public $mode = 'User';
		public $forStats = false;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Title' ),
				Array( 'value' => ' $this->grid->t( $data->getStatus() ) ', 'header' => 'Status', 'name' => 'status', 'class' => "components.dataColumns.AdvertisementCampaignStatusesDataColumn", ),
				Array( 'value' => ' CommonLib::numberFormat( $data->countShows ) ', 'header' => 'Shows' ),
				Array( 'value' => ' CommonLib::numberFormat( $data->countClicks ) ', 'header' => 'Clicks' ),
				Array( 'value' => ' $this->grid->formatCTR( $data ) ', 'header' => 'CTR' ),
			);
			if( $this->mode == 'Admin' ) {
				$columns[] = Array( 'value' => ' $data->owner->user_login ', 'header' => 'Owner' );
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatCTR( $data ) {
			if( $data->CTR === null ) return '';
			$value = CommonLib::numberFormat( $data->CTR );
			$value .= '%';
			return $value;
		}
	}

?>