<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminContestWinersGridViewWidget extends GridViewWidgetBase {
		public $w = 'wContestWinersGridView';
		public $class = 'iGridView i04';
		public $rowHtmlOptionsExpression = ' Array( "idWiner" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		
		function renderItems() {
			if( !$this->filter ) {
				parent::renderItems();
				return;
			}
			echo CHtml::openTag( 'form', Array( 'method' => 'POST', 'action' => $this->filterURL ));
			parent::renderItems();
			echo CHtml::closeTag( 'form' );
			$id = $this->getId();
			$lSearch = json_encode( Yii::t( '*', 'Search' ));
			echo "
				<script> 
					!function( $ ) {
						var inputs = $( '#{$id} .filters input' );
						var selects = $( '#{$id} .filters select' );
						var form = selects.closest( 'form' );
						var lSearch = {$lSearch};
						inputs.keydown( function ( event ) { if( event.which == 13 ) { form.submit(); }});
						selects.change( function ( event ) { form.submit(); });
						inputs.attr( 'placeholder', lSearch );
					}( window.jQuery );
				</script>
			";
		}
		
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->member->user->showName ', 'header' => 'User', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'name' => 'idContest', 'value' => ' $data->contest->currentLanguageI18N->name ', 'header' => 'Contest', 'class' => "components.dataColumns.ContestWinersContestDataColumn", 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'value' => '$data->prize', 'header' => 'Prize', 'headerHtmlOptions' => Array( 'class' => 'c03' )),
				Array( 'value' => '$data->place', 'header' => 'Place', 'headerHtmlOptions' => Array( 'class' => 'c04' )),
				
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>