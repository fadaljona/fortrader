<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminTradeAccountsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wTradeAccountsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idAccount" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'name' => 'accountNumber', 'header' => 'Account number', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'name' => 'groups', 'value' => ' $this->grid->formatGroups( $data->groups ) ', 'header' => 'Groups', 'class' => "components.dataColumns.TradeAccountGroupsDataColumn", 'headerHtmlOptions' => Array( 'class' => 'c03' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatGroups( $groups ) {
			$groups = explode( ',', $groups );
			foreach( $groups as &$group ) $group =  Yii::t( $this->NSi18n, $group );
			$groups = implode( ', ', $groups );
			return $groups;
		}
	}

?>