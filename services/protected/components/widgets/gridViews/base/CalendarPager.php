<?php
class CalendarPager extends CLinkPager{
	const CSS_FIRST_PAGE='calendar_page-link';
	const CSS_LAST_PAGE='calendar_page-link';
	const CSS_PREVIOUS_PAGE='calendar_page-link';
	const CSS_NEXT_PAGE='calendar_page-link';
	const CSS_SELECTED_PAGE='calendar_page-active';
	const CSS_INTERNAL_PAGE='';

	public $firstPageCssClass=self::CSS_FIRST_PAGE;
	public $lastPageCssClass=self::CSS_LAST_PAGE;
	public $previousPageCssClass=self::CSS_PREVIOUS_PAGE;
	public $nextPageCssClass=self::CSS_NEXT_PAGE;
	public $internalPageCssClass=self::CSS_INTERNAL_PAGE;
	public $selectedPageCssClass=self::CSS_SELECTED_PAGE;
	public $maxButtonCount=5;
	public $nextPageLabel;
	public $prevPageLabel;
	public $firstPageLabel;
	public $lastPageLabel;
	public $htmlOptions=array();
	private $prevButton = '';
	private $nextButton = '';
	private $lastButton = '';
	private $firstButton = '';

	public function init(){
		if($this->nextPageLabel===null)
			$this->nextPageLabel=Yii::t('pager','Next');
		if($this->prevPageLabel===null)
			$this->prevPageLabel=Yii::t('pager','Previous');
		if($this->firstPageLabel===null)
			$this->firstPageLabel=Yii::t('pager','First');
		if($this->lastPageLabel===null)
			$this->lastPageLabel=Yii::t('pager','Last');

		if(!isset($this->htmlOptions['id']))
			$this->htmlOptions['id']=$this->getId();
		if(!isset($this->htmlOptions['class']))
			$this->htmlOptions['class']='calendar-page__wr';
	}

	public function run(){
		$buttons=$this->createPageButtons();
		if(empty($buttons))
			return;
		
		
		echo CHtml::openTag('div', $this->htmlOptions );
			echo $this->firstButton;
			echo $this->prevButton ;

				echo implode("\n",$buttons);

			echo $this->nextButton;
			echo $this->lastButton;
		echo CHtml::closeTag('div');
		
	}
	protected function createPageButtons(){
		if(($pageCount=$this->getPageCount())<=1)
			return array();

		list($beginPage,$endPage)=$this->getPageRange();
		$currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons=array();
		
		// prev page
		if(($page=$currentPage-1)<0)
			$page=0;
		$this->prevButton = $this->createPrevButton($this->prevPageLabel,$page,$this->previousPageCssClass,$currentPage<=0);
		$this->firstButton = $this->createfirstButton($this->firstPageLabel,0,$this->firstPageCssClass,$currentPage<=0);
		
		// next page
		if(($page=$currentPage+1)>=$pageCount-1)
			$page=$pageCount-1;
		$this->nextButton = $this->createNextButton($this->nextPageLabel,$page,$this->nextPageCssClass,$currentPage>=$pageCount-1);
		$this->lastButton = $this->createfirstButton($this->lastPageLabel,$pageCount-1,$this->lastPageCssClass,$currentPage>=$pageCount-1);
		
		// first page
		$buttons[]=$this->createPageButton(1,0,'',$currentPage<=0,$currentPage==0);
		
		
		// internal pages
		for($i=$beginPage+1;$i<=$endPage;++$i)
			$buttons[]=$this->createPageButton($i+1,$i,$this->internalPageCssClass,false,$i==$currentPage);
		


		// last page
		//$buttons[]=$this->createPageButton($this->getPageCount(),$pageCount-1,$this->lastPageCssClass,$currentPage>=$pageCount-1,$currentPage==$this->getPageCount()-1);

		return $buttons;
	}

	protected function createPrevButton($label,$page,$class,$hidden){
		if( !$hidden ){
			return CHtml::link( 
				$label,
				$this->createPageUrl($page),
				array('class' => $class )
			);
		}
	}
	protected function createNextButton($label,$page,$class,$hidden){
		if( !$hidden ){
			return CHtml::link( 
				$label,
				$this->createPageUrl($page),
				array('class' => $class )
			);
		}
	}
	protected function createfirstButton($label,$page,$class,$hidden){
		if( !$hidden ){
			return CHtml::link( 
				$label,
				$this->createPageUrl($page),
				array('class' => $class )
			);
		}
	}
	protected function createPageButton($label,$page,$class,$hidden,$selected){
		if($selected){
			$class.=' '.$this->selectedPageCssClass;
			return CHtml::link($label,$this->createPageUrl($page), array('class' => 'calendar_page-link ' . $class));
		}
		return CHtml::link($label,$this->createPageUrl($page), array('class' => 'calendar_page-link ' . $class));
	}
}
