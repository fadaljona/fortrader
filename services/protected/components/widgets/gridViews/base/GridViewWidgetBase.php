<?
	Yii::import( 'controllers.base.ActionBase' );
	Yii::import( 'bootstrap.widgets.TbGridView' );
	
	abstract class GridViewWidgetBase extends TbGridView {
		public $class = 'iGridView';
		public $ins;
		public $NSi18n = '*';
		public $type = 'bordered';
		public $template = '{items}{pager}';
		public $ajaxUpdate = false;
		public $enableSorting = false;
		public $pagerCssClass = 'pagination pagination-centered';
		public $ajax = false;
		public $filterSelector = 'dummy';
		public $filterURL;
		public $rowHtmlOptionsExpression = ' Array( "data-id" => $data->id ) ';
		public $pager = Array(
			'class'=>'bootstrap.widgets.TbPager',
			'prevPageLabel' => 'Prev',
			'nextPageLabel' => 'Next',
			'maxButtonCount' => ActionBase::DPPagesCount,
		);
		
		abstract protected function getColumns();
		
		function init() {
			$this->columns = $this->getColumns();
			
			if( empty( $this->htmlOptions[ 'class' ])) {
				$w = $this->getW();
				$this->htmlOptions[ 'class' ] = "{$this->class} grid-view {$this->ins} {$w}";
			}
			
			parent::init();
		}
		protected function getW() {
			$class = get_class( $this );
			return "w{$class}";
		}
		function renderKeys() {
			
		}
		function renderItems() {
			if( !$this->filter ) {
				parent::renderItems();
				return;
			}
			echo CHtml::openTag( 'form', Array( 'method' => 'POST', 'action' => $this->filterURL ));
			parent::renderItems();
			echo CHtml::closeTag( 'form' );
			$id = $this->getId();
			$lSearch = json_encode( Yii::t( '*', 'Search' ));
			echo "
				<script> 
					!function( $ ) {
						var inputs = $( '#{$id} .filters input' );
						var selects = $( '#{$id} .filters select' );
						var form = inputs.closest( 'form' );
						var lSearch = {$lSearch};
						inputs.keydown( function ( event ) { if( event.which == 13 ) { form.submit(); }});
						selects.change( function ( event ) { form.submit(); });
						inputs.attr( 'placeholder', lSearch );
					}( window.jQuery );
				</script>
			";
		}
		function t( $text ) {
			return Yii::t( $this->NSi18n, $text );
		}
	}

?>