<?php
class Wp2Pager extends CLinkPager{
	const CSS_FIRST_PAGE='first';
	const CSS_LAST_PAGE='last';
	const CSS_PREVIOUS_PAGE='pagination-nav__prev';
	const CSS_NEXT_PAGE='pagination-nav__next';
	const CSS_SELECTED_PAGE='pagination-nav_active';
	const CSS_INTERNAL_PAGE='pagination-nav__item';

	public $firstPageCssClass=self::CSS_FIRST_PAGE;
	public $lastPageCssClass=self::CSS_LAST_PAGE;
	public $previousPageCssClass=self::CSS_PREVIOUS_PAGE;
	public $nextPageCssClass=self::CSS_NEXT_PAGE;
	public $internalPageCssClass=self::CSS_INTERNAL_PAGE;
	public $selectedPageCssClass=self::CSS_SELECTED_PAGE;
	public $maxButtonCount=5;
	public $nextPageLabel = '<svg xmlns="http://www.w3.org/2000/svg" width="10" height="18" viewBox="0 0 10 18"><path id="Forma_1" data-name="Forma 1" class="cls-1" d="M1170.13,971.813a0.687,0.687,0,0,1-.94,0,0.634,0.634,0,0,1,0-.9l8.22-7.914-8.22-7.915a0.634,0.634,0,0,1,0-.9,0.687,0.687,0,0,1,.94,0l8.68,8.364a0.635,0.635,0,0,1,0,.9Z" transform="translate(-1169 -954)"></path></svg>';
	public $prevPageLabel = '<svg xmlns="http://www.w3.org/2000/svg" width="10" height="18" viewBox="0 0 10 18"><path id="Forma_1" data-name="Forma 1" class="cls-1" d="M879.873,971.813a0.679,0.679,0,0,0,.934,0,0.62,0.62,0,0,0,0-.9L872.594,963l8.213-7.915a0.62,0.62,0,0,0,0-.9,0.679,0.679,0,0,0-.934,0l-8.68,8.364a0.623,0.623,0,0,0,0,.9Z" transform="translate(-871 -954)"/></svg>';
	public $firstPageLabel;
	public $lastPageLabel;
	public $htmlOptions=array('class' => 'pagination-nav');
	private $prevButton = '';
	private $nextButton = '';
	private $lastButton = '';
	private $firstButton = '';

	public function init(){
		if($this->nextPageLabel===null)
			$this->nextPageLabel=Yii::t('pager','Next');
		if($this->prevPageLabel===null)
			$this->prevPageLabel=Yii::t('pager','Previous');
		if($this->firstPageLabel===null)
			$this->firstPageLabel=Yii::t('pager','First');
		if($this->lastPageLabel===null)
			$this->lastPageLabel=Yii::t('pager','Last');

		if(!isset($this->htmlOptions['id']))
			$this->htmlOptions['id']=$this->getId();
		if(!isset($this->htmlOptions['class']))
			$this->htmlOptions['class']='navigation_box_2 clearfix d_none992';
	}

	public function run(){
		$buttons=$this->createPageButtons();
		if(empty($buttons))
			return;
		
		echo CHtml::openTag('div', $this->htmlOptions );
			//echo $this->firstButton;
			echo $this->prevButton ;
			echo CHtml::openTag('div', array( 'class' => 'pagination-nav__nums' ) );
				echo implode("\n",$buttons);
			echo CHtml::closeTag('div');
			echo $this->nextButton;
			//echo $this->lastButton;
		echo CHtml::closeTag('div');
		
	}
	protected function createPageButtons(){
		if(($pageCount=$this->getPageCount())<=1)
			return array();

		list($beginPage,$endPage)=$this->getPageRange();
		$currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons=array();
		
		// prev page
		if(($page=$currentPage-1)<0)
			$page=0;
		$this->prevButton = $this->createPrevButton($this->prevPageLabel,$page,$this->previousPageCssClass,$currentPage<=0);
		$this->firstButton = $this->createfirstButton($this->firstPageLabel,0,$this->firstPageCssClass,$currentPage<=0);
		
		// next page
		if(($page=$currentPage+1)>=$pageCount-1)
			$page=$pageCount-1;
		$this->nextButton = $this->createNextButton($this->nextPageLabel,$page,$this->nextPageCssClass,$currentPage>=$pageCount-1);
		$this->lastButton = $this->createfirstButton($this->lastPageLabel,$pageCount-1,$this->lastPageCssClass,$currentPage>=$pageCount-1);
		
		// first page
		$buttons[]=$this->createPageButton(1,0,$this->internalPageCssClass,$currentPage<=0,$currentPage==0);
		
		
		// internal pages
		for($i=$beginPage+1;$i<=$endPage;++$i)
			$buttons[]=$this->createPageButton($i+1,$i,$this->internalPageCssClass,false,$i==$currentPage);
		
		// last page
		//$buttons[]=$this->createPageButton($this->getPageCount(),$pageCount-1,$this->lastPageCssClass,$currentPage>=$pageCount-1,$currentPage==$this->getPageCount()-1);

		return $buttons;
	}

	protected function createPrevButton($label,$page,$class,$hidden){
		if( !$hidden ){
			return
				CHtml::openTag('div', array('class' => $class)) . 
					CHtml::link($label, $this->createPageUrl($page), array('class' => $this->internalPageCssClass)) .
				CHtml::closeTag('div')
			;
		}
	}
	protected function createNextButton($label,$page,$class,$hidden){
		if( !$hidden ){
			return
				CHtml::openTag('div', array('class' => 'pagination-nav__other')) . 
					CHtml::link('...', $this->createPageUrl($page), array('class' => $this->internalPageCssClass)) .
				CHtml::closeTag('div') .
				CHtml::openTag('div', array('class' => $class)) . 
					CHtml::link($label, $this->createPageUrl($page), array('class' => $this->internalPageCssClass)) .
				CHtml::closeTag('div')
			;
		}
	}
	protected function createfirstButton($label,$page,$class,$hidden){
		if( !$hidden ){
			return CHtml::link( 
				$label,
				$this->createPageUrl($page),
				array('class' => $class )
			);
		}
	}
	protected function createPageButton($label,$page,$class,$hidden,$selected){
		if ($selected) {
			$class .= ' '.$this->internalPageCssClass.' '.$this->selectedPageCssClass;
			return CHtml::link($label,$this->createPageUrl($page),array('class' => $class));
		}
		return CHtml::link($label,$this->createPageUrl($page),array('class' => $class));
	}
}
