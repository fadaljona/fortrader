<?
	Yii::import( 'controllers.base.ActionBase' );
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );
	
	abstract class GridViewWidgetFrontBase extends GridViewWidgetBase {
		public $type = 'striped bordered hover';
		public $pager = Array(
			'class'=>'bootstrap.widgets.TbPager',
			'prevPageLabel' => '<i class="icon-double-angle-left"></i>',
			'nextPageLabel' => '<i class="icon-double-angle-right"></i>',
			'maxButtonCount' => ActionBase::DPPagesCount,
		);
	}

?>