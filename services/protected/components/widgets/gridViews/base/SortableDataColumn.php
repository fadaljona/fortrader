<?php

Yii::import('zii.widgets.grid.CDataColumn');

class SortableDataColumn extends CDataColumn
{
	public $sortType;
	
	protected function renderHeaderCellContent()
	{
		if ($this->sortType == 'sorting_desc'){	
			echo '<div>' . $this->header . '<i class="fa fa-sort-desc" aria-hidden="true"></i></div>';
		}elseif($this->sortType == 'sorting_asc'){
			echo '<div>' . $this->header . '<i class="fa fa-sort-asc" aria-hidden="true"></i></div>';
		}else
		{
			if ($this->name !== null && $this->header === null)
			{
				if ($this->grid->dataProvider instanceof CActiveDataProvider)
					echo CHtml::encode($this->grid->dataProvider->model->getAttributeLabel($this->name));
				else
					echo CHtml::encode($this->name);
			}
			else
				parent::renderHeaderCellContent();
		}
	}
}
