<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminQuotesInformerLogGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminQuotesInformerLogGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = '  ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				array( 'name' => 'accessDate' ),
				array( 'name' => 'domain' ),
				array( 'name' => 'url' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>