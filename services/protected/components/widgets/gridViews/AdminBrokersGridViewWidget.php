<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminBrokersGridViewWidget extends GridViewWidgetBase {
		public $w = 'wBrokersGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idBroker" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'header' => 'Logo', 'type' => 'raw', 'value' => '$data->thumbImage', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ), 'htmlOptions' => Array( 'style' => 'text-align:center' ), ),
				Array( 'name' => 'officialName', 'header' => 'Official name', 'value' => '$data->brandName' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>