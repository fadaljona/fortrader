<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminUsersGridViewWidget extends GridViewWidgetBase {
		public $w = 'wUsersGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idUser" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'name' => 'user_login',
					'value' => 'CommonLib::shorten( $data->user_login, 25 ) ',
					'header' => 'Login', 
					'headerHtmlOptions' => Array( 'class' => 'c01' )
				),
				Array( 
					'name' => 'group',
					'class' => 'dataColumns.UserGroupsDataColumn', 
					'header' => 'Group',
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>