<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );

	final class ContestMemberAccountMonitorGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMemberAccountMonitorGridView';
		public $class = 'iGridView i05';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		protected function getColumns() {

			$columns = Array(
				Array(
                    'value' => '1 + $row + $this->grid->dataProvider->getPagination()->currentPage '
                            . '* $this->grid->dataProvider->getPagination()->pageSize',
                    'header' => '',
                    'type' => 'raw',
					'htmlOptions' => Array( 'class' => 'height', 'data-title' => $this->t('№') )
                ),
				Array( 
					'value' => ' $this->grid->formatTime( $data->dateAdd ) ', 
					'header' => 'Time',
					'htmlOptions' => Array( 'data-title' => $this->t('Time') )
				),
				Array( 
					'value' => ' $this->grid->formatComment( $data->comment ) ', 
					'type' => 'raw', 
					'header' => 'Comment', 
					'htmlOptions' => Array( 'data-title' => $this->t('Comment') )
				),
				Array( 
					'value' => ' $data->comment == "update" && $data->accountHistory > 0 ? "+".$data->accountHistory : $data->accountHistory ', 
					'header' => 'Account History', 
					'htmlOptions' => Array( 'data-title' => $this->t('Account History') )
				),
				Array( 
					'value' => ' $data->comment == "history" ? "-" : $data->openDeals ', 
					'header' => 'Open Deals', 
					'htmlOptions' => Array( 'data-title' => $this->t('Open Deals') )
				),
				Array( 
					'value' => ' $data->balance ', 
					'header' => 'Balance', 
					'htmlOptions' => Array( 'data-title' => $this->t('Balance') )
				),
				Array( 
					'value' => ' $data->equity ', 
					'header' => 'Equity', 
					'htmlOptions' => Array( 'data-title' => $this->t('Equity') )
				),
			
			);
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}

		function formatTime( $time ) {
			$DT = date( "Y.m.d H:i", strtotime($time) );
			return $DT;
		}
		function formatComment( $comment ) {
			switch ($comment) {
				case 'start':
					$returnStr = Yii::t( $this->NSi18n, 'Start account monitoring');
					break;
				case 'update':
					$returnStr = Yii::t( $this->NSi18n, 'Update');
					break;
				case 'history':
					$returnStr = Yii::t( $this->NSi18n, 'Download account history');
					break;
			}
			return $returnStr;
		}
	}

?>