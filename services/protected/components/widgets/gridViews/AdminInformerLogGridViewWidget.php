<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminInformerLogGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminInformerLogGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = '  ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				array( 'name' => 'accessDate' ),
				array( 'name' => 'domain' ),
				array( 'name' => 'url' ),
				array( 'header' => 'Style', 'value' => ' $data->stModel->title ' ),
				array( 'header' => 'Category', 'value' => ' $data->catModel->title ' ),
				array( 'name' => 'isHtml' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>