<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminEAMonitoringServersGridViewWidget extends GridViewWidgetBase {
		public $w = 'wEAMonitoringServersGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idServer" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>