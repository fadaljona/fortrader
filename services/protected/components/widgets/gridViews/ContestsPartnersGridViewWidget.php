<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ContestActionsGridColumn' );

	final class ContestsPartnersGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestsGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "data-idContest" => $data->id, "class" => "contest-table__row" ) ';
		public $selectableRows = 2;
		public $label;
		
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass} contest-table\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}

		public function renderTableBody(){
			$data=$this->dataProvider->getData();
			$n=count($data);

			if ($n>0) {
				for ($row = 0; $row < $n; ++$row) {
					$this->renderTableRow($row);
				}
			} else {
				echo '<tr><td colspan="'.count($this->columns).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
		}
		
		public function renderTableHeader(){
			if(!$this->hideHeader){
				echo "<thead>\n";

				if($this->filterPosition===self::FILTER_POS_HEADER) $this->renderFilter();
				
				echo "<tr class='contest-table__row'>";
				foreach($this->columns as $n => $column) {
					if (!$n) {
						$head_align = "left";
					} else {
						$head_align = "center";
					}
					$column->headerHtmlOptions = array('class' => 'data_'.($n+1), 'align' => $head_align);
					$column->renderHeaderCell();
				}
				echo "</tr>";

				if($this->filterPosition===self::FILTER_POS_BODY) $this->renderFilter();

				echo "</thead>\n";
			}elseif($this->filter!==null && ($this->filterPosition===self::FILTER_POS_HEADER || $this->filterPosition===self::FILTER_POS_BODY)){
				echo "<thead>\n";
				$this->renderFilter();
				echo "</thead>\n";
			}
		}
		
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => 'CHtml::link($data->title, $data->getLink())', 
					'header' => 'Contest',
					'type' => 'raw',
					'htmlOptions' => Array( 'class' => 'data_1', 'data-title' => Yii::t( $this->NSi18n, 'Contest title' ) )
				),
				Array( 
					'value' => ' "$" . $data->prize ', 
					'header' => 'Prize', 
					'htmlOptions' => Array('class' => 'data_2', 'align' => 'center', 'data-title' => Yii::t( $this->NSi18n, 'Prize amount' ) )
				),
				Array( 
					'value' => ' Yii::app()->dateFormatter->format( "d MMMM yyyy", strtotime($data->begin) ) ', 
					'header' => 'Begin', 
					'htmlOptions' => Array('class' => 'data_3', 'align' => 'center', 'data-title' => Yii::t( $this->NSi18n, 'Begin' ) )
				),
				Array( 
					'value' => ' $data->sponsor ', 
					'header' => 'Sponsor', 
					'htmlOptions' => Array('class' => 'data_4', 'align' => 'center', 'data-title' => Yii::t( $this->NSi18n, 'Sponsor' ) )
				)
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}

	}

?>