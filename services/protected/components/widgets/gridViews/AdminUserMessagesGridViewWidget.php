<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminUserMessagesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wUserMessagesGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idMessage" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $data->user->user_login ', 'header' => 'From' ),
				Array( 'value' => ' $data->getHTMLText( 100 ) ', 'header' => 'Message', 'type' => 'raw' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>