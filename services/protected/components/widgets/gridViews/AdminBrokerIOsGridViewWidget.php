<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminBrokerIOsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wBrokerIOsGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "idIO" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'name', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>