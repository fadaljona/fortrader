<!-- summary_list-->
<div class="section_offset">
	<ul class="summary_list clearfix">
	<?php 
		if( $this->member->stats->profit !== null ){
			echo CHtml::tag('li', array(), Yii::t( $this->NSi18n, "Profit" ) . ': ' . CHtml::tag('span', array( 'class' => 'green_color' ), CommonLib::numberFormat( $this->member->stats->profit ) ) );
		}
		if( $this->member->stats->deposit !== null ){
			echo CHtml::tag('li', array(), Yii::t( $this->NSi18n, "Deposit" ) . ': ' . CHtml::tag('span', array( 'class' => 'green_color' ), CommonLib::numberFormat( $this->member->stats->deposit ) ) );
		}
		if( $this->member->stats->balance !== null ){
			echo CHtml::tag('li', array(), Yii::t( $this->NSi18n, "Balance" ) . ': ' . CHtml::tag('span', array( 'class' => 'green_color' ), CommonLib::numberFormat( $this->member->stats->balance ) ) );
		}
		
		echo CHtml::tag( 'li', array(), CommonLib::numberFormat( $this->sums->sumSwap ) . CHtml::tag('span', array(), CommonLib::numberFormat( $this->sums->sumProfit ) ) );
	?>
	</ul>
</div>
<!--End of the summary_list-->