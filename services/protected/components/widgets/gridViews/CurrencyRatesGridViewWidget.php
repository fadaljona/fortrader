<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class CurrencyRatesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wBrokerInstrumentsGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		public $returnOnlyTrs = false;
		public $dayType;
		public $toCurrency;
		
		function init() {
			$cs=Yii::app()->getClientScript();
			$formHelpersBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.assets-static.formHelpers') );
			$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers-countries.flags.css');
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='2' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		public function renderTableBody(){
			$data=$this->dataProvider->getData();
			$n=count($data);
			if( !$this->returnOnlyTrs )echo "<tbody>\n";

			if($n>0)
			{
				for($row=0;$row<$n;++$row)
					$this->renderTableRow($row);
			}
			else
			{
				echo '<tr><td colspan="'.count($this->columns).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
			if( !$this->returnOnlyTrs )echo "</tbody>\n";
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $this->grid->formatCode( $data ) ', 
					'header' => 'Code' , 
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_1',
					)
				),
				Array( 'name' => 'lastDataNominal', 'header' => 'CurrNominal'),
				Array( 
					'value' => ' $this->grid->formatTitle( $data ) ',
					'header' => 'Title' ,
					'type' => 'raw', 
				),
				Array( 
					'value' => ' $this->grid->formatValue( $data ) ',
					'header' => $this->toCurrency->code,
					'type' => 'raw', 
				),
				Array( 
					'value' => ' $this->grid->formatvalueDiff( $data ) ',
					'header' => 'Value diff',
					'type' => 'raw', 
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatCode( $data ) {
			$code = CHtml::link(
				$data->code, 
				$data->singleURL,
				array()
			);
			if( $data->country ){
				return "{$data->country->getImgFlag("shiny", 16)} $code";
			}else{
				return $code;
			}
		}
		function formatTitle( $data){
			return CHtml::link( 
				CHtml::encode( $data->name ), 
				$data->singleURL,
				array( )
			);
		}
		function formatvalueDiff( $data ){
			if( $data->id == $this->toCurrency->id ){
				$diffValue = 0;
			}else{
				if( $this->dayType == 'today' ){
					$todayVal = $data->todayData->value / ($this->toCurrency->todayData->value / $this->toCurrency->todayData->nominal);
					$prevDayVal = $data->prevDayData->value / ($this->toCurrency->prevDayData->value / $this->toCurrency->prevDayData->nominal);
					$diffValue = $todayVal - $prevDayVal;
				}
				if( $this->dayType == 'tomorrow' ){
					$tomorrowVal = $data->tomorrowData->value / ($this->toCurrency->tomorrowData->value / $this->toCurrency->tomorrowData->nominal);
					$todayVal = $data->todayData->value / ($this->toCurrency->todayData->value / $this->toCurrency->todayData->nominal);
					$diffValue = $tomorrowVal - $todayVal;
				}
			}
			
			if( !$diffValue ) return '';
			if( $diffValue == 0 ) return $diffValue;
			$diffValue = number_format($diffValue, CommonLib::getDecimalPlaces( $diffValue, 4 ), '.', '');
			if( $diffValue < 0 )return "<span class='red_color'>" . $diffValue . "</span>";
			if( $diffValue > 0 )return "<span class='green_color'>+" . $diffValue . "</span>";
		}
		function formatValue( $data ){
			if( $this->dayType == 'today' ){
				$floatVal = $data->todayData->value / ($this->toCurrency->todayData->value / $this->toCurrency->todayData->nominal);
				return number_format($floatVal, CommonLib::getDecimalPlaces( $floatVal, 4 ), '.', '');
			}
			if( $this->dayType == 'tomorrow' ){
				$floatVal = $data->tomorrowData->value / ($this->toCurrency->tomorrowData->value / $this->toCurrency->tomorrowData->nominal);
				return number_format($floatVal, CommonLib::getDecimalPlaces( $floatVal, 4 ), '.', '');
			}
		}
	}

?>