<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class ContestMemberStatementGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMemberStatementGridView';
		public $class = 'iGridView i05';

		public $selectableRows = 1;
		public $closedDeals;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		
		
		
		public function renderItems()
		{
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
			{
				echo "<table class='items table table-striped  table-hover inner-table'>\n";
				//$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}
			else
				$this->renderEmptyText();
		}
		public function renderTableBody()
		{
			$data=$this->dataProvider->getData();
			$n=count($data);
			$rows=count($this->columns);
			echo "<tbody>\n";

			if($n>0)
			{
				for($row=0;$row<$rows;++$row)
					$this->renderTableRow($row);
			}
			else
			{
				echo '<tr><td colspan="'.count($n+1).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
			echo "</tbody>\n";
		}
		public function renderTableRow($row)
		{
			$htmlOptions=array();
			$n=count($data);
			echo CHtml::openTag('tr', $htmlOptions)."\n";
			for( $i=0; $i<$n+1; $i++   ){
				if( $i==0 ) echo '<td class="'.$this->columns[$row]->headerHtmlOptions['class'].'" >'.$this->columns[$row]->header.'</td>';
				$this->columns[$row]->renderDataCell($i);
			}
			echo "</tr>\n";
		}

		protected function getColumns() {
			$columns = Array();
			
			if($this->closedDeals){
				$columns[] = Array( 'value' => ' $data->ACCOUNT_ORDERS_HISTORY_TOTAL ', 'type' => 'raw', 'header' => 'Total Trades');
				$columns[] = Array( 'value' => ' $data->ACCOUNT_ORDERS_HISTORY_PENDING_TOTAL ', 'type' => 'raw', 'header' => 'Canceled Orders');
				$columns[] = Array( 'value' => ' $this->grid->formatTradeTime($data->ACCOUNT_ORDERS_HISTORY_TIME_AVERAGE) ', 'type' => 'raw', 'header' => 'Average Order Lifetime');
				$columns[] = Array( 'value' => ' $this->grid->formatSingMoney($data->ACCOUNT_ORDERS_HISTORY_BEST) ', 'type' => 'raw', 'header' => 'Best Trade');
				$columns[] = Array( 'value' => ' $this->grid->formatSingMoney($data->ACCOUNT_ORDERS_HISTORY_WORSE) ', 'type' => 'raw', 'header' => 'Worse Trade');
				$columns[] = Array( 'value' => ' $this->grid->formatSingMoney($data->ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE) ', 'type' => 'raw', 'header' => 'Average Profit');
				$columns[] = Array( 'value' => ' $this->grid->formatSingMoney($data->ACCOUNT_ORDERS_HISTORY_LOSS_AVERAGE) ', 'type' => 'raw', 'header' => 'Average Worse');
				$columns[] = Array( 'value' => ' $this->grid->formatPips($data->ACCOUNT_ORDERS_HISTORY_BEST_PIPS) ', 'type' => 'raw', 'header' => 'Best Trade In Pips');
				$columns[] = Array( 'value' => ' $this->grid->formatPips($data->ACCOUNT_ORDERS_HISTORY_WORSE_PIPS) ', 'type' => 'raw', 'header' => 'Worse Trade in Pips');
				$columns[] = Array( 'value' => ' CommonLib::numberFormat($data->ACCOUNT_ORDERS_HISTORY_PROFIT_PERCENT)."%" ', 'type' => 'raw', 'header' => 'Profit Trades (% of total)');
				$columns[] = Array( 'value' => ' number_format($data->ACCOUNT_ORDERS_HISTORY_PROFIT_FACTOR, 2, ".", " ") ', 'type' => 'raw', 'header' => 'Profit Factor');
				$columns[] = Array( 'value' => ' number_format($data->ACCOUNT_ORDERS_HISTORY_MAXLOT, 2, ".", " ") ', 'type' => 'raw', 'header' => 'Maximal Lot');
				$columns[] = Array( 'value' => ' number_format($data->ACCOUNT_ORDERS_HISTORY_MINLOT, 2, ".", " ") ', 'type' => 'raw', 'header' => 'Minimal Lot');
				$columns[] = Array( 'value' => ' number_format($data->ACCOUNT_ORDERS_HISTORY_LOT_AVERAGE, 2, ".", " ") ', 'type' => 'raw', 'header' => 'Average Lot');
				$columns[] = Array( 'value' => ' number_format($data->ACCOUNT_ORDERS_HISTORY_LOT_SUM, 2, ".", " ") ', 'type' => 'raw', 'header' => 'Total Lot');
				$columns[] = Array( 'value' => ' $data->ACCOUNT_ORDERS_HISTORY_DAY ', 'type' => 'raw', 'header' => 'Orders Average Per Day');
				$columns[] = Array( 'value' => ' $data->ACCOUNT_ORDERS_HISTORY_PROFIT_TOTAL ', 'type' => 'raw', 'header' => 'Total Profit Trades');
				$columns[] = Array( 'value' => ' $data->ACCOUNT_ORDERS_HISTORY_LOSS_TOTAL ', 'type' => 'raw', 'header' => 'Total Loss Trades');
				$columns[] = Array( 'value' => ' $this->grid->formatSummUsd($data->ACCOUNT_ORDERS_HISTORY_GROSS_LOSS) ', 'type' => 'raw', 'header' => 'Gross Loss');
				$columns[] = Array( 'value' => ' $this->grid->formatSummUsd($data->ACCOUNT_ORDERS_HISTORY_GROSS_PROFIT) ', 'type' => 'raw', 'header' => 'Gross Profit');
				$columns[] = Array( 'value' => ' $this->grid->formatSummUsd($data->ACCOUNT_ORDERS_HISTORY_PROFIT) ', 'type' => 'raw', 'header' => 'Total Net Profit');
			}else{
				$columns[] = Array( 'value' => ' $data->ACCOUNT_ORDERS_PENDING_TOTAL ', 'type' => 'raw', 'header' => 'Pending Orders');
				$columns[] = Array( 'value' => ' $data->ACCOUNT_ORDERS_TOTAL ', 'type' => 'raw', 'header' => 'Open Orders');
				$columns[] = Array( 'value' => ' $data->ACCOUNT_ORDERS_LOT_SUM ', 'type' => 'raw', 'header' => 'Total Lot Open Orders');
				$columns[] = Array( 'value' => ' $data->ACCOUNT_ORDERS_LOT_AVERAGE ', 'type' => 'raw', 'header' => 'Average Lot Open Orders', 'headerHtmlOptions' => Array( 'class' => 'last-row' ), 'htmlOptions' => Array( 'class' => 'last-row' ));
			}
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatTradeTime( $time ) {
			$d = gmdate("j", $time);
			$h = gmdate("G", $time);
			$m = gmdate("i", $time);
			$outDate = '';
			if( $d-1 ) $outDate .= ($d-1).Yii::t( '*', 'IntervalDays' ).' ';
			if( $h ) $outDate .= $h.Yii::t( '*', 'IntervalHours' ).' ';
			if( $m ) $outDate .= $m.Yii::t( '*', 'IntervalMinutes' ).' ';
			return $outDate;
		}
		function formatSingMoney( $money ) {
			if( $money ){
				if( $money > 0 ){
					return '+'.CommonLib::numberFormat( $money ).' $';
				}
				if( $money < 0 ){
					return CommonLib::numberFormat( $money ).' $';
				}
			}else return Yii::t( '*', 'no data' );
		}
		function formatPips( $pips ) {
			if( $pips ){
				if( $pips > 0 ){
					return '+'.number_format( $pips, 4, ".", " " );
				}else{
					return number_format( $pips, 4, ".", " " );
				}
			}else return Yii::t( '*', 'no data' );
		}
		function formatSummUsd( $usd ) {
			if( $usd ){
				return '$'.number_format( abs($usd), 0, ".", " " );
			}else return Yii::t( '*', 'no data' );
		}

	}

?>