<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.EAStatementsGridColumn' );

	final class EAStatementsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wEAStatementsGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "idStatement" => $data->id ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		public $idEA;
		public $idVersion;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array();
			$columns[] = Array( 
				'class' => 'ACECheckBoxGridColumn',  
				'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' ),  
				'htmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' )
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatUploaded( $data ) ', 
				'header' => 'Uploaded', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c02 hidden-640 ',//.$this->getSorting( 'version' ),
					//'sortField' => 'version' 
				), 
				'htmlOptions' => Array( 'class' => 'c02 hidden-640' )
			);
			if( !$this->idVersion ) {
				$columns[] = Array( 
					'value' => ' $this->grid->formatVersion( $data ) ', 
					'header' => 'Version', 
					'type' => 'raw', 
					'headerHtmlOptions' => Array( 
						'class' => 'c02 hidden-640 ',//.$this->getSorting( 'version' ),
						//'sortField' => 'version' 
					), 
					'htmlOptions' => Array( 'class' => 'c02 hidden-640' )
				);
			}
			$columns[] = Array( 
				'value' => ' $this->grid->formatDesc( $data ) ', 
				'header' => 'Description', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c02 ',//.$this->getSorting( 'version' ),
					//'sortField' => 'version' 
				), 
				'htmlOptions' => Array( 'class' => 'c02' )
			);
			$columns[] = Array( 
				'name' => 'symbol', 
				'header' => 'Symbol', 
				'headerHtmlOptions' => Array( 
					'class' => 'c02 ',//.$this->getSorting( 'version' ),
					//'sortField' => 'version' 
				), 
				'htmlOptions' => Array( 'class' => 'c02' )
			);
			$columns[] = Array( 
				'name' => 'period', 
				'header' => 'Period', 
				'headerHtmlOptions' => Array( 
					'class' => 'c02 hidden-640 ',//.$this->getSorting( 'version' ),
					//'sortField' => 'version' 
				), 
				'htmlOptions' => Array( 'class' => 'c02 hidden-640' )
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatGain( $data->gain ) ', 
				'header' => 'Gain', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c04 ',//.$this->getSorting( 'gain' ),
					'sortField' => 'gain' 
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap c04' )
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatDrow( $data->drow ) ', 
				'header' => 'Drow', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c05 hidden-1024 ',//.$this->getSorting( 'drow' ),
					'sortField' => 'drow' 
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap c05 hidden-1024' )
			);
			$columns[] = Array( 
				'name' => 'trades', 
				'header' => 'Trades', 
				'headerHtmlOptions' => Array( 
					'class' => 'c02 hidden-1200 ',//.$this->getSorting( 'version' ),
					//'sortField' => 'version' 
				), 
				'htmlOptions' => Array( 'class' => 'c02 hidden-1200' )
			);
			$columns[] = Array( 
				'value' => ' CommonLib::numberFormat( $data->countMessages ) ', 
				'header' => 'Comments', 
				'headerHtmlOptions' => Array( 
					'class' => 'c06 hidden-1400 ', //.$this->getSorting( 'tradese' ),
					//'sortField' => 'tradese',
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap hidden-1400' )
			);
			$columns[] = Array( 
				'value' => ' CommonLib::numberFormat( $data->views ) ', 
				'header' => 'Popular', 
				'headerHtmlOptions' => Array( 
					'class' => 'c06 hidden-1600 ', //.$this->getSorting( 'tradese' ),
					//'sortField' => 'tradese',
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap hidden-1600' )
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatType( $data->type ) ', 
				'header' => 'Type', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c07 hidden-640 '/*.$this->getSorting( 'type' )*/,
					//'sortField' => 'type' 
				), 
				'htmlOptions' => Array( 'class' => 'c07 hidden-640' )
			);
			//if( Yii::App()->user->checkAccess( 'eaControl' )) {
				$columns[] = Array( 'class' => 'EAStatementsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			//}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatUploaded( $data ) {
			return CHtml::link( CHtml::encode( $data->user->user_login ), $data->user->getProfileURL() );
		}
		function formatVersion( $data ) {
			return CHtml::link( CHtml::encode( $data->version->version ), $data->version->getSingleURL() );
		}
		function formatDesc( $data ) {
			$view = Yii::t( $this->NSi18n, "View" );
			return CHtml::link( $view, $data->getSingleURL() );
		}
		function formatPercent( $value ) {
			$title = CommonLib::numberFormat( $value );
			$title = "{$title}%";
			$class = $value ? $value > 0 ? 'text-success' : 'text-error' : '';
			if( $value > 0 ) $title = "+{$title}";
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatGain( $gain ) {
			return $gain !== null ? $this->formatPercent( $gain ) : '';
		}
		function formatDrow( $drow ) {
			return $drow !== null ? $this->formatPercent( $drow ) : '';
		}
		function formatType( $status ) {
			$classes = Array(
				'Demo' => 'label label-warning',
				'Real' => 'label label-success',
				'Backtest' => 'label label-info arrowed arrowed-righ',
			);
			$class = @$classes[ $status ];
			$title = Yii::t( $this->NSi18n, $status );
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
	}

?>