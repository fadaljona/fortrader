<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminMessagesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wMessagesGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idMessageKey" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'name' => 'key',
					'value' => ' $this->grid->formatValue( $data->getCurrentLanguageValue() ) ', 
					'header' => 'Message', 
					'headerHtmlOptions' => Array( 'class' => 'c01' )
				),
				Array( 'name' => 'ns', 'header' => 'Group' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatValue( $value ){
			$clean = strip_tags( $value );
			$value = strlen( $clean ) ? $clean : $value;
			return CommonLib::shorten( $value, 150 );
		}
	}

?>