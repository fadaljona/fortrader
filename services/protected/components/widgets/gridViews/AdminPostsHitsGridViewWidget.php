<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminPostsHitsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminPostsHitsGridViewWidget';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "postId" => $data->postId ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'postId', 'header' => 'post ID', 'filter'=>false ),
				//Array( 'name' => 'remoteHost', 'header' => 'Host' ),
				Array( 'name' => 'referer', 'header' => 'Referer', 'value' => ' $this->grid->formatReferer( $data ) ', 'type' => 'raw' ),
				Array( 'name' => 'keyword', 'header' => 'Keyword' ),
				Array( 'name' => 'visitDate', 'value' => ' $this->grid->formatDate( $data ) ', 'header' => 'Visit date', 'filter'=>false ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $data ) {
			return date( 'Y-m-d', strtotime( $data->visitDate ) );
		}
		function formatReferer( $data ) {
			if( $data->referer == 'direct' ) return $data->referer;
			return "<a href='{$data->referer}' target='_blank'>" . substr($data->referer, 0, 60 ) . "...</a>";
		}
	}

?>