<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminMailTplsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wMailTplsGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = ' Array( "idTpl" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $this->grid->t( $data->key ) ', 'header' => 'Key' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>