<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminBrokerLinksClickLogGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminQuotesInformerLogGridView';
		public $class = 'iGridView';
		public $rowHtmlOptionsExpression = '  ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				array( 'value' => ' $this->grid->formatName( $data ) ', 'type' => 'raw', 'header' => 'brandName' ),
				array( 'name' => 'brokerId' ),
				array( 'name' => 'type' ),
				array( 'name' => 'ip' ),
				array( 'name' => 'clickDate' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatName( $data ) {
			return print_r( $data->currentLanguageI18N->brandName, true );
		}
	}

?>