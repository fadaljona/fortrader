<!-- summary_list-->
<div class="section_offset">
	<ul class="summary_list clearfix">
	<?php 
		if( $this->member->stats->balance !== null ){
			echo CHtml::tag('li', array(), Yii::t( $this->NSi18n, "Balance" ) . ': ' . CHtml::tag('span', array( 'class' => 'green_color' ), CommonLib::numberFormat( $this->member->stats->balance ) ) );
		}
		if( $this->member->stats->equity !== null ){
			echo CHtml::tag('li', array(), Yii::t( $this->NSi18n, "Equity" ) . ': ' . CHtml::tag('span', array( 'class' => 'green_color' ), CommonLib::numberFormat( $this->member->stats->equity ) ) );
		}
		if( $this->member->stats->freeMargin !== null ){
			echo CHtml::tag('li', array(), Yii::t( $this->NSi18n, "Margin Free" ) . ': ' . CHtml::tag('span', array( 'class' => 'green_color' ), CommonLib::numberFormat( $this->member->stats->freeMargin ) ) );
		}
		if( $this->member->stats->margin !== null ){
			echo CHtml::tag('li', array(), Yii::t( $this->NSi18n, "Margin level" ) . ': ' . CHtml::tag('span', array( 'class' => 'green_color' ), CommonLib::numberFormat( $this->member->stats->margin ) . '%' ) );
		}
		
		echo CHtml::tag( 'li', array(), CommonLib::numberFormat( $this->sums->sumSwap ) . CHtml::tag('span', array(), CommonLib::numberFormat( $this->sums->sumProfit) ) );
	?>
	</ul>
</div>
<!--End of the summary_list-->