<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );

	final class ContestMembersRatingGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMembersRatingGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "data-idMember" => $data->id ) ';
		public $selectableRows = 2;
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			$this->setId( $this->w );
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='3' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		public function renderTableHeader(){
			if(!$this->hideHeader){
				echo "<thead>\n";

				if($this->filterPosition===self::FILTER_POS_HEADER) $this->renderFilter();
				
				echo '<tr class="competition_tbl_head">
					<th colspan="4" class="clearfix">' . Yii::t( $this->NSi18n, 'Rating Members Forex Contests' ) .'<i class="fa fa-line-chart" aria-hidden="true"></i></th>
				</tr>';

				echo "<tr>\n";
				foreach($this->columns as $column) $column->renderHeaderCell();
				echo "</tr>\n";

				if($this->filterPosition===self::FILTER_POS_BODY) $this->renderFilter();

				echo "</thead>\n";
			}elseif($this->filter!==null && ($this->filterPosition===self::FILTER_POS_HEADER || $this->filterPosition===self::FILTER_POS_BODY)){
				echo "<thead>\n";
				$this->renderFilter();
				echo "</thead>\n";
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $data->user ? CHtml::link( CHtml::encode( $data->user->showName ), $data->user->getProfileURL()) : $data->user->ID ', 
					'header' => 'Member', 
					'type' => 'raw',
					'htmlOptions' => Array( 'class' => 'col_name', 'data-title' => Yii::t( $this->NSi18n, 'Memeber' ) )
				),
				Array( 
					'value' => ' $this->grid->formatPlaces( $data ) ', 
					'header' => 'Prizes', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Prizes' ) )
				),
				Array( 
					'value' => ' $this->grid->formatWins( $data->sumContestPrizes ) ', 
					'header' => 'Winning amount', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Winning amount' ) )
				),
				Array( 
					'value' => ' $this->grid->formatAvgPlace( $data ) ', 
					'header' => 'Avg. place', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Avg. place' ) )
				),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatCountry( $country ) {
			//$name = Yii::t( $this->NSi18n, $country->name );
			//return "{$country->icon}&nbsp;{$name}";
			return "{$country->icon}";
		}
		function formatWins( $wins ) {
			if ( !$wins ) $wins = "0";
			return "\${$wins}";
		}
		function formatAwards( $data ) {
			$output = '';
			foreach( $data->user->memberContests as $memberContest ) {
				if( $memberContest->winer ) {
					$output .= $memberContest->winer->getIcon();
				}
			}
			return $output;
		}
		function formatPlaces( $data ) {
			$count = 0;
			foreach( $data->user->memberContests as $memberContest ) {
				if( $memberContest->winer ) {
					$count ++;
				}
			}
			return $count;
		}
		function formatAvgPlace( $data ) {
			$count = 0;
			$summ = 0;
			foreach( $data->user->memberContests as $memberContest ) {
				if( $memberContest->winer ) {
					$summ += $memberContest->winer->place;
					$count ++;
				}
			}
			return round($summ / $count);
		}
	}

?>