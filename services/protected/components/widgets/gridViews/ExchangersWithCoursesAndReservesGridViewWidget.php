<?php
Yii::import('widgets.gridViews.base.GridViewWidgetFrontBase');

final class ExchangersWithCoursesAndReservesGridViewWidget extends GridViewWidgetFrontBase
{
    public $w = '';
    public $class = 'fx_exchange-table';
    public $rowHtmlOptionsExpression = ' Array( "data-exchanger" => $data->exchanger->id, "class" => "ex__trow data-exchanger" . $data->exchanger->id ) ';
    public $itemsCssClass = "ex__table";

    public function init()
    {
        $this->htmlOptions = array(
            'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
        );
        parent::init();
    }
    public function renderItems()
    {
        if ($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
        {
            echo "<table class=\"{$this->itemsCssClass}\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
            $this->renderTableHeader();
            ob_start();
            $this->renderTableBody();
            $body=ob_get_clean();
            $this->renderTableFooter();
            echo $body; // TFOOT must appear before TBODY according to the standard.
            echo "</table>";
        } else {
            $this->renderEmptyText();
        }   
    }
    public function renderTableHeader()
    {
        if (!$this->hideHeader) {
            echo "<thead>\n";
            if ($this->filterPosition===self::FILTER_POS_HEADER) {
                $this->renderFilter();
            }
            echo "<tr class='ex__trow ex_header'>\n";
            foreach ($this->columns as $column) {
                $column->renderHeaderCell();
            }
            echo "</tr>\n";

            if ($this->filterPosition===self::FILTER_POS_BODY) {
                $this->renderFilter();
            }
            echo "</thead>\n";
        } elseif ($this->filter!==null && ($this->filterPosition===self::FILTER_POS_HEADER || $this->filterPosition===self::FILTER_POS_BODY)) {
            echo "<thead>\n";
            $this->renderFilter();
            echo "</thead>\n";
        }
    }
    protected function getColumns()
    {
        $columns = array(
            array(
                'header' => 'Exchanger',
                'value' => ' $this->grid->formatExchanger( $data->exchanger ) ',
                'type' => 'raw',
                'headerHtmlOptions' => array(
                    'class' => 'ex__thead column_reverse ex_decor'
                ),
                'htmlOptions' => array('class' => 'ex__tdata')
            ),
            array(
                'header' => 'You give',
                'value' => ' $this->grid->formatYouGive( $data ) ',
                'type' => 'raw',
                'headerHtmlOptions' => array(
                    'class' => 'ex__thead column_return'
                ),
                'htmlOptions' => array('class' => 'ex__tdata')
            ),
            array(
                'header' => 'You get',
                'value' => ' $this->grid->formatYouGet( $data ) ',
                'type' => 'raw',
                'headerHtmlOptions' => array(
                    'class' => 'ex__thead column_receive'
                ),
                'htmlOptions' => array('class' => 'ex__tdata')
            ),
            array(
                'header' => 'Reserve',
                'value' => ' $this->grid->formatReserve( $data ) ',
                'headerHtmlOptions' => array(
                    'class' => 'ex__thead column_reserv'
                ),
                'htmlOptions' => array('class' => 'ex__tdata')
            ),
            array(
                'header' => 'Rating',
                'value' => ' $data->exchanger->exchangerVoteStats ? 
                    $data->exchanger->exchangerVoteStats->votesCount . " / 10" : "0 / 10" ',
                'headerHtmlOptions' => array(
                    'class' => 'ex__thead column_review'
                ),
                'htmlOptions' => array('class' => 'ex__tdata')
            ),
        );
        foreach ($columns as &$column) {
            if (isset($column[ 'header' ])) {
                $column[ 'header' ] = Yii::t($this->NSi18n, $column[ 'header' ]);
            }
        }
        unset($column);
        return $columns;
    }
    public function formatReserve($data)
    {
        if ($data->toReserve) {
            return CommonLib::formatThousands($data->toReserve);
        }
        if ($data->courseExchangerReserve) {
            return CommonLib::formatThousands($data->courseExchangerReserve->amount);
        }
        return false;
    }
    public function formatYouGive($data)
    {
        return CHtml::link(
            CommonLib::formatThousands($data->fromAmount) . " " . $data->fromCurrencyModel->title,
            PaymentSystemCurrencyModel::getExchangeDirectionUrl(
                $data->fromCurrencyModel->slug,
                $data->toCurrencyModel->slug
            )
        );
    }
    public function formatYouGet($data)
    {
        return CommonLib::formatThousands($data->toAmount) . " " . CHtml::tag('span', array('class' => 'ex_payment'), $data->toCurrencyModel->title);
    }
    public function formatExchanger($exchanger)
    {
        ob_start();
        require dirname(__FILE__).'/exchangersWithCoursesAndReserves/exchanger.php';
        return ob_get_clean();
    }
}
