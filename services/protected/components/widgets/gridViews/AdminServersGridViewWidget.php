<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminServersGridViewWidget extends GridViewWidgetBase {
		public $w = 'wServersGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "idServer" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'id', 'header' => 'ID' ),
				Array( 'name' => 'name', 'header' => 'Title', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'name' => 'url', 'header' => 'URL', 'headerHtmlOptions' => Array( 'class' => 'c02' )),
				Array( 'value' => ' $data->broker->officialName ', 'header' => 'Broker', 'headerHtmlOptions' => Array( 'class' => 'c03' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>