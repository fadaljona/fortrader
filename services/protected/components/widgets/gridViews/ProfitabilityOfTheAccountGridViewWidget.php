<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class ProfitabilityOfTheAccountGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wProfitabilityOfTheAccountGridView';
		public $class = '';
		public $rowHtmlOptionsExpression = ' Array( ) ';
		public $selectableRows = 0;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		public $tabType = false;
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='2' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}

		protected function getColumns() {
			$columns = Array(
				array( 
					'header' => 'Daily',
					'value' => ' $this->grid->formatPercent( $data->monitoringStats->dayBalance, $data->stats->deposit ) ', 
					'type' => 'raw',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Daily' ) ),
				),
				array( 
					'header' => 'Weekly',
					'value' => ' $this->grid->formatPercent( $data->monitoringStats->weekBalance, $data->stats->deposit ) ', 
					'type' => 'raw',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Weekly' ) ),
				),
				array( 
					'header' => 'Monthly',
					'value' => ' $this->grid->formatPercent( $data->monitoringStats->monthBalance, $data->stats->deposit ) ', 
					'type' => 'raw',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Monthly' ) ),
				),
				array( 
					'header' => '3 Months',
					'value' => ' $this->grid->formatPercent( $data->monitoringStats->threeMonthsBalance, $data->stats->deposit ) ', 
					'type' => 'raw',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, '3 Months' ) ),
				),
				array( 
					'header' => '6 Months',
					'value' => ' $this->grid->formatPercent( $data->monitoringStats->sixMonthsBalance, $data->stats->deposit ) ', 
					'type' => 'raw',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, '6 Months' ) ),
				),
				array( 
					'header' => '9 Months',
					'value' => ' $this->grid->formatPercent( $data->monitoringStats->nineMonthsBalance, $data->stats->deposit ) ', 
					'type' => 'raw',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, '9 Months' ) ),
				),
				array( 
					'header' => 'Total',
					'value' => ' $this->grid->formatPercent( $data->monitoringStats->allDayBalance, $data->stats->deposit ) ', 
					'type' => 'raw',
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Total' ) ),
				),

			);
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	
		
		function formatPercent( $balance, $deposit ) {
			if( !$balance || !$deposit ) return Yii::t( $this->NSi18n, 'n/a' );
			$val = $balance/$deposit * 100;
			$printVal = number_format( $val, CommonLib::getDecimalPlaces( $val, 2 ), '.', '' );
			if( $val > 0 ){
				return CHtml::tag('span', array('class' => 'green_color'), '+' . $printVal . '%');
			}
			return CHtml::tag('span', array('class' => 'red_color'),  $printVal . '%');
		}
		
	}

?>