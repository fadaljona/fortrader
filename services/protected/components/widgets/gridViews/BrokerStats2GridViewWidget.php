<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class BrokerStats2GridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wBrokerStats2GridView';
		public $class = 'iGridView i05 i07';
		public $rowHtmlOptionsExpression = ' Array( "idStats" => $data->id ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => ' $this->grid->formatName( $data ) ', 'header' => 'Company', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c01', 'style' => "width:20%" )),
				Array( 'value' => ' $this->grid->formatLinks( $data ) ', 'header' => 'Clients', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c02', 'style' => "width:20%" )),
				Array( 'value' => ' $this->grid->formatMarks( $data ) ', 'header' => 'Polls', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c03 hidden-480', 'style' => "width:20%" ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $this->grid->formatReviews( $data ) ', 'header' => 'Reviews', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c04 hidden-480', 'style' => "width:20%" ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
				Array( 'value' => ' $this->grid->formatRedirects( $data ) ', 'header' => 'Transitions', 'type' => 'raw', 'headerHtmlOptions' => Array( 'class' => 'c05 hidden-480', 'style' => "width:20%" ), 'htmlOptions' => Array( 'class' => 'hidden-480' )),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatName( $data ) {
			return CHtml::link( CHtml::encode( $data->shortName ), $data->getSingleURL() );
		}
		function formatPair( $a, $b ) {
			$out = Array();
			if( $a ) {
				$today = Yii::t( '*', "Today" );
				$out[] = CHtml::tag( 'span', Array( 'class' => 'label label-success', 'title' => $today ), "+{$a}" );
				$out[] = "&nbsp;";
			}
			if( $b ) {
				$total = Yii::t( '*', "Total" );
				$out[] = CHtml::tag( 'span', Array( 'class' => 'label label-warning', 'title' => $total ), $b );
				$out[] = "&nbsp;";
			}
			return implode( $out );
		}
		function formatLinks( $data ) {
			return $this->formatPair( $data->stats2->dayCountLinks, $data->stats2->countLinks );
		}
		function formatMarks( $data ) {
			return $this->formatPair( $data->stats2->dayCountMarks, $data->stats2->countMarks );
		}
		function formatReviews( $data ) {
			return $this->formatPair( $data->stats2->dayCountReviews, $data->stats2->countReviews );
		}
		function formatRedirects( $data ) {
			return $this->formatPair( $data->stats2->dayCountRedirects, $data->stats2->countRedirects );
		}
	}

?>