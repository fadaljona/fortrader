<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class CurrencyRatesEcbArchiveGridViewWidget extends GridViewWidgetBase {
		public $w = 'wCurrencyRatesArchiveGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data["id"] ) ';
		public $returnOnlyTrs = false;
		
		public $year;
		public $mo;
		public $day;
		

		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				$fixedWidthTitle = '';
				if( !$this->day )
					$fixedWidthTitle = 'fixedWidthTitle';
				echo "<table data-item='2' class=\"tabl_quotes table_currency {$fixedWidthTitle}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $this->grid->formatCode( $data ) ', 
					'header' => 'Code' , 
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_1',
					)
				),
				Array( 
					'value' => ' $this->grid->formatTitle( $data ) ',
					'header' => 'Title' ,
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'Title' )
					),
					'headerHtmlOptions' => Array( 
						'class' => 'currency-title'
					)
				),
			);
			if( $this->day ){
				/*$columns[] = Array(
					'value' => ' $this->grid->formatValue( $data ) ',
					'header' => 'Course Ecb to EUR',
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'Course Ecb to EUR' )
					)
				);*/
				$columns[] = Array(
					'value' => ' $this->grid->formatFromValue( $data ) ',
					'header' => 'Course Ecb for 1 EUR',
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'Course Ecb from EUR' )
					)
				);
			}else{
				/*$columns[] = Array(
					'value' => ' $this->grid->formatMinMaxCourse( $data, "min", "to" ) ',
					'header' => 'min Course ecb to EUR',
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'min Course ecb to EUR' )
					)
				);*/
				/*$columns[] = Array(
					'value' => ' $this->grid->formatMinMaxCourse( $data, "max", "to" ) ',
					'header' => 'max Course ecb to EUR',
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'max Course ecb to EUR' )
					)
				);*/
				$columns[] = Array(
					'value' => ' $this->grid->formatMinMaxCourse( $data, "min", "from" ) ',
					'header' => 'min Course for 1 EUR',
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'min Course for 1 EUR' )
					)
				);
				$columns[] = Array(
					'value' => ' $this->grid->formatMinMaxCourse( $data, "max", "from" ) ',
					'header' => 'max Course for 1 EUR',
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'data-title' => Yii::t( $this->NSi18n, 'max Course for 1 EUR' )
					)
				);
			}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatMinMaxCourse( $data, $type = "min", $dest = 'to' ){
			if( $dest == 'to' ){
				if( $type == "min" ){
					$classColor = 'green_color';
					$date = date('d.m.Y', strtotime($data['minValDate']));
					$val = 1/$data['minVal']; 
				}else{
					$classColor = 'red_color';
					$date = date('d.m.Y', strtotime($data['maxValDate']));
					$val = 1/$data['maxVal'];
				}
			}elseif( $dest == 'from' ){
				if( $type == "min" ){
					$classColor = 'green_color';
					$date = date('d.m.Y', strtotime($data['maxValDate']));
					$val = $data['maxVal'];
				}else{
					$classColor = 'red_color';
					$date = date('d.m.Y', strtotime($data['minValDate']));
					$val = $data['minVal']; 
				}
			}
			
			$val = number_format( $val, CommonLib::getDecimalPlaces( $val, 4 ), '.', '' );
			return "<span><span class='$classColor'>$val \ </span>$date</span>";
		}
	
		function formatCode( $data ) {

			$code = CHtml::link(
				$data['code'], 
				CurrencyRatesModel::getArchiveSingleURLByCode($data['code'], $this->year, $this->mo, $this->day, 'ecb'),
				array()
			);
			if( $data['cAlias'] ){
				return CountryModel::getImgFlagByAlias('shiny', 16, $data['cAlias']) . " $code";
			}else{
				return $code;
			}
		}
		function formatTitle( $data){
			return CHtml::link( 
				CHtml::encode( $data['currencyName'] ), 
				CurrencyRatesModel::getArchiveSingleURLByCode($data['code'], $this->year, $this->mo, $this->day, 'ecb'),
				array( )
			);
		}
		function formatValue( $data ){
			$date = date('d.m.Y', strtotime($data['dataDate']));
			$val = floatval(1/$data['dataValue']);
			return "<span>" . number_format( $val, CommonLib::getDecimalPlaces( $val, 4 ), '.', '' ) ." \ $date</span>";
		}
		function formatFromValue( $data ){
			$date = date('d.m.Y', strtotime($data['dataDate']));
			$val = floatval( $data['dataValue'] );
			if( $val ) return "<span>" . number_format( $val, CommonLib::getDecimalPlaces( $val, 4 ), '.', '' ) ." \ $date</span>";
		}
	}

?>