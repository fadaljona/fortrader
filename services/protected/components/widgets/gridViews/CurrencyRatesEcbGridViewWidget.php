<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class CurrencyRatesEcbGridViewWidget extends GridViewWidgetBase {
		public $w = 'wBrokerInstrumentsGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		public $returnOnlyTrs = false;
		public $dayType;
		public $toCurrency;
		
		function init() {
			$cs=Yii::app()->getClientScript();
			$formHelpersBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.assets-static.formHelpers') );
			$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers-countries.flags.css');

			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='2' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		public function renderTableBody(){
			$data=$this->dataProvider->getData();
			$n=count($data);
			if( !$this->returnOnlyTrs )echo "<tbody>\n";

			if($n>0)
			{
				for($row=0;$row<$n;++$row)
					$this->renderTableRow($row);
			}
			else
			{
				echo '<tr><td colspan="'.count($this->columns).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
			if( !$this->returnOnlyTrs )echo "</tbody>\n";
		}
		protected function getColumns() {
			$columns = Array(
				Array( 
					'value' => ' $this->grid->formatCode( $data ) ', 
					'header' => 'Code' , 
					'type' => 'raw', 
					'htmlOptions' => Array( 
						'class' => 'table_rating_col_1',
					)
				),
				Array( 
					'value' => ' $this->grid->formatTitle( $data ) ',
					'header' => 'Title' ,
					'type' => 'raw', 
				),
		/*		Array( 
					'value' => ' $this->grid->formatToValue( $data ) ',
					'header' => Yii::t($this->NSi18n, 'To currency') . ' ' . $this->toCurrency->code,
					'type' => 'raw', 
				),*/
				Array( 
					'value' => ' $this->grid->formatFromValue( $data ) ',
					'header' => '1 ' . $this->toCurrency->code . ' =',
					'type' => 'raw', 
				),
	/*			Array( 
					'value' => ' $this->grid->formatvalueDiff( $data ) ',
					'header' => 'Value diff',
					'type' => 'raw', 
				),*/
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatCode( $data ) {
			$data->dataType = 'ecb';
			$code = CHtml::link(
				$data->code, 
				$data->singleURL,
				array()
			);
			if( $data->country ){
				return "{$data->country->getImgFlag("shiny", 16)} $code";
			}else{
				return $code;
			}
		}
		function formatTitle( $data){
			$data->dataType = 'ecb';
			return CHtml::link( 
				CHtml::encode( $data->name ), 
				$data->singleURL,
				array( )
			);
		}
		function formatvalueDiff( $data ){
			if( $data->id == $this->toCurrency->id ){
				$diffValue = 0;
			}else{
				$todayVal = $this->toCurrency->todayEcbData->value / $data->todayEcbData->value;
				$prevDayVal = $this->toCurrency->prevEcbDayData->value / $data->prevEcbDayData->value;
				$diffValue = $todayVal - $prevDayVal;
			}
			
			if( !$diffValue ) return '';
			if( $diffValue == 0 ) return $diffValue;
			$diffValue = number_format($diffValue, CommonLib::getDecimalPlaces( $diffValue, 4 ), '.', '');
			if( $diffValue < 0 )return "<span class='red_color'>" . $diffValue . "</span>";
			if( $diffValue > 0 )return "<span class='green_color'>+" . $diffValue . "</span>";
		}
		function formatToValue( $data ){
			$floatVal =  $this->toCurrency->todayEcbData->value / $data->todayEcbData->value;
			return number_format($floatVal, CommonLib::getDecimalPlaces( $floatVal, 4 ), '.', '');
			
		}
		function formatFromValue( $data ){
			$floatVal = $data->todayEcbData->value / $this->toCurrency->todayEcbData->value;
			return number_format($floatVal, CommonLib::getDecimalPlaces( $floatVal, 4 ), '.', '');
		}
	}

?>