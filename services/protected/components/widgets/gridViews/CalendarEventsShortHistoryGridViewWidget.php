<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class CalendarEventsShortHistoryGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wCalendarEventsShortHistoryGridView';
		public $rowHtmlOptionsExpression = ' Array( "data-id" => $data->indicator_id, "data-dt" => $data->dt ) ';
		public $itemsCssClass = 'description_history_table';
	
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems(){
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table class=\"{$this->itemsCssClass}\">\n";
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}
			else
				$this->renderEmptyText();
		}
		public function renderTableBodyWithoutBody(){
			$data=$this->dataProvider->getData();
			$n=count($data);
			if($n>0){
				for($row=0;$row<$n;++$row)
					$this->renderTableRow($row);
			}else{
				echo '<tr><td colspan="'.count($this->columns).'" class="empty">';
				$this->renderEmptyText();
				echo "</td></tr>\n";
			}
		}
	
		protected function getColumns() {
			$columns = Array(
				Array( 'header' => '', 'value' => ' $this->grid->formatDate( $data->dt ) ' ),
				Array( 'header' => '', 'value' => ' $data->fact_value ' ),
			);

			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatDate( $date ) {
			return str_replace('.', '', Yii::app()->dateFormatter->format( 'LLL-dd',strtotime($date) ));
		}
	}
?>