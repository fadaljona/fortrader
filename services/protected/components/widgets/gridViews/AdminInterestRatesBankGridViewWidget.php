<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );
	Yii::import( 'gridColumns.AdminInterestRatesBankColorGridColumn' );

	final class AdminInterestRatesBankGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminCommonGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'header' => 'Title', 'value' => '$data->title'),
				Array( 'header' => 'Short title', 'value' => '$data->shortTitle'),
				Array( 'header' => 'Country', 'value' => ' $data->country ? Yii::t( CountryModel::getModelNSi18n("CountryModel"), $data->country->name) : "" ', ),
				Array( 'header' => 'Show In First Chart?', 'value' => ' $data->showInFirstChart ? Yii::t( "*", "Yes") : Yii::t( "*", "No") ',),
				Array( 'header' => 'Show In Second Chart?', 'value' => ' $data->showInSecondChart ? Yii::t( "*", "Yes") : Yii::t( "*", "No") ',),
				Array( 
					'header' => 'Second Chart Color', 
					'styleExpression' => ' $data->secondChartColor ',
					'class' => 'AdminInterestRatesBankColorGridColumn'
				),
				Array( 'name' => 'order'),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>