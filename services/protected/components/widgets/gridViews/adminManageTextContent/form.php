<?php

$formModelName = $this->modelFormName;
$formModel = new $formModelName;

$load = $formModel->load($data->id);

if( !$load ) throw new Exception( "Can't load data to form model!" );

$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'class' => 'wAdminFullWidthForm100 wAdminFullWidthForm wAdminManageTextContentForm' )));
	echo $form->hiddenField( $formModel, 'id', Array( 'class' => "formid" ));
	
	echo CHtml::tag(
		'div',
		array('class' => 'alert alert-info'),
		CHtml::link(Yii::t('*', 'Link to page'), $data->singleUrl, array('target' => '_blank'))
	);
	
	echo CHtml::openTag('table', array('class' => 'table'));
		echo CHtml::openTag('tr');
			foreach( $this->langs as $i=>$language ){
				echo CHtml::tag('th', array(), $language->name);
			}
		echo CHtml::closeTag('tr');
		echo CHtml::openTag('tr');
		foreach( $this->langs as $i=>$language ){
			echo CHtml::openTag('td');
				
				foreach( $formModel->textFields as $field => $inpType ){
					if( $inpType == 'textAreaRow' ){
						echo $form->{$inpType}( $formModel, $field . "[{$language->id}]", Array( 'class' => "wFullWidth", 'rows' => 10 ));
					}else{
						echo $form->{$inpType}( $formModel, $field . "[{$language->id}]", Array( 'class' => "wFullWidth" ));
					}
					
				}
				
			echo CHtml::closeTag('td');
		}
		echo CHtml::closeTag('tr');
	echo CHtml::closeTag('table');

	
	$this->widget( 'bootstrap.widgets.TbButton', Array( 
		'buttonType' => 'submit', 
		'type' => 'success',
		'label' => Yii::t( '*', 'Done!' ),
		'htmlOptions' => Array(
			'class' => "pull-right wSubmit",
		),
	));
$this->endWidget();
