<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );
	Yii::import( 'gridColumns.ACECheckBoxGridColumn' );
	Yii::import( 'gridColumns.EAVersionsGridColumn' );

	final class EAVersionsGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wEAVersionsGridView';
		public $class = 'iGridView i05';
		public $rowHtmlOptionsExpression = ' Array( "idVersion" => $data->id ) ';
		public $selectableRows = 2;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}
		protected function getColumns() {
			$columns = Array();
			$columns[] = Array( 
				'class' => 'ACECheckBoxGridColumn',  
				'headerHtmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' ),  
				'htmlOptions' => Array( 'class' => 'checkbox-column hidden-phone' )
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatVersion( $data ) ', 
				'header' => 'Version', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c02 ',//.$this->getSorting( 'version' ),
					//'sortField' => 'version' 
				), 
				'htmlOptions' => Array( 'class' => 'c02' )
			);
			$columns[] = Array( 
				'value' => ' CommonLib::numberFormat( $data->countMessages ) ', 
				'header' => 'Comments', 
				'headerHtmlOptions' => Array( 
					'class' => 'c06 ', //.$this->getSorting( 'tradese' ),
					//'sortField' => 'tradese',
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap' )
			);
			$columns[] = Array( 
				'value' => ' CommonLib::numberFormat( $data->views ) ', 
				'header' => 'Popular', 
				'headerHtmlOptions' => Array( 
					'class' => 'c06 hidden-480 ', //.$this->getSorting( 'tradese' ),
					//'sortField' => 'tradese',
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap hidden-480' )
			);
			$columns[] = Array( 
				'value' => ' $this->grid->formatDate( $data->release ) ', 
				'header' => 'Release', 
				'headerHtmlOptions' => Array( 
					'class' => 'c06 hidden-480 ', //.$this->getSorting( 'tradese' ),
					//'sortField' => 'tradese',
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap hidden-480' )
			);
			/*
			$columns[] = Array( 
				'value' => ' $this->grid->formatStatus( $data->status ) ', 
				'header' => 'Status', 
				'type' => 'raw', 
				'headerHtmlOptions' => Array( 
					'class' => 'c06 hidden-480 ', //.$this->getSorting( 'tradese' ),
					//'sortField' => 'tradese',
				), 
				'htmlOptions' => Array( 'class' => 'iNoWrap hidden-480' )
			);
			*/
			//if( Yii::App()->user->checkAccess( 'eaControl' )) {
				$columns[] = Array( 'class' => 'EAVersionsGridColumn', 'headerHtmlOptions' => Array( 'style' => 'width:15px;' ));
			//}
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		function formatVersion( $data ) {
			return CHtml::link( CHtml::encode( "{$data->EA->name} v.{$data->version}" ), $data->getSingleURL() );
		}
		function formatStatus( $status ) {
			$classes = Array(
				'Developing' => 'label label-warning',
				'Finish' => 'label label-success',
				'Abandoned' => 'label label-inverse arrowed-in',
			);
			$class = @$classes[ $status ];
			$title = Yii::t( $this->NSi18n, $status );
			$label = CHtml::tag( "span", Array( 'class' => $class ), $title );
			return $label;
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$Y = date( "Y", $time );
			$M = date( "M", $time );
			$d = date( "d", $time );
			$date = "{$d} {$M}";
			
			$yearNow = date( "Y" );
			if( $Y != $yearNow ) $date = "{$Y} {$date}";
			
			return $date;
		}
	}

?>