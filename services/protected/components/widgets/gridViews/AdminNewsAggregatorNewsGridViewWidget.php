<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminNewsAggregatorNewsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminCommonGridView';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'title', 'header' => 'Title'),
				Array( 
					'name' => 'idSource',
					'header' => 'Source', 
					'value' => ' $data->rss->source->title ',
					'class' => 'components.dataColumns.NewsAggregatorRssSourceDataColumn'
				),
				Array( 
					'name' => 'idLanguage',
					'header' => 'Language', 
					'value' => ' $data->idLanguage == 0 ? Yii::t("*", "English") : $data->lang->name ',
					'class' => 'components.dataColumns.LangsDataColumn'
				),
				Array( 
					'name' => 'idCat',
					'header' => 'Category', 
					'value' => ' $data->category->title ',
					'class' => 'components.dataColumns.NewsAggregatorCatsDataColumn'
				),
				Array( 
					'header' => 'Date', 
					'value' => ' gmdate("Y-m-d H:i:s", $data->gmtTime) ',
				),
				
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>