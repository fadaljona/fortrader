<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class MonitoringListWithFiltersGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMembersGridView';
		public $class = 'iGridView i05 i06';
		public $rowHtmlOptionsExpression = ' Array( "data-id" => $data->id, "data-server" => $data->idServer, "data-account" => $data->accountNumber ) ';
		public $selectableRows = 0;
		public $itemsCssClass = "dataTable items";
		public $sortField;
		public $sortType;
		public $tabType = false;
		
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table data-item='2' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}
		private function getSorting( $field ) {
			if( $field == $this->sortField ) {
				if( $this->sortType == 'ASC' ) {
					return 'sorting_asc';
				}
				else{
					return 'sorting_desc';
				}
			}
			else{
				return 'sorting';
			}
		}

		protected function getColumns() {
			$columns = Array(
				array( 
					'header' => 'Trader',
					'type' => 'raw',
					'value' => ' CHtml::link( CHtml::encode( $data->user->showName ), $data->user->profileURL ) ', 
				),
				array( 
					'header' => 'Account number',
					'type' => 'raw',
					'value' => ' $this->grid->formatAccountLink( $data ) ', 
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Account number' ) )
				),
				array( 
					'header' => '<span>' .  Yii::t( $this->NSi18n, 'Balance' ) . ' <span class="table_sort"></span></span>',
					'value' => ' round( $data->stats->balance, 1) . " USD" ', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'balance' ),
						'data-sortField' => 'balance' 
					),
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Balance' ) )
				),
				array( 
					'header' => '<span>' .  Yii::t( $this->NSi18n, 'Equity' ) . ' <span class="table_sort"></span></span>',
					'value' => ' round( $data->stats->equity, 1) . " USD" ', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'equity' ),
						'data-sortField' => 'equity' 
					),
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Equity' ) )
				),
				/*array( 
					'header' => '<span>' .  Yii::t( $this->NSi18n, 'Today deals' ) . ' <span class="table_sort"></span></span>',
					'value' => ' $data->monitoringStats->closedDealsToday ', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'todayDeals' ),
						'data-sortField' => 'todayDeals' 
					),
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Today deals' ) )
				),*/
				array( 
					'header' => '<span>' .  Yii::t( $this->NSi18n, 'Total deals' ) . ' <span class="table_sort"></span></span>',
					'value' => ' $data->stats->countTrades ', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'totalDeals' ),
						'data-sortField' => 'totalDeals' 
					),
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Total deals' ) )
				),
				array( 
					'header' => '<span>' .  Yii::t( $this->NSi18n, 'Balance of the day' ) . ' <span class="table_sort"></span></span>',
					'value' => ' $this->grid->formatSignWithUsd( $data->monitoringStats->balanceToday ) ', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'balanceDay' ),
						'data-sortField' => 'balanceDay' 
					),
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Balance of the day' ) ),
					'type' => 'raw',
				),
				array( 
					'header' => '<span>' .  Yii::t( $this->NSi18n, 'Gain' ) . ' <span class="table_sort"></span></span>',
					'value' => ' $this->grid->formatPercent( $data->stats->gain ) ', 
					'headerHtmlOptions' => Array( 
						'class' => $this->getSorting( 'gain' ),
						'data-sortField' => 'gain' 
					),
					'htmlOptions' => Array( 'data-title' => Yii::t( $this->NSi18n, 'Gain' ) ),
					'type' => 'raw',
				),

			);
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	
		
		function formatAccountLink( $data ) {
			return CHtml::link($data->accName, array('monitoring/single', 'id' => $data->id), array('class' => 'blue_color') );
		}
		function formatSignWithUsd( $val ){
			if( !$val ) return 0;
			if( $val > 0 ) return CHtml::tag( 'span', array( 'class' => 'green_color' ), '+' . round( $val, 1) . ' USD' );
			return CHtml::tag( 'span', array( 'class' => 'red_color' ), round( $val, 1) . ' USD' );
		}
		function formatPercent( $val ) {
			if( !$val ) return 0;
			if( $val > 0 ) return CHtml::tag( 'span', array( 'class' => 'green_color' ), '+' . round( $val, 1) . ' %' );
			return CHtml::tag( 'span', array( 'class' => 'red_color' ), round( $val, 1) . ' %' );
		}

	}

?>