<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminContestMonitoringGroupedErrorsGridViewWidget extends GridViewWidgetBase {
		public $w = 'wAdminContestMonitoringGroupedErrorsGridViewWidget';
		public $class = 'iGridView i02';
		public $rowHtmlOptionsExpression = ' Array( "data-idObj" => $data->ACCOUNT_NUMBER ) ';

		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'name' => 'last_error_code'),
				Array( 'name' => 'last_error_descr'),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>