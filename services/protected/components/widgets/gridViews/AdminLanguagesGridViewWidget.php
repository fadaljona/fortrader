<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetBase' );

	final class AdminLanguagesGridViewWidget extends GridViewWidgetBase {
		public $w = 'wLanguagesGridView';
		public $class = 'iGridView i01';
		public $rowHtmlOptionsExpression = ' Array( "idLanguage" => $data->id ) ';
		function init() {
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		protected function getColumns() {
			$columns = Array(
				Array( 'value' => 'Yii::t( "widgets/adminLanguagesGridView", $data->name )', 'header' => 'Name', 'headerHtmlOptions' => Array( 'class' => 'c01' )),
				Array( 'name' => 'alias', 'header' => 'Alias' ),
			);
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
	}

?>