<?
	Yii::import( 'widgets.gridViews.base.GridViewWidgetFrontBase' );

	final class ContestMemberLastDealsForForumGridViewWidget extends GridViewWidgetFrontBase {
		public $w = 'wContestMembersGridView';
		public $class = 'iGridView i05 i06';
		public $rowHtmlOptionsExpression = 'array()';
		public $selectableRows = 0;
		public $itemsCssClass = "deals-table";
		public $sortField;
		public $sortType;
		public $tabType = false;
		function init() {
			
			$this->htmlOptions = Array(
				'class' => "{$this->class} grid-view {$this->ins} {$this->w}",
			);
			parent::init();
		}
		public function renderItems() {
			if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty){
				echo "<table id='deals-table' class=\"{$this->itemsCssClass}\">\n";
				$this->renderTableHeader();
				ob_start();
				$this->renderTableBody();
				$body=ob_get_clean();
				$this->renderTableFooter();
				echo $body; // TFOOT must appear before TBODY according to the standard.
				echo "</table>";
			}else
				$this->renderEmptyText();
		}


		protected function getColumns() {
			$columns = Array(
				array( 
					'header' => CommonLib::getInfoTextForForumInformer($this->NSi18n, 'LastDeals') . Yii::t($this->NSi18n, 'Last deals'),
					'htmlOptions' => Array( 'class' => 'last-deals' ),
					'value' => ' $this->grid->formatLastDeals( $data ) ', 
				),
				array( 
					'header' => CommonLib::getInfoTextForForumInformer($this->NSi18n, 'CloseTime') . Yii::t($this->NSi18n, 'Close Time'),
					'htmlOptions' => Array( 'class' => 'closed' ),
					'value' => ' $this->grid->formatCloseTime( $data->timestamp ) ', 
				),
				array( 
					'header' => CommonLib::getInfoTextForForumInformer($this->NSi18n, 'Lots') . Yii::t($this->NSi18n, 'Lots'),
					'htmlOptions' => Array( 'class' => 'lots' ),
					'value' => ' $data->lots ', 
				),
				array( 
					'header' => CommonLib::getInfoTextForForumInformer($this->NSi18n, 'Swap') . Yii::t($this->NSi18n, 'Swap'),
					'htmlOptions' => Array( 'class' => 'return grey' ),
					'value' => ' $this->grid->formatSignNumber( $data->swap ) ', 
					'type' => 'raw',
				),
				array( 
					'header' => CommonLib::getInfoTextForForumInformer($this->NSi18n, 'Profit') . Yii::t($this->NSi18n, 'Profit'),
					'htmlOptions' => Array( 'class' => 'return grey' ),
					'value' => ' $this->grid->formatSignNumber( $data->profit ) ', 
					'type' => 'raw',
				),
				array( 
					'header' => CommonLib::getInfoTextForForumInformer($this->NSi18n, 'Commission') . Yii::t($this->NSi18n, 'Commission'),
					'htmlOptions' => Array( 'class' => 'return grey' ),
					'value' => ' $this->grid->formatSignNumber( $data->commission ) ', 
					'type' => 'raw',
				),

			);
			
			foreach( $columns as &$column ) if( isset( $column[ 'header' ])) $column[ 'header' ] = Yii::t( $this->NSi18n, $column[ 'header' ]); unset( $column );
			return $columns;
		}
		
		function formatSignNumber( $data ){
			if( $data < 0 ) return CHtml::tag('span', array('class' => 'redColor'), $data);
			if( $data > 0 ) return CHtml::tag('span', array('class' => 'greenColor'), '+' . $data);
			return $data;
		}
		function formatCloseTime( $timestamp ){
			$this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $timestamp ));
		}
		function formatLastDeals( $data ){
			return $data->SYMBOL . ' ' . Yii::t($this->NSi18n, $data->dealTypeTitle) . ' ' . $data->closePrice;
		}
	}

?>