<?php
Yii::import('components.widgets.base.WidgetBase');

final class ChooseExchangerByDirectionWidget extends WidgetBase
{
    public $fromCurrency = false;
    public $toCurrency = false;

    private function getToAndFromFilterContent()
    {
        $currenciesForFilter = PaymentSystemCurrencyModel::getCurrenciesForFilter();
        $fromStr = $toStr = '';
        foreach ($currenciesForFilter as $id => $data) {
            $hasFromDisabledClass = '';
            if (!$data['hasFrom']) {
                $hasFromDisabledClass = 'exchange-item_disable';
            }
            $hasToDisabledClass = '';
            if (!$data['hasTo']) {
                $hasToDisabledClass = 'exchange-item_disable';
            }

            $hasFromActivaClass = '';
            if ($this->fromCurrency && $this->fromCurrency == $id) {
                $hasFromActivaClass = 'exchange-item_active';
            }

            $hasToActivaClass = '';
            if ($this->toCurrency && $this->toCurrency == $id) {
                $hasToActivaClass = 'exchange-item_active';
            }

            $fromStr .= CHtml::tag('div', array('class' => 'exchange__item ' . $hasFromDisabledClass . ' ' . $hasFromActivaClass, 'data-id' => $id), $data['name']);
            $toStr .= CHtml::tag('div', array('class' => 'exchange__item ' . $hasToDisabledClass . ' ' . $hasToActivaClass, 'data-id' => $id), $data['name']);
        }
        return array('from' => $fromStr, 'to' => $toStr);
    }

    public function run()
    {
        $this->render($this->getCleanClassName() . "/view", array(
            'filterContent' => $this->getToAndFromFilterContent(),
            'fromCurrency' => $this->fromCurrency,
            'toCurrency' => $this->toCurrency
        ));
    }
}