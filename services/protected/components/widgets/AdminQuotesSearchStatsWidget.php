<?
	Yii::import( 'components.widgets.base.WidgetBase' );

	final class AdminQuotesSearchStatsWidget extends WidgetBase {
		public $DP;
		public $modelName = 'QuotesSearchStatsModel';

		private function getRange() {
			$begin = @$_GET[ 'begin' ];
			$end = @$_GET[ 'end' ];
			if( !CommonLib::isDate( $begin )) $begin = date( "Y-m-d", time() - 60*60*24 );
			if( !CommonLib::isDate( $end )) $end = date( "Y-m-d" );
			return $end >= $begin ? Array( $begin, $end ) : Array( $end, $begin );
		}
		private function getBegin() {
			list( $begin, $end ) = $this->getRange();
			return $begin;
		}
		private function getEnd() {
			list( $begin, $end ) = $this->getRange();
			return $end;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function createDP() {
			$c = new CDbCriteria();
			$c->select = Array( " `keyword` ", " count(`id`) AS `count` " );
			$c->addCondition( " DATE( `searchDate` ) BETWEEN :begin AND :end " );
			$c->params = array( ':begin' => $this->getBegin(), ':end' => $this->getEnd() );
			$c->group = " `keyword` ";
			$c->order = " `count` DESC ";
			
			$DP = new CActiveDataProvider( $this->modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			$DP->pagination->pageSize = 50;
			$this->DP = $DP;
			return $DP;
		}


		function run() {
			$class = $this->getCleanClassName();
			
			$this->render( "{$class}/view", array(
				'begin' => $this->getBegin(),
				'end' => $this->getEnd(),
				'DP' => $this->getDP(),
			));
		}
	}

?>