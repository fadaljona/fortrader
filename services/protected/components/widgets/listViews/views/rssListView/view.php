<?php
	
	$sourceImg = $data->rss->source->srcTmbLogo;
?>

<li class="news_big_list_item" data-time="<?=$data->gmtTime?>">
	<div class="news_top_box">
		<div class="new_abs_img">
		<?php
			if( $sourceImg ){
				echo CHtml::tag('img', array('src' => $sourceImg, 'alt' => $data->title));
			}
		?>
		</div>
		<h2 class="news_title"><span><?=$data->title?></span></h2>
		<div class="date_with_share_box clearfix">
			<time class="date_news"><?=Yii::app()->dateFormatter->format( "d MMMM yyyy", $data->gmtTime )?></time>
		</div>
	</div>
	<div class="dropdown_news">
		<h3 class="dropdown_news_title"><?=$data->title?></h3>
		<p><?=$data->description?></p>
		<a href="<?=$data->redirectSingleURL?>" class="dropdown_button blue_btn" rel="nofollow" target="_blank"><?=Yii::t('NewsAggregatorCatsWidget', 'Read more')?></a>
	</div>
</li>