<?php 
	$cookieObj = Yii::app()->request->cookies['disabledSourcesFT']; 
	$disabledSourcesFT = $cookieObj->value;
	
	$disabledInp = 'checked';
	$enabledInp = '';
	if( strpos( ','.$disabledSourcesFT.',', ','.$data->id.',' ) === false ){
		$disabledInp = '';
		$enabledInp = 'checked';
	}
?>
<div class="clearfix news2_wrap" data-source-id="<?=$data->id?>" >
	<div class="news2_box_link">
		<div class="news2_link_img"><img src="<?=$data->srcTmbLogo?>" alt="<?=$data->title?>"></div>
		<?php $parsedUrl = parse_url($data->url);?>
		<a href="<?=$data->singleUrl?>" rel="nofollow" target="_blank" class="news2_link"><?=$data->title?><span> / <?=$parsedUrl['host']?></span></a>
	</div>
	<div class="box_radio_btn1 clearfix">
		<input type="radio" name="enabled<?=$data->id?>" id="enabledId<?=$data->id?>" class="sources_off" <?=$disabledInp?> value="disabled" data-source-id="<?=$data->id?>">
		<label for="enabledId<?=$data->id?>" class="on_off red"><?=Yii::t('RssSourcesListViewWidget', 'Off')?></label>
		
		
		<input type="radio" name="enabled<?=$data->id?>" id="enabledId<?=$data->id?>1" class="sources_on" <?=$enabledInp?> value="enabled" data-source-id="<?=$data->id?>">
		<label for="enabledId<?=$data->id?>1" class="on_off  blue"><?=Yii::t('RssSourcesListViewWidget', 'On')?></label>
	</div>
</div>