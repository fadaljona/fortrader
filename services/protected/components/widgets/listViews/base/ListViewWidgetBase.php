<?
	Yii::import( 'controllers.base.ActionBase' );
	Yii::import( 'zii.widgets.CListView' );

	class ListViewWidgetBase extends CListView {
		public $NSi18n = '*';
		public $DPLimit = ActionBase::DPLimit;
		public $DPPageVar = ActionBase::DPPageVar;
		public $itemView = 'y';

		protected function getDPLimit() {
			return $this->DPLimit;
		}
		protected function setDPLimit( $limit ) {
			$this->DPLimit = $limit;
		}
		protected function getDPPageVar() {
			return $this->DPPageVar;
		}
		protected function setDPPageVar( $var ) {
			$this->DPPageVar = $var;
		}
		protected function getDPPagination( $pageSize = null, $pageVar = null ) {
			return Array(
				'pageSize' => $pageSize === null ? $this->getDPLimit() : $pageSize,
				'pageVar' => $pageVar === null ? $this->getDPPageVar() : $pageVar,
			);
		}
		protected function resolveClassName( $class ) {
			return $class ? $class : get_class( $this );
		}
		function getCleanClassName( $class = null ) {
			$class = $this->resolveClassName( $class );
			$class = preg_replace( "#Widget$#", "", $class );
			$class[0] = strtolower($class[0]);
			return $class;
		}
		function getNSi18n( $class = null ) {
			$class = $this->getCleanClassName( $class );
			return "components/widgets/{$class}";
		}
		protected function throwI18NException( $message, $params = Array(), $class = null ) {
			$NSi18n = $this->getNSi18n( $class );
			throw new Exception( Yii::t( $NSi18n, $message, $params ));
		}
		public function renderItems(){
			echo CHtml::openTag($this->itemsTagName,array('class'=>$this->itemsCssClass))."\n";
			$this->renderRows();
			echo CHtml::closeTag($this->itemsTagName);
		}
		public function renderRows(){
			$data=$this->dataProvider->getData();
			if(($n=count($data))>0)
			{
				$owner=$this->getOwner();
				
				$reflection = new ReflectionClass($this);
				$directory = dirname($reflection->getFileName()) ;

				$viewFile= $directory . '/views/' . $this->getCleanClassName() .  '/view.php';
				
				if( !file_exists( $viewFile ) ){
					throw new CException(Yii::t('yii','{widget} cannot find the view "{view}".',
						array('{widget}'=>get_class($this), '{view}'=>$viewFile )));
				}
				
				$j=0;
				foreach($data as $i=>$item)
				{
					$data=$this->viewData;
					$data['index']=$i;
					$data['data']=$item;
					$data['widget']=$this;
					$owner->renderFile($viewFile,$data);
					if($j++ < $n-1)
						echo $this->separator;
				}
			}
			else
				$this->renderEmptyText();
		}
	}

?>