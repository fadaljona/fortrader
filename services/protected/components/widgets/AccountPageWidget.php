<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AccountPageWidget extends WidgetBase {
		
		public $NSi18n;
		public $accountModel;
		public $rev;
		public $revlist;
		public $linklist;
		
		private function getAccountModel() {
			return $this->accountModel;
		}

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'NSi18n' => $this->NSi18n,
				'accountModel' => $this->getAccountModel(),
				'rev' => $rev,
				'revlist' => $revlist,
				'linklist' => $linklist,

			));
		}
	}

?>