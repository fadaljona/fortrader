<?php

Yii::import('components.widgets.base.WidgetBase');

final class VideoChanelWidget extends WidgetBase
{
    public $chanel;

    private function getModels()
    {
        if (!$this->chanel) {
            return false;
        }
        return VideoChanelItemModel::model()->findAll(array(
            'with' => array(
                'currentLanguageI18N',
                'chanel' => array('with' => 'currentLanguageI18N')
            ),
            'condition' => " `cLI18NVideoChanelItem`.`url` IS NOT NULL AND `cLI18NVideoChanelItem`.`url` <> '' AND `cLI18NVideoChanelItem`.`title` IS NOT NULL AND `cLI18NVideoChanelItem`.`title` <> '' AND `cLI18NVideoChanel`.`title` IS NOT NULL AND `cLI18NVideoChanel`.`title` <> '' ",
            'order' => " `t`.`id` DESC ",
            'limit' => 30,
        ));
    }

    public function run()
    {
        $models = $this->getModels();
        if (!$models) {
            return false;
        }
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'chanel' => $models[0]->chanel,
            'models' => $models
        ));
    }
}
