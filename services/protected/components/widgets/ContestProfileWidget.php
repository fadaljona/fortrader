<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestProfileWidget extends WidgetBase {
		public $model;
		private function getModel() {
			return $this->model;
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$Y = date( "Y", $time );
			$F = date( "F", $time );
			$d = date( "d", $time );
			$F = strtolower( $F );
			$F = Yii::t( '*', $F );
			$date = "{$d} {$F}";
			
			$yearNow = date( "Y" );
			if( $Y != $yearNow ) $date = "{$Y} {$date}";
			
			return $date;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
			));
		}
	}

?>