<?php

Yii::import('components.widgets.base.WidgetBase');

final class SmByHashTagWidget extends WidgetBase
{
    public function run()
    {
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array());
    }
}
