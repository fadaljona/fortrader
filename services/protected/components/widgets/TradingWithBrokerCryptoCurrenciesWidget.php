<?php
Yii::import('components.widgets.base.WidgetBase');

final class TradingWithBrokerCryptoCurrenciesWidget extends WidgetBase
{
    const MODEL_NAME = 'BrokerModel';
    public $DP;
    public $ajax = false;

    private function createDP()
    {
        $c = new CDbCriteria();
        $c->with['currentLanguageI18N'] = array();
        $c->with['stats'] = array();
      
        $c->condition = " `t`.`toCryptoCurrencies` = 1 ";
        $c->order = "`stats`.`place` ASC ";
        
        $DP = new CActiveDataProvider(self::MODEL_NAME, array(
            'criteria' => $c,
            'pagination' => array(
                'pageSize' => 2,
                'pageVar' => 'pageTradingWithBrokerCryptoCurrencies'
            )
        ));
        return $DP;
    }
        
    private function getDP()
    {
        return $this->DP ? $this->DP : $this->createDP();
    }
        
        
    public function run()
    {
        if (Yii::app()->language == 'en_us') {
            return false;
        }
        
        $class = $this->getCleanClassName();
        $DP = $this->getDP();

        $this->render("{$class}/view", array(
            'DP' => $DP,
            'ajax' => $this->ajax
        ));
    }
}
