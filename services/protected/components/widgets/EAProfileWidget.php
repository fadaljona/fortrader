<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminEAMarkFormModel' );
	
	final class EAProfileWidget extends WidgetBase {
		public $model;
		private function getMarkModel() {
			$formModel = new AdminEAMarkFormModel();
			$formModel->setIDEA( $this->model->id );
			$formModel->load();
			return $formModel;
		}
		private function getModel() {
			return $this->model;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
				'markModel' => $this->getMarkModel(),
			));
		}
	}

?>