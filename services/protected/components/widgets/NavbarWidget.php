<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class NavbarWidget extends WidgetBase {
		public $idLastNotice;
		public $limitNotices = 3;
        public $notices = null;
		private function getCountNotices() {
			if( Yii::App()->user->isGuest ) return 0;
			return $this->countNotices();
		}
		private function getCountNotViewedNotices() {
			if( Yii::App()->user->isGuest ) return 0;
			return $this->countNotViewedNotices();
		}
		private function getIDLastNotice() {
			if( $this->idLastNotice ) return $this->idLastNotice;
			$id = (int)@$_GET['idLastNotice'];
			return $id ? $id : null;
		}

        private function countNotices() {
            $c = new CDbCriteria();
            $c->join = 'LEFT OUTER JOIN {{journal}} AS `journal` ON `t`.`idObject` = `journal`.`id`';
            $c->order = " `t`.`createdDT` DESC ";
            $c->addCondition( " `t`.`idUser` = :idUser " );
            $c->addCondition( "((`journal`.`status` IS NULL OR `journal`.`status` = '0') AND `t`.`type` = 'create_journal') OR `t`.`type` != 'create_journal'");
            $c->params[ ':idUser' ] = Yii::App()->user->id;

            $models = UserNoticeModel::model()->findAll( $c );

            return count($models);
        }

        private function countNotViewedNotices() {
            $c = new CDbCriteria();
            $c->order = " `t`.`createdDT` DESC ";
            $c->addCondition( " `t`.`idUser` = :idUser AND `t`.`viewed` = 0" );
            $c->params[ ':idUser' ] = Yii::App()->user->id;

            $models = UserNoticeModel::model()->findAll( $c );
            return count($models);
        }

		private function getCriteriaNotices( $forCount = false ) {
			$c = new CDbCriteria();
			
			if( !$forCount ) {
				$c->with[ 'user' ] = Array(
					'select' => ' ID, user_login, user_nicename, display_name, firstName, lastName ',
				);
				$c->with[ 'currentLanguageI18N' ] = Array();
				$c->limit = $this->limitNotices;
				$c->order = " `t`.`createdDT` DESC ";
			}

			$c->addCondition( " `t`.`idUser` = :idUser " );
			$c->addNotInCondition( " `t`.`type`", array('create_journal') );
			$c->params[ ':idUser' ] = Yii::App()->user->id;
			
			$idLastNotice = $this->getIDLastNotice();			
			if( $idLastNotice ) {
				$c->addCondition( " `t`.`id` < :idLastNotice " );
				$c->params[ ':idLastNotice' ] = $idLastNotice;
			}
			
			return $c;
		}

        private function getCriteriaNoticesForJournal() {
            $c = new CDbCriteria();
            $c->join = 'LEFT OUTER JOIN {{journal}} AS `journal` ON `t`.`idObject` = `journal`.`id`';
            $c->order = " `t`.`createdDT` DESC ";
            $c->addCondition( " `t`.`idUser` = :idUser " );
            $c->addCondition( " `t`.`type` = 'create_journal'" );
            $c->addCondition( " `journal`.`status` IS NULL OR `journal`.`status` = '0'" );
            $c->params[ ':idUser' ] = Yii::App()->user->id;

            $idLastNotice = $this->getIDLastNotice();
            if( $idLastNotice ) {
                $c->addCondition( " `t`.`id` < :idLastNotice " );
                $c->params[ ':idLastNotice' ] = $idLastNotice;
            }

            return $c;
        }

        private function cmp($a, $b)
        {
            $al = strtolower($a->id);
            $bl = strtolower($b->id);
            if ($al == $bl) {
                return 0;
            }
            return ($al > $bl) ? -1 : +1;
        }

		private function getNotices() {
			if( Yii::App()->user->isGuest ) return Array();
			$c = $this->getCriteriaNotices();
			$d = $this->getCriteriaNoticesForJournal();
			$models = UserNoticeModel::model()->findAll( $c );
			$journalNotices = UserNoticeModel::model()->findAll( $d );
            $models = array_merge($models, $journalNotices);

            usort($models, array($this, "cmp"));
			return $models;
		}
		function renderNotices() {
			$class = $this->getCleanClassName();
			$notices = $this->getNotices();
			foreach( $notices as $notice ) {
				$this->render( "{$class}/userNav/_notice", Array(
					'notice' => $notice,
				));
				$this->idLastNotice = $notice->id;
			}
		}
		function issetMoreNotices() {
			$c = $this->getCriteriaNotices( true );
			$exists = UserNoticeModel::model()->exists( $c );
			return $exists;
		}
		private function getCountPrivateMessages() {
			if( Yii::App()->user->isGuest ) return 0;
			return Yii::App()->user->getModel()->countPrivateMessages;
		}
		private function getCountNotViewedPrivateMessages() {
			if( Yii::App()->user->isGuest ) return 0;
			return Yii::App()->user->getModel()->countNotViewedPrivateMessages;
		}
		private function getPrivateMessages() {
			if( Yii::App()->user->isGuest ) return Array();
			$models = UserPrivateMessageModel::model()->findAll( Array(
				'with' => Array(
					'from' => Array(
						'select' => ' ID, user_login, user_nicename, display_name, firstName, lastName '
					),
				),
				'condition' => " 
					`t`.`idTo` = :idUser
				",
				'order' => " `t`.`createdDT` DESC ",
				'limit' => 3,
				'params' => Array(
					':idUser' => Yii::App()->user->id
				),
			));
			return $models;
		}
		private function getLanguage() {
			return Yii::app()->getLanguage();
		}
		private function getLanguages() {
			$items = Array();
			$languages = LanguageModel::getAll();
			foreach( $languages as $language ) {
				$items[ $language->alias ] = $language->name;
			}
			return $items;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'countPrivateMessages' => $this->getCountPrivateMessages(),
				'countNotViewedPrivateMessages' => $this->getCountNotViewedPrivateMessages(),
				'privateMessages' => $this->getPrivateMessages(),
				'countNotices' => $this->getCountNotices(),
				'countNotViewedNotices' => $this->getCountNotViewedNotices(),
				'language' => $this->getLanguage(),
				'languages' => $this->getLanguages(),
			));
		}
	}

?>