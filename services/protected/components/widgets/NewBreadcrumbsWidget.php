<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class NewBreadcrumbsWidget extends WidgetBase {
		function getItems() {
			$NSi18n = $this->getNSi18n();
			$items = isset($this->controller->commonBag->breadcrumbs) ? $this->controller->commonBag->breadcrumbs : Array();
            
            if (!Yii::app()->baseUrl) {
                $homeUrl = '/';
            } else {
                $homeUrl = Yii::app()->baseUrl . '/';
            }

			array_unshift( $items, (object)Array( 
				'label' => 'Home',
				'url' =>  $homeUrl,
				'icon' => 'icon-home home-icon',
				)
			);
			
			return $items;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'items' => $this->getItems(),
				'views' => isset($this->controller->commonBag->views) ? $this->controller->commonBag->views : false,
			));
		}
	}

?>