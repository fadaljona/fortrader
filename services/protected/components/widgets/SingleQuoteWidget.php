<?
Yii::import('components.widgets.base.WidgetBase');

final class SingleQuoteWidget extends WidgetBase{
	public $model;

	function run() {

		$this->render( $this->getCleanClassName() . "/view", Array(
			'model' => $this->model,
		));
	}
	
}
?>