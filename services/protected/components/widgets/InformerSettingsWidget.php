<?
Yii::import('components.widgets.base.WidgetBase');

final class InformerSettingsWidget extends WidgetBase{
	public $category;
	public $idMember = false;
	public $ajax = false;
	
	private $defaultStyle;
	private $stylesList = array();

	
	private function setStyles(){

		$styles = InformersStyleModel::model()->findAll(array(
			'with' => array( 'currentLanguageI18N', 'categoryTypes' ),
			'condition' => ' `cLI18NInformersStyleModel`.`title` IS NOT NULL AND `cLI18NInformersStyleModel`.`title` <> "" AND `categoryTypes`.`type` = :type AND `t`.`hidden` = 0 ',
			'order' => ' `t`.`order` ASC, `t`.`id` ASC ',
			'params' => array( ':type' => $this->category->type ),
		));
		if( !$styles ) return false;
		
		
		foreach( $styles as $style ){
			if( $style->defaultStyle ) $this->defaultStyle = $style;
			$this->stylesList[$style->id] = $style->title;
		}
		
		if( !$this->defaultStyle ) $this->defaultStyle = $styles[0];
	}
	
	function run() {
		
		$this->setStyles();

		//if( !$this->defaultStyle ) return false;

		if( $this->category->type == 'quotes' && $this->category->idInType == 0 ){
			if( !$this->category->items ) return false;

			$this->render( $this->getCleanClassName() . "/quotesAll", Array(
				'category' => $this->category,
				'defaultStyle' => $this->defaultStyle,
				'stylesList' => $this->stylesList,
				'colums' => $this->category->columns,
				'items' => $this->category->items,
			));
			return true;
			
		}elseif( $this->category->type == 'quotes' ){
			if( !$this->category->items ) return false;

			$this->render( $this->getCleanClassName() . "/quotes", Array(
				'category' => $this->category,
				'defaultStyle' => $this->defaultStyle,
				'stylesList' => $this->stylesList,
				'colums' => $this->category->columns,
				'items' => $this->category->items,
			));
			return true;
			
		}elseif( $this->category->type == 'currencies' && $this->category->idInType == 0 ){
			if( !$this->category->items ) return false;

			$this->render( $this->getCleanClassName() . "/currenciesCbr", Array(
				'category' => $this->category,
				'defaultStyle' => $this->defaultStyle,
				'stylesList' => $this->stylesList,
				'colums' => $this->category->columns,
				'items' => $this->category->items,
				'defautCurrency' => CurrencyRatesModel::getDefaultCbrCurrency()
			));
			return true;
			
		}elseif( $this->category->type == 'currencies' && $this->category->idInType == 1 ){
			if( !$this->category->items ) return false;

			$this->render( $this->getCleanClassName() . "/currenciesEcb", Array(
				'category' => $this->category,
				'defaultStyle' => $this->defaultStyle,
				'stylesList' => $this->stylesList,
				'colums' => $this->category->columns,
				'items' => $this->category->items,
				'defautCurrency' => CurrencyRatesModel::getDefaultEcbCurrency()
			));
			return true;
			
		}elseif( $this->category->type == 'converter' ){

			$this->render( $this->getCleanClassName() . "/{$this->category->type}", Array(
				'category' => $this->category,
				'defaultStyle' => $this->defaultStyle,
				'stylesList' => $this->stylesList,
				'currenciesForConverter' => CurrencyRatesModel::getCurrenciesForConverter(),
				'defaultCurrency' => CurrencyRatesModel::getDefaultCurrency(),
				'settings' => InformersSettingsModel::getModel(),
			));
			return true;
			
		}elseif( $this->category->type == 'cbrfMetals' ){

			$this->render( $this->getCleanClassName() . "/{$this->category->type}", Array(
				'category' => $this->category,
				'defaultStyle' => $this->defaultStyle,
				'stylesList' => $this->stylesList,
				'items' => $this->category->items,
			));
			return true;
			
		}elseif( $this->category->type == 'monitoring' ){
			if( !$this->idMember ) return false;
			$this->render( $this->getCleanClassName() . "/{$this->category->type}", Array(
				'stylesList' => $this->stylesList,
				'defaultStyle' => $this->defaultStyle,
				'category' => $this->category,
				'idMember' => $this->idMember,
				'ajax' => $this->ajax,
			));
			return true;
			
		}elseif( $this->category->type == 'news' ){
			$this->render( $this->getCleanClassName() . "/{$this->category->type}", Array(
				'category' => $this->category,
				'defaultStyle' => $this->defaultStyle,
				'stylesList' => $this->stylesList,
				'items' => $this->category->items,
			));
			return true;
			
		}elseif( $this->category->type == 'calendar' ){
			$this->render( $this->getCleanClassName() . "/{$this->category->type}", Array(
				'category' => $this->category,
				'defaultStyle' => $this->defaultStyle,
				'currecies' => CalendarEvent2Model::getAllCurrencies(),
			));
			return true;
			
		}
		
	}
	
}
?>