<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class JournalsListWidget extends WidgetBase {
		const modelName = 'JournalModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		//public $name;
		//public $sortField = 'place';
		//public $sortType = 'ASC';
		//const MINCountRows = 10;
		//const MAXCountRows = 100;
		/*
		private function getSortField() {
			$sortField = @$_GET[ 'sortField' ];
			if( !in_array( $sortField, Array( 'place', 'name', 'year', 'country_name', 'minDepo' ))) $sortField = $this->sortField;
			return $sortField;
		}
		private function getSortType() {
			$sortType = @$_GET[ 'sortType' ];
			if( !in_array( $sortType, Array( 'ASC', 'DESC' ))) $sortType = $this->sortType;
			return $sortType;
		}
		private function getName() {
			if( $this->name ) return $this->name;
			return @$_GET[ 'name' ];
		}
		private function getCountRows() {
			$countRows = abs((int)@$_COOKIE[ 'BrokersStats2ListWidget_countRows' ]);
			if( !$countRows ) $countRows = $this->DPLimit;
			$countRows = CommonLib::minimax( $countRows, self::MINCountRows, self::MAXCountRows );
			return $countRows;
		}
		*/
		/*
		private function getOrderDP() {
			$sortField = $this->getSortField();
			$sortType = $this->getSortType();
			switch( $sortField ) {
				case 'place': 
					return " `stats`.`place` {$sortType} ";
				case 'name': 
					return " 
						`cLI18NBroker`.`shortName` IS NULL,
						IF( `cLI18NBroker`.`shortName` = '', `cLI18NBroker`.`officialName`, `cLI18NBroker`.`shortName` ) {$sortType}
					";
				case 'year': 
					return " `t`.`year` IS NULL, `t`.`year` {$sortType} ";
				case 'country_name': 
					$idLanguage = LanguageModel::getCurrentLanguageID();
					return " 
						`t`.`idCountry` IS NULL,
						( 
							SELECT 			{{message_i18n}}.`value`
							FROM 			{{message}} 
							INNER JOIN 		{{message_i18n}} ON `idMessage` = {{message}}.`id` AND `idLanguage` = {$idLanguage}
							WHERE 			`key` = `country`.`name` 
							LIMIT 			1
						) 
						{$sortType}
					";
				case 'minDepo': 
					return " `t`.`minDeposit` IS NULL, `t`.`minDeposit` {$sortType} ";
			}
		}
		*/
		private function createDP() {
			BrokerStats2Model::flush();
			
			$c = new CDbCriteria();
			
			$c->with[ 'currentLanguageI18N' ] = Array();
			$c->with[ 'i18ns' ] = Array();
			$c->with[ 'i18ns.language' ] = Array();

            $c->condition="status = '0'";
			$c->order = " `t`.`date` DESC, `t`.`id` DESC ";//$this->getOrderDP();

			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			$DP->pagination->pageVar = "journalsPage";
			$DP->pagination->pageSize = 10;//$this->getCountRows();
			
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'single';
			}
			
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				//'countRows' => $this->getCountRows(),
				//'name' => $this->getName(),
				//'sortField' => $this->getSortField(),
				//'sortType' => $this->getSortType(),
			));
		}
	}

?>