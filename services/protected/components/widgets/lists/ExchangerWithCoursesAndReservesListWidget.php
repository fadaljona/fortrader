<?php
Yii::import('components.widgets.base.WidgetBase');

final class ExchangerWithCoursesAndReservesListWidget extends WidgetBase
{
    const MODEL_NAME = 'PaymentExchangerCourseModel';

    public $ajax = true;
    public $showOnEmpty = true;
    public $hidden = false;
    public $fromCurrency = false;
    public $toCurrency = false;

    private function getFromCurrency()
    {
        return $this->getToFromCurrency($this->fromCurrency);
    }
    private function getToCurrency()
    {
        return $this->getToFromCurrency($this->toCurrency);
    }
    private function getToFromCurrency($currency)
    {
        if (!$currency) {
            return false;
        }
        if (!PaymentSystemCurrencyModel::model()->findByPk($currency)) {
            return false;
        }
        return $currency;
    }
    private function getDP()
    {
        $c = new CDbCriteria();

        $c->addCondition(" `t`.`date` = :date ");
        $c->params[':date'] = date('Y-m-d');

        $c->with['exchanger'] = array(
            'with' => array(
                'currentLanguageI18N',
                'exchangerVoteStats',
                'country',
            )
        );
        $c->addCondition(" `exchanger`.`parserName` <> '' AND `exchanger`.`parserName` IS NOT NULL ");
        $c->addCondition(" `cLI18NPaymentExchangerModel`.`title` <> '' AND `cLI18NPaymentExchangerModel`.`title` IS NOT NULL ");

        $c->with['fromCurrencyModel'] = array(
            'with' => array('currentLanguageI18N')
        );
        $c->addCondition(" `cLI18NPaymentSystemCurrencyModel`.`title` <> '' AND `cLI18NPaymentSystemCurrencyModel`.`title` IS NOT NULL ");

        $c->with['toCurrencyModel'] = array(
            'with' => array('toCurrentLanguageI18N')
        );
        $c->addCondition(" `toCLI18NPaymentSystemCurrencyModel`.`title` <> '' AND `toCLI18NPaymentSystemCurrencyModel`.`title` IS NOT NULL ");
        
        $c->with['courseExchangerReserve'] = array();
        $c->addCondition(" `courseExchangerReserve`.`amount` <> 0 AND `courseExchangerReserve`.`amount` IS NOT NULL ");

        if ($fromCurrency = $this->getFromCurrency()) {
            $c->addCondition(" `t`.`fromCurrency` = :fromCurrency ");
            $c->params[':fromCurrency'] = $fromCurrency;
        }

        if ($toCurrency = $this->getToCurrency()) {
            $c->addCondition(" `t`.`toCurrency` = :toCurrency ");
            $c->params[':toCurrency'] = $toCurrency;
        }

        $c->order = " `t`.`fromCurrency`, `t`.`toCurrency`, `t`.`toAmount`/`t`.`fromAmount` DESC ";
        //

        $DP = new CActiveDataProvider(self::MODEL_NAME, array(
            'criteria' => $c,
            'pagination' => $this->getDPPagination()
        ));
        $DP->pagination->pageVar = 'selectBestCoursePage';

        return $DP;
    }
    public function run()
    {
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'DP' => $this->getDP(),
            'ajax' => $this->ajax,
            'showOnEmpty' => $this->showOnEmpty,
            'hidden' => $this->hidden,
        ));
    }
}