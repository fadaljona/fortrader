<?
	Yii::import( 'components.widgets.base.WidgetBase' );

	final class ContestsTradersListWidget extends WidgetBase {
		const modelName = 'ContestModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $label;
		private function createDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 'servers', 'servers.broker', 'servers.broker.currentLanguageI18N', 'servers.broker.i18ns', 'countMembers', 'currentLanguageI18N' ),
					'order' => ' `t`.`begin` DESC, `cLI18NContest`.`name` ASC',
				),
				'pagination' => $this->getDPPagination(),
			));
			$DP->pagination->pageVar = "contestsPage";
			$DP->pagination->pageSize = 6;
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'list';
			}
			return $DP;
			до какого коммита откатываемся, тот шо я обвожу
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				'label' => $this->label
			));
		}
	}

?>