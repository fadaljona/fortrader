<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminManageTextContentStatsListWidget extends WidgetBase {
		
		private function getDp(){	
		
			$c = new CDbCriteria();
			$c->with = array('user');
			$c->select = array(
				" `t`.* ",
				" SUM( CHAR_LENGTH (replace(`t`.`newVal`, `t`.`oldVal`, '')) ) as dayLength "
			);
			$c->group = " `t`.`dayDate`, `t`.`idUser`, `t`.`lang` ";
			$c->order = " `t`.`dayDate` DESC ";
			
			if( !Yii::App()->user->checkAccess( 'textContentAdminControl' ) ){
				$c->addCondition('  `t`.`idUser` = :idUser ');
				$c->params[':idUser'] = Yii::app()->user->id;
			}
		
			
			$DP = new CActiveDataProvider( 'ManageTextContentHistoryModel', Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination()
			));
			
			return $DP;
		}

		function run() {
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDp(),
			));
		}
	}

?>