<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminPostsStatsWpSearchStatsListWidget extends WidgetBase {
		const modelName = 'WpSearchStatsModel';
		public $DP;
		public $startDate;
		public $endDate;
		private $inSearch = false;

		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function createDP() {
			$c = new CDbCriteria();

			if( $this->startDate && $this->endDate ){
				$c->addCondition( "
					`t`.`searchDate` >= :startDate AND `t`.`searchDate` <= :endDate
				");
				$c->params[':startDate'] = $this->startDate;
				$c->params[':endDate'] = date('Y-m-d H:i:s', strtotime( $this->endDate ) + 60*60*24);
				$this->setInSearch();
			}
			
			$c->select = Array( " `keyword` ", " count(`keyword`) AS `count` ", " MAX( `searchDate` ) AS `lastDate` " );
			$c->group = " `keyword` ";
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
				'sort' => array(
					'sortVar' => 'sort',
					'attributes' => array(
						'keyword' => array(
							'default'=>'desc',
						),
						'count' => array(
							'default'=>'desc',
						),
						'lastDate' => array(
							'default'=>'desc',
						),
					),
					'defaultOrder'=>array(
						'count'=>CSort::SORT_DESC,
					)
				)
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'inSearch' => $this->getInSearch(),
				'startDate' => $this->startDate,
				'endDate' => $this->endDate
			));
		}
	}

?>