<?
Yii::import('components.widgets.base.WidgetBase');

final class OtherQuotesListWidget extends WidgetBase{
	const cacheTime = 360; //5mins
	const yahooOtherQuotesListCacheKey = 'yahooOtherQuotesListCacheKey';
	
	private function getAllYahooNames() {
		$sql =  " 
SELECT `symbols`.`name` FROM `{{quotes_symbols}}` `symbols` 
LEFT JOIN `{{quotes_views}}` `views` ON `views`.`symbolId` = `symbols`.`id` 
LEFT JOIN `{{quotes_symbols_i18n}}` `i18n` ON `i18n`.`idSymbol` = `symbols`.`id` and `i18n`.`idLanguage` = ". LanguageModel::getCurrentLanguageID() ." 
WHERE `symbols`.`sourceType` = 'yahoo' AND `symbols`.`histName` <> '' AND `i18n`.`visible` = 'Visible'
ORDER BY `views`.`views` DESC ";
		$result = Yii::App()->db->createCommand( $sql )->queryAll( );
		return $result;
	}

	function run() {
		
		if( Yii::app()->cache->get( self::yahooOtherQuotesListCacheKey ) ){ echo Yii::app()->cache->get( self::yahooOtherQuotesListCacheKey ); return true; };
			ob_start();
			$this->render( $this->getCleanClassName() . "/view", Array(
				'quotes' => $this->getAllYahooNames(),
			));
			$content = ob_get_clean();
		Yii::app()->cache->set( self::yahooOtherQuotesListCacheKey, $content, self::cacheTime * 60 );
		echo $content;
	}
	
}
?>