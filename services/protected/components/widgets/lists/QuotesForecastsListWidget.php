<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class QuotesForecastsListWidget extends WidgetBase {
		const modelName = 'QuotesForecastsModel';
		public $DP;
		public $userId;

		private function createDP() {

			$c = new CDbCriteria();
			
			$c->with = Array('symbol');
            $c->condition = " t.status <> 2 AND t.userId = :userId ";
			$c->params = array(':userId' => $this->userId );

			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			//$DP->pagination->pageSize = 1;

			$this->DP = $DP;
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
			));
		}
	}

?>