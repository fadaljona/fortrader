<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminBrokerLinksClickStatsListWidget extends WidgetBase {
		const modelName = 'BrokerLinksStatsModel';
		public $DP;
		public $startDate;
		public $endDate;
		private $inSearch = false;

		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function createDP() {
			$c = new CDbCriteria();

			$statsWhereSql = '';
			if( $this->startDate && $this->endDate ){
				$c->addCondition( "
					`t`.`clickDate` >= :startDate AND `t`.`clickDate` <= :endDate
				");
				$c->params[':startDate'] = $this->startDate;
				$c->params[':endDate'] = date('Y-m-d H:i:s', strtotime( $this->endDate ) + 60*60*24);
				$this->setInSearch();
				$statsWhereSql = 'AND `clickDate` >= :startDate AND `clickDate` <= :endDate';
			}
			
			$openAccountExp = new CDbExpression(
				" (SELECT count(brokerId) FROM {{broker_links_stats}} WHERE type='openAccount' AND brokerId = t.brokerId $statsWhereSql ) AS `openAccountClicks` "
			);
			$openDemoAccountExp = new CDbExpression(
				" (SELECT count(brokerId) FROM {{broker_links_stats}} WHERE type='openDemoAccount' AND brokerId = t.brokerId $statsWhereSql ) AS `openDemoAccountClicks` "
			);
			$siteExp = new CDbExpression(
				" (SELECT count(brokerId) FROM {{broker_links_stats}} WHERE type='site' AND brokerId = t.brokerId $statsWhereSql ) AS `siteClicks` "
			);
			$conditionsExp = new CDbExpression(
				" (SELECT count(brokerId) FROM {{broker_links_stats}} WHERE type='conditions' AND brokerId = t.brokerId $statsWhereSql ) AS `conditionsClicks` "
			);
			$termsExp = new CDbExpression(
				" (SELECT count(brokerId) FROM {{broker_links_stats}} WHERE type='terms' AND brokerId = t.brokerId $statsWhereSql ) AS `termsClicks` "
			);
			
			$c->select = Array( " `brokerId` ", " MAX( `clickDate` ) AS `lastDate` ", $openAccountExp, $siteExp, $conditionsExp, $termsExp, $openDemoAccountExp );
			$c->group = " `brokerId` ";
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
				'sort' => array(
					'sortVar' => 'sort',
					'attributes' => array(
						'lastDate' => array(
							'default'=>'desc',
						),
						'brokerId' => array(
							'default'=>'desc',
						),
						'openAccountClicks' => array(
							'default'=>'desc',
						),
						'openDemoAccountClicks' => array(
							'default'=>'desc',
						),
						'siteClicks' => array(
							'default'=>'desc',
						),
						'conditionsClicks' => array(
							'default'=>'desc',
						),
						'termsClicks' => array(
							'default'=>'desc',
						),
					),
					'defaultOrder'=>array(
						'lastDate'=>CSort::SORT_DESC,
					)
				)
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'inSearch' => $this->getInSearch(),
				'startDate' => $this->startDate,
				'endDate' => $this->endDate
			));
		}
	}

?>