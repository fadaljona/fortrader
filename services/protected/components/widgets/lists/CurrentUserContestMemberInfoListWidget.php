<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrentUserContestMemberInfoListWidget extends WidgetBase {
		const modelName = 'ContestMemberModel';
		public $ajax = false;
		public $idContest;
		public $model;

		private function getIDUser() {
			return Yii::App()->user->id;
		}
		private function loadModel() {
			$idContest = $this->idContest;
			$this->model = ContestMemberModel::model()->find( Array(
				'select' => " idContest, idUser, accountNumber, status ",
				'with' => Array( 
					'stats' => Array(
						'select' => Array( 
							" balance ",
							" IF( `balance` IS NULL, NULL, `place` ) AS place ",
							" gain ",
							" drowMax ",
							" countTrades ",
							" countOpen ",
							" last_error_code ",
							" trueDT ",
						),
					),
				),
				'condition' => "
					`t`.`idContest` = :idContest
					AND `t`.`idUser` = :idUser
				",
				'params' => Array(
					':idContest' => $idContest,
					':idUser' => $this->getIDUser(),
				),
			));
			return $this->model;
		}
		private function getModel() {
			return $this->model ? $this->model : $this->loadModel();
		}
		private function getAjax() {
			return $this->ajax;
		}
		function run() {
			if( !Yii::App()->user->isGuest and Yii::App()->user->getModel()->isRegisteredInContest( $this->idContest )){
				$class = $this->getCleanClassName();
				$model = $this->getModel();
				$itCurrentModel = false;
				if( $this->getIDUser() == $model->idUser ) $itCurrentModel = true;
				$this->render( "{$class}/view", Array(
					'ajax' => $this->getAjax(),
					'model' => $model,
					'itCurrentModel' => $itCurrentModel
				));
			}
		}
	}

?>