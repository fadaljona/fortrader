<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminCalendarEventsListWidget extends WidgetBase {
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $filterURL;
		public $filterModel;
		public $inSearch;
		private function getDP() {
			return $this->DP ? $this->DP : false;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
				'hidden' => $this->getHidden(),
			));
		}
	}

?>
