<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EATradeAccountTradesListWidget extends WidgetBase {
		const modelName = 'MT5AccountTradesModel';
		public $DP;
		public $ajax = false;
		public $idAccount;
		public $account;
		private function getIDAccount() {
			return $this->idAccount;
		}
		private function getAccount() {
			if( !$this->account ) {
				$idAccount = $this->getIDAccount();
				$this->account = TradeAccountModel::model()->find( Array(
					'select' => ' id, accountNumber, idServer ',
					'with' => Array( 
						'stats' => Array(
							'select' => ' balance, equity, margin, freeMargin ',
						),
					),
					'condition' => "
						`t`.`id` = :idAccount
					",
					'params' => Array(
						':idAccount' => $idAccount,
					),
				));
				if( !$this->account ) $this->throwI18NException( "Can't find Account!" );
			}
			return $this->account;
		}
		private function getDP() {
			if( !$this->DP ) {
				$account = $this->getAccount();
				$this->DP = new CActiveDataProvider( self::modelName, Array(
					'criteria' => Array(
						'select' => ' id, SYMBOL, DEAL_TICKET, DEAL_TIME, DEAL_TYPE, DEAL_VOLUME, DEAL_SL, DEAL_TP, DEAL_PRICE, DEAL_SWAP, DEAL_PROFIT ',
						'condition' => "
							`t`.`ACCOUNT_NUMBER` = :accountNumber
							AND `t`.`AccountServerId` = :idServer
						",
						'order' => " 
							`t`.DEAL_TYPE IN ( 'buy', 'sell' ) DESC, `t`.`DEAL_TIME` DESC
						",
						'params' => Array(
							':accountNumber' => $account->accountNumber,
							':idServer' => $account->idServer,
						),
					),
					'pagination' => $this->getDPPagination(),
				));
				$this->DP->pagination->pageVar = "tradesPage";
				if( Yii::App()->request->isAjaxRequest ){
					$this->DP->pagination->route = 'single';
				}
			}
			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getSums() {
			$account = $this->getAccount();
			
			$sums = Yii::App()->db->createCommand("
				SELECT			(
									SELECT		SUM( `DEAL_SWAP` )
									FROM		`mt5_account_trades`
									WHERE		`ACCOUNT_NUMBER` = :accountNumber
										AND		`AccountServerId` = :idServer
								) AS `sumSwap`,
								(
									SELECT		SUM( `DEAL_PROFIT` )
									FROM		`mt5_account_trades`
									WHERE		`ACCOUNT_NUMBER` = :accountNumber
										AND		`AccountServerId` = :idServer
										AND		`DEAL_TYPE` IN ( '', '0', '1' )
								) AS `sumProfit`;
			")->setFetchMode( PDO::FETCH_OBJ )->queryRow( true, Array(
				':accountNumber' => $account->accountNumber,
				':idServer' => $account->idServer,
			));
						
			return $sums;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
				'DP' => $this->getDP(),
				'account' => $this->getAccount(),
				'sums' => $this->getSums(),
			));
		}
	}

?>