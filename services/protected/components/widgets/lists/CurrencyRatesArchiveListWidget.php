<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesArchiveListWidget extends WidgetBase {
		
		public $year;
		public $mo = false;
		public $day = false;
		
		const modelName = 'CurrencyRatesModel';
		public $DP;
		public $dataType;

		public $ajax = false;
		public $showOnEmpty = false;

		private function createDP( ) {
			return CurrencyRatesModel::getArchiveListDp( $this->year, $this->mo, $this->day, $this->dataType );
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		
		
		function run() {
			$class = $this->getCleanClassName();
			
			$DP = $this->getDP();

			$cacheKey = 'CurrencyRatesArchiveGridView' . implode('-', $DP->params ) . '.dataType.' . $this->dataType;
			
			if($this->beginCache($cacheKey , array('duration'=>60*60*24))) {
				$this->render( "{$class}/view", Array(
					'DP' => $DP,
					'ajax' => $this->getAjax(),
					'showOnEmpty' => true,
					'year' => $this->year,
					'mo' => $this->mo,
					'day' => $this->day,
					'dataType' => $this->dataType,
				));
			$this->endCache(); }
		}
	}

?>