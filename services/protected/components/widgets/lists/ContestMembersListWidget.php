<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMembersListWidget extends WidgetBase {
		const modelName = 'ContestMemberModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $idContest;
		public $accountNumber;
        public $sortField = 'equity';
		public $sortType = 'DESC';
		const MINCountRows = 10;
		const MAXCountRows = 100;
		private function getSortField() {
			$sortField = @$_GET[ 'sortField' ];
			if( !in_array( $sortField, Array( 'user_login', 'accountNumber', 'country_name', 'gain', 'drow', 'balance', 'equity', 'tradese', 'open', 'status', 'revision' ))) $sortField = $this->sortField;
			return $sortField;
		}
		private function getSortType() {
			$sortType = @$_GET[ 'sortType' ];
			if( !in_array( $sortType, Array( 'ASC', 'DESC' ))) $sortType = $this->sortType;
			return $sortType;
		}
		private function getAccountNumber() {
			if( $this->accountNumber ) return $this->accountNumber;
			return @$_GET[ 'accountNumber' ];
		}
		private function getCountRows() {
			$countRows = abs((int)@$_COOKIE[ 'ContestMembersListWidget_countRows' ]);
			if( !$countRows ) $countRows = $this->DPLimit;
			$countRows = max( $countRows, self::MINCountRows );
			$countRows = min( $countRows, self::MAXCountRows );
			return $countRows;
		}
		private function getIDContest() {
			if( $this->idContest ) return $this->idContest;
			return (int)@$_GET[ 'id' ];
		}
		private function getOrderDP() {
			$sortField = $this->getSortField();
			$sortType = $this->getSortType();
			switch( $sortField ) {
				case 'user_login': 
					return "
						IF( `user`.`display_name`, `user`.`display_name`, IF( `user`.`user_nicename`, `user`.`user_nicename`, `user`.`user_login` )) {$sortType}
					";
				case 'accountNumber': 
					return " `t`.`accountNumber` {$sortType} ";
				case 'country_name': 
					$idLanguage = LanguageModel::getCurrentLanguageID();
					return " 
						`user`.`idCountry` IS NULL,
						( 
							SELECT 			{{message_i18n}}.`value`
							FROM 			{{message}} 
							INNER JOIN 		{{message_i18n}} ON `idMessage` = {{message}}.`id` AND `idLanguage` = {$idLanguage}
							WHERE 			`key` = `country`.`name` 
							LIMIT 			1
						) 
						{$sortType}
					";
				case 'gain': 
					return " `stats`.`gain` IS NULL, `stats`.`gain` {$sortType} ";
				case 'drow': 
					return " `stats`.`drowMax` IS NULL, `stats`.`drowMax` {$sortType} ";
				case 'equity': 
					return " `stats`.`equity` IS NULL, `stats`.`equity` {$sortType}, IF( `stats`.`last_error_code` = -255, 'invalid_account', `t`.`status`) {$sortType}  ";
				case 'balance': 
					return " `stats`.`balance` IS NULL, `stats`.`balance` {$sortType} ";
				case 'tradese': 
					return " `stats`.`countTrades` {$sortType} ";
				case 'open': 
					return " `stats`.`countOpen` {$sortType} ";
				case 'status': 
					return " IF( `stats`.`last_error_code` = -255, 'invalid_account', `t`.`status`) {$sortType} ";
					//return " `stats`.`last_error_code`, `t`.`status` {$sortType} ";
				case 'revision':
					return " `t`.`revision` {$sortType} ";
			}
		}
		private function createDP($type = 'contest_members') {
            switch ($type) {
                case 'contest_members':
                    $extraCondition = "AND (`t`.`status` = 'participates' OR `t`.`status` = 'completed') AND (`stats`.`last_error_code` = 0 OR `stats`.`last_error_code` = -1 OR `stats`.`last_error_code` = -2 OR `stats`.`last_error_code` IS NULL)";
                    break;
                case 'disqualified_members':
                    $extraCondition = "AND (`stats`.`last_error_code` = 0 OR `stats`.`last_error_code` IS NULL ) AND `t`.`status` = 'disqualified'";
                    break;
                case 'with_error':
                    $extraCondition = "AND `stats`.`last_error_code` = -255";
                    break;
            }

			$idContest = $this->getIDContest();
			
			$c = new CDbCriteria();
			$c->select = " id, idContest, accountNumber, status ";
			
			$c->with['stats'] = Array(
				'select' => " gain, drowMax, balance, equity, countTrades, countOpen, trueDT, last_error_code, place ",
			);
			$c->with['user'] = Array(
				'select' => " ID, user_login, user_nicename, display_name, firstName, lastName ",
			);
			$c->with['user.country'] = Array(
				'select' => " id, name, alias ",
			);
			$c->condition = " `t`.`idContest` = :idContest $extraCondition";
			$c->params[ ':idContest' ] = $idContest;
			
			$accountNumber = $this->getAccountNumber();
			if( $accountNumber ) {
				$accountNumber = CommonLib::escapeLike( $accountNumber );
				$accountNumber = CommonLib::filterSearch( $accountNumber );
				$conditionUser = "
					OR `user`.`user_login` LIKE :accountNumber
					OR `user`.`user_nicename` LIKE :accountNumber
					OR `user`.`display_name` LIKE :accountNumber
				";
				if( substr_count( $accountNumber, '@' )) {
					$conditionUser .= " OR `user`.`user_email` LIKE :accountNumber ";
				}
				$c->condition .= " AND ( `t`.`accountNumber` LIKE :accountNumber {$conditionUser} ) ";
				$c->params[ ':accountNumber' ] = $accountNumber;
			}
			
			$c->order = $this->getOrderDP();

            $DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));

			$DP->pagination->pageVar = "membersPage";
			$DP->pagination->pageSize = $this->getCountRows();
			//$DP->pagination->pageSize = 1;
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'single';
			}
			
			return $DP;
		}
		private function getDP() {
            if(!$this->DP) {
                $types = array('contest_members', 'disqualified_members', 'with_error');
                foreach ($types as $type) {
                    $this->DP[$type] = $this->createDP($type);
                }
            }

			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				'idContest' => $this->getIDContest(),
				'countRows' => $this->getCountRows(),
				'accountNumber' => $this->getAccountNumber(),
				'sortField' => $this->getSortField(),
				'sortType' => $this->getSortType(),
			));
		}
	}

?>
