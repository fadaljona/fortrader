<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminPostsStatsListWidget extends WidgetBase {
		const modelName = 'PostsStatsCommonModel';
		public $DP;
		public $filterURL;
		public $filterModel;
		public $startPostDate;
		public $endPostDate;
		private $inSearch = false;
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function createDP() {
			$c = new CDbCriteria();
			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				if( strlen( $filterModel->title )) {
					$title = CommonLib::escapeLike( $filterModel->title );
					$title = CommonLib::filterSearch( $title );
					$c->addCondition( "
						`t`.`title` LIKE :title
					");
					$c->params[':title'] = $title;
					$this->setInSearch();
				}
				if( strlen( $filterModel->postId )) {
					$postId = trim( $filterModel->postId );
					$c->addCondition( "
						`t`.`postId` = :postId
					");
					$c->params[':postId'] = $postId;
					$this->setInSearch();
				}
				if( $this->startPostDate && $this->endPostDate ){
					$c->addCondition( "
						`t`.`postDate` >= :startPostDate AND `t`.`postDate` <= :endPostDate
					");
					$c->params[':startPostDate'] = $this->startPostDate;
					$c->params[':endPostDate'] = date('Y-m-d H:i:s', strtotime( $this->endPostDate ) + 60*60*24);
					$this->setInSearch();
				}
			}
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
				'sort' => array(
					'sortVar' => 'sort',
					'attributes' => array(
						'postId' => array(
							'default'=>'desc',
						),
						'title' => array(
							'default'=>'desc',
						),
						'commentCount' => array(
							'default'=>'desc',
						),
						'postViews' => array(
							'default'=>'desc',
						),
						'interest' => array(
							'default'=>'desc',
						),
						'fbLikes' => array(
							'default'=>'desc',
						),
						'fbShare' => array(
							'default'=>'desc',
						),
						'vkShare' => array(
							'default'=>'desc',
						),
						'tw' => array(
							'default'=>'desc',
						),
						'gp' => array(
							'default'=>'desc',
						),
						'postDate' => array(
							'default'=>'desc',
						),
					),
					'defaultOrder'=>array(
						'postDate'=>CSort::SORT_DESC,
					)
				)
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
				'startPostDate' => $this->startPostDate,
				'endPostDate' => $this->endPostDate
			));
		}
	}

?>