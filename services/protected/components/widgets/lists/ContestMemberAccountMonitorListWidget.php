<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberAccountMonitorListWidget extends WidgetBase {
		public $DP;
		public $ajax = false;
		public $memberId;
		
		private function getMember() {
			return ContestMemberModel::model()->findByPk( $this->memberId );
		}

		private function getDP() {
			if( !$this->DP ) {
				$member = $this->getMember();
				
				$this->DP = new CActiveDataProvider( 'MT4AccountMonitorModel', Array(
					'criteria' => Array(
						'select' => ' dateAdd, comment, accountHistory, openDeals, balance, equity ',
						'condition' => "
							`t`.`accountNumber` = :accountNumber
							AND `t`.`accountServerId` = :idServer

						",
						'order' => " `t`.`dateAdd` ASC ",
						'params' => Array(
							':accountNumber' => $member->accountNumber,
							':idServer' => $member->idServer,
						),
					),
					'pagination' => $this->getDPPagination(),
				));
				
				$this->DP->pagination->pageVar = "accountMonitorPage";
				if( Yii::App()->request->isAjaxRequest ){
					$this->DP->pagination->route = 'single';
				}
			}
			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
				'DP' => $this->getDP(),
				'memberId' => $this->memberId,
			));
		}
	}

?>
