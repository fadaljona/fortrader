<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminBrokerRegulatorsListWidget extends WidgetBase {
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		private function getDP() {
			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
			));
		}
	}

?>