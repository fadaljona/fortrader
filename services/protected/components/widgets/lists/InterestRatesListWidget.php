<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class InterestRatesListWidget extends WidgetBase {
		const modelName = 'InterestRatesRateModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		private function createDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => array(
						'bank' => array(
							'with' => array( 'currentLanguageI18N' )
						),
						'currentLanguageI18N',
					),
					'order' => " `bank`.`order` DESC ",
					'condition' => " `cLI18NInterestRatesRateModel`.`title` <> '' AND `cLI18NInterestRatesRateModel`.`title` IS NOT NULL AND `cLI18NInterestRatesBankModel`.`title` <> '' AND `cLI18NInterestRatesBankModel`.`title` IS NOT NULL ",
				),
				'pagination' => $this->getDPPagination(),
			));
			$DP->pagination->pageVar = "interestRatePage";
			//$DP->pagination->pageSize = 3;
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'index';
			}
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
			));
		}
	}

?>