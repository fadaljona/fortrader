<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminForeignContestsListWidget extends WidgetBase {
		public $modelName = 'ContestForeignModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		private function getDP() {
			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				'modelName' => $this->modelName
			));
		}
	}

?>