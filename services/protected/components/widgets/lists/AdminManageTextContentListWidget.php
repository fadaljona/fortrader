<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	
	final class AdminManageTextContentListWidget extends WidgetBase {
		public $modelName;
		public $modelFormName;
		
		private function getDp(){
			$modelName = $this->modelName;
			$dp = $modelName::model()->getManageTextContentDp();
			$dp->pagination = $this->getDPPagination();
			$dp->pagination->pageSize = 10;
			return $dp;
		}
		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}

		function run() {
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDp(),
				'langs' => $this->getLanguages(),
				'modelFormName' => $this->modelFormName,
			));
		}
	}

?>