<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	
	final class YourMonitoringAccountsListWidget extends WidgetBase {
		public $type = 'ft';
		public $forumId = false;
		
		private function getDp(){

			if( !Yii::app()->user->id ) return false;
			
			$c = new CDbCriteria();
			$c->with[ 'server' ] = Array();
			$c->with[ 'stats' ] = Array();
			$c->with[ 'mt5AccountInfo' ] = Array();

			$c->addCondition( " `t`.`idContest` = 0 AND `t`.`idUser` = :idUser " );
			
			if( $this->type == 'frameForForum' ){
				$c->addCondition( " `server`.`forReports` = 0 " );
			}
				
			$c->params[':idUser'] = Yii::app()->user->id;
			$c->order = ' `t`.`id` DESC ';
			
			$DP = new CActiveDataProvider( 'ContestMemberModel', Array(
				'criteria' => $c,
				'pagination' => array(
					'pageSize' => 10,
					'pageVar' => 'yourMonitoringAccountsListPage'
				),
				//'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}

		function run() {
			$dp = $this->getDp();
	//		if( !$dp->totalItemCount ) return false;
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $dp,
				'type' => $this->type,
				'forumId' => $this->forumId
			));
		}
	}

?>