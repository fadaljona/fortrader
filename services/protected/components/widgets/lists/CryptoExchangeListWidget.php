<?php
Yii::import('components.widgets.base.WidgetBase');

final class CryptoExchangeListWidget extends WidgetBase {
    
    const MODEL_NAME = 'CryptoCurrenciesExchangesSymbolToOurSymbolModel';
    public $DP;
       
    public $ajax = false;
    public $showOnEmpty = true;

    public $symbol = 133;

    private function createDP()
    {
        $c = new CDbCriteria();
        $c->with['ourSymbol'] = array();
        $c->with['stats'] = array();
        $c->with['exchange'] = array();
      
        $c->condition = " `ourSymbol`.`id` IS NOT NULL AND `stats`.`idSymbolToSymbol` IS NOT NULL ";
        //$c->order = "`cLI18NQuotesSymbolsModel`.`weightInCat` DESC ";

        if ($this->symbol && QuotesSymbolsModel::model()->findByPk($this->symbol)) {
            $c->addCondition(" `t`.`symbolId` = :symbolId ");
            $c->params[':symbolId'] = $this->symbol;
        }

        $c->order = '`stats`.`volume` DESC';
        
        $DP = new CActiveDataProvider(self::MODEL_NAME, array(
            'criteria' => $c,
            'pagination' => false
        ));
        return $DP;
    }
        
    private function getDP()
    {
        return $this->DP ? $this->DP : $this->createDP();
    }
        
        
    public function run()
    {
        $class = $this->getCleanClassName();
        $DP = $this->getDP();

        $this->render("{$class}/view", array(
            'DP' => $DP,
            'ajax' => $this->getAjax(),
            'showOnEmpty' => $this->showOnEmpty,
        ));
    }
}
