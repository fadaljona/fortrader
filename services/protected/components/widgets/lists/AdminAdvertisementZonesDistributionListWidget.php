<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminAdvertisementZonesDistributionListWidget extends WidgetBase {
		public $DP;
		private function createDP() {
			
			$selectSql = 'SELECT';
			
			$selectCountSql = ' COUNT(`zones`.`name`) ';
			$selectFieldsSql = ' 
				`zones`.`name` as `name`, 
				`zones`.`id` as `zId`, 
				`zones`.`site` as `site`, 
				`campaign`.`name` as `campaignName`, 
				`campaign`.`id` as `cId`, 
				`campaign`.`weight` as `campaignWeight`, 
				`ads`.`header` as `adsName`, 
				`ads`.`id` as `adsId`, 
				`ads`.`weight` as `adsWeight`, 
				`owner`.`idBroker` as `idBroker`, 
				`campaign`.`id` as `idCampaign`
			';
			
			$fromSql = 'FROM {{advertisement_zone}} `zones`';
			
			$joinSql = '
				LEFT JOIN {{advertisement_zone_to_advertisement}} `zoneToAd` ON `zoneToAd`.`idZone` = `zones`.`id`
				LEFT JOIN {{advertisement}} `ads` ON `ads`.`id` = `zoneToAd`.`idAd`
				LEFT JOIN {{advertisement_campaign}} `campaign` ON `campaign`.`id` = `ads`.`idCampaign`
				LEFT JOIN `wp_users` `owner` ON `owner`.`ID` = `campaign`.`idUser`
			';
			
			$whereSql = '
				where 
					`ads`.`dontShow` = 0 
					AND `ads`.`type` = "Image"  
					AND `campaign`.`status` = "Active" 
					AND CURDATE() BETWEEN `campaign`.`begin` AND `campaign`.`end` 
					AND `owner`.`balance` > 0
			';
			
			$count=Yii::app()->db->createCommand( $selectSql . $selectCountSql . $fromSql . $joinSql . $whereSql )->queryScalar();
			
			$DP = new CSqlDataProvider( $selectSql . $selectFieldsSql . $fromSql . $joinSql . $whereSql , array(
				'totalItemCount'=>$count,
				'sort'=>array(
					'attributes'=>array(
						'name', 'campaignName'
					),
					'defaultOrder' => array( 
						'name' => CSort::SORT_ASC,
						'campaignName' => CSort::SORT_ASC,
					),
				),
				'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
			));

			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
			));
		}
	}

?>