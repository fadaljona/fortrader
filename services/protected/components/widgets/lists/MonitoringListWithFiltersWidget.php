<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	
	final class MonitoringListWithFiltersWidget extends WidgetBase {
		private $DP;
		private $periods = array(
			'0' => 'All accounts',
			'1' => '> 1 Day',
			'30' => '> 30 Days',
			'90' => '> 3 Months',
		);
		private $sortTypes = array(
			'views' => 'By views',
			'comments' => 'By comments',
			'balance' => 'By balance',
			'equity' => 'By equity',
			'todayDeals' => 'By today deals',
			'totalDeals' => 'By total deals',
			'balanceDay' => 'By day balance',
			'gain' => 'Gain',
		);
		private $pageSize = 10;
		
		public $period;
		public $sortType;
		public $sortTypeOrder;
		public $secondarySortType;
		public $secondarySortTypeOrder;
		public $accountSearchInput = false;
		public $pagesCount = 1;
		public $negativFilter = 1;
		public $accountType = 'demo';
		
		public function getPageSize(){
			return $this->pageSize;
		}
		public function getDp(){
			if( !$this->DP ) return $this->createDp();
			return $this->DP;
		}
		private function setParameters(){
			if( !array_key_exists($this->period, $this->periods) ) $this->period = 0;
			if( !array_key_exists($this->sortType, $this->sortTypes) ) $this->sortType = 'views';
			if( $this->sortTypeOrder != 'ASC' && $this->sortTypeOrder != 'DESC' ) $this->sortTypeOrder = 'DESC';
			if( !array_key_exists($this->secondarySortType, $this->sortTypes) ) $this->secondarySortType = 'comments';
			if( $this->secondarySortTypeOrder != 'ASC' && $this->secondarySortTypeOrder != 'DESC' ) $this->secondarySortTypeOrder = 'ASC';
		}
		private function getOrderParam( $sortType ){
			switch ($sortType) {
				case 'views':
					return ' `views`.`views` ';
					break;
				case 'comments':
					return ' `monitoringDecommentsPosts`.`commentCount` ';
					break;
				case 'balance':
					return ' `stats`.`balance` ';
					break;
				case 'equity':
					return ' `stats`.`equity` ';
					break;
				case 'todayDeals':
					return ' `monitoringStats`.`closedDealsToday` ';
					break;
				case 'totalDeals':
					return ' `stats`.`countTrades` ';
					break;
				case 'balanceDay':
					return ' `monitoringStats`.`balanceToday` ';
					break;
				case 'gain':
					return ' `stats`.`gain` ';
					break;
			}
			return ' `views`.`views` ';
		}
		private function createDp(){
			$this->setParameters();
			$c = new CDbCriteria();
			

			$c->with[ 'views' ] = Array();
			$c->with[ 'stats' ] = Array();
			$c->with[ 'monitoringStats' ] = Array();
			$c->with[ 'monitoringDecommentsPosts' ] = Array();

			$c->addCondition( " `t`.`idContest` = 0 ");
			
			if( $this->period ){
				$c->addCondition( " `t`.`createdDT` < :monitoringDate ");
				$c->params[':monitoringDate'] = date( 'Y-m-d H:i:s', time() - 60*60*24*$this->period );
			}
			
			$c->order = $this->getOrderParam( $this->sortType );
			$c->order .= ' ' . $this->sortTypeOrder;// . ', ';
	//		$c->order .= ' ' . $this->getOrderParam( $this->secondarySortType ) . ' ' . $this->secondarySortTypeOrder;

			if( $this->accountSearchInput ){
				$c->addCondition( " `t`.`accountNumber` like :accountSearchInput OR `t`.`accountTitle` like :accountSearchInput ");
				$accountSearchInput = CommonLib::escapeLike( $this->accountSearchInput );
				$accountSearchInput = CommonLib::filterSearch( $accountSearchInput );
				$c->params[':accountSearchInput'] = $accountSearchInput;
			}
			
			if( !$this->negativFilter ){
				$c->with[ 'server' ] = Array();
				$c->with[ 'mt5AccountInfo' ] = Array();
				$monitoringSettings = MonitoringSettingsModel::getModel();
				$inactiveDaysCondition = '';
				if( $monitoringSettings->inactiveDays ){
					$inactiveDaysCondition = ' AND `mt5AccountInfo`.`ACCOUNT_UPDATE` > :inactiveDaysTime';
					$c->params[':inactiveDaysTime'] = time() - 60*60*24*$monitoringSettings->inactiveDays;
				}
				$c->addCondition( " `mt5AccountInfo`.`ACCOUNT_UPDATE` IS NOT NULL AND `mt5AccountInfo`.`last_error_code` <> -255 AND `mt5AccountInfo`.`last_error_code` <> -1 AND `mt5AccountInfo`.`last_error_code` <> -254 AND `mt5AccountInfo`.`last_error_code` <> -3" . $inactiveDaysCondition);
			}
			
			if( $this->accountType == 'real' || $this->accountType == 'demo' ){
				$c->with[ 'server' ] = Array();
				$c->with[ 'mt5AccountInfo' ] = Array();
				if( $this->accountType == 'real' ){
					$c->addCondition( " `mt5AccountInfo`.`ACCOUNT_UPDATE` IS NOT NULL AND `mt5AccountInfo`.`IsDemo` = 0 ");
				}else{
					$c->addCondition( " `mt5AccountInfo`.`ACCOUNT_UPDATE` IS NOT NULL AND `mt5AccountInfo`.`IsDemo` = 1 ");
				}
			}
			
			
			$DP = new CActiveDataProvider( 'ContestMemberModel', Array(
				'criteria' => $c,
				'pagination' => array(
					'pageSize' => $this->pageSize * $this->pagesCount,
					'pageVar' => 'monitoringAccountsListPage'
				),
			));
			$this->DP = $DP;
			return $this->DP;
		}
		public function createGridView(){
			$this->setParameters();
			return $this->controller->createWidget( 'widgets.gridViews.MonitoringListWithFiltersGridViewWidget', Array(
				'NSi18n' => $this->getNSi18n(),
				'ins' => $this->getINS(),
				'showTableOnEmpty' => false,
				'sortField' => $this->sortType,
				'sortType' => $this->sortTypeOrder,
				'dataProvider' => $this->getDp(),
				'ajax' => true,
				'template' => '{items}',
			));
		}

		function run() {		
			$this->setParameters();
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDp(),
				'periods' => $this->periods,
				'gridWidget' => $this->createGridView(),
				'sortTypes' => $this->sortTypes,
				'period' => $this->period,
				'sortType' => $this->sortType,
				'sortTypeOrder' => $this->sortTypeOrder,
				'secondarySortType' => 'comments',
			//	'secondarySortTypeOrder' => $this->secondarySortTypeOrder,
				'pageSize' => $this->pageSize,
				'negativFilter' => $this->negativFilter,
				'accountType' => $this->accountType
			));
		}
	}

?>