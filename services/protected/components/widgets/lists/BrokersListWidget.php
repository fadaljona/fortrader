<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class BrokersListWidget extends WidgetBase {
		const modelName = 'BrokerModel';
		public $DP;
		public $AdsDP;
		public $FullDP;
		public $brokerAdsIds='';
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $name;
		public $sortField = 'place';
		public $sortType = 'ASC';
		public $type = 'broker';
		public $cookieRowsCount = 'BrokersListWidget_countRows';
		const MINCountRows = 10;
		const MAXCountRows = 100;
		private function getType() {
			if( isset( $_GET[ 'type' ] ) && $_GET[ 'type' ] && in_array( $_GET[ 'type' ], array( 'broker','binary' ) ) ) $this->type = $_GET[ 'type' ];
			return $this->type;
		}
		private function getSortField() {
			$sortField = @$_GET[ 'sortField' ];
			if( !in_array( $sortField, Array( 'place', 'name', 'year', 'country_name', 'minDepo', 'trust' ))) $sortField = $this->sortField;
			return $sortField;
		}
		private function getSortType() {
			$sortType = @$_GET[ 'sortType' ];
			if( !in_array( $sortType, Array( 'ASC', 'DESC' ))) $sortType = $this->sortType;
			return $sortType;
		}
		private function getName() {
			if( $this->name ) return $this->name;
			return @$_GET[ 'name' ];
		}
		private function getCountRows() {
			$countRows = abs((int)@$_COOKIE[ $this->cookieRowsCount ]);
			if( !$countRows ) $countRows = $this->DPLimit;
			$countRows = CommonLib::minimax( $countRows, self::MINCountRows, self::MAXCountRows );
			return $countRows;
		}
		private function getOrderDP() {
			$sortField = $this->getSortField();
			$sortType = $this->getSortType();
			
			$abBtnsOrder = ", `t`.`conditions` DESC, `t`.`terms` DESC, `t`.`site` DESC, `t`.`openAccount` DESC, `t`.`openDemoAccount` DESC, `t`.`id` DESC";
			
			switch( $sortField ) {
				case 'place': 
					return " `stats`.`place` {$sortType} " . $abBtnsOrder;
				case 'name': 
					return " 
						`cLI18NBroker`.`shortName` IS NULL,
						IF( `cLI18NBroker`.`shortName` = '', `cLI18NBroker`.`officialName`, `cLI18NBroker`.`shortName` ) {$sortType}
					" . $abBtnsOrder;
				case 'year': 
					return " `t`.`year` IS NULL, `t`.`year` {$sortType} " . $abBtnsOrder;
				case 'country_name': 
					$idLanguage = LanguageModel::getCurrentLanguageID();
					return " 
						`t`.`idCountry` IS NULL,
						( 
							SELECT 			{{message_i18n}}.`value`
							FROM 			{{message}} 
							INNER JOIN 		{{message_i18n}} ON `idMessage` = {{message}}.`id` AND `idLanguage` = {$idLanguage}
							WHERE 			`key` = `country`.`name` 
							LIMIT 			1
						) 
						{$sortType}
					" . $abBtnsOrder;
				case 'minDepo': 
					return " `t`.`minDeposit` IS NULL, `t`.`minDeposit` {$sortType} " . $abBtnsOrder;
				case 'trust': 
					return " `stats`.`trust` IS NULL, `stats`.`trust` {$sortType} " . $abBtnsOrder;
			}
		}
		private function createDP() {
			BrokerModel::updateStats();
			
			$this->getBrokerAdsIds();
			$exclude_brokers = '';
			if( $this->brokerAdsIds ){
				$exclude_brokers = " and `t`.`id` not in (".$this->brokerAdsIds.")";
			}
			
			$c = new CDbCriteria();
			
			$c->with[ 'stats' ] = Array(
				'select' => Array(
					" `stats`.* ",
					" DATEDIFF( NOW(), `stats`.`changePlaceDT` ) as placeBusyDays",
				),
			);
			$c->with[ 'country' ] = Array();
			$c->with[ 'regulators' ] = Array(
				'together' => false,
				'with' => Array(
					'currentLanguageI18N' => Array(),
					'i18ns' => Array(),
				),
			);
			$c->with[ 'currentLanguageI18N' ] = Array();
			$c->with[ 'i18ns' ] = Array();
			$c->condition = " `t`.`showInRating` = 1 ".$exclude_brokers;
			$c->order = $this->getOrderDP();
			
			$name = $this->getName();
			if( $name ) {
				$name = CommonLib::escapeLike( $name );
				$name = CommonLib::filterSearch( $name );
				$c->addCondition( " `cLI18NBroker`.`officialName` LIKE :name " );
				$c->addCondition( " `cLI18NBroker`.`brandName` LIKE :name ", "OR" );
				$c->addCondition( " `cLI18NBroker`.`shortName` LIKE :name ", "OR" );
				$c->params[ ':name' ] = $name;
			}
			
			$c->addCondition( " `t`.`type` = :type ");
			$c->params[':type'] = $this->getType();
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => false
				//'pagination' => $this->getDPPagination(),
			));
			/*$DP->pagination->pageVar = "brokersPage";
			$DP->pagination->pageSize = $this->getCountRows();
			
			
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'list';
			}*/
			
			return $DP;
		}
		
		
		private function getBrokerAdsIds() {
			
			if( ! $this->brokerAdsIds && !isset( $_GET['name'] ) ){
		
				$array_AdsIds = Yii::app()->db->createCommand()
						->select('users.idBroker')
						->from('{{advertisement_campaign}} campaign')
						->join('wp_users users', 'users.ID = campaign.idUser')
						->join('{{broker}} broker', 'users.idBroker = broker.id')
						->where( "campaign.type='Rating Brokers' and campaign.status='Active' and CURDATE() BETWEEN campaign.begin and campaign.end and broker.type = :type ", array( ':type' => $this->getType() ) )
						->queryAll();
				
				if( count( $array_AdsIds ) ){
					foreach ( $array_AdsIds as $AdsId )
					{
						if( $this->brokerAdsIds )
							$this->brokerAdsIds.=','.$AdsId['idBroker'];
						else
							$this->brokerAdsIds.=$AdsId['idBroker'];
					}
				}
			
			}
		
		}
		
		private function createAdsDP() {
			
			$this->getBrokerAdsIds();
			$exclude_brokers = '';
			if( $this->brokerAdsIds ){
				$exclude_brokers = " AND `t`.`id` IN ( ".$this->brokerAdsIds." )";
			
			
				$c = new CDbCriteria();
				
				$c->with[ 'stats' ] = Array(
					'select' => Array(
						" `stats`.* ",
						" DATEDIFF( NOW(), `stats`.`changePlaceDT` ) as placeBusyDays",
					),
				);
				$c->with[ 'country' ] = Array();
				$c->with[ 'regulators' ] = Array(
					'together' => false,
					'with' => Array(
						'currentLanguageI18N' => Array(),
						'i18ns' => Array(),
					),
				);
				$c->with[ 'currentLanguageI18N' ] = Array();
				$c->with[ 'i18ns' ] = Array();
				$c->condition = " `t`.`showInRating`".$exclude_brokers;
				$c->order = " `stats`.`place` ASC ";
				
				$name = $this->getName();
				if( $name ) {
					$name = CommonLib::escapeLike( $name );
					$name = CommonLib::filterSearch( $name );
					$c->addCondition( " `cLI18NBroker`.`officialName` LIKE :name " );
					$c->addCondition( " `cLI18NBroker`.`brandName` LIKE :name ", "OR" );
					$c->addCondition( " `cLI18NBroker`.`shortName` LIKE :name ", "OR" );
					$c->params[ ':name' ] = $name;
				}
				
				$AdsDP = new CActiveDataProvider( self::modelName, Array(
					'criteria' => $c,
					'pagination' => false
				));
				
				return $AdsDP;
			}
		}
		
		
		private function createFullDP() {
			
			$DP = $this->getDP();
			$AdsDP = $this->getAdsDP();
			

			
			$records = array();
			
			for ($i = 0; $i < $AdsDP->totalItemCount; $i++) {
				$data = $AdsDP->data[$i];
				$data->stats->originPlace = $data->stats->place;
				$data->stats->place=0;
				array_push($records, $data);
			}
			
			for ($i = 0; $i < $DP->totalItemCount; $i++) {
				$data = $DP->data[$i];
				$data->stats->originPlace = $data->stats->place;
				array_push($records, $data);
			}
			
			$dataProviderOptions['pagination'] = $this->getDPPagination();
			
			$provAll = new CArrayDataProvider($records, $dataProviderOptions );
			

			$provAll->pagination->pageVar = "brokersPage";
			$provAll->pagination->pageSize = $this->getCountRows();


			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'list';
			}

			
			return $provAll;
		}
		

		private function getAdsDP() {
			return $this->AdsDP ? $this->AdsDP : $this->createAdsDP();
		}
		
		private function getFullDP() {
			return $this->FullDP ? $this->FullDP : $this->createFullDP();

		}
		
		
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
		
			
			$provAll = $this->getFullDP();

			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $provAll,

				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				'countRows' => $this->getCountRows(),
				'name' => $this->getName(),
				'sortField' => $this->getSortField(),
				'sortType' => $this->getSortType(),
				'type' => $this->getType(),
				'cookieRowsCount' => $this->cookieRowsCount
			));
		}
	}

?>
