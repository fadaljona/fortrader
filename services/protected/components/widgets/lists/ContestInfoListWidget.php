<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestInfoListWidget extends WidgetBase {
		const modelName = 'ContestModel';
		public $DP;
		public $ajax = false;
		public $idContest;
		private function getIDContest() {
			$idContest = $this->idContest;
			if( !$idContest ) $idContest = (int)@$_GET['id'];
			return $idContest;
		}
		private function createDP() {
			$this->DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 'countMembers', 'servers', 'servers.broker' ),
					'condition' => "
						`t`.`id` = :id
					",
					'params' => Array(
						':id' => $this->getIDContest(),
					),
				),
				'pagination' => false,
			));
			return $this->DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		protected function createGridViewWidget() {
			$gridWidget = $this->createWidget( 'widgets.gridViews.ContestInfoGridViewWidget', Array(
				'NSi18n' => $this->getNSi18n(),
				'ins' => $this->getINS(),
				'dataProvider' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'template' => '{items}',
			));
			return $gridWidget;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
			));
		}
		function renderRow() {
			$gridWidget = $this->createGridViewWidget();
			$gridWidget->renderTableRow( 0 );
		}
	}

?>