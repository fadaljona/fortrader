<?
  Yii::import( 'widgets.base.ListWidgetBase' );

	abstract class ColumnListWidget extends ListWidgetBase {
    const modelName = '';
    public $ins;
    public $ajax;
    public $template;
    public $htmlOptions;
    public $NSi18n = '*';
    public $dataProvider;
		public $w = 'wColumnList';
		public $class = 'iColumnList';
		public $itemsCssClass = "columnList items";
		public $sortField;
		public $sortType = 'DESC';
    public $showOnEmpty = false;
    public $limit = 3;
		
		public function init() {
      parent::init();
      
      if (!Yii::App()->request->isAjaxRequest) {
        Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/assets-static/css/column-widget.css" );
      }
      
			$this->htmlOptions = array(
				'class' => "{$this->class} column-list {$this->ins} {$this->w}",
			);
      $this->limit = !empty($_GET['limit']) ? (int)$_GET['limit'] : $this->limit;
		}

    public function run( $view = null, $data = Array(), $return = false ) {
      $oldDBProfiling = CommonLib::getDBProfiling();
      if( $this->getDebug() && !$oldDBProfiling )
        CommonLib::enableDBProfiling();

      $result = $this->render("widgets.lists.views._parts.columnListBase", array('data' => $data), true);
  
      if( $this->getDebug() && !$oldDBProfiling )
        CommonLib::disableDBProfiling();

      if ($return) {
        return $result;
      } else {
        echo $result;
        
        return $this;
      }
    }

    public function formatName($data) {
      return CHtml::tag('div', array('class' => 'EAname wrap_link'), CHtml::link(CHtml::encode($data->name), $data->getSingleURL()));
    }

    public function formatTime($data) {
      $time = strtotime($data->createdDT);
      $today = strtotime(date('Y-m-d 00:00:00'));
      
      return CHtml::tag('div', array('class' => 'EAtime'), date(($time < $today ? 'd.m.Y ' : 'H:i'), strtotime($data->createdDT)));
    }
    
    public function detView() {                        // getView
      return "widgets.lists.views.{$this->getCleanClassName()}.view";
    }
  
    public function getCleanClassName( $class = null ) {
      $class = $this->resolveClassName( $class );
      $class = preg_replace( "#Widget$#", "", $class );
      $class[0] = strtolower($class[0]);
      return $class;
    }

    protected function resolveClassName( $class ) {
      return $class ? $class : get_class( $this );
    }
    
    protected  function getDP() {
      if (!$this->dataProvider) {
        $this->dataProvider = $this->createDP();
      }
      
      return $this->dataProvider;
    }
    
    protected function getAjax() {
      return $this->ajax;
    }
    
    protected function getShowOnEmpty() {
      return $this->showOnEmpty;
    }
    
    protected function createDP() {
      $c = new CDbCriteria();
      $c->order = " `t`.`{$this->sortField}` {$this->sortType} ";

      $DP = new CActiveDataProvider(static::modelName, array('criteria' => $c));

      return $DP;
    }

    protected function getNS() {
      return $this->ns;
    }

    protected function getINS() {
      $class = get_class( $this );
      
      return "ins{$class}";
    }

    public function getNSi18n() {
      return $this->NSi18n;
    }
    
    abstract protected function getColumnTitle();
    abstract protected function getColumnDesc();
}
?>