<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CalendarInformerEventsListWidget extends WidgetBase {
		const modelName = 'CalendarEvent2ValueModel';
		public $DP;
		
		public $impact;
		public $symbols;
		public $update;
		
		private $dataTimeZone = 3;

		private function addFilterTypesToCriteria( $c ) {
			if( isset(Yii::app()->request->cookies['ftVolatility']) && Yii::app()->request->cookies['ftVolatility']->value ){
				$c->addCondition( " `t`.`serious` = :serious " );
				$c->params[ ':serious' ] = Yii::app()->request->cookies['ftVolatility']->value;
			}

			if( isset(Yii::app()->request->cookies['ftCalendarCountries']) && Yii::app()->request->cookies['ftCalendarCountries']->value ){
				$ftCalendarCountries = Yii::app()->request->cookies['ftCalendarCountries']->value;
				$list = substr($list, 0, strlen($list) - 1) ;
				$c->addCondition( " `country`.`id` in (".$ftCalendarCountries.") " );
			}
		}
		private function createDP() {
			static $DP;
			
			if( !$DP ) {
				$c = new CDbCriteria();
				
				$c->select = Array(
					" id ",
					" dt ",
					" serious ",
					" indicator_id ",
					" before_value ",
					" before_value_sec ",
					" mb_value ",
					" mb_value_sec ",
					" fact_value ",
					" fact_value_sec ",
					" `cLI18NCalendarEvent`.`hasNews` as hasNews ",
					" `cLI18NCalendarEvent`.`indicator_description` as hasDesc ",
					" `cLI18NCalendarEvent`.`indicator_name` as indicatorName ",
					" `cLI18NCalendarEvent`.`before_value_name` as beforeValueName ",
				);
				
				$c->with[ 'event' ] = Array(
					'with' => array( 'country', 'currentLanguageI18N' )
				);
				$c->addCondition( " `cLI18NCalendarEvent`.`indicator_name` <> '' AND `cLI18NCalendarEvent`.`indicator_name` IS NOT NULL " );
				
				if( $this->impact != 'all' && $this->impact ){
					$impact = explode(',', $this->impact);
					$c->addInCondition( '`t`.`serious`', $impact);
				}
				if( $this->symbols != 'all' && $this->symbols ){
					$symbols = explode(',', $this->symbols);
					$c->addInCondition( '`event`.`currency`', $symbols);
				}

				$c->addCondition( " `t`.`dt` >= :begin " );
				$c->params[ ':begin' ] = date( "Y-m-d H:i");
					
				$c->order = " `t`.`dt` ASC ";

				$DP = new CActiveDataProvider( self::modelName, Array(
					'criteria' => $c,
					'pagination' => $this->getDPPagination(),
				));
			}
			
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function run() {
			$class = $this->getCleanClassName();
			
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'dataTimeZone' => $this->dataTimeZone,
				'impact' => $this->impact,
				'symbols' => $this->symbols,
				'update' => $this->update,
			));
		}
	}

?>