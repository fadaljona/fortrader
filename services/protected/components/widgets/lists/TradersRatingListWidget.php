<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class TradersRatingListWidget extends WidgetBase {
		const modelName = 'UserModel';
		public $DP;
		public $userId;

		private function createDP() {

			$c = new CDbCriteria();
			
			$c->with = Array('karma');
            $c->condition = " karma.karma IS NOT NULL ";
			$c->order = " karma.karma DESC ";

			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			//$DP->pagination->pageSize = 1;

			$this->DP = $DP;
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
			));
		}
	}

?>