<?
  Yii::import( 'widgets.lists.base.ColumnListWidget' );

	final class EALastColumnWidget extends ColumnListWidget {
    const modelName = 'EAModel';
    public $ns = 'nsEALastColumn';
    public $ins;
    public $ajax;
    public $template;
    public $htmlOptions;
    public $NSi18n = 'EA';
    public $dataProvider;
		public $w = 'wEALastColumn';
		public $class = 'iEALastColumn';
		public $itemsCssClass = "eaLastColumnList items";
		public $sortField = 'createdDT';
		public $sortType = 'DESC';
    public $showOnEmpty = false;

    public function init() {
      parent::init();

      if (!Yii::App()->request->isAjaxRequest) {
        Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/assets-static/css/ea-last-column-widget.css" );
      }
    }
    
    public function formatUser($data) {
      $user = UserModel::model()->findByPk($data->idUser);
      if ($user) {
        return CHtml::link(CHtml::encode($user->getShowName()), $user->getProfileURL());
      }
    }

    public function formatPlatform($data) {
      foreach ($data->tradePlatforms as $platform) {
        return CHtml::tag('div', array('class' => 'EAplatform'), $platform->name);
      }
    }

    protected function createDP() {
      $c = new CDbCriteria();
      $c->with['tradePlatforms'] = array();
      $c->order = " `t`.`{$this->sortField}` {$this->sortType} ";

      $DP = new CActiveDataProvider(static::modelName, array('criteria' => $c));

      return $DP;
    }

    protected function getColumnTitle() {
      return Yii::t($this->NSi18n, 'EA last column title');
    }

    protected function getColumnDesc() {
      return Yii::t($this->NSi18n, 'EA last column desc');
    }
}
?>