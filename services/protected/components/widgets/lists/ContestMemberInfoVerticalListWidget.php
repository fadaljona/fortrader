<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberInfoVerticalListWidget extends WidgetBase {
		const modelName = 'ContestMemberModel';
		public $model;
		public $ajax = false;
		public $idMember;
		private function getIDMember() {
			return $this->idMember;
		}
		private function loadModel() {
			$idMember = $this->getIDMember();
			$this->model = ContestMemberModel::model()->find( Array(
				'select' => ' idContest, idUser, accountNumber, status ',
				'with' => Array( 
					'user' => Array(
						'select' => ' user_login, user_nicename, display_name, firstName, lastName ',
					),
					'server' => Array( 
						'select' => ' id ',
					),
					'server.broker' => Array( 
						'select' => ' id ',
					),
					'server.broker.currentLanguageI18N' => Array(),
					'server.broker.i18ns' => Array(),
					'stats' => Array(
						'select' => Array( 
							' balance ',
							' IF( `stats`.`balance` IS NULL, NULL, `place` ) AS place ',
							' gain ',
							' drowMax ',
							' countTrades ',
							' countOpen ',
							' last_error_code ',
							' trueDT ',
							' equity ', 
							' profit ', 
							' deposit ', 
							' withdrawals ', 
							' countTrades ',
							' leverage ', 
							' leverageCganged ', 
							' leverageCgangedDate ', 
							' avgTradeTimeSec ', 
							' bestTrade ', 
							' worstTrade ', 
							' profitableTrades ', 
							' allTrades ',
						),
					),
				),
				'condition' => "
					`t`.`id` = :idMember
				",
				'limit' => 1,
				'params' => Array(
					':idMember' => $idMember,
				),
			));
			return $this->model;
		}
		private function getModel() {
			return $this->model ? $this->model : $this->loadModel();
		}
		private function getAjax() {
			return $this->ajax;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
			));
		}
	}

?>