<?
  Yii::import( 'widgets.lists.base.ColumnListWidget' );

	final class EAPopularColumnWidget extends ColumnListWidget {
    const modelName = 'EAModel';
    public $ns = 'nsEAPopularColumn';
    public $ins;
    public $ajax;
    public $template;
    public $htmlOptions;
    public $NSi18n = 'EA';
    public $dataProvider;
		public $w = 'wEAPopularColumn';
		public $class = 'iEAPopularColumn';
		public $itemsCssClass = "eaPopularColumnList items";
		public $sortField = 'countDownloads';
		public $sortType = 'DESC';
    public $showOnEmpty = false;

    public function init() {
      parent::init();

      if (!Yii::App()->request->isAjaxRequest) {
        Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/assets-static/css/ea-popular-column-widget.css" );
      }
    }
    
    protected function createDP() {
      $week = date('W');
      $year = date('Y');
      $date = new DateTime();

      $date->setISODate($year, $week);
      $startDate = $date->format('Y-m-d 00:00:00');
      
      $date->setISODate($year, $week, 7);
      $endDate = $date->format('Y-m-d 23:59:59');
      
      $c = new CDbCriteria();
      $c->select = "`t`.*, COUNT(`ead`.id) AS `downloaded`";
      $c->join = " JOIN `{{ea_version}}` `eav` ON `eav`.`idEA` = `t`.`id` LEFT JOIN `{{ea_version_download}}` `ead` ON `ead`.`idEAVersion` = `eav`.`id` AND `ead`.`downloadedDT` BETWEEN '{$startDate}' AND '{$endDate}' GROUP BY `t`.`id`"; 
      $c->order = " `downloaded` {$this->sortType} ";

      $DP = new CActiveDataProvider(static::modelName, array('criteria' => $c));

      return $DP;
    }

    public function formatPopular($data) {
      $week = date('W');
      $year = date('Y');
      $date = new DateTime();
      $interval = new DateInterval('P7D');

      $date->setISODate($year, $week);
      $date->sub($interval);
      $startLastDate = $date->format('Y-m-d H:i:s');

      $date->setISODate($year, $week, 7);
      $date->sub($interval);
      $endLastDate = $date->format('Y-m-d H:i:s');
      
      $downloadedBefore = EAVersionDownloadModel::getDownloadCount($data->id, $startLastDate, $endLastDate);
      $dt = $data->downloaded - $downloadedBefore;

      return CHtml::tag('div', array('class' => 'EApopular pull-right'), $data->downloaded . '&nbsp;&nbsp;' . ($dt > 0 ? '+' : '') . $dt);
    }

    protected function getColumnTitle() {
      return Yii::t($this->NSi18n, 'EA popular column title');
    }

    protected function getColumnDesc() {
      return Yii::t($this->NSi18n, 'EA popular column desc');
    }
    
}
?>