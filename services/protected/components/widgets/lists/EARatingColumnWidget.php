<?
  Yii::import( 'widgets.lists.base.ColumnListWidget' );

	final class EARatingColumnWidget extends ColumnListWidget {
    const modelName = 'EAModel';
    public $ns = 'nsEARatingColumn';
    public $ins;
    public $ajax;
    public $template;
    public $htmlOptions;
    public $NSi18n = 'EA';
    public $dataProvider;
		public $w = 'wEARatingColumn';
		public $class = 'iEARatingColumn';
		public $itemsCssClass = "eaRatingColumnList items";
		public $sortField = 'average';
		public $sortType = 'DESC';
    public $showOnEmpty = false;
    public $afterAjaxUpdate = "function(){ $('.nsEARatingColumn div.EArating input ').rating({readOnly: true}); }";

    public function init() {
      parent::init();

      if (!Yii::App()->request->isAjaxRequest) {
        Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/assets-static/css/ea-rating-column-widget.css" );
      }
    }
    
    protected function createDP() {
      $c = new CDbCriteria();
      $c->with['stats'] = array();
      $c->order = " `stats`.`{$this->sortField}` {$this->sortType} ";

      $DP = new CActiveDataProvider(static::modelName, array('criteria' => $c));

      return $DP;
    }

    protected function getColumnTitle() {
      return Yii::t($this->NSi18n, 'EA rating column title');
    }

    protected function getColumnDesc() {
      return Yii::t($this->NSi18n, 'EA rating column desc');
    }

    public function formatRating( $average ) {
      if( $average === null ) {
        return 0;
      }

      return floor($average / 20);
    }
}
?>