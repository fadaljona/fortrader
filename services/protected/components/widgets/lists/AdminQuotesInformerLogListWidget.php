<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminQuotesInformerLogListWidget extends WidgetBase {
		const modelName = 'QuotesInformerStatsModel';
		public $DP;
		public $startDate;
		public $endDate;
		private $inSearch = false;

		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function createDP() {
			$c = new CDbCriteria();

			if( $this->startDate && $this->endDate ){
				$c->addCondition( "
					`t`.`accessDate` >= :startDate AND `t`.`accessDate` <= :endDate
				");
				$c->params[':startDate'] = $this->startDate;
				$c->params[':endDate'] = date('Y-m-d H:i:s', strtotime( $this->endDate ) + 60*60*24);
				$this->setInSearch();
			}
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
				'sort' => array(
					'sortVar' => 'sort',
					'attributes' => array(
						'accessDate' => array(
							'default'=>'desc',
						),
						'domain' => array(
							'default'=>'desc',
						),
						'url' => array(
							'default'=>'desc',
						),
					),
					'defaultOrder'=>array(
						'accessDate'=>CSort::SORT_DESC,
					)
				)
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'inSearch' => $this->getInSearch(),
				'startDate' => $this->startDate,
				'endDate' => $this->endDate
			));
		}
	}

?>