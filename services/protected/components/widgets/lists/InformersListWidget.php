<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class InformersListWidget extends WidgetBase {
		public $title;
		private function getCats() {
			return InformersCategoryModel::model()->findAll(array(
				'with' => array('currentLanguageI18N'),
				'condition' => " `cLI18NInformersCategoryModel`.`title` IS NOT NULL AND `cLI18NInformersCategoryModel`.`title` <> ''  AND `t`.`hidden` = 0 ",
				'order' => " `t`.`order` DESC ",
			));
		}

		function run() {

			$cats = $this->getCats();
			if( !$cats ) return false;
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'title' => $this->title,
				'cats' => $cats,
			));
		}
	}

?>