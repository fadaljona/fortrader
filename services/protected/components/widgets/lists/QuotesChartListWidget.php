<?
Yii::import('components.widgets.base.WidgetBase');

final class QuotesChartListWidget extends WidgetBase{
	protected $quotes;
	
	private function getQuotes() {
		if( !$this->quotes ) {
			$this->quotes = QuotesCategoryModel::model()->findAll(Array(
				'with' => array( 
					'symbols' => array(
						'with' => array('currentLanguageI18N')
					),
					'currentLanguageI18N',
				),
				
				'order' => " `cLI18NQuotesSymbolsModel`.`toChartList` DESC ",
				'condition' => " `cLI18NQuotesCategoryModel`.`name` IS NOT NULL AND `cLI18NQuotesCategoryModel`.`name` <> '' AND `cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '' AND `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `symbols`.`histName` <> '' ",
			));
		}
		return $this->quotes;
	}

	function run() {
		$quotesCats = $this->getQuotes();
		if( !$quotesCats ) return false;
		
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'quotesCats' => $quotesCats,
		));
	}
	
}
?>