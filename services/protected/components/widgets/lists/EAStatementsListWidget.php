<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EAStatementsListWidget extends WidgetBase {
		const modelName = 'EAStatementModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		//public $name;
		public $sortField = 'version';
		public $sortType = 'ASC';
		public $idEA;
		public $idVersion;
		//const MINCountRows = 10;
		//const MAXCountRows = 100;
		private function getSortField() {
			$sortField = @$_GET[ 'sortField' ];
			if( !in_array( $sortField, Array( 'version' ))) $sortField = $this->sortField;
			return $sortField;
		}
		private function getSortType() {
			$sortType = @$_GET[ 'sortType' ];
			if( !in_array( $sortType, Array( 'ASC', 'DESC' ))) $sortType = $this->sortType;
			return $sortType;
		}
		/*
		private function getName() {
			if( $this->name ) return $this->name;
			return @$_GET[ 'name' ];
		}
		private function getCountRows() {
			$countRows = abs((int)@$_COOKIE[ 'BrokersListWidget_countRows' ]);
			if( !$countRows ) $countRows = $this->DPLimit;
			$countRows = CommonLib::minimax( $countRows, self::MINCountRows, self::MAXCountRows );
			return $countRows;
		}
		private function getOrderDP() {
			$sortField = $this->getSortField();
			$sortType = $this->getSortType();
			switch( $sortField ) {
				case 'version': 
					return " `t`.`version` {$sortType} ";
				
			}
		}
		*/
		private function createDP() {
			$c = new CDbCriteria();
			
			$c->select = "id,idUser,symbol,period,gain,drow,trades,type,views";
			
			$c->with[ 'i18ns' ] = Array(
				'together' => false,
			);
			$c->with[ 'currentLanguageI18N' ] = Array();
			$c->with[ 'countMessages' ] = Array();
			$c->with[ 'user' ] = Array(
				'select' => "ID,user_login",
			);
			
			if( $this->idEA ) {
				$c->with[ 'version' ] = Array();
				$c->addCondition( " `t`.`idEA` = :idEA " );
				$c->params[ ':idEA' ] = $this->idEA;
			}
			elseif( $this->idVersion ) {
				$c->addCondition( " `t`.`idVersion` = :idVersion " );
				$c->params[ ':idVersion' ] = $this->idVersion;
			}
			$c->order = " `t`.`date` DESC ";
					
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => null,
			));
			
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				//'countRows' => $this->getCountRows(),
				//'name' => $this->getName(),
				'sortField' => $this->getSortField(),
				'sortType' => $this->getSortType(),
				'idEA' => $this->idEA,
				'idVersion' => $this->idVersion,
			));
		}
	}

?>