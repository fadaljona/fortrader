<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberDealsHistoryListWidget extends WidgetBase {
		public $DP;
		public $ajax = false;
		public $idMember;
		public $member;

		private function getIDMember() {
			if( !$this->idMember ) $this->idMember = (int)@$_GET['id'];
			return $this->idMember;
		}
		private function getMember() {
			if( !$this->member ) {
				$idMember = $this->getIDMember();
				$this->member = ContestMemberModel::model()->find( Array(
					'select' => ' id, accountNumber, idServer ',
					'with' => Array( 
						'stats' => Array(
							'select' => ' profit, deposit, balance ',
						),
					),
					'condition' => "
						`t`.`id` = :idMember
					",
					'params' => Array(
						':idMember' => $idMember,
					),
				));
				if( !$this->member ) $this->throwI18NException( "Can't find Member!" );
			}
			return $this->member;
		}
		private function getDP() {
			if( !$this->DP ) {
				$member = $this->getMember();
				
				if( $member->server->tradePlatform->name == 'MetaTrader 5' ) {
					$this->DP = new CActiveDataProvider( 'MT5AccountHistoryDealsModel', Array(
						'criteria' => Array(
							'select' => ' id, DEAL_TIME, DEAL_ORDER, DEAL_TICKET, SYMBOL, DEAL_TYPE, DEAL_ENTRY, DEAL_VOLUME, DEAL_PRICE, DEAL_SWAP, DEAL_PROFIT ',
							'condition' => "
								`t`.`ACCOUNT_NUMBER` = :accountNumber
								AND `t`.`AccountServerId` = :idServer
							",
							'order' => " `t`.`DEAL_TIME` DESC",
							'params' => Array(
								':accountNumber' => $member->accountNumber,
								':idServer' => $member->idServer,
							),
						),
						'pagination' => $this->getDPPagination(),
					));
				}
				
				if( $member->server->tradePlatform->name == 'MetaTrader 4' ) {
					$this->DP = new CActiveDataProvider( 'MT4AccountHistoryModel', Array(
						'criteria' => Array(
							'select' => ' id, OrderTicket, OrderOpenTime, OrderType, OrderLots, OrderSymbol, OrderStopLoss, OrderTakeProfit, OrderCloseTime, OrderClosePrice, OrderSwap, OrderProfit ',
							'condition' => "
								`t`.`AccountNumber` = :accountNumber
								AND `t`.`AccountServerId` = :idServer

							",
							'order' => " `t`.`OrderCloseTime` DESC ",
							'params' => Array(
								':accountNumber' => $member->accountNumber,
								':idServer' => $member->idServer,

							),
						),
						'pagination' => $this->getDPPagination(),
					));
				}
				
				$this->DP->pagination->pageVar = "dealsHistoryPage";
				if( Yii::App()->request->isAjaxRequest ){
					$this->DP->pagination->route = 'single';
				}
			}
			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getSums() {
			$member = $this->getMember();
			
			if( $member->server->tradePlatform->name == 'MetaTrader 5' ) {
				$sums = Yii::App()->db->createCommand("
					SELECT			(
										SELECT		SUM( `DEAL_SWAP` )
										FROM		`mt5_account_history_deals`
										WHERE		`ACCOUNT_NUMBER` = :accountNumber
											AND		`AccountServerId` = :idServer
									) AS `sumSwap`,
									(
										SELECT		SUM( `DEAL_PROFIT` )
										FROM		`mt5_account_history_deals`
										WHERE		`ACCOUNT_NUMBER` = :accountNumber
											AND		`AccountServerId` = :idServer
											AND		( `DEAL_TYPE` = 'sell' OR `DEAL_TYPE` = 'buy' )
									) AS `sumProfit`;
				")->setFetchMode( PDO::FETCH_OBJ )->queryRow( true, Array(
					':accountNumber' => $member->accountNumber,
					':idServer' => $member->idServer,
				));
			}
			
			if( $member->server->tradePlatform->name == 'MetaTrader 4' ) {
				$sums = Yii::App()->db->createCommand("
					SELECT			(
										SELECT		SUM( `OrderSwap` )
										FROM		`mt4_account_history`
										WHERE		`AccountNumber` = :accountNumber
											AND		`AccountServerId` = :idServer

									) AS `sumSwap`,
									(
										SELECT		SUM( `OrderProfit` )
										FROM		`mt4_account_history`
										WHERE		`AccountNumber` = :accountNumber
											AND		`AccountServerId` = :idServer

											AND		( `OrderType` = 0 OR `OrderType` = 1 )
									) AS `sumProfit`;
				")->setFetchMode( PDO::FETCH_OBJ )->queryRow( true, Array(
					':accountNumber' => $member->accountNumber,
					':idServer' => $member->idServer,

				));
			}
			
			return $sums;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
				'DP' => $this->getDP(),
				'member' => $this->getMember(),
				'sums' => $this->getSums(),
			));
		}
	}

?>
