<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberOrdersHistoryListWidget extends WidgetBase {
		const modelName = 'MT5AccountHistoryOrdersModel';
		public $DP;
		public $ajax = false;
		public $idMember;
		public $member;
		private function getIDMember() {
			if( !$this->idMember ) $this->idMember = (int)@$_GET['id'];
			return $this->idMember;
		}
		private function getMember() {
			if( !$this->member ) {
				$idMember = $this->getIDMember();
				$this->member = ContestMemberModel::model()->find( Array(
					'select' => Array( 
						'id', 
						'accountNumber',
						'idServer',
						"( 
							SELECT 	COUNT(*) 
							FROM 	`mt5_account_history_orders` 
							WHERE 	`ACCOUNT_NUMBER` = `t`.`accountNumber` 
								AND `AccountServerId` = `t`.`idServer` 
						) AS countOrders",
						"( 
							SELECT 	COUNT(*) 
							FROM 	`mt5_account_history_orders` 
							WHERE 	`ACCOUNT_NUMBER` = `t`.`accountNumber` 
								AND `AccountServerId` = `t`.`idServer` 
								AND `ORDER_ENTRY` = 'filled'
						) AS countFilledOrders",
						"( 
							SELECT 	COUNT(*) 
							FROM 	`mt5_account_history_orders` 
							WHERE 	`ACCOUNT_NUMBER` = `t`.`accountNumber` 
								AND `AccountServerId` = `t`.`idServer` 
								AND `ORDER_ENTRY` = 'canceled'
						) AS countCanceledOrders"
					),
					'condition' => "
						`t`.`id` = :idMember
					",
					'params' => Array(
						':idMember' => $idMember,
					),
				));
				if( !$this->member ) $this->throwI18NException( "Can't find Member!" );
			}
			return $this->member;
		}
		private function getDP() {
			if( !$this->DP ) {
				$member = $this->getMember();
				$this->DP = new CActiveDataProvider( self::modelName, Array(
					'criteria' => Array(
						'select' => ' id, ORDER_TIME, ORDER_TICKET, SYMBOL, ORDER_TYPE, ORDER_VOLUME, ORDER_PRICE, ORDER_SL, ORDER_TP, ORDER_ENTRY ',
						'condition' => "
							`t`.`ACCOUNT_NUMBER` = :accountNumber
							AND `t`.`AccountServerId` = :idServer
						",
						'order' => " `t`.`ORDER_TIME` ",
						'params' => Array(
							':accountNumber' => $member->accountNumber,
							':idServer' => $member->idServer,
						),
					),
					'pagination' => $this->getDPPagination(),
				));
				$this->DP->pagination->pageVar = "ordersHistoryPage";
				if( Yii::App()->request->isAjaxRequest ){
					$this->DP->pagination->route = 'single';
				}
			}
			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
				'DP' => $this->getDP(),
				'member' => $this->getMember(),
			));
		}
	}

?>
