<?
  Yii::import( 'widgets.base.ListWidgetBase' );

	final class EAListViewWidget extends ListWidgetBase {
    public $ins;
    public $ajax;
    public $template;
    public $pagerCssClass;
    public $htmlOptions;
    public $NSi18n = '*';
    public $dataProvider;
		public $instance = 'list';
		public $w = 'wEAListView';
		public $class = 'iEAListView';
		public $rowHtmlOptionsExpression = ' Array( "idEA" => $data->id ) ';
		public $itemsCssClass = "dataList items";
		public $sortField;
		public $sortType;
    public $showTableOnEmpty = false;
    public $enablePagination = true;
    public $pager = Array(
      'class'=>'bootstrap.widgets.TbPager',
      'prevPageLabel' => '<i class="icon-double-angle-left"></i>',
      'nextPageLabel' => '<i class="icon-double-angle-right"></i>',
      'maxButtonCount' => ActionBase::DPPagesCount,
    );
		
		public function init() {
			$this->htmlOptions = array(
				'class' => "{$this->class} list-view {$this->ins} {$this->w}",
			);
      
      if($this->enablePagination && $this->dataProvider->getPagination()===false)
        $this->enablePagination=false;
      
      $_GET['sortField'] = $this->sortField;
      
      parent::init();
		}

    public function run( $view = null, $data = Array(), $return = false ) {
      $_GET['sortField'] = $this->sortField;
      $this->dataProvider->pagination->pageSize = SettingsModel::getModel()->common_eaPerPage;
      $oldDBProfiling = CommonLib::getDBProfiling();
      if( $this->getDebug() && !$oldDBProfiling )
        CommonLib::enableDBProfiling();

      $result = '';
      foreach ($this->dataProvider->getData() as $data) {
        $result .= $this->render( $view, array('data' => $data), $return );
      }
  
      if( $this->getDebug() && !$oldDBProfiling )
        CommonLib::disableDBProfiling();
  
      return $result;
    }

    public function formatName( $data ) {
      return CHtml::link( CHtml::encode( $data->name ), $data->getSingleURL() );
    }
    
    public function formatRating( $average ) {
      if( $average === null ) {
        return 0;
      } 
      
      return floor($average / 20);
    }

    public function formatViews($views) {
      if ($views > 10 && $views < 20) {
        $key = 'EA views';
      } else {
        $val = $views % 10;
        if ($val == 1) {
          $key = 'EA views1';
        } elseif ($val > 1 && $val < 5) {
          $key = 'EA views2';
        }
        else {
          $key = 'EA views';
        }
      }

      return $views . ' ' . CHtml::tag('span', array('class' => "text-grey"), Yii::t($this->NSi18n, $key));
    }

    public function detView() {                        // getView
      return "widgets.lists.views.{$this->getCleanClassName()}.view";
    }
  
    public function getCleanClassName( $class = null ) {
      $class = $this->resolveClassName( $class );
      $class = preg_replace( "#Widget$#", "", $class );
      $class[0] = strtolower($class[0]);
      return $class;
    }

    public function renderPager() {
      if(!$this->enablePagination)
        return;
  
      $pager=array();
      $class='CLinkPager';
      if(is_string($this->pager))
        $class=$this->pager;
      elseif(is_array($this->pager))
      {
        $pager=$this->pager;
        if(isset($pager['class']))
        {
          $class=$pager['class'];
          unset($pager['class']);
        }
      }
      $pager['pages'] = $this->dataProvider->getPagination();
  
      if($pager['pages']->getPageCount()>1)
      {
        echo '<div class="'.$this->pagerCssClass.'">';
        $this->widget($class,$pager);
        echo '</div>';
      }
      else
        $this->widget($class,$pager);
    }

    private function resolveClassName( $class ) {
      return $class ? $class : get_class( $this );
    }
}
?>