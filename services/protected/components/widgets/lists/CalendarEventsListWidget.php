<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CalendarEventsListWidget extends WidgetBase {
		const modelName = 'CalendarEventModel';
		const cookieFilterTypesVarName = 'wCalendarEventsList_filterTypes';
		public $DP;
		public $period;
		public $date;
		public $idsSkip;
		private function getPeriod() {
			if( $this->period ) return $this->period;
			$period = @$_GET[ 'period' ];
			if( !in_array( $period, Array( 'today', 'tomorrow', 'this_week', 'next_week','date_v', 'day', 'soon', 'soon_more','forthcoming','choice_dates' ))) $period = 'today';
			return $period;
		}
		function getRange() {
			static $eval = false;
			static $begin, $end, $more;
			
			if( $eval ) return Array( $begin, $end, $more );
					
			$begin = $end = $more = null;
			switch( $this->getPeriod() ) {
				case 'choice_dates':{
					$begin = date( "Y-m-d H:i:s", time());
					$end = date( "Y-m-d H:i:s", time()+4*60*60);
					break;
				}
				case 'forthcoming':{
					$begin = date( "Y-m-d H:i:s", time());
					$end = date( "Y-m-d H:i:s", time()+4*60*60);
					break;
				}
				case 'date_v':{
					//$begin = date("Y-m-d H:i", strtotime("Monday this week"));
					//$end = date( "Y-m-d H:i", strtotime("Monday this week")+7*24*3600-1);
					$begin = date( "Y-m-d", strtotime( preg_replace("|(\d+)\.(\d+)\.(\d+)|","$3-$2-$1",$_GET["d_view"])));
					$end = date( "Y-m-d", strtotime( preg_replace("|(\d+)\.(\d+)\.(\d+)|","$3-$2-$1",$_GET["d_view"]))+24*3600);
					break;
				}
				case 'today':{
					$begin = date( "Y-m-d H:i", strtotime( "today" ));
					$end = date( "Y-m-d H:i", strtotime( "today 23:59" ));
					break;
				}
				case 'tomorrow':{
					$begin = date( "Y-m-d H:i", strtotime( "tomorrow" ));
					$end = date( "Y-m-d H:i", strtotime( "tomorrow 23:59" ));
					break;
				}
				case 'this_week':{
					$begin = date("Y-m-d H:i", strtotime("Monday this week"));
					$end = date( "Y-m-d H:i", strtotime("Monday this week")+7*24*3600-1);
					break;
				}
				case 'next_week':{
					$begin = date("Y-m-d H:i", strtotime("Monday this week")+7*24*3600);
					$end = date( "Y-m-d H:i", strtotime("Monday this week")+14*24*3600-1);
					break;
				}
				case 'day':{
					$begin = date( "Y-m-d H:i", strtotime( $this->date ));
					$end = date( "Y-m-d H:i", strtotime( "{$this->date} 23:59" ));
					break;
				}
				case 'soon': {
					$begin = date( "Y-m-d H:i", time());
					break;
				}
				case 'soon_more':{
					$begin = date( "Y-m-d H:i", time());
					$end = date( "Y-m-d H:i", strtotime( "today 23:59" ));
					
					$c = new CDbCriteria();
					$c->addCondition( " `t`.`dt` BETWEEN :begin AND :end " );
					$c->params[ ':begin' ] = $begin;
					$c->params[ ':end' ] = $end;
					
					if( $this->idsSkip ) {
						$c->addNotInCondition( '`t`.`id`', $this->idsSkip );
					}
					
					if( !CalendarEventModel::model()->exists( $c )) {
						$begin = date( "Y-m-d H:i", strtotime( "tomorrow" ));
						$end = date( "Y-m-d H:i", strtotime( "tomorrow 23:59" ));
					}
				
					break;
				}
				case 'default':{
					$begin = date( "Y-m-d H:i", strtotime( "today" ));
					if( in_array( date( "N" ), Array( 6, 7 ))) {
						$end = date( "Y-m-d H:i", strtotime( "next Monday 23:59" ));
					}
					else{
						$end = date( "Y-m-d H:i", strtotime( "next Sunday 23:59" ));
					}
					break;
				}
			}
			
			$more = date( "Y-m-d", strtotime( "+1 day {$end}" ));
			$eval = true;
			return Array( $begin, $end, $more );
		}
		private function addFilterTypesToCriteria( $c ) {
			$types = @$_COOKIE[ self::cookieFilterTypesVarName ];
			$types = strlen( $types ) ? explode( ',', $types ) : Array();
			
			if ( isset($_GET['amount']) and $_GET['amount'] != 0) {
				$c->addCondition( " `t`.`serious` = :serious " );
				$c->params[ ':serious' ] = $_GET['amount'];
			}

			if ( isset($_GET['country_filters']) ) {
				$list = '';
				foreach ($_GET['country_filters'] as $one) {
					$list .= '"'.$one.'",';
				}
				$list = substr($list, 0, strlen($list) - 1) ;
				$c->addCondition( " `linkedCountry`.`alias` in (".$list.") " );
				//$c->params[ ':alias' ] = "au, jp";
			}

			/*if( $types ) {
				$serious = Array();
				foreach( $types as $title ) {
					$num = CalendarEventModel::getModelNumSerious( $title );
					if( $num !== false ) $serious[] = $num;
				}
				if( $serious ) {
					$c->addInCondition( " `t`.`serious` ", $serious );
				}
			}*/
		}
		private function createDP() {
			static $DP;
			
			if( !$DP ) {
				$c = new CDbCriteria();
				
				$c->select = Array(
					" id ",
					" dt ",
					" serious ",
					"
						`ruI18N`.`indicator` IS NOT NULL
						AND `ruI18N`.`country` IS NOT NULL
						AND EXISTS (
							SELECT 
								1
							FROM 
								`wp_posts`
							INNER JOIN 
								`wp_postmeta` AS `index` ON `index`.`post_id` = `wp_posts`.`ID` AND `index`.`meta_key` = 'index'
							INNER JOIN 
								`wp_postmeta` AS `country` ON `country`.`post_id` = `wp_posts`.`ID` AND `country`.`meta_key` = 'country' 
							WHERE
								`index`.`meta_value` = `ruI18N`.`indicator`
								AND `country`.`meta_value` = `ruI18N`.`country`
							LIMIT
								1
						) as hasNews
					",
					"
						EXISTS (
							SELECT
								1
							FROM
								`{{calendar_event_desc}}`
							WHERE
								`{{calendar_event_desc}}`.`idIndicator` = `t`.`indicator_id`
							LIMIT
								1
						) as hasDesc
					"
				);
				
				$c->join = "
					LEFT JOIN
						`{{calendar_event_i18n}}` AS `ruI18N` ON `ruI18N`.`idEvent` = `t`.`id` AND `idLanguage` = :idLanguageRU
				";
				
				$c->params[ ':idLanguageRU' ] = LanguageModel::getByAlias( 'ru' )->id;
				
					
				$c->with[ 'currentLanguageI18N' ] = Array(
					'select' => " indicator, country, before_value, mb_value, fact_value, idCountry ",
					'with' => Array( 
						"linkedCountry" => Array(
							'select' => " name, alias ",
						),
					),
				);
					
				list( $begin, $end, $more ) = $this->getRange();
				if( $begin and $end ) {
					$c->addCondition( " `t`.`dt` BETWEEN :begin AND :end " );
					$c->params[ ':begin' ] = $begin;
					$c->params[ ':end' ] = $end;
				}
				elseif( $begin ) {
					$c->addCondition( " `t`.`dt` >= :begin " );
					$c->params[ ':begin' ] = $begin;
				}

					
				$c->order = " `t`.`dt` ";
				
				if( $this->getPeriod() == 'soon' ) {
					$c->limit = 3;
				}
				if( $this->getPeriod() == 'soon_more' and $this->idsSkip ) {
					$c->addNotInCondition( '`t`.`id`', $this->idsSkip );
				}
					
				$this->addFilterTypesToCriteria( $c );

				$DP = new CActiveDataProvider( self::modelName, Array(
					'criteria' => $c,
					//'pagination' => $this->getDPPagination(),
					'pagination' => false,
				));
				//$DP->pagination->pageVar = "eventsPage";
				//$DP->pagination->pageSize = 1;
				//if( Yii::App()->request->isAjaxRequest ){
				//	$DP->pagination->route = 'list';
				//}
			}
			return $DP;
		}
		private function createGridWidget() {
			$ns = $this->getNS();
			$NSi18n = $this->getNSi18n();
			$ins = $this->getINS();
			
			$DP = $this->createDP();
			list( $begin, $end, $more ) = $this->getRange();
			if( $begin ) $begin = date( "Y-m-d", strtotime( $begin ));
			if( $end ) $end = date( "Y-m-d", strtotime( $end ));
			$gridWidget = $this->createWidget( 'widgets.gridViews.CalendarEventsGridViewWidget', Array(
				'period' => $this->getPeriod(),
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'showTableOnEmpty' => true,
				'dataProvider' => $DP,
				'template' => '{items}',
				'pagerCssClass' => 'pagination pagination-right',
				'begin' => $begin,
				'end' => $end,
				'selectionChanged' => "{$ns}.wCalendarEventsListWidget.selectionChanged",
			));
			return $gridWidget;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function renderTableBody() {
			$gridWidget = $this->createGridWidget();
			$gridWidget->renderTableBody();
		}
		function run() {
			$class = $this->getCleanClassName();
			list( $begin, $end, $more ) = $this->getRange();
			$this->render( "{$class}/view", Array(
				'period' => $this->getPeriod(),
				'gridWidget' => $this->createGridWidget(),
				'moreDate' => $this->createGridWidget(),
				'moreDate' => $more,
			));
		}
	}

?>
