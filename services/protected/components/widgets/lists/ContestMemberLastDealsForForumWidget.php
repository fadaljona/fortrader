<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	
	final class ContestMemberLastDealsForForumWidget extends WidgetBase {
		public $contestMember;
		
		private function getDp(){
			
			if( $this->contestMember->server->tradePlatform->name == 'MetaTrader 5' ){
				$cHist = new CDbCriteria();
				$cHist->select = Array(
					" `SYMBOL` ",
					" `DEAL_TYPE` ",
					" `DEAL_PRICE` AS `closePrice` ",
					" `DEAL_TIME` AS `timestamp` ",
					" `DEAL_PROFIT` AS `profit` ",
					" `DEAL_SWAP` AS `swap` ",
					" `DEAL_VOLUME` AS lots ",
					" `DEAL_COMMISSION` AS `commission` "
				);
				$cHist->addCondition( ' `ACCOUNT_NUMBER` = :accountNumber ');
				$cHist->addCondition( ' `AccountServerId` = :accountServerId');
				
				$cHist->params[':accountNumber'] = $this->contestMember->accountNumber;
				$cHist->params[':accountServerId'] = $this->contestMember->idServer;
				
				$cHist->order = " `DEAL_TIME` DESC";
				$cHist->limit = 5;
				
				return new CActiveDataProvider( 'MT5AccountHistoryDealsModel', Array(
					'criteria' => $cHist,
				));
				
			}elseif( $this->contestMember->server->tradePlatform->name == 'MetaTrader 4' ){
				$cHist = new CDbCriteria();
				$cHist->select = Array(
					" `OrderSymbol` AS `SYMBOL` ",
					" `OrderType` AS `DEAL_TYPE` ",
					" `OrderClosePrice` AS `closePrice` ",
					" `OrderCloseTime` AS `timestamp` ",
					" `OrderProfit` AS `profit` ",
					" `OrderSwap` AS `swap` ",
					" `OrderLots` AS lots ",
					" `OrderCommission` AS `commission` "
				);
				$cHist->addCondition( ' `AccountNumber` = :accountNumber' );
				$cHist->addCondition( ' `AccountServerId`  = :accountServerId' );
				$cHist->addCondition( ' `OrderType` = 0 OR `OrderType` = 1 ' );
				
				$cHist->params[':accountNumber'] = $this->contestMember->accountNumber;
				$cHist->params[':accountServerId'] = $this->contestMember->idServer;
				
				$cHist->order = " `OrderCloseTime` DESC";
				$cHist->limit = 5;
				
				return new CActiveDataProvider( 'MT4AccountHistoryModel', Array(
					'criteria' => $cHist,
					'pagination' => array(
						'pageSize' => 5
					)
				));
			}
		}

		function run() {
			if( !$this->contestMember ) return false;
			
			$dp = $this->getDp();
			if( !$dp->totalItemCount ) return false;
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $dp,
			));
		}
	}

?>