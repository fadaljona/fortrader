<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberTradesSymbolsListWidget extends WidgetBase {
		public $DP;
		public $ajax = false;
		public $idMember;
		public $member;
		public $revision;
		public $byDirection = false;
		public $byBuy = false;
		public $bySell = false;
		public $type = 'open';
		public $search = '';
		
		private $sortField = 'symbol';
		private $sortType = 'DESC';
		
		private $availableSortField = array('symbol', 'dealsCount', 'action', 'volume', 'swap', 'profit');
		private $availableSortType = array('DESC', 'ASC');
		
		private function setSortField(){
			if( isset($_GET['sortField']) && $_GET['sortField'] && in_array( $_GET['sortField'], $this->availableSortField ) ){
				$this->sortField = $_GET['sortField'];
			} 
		}
		private function setSortType(){
			if( isset($_GET['sortType']) && $_GET['sortType'] && in_array( $_GET['sortType'], $this->availableSortType ) ){
				$this->sortType = $_GET['sortType'];
			} 
		}
		
		private function getIDMember() {
			if( !$this->idMember ) $this->idMember = (int)@$_GET['id'];
			return $this->idMember;
		}
		private function getByDirection() {
			if( isset( $_GET['byDirection'] ) ){
				if( $_GET['byDirection'] == 1 ) return true;
				if( $_GET['byDirection'] == 0 ) return false;
			}
			return $this->byDirection;
		}
		private function getSearch() {
			if( isset( $_GET['search'] ) ){
				if( $_GET['search'] != '' ) return $_GET['search'];
			}
			return $this->search;
		}
		private function getFilteredSearch() {
			if( isset( $_GET['search'] ) ){
				if( $_GET['search'] != '' ) return CommonLib::filterSearch( CommonLib::escapeLike( $_GET['search'] ) );
			}
			return $this->search;
		}
		private function getType() {
			if( isset( $_GET['type'] ) ){
				if( $_GET['type'] == 'open' ) return 'open';
				if( $_GET['type'] == 'closed' ) return 'closed';
			}
			return $this->type;
		}
		private function getByBuy() {
			if( isset( $_GET['byBuy'] ) ){
				if( $_GET['byBuy'] == 1 ) return true;
				if( $_GET['byBuy'] == 0 ) return false;
			}
			return $this->byBuy;
		}
		private function getBySell() {
			if( isset( $_GET['bySell'] ) ){
				if( $_GET['bySell'] == 1 ) return true;
				if( $_GET['bySell'] == 0 ) return false;
			}
			return $this->bySell;
		}
		private function getMember() {
			if( !$this->member ) {
				$idMember = $this->getIDMember();
				$this->member = ContestMemberModel::model()->find( Array(
					'select' => ' id, accountNumber, idServer ',
					'with' => Array( 
						'stats' => Array(
							'select' => ' balance, equity, margin, freeMargin ',
						),
					),
					'condition' => "
						`t`.`id` = :idMember
					",
					'params' => Array(
						':idMember' => $idMember,
					),
				));
				if( !$this->member ) $this->throwI18NException( "Can't find Member!" );
			}
			return $this->member;
		}
		private function getOrder(){
			if( $this->sortField == 'symbol' ){
				return '`SYMBOL` ' . $this->sortType;
			}elseif( $this->sortField == 'dealsCount' ){
				return '`DEALS_COUNT` ' . $this->sortType;
			}elseif( $this->sortField == 'action' ){
				return '`DEAL_TYPE` ' . $this->sortType;
			}elseif( $this->sortField == 'volume' ){
				return '`SUM_DEAL_VOLUME` ' . $this->sortType;
			}elseif( $this->sortField == 'swap' ){
				return '`SUM_DEAL_SWAP` ' . $this->sortType;
			}elseif( $this->sortField == 'profit' ){
				return '`SUM_DEAL_PROFIT` ' . $this->sortType;
			}
		}
		
		private function getDP() {
			if( !$this->DP ) {
				$member = $this->getMember();
				
				
				if( $this->getType() == 'open' ){
					
					$groupBy = " `t`.`SYMBOL`, `t`.`DEAL_TYPE` ";
					$select = ' id, COUNT(id) AS DEALS_COUNT, SYMBOL, DEAL_TYPE, SUM( DEAL_VOLUME ) AS SUM_DEAL_VOLUME, SUM(DEAL_SWAP) AS SUM_DEAL_SWAP, SUM(DEAL_PROFIT) AS SUM_DEAL_PROFIT ';
					if( !$this->getByDirection() ){
						$groupBy = " `t`.`SYMBOL` ";
					} 
					
					$params = Array(
						':accountNumber' => $member->accountNumber,
						':idServer' => $member->idServer,
							
					);
					
					$addCondition = '';
					if( $this->getByDirection() && $this->getByBuy() ){
						$addCondition = " AND `t`.`DEAL_TYPE` = 0 ";
					} 
					if( $this->getByDirection() && $this->getBySell() ){
						$addCondition = " AND `t`.`DEAL_TYPE` = 1 ";
					} 
					if( $this->getFilteredSearch() ){
						$addCondition = " AND `t`.`SYMBOL` LIKE :filteredSearch ";
						$params[':filteredSearch'] = $this->getFilteredSearch();
					}
					
					$this->DP = new CActiveDataProvider( 'MT5AccountTradesModel', Array(
						'criteria' => Array(
							'select' => $select,
							'condition' => "
								`t`.`ACCOUNT_NUMBER` = :accountNumber
								AND `t`.`AccountServerId` = :idServer
								AND ( `t`.`DEAL_TYPE` = 0 OR `t`.`DEAL_TYPE` = 1 )
							" . $addCondition,
							'order' => $this->getOrder(),
							'group' => $groupBy,
							'params' => $params,
						),
						'pagination' => $this->getDPPagination(),
					));
					
				}elseif( $this->getType() == 'closed' ){
					
					
					if( $member->server->tradePlatform->name == 'MetaTrader 5' ){
						
						$groupBy = " `t`.`SYMBOL`, `t`.`DEAL_TYPE` ";
						$select = ' id, COUNT(id) AS DEALS_COUNT, SYMBOL, DEAL_TYPE, SUM( DEAL_VOLUME ) AS SUM_DEAL_VOLUME, SUM(DEAL_SWAP) AS SUM_DEAL_SWAP, SUM(DEAL_PROFIT) AS SUM_DEAL_PROFIT ';
						if( !$this->getByDirection() ){
							$groupBy = " `t`.`SYMBOL` ";
						} 
						
						$params = Array(
							':accountNumber' => $member->accountNumber,
							':idServer' => $member->idServer,
						);
						
						$addCondition = '';
						if( $this->getByDirection() && $this->getByBuy() ){
							$addCondition = " AND `t`.`DEAL_TYPE` = 'buy' ";
						} 
						if( $this->getByDirection() && $this->getBySell() ){
							$addCondition = " AND `t`.`DEAL_TYPE` = 'sell' ";
						} 
						if( $this->getFilteredSearch() ){
							$addCondition = " AND `t`.`SYMBOL` LIKE :filteredSearch ";
							$params[':filteredSearch'] = $this->getFilteredSearch();
						}
						
						$this->DP = new CActiveDataProvider( 'MT5AccountHistoryDeals', Array(
							'criteria' => Array(
								'select' => $select,
								'condition' => "
									`t`.`ACCOUNT_NUMBER` = :accountNumber
									AND `t`.`AccountServerId` = :idServer
									AND ( `t`.`DEAL_TYPE` = 'sell' OR `t`.`DEAL_TYPE` = 'buy' )
								" . $addCondition,
								'order' => $this->getOrder(),
								'group' => $groupBy,
								'params' => $params,
							),
							'pagination' => $this->getDPPagination(),
						));
						
					}elseif( $member->server->tradePlatform->name == 'MetaTrader 4' ){
						
						$groupBy = " `t`.`OrderSymbol`, `t`.`OrderType` ";
						$select = ' id, COUNT(id) AS DEALS_COUNT, OrderSymbol AS SYMBOL, OrderType, OrderType AS DEAL_TYPE, SUM( OrderLots ) AS SUM_DEAL_VOLUME, SUM(OrderSwap) AS SUM_DEAL_SWAP, SUM(OrderProfit) AS SUM_DEAL_PROFIT ';
						if( !$this->getByDirection() ){
							$groupBy = " `t`.`OrderSymbol` ";
						} 
						
						$params = Array(
							':accountNumber' => $member->accountNumber,
							':idServer' => $member->idServer,	
						);
						
						$addCondition = '';
						if( $this->getByDirection() && $this->getByBuy() ){
							$addCondition = " AND `t`.`OrderType` = 0 ";
						} 
						if( $this->getByDirection() && $this->getBySell() ){
							$addCondition = " AND `t`.`OrderType` = 1 ";
						} 
						if( $this->getFilteredSearch() ){
							$addCondition = " AND `t`.`OrderSymbol` LIKE :filteredSearch ";
							$params[':filteredSearch'] = $this->getFilteredSearch();
						}
						
						$this->DP = new CActiveDataProvider( 'MT4AccountHistoryModel', Array(
							'criteria' => Array(
								'select' => $select,
								'condition' => "
									`t`.`AccountNumber` = :accountNumber
									AND `t`.`AccountServerId` = :idServer
									AND ( `t`.`OrderType` = 0 OR `t`.`OrderType` = 1 )
								" . $addCondition,
								'order' => $this->getOrder(),
								'group' => $groupBy,
								'params' => $params,
							),
							'pagination' => $this->getDPPagination(),
						));
						
					}
					
				}
				
				
				if( $this->DP ){
					$this->DP->pagination->pageVar = "tradesSymbolsPage" . $this->getType();
				//	$this->DP->pagination->pageSize = 2;
					if( Yii::App()->request->isAjaxRequest ){
						$this->DP->pagination->route = 'single';
					}
				}
				
			}
			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		function run() {
			$this->setSortField();
			$this->setSortType();
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
				'DP' => $this->getDP(),
				'member' => $this->getMember(),
				'byDirection' => $this->getByDirection(),
				'bySell' => $this->getBySell(),
				'byBuy' => $this->getByBuy(),
				'type' => $this->getType(),
				'search' => $this->getSearch(),
				'sortType' => $this->sortType,
				'sortField' => $this->sortField,
			));
		}
	}

?>
