<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EATradeAccountsListWidget extends WidgetBase {
		const modelName = 'EATradeAccountModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $name;
		public $sortField = 'gain';
		public $sortType = 'DESC';
		public $idEA;
		//const MINCountRows = 10;
		//const MAXCountRows = 100;
		private function getSortField() {
			$sortField = @$_GET[ 'sortField' ];
			if( !in_array( $sortField, Array( 'name', 'version', 'number', 'statement', 'type', 'gain', 'drow', 'tradese' ))) $sortField = $this->sortField;
			return $sortField;
		}
		private function getSortType() {
			$sortType = @$_GET[ 'sortType' ];
			if( !in_array( $sortType, Array( 'ASC', 'DESC' ))) $sortType = $this->sortType;
			return $sortType;
		}
		/*
		private function getName() {
			if( $this->name ) return $this->name;
			return @$_GET[ 'name' ];
		}
		private function getCountRows() {
			$countRows = abs((int)@$_COOKIE[ 'BrokersListWidget_countRows' ]);
			if( !$countRows ) $countRows = $this->DPLimit;
			$countRows = CommonLib::minimax( $countRows, self::MINCountRows, self::MAXCountRows );
			return $countRows;
		}
		*/
		private function getOrderDP() {
			$sortField = $this->getSortField();
			$sortType = $this->getSortType();
			switch( $sortField ) {
				case 'name': 
					return " `EA`.`name` {$sortType} ";
				case 'version': 
					return " `version`.`version` {$sortType} ";
				case 'number': 
					return " `tradeAccount`.`accountNumber` {$sortType} ";
				case 'statement': 
					return " `cLI18NEAStatement`.`title` {$sortType} ";
				case 'type': 
					return " `tradeAccount`.`type` {$sortType} ";
				case 'gain': 
					return " `stats`.`gain` {$sortType} ";
				case 'drow': 
					return " `stats`.`drowMax` {$sortType} ";
				case 'tradese': 
					return " `stats`.`countTrades` {$sortType} ";
			}
		}
		private function createDP() {
			TradeAccountModel::updateStats();
			
			$c = new CDbCriteria();
			
			$c->with[ 'tradeAccount' ] = Array(
				'with' => Array(
					'stats' => Array(
						'select' => "gain, drowMax, countTrades, trueDT",
					),
				),
			);
			
			$c->with[ 'EA' ] = Array();
			
			$c->with[ 'version' ] = Array(
				'select' => "id,version",
			);
			$c->with[ 'statement' ] = Array(
				'select' => "id",
				'with' => Array(
					'currentLanguageI18N', 
					'i18ns' => Array(
						'together' => false,
					),
				),
			);
			
			$c->order = $this->getOrderDP();
			
			if( $this->idEA ) {
				$c->addCondition( " `t`.`idEA` = :idEA " );
				$c->params[ ':idEA' ] = $this->idEA;
			}
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			if( $this->idEA ) {
				$DP->pagination = null;
			}
			else {
				$DP->pagination->pageVar = "eaAccountTradePage";
				$DP->pagination->pageSize = 10; //$this->getCountRows();
			}
			
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'list';
			}
			
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				//'countRows' => $this->getCountRows(),
				//'name' => $this->getName(),
				'sortField' => $this->getSortField(),
				'sortType' => $this->getSortType(),
				'idEA' => $this->idEA,
			));
		}
	}

?>