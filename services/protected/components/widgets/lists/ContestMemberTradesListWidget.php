<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberTradesListWidget extends WidgetBase {
		const modelName = 'MT5AccountTradesModel';
		public $DP;
		public $ajax = false;
		public $idMember;
		public $member;
		public $revision;
		private function getIDMember() {
			if( !$this->idMember ) $this->idMember = (int)@$_GET['id'];
			return $this->idMember;
		}
		private function getMember() {
			if( !$this->member ) {
				$idMember = $this->getIDMember();
				$this->member = ContestMemberModel::model()->find( Array(
					'select' => ' id, accountNumber, idServer ',
					'with' => Array( 
						'stats' => Array(
							'select' => ' balance, equity, margin, freeMargin ',
						),
					),
					'condition' => "
						`t`.`id` = :idMember
					",
					'params' => Array(
						':idMember' => $idMember,
					),
				));
				if( !$this->member ) $this->throwI18NException( "Can't find Member!" );
			}
			return $this->member;
		}
		private function getDP() {
			if( !$this->DP ) {
				$member = $this->getMember();
				$this->DP = new CActiveDataProvider( self::modelName, Array(
					'criteria' => Array(
						'select' => ' id, SYMBOL, DEAL_TICKET, DEAL_TIME, DEAL_TYPE, DEAL_VOLUME, DEAL_SL, DEAL_TP, DEAL_PRICE, DEAL_SWAP, DEAL_PROFIT ',
						'condition' => "
							`t`.`ACCOUNT_NUMBER` = :accountNumber
							AND `t`.`AccountServerId` = :idServer
							
						",
						'order' => " 
							`t`.DEAL_TYPE IN ( 'buy', 'sell' ) DESC, `t`.`DEAL_TIME` DESC
						",
						'params' => Array(
							':accountNumber' => $member->accountNumber,
							':idServer' => $member->idServer,
						
						),
					),
					'pagination' => $this->getDPPagination(),
				));
				$this->DP->pagination->pageVar = "tradesPage";
				if( Yii::App()->request->isAjaxRequest ){
					$this->DP->pagination->route = 'single';
				}
			}
			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getSums() {
			$member = $this->getMember();
			
			$sums = Yii::App()->db->createCommand("
				SELECT			(
									SELECT		SUM( `DEAL_SWAP` )
									FROM		`mt5_account_trades`
									WHERE		`ACCOUNT_NUMBER` = :accountNumber
										AND		`AccountServerId` = :idServer
										
								) AS `sumSwap`,
								(
									SELECT		SUM( `DEAL_PROFIT` )
									FROM		`mt5_account_trades`
									WHERE		`ACCOUNT_NUMBER` = :accountNumber
										AND		`AccountServerId` = :idServer
									
										AND		`DEAL_TYPE` IN ( '', '0', '1' )
								) AS `sumProfit`;
			")->setFetchMode( PDO::FETCH_OBJ )->queryRow( true, Array(
				':accountNumber' => $member->accountNumber,
				':idServer' => $member->idServer,
			
			));
						
			return $sums;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
				'DP' => $this->getDP(),
				'member' => $this->getMember(),
				'sums' => $this->getSums(),
			));
		}
	}

?>
