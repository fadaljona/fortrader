<?
Yii::import('components.widgets.base.WidgetBase');

final class DeCommentsListWidget extends WidgetBase{
	public $cat = 'default';
	public $modelName;
	public $limit = 0;
	public $label;
	
	private function getPostsIds(){
		$decommentsPosts = DecommentsPostsModel::model()->findAll(array(
			'select' => array( ' `id` ' ),
			'condition' => " `t`.`cat` = :cat ",
			'params' => array( 'cat' => $this->cat )
		));
		$outArr = array();
		foreach( $decommentsPosts as $post ){
			$outArr[] = $post->id;
		}
		return $outArr;
	}
	public function customDecomEndComment( $comment, $args, $depth ){
		$postKey = DecommentsPostsModel::getPostKey($comment->comment_post_ID);
		$arr = explode('.', $postKey);
		$returnStr = '';
		if( $arr[2] ){
			$id = preg_replace( '/[a-z,_]+_/', '', $arr[2] );
			$modelName = $this->modelName;
			$model = $modelName::model()->findByPk( $id );
			if( !$model ) return false;
			$returnStr = CHtml::link(
				$model->singleTitle, 
				Yii::App()->controller->createUrl( $arr[0] . '/' . $arr[1], array('slug' => $model->slug) ),
				array( 'target' => '_blank' )
			);
		}
		echo $returnStr . '</div>';
	}
	private function getDecomSettings(){
		$decomSettingsCacheKey = 'decomSettings' . Yii::app()->language;
		$decomSettingsCache = Yii::app()->cache->get($decomSettingsCacheKey);
		if( !$decomSettingsCache ){
			CommonLib::loadWp();
			$settings = array(
				'decom_get_options' => decom_get_options(),
				'option_show_avatars' => get_option( 'show_avatars' ),
				'texts' => array(
					'Add a picture' => __( 'Add a picture', DECOM_LANG_DOMAIN ),
					'Choose file' => __( 'Choose file', DECOM_LANG_DOMAIN ),
					'Submit' => __( 'Submit', DECOM_LANG_DOMAIN ),
					'Cancel' => __( 'Cancel', DECOM_LANG_DOMAIN ),
					'Attention' => __( 'Attention', DECOM_LANG_DOMAIN ),
					'Add a quote' => __( 'Add a quote', DECOM_LANG_DOMAIN ),
					'Subscribe' => __('Subscribe', DECOM_LANG_DOMAIN),
					'decom_comment_single_translate' => ' ' . _n( 'comment', 'comments', 1, DECOM_LANG_DOMAIN ),
					'decom_comment_twice_translate' => ' ' . _n( 'comment', 'comments', 2, DECOM_LANG_DOMAIN ),
					'decom_comment_plural_translate' => ' ' . _n( 'comment', 'comments', 5, DECOM_LANG_DOMAIN ),
					'Name' => __( 'Name', DECOM_LANG_DOMAIN ),
					'decom-name-author-val' => $_COOKIE['decommentsa'],
					'E-mail' => __( 'E-mail', DECOM_LANG_DOMAIN ),
					'Website' => __( 'Website', DECOM_LANG_DOMAIN ),
					'E-mail is already registered' => __( 'E-mail is already registered on the site. Please use the ', DECOM_LANG_DOMAIN ),
					'or' => __( ' or ', DECOM_LANG_DOMAIN ),
					'enter another' => __( 'enter another', DECOM_LANG_DOMAIN ),
					'incorrect username or password' => __( 'You entered an incorrect username or password', DECOM_LANG_DOMAIN ),
					'Password' => __( 'Password', DECOM_LANG_DOMAIN ),
					'Log in' => __( 'Log in', DECOM_LANG_DOMAIN ),
					'login form' => __( 'login form', DECOM_LANG_DOMAIN ),
					'Add comment' => __( 'Add comment', DECOM_LANG_DOMAIN ),
					'Log out' => __( 'Log out', DECOM_LANG_DOMAIN ),
					'Replies to my comments' => __( 'Replies to my comments', DECOM_LANG_DOMAIN ),
					'All comments' => __( 'All comments', DECOM_LANG_DOMAIN ),
					'Sorry, you must be logged in to post a comment.' => __( 'Sorry, you must be logged in to post a comment.', DECOM_LANG_DOMAIN ),
				),
			);
			Yii::app()->cache->set($decomSettingsCacheKey, $settings);
		}
		return Yii::app()->cache->get($decomSettingsCacheKey);
	}

	function run() {
		$this->cat = $this->cat . DecommentsPostsModel::getCatLangSuffix();
		$postIds = $this->getPostsIds();
		
		
		if( count($postIds) ){			
			$this->render( $this->getCleanClassName() . "/view", Array(
				'postIds' => $postIds,
				'limit' => $this->limit,
				'cat' => $this->cat,
				'label' => $this->label,
				'decomSettings' => $this->getDecomSettings(),
			));
		}
	}
	
}
?>