<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class UserWinersListWidget extends WidgetBase {
		public $idUser;
		private $DP;
		public $showOnEmpty = false;
		public $hidden = false;
		private function getDP() {
			if( !$this->DP ) {
				$this->DP = new CActiveDataProvider( 'ContestMemberModel', Array(
					'criteria' => Array(
						'with' => Array( 'contest', 'winer' ),
						'condition' => " `t`.`idUser` = :idUser AND `winer`.`place` IS NOT NULL AND `t`.`idContest` > 0 AND `contest`.`status` IS NOT NULL ",
						'order' => " `contest`.`begin` DESC ",
						'params' => Array(
							':idUser' => $this->idUser,
						),
					),
					'pagination' => $this->getDPPagination(),
				));
				$this->DP->pagination->pageVar = 'winnerMembersPage';
			}
			return $this->DP;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$dp = $this->getDP();
			
			if( !$dp->getTotalItemCount() ) return false;
			
			$this->render( "{$class}/view", Array(
				'DP' => $dp,
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
			));
		}
	}

?>