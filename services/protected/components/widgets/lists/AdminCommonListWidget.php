<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminCommonListWidget extends WidgetBase {
		public $modelName;
		public $gridViewWidget;
		public $loadRowRoute;
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $filterEnabled = false;
		public $enableSorting;
		public $filterModelName;
		public $filterUrl;
		private $filterModel = NULL;
		
		private function getDP() {
			$filterModel = $this->getFilterModel();
			$dp = call_user_func( array( $this->modelName, 'getAdminListDp' ), $filterModel );
			$dp->pagination = $this->getDPPagination();
			$dp->pagination->pageSize = 50;
			return $dp;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		private function getFilterURL(){
			if( $this->filterUrl ) return $this->filterUrl;
			return $this->controller->createUrl( $this->controller->action->id );
		}
		private function detFilterModel(){
			if( !$this->filterEnabled || $this->filterModel ) return;
			
			$stateKey = $this->controller->module->id . '.' . $this->controller->id . '.' . $this->controller->action->id . '.' . $this->modelName . '.filterModel';
			
			if( $this->filterModelName ){
				$modelName = $this->filterModelName;
			}else{
				$modelName = $this->modelName;
			}
			
			$this->filterModel = new $modelName;
			if( isset( $_POST[$modelName] ) ){
				$modelColumns = $this->filterModel->getMetaData()->tableSchema->getColumnNames();
				$modelToStore = $this->filterModel;
				foreach( $modelColumns as $column ){
					$postColumn = $_POST[$modelName][$column];
					if( isset( $postColumn ) && $postColumn != '' ){
						$modelToStore->{$column} = $postColumn;
					}
				}
				if( property_exists($modelName, 'extendedSearchFields') ){
					foreach( $this->filterModel->extendedSearchFields as $field ){
						$postColumn = $_POST[$modelName][$field];
						if( isset( $postColumn ) && $postColumn != '' ){
							$modelToStore->{$field} = $postColumn;
						}
					}
				}
				Yii::App()->user->setState( $stateKey, $modelToStore );
			}
			if( Yii::App()->user->getState( $stateKey ) ) $this->filterModel = Yii::App()->user->getState( $stateKey );
		}
		private function getFilterModel(){
			if( !$this->filterModel ) $this->detFilterModel();
			return $this->filterModel;
		}
		function run() {
			$this->detFilterModel();
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				'modelName' => $this->modelName,
				'gridViewWidget' => $this->gridViewWidget,
				'loadRowRoute' => $this->loadRowRoute,
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'enableSorting' => $this->enableSorting
			));
		}
	}

?>