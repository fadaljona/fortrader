<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminInformerStatsListWidget extends WidgetBase {
		const modelName = 'InformersStatsModel';
		public $DP;
		public $startDate;
		public $endDate;
		public $groupBy;
		public $informerCat;
		
		private $inSearch = false;

		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function createDP() {
			$c = new CDbCriteria();

			if( $this->startDate && $this->endDate ){
				$c->addCondition( "
					`t`.`accessDate` >= :startDate AND `t`.`accessDate` <= :endDate
				");
				$c->params[':startDate'] = $this->startDate;
				$c->params[':endDate'] = date('Y-m-d H:i:s', strtotime( $this->endDate ) + 60*60*24);
				$this->setInSearch();
			}
			
			$c->select = Array( " `domain` ", " count(`domain`) AS `count` ", " MAX( `accessDate` ) AS `lastDate` ", " `cat` ", " `style` ", " `isHtml` " );
			$c->group = 'isHtml, ' . $this->groupBy;
			
			if( $this->informerCat ){
				$c->addCondition( "
					`t`.`cat` = :informerCat
				");
				$c->params[':informerCat'] = $this->informerCat;
			}
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
				'sort' => array(
					'sortVar' => 'sort',
					'attributes' => array(
						'domain' => array(
							'default'=>'desc',
						),
						'count' => array(
							'default'=>'desc',
						),
						'lastDate' => array(
							'default'=>'desc',
						),
					),
					'defaultOrder'=>array(
						'count'=>CSort::SORT_DESC,
					)
				)
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getInformerCats(){
			$models = InformersCategoryModel::model()->findAll(array(
				'with' => array( 'currentLanguageI18N' )
			));
			$outArr = array( '0' => Yii::t('*', 'All categories') );
			
			foreach( $models as $model ){
				$outArr[$model->id] = $model->title;
			}
			return $outArr;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'inSearch' => $this->getInSearch(),
				'startDate' => $this->startDate,
				'endDate' => $this->endDate,
				'groupBy' => $this->groupBy,
				'informerCats' => $this->getInformerCats(),
				'informerCat' => $this->informerCat
			));
		}
	}

?>