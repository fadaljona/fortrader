<?php
Yii::import('components.widgets.base.WidgetBase');

final class CryptoCurrenciesPageListWidget extends WidgetBase {
    
    const MODEL_NAME = 'QuotesSymbolsModel';
    public $DP;
       
    public $ajax = false;
    public $showOnEmpty = false;

    private function createDP()
    {
        $c = new CDbCriteria();
        $c->with[ 'currentLanguageI18N' ] = array();
      
        $c->condition = " `cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '' AND `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`toCryptoCurrenciesPage` = 1 ";
        $c->order = "`cLI18NQuotesSymbolsModel`.`weightInCat` DESC ";
        
        $DP = new CActiveDataProvider(self::MODEL_NAME, array(
            'criteria' => $c,
            'pagination' => false
        ));
        return $DP;
    }
        
    private function getDP()
    {
        return $this->DP ? $this->DP : $this->createDP();
    }
        
        
    public function run()
    {
        $class = $this->getCleanClassName();
        $DP = $this->getDP();

        $settings = CryptoCurrenciesSettingsModel::getModel();
        if (!$settings->bitcoinSymbol) {
            $bitcoinKey = '';
        } else {
            $bitcoinKey = $settings->bitcoinSymbol->name;
        }

        $this->render("{$class}/view", array(
            'DP' => $DP,
            'ajax' => $this->getAjax(),
            'showOnEmpty' => true,
            'bitcoinKey' => $bitcoinKey,
        ));
    }
}
