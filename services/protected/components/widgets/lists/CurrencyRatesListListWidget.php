<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesListListWidget extends WidgetBase {

		const cacheTime = 5; //5mins
		const currenciesListPageCacheKey = 'currenciesListPageCacheKeyCurrencyRatesListListWidget';
		
		public $type = 'today';
		public $dataType = 'cbr';
		public $toCurrency;
		public $daySecondsToNewData = false;
		
		private $DP;
		private $tomorrowDP;
		
		private function createDP( $type = 'today' ) {
			return CurrencyRatesModel::getListDp( $type );
		}
		private function getDP() {
			if( $this->dataType == 'cbr' ){
				
				if( $this->type == 'today' ){
					return $this->DP ? $this->DP : $this->createDP('today');
				}elseif( $this->type == 'tomorrow' ){
					return $this->getTomorrowDP();
				}
				
			}elseif( $this->dataType == 'ecb' ){
				return CurrencyRatesModel::getEcbListDp( );
			}
			
		}
		private function getTomorrowDP() {
			if( !$this->tomorrowDP ) $this->tomorrowDP = $this->createDP('tomorrow');
			if( !$this->tomorrowDP->totalItemCount ){
				$this->tomorrowDP = new CActiveDataProvider('CurrencyRatesModel',array('data'=>array()));
			}
			return $this->tomorrowDP;
		}
	
		function run() {
			$cachKey = self::currenciesListPageCacheKey . 'type' . $this->type . 'dataType' . $this->dataType . 'toCurrency' . $this->toCurrency->id . '.lang.'. Yii::app()->language;
			
			if( $content = Yii::app()->cache->get( $cachKey ) ){
				echo $content;
				return;
			};
			
			ob_start();
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'type' => $this->type,
				'dataType' => $this->dataType,
				'toCurrency' => $this->toCurrency,
				'daySecondsToNewData' => $this->daySecondsToNewData
			));
			$content = ob_get_clean();
			
			Yii::app()->cache->set( $cachKey, $content, self::cacheTime * 60 );
			
			echo $content;
		}
	}

?>