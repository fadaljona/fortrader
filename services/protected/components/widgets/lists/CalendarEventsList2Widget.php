<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CalendarEventsList2Widget extends WidgetBase {
		const modelName = 'CalendarEvent2ValueModel';
		const cookieFilterTypesVarName = 'wCalendarEventsList_filterTypes';
		public $DP;
		public $period;
		public $date;
		public $idsSkip;
		public $idIndicator = false;
		public $fromEventDt = false;
		private $dataTimeZone = 3;
		private $forthcomingLimit = 20;
		public function getPeriod() {
			if( $this->period ) return $this->period;
			$period = @$_GET[ 'period' ];
			if( !in_array( $period, Array( 'today', 'tomorrow', 'this_week', 'next_week','date_v', 'day', 'soon', 'soon_more','forthcoming','choice_dates', 'futureOfEvent' ))) $period = 'today';
			if( isset($_GET["d_view"]) && $_GET["d_view"] ) $period = 'date_v';
			return $period;
		}
		private function setZoneToUserZone(){
			$zonesList = CommonLib::getTimeZonesWithOffsets();
			$userZone = UserProfileModel::getTimezone('cookie');
			
			if( $zonesList && isset($userZone) && isset( $zonesList[$userZone] ) ){
				date_default_timezone_set( $zonesList[$userZone] );
			}
		}
		function getRange() {
			static $eval = false;
			static $begin, $end, $more, $rangeTitle;
			
			if( $eval ) return Array( $begin, $end, $more, $rangeTitle );
			
			
			$defaultZone = date_default_timezone_get();
			
			$begin = $end = $more = null;
			
			$period = $this->getPeriod();
			
			switch( $period ) {
				case 'futureOfEvent':{
					if( !$this->idIndicator || !$this->fromEventDt ) return false;
					$begin = strtotime($this->fromEventDt); 
					break;
				}
				case 'forthcoming':{
				
					if( isset( $this->date ) && $this->date ){
						$begin = $this->date;
					}else{
						$begin = time();
					}
					$rangeTitle = Yii::t('*', 'Forthcoming events');
					
					break;
				}
				case 'date_v':{
	
					$this->setZoneToUserZone();
					
					$begin = strtotime( preg_replace("|(\d+)\.(\d+)\.(\d+)|","$3-$2-$1",$_GET["d_view"]));
					$end = strtotime( preg_replace("|(\d+)\.(\d+)\.(\d+)|","$3-$2-$1",$_GET["d_view"]))+24*3600-1;
					$rangeTitle = Yii::app()->dateFormatter->format( 'cccc, LLLL dd\yyyy',$begin);
					date_default_timezone_set( $defaultZone );
					
					break;
				}
				case 'day':{
					
					$this->setZoneToUserZone();
					
					$begin = strtotime( $this->date );
					$end = strtotime( "{$this->date} 23:59" );
					
					date_default_timezone_set( $defaultZone );
					
					break;
				}
				case 'today':{
	
					$this->setZoneToUserZone();
					
					$begin = strtotime( "today" );
					$end = strtotime( "today 23:59" );
					$rangeTitle = Yii::app()->dateFormatter->format( 'cccc, LLLL dd\yyyy',$begin);
					date_default_timezone_set( $defaultZone );
					
					break;
				}
				case 'tomorrow':{
					
					$this->setZoneToUserZone();
					
					$begin = strtotime( "tomorrow" );
					$end = strtotime( "tomorrow 23:59" );
					$rangeTitle = Yii::app()->dateFormatter->format( 'cccc, LLLL dd\yyyy',$begin);
					date_default_timezone_set( $defaultZone );
					
					break;
				}
				case 'this_week':{
					
					$this->setZoneToUserZone();
					
					$begin = strtotime("Monday this week");
					$end = strtotime("Monday this week")+7*24*3600-1;
					$rangeTitle = Yii::app()->dateFormatter->format( 'LLLL dd\yyyy',$begin) . ' - ' . Yii::app()->dateFormatter->format( 'LLLL dd\yyyy',$end);
					date_default_timezone_set( $defaultZone );
					
					break;
				}
				case 'next_week':{
					
					$this->setZoneToUserZone();
					
					$begin = strtotime("Monday this week")+7*24*3600;
					$end = strtotime("Monday this week")+14*24*3600-1;
					$rangeTitle = Yii::app()->dateFormatter->format( 'LLLL dd\yyyy',$begin) . ' - ' . Yii::app()->dateFormatter->format( 'LLLL dd\yyyy',$end);
					date_default_timezone_set( $defaultZone );
					
					break;
				}
				case 'default':{
					$this->setZoneToUserZone();
					
					$begin = strtotime( "today" );
					if( in_array( date( "N" ), Array( 6, 7 ))) {
						$end = strtotime( "next Monday 23:59" );
					}
					else{
						$end = strtotime( "next Sunday 23:59" );
					}
					date_default_timezone_set( $defaultZone );
					break;
				}
			}
			
			$this->setZoneToUserZone();
			$more = $end + 60*60*24;
			$more = date( 'Y-m-d' ,$more );
			date_default_timezone_set( $defaultZone );
			
			$eval = true;
			return Array( $begin, $end, $more, $rangeTitle );
		}
		private function addFilterTypesToCriteria( $c ) {
			if( isset(Yii::app()->request->cookies['ftVolatility']) && Yii::app()->request->cookies['ftVolatility']->value ){
				$c->addCondition( " `t`.`serious` = :serious " );
				$c->params[ ':serious' ] = Yii::app()->request->cookies['ftVolatility']->value;
			}

			if( isset(Yii::app()->request->cookies['ftCalendarCountries']) && Yii::app()->request->cookies['ftCalendarCountries']->value ){
				$ftCalendarCountries = Yii::app()->request->cookies['ftCalendarCountries']->value;
				$list = substr($list, 0, strlen($list) - 1) ;
				$c->addCondition( " `country`.`id` in (".$ftCalendarCountries.") " );
			}

		}
		private function createDP() {
			static $DP;
			
			if( !$DP ) {
				$c = new CDbCriteria();
				
				$c->select = Array(
					" id ",
					" dt ",
					" serious ",
					" indicator_id ",
					" before_value ",
					" before_value_sec ",
					" mb_value ",
					" mb_value_sec ",
					" fact_value ",
					" fact_value_sec ",
					" `cLI18NCalendarEvent`.`hasNews` as hasNews ",
					" `cLI18NCalendarEvent`.`indicator_description` as hasDesc ",
					" `cLI18NCalendarEvent`.`indicator_name` as indicatorName ",
					" `cLI18NCalendarEvent`.`before_value_name` as beforeValueName ",
				);
				
					
				$c->with[ 'event' ] = Array(
					'with' => array( 'country', 'currentLanguageI18N' )
				);
				$c->addCondition( " `cLI18NCalendarEvent`.`indicator_name` <> '' AND `cLI18NCalendarEvent`.`indicator_name` IS NOT NULL " );
					
				list( $begin, $end, $more, $rangeTitle ) = $this->getRange();
				if( $begin and $end ) {
					$c->addCondition( " `t`.`dt` BETWEEN :begin AND :end " );
					$c->params[ ':begin' ] = date( "Y-m-d H:i", $begin);
					$c->params[ ':end' ] = date( "Y-m-d H:i", $end);
				}
				elseif( $begin ) {
					$c->addCondition( " `t`.`dt` >= :begin " );
					$c->params[ ':begin' ] = date( "Y-m-d H:i", $begin);
				}

					
				$c->order = " `t`.`dt` ";
				
				if( $this->getPeriod() == 'forthcoming' ) {
					$c->limit = $this->forthcomingLimit;
				}
				if( $this->getPeriod() == 'forthcoming' and $this->idsSkip ) {
					$c->addNotInCondition( '`t`.`id`', $this->idsSkip );
				}
				if( $this->getPeriod() == 'futureOfEvent' ){
					$c->addCondition( '`t`.`dt` > :fromEventDt AND `t`.`indicator_id` = :idIndicator ' );
					$c->params[':fromEventDt'] = $this->fromEventDt;
					$c->params[':idIndicator'] = $this->idIndicator;
				}
					
				if( $this->getPeriod() != 'futureOfEvent' ) $this->addFilterTypesToCriteria( $c );

				$DP = new CActiveDataProvider( self::modelName, Array(
					'criteria' => $c,
					'pagination' => false,
				));
			}
			
			return $DP;
		}
		public function createGridWidget() {
			$ns = $this->getNS();
			$NSi18n = $this->getNSi18n();
			$ins = $this->getINS();
			
			$DP = $this->createDP();
			
			list( $begin, $end, $more, $rangeTitle ) = $this->getRange();
			
			$selectionChanged = 'false';
			$showTableOnEmpty = false;
			$gridEmptyText = false;
			if( $this->getPeriod() != 'futureOfEvent' ){
				$selectionChanged = "{$ns}.wCalendarEventsListWidget.selectionChanged";
				$showTableOnEmpty = true;
				$gridEmptyText = Yii::t('zii','No results found.');
			}
			
			if( $DP->totalItemCount && $this->getPeriod() == 'futureOfEvent' ) echo '<div class="calendar_info futureOfEvent clearfix"><h3 class="calendar_info_title">' . Yii::t($NSi18n, 'Event in future') . '</h3></div>';

			$gridWidget = $this->createWidget( 'widgets.gridViews.CalendarEventsGridViewWidget', Array(
				'period' => $this->getPeriod(),
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'showTableOnEmpty' => $showTableOnEmpty,
				'dataProvider' => $DP,
				'template' => '{items}',
				'pagerCssClass' => 'pagination pagination-right',
				'begin' => $begin,
				'end' => $end,
				'selectionChanged' => $selectionChanged,
				'dataTimeZone' => $this->dataTimeZone,
				'forthcomingLimit' => $this->forthcomingLimit,
				'emptyText' => $gridEmptyText
			));
			return $gridWidget;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function renderTableBody() {
			$gridWidget = $this->createGridWidget();
			$gridWidget->renderTableBody();
		}
		function run() {
			$class = $this->getCleanClassName();
			list( $begin, $end, $more, $rangeTitle ) = $this->getRange();
			$this->render( "{$class}/view", Array(
				'period' => $this->getPeriod(),
				'gridWidget' => $this->createGridWidget(),
				'moreDate' => $more,
				'rangeTitle' => $rangeTitle
			));
		}
	}

?>
