<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContactsListWidget extends WidgetBase {
		const modelName = 'ContactModel';
		private function getContacts() { 
			$contacts = ContactModel::model()->findAll( Array(
				'with' => Array( 'currentLanguageI18N' ),
				'condition' => " NOT `t`.`hidden` ",
				'order' => " `t`.`position` IS NULL, `t`.`position` ",
			));
			return $contacts;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'contacts' => $this->getContacts(),
			));
		}
	}

?>