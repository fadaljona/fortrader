<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminBrokersListWidget extends WidgetBase {
		const modelName = 'BrokerModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $filterURL;
		public $filterModel;
		private function createDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'order' => '`t`.`createdDT` DESC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
			));
		}
	}

?>