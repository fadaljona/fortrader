<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class InterestRatesReviewsWidget extends WidgetBase {
		private $lastDate;
		private $lastId;
		private $postIds = array();
		private $total;
		public $num = 6;
		private function setPostIds() {

			$this->total = InterestRatesValueModel::getTotalReviews();
			
			$models = InterestRatesValueModel::model()->findAll(array(
				'with' => array( 'currentLanguageI18N' ),
				'condition' => ' `cLI18NInterestRatesValueModel`.`postId` <> 0 AND `cLI18NInterestRatesValueModel`.`postId` IS NOT NULL ',
				'order' => ' `t`.`modDate` DESC, `t`.`id` ASC ',
				'limit' => $this->num
			));
			foreach( $models as $model ){
				$this->postIds[] = $model->postId;
			}
			$this->lastDate = $models[$this->num-1]->modDate;
			$this->lastId = $models[$this->num-1]->id;
		}

		function run() {
			$this->setPostIds();
			if( !count($this->postIds) ) return false;
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'lastDate' => $this->lastDate,
				'lastId' => $this->lastId,
				'postIds' => $this->postIds,
				'num' => $this->num,
				'total' => $this->total
			));
		}
	}

?>