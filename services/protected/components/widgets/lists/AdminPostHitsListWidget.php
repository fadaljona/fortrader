<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminPostHitsListWidget extends WidgetBase {
		const modelName = 'PostsStatsTrafficModel';
		public $DP;
		public $filterURL;
		public $filterModel;
		private $inSearch = false;
		public $postId;
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function createDP() {
			$c = new CDbCriteria();
			$filterModel = $this->getFilterModel();
			if( $filterModel ) {
				/*if( strlen( $filterModel->remoteHost )) {
					$remoteHost = CommonLib::escapeLike( $filterModel->remoteHost );
					$remoteHost = CommonLib::filterSearch( $remoteHost );
					$c->addCondition( "
						`t`.`remoteHost` LIKE :remoteHost
					");
					$c->params[':remoteHost'] = $remoteHost;
					$this->setInSearch();
				}*/
				if( strlen( $filterModel->keyword )) {
					$keyword = CommonLib::escapeLike( $filterModel->keyword );
					$keyword = CommonLib::filterSearch( $keyword );
					$c->addCondition( "
						`t`.`keyword` LIKE :keyword
					");
					$c->params[':keyword'] = $keyword;
					$this->setInSearch();
				}
				if( strlen( $filterModel->referer )) {
					$referer = CommonLib::escapeLike( $filterModel->referer );
					$referer = CommonLib::filterSearch( $referer );
					$c->addCondition( "
						`t`.`referer` LIKE :referer
					");
					$c->params[':referer'] = $referer;
					$this->setInSearch();
				}
			}
			
			if( $this->postId ){
				$c->addCondition( "
					`t`.`postId` LIKE :postId
				");
				$c->params[':postId'] = $this->postId;
			}
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
				'sort' => array(
					'sortVar' => 'sort',
					'defaultOrder'=>array(
						'visitDate'=>CSort::SORT_DESC,
					)
				),
				
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
			));
		}
	}

?>