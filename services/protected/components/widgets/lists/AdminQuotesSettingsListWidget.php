<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminQuotesSettingsListWidget extends WidgetBase {
		const modelName = 'QuotesSettingsModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = true;
		public $hidden = false;
		private function createDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'order' => '`t`.`name` ASC',
				),
				'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
			));
		}
	}
