<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EATradeAccountOrdersHistoryListWidget extends WidgetBase {
		const modelName = 'MT5AccountHistoryOrdersModel';
		public $DP;
		public $ajax = false;
		public $idAccount;
		public $account;
		private function getIDAccount() {
			return $this->idAccount;
		}
		private function getAccount() {
			if( !$this->account ) {
				$idAccount = $this->getIDAccount();
				$this->account = TradeAccountModel::model()->find( Array(
					'select' => Array( 
						'id', 
						'accountNumber',
						'idServer',
						"( 
							SELECT 	COUNT(*) 
							FROM 	`mt5_account_history_orders` 
							WHERE 	`ACCOUNT_NUMBER` = `t`.`accountNumber` 
								AND `AccountServerId` = `t`.`idServer` 
						) AS countOrders",
						"( 
							SELECT 	COUNT(*) 
							FROM 	`mt5_account_history_orders` 
							WHERE 	`ACCOUNT_NUMBER` = `t`.`accountNumber` 
								AND `AccountServerId` = `t`.`idServer` 
								AND `ORDER_ENTRY` = 'filled'
						) AS countFilledOrders",
						"( 
							SELECT 	COUNT(*) 
							FROM 	`mt5_account_history_orders` 
							WHERE 	`ACCOUNT_NUMBER` = `t`.`accountNumber` 
								AND `AccountServerId` = `t`.`idServer` 
								AND `ORDER_ENTRY` = 'canceled'
						) AS countCanceledOrders"
					),
					'condition' => "
						`t`.`id` = :idAccount
					",
					'params' => Array(
						':idAccount' => $idAccount,
					),
				));
				if( !$this->account ) $this->throwI18NException( "Can't find Account!" );
			}
			return $this->account;
		}
		private function getDP() {
			if( !$this->DP ) {
				$account = $this->getAccount();
				$this->DP = new CActiveDataProvider( self::modelName, Array(
					'criteria' => Array(
						'select' => ' id, ORDER_TIME, ORDER_TICKET, SYMBOL, ORDER_TYPE, ORDER_VOLUME, ORDER_PRICE, ORDER_SL, ORDER_TP, ORDER_ENTRY ',
						'condition' => "
							`t`.`ACCOUNT_NUMBER` = :accountNumber
							AND `t`.`AccountServerId` = :idServer
						",
						'order' => " `t`.`ORDER_TIME` ",
						'params' => Array(
							':accountNumber' => $account->accountNumber,
							':idServer' => $account->idServer,
						),
					),
					'pagination' => $this->getDPPagination(),
				));
				$this->DP->pagination->pageVar = "ordersHistoryPage";
				if( Yii::App()->request->isAjaxRequest ){
					$this->DP->pagination->route = 'single';
				}
			}
			return $this->DP;
		}
		private function getAjax() {
			return $this->ajax;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
				'DP' => $this->getDP(),
				'account' => $this->getAccount(),
			));
		}
	}

?>