<?
  Yii::import( 'widgets.lists.base.ColumnListWidget' );

	final class EACommentColumnWidget extends ColumnListWidget {
    const modelName = 'UserMessageModel';
    const commentLimit = 50; //50 chars
    public $ns = 'nsEACommentColumn';
    public $ins;
    public $ajax;
    public $template;
    public $htmlOptions;
    public $NSi18n = 'EA';
    public $dataProvider;
		public $w = 'wEACommentColumn';
		public $class = 'iEACommentColumn';
		public $itemsCssClass = "eaCommentColumnList items";
		public $sortField = 'createdDT';
		public $sortType = 'DESC';
    public $showOnEmpty = false;

    public function init() {
      parent::init();

      if (!Yii::App()->request->isAjaxRequest) {
        Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/assets-static/css/ea-comment-column-widget.css" );
      }
    }

    public function formatUser($data) {
      $user = UserModel::model()->findByPk($data->idUser);
      if ($user) {
        return CHtml::link(CHtml::encode($user->getShowName()), $user->getProfileURL());
      }
    }

    public function formatComment($data) {
      if (mb_strlen($data->text) > self::commentLimit) {
        $dotPos = mb_strpos($data->text, '.', self::commentLimit);
        $commentLength = $dotPos === false ? self::commentLimit : $dotPos;
        $data->text = mb_substr($data->text, 0, $commentLength);
      }
      
      return CHtml::tag('div', array('class' => 'EAcomment'), $data->text);
    }

    protected function createDP() {
      $c = new CDbCriteria();
      $c->condition = " `t`.`instance` = 'ea/single' ";
      $c->order = " `t`.`{$this->sortField}` {$this->sortType} ";

      $DP = new CActiveDataProvider(static::modelName, array('criteria' => $c));

      return $DP;
    }

    protected function getColumnTitle() {
      return Yii::t($this->NSi18n, 'EA comment column title');
    }

    protected function getColumnDesc() {
      return Yii::t($this->NSi18n, 'EA comment column desc');
    }
}
?>