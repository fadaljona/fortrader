<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EAListWidget extends WidgetBase {
		const modelName = 'EAModel';
		public $instance = 'list';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $name;
		public $sortField;
		public $platform = 'all';
		//public $sortType = 'DESC';
		//const MINCountRows = 10;
		//const MAXCountRows = 100;
		private function getSortField() {
			$sortField = @$_GET[ 'sortField' ];
			if( !in_array( $sortField, Array( 'name', 'author', 'comments', 'popular', 'rating', 'last' ))) $sortField = $this->sortField;
			return $sortField;
		}
		private function getSortType() {
			$sortField = $this->getSortField();
			switch( $sortField ) {
				case 'name': return " ASC ";
				case 'author': return " ASC ";
				case 'comments': return " DESC ";
				case 'popular': return " DESC ";
				case 'rating': return " DESC ";
				case 'last': return " DESC ";
			}
		}
		private function getCommercial() {
			return @$_GET[ 'commercial' ] == 'on' || !@$_GET[ 'dummy' ];
		}
		private function getFree() {
			return @$_GET[ 'free' ] == 'on' || !@$_GET[ 'dummy' ];
		}
    private function getPlatform() {
      $platform = @$_GET[ 'platform' ];
      if (!in_array($platform, TradePlatformModel::getPlatforms())) {
        $platform = $this->platform;
      }
      
      return $platform;
    }
    
		/*
		private function getName() {
			if( $this->name ) return $this->name;
			return @$_GET[ 'name' ];
		}
		private function getCountRows() {
			$countRows = abs((int)@$_COOKIE[ 'BrokersListWidget_countRows' ]);
			if( !$countRows ) $countRows = $this->DPLimit;
			$countRows = CommonLib::minimax( $countRows, self::MINCountRows, self::MAXCountRows );
			return $countRows;
		}
		*/
		private function getOrderDP() {
			$sortField = $this->getSortField();
			$sortType = $this->getSortType();
			switch( $sortField ) {
				case 'name': 
					return " `t`.`name` {$sortType} ";
				case 'author': 
					return " `t`.`creator` = '', `t`.`creator` {$sortType} ";
				case 'comments': 
					return " 
						(
							SELECT	COUNT(*)
							FROM	`{{user_message}}`
							WHERE	`{{user_message}}`.`instance` = 'ea/single'
								AND	`{{user_message}}`.`idLinkedObj` = `t`.`id`
						)
						{$sortType}
					";
				case 'popular': 
					return " `t`.`views` {$sortType} ";
				case 'rating': 
					return " `stats`.`average` {$sortType} ";
        case 'last':
          return " `t`.`createdDT` {$sortType} ";
			}
		}
		private function createDP() {
			EAModel::updateStats();
			
			$c = new CDbCriteria();
			
			if( $this->instance == 'list' ) {
				$commercial = $this->getCommercial();
				if( $commercial ) {
					$c->addCondition( "`t`.`type` = 'Commercial'" );
				}
				
				$free = $this->getFree();
				if( $free ) {
					$c->addCondition( "`t`.`type` = 'Free'", 'OR' );
				}

        $platform = $this->getPlatform();
        if($platform && $platform != 'all') {
          $c->with['tradePlatforms'] = array(
            'condition' => 'tradePlatforms.name = :platform',
            'params' => array(':platform' => $platform),
            'together' => true,
          );
        }
			}
			if( $this->instance == 'laboratory' ) {
				$c->addCondition( " FIND_IN_SET( 'LAB', `t`.`params` ) " );
				$c->with[ 'countVersions' ] = Array();
				$c->with[ 'countDownloads' ] = Array();
			}

			$c->with[ 'stats' ] = Array();
			
			$c->order = $this->getOrderDP();
			$c->with[ 'countMessages' ] = Array();
						
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			$DP->pagination->pageVar = "eaPage";
			$DP->pagination->pageSize = 10;//$this->getCountRows();
			
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'list';
			}
			
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
      $this->sortField = $this->instance == 'list' ? 'last' : 'rating';
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'instance' => $this->instance,
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				//'countRows' => $this->getCountRows(),
				//'name' => $this->getName(),
				'sortField' => $this->getSortField(),
				'sortType' => $this->getSortType(),
				'commercial' => $this->getCommercial(),
				'free' => $this->getFree(),
			));
		}
	}

?>