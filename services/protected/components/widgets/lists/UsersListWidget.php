<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'dps.UsersListDP' );
	
	final class UsersListWidget extends WidgetBase {
		const modelName = 'UserModel';
		public $DP;
		public $name = '';
		public $ajax = true;
		public $showOnEmpty = true;
		public $sortField = 'reputation';
		private function getSortField() {
			$sortField = @$_GET[ 'sortField' ];
			if( !in_array( $sortField, Array( 'name', 'reputation', 'activity', 'awards' ))) $sortField = $this->sortField;
			return $sortField;
		}
		private function getSortType() {
			$sortField = $this->getSortField();
			switch( $sortField ) {
				case 'name': return " ASC ";
				case 'reputation': return " DESC ";
				case 'activity': return " DESC ";
				case 'awards': return " DESC ";
			}
		}
		private function getOrderDP() {
			$sortField = $this->getSortField();
			$sortType = $this->getSortType();
			switch( $sortField ) {
				case 'name': 
					return " `t`.`display_name` = '', `t`.`display_name` {$sortType} ";
				case 'reputation': 
					return " `t`.`countReputation` {$sortType}, `t`.`countActivities` {$sortType} ";
				case 'activity': 
					return " `t`.`countActivities` {$sortType},	`t`.`countReputation` {$sortType} ";
				case 'awards': 
					return "
						(
							SELECT  
								COUNT(*)
							FROM	
								`{{contest_member}}`
							INNER JOIN	
								`{{contest_winer}}` ON `{{contest_winer}}`.`idMember` = `{{contest_member}}`.`id`
							WHERE	
								`{{contest_member}}`.`idUser` = `t`.`ID`
						)
						{$sortType},
						(
							SELECT  
								SUM( 10 - `{{contest_winer}}`.`place` )
							FROM	
								`{{contest_member}}`
							INNER JOIN	
								`{{contest_winer}}` ON `{{contest_winer}}`.`idMember` = `{{contest_member}}`.`id`
							WHERE	
								`{{contest_member}}`.`idUser` = `t`.`ID`
						)
						{$sortType},
						`t`.`countReputation` {$sortType}
					";
			}
		}
		private function createDP() {
			$traders = @$_GET[ 'traders' ] == 'on';
			$managers = @$_GET[ 'managers' ] == 'on';
			$investors = @$_GET[ 'investors' ] == 'on';
			$programmers = @$_GET[ 'programmers' ] == 'on';

			if( $traders || $managers || $investors || $programmers ) {
				$condition = ' FALSE ';
				if( $traders ) $condition .= " OR `profile`.`isTrader` ";
				if( $managers ) $condition .= " OR `profile`.`isManager` ";
				if( $investors ) $condition .= " OR `profile`.`isInvestor` ";
				if( $programmers ) $condition .= " OR `profile`.`isProgrammer` ";
			}
			else {
				$condition = '';
			}
			
			$c = new CDbCriteria();
			$cCount = new CDbCriteria();
			
			$c->with = Array( 
				'avatar',
				'memberContests' => Array(
					'together' => false,
					'with' => Array(
						'winer' => Array(
							'with' => Array(
								'contest' => Array(
									'with' => Array(
										'i18ns' => Array(
											'together' => false,
										),
										'currentLanguageI18N',
									),
								),
							),
						),
					),
				),
				'profile' => Array(
					'select' => " isTrader, isManager, isInvestor, isProgrammer ",
					'joinType' => $traders || $managers || $investors || $programmers ? "INNER JOIN" : "LEFT JOIN",
				),
			);
			
			$c->condition = $condition;
			$c->order = $this->getOrderDP();
			
			if( $traders || $managers || $investors || $programmers ) {
				$cCount->with = Array(
					'profile' => Array(
						'select' => false,
						'joinType' => "INNER JOIN",
					),
				);
			}
			
			if( $this->name ){
				
				$c->addCondition( "
					`t`.`user_login` LIKE :name OR `t`.`user_nicename` LIKE :name OR `t`.`user_email` LIKE :name OR `t`.`display_name` LIKE :name  OR `t`.`firstName` LIKE :name  OR `t`.`lastName` LIKE :name	
				");
				$c->params[':name'] = '%'.$this->name.'%';
				
				$cCount->addCondition( "
					`t`.`user_login` LIKE :name OR `t`.`user_nicename` LIKE :name OR `t`.`user_email` LIKE :name OR `t`.`display_name` LIKE :name  OR `t`.`firstName` LIKE :name  OR `t`.`lastName` LIKE :name	
				");
				$cCount->params[':name'] = '%'.$this->name.'%';
				
			}
			
			$DP = new UsersListDP( self::modelName, Array(
				'criteria' => $c,
				'criteriaCount' => $cCount,
				'pagination' => $this->getDPPagination(),
			));
			
			$DP->pagination->pageSize = 50;
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'list';
			}
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function detName() {
			$this->name = @$_GET[ 'name' ];
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->detName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'sortField' => $this->getSortField(),
				'sortType' => $this->getSortType(),
				'name' => $this->name,
			));
		}
	}

?>