<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wAdminUserBalancesListWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wAdminUserBalancesListWidget">
	<?
		$this->widget( 'widgets.gridViews.AdminUserBalancesGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => $showOnEmpty,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'selectionChanged' => $ajax && $mode == 'Admin' ? "{$ns}.wAdminUserBalancesListWidget.selectionChanged" : null,
			'filterURL' => $filterURL,
			'filter' => $filterModel,
			'selectableRows' => $mode == 'Admin' ? 1 : 0,
			'mode' => $mode,
		));
	?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="2">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		
		ns.wAdminUserBalancesListWidget = wAdminUserBalancesListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminUserBalancesListWidget',
			ajax: ajax,
		});
	}( window.jQuery );
</script>