<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
?>

<div class="informer-item-container <?=$ins?>">
	<?php
		if( $title ){
			echo CHtml::openTag('div', array( 'class' => 'informer-header' ));
				echo CHtml::tag('h3', array(), $title);
				echo CHtml::tag('i', array( 'class' => 'informer-logo-img' ), '');
			echo CHtml::closeTag('div');
		}
		
		foreach( $cats as $cat ){
			echo CHtml::openTag('div', array( 'class' => 'informer-item' ));
				echo CHtml::openTag('a', array( 'href' => $cat->singleURL ));
					if( $cat->imgSrc ) echo CHtml::image( $cat->imgSrc, $cat->title, array('width' => 210));
					echo CHtml::tag('h4', array(), $cat->title);
				echo CHtml::closeTag('a');
				echo CHtml::tag('p', array(), $cat->description );
			echo CHtml::closeTag('div');
		}
	?>
</div>