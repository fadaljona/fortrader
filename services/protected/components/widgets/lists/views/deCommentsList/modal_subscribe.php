<div id="deco_modal_overlay" class="deco_modal_overlayBG" onclick="decom.closeModal(); return false;"></div>
<div id="deco_modal_window">
    <div id="deco_modal_title">
        <div id="deco_modal_ajaxWindowTitle"><?=$decomSettings['texts']['Subscribe']?></div>
        <div id="deco_modal_closeAjaxWindow">
            <a href="#" id="deco_modal_closeWindowButton">
                <div class="deco_modal-close-icon" onclick="decom.closeModal(); return false;"><img class="svg" src="/wp-content/plugins/decomments/templates/decomments/assets/images/svg/close_modal.svg"/></div>
            </a>
        </div>
    </div>
    <div id="deco_modal_ajaxContent">
        <div class="decomments-popup-style modal-sub-content">
            <form class="modal-sub-form">
                <a class="decomments-checkbox" href="javascript:void(0)" id="subscribe_my_comment">Replies to my
                    comments</a>
                <a class="decomments-checkbox" href="javascript:void(0)" id="subscribe_my_comment">All comments</a>
                <a class="decomments-checkbox" href="javascript:void(0)" id="subscribe_my_comment">Replies to my
                    comments</a>
                <a class="decomments-checkbox" href="javascript:void(0)" id="subscribe_my_comment">Replies to my
                    comments</a>

                <button class="decomments-button"><?=$decomSettings['texts']['Submit']?></button>
                <button class="decomments-button" onclick="decom.closeModal(); return false;"><?=$decomSettings['texts']['Cancel']?></button>

            </form>
        </div>


    </div>
</div>