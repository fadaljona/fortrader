<?php 
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/decom.js' ), CClientScript::POS_END );
	
	Yii::app()->clientScript->registerCss('servicesDeComments', '
		a.decomments-button.decomments-button-reply{
			display:none;
		}
	');
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="wQuotesInformerSettingsWidget <?=$ins?>">
	
<div class="decomments-comment-section decomments-block-form-wrapper"
		 data-modal-alert='<?php include 'modal_alerts.php'; ?>'
		 data-modal-addimage='<?php include 'modal_addimage.php'; ?>'
		 data-modal-quote='<?php include 'modal_quote.php'; ?>'
		 data-modal-preview='<?php include 'modal_preview.php'; ?>'
		 data-modal-sub='<?php include 'modal_subscribe.php'; ?>'
		>
	<script type="text/javascript">
		var decom_user_id = "<?php echo Yii::App()->user->id; ?>";
	</script>	
</div>
<?php

$userModel = UserModel::model()->findByPk(Yii::app()->user->id);

if ($userModel && $userModel->userHaveWpRoles(['editor', 'administrator'])) {
    $sufix = 'admin';
} else {
    $sufix = 'other';
}

if( !Yii::app()->cache->get($commmentsListCacheKey) ){
	ob_start();
	CommonLib::loadWp();
	global $wp_query;
	
	$settings = $decom_settings = decom_get_options();
	echo '<div class="decom_dop_bloc">';
	if ( isset( $decom_settings['show_avatars'] ) && intval( $decom_settings['show_avatars'] ) ) {
		$da_position = $decom_settings['display_avatars_right'] ? ' decomments-avatar-right' : '';
	} else {
		$da_position = ' no-avatar';
	}
	$current_user_id = DECOM_Loader_MVC::getComponentModel( 'comments', 'user' )->getCurrentUserId();
	$model_comments = DECOM_Loader_MVC::getComponentModel( 'comments', 'comments' );
	$model_votes    = DECOM_Loader_MVC::getComponentModel( 'comments', 'comments-votes' );

	$display_round_avatars = isset( $decom_settings['display_round_avatars'] ) ? intval( $decom_settings['decom_display_round_avatars'] ) : 0;
	$comments_per_page     = - 1;
	
	$comments_per_page     = ( isset( $decom_settings['page_comments'] ) && intval( $decom_settings['page_comments'] ) && isset( $decom_settings['comments_per_page'] ) ) ? intval( $decom_settings['comments_per_page'] ) : 20;
	

	$user_sort = 'newer';

	$comments = $model_comments->getCommentsForList( $postIds, $limit );

	if ( $comments ) {
		$pages_count = ( count( $comments ) - (count( $comments ) % $comments_per_page) ) / $comments_per_page + 1;
	} else {
		$pages_count = 0;
	}
	?>
	
	<div id="decomments-comment-section" class="decomments-comment-section">
		
	<?php
		$number        = count( $comments );
		$have_comments = $number > 0 ? true : false;
		if ( $have_comments ) { ?>
			<div class="decomments-head">
			<?php 
			$display = ( $number == 0 ) ? ' decomments-hide' : ' decomments-block';
			if(!$limit){ 
				$comment_str = '<h3><i><span class="decomments-comments-number' . $display . '">' . $number . '</span><span class="decomments-comment-title">' . ' ' . _n( 'comment', 'comments', $number, DECOM_LANG_DOMAIN ) . '</span></i></h3>';
				echo $comment_str;
			 }else{
				echo $comment_str = '<h3><i><span class="decomments-comment-title">' . $label . '</span></i></h3>';
			}?>
			</div>
			<div class="decomments-comment-list<?php echo $da_position; ?>">
				<div class="loader-ball-scale lbs-remove">
					<div></div>
					<div></div>
					<div></div>
				</div>
				<?php	
				$settings['sort_comments'] = 'newer';
				wp_list_comments( array(
					'callback'          => 'decom_render_comment',
					'end-callback'      => array($this, 'customDecomEndComment'),
					'style'             => 'div',
					'walker'            => DECOM_Loader_MVC::getComponentClass( 'comments', 'comments-walker' ),
					'settings'          => $settings,
					'votes'             => $votes,
					'user_voice'        => $user_voice,
					'reverse_top_level' => $user_sort,
					'per_page'          => '',
					'page'              => $current_comments_page ? $current_comments_page : '1'
				), $comments );
				?>
			</div><!-- .commentlist -->
			<?php
		} // have_comments()
	?>
	</div>
	</div>
	<?php
	$commmentsList = ob_get_clean();
	Yii::app()->cache->set($commmentsListCacheKey, $commmentsList, 60*60*24);
}
echo Yii::app()->cache->get($commmentsListCacheKey);
	
?>
	
</div>