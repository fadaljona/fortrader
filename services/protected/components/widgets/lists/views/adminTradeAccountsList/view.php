<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wAdminTradeAccountsListWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wAdminTradeAccountsListWidget">
	<?
		$gridview = $this->widget( 'widgets.gridViews.AdminTradeAccountsGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => $showOnEmpty,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'selectionChanged' => $ajax ? "{$ns}.wAdminTradeAccountsListWidget.selectionChanged" : null,
			'filterURL' => $filterURL,
			'filter' => $filterModel,
		));
	?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="<?=count($gridview->columns)?>">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var inSearch = <?=CommonLib::boolToStr($inSearch)?>;
		
		ns.wAdminTradeAccountsListWidget = wAdminTradeAccountsListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminTradeAccountsListWidget',
			ajax: ajax,
			hidden: hidden,
			inSearch: inSearch,
		});
	}( window.jQuery );
</script>