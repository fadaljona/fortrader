<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	Yii::app()->clientScript->registerScript('statsChangGroupBy', "
!function( $ ) {	
	$('#statsGroupBy').on('change', function() { $('#statsGroupBy').closest('form').submit(); });
	$('#informerCat').on('change', function() { $('#informerCat').closest('form').submit(); });
}( window.jQuery );
	", CClientScript::POS_END);
?>
<div class="accordion" id="accordion2">
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"><?=Yii::t( $NSi18n, 'Select Date' )?></a>
		</div>
		<div id="collapseOne" class="accordion-body collapse" style="height: 0px;">
			<div class="accordion-inner">
				
				<div class="well pull-left iFormWidget wAdminQuotesInformerStatsListWidget">
					<h3><?=Yii::t( $NSi18n, 'Select Date' )?></h3>
					<?php 
						$form=$this->beginWidget('CActiveForm', array(
							'id' => 'page-form',
							'htmlOptions'=>array(
								'class' => 'form-vertical',
							),
						)); 
					?>
					 
					<label class="required" for="startDate"><?=Yii::t( $NSi18n, 'Select start date' )?></label>
					<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'name'=>'startDate',
					    'value'=> $startDate,
						'language' => substr(Yii::app()->language, 0, 2),
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>'yy-mm-dd',
							'selectOtherMonths' => true,
							'changeMonth' => true,
							'changeYear' => true,
							'showButtonPanel' => true,
						),
						'htmlOptions'=>array(
							'class' => 'startDate',
						),
					));
					?>
					<label class="required" for="endDate"><?=Yii::t( $NSi18n, 'Select end date' )?></label>
					<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'name' => 'endDate',
						'value' => $endDate,
						'language' => substr(Yii::app()->language, 0, 2),
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>'yy-mm-dd',
							'selectOtherMonths' => true,
							'changeMonth' => true,
							'changeYear' => true,
							'showButtonPanel' => true,
						),
						'htmlOptions'=>array(
							'class' => 'endDate',
						),
					));
					?>
					<div class="clear"></div>
					<?php echo CHtml::submitButton(Yii::t( $NSi18n,'Select'), array('class' => 'btn btn-success')); ?>
					<?php $this->endWidget(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="iClear"></div>

<?php $this->beginWidget('CActiveForm', array( 'id' => 'statsGroupBy-form', 'htmlOptions'=>array( 'class' => 'form-vertical',),)); ?>

<div class="row-fluid">
	<div class="span6">
		<h5><?=Yii::t( $NSi18n, 'Group by' )?></h5>
		<?=CHtml::dropDownList( 'statsGroupBy', $groupBy, array('domain' => Yii::t( $NSi18n, 'Domain' ), 'domain, cat, style' => Yii::t( $NSi18n, 'Domain, Category, Style' ) ) )?>
	</div>
	<div class="span6">
		<h5><?=Yii::t( $NSi18n, 'Select category' )?></h5>
		<?=CHtml::dropDownList( 'informerCat', $informerCat, $informerCats )?>
	</div>
</div>
<?php $this->endWidget(); ?>

<div class="wAdminAdvertisementZonesDistributionListWidget">
	<?
		$listWidget = $this->widget( 'widgets.gridViews.AdminInformerStatsGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'ajax' => false,
			'dataProvider' => $DP,
			'selectionChanged' => $ajax ? "{$ns}.AdminInformerStatsGridViewWidget.selectionChanged" : null,
			'enableSorting' => true,
		));
	?>

</div>