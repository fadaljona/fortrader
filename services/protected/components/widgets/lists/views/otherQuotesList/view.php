<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="<?=$ins?> wQuotesChartListWidget">
	<?php
		$i=0;
		foreach( $quotes as $row ){
			if( $i ) echo ", ";
			echo CHtml::link( CHtml::encode( $row['name'] ), Yii::app()->createUrl( 'quotesNew/single', array( 'slug' => strtolower( $row['name'] ) )) );
			$i++;
		}
	?>
</div>