<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();

	$cs=Yii::app()->getClientScript();
	
	
	if( !Yii::App()->request->isAjaxRequest ){
		
		$cs->registerCoreScript( 'jquery' );

		$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
		$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
		$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
		
		$cs->registerCssFile( Yii::app()->params['wpThemeUrl'] .'/informerCalendar.css');
		
		new JsTrans('app',Yii::app()->language);
		
		$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wTableSliderWidget.js' ) );
		$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wCalendarInformerEventsListWidget.js' ), CClientScript::POS_END );
	}
	
	
	$jsls = Array(

	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);

?>
<section class="informer-calendar__section-calendar <?=$ins?> position-relative spinner-margin">
	<div class="informer-calendar__header clearfix">
		<div class="calendar-header__name">
			<?=Yii::t($NSi18n, 'Calendar of events')?>
			
			<?php
				$tooltipTitle = Yii::t($NSi18n, 'calendarEventToolTipTitle');
				$tooltipContent = Yii::t($NSi18n, 'calendarEventToolTipContent');
				if( $tooltipTitle == 'calendarEventToolTipTitle' ) $tooltipTitle = false;
				if( $tooltipContent == 'calendarEventToolTipContent' ) $tooltipContent = false;
				if( $tooltipTitle || $tooltipContent ){
			?>
			<div class="calendar-header__icon">    
				<div class="calendar-header__info">
					<?php if($tooltipTitle){ ?><div class="calendar-header__info-title"><?=$tooltipTitle?></div><?php }?>
					<?php if($tooltipContent){ ?><div class="calendar-header__info-text"><?=$tooltipContent?></div><?php }?>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php 
			$zonesList = CommonLib::getTimeZonesWithOffsets();
			$zonesListForSelect = array();
			foreach( $zonesList as $zoneOffset => $zoneName ){
				$zoneSign = '';
				if( $zoneOffset > 0 ) $zoneSign = '+';
				if( strpos($zoneOffset, '.') !== false ){
					$minutePos = strpos($zoneOffset, '.');
					$hours = substr($zoneOffset, 0, $minutePos);
					$mins = 60 * abs($zoneOffset+$hours*-1);
					$zoneUtcVal = "(UTC " . $zoneSign . $hours . ":".$mins.")";
				}else{
					$zoneUtcVal = "(UTC " . $zoneSign . $zoneOffset . ":00)";
				}
				$slashPos = strpos( $zoneName, '/' );
				if( $slashPos ){
					$zoneNameStr = Yii::t($NSi18n, substr($zoneName, 0, $slashPos) ) . ' / ' . Yii::t($NSi18n, substr($zoneName, $slashPos+1) );
				}else{
					$zoneNameStr = $zoneName;
				}
				
				$zonesListForSelect[$zoneOffset] = $zoneUtcVal . ' ' . $zoneNameStr;
			}
			echo CHtml::dropDownList('user_utc',UserProfileModel::getTimezone('cookie'),$zonesListForSelect, array( 'class'=>'calendar-header__select timezone_select', )); 
		?>						
	</div>

	<div class="turn_content">
		<div class="tabl_quotes informer-calendar__tb">
			
			<?php
				$gridWidget = $this->widget( 'widgets.gridViews.CalendarInformerEventsGridViewWidget', Array(
					'NSi18n' => $NSi18n,
					'ins' => $ins,
					'showTableOnEmpty' => true,
					'dataProvider' => $DP,
					'ajax' => true,
					'template' => '{items}',
					'pagerCssClass' => 'informer-calendar__page clearfix',
					'pager' => array(
						'class'=> 'widgets.gridViews.base.CalendarPager'
					),
				));
			?>
		</div>
	</div>
	
	<?php if( $DP->pagination->getPageCount() > 1 ){ echo $gridWidget->renderPager();} ?>

</section>

<?if( !Yii::App()->request->isAjaxRequest ){

Yii::app()->clientScript->registerScript('informersCalendarJs', '

	!function( $ ) {
		var ns = ' . $ns . ';
		var nsText = ".'. $ns .'";
		var ins = ".'. $ins .'";
		
		var ls = '. json_encode( $jsls ) .';
		
		ns.wCalendarInformerEventsListWidget = wCalendarInformerEventsListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: ".'. $ins .'",
			ls: ls,
			ajaxLoadListURL: "'. Yii::app()->createUrl('calendarEvent/ajaxLoadInformerEventsList') .'",
			ajaxLoadRowsURL: "'. Yii::app()->createUrl('calendarEvent/ajaxLoadInformerRows') .'",
			impact: "'. $impact .'",
			symbols: "'. $symbols .'",
			update: "'. $update .'",
		
		});
	}( window.jQuery );

', CClientScript::POS_END);
 }?>