<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();

	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wForeignContestsListWidget.js' ) );
	}
?>
<div class="position-relative spinner-margin <?=$ins?>">
	<!-- - - - - - - - - - - - - - Tabl page  2- - - - - - - - - - - - - - - - -->
	<section class="section_offset_2 turn_box">
		<div class="competition_block_head clearfix">
			<h4><?=Yii::t( $NSi18n, 'Table contests friendly resources' )?><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></h4>
		</div>
		<div class="turn_content">
			<div class="tabl_quotes competition_table table2">
				<?php
					$gridWidget = $this->widget( 'widgets.gridViews.ForeignContestsGridViewWidget', Array(
						'NSi18n' => $NSi18n,
						'ins' => $ins,
						'showTableOnEmpty' => $showOnEmpty,
						'dataProvider' => $DP,
						'ajax' => $ajax,
						'template' => '{items}',
						'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
						'pager' => array(
							'class'=> 'widgets.gridViews.base.WpPager'
						),
					));
				?>
			</div>
		</div>
	</section>
	<!-- - - - - - - - - - - - - - End of Tabl page 2- - - - - - - - - - - - - - - - -->
	<div class="section_offset">
		<div class="clearfix">
			<?php
				if( $DP->pagination->getPageCount() > 1 ){
					echo CHtml::link( Yii::t( $NSi18n, 'Show more' ), '#', array( 'class' => 'download_more', 'data-page' => 1, ) );
				}
			?>
			<button class="add_btn"><?=Yii::t( $NSi18n, 'Add' )?></button>
		</div>
	</div>

</div>


<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var hidden = <?=CommonLib::boolToStr($hidden)?>;

			ns.wForeignContestsListWidget = wForeignContestsListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .<?=$ins?>',
				ajax: ajax,
				hidden: hidden,
				pagesCount: <?=$DP->pagination ? $DP->pagination->getPageCount() : 0?>,
				ajaxLoadListURL: '<?=Yii::app()->createAbsoluteUrl('contest/ajaxLoadForeignList')?>',
			});
		}( window.jQuery );
	</script>
<?}?>