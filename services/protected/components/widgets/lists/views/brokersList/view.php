<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::app()->clientScript->registerCss('BrokersListWidgetCss', '
			.search_number .fa-angle-down:before {
				content: "' . Yii::t( $NSi18n, 'DisplayRBL' ) . ':";
			}
		');
		
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wBrokersListWidget.js' ), CClientScript::POS_END );
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wBrokerLinksStats.js' ) );

		Yii::app()->clientScript->registerScript('BrokerLinksStats', "
!function( $ ) {	
	wBrokerLinksStats({
		statsUrl: '" . Yii::app()->createAbsoluteUrl('broker/ajaxSaveLinksStats') . "',	
	});
}( window.jQuery );
	", CClientScript::POS_END);
	}
	
	$jsls = Array(
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="iListWidget i01 wBrokersListWidget iRelative">
	
	<!-- - - - - - - - - - - - - - Tabs - - - - - - - - - - - - - - - - -->
	<div class="tabs_box tabs_rating relative">
		<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
		<?php /*<ul class="tabs_list clearfix PFDregular">
			<li class="active tabs_list_1"><a href="javascript:;"><?=Yii::t( $NSi18n, 'Broker ratings' )?></a></li>
			<li class="tabs_list_2"><a href="javascript:;">Рейтинг брокеров бинарных опционов</a></li>
		</ul>*/?>
		<!-- - - - - - - - - - - - - - End of Tabs List  - - - - - - - - - - - - - - - - -->
		<!-- - - - - - - - - - - - - - Tabs Contant - - - - - - - - - - - - - - - - -->
		<div class="tabs_contant">
			<!-- - - - - - - - - - - - - - Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<div class="active">
				<!-- - - - - - - - - - - - - - Tabl page  - - - - - - - - - - - - - - - - -->
				<div class="section_offset_2 turn_box paddingBottom0">
					<div class="turn_content">
						<div class="tabl_quotes table_rating" <?php /*itemscope itemtype="http://schema.org/ItemList"*/?>>
							<?
								$gridWidget = $this->widget( 'widgets.gridViews.BrokersGridViewWidget', Array(
									'NSi18n' => $NSi18n,
									'ins' => $ins,
									'showTableOnEmpty' => $showOnEmpty,
									'dataProvider' => $DP,
									'ajax' => $ajax,
									'template' => '{items}',
									'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
									'sortField' => $sortField,
									'sortType' => $sortType,
									'pager' => array(
										'class'=> 'widgets.gridViews.base.WpPager'
									),
								));
							?>
						</div>
					</div>
					<?if( $DP->pagination->getPageCount() > 1 ){?>
						<?=$gridWidget->renderPager()?>
					<?}?>
				</div>
				<!-- - - - - - - - - - - - - - End of Tabl page - - - - - - - - - - - - - - - - -->
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<!-- - - - - - - - - - - - - - Tabs Item 2 - - - - - - - - - - - - - - - - -->
			<div>
				<div class="add_mood_box">
					<p>2</p>
				</div>
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs Item 2 - - - - - - - - - - - - - - - - -->
		</div><!-- / .tabs_contant -->
		<!-- - - - - - - - - - - - - - End of Tabs Contant - - - - - - - - - - - - - - - - -->
		
		<div class="spinner-wrap hide">
			<div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div>
			</div>
		</div>
			
	</div>
	<!-- - - - - - - - - - - - - - End of Tabs - - - - - - - - - - - - - - - - -->
	
</div>
<?if( !Yii::App()->request->isAjaxRequest ){
	
Yii::app()->clientScript->registerScript('brokersListWidgetJs', '

	!function( $ ) {
		var ns = ' . $ns . ';
		var nsText = ".' . $ns . '";
		var ins = ".' . $ins . '";
		var ajax = ' . CommonLib::boolToStr($ajax) . ';
		var hidden = ' . CommonLib::boolToStr($hidden) . ';
		var sortField = "' . $sortField . '";
		var sortType = "' . $sortType . '";
			
		var ls = ' . json_encode( $jsls ) . ';
			
		ns.wBrokersListWidget = wBrokersListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+" .wBrokersListWidget",
			ajax: ajax,
			nsText: nsText,
			hidden: hidden,
			ls: ls,
			sortField: sortField,
			sortType: sortType,
			ajaxLoadListURL: "' . Yii::app()->createAbsoluteUrl("broker/ajaxLoadList") . '",
			type: "' . $type . '",
			cookieRowsCount: "' . $cookieRowsCount . '",
		});
	}( window.jQuery );

', CClientScript::POS_END);
	
}?>