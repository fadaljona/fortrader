<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();

?>

<div class="wAdminAdvertisementZonesDistributionListWidget">
	<?
		$listWidget = $this->widget( 'widgets.gridViews.AdminPostsHitsGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'ajax' => false,
			'dataProvider' => $DP,
			'selectionChanged' => $ajax ? "{$ns}.wAdminPostsStatsListWidget.selectionChanged" : null,
			'filterURL' => $filterURL,
			'filter' => $filterModel,
			'enableSorting' => true,
		));
	?>
</div>