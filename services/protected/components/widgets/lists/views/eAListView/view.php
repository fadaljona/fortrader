<div class="EA <?=$this->ins?> <?=$this->w?>">
  <div class="row-fluid">
    <div class="EAname pull-left"><?=$this->formatName($data)?></div>
    <div class="EArating pull-right">
      <?$this->widget('CStarRating', array(
        'name'=> $this->w . "_average[{$data->id}]",
        'maxRating' => 5,
        'value' => $this->formatRating($data->stats->average),
        'readOnly' => true,
      ))?>
    </div>
    <div class="EAviews pull-right">
      <?=$this->formatViews($data->views)?>
    </div>
  </div>
  
  <div class="row-fluid">
    <div class="EAdesc pull-left" style="display: block;"><?=empty($data->desc) ? '&nbsp;' : $data->desc?></div>
  </div>
</div>
<!--<div class="iClear"></div>-->