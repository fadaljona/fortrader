<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="accordion" id="accordion2">
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"><?=Yii::t( $NSi18n, 'Select Date' )?></a>
		</div>
		<div id="collapseOne" class="accordion-body collapse" style="height: 0px;">
			<div class="accordion-inner">
				
				<div class="well pull-left iFormWidget wAdminPostDateFormWidget">
					<h3><?=Yii::t( $NSi18n, 'Select Date' )?></h3>
					<?php 
						$form=$this->beginWidget('CActiveForm', array(
							'id' => 'page-form',
							'htmlOptions'=>array(
								'class' => 'form-vertical',
							),
						)); 
					?>
					 
					<label class="required" for="startPostDate"><?=Yii::t( $NSi18n, 'Select start post date' )?></label>
					<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'name'=>'PostsStatsCommonModel[startPostDate]',
					    'value'=> $startPostDate,
						'language' => substr(Yii::app()->language, 0, 2),
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>'yy-mm-dd',
							'selectOtherMonths' => true,
							'changeMonth' => true,
							'changeYear' => true,
							'showButtonPanel' => true,
						),
						'htmlOptions'=>array(
							'class' => 'startPostDate',
						),
					));
					?>
					<label class="required" for="endPostDate"><?=Yii::t( $NSi18n, 'Select end post date' )?></label>
					<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'name' => 'PostsStatsCommonModel[endPostDate]',
						'value' => $endPostDate,
						'language' => substr(Yii::app()->language, 0, 2),
						'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>'yy-mm-dd',
							'selectOtherMonths' => true,
							'changeMonth' => true,
							'changeYear' => true,
							'showButtonPanel' => true,
						),
						'htmlOptions'=>array(
							'class' => 'endPostDate',
						),
					));
					?>
					<div class="clear"></div>
					<?php echo CHtml::submitButton(Yii::t( $NSi18n,'Select'), array('class' => 'btn btn-success')); ?>
					<?php $this->endWidget(); ?>
				</div>
				
			</div>
		</div>
	</div>
</div>
<div class="iClear"></div>

<div class="wAdminAdvertisementZonesDistributionListWidget">
	<?
		$listWidget = $this->widget( 'widgets.gridViews.AdminPostsStatsGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'ajax' => false,
			'dataProvider' => $DP,
			'selectionChanged' => $ajax ? "{$ns}.wAdminPostsStatsListWidget.selectionChanged" : null,
			'filterURL' => $filterURL,
			'filter' => $filterModel,
			'enableSorting' => true,
		));
	?>
	<div class="well">
		C - <?=Yii::t( $NSi18n, 'comment count' )?><br />
		V - <?=Yii::t( $NSi18n, 'post views' )?><br />
		I - <?=Yii::t( $NSi18n, 'interest' )?><br />
		FBL - <?=Yii::t( $NSi18n, 'fb likes' )?><br />
		FBS - <?=Yii::t( $NSi18n, 'fb share' )?><br />
		VK - <?=Yii::t( $NSi18n, 'vk share' )?><br />
		TW - <?=Yii::t( $NSi18n, 'twitter' )?><br />
		GP - <?=Yii::t( $NSi18n, 'google plus' )?><br />
		Date - <?=Yii::t( $NSi18n, 'wp post date' )?>
	</div>
</div>