<? /** @var EARatingColumnWidget $this */?>
<div class="column-row row-fluid">
  <div class="row-fluid">
      <?=$this->formatName($data)?>
    <div class="EArating pull-right">
      <?$this->widget('CStarRating', array(
        'name'=> $this->w . "_average[{$data->id}]",
        'maxRating' => 5,
        'value' => $this->formatRating($data->stats->average),
        'readOnly' => true,
      ))?>
    </div>
  </div>
</div>
