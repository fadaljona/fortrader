<?php
Yii::App()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('webroot.js').'/autobahn.min.js'));
Yii::App()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('webroot.js.widgets.lists').'/wCryptoCurrenciesPageListWidget.js'));

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
$quotesSettings = QuotesSettingsModel::getInstance();
?>

<div class="crypto__currencies wide_scroll js_scrolling <?=$ins?>">
    <?php
        $gridview = $this->widget('widgets.gridViews.CryptoCurrenciesPageGridViewWidget', array(
            'NSi18n' => $NSi18n,
            'ins' => $ins,
            'showTableOnEmpty' => $showOnEmpty,
            'dataProvider' => $DP,
            'ajax' => $ajax,
            'template' => '{items}',
            'selectionChanged'=>'js:function() {
                var tr = $(".'. $ins .'").find("table tr.selected");
                if (tr.length) {
                    cryptoCurrenciesChartHighcharts.reDrawChart(tr.attr("data-idobj"));
                    cryptoCurrenciesChartHighcharts.scrollToChart();
                    cryptoCurrenciesPageListWidget.loadCryptoExchangeList(tr.attr("data-idobj"));
                }
            }'
        ));

        $quotesKey = '';
        $i=0;
        $hasBitcoin = false;
        foreach ($DP->data as $item) {
            if ($i==0) {
                $quotesKey = $item->name;
                $i=1;
            } else {
                $quotesKey  .= ',' . $item->name;
            }
            if ($item->name == $bitcoinKey) {
                $hasBitcoin = true;
            }
        }
        if (!$hasBitcoin && $bitcoinKey) {
            $quotesKey  .= ',' . $bitcoinKey;
        }
    ?>
</div>
<script>

var cryptoCurrenciesPageListWidget;
!function( $ ) {
    var ns = ".<?=$ns?>";
    var connUrl = '<?=$quotesSettings->wsServerUrl?>';

    cryptoCurrenciesPageListWidget = ns.wCryptoCurrenciesPageListWidget = wCryptoCurrenciesPageListWidgetOpen({
        ns: ns,
        connUrl: connUrl,
        quotesKey: "<?=$quotesKey?>",
        selector: ".<?=$ins?>",
        bitcoinKey: '<?= $bitcoinKey ?>',
        loadCryptoExchangeUrl: '<?= Yii::app()->createUrl('cryptoCurrencies/ajaxLoadCryptoExchange')?>'
    });
}( window.jQuery );

!function( $ ) {
    var ns = ".<?=$ns?>";
    var firstTr = $('.<?=$ins?>').find('table tr').first().addClass('selected');
    cryptoCurrenciesChartHighcharts.reDrawChart(firstTr.attr('data-idobj'));
    //cryptoCurrenciesPageListWidget.loadCryptoExchangeList(firstTr.attr('data-idobj'));
    $('.<?=$ins?>').find('table tr a').click(function(e){
        e.stopPropagation();
    });
}( window.jQuery );
</script>