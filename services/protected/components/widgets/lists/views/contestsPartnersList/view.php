<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wContestsListWidget.js" );
		$jsls = Array(
			'lDeleteConfirm' => 'Delete?',
		);
		foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	}
?>
<section class="section_offset_2 turn_box iListWidget i01 wContestsPartnersListWidget iRelative spinner-margin position-relative">

	<div class="turn_content">
		<div class="competition_table table2">
		<?php
			$gridWidget = $this->widget( 'widgets.gridViews.ContestsPartnersGridViewWidget', Array(
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'showTableOnEmpty' => $showOnEmpty,
				'dataProvider' => $DP,
				'ajax' => $ajax,
				'template' => '{items}',
				'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
				'pager' => array(
					'class'=> 'widgets.gridViews.base.Wp2Pager'
				),
				'label' => $label
			));
		?>
		</div>
	</div>
	<?php if ( $DP->pagination->getPageCount() > 1 ):?>
		<?=$gridWidget->renderPager();?>
	<?php endif?>
</section>

<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var hidden = <?=CommonLib::boolToStr($hidden)?>;
			
			var ls = <?=json_encode( $jsls )?>;

			ns.wContestsListWidget = wContestsListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wContestsPartnersListWidget',
				ajax: ajax,
				hidden: hidden,
				ls: ls,
				ajaxLoadListURL: '<?=Yii::app()->createAbsoluteUrl('contest/ajaxLoadContestsPartnersList')?>',
			});
		}( window.jQuery );
	</script>
<?}?>