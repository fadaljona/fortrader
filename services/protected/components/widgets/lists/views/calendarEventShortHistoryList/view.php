<?php
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wCalendarEventShortHistoryListWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	

?>

<div class="<?=$ins?>">

	<h5 class="description_history_title"><?=Yii::t( '*', 'History' )?></h5>
	<div class="description_history_box">
		<?php $gridWidget->run(); ?>	
	</div>
	<?php if( $DP->totalItemCount > $pageSize ){ ?><a href="javascript:;" class="description_history_btn"><?=Yii::t( '*', 'Show more' )?></a><?php } ?>
	
	
</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
				
		ns.wCalendarEventShortHistoryListWidget = wCalendarEventShortHistoryListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: '.<?=$ins?>',
			loadUrl: '<?=Yii::app()->createUrl('calendarEvent/ajaxLoadShortHistory')?>',
			pageSize: <?=$pageSize?>,
		});
	}( window.jQuery );
</script>