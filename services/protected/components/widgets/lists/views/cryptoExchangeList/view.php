<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>

<div class="crypto__exchange <?=$ins?>">
    <div class="crypto-exch__title"><?= Yii::t($NSi18n, 'Currency Exchange')?></div>
    <?php
        $gridview = $this->widget('widgets.gridViews.CryptoCurrenciesExchangesGridViewWidget', array(
            'NSi18n' => $NSi18n,
            'ins' => $ins,
            'showTableOnEmpty' => $showOnEmpty,
            'dataProvider' => $DP,
            'ajax' => $ajax,
            'template' => '{items}',
        ));
    ?>
</div>