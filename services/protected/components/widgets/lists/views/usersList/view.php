<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wUsersListWidget.js" );
	}
	
	$traders = @$_GET[ 'traders' ] == 'on';
	$managers = @$_GET[ 'managers' ] == 'on';
	$investors = @$_GET[ 'investors' ] == 'on';
	$programmers = @$_GET[ 'programmers' ] == 'on';
	
	$user = Yii::App()->user->getModel();
	$isProfile = $user && $user->profile;
	$isTrader = $isProfile && $user->profile->isTrader;
	
	$dontShowTraderProfileAlert = @$_COOKIE[ 'dontShowTraderProfileAlert' ] == 'on';
	
	$sorts = Array( 'name', 'reputation', 'activity', 'awards' );
	$sorts = array_combine( $sorts, $sorts );
	foreach( $sorts as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="iListWidget i01 wUsersListWidget iRelative">
	<?if( $user and !$isTrader and $traders and !$dontShowTraderProfileAlert ){?>
		<div class="alert alert-block alert-success <?=$ins?> wTraderAlert">
			<p>
				<?$params['tabProfileTrader'] = 'yes';?>
				<?$url = $this->controller->createURL( '/user/settings', $params )?>
				<?=Yii::t( '*', "You didn't fill the {trader profile} for display in this list.", Array( '{trader profile}' => CHtml::link( Yii::t( 'lower', 'trader profile' ), $url )))?>
			</p>
			<p>
				<button class="btn btn-small btn-success wRedirect"><?=Yii::t( $NSi18n, 'Do it' )?></button>
				<button class="btn btn-small wCancel"><?=Yii::t( $NSi18n, 'Cancel' )?></button>
			</p>
		</div>
	<?}?>
	<form style="margin:0;">
		<label class="iLabel i01">
			<?=CHtml::checkBox( 'traders', $traders, Array( 'value' => 'on' ))?>
			<span class="lbl">
				<?=Yii::t( '*', 'Trader' )?>
			</span>
		</label>
		
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<label class="iLabel i01">
			<?=CHtml::checkBox( 'managers', $managers, Array( 'value' => 'on' ) )?>
			<span class="lbl">
				<?=Yii::t( '*', 'Manager' )?>
			</span>
		</label>
		
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<label class="iLabel i01">
			<?=CHtml::checkBox( 'investors', $investors, Array( 'value' => 'on' ) )?>
			<span class="lbl">
				<?=Yii::t( '*', 'Investor' )?>
			</span>
		</label>
		
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<label class="iLabel i01">
			<?=CHtml::checkBox( 'programmers', $programmers, Array( 'value' => 'on' ) )?>
			<span class="lbl">
				<?=Yii::t( '*', 'Programmer' )?>
			</span>
		</label>
		
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<div style="display:inline-block">
			<?=Yii::t( '*', 'Sort by' )?>:&nbsp;
			<?=CHtml::dropDownList( 'sortField', $sortField, $sorts, Array( 'style' => 'margin:0' ))?>
		</div>
		
		<div style="padding-top:20px;">
			<?if( $name ){?>
				<div class="table-header">
					<?=Yii::t( $NSi18n, 'Results for' )?>
					"<?=htmlspecialchars( $name )?>"
				</div>
			<?}?>
			<div class="iDiv i01 i08 iRelative" style="padding-top:0;height:38px;">
				<div class="iDiv i02 pull-left" style="position:absolute;top:10px;left:10px;">
					<span class="input-icon">
						<?=CHtml::textField( 'name', $name, Array( 'class' => "iInput i02 {$ins} wName", 'placeholder' => Yii::t( $NSi18n, 'Search' ), 'maxlength' => 50 ))?>
						<i class="icon-search" id="nav-search-icon"></i>
					</span>
				</div>
				<div class="iClear"></div>
			</div>
		</div>
		
	</form>
	
	
	
	
	<?
		$gridWidget = $this->widget( 'widgets.gridViews.UsersGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => $showOnEmpty,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'pagerCssClass' => 'pagination pagination-left',
			'sortField' => $sortField,
			'sortType' => $sortType,
		));
	?>
	<div class="iDummReload i01 <?=$ins?> wDummReload" style="display:none;"></div>
	<table class="iDummReloadText i01 <?=$ins?> wDummReloadText" style="display:none;">
		<tr>
			<td valign="middle" align="center">
				<span class="iSpan i01">
					<?=Yii::t( $NSi18n, 'Loading...' )?>
				</span>
			</td>
		</tr>
	</table>
</div>
<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			
			ns.wUsersListWidget = wUsersListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wUsersListWidget',
				ajax: ajax,
			});
		}( window.jQuery );
	</script>
<?}?>