<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();


?>
<div class="deals <?=$ins?>">
	<!--deals table-->
	<?php
		$gridWidget = $this->widget( 'widgets.gridViews.ContestMemberLastDealsForForumGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => false,
			'dataProvider' => $DP,
			'ajax' => true,
			'template' => '{items}',
		));
	?>
<!--deals table-->
</div>