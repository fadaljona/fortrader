<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<!-- - - - - - - - - - - - - - Monitoring filter - - - - - - - - - - - - - - - - -->
<section class="section_offset <?=$ins?> paddingBottom0">
	<?php
		$this->widget('widgets.wizards.RegistrationContestMemberWizardWidget', Array(
			'ns'=>$ns,
			'contest'=>false,
		));
	?>
	<?php 
		$this->widget( 'widgets.lists.YourMonitoringAccountsListWidget', Array(
			'ns'=>$ns,
		));
	?>
	<?php 
		$this->widget( 'widgets.lists.MonitoringListWithFiltersWidget', Array(
			'ns'=>$ns,
		));
	?>
</section>
<!-- - - - - - - - - - - - - - End of Monitoring filter - - - - - - - - - - - - - - - - -->