<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wJournalsWebmasterListWidget.js' ) );
	}
	
	$jsls = Array(
		'lDeleteConfirm' => 'Delete?',
        'lBlockConfirm' => Yii::t("*", 'Block?'),
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="iListWidget i01 wJournalsWebmasterListWidget iRelative">

	<!-- Magazine page table -->
	<div class="section_offset turn_content positionRelative paddingBottom0">
		<div class="tabl_quotes magazine_page_table">
			<?
				$gridWidget = $this->widget( 'widgets.gridViews.JournalsWebmasterGridViewWidget', Array(
					'NSi18n' => $NSi18n,
					'ins' => $ins,
					'showTableOnEmpty' => $showOnEmpty,
					'dataProvider' => $DP,
					'ajax' => $ajax,
					'template' => '{items}',
					'journalId'=>$this->journalId,
					'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
					'pager' => array(
						'class'=> 'widgets.gridViews.base.WpPager'
					),
				));
			?>
		</div>

		<?if( $DP->pagination->getPageCount() > 1 ){?>
			<?=$gridWidget->renderPager()?>
		<?}?>
		
		<div class="spinner-wrap hide">
			<div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div>
			</div>
		</div>

	</div> <!-- end section offset  turn content-->
	<!--End of the Magazine page table -->


	
	
</div>
<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var hidden = <?=CommonLib::boolToStr($hidden)?>;
			//var sortField = "<?//=$sortField?>";
			//var sortType = "<?//=$sortType?>";
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wJournalsWebmasterListWidget = wJournalsWebmasterListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wJournalsWebmasterListWidget',
				ajax: ajax,
				hidden: hidden,
				ls: ls,
				ajaxLoadListURL: '<?=Yii::app()->createUrl('journal/ajaxLoadWebmasterList')?>',
				ajaxDeleteURL: '<?=Yii::app()->createUrl('admin/journal/ajaxDeleteWebmaster')?>',
				ajaxBlockURL: '<?=Yii::app()->createUrl('admin/journal/ajaxBlockWebmaster')?>',
			});
		}( window.jQuery );
	</script>
<?}?>