<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="iListWidget i01 <?=$ins?> iRelative">

	<!-- - - - - - - - - - - - - - Tabl page  - - - - - - - - - - - - - - - - -->
	<section class="section_offset_2 turn_box">
		<div class="turn_content">
			<div class="tabl_quotes table_currency table_currency_archive" data-table="2">
			<?
			if( $dataType == 'cbr' ){
				$gridview = $this->widget( 'widgets.gridViews.CurrencyRatesArchiveGridViewWidget', Array(
					'NSi18n' => $NSi18n,
					'ins' => $ins,
					'showTableOnEmpty' => $showOnEmpty,
					'dataProvider' => $DP,
					'ajax' => $ajax,
					'template' => '{items}',
					'year' => $year,
					'mo' => $mo,
					'day' => $day
				));
			}elseif( $dataType == 'ecb' ){
				$gridview = $this->widget( 'widgets.gridViews.CurrencyRatesEcbArchiveGridViewWidget', Array(
					'NSi18n' => $NSi18n,
					'ins' => $ins,
					'showTableOnEmpty' => $showOnEmpty,
					'dataProvider' => $DP,
					'ajax' => $ajax,
					'template' => '{items}',
					'year' => $year,
					'mo' => $mo,
					'day' => $day
				));
			}
				
			?>
			</div>
		</div>
	</section>
	<!-- - - - - - - - - - - - - - End of Tabl page - - - - - - - - - - - - - - - - -->

</div>