<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wContestMemberTradesListWidget.js' ) );
	}
?>

<div class="section_offset_2 turn_box iListWidget i01 wContestMemberTradesListWidget spinner-margin position-relative">
	<div class="turn_content">
		<div class="tabl_quotes tabl_inner_contest">
		<?
			$gridWidget = $this->widget( 'widgets.gridViews.ContestMemberTradesGridViewWidget', Array(
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'dataProvider' => $DP,
				'ajax' => $ajax,
				'template' => '{items}',
				'member' => $member,
				'sums' => $sums,
				'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
				'pager' => array(
					'class'=> 'widgets.gridViews.base.WpPager'
				),
			));
			if( $DP->pagination->getPageCount() > 1 ){
				echo $gridWidget->renderPager();
			}
		?>
		</div>
	</div>
</div>

<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var idMember = "<?=$member->id?>";
			
			ns.wContestMemberTradesListWidget = wContestMemberTradesListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wContestMemberTradesListWidget',
				ajax: ajax,
				idMember: idMember,
				ajaxLoadListURL: '<?=Yii::app()->createUrl('contestMember/ajaxLoadMemberTradesList');?>',
			});
		}( window.jQuery );
	</script>
<?}?>