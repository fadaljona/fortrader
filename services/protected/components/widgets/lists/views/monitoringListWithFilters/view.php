<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$cs=Yii::app()->getClientScript();
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wMonitoringListWithFiltersWidget.js' ), CClientScript::POS_END );
	
	Yii::app()->clientScript->registerScript('InitTickSession', "
		!function( $ ) {
			var ns = {$ns};
			var nsText = '.{$ns}';
			var ins = '.{$ins}';
			var ls = ". json_encode( $jsls ) .";
					
			ns.wMonitoringListWithFiltersWidget = wMonitoringListWithFiltersWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' ' + ins,
				ls: ls,
				ajaxLoadListURL: '". Yii::app()->createUrl('monitoring/ajaxLoadAccountsList') ."',
				pageSize: ".$pageSize.",
				sortType: '$sortType',
			});
		}( window.jQuery );
	", CClientScript::POS_END);

	foreach( $periods as &$item ){
		$item = Yii::t($NSi18n, $item);
	}
	foreach( $sortTypes as &$item ){
		$item = Yii::t($NSi18n, $item);
	}
	$jsls = array();
?>

<div class="box_monitoring <?=$ins?> position-relative spinner-margin">
	<div class="row">
		<div class="grid_3 grid_md_12">
			<div class="informer_setings accountTypeBtns">
				<label><?=Yii::t($NSi18n, 'Account type')?>:</label>
				<button class="informer_setings_default monitoring_btn_type <?php if( $accountType == 'demo' ) echo 'active';?>" data-type="demo"><?=Yii::t($NSi18n, 'Demo')?></button>
				<button class="informer_setings_default monitoring_btn_type <?php if( $accountType == 'real' ) echo 'active';?> " data-type="real"><?=Yii::t($NSi18n, 'Real')?></button>
			</div><!-- / .monitoring_setings -->
		</div><!-- / .col- -->
		<div class="grid_3 grid_md_12">
			<div class="informer_setings">
				<label><?=Yii::t($NSi18n, 'Period')?>:</label>
				<?php echo CHtml::dropDownList('period', $period,  $periods, array( 'class' => 'select_type1 accountPeriod' ) );?>
			</div><!-- / .informer_setings -->
		</div><!-- / .col- -->
		<div class="grid_6 grid_md_12">
			<div class="informer_setings not_label">
				<label></label>
				<form class="quotes_search accountSearchForm">
					<input class="quotes_search_input accountSearchInput" type="text" placeholder="<?=Yii::t($NSi18n, 'Search...')?>">
					<button class="quotes_search_btn clearAccountSearchInput" type="button"><?=Yii::t($NSi18n, 'Clear')?></button>
				</form>
			</div><!-- / .informer_setings -->
		</div><!-- / .col- -->
	</div><!-- / .row -->
<?php /*	<div class="row">
		<div class="grid_3 grid_md_12">
			<div class="informer_setings">
				<label><?=Yii::t($NSi18n, 'Sorting')?>:</label>
				<?php echo CHtml::dropDownList('sortType', $sortType,  $sortTypes, array( 'class' => 'select_type1 sortType' ) );?>
			</div><!-- / .informer_setings -->
		</div><!-- / .col- -->
		<div class="grid_3 grid_md_12">
			<div class="informer_setings not_label sortTypeOrder">
				<label></label>
				<button class="informer_setings_default monitoring_btn_sort <?php if( $sortTypeOrder == 'ASC' ) echo 'active';?>" data-type="ASC">A-Z</button>
				<button class="informer_setings_default monitoring_btn_sort <?php if( $sortTypeOrder == 'DESC' ) echo 'active';?>" data-type="DESC">Z-A</button>
			</div><!-- / .monitoring_setings -->
		</div><!-- / .col- -->
		<div class="grid_3 grid_md_12">
			<div class="informer_setings">
				<label><?=Yii::t($NSi18n, 'Secondary Sorting')?>:</label>
				<?php echo CHtml::dropDownList('secondarySortType', $secondarySortType,  $sortTypes, array( 'class' => 'select_type1 secondarySortType' ) );?>
			</div><!-- / .informer_setings -->
		</div><!-- / .col- -->
		<div class="grid_3 grid_md_12">
			<div class="informer_setings not_label secondarySortTypeOrder">
				<label></label>
				<button class="informer_setings_default monitoring_btn_sort <?php if( $secondarySortTypeOrder == 'ASC' ) echo 'active';?>" data-type="ASC">A-Z</button>
				<button class="informer_setings_default monitoring_btn_sort <?php if( $secondarySortTypeOrder == 'DESC' ) echo 'active';?>"  data-type="DESC">Z-A</button>
			</div><!-- / .monitoring_setings -->
		</div><!-- / .col- -->
	</div><!-- / .row -->*/?>
	<div class="row negativ_filter">
		<input type="checkbox" id="negativ_filter" class="negativFilter" <?php if( $negativFilter ) echo 'checked';?> >
			<label for="negativ_filter" class="square_input">
			<i></i>
			<?=Yii::t($NSi18n, 'Include inactive accounts in the filter')?>
		</label>
	</div>
				<!-- - - - - - - - - - - - - - Tabs - - - - - - - - - - - - - - - - -->

	<div class="tabs_box section_offset">
		<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
		<div class="clearfix tabs_header">
			<ul class="tabs_list monitoring_tabs_list sortingByTabs">
				<li data-type="<?=$sortType?>"><a href="javascript:;"><?=$sortTypes[$sortType]?></a></li>
				<li data-type="<?=$secondarySortType?>"><a href="javascript:;"><?=$sortTypes[$secondarySortType]?></a></li>
				<li class="ortherSorting"><a href="javascript:;"></a></li>
			</ul>
			<?php /*<div class="informer_setings tabs_filter">
				<select class="select_type1 hzfilter">
					<option value="1">для чего?</option>
					<option value="1">Все счета</option>
					<option value="1">Все счета</option>
				</select>
			</div><!-- / .informer_setings -->*/?>
		</div>
		<!-- - - - - - - - - - - - - - End of Tabs List  - - - - - - - - - - - - - - - - -->
		<!-- - - - - - - - - - - - - - Tabs Contant - - - - - - - - - - - - - - - - -->
		<div class="tabs_contant">
			<!-- - - - - - - - - - - - - - Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<div class="active">
				<!-- - - - - - - - - - - - - - Tabl page  - - - - - - - - - - - - - - - - -->
				<section class="section_offset_2 turn_box">
					<div class="turn_content">
						<div class="tabl_quotes table_monitoring table_rating">
						<?php $gridWidget->run(); ?>
						</div>
					</div>
				</section>
				<?php
					if( $DP->pagination->getPageCount() > 1 ){
						echo '<a href="#" class="load_btn chench_btn" data-text="'. Yii::t($NSi18n, 'Show more') .'" data-shot-text="'. Yii::t($NSi18n, 'Show more') .'" data-page="1" data-count="'.$DP->pagination->getPageCount().'"></a>';
					}
				?>
				
				<!-- - - - - - - - - - - - - - End of Tabl page - - - - - - - - - - - - - - - - -->
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<?php /*<!-- - - - - - - - - - - - - - Tabs Item 2 - - - - - - - - - - - - - - - - -->
			<div>
				<!-- - - - - - - - - - - - - - Tabl page  - - - - - - - - - - - - - - - - -->
				<section class="section_offset_2 turn_box">
					<div class="turn_content">
						<div class="tabl_quotes table_monitoring">
							<table data-item='2'>
								<thead>
									<tr>
										<th><span>Счет</span></th>
										<th><span>Прирост</span></th>
										<th><span>Посадка</span></th>
										<th><span>Сделки</span></th>
										<th><span>Мониторинг</span></th>
										<th><span>Итог дня баланс</span></th>
										<th><span>Статус средства</span></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="">
											<span class=""><img src="http://web.com/ft/images/nwe_img/Currency-CB-RF-USD.png" height="13" width="16" alt=""></span>
											<span>Bpic.Msk</span>
											<span class="blue_color">7612303</span>
										</td>
										<td data-title='Прирост'><span class="monitoring_label_red">OPEN</span> <span class="">25%</span></td>
										<td data-title='Посадка'><span class="">254000.2 USD</span></td>
										<td data-title='Сделки'><span class="">17</span></td>
										<td data-title='Мониторинг'><span class="">456</span></td>
										<td data-title='Итог дня баланс'><span class="red_color">-59.6 USD</span></td>
										<td data-title='Статус средства'><span class="green_color">+12359.6 USD</span></td>
									</tr>
									<tr>
										<td class="">
											<span class=""><img src="http://web.com/ft/images/nwe_img/Currency-CB-RF-USD.png" height="13" width="16" alt=""></span>
											<span>Bpic.Msk</span>
											<span class="blue_color">7612303</span>
										</td>
										<td data-title='Прирост'><span class="monitoring_label_red">OPEN</span> <span class="">25%</span></td>
										<td data-title='Посадка'><span class="">254000.2 USD</span></td>
										<td data-title='Сделки'><span class="">17</span></td>
										<td data-title='Мониторинг'><span class="">456</span></td>
										<td data-title='Итог дня баланс'><span class="red_color">-59.6 USD</span></td>
										<td data-title='Статус средства'><span class="green_color">+12359.6 USD</span></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</section>
				<a href="#" class="load_btn chench_btn" data-text="Показать больше" data-shot-text="Показать больше"></a>
				<!-- - - - - - - - - - - - - - End of Tabl page - - - - - - - - - - - - - - - - -->
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs Item 2 - - - - - - - - - - - - - - - - -->*/?>
		</div>
		<!-- - - - - - - - - - - - - - End of Tabs Contant - - - - - - - - - - - - - - - - -->
	</div><!--/ .tabs_box -->
</div><!-- / .box_informer -->