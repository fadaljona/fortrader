<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wYourMonitoringAccountsListWidget.js' ), CClientScript::POS_END );
	}
	$jsls = Array(
		'lDeleteConfirm' => 'Delete?',
		'lEditBtnText' => 'Edit',
		'lAddAccountBtnText' => 'Add account',
		'lCancelBtnText' => 'Cancel',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<section class="section_offset_2 turn_box <?=$ins?> position-relative spinner-margin <?php if( !$DP->totalItemCount ) echo 'hide';?>">
	<hr class="separator2">
	<h3><?=Yii::t($NSi18n, 'Your accounts')?></h3>
	<div class="turn_content">
		<div class="<?php if( $type == 'frameForForum' ) echo 'tabl_ft'; else echo 'tabl_quotes';?> table_monitoring ">
		<?php
		if( $DP ){
			$gridWidget = $this->widget( 'widgets.gridViews.YourMonitoringAccountsGridViewWidget', Array(
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'showTableOnEmpty' => false,
				'dataProvider' => $DP,
				'ajax' => true,
				'template' => '{items}',
				'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
				'pager' => array(
					'class'=> 'widgets.gridViews.base.WpPager'
				),
				'type' => $type,
			));
								
			if( $DP->pagination->getPageCount() > 1 ){
				echo $gridWidget->renderPager();
			}
		}
		?>
		</div>
	</div>
</section>

<?php
if( !Yii::App()->request->isAjaxRequest ){
	
	
Yii::app()->clientScript->registerScript('YourMonitoringAccountsListJs', '

	!function( $ ) {
		var ns = '. $ns .';
		var nsText = ".'. $ns .'";
		var ins = ".'. $ins .'";
		var ajax = '. CommonLib::boolToStr(Yii::App()->request->isAjaxRequest) .';
			
		var ls = '. json_encode( $jsls ) .';
			
		ns.wYourMonitoringAccountsListWidget = wYourMonitoringAccountsListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+" " + ins,
			ajax: ajax,
			ls: ls,
			ajaxDeleteURL: "'. Yii::app()->createUrl('admin/contestMember/ajaxDelete') .'",
			ajaxLoadListURL: "'. Yii::app()->createUrl('monitoring/ajaxLoadYourAccounts') .'",
			ajaxLoadOneAccountInfo:  "'. Yii::app()->createUrl('monitoring/ajaxLoadOneAccountInfo') .'",
			type: "' . $type . '",
			forumId: "' . $forumId . '",
			ajaxSendAccIdToForumUrl:  "'. Yii::app()->createUrl('monitoring/ajaxSendAccIdToForumUrl') .'",
		});
	}( window.jQuery );

', CClientScript::POS_END);

}?>