<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wContestMemberTradesSymbolsListWidget.js' ) );
	}
?>

<div class="section_offset_2 turn_box iListWidget i01 wContestMemberTradesSymbolsListWidget<?=$type?> spinner-margin position-relative">
	<div class="monitoringSymbolsCheckboxWrapper">
	
	
		<div class="informer_setings">
			<form class="quotes_search symbolSearchForm">
				<input class="quotes_search_input symbolSearchInput" placeholder="<?=Yii::t($NSi18n, 'Symbol search...')?>" type="text" value="<?=$search?>">
			</form>
		</div>
	
	
		<div>
			<input id="byDirection<?=$type?>" class="byDirection<?=$type?>" <?php if( $byDirection ) echo 'checked';?> type="checkbox">
			<label for="byDirection<?=$type?>" class="square_input">
			<i></i>
			<?=Yii::t($NSi18n, 'By Direction')?></label>
		</div>
		
		<div>
			<input id="bySell<?=$type?>" class="bySell<?=$type?>" <?php if( $bySell ) echo 'checked';?> <?php /*if( !$byDirection ) echo 'disabled';*/?> type="checkbox">
			<label for="bySell<?=$type?>" class="square_input">
			<i></i>
			<?=Yii::t($NSi18n, 'By sell')?></label>
		</div>
			
		<div>
			<input id="byBuy<?=$type?>" class="byBuy<?=$type?>" <?php if( $byBuy ) echo 'checked';?> <?php /*if( !$byDirection ) echo 'disabled';*/?> type="checkbox">
			<label for="byBuy<?=$type?>" class="square_input">
			<i></i>
			<?=Yii::t($NSi18n, 'By buy')?></label>
		</div>
	</div>
	<div class="turn_content">
		<div class="tabl_quotes tabl_inner_contest tabl_inner_sortable">
		<?
			$gridWidget = $this->widget( 'widgets.gridViews.ContestMemberTradesSymbolsGridViewWidget', Array(
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'dataProvider' => $DP,
				'ajax' => $ajax,
				'template' => '{items}',
				'member' => $member,
				'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
				'byDirection' => $byDirection,
				'sortField' => $sortField,
				'sortType' => $sortType,
				'pager' => array(
					'class'=> 'widgets.gridViews.base.WpPager'
				),
			));
			if( $DP->pagination->getPageCount() > 1 ){
				echo $gridWidget->renderPager();
			}
		?>
		</div>
	</div>
</div>

<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;

			ns.wContestMemberTradesSymbolsListWidget<?=$type?> = wContestMemberTradesSymbolsListWidgetOpen({
				ns: <?=$ns?>,
				ins: ".<?=$ins?>",
				selector: ".<?=$ns?>"+' .wContestMemberTradesSymbolsListWidget<?=$type?>',
				ajax: <?=CommonLib::boolToStr($ajax)?>,
				idMember: "<?=$member->id?>",
				ajaxLoadListURL: '<?=Yii::app()->createUrl('contestMember/ajaxLoadMemberTradesSymbolsList');?>',
				type: '<?=$type?>',
				pageVar: '<?=$DP->pagination->pageVar?>',
				sortField: '<?=$sortField?>',
				sortType: '<?=$sortType?>',
			});
		}( window.jQuery );
	</script>
<?}?>