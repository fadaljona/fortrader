<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wAdminCurrencyRatesListWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wAdminCurrencyRatesListWidget">
	<?
		$gridview = $this->widget( 'widgets.gridViews.AdminCurrencyRatesGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => $showOnEmpty,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'selectionChanged' => $ajax ? "{$ns}.wAdminCurrencyRatesListWidget.selectionChanged" : null,
		));
	?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="<?=count($gridview->columns)?>">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		ns.wAdminCurrencyRatesListWidget = wAdminCurrencyRatesListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminCurrencyRatesListWidget',
			ajax: ajax,
			hidden: hidden,
			ajaxLoadURL: '<?=Yii::app()->createUrl('admin/currencyRates/ajaxLoadOptionRow')?>',
			modelName: '<?=$modelName?>',
		});
	}( window.jQuery );
</script>