<?
Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wAdminQuotesSymbolsListWidget.js" );
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wAdminQuotesSymbolsListWidget">
	<?
		$this->widget( 'widgets.gridViews.AdminQuotesSymbolsGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'selectionChanged' => $ajax ? "{$ns}.wAdminQuotesSymbolsListWidget.selectionChanged" : null,
			'filterURL' => $filterURL,
			'filter' => $filterModel
		));
	?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="2">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = false;
		var inSearch = <?=CommonLib::boolToStr($inSearch)?>;

		ns.wAdminQuotesSymbolsListWidget = wAdminQuotesSymbolsListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminQuotesSymbolsListWidget',
			ajax: ajax,
			inSearch: inSearch,
			hidden: hidden
		});
	}( window.jQuery );
</script>
