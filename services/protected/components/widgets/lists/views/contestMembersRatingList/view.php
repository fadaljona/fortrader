<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wContestMembersRatingListWidget.js' ) );
	}
?>

<!-- - - - - - - - - - - - - - Tabl page  1- - - - - - - - - - - - - - - - -->
<section class="section_offset_2 turn_box iListWidget i01 wContestMembersRatingListWidget iRelative spinner-margin position-relative">
	<div class="competition_block_head clearfix">
		<h4><?=Yii::t( $NSi18n, 'Rating Members Forex Contests' )?><i class="fa fa-line-chart" aria-hidden="true"></i></h4>
	</div>
	<div class="turn_content">
		<div class="tabl_quotes competition_table table2">
		<?php
			$gridWidget = $this->widget( 'widgets.gridViews.ContestMembersRatingGridViewWidget', Array(
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'showTableOnEmpty' => $showOnEmpty,
				'dataProvider' => $DP,
				'ajax' => $ajax,
				'template' => '{items}',
				'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
				'pager' => array(
					'class'=> 'widgets.gridViews.base.WpPager'
				),
			));
		?>
		</div>
	</div>
	<?php
		if( $DP->pagination->getPageCount() > 1 ){
			echo $gridWidget->renderPager();
		}
	?>
</section>
<!-- - - - - - - - - - - - - - End of Tabl page 1- - - - - - - - - - - - - - - - -->

<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var hidden = <?=CommonLib::boolToStr($hidden)?>;
			
			ns.wContestMembersRatingListWidget = wContestMembersRatingListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wContestMembersRatingListWidget',
				ajax: ajax,
				hidden: hidden,
				ajaxLoadListURL: '<?=Yii::app()->createAbsoluteUrl('contest/ajaxLoadMembersRatingList')?>',
			});
		}( window.jQuery );
	</script>
<?}?>