<?php
$cs=Yii::app()->getClientScript();

$cs->registerScriptFile(
    CHtml::asset(Yii::getPathOfAlias('webroot.js.widgets.lists').'/wChooseExchangerByDirectionWidget.js'),
    CClientScript::POS_END
);

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>

<div class="fx_section <?=$ins?> position-relative spinner-margin">
    <div class="wExchangersWithCoursesAndReservesGridView">
    <?php
    $gridWidget = $this->widget('widgets.gridViews.ExchangersRatingGridViewWidget', array(
        'NSi18n' => $NSi18n,
        'ins' => $ins,
        'showTableOnEmpty' => $showOnEmpty,
        'dataProvider' => $DP,
        'ajax' => $ajax,
        'template' => '{items}',
        'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
        'pager' => array(
            'class'=> 'widgets.gridViews.base.WpPager'
        ),
    ));

    if ($DP->pagination->getPageCount() > 1) {
        echo $gridWidget->renderPager();
    }
    ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScript('ChooseExchangerByDirectionWidgetJs', '

!function( $ ) {
    var ns = ' . $ns . ';
    var ins = ".' . $ins . '";

    ns.wChooseExchangerByDirectionWidget = wChooseExchangerByDirectionWidgetOpen({
        ns: ns,
        ins: ins,
        selector: ".'.$ins.'",
        ajaxLoadListURL: "' . Yii::app()->createAbsoluteUrl("exchangeECurrency/ajaxLoadListExchangersRating") . '",
        ajaxAddMarkURL: "' . Yii::app()->createUrl('exchangeECurrency/ajaxAddMarkToExchanger') . '",
    });
}( window.jQuery );

', CClientScript::POS_END);