<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wCurrentUserContestMemberInfoListWidget.js' ) );
		$jsls = Array(
			'lDeleteConfirm' => 'Delete?',
		);
		foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	}

?>
<div class="<?=$ins?> spinner-margin position-relative">
	<section class="section_offset paddingBottom30">
		<h5 class="info_title"><?=Yii::t( $NSi18n, 'Account Information' )?></h5>
	</section>
	<div class="section_offset <?=$ins?>">
		<div class="wrapper">
			<?php $this->widget( "widgets.detailViews.ContestMemberInfoDetailViewWidget", Array( 'data' => $model ));?>
		</div>
	</div>
</div>

<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wCurrentUserContestMemberInfoListWidget = wCurrentUserContestMemberInfoListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .<?=$ins?>',
				ajax: ajax,
				ls: ls,
				ajaxDeleteURL: '<?=Yii::app()->createUrl('user/ajaxDeleteContestMember', array( 'id' => $model->id ) )?>',
				editURL: '<?=Yii::app()->createUrl('user/profile', array( 'tabTA' => 'yes', 'idEditMember' => $model->id ) )?>',
			});
		}( window.jQuery );
	</script>
<?}?>