<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wEAStatementsListWidget.js" );
	}
	//$countsRows = Array( 10, 20, 50, 100 );
	//$countsRows = array_combine( $countsRows, $countsRows );
	
	$jsls = Array(
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="iListWidget i01 wEAStatementsListWidget iRelative">
	<?/*if( $name ){?>
		<div class="table-header">
			<?=Yii::t( $NSi18n, 'Results for' )?>
			"<?=htmlspecialchars( $name )?>"
		</div>
	<?}*/?>
	<?/*
	<div class="iDiv i01 iRelative" style="padding-top:0;height:38px;">
		<div class="pull-left" style="padding-top:10px;">
			<?=Yii::t( $NSi18n, 'Display records' )?>
			<?=CHtml::dropDownList( 'countRows', $countRows, $countsRows, Array( 'class' => "iSelect i02 {$ins} wSelectCountRows" ))?>
		</div>
		<div class="iDiv i02 pull-right" style="position:absolute;top:10px;right:10px;">
			<span class="input-icon">
				<?=CHtml::textField( 'name', $name, Array( 'class' => "iInput i02 {$ins} wName", 'placeholder' => Yii::t( $NSi18n, 'Search' ), 'maxlength' => 50 ))?>
				<i class="icon-search" id="nav-search-icon"></i>
			</span>
		</div>
		<div class="iClear"></div>
	</div>
	*/?>
	<?
		$gridWidget = $this->widget( 'widgets.gridViews.EAStatementsGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => $showOnEmpty,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'template' => '{items}',
			'pagerCssClass' => 'pagination pagination-right',
			'sortField' => $sortField,
			'sortType' => $sortType,
			'idEA' => $idEA,
			'idVersion' => $idVersion,
		));
	?>
	<?if( $DP->pagination->getPageCount() > 1 ){?>
		<div class="iDiv i01">
			<?=$gridWidget->renderPager()?>
		</div>
	<?}?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="<?=count( $gridWidget->columns )?>">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
	<div class="iDummReload i01 <?=$ins?> wDummReload" style="display:none;"></div>
	<table class="iDummReloadText i01 <?=$ins?> wDummReloadText" style="display:none;">
		<tr>
			<td valign="middle" align="center">
				<span class="iSpan i01">
					<?=Yii::t( $NSi18n, 'Loading...' )?>
				</span>
			</td>
		</tr>
	</table>
</div>
<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var hidden = <?=CommonLib::boolToStr($hidden)?>;
			var sortField = "<?=$sortField?>";
			var sortType = "<?=$sortType?>";
			var idEA = <?=$idEA?$idEA:'null'?>;
			var idVersion = <?=$idVersion?$idVersion:'null'?>;
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wEAStatementsListWidget = wEAStatementsListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wEAStatementsListWidget',
				ajax: ajax,
				hidden: hidden,
				ls: ls,
				sortField: sortField,
				sortType: sortType,
				idEA: idEA,
			});
		}( window.jQuery );
	</script>
<?}?>