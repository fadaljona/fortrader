<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wContestMembersListWidget.js' ) );
	}
	$countsRows = Array( 10, 20, 50, 100 );
	$countsRows = array_combine( $countsRows, $countsRows );
	
	$jsls = Array(
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="iListWidget i01 wContestMembersListWidget iRelative">
	<?if( $accountNumber ){?>
		<div class="table-header">
			<?=Yii::t( $NSi18n, 'Results for' )?>
			"<?=htmlspecialchars( $accountNumber )?>"
		</div>
	<?}?>
	<div class="iDiv i01 iRelative" style="padding-top:0;height:38px;">
		<div class="pull-left" style="padding-top:10px;">
			<?=Yii::t( $NSi18n, 'Display records' )?>
			<?=CHtml::dropDownList( 'countRows', $countRows, $countsRows, Array( 'class' => "iSelect i02 {$ins} wSelectCountRows" ))?>
		</div>
		<div class="iDiv i02 pull-right" style="position:absolute;top:10px;right:10px;">
			<span class="input-icon">
				<?=CHtml::textField( 'accountNumber', $accountNumber, Array( 'class' => "iInput i02 {$ins} wAccountNumber", 'placeholder' => Yii::t( $NSi18n, 'Search' ), 'maxlength' => 50 ))?>
				<i class="icon-search" id="nav-search-icon"></i>
			</span>
		</div>
		<div class="iClear"></div>
	</div>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#contest_members" aria-controls="contest_members" role="tab" data-toggle="tab"><?=Yii::t( $NSi18n, 'Contest members' )?></a></li>
            <li role="presentation"><a href="#disqualified_members" aria-controls="disqualified_members" role="tab" data-toggle="tab"><?=Yii::t( $NSi18n, 'Disqualified members' )?></a></li>
            <li role="presentation"><a href="#with_error" aria-controls="with_error" role="tab" data-toggle="tab"><?=Yii::t( $NSi18n, 'With error' )?></a></li>
        </ul>
        <div class="tab-content" style="padding: 0">
            <div role="tabpanel" class="tab-pane active" id="contest_members">
                <?
                $gridWidget = $this->widget( 'widgets.gridViews.ContestMembersGridViewWidget', Array(
                        'NSi18n' => $NSi18n,
                        'ins' => $ins,
                        'showTableOnEmpty' => $showOnEmpty,
                        'dataProvider' => $DP['contest_members'],
                        'ajax' => $ajax,
                        'template' => '{items}',
                        'pagerCssClass' => 'pagination pagination-right',
                        'sortField' => $sortField,
                        'sortType' => $sortType,
                ));
                ?>
                <?if( $DP['contest_members']->pagination->getPageCount() > 1 ){?>
                    <div class="iDiv i01">
                        <?=$gridWidget->renderPager()?>
                    </div>
                <?}?>
            </div>
            <div role="tabpanel" class="tab-pane" id="disqualified_members">
                <?
                $gridWidget = $this->widget( 'widgets.gridViews.ContestMembersGridViewWidget', Array(
                        'NSi18n' => $NSi18n,
                        'ins' => $ins,
                        'showTableOnEmpty' => $showOnEmpty,
                        'dataProvider' => $DP['disqualified_members'],
                        'ajax' => $ajax,
                        'template' => '{items}',
                        'pagerCssClass' => 'pagination pagination-right',
                        'sortField' => $sortField,
                        'sortType' => $sortType,
                ));
                ?>
                <?if( $DP['disqualified_members']->pagination->getPageCount() > 1 ){?>
                    <div class="iDiv i01">
                        <?=$gridWidget->renderPager()?>
                    </div>
                <?}?>
            </div>
            <div role="tabpanel" class="tab-pane" id="with_error">
                <?
                $gridWidget = $this->widget( 'widgets.gridViews.ContestMembersGridViewWidget', Array(
                        'NSi18n' => $NSi18n,
                        'ins' => $ins,
                        'showTableOnEmpty' => $showOnEmpty,
                        'dataProvider' => $DP['with_error'],
                        'ajax' => $ajax,
                        'template' => '{items}',
                        'pagerCssClass' => 'pagination pagination-right',
                        'sortField' => $sortField,
                        'sortType' => $sortType,
                ));
                ?>
                <?if( $DP['with_error']->pagination->getPageCount() > 1 ){?>
                    <div class="iDiv i01">
                        <?=$gridWidget->renderPager()?>
                    </div>
                <?}?>
            </div>
        </div>

    </div>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="<?=count( $gridWidget->columns )?>">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
	<div class="iDummReload i01 <?=$ins?> wDummReload" style="display:none;"></div>
	<table class="iDummReloadText i01 <?=$ins?> wDummReloadText" style="display:none;">
		<tr>
			<td valign="middle" align="center">
				<span class="iSpan i01">
					<?=Yii::t( $NSi18n, 'Loading...' )?>
				</span>
			</td>
		</tr>
	</table>
</div>
<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>

		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var hidden = <?=CommonLib::boolToStr($hidden)?>;
			var idContest = <?=$idContest?>;
			var sortField = "<?=$sortField?>";
			var sortType = "<?=$sortType?>";
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wContestMembersListWidget = wContestMembersListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wContestMembersListWidget',
				ajax: ajax,
				hidden: hidden,
				idContest: idContest,
				sortField: sortField,
				sortType: sortType,
				ls: ls,
			});
		}( window.jQuery );
	</script>
<?}?>