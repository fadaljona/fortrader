<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wContestMembersListWidget.js' ) );
	}
	$countsRows = Array( 10, 20, 50, 100 );
	$countsRows = array_combine( $countsRows, $countsRows );
	
	$jsls = Array(
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div id="singleContestMembersListWidget" class="iListWidget i01 wContestMembersListWidget iRelative position-relative spinner-margin">
<?php
	$searchVal = '';
	if( $accountNumber ){
		$searchVal = htmlspecialchars( $accountNumber );
	}
?>
	<hr class="separator2">
	<section class="section_offset">
		<h3><?=Yii::t( $NSi18n, 'Contest members rating' )?><i class="fa fa-trophy" aria-hidden="true"></i></h3>
		<hr class="separator2">
	</section>
	<!-- - - - - - - - - - - - - - Tabs - - - - - - - - - - - - - - - - -->
	<div class="tabs_box accordion_tabs tabs_rating">
		<div class="clearfix">
			<div class="wrapper_quotes_search_box_2">
				<div class="quotes_search_box_2">
					<form method="get" action="./" onsubmit="event.preventDefault();">
						<button class="quotes_search_btn"><i class="fa fa-search"></i></button>
						<input type="text" placeholder="<?=Yii::t( $NSi18n, 'Search' )?>" name="accountNumber<?=rand()?>" class="<?=$ins?> wAccountNumber" <?php if( $searchVal ){ ?>value="<?=$searchVal?>"<?php }?>>
					</form>
				</div>
			</div>
			<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
			<div class="wrapper_tabs_list">
			<?php
				$contest_members_active = $DP['contest_members']->totalItemCount ? 'active' : '';
				$disqualified_members_active = $DP['disqualified_members']->totalItemCount ? 'active' : '';
				$with_error_active = $DP['with_error']->totalItemCount ? 'active' : '';
				
				$disqualifiedTotalItemCount = $DP['disqualified_members']->totalItemCount;
				$withErrorTotalItemCount = $DP['with_error']->totalItemCount;
				
				if( !($contest_members_active||$disqualified_members_active||$with_error_active) ){
					$contest_members_active = 'active';
				}elseif($contest_members_active){
					$disqualified_members_active='';
					$with_error_active='';
				}elseif( $disqualified_members_active && !$contest_members_active){
					$with_error_active='';
				}
			?>
			<?php if($disqualifiedTotalItemCount || $withErrorTotalItemCount){ ?>
				<ul class="tabs_list clearfix">
					<li class="<?=$contest_members_active?>"><a href="javascript:;"><?=Yii::t( $NSi18n, 'Participate' )?></a></li>
					<?php if($disqualifiedTotalItemCount){ ?><li class="<?=$disqualified_members_active?>"><a href="javascript:;"><?=Yii::t( $NSi18n, 'Disqualified' )?></a></li><?php } ?>
					<?php if($withErrorTotalItemCount){ ?><li class="<?=$with_error_active?>"><a href="javascript:;"><?=Yii::t( $NSi18n, 'Errors' )?></a></li><?php } ?>
				</ul>
			<?php } ?>
			</div>
		</div>
		<!-- - - - - - - - - - - - - - End of Tabs List  - - - - - - - - - - - - - - - - -->
		<!-- - - - - - - - - - - - - - Tabs Contant - - - - - - - - - - - - - - - - -->
		<div class="tabs_contant">
			<!-- - - - - - - - - - - - - - Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<div class="<?=$contest_members_active?>">
				<!-- - - - - - - - - - - - - - Tabl page  - - - - - - - - - - - - - - - - -->
				<div class="section_offset_2 turn_box">
					<div class="turn_content">
						<div class="tabl_quotes tabl_inner_contest">
							<?
							$gridWidget = $this->widget( 'widgets.gridViews.ContestMembersGridViewWidget', Array(
									'NSi18n' => $NSi18n,
									'ins' => $ins,
									'showTableOnEmpty' => $showOnEmpty,
									'dataProvider' => $DP['contest_members'],
									'ajax' => $ajax,
									'template' => '{items}',
									'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
									'pager' => array(
										'class'=> 'widgets.gridViews.base.WpPager'
									),
									'sortField' => $sortField,
									'sortType' => $sortType,
									'tabType' => 'Participate',
							));
							
							if( $DP['contest_members']->pagination->getPageCount() > 1 ){
								echo $gridWidget->renderPager();
							}
							?>
	
						</div>
					</div>
				</div>
				<!-- - - - - - - - - - - - - - End of Tabl page - - - - - - - - - - - - - - - - -->
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<?php if($disqualifiedTotalItemCount){ ?>
				<!-- - - - - - - - - - - - - - Tabs Item 2 - - - - - - - - - - - - - - - - -->
				<div class="<?=$disqualified_members_active?>">
					<!-- - - - - - - - - - - - - - Tabl page  - - - - - - - - - - - - - - - - -->
					<div class="section_offset_2 turn_box">
						<div class="turn_content">
							<div class="tabl_quotes tabl_inner_contest">
								<?
								$gridWidget = $this->widget( 'widgets.gridViews.ContestMembersGridViewWidget', Array(
										'NSi18n' => $NSi18n,
										'ins' => $ins,
										'showTableOnEmpty' => $showOnEmpty,
										'dataProvider' => $DP['disqualified_members'],
										'ajax' => $ajax,
										'template' => '{items}',
										'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
										'pager' => array(
											'class'=> 'widgets.gridViews.base.WpPager'
										),
										'sortField' => $sortField,
										'sortType' => $sortType,
								));
								
								if( $DP['disqualified_members']->pagination->getPageCount() > 1 ){
									echo $gridWidget->renderPager();
								}
								?>
		
							</div>
						</div>
					</div>
					<!-- - - - - - - - - - - - - - End of Tabl page - - - - - - - - - - - - - - - - -->
				</div>
				<!-- - - - - - - - - - - - - - End of Tabs Item 2 - - - - - - - - - - - - - - - - -->
			<?php } ?>
			<?php if($withErrorTotalItemCount){ ?>
				<!-- - - - - - - - - - - - - - Tabs Item 3 - - - - - - - - - - - - - - - - -->
				<div class="<?=$with_error_active?>">
					<!-- - - - - - - - - - - - - - Tabl page  - - - - - - - - - - - - - - - - -->
					<div class="section_offset_2 turn_box">
						<div class="turn_content">
							<div class="tabl_quotes tabl_inner_contest">
								<?
								$gridWidget = $this->widget( 'widgets.gridViews.ContestMembersGridViewWidget', Array(
										'NSi18n' => $NSi18n,
										'ins' => $ins,
										'showTableOnEmpty' => $showOnEmpty,
										'dataProvider' => $DP['with_error'],
										'ajax' => $ajax,
										'template' => '{items}',
										'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
										'pager' => array(
											'class'=> 'widgets.gridViews.base.WpPager'
										),
										'sortField' => $sortField,
										'sortType' => $sortType,
										'tabType' => 'Error',
								));
								
								if( $DP['with_error']->pagination->getPageCount() > 1 ){
									echo $gridWidget->renderPager();
								}
								?>
		
							</div>
						</div>
					</div>
					<!-- - - - - - - - - - - - - - End of Tabl page - - - - - - - - - - - - - - - - -->
				</div>
				<!-- - - - - - - - - - - - - - End of Tabs Item 3 - - - - - - - - - - - - - - - - -->
			<?php } ?>
		</div><!-- / .tabs_contant -->
	
		<!-- - - - - - - - - - - - - - End of Tabs Contant - - - - - - - - - - - - - - - - -->
	</div>
	<!-- - - - - - - - - - - - - - End of Tabs - - - - - - - - - - - - - - - - -->
</div>

<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>

		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var hidden = <?=CommonLib::boolToStr($hidden)?>;
			var idContest = <?=$idContest?>;
			var sortField = "<?=$sortField?>";
			var sortType = "<?=$sortType?>";
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wContestMembersListWidget = wContestMembersListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wContestMembersListWidget',
				ajax: ajax,
				hidden: hidden,
				idContest: idContest,
				sortField: sortField,
				sortType: sortType,
				ls: ls,
				ajaxDeleteURL: '<?=Yii::app()->createUrl('admin/contestMember/ajaxDelete')?>',
				ajaxLoadListURL: '<?=Yii::app()->createUrl('contest/ajaxLoadMembersList')?>',
			});
		}( window.jQuery );
	</script>
<?}?>