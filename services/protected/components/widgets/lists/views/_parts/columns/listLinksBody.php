<?
	$out = Array();
	foreach( $data->{$column->relation} as $data ){
		$label = $data->{$column->field};
		$url = @$column->fieldURL ? $data->{$column->fieldURL} : $value;
		$url = CommonLib::getURL( $url );
		$out[] = CHtml::link( CHtml::encode( $label ), $url );
	}
	echo implode( ", ", $out );
?>
