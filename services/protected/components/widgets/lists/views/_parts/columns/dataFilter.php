<?
	if( !@$column->field || !@$column->filtering == 'yes' )
		return;
		
	$model = $this->getModel();
	if( @$column->relation ) {
		$relation = $model->_getRelation( $column->relation );
		if( $relation instanceof CHasManyRelation )
			return;
	}
	
	$variants = @$column->relation ? $model->_getFieldVariants( $relation->foreignKey )
	                               : $model->_getFieldVariants( $column->field );
	
	if( $variants ) {
		if( !array_key_exists( "", $variants )) {
			$variants = CommonLib::mergeAssocs( Array( '' => '' ), $variants );
		}
	}
	
	
	$filters = $this->getFilters();
	$value = @$filters->{$column->name};
?>
<?if( $variants ){?>
	<?=CHtml::dropDownList( 'test', $value, $variants, Array( 'class' => 'iInput i03 i04' ))?>
<?}else{?>
	<input type="text" value="<?=htmlspecialchars($value)?>" class="iInput i03 i04">
<?}?>
