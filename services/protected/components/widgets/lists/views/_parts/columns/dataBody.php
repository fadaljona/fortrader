<?
	if( @$column->raw != 'yes' )
		$value = htmlspecialchars( $value );
?>
<?if( @$column->popup == 'yes' && $countValue >= (int)@$column->minCountForPopup ){?>
	<div class="position-relative">
		<a href="#" data-toggle="dropdown"><?=Yii::t('*','List')?></a>
		<div class="dropdown-menu dropdown-caret" style="padding:10px;">
			<?=$value?>
		</div>
	</div>
<?}else{?>
	<?=$value?>
<?}?>