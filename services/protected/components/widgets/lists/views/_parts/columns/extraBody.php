<?
	if( !@$column->action )
		return;
	
?>
<div class="wExtraActionsBlock iRelative">
	<button class="btn btn-mini btn-info wExtraActionsButton">
		<i class="icon-cog"></i>
	</button>
	<div class="iDiv i73 wExtraActionsPane" style="">
		<div>
			<?foreach( $column->action as $action ){?>
				<<?=$action->tag?> class="btn btn-mini <?=$action->class?>">
					<i class="<?=$action->icon?>"></i>
					<?=$this->translateR( $action->title )?> 
				</<?=$action->tag?>>
			<?}?>
		</div>
	</div>
</div>