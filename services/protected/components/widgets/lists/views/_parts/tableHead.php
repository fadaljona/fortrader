<thead>
	<tr>
		<?foreach( $columns as $column ){?>
			<?call_user_func( Array( $this, $column->funcHeader ), @$column->viewHeader, Array( 'column' => $column ));?>
		<?}?>
	</tr>
	<tr class="iTrFilter i01">
		<?foreach( $columns as $column ){?>
			<?call_user_func( Array( $this, $column->funcFilter ), @$column->viewFilter, Array( 'column' => $column ));?>
		<?}?>
	</tr>
</thead>