<?
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>

<?if (!Yii::App()->request->isAjaxRequest) {?>
<div class="<?=$this->getNS()?>">
<?}?>
  <div class="<?=$this->class?> i01 <?=$this->w?> iRelative row-fluid columnWidget">
    <h4 class="row-fluid"><?=$this->getColumnTitle()?></h4>
    <p class="row-fluid"><?=$this->getColumnDesc()?></p>

<?
  $iCounter = 0;
  $dataList = $this->getDP()->getData(true);
  foreach ($dataList as $data) {
    if ($iCounter++ < $this->limit) {
      $this->render($view, array('data' => $data));
    }
  }
?>

<?if ($this->limit < count($dataList)) {?>
    <div class="moreLink">
      <i class="more-i"></i>
      <?=CHtml::link(Yii::t($NSi18n, 'Show more'), '#')?>
    </div>
<?}?>
  
    <table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
      <tr class="<?=$ins?> wLoading">
        <td>
          <div class="progress progress-info progress-striped active">
            <div class="bar" style="width: 100%;"></div>
          </div>
        </td>
      </tr>
    </table>
    <div class="iDummReload i01 <?=$ins?> wDummReload" style="display:none;"></div>
    <table class="iDummReloadText i01 <?=$ins?> wDummReloadText" style="display:none;">
      <tr>
        <td valign="middle" align="center">
				<span class="iSpan i01">
					<?=Yii::t( $NSi18n,'Loading...')?>
				</span>
        </td>
      </tr>
    </table>
  </div>
<?if (!Yii::App()->request->isAjaxRequest) {?>
</div>
<?}?>

<?if (!Yii::App()->request->isAjaxRequest) {
  Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wColumnListWidget.js" );
}?>
  
<?if (!Yii::App()->request->isAjaxRequest) {?>
  <script>
    var <?=$ns?> = {};

    !function( $ ) {
      var ns = <?=$ns?>;
      var nsText = ".<?=$ns?>";
      var ins = ".<?=$ins?>";
      var ajax = <?=CommonLib::boolToStr($ajax)?>;
      var hidden = <?=CommonLib::boolToStr($hidden)?>;
      //var sortField = "<?//=$sortField?>";
      //var sortType = "<?//=$sortType?>";

      var ajaxUpdate = <?=!empty($this->afterAjaxUpdate) ? $this->afterAjaxUpdate : 'null'?>

      ns.wColumnListWidget = wColumnListWidgetOpen({
        ns: ns,
        ins: ins,
        selector: nsText+' .<?=$this->w?>',
        ajax: ajax,
        hidden: hidden,
//        ls: ls,
        //sortField: sortField,
        //sortType: sortType,
        afterAjaxUpdate: ajaxUpdate,
      });
    }( window.jQuery );
  </script>
<?}?>