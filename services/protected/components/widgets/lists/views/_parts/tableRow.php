<tr data-id="<?=$data->getPrimaryKey()?>">
	<?foreach( $columns as $column ){?>
		<?call_user_func( Array( $this, $column->funcBody ), @$column->viewBody, Array( 'column' => $column, 'data' => $data ));?>
	<?}?>
</tr>