<?
	$pagination = $this->getDP()->getPagination();
	if( !$pagination )
		return;
?>
<div class="pagination iPagination i01">
	<?$this->widget( 'CLinkPager', Array(
		'pages' => $pagination,
		'header' => '',
		'firstPageLabel' => '««&nbsp;&nbsp;'.$this->translateR( 'First' ),
		'lastPageLabel' => $this->translateR( 'Last' ).'&nbsp;&nbsp;»»',
		'prevPageLabel' => '«&nbsp;&nbsp;'.$this->translateR( 'Previous' ),
		'nextPageLabel' => $this->translateR( 'Next' ).'&nbsp;&nbsp;»',
		'hiddenPageCssClass' => 'disabled',
		'selectedPageCssClass' => 'active',
		'maxButtonCount' => 5,
		'htmlOptions' => Array(
			'class' => '',
		),
	))?>
</div>