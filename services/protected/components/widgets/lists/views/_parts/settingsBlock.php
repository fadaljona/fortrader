<?
	$columns = $this->getColumns();
	
	$DP = $this->getDP();
	$DP->getData();
	$pagination = $DP->getPagination();
	$page = $pagination ? $pagination->getCurrentPage() : null;
	$pagesCount = $pagination ? $pagination->getPageCount() : null;
?>
<div class="pull-right iRelative wSettingsBlock">
	<button class="btn btn-mini btn-inverse iBtn i14 wToggleSettingsBlock" type="button">
		<i class="icon-cog"></i>
	</button>
	<div class="wSettingsPane iDiv i70">
		<form style="margin:0;">
			<div class="iDiv i71 text-center">
				<i class="icon-cog"></i>
				<?=$this->translateR( 'Settings' )?>
			</div>
			<div class="iDiv i72">
				<div>
					<?if( count( $columns ) > 1 ){?>
						<input type="checkbox" name="allColumns"> 
						<span class="lbl">
							<?=$this->translateR( 'Columns' )?>:
						</span>
					<?}else{?>
						<?=$this->translateR( 'Columns' )?>:
					<?}?>
					<?foreach( $columns as $column ){?>
						<?if( @$column->required == 'yes' ) continue;?>
						<?if( @$column->hidden == 'yes' ) continue;?>
						<label>
							<?=CHtml::tag( 'input', Array(
								'name' => 'column',
								'type' => 'checkbox',
								'data-name' => $column->name,
							))?>
							<span class="lbl">
								<b><?=$column->header?></b>
							</span>
						</label>
					<?}?>
				</div>
								
				<div style="display: inline-block;">
					<div>
						<label><?=$this->translateR( 'Limit' )?>:</label>
						<?=CHtml::dropDownList( 'limit', $this->getDPLimit(), $this->getAvailableLimits(), Array( 'class' => 'input-small' ))?>
					</div>
				</div>
				<div style="display: inline-block;">
					<div>
						<label><?=$this->translateR( 'Page' )?>:</label>
						<div class="wSettingsSpotPage" data-current-page="<?=$page?>" data-pages-count="<?=$pagesCount?>" data-total-items-count="<?=$DP->getTotalItemCount()?>"></div>
					</div>
				</div>
							
				<label>
					<input type="checkbox" name="resetSize"> 
					<span class="lbl">
						<?=$this->translateR( 'Reset sizes' )?>
					</span>
				</label>
				
				<label>
					<input type="checkbox" name="resetSort"> 
					<span class="lbl">
						<?=$this->translateR( 'Reset sorting' )?>
					</span>
				</label>
							
				<div class="text-center">
					<button class="btn btn-small btn-success wSaveSettings" type="submit" style="margin-top:5px;">
						<i class="icon-ok"></i>
						<?=$this->translateR( 'Save' )?>
					</button>
				</div>
			</div>
		</form>
	</div>
</div>