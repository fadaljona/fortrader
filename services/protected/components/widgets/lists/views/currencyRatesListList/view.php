<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$gridEmptyText = Yii::t('zii','No results found.');
	
	if( $dataType == 'cbr' && $type=='tomorrow' ){
		if( $daySecondsToNewData ){
			$gridEmptyText = Yii::t($NSi18n, 'tomorrowCurrencyRatesEmptyText') . ' ' . Chtml::tag('span', array( 'class' => 'endDateRates' ), '');
		}else{
			$gridEmptyText = Yii::t($NSi18n, 'tomorrowCurrencyRatesEmptyTextNoCounter') . ' ' . Chtml::tag('span', array( 'class' => 'endDateRates' ), '');
		}
	}

	if( $dataType == 'cbr' ){
		$gridview = $this->widget( 'widgets.gridViews.CurrencyRatesGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'dataProvider' => $DP,
			'dayType' => $type,
			'template' => '{items}',
			'emptyText' => $gridEmptyText,
			'toCurrency' => $toCurrency
		));
	}elseif( $dataType == 'ecb' ){
		$gridview = $this->widget( 'widgets.gridViews.CurrencyRatesEcbGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'dataProvider' => $DP,
			'dayType' => $type,
			'template' => '{items}',
			'emptyText' => $gridEmptyText,
			'toCurrency' => $toCurrency
		));
	}
	
