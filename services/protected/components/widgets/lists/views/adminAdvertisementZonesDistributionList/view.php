<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wAdminAdvertisementZonesDistributionListWidget">
	<?
		$listWidget = $this->widget( 'widgets.gridViews.AdminAdvertisementZonesDistributionGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'dataProvider' => $DP,
			'selectionChanged' => $ajax ? "{$ns}.wAdminAdvertisementZonesDistributionListWidget.selectionChanged" : null,
		));
	?>
</div>