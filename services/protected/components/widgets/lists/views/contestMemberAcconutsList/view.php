<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wUserProfilePagingListWidget.js' ) );
	
	$jsls = Array(
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$itCurrentModel = $this->idUser === Yii::App()->user->id;
?>
<section class="informer-calendar__section-calendar <?=$ins ?> spinner-margin position-relative">
	<div class="trade-account__title"><?=Yii::t( $NSi18n, 'Trading accounts' )?></div>
	<div class="turn_content">
		<div class="tabl_quotes informer-calendar__tb trade-account__table">
			<?
				$gridWidget = $this->widget( 'widgets.gridViews.ContestMemberAcconutsGridViewWidget', Array(
					'NSi18n' => $NSi18n,
					'ins' => $ins,
					'showTableOnEmpty' => $showOnEmpty,
					'dataProvider' => $DP,
					'template' => '{items}',
					'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
					'pager' => array(
						'class'=> 'widgets.gridViews.base.WpPager'
					),
				));
			?>
		</div>
	</div>
	<?php
		if( $DP->pagination->getPageCount() > 1 ){
			echo $gridWidget->renderPager();
		}
	?>
</section>


<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var hidden = <?=CommonLib::boolToStr($hidden)?>;
			var idUser = <?=$this->idUser?>;
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wUserProfilePagingListWidget = wUserProfilePagingListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .<?=$ins ?>',
				hidden: hidden,
				idUser: idUser,
				ls: ls,
				ajaxLoadListURL: '<?=Yii::app()->createUrl('user/ajaxLoadContestMembersList')?>',
				pageVar: 'membersPage',
			});
		}( window.jQuery );
	</script>
<?}?>