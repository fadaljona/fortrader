<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wContestMemberInfoListWidget.js" );
		$jsls = Array(
			'lDeleteConfirm' => 'Delete?',
		);
		foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	}
?>
<div class="iListWidget i01 wContestMemberInfoListWidget">
	<?
		$gridWidget = $this->createGridViewWidget();
		$gridWidget->run();
	?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="<?=count($gridWidget->columns)?>">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wContestMemberInfoListWidget = wContestMemberInfoListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wContestMemberInfoListWidget',
				ajax: ajax,
				ls: ls,
			});
		}( window.jQuery );
	</script>
<?}?>