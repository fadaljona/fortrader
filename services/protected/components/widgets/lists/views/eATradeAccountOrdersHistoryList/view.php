<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wEATradeAccountOrdersHistoryListWidget.js" );
	}
?>
<div class="iListWidget i01 wEATradeAccountOrdersHistoryListWidget">
	<?
		$gridWidget = $this->widget( 'widgets.gridViews.ContestMemberOrdersHistoryGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'template' => '{items}',
			'pagerCssClass' => 'pagination pagination-right',
			'member' => $account,
		));
	?>
	<?if( $DP->pagination->getPageCount() > 1 ){?>
		<div class="iDiv i01">
			<?=$gridWidget->renderPager()?>
		</div>
	<?}?>
	<div class="iDummReload i01 <?=$ins?> wDummReload" style="display:none;"></div>
	<table class="iDummReloadText i01 <?=$ins?> wDummReloadText" style="display:none;">
		<tr>
			<td valign="middle" align="center">
				<span class="iSpan i01">
					<?=Yii::t( $NSi18n, 'Loading...' )?>
				</span>
			</td>
		</tr>
	</table>
</div>
<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var idAccount = "<?=$account->id?>";
			
			ns.wEATradeAccountOrdersHistoryListWidget = wEATradeAccountOrdersHistoryListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wEATradeAccountOrdersHistoryListWidget',
				ajax: ajax,
				idAccount: idAccount,
			});
		}( window.jQuery );
	</script>
<?}?>