<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>

<div class="wExchangersWithCoursesAndReservesGridView">
<?php
$gridWidget = $this->widget('widgets.gridViews.ExchangersWithCoursesAndReservesGridViewWidget', array(
    'NSi18n' => $NSi18n,
    'ins' => $ins,
    'showTableOnEmpty' => $showOnEmpty,
    'dataProvider' => $DP,
    'ajax' => $ajax,
    'template' => '{items}',
    'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
    'pager' => array(
        'class'=> 'widgets.gridViews.base.WpPager'
    ),
));

if ($DP->pagination->getPageCount() > 1) {
    echo $gridWidget->renderPager();
}
?>
</div>