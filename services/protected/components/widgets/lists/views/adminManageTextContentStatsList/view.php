<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="wAdminManageTextContentStatsList <?=$ins?>">
	<?php
	
		$gridview = $this->widget( 'widgets.gridViews.AdminManageTextContentStatsGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'dataProvider' => $DP,
			'ajax' => true,
		));
	?>

</div>