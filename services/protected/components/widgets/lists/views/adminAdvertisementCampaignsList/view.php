<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wAdminAdvertisementCampaignsListWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wAdminAdvertisementCampaignsListWidget">
	<?
		$listWidget = $this->widget( 'widgets.gridViews.AdminAdvertisementCampaignsGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => $showOnEmpty,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'mode' => $mode,
			'selectionChanged' => $ajax ? "{$ns}.wAdminAdvertisementCampaignsListWidget.selectionChanged" : null,
			'filterURL' => $filterURL,
			'filter' => $filterModel,
		));
	?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="<?=count($listWidget->columns)?>">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		ns.wAdminAdvertisementCampaignsListWidget = wAdminAdvertisementCampaignsListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminAdvertisementCampaignsListWidget',
			ajax: ajax,
			hidden: hidden,
		});
	}( window.jQuery );
</script>