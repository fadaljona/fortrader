<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wAdminAdvertisementZonesListWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wAdminAdvertisementZonesListWidget">
	<?
		$listWidget = $this->widget( 'widgets.gridViews.AdminAdvertisementZonesGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => $showOnEmpty,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'selectionChanged' => $ajax ? "{$ns}.wAdminAdvertisementZonesListWidget.selectionChanged" : null,
		));
	?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="<?=count( $listWidget->columns )?>">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
	<div class="modal hide <?=$ins?> iModalZoneCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 id="myModalLabel"><?=Yii::t( $NSi18n, 'Zone Code' )?></h3>
		</div>
		<div class="modal-body">
			<textarea class="iZoneCode span12" rows="10"></textarea>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true"><?=Yii::t( $NSi18n, 'Close' )?></button>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var tplCode = <?=json_encode( AdvertisementZoneModel::getTplCode())?>;
		
		ns.wAdminAdvertisementZonesListWidget = wAdminAdvertisementZonesListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminAdvertisementZonesListWidget',
			ajax: ajax,
			hidden: hidden,
			tplCode: tplCode,
		});
	}( window.jQuery );
</script>