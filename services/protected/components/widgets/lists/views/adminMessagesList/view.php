<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wAdminMessagesListWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wAdminMessagesListWidget">
	<?
		$this->widget( 'widgets.gridViews.AdminMessagesGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => $showOnEmpty,
			'dataProvider' => $DP,
			'ajax' => $ajax,
			'selectionChanged' => $ajax ? "{$ns}.wAdminMessagesListWidget.selectionChanged" : null,
			'filterURL' => $filterURL,
			'filter' => $filterModel,
		));
	?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="2">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var inSearch = <?=CommonLib::boolToStr($inSearch)?>;
		
		ns.wAdminMessagesListWidget = wAdminMessagesListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminMessagesListWidget',
			ajax: ajax,
			hidden: hidden,
			inSearch: inSearch,
		});
	}( window.jQuery );
</script>