<?php
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wAdminManageTextContentListWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => Yii::t( $NSi18n,'Saving...'),
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>

<div class="wAdminManageTextContentListWidget <?=$ins?>">
	<?
		$gridview = $this->widget( 'widgets.gridViews.AdminManageTextContentGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'dataProvider' => $DP,
			'ajax' => true,
			'langs' => $langs,
			'modelFormName' => $modelFormName,
		));
	?>

</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminManageTextContentListWidget = wAdminManageTextContentListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			saveUrl: '<?=Yii::app()->createUrl('admin/manageTextContent/ajaxSave')?>'
		});
	}( window.jQuery );
</script>
