<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wContestMembersTAListWidget.js" );
	
	$jsls = Array(
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$itCurrentModel = $this->idUser === Yii::App()->user->id;
?>
<div class="iListWidget i01 wContestMembersTAListWidget iRelative">
	<?
		$gridWidget = $this->widget( 'widgets.gridViews.ContestMembersTAGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => $showOnEmpty,
			'dataProvider' => $DP,
			'template' => '{items}',
			'pagerCssClass' => 'pagination pagination-right',
			'itCurrentModel' => $itCurrentModel,
			'selectionChanged' => "{$ns}.wContestMembersTAListWidget.selectionChanged",
		));
	?>
	<?if( $DP->pagination->getPageCount() > 1 ){?>
		<div class="iDiv i01">
			<?=$gridWidget->renderPager()?>
		</div>
	<?}?>
	<table class="<?=$ins?> wTabTpl" width="100%" style="display:none;">
		<tr class="<?=$ins?> wLoading">
			<td colspan="<?=count( $gridWidget->columns )?>">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</td>
		</tr>
	</table>
	<div class="iDummReload i01 <?=$ins?> wDummReload" style="display:none;"></div>
	<table class="iDummReloadText i01 <?=$ins?> wDummReloadText" style="display:none;">
		<tr>
			<td valign="middle" align="center">
				<span class="iSpan i01">
					<?=Yii::t( $NSi18n, 'Loading...' )?>
				</span>
			</td>
		</tr>
	</table>
</div>
<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var hidden = <?=CommonLib::boolToStr($hidden)?>;
			var idUser = <?=$this->idUser?>;
			
			var ls = <?=json_encode( $jsls )?>;
			
			ns.wContestMembersTAListWidget = wContestMembersTAListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wContestMembersTAListWidget',
				hidden: hidden,
				idUser: idUser,
				ls: ls,
			});
		}( window.jQuery );
	</script>
<?}?>