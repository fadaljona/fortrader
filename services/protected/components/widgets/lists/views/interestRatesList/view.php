<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wInterestRatesListWidget.js' ) );
	}
?>

<!-- - - - - - - - - - - - - - Tabl page  1- - - - - - - - - - - - - - - - -->
<section class="section_offset_2 turn_box wInterestRatesListWidget spinner-margin position-relative">
	<div class="turn_content">
		<div class="tabl_quotes table_currency" data-table="3">
		<?php
			$gridWidget = $this->widget( 'widgets.gridViews.InterestRatesGridViewWidget', Array(
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'showTableOnEmpty' => $showOnEmpty,
				'dataProvider' => $DP,
				'ajax' => $ajax,
				'template' => '{items}',
				'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
				'pager' => array(
					'class'=> 'widgets.gridViews.base.WpPager'
				),
			));
		?>
		</div>
	</div>
	<?php
		if( $DP->pagination->getPageCount() > 1 ){
			echo $gridWidget->renderPager();
		}
	?>
</section>
<!-- - - - - - - - - - - - - - End of Tabl page 1- - - - - - - - - - - - - - - - -->


<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			var nsText = ".<?=$ns?>";
			var ins = ".<?=$ins?>";
			var ajax = <?=CommonLib::boolToStr($ajax)?>;
			var hidden = <?=CommonLib::boolToStr($hidden)?>;
			
			ns.wInterestRatesListWidget = wInterestRatesListWidgetOpen({
				ns: ns,
				ins: ins,
				selector: nsText+' .wInterestRatesListWidget',
				ajax: ajax,
				hidden: hidden,
				ajaxLoadListURL: '<?=Yii::app()->createAbsoluteUrl('interestRates/ajaxLoadInterestRatesList')?>',
			});
		}( window.jQuery );
	</script>
<?}?>