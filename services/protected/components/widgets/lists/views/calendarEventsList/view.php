<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$baseUrl = Yii::app()->baseUrl;
	
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wCalendarEventsListWidget.js" );
	$jsls = Array(
		'lDone' => 'Done',
		'lSec' => 'sec',
		'lMin' => 'min',
		'lH' => 'h',
		'lDay_ip' => 'day_ip',
		'lDay_rp' => 'day_rp',
		'lDay_mrp' => 'day_mrp',
		'lMonth_ip' => 'month_ip',
		'lMonth_rp' => 'month_rp',
		'lMonth_mrp' => 'month_mrp',
		'lYear_ip' => 'year_ip',
		'lYear_rp' => 'year_rp',
		'lYear_mrp' => 'year_mrp',
		'lDeleteConfirm' => 'Delete?',
		'lSelectEvents' => 'Select events!',
		'lLoading' => 'Loading...',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="iListWidget i01 wCalendarEventsListWidget iRelative">
	<div class="pull-left" style="margin-bottom:10px;">
		<?=CHtml::openTag( 'a', Array( 'href' => BrokerModel::getModelSingleURL( 3 )))?><img src="<?=$baseUrl?>/assets-static/images/fxpro.png" /><?=CHtml::closeTag( 'a' )?>
		&nbsp;
		<b><?=Yii::t( '*', "Supported by" )?> <?=CHtml::link( "FxPro", BrokerModel::getModelSingleURL( 3 ))?></b>
	</div>
	<?if( $period != 'soon' ){?>
		<style>
			#block_date{
				display:inline-block;
			}
			#block_date input{
				display: none;
			}
			.ui-datepicker-trigger{
				background: none repeat scroll 0 0 #e7e7e7;
				color: black;
				display: inline-block;
				margin-left: 6px;
				padding: 8px 18px;
				text-decoration: none;
				border:none;
				font-size:13px;
			}
			#ui-datepicker-div .ui-widget-header {
				background:none;
				border: 1px solid #aaa;
				color: #222;
				font-weight: bold;
			}
			#ui-datepicker-div .ui-datepicker-title span{
			 color:#444;
			}
			#ui-datepicker-div thead tr{
				border: 1px solid #c6c6c6;
			}
			#ui-datepicker-div thead tr th{
				background-color: #e6e6e6;
			}
			#ui-datepicker-div thead tr th span{
				color:#7e7e7e;
			}
			#ui-datepicker-div{
			
			}
			#ui-datepicker-div .ui-datepicker .ui-datepicker-prev span, #ui-datepicker-div .ui-datepicker .ui-datepicker-next span{
				background-position: 0 0;
				display: block;
				height: 28px;
				left: 0;
				margin-left: 0;
				margin-top: 0;
				position: absolute;
				top: 0;
				width: 28px;
			}
			#ui-datepicker-div .ui-widget-header .ui-icon {
				background-image: url("/services/assets-static/img/ui-datepicker.jpg");
			}
			#ui-datepicker-div .ui-widget-header {
				background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
				border: 0px;
				color: #222;
				font-weight: bold;
			}
			.ui-datepicker .ui-datepicker-prev span, .ui-datepicker .ui-datepicker-next span {
				background-position: 0 0;
				display: block;
				height: 28px;
				left: 0;
				margin-left: 0;
				margin-top: 0;
				position: absolute;
				top: 0;
				width: 28px;
			}
			#ui-datepicker-div .ui-datepicker .ui-datepicker-prev, #ui-datepicker-div  .ui-datepicker .ui-datepicker-next {
				height: 28px;
				position: absolute;
				top: 2px;
				width: 28px;
			}
			#ui-datepicker-div .ui-datepicker-month, #ui-datepicker-div .ui-datepicker-year {
				height: 24px;
				margin-top: -3px;
				padding: 1px;
				text-align: center;
				width: 49%;
			} 
			#ui-datepicker-div  .ui-datepicker-next .ui-icon{
				-webkit-transform: rotate(180deg); 
			    -moz-transform: rotate(180deg);
			    -ms-transform: rotate(180deg);
			    -o-transform: rotate(180deg);
			    transform: rotate(180deg);
			}
			#ui-datepicker-div .ui-state-hover{
				background:none;
				background-color:#bce3fa;
			}
			#ui-datepicker-div .ui-datepicker td {
				border: 0 none;
				padding: 0px;
			}
			#ui-datepicker-div .ui-datepicker td span, .ui-datepicker td a {
				display: block;
				padding: 0.2em;
				text-align: center;
				text-decoration: none;
			}
			#ui-datepicker-div .ui-state-active{
				background:none;
				background-color:#4ea0e4;
				color:#fff;
			}
		</style>
		<br>
		<br>
		<br>
		<div class="pull-right" style="margin-bottom:10px;">
			<? require dirname( __FILE__ )."/_filter.php"; ?>
			<div id="block_date"><input type="text" id="date" value="<?php echo $_GET["d_view"];?>"/></div>
		</div>
		<div class="pull-left">
			<b>Часовой пояс: </b>
			<?php echo CHtml::dropDownList('user_utc',UserProfileModel::getTimezone(),array(
				'-12' => '(UTC -12:00)',
				'-11' => '(UTC -11:00)',
				'-10' => '(UTC -10:00)',
				'-9'  => '(UTC -9:00)',
				'-8'  => '(UTC -8:00)',
				'-7'  => '(UTC -7:00)',
				'-6'  => '(UTC -6:00)',
				'-5'  => '(UTC -5:00)',
				'-4'  => '(UTC -4:00)',
				'-3'  => '(UTC -3:00)',
				'-2'  => '(UTC -2:00)',
				'-1'  => '(UTC -1:00)',
				'0'   => '(UTC +0:00)',
				'1'   => '(UTC +1:00)',
				'2'   => '(UTC +2:00)',
				'3'   => '(UTC +3:00)',
				'4'   => '(UTC +4:00)',
				'5'   => '(UTC +5:00)',
				'6'   => '(UTC +6:00)',
				'7'   => '(UTC +7:00)',
				'8'   => '(UTC +8:00)',
				'9'   => '(UTC +9:00)',
				'10'  => '(UTC +10:00)',
				'11'  => '(UTC +11:00)',
				'12'  => '(UTC +12:00)',
			), array(
				'style'=>'width:130px;', 
				'id'=>'utc_selector', 
				'onchange'=>'time.settz($(this).val(),1,true);', 
			)); ?>
		</div>
		<script>
			$(document).ready(function(){
				$.datepicker.regional['ru'] = {
					closeText: 'Закрыть',
					prevText: '&#x3c;Пред',
					nextText: 'След&#x3e;',
					currentText: 'Сегодня',
					monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
					'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
					'Июл','Авг','Сен','Окт','Ноя','Дек'],
					dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
					dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					dateFormat: 'dd.mm.yy',
					firstDay: 1,
					isRTL: false
				};
				$.datepicker.setDefaults($.datepicker.regional['ru']); 
				$('#date').datepicker({
				  showOn: 'both',
				  buttonText: '<?=Yii::t( '*', "Сhoice dates" )?>',
				  buttonImageOnly: false,
				  numberOfMonths: 3,
				  maxDate: '01.11.2014',
				  minDate: '01.01.2013',
				  changeMonth:true,
				  changeYear:true,
				  selectOtherMonths:true,
				  showOtherMonths:true 
				});
				$(".choice_dates").click(function(e){
					e.preventDefault();
					$(".ui-datepicker-trigger").click();
					$("#ui-datepicker-div").css("display","block");
				});
				$("#date").change(function(){
					window.location.replace('/services/calendarEvent/list?period=date_v&d_view='+$("#date").val()); 
				});
			});
		</script>
	<?}?>
	<div class="iClear"></div>
	<? require dirname( __FILE__ )."/_filter2.php"; ?>
	<div class="iClear"></div>
	<? $gridWidget->run(); ?>
	<div class="progress progress-info progress-striped active <?=$ins?> wProgress" style="display:none;">
		<div class="bar" style="width: 100%;"></div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		var ls = <?=json_encode( $jsls )?>;
		var moreDate = "<?=$moreDate?>";
		var period = "<?=$period?>";
		
		ns.wCalendarEventsListWidget = wCalendarEventsListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wCalendarEventsListWidget',
			ls: ls,
			moreDate: moreDate,
			period: period,
		});
	}( window.jQuery );
</script>
