<?php 

$amount = isset($_GET['amount']) ? $_GET['amount'] : 0;

$country_filters = isset($_GET['country_filters']) ? json_encode($_GET['country_filters']) : 0;

Yii::app()->clientScript->registerScript('filter2', "

	var filter2 = {
		amount:".$amount.",
		country_filters:".$country_filters.",
		showbtntoggle:function (showt, hidet) {
			var e = $('#filter_show_btn');
			if ( e.hasClass('active') ) {
				e.html(hidet);
				e.removeClass('active');
			}else{
				e.html(showt);
				e.addClass('active');
			}
		}, 
		all:function ( ) {
			$('.city_checkbox_form input').each(function ( ) {
				$(this).prop('checked', true);
			});
		}, 
		nothing:function ( ) {
			$('.city_checkbox_form input').each(function ( ) {
				$(this).prop('checked', false);
			});
		}, 
		reset:function ( ) {
			window.location.href = '/services/calendarEvent/list';
		}
	};

", CClientScript::POS_HEAD);

?>

<div class="iGridView i05 grid-view insCalendarEventsListWidget wCalendarEventsGridView" id="wCalendarEventsGridView">
	<form action="" class="calendar_main slider_box">
		<div class="calendar_2 position-relative">
			<span class="cal_of_events">Календарь событий</span>
			<div class="pull-right">
				<div 
					class="position-relative slider_btn active"
					onclick="filter2.showbtntoggle('<?=Yii::t( '*', 'Show Filter' )?>', '<?=Yii::t( '*', 'Hide Filter' )?>');"
				>
					<div 
						id="filter_show_btn" 
						class="iDiv i65 slider_btn_it active"
					>
						<?=Yii::t( '*', 'Show Filter' )?>
					</div>
				</div>
			</div>
			<div class="pull-right cal_box">
				<button 
					title="<?php echo Yii::t( '*', 'Reset' ); ?>"
					type="reset" 
					class="d_ib update_cal" 
					style="margin-right:3px;" 
					onclick="filter2.reset(); return false;"
				></button>
				<?php 
				/*<button 
					type="button" 
					class="d_ib ok_cal <?php if(isset($_GET['country_filters']) or isset($_GET['amount'])) echo 'active'; ?>"
				></button>
				<button type="button" class="d_ib settings_cal" data-toggle="dropdown"></button>*/
				?>
				<?php 
					/*
					<div class="dropdown-menu iDiv i66 iKeepOpen">
						<img class="triger_cal" src="/images/triger_cal.png" height="5" width="9" alt="">
						<div class="iDiv i68">
							<label>
								<input class="insCalendarEventsListWidget wFilterType" value="high" type="checkbox" name="type_high" id="type_high">
								<span class="lbl iSpan">На почту за [4] мин</span>
							</label>
						</div>
						<div class="iDiv i68">
							<label>
								<input class="insCalendarEventsListWidget wFilterType" value="medium" type="checkbox" name="type_medium" id="type_medium">
								<span class="lbl iSpan">На сайте за [5] мин</span>
							</label>
						</div>
						<div class="iDiv i68">
							<label>
								<input class="insCalendarEventsListWidget wFilterType" value="low" type="checkbox" name="type_low" id="type_low">
								<span class="lbl iSpan">По SMS за [15] мин</span>
							</label>
						</div>
					</div>*/
				?>
			</div>
		</div>
		<div class="calendar clearfix slider_content" style="display:none;">
			<div class="box1 pull-left">
				<span class="d_bl"><strong>Страна</strong></span>
				<a href="#" class="calendar_link" onclick="filter2.all(); return false;">Все</a> /
				<a href="#" class="calendar_link" onclick="filter2.nothing(); return false;"> ни одного</a>
			</div>
			<div action="" class="city_checkbox_form pt24 clearfix">
				<?php 
					$n = 1;
					foreach (CountryModel::getAllExist() as $one) {
						if ( $one->name != '' ) {
							if ( $n == 1 ) {
								echo '<div class="city_checkbox pull-left">';
							}
							if ( $n % 56 == 0 ) {
								echo '</div>';
								echo '<div class="city_checkbox pull-left">';
							}
							$chek = (isset($_GET['country_filters'][$n])) ? 'checked="checked"' : false;
							echo '<input type="checkbox" id="fiter_input'.$n.'" '.$chek.' name="country_filters['.$n.']" value="'.$one->alias.'">';
							echo '<label class="filter_label" for="fiter_input'.$n.'">';
								echo CountryModel::getIconPic($one->name, $one->alias).Yii::t( '*', $one->name ); 
							echo '</label>';
							$n++;
						}
					}
				?>
			  </div>
			</div>
			<br />
			<br />
			<div action="" class="filter_volatility_form clearfix">
					<p class="filter_volatility pull-left">
					  <label for="amount"><strong>Волатильность</strong></label>
					  <input type="hidden" id="amount" name="amount">
					</p>
					<div class="vol1 position-relative">
						<img class="position-absolute filter_vol1" src="/services/assets-static/images/filter_vol1.png" alt="">
						<img class="position-absolute filter_vol2" src="/services/assets-static/images/filter_vol2.png" alt="">
						<img class="position-absolute filter_vol3" src="/services/assets-static/images/filter_vol3.png" alt="">
						<img class="position-absolute filter_vol4" src="/services/assets-static/images/filter_vol4.png" alt="">
						<img class="position-absolute filter_vol5" src="/services/assets-static/images/filter_vol4.png" alt="">
						<img class="position-absolute filter_vol6" src="/services/assets-static/images/filter_vol4.png" alt="">
						<img class="position-absolute filter_vol7" src="/services/assets-static/images/filter_vol4.png" alt="">
						<div id="slider-range-max" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
							<?php 
							/*<div class="ui-slider-range ui-widget-header ui-corner-all ui-slider-range-max" style="width: 88.88888888888889%;"></div>*/
							?>
							<span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 11.11111111111111%;"></span>
						</div>
					</div>
			</div>
			
			<script>
			  $(function() {
				$( "#slider-range-max" ).slider({
				  range: "max",
				  min: 0,
				  max: 3,
				  value: <?php echo isset($_GET['amount']) ? $_GET['amount'] : 0; ?>,
				  slide: function( event, ui ) {
					$( "#amount" ).val( ui.value );
				  }
				});
				$( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );
			  });
			</script>
			<div class="text-center">
				<button class="filter_button">Отфильтровать</button>
			</div>
		</div>
	</form>
	<script>
		$(document).ready(function(){
		  $('.slider_box').each(function(){
			var _this = $(this);
			$(_this).find('.slider_btn').on('click',function(){
			  $(_this).find('.slider_content').slideToggle();
			  $(_this).find('.slider_btn').toggleClass('active');
			});
		  });
		});
	</script>
</div>
