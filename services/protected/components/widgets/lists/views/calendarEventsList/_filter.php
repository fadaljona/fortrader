<?
	$periods = Array(
		"forthcoming" => "Forthcoming",	
		"today" => "Today",
		"tomorrow" => "Tomorrow",
		"this_week" => "This week",
		"next_week" => "Next week",
	);
?>
<?foreach( $periods as $key=>$title ){?>
	<?
		$params = $key == $period ? Array() : Array( 'period' => $key );
		$url = $this->controller->createURL( "", $params );
		$class = $key == $period ? "active" : "";
		$dop_class= $key == "choice_dates" ? "choice_dates" : "";
	?>
	<a href="<?=$url?>" class="iA i21 <?=$class?> <?=$dop_class?>">
		<?=Yii::t( '*', $title )?>
	</a>
<?}?>
