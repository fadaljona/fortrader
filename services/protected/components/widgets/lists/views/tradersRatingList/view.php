<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="<?=$ins?>">
	<div class="turn_content">
	<?
		$gridWidget = $this->widget( 'widgets.gridViews.TradersRatingForecastGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => 1,
			'dataProvider' => $DP,
			'ajax' => false,
			'template' => '{items}',
			'pagerCssClass' => 'navigation_box clearfix margin-top-10',
			'pager' => array(
				'class'=> 'widgets.gridViews.base.WpPager'
			),
		));
	?>
	</div>
	<?if( $DP->pagination->getPageCount() > 1 ){?>
		<?=$gridWidget->renderPager()?>
	<?}?>
</div>