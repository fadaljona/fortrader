<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wInterestRatesReviewsWidget.js' ), CClientScript::POS_END );
?>

<section class="section_offset <?=$ins?> position-relative spinner-margin">
	<hr class="separator2">
	<h3><?=Yii::t($NSi18n, 'Reviews of meetings')?></h3>
	<hr>
	<div class="post_box load-more-wrapper">
		<?php 
			$cacheKey = 'interestRatesReviewsBlock' . implode( '_', $postIds ) . 'num' . $num;
			if( !Yii::app()->cache->get( $cacheKey ) ){
				ob_start();
				require_once Yii::App()->params['wpThemePath'].'/templates/interestRatesReviews.php'; 
				$blockContent = ob_get_clean();
				Yii::app()->cache->set( $cacheKey, $blockContent, 60*60 * 24);
			}
			echo Yii::app()->cache->get( $cacheKey );
		?>
	</div>
	<?php if( $total > $num ){ ?>
	<a href="#" class="load_btn chench_btn" data-text="<?=Yii::t($NSi18n, 'Show more reviews')?>" data-shot-text="<?=Yii::t($NSi18n, 'Show more')?>"></a>
	<?php } ?>
</section>


<?php

Yii::app()->clientScript->registerScript('informersSettingsJs', '

	!function( $ ) {
		var ns = ' . $ns . ';
		var nsText = ".' . $ns . '";
		var ins = ".' . $ins . '";
		
		ns.wInterestRatesReviewsWidget = wInterestRatesReviewsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+" " + ins,
			ajaxLoadListURL: "' . Yii::app()->createAbsoluteUrl("interestRates/ajaxLoadInterestRatesReviews") . '",
			lastDate: "' . $lastDate . '",
			lastId: ' . $lastId . ',
			num: ' . $num . ',
		});
	}( window.jQuery );

', CClientScript::POS_END);

?>