<?php 
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wQuotesChartListWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$checkedQuotes = (string)Yii::app()->request->cookies['checkedQuotesOnChartsList' . DecommentsPostsModel::getCatLangSuffix()];
	if( $checkedQuotes ){
		$checkedQuotes = explode(',', $checkedQuotes);
	}
?>
<div class="<?=$ins?> wQuotesChartListWidget">
	<?php foreach( $quotesCats as $quotesCat ){ ?>
		<!-- - - - - - - - - - - - - - Tabl Quotes - - - - - - - - - - - - - - - - -->
		<section class="section_offset turn_box">
			<div class="tabl_quotes_header clearfix">
				<div class="tabl_quotes_btn alignright nav_buttons">
					<a href="javascript:;" class="options_btn">
						<i class="fa fa-sliders"></i>
						<span class="tooltip"><?=Yii::t( "*",'Settings') ?></span>
					</a>
					<a href="javascript:;" class="turn_btn"><span class="tooltip"></span></a>
				</div>
				<h4 class="tabl_quotes_title">
					<?=$quotesCat->name?>
					<?php if($quotesCat->desc){?><br /><span><?=$quotesCat->desc?></span><?php }?>
				</h4>
				<div class="options_box">
					<h6 class="options_title"><?=Yii::t( "*",'Select symbols') ?></h6>
					<div class="clearfix">
						<?php 
							$graphsToShowArr = array();
							foreach( $quotesCat->symbols as $symbol ){	
								$condition = false;
								if( $checkedQuotes ){
									$condition = in_array($symbol->name, $checkedQuotes);
								}else{
									$condition = $symbol->toChartList == 1;
								}
								if( $condition )$graphsToShowArr[] = $symbol;
								
						?>
							<div class="options_item">
								<input type="checkbox" value="<?=$symbol->name?>" data-id="<?=$symbol->id?>" id="option<?=$symbol->name;?>" <?=$condition ? 'checked' : '' ?>>
								<label for="option<?=$symbol->name;?>"><?=$symbol->nalias ? $symbol->nalias : $symbol->name;?></label>
							</div>                                       
						<?php }?>						
					</div>
				</div>
			</div>
			<div class="turn_content all_graph_box clearfix">
			<?php 
				foreach($graphsToShowArr as $symbol){
					$this->widget('widgets.QuoteOneChartWidget',Array( 'model' => $symbol, 'ajax' => false ));
				}
			?>
			</div>
		</section>
		<!-- - - - - - - - - - - - - - End of Tabl Quotes - - - - - - - - - - - - - - - - -->		
	<?php } ?>
</div>


<script>
!function( $ ) {
	var ns = ".<?=$ns?>";
	var nsText = ".<?=$ns?>";

	ns.wQuotesChartListWidget = wQuotesChartListWidgetOpen({
		ns: ns,
		selector: nsText+' .<?=$ins?>',
		ajaxSubmitURL: '<?=Yii::app()->createAbsoluteUrl('quotesNew/ajaxLoadOneChart');?>',
		lang: '<?=DecommentsPostsModel::getCatLangSuffix()?>',
	});
}( window.jQuery );
</script>