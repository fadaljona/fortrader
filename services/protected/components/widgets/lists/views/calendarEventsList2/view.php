<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();

	$baseUrl = Yii::app()->baseUrl;
	
	$cs=Yii::app()->getClientScript();
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js');
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');

	$ionRangeSliderBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/ion.rangeSlider' );
	$cs->registerScriptFile($ionRangeSliderBaseUrl.'/js/ion-rangeSlider/ion.rangeSlider.min.js');
	$cs->registerCssFile($ionRangeSliderBaseUrl.'/css/ion.rangeSlider.css');
	
	new JsTrans('app',Yii::app()->language);
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wCalendarEventsListWidget.js' ) );
	
	$cs->registerCss('dataPicker', '#ui-datepicker-div{z-index:100 !important}');
	$cs->registerCss('jqSelectbox', ".timezone_select .jq-selectbox__select-text:before{ content: '".Yii::t($NSi18n, 'Timezone')." '; }");
	
	$jsls = Array(
		'lDone' => 'Done',
		'lSec' => 'sec',
		'lMin' => 'min',
		'lH' => 'h',
		'lDay_ip' => 'day_ip',
		'lDay_rp' => 'day_rp',
		'lDay_mrp' => 'day_mrp',
		'lMonth_ip' => 'month_ip',
		'lMonth_rp' => 'month_rp',
		'lMonth_mrp' => 'month_mrp',
		'lYear_ip' => 'year_ip',
		'lYear_rp' => 'year_rp',
		'lYear_mrp' => 'year_mrp',
		'lDeleteConfirm' => 'Delete?',
		'lSelectEvents' => 'Select events!',
		'lLoading' => 'Loading...',
		'lFilteredByCountry' => 'Filtered by countries',
		'lFilteredByVolatility' => 'Filtered by volatility',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);

?>
<div class="<?=$ins?> spinner-margin position-relative">

	<hr class="separator4">
	<form class="calendar_form">
		<div class="calendar_form_inner clearfix">
			<div class="calendar_form_col_30">
			<?php 
			$zonesList = CommonLib::getTimeZonesWithOffsets();
			$zonesListForSelect = array();
			foreach( $zonesList as $zoneOffset => $zoneName ){
				$zoneSign = '';
				if( $zoneOffset > 0 ) $zoneSign = '+';
				if( strpos($zoneOffset, '.') !== false ){
					$minutePos = strpos($zoneOffset, '.');
					$hours = substr($zoneOffset, 0, $minutePos);
					$mins = 60 * abs($zoneOffset+$hours*-1);
					$zoneUtcVal = "(UTC " . $zoneSign . $hours . ":".$mins.")";
				}else{
					$zoneUtcVal = "(UTC " . $zoneSign . $zoneOffset . ":00)";
				}
				$zonesListForSelect[$zoneOffset] = $zoneUtcVal;
			}
			echo CHtml::dropDownList('user_utc',UserProfileModel::getTimezone('cookie'),$zonesListForSelect, array( 'class'=>'select_type2 timezone_select', )); 
			?>	
			</div>
			<div class="calendar_form_col_30">
				<?php
				$cJuiDatePickerLang = Yii::app()->language;
				if( $cJuiDatePickerLang == 'en_us' ) $cJuiDatePickerLang = 'en';
				$this->widget('zii.widgets.jui.CJuiDatePicker',array(
					'name'=>'calendarSelectDate',
					'value' => @$_GET["d_view"],
					'language' => $cJuiDatePickerLang,
					'options'=>array(
						'dateFormat' => 'dd.mm.yy',
						'numberOfMonths' => 1,
						'maxDate' => date('d.m.Y', strtotime("+1 month", time()) ),
						'minDate' => '19.05.2014',
						'changeMonth' => true,
						'changeYear' => true,
						'selectOtherMonths' => true,
						'showOtherMonths' => true,
						'buttonImageOnly' => false,
						'showOn' => 'both',
						'buttonText' => Yii::t( '*', "Сhoice dates" ),
					),
					'htmlOptions'=>array(
					),
				));
				?>
			</div>
			<div class="calendar_form_col_30">
				<a href="javascript:;" class="filter_btn showCountrySliderFilter"><?=Yii::t( '*', 'Show Filter' )?></a>
			</div>
			<div class="calendar_form_col_10">
				<a href="javascript:;" class="calendar_btn_reload" title="<?php echo Yii::t( '*', 'Reset' ); ?>"><i class="fa fa-refresh" aria-hidden="true"></i></a>
			</div>
		</div>
	</form>
	<div class="showCalendarFilterData"></div>

	<ul class="buttons_links2 clearfix">
	<?php 
		$periods = Array(
			"forthcoming" => "Forthcoming",	
			"today" => "Today",
			"tomorrow" => "Tomorrow",
			"this_week" => "This week",
			"next_week" => "Next week",
		);
		foreach( $periods as $key=>$title ){
			$params = $key == 'today' ? Array() : Array( 'period' => $key );
			$url = $this->controller->createURL( "", $params );
			$class = $key == $period ? "active" : "";
			echo CHtml::tag('li', array('class' => $class ),
				CHtml::link( CHtml::tag('span', array(), Yii::t( '*', $title ) ), $url)
			);
		}
	?>
	</ul>
	<div class="section_offset_2 turn_box">
		<div class="section_offset_2">
			<div class="calendar_info clearfix">
				<h3 class="calendar_info_title"><?=Yii::t($NSi18n, 'Calendar of events')?></h3>
				<time datetime="2016-12-5" class="date_calendar"><?=$rangeTitle?></time>
			</div>
			<? $gridWidget->run(); ?>
		</div>
	</div>
	
	
	<div class="country_wrapp">
		<div class="country_top clearfix">
			<h5 class="country_title"><?=Yii::t($NSi18n, 'Country')?></h5>
			<div class="country_btn_box clearfix">
				<button class="country_btn reset"><?=Yii::t($NSi18n, 'Reset - All countries')?></button>
				<?php /*<button class="country_btn check"><?=Yii::t($NSi18n, 'All')?></button>*/?>
			</div>
		</div>
		<div class="country_content clearfix">
		<?php
			$selectedVolatility = 0;
			if( isset(Yii::app()->request->cookies['ftVolatility']) && Yii::app()->request->cookies['ftVolatility']->value ){
				$selectedVolatility = Yii::app()->request->cookies['ftVolatility']->value;
			}
			$countries = CountryModel::getAllExist();
			$countriesCount = count( $countries );
			if( $countriesCount % 3 != 0 ) $countriesCount += 3;
			$countForColumn = ($countriesCount - $countriesCount % 3) / 3;
			$i=1;
			$selectedCountries = array();
			if( isset(Yii::app()->request->cookies['ftCalendarCountries']) && Yii::app()->request->cookies['ftCalendarCountries']->value ){
				$selectedCountries = explode(',', Yii::app()->request->cookies['ftCalendarCountries']->value);
			}
			foreach( $countries as $index => $country ){
				if( $i % $countForColumn == 1 ) echo '<div class="country_col">';
					$checked = '';
					if( in_array( $index, $selectedCountries ) ) $checked = 'checked';
					echo '<input type="checkbox" id="country'.$i.'" name="country_filters[]" value="'.$index.'" '.$checked.'>';
					echo '<label for="country'.$i.'" class="square_input"><i></i>';
						echo '<span class="checkbox_img"><img src="'.$country->img16.'" alt="'.$country->name.'"></span>' . $country->name;
					echo '</label>';
				
				if( $i % $countForColumn == 0 ) echo '</div>';
				$i++;
			}
			if( $i % $countForColumn != 1 ) echo '</div>';
		?>
		</div>
		<div class="country_bottom">
			<h5 class="country_bottom_title"><?=Yii::t( $NSi18n, 'Volatility')?></h5>
			<input type="text" class="range_slider1" />
		</div>
		<a data-shot-text="<?=Yii::t( $NSi18n, 'Filter')?>" data-text="<?=Yii::t( $NSi18n, 'Filter')?>" class="load_btn chench_btn" href="#"></a>
	</div>


</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		var ls = <?=json_encode( $jsls )?>;
		var moreDate = "<?=$moreDate?>";
		var period = "<?=$period?>";
		
		ns.wCalendarEventsListWidget = wCalendarEventsListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: '.<?=$ins?>',
			ls: ls,
			moreDate: moreDate,
			period: period,
			ajaxLoadListURL: '<?=Yii::app()->createUrl('calendarEvent/ajaxLoadList')?>',
			ajaxLoadUpdatedList: '<?=Yii::app()->createUrl('calendarEvent/ajaxLoadUpdatedList')?>',
			ajaxLoadRowsURL: '<?=Yii::app()->createUrl('calendarEvent/ajaxLoadRows')?>',
			dayUrl: '<?=Yii::app()->createUrl('calendarEvent/list', array('d_view' => 'date_v') )?>',
			countriesFilter: <?=json_encode($countries)?>,
			selectedVolatility: <?=$selectedVolatility?>,
			d_view: '<?=isset($_GET["d_view"]) ? $_GET["d_view"] : ''?>',
		});
	}( window.jQuery );
</script>
