<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminUserBalancesListWidget extends WidgetBase {
		const modelName = 'UserModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $filterURL;
		public $filterModel;
		public $inSearch;
		public $mode;
		private function getMode() {
			if( !$this->mode ) {
				$this->mode = Yii::App()->user->checkAccess( 'userBalanceControl' ) ? 'Admin' : 'User';
			}
			return $this->mode;
		}
		private function createDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'order' => '`t`.`createdDT` DESC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		private function getFilterURL() {
			return $this->filterURL;
		}
		private function getFilterModel() {
			return $this->filterModel;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'filterURL' => $this->getFilterURL(),
				'filterModel' => $this->getFilterModel(),
				'inSearch' => $this->getInSearch(),
				'mode' => $this->getMode(),
			));
		}
	}

?>