<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class UserActivitiesListWidget extends WidgetBase {
		public $idUser;
		private $DP;
		public $showOnEmpty = false;
		public $hidden = false;
		private function getDP() {
			if( !$this->DP ) {
				$this->DP = new CActiveDataProvider( 'UserActivityModel', Array(
					'criteria' => Array(
						'with' => Array( 'user', 'currentLanguageI18N' ),
						'condition' => " `t`.`idUser` = :idUser ",
						'order' => " `t`.`createdDT` DESC ",
						'params' => Array(
							':idUser' => $this->idUser,
						),
					),
					'pagination' => $this->getDPPagination(),
				));
				$this->DP->pagination->pageVar = 'userActivitiesPage';
			}
			return $this->DP;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$dp = $this->getDP();
			
			if( !$dp->getTotalItemCount() ) return false;
			
			$this->render( "{$class}/view", Array(
				'DP' => $dp,
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
			));
		}
	}

?>