<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberStatsListWidget extends WidgetBase {
		const modelName = 'ContestMemberModel';
		public $DP;
		public $ajax = false;
		public $idMember;
		private function getIDMember() {
			$idMember = $this->idMember;
			if( !$idMember ) $idMember = (int)@$_GET['id'];
			return $idMember;
		}
		private function createDP() {
			$idMember = $this->getIDMember();
			$this->DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'select' => ' id ',
					'with' => Array( 
						'stats' => Array(
							'select' => ' equity, profit, deposit, withdrawals, countTrades ',
						),
					),
					'condition' => "
						`t`.`id` = :idMember
					",
					'limit' => 1,
					'params' => Array(
						':idMember' => $idMember,
					),
				),
				'pagination' => false,
			));
			return $this->DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		protected function createGridViewWidget() {
			$gridWidget = $this->createWidget( 'widgets.gridViews.ContestMemberStatsGridViewWidget', Array(
				'NSi18n' => $this->getNSi18n(),
				'ins' => $this->getINS(),
				'dataProvider' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'template' => '{items}',
			));
			return $gridWidget;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
			));
		}
		function renderRow() {
			$gridWidget = $this->createGridViewWidget();
			$gridWidget->renderTableRow( 0 );
		}
	}

?>