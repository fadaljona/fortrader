<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminCurrencyRatesConverterlogListWidget extends WidgetBase {
		const modelName = 'CurrencyRatesConvertHistoryModel';
		public $DP;
		public $startDate;
		public $endDate;
		private $inSearch = false;

		private function setInSearch( $inSearch = true ) {
			$this->inSearch = $inSearch;
		}
		private function getInSearch() {
			return $this->inSearch;
		}
		private function createDP() {
			$c = new CDbCriteria();

			if( $this->startDate && $this->endDate ){
				$c->addCondition( "
					`t`.`createdDT` >= :startDate AND `t`.`createdDT` <= :endDate
				");
				$c->params[':startDate'] = $this->startDate;
				$c->params[':endDate'] = date('Y-m-d H:i:s', strtotime( $this->endDate ) + 60*60*24);
				$this->setInSearch();
			}
			$c->with = array('convert');
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => array(
					'pageSize' => 50,
					'pageVar' => 'page',
				),
				'sort' => array(
					'sortVar' => 'sort',
					'attributes' => array(
						'idConvert' => array(
							'default'=>'desc',
						),
						'ip' => array(
							'default'=>'desc',
						),
						'`t`.`createdDT`' => array(
							'default'=>'desc',
						),
					),
					'defaultOrder'=>array(
						'`t`.`createdDT`'=>CSort::SORT_DESC,
					)
				)
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'inSearch' => $this->getInSearch(),
				'startDate' => $this->startDate,
				'endDate' => $this->endDate
			));
		}
	}

?>