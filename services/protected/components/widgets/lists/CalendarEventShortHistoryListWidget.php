<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CalendarEventShortHistoryListWidget extends WidgetBase {
		const modelName = 'CalendarEvent2ValueModel';
		public $model;
		public $dt = false;
		public $id = false;
		private $DP = false;
		
		private function getDP(){
			if( !$this->DP ){
				$this->createDP();
			}
			return $this->DP;
		}
		private function createDP(){
			$c = new CDbCriteria();
			
			$idParam = $this->model->indicator_id;
			if( $this->id ) $idParam = $this->id;
			
			$c->addCondition(" `t`.`fact_value` <> '' AND `t`.`fact_value` IS NOT NULL AND `t`.`indicator_id` = :indicatorId ");
			$c->params[':indicatorId'] = $idParam;
			
			if( $this->dt ){
				$c->addCondition(" `t`.`dt` < :dt ");
				$c->params[':dt'] = $this->dt;
			}
			
			$c->order = " `t`.`dt` DESC ";
			
			$this->DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
		}
		
		private function createGridWidget() {
			$ns = $this->getNS();
			$NSi18n = $this->getNSi18n();
			$ins = $this->getINS();
			
			$DP = $this->getDP();
			
			$gridWidget = $this->createWidget( 'widgets.gridViews.CalendarEventsShortHistoryGridViewWidget', Array(
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'showTableOnEmpty' => true,
				'dataProvider' => $DP,
				'template' => '{items}',
			));
			return $gridWidget;
		}
		function getTotalItemCount(){
			return $this->getDP()->totalItemCount;
		}
		function renderTableBody() {
			$gridWidget = $this->createGridWidget();
			$gridWidget->renderTableBodyWithoutBody();
		}

		function run() {
			$class = $this->getCleanClassName();
			
			$pagination = $this->getDPPagination();
			
			$this->render( "{$class}/view", Array(
				'gridWidget' => $this->createGridWidget(),
				'DP' => $this->getDP(),
				'pageSize' => $pagination['pageSize'],
			));
		}
	}

?>
