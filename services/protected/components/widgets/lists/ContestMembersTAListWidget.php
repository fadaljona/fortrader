<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMembersTAListWidget extends WidgetBase {
		const model = 'ContestMemberModel';
		public $idUser;
		public $DP;
		public $showOnEmpty = false;
		public $hidden = false;
		private function getDP() {
			if( !$this->DP ) {
				$this->DP = new CActiveDataProvider( self::model, Array(
					'criteria' => Array(
						'with' => Array( 'contest', 'contest.currentLanguageI18N', 'contest.i18ns', 'stats' ),
						'condition' => " `t`.`idUser` = :idUser ",
						'params' => Array(
							':idUser' => $this->idUser,
						),
						'order' => ' `t`.`createdDT` DESC, `t`.`id` DESC ',
					),
					'pagination' => $this->getDPPagination(),
				));
				//$this->DP->pagination->pageSize = 2;
				$this->DP->pagination->pageVar = 'membersPage';
			}
			return $this->DP;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
			));
		}
	}

?>