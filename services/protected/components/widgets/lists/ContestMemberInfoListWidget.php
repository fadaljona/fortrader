<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberInfoListWidget extends WidgetBase {
		const modelName = 'ContestMemberModel';
		public $DP;
		public $ajax = false;
		public $idMember;
		private function getIDMember() {
			$idMember = $this->idMember;
			if( !$idMember ) $idMember = (int)@$_GET['id'];
			return $idMember;
		}
		private function createDP() {
			$idMember = $this->getIDMember();
			$this->DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'select' => ' idContest, idUser, accountNumber, status ',
					'with' => Array( 
						'user' => Array(
							'select' => ' user_login, user_nicename, display_name, firstName, lastName ',
						),
						'server' => Array( 
							'select' => ' id ',
						),
						'server.broker' => Array( 
							'select' => ' id ',
						),
						'server.broker.currentLanguageI18N' => Array(),
						'server.broker.i18ns' => Array(),
						'stats' => Array(
							'select' => Array( 
								' balance ',
								' IF( `stats`.`balance` IS NULL, NULL, `place` ) AS place ',
								' gain ',
								' drowMax ',
								' countTrades ',
								' countOpen ',
								' last_error_code ',
								' trueDT ',
							),
						),
					),
					'condition' => "
						`t`.`id` = :idMember
					",
					'limit' => 1,
					'params' => Array(
						':idMember' => $idMember,
					),
				),
				'pagination' => false,
			));
			return $this->DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		protected function createGridViewWidget() {
			$DP = $this->getDP();
			$member = @$DP->data[0];
			$itCurrentModel = $member && $member->idUser === Yii::App()->user->id;
			$gridWidget = $this->createWidget( 'widgets.gridViews.ContestMemberInfoGridViewWidget', Array(
				'NSi18n' => $this->getNSi18n(),
				'ins' => $this->getINS(),
				'dataProvider' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'template' => '{items}',
				'itCurrentModel' => $itCurrentModel,
			));
			return $gridWidget;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
			));
		}
		function renderRow() {
			$gridWidget = $this->createGridViewWidget();
			$gridWidget->renderTableRow( 0 );
		}
	}

?>