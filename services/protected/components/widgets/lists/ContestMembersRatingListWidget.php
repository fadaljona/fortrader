<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMembersRatingListWidget extends WidgetBase {
		const modelName = 'ContestMemberModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		private function createDP() {
			$DP = new ContestMembersRatingDP( self::modelName, Array(
				'criteria' => Array(
					'select' => Array( 
						" `t`.`id` ",
						" SUM( `winer`.`prize` ) AS sumContestPrizes ",
					),
					'with' => Array( 
						'winer' => Array(
							'select' => false,
						),
						'user' => Array(
							'select' => " ID, user_login, user_nicename, display_name, firstName, lastName ",
						),
						'user.country' => Array(
							'select' => " id, name, alias ",
						),
						'user.memberContests' => Array(
							'together' => false,
							'with' => Array(
								'winer' => Array(
									'with' => Array(
										'contest' => Array(
											'with' => Array(
												'i18ns' => Array(
													'together' => false,
												),
												'currentLanguageI18N',
											),
										),
									),
								),
							),
						),
					),
					'group' => " `t`.`idUser` ",
					'condition' => " `user`.`ID` IS NOT NULL ",
					'order' => " `sumContestPrizes` DESC, `user`.`user_registered` DESC ",
				),
				'pagination' => $this->getDPPagination(),
			));
			$DP->pagination->pageVar = "membersPage";
			//$DP->pagination->pageSize = 1;
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'list';
			}
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
			));
		}
	}
	
	class ContestMembersRatingDP extends CActiveDataProvider {
		protected function calculateTotalItemCount(){
			return Yii::App()->db->createCommand("
				SELECT			COUNT( DISTINCT( `idUser` ))
				FROM 			`{{contest_member}}`
			")->queryScalar();
		}
	}

?>