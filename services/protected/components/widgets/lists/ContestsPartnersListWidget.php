<?
	Yii::import( 'components.widgets.base.WidgetBase' );

	final class ContestsPartnersListWidget extends WidgetBase {
		const modelName = 'ContestForeignModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $label;
		private function createDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'condition' => " `t`.`approved` = 1 ",
					'with' => Array( 'currentLanguageI18N', 'i18ns' ),
					'order' => ' `t`.`begin` DESC ',
				),
				'pagination' => $this->getDPPagination(),
			));
			$DP->pagination->pageVar = "contestsPage";
			$DP->pagination->pageSize = 5;
			if( Yii::App()->request->isAjaxRequest ){
				$DP->pagination->route = 'list';
			}
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				'label' => $this->label
			));
		}
	}

?>