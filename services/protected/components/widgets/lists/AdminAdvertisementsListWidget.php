<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminAdvertisementsListWidget extends WidgetBase {
		const modelName = 'AdvertisementModel';
		public $DP;
		public $ajax = false;
		public $showOnEmpty = false;
		public $hidden = false;
		public $mode = 'User';
		public $type;
		public $sortField;
		public $sortType;
		private function createDP() {
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'order' => '`t`.`createdDT` DESC',
				),
				'pagination' => $this->getDPPagination(),
			));
			return $DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getShowOnEmpty() {
			return $this->showOnEmpty;
		}
		protected function getHidden() {
			return $this->hidden;
		}
		private function getMode() {
			return $this->mode;
		}
		private function getType() {
			return $this->type;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'DP' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'showOnEmpty' => $this->getShowOnEmpty(),
				'hidden' => $this->getHidden(),
				'mode' => $this->getMode(),
				'type' => $this->getType(),
				'sortField' => $this->sortField,
				'sortType' => $this->sortType,
			));
		}
	}

?>