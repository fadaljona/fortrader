<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EATradeAccountInfoListWidget extends WidgetBase {
		const modelName = 'EATradeAccountModel';
		public $DP;
		public $ajax = false;
		public $idAccount;
		private function getIDAccount() {
			$idAccount = $this->idAccount;
			if( !$idAccount ) $idAccount = (int)@$_GET['id'];
			return $idAccount;
		}
		private function createDP() {
			$idAccount = $this->getIDAccount();
			$this->DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => Array(
					'with' => Array( 
						'tradeAccount' => Array(
							'with' => Array(
								'stats' => Array(),
							),
						),
					),
					'condition' => "
						`t`.`id` = :idAccount
					",
					'limit' => 1,
					'params' => Array(
						':idAccount' => $idAccount,
					),
				),
				'pagination' => false,
			));
			return $this->DP;
		}
		private function getDP() {
			return $this->DP ? $this->DP : $this->createDP();
		}
		private function getAjax() {
			return $this->ajax;
		}
		protected function createGridViewWidget() {
			$DP = $this->getDP();
			$gridWidget = $this->createWidget( 'widgets.gridViews.EATradeAccountInfoGridViewWidget', Array(
				'NSi18n' => $this->getNSi18n(),
				'ins' => $this->getINS(),
				'dataProvider' => $this->getDP(),
				'ajax' => $this->getAjax(),
				'template' => '{items}',
			));
			return $gridWidget;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->getAjax(),
			));
		}
		function renderRow() {
			$gridWidget = $this->createGridViewWidget();
			$gridWidget->renderTableRow( 0 );
		}
	}

?>