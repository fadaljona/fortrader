<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.UserMessageFormModel' );
	
	final class EAUserRecentActivitiesWidget extends WidgetBase {
		public $instance = 'list';
		public $ajax = true;
		public $limitActivities = 5;
		public $idLastActivity;
		private function getIDLastActivity() {
			if( $this->idLastActivity ) return $this->idLastActivity;
			$id = (int)@$_GET['idLastActivity'];
			return $id ? $id : null;
		}
		private function getCriteriaActivities( $forCount = false ) {
			$c = new CDbCriteria();
			
			if( !$forCount ) {
				$c->with[ 'currentLanguageI18N' ] = Array();
				$c->with[ 'i18ns' ] = Array();
				$c->with[ 'user' ] = Array(
					'select' => Array( 'ID', 'user_login', 'user_nicename', 'display_name', 'firstName', 'lastName' ),
				);
				$c->with[ 'user.avatar' ] = Array(
					'select' => Array( 'idUser', 'thumbPath' ),
				);
			}
			
			$idLastActivity = $this->getIDLastActivity();			
			if( $idLastActivity ) {
				$c->addCondition( " `t`.`id` < :idLastActivity " );
				$c->params[ ':idLastActivity' ] = $idLastActivity;
			}
			
			if( !$forCount ) {
				$c->order = " `t`.`id` DESC ";
				$c->limit = $this->limitActivities;
			}
			
			if( $this->instance == 'list' ) {
				$c->addInCondition( '`t`.`type`', Array( 'create_ea_review','create_ea_mark','download_ea_version','create_ea','create_ea_version','create_ea_statement' ));
			}
			if( $this->instance == 'laboratory' ) {
				$c->addCondition( " 
					`t`.`type` IN ( 'create_ea_review', 'create_ea_mark', 'create_ea' )
					AND EXISTS (
						SELECT 	1
						FROM	`{{ea}}`
						WHERE	`{{ea}}`.`id` = `t`.`idObject`
							AND FIND_IN_SET( 'LAB', `{{ea}}`.`params` )
						LIMIT	1
					)
					OR `t`.`type` IN ( 'download_ea_version', 'create_ea_version' )
					AND EXISTS (
						SELECT 	1
						FROM	`{{ea}}`
						WHERE	`{{ea}}`.`id` = ( 
									SELECT 	`{{ea_version}}`.`idEA` 
									FROM 	`{{ea_version}}` 
									WHERE   `{{ea_version}}`.`id` = `t`.`idObject`
									LIMIT	1
								)
							AND FIND_IN_SET( 'LAB', `{{ea}}`.`params` )
						LIMIT	1
					)
					OR `t`.`type` IN ( 'create_ea_statement' )
					AND EXISTS (
						SELECT 	1
						FROM	`{{ea}}`
						WHERE	`{{ea}}`.`id` = ( 
									SELECT 	`{{ea_statement}}`.`idEA` 
									FROM 	`{{ea_statement}}` 
									WHERE   `{{ea_statement}}`.`id` = `t`.`idObject`
									LIMIT	1
								)
							AND FIND_IN_SET( 'LAB', `{{ea}}`.`params` )
						LIMIT	1
					)
				" );
			}
			
			return $c;
		}
		private function getActivities() {
			$c = $this->getCriteriaActivities();
			$activities = UserActivityModel::model()->findAll( $c );
			return $activities;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'instance' => $this->instance,
				'ajax' => $this->ajax,
			));
		}
		function renderActivities() {
			$activities = $this->getActivities();
			foreach( $activities as $activity ){
				$this->renderActivity( $activity );
			}
		}
		function renderActivity( $activity ) {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/activity", Array(
				'activity' => $activity,
			));
			$this->idLastActivity = $activity->id;
		}
		function issetMoreActivities() {
			$c = $this->getCriteriaActivities( true );
			$exists = UserActivityModel::model()->exists( $c );
			return $exists;
		}
	}

?>