<?php if($period){ ?>	
	<div class="exchange_rates_box">
		<div class="exchange_rates_h"><strong><?=Yii::app()->dateFormatter->format( 'd MMMM yyyy',strtotime($minData['date']) )?></strong></div>
		<div class="exchange_rates_ins">
			<span><strong><?=number_format( $minData['value'], CommonLib::getDecimalPlaces( $minData['value'] ), ',', ' ' )?></strong></span>
			<p><?=Yii::t( $NSi18n, 'Rubles for' )?> <?=$minData['nominal']?></p>
		</div>
	</div>
	<div class="exchange_rates_box">
		<div class="exchange_rates_h"><strong><?=Yii::app()->dateFormatter->format( 'd MMMM yyyy',strtotime($maxData['date']) )?></strong></div>
		<div class="exchange_rates_ins">
			<span><strong><?=number_format( $maxData['value'], CommonLib::getDecimalPlaces( $maxData['value'] ), ',', ' ' )?></strong></span>
			<p><?=Yii::t( $NSi18n, 'Rubles for' )?> <?=$maxData['nominal']?></p>
		</div>
	</div>
<?php }else{ ?>
	<div class="exchange_rates_box">
		<div class="exchange_rates_h"><strong><?=Yii::app()->dateFormatter->format( 'd MMMM yyyy',strtotime( $dateStr ) )?></strong></div>
		<div class="exchange_rates_ins">
			<span><strong><?=number_format( $dayDataModel->value, CommonLib::getDecimalPlaces( $dayDataModel->value), ',', ' ' )?></strong></span>
			<p><?=Yii::t( $NSi18n, 'Rubles for' )?> <?=$dayDataModel->nominal?></p>
		</div>
	</div>
<?php } ?>