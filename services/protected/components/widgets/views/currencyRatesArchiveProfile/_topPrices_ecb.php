<?php 
$defaultCurrency = CurrencyRatesModel::getDefaultCurrency('ecb');
if($period){ 
	$minValue =  $minData['value'];
	$maxValue =  $maxData['value'];
?>	
	<div class="exchange_rates_box">
		<div class="exchange_rates_h"><strong><?=Yii::app()->dateFormatter->format( 'd MMMM yyyy',strtotime($minData['date']) )?></strong></div>
		<div class="exchange_rates_ins">
			<span><strong><?=number_format( $minValue, CommonLib::getDecimalPlaces( $minValue ), ',', ' ' )?></strong></span>
			<p><?=$model->code?> <?=Yii::t( $NSi18n, 'forOneText' )?> 1 <?=$defaultCurrency->code?></p>
		</div>
	</div>
	<div class="exchange_rates_box">
		<div class="exchange_rates_h"><strong><?=Yii::app()->dateFormatter->format( 'd MMMM yyyy',strtotime($maxData['date']) )?></strong></div>
		<div class="exchange_rates_ins">
			<span><strong><?=number_format( $maxValue, CommonLib::getDecimalPlaces( $maxValue ), ',', ' ' )?></strong></span>
			<p><?=$model->code?> <?=Yii::t( $NSi18n, 'forOneText' )?> 1 <?=$defaultCurrency->code?></p>
		</div>
	</div>
<?php 
}else{ 
	$dayValue =  $dayDataModel->value;
?>
	<div class="exchange_rates_box">
		<div class="exchange_rates_h"><strong><?=Yii::app()->dateFormatter->format( 'd MMMM yyyy',strtotime( $dateStr ) )?></strong></div>
		<div class="exchange_rates_ins">
			<span><strong><?=number_format( $dayDataModel->value, CommonLib::getDecimalPlaces( $dayDataModel->value ), ',', ' ' )?></strong></span>
			<p><?=$model->code?> <?=Yii::t( $NSi18n, 'forOneText' )?> 1 <?=$defaultCurrency->code?></p>
		</div>
	</div>
<?php } ?>