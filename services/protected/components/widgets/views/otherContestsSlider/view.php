<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	if( !Yii::App()->request->isAjaxRequest ){
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wContestSliderWidget.js' ), CClientScript::POS_END );
	}
?>

<!-- - - - - - - - - - - - - - Brokers Slider - - - - - - - - - - - - - - - - -->
<hr class="separator2">
<section class="section_offset turn_box <?=$ins?>">
	<div class="clearfix">
		<h3 class="alignleft"><?=CHtml::link( Yii::t( $NSi18n, 'Other contests'), Yii::app()->createUrl('contest/all') )?></h3>
		<!-- - - - - - - - - - - - - - Nav Buttons - - - - - - - - - - - - - - - - -->
		<div class="nav_buttons alignright">
			<a href="#" class="prev_btn"><span class="tooltip"><?=Yii::t( $NSi18n, 'Previous' )?></span></a>
			<a href="#" class="next_btn"><span class="tooltip"><?=Yii::t( $NSi18n, 'Next' )?></span></a>
		</div>
		<!-- - - - - - - - - - - - - - End of Nav Buttons - - - - - - - - - - - - - - - - -->
	</div>
	<hr>
	<div class="carousel-wrapper">
	<div class="post_carousel owl-carousel" data-desktop="3" data-large="3" data-medium="1" data-small="1" data-extra-small="1" data-currentitem="0" data-maximumitem="1" data-offset="3">
		<?=$items?>
	</div><!-- / .owl-carousel -->
	</div>
</section>
<!-- - - - - - - - - - - - - - End of Brokers Slider - - - - - - - - - - - - - - - - -->

<?if( !Yii::App()->request->isAjaxRequest ){

Yii::app()->clientScript->registerScript('otherContestsSliderJs', '

	!function( $ ) {
		var ns = ' . $ns . ';
		var ls = ' . json_encode( $jsls ) . ';
			
		ns.wContestSliderWidget = wContestSliderWidgetOpen({
			ns: ns,
			selector: ".' . $ins . '",
			ajaxLoadListURL: "' . Yii::app()->createAbsoluteUrl("contest/ajaxLoadSliderItems") . '",
			exclude: ' . $excludeId . ',
		});
	}( window.jQuery );

', CClientScript::POS_END);

}?>