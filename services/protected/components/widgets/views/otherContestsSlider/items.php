<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
			
	foreach( $models as $model ){
		echo CHtml::openTag('div');
			echo CHtml::openTag('figure', array( 'class' => 'broker_post' ));
				echo CHtml::link(
					CHtml::tag( 'img', array( 'src' => $model->getSizedSrcImage( 623, 318 ), 'title' => $model->name, 'alt' => $model->name ) ),
					$model->singleUrl
				);
				echo CHtml::tag('p', array( ), $model->name );
						
				$labelCssClass = 'info_result';
				if( $model->status != 'completed' ) $labelCssClass = 'info_result current';
				$label = ucfirst( $model->status );
				$link = $model->singleUrl;
				if( $label == 'Completed' ) {
					$label = 'Contest finished';
				}
				
				
				if( $label == 'Registration' && ( Yii::App()->user->isGuest || ( !Yii::App()->user->isGuest && !Yii::App()->user->getModel()->isRegisteredInContest( $model->id )) ) ){
					$link .= '#contestRegistrationForm';
				}
				
				if( !Yii::App()->user->isGuest && Yii::App()->user->getModel()->isRegisteredInContest( $model->id ) && $model->status != 'completed' ){
					$label = 'You are registered';
				}
				
				$label = Yii::t( $NSi18n, $label );

				echo CHtml::link(
					$label,
					$link,
					array( 'class' => $labelCssClass )
				);
				
			echo CHtml::closeTag('figure');
		echo CHtml::closeTag('div');
	}
?>