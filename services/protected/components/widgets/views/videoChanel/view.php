<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>

<div class="fx_section turn_box crypto-turn_box <?=$ins?>" id="VideoChanelWidget">
    <div class="fx_content-title">
        <h3 class="fx_content-title-link"><?=$chanel->title?></h3>

        <?=$chanel->subTitle ? CHtml::tag('span', array('class' => 'fx_content-subtitle'), $chanel->subTitle) : ''?>

        <div class="nav_buttons fx_slider-nav alignright">
            <?=$chanel->url ? CHtml::link(Yii::t($NSi18n, 'All videos'), $chanel->url, array('class' => 'fx_all-videos')) : ''?>
            <a href="#" class="prev_btn fx_slider-prev">
                <span class="tooltip"><?=Yii::t($NSi18n, 'Previous')?></span>
            </a>
            <a href="#" class="next_btn fx_slider-next">
                <span class="tooltip"><?=Yii::t($NSi18n, 'Next')?></span>
            </a>
        </div>
    </div>
        
    <div class="fx_slider-wr owl-carousel post_carousel" data-desktop="2" data-large="2" data-medium="2" data-small="1" data-extra-small="1">
    <?php foreach ($models as $model) {?>
        <div>
            <!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
            <figure class="post">
                <div class="fx_video-post">
                    <div class="fx_videopost-wr">
                        <iframe src="<?=$model->url?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                    <div class="fx_videopost-title">
                        <span><?=$model->title?></span>
                    </div>
                </div>
            </figure>
            <!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
        </div>
    <?php }?>
    </div>
</div>