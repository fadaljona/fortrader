<?php 
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wSearchBrokersWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="search_col75 <?=$ins?>">
	<div class="quotes_search_box_2">
		<form>
			<button class="quotes_search_btn"><i class="fa fa-search"></i></button>
			<input type="text" name="searchField" class="brokerSearchField" placeholder="<?=Yii::t( $NSi18n, 'SearchFor' . $type )?>">
			<ul class="quotes_search_list">
			</ul>
		</form>
	</div>
</div>

<script>
!function( $ ) {
	var ns = ".<?=$ns?>";
	var nsText = ".<?=$ns?>";
	var ins = ".<?=$ins?>";
	var minInputChars = <?=$minInputChars ? $minInputChars : 0?>;

	ns.wSearchBrokersWidget = wSearchBrokersWidgetOpen({
		ns: ns,
		ins: ins,
		selector: nsText+' .<?=$ins?>',
		minInputChars: minInputChars,
		ajaxSubmitURL: '<?=Yii::app()->createAbsoluteUrl('broker/ajaxSearch');?>',
		type: '<?=$type?>',
	});
}( window.jQuery );
</script>