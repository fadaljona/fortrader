<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="profile-activity clearfix" idActivity="<?=$activity->id?>">
	<?=$activity->user->getHTMLAvatar( Array( 'class' => 'pull-left' ))?>
	<?=$activity->getIcon()?>
	<a class="user" href="<?=$activity->user->getProfileURL()?>"><?=CHtml::encode($activity->user->showName)?></a>
	<?=$activity->message?>
	<div class="time">
		<?$this->widget( 'widgets.parts.TimeDiffWidget', Array( 'time' => $activity->createdDT ))?>
	</div>
</div>