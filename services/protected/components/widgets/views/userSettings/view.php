<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wUserSettingsWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="<?=$ins?>">
	<div class="profile-inside__menu">
		<div class="inside-menu__gen clearfix topSettingsTabs">
			<a href="#" class="inside-menu__profile inside-menu__active"><i class="inside-menu_ic ic_profile"></i><?=Yii::t( $NSi18n, 'Profile' )?></a>
			<a href="#" class="inside-menu__avatar"><i class="inside-menu_ic ic_avatar"></i><?=Yii::t( $NSi18n, 'Avatar' )?></a>
			<a href="#" class="inside-menu__pass"><i class="inside-menu_ic ic_pass"></i><?=Yii::t( $NSi18n, 'Password' )?></a>
			<a href="#" class="inside-menu__setting"><i class="inside-menu_ic ic_setting"></i><?=Yii::t( $NSi18n, 'Settings' )?></a>
		</div>
	</div>
	<div class="topSettingsContentTabs">
		<?
			$this->widget( 'widgets.forms.UserProfileFormWidget', Array(
				'ns' => 'nsActionView',
				'model' => $generalFormModel,
				'ajax' => true,
				'itCurrentModel' => $itCurrentModel,
			));
		?>
		<?
			$this->widget( 'widgets.forms.UserAvatarFormWidget', Array(
				'ns' => 'nsActionView',
				'model' => $avatarFormModel,
				'ajax' => true,
			));
		?>
		<?
			$this->widget( 'widgets.forms.UserPasswordFormWidget', Array(
				'ns' => 'nsActionView',
				'model' => $passwordFormModel,
				'ajax' => true,
			));
		?>
		<?
			$this->widget( 'widgets.forms.UserSettingsFormWidget', Array(
				'ns' => 'nsActionView',
				'model' => $settingsFormModel,
				'ajax' => true,
			));
		?>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
				
		ns.wUserSettingsWidget = wUserSettingsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .<?=$ins?>',
		});
	}( window.jQuery );
</script>
