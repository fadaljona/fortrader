<?
	if( Yii::App()->user->checkAccess( 'viewTopAdminMenuControl' )) {
		$menuItems = Array(
			'class' => 'bootstrap.widgets.TbMenu',
			'items' => Array(
				Array( 'label' => 'Home', 'url' => '#' ),
				Array( 'label' => 'About', 'url' => '#' ),
				Array( 'label' => 'Contact', 'url' => '#' ),
				Array( 'label' => 'Front', 'url' => Array( '/' ), 'itemOptions' => Array( 'class' => 'wFront' )),
                Array( 'label' => 'Flush cache', 'url' => array('helper/flushCache') ),
                Array( 'label' => 'Models for replace', 'url' => array('helper/modelsForReplace') ),
			),
		);
	}else{
		$menuItems = array();
	}
	$this->widget( 'bootstrap.widgets.TbNavbar', Array(
		'type' => 'inverse',
		'brand' => 'FORTRADER.ORG',
		'brandUrl' => Array( '/admin' ),
		'collapse' => true,
		'fluid' => true,
		'htmlOptions' => Array(
			'class' => 'iAdminNavbarWidget i01 wAdminNavbarWidget',
		),
		'items' => Array(
			$menuItems,
			$this->render( "{$class}/_logged", Array( 'name' => $userName ), true ),
		),
	));
?>