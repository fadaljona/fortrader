<?php
	if( !$indicatorId ) Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wCalendarComingEventsWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$wrapperClass = $ins;
	if( $indicatorId ){
		$wrapperClass = $ins . $indicatorId;
	}
	
echo CHtml::openTag('div', array('class' => $ins . ' ' . $wrapperClass . ' spinner-margin position-relative'));

	echo CHtml::openTag('ul', array('class' => 'calendar exchange-links'));
		echo CHtml::tag('li', array('class' => 'exchange-links-header'), $title);
		echo $list;
	echo CHtml::closeTag('ul');
	
	if( !$indicatorId && ($count == $modelsCount) ) echo CHtml::link(' ', '#', array(
		'class' => 'load_btn chench_btn',
		'data-text' => Yii::t($NSi18n, 'Load more'),
		'data-shot-text' => Yii::t($NSi18n, 'Load more')
	));


echo CHtml::closeTag('div');
?>

<?php if( !$indicatorId ) { ?>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
				
		ns.wCalendarComingEventsWidget = wCalendarComingEventsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: '.<?=$wrapperClass?>',
			loadUrl: '<?=Yii::app()->createUrl('calendarEvent/ajaxLoadCalendarComingEvents')?>',
		});
	}( window.jQuery );
</script>
<?php }?>