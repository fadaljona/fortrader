<?
	Yii::import( 'components.sys.CHtmlEx' );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="iEditorWidget i01 <?=$ins?>">
	<form>
		<?=Yii::t( $NSi18n, 'Begin' )?><br>
		<?=CHtmlEx::dateField( "begin", $begin, Array( "class" => "{$ins} wBegin", "onchange" => "this.form.submit()" ))?><br>
		<?=Yii::t( $NSi18n, 'End' )?><br>
		<?=CHtmlEx::dateField( "end", $end, Array( "class" => "{$ins} wEnd", "onchange" => "this.form.submit()" ))?><br>
	</form>
	<?
		$this->widget( 'widgets.gridViews.AdminQuotesSearchStatsGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'dataProvider' => $DP,
			'ajax' => false,
		));
	?>
</div>