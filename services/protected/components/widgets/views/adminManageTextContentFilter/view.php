<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="wAdminManageTextContentFilterWidget <?=$ins?>">

<?php
	echo CHtml::openTag('table', array('class' => 'table'));
		echo CHtml::openTag('tr');
			echo CHtml::tag('th', array(), ' ');
			foreach( $textFields as $field ){
				echo CHtml::tag('th', array(), Yii::t('*', str_replace('_', ' ', join( preg_split('/(?<=[a-z])(?=[A-Z])/x', ucfirst($field)), " " ))) );
			}
		echo CHtml::closeTag('tr');
		
		foreach( $langs as $lang ){
			echo CHtml::openTag('tr');
				echo CHtml::tag('td', array(), $lang->alias . ' ' . Yii::t($NSi18n, 'yes'));
				foreach( $textFields as $field ){
					$arrKey = $field . $lang->id . 'yes';
					
					$linkClass = 'btn-default';
					if( Yii::app()->request->getParam('lang') == $lang->id && Yii::app()->request->getParam('field') == $field && Yii::app()->request->getParam('type') == 'yes' ) $linkClass = 'btn-success';
					
					echo CHtml::tag(
						'td', 
						array(), 
						CHtml::link(
							$filedStats[$arrKey], 
							Yii::app()->createUrl( 
								'admin/' . ManageTextContentLib::getSigleRoute(), 
								array( 'slug' => ManageTextContentLib::getCurrentSlug(), 'lang' => $lang->id, 'field' => $field, 'type' => 'yes' )
							), 
							array('class' => 'btn btn-mini ' . $linkClass)
						)
					);
				}
			echo CHtml::closeTag('tr');
			echo CHtml::openTag('tr');
				echo CHtml::tag('td', array(), $lang->alias . ' ' . Yii::t($NSi18n, 'no'));
				foreach( $textFields as $field ){
					$arrKey = $field . $lang->id . 'no';
					
					$linkClass = 'btn-default';
					if( Yii::app()->request->getParam('lang') == $lang->id && Yii::app()->request->getParam('field') == $field && Yii::app()->request->getParam('type') == 'no' ) $linkClass = 'btn-success';
					
					echo CHtml::tag(
						'td', 
						array(), 
						CHtml::link(
							$filedStats[$arrKey], 
							Yii::app()->createUrl( 
								'admin/' . ManageTextContentLib::getSigleRoute(), 
								array( 'slug' => ManageTextContentLib::getCurrentSlug(), 'lang' => $lang->id, 'field' => $field, 'type' => 'no' )
							), 
							array('class' => 'btn btn-mini ' . $linkClass)
						)
					);
				}
			echo CHtml::closeTag('tr');
		}
		
	echo CHtml::closeTag('table');
?>
</div>