<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wEAVersionProfileWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lSaidThank' => 'Said thank you',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$countThanks = ThankModel::getCount( 'eaVersion/single', $model->id );
?>
<div class="row-fluid wEAVersionProfileWidget">
	<div class="row-fluid">
		<div class="span3 center">
			<?if( strlen( $model->EA->nameImage )){?>
				<span class="profile-picture" style="position:relative;">
					<?=$model->EA->getImage()?>
					<div style="position:absolute; right:0px; bottom: 0px;text-align:right;">
						<span class="label label-info arrowed"><?=Yii::t( '*', 'Version' )?> <?=$model->version?></span><br>
						<span class="label label-important"><?=Yii::t( '*', $model->status )?></span>
					</div>
				</span>
				<div class="space-6"></div>
			<?}?>
			<?if( strlen( $model->nameFile )){?>
				<div class="profile-contact-info">
					<div class="profile-contact-links align-left">
						<a class="btn btn-link" href="<?=$model->getDownloadURL()?>">
							<i class="icon-download bigger-120 pink"></i>
							<?=Yii::t( '*', 'Download' )?>
						</a>
					</div>
				</div>
			<?}?>
			<div class="hr hr12 dotted"></div>
			<div class="clearfix">
				<div class="grid2">
					<span class="bigger-175 blue"><?=CommonLib::numberFormat( $model->views )?></span>
					<br>
					<?=Yii::t( '*', 'Views' )?>
				</div>
				<div class="grid2">
					<span class="bigger-175 green"><?=CommonLib::numberFormat( $model->downloads )?></span>
					<br>
					<?=Yii::t( '*', 'Downloads' )?>
				</div>
			</div>
		</div>
		<div class="span9">
			<?if( strlen( $model->desc )){?>
				<div class="row-fluid">
					<div class="span9">
						<div class="widget-box transparent">
							<div class="widget-header widget-header-small">
								<h4 class="smaller">
									<i class="icon-check bigger-110"></i>
									<?=Yii::t( '*', 'About version' )?> <?=$model->version?>
								</h4>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<?=$model->desc?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?}?>
			<?$this->controller->widget( "widgets.detailViews.EAVersionProfileDetailViewWidget", Array( 'data' => $model ))?>
			
			<?if( !Yii::App()->user->isGuest ){?>
				<div class="space-6"></div>
				
				<div class="row-fluid">
					<a href="#" class="iBtn i10 wThank">
						<?=Yii::t( '*', 'Thanks' )?> <?if($countThanks){?>(<?=$countThanks?>)<?}?>
					</a>
				</div>
				<div class="space-6"></div>
				<div class="wThanks iDiv i16"></div>
			<?}?>
			
			<?$this->controller->widget( "widgets.EAStatementsWidget", Array( 
				'idEAVersion' => $model->id, 
				'ns' => $ns,
			))?>
			
			<div class="row-fluid" style="margin-top:10px;">
				<?
					$this->widget( 'widgets.lists.EAStatementsListWidget', Array(
						'ns' => 'nsActionView',
						'ajax' => true,
						'showOnEmpty' => true,
						'idVersion' => $model->id,
					));
				?>
				<a href="<?=EAStatementModel::getAddURL( $model->idEA, $model->id )?>" class="iA i07"><?=Yii::t( '*', 'Add' )?></a>
			</div>
			
			<br><br>
			<div class="row-fluid">
				<?
					$this->widget( 'widgets.UsersConversationWidget', Array(
						'ns' => $ns,
						'ajax' => true,
					));
				?>
			</div>
			
			<div class="row-fluid" style="margin-top:20px;">
				<?
					$this->widget( 'widgets.PostInformWidget', Array(
						'ns' => 'nsActionView',
					));
				?>
			</div>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idVersion = <?=$model->id?>;
				
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wEAVersionProfileWidget = wEAVersionProfileWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wEAVersionProfileWidget',
			idVersion: idVersion,
			ls: ls,
		});
	}( window.jQuery );
</script>