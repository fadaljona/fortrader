<?php
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::App()->params['wpThemePath'].'/plagins/jquery.countdown.min.js' ), CClientScript::POS_END );
	Yii::App()->clientScript->registerScriptFile("//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment-with-locales.min.js");
	Yii::App()->clientScript->registerScriptFile("//cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.4.0/moment-timezone-with-data-2010-2020.min.js");
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	
	$contestTime = '';
	$status = $model->status;
	
	$statusStartedNotRegistered = false;
	$mStatus = '';
    if( !Yii::App()->user->isGuest and Yii::App()->user->getModel()->isRegisteredInContest( $model->id )){
        $mStatus = 'registered';
    }elseif( $model->status == 'started' ){
		$statusStartedNotRegistered = true;
	}
	 $contestMember = ContestModel::getContestMemberByContestId($model->id);
	 
	if($mStatus == 'registered') {
		if($contestMember) {
			$statusButton = CHtml::link( Yii::t($NSi18n, 'You are registered') .CHtml::tag('span', array(), Yii::t($NSi18n, 'Go to your member page')), ContestMemberModel::getModelSingleURL($contestMember->id), array( 'class' => 'silver_btn red' ) );
		}
	}elseif($statusStartedNotRegistered){
		$statusButton = CHtml::link( Yii::t($NSi18n, 'Contest is started') .CHtml::tag('span', array(), Yii::t($NSi18n, 'Contest is started')), '#', array( 'class' => 'silver_btn red', 'onclick' => "return false;" ) );
	}
	
	if( $status == 'registration' ){

			$contestTime = strtotime( $model->begin );
			$timeTitle = Yii::t($NSi18n, 'Prior to the contest') . ': ';
			if( strtotime( $model->begin ) <= time() ){
				$contestTime = strtotime( $model->end );
				$timeTitle =  Yii::t($NSi18n, 'Before the end of the contest') . ': ';
			}
			$contestLink = '<a href="'.$model->singleUrl.'#contestRegistrationForm" class="registration_btn">'.Yii::t($NSi18n, 'Registration').'</a>';
			$statusButton = CHtml::link( Yii::t($NSi18n, 'Registration') .CHtml::tag('span', array(), Yii::t($NSi18n, 'Register in the contest')), '#contestRegistrationForm', array( 'class' => 'silver_btn red' ) );
			if( $this->isCurrentUserInContest( $model->id )) {
				$contestLink = '<a href="'.$contestMember->singleUrl.'" class="registration_btn">'.Yii::t($NSi18n, 'You are registered').'</a>';
				$statusButton = CHtml::tag('span', array( 'class' => 'silver_btn red' ), Yii::t($NSi18n, 'Registration') .CHtml::tag('span', array(), Yii::t($NSi18n, 'You are registered')));
			}
		
	}
	if( $status == 'started' ){

			$contestTime = strtotime( $model->end );
			$timeTitle =  Yii::t($NSi18n, 'Before the end of the contest') . ': ';
			$contestLink = '<a href="'.$model->singleUrl.'" class="registration_btn">'.Yii::t($NSi18n, 'Views contest').'</a>';
		
	}
	if( $status == 'completed' ){
		$timeTitle = Yii::t($NSi18n, 'Contest completed');
		$contestLink = '<a href="'.$model->singleUrl.'" class="registration_btn">'.Yii::t($NSi18n, 'Views contest results').'</a>';
		
		$statusButton = CHtml::link( Yii::t($NSi18n, 'Completed') .CHtml::tag('span', array(), Yii::t($NSi18n, 'Contest is completed')), '#', array( 'class' => 'silver_btn red', 'onclick' => "return false;" ) );
	}
	if( $status == 'registration stoped' ){
		$timeTitle = Yii::t($NSi18n, 'Registration stoped');
		$contestLink = '<a href="'.$model->singleUrl.'" class="registration_btn">'.Yii::t($NSi18n, 'Views contest').'</a>';
		
		$statusButton = CHtml::link( Yii::t($NSi18n, 'Registration stoped') .CHtml::tag('span', array(), Yii::t($NSi18n, 'Registration stoped')), '#', array( 'class' => 'silver_btn red', 'onclick' => "return false;" ) );
	}
					
?>


<?php 
if( $contestTime ){
	new JsTrans('app',Yii::app()->language);
	
	$timeLeft = $contestTime - time();
	$ostH = $timeLeft % (60*60*24);
	$days = ($timeLeft-$ostH)/(60*60*24);
	$ostM = $ostH % (60*60);
	$hours = ( $ostH - $ostM ) / (60*60);
	$ostS = $ostM % 60;
	$minutes = ( $ostM - $ostS ) / 60;

	Yii::app()->clientScript->registerScript('countDownJs', '
	!function( $ ) {	
		if($(".countdown_box").length){
			$(".countdown_box").each(function(){
				var $this = $(this),
				date = $this.attr("data-date");
				var jsDate = moment.tz(date + " 00:00", "Europe/Moscow");
				$($this).countdown(jsDate.toDate(), function(event) {
					var $this = $(this).html(event.strftime(
						"<div class=\'countdown alignright\'><div class=\'clock countDays\'><p>%D</p><span>'.substr(Yii::t( $NSi18n, '{n} day|{n} days', $days ), strpos(Yii::t( $NSi18n, '{n} day|{n} days', $days ), ' ')).'</span></div><div class=\'clock countHours\'><p>%H</p><span>'.substr(Yii::t( $NSi18n, '{n} hour|{n} hours', $hours ), strpos(Yii::t( $NSi18n, '{n} hour|{n} hours', $hours ), ' ')).'</span></div><div class=\'clock countMins\'><p>%M</p><span>'.substr(Yii::t( $NSi18n, '{n} minute|{n} minutes', $minutes ), strpos(Yii::t( $NSi18n, '{n} minute|{n} minutes', $minutes ), ' ')).'</span></div></div>"
					));
				}).on("update.countdown", function(event) {
					var mins = $this.find(".countMins");
					var minsCount = parseInt( mins.find("p").text() );
					var minsMsg = Yii.t("app", "{n} minute|{n} minutes", minsCount);
					var arr = minsMsg.split(" ");
					mins.find("span").text(arr[1]);
					
					
					var hours = $this.find(".countHours");
					var hoursCount = parseInt( hours.find("p").text() );
					var hoursMsg = Yii.t("app", "{n} hour|{n} hours", hoursCount);
					var arr = hoursMsg.split(" ");
					hours.find("span").text(arr[1]);
					
					var days = $this.find(".countDays");
					var daysCount = parseInt( days.find("p").text() );
					var daysMsg = Yii.t("app", "{n} day|{n} days", daysCount);
					var arr = daysMsg.split(" ");
					days.find("span").text(arr[1]);
					
				});
			});
		}
	}( window.jQuery );
	', CClientScript::POS_END);
}
?>	

<div class="<?=$ins?>" >
	<div class="section_offset paddingBottom30">
		<div class="blockdiv5 clearfix">
			<?php
			$itemPropUrl = '';
			if( $mode == 'single' ) $itemPropUrl = 'itemprop="url"';
			?>
			<a href="<?=$model->singleURL?>" <?=$itemPropUrl?> class="blockdiv5_box1">
				<?php 
					$itempropName = '';
					if( $mode == 'single' ){ $itempropName = 'itemprop="name"'; }
				?>
				<span <?=$itempropName?>><?=$model->name?></span>
				<span class="blockdiv5_box1_count"><?=Yii::t($NSi18n, 'Members count')?>: <?=$model->membersCount?></span>
			</a>
			<span class="blockdiv5_box2 alignright">
				<span class="alignleft"><?=Yii::t($NSi18n, 'Sponsor')?>:</span>
				<?php
					foreach( $model->brokers as $broker ) {
						$key = Yii::t( '*', "{broker} Forex Contests", Array( '{broker}' => $broker->brandName ));
						$thumbImage = $broker->getContestSponsorImage(Array( 'title' => $key, 'alt' => $key, 'class' => 'alignright' ));
						$title = $thumbImage ? $thumbImage : CHtml::encode( $broker->brandName );
						$out[] = CHtml::link( $title, $broker->getSingleURL() );
					}
					$out = implode( ', ', $out );
					$out = str_replace( '>, <', '> <', $out );
					echo $out;
				?>
			</span>
		</div>
	</div>
	
	
	
	<?php if( $mode == 'single' ){ ?>
	<div class="section_offset clearfix">
		<p class="countdown_text"><?=Yii::t($NSi18n, 'The prize fund')?>
			<span class="red_color"><?=$model->sumPrizes?></span> USD,
			<span class="red_color"><?=$model->countPrizes?></span> <?=Yii::t($NSi18n, 'Prize places')?>, <?=$timeTitle?>
		</p>
		<?php if( $contestTime ){ ?><div class="countdown_box clearfix" data-date='<?=date( 'Y-m-d', $contestTime)?>'></div><?php }?>
	</div>
	<?php }?>
	
	
	
	<div class="section_offset">
		<div class="post_contest clearfix">
			<div class="post_contest_box1">
			<?php if( $mode == 'single' ){ ?>
				<a itemprop="image" href="<?=$model->singleURL?>" itemscope itemtype="http://schema.org/ImageObject">
					<img itemprop="contentUrl url" src="<?=$model->getSizedSrcImage( 729, 'auto' )?>" alt="">
				</a>
			<?php }else{ ?>
				<a href="<?=$model->singleURL?>" ><img src="<?=$model->getSizedSrcImage( 729, 'auto' )?>" alt=""></a>
			<?php } ?>
			</div>
			<div class="post_contest_box2">
			<?php if( $mode == 'single' ){ ?>
				<ul class="post_contest_box2_list clearfix">
					<li>
						<a class="silver_btn" href="<?=CommonLib::getUrl( $model->urlRules )?>">
							<?=Yii::t($NSi18n, 'Conditions')?>
							<span><?=Yii::t($NSi18n, 'Conditions of participation in the contest')?></span>
						</a>
					</li>
					<li>
						<?=$statusButton?>
					</li>
					<li>
						<a class="silver_btn" href="#singleContestMembersListWidget">
							<?=Yii::t($NSi18n, 'Rating')?>
							<span><?=Yii::t($NSi18n, 'Rating of participants of the contest')?></span>
						</a>
					</li>
				</ul>
			<?php }else{ ?>
				<div class="post_contest_inner_box2">
					<p class="reg"><?=$timeTitle?></p><div class="clear"></div>
					<?php if($contestTime){?><div class="countdown_box clearfix" data-date='<?=date( 'Y-m-d', $contestTime)?>'></div><?php }?>
					<div class="countdown_text">
						<p class="reg"><?=Yii::t($NSi18n, 'The prize fund')?></p><div class="clear"></div>
						<p class="big_number"><?=$model->sumPrizes?> <span>USD</span></p><div class="clear"></div>
						<p><?=Yii::t($NSi18n, 'Prize places')?> <span class="blue_color"><?=$model->countPrizes?></span></p><div class="clear"></div>
						<?=$contestLink?>
					</div>
				</div>
			<?php }?>	
			</div>
		</div>
	</div>
	
	<?php if( $mode == 'single' ){ ?>
	<div class="section_offset clearfix">
		<ul class="list_info_date">
			<li>
				<span class="list_info_date_icon_box"><i aria-hidden="true" class="fa fa-calendar list_info_date_icon"></i></span>
				<span><?=Yii::t($NSi18n, 'Contest start date')?><br><time datetime="<?=$model->begin?>"><?=Yii::app()->dateFormatter->format( "d MMMM yyyy", strtotime($model->begin) )?></time></span>
			</li>
			<li>
				<?=Yii::t($NSi18n, 'End date of registration')?><br><time datetime="<?=$model->endReg?>"><?=Yii::app()->dateFormatter->format( "d MMMM yyyy", strtotime($model->endReg) )?></time>
			</li>
			<li>
				<?=Yii::t($NSi18n, 'Date of completion of the contest')?><br><time datetime="<?=$model->end?>"><?=Yii::app()->dateFormatter->format( "d MMMM yyyy", strtotime($model->end) )?></time>
			</li>
		</ul>
	</div>
	<?php }?>
	
</div>