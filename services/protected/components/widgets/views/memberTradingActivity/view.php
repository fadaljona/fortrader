<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();



	

	$contestMemberTradesListWidgetContent = false;

if($accountModel) {
?>
	<?php if( $accountModel->idContest ) echo '<hr class="separator2">';?>
	<section class="section_offset <?php if( !$accountModel->idContest ) echo 'paddingBottom0';?>">
		<?php
			if( $accountModel->idContest ){
				echo CHtml::tag('h3', array(), Yii::t( $NSi18n, 'Trading Activity' ) . CHtml::tag('i', array('class' => 'fa fa-cogs', 'aria-hidden' => 'true'), '') ) . 
				CHtml::tag('hr', array('class' => 'separator2'));
			}else{
				echo CHtml::tag('h4', array('class' => 'toggle_btn section_title'), Yii::t( $NSi18n, 'Trading Activity' ));
				
				if( Yii::App()->user->checkAccess( 'monitoringControl' ) ){
				ob_start();
				$this->widget( 'widgets.graphs.ContestMemberDealsHistoryGraphWidget', Array(
					'ns' => 'nsActionView',
					'member' => $accountModel,
				));
				$сontestMemberDealsHistoryGraphWidgetContent = ob_get_clean();
				}

			}
		?>
		<?php if( $accountModel->idContest ) echo '</section><section class="section_offset">';?>
		<?php if( !$accountModel->idContest ) echo '<div class="toggle_content">';?>
		<!-- - - - - - - - - - - - - - Tabs - - - - - - - - - - - - - - - - -->
		<?php
			ob_start();
				$contestMemberTradesListWidget = $this->widget( 'widgets.lists.ContestMemberTradesListWidget', Array(
					'ns' => 'nsActionView',
					'ajax' => true,
				));
			$contestMemberTradesListWidgetContent = ob_get_clean();
		?>
		<div class="tabs_box tabs_rating">
			<div class="clearfix">
				<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
				<div class="wrapper_tabs_list">
					<ul class="tabs_list clearfix">
						<?php if(!$accountModel->server->forReports){?><li class="<?=$contestMemberTradesListWidget->DP->getTotalItemCount() ? 'active' : ''?>"><a href="javascript:;"><?=Yii::t( $NSi18n, 'Trade' )?></a></li><?php }?>
						<li class="<?=!$contestMemberTradesListWidget->DP->getTotalItemCount() ? 'active' : ''?>"><a href="javascript:;"><?=Yii::t( $NSi18n, 'Deals History' )?></a></li>
						<?php if( $сontestMemberDealsHistoryGraphWidgetContent ){ ?>
							<li class=""><a href="javascript:;"><?=Yii::t( $NSi18n, 'History on graph' )?></a></li>
						<?php }?>
						<?php 
							$checkAccountMonitorData = MT4AccountMonitorModel::hasData( $accountModel->accountNumber, $accountModel->idServer );
							$hasArchivedData = MT4AccountMonitorModel::hasArchivedData( $accountModel->accountNumber, $accountModel->id );
							if($checkAccountMonitorData || $hasArchivedData){?>
							<li class=""><a href="javascript:;"><?=Yii::t( $NSi18n, 'Account Monitoring' )?></a></li>
						<?php 
							}
						?>
					</ul>
				</div>
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs List  - - - - - - - - - - - - - - - - -->
			<!-- - - - - - - - - - - - - - Tabs Contant - - - - - - - - - - - - - - - - -->
			<div class="tabs_contant">
				<?php if(!$accountModel->server->forReports){?>
				<!-- - - - - - - - - - - - - - Tabs Item 1 - - - - - - - - - - - - - - - - -->
				<div class="<?=$contestMemberTradesListWidget->DP->getTotalItemCount() ? 'active' : ''?>">
					<?=$contestMemberTradesListWidgetContent?>
				</div>
				<!-- - - - - - - - - - - - - - End of Tabs Item 1 - - - - - - - - - - - - - - - - -->
				<?php }?>
				<!-- - - - - - - - - - - - - - Tabs Item 2 - - - - - - - - - - - - - - - - -->
				
				<div class="<?=!$contestMemberTradesListWidget->DP->getTotalItemCount() ? 'active' : ''?>">
				<?
					$this->widget( 'widgets.lists.ContestMemberDealsHistoryListWidget', Array(
						'ns' => 'nsActionView',
						'ajax' => true,
					));
				?>
				</div>
				<!-- - - - - - - - - - - - - - End of Tabs Item 2 - - - - - - - - - - - - - - - - -->
				
				<?php if( $сontestMemberDealsHistoryGraphWidgetContent ){ echo '<div>'.$сontestMemberDealsHistoryGraphWidgetContent.'</div>'; }?>
				
				<?php 
					if($checkAccountMonitorData){?>
					<div>
					<?
						$this->widget( 'widgets.lists.ContestMemberAccountMonitorListWidget', Array(
							'ns' => 'nsActionView',
							'ajax' => true,
							'memberId' => $accountModel->id,
						));
					?>
					</div>
				<?php 
					}elseif( $hasArchivedData ){
						echo CHtml::tag(
							'div', 
							array('class' => 'section_offset'), 
							CHtml::link( 
								Yii::t($NSi18n, 'Download account monitoring history'), 
								MT4AccountMonitorModel::linkToArchivedData( $accountModel->accountNumber, $accountModel->id ) 
							)
						);
					}
				?>
			</div><!-- / .tabs_contant -->
		</div> <!-- tabs box tabs rating -->
		<?php if( !$accountModel->idContest ) echo '</div>';?>
	</section>
	
	<?php if( !$accountModel->idContest ){ ?>
		<section class="section_offset">
			<h4 class="toggle_btn section_title"><?=Yii::t($NSi18n, 'Statistics')?></h4>
			<div class="toggle_content">
				<div class="tabs_box tabs_rating">
					<div class="clearfix">
						<div class="wrapper_tabs_list">
							<ul class="tabs_list clearfix">
								<?php 
									if(!$accountModel->server->forReports) echo CHtml::tag('li', array(), CHtml::link(Yii::t( $NSi18n, 'Open symbols' ), 'javascript:;') );
									echo CHtml::tag('li', array( 'class' => 'active' ), CHtml::link(Yii::t( $NSi18n, 'Closed symbols' ), 'javascript:;') );
								?>
							</ul>
						</div>
					</div>
					<div class="tabs_contant">
						<?php
						if(!$accountModel->server->forReports){
							echo '<div>';
								$this->widget( 'widgets.lists.ContestMemberTradesSymbolsListWidget', Array(
									'ns' => 'nsActionView',
									'ajax' => true,
									'type' => 'open',
								));
							echo '</div>';
						}
							echo '<div class="active">';
								$this->widget( 'widgets.lists.ContestMemberTradesSymbolsListWidget', Array(
									'ns' => 'nsActionView',
									'ajax' => true,
									'type' => 'closed',
								));
							echo '</div>';
						?>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>	
<?php } ?>