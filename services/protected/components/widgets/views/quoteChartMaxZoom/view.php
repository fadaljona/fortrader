<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();


$cs = Yii::App()->clientScript;
$cs->registerScriptFile('https://code.jquery.com/jquery-3.1.1.min.js');
$cs->registerScriptFile('https://code.highcharts.com/highcharts.js');
$cs->registerScriptFile('https://code.highcharts.com/modules/exporting.js');
$cs->registerScriptFile(
    CHtml::asset(
        Yii::getPathOfAlias('webroot.js.widgets').'/wQuoteChartMaxZoomWidget.js'
    )
);
?>
<div class="fx_statistic-right <?=$ins?>" id="<?=$ins?>"></div>
<script>
!function( $ ) {
    var ns = ".<?=$ns?>";
    ns.wQuoteChartMaxZoomWidget = wQuoteChartMaxZoomWidgetOpen({
        selector: '.<?= $ins ?>',
        loadDataUrl: '<?= Yii::app()->createUrl('cryptoCurrencies/ajaxLoadChartData') ?>',
        chartContainer: '<?=$ins?>',
        symbolId: <?= $symbolId ?>,
        langOptions: {
            loading: '<?= Yii::t($NSi18n, 'Loading...') ?>',
            months: [
                '<?= Yii::t($NSi18n, 'January') ?>',
                '<?= Yii::t($NSi18n, 'February') ?>',
                '<?= Yii::t($NSi18n, 'March') ?>',
                '<?= Yii::t($NSi18n, 'April') ?>',
                '<?= Yii::t($NSi18n, 'May') ?>',
                '<?= Yii::t($NSi18n, 'June') ?>',
                '<?= Yii::t($NSi18n, 'July') ?>',
                '<?= Yii::t($NSi18n, 'August') ?>',
                '<?= Yii::t($NSi18n, 'September') ?>',
                '<?= Yii::t($NSi18n, 'October') ?>',
                '<?= Yii::t($NSi18n, 'November') ?>',
                '<?= Yii::t($NSi18n, 'December') ?>'
            ],
            weekdays: [
                '<?= Yii::t($NSi18n, 'Sunday') ?>',
                '<?= Yii::t($NSi18n, 'Monday') ?>',
                '<?= Yii::t($NSi18n, 'Tuesday') ?>',
                '<?= Yii::t($NSi18n, 'Wednesday') ?>',
                '<?= Yii::t($NSi18n, 'Thursday') ?>',
                '<?= Yii::t($NSi18n, 'Friday') ?>',
                '<?= Yii::t($NSi18n, 'Saturday') ?>'
            ],
            shortMonths: [
                '<?= Yii::t($NSi18n, 'Jan') ?>',
                '<?= Yii::t($NSi18n, 'Feb') ?>',
                '<?= Yii::t($NSi18n, 'Mar') ?>',
                '<?= Yii::t($NSi18n, 'Apr') ?>',
                '<?= Yii::t($NSi18n, 'May') ?>',
                '<?= Yii::t($NSi18n, 'Jun') ?>',
                '<?= Yii::t($NSi18n, 'Jul') ?>',
                '<?= Yii::t($NSi18n, 'Aug') ?>',
                '<?= Yii::t($NSi18n, 'Sep') ?>',
                '<?= Yii::t($NSi18n, 'Oct') ?>',
                '<?= Yii::t($NSi18n, 'Nov') ?>',
                '<?= Yii::t($NSi18n, 'Dec') ?>',
            ],
        }
    });
}( window.jQuery );
</script>