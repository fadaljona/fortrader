<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wCurrencyRatesArchiveWidget.js' ) );
?>

<div class="<?=$ins?>" >

	<div class="choose_date_box position-relative spinner-margin">

		<!-- choose_date -->
		<div class="choose_date choose_years">
			<div class="clearfix choose_date_top">
				<div class="choose_date_text"><?=Yii::t($NSi18n, 'Choose year')?>: <span class="red_color"><?=$filterData['choosedYear']?></span></div>
				<!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->
				<div class="navigation_box_2 clearfix">
					<a href="javascript:;" class="prev"><span class="tooltip"><?=Yii::t($NSi18n, 'Previous')?></span></a>
					<a href="javascript:;" class="next"><span class="tooltip"><?=Yii::t($NSi18n, 'Next')?></span></a>
				</div>
				<!-- - - - - - - - - - - - - - End of Navigation - - - - - - - - - - - - - - - - -->
			</div>

			<div class="choose_date_list_wr">
				<div class="choose_date_list">
					<ul class="choose_date_bottom">
					<?php
						foreach( $filterData['years'] as $item ){
							$currentClass = '';
							if( $filterData['choosedYear'] == $item['year'] ) $currentClass = ' current';
							echo Chtml::tag(
								'li',
								array( 'class' => "choose_date_btm_item" . $currentClass ),
								Chtml::link( $item['year'], CurrencyRatesModel::getArchiveUrl( array('year' => $item['year'], 'type' => $dataType ) ) )
							);
						}
					?>
					</ul>
				</div>
			</div>              
		</div>
		<!--/ choose_date -->

		<!-- choose_month -->
		<div class="choose_date chooseMonth">
			<div class="clearfix choose_date_top">
				<div class="choose_date_text"><?=Yii::t($NSi18n, 'Choose month')?>: <span class="blue_color choosedItem"><?=$filterData['choosedMo'] ? $filterData['months'][$filterData['choosedMo']] : ''?></span></div>
				<!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->
				<div class="navigation_box_2 clearfix">
					<a href="javascript:;" class="prev"><span class="tooltip"><?=Yii::t($NSi18n, 'Previous')?></span></a>
					<a href="javascript:;" class="next"><span class="tooltip"><?=Yii::t($NSi18n, 'Next')?></span></a>
				</div>
				<!-- - - - - - - - - - - - - - End of Navigation - - - - - - - - - - - - - - - - -->
			</div>

			<div class="choose_date_list_wr">
				<div class="choose_date_list">
					<ul class="choose_date_bottom">
					<?php
						foreach( $filterData['months'] as $num => $name ){
							$currentClass = '';
							if( $filterData['choosedMo'] == $num ) $currentClass = ' current';
							$linkNum = $num < 10 ? '0' . $num : $num;
							echo Chtml::tag(
								'li',
								array( 'class' => "choose_date_btm_item" . $currentClass, 'data-mo' => $num ),
								Chtml::link( $name, CurrencyRatesModel::getArchiveUrl( array('year' => $filterData['choosedYear'], 'mo' => $linkNum, 'type' => $dataType ) ) )
							);
						}
					?>
					</ul>
				</div>
			</div>             
		</div>
		<!--/ choose_month -->

		<?php if( $filterData['choosedMo'] ){ ?>
			<!-- choose_day -->
			<div class="choose_date chooseDay">
				<div class="clearfix choose_date_top">
					<div class="choose_date_text"><?=Yii::t($NSi18n, 'Choose day')?>: <span class="blue_color choosedItem">
						<?php
						if( $filterData['choosedDay'] ){
							if( $filterData['choosedDay']<10 ){
								echo '0'.$filterData['choosedDay'];
							}else{
								echo $filterData['choosedDay'];
							}
						}
						?>
					</span></div>
					<!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->
					<div class="navigation_box_2 clearfix">
						<a href="javascript:;" class="prev"><span class="tooltip"><?=Yii::t($NSi18n, 'Previous')?></span></a>
						<a href="javascript:;" class="next"><span class="tooltip"><?=Yii::t($NSi18n, 'Next')?></span></a>
					</div>
					<!-- - - - - - - - - - - - - - End of Navigation - - - - - - - - - - - - - - - - -->
				</div>

				<div class="choose_date_list_wr">
					<div class="choose_date_list">
						<ul class="choose_date_bottom">
						<?php
							for( $i=$filterData['firstDay']; $i<=$filterData['lastDay']; $i++ ){
								$currentClass = '';
								if( $filterData['choosedDay'] == $i ) $currentClass = ' current';
								
								$moNum = $filterData['choosedMo'] < 10 ? '0'.$filterData['choosedMo'] : $filterData['choosedMo'];
								$dayNum = $i < 10 ? '0'.$i : $i;
								
								echo Chtml::tag(
									'li',
									array( 'class' => "choose_date_btm_item" . $currentClass, 'data-day' => $i ),
									Chtml::link( $dayNum, CurrencyRatesModel::getArchiveUrl( array('year' => $filterData['choosedYear'], 'mo' => $moNum, 'day' => $dayNum, 'type' => $dataType ) ) )
								);
							}
						?>
						</ul>
					</div>
				</div>
			</div>
			<!--/ choose_day -->
		<?php }?>
	</div>
	
	<div class="position-relative spinner-margin CurrencyRatesArchiveListWidget">
	<?php
		$this->widget( 'widgets.lists.CurrencyRatesArchiveListWidget', Array(
			'ns' => 'nsActionView',
			'year' => $filterData['choosedYear'],
			'mo' => $filterData['choosedMo'],
			'day' => $filterData['choosedDay'],
			'dataType' => $dataType,
		));
	?>
	</div>
	
</div>
<script>

!function( $ ) {
	var ns = <?=$ns?>;
		
	
	ns.wCurrencyRatesArchiveWidget = wCurrencyRatesArchiveWidgetOpen({
		selector: '.<?=$ns?> .<?=$ins?>',
		archiveDataUrl: '<?=Yii::app()->createUrl('currencyRates/ajaxLoadArchiveListData')?>',
	});
}( window.jQuery );

</script>