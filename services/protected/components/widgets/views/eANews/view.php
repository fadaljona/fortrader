<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wEANewsWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="widget-box wEANewsWidget" style="border-bottom:none;">
	<div class="widget-header">
		<h4 class="lighter smaller">
			<i class="icon-rss orange"></i>
			<?=Yii::t( $NSi18n, 'Activity' )?>
		</h4>
		<span class="widget-toolbar">
			<a href="#" class="<?=$ins?> wReload"><i class="icon-refresh"></i></a>
		</span>
	</div>
	<div class="widget-body">
		<div class="widget-main no-padding">
			<div class="idLastActivity <?=$ins?> wNewsList">
				<?$this->renderNews()?>
			</div>
			<div class="<?=$ins?> wTplItem" style="display:none; padding-bottom:14px;">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</div>
		</div>
	</div>
	<?if( $this->issetMore()){?>
		<div class="text-center" style="padding-top:14px;">
			<button class="btn btn-small btn-yellow no-radius <?=$ins?> wShowMore">
				<i class="icon-arrow-down"></i>
				<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Show more' )?></span>
			</button>
		</div>
	<?}?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var ins = ".<?=$ins?>";
		var nsText = ".<?=$ns?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
				
		ns.wEANewsWidget = wEANewsWidgetOpen({
			ins: ins,
			selector: nsText+' .wEANewsWidget',
			ajax: ajax,
		});
	}( window.jQuery );
</script>