<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wContestStatsWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="<?=$ns?>">
<div class="statistics_box pull-left wContestStatsWidget">
	<h3 class="header smaller lighter blue"><?=Yii::t( '*', 'Contest Stats' )?></h3>
	<div class="clearfix">
		<div class="statistics_left pull-left">
			<div class="lider_statistics l_statistics_competition" data-val="<?=$model->wleaderMember->stats->gain?>" data-fullval="<? echo $model->wleaderMember->stats->gain+1000; ?>" data-color="#fec16b">
				<p class="lider_statistics_text">
					<span><?=Yii::t( '*', 'Profit' )?>:</span>
					<span class="lider_statistics_val l_statistics_val"></span>
				</p>
				<canvas id="statistics_graph_bg1" class="statistics_graph_bg statistics_graph_bg0" with="100%" height="100%"></canvas>
				<canvas id="statistics_graph1" class="statistics_graph" with="100%" height="100%"></canvas>
			</div>
			<p><?=Yii::t( '*', 'Current leader' )?>:</p>
			<span class="leader-link"><?=CHtml::link( CHtml::encode( $model->wleaderMember->user->showName ), $this->controller->createURL( '/contestMember/single', array( 'id' => $model->leaderMember ) ), array('class' => 'lider_statistics_name'))?></span>
		</div>
		<div class="statistics_right pull-left">
			<div class="clearfix mb20">
				<div class="scale_box totalProfit pull-left" data-val="<?=round($model->totalProfit,0)?>" data-full="<? echo round($model->totalProfit,0)+$model->totalTraders*$model->contest->startDeposit;?>">
					<div class="clearfix mb10">
						<div class="scale_text pull-left"><?=Yii::t( '*', 'Total profit' )?>:</div>
						<div class="scale_text_val pull-right">+<span class='profit-summ0'></span></div>
					</div>
					<div class="scale_box_item">
						<div class="scale_box_item_inner scale_box_item_inner0" style="background-color:#c86554;"></div>
					</div>
				</div>
				<div class="scale_box totalLoss pull-left ml40" data-val="<?=abs(round($model->totalLoss,0))?>" data-full="<? echo round($model->totalProfit,0)+$model->totalTraders*$model->contest->startDeposit;?>">
					<div class="clearfix mb10">
						<div class="scale_text pull-left"><?=Yii::t( '*', 'Total loss' )?>:</div>
						<div class="scale_text_val pull-right">-<span class='profit-summ1'></span></div>
					</div>
					<div class="scale_box_item">
						<div class="scale_box_item_inner scale_box_item_inner1" style="background-color:#74aed5;"></div>
					</div>
				</div>
			</div>
			<ul class="statistics_list">
				<li class="statistics_list_item clearfix">
					<div class="statistics_list_property"><?=Yii::t( '*', 'All committed transactions' )?>:</div>
					<div class="statistics_list_value committedDeals"><?=number_format($model->committedDeals, 0, '.', ' ')?></div>
				</li>
				<li class="statistics_list_item clearfix">
					<div class="statistics_list_property"><?=Yii::t( '*', 'Best deal' )?>:</div>
					<div class="statistics_list_value bestDeal">+ <?=number_format($model->bestDeal, 2, '.', ' ')?> <?=Yii::t( '*', 'pips' )?> </div>
				</li>
				<li class="statistics_list_item clearfix">
					<div class="statistics_list_property"><?=Yii::t( '*', 'The best ratio of profit/risk' )?>:</div>
					<div class="statistics_list_value bestProfitRiskMember"><?=CHtml::link( CHtml::encode( $model->wbestProfitRiskMember->user->showName ), $this->controller->createURL( '/contestMember/single', array( 'id' => $model->bestProfitRiskMember )))?></div>
				</li>
				<li class="statistics_list_item clearfix">
					<div class="statistics_list_property"><?=Yii::t( '*', 'The last deal' )?>:</div>
					<div class="statistics_list_value lastDeal">
					<?php 
						if($model->lastDealOrderSymbol){
							echo Yii::t( '*', 'SELL' );
							echo ' '.$model->lastDealOrderSymbol.' -';
						}else{
							echo Yii::t( '*', 'BUY' );
							echo ' '.$model->lastDealOrderSymbol.' +';
						}  ?>
						 <?=number_format(abs($model->lastDealOrderOpenPrice-$model->lastDealOrderClosePrice), 4, '.', ' ')?> <?=Yii::t( '*', 'pips' )?>
					</div>				
				</li>
			</ul>
						
			<div class="statistics_block clearfix">
				<div class="statistics_block_item">
					<div class="statistics_block_item_inner l_statistics_competition Traders activeTraders" data-val="<?=$model->activeTraders?>" data-fullval="<?=$model->totalTraders?>" data-color="#98c191">
						<p class="statistics_block_text statistics_block_text1 l_statistics_val"></p>
						<canvas id="statistics_graph_bg2" class="statistics_graph_bg statistics_graph_bg1" with="100%" height="100%"></canvas>
						<canvas id="statistics_graph2" class="statistics_graph" with="100%" height="100%"></canvas>
					</div>
					<h6 class="statistics_block_title"><?=Yii::t( '*', 'Active traders' )?></h6>
				</div>

				<div class="statistics_block_item">
					<div class="statistics_block_item_inner l_statistics_competition Traders profitTraders" data-val="<?=$model->profitTraders?>" data-fullval="<?=$model->totalTraders?>" data-color="#84bde3">
						<p class="statistics_block_text statistics_block_text2 l_statistics_val"></p>
						<canvas id="statistics_graph_bg3" class="statistics_graph_bg statistics_graph_bg2" with="100%" height="100%"></canvas>
						<canvas id="statistics_graph3" class="statistics_graph" with="100%" height="100%"></canvas>
					</div>
					<h6 class="statistics_block_title"><?=Yii::t( '*', 'Traders in profit' )?></h6>
				</div>

				<div class="statistics_block_item">
					<div class="statistics_block_item_inner l_statistics_competition Traders lossTraders" data-val="<?=$model->lossTraders?>" data-fullval="<?=$model->totalTraders?>" data-color="#d77362">
						<p class="statistics_block_text statistics_block_text3 l_statistics_val"></p>
						<canvas id="statistics_graph_bg4" class="statistics_graph_bg statistics_graph_bg3" with="100%" height="100%"></canvas>
						<canvas id="statistics_graph4" class="statistics_graph" with="100%" height="100%"></canvas>
					</div>
					<h6 class="statistics_block_title"><?=Yii::t( '*', 'Traders lose out' )?></h6>
				</div>
			</div>

		</div>
	</div>
</div>
</div>
<script>
	!function( $ ) {
		var ns = ".<?=$ns?>";
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idContest = <?=$model->idContest?>;		
		var ls = <?=json_encode( $jsls )?>;
		<?php if( $model->contest->status != 'completed' ){ ?>
		var startTimer = true;
		<?php }else{ ?>
		var startTimer = false;
		<?php } ?>
				
		ns.wContestStatsWidget = wContestStatsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wContestStatsWidget',
			ls: ls,
			idContest: idContest,
			startTimer: startTimer,
		});
	}( window.jQuery );
</script>