<?php
if( $viewFilesDir == 'quotes,currencies,cbrfMetals' ){
	
	$i=1;
	foreach( $currentColors as $key => $color ){
		
		if( $i % 4 == 1 ){
			echo CHtml::openTag( 'div', array( 'class' => 'clearfix ' ) );
		}
		
		echo CHtml::openTag('div', array( 'class' => 'change_color_input ' ) );
			echo CHtml::openTag('div', array( 'class' => 'col3' ) );
				echo CHtml::tag('label', array(), Yii::t('informerColors', $key));
				
				echo CHtml::openTag('div', array( 'class' => 'positionRelative' ));
					echo CHtml::textField( $key , '', array( 'placeholder' => $color, 'readonly' => true ));
					echo CHtml::tag('div', array( 'class' => 'colorSelector', 'data-bg' => $color  ), '');
				echo CHtml::closeTag('div');
				
			echo CHtml::closeTag('div');

		echo CHtml::closeTag('div');
		
		if( $i % 4 == 0 ) echo CHtml::closeTag( 'div');
		$i++;
	}
	
	if( ($i-1) %4 !=0 )  echo CHtml::closeTag( 'div');

}elseif( $viewFilesDir == 'converter' ){

	$i=1;
	foreach( $currentColors as $key => $color ){
		if( strpos( $color, '#' ) === false || strpos( $color, '#' ) != 0 ) continue;
		if( $i % 4 == 1 ){
			echo CHtml::openTag( 'div', array( 'class' => 'row ' ) );
		}
		
		echo CHtml::openTag('div', array( 'class' => 'grid_3 grid_md_6 grid_sm_12 change_informer_color_el ' ) );
			echo CHtml::openTag('div', array( 'class' => 'informer_setings' ) );
				echo CHtml::openTag('div', array( 'class' => 'col3' ));
					echo CHtml::tag('label', array(), Yii::t('informerColors', $key));
					echo CHtml::textField( $key , '', array( 'placeholder' => $color, 'readonly' => true ));
				echo CHtml::closeTag('div');
				echo CHtml::tag('div', array( 'class' => 'colorSelector', 'data-bg' => $color ), '');
			echo CHtml::closeTag('div');
		echo CHtml::closeTag('div');
		
		if( $i % 4 == 0 ) echo CHtml::closeTag( 'div');
		$i++;
	}
	
	if( ($i-1) %4 !=0 )  echo CHtml::closeTag( 'div');
	
}elseif( $viewFilesDir == 'monitoring' ){
	$i=1;
	foreach( $currentColors as $key => $color ){
		
		if( strpos( $key, 'Color' ) === false ) continue;
		if( $i % 4 == 1 ){
			echo CHtml::openTag( 'div', array( 'class' => 'clearfix ' ) );
		}
		
		echo CHtml::openTag('div', array( 'class' => 'change_color_input ' ) );
			echo CHtml::openTag('div', array( 'class' => 'col3' ) );
				echo CHtml::tag('label', array(), Yii::t('informerColors', $key));
				
				echo CHtml::openTag('div', array( 'class' => 'positionRelative' ));
					echo CHtml::textField( $key , '', array( 'placeholder' => $color, 'readonly' => true ));
					echo CHtml::tag('div', array( 'class' => 'colorSelector', 'data-bg' => $color  ), '');
				echo CHtml::closeTag('div');
				
			echo CHtml::closeTag('div');

		echo CHtml::closeTag('div');
		
		if( $i % 4 == 0 ) echo CHtml::closeTag( 'div');
		$i++;
	}
	
	if( ($i-1) %4 !=0 )  echo CHtml::closeTag( 'div');
}elseif( $viewFilesDir == 'news' ){
	
	$i=1;
	foreach( $currentColors as $key => $color ){
		
		if( $i % 4 == 1 ){
			echo CHtml::openTag( 'div', array( 'class' => 'clearfix ' ) );
		}
		
		echo CHtml::openTag('div', array( 'class' => 'change_color_input ' ) );
			echo CHtml::openTag('div', array( 'class' => 'col3' ) );
				echo CHtml::tag('label', array(), Yii::t('informerColors', $key));
				
				echo CHtml::openTag('div', array( 'class' => 'positionRelative' ));
					echo CHtml::textField( $key , '', array( 'placeholder' => $color, 'readonly' => true ));
					echo CHtml::tag('div', array( 'class' => 'colorSelector', 'data-bg' => $color  ), '');
				echo CHtml::closeTag('div');
				
			echo CHtml::closeTag('div');

		echo CHtml::closeTag('div');
		
		if( $i % 4 == 0 ) echo CHtml::closeTag( 'div');
		$i++;
	}
	
	if( ($i-1) %4 !=0 )  echo CHtml::closeTag( 'div');

}
?>