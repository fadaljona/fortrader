<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="exchange-rate-bars-container clearfix <?=$ins?>">
<?php
	if( $similars ){
		echo CHtml::openTag('ul', array( 'class' => "exchange-links" ));
			echo CHtml::tag('li', array( 'class' => 'exchange-links-header' ), Yii::t( $NSi18n, 'Related queries' ) );
			foreach( $similars as $similar ){
				echo CHtml::tag('li', array(), CHtml::link( $similar->modelTitle, $similar->singleUrl ) );
			}
		echo CHtml::closeTag('ul');
	}
	if($inOtherCurrencies) {
		echo CHtml::openTag('ul', array( 'class' => "exchange-links" ));
			echo CHtml::tag('li', array( 'class' => 'exchange-links-header' ), Yii::t( $NSi18n, 'In other currencies' ) );
			foreach( $inOtherCurrencies as $other ){
				echo CHtml::tag('li', array(), CHtml::link( $other->modelTitle, $other->singleUrl ) );
			}
		echo CHtml::closeTag('ul');
	}
?>
</div>
