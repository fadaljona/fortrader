<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wContestProfileWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lErrorAgreed' => 'You did not agree with the terms of the competition!',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="wContestProfileWidget">
	<div class="pull-left" style="max-width:300px; margin-right:20px;margin-bottom:20px;">
		<?=$model->getImage()?>
		<div class="space-4"></div>
		<? require dirname( __FILE__ ).'/_status.php'; ?>
	</div>
	<div class="pull-left" style="margin-left:0px;">
		<? require dirname( __FILE__ ).'/_buttons.php'; ?>
		<div class="space-10"></div>
		
		<div class="row-fluid">
			<div class="pull-left" style="width:320px; margin-right:20px;">
				<? require dirname( __FILE__ ).'/_rules.php'; ?>
				<div class="hr hr12 dotted"></div>
				<div class="clearfix text-center">
					<div class="grid2">
						<span class="bigger-175 blue"><?=CommonLib::numberFormat( $model->getCountPrizes() )?></span>
						<br>
						<?=Yii::t( '*', 'Prizes' )?>
					</div>
					<div class="grid2">
						<span class="bigger-175 green">$<?=CommonLib::numberFormat( $model->getFirstPrize() )?></span>
						<br>
						<?=Yii::t( '*', 'First place' )?>
					</div>
				</div>
				<div class="hr hr12 dotted"></div>
			</div>
		</div>
	</div>
	<div class="iClear"></div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
				
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wContestProfileWidget = wContestProfileWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wContestProfileWidget',
			ls: ls,
		});
	}( window.jQuery );
</script>