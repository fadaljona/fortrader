<div class="iBtn i11 i11_1">
	<div>
		<span class="iSpan i06">$<?=CommonLib::numberFormat( $model->sumPrizes )?></span><br>
		<span class="iSpan i07"><?=Yii::t( '*', 'Prize fund' )?></span>
	</div>
</div>
<div class="iBtn i11 i11_2">
	<div>
		<span class="iSpan i06"><?=CommonLib::numberFormat( $model->countMembers )?></span><br>
		<span class="iSpan i07"><?=Yii::t( '*', 'Members' )?></span>
	</div>
</div>
<div class="iBtn i11 i11_3">
	<div>
		<span class="iSpan i06"><?=CommonLib::numberFormat( $model->views )?></span><br>
		<span class="iSpan i07"><?=Yii::t( '*', 'Views' )?></span>
	</div>
</div>