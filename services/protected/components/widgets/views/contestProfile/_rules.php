<?
	$brokers = $model->getBrokers();
?>
<div class="iDiv i45">
	<?=Yii::t( '*', 'Contest Rules' )?>
</div>
<div class="iDiv i46">
	<table style="width:100%" class="iTable i01">
		<col width="50%">
		<col width="50%">
		<?if( $brokers ){?>
			<tr>
				<td><?=Yii::t( '*', 'Sponsor' )?>:</td>
				<td>
					<?=$brokers[0]->getThumbImage( Array( 'width' => 25 ))?>
					&nbsp;<?=CHtml::link( CHtml::encode( $brokers[0]->brandName ), $brokers[0]->getSingleURL(), Array( 'class' => 'iA i20' ))?>
				</td>
			</tr>
		<?}?>
		<?if( strlen( $model->modeEA )){?>
			<tr>
				<td><?=Yii::t( '*', 'EA' )?>:</td>
				<td>
					<strong><?=Yii::t( '*', $model->modeEA )?></strong>
				</td>
			</tr>
		<?}?>
		<tr>
			<td><?=Yii::t( '*', 'Begin' )?>:</td>
			<td>
				<strong><?=$this->formatDate( $model->begin )?></strong>
			</td>
		</tr>
		<tr>
			<td><?=Yii::t( '*', 'End' )?>:</td>
			<td>
				<strong><?=$this->formatDate( $model->end )?></strong>
			</td>
		</tr>
	</table>
</div>