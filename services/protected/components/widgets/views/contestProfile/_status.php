<?
	$statusStartedNotRegistered = false;
    if( !Yii::App()->user->isGuest and Yii::App()->user->getModel()->isRegisteredInContest( $model->id )){
        $model->status = 'registered';
    }elseif( $model->status == 'started' ){
		$statusStartedNotRegistered = true;
	}

    $contestMember = ContestModel::getContestMemberByContestId($model->id);
?>
<?if( $model->status == 'registration' ){?>
	<div class="iDiv i44 i44_1">
		<i class="iI i27 i27_1"></i>
		<?=Yii::t( '*', 'Contest opened' )?>
	</div>
	<div class="space-2"></div>
	<a class="btn btn-block btn-success <?=$ins?> wRegister" href="<?=ContestModel::getRegisterMemberURL( $model->id )?>">
		<strong><?=Yii::t( '*', 'Registration' )?></strong>
	</a>
	<div class="space-6"></div>
	
	<?if( strlen( $model->urlRules )){?>
		<div>
			<?=CHtml::checkBox( 'agreed', false, Array( 'class' => "{$ins} wAgreed" ))?>
			<span class="lbl">
				<?=Yii::t( '*', 'Reviewed and agree to {link}the terms of the competition{/link}', Array( 
					'{link}' => CHtml::openTag( 'a', Array( 'target' => '_blank', 'href' => CommonLib::getUrl( $model->urlRules ))),
					'{/link}' => CHtml::closeTag( 'a' ),
				))?>
			</span>
		</div>
	<?}?>
<?} elseif($model->status == 'registration stoped') {?>
    <a class="btn btn-block btn-inverse <?=$ins?> wRegister" href="#" onclick="return false;">
        <strong><?=Yii::t( '*', 'Registration stoped' )?></strong>
    </a>
    <div class="space-6"></div>	
	<? if( strlen( $model->urlRules )){?>
        <div>
            <span class="lbl">
				<?=Yii::t( '*', '{link}The terms of the competition{/link}', Array(
                        '{link}' => CHtml::openTag( 'a', Array( 'target' => '_blank', 'href' => CommonLib::getUrl( $model->urlRules ))),
                        '{/link}' => CHtml::closeTag( 'a' ),
                ))?>
			</span>
        </div>
    <?}?>
<?} elseif($model->status == 'completed') {?>
    <a class="btn btn-block btn-inverse <?=$ins?> wRegister" href="#" onclick="return false;">
        <strong><?=Yii::t( '*', 'Contest is completed' )?></strong>
    </a>
    <div class="space-6"></div>
	<? if( strlen( $model->urlRules )){?>
        <div>
            <span class="lbl">
				<?=Yii::t( '*', '{link}The terms of the competition{/link}', Array(
                        '{link}' => CHtml::openTag( 'a', Array( 'target' => '_blank', 'href' => CommonLib::getUrl( $model->urlRules ))),
                        '{/link}' => CHtml::closeTag( 'a' ),
                ))?>
			</span>
        </div>
    <?}?>
<?} elseif($model->status == 'registered') {
    if($contestMember) {
    ?>
        <a class="btn btn-block btn-primary <?= $ins ?> wRegister"
           href="<?= ContestMemberModel::getModelSingleURL($contestMember->id) ?>">
            <strong><?= Yii::t('*', 'You are registered') ?></strong>
        </a>
        <div class="space-6"></div>
    <?
    }
        if( strlen( $model->urlRules )){?>
        <div>
            <?=CHtml::checkBox( 'agreed', true, Array( 'class' => "{$ins} wAgreed", 'disabled' => 'disabled' ))?>
            <span class="lbl">
				<?=Yii::t( '*', 'Reviewed and agree to {link}the terms of the competition{/link}', Array(
                        '{link}' => CHtml::openTag( 'a', Array( 'target' => '_blank', 'href' => CommonLib::getUrl( $model->urlRules ))),
                        '{/link}' => CHtml::closeTag( 'a' ),
                ))?>
			</span>
        </div>
    <?}?>
<?}elseif($statusStartedNotRegistered){?>
	<a class="btn btn-block btn-success <?=$ins?> wRegister" href="#" onclick="return false;">
        <strong><?=Yii::t( '*', 'Contest is started' )?></strong>
    </a>
    <div class="space-6"></div>
	<? if( strlen( $model->urlRules )){?>
        <div>
            <span class="lbl">
				<?=Yii::t( '*', '{link}The terms of the competition{/link}', Array(
                        '{link}' => CHtml::openTag( 'a', Array( 'target' => '_blank', 'href' => CommonLib::getUrl( $model->urlRules ))),
                        '{/link}' => CHtml::closeTag( 'a' ),
                ))?>
			</span>
        </div>
    <?}?>
<?}?>
