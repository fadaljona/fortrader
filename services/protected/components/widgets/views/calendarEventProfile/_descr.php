<div class="quote_marker">
	<h5 class="quote_marker_title"><?=Yii::t( '*', 'Importance' )?>: <?php
	
	$this->widget( 'widgets.WebRatingWidget', Array(
		'ns' => $ns,
		'currentValue' => $model->nextValue->serious,
		'numStars' => 3,
		'onChangeValue' => '',
		'readOnly' => true,
		'starWidth' => 12,
		'maxValue' => 3,
		'ratedFill' => '#FF1616',
		'wrapperStyle' => 'display:inline-block !important;',
	));
	?></h5>
	<?php
		if( $model->description ){
			echo "<div class='quote_marker_txt'>" . $model->description . "</div>" . CHtml::tag('i', array('class' => 'fa fa-quote-right quote_marker_icon', 'aria-hidden' => 'true'), ' ');
		}
	?>
</div>
