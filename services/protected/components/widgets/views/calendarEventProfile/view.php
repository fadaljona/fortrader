<?php
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wCalendarEventProfileWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
?>


<div class="<?=$ins?>">

	<?php require dirname( __FILE__ ).'/_buttons.php' ?>
	<?php require dirname( __FILE__ ).'/_descr.php' ?>
	
<?php if( $model->prevValue && $model->prevValue->fact_value ){ ?>
	<h5 class="description_title"><?=$model->chartTitle?></h5>
	<?php if( $model->chartSubTitle ) echo CHtml::tag('p', array('class' => 'description_subtitle'), $model->chartSubTitle) ?>
	<div class="table_info_wrapp clearfix">
	
		<div class="description_table">
		<?php
			$this->widget( 'widgets.graphs.CalendarEventGraphWidget', Array(
				'ns' => 'nsActionView',
				'event' => $model->nextValue,
			));
		?>
		</div>
		<div class="description_history_wrapp">
			<?php
				$this->widget( 'widgets.lists.CalendarEventShortHistoryListWidget', Array(
					'ns' => 'nsActionView',
					'model' => $model
				));
			?>
		</div>
	</div>
<?php }?>	
	
	<?php
		$this->widget( 'widgets.CalendarComingEventsWidget', Array(
			'ns' => 'nsActionView',
			'excludeId' => $model->nextValue->id,
			'title' => Yii::t($NSi18n, 'Coming events')
		));
	
		$listFuture = $this->createWidget( 'widgets.lists.CalendarEventsList2Widget', Array(
			'ns' => 'nsActionView',
			'period' => 'futureOfEvent',
			'idIndicator' => $model->indicator_id,
			'fromEventDt' => $model->nextValue->dt,
		));
		$listFuture->createGridWidget()->run();
	?>


</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
				
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wCalendarEventProfileWidget = wCalendarEventProfileWidgetOpen({
			ns: ns,
			ins: ins,
			selector: '.<?=$ins?>',
			ls: ls,
		});
	}( window.jQuery );
</script>