<? 
if ( isset($_GET['y']) ) {
	$sel = 1;
}else{
	$sel = 0;
}
Yii::app()->clientScript->registerScript(
	'ca_single_history', 
	"
		var cahistory = {
			render:function (limit) {
				var load = $('#h_loader');
				var btn = $('#history_more_btn');
				if ( !limit ) {
					var limit = 12;
				}
				$.ajax({
					type: 'GET',
					url: '/services/calendarEvent/history',
					data:'id=".$model->indicator_id."&limit='+limit+'&sel=".$sel."', 
					beforeSend:function () {
						load.fadeIn();
						btn.hide();
					},
					success: function(data){
						$('#hist_res').replaceWith(data);
					}
				});
			}
		};
	", 
	CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript(
	'ca_single_history_ready', 
	"cahistory.render();", 
	CClientScript::POS_READY);
?>

<div id="hist_res"></div>
