<?php

	if( $model->nextValue->timeLeftInSeconds ){
		
		$days = ($model->nextValue->timeLeftInSeconds - $model->nextValue->timeLeftInSeconds % (60*60*24)) / (60*60*24);
		$jsTime = $model->nextValue->timeLeftInSeconds * 1000;
		
		Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::App()->params['wpThemePath'].'/plagins/jquery.countdown.min.js' ), CClientScript::POS_END );
		Yii::App()->clientScript->registerScript('countdownJs', '
		!function( $ ) {
			if($(".clock2").length){
				var i = new Date().getTime() + '.$jsTime.'; //in seconds 1s = 1000
				$(".clock2").countdown(i, {elapse: false}).on("update.countdown", function(event) {
						var $this = $(this);
						if( event.offset.totalDays != 0 )
							$this.html(event.strftime(\'<p class="event_waiting_txt1 withDays black">%D '.Yii::t( '*', 'day|days', $days ).' %H:%M<span class="seconds">:%S</span></p>\'));
						else
							$this.html(event.strftime(\'<p class="event_waiting_txt1 black">%H:%M<span class="seconds">:%S</span></p>\'));
					}).on("finish.countdown", function(event) {
						location.reload();
					});
			}
		}( window.jQuery );
		', CClientScript::POS_END);
		
		$statusHtml = '
			<div class="event_waiting_box bg_yellow">
				<div class="clock2"></div>
				<p class="event_waiting_txt2">' . Yii::t( $NSi18n, 'Left' ) . '</p>
				<i class="fa fa-clock-o event_waiting_icon" aria-hidden="true"></i>
			</div>
		';
	}else{
		$statusHtml = '
			<div class="event_waiting_box bg_green">
				<p class="event_waiting_txt1 green2">' . Yii::t( $NSi18n, 'Event released' ) . '</p>
				<i class="fa fa-check event_waiting_check" aria-hidden="true"></i>
			</div>
		';
	}
	
	
	
	if( $model->nextValue->factValue ){
		$factValueHtml = CHtml::tag('p', array('class' => 'event_waiting_txt1 blue'), $model->nextValue->factValue);
	}else{
		if( !$model->nextValue->timeLeftInSeconds )
			$factValueHtml = CHtml::tag('p', array('class' => 'event_waiting_txt1 green2'), '---');
		else
			$factValueHtml = CHtml::tag('p', array('class' => 'event_waiting_txt1 green2'), Yii::t( $NSi18n, 'Expected' ));
	}
	
	if( $model->nextValue->mbValue ){
		$mbValueHtml = CHtml::tag('p', array('class' => 'event_waiting_txt1 blue'), $model->nextValue->mbValue);
	}else{
		if( !$model->nextValue->timeLeftInSeconds )
			$mbValueHtml = CHtml::tag('p', array('class' => 'event_waiting_txt1 green2'), '---');
		else
			$mbValueHtml = CHtml::tag('p', array('class' => 'event_waiting_txt1 green2'), Yii::t( $NSi18n, 'Expected' ));
	}
	
	if( $model->nextValue->beforeValue ){
		$beforeValueHtml = CHtml::tag('p', array('class' => 'event_waiting_txt1 blue'), $model->nextValue->beforeValue);
	}else{
		if( !$model->nextValue->timeLeftInSeconds )
			$beforeValueHtml = CHtml::tag('p', array('class' => 'event_waiting_txt1 green2'), '---');
		else
			$beforeValueHtml = CHtml::tag('p', array('class' => 'event_waiting_txt1 green2'), Yii::t( $NSi18n, 'Expected' ));
	}
?>

	<div class="event_waiting_wrapp clearfix">
		<div class="event_waiting_col">
			<div class="event_waiting_box">
				<?=$factValueHtml?>
				<p class="event_waiting_txt2"><?=Yii::t( $NSi18n, 'Fact value' )?></p>
			</div>
		</div>
		<div class="event_waiting_col">
			<div class="event_waiting_box">
				<?=$mbValueHtml?>
				<p class="event_waiting_txt2"><?=Yii::t( $NSi18n, 'Forecast' )?></p>
			</div>
		</div>
		<div class="event_waiting_col">
			<div class="event_waiting_box">
				<?=$beforeValueHtml?>
				<p class="event_waiting_txt2"><?=Yii::t( $NSi18n, 'Before value' )?></p>
			</div>
		</div>
		<div class="event_waiting_col">
			<?=$statusHtml?>
		</div>
	</div>