<div class="page-header" style="margin-bottom:0px;">
	<h1>
		<?=Yii::t( $NSi18n, "News event" )?>
	</h1>
</div>
<? $news = array_reverse($news); ?>
<?foreach( $news as $post ){?>
	<div class="iDiv i55">
		<div class="iDiv i56">
			<? require dirname( __FILE__ ).'/_news_ago.php' ?>
		</div>
		<div class="iDiv i57">
			<a href="<?=$post->getShortUrl()?>" class="iA i24" target="_blank" style="display:inline-block; margin:0 20px 0 0;"><?=$post->post_title?></a>
			<div style="display:inline-block;">
				<div style="display:inline-block; padding:0 10px 0 0;">
					<?=$post->author->showName?>
				</div>
				<?if( $post->categories ){?>
					<div class="iDiv i58" style="display:inline-block; padding:0 10px;">
						<a class="iA i25" href="<?=$post->categories[0]->getCategoryURL()?>" target="_blank">
							<?=$post->categories[0]->name?>
						</a>
					</div>
				<?}?>
			</div>
			<br>
			<?=$post->filterContent()?>
		</div>
		<div class="iClear"></div>
	</div>
<?}?>
