<? 
Yii::app()->clientScript->registerScript(
	'ca_single_coming', 
	"
		var coming = {
			limit:0, 
			render:function (limit) {
				var el = $('#coming_res_div');
				var load = $('#c_loader');
				var btn = $('#coming_more_btn');
				if ( !limit ) {
					var limit = 3;
				}
				var lh = el.height();
				$.ajax({
					type: 'GET',
					url: '/services/calendarEvent/coming',
					data:'limit='+limit, 
					beforeSend:function () {
						load.fadeIn();
						btn.hide();
					},
					success: function(data){
						load.fadeOut();
						el.html(data);
					}
				});
			}
		};
	", 
	CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript(
	'ca_single_coming_ready', 
	"coming.render();", 
	CClientScript::POS_READY);
?>
<div id="coming_res_div" class="row-fluid" style="margin-top:20px; position:relative;"></div>
