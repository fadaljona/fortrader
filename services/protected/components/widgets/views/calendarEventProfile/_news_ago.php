<?
	$time = $post->post_date;
	$time = is_numeric( $time ) ? $time : @strtotime( $time );
	if( !$time ) return;
	
	$currentTime = time();
	$diff = $currentTime - $time;
	$diff = max( $diff , 0 );
	list( $seconds, $minets, $hours, $days, $months, $years ) = CommonLib::explodeTimeDiff( $diff );
	
	$NSi18n = $this->getNSi18n();
	
	$title = "";
	$value = 0;
	$ago = 'ago';
	if( $seconds == 0 ) {
		$value = Yii::t( $NSi18n, 'Now' );
		$title = '';
		$ago = '';
	}
	elseif( $minets == 0 ) {
		$value = $seconds;
		$title = Yii::t( $NSi18n, 'sec', $value );
	}
	elseif( $hours == 0 ) {
		$value = $minets;
		$title = Yii::t( $NSi18n, 'min', $value );
	}
	elseif( $days == 0 ) {
		$value = $hours;
		$title = Yii::t( $NSi18n, 'hour|hours', $value );
	}
	elseif( $months == 0 ) {
		$value = $days;
		$title = Yii::t( $NSi18n, 'day|days', $value );
	}
	elseif( $years == 0 ) {
		$value = $months;
		$title = Yii::t( $NSi18n, 'month|months', $value );
	}
	else{
		$value = $years;
		$title = Yii::t( $NSi18n, 'year|years', $value );
	}
?>
<span class="iSpan i11"><?=$value?></span><br>
<span class="iSpan i12"><?=$title?> <?=Yii::t( $NSi18n, $ago )?></span>