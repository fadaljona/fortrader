<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	/*
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lLoading' => 'Loading...',
		'lLike' => 'Like',
		'lDisLike' => 'Dislike',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	*/
?>
<div class="widget-box wPostInformWidget">
	<div class="widget-header widget-header-small">
		<h5 class="smaller">
			<?=Yii::t( $NSi18n, "Editor recommends" )?>
		</h5>
		<span class="widget-toolbar">
			<a href="#" class="<?=$ins?> wLeft"><i class="icon-chevron-left"></i></a>
			&nbsp;<a href="#" class="<?=$ins?> wRight"><i class="icon-chevron-right"></i></a>
		</span>
	</div>
	<div class="widget-body <?=$ins?> wBody">
		<div class="<?=$ins?> wPosts">
			<?foreach( $posts as $post ){?>
				<a href="<?=$post->getShortUrl()?>" class="<?=$ins?> wPost" target="_blank">
					<div class="c01">
						<?=$post->getThumbnail()?><br>
					</div>
					<div class="c02">
						<?=CommonLib::shorten( $post->post_title, 50 )?>
						<?if( $post->categories ){?>
							<span class="iSpan i08"><?=$post->categories[0]->name?></span>
						<?}?>
					</div>
					<div class="iClear"></div>
				</a>
			<?}?>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var ins = ".<?=$ins?>";
		var nsText = ".<?=$ns?>";
				
		//var ls = <?//=json_encode( $jsls )?>;
		
		ns.wPostInformWidget = wPostInformWidgetOpen({
			ins: ins,
			selector: nsText+' .wPostInformWidget',
			//ls: ls,
		});
	}( window.jQuery );
</script>