<?
	$baseUrl = Yii::app()->baseUrl;
?>
<div class="sidebar-shortcuts" id="sidebar-shortcuts">
	<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
		<a href="<?=Yii::App()->createURL( '/' )?>" class="btn btn-small btn-success">
			<i class="icon-signal"></i>
		</a>

		<?if( !Yii::App()->user->isGuest ){?>
			<a href="<?=$editURL?>" class="btn btn-small btn-info">
				<i class="icon-pencil"></i>
			</a>

			<a href="<?=$this->controller->createURL( '/user/profile' )?>" class="btn btn-small btn-warning">
				<i class="icon-group"></i>
			</a>

			<?$url = Yii::App()->user->checkAccess( 'advertisementControl' ) ? $this->controller->createURL( '/admin' ) : $this->controller->createURL( '/user/settings?tabSettings=yes' )?>
			<a href="<?=$url?>" class="btn btn-small btn-danger">
				<i class="iI i29"></i>
			</a>
		<?}?>
	</div>

	<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
		<span class="btn btn-success"></span>

		<span class="btn btn-info"></span>

		<span class="btn btn-warning"></span>

		<span class="btn btn-danger"></span>
	</div>
</div>