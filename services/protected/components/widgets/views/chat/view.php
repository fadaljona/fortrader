<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wChatWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
?>
<div class="wChatWidget">
	<div class="row-fluid">
		<div class="span7 <?=$ins?> wChatBody">
			<div class="widget-box iChatRoomWidgetBox i01 wTplChatRoomWidgetBox">
				<div class="widget-header">
					<span class="wTitle">wTitle</span>
					<span class="widget-toolbar">
						&nbsp;<a href="#" data-action="collapse"><i class="icon-chevron-up"></i></a>
					</span>
				</div>
				<div class="widget-body wBody">
					<div class="wMessageList"></div>
					
				</div>
			</div>
		</div>
		<div class="span5" style="max-width:400px;">
			span-5
		</div>
	</div>
</div>
<script>/*
	!function( $ ) {
		var ns = <?=$ns?>;
		var ins = ".<?=$ins?>";
		var nsText = ".<?=$ns?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wChatWidget = wChatWidgetOpen({
			ins: ins,
			selector: nsText+' .wChatWidget',
			ls: ls,
		});
	}( window.jQuery );
*/</script>