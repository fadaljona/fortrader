<?php
	if( !$ajax ) Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wNewsAggregatorSourcesWidget.js' ) );
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
?>

<div class="<?=$ins?> spinner-margin position-relative">

	<div class="clearfix mb30">
		<div class="news2_col60">
			<div class="quotes_search_box_2">
				<form class="sourceSearchForm" method="get" action="./">
					<button class="quotes_search_btn"><i class="fa fa-search"></i></button>
					<input type="text" name="searchKey" class="sourceSearchKey" placeholder="<?=Yii::t($NSi18n, 'Search source' )?>" value="<?=$searchKey?>">
				</form>
			</div>
		</div>
		<div class="news2_col40 clearfix">
			<button class="news2_btn_on sources_enable_all"><?=Yii::t($NSi18n, 'Enable all')?></button>
			<div class="news2_count">
				<p class="news2_count_txt">
					<?=Yii::t($NSi18n, 'Disabled')?> <span class="news2_result sources_indicator"><?=$disableCount?></span>
				</p>
			</div>
		</div>
	</div>
	
	<?php
		$listWidget = $this->widget('widgets.listViews.RssSourcesListViewWidget', array(
			'dataProvider' => $sourcesDP,
			'template' => '{items}',
			'itemsTagName' => 'form',
			'itemsCssClass' => 'sources_container',
			'htmlOptions' => array(
				'class' => 'rssSourcesNewsList'
			),
			'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
			'pager' => array(
				'class'=> 'widgets.gridViews.base.WpPager'
			),
		));
		
	?>
	
	<?php if( $sourcesDP->pagination->getPageCount() > 1 ){ 
		echo $listWidget->renderPager();
	}?>
	
</div>

<?php if( !$ajax ){ ?>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
	
		ns.wNewsAggregatorSourcesWidget = wNewsAggregatorSourcesWidgetOpen({
			ns: ns,		
			selector: '.<?=$ins?>',
			ajaxLoadListURL: '<?=Yii::app()->createUrl('newsAggregator/ajaxLoadSourcesList')?>',
			cookieName: '<?=$cookieName?>',
		});
	}( window.jQuery );
</script>
<?php } ?>