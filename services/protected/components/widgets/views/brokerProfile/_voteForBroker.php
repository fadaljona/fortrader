<?php
$guestUser = Yii::App()->user->isGuest ? true : false;
?>
<div class="announcement brokerRating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<?=Yii::t( '*', 'Current rating is' )?>
	<span itemprop="ratingValue"><?=$model->rating?></span>
	<meta itemprop="bestRating" content="<?=$model->bestRating?>">
	<meta itemprop="worstRating" content="0">
	<meta itemprop="reviewCount" content="<?=$model->likesCount?>">
</div>

<div class="announcement">
	<div class="red_color"><?=Yii::t( '*', 'Evaluate the company' )?></div><br>
	<?=Yii::t( '*', 'To give voice for the company, click like' )?>
</div>
<div class="brokerSmWrapper">
	<div class="social_button social_button_native">
		<div id="vk_like_custom"></div>
	</div>
	<div class="social_button social_button_native">
		<div id="fb-root"></div>
		<div class="fb-like sendRating" data-href="<?=Yii::App()->request->getHostInfo() . Yii::App()->request->getUrl()?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
	</div>
	<?php /*<div class="disableSm <?php if( $guestUser ) echo 'arcticmodal'; else echo 'hide'; ?>" <?php if( $guestUser ) echo 'data-modal="#logIn"'; ?>></div>*/?>
</div>