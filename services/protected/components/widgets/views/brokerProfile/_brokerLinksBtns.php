<?php
echo CHtml::openTag('div', array( 'class' => 'btn_box2 PFDmedium' ));	
	if( strlen( $model->site )){
		echo CHtml::openTag('div', array( 'class' => 'btn_box2_inner' ));	
			echo CHtml::link( 
				Yii::t( '*', 'Go to the wesite' ), 
				$model->getRedirectURL( 'site' ),
				array(
					'class' => 'saveBrokerLinksStats',
					'data-broker-id' => $model->id,
					'data-link-type' => 'site',
					'target' => '_blank'
				)
			);
		echo CHtml::closeTag('div');
	}
	if( strlen( $model->conditions )){
		echo CHtml::openTag('div', array( 'class' => 'btn_box2_inner' ));	
			echo CHtml::link( 
				Yii::t( '*', 'Veiew conditions' ), 
				$model->getRedirectURL( 'conditions' ),
				array(
					'class' => 'saveBrokerLinksStats',
					'data-broker-id' => $model->id,
					'data-link-type' => 'conditions',
					'target' => '_blank'
				)
			);
		echo CHtml::closeTag('div');
	}
	if( strlen( $model->terms )){
		echo CHtml::openTag('div', array( 'class' => 'btn_box2_inner' ));	
			echo CHtml::link( 
				Yii::t( '*', 'Veiew reglament' ), 
				$model->getRedirectURL( 'terms' ),
				array(
					'class' => 'saveBrokerLinksStats',
					'data-broker-id' => $model->id,
					'data-link-type' => 'terms',
					'target' => '_blank'
				)
			);
		echo CHtml::closeTag('div');
	}
echo CHtml::closeTag('div');
?>