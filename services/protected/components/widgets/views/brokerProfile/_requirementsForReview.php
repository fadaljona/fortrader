<!-- - - - - - - - - - - - - - Requirements - - - - - - - - - - - - - - - - -->
<div class="requirements_wr">
	<div class="requirements_box requirements_dropdown">
		<p><?=Yii::t( '*', 'Content requirements review' )?></p>
		<div class="requirements_button requirements_dropdown_button"><span></span></div>
	</div>
	<div class="requirements_cont">
		<div class="box_wr">
			<?=Yii::t( '*', '[Content requirements review]' )?>
		</div>
	</div>
</div>
<!-- - - - - - - - - - - - - - Requirements of Search - - - - - - - - - - - - - - - - -->