<!-- - - - - - - - - - - - - - Counter-Up-master  - - - - - - - - - - - - - - - - -->
<div class="counter_wr">

<?if( $model->minDeposit !== null ){?>
	<div class="counter_box counter_box_1">
		<div class="counter_text">
			<p><strong><?=Yii::t( '*', 'Min depo' )?></strong></p>
		</div>
		<div class="counter_ins">
			<span>$</span><span class="counter"><?=$model->minDeposit?></span>
		</div>
	</div>
<?}?>
<?php
if( $model->type == 'binary' ){
	if( strlen( $model->contract )){?>
		<div class="counter_box counter_box_2">
			<div class="counter_text">
				<p><strong><?=Yii::t( '*', 'Contract from' )?></strong></p>
			</div>
			<div class="counter_ins">
				<span>$</span>
				<span class="counter"><?=$model->contract?></span>
			</div>
		</div>
	<?}
}else{
	if( strlen( $model->maxLeverage )){?>
		<div class="counter_box counter_box_2">
			<div class="counter_text">
				<p><strong><?=Yii::t( '*', 'Leverage' )?></strong></p>
			</div>
			<div class="counter_ins">
				<?php $leverage = explode( ':', $model->maxLeverage ); ?>
				<span><?=$leverage[0]?>:</span>
				<span class="counter"><?=$leverage[1]?></span>
			</div>
		</div>
	<?}
}
?>

	<div class="counter_box counter_box_3">
		<div class="counter_text_2">
			<p><?=Yii::t( '*', 'Votes' )?></p><p><strong><?=Yii::t( '*', 'Vk' )?></strong></p>
		</div>
		<div class="counter_ins">
			<span class="counter"><?=$model->vkCount?></span>
		</div>
	</div>
	<div class="counter_box counter_box_4">
		<div class="counter_text_2">
			<p><?=Yii::t( '*', 'Votes' )?></p><p><strong><?=Yii::t( '*', 'Facebook' )?></strong></p>
		</div>
		<div class="counter_ins"><span class="counter"><?=$model->fbCount?></span></div>
	</div>
	
	<div class="counter_box counter_box_5">
		<div class="counter_text">
			<p><strong><?=Yii::t( '*', 'Views' )?></strong></p>
		</div>
		<div class="counter_ins">
			<span class="counter"><?=$model->countViews?></span>
		</div>
	</div>
	
	<div class="clear"></div>
</div>
<!-- - - - - - - - - - - - - - End Counter-Up-master - - - - - - - - - - - - - - - - -->