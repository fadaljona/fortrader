<?
	$linked = Yii::App()->user->isGuest ? false : $model->linkedUser();
	$class = $linked ? 'wUnLinkFromUser' : 'wLinkToUser';
	$title = $linked ? 'I do not trade here' : 'I trade here';
	$count = $model->getCountLinkedUsers();
	$guestUser = Yii::App()->user->isGuest ? true : false;
?>
<div class="message_box activeBtn <?=$class?> <?php if( $guestUser ) echo 'arcticmodal'; ?>" <?php if( $guestUser ) echo 'data-modal="#logIn"'; ?>>
	<img src="<?=Yii::App()->params['wpThemeUrl']?>/images/nwe_img/trading_here_ico.jpg" height="44" width="44" alt="">
	<span><?=Yii::t( '*', $title )?></span>
	<div class="activeBtnDisabled hide"></div>
</div>

<div class="message_box wCountLinkedUsers">
	<img src="<?=Yii::App()->params['wpThemeUrl']?>/images/nwe_img/people_online_ico.jpg" height="44" width="44" alt="">
	<span>	
		<?=Yii::t( '*', "{n} client on the site|{n} clients on the site", '<span class="red_color">' . $count . '</span>' )?>
	</span>
</div>