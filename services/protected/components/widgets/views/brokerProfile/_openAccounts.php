<?php
if( strlen( $model->openAccount )){
	echo CHtml::openTag('div', array( 'class' => 'btn_box1 PFDmedium' ));	
		echo CHtml::link( 
			Yii::t( '*', 'Open account' ), 
			$model->getRedirectUrl( "openAccount" ), 
			array(
				'class' => 'saveBrokerLinksStats btn_type1 red',
				'data-broker-id' => $model->id,
				'data-link-type' => 'openAccount',
				'target' => '_blank'
			)
		);
	echo CHtml::closeTag('div');
}
if( strlen( $model->openDemoAccount )){
	echo CHtml::openTag('div', array( 'class' => 'btn_box1 PFDmedium' ));	
		echo CHtml::link( 
			Yii::t( '*', 'Open demo account' ), 
			$model->getRedirectUrl( "openDemoAccount" ), 
			array(
				'class' => 'saveBrokerLinksStats btn_type1 grey',
				'data-broker-id' => $model->id,
				'data-link-type' => 'openDemoAccount',
				'target' => '_blank'
			)
		);
	echo CHtml::closeTag('div');
}
?>