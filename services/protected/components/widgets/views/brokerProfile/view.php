<?
	$cs=Yii::app()->getClientScript();
	
	$cs->registerScriptFile( CHtml::asset( Yii::App()->params['wpThemePath'] . '/js/waypoints.min.js' ) );

	$counterUpBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/Counter-Up-master' );
	$cs->registerScriptFile($counterUpBaseUrl.'/jquery.counterup.min.js');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wCounterUp.js' ) );
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wBrokerProfileWidget.js' ) );
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wBrokerLinksStats.js' ) );
	
	Yii::app()->clientScript->registerScript('BrokerLinksStats', "
		!function( $ ) {	
			wBrokerLinksStats({
				statsUrl: '" . Yii::app()->createAbsoluteUrl('broker/ajaxSaveLinksStats') . "',	
			});
		}( window.jQuery );
	", CClientScript::POS_END);
	

	Yii::app()->clientScript->registerScript('Vk', "
		
		setTimeout(function() {
			var el = document.createElement('script');
			el.type = 'text/javascript';
			el.src = '//vk.com/js/api/openapi.js';
			el.async = true;
			document.getElementById('vk_api_transport').appendChild(el);
		}, 0);
		
	", CClientScript::POS_END);
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lITradeHere' => 'I trade here',
		'lINotTradeHere' => 'I do not trade here',
		'lYouAnd' => 'You and',
		'lTerribly' => 'Terribly',
		'lBad' => 'Bad',
		'lNormal' => 'Normal',
		'lGood' => 'Good',
		'lExcellent' => 'Excellent',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>

<div class="row-fluid wBrokerProfileWidget" id="vk_api_transport" itemscope itemtype="http://schema.org/FinancialService">

	<? require dirname( __FILE__ ).'/_buttons.php'; ?>
	
	<!-- - - - - - - - - - - - - - Сontent Block - - - - - - - - - - - - - - - - -->
	<div class="clearfix section_offset">
		<div class="financial_box alignleft">

			<a href="<?=$model->site ? $model->getRedirectURL( 'site' ) : $model->AbsoluteSingleUrl?>" itemprop="url">
				<span itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
						<?=$model->nameMiddleImage ? CHtml::tag( 'img', array( 'src' => $model->srcMiddleImage, 'title' => $model->brandName, 'alt' => $model->brandName, 'itemprop' => 'contentUrl url' ) ) : '';?>
						<meta itemprop="width" content="230" />
				</span>
			</a>
			<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<meta itemprop="addressLocality" content="<?=Yii::t( '*', $model->country->name )?>" />
			</span>
			<meta itemprop="telephone" content="<?=WpOptionsModel::getOptionVal('fortraderTelephone')?>" />
			
			<? require dirname( __FILE__ ).'/_iTradeHere.php'; ?>
			<? require dirname( __FILE__ ).'/_voteForBroker.php'; ?>
			<? require dirname( __FILE__ ).'/_openAccounts.php'; ?>
		</div>
		<div class="wrapper">
			<table class="inside_table">
				<tbody>
					<?$this->controller->widget( "widgets.detailViews.BrokerTradingConditionsDetailViewWidget", Array( 'data' => $model ))?>
				</tbody>
			</table>
		</div>
		<div class="clear"></div>
		<? require dirname( __FILE__ ).'/_brokerLinksBtns.php'; ?>				
	</div>
	<!-- - - - - - - - - - - - - - End of Сontent Block - - - - - - - - - - - - - - - - -->
	
	<?php
		if( $model->wpPosts ){
			$cacheKey = 'brokerNewsBlock' . $model->id;
			if( !Yii::app()->cache->get( $cacheKey ) ){
				ob_start();	
				CommonLib::loadWp();
				require_once Yii::App()->params['wpThemePath'].'/templates/broker-page-news.php';	
				$blockContent = ob_get_clean();
				Yii::app()->cache->set( $cacheKey, $blockContent, 60*60 * 6);
			}
			echo Yii::app()->cache->get( $cacheKey );
		}
	?>
	
	<? require dirname( __FILE__ ).'/_requirementsForReview.php'; ?>
	

	
</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idBroker = <?=$model->id?>;
		
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wBrokerProfileWidget = wBrokerProfileWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wBrokerProfileWidget',
			idBroker: idBroker,
			ls: ls,
			logedInUser: <?=Yii::App()->user->isGuest ? 'false' : 'true'?>,
			vkAppId: <?=$vkAppId?>,
			<?php /*sendSmUrl: '<?=Yii::app()->createAbsoluteUrl('broker/ajaxSaveSm')?>',*/?>
			ajaxLinkToUserURL: '<?=Yii::app()->createAbsoluteUrl('broker/ajaxLinkToUser')?>',
			ajaxUnLinkFromUserURL: '<?=Yii::app()->createAbsoluteUrl('broker/ajaxUnLinkFromUser')?>',
		});
	}( window.jQuery );
</script>