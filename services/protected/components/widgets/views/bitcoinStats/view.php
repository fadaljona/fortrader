<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>

<div class="fx_section <?= $ins ?>">
    <div class="fx_content-title clearfix">
        <h3 class="fx_content-title-link"><?= Yii::t($NSi18n, 'Bitcoin stats') ?></h3>
        <span class="fx_content-subtitle"><?= Yii::t($NSi18n, 'Bitcoin stats for 24 hour') ?></span>
        <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/deal_icon.png" alt="" class="fx_title-icon">
    </div>

    <div class="fx_section-box">
        <div class="fx_counter">
            <div class="fx_counter-wr">
            <?php
            foreach (str_split($model->circulatingSupply) as $num) {
                echo CHtml::tag('div', array('class' => 'fx_counter-item'), $num);
            }
            ?>
            </div>
            <div class="fx_counter-descr"><?= Yii::t($NSi18n, 'Circulating supply / The number of coins in existence available to the public') ?></div>
        </div>

        <div class="fx_statistic-box clearfix">
            <div class="fx_statistic-left">
                <div class="fx_statistic-item clearfix">
                    <span class="fx_decorline"></span>
                    <div class="fx_statistic-name"><?= Yii::t($NSi18n, 'Market Capitalization') ?>:</div>
                    <div class="fx_statistic-value">$<?= number_format($model->marketCap, 2)?></div>
                </div>
                <div class="fx_statistic-item clearfix">
                    <div class="fx_statistic-name"><?= Yii::t($NSi18n, 'Volume 24h') ?>:</div>
                    <div class="fx_statistic-value">$<?= number_format($model->volume, 2)?></div>
                </div>
                <div class="fx_statistic-item fx_statistic-<?= $this->getChangeClass() ?> clearfix">
                    <div class="fx_statistic-name"><?= Yii::t($NSi18n, 'Price') ?>:</div>
                    <div class="fx_statistic-value">1 BTC = $
                        <span 
                            class="pid-<?= $this->bitcoin->name ?>-bid"
                            data-precision=<?= $this->bitcoin->precision ?>
                            data-last-bid=<?= $this->bitcoin->lastBid ?>
                        ><?= number_format($this->bitcoin->bid, $this->bitcoin->precision) ?></span>
                    </div>
                </div>
            </div>
            <?php $this->widget('widgets.QuoteChartMaxZoomWidget', array('ns' => $ns, 'symbolId' => $this->bitcoin->id));?>
        </div>
    </div>
</div>