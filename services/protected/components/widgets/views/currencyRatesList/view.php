<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$cs=Yii::app()->getClientScript();
	
	$activeDataTypeClassCbr = $activeDataTypeClassEcb = '';
	if( $dataType == 'cbr' ){
		$todayActiveClass = $tomorrowActiveClass = '';
		if( $type=='today' ){
			$todayActiveClass = 'active';
		}elseif($type=='tomorrow'){
			$tomorrowActiveClass = 'active';
			
			if( $daySecondsToNewData ){
				$timerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/js/timer' );
				$cs->registerScriptFile($timerBaseUrl.'/jquery.plugin.js', CClientScript::POS_END);
				$cs->registerScriptFile($timerBaseUrl.'/jquery.countdown.js', CClientScript::POS_END);
				
				Yii::app()->clientScript->registerScript('initCountDown', "
					!function( $ ) {	
						var endDateRates = new Date();
						endDateRates.setSeconds(endDateRates.getSeconds()+$daySecondsToNewData);
						$('.endDateRates').countdown({until: endDateRates, compact: true, description: '', format: 'HMS'});
					}( window.jQuery );
				", CClientScript::POS_END);
			}
			
		}
		$activeDataTypeClassCbr = 'active';
		$todayLink = Yii::app()->createUrl('currencyRates/index');
	}elseif( $dataType == 'ecb' ){
		$activeDataTypeClassEcb = 'active';
		$todayActiveClass = 'active oneTab';
		$todayLink = Yii::app()->createUrl('currencyRates/ecbIndex');
	}
	
	if( !Yii::App()->request->isAjaxRequest ){
		$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
		$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js');
		$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
		$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wCurrencyRatesListWidget.js' ) );
	}

?>
<div class="iListWidget i01 <?=$ins?> position-relative spinner-margin">
	<!-- - - - - - - - - - - - - - Tabs - - - - - - - - - - - - - - - - -->
	<div class="tabs_box tabs_rating relative">

		<div class="clearfix">
			<div class="wrapper_currency_filter_box">
				<a href="<?=Yii::app()->createUrl('currencyRates/index')?>" class="currency_filter_item <?=$activeDataTypeClassCbr?> currency_filter_cbrf"><?=Yii::t($NSi18n, 'Cbr cources')?></a>
			</div>

			<div class="wrapper_currency_filter_box">
				<a href="<?=Yii::app()->createUrl('currencyRates/ecbIndex')?>" class="currency_filter_item <?=$activeDataTypeClassEcb?> currency_filter_ecb"><?=Yii::t($NSi18n, 'Ecb cources')?></a>
			</div>

			<div class="wrapper_currency_filter_box">
				<select class="currency_filter_change_value">
				<?php
					$i=1;
					$flagStyles = '';
					foreach( $toCurrencies as $key => $currency ){
						echo CHtml::tag(
							'option',
							array(
								'value' => $currency['id'],
								'class' => 'lang ' . $currency['country'],
								'selected' => false,
							),
							$key
						);
						$flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $currency['country']);
						if( $flagSrc ) $flagStyles .= ".lang.{$currency['country']}, .lang.{$currency['country']} .jq-selectbox__select-text{background-image: url({$flagSrc});}";
						if( $i == 2 ){
							echo CHtml::tag(
								'option', 
								array(
									'value' => $defaultCurrency->id,
									'class' => 'lang ' . $defaultCurrency->country->alias,
									'selected' => true,
								), 
								$defaultCurrency->code
							);
							$flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $defaultCurrency->country->alias);
							if( $flagSrc ) $flagStyles .= ".lang.{$defaultCurrency->country->alias}, .lang.{$defaultCurrency->country->alias} .jq-selectbox__select-text{background-image: url({$flagSrc});}";
						}
						$i++;
					}
				?>
				</select>
			</div>
			
			<? $this->widget('widgets.SearchCurrencyRatesWidget',Array( 'ns' => 'nsActionView', 'minInputChars' => 3, 'type' => $dataType )); ?>
				
			
			<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
 			<div class="wrapper_tabs_list currency_change_tabs_list">
				<ul class="tabs_list clearfix PFDregular">
					<li class="<?=$todayActiveClass?> tabs_list_1">
						<?=CHtml::link( Yii::t($NSi18n, 'Today') . ' ' . Yii::app()->dateFormatter->format( 'd MMMM yyyy',time() ), $todayLink, array('class' => 'currency_change_tabs_link') )?>
					</li>	
					<?php if( $dataType == 'cbr' ){?>
					<li class="<?=$tomorrowActiveClass?> tabs_list_2">
						<?=CHtml::link( Yii::t($NSi18n, 'Tomorrow') . ' ' . Yii::app()->dateFormatter->format( 'd MMMM yyyy',time() + 60*60*24 ), Yii::app()->createUrl('currencyRates/tomorrow'), array('class' => 'currency_change_tabs_link') )?>
					</li>
					<?php }?>
				</ul>
			</div>
			
		</div>
		
		<!-- - - - - - - - - - - - - - Tabs Contant - - - - - - - - - - - - - - - - -->
		<div class="tabs_contant">
			<!-- - - - - - - - - - - - - - Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<div class="active currencyListing" data-type="today">
				<!-- - - - - - - - - - - - - - Tabl page  - - - - - - - - - - - - - - - - -->
				<div class="section_offset_2 turn_box">
					<div class="turn_content">
						<div class="tabl_quotes table_currency">
						<?
							$this->widget( 'widgets.lists.CurrencyRatesListListWidget', Array(
								'type' => $type,
								'dataType' => $dataType,
								'toCurrency' => $defaultCurrency,
								'daySecondsToNewData' => $daySecondsToNewData
							));
						?>
						</div>
					</div>
				</div>

				<!-- - - - - - - - - - - - - - End of Tabl page - - - - - - - - - - - - - - - - -->
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs Item 1 - - - - - - - - - - - - - - - - -->
			
			<!-- - - - - - - - - - - - - - End of Tabs Item 2 - - - - - - - - - - - - - - - - -->
		</div><!-- / .tabs_contant -->
		<!-- - - - - - - - - - - - - - End of Tabs Contant - - - - - - - - - - - - - - - - -->
		

	</div>
	<!-- - - - - - - - - - - - - - End of Tabs - - - - - - - - - - - - - - - - -->
</div>
<?php Yii::app()->clientScript->registerCss('flagStyles', $flagStyles);?>
<?if( !Yii::App()->request->isAjaxRequest ){?>
	<script>
		!function( $ ) {
			var ns = <?=$ns?>;
			ns.wCurrencyRatesListWidget = wCurrencyRatesListWidgetOpen({
				ns: <?=$ns?>,
				selector: ".<?=$ns?>"+' .<?=$ins?>',
				ajax: false,
				nsText: ".<?=$ns?>",
				type: '<?=$type?>',
				dataType: '<?=$dataType?>',
				ajaxLoadListURL: '<?=Yii::app()->createAbsoluteUrl('currencyRates/ajaxLoadList')?>',
			});
		}( window.jQuery );
	</script>
<?}?>