<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wCurrencyRatesArchivePreviewWidget.js' ) );
?>
<div class="<?=$ins?>" >
	<h2 class="page_title1"><?=Chtml::link($title, CurrencyRatesModel::getArchiveIndexURL($dataType) )?></h2>
	<div class="choose_date_box position-relative spinner-margin">
		<!-- choose_date -->
		<div class="choose_date choose_years">
			<div class="clearfix choose_date_top">
				<div class="choose_date_text"><?=Yii::t($NSi18n, 'Choose year')?></div>
				<!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->
				<div class="navigation_box_2 clearfix">
					<a href="javascript:;" class="prev"><span class="tooltip"><?=Yii::t($NSi18n, 'Previous')?></span></a>
					<a href="javascript:;" class="next"><span class="tooltip"><?=Yii::t($NSi18n, 'Next')?></span></a>
				</div>
				<!-- - - - - - - - - - - - - - End of Navigation - - - - - - - - - - - - - - - - -->
			</div>
			<div class="choose_date_list_wr">
				<div class="choose_date_list">
					<ul class="choose_date_bottom">
					<?php
						foreach( $years as $item ){
							echo Chtml::tag(
								'li',
								array( 'class' => "choose_date_btm_item" ),
								Chtml::link( $item['year'], CurrencyRatesModel::getArchiveUrl( array('year' => $item['year'], 'type' => $dataType ) ) )
							);
						}
					?>
					</ul>
				</div>
			</div>              
		</div>
		<!--/ choose_date -->
	</div>
</div>


<script>

!function( $ ) {
	var ns = <?=$ns?>;
		
	
	ns.wCurrencyRatesArchivePreviewWidget = wCurrencyRatesArchivePreviewWidgetOpen({
		selector: '.<?=$ns?> .<?=$ins?>',
	});
}( window.jQuery );

</script>