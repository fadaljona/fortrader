<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
    $NSi18n = $this->getNSi18n();
    
    $cs=Yii::app()->getClientScript();

    $cs->registerScript('jsCalendarEventsExportSlideToggleSubscribeBlock', '
        !function( $ ) {
            $(".calendarEventsExportWidget .colorInformer").click(function(){ $(this).parent(".section_offset").find(".box2").slideToggle("slow"); });
        }( window.jQuery );
    ', CClientScript::POS_END);
?>
<section class="section_offset position-relative spinner-margin paddingBottom20 calendarEventsExportWidget <?=$ins?>">
	<h4 class="title7 colorInformer"><?=Yii::t($NSi18n, 'Export')?></h4>
    <div class="box2 box_informer" style="display:none">
        <ul class="mycolumn clearfix">
            <li><a href="<?=Yii::app()->createUrl('calendarEvent/export', array( 'type' => 'csv' ))?>">CSV</a></li>
            <li><a href="<?=Yii::app()->createUrl('calendarEvent/export', array( 'type' => 'xml' ))?>">XML</a></li>
        </ul>
    </div>
</section>