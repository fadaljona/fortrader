<?
	if( Yii::App()->user->isGuest ) return;
?>
<?$form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm', Array( 'htmlOptions' => Array( 'class' => "{$ins} wMarkForm" )))?>
	<?=CHtml::hiddenField( 'idEA', $model->id )?>
	<div class="space-6"></div>
	<h4 class="blue"><?=Yii::t( '*', 'Rate this EA' )?></h4>
	<?=Yii::t( '*', 'Move the slider' )?><br>

	<div class="space-12"></div>
	<?=Yii::t( '*', 'Idea' )?>: <span class="<?=$ins?> wConcept"></span><br>
	<div class="space-6"></div>
	<div class="<?=$ins?> wConceptSlider ui-slider-small ui-slider-blue"></div>
	<?=$form->hiddenField( $markModel, 'concept', Array( 'class' => "{$ins} wConceptInput" ))?>

	<div class="space-12"></div>
	<?=Yii::t( '*', 'Result' )?>: <span class="<?=$ins?> wResult"></span><br>
	<div class="space-6"></div>
	<div class="<?=$ins?> wResultSlider ui-slider-small ui-slider-green"></div>
	<?=$form->hiddenField( $markModel, 'result', Array( 'class' => "{$ins} wResultInput" ))?>
	
	<div class="space-12"></div>
	<?=Yii::t( '*', 'Stability' )?>: <span class="<?=$ins?> wStability"></span><br>
	<div class="space-6"></div>
	<div class="<?=$ins?> wStabilitySlider ui-slider-small ui-slider-red"></div>
	<?=$form->hiddenField( $markModel, 'stability', Array( 'class' => "{$ins} wStabilityInput" ))?>

	<div class="space-12"></div>
	<?=Yii::t( '*', 'Average' )?>:<br>
	<span class="<?=$ins?> wAverage" style="font-size:30px;line-height:normal;"></span>

	<div class="space-12"></div>
	<button class="btn btn-success" type="submit"><?=Yii::t( '*', 'Vote!' )?></button>
<?$this->endWidget()?>
<div class="space-20"></div>