<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wEAProfileWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lTerribly' => 'Terribly',
		'lBad' => 'Bad',
		'lNormal' => 'Normal',
		'lGood' => 'Good',
		'lExcellent' => 'Excellent',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="row-fluid wEAProfileWidget">
	<div class="row-fluid">
		<div class="span3 center">
			<?if( strlen( $model->nameImage )){?>
				<span class="profile-picture">
					<?=$model->getImage()?>
				</span>
				<div class="space-6"></div>
			<?}?>
			<div class="profile-contact-info">
				<div class="profile-contact-links align-left">
					<a class="btn btn-link" href="#">
						<i class="icon-globe bigger-125 blue"></i>
						<?=Yii::t( '*', 'View' )?>
					</a>
				</div>
			</div>
			<div class="hr hr12 dotted"></div>
			<div class="clearfix">
				<div class="grid2">
					<span class="bigger-175 blue"><?=CommonLib::numberFormat( $model->views )?></span>
					<br>
					<?=Yii::t( '*', 'Views' )?>
				</div>
			</div>
			<? require dirname( __FILE__ ).'/_mark.php'; ?>
		</div>
		<div class="span9">
			<?if( $model->currentLanguageI18N ){?>
				<div class="row-fluid">
					<div class="span9">
						<div class="widget-box transparent">
							<div class="widget-header widget-header-small">
								<h4 class="smaller">
									<i class="icon-check bigger-110"></i>
									<?=Yii::t( '*', 'About EA' )?>
								</h4>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<?=$model->currentLanguageI18N->about?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?}?>
			<?$this->controller->widget( "widgets.detailViews.EAProfileDetailViewWidget", Array( 'data' => $model ))?>
			
			<?/*$this->controller->widget( "widgets.EAStatementsWidget", Array( 
				'idEA' => $model->id, 
				'ns' => $ns,
			))*/?>
			
			<h4 class="header smaller lighter blue" style="margin-top:30px;text-transform:uppercase;"><?=Yii::t( $NSi18n, 'EA versions' )?></h4>
			<div class="row-fluid">
				<?
					$this->widget( 'widgets.lists.EAVersionsListWidget', Array(
						'ns' => 'nsActionView',
						'ajax' => true,
						'showOnEmpty' => true,
						'idEA' => $model->id,
					));
				?>
				<a href="<?=EAVersionModel::getAddURL( $model->id )?>" class="iA i07"><?=Yii::t( '*', 'Add' )?></a>
			</div>
			
			<h4 class="header smaller lighter blue" style="margin-top:30px;text-transform:uppercase;"><?=Yii::t( $NSi18n, 'EA monitoring' )?></h4>
			<div class="row-fluid">
				<?
					$this->widget( 'widgets.lists.EATradeAccountsListWidget', Array(
						'ns' => 'nsActionView',
						'ajax' => true,
						'showOnEmpty' => true,
						'idEA' => $model->id,
					));
				?>
			</div>
			
			<h4 class="header smaller lighter blue" style="margin-top:30px;text-transform:uppercase;"><?=Yii::t( '*', 'Config and statment' )?></h4>
			<div class="row-fluid">
				<?
					$this->widget( 'widgets.lists.EAStatementsListWidget', Array(
						'ns' => 'nsActionView',
						'ajax' => true,
						'showOnEmpty' => true,
						'idEA' => $model->id,
					));
				?>
				<a href="<?=EAStatementModel::getAddURL( $model->id )?>" class="iA i07"><?=Yii::t( '*', 'Add' )?></a>
			</div>
			
			<br><br>
			<div class="row-fluid">
				<?
					$this->widget( 'widgets.UsersConversationWidget', Array(
						'ns' => $ns,
						'ajax' => true,
					));
				?>
			</div>
			
			<div class="row-fluid" style="margin-top:20px;">
				<?
					$this->widget( 'widgets.PostInformWidget', Array(
						'ns' => 'nsActionView',
					));
				?>
			</div>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEA = <?=$model->id?>;
		
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wEAProfileWidget = wEAProfileWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wEAProfileWidget',
			idEA: idEA,
			ls: ls,
		});
	}( window.jQuery );
</script>