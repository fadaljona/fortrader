<?
	$NSi18n = $this->getNSi18n();
	$class = "ft{$prefix}Zone_{$zone->id}";
	
	$zoneNameEncoded = str_replace( "'","\'", $zone->name );
	if( $zone->typeAds == 'Text' && $zone->mixedZone == 1 && $ads[0]->type == 'Image' ){
		$zone->typeAds = 'Media';
		$adToShow = array( $ads[0] );
		unset($ads);
		$ads = $adToShow;
	}
if( $ads[0]->htmlBaner && $ads[0]->type == 'Image' ){
	$rrrr = rand();
}else{
?>
<style type="text/css">
	.<?=$class?><?php echo $zone->typeBlock == 'Popup' ? '' : ', .'.$class.' *'; ?> {
		<?php echo $zone->typeBlock == 'Popup' ? 'background: none;' : 'background: none !important;'; ?>
		border: none !important;
		box-shadow: none !important;
		-moz-box-shadow: none !important;
		-webkit-box-shadow: none !important;
		clear: none !important;
		clip: auto !important;
		cursor: auto !important;
		float: none !important;
		<?php echo $zone->typeBlock == 'Popup' ? 'height:100% !important;' : '';//'height: auto !important;'; ?>
		letter-spacing: auto !important;
		line-height: auto !important;
		margin: 0 !important;
		max-height: none !important;
		max-width: none !important;
		min-height: 0 !important;
		min-width: 0 !important;
		opacity: 1 !important;
		overflow: hidden !important;
		padding: 0 !important;
		position: static !important;
		text-align: left !important;
		text-decoration: none !important;
		text-indent: 0 !important;
		text-overflow: clip !important;
		text-shadow: none !important;
		text-transform: none !important;
		transform: none !important;
		vertical-align: baseline !important;
		visibility: visible !important;
		white-space: normal !important;
		<?php echo $zone->typeBlock == 'Popup' ? 'width:100% !important;' : ''; ?>
		<?php echo $zone->typeBlock == 'Popup' ? 'position: fixed !important;background: rgba(255,255,255,0.82);z-index: 9000 !important;top: 0 !important;left: 0 !important;' : ''; ?>
		word-break: normal !important;
		word-spacing: normal !important;
		word-wrap: normal !important;
		box-sizing: border-box !important;
		-moz-box-sizing: border-box !important;
		-webkit-box-sizing: border-box !important;
	}
	.<?=$class?> .click-link-ads{
		text-align:right;
		margin-top:5px;
	}
	.<?=$class?> .click-link-ads a:hover{
		text-decoration:none !important;
	}
	.<?=$class?> .click-link-ads a{
		color:#09548f;
		font-family:Verdana;
		font-size:15px;
	}
	.<?=$class?> #popup-cont .close-popup{
		float:right;
		cursor:pointer;
	}
	.<?=$class?> #popup-cont .time-to-site{
		float:right;
		padding-right:8px;
	}
	.<?=$class?> #popup-cont .above-img{
		position:absolute;
		top:-50px;
		height:50px;
		line-height:45px;
		font-size:12px;
		color:#000;
		width:100%;
		font-family:Arial;
	}
	.<?=$class?> {
		display: block;
		white-space: nowrap !important;
		<?if( $zone->typeBlock == 'Fixed' ){?>
			width: <?=$zone->widthBlock?>px !important;
			height: <?=$zone->heightBlock?>px !important;
		<?}?>
		<?if( $zone->typeBorder == 'All' ){?>
			background: <?=$zone->colorBG?> !important;
			<?if( $zone->typeAds == 'Offers' ){?>
				padding: 5px 0px !important;
			<?}else{?>
				padding: 5px 15px !important;
			<?}?>
			border: 1px solid <?=$zone->colorBorder?> !important;
			<?if( $zone->roundedShapeBlocks ){?>
				border-radius: 4px !important;
				-moz-border-radius: 4px !important;
				-webkit-border-radius: 4px !important;
			<?}?>
		<?}?>
	}
	.<?=$class?> .ft<?=$prefix?>Cont{
		display: block !important;
		<?if( $zone->getStructureAds() == 'Horizontal' ){?>
			<?if( count( $ads ) > 1 ){?>
				<?if( $zone->typeAds == 'Offers' ){?>
					display: inline-block !important;
					vertical-align: top !important;
					width: <?=round( 100 / min(count( $ads ), 4), 2 )?>% !important;
					position:relative !important;
				<?}else{?>
					float:left !important;
					width: <?=round( 100 / count( $ads ), 2 )?>% !important;
				<?}?>
			<?}?>
		
		<?}else{
			if( $ads[0]->type == 'Image' ){
				$rrrr = rand();
			}else{
				echo 'padding: 5px 5px !important;';
			}
 		}?>
		<?php echo $zone->typeBlock == 'Popup' ? 'position: fixed !important; top: 200px; left: 38%;padding: 0px !important;' : ''; ?>
	}
	.<?=$class?> .ft<?=$prefix?>Item{
		<?php echo $zone->typeBlock == 'Popup' ? '' : ''; ?>
		display: block !important;
		<?if( $zone->typeBorder == 'Each' ){?>
			background: <?=$zone->colorBG?> !important;
			padding: 5px 15px !important;
			border: 1px solid <?=$zone->colorBorder?> !important;
			<?if( $zone->roundedShapeBlocks ){?>
				border-radius: 4px !important;
				-moz-border-radius: 4px !important;
				-webkit-border-radius: 4px !important;
			<?}?>
		<?}?>
	}
	.<?=$class?> .ft<?=$prefix?>Item h5 a{
		display: inline !important;
		color: <?=$zone->colorHeader?> !important;
	}
	.<?=$class?> .ft<?=$prefix?>Item h5 a:hover{
		color: <?=$zone->colorHoverHeader?> !important;
	}
	.<?=$class?> .ft<?=$prefix?>Item p{
		display: block !important;
		color: <?=$zone->colorText?> !important;
		margin: 0 0 10px !important;
	}
	.<?=$class?> .ft<?=$prefix?>Item .ft_plink a{
		display: inline !important;
		color: <?=$zone->colorURL?> !important;
	}
	.<?=$class?> .ft<?=$prefix?>Item .ft_plink a:hover{
		color: <?=$zone->colorHoverURL?> !important;
	}
	.<?=$class?> .ft<?=$prefix?>Item h5{
		display: block !important;
		font: <?=$zone->sizeHeader?>px/normal <?=$zone->font?> !important;
		margin: 10px 0 !important;
	}
	.<?=$class?> .ft<?=$prefix?>Item p{
		font: <?=$zone->sizeText?>px/normal <?=$zone->font?> !important;
	}
	.<?=$class?> .ft<?=$prefix?>Item a, .<?=$class?> .ft<?=$prefix?>Item a *{
		cursor: pointer !important;
		<?php echo $zone->typeBlock == 'Popup' ? '' : ''; ?>
	}
	.<?=$class?> a{
		text-decoration:none !important;
	}
	.<?=$class?> a:hover{
		text-decoration:underline !important;
	}
	<?if( $zone->typeAds != 'Offers' and $zone->getStructureAds() == 'Horizontal' and $zone->getCountAds() <= 4 ){?>
		<?$w = 400?>
		@media (min-width: <?=$w*2+1?>px) and (max-width: <?=$w*3?>px) {
			.<?=$class?> .ft<?=$prefix?>Cont{
				display:none !important;
			}
			.<?=$class?> .ft<?=$prefix?>Cont_0, .<?=$class?> .ft<?=$prefix?>Cont_1, .<?=$class?> .ft<?=$prefix?>Cont_2{
				display:block !important;
				min-width:33.33% !important;
			}
		}
		@media (min-width: <?=$w+1?>px) and (max-width: <?=$w*2?>px) {
			.<?=$class?> .ft<?=$prefix?>Cont{
				display:none !important;
			}
			.<?=$class?> .ft<?=$prefix?>Cont_0, .<?=$class?> .ft<?=$prefix?>Cont_1{
				display:block !important;
				min-width:50% !important;
			}
		}
		@media (max-width: <?=$w?>px) {
			.<?=$class?> .ft<?=$prefix?>Cont{
				display:none !important;
			}
			.<?=$class?> .ft<?=$prefix?>Cont_0{
				display:block !important;
				min-width:100% !important;
			}
		}
	<?}?>
	<?if( $zone->typeAds == 'Offers' ){?>
		<?
			$w = 300;
			$mod = (float)@$_GET['mod'];
			if( $mod ) $w = (int)($w/$mod);
		?>
		<?php if($zone->getStructureAds() == 'Horizontal'){?>
		@media (max-width: <?=$w*6?>px) {
			.<?=$class?> .ft<?=$prefix?>Cont{
				width: 33.33% !important;
			}
		}
		@media (max-width: <?=$w*4?>px) {
			.<?=$class?> .ft<?=$prefix?>Cont{
				width: 50% !important;
			}
		}
		@media (max-width: <?=$w*2?>px) {
			.<?=$class?> .ft<?=$prefix?>Cont{
				width: 100% !important;
			}
		}
		<?php }else{?>
		@media (min-width: 768px) {
			.offer-img{
				display:none;
			}
		}
		@media (max-width: 767px) {
			.offer-img{
				display:table-cell;
			}
		}
		<?php } ?>
		
		.<?=$class?> .ft<?=$prefix?>Cont .ft<?=$prefix?>OfferTab{
			width:100% !important;
		}
		.<?=$class?> .ft<?=$prefix?>Cont .ft<?=$prefix?>OfferTab td{
			vertical-align:top !important;
			
		}
		.<?=$class?> .ft<?=$prefix?>Cont .ft<?=$prefix?>OfferTab td h5{
			margin:5px 0 3px !important;
		}
		.<?=$class?> .ft<?=$prefix?>Cont .ft<?=$prefix?>NewOffer{
			color: #13a100 !important;
		}
		<?php if($zone->getStructureAds() == 'Horizontal'){?>
		.<?=$class?> .ft_hidden{
			display: none !important;
		}
		<?php }?>
		
		.<?=$class?> .ft<?=$prefix?>Item{
			border: solid #e4e4e4 1px !important;
			background: white !important;
			border-radius: 0px !important;
			-moz-border-radius: 0px !important;
			-webkit-border-radius: 0px !important;
			padding:0 !important;
		}
		.<?=$class?> .ft<?=$prefix?>OfferBottom{
			background: #f2f5f7 !important;
			padding:7px 10px 6px !important;
		}
		.<?=$class?> .ft<?=$prefix?>More{
			float:right !important;
			padding:0px 0 2px 10px !important;
			background: url('<?=Yii::App()->createAbsoluteURL('assets-static/images/dotted01.png')?>') 0% 50% no-repeat !important;
		}
		.<?=$class?> .ft<?=$prefix?>Likes{
			float:right !important;
			padding:0px 10px 2px 10px !important;
		}
		.<?=$class?> .ft<?=$prefix?>Likes a{
			text-decoration:none !important;
			color:black !important;
		}
		.<?=$class?> .ft<?=$prefix?>Heart{
			display: inline-block !important;
			width:14px !important;
			height:12px !important;
			background: url('<?=Yii::App()->createAbsoluteURL('assets-static/images/heart01.png')?>') !important;
			position:relative !important;
			top:1px !important;
			left:-1px !important;
		}
		.<?=$class?> .ft<?=$prefix?>Likes a:hover .ft<?=$prefix?>Heart, .<?=$class?> .ft<?=$prefix?>HeartHover{
			background: url('<?=Yii::App()->createAbsoluteURL('assets-static/images/heart01h.png')?>') !important;
		}
	<?}?>
</style>
<?php }?>
<div class="<?=$class?> zoneWrapper" <?php echo $zone->typeBlock == 'Popup' ? 'id="parent-cont-popup"' : ''; ?> data-zone="<?=$zone->id?>">
	<?foreach( $ads as $i=>$ad ){
		$clickURL = $ad->getClickURL( $zone );
		$onClick = "(typeof _gaq!='undefined')&&_gaq.push(['_trackEvent', 'Context', 'Click', '{$zoneNameEncoded}']); return true;"
		?><div class="ft<?=$prefix?>Cont jsCont ft<?=$prefix?>Cont_<?=$i?>" <?php echo $zone->typeBlock == 'Popup' ? 'id="popup-cont"' : ''; ?>>
			<?php echo $zone->typeBlock == 'Popup' ? '
				<div class="above-img">
					<div class="close-popup" id="close-popup"><img src="'. Yii::App()->createAbsoluteURL('uploads/ads/i-close_dark.png') .'" alt="close" /></div>
					<div class="time-to-site">'. Yii::t('*', 'Before moving to the site remained') .' <span id="timer-service-popup">10</span> '. Yii::t('*', 'seconds') .'</div>
					<div style="clear:both;"></div>
				</div>' : ''; ?>
			<div class="ft<?=$prefix?>Item itemWrapper" data-item="<?=$ad->id?>">
				<?if( $ad->type == 'Text' ){?>
					<h5>
						<a href="<?=$clickURL?>" onclick="<?=$onClick?>" target="_blank">
							<?=htmlspecialchars( $ad->header )?>
						</a>
					</h5>
					<p>
						<?=$ad->getHTMLText()?>
					</p>
					<p class="ft_plink">
						<small>
							<a href="<?=$clickURL?>" onclick="<?=$onClick?>" target="_blank"><?=htmlspecialchars( $ad->getTitledURL() )?></a>
						</small>
					</p>
				<?}?>
				<?if( $ad->type == 'Image' or $ad->type == 'Button' ){?>
					<?php if( $ad->htmlBaner ){
						echo $ad->text;
					}else{ ?>
						<?php if( strtolower( CommonLib::getExtension( $ad->getAubsoluteImageURL() ) ) == 'swf' ){ ?>
							<div style="position:relative !important;">
								<object style="height:<?=$ad->heightImage?>px !important; cursor:pointer; width:<?=$ad->widthImage?>px !important" type='application/x-shockwave-flash' data='<?=$ad->getAubsoluteImageURL()?>' width='<?=$ad->widthImage?>' height='<?=$ad->heightImage?>'>
									<param name='movie' value='<?=$ad->getAubsoluteImageURL()?>'>
									<param name="wmode" value="transparent" />
								</object>
								<a href="<?=$clickURL?>" onclick="<?=$onClick?>" target="_blank" style="display:block;position:absolute !important;top:0px;left:0px;z-index:10;width:<?=$ad->widthImage?>px !important;height:<?=$ad->heightImage?>px !important;"></a>
							</div>
						<?php }else{ ?>
							<a href="<?=$clickURL?>" onclick="<?=$onClick?>" target="_blank">
								<img id="imgId<?=$prefix?>" src="<?=$ad->getAubsoluteImageURL()?>">
							</a>
						<?php } ?>
					<?php } ?>
				<?}?>
				<?if( $ad->type == 'Offer' ){?>
					<table width="100%" class="ft<?=$prefix?>OfferTab">
						<tr>
							<td class="offer-img">
								<a href="<?=$clickURL?>" onclick="<?=$onClick?>" target="_blank">
									<img src="<?=$ad->getAubsoluteImageURL()?>" width="<?=$ad->widthImage?>" height="<?=$ad->heightImage?>">
								</a>
							</td>
							<td style="width:100% !important;">
								<h5>
									<a href="<?=$clickURL?>" onclick="<?=$onClick?>" target="_blank">
										<?=htmlspecialchars( $ad->header )?>
									</a>
								</h5>
								<p>
									<?=$ad->getHTMLText()?>
								</p>
							</td>
						</tr>
					</table>
					<div class="ft<?=$prefix?>OfferBottom">
						<?if( $ad->isNew()){?>
							<b class="ft<?=$prefix?>NewOffer"><?=Yii::t( $NSi18n, 'New' )?></b>
						<?}?>
						<div class="ft<?=$prefix?>More">
							<a class="ft_plink" href="<?=$clickURL?>" onclick="<?=$onClick?>" target="_blank">
								<?=Yii::t( $NSi18n, 'Read more' )?>
							</a>
						</div>
						<div class="ft<?=$prefix?>Likes">
							<a href="#" class="ft<?=$prefix?>OfferLike jsOfferLike" data-id="<?=$ad->id?>" data-likes="<?=$ad->countLikes?>">
								<span class="saving"></span>
								<div class="ft<?=$prefix?>Heart jsHeart <?if( $ad->likeCurrentUser ){?>ft<?=$prefix?>HeartHover jsHeartHover<?}?>"></div>
								<span class="likes">
									<?=CommonLib::numberFormat( $ad->countLikes )?>
								</span>
							</a>
						</div>
						<div style="clear:both !important;"></div>
					</div>
				<?}?>
			</div>
			<?php if( $zone->typeBlock == 'Popup' ){ ?>

						
<script>
var popupCloseClicked = false;
function clickOutclosePopup(e){
	if( popupCloseClicked ) return false;
	var popup = document.getElementById('popup-cont');
	if (e.target.id != popup.id )  {
		popupCloseClicked = true;
		animateBackgroundOpacity( parentCont, 0.92, 0, 1000, 'out' );
		var cTop = (wHeight - popupCont.offsetHeight)/2-5;
		animateEl( 'top', popupCont, cTop, wHeight+1000, 1000, 'out' );
	}
}

function closePopup(){
	if( popupCloseClicked ) return false;
	popupCloseClicked = true;
	animateBackgroundOpacity( parentCont, 0.92, 0, 1000, 'out' );
	var cTop = (wHeight - popupCont.offsetHeight)/2-5;
	animateEl( 'top', popupCont, cTop, wHeight+1000, 1000, 'out' );
}
		
function createpCookie(name, value, mins) {
	var expires;
	if (mins) {
		var date = new Date();
		date.setTime(date.getTime() + (mins * 60 * 1000));
		expires = "; expires=" + date.toGMTString();
	}
	else {
		expires = "";
	}
	document.cookie = name + "=" + value + expires + "; path=/";
}

function getpCookie(c_name) {
	if (document.cookie.length > 0) {
		c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length + 1;
			c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1) {
				c_end = document.cookie.length;
			}
			return unescape(document.cookie.substring(c_start, c_end));
		}
	}
	return null;
}

function delta(progress, param) {
	if( param == 'in' ){
		return 1 - Math.pow(1 - progress, 4);
	}
	if( param == 'out' ){
		return Math.pow(progress, 4);
	}
}
function animateEl( property, element, from, to, duration, deltaParam ){
	to = parseFloat(to);
	from= parseFloat(from);
	var start = new Date().getTime(); 
	setTimeout(function() {
		var now = (new Date().getTime()) - start;
		var progress = now / duration;
							
		if (progress >= 1){
			var result = to;
			if(deltaParam == 'in') {
				var $timer = document.getElementById('timer-service-popup');
				setTimeout(function() {
					var timerVal = parseInt($timer.innerHTML);
					if( timerVal > 0 ){
						timerVal = timerVal -1 ;
						$timer.innerHTML = timerVal;
						setTimeout(arguments.callee, 1000);
					}else{
						closePopup();
					}
				},1000);
			};
		}else{
			var result = (to - from) * delta(progress, deltaParam) + from;
		}
		element.style[property] = Math.ceil(result) + "px";
		if (progress < 1){
			setTimeout(arguments.callee, 1);
		}else{
			if(deltaParam == 'in') {
				popupCloseClicked = false;
			}
		}
	}, 1);
}
function animateBackgroundOpacity( element, from, to, duration, deltaParam ){
	to = parseFloat(to);
	from= parseFloat(from);
	var start = new Date().getTime(); 
	setTimeout(function() {
		var now = (new Date().getTime()) - start;
		var progress = now / duration;
							
		if (progress >= 1){
			var result = to;
			if(deltaParam == 'out') element.style.display = 'none';
		}else{
			var result = (to - from) * delta(progress, deltaParam) + from;
		}
		element.style.background = 'rgba(255,255,255,'+result+')';
		if (progress < 1)
			setTimeout(arguments.callee, 1);
	}, 1);
}
function contPosition(){
	var wWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
	var wHeight = (window.innerHeight > 0) ? window.innerHeight : screen.Height;
	var popupCont = document.getElementById('popup-cont');
	var cTop = (wHeight - popupCont.offsetHeight)/2-5;
	var cLeft = (wWidth - popupCont.offsetWidth)/2-5;
	popupCont.style.top = cTop+'px';
	popupCont.style.left = cLeft+'px';
}
function startMainScript(){

	if( (wWidth > 1000 ) || <?php echo $ad->campaign->hideOnMobiles;?> == 0 ){		
		<?php 
			$popupDelay = $popupDelay * 1000;
			if( !$popupDelay ) $popupDelay = 20 * 1000; 
		?>
		<?php if($ad->campaign->minPopup){ ?>
		if (getpCookie('popup_services_displayed') == null) {
			setTimeout(function(){ showPopup() }, <?=$popupDelay?>);
			/*popupServicesDisplayed = true;*/
			var d = new Date();
			var arrToCokie = [ <?php echo $ad->campaign->minPopup;?>, d.getTime() ];
			createpCookie('popup_services_displayed', arrToCokie.join('|'), <?php echo $ad->campaign->minPopup;?>);
		}
		<?php }else{ ?>
			setTimeout(function(){ showPopup() }, <?=$popupDelay?>);
			/*popupServicesDisplayed = true;*/
		<?php } ?>
	}
}
function showPopup(){
	parentCont.style.background = 'rgba(255,255,255,0)';
	parentCont.style.display = 'block';
	animateBackgroundOpacity( parentCont, 0, 0.92, 1000, 'in' );
	
	var fromTop = '-<?=$ad->heightImage+30?>';
	var cTop = (wHeight - popupCont.offsetHeight)/2-5;
	var cLeft = (wWidth - popupCont.offsetWidth)/2-5;
				
	popupCont.style.width = '<?=$ad->widthImage?>px';
	popupCont.style.height = '<?=$ad->heightImage?>px';
	popupCont.style.top = fromTop+'px';
	popupCont.style.left = cLeft+'px';
	
	animateEl( 'top', popupCont, fromTop, cTop, 1000, 'in' );
}

					var wWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
					
					var wHeight = (window.innerHeight > 0) ? window.innerHeight : screen.Height;
					var parentCont = document.getElementById('parent-cont-popup');
					parentCont.style.display = 'none';
					
					var popupCont, cTop, cLeft;
					
					popupCont = document.getElementById('popup-cont');
					
					/*var popupServicesDisplayed = false;*/
					
					startMainScript();

					window.addEventListener("resize", contPosition);
					
					document.getElementById('close-popup').addEventListener("click", closePopup);
					parentCont.addEventListener("click", clickOutclosePopup);

</script>
			<?php } ?>
			
		</div><?}?>
</div>