<?php
    $cs=Yii::app()->getClientScript();
    $colorpickerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/bootstrap-colorpicker-plus' );
    $cs->registerCssFile($colorpickerBaseUrl.'/css/my.css');
    $cs->registerCssFile($colorpickerBaseUrl.'/css/bootstrap-colorpicker.min.css');
    $cs->registerCssFile($colorpickerBaseUrl.'/css/bootstrap-colorpicker-plus.css');
    $cs->registerScriptFile($colorpickerBaseUrl.'/js/bootstrap-colorpicker.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($colorpickerBaseUrl.'/js/bootstrap-colorpicker-plus.js', CClientScript::POS_END);
    
    $cs->registerScriptFile( '//code.jquery.com/ui/1.11.4/jquery-ui.js', CClientScript::POS_END );
    $cs->registerCssFile( Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css' );
    
    $cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wInformerAllQuotesSettingsWidget.js' ), CClientScript::POS_END );
    $cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/clipboard.min.js' ) );
    
    $ns = $this->getNS();
    $ins = $this->getINS();
    $NSi18n = $this->getNSi18n();
?>
<div class="<?=$ins?> position-relative spinner-margin" >
    <section class="section_offset">
        <div class="blockCounter">
            <div class="numberSquare">1</div>
            <?=Yii::t( $NSi18n, 'Informer style' );?>
        </div>
        <h4 class="title7 colorInformer instrumentMatgin1"><?=Yii::t( $NSi18n, 'Choose informer style' );?></h4>
        <div class="box2 box_informer">
            <div class="col6">
            <?=CHtml::dropDownList( 'style', $defaultStyle->id, $stylesList, array( 'class' => 'stylesList' ) );?>
            </div>
            <div class="col6">
                <div class="myInformer styleIframeWrap">
                </div>
            </div>
        </div>
    </section>
    <div class="box2 box_informer informerColorsPreview"></div>
    <!-- - - - - - - - - - - - - - Change Color Box - - - - - - - - - - - - - - - - -->
    <section class="section_offset">
        <div class="blockCounter">
            <div class="numberSquare">2</div>
            <?=Yii::t( $NSi18n, 'Informer settings' );?>
        </div>
        
        <h4 class="title7 colorInformer instrumentMatgin1"><?=Yii::t( $NSi18n, 'Change informer colors' );?></h4>
        <div class="box2 box_informer informerColorsBox"></div>
    </section>
    <!-- - - - - - - - - - - - - - End of Change Color Box - - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - End of Choose Currency - - - - - - - - - - - - - - - - -->
    <section class="section_offset">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Select items from category' )?> <?=$category->title?></h4>
        <div class="box2 box_informer informerItemsBox allQuotes">
        <?php
            $newItems = array();
            foreach( $items as $key => $item ){
                if( isset( $newItems[$item['cat']] ) ){
                    $newItems[$item['cat']][$key] = $item;
                }else{
                    $newItems[$item['cat']] = array( $key => $item );
                }
            }

            $defaultItems = '';
            $defaultItemsCount = 0;
            
            foreach( $newItems as $cat => $items ){
                echo CHtml::tag('h4', array(), $cat);
                $i=1;
                foreach( $items as $key => $item ){
                    if( $i % 4 == 1 ){
                        echo CHtml::openTag( 'ul', array( 'class' => 'mycolumn clearfix ' ) );
                    } 
                        $activeClass = '';
                        if( $i==1 && $defaultItemsCount < 5 ){
                            if( !$defaultItems ){
                                $defaultItems = $key;
                            }else{
                                $defaultItems .= ',' . $key;
                            }
                            $activeClass = 'columnActive';
                            $defaultItemsCount ++;
                        } 
                        echo CHtml::tag( 'li', array( 'class' => $activeClass, 'data-item' => $key ), $item['name'] );
                    if( $i % 4 == 0 ) echo CHtml::closeTag( 'ul');
                    $i++;
                    
                }
                if( ($i-1) %4 !=0 )  echo CHtml::closeTag( 'ul');
            }
        ?>
        
        </div>
    </section>

    <section class="section_offset informerColumsSection">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Select columns' )?></h4>
        <div class="box2 box_informer informerColumsBox">
        <?php
            $i=1;
            $defaultColumns = '';
            foreach( $colums as $colum ){
                if( $i % 4 == 1 ){
                    $marginClass = '';
                    if( $i > 1 ) $marginClass = 'instrumentMatgin';
                    echo CHtml::openTag( 'ul', array( 'class' => 'mycolumn clearfix ' . $marginClass ) );
                } 
                    $activeClass = '';
                    if( $i == 1 ){
                        $defaultColumns = $colum;
                        $activeClass = 'columnActive';
                    } 
                    echo CHtml::tag( 'li', array( 'class' => $activeClass, 'data-colum' => $colum ), Yii::t( 'informerColumnNames', $colum ) );
                if( $i % 4 == 0 ) echo CHtml::closeTag( 'ul');
                $i++;
            }
            if( ($i-1) %4 !=0 )  echo CHtml::closeTag( 'ul');
        ?>
        </div>
    </section>
    

    <section class="section_offset informerTitleSection">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Enter informer title' )?></h4>
        <div class="box2 box_informer">
            <input type="text" id="informerTitle" name="informerTitle" value="<?=$category->informerTitle?>" placeholder="<?=$category->informerTitle?>">
        </div>
    </section>
    <section class="section_offset informerTitleSection">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Enter columns titles' )?></h4>
        <div class="informerTextsSettings clearfix">
            <div class="box2 box_informer">
                <input type="text" name="texts[toolTitle]" class="informerTexts" value="<?=$category->toolTitle?>" placeholder="<?= $category->toolTitle ?>">
            </div>
            <?php
            foreach ($colums as $index => $colum) {
                $hideClass = 'hide';
                if (!$index) {
                    $hideClass = '';
                }
                echo CHtml::openTag('div', array('class' => 'box2 box_informer ' . $hideClass));
                    echo CHtml::textField(
                        "texts[{$colum}]",
                        $category->getColumnName($colum),
                        array(
                            'placeholder' => $category->getColumnName($colum),
                            'class' => 'informerTexts'
                        )
                    );
                echo CHtml::closeTag('div');
            }
            ?>
        </div>
    </section>

    

    <section class="section_offset chooseFontSizeAndPaddings changeSettings">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Change font size, paddings, margins and width' )?></h4>
        <div class="box2 box_informer">
            <div class="sliderVal"></div>    
            
            <div class="checkbox_box">
                <input type="checkbox" class="inpSettings" name="fixedWidthBtn" id="fixedWidthBtn">
                <label class="square_input" for="fixedWidthBtn"><i></i><?=Yii::t( $NSi18n, 'Fixed width' )?></label>
            </div>
            <div class="sliderWidthBox">
                <div class="sliderWidthVal"></div>    
                <input type="text" class="widthVal">
            </div>
        </div>
    </section>
    
    <section class="section_offset">
        <div class="checkbox_box">
            <input type="checkbox" checked="checked" class="inpSettings" name="hideGetInformerBtn" id="hideGetInformerBtn">
            <label class="square_input" for="hideGetInformerBtn"><i></i><?=Yii::t( $NSi18n, 'Hide get informer button' )?></label>
        </div>
    </section>
    
    <section class="section_offset hideDiffValuesBox">
        <div class="checkbox_box">
            <input type="checkbox" checked="checked" class="inpSettings" name="hideDiffValues" id="hideDiffValues">
            <label class="square_input" for="hideDiffValues"><i></i><?=Yii::t( $NSi18n, 'Hide diff values' )?></label>
        </div>
    </section>
    
    <section class="section_offset disableRealTime">
        <div class="checkbox_box">
            <input type="checkbox" class="inpSettings" name="disableRealTime" id="disableRealTime">
            <label class="square_input" for="disableRealTime"><i></i><?=Yii::t( $NSi18n, 'Disable real time update' )?></label>
        </div>
    </section>

    
    <section class="section_offset">
        <div class="blockCounter xsCode">
            <div class="numberSquare">3</div>
            <?=Yii::t( $NSi18n, 'Copy the code to insert into your website' )?>
        </div>
        <div class="codeForInsert clearfix">
            <textarea name="code" id="code" rows="10" cols="30" class="informerCodeWrap"></textarea>
        </div>
        <div class="copyCode" data-clipboard-action="copy" data-clipboard-target="#code"><?=Yii::t( $NSi18n, 'Copy to clipboard' )?></div>
    </section>
    
    <?php
            $this->widget('widgets.UnisenderSubscribeWidget', array(
                'ns' => 'nsActionView',
                'listId' => 12531533
            ));
        ?>
    
</div>

<?php

Yii::app()->clientScript->registerScript('informersSettingsJs', '

    !function( $ ) {
        var ns = ".' . $ns . '";
        var nsText = ".' . $ns . '";
        var ins = ".' . $ins . '";

        ns.wInformerSettingsWidget = wInformerSettingsWidgetOpen({
            ns: ns,
            ins: ins,
            selector: nsText+" .' . $ins . '",
            catId: ' . $category->id . ',
            getInformerUrl: "' . CommonLib::httpsProtocol( Yii::app()->createAbsoluteUrl("informers/getInformer" ) ) . '",
            getInformerColors: "' . Yii::app()->createAbsoluteUrl("informers/getColors" ) . '",
            minWidth: 150,
            maxWidth: 2000,
            catType: "' . $category->type . '",
            defaultItems: "' . $defaultItems . '",
            defaultColumns: "' . $defaultColumns . '",
            idInType: '.$category->idInType.'
        });
    }( window.jQuery );

', CClientScript::POS_END);

?>