<?php
if( !$ajax ){
	$cs=Yii::app()->getClientScript();
	$colorpickerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/bootstrap-colorpicker-plus' );
	$cs->registerCssFile($colorpickerBaseUrl.'/css/my.css');
	$cs->registerCssFile($colorpickerBaseUrl.'/css/bootstrap-colorpicker.min.css');
	$cs->registerCssFile($colorpickerBaseUrl.'/css/bootstrap-colorpicker-plus.css');
	$cs->registerScriptFile($colorpickerBaseUrl.'/js/bootstrap-colorpicker.min.js', CClientScript::POS_END);
	$cs->registerScriptFile($colorpickerBaseUrl.'/js/bootstrap-colorpicker-plus.js', CClientScript::POS_END);
	
	$cs->registerScriptFile( '//code.jquery.com/ui/1.11.4/jquery-ui.js', CClientScript::POS_END );
	$cs->registerCssFile( Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css' );
	
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wInformeMonitoringSettingsWidget.js' ), CClientScript::POS_END );
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/clipboard.min.js' ) );
}

	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
if( !$ajax ){
?>
<section class="section_offset <?=$ins?> position-relative spinner-margin">

	<h4 class="toggle_btn section_title"><?=Yii::t($NSi18n, 'Informers')?></h4>

	<div class="toggle_content" style="display: none;">
<?php } ?>
<?php if( $ajax ){ ?>
		<section class="section_offset paddingBottom30">
		<div class="blockCounter">
			<div class="numberSquare">1</div>
			<?=Yii::t( $NSi18n, 'Informer style' );?>
		</div>
		<div class="box2 box_informer">
			<div class="row stylesList" >
			<?php
				foreach( $stylesList as $styleId => $styleTitle ){
					echo CHtml::openTag('div', array('class' => 'imgMonitoringInformer', 'data-styleId' => $styleId));
						echo CHtml::image( Yii::app()->createAbsoluteUrl("informers/getInformer", array('st' => $styleId, 'cat' => $category->id, 'accId' => $idMember) ) );
						if( $defaultStyle->id == $styleId ) echo CHtml::tag('div', array('class' => 'selected'), ' ');
					echo CHtml::closeTag('div');
				}
			?>
			</div>

		</div>
	</section>

	<!-- - - - - - - - - - - - - - Change Color Box - - - - - - - - - - - - - - - - -->
	<section class="section_offset">
		<div class="blockCounter">
			<div class="numberSquare">2</div>
			<?=Yii::t( $NSi18n, 'Informer settings' );?>
		</div>
		<div class="box2 box_informer informerColorsPreview"></div>
		<div class="box_informer">
		
			<div class="informerColorsBox"><?php $this->widget( 'widgets.InformerColorsWidget', Array( 'model' => $defaultStyle ));?></div>
			
	
		</div><!-- / .box_informer -->
	</section>

	<!-- - - - - - - - - - - - - - End of Change Color Box - - - - - - - - - - - - - - - - -->
	
	<section class="section_offset chooseFontSizeAndPaddings changeSettings">
		<h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Change width' )?></h4>
		<div class="box2 box_informer">
			<div class="sliderWidthBox">
				<input type="text" class="widthVal">
			</div>
		</div>
	</section>

	<!-- - - - - - - - - - - - - - End of Choose Currency - - - - - - - - - - - - - - - - -->

	<section class="section_offset">
		<h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Preview' )?></h4>
		<div class="box2 box_informer">
			<div class="clearfix">
				<div class="previewInformerWrap"></div>
			</div>
		</div>
	</section>

	<section class="section_offset">
		<div class="blockCounter xsCode">
			<div class="numberSquare">3</div>
			<?=Yii::t( $NSi18n, 'Copy the code to insert into your website' )?>
		</div>
		<div class="codeForInsert clearfix">
			<textarea name="code" id="code" rows="10" cols="30" class="informerCodeWrap"></textarea>
		</div>
		<div class="copyCode" data-clipboard-action="copy" data-clipboard-target="#code"><?=Yii::t( $NSi18n, 'Copy to clipboard' )?></div>
	</section>

<?php } ?>		
<?php if( !$ajax ){ ?>
	</div>

</section>

<?php

Yii::app()->clientScript->registerScript('informersSettingsJs', '

	!function( $ ) {
		var ns = ".' . $ns . '";
		var nsText = ".' . $ns . '";
		var ins = ".' . $ins . '";

		ns.wInformeMonitoringSettingsWidget = wInformeMonitoringSettingsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+" .' . $ins . '",
			catId: ' . $category->id . ',
			getInformerUrl: "' . CommonLib::httpsProtocol( Yii::app()->createAbsoluteUrl("informers/getInformer" ) ) . '",
			getInformerColors: "' . Yii::app()->createAbsoluteUrl("informers/getColors" ) . '",
			memberUrl: "' . Yii::app()->createAbsoluteUrl("monitoring/single", array('id' => $idMember) ) . '",
			loadContentUrl: "' . Yii::app()->createAbsoluteUrl("monitoring/ajaxLoadInformerSettingsWidget" ) . '",
			minWidth: 135,
			maxWidth: 2000,
			idMember: '.$idMember.',
		});
	}( window.jQuery );

', CClientScript::POS_END);
}
?>