<?php

	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$cs=Yii::app()->getClientScript();
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wInformerCalendarSettingsWidget.js' ), CClientScript::POS_END );
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/clipboard.min.js' ) );
	
	$cs->registerScript('jsCalendarInformerSettings', '
		!function( $ ) {
			$(".colorInformer").click(function(){ $(this).parent(".section_offset").find(".box2").slideToggle("slow"); });
			if (window.innerWidth < 410){ $("textarea").attr("rows", 12); }	
		}( window.jQuery );
	', CClientScript::POS_END);
	

?>
<div class="<?=$ins?> position-relative spinner-margin" >
	
	<?php /*<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:735px" scrolling="no" src="https://devv.fortrader.ru/informers/getInformer?st=55&cat=24"></iframe>*/?>
	
	
	<section class="section_offset hrNone">
		<section class="section_offset paddingBottom30">
			<div class="blockCounter"><?=Yii::t($NSi18n, 'Informer settings')?></div>
		</section>

		<section class="section_offset">
			<h4 class="title7 colorInformer"><?=Yii::t($NSi18n, 'Update calendar events in real time')?></h4>
			<div class="box2 box_informer">
				<ul class="mycolumn realTimeUpdate">
					<li class="columnActive" data-val="1"><?=Yii::t($NSi18n, 'Enable')?></li>
					<li data-val="0"><?=Yii::t($NSi18n, 'Disable')?></li>
				</ul>
			</div>
		</section>

		<div class="box2 box_informer informerColorsPreview marginBottom20"></div>

		<?php /*<section class="section_offset">
			<h4 class="title7 colorInformer">Експорт</h4>
			<div class="box2 box_informer">
				<ul class="mycolumn clearfix">
					<li class="columnActive">CSV</li>
					<li>XML</li>
				</ul>
			</div>
		</section>*/?>
		
		<section class="section_offset">
			<h4 class="title7 colorInformer"><?=Yii::t($NSi18n, 'Impact')?></h4>
			<div class="box2 box_informer">
				<ul class="informer-calendar__event clearfix impactTabs">
					<li class="calendar__event-high active" data-val="3"><?=Yii::t( '*', ucfirst( CalendarEvent2ValueModel::getModelTitleSerious( 3 ) ))?></li>
					<li class="calendar__event-medium active" data-val="2"><?=Yii::t( '*', ucfirst( CalendarEvent2ValueModel::getModelTitleSerious( 2 ) ))?></li>
					<li class="calendar__event-low active" data-val="1"><?=Yii::t( '*', ucfirst( CalendarEvent2ValueModel::getModelTitleSerious( 1 ) ))?></li>
				</ul>
			</div>
		</section>
		<section class="section_offset">
			<h4 class="title7 colorInformer"><?=Yii::t($NSi18n, 'Symbols')?></h4>
			
			<div class="box2 box_informer symbolsTabs">
				<?php
					foreach( $currecies as $i => $currecy ){
						if( $i%4 == 0 ){
							$class = 'mycolumn instrumentMatgin clearfix';
							if( $i==0 ) $class = 'mycolumn clearfix';
							 echo CHtml::openTag('ul', array('class' => $class));
						} 
						
						echo CHtml::tag('li', array('class' => 'columnActive', 'title' => $currecy['name']), $currecy['code']);
						if( $i%4 == 3 ) echo CHtml::closeTag('ul');
					}
					if( $i%4 != 3 ) echo CHtml::closeTag('ul');
				?>
			</div>
		</section>
		
		
<?php /*		<section class="section_offset">
			<h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Preview' )?></h4>
			<div class="box2 box_informer">

				<div class="myText"><?=Yii::t( $NSi18n, 'PreviewInformerTex' )?></div>
				<div class="clearfix">
					<div class="previewInformerWrap"></div>
				</div>
			</div>
		</section>*/?>
		
		<section class="section_offset">
			<div class="informer-calendar__text-code">
				
				<?=Yii::t( $NSi18n, 'Copy the code to insert into your website' )?>
			</div>
			<div class="codeForInsert clearfix">
				<textarea name="code" id="code" rows="10" cols="30" class="informerCodeWrap"></textarea>
			</div>
			<div class="copyCode" data-clipboard-action="copy" data-clipboard-target="#code"><?=Yii::t( $NSi18n, 'Copy to clipboard' )?></div>
		</section>
		
        <?php
            $this->widget('widgets.UnisenderSubscribeWidget', array(
                'ns' => 'nsActionView',
                'listId' => 12531533
            ));
        ?>
		
	</section>
			
</div>


<?php

Yii::app()->clientScript->registerScript('informersSettingsJs', '

	!function( $ ) {
		var ns = ".' . $ns . '";
		var nsText = ".' . $ns . '";
		var ins = ".' . $ins . '";

		ns.wInformerSettingsWidget = wInformerSettingsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+" .' . $ins . '",
			catId: ' . $category->id . ',
			getInformerUrl: "' . CommonLib::httpsProtocol( Yii::app()->createAbsoluteUrl("informers/getInformer" ) ) . '",
			styleId: ' . $defaultStyle->id . ',

		});
	}( window.jQuery );

', CClientScript::POS_END);

?>