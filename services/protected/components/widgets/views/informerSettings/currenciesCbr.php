<?php
    $cs=Yii::app()->getClientScript();
    $colorpickerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/bootstrap-colorpicker-plus' );
    $cs->registerCssFile($colorpickerBaseUrl.'/css/my.css');
    $cs->registerCssFile($colorpickerBaseUrl.'/css/bootstrap-colorpicker.min.css');
    $cs->registerCssFile($colorpickerBaseUrl.'/css/bootstrap-colorpicker-plus.css');
    $cs->registerScriptFile($colorpickerBaseUrl.'/js/bootstrap-colorpicker.min.js', CClientScript::POS_END);
    $cs->registerScriptFile($colorpickerBaseUrl.'/js/bootstrap-colorpicker-plus.js', CClientScript::POS_END);
    
    $cs->registerScriptFile( '//code.jquery.com/ui/1.11.4/jquery-ui.js', CClientScript::POS_END );
    $cs->registerCssFile( Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css' );
    
    $jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
    $cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
    $cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
    
    $cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wInformerCurrenciesCbrSettingsHtmlWidget.js' ), CClientScript::POS_END );
    $cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/clipboard.min.js' ) );
    
    $ns = $this->getNS();
    $ins = $this->getINS();
    $NSi18n = $this->getNSi18n();
?>
<div class="<?=$ins?> position-relative spinner-margin" >
    <div class='stylesAndJsFromInformer'></div>
    <section class="section_offset paddingBottom30">
        <div class="blockCounter">
            <div class="numberSquare">1</div>
            <?=Yii::t( $NSi18n, 'Select destination currency' );?>
        </div>
        <div class="box2 box_informer selectToCurrencyCourse">
        

        <select class="select_type1 toCurrencyList">
        <?php
            $i=1;
            $flagStyles = '';
            foreach( $items as $key => $val ){
                echo CHtml::tag(
                    'option',
                    array(
                        'value' => $key,
                        'class' => 'lang ' . $val['country'],
                        'selected' => false,
                    ),
                    $val['name']
                );
                $flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $val['country']);
                if( $flagSrc ) $flagStyles .= ".lang.{$val['country']}, .lang.{$val['country']} .jq-selectbox__select-text{background-image: url({$flagSrc});}";
                if( $i == 2 ){
                    echo CHtml::tag(
                        'option', 
                        array(
                            'value' => $defautCurrency->id,
                            'class' => 'lang ' . $defautCurrency->country->alias,
                            'selected' => true,
                        ), 
                        $defautCurrency->name
                    );
                    $flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $defautCurrency->country->alias);
                    if( $flagSrc ) $flagStyles .= ".lang.{$defautCurrency->country->alias}, .lang.{$defautCurrency->country->alias} .jq-selectbox__select-text{background-image: url({$flagSrc});}";
                }
                $i++;
            }
            ?>
        </select>
        </div>
    </section>
    <section class="section_offset">
        <div class="blockCounter">
            <div class="numberSquare">2</div>
            <?=Yii::t( $NSi18n, 'Informer style' );?>
        </div>
        <h4 class="title7 colorInformer instrumentMatgin1"><?=Yii::t( $NSi18n, 'Choose informer style' );?></h4>
        <div class="box2 box_informer">
            <div class="col6">
            <?=CHtml::dropDownList( 'style', $defaultStyle->id, $stylesList, array( 'class' => 'stylesList' ) );?>
            </div>
            <div class="col6">
                <div class="myInformer styleIframeWrap">
                </div>
            </div>
        </div>
    </section>
    <div class="box2 box_informer informerColorsPreview"></div>
    <!-- - - - - - - - - - - - - - Change Color Box - - - - - - - - - - - - - - - - -->
    <section class="section_offset">
        <div class="blockCounter">
            <div class="numberSquare">3</div>
            <?=Yii::t( $NSi18n, 'Informer settings' );?>
        </div>
        <h4 class="title7 colorInformer instrumentMatgin1"><?=Yii::t( $NSi18n, 'Change informer colors' );?></h4>
        <div class="box2 box_informer informerColorsBox"></div>
    </section>
    <!-- - - - - - - - - - - - - - End of Change Color Box - - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - End of Choose Currency - - - - - - - - - - - - - - - - -->
    <section class="section_offset">
        <div class="box2 box_informer informerCategoriesPreview"></div>
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Select items from category' )?> <?=$category->title?></h4>
        <div class="box2 box_informer informerItemsBox">
        <?php
            $i=1;
            $defaultItems = '';
            foreach( $items as $key => $item ){
                if( $i % 4 == 1 ){
                    $marginClass = '';
                    if( $i > 1 ) $marginClass = 'instrumentMatgin';
                    echo CHtml::openTag( 'ul', array( 'class' => 'mycolumn clearfix ' . $marginClass ) );
                } 
                    $activeClass = '';
                    if( $item['toInformer'] ){
                        if( !$defaultItems ){
                            $defaultItems = $key;
                        }else{
                            $defaultItems .= ',' . $key;
                        }
                        $activeClass = 'columnActive';
                    } 
                    echo CHtml::tag( 'li', array( 'class' => $activeClass, 'data-item' => $key ), $item['name'] );
                if( $i % 4 == 0 ) echo CHtml::closeTag( 'ul');
                $i++;
            }
            if( ($i-1) %4 !=0 )  echo CHtml::closeTag( 'ul');
            
            if( $category->type == 'currencies' ){ ?>
                <div class="checkbox_box marginTop20">
                    <input type="checkbox" checked="checked" class="inpSettings" name="currenciesByCode" id="currenciesByCode">
                    <label class="square_input" for="currenciesByCode"><i></i><?=Yii::t( $NSi18n, 'Show currency code' )?></label>
                </div>
        <?php
            }
        ?>
        
        </div>
    </section>

    <section class="section_offset informerColumsSection">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Select columns' )?></h4>
        <div class="box2 box_informer informerColumsBox">
        <?php
            $i=1;
            $defaultColumns = '';
            foreach( $colums as $colum ){
                if( $i % 4 == 1 ){
                    $marginClass = '';
                    if( $i > 1 ) $marginClass = 'instrumentMatgin';
                    echo CHtml::openTag( 'ul', array( 'class' => 'mycolumn clearfix ' . $marginClass ) );
                } 
                    $activeClass = '';
                    if( $i == 1 ){
                        $defaultColumns = $colum;
                        $activeClass = 'columnActive';
                    } 
                    echo CHtml::tag( 'li', array( 'class' => $activeClass, 'data-colum' => $colum ), Yii::t( 'informerColumnNames', $colum ) );
                if( $i % 4 == 0 ) echo CHtml::closeTag( 'ul');
                $i++;
            }
            if( ($i-1) %4 !=0 )  echo CHtml::closeTag( 'ul');
        ?>
        </div>
    </section>


    <section class="section_offset informerTitleSection">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Enter informer title' )?></h4>
        <div class="box2 box_informer">
            <input type="text" id="informerTitle" name="informerTitle" value="<?=$category->informerTitle?>" placeholder="<?=$category->informerTitle?>">
        </div>
    </section>
    <section class="section_offset informerTitleSection">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Enter columns titles' )?></h4>
        <div class="informerTextsSettings clearfix">
            <div class="box2 box_informer">
                <input type="text" name="texts[toolTitle]" class="informerTexts" value="<?=$category->toolTitle?>" placeholder="<?= $category->toolTitle ?>">
            </div>
            <?php
            foreach ($colums as $index => $colum) {
                $hideClass = 'hide';
                if (!$index) {
                    $hideClass = '';
                }
                echo CHtml::openTag('div', array('class' => 'box2 box_informer ' . $hideClass));
                    echo CHtml::textField(
                        "texts[{$colum}]",
                        $category->getColumnName($colum),
                        array(
                            'placeholder' => $category->getColumnName($colum),
                            'class' => 'informerTexts'
                        )
                    );
                echo CHtml::closeTag('div');
            }
            ?>
        </div>
    </section>
    

    <section class="section_offset chooseFontSizeAndPaddings changeSettings">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Change font size, paddings, margins and width' )?></h4>
        <div class="box2 box_informer">
            <div class="sliderVal"></div>    
            
            <div class="checkbox_box">
                <input type="checkbox" class="inpSettings" name="fixedWidthBtn" id="fixedWidthBtn">
                <label class="square_input" for="fixedWidthBtn"><i></i><?=Yii::t( $NSi18n, 'Fixed width' )?></label>
            </div>
            <div class="sliderWidthBox">
                <div class="sliderWidthVal"></div>    
                <input type="text" class="widthVal">
            </div>
        </div>
    </section>
    
    <section class="section_offset">
        <div class="checkbox_box">
            <input type="checkbox" checked="checked" class="inpSettings" name="hideGetInformerBtn" id="hideGetInformerBtn">
            <label class="square_input" for="hideGetInformerBtn"><i></i><?=Yii::t( $NSi18n, 'Hide get informer button' )?></label>
        </div>
    </section>

    <section class="section_offset">
        <div class="checkbox_box">
            <input type="checkbox" class="inpSettings" name="hideHeader" id="hideHeader">
            <label class="square_input" for="hideHeader"><i></i><?=Yii::t( $NSi18n, 'Hide header' )?></label>
        </div>
    </section>

    <section class="section_offset">
        <div class="checkbox_box">
            <input type="checkbox" class="inpSettings" name="hideDate" id="hideDate">
            <label class="square_input" for="hideDate"><i></i><?=Yii::t( $NSi18n, 'Hide date' )?></label>
        </div>
    </section>
    
    <section class="section_offset hideDiffValuesBox">
        <div class="checkbox_box">
            <input type="checkbox" class="inpSettings" name="hideDiffValues" id="hideDiffValues">
            <label class="square_input" for="hideDiffValues"><i></i><?=Yii::t( $NSi18n, 'Hide diff values' )?></label>
        </div>
    </section>


     <?php/*<section class="section_offset">
        <h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Preview' )?></h4>
        <div class="box2 box_informer">

            <div class="myText"><?=Yii::t( $NSi18n, 'PreviewInformerTex' )?></div>
            <div class="clearfix">
                <div class="previewInformerWrap"></div>
            </div>
        </div>
    </section>*/?>
    
    <section class="section_offset">
        <div class="blockCounter xsCode">
            <div class="numberSquare">4</div>
            <?=Yii::t( $NSi18n, 'Copy the code to insert into your website' )?>
        </div>

        <?php /*<h4 class="title7 title72"><?=Yii::t( $NSi18n, 'Html' )?></h4>
        <div class="codeForInsert clearfix">
            <textarea name="code" id="code" rows="10" cols="30" class="informerCodeWrap"></textarea>
        </div>
        <div class="copyCode copyCode1" data-clipboard-action="copy" data-clipboard-target="#code"><?=Yii::t( $NSi18n, 'Copy to clipboard' )?></div>

        <h4 class="title7 title72"><?=Yii::t( $NSi18n, 'Iframe' )?></h4>*/?>
        <div class="codeForInsert clearfix">
            <textarea name="code2" id="code2" rows="10" cols="30" class="informerCodeWrap2"></textarea>
        </div>
        <div class="copyCode copyCode2" data-clipboard-action="copy" data-clipboard-target="#code2"><?=Yii::t( $NSi18n, 'Copy to clipboard' )?></div>
    </section>
    
    <?php
            $this->widget('widgets.UnisenderSubscribeWidget', array(
                'ns' => 'nsActionView',
                'listId' => 12531533
            ));
        ?>
    
</div>

<?php Yii::app()->clientScript->registerCss('flagStyles', $flagStyles);?>

<?php

Yii::app()->clientScript->registerScript('informersSettingsJs', '

    !function( $ ) {
        var ns = ".' . $ns . '";
        var nsText = ".' . $ns . '";
        var ins = ".' . $ins . '";

        ns.wInformerSettingsWidget = wInformerSettingsWidgetOpen({
            ns: ns,
            ins: ins,
            selector: nsText+" .' . $ins . '",
            catId: ' . $category->id . ',
            getInformerUrl: "' . CommonLib::httpsProtocol( Yii::app()->createAbsoluteUrl("informers/getInformer" ) ) . '",
            getInformerColors: "' . Yii::app()->createAbsoluteUrl("informers/getColors" ) . '",
            minWidth: 150,
            maxWidth: 2000,
            catType: "' . $category->type . '",
            defaultItems: "' . $defaultItems . '",
            defaultColumns: "' . $defaultColumns . '",
        });
    }( window.jQuery );

', CClientScript::POS_END);

?>