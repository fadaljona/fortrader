<?php
	$cs=Yii::app()->getClientScript();
	$colorpickerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/bootstrap-colorpicker-plus' );
	$cs->registerCssFile($colorpickerBaseUrl.'/css/my.css');
	$cs->registerCssFile($colorpickerBaseUrl.'/css/bootstrap-colorpicker.min.css');
	$cs->registerCssFile($colorpickerBaseUrl.'/css/bootstrap-colorpicker-plus.css');
	$cs->registerScriptFile($colorpickerBaseUrl.'/js/bootstrap-colorpicker.min.js', CClientScript::POS_END);
	$cs->registerScriptFile($colorpickerBaseUrl.'/js/bootstrap-colorpicker-plus.js', CClientScript::POS_END);
	
	$cs->registerScriptFile( '//code.jquery.com/ui/1.11.4/jquery-ui.js', CClientScript::POS_END );
	$cs->registerCssFile( Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css' );
	
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wInformerConverterSettingsWidget.js' ), CClientScript::POS_END );
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/clipboard.min.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="<?=$ins?> position-relative spinner-margin" >

	<section class="section_offset paddingBottom0">
		<div class="blockCounter">
			<div class="numberSquare">1</div>
			<?=Yii::t( $NSi18n, 'Informer style' );?>
		</div>
		<div class="box2 box_informer">
			<div class="row">
				<div class="grid_6 grid_md_12 informer_setings">
				<?=CHtml::dropDownList( 'style', $defaultStyle->id, $stylesList, array( 'class' => 'stylesList select_type1' ) );?>
				</div>
			</div>

			<div class="myInformer styleIframeWrap informer_setings"></div>

		</div>
	</section>
	<div class="box2 box_informer informerColorsPreview marginBottom20"></div>

	<!-- - - - - - - - - - - - - - Change Color Box - - - - - - - - - - - - - - - - -->
	<section class="section_offset">
		<div class="blockCounter">
			<div class="numberSquare">2</div>
			<?=Yii::t( $NSi18n, 'Informer settings' );?>
		</div>
		
		<div class="box_informer">
		
			<div class="informerColorsBox"><?php $this->widget( 'widgets.InformerColorsWidget', Array( 'model' => $defaultStyle ));?></div>
			
			<div class="row">
				<div class="grid_3 grid_md_6 grid_sm_12">
					<div class="informer_setings">
						<label><?=Yii::t( $NSi18n, 'From currency' );?></label>
						<select class="select_type1 fromCurrencyList">
						<?php
							$i=1;
							$flagStyles = '';
							foreach( $currenciesForConverter as $key => $val ){
								$selected = false;
								if( $settings->convertFrom == $val['id'] ) $selected = true;
								echo CHtml::tag(
									'option',
									array(
										'data-value' => $val['value'],
										'value' => $val['id'],
										'data-nominal' => $val['nominal'],
										'class' => 'lang ' . $val['country'],
										'selected' => $selected,
									),
									$key
								);
								$flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $val['country']);
								if( $flagSrc ) $flagStyles .= ".lang.{$val['country']}, .lang.{$val['country']} .jq-selectbox__select-text{background-image: url({$flagSrc});}";
								if( $i == 2 ){
									$selected = false;
									if( $settings->convertFrom == $defaultCurrency->id ) $selected = true;
									echo CHtml::tag(
										'option', 
										array(
											'data-value' => 1,
											'value' => $defaultCurrency->id,
											'data-nominal' => 1,
											'class' => 'lang ' . $defaultCurrency->country->alias,
											'selected' => $selected,
										), 
										$defaultCurrency->code
									);
									$flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $defaultCurrency->country->alias);
									if( $flagSrc ) $flagStyles .= ".lang.{$defaultCurrency->country->alias}, .lang.{$defaultCurrency->country->alias} .jq-selectbox__select-text{background-image: url({$flagSrc});}";
								}
								$i++;
							}
						?>
						</select>
					</div><!-- / .informer_setings -->
				</div><!-- / .col- -->
				
				<div class="grid_3 grid_md_6 grid_sm_12">
					<div class="informer_setings">
						<label><?=Yii::t( $NSi18n, 'To currency' );?></label>
						<select class="select_type1 toCurrencyList">
						<?php
							$i=1;
							foreach( $currenciesForConverter as $key => $val ){
								$selected = false;
								if( $settings->convertTo == $val['id'] ) $selected = true;
								echo CHtml::tag(
									'option',
									array(
										'data-value' => $val['value'],
										'value' => $val['id'],
										'data-nominal' => $val['nominal'],
										'class' => 'lang ' . $val['country'],
										'selected' => $selected,
									),
									$key
								);
								if( $i == 2 ){
									$selected = false;
									if( $settings->convertTo == $defaultCurrency->id ) $selected = true;
									echo CHtml::tag(
										'option', 
										array(
											'data-value' => 1,
											'value' => $defaultCurrency->id,
											'data-nominal' => 1,
											'class' => 'lang ' . $defaultCurrency->country->alias,
											'selected' => $selected,
										), 
										$defaultCurrency->code
									);
								}
								$i++;
							}
						?>
						</select>
					</div><!-- / .informer_setings -->
				</div><!-- / .col- -->
				<div class="grid_3 grid_md_6 grid_sm_12">
					<div class="informer_setings">
						<label><?=Yii::t( $NSi18n, 'The default amount' );?></label>
						<input type="text" value="<?=$settings->convertFromVal?>" class="align_center defaultAmount">
					</div><!-- / .informer_setings -->
				</div><!-- / .col- -->
				<div class="grid_3 grid_md_6 grid_sm_12">
					<div class="informer_setings not_label">
						<label></label>
						<button class="informer_setings_default restoreDefaults"><?=Yii::t( $NSi18n, 'Restore defaults' );?></button>
					</div><!-- / .informer_setings -->
				</div><!-- / .col- -->
			</div><!-- / .row -->
		</div><!-- / .box_informer -->
	</section>

	<!-- - - - - - - - - - - - - - End of Change Color Box - - - - - - - - - - - - - - - - -->
	
	<section class="section_offset chooseFontSizeAndPaddings changeSettings">
		<h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Change width' )?></h4>
		<div class="box2 box_informer">
			
			<div class="checkbox_box">
				<input type="checkbox" class="inpSettings" name="fixedWidthBtn" id="fixedWidthBtn">
				<label class="square_input" for="fixedWidthBtn"><i></i><?=Yii::t( $NSi18n, 'Fixed width' )?></label>
			</div>
			<div class="sliderWidthBox">
				<div class="sliderWidthVal"></div>	
				<input type="text" class="widthVal">
			</div>
		</div>
	</section>
	
	<section class="section_offset hideGetInformerBtnBox">
		<div class="checkbox_box">
			<input type="checkbox" checked="checked" class="inpSettings" name="hideGetInformerBtn" id="hideGetInformerBtn">
			<label class="square_input" for="hideGetInformerBtn"><i></i><?=Yii::t( $NSi18n, 'Hide get informer button' )?></label>
		</div>
	</section>

	<!-- - - - - - - - - - - - - - End of Choose Currency - - - - - - - - - - - - - - - - -->

<?php /*	<section class="section_offset">
		<h4 class="title7 colorInformer"><?=Yii::t( $NSi18n, 'Preview' )?></h4>
		<div class="box2 box_informer">
			<div class="clearfix">
				<div class="previewInformerWrap"></div>
			</div>
		</div>
	</section>*/?>

	<section class="section_offset">
		<div class="blockCounter xsCode">
			<div class="numberSquare">3</div>
			<?=Yii::t( $NSi18n, 'Copy the code to insert into your website' )?>
		</div>
		<div class="codeForInsert clearfix">
			<textarea name="code" id="code" rows="10" cols="30" class="informerCodeWrap"></textarea>
		</div>
		<div class="copyCode" data-clipboard-action="copy" data-clipboard-target="#code"><?=Yii::t( $NSi18n, 'Copy to clipboard' )?></div>
	</section>
	
	<?php
            $this->widget('widgets.UnisenderSubscribeWidget', array(
                'ns' => 'nsActionView',
                'listId' => 12531533
            ));
        ?>

<?php Yii::app()->clientScript->registerCss('flagStyles', $flagStyles);?>
</div>

<?php

Yii::app()->clientScript->registerScript('informersSettingsJs', '

	!function( $ ) {
		var ns = ".' . $ns . '";
		var nsText = ".' . $ns . '";
		var ins = ".' . $ins . '";

		ns.wInformerConverterSettingsWidget = wInformerConverterSettingsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+" .' . $ins . '",
			catId: ' . $category->id . ',
			getInformerUrl: "' . CommonLib::httpsProtocol( Yii::app()->createAbsoluteUrl("informers/getInformer" ) ) . '",
			getInformerColors: "' . Yii::app()->createAbsoluteUrl("informers/getColors" ) . '",
			minWidth: 100,
			maxWidth: 2000,
			convertFrom: ' . $settings->convertFrom . ',
			convertTo: ' . $settings->convertTo . ',
			convertFromVal: ' . $settings->convertFromVal . ',
		});
	}( window.jQuery );

', CClientScript::POS_END);

?>