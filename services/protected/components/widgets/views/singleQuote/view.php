<?php 
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/autobahn.min.js' ), ClientScript::POS_TOP_HEAD );
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wSingleQuoteWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="<?=$ins?>" itemscope itemtype="http://schema.org/FinancialProduct">

	<meta itemprop="name" content="<?=$model->name?>" />
	<meta itemprop="serviceType" content="Quotes" />
	<meta itemprop="description" content="<?=$model->tooltip?>" />

	<!-- - - - - - - - - - - - - - Title Box 1 - - - - - - - - - - - - - - - - -->
	<div class="title_box1 clearfix">
		<div class="rates_box1">
			<?php
			if( $model->sourceType != 'yahoo' ) $bid = $model->currentD1Bid;
			if( $model->sourceType == 'yahoo' ) $bid = $model->lastBidForYahoo;
			if( $model->lastBid ){
				$data_last_bid = $model->lastBid;
				
				if( !isset( $model->tickData->precision ) ){
					$precision = strlen($bid) - strpos( $bid, '.' ) - 1;
					if( $precision > 6 ) $precision = 6;
				}else{
					$precision = $model->tickData->precision;
                }
                if ($precision < 0) {
                    $precision = 2;
                }
				$Chg = $bid - $model->lastBid;
				$ChgPercent = $Chg / $model->lastBid * 100 ;
				$Chg = number_format($Chg, $precision, '.', ' ');
				$ChgPercent = number_format($ChgPercent, $precision, '.', ' ');
				$arrow = 'fa-long-arrow-down red_color';
				$color = 'red_color';
				if( $Chg > 0 ){
					$arrow = 'fa-long-arrow-up green_color';
					$color = 'green_color';
					$Chg = '+'.$Chg;
					$ChgPercent = '+'.$ChgPercent;
				}		
			}else{
				$ChgPercent = $Chg = $data_last_bid = 'n/a';
				$ChgColorClass = '';
			}
			?>
			<i class="fa <?=$arrow?> arrow-icon"></i>
			<p class="rates_box1_inner pid-<?=$model->name?>-bid" data-precision="<?=$precision?>" data-last-bid="<?=$data_last_bid?>"><?=$bid?></p>
			<div class="rates_box1_difference">
				<span class="pid-<?=$model->name?>-chg"><?=$Chg?></span> 
				<span class="<?=$color?> <?=$model->name?>-chg-percent-wrap">(<span class="pid-<?=$model->name?>-chg-percent"><?=$ChgPercent?></span>%)</span>
			</div>
		</div>
		<div class="watch_box">
			<p><?=Yii::t( '*', 'Real time' )?></p>
			<i class="fa fa-clock-o watch-icon"></i>
			<div id="digital_watch"></div>
		</div>
	</div>
	
	<div class="forecast_box">
		<div class="real_time_choice">
			<?php if ($model->sourceType != 'yahoo') {?>
			<input type="checkbox" id="real_time" checked>
			<label for="real_time" class="square_input"><i></i><?=Yii::t( '*', 'Update in real time' )?></label>
			<?php }?>
		</div>
	</div>	
	
	<!-- - - - - - - - - - - - - - End of Title Box 1 - - - - - - - - - - - - - - - - -->
</div>
<script>
	!function( $ ) {
		var ns = ".<?=$ns?>";
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var quotesKey = '<?=$model->name?>';

		ns.wSingleQuoteWidget = wSingleQuoteWidgetOpen({
			ns: ns,
			ins: ins,
			quotesKey: quotesKey,
			selector: nsText+' .<?=$ins?>',
			sourceType: '<?=$model->sourceType?>',
		});
	}( window.jQuery );
</script>