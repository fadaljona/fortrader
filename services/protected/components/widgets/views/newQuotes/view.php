<?php 
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/autobahn.min.js' ) );
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wNewQuotesWidget.js' ) );
		
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$quotesSettings = QuotesSettingsModel::getInstance();
?>

<div class="wNewQuotesWidget">
	<?=$tables?>
	
	<? $this->widget('widgets.YahooPopularQuotesWidget',Array( 'title'  => Yii::t('*', 'Popular quotes') )); ?>
	
</div>

<script>


	!function( $ ) {
		var ns = ".<?=$ns?>";
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var connUrl = '<?=$quotesSettings->wsServerUrl?>';
		var quotesKey = 'ALL';

		ns.wNewQuotesWidget = wNewQuotesWidgetOpen({
			ns: ns,
			ins: ins,
			connUrl: connUrl,
			quotesKey: quotesKey,
			timeDataOffset: <?=$quotesSettings->realQuotesDataTimeDifference?>,
			selector: nsText+' .wNewQuotesWidget',
			lang: '<?=DecommentsPostsModel::getCatLangSuffix()?>',
		});
	}( window.jQuery );
</script>