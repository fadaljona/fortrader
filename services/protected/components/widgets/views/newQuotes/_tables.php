<?php
	$checkedQuotes = (string)Yii::app()->request->cookies['checkedQuotesOnFullList' . DecommentsPostsModel::getCatLangSuffix()];
	if( $checkedQuotes ){
		$checkedQuotes = explode(',', $checkedQuotes);
	}
	
	foreach( $quotesCats as $quotesCat ){?>
		<!-- - - - - - - - - - - - - - Tabl Quotes - - - - - - - - - - - - - - - - -->
		<section class="section_offset turn_box">
			<?php if(!$catId){?>
			<div class="tabl_quotes_header clearfix">
				<div class="tabl_quotes_btn alignright nav_buttons">
					<a href="javascript:;" class="options_btn">
						<i class="fa fa-sliders"></i>
						<span class="tooltip"><?=Yii::t( "*",'Settings') ?></span>
					</a>
					<a href="javascript:;" class="turn_btn"><span class="tooltip"></span></a>
				</div>
				<h4 class="tabl_quotes_title">
					<?=CHtml::link($quotesCat->name, $quotesCat->singleURL )?>
					<?php if($quotesCat->desc){?><br /><span><?=$quotesCat->desc?></span><?php }?>
				</h4>
				<div class="options_box">
					<h6 class="options_title"><?=Yii::t( "*",'Select symbols') ?></h6>
					<div class="clearfix">
					<?php 
						foreach( $quotesCat->symbols as $symbol ){
							$condition = '';
							if( $checkedQuotes ){
								$condition = in_array($symbol->name, $checkedQuotes);
							}else{
								$condition = $symbol->default == 'Yes';
							}
					?>
						<div class="options_item">
							<input type="checkbox" value="<?=$symbol->name?>" id="option<?=$symbol->name;?>" <?=$condition ? 'checked' : '' ?>>
							<label for="option<?=$symbol->name;?>"><?=$symbol->nalias ? $symbol->nalias : $symbol->name;?></label>
						</div>                                       
					<?php }?>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="turn_content">
				<div class="tabl_quotes">
					<table>
						<thead>
							<tr><th>Type</th><th>Last</th><th>Chg.</th><th>Chg.%</th><th>Time</th></tr>
						</thead>
						<tbody>
						<?php
							foreach( $quotesCat->symbols as $symbol ){
								$condition = '';
								if( $checkedQuotes ){
									$condition = in_array($symbol->name, $checkedQuotes);
								}else{
									$condition = $symbol->default == 'Yes';
								}
								if( $catId ) $condition = true; 
						?>
							<tr data-symbol="<?=$symbol->name?>" class="<?=!$condition ? 'hide' : ''?>">
								<td class="tabl_quotes_tipe">
									<span class="p_rel blue_color">
										<a href="<?=Yii::app()->createUrl('quotesNew/single', array( 'slug' => strtolower($symbol->name) ))?>">
											<?=$symbol->nalias ? $symbol->nalias : $symbol->name;?>
											<?=$symbol->tooltip ? '<span class="tooltip">'.$symbol->tooltip.'</span>' : ''?>
										</a>
									</span>
								</td>
								<?php 
									$colorClass = '';
									if( $symbol->tickData->trend == 1 ){
										$colorClass = 'green_color';
									}elseif( $symbol->tickData->trend == -1 ){
										$colorClass = 'red_color';
									}
                                    $bid = $symbol->tickData->bid;
                                    if (!$bid) {
                                        $bid = $symbol->closeQuote;
                                    }
									if( $symbol->lastBid ){
										$data_last_bid = $symbol->lastBid;
										$Chg = $bid - $symbol->lastBid;
										$ChgPercent = $Chg / $symbol->lastBid * 100 ;
										
										$ChgColorClass = 'red_color';
										if( $Chg > 0 ) $ChgColorClass = 'green_color';
										
										if( !isset( $symbol->tickData->precision ) ){
											$precision = strlen($bid) - strpos( $bid, '.' ) - 1;
										}else{
											$precision = $symbol->tickData->precision;
                                        }
										
										$Chg = number_format($Chg, $precision, '.', ' ');
										$ChgPercent = number_format($ChgPercent, $precision, '.', ' ');
										if( $Chg > 0 ){
											$Chg = '+'.$Chg;
											$ChgPercent = '+'.$ChgPercent;
										}
									}else{
										$ChgPercent = $Chg = $data_last_bid = Yii::t( "*",'n/a');
										$ChgColorClass = '';
                                    }
                                    
                                    if ($symbol->tickData) {
                                        $updateTime = $symbol->tickData->time;
                                    } else {
                                        $updateTime = date('H:i:s', $symbol->barTimeQuote);
                                    }
                                    
								?>
								<td data-title="Last" data-precision="<?=$precision?>" data-last-bid="<?=$data_last_bid?>" class="<?=$colorClass?> pid-<?=$symbol->name?>-bid">
									<?=$bid?>
								</td>
								<td data-title="Chg." class="<?=$ChgColorClass?> pid-<?=$symbol->name?>-chg">
									<?=$Chg?>
								</td>
								<td data-title="Chg.%" class="<?=$ChgColorClass?> pid-<?=$symbol->name?>-chg-percent">
									<?=$ChgPercent?>
								</td>
								<td data-title="Time" data-start-time="<?=$updateTime?>" class="pid-<?=$symbol->name?>-time pid-time"></td>
							</tr>
						<?php 
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</section>
		<!-- - - - - - - - - - - - - - End of Tabl Quotes - - - - - - - - - - - - - - - - -->
<?php	
	}
?>