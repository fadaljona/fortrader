<?php
	Yii::app()->clientScript->registerCoreScript('jquery.ui');
	Yii::app()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/autobahn.min.js' ) );
	Yii::app()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wNewsAggregatorWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$quotesSettings = QuotesSettingsModel::getInstance();

?>

<div class="<?=$ins?> spinner-margin position-relative">
	<a href="#" class="load_btn chench_btn second mb30 loadNewNews" style="display:none;"></a>
	
	<div class="hide newNews"></div>
	
	<?php
		$this->widget('widgets.listViews.RssListViewWidget', array(
			'dataProvider' => $newsDP,
			'template' => '{items}',
			'itemsTagName' => 'ul',
			'itemsCssClass' => 'news_big_list_box',
			'htmlOptions' => array(
				'class' => 'rssNewsList'
			)
		));
	?>
	
	<?php if( $newsDP->pagination->getPageCount() > 1 ){ ?>
		<a data-shot-text="<?=Yii::t($NSi18n, 'Load more news')?>" data-text="<?=Yii::t($NSi18n, 'Load more news')?>" class="load_btn loadOldNews chench_btn" href="#"></a>
	<?php }?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
	
		ns.wNewsAggregatorWidget = wNewsAggregatorWidgetOpen({
			ns: ns,
			lang: '<?=Yii::app()->language?>',
			connUrl: '<?=$quotesSettings->wsServerUrl?>',
			wsKey: '<?php if($currentCat) echo 'rssNewsCategory' . $currentCat; else echo 'rssNewsAll'; ?>',
			selector: '.<?=$ins?>',
			currentCat: <?=$currentCat?>,
			ajaxLoadListURL: '<?=Yii::app()->createUrl('newsAggregator/ajaxLoadList')?>',

		});
	}( window.jQuery );
</script>