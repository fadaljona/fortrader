<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="language_dropdown <?=$ins?>">
<?php

	$dropdownList = '<ul class="language_dropdown_list">';
	$label = '';
	foreach( $langs as $lang ){
		$model = LanguageModel::getByAlias( $lang['langAlias'] );

		if( $lang['langAlias'] == Yii::app()->language ){
			$label = '<div class="language_dropdown_label">' . CountryModel::getImgFlagByAlias('shiny', 16, $lang['country']) .$model->name. '</div>';
		}else{
			$dropdownList .= '<li><a href="' . $lang['baseUrl'] . substr(Yii::App()->request->getUrl(), strlen(Yii::app()->baseUrl)) . '">' .  CountryModel::getImgFlagByAlias('shiny', 16, $lang['country']) .$model->name. '</a></li>';
		}
	}
	$dropdownList .= '</ul>';
	
	echo $label;
	echo $dropdownList;
?>
</div>