<?
	$NSi18n = $this->getNSi18n();
?>
<div class="konk_pedestal clearfix1 row-fluid">
    <div class="pedestal position-relative pull-left">
        <?php if(count($topWinners) > 0) {?>
        <div class="pedestal_span span1 clearfix">
                    <span class="d_bl">
                        <? if( isset($topWinners[1]->member->user->country) ) echo $topWinners[1]->member->user->country->getIcon(' d_ib pull-left');?>
                        <p class="pull-left d_ib blue">
							<?=CHtml::link( CHtml::encode( $topWinners[1]->member->user->getShowName() ), $this->controller->createURL( '/contestMember/single', array( 'id' => $topWinners[1]->member->id ) ) )?>
                        </p>
                    </span>
            <span class="pull-left pl40 d_bl fw700">$<?=$topWinners[1]->prize?></span>
        </div>
        <div class="pedestal_span span2 clearfix">
                    <span class="d_bl">
                        <? if( isset($topWinners[0]->member->user->country) ) echo $topWinners[0]->member->user->country->getIcon(' d_ib pull-left');?>
                        <p class="pull-left d_ib blue">
							<?=CHtml::link( CHtml::encode( $topWinners[0]->member->user->getShowName() ), $this->controller->createURL( '/contestMember/single', array( 'id' => $topWinners[0]->member->id ) ) )?>
                        </p>
                    </span>
            <span class="pull-left pl45 d_bl fw700">$<?=$topWinners[0]->prize?></span>
        </div>
        <div class="pedestal_span span3 clearfix">
                    <span class="d_bl">
                        <? if( isset($topWinners[2]->member->user->country) ) echo $topWinners[2]->member->user->country->getIcon(' d_ib pull-left');?>
                        <p class="pull-left d_ib blue">
							<?=CHtml::link( CHtml::encode( $topWinners[2]->member->user->getShowName() ), $this->controller->createURL( '/contestMember/single', array( 'id' => $topWinners[2]->member->id ) ) )?>
                        </p>
                    </span>
            <span class="pull-left pl45 d_bl fw700">$<?=$topWinners[2]->prize?></span>
        </div>
        <img src="/services/assets-static/images/pedestal.jpg" alt="" height="310" width="453">
        <?php }?>
    </div>
    <div class="konkurs_table pull-right">
        <table class="zebra3" cols="4">
            <tbody>
            <tr>
                <th class=""><?=Yii::t( $NSi18n, 'Place' )?></th>
                <th class=""><?=Yii::t( $NSi18n, 'Country' )?></th>
                <th class=""><?=Yii::t( $NSi18n, 'Name' )?></th>
                <th class=""><?=Yii::t( $NSi18n, '% of earnings' )?></th>
                <th class=""><?=Yii::t( $NSi18n, 'Prize' )?></th>
            </tr>
            <?php
            foreach($winners as $winner) {
            ?>
                <tr class="hov3">
                    <td class="tac_pl0">
                        <?=strtoupper($winner->place)?>
                    </td>
                    <td class="tac_pl0">
                        <? if( isset($winner->member->user->country) ) echo $winner->member->user->country->getIcon();?>
                    </td>
                    <td class="">
						<?=CHtml::link( CHtml::encode( $winner->member->user->getShowName() ), $this->controller->createURL( '/contestMember/single', array( 'id' => $winner->member->id ) ) )?>
                    </td>
                    <td class="tac_pl0">
                        <?//=round($winner->prize * 100 / $allPrize, 1) %?>
						<?php
							$winner_gain = CommonLib::numberFormat( $winner->member->stats->gain );
							if( $winner->member->stats->gain > 0 ) $winner_gain = "+".$winner_gain;
							echo $winner_gain .= ' %';
						?>
                    </td>
                    <td class="tac_pl0">
                        $<?=$winner->prize?>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
