<?php
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wSearchCurrencyRatesWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="wrapper_quotes_search_box_2 <?=$ins?>">
	<div class="quotes_search_box_2">
		<form>
			<button class="quotes_search_btn"><i class="fa fa-search"></i></button>
			<input type="text" class="typeInSearchField" placeholder="<?=Yii::t( $NSi18n, 'Search' )?>">
			<ul class="quotes_search_list">
			</ul>
		</form>
	</div>
</div>

<script>
!function( $ ) {
	var ns = ".<?=$ns?>";
	var nsText = ".<?=$ns?>";
	var ins = ".<?=$ins?>";
	var minInputChars = <?=$minInputChars ? $minInputChars : 0?>;

	ns.wSearchCurrencyRatesWidget = wSearchCurrencyRatesWidgetOpen({
		ns: ns,
		ins: ins,
		selector: nsText+' .<?=$ins?>',
		minInputChars: minInputChars,
		ajaxSubmitURL: '<?=Yii::app()->createAbsoluteUrl('currencyRates/ajaxSearch');?>',
		type: '<?=$type?>',
	});
}( window.jQuery );
</script>