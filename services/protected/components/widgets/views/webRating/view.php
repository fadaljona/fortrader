<?php
	$cs=Yii::app()->getClientScript();
	$webRatingBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jquery.webRating' );

	if( $type == 'yii' ){
        if (!Yii::App()->request->isAjaxRequest) {
            $cs->registerCssFile($webRatingBaseUrl.'/jquery.rateyo.min.css');
            $cs->registerScriptFile($webRatingBaseUrl.'/jquery.rateyo.js', CClientScript::POS_END);
        
            $cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wWebRatingWidget.js' ), CClientScript::POS_END );

            if( $wrapperStyle ){
                $cs->registerCss($ins, '.' . $ins . '{' . $wrapperStyle . '}' );
            }
        }
	}elseif( $type == 'wp' ){
		wp_enqueue_style( 'jquery.rateyo.min.css', $webRatingBaseUrl.'/jquery.rateyo.min.css', array(), null );
		wp_enqueue_script('jquery.rateyo.js', $webRatingBaseUrl.'/jquery.rateyo.js', array('jquery'), null);
		wp_enqueue_script('wWebRatingWidget.js', CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wWebRatingWidget.js' ), array('jquery'), null);
	}
	
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	
	
?>
<div class="<?=$ins?>"></div>

<?php ob_start(); ?>
;(function($){
    $(document).ready(function(){
		wWebRatingWidgetOpen({
			selector: '.<?=$ns?> .<?=$ins?>',
			onChangeValue: <?=$onChangeValue ? '"'.$onChangeValue.'"' : 'null'?>,
			currentValue: <?=$currentValue?>,
			readOnly: <?=$readOnly ? 'true' : 'false'?>,
			numStars: <?=$numStars?>,
			starWidth: <?=$starWidth?>,
			normalFill: '<?=$normalFill?>',
			ratedFill: '<?=$ratedFill?>',
			maxValue: <?=$maxValue?>,
			returnNs: <?=CommonLib::boolToStr($returnNs)?>,
			nsToReturn: '<?=$ns?>',
		});
	});
})(jQuery);
<?php
	$widgetInitJs = ob_get_clean();

	if( $type == 'yii' ){
        if (!Yii::App()->request->isAjaxRequest) {
            Yii::app()->clientScript->registerScript('WebRatingWidget'. $ns . $ins . $onChangeValue , $widgetInitJs, CClientScript::POS_END);
        } else {
            echo '<script>' . $widgetInitJs . '</script>';
        }
		
	}elseif( $type == 'wp' ){
		add_action('wp_footer',function() use ($widgetInitJs){
			echo "<script>$widgetInitJs</script>";
		}, 100);
	}
?>