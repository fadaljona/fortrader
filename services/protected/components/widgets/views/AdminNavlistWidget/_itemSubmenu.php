<?
	if( $item->getDisable()) return;
	if( !$item->userHasRight()) return;
	
	$url = $item->getURL();
	$liClass = $item->getClass();
	
	$liClass = $item->getClass();
	if( $item->getActive()) {
		$liClass .= " active";
	}
?>
<li class="<?=$liClass?>">
	<a href="<?=$item->getURL()?>">
		<?=$item->getLabel()?>
	</a>
</li>