<?
	if( $item->getDisable()) return;
	if( !$item->userHasRight()) return;
	
	$url = $item->getURL();
	$subitems = $item->getSubitems();
	
	$liClass = $item->getClass();
	if( $item->getActive() or $item->hasActiveSubitems() ) {
		$liClass .= " active";
	}
	
	$linkClass = $subitems ? 'dropdown-toggle' : '';
?>
<?if( $level == 1 && !strlen( $url )){?>
	<li class="nav-header"><?=$item->getLabel()?></li>
	<?if( $subitems ){?>
		<?foreach( $subitems as $subitem ){?>
			<?$this->render( "_item", Array( 'item' => $subitem, 'level' => $level + 1 ))?>
		<?}?>
	<?}?>
<?}else{?>
	<li class="<?=$liClass?>">
		<a href="<?=$url?>" class="<?=$linkClass?>">
			<?=$item->getLabel()?>
			<?if( $subitems ){?>
				<b class="arrow icon-angle-down"></b>
			<?}?>
		</a>
		<?if( $subitems ){?>
			<ul class="submenu">
				<?foreach( $subitems as $subitem ){?>
					<?$this->render( "_item", Array( 'item' => $subitem, 'level' => $level + 1 ))?>
				<?}?>
			</ul>
		<?}?>
	</li>
<?}?>