<?php 
	Yii::app()->clientScript->registerCssFile( Yii::app()->params['wpThemeUrl'] .'/informer.css');
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl . "/js/jquery.js" );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/autobahn.min.js" );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wQuotesInformerWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<style>
.ticker_quotes_table th, .ticker_quotes_table tr:last-child td, .ticker_quotes_table th a{
	color: <?=$colors['thTextColor']?>;
}
.ticker_quotes_table tr:nth-child(1), .ticker_quotes_table tr:nth-child(2), .ticker_quotes_table tr:last-child{
	background-color: rgba( <?=CommonLib::hex2rgb($colors['thBackgroundColor'])?>, <?=$colors['thBackgroundOpacity'] ? '0.5' : '1'?> ) !important;
}
.ticker_quotes_table tr:nth-child(2n+4){
	background-color: <?=$colors['evenBackgroundTrColor']?>;
}
.ticker_quotes_table tr:nth-child(2n+3){
	background-color: <?=$colors['oddBackgroundTrColor']?>;
}
.ticker_quotes_table td a{
	color: <?=$colors['symbolTextColor']?>;
}
.ticker_quotes_table td{
	color: <?=$colors['tableTextColor']?>;
	border: 1px solid <?=$colors['borderTdColor']?>;
}
.ticker_quotes_table th{
	border: 1px solid <?=$colors['borderTdColor']?>;
}
.ticker_quotes_table{
	border: 1px solid <?=$colors['tableBorderColor']?>;
}
.ticker_quotes_table td.profitColor{
	color: <?=$colors['profitTextColor']?>;
}
.ticker_quotes_table td.lesionColor{
	color: <?=$colors['lossTextColor']?>;
}
.profit_bg{
	background-color: rgba( <?=CommonLib::hex2rgb($colors['profitBackgroundColor'])?>, <?=$colors['profitBackgroundOpacity'] ? '0.5' : '1'?> );
}
.loss_bg{
	background-color: rgba( <?=CommonLib::hex2rgb($colors['lossBackgroundColor'])?>, <?=$colors['lossBackgroundOpacity'] ? '0.5' : '1'?> );
}
<?php

$m10 = ceil( 10 * $multiplier );
$m12 = ceil( 12 * $multiplier );
$m13 = ceil( 13 * $multiplier );
$m15 = ceil( 15 * $multiplier );
$m16 = ceil( 16 * $multiplier );
$m18 = ceil( 18 * $multiplier );
$m19 = ceil( 19 * $multiplier );
$m20 = ceil( 20 * $multiplier );
$m24 = ceil( 24 * $multiplier);
$m25 = ceil( 25 * $multiplier );
$m30 = ceil( 30 * $multiplier );
$m45 = ceil( 45 * $multiplier );
$m57 = ceil( 57 * $multiplier );
$m210 = ceil( 210 * $multiplier );

$mWH2 = ceil( ($m13 + $m13 + $m19) /2 );

?>
.ticker_quotes_table th{
	font: <?=$m18?>px/<?=$m25?>px 'pf_dindisplay_promedium', sans-serif;
	padding: <?=$m15?>px <?=$m10?>px;
}
.ticker_quotes_table th.ticker_quotes_type{
	width: <?=$m210?>px;
	padding: <?=$m15?>px <?=$m10?>px <?=$m15?>px <?=$m24?>px;
}
.ticker_quotes_table td.ticker_quotes_type{
	padding-left: <?=$m24?>px;
}
.ticker_quotes_table td{
	padding: <?=$m15?>px <?=$m10?>px;
	font: <?=$m18?>px/<?=$m25?>px 'pf_dindisplay_proregular',sans-serif;
}
.ticker_quotes_table td.ticker_quotes_change{
	font: <?=$m18?>px/<?=$m25?>px 'pf_dindisplay_promedium', sans-serif;
}


.ticker_quotes_table:not(:last-child){
	margin-bottom: <?=$m10?>px;
}
.link_type1{
	font-size: <?=$m16?>px;
	line-height: <?=$m19?>px;
	padding: <?=$m13?>px <?=$m57?>px <?=$m13?>px <?=$m12?>px;
	color: <?=$colors['thTextColor']?>;
	background-color: rgba( <?=CommonLib::hex2rgb($colors['thBackgroundColor'])?>, <?=$colors['thBackgroundOpacity'] ? '0.5' : '1'?> ) !important;
}

.link_type1_icon{
	width: <?=$m45?>px;
}

.link_type1_icon:before{
	font:normal normal normal <?=$m20?>px/44px FontAwesome;
	width: <?=$mWH2*2?>px;
	height: <?=$mWH2*2?>px;
	margin-top: -22px;
	margin-left: -<?=($mWH2+1)?>px;
}
</style>

<div class="wQuotesInformerWidget <?=$ins?>">
<?php
	echo CHtml::openTag('table', array( 'class' => 'ticker_quotes_table' ));
	
		echo CHtml::openTag('tr');
			echo CHtml::tag(
				'th', 
				array( 'colspan' => count($columns)+1 ), 
				CHtml::link(
					$cat, 
					$catLink,
					array( 'target' => '_blank' )
				)
			);
		echo CHtml::closeTag('tr');
		
		echo CHtml::openTag('tr');
			echo CHtml::tag('th', array( 'class' => 'ticker_quotes_type' ), Yii::t( $NSi18n, 'Symbol' ));
			foreach( $columns as $column ){
				echo CHtml::tag('th', array( ), Yii::t( $NSi18n, $column ));
			}
		echo CHtml::closeTag('tr');
	
	$quotesKey = array();
	foreach( $symbols as $id => $symbol ){
		
		echo CHtml::openTag('tr', array( 'class' => $symbol['profit'], 'data-precision' => $symbol['precision'], 'data-symbol' => $symbol['name'], 'data-last-bid' => $symbol['lastBid'], 'data-old-bid' => $symbol['oldBid'], 'data-prev-chg-val' => $symbol['prevChgVal'] ) );
			echo CHtml::openTag('td', array( 'class' => 'ticker_quotes_type' ));
				echo CHtml::openTag('div', array( 'class' => 'ticker_quotes_type_inner' ));
					echo CHtml::link(
						$symbol['nalias'] ? $symbol['nalias'] : $symbol['name'], 
						Yii::App()->controller->createAbsoluteUrl( '/quotesNew/single', array('slug' => strtolower($symbol['name']) ) ),
						array( 'target' => '_blank', 'title' => $symbol['tooltip'] )
					);
				echo CHtml::closeTag('div');
			echo CHtml::closeTag('td');
			
			foreach( $columns as $column ){
				$htmlOptions = array( 'class' => $column . ' pid-' . $symbol['name'] . '-' . $column);
				if( $column == 'ChgPer' || $column == 'Chg' ){
					if( $symbol['profit'] == 'profit' ){
						$htmlOptions['class'] .= ' profitColor';
					}elseif( $symbol['profit'] == 'lesion' ){
						$htmlOptions['class'] .= ' lesionColor';
					}
				}
				echo CHtml::tag('td', $htmlOptions, $symbol[$column]);
			}
			
		echo CHtml::closeTag('tr');
		$quotesKey[] = $symbol['name'];
	}
		echo CHtml::openTag('tr');
			echo CHtml::tag('td', array( 'colspan' => count($columns)+1, 'class' => 'timeTr' ), Yii::t( $NSi18n, 'Quotes data for' ).' ' . CHtml::tag('span', array(), date('d.m.Y H:i', time() ) ) . ' '.Yii::t( $NSi18n, 'Quotes msk' ) );
		echo CHtml::closeTag('tr');
		
	echo CHtml::closeTag('table');
	
	if( $showGetBtn ){
		echo CHtml::openTag('div', array( 'class' => 'clearfix' ) );
			echo CHtml::link(
				Yii::t( $NSi18n, 'Get informer for your site' ) . '<i class="link_type1_icon"></i>' , 
				Yii::App()->controller->createAbsoluteUrl( '/quotesNew/informer' ),
				array( 'target' => '_blank', 'class' => 'link_type1 settings alignright' )
			);
		echo CHtml::closeTag('div');
	}
	
	$quotesSettings = QuotesSettingsModel::getInstance();
?>

</div>

<script>


	!function( $ ) {
		var ns = ".<?=$ns?>";
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var connUrl = '<?=$quotesSettings->wsServerUrl?>';
		var quotesKey = '<?=implode(',', $quotesKey)?>';

		ns.wQuotesInformerWidget = wQuotesInformerWidgetOpen({
			ns: ns,
			ins: ins,
			connUrl: connUrl,
			quotesKey: quotesKey,
			timeDataOffset: <?=$quotesSettings->realQuotesDataTimeDifference?>,
			selector: nsText+' .wQuotesInformerWidget',
			columns: <?=json_encode($columns)?>,
			noReal: <?=$noReal?>

		});
	}( window.jQuery );
</script>