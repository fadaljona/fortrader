<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

$cs = Yii::App()->clientScript;
$cs->registerScriptFile('https://code.jquery.com/jquery-3.1.1.min.js');
$cs->registerScriptFile('https://code.highcharts.com/highcharts.js');
$cs->registerScriptFile('https://code.highcharts.com/modules/exporting.js');
$cs->registerScriptFile(
    CHtml::asset(
        Yii::getPathOfAlias('webroot.js.widgets').'/wCryptoCurrenciesChartWidget.js'
    )
);
?>

<div class="crypto__graph <?= $ins ?>">
    <div class="crypto-graph__controllers clearfix">
        <a href="#" class="contrtoller__item" data-type="1d"><?= Yii::t($NSi18n, '1 day zoom') ?></a>
        <a href="#" class="contrtoller__item" data-type="1m"><?= Yii::t($NSi18n, '1 month zoom') ?></a>
        <a href="#" class="contrtoller__item" data-type="3m"><?= Yii::t($NSi18n, '3 month zoom') ?></a>
        <a href="#" class="contrtoller__item contrtoller__item--active" data-type="1y"><?= Yii::t($NSi18n, '1 year zoom') ?></a>
        <a href="#" class="contrtoller__item" data-type="5y"><?= Yii::t($NSi18n, '5 year zoom') ?></a>
        <a href="#" class="contrtoller__item" data-type="my"><?= Yii::t($NSi18n, 'max year zoom') ?></a>
    </div>
    <div class="crypto-graph__output" id="crypto-graph__output"></div>
</div>
<script>
var cryptoCurrenciesChartHighcharts;
!function( $ ) {
    var ns = ".<?=$ns?>";
    cryptoCurrenciesChartHighcharts = ns.wCryptoCurrenciesChartWidget = wCryptoCurrenciesChartWidgetOpen({
        selector: '.<?= $ins ?>',
        loadDataUrl: '<?= Yii::app()->createUrl('cryptoCurrencies/ajaxLoadChartData') ?>',
        chartContainer: 'crypto-graph__output',
        langOptions: {
            loading: '<?= Yii::t($NSi18n, 'Loading...') ?>',
            months: [
                '<?= Yii::t($NSi18n, 'January') ?>',
                '<?= Yii::t($NSi18n, 'February') ?>',
                '<?= Yii::t($NSi18n, 'March') ?>',
                '<?= Yii::t($NSi18n, 'April') ?>',
                '<?= Yii::t($NSi18n, 'May') ?>',
                '<?= Yii::t($NSi18n, 'June') ?>',
                '<?= Yii::t($NSi18n, 'July') ?>',
                '<?= Yii::t($NSi18n, 'August') ?>',
                '<?= Yii::t($NSi18n, 'September') ?>',
                '<?= Yii::t($NSi18n, 'October') ?>',
                '<?= Yii::t($NSi18n, 'November') ?>',
                '<?= Yii::t($NSi18n, 'December') ?>'
            ],
            weekdays: [
                '<?= Yii::t($NSi18n, 'Sunday') ?>',
                '<?= Yii::t($NSi18n, 'Monday') ?>',
                '<?= Yii::t($NSi18n, 'Tuesday') ?>',
                '<?= Yii::t($NSi18n, 'Wednesday') ?>',
                '<?= Yii::t($NSi18n, 'Thursday') ?>',
                '<?= Yii::t($NSi18n, 'Friday') ?>',
                '<?= Yii::t($NSi18n, 'Saturday') ?>'
            ],
            shortMonths: [
                '<?= Yii::t($NSi18n, 'Jan') ?>',
                '<?= Yii::t($NSi18n, 'Feb') ?>',
                '<?= Yii::t($NSi18n, 'Mar') ?>',
                '<?= Yii::t($NSi18n, 'Apr') ?>',
                '<?= Yii::t($NSi18n, 'May') ?>',
                '<?= Yii::t($NSi18n, 'Jun') ?>',
                '<?= Yii::t($NSi18n, 'Jul') ?>',
                '<?= Yii::t($NSi18n, 'Aug') ?>',
                '<?= Yii::t($NSi18n, 'Sep') ?>',
                '<?= Yii::t($NSi18n, 'Oct') ?>',
                '<?= Yii::t($NSi18n, 'Nov') ?>',
                '<?= Yii::t($NSi18n, 'Dec') ?>',
            ],
        }
    });
}( window.jQuery );
</script>