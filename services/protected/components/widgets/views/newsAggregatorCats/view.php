<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<!-- link_news_box -->
<div class="link_news_container <?=$ins?>">
	<div class="hide link_news_btns"><?=Yii::t( $NSi18n, 'All news' )?></div>
	<ul class="link_news_box clearfix">
		<?php
			$activeClass = '';
			if( $currentCat == 0 ) $activeClass = 'active';
			echo CHtml::tag(
				'li', 
				array('class' => 'link_news_box_item'), 
				CHtml::link( 
					Yii::t( $NSi18n, 'All news' ), 
					NewsAggregatorCatsModel::getAllNewsUrl(), 
					array('class' => 'link_news_box_a ' . $activeClass) 
				)
			);
			$activeClass = '';
			foreach( $cats as $cat ){
				if( $currentCat == $cat->id ) $activeClass = 'active';
				echo CHtml::tag(
					'li', 
					array('class' => 'link_news_box_item'), 
					CHtml::link( 
						$cat->title, 
						$cat->singleUrl, 
						array('class' => 'link_news_box_a ' . $activeClass ) 
					)
				);
				$activeClass = '';
			}
		?>
	</ul>
</div>
<!--/ link_news_box -->