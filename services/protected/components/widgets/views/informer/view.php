<?php 
	Yii::app()->clientScript->registerCssFile(  Yii::app()->params['wpThemeUrl'] .'/informers.css');
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
?>

<div class="<?=$ins?>">


<?/*
<!-- informer_box -->
<div class="informer_box informer_box_example clearfix">

стиль 1

<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#fff';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#fff';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e8e8e8';
	$tableBorderColor = '#e8e8e8';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$dataTextColor = '#787878';
	$dataBackgroundColor = '#fff';
	
	$informerLinkTextColor = '#5e5e5e';
	$informerLinkBackgroundColor = '#f1f1f1';
?>	

<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:before{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table tbody .col-data time{
	color: <?=$dataTextColor?>;
}
.informer_table tbody .col-data{
	background-color: <?=$dataBackgroundColor?> !important;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}


</style>
	
	<table class="informer_table informer1">
		<thead>
			<th colspan="2">Курсы валют к рублю</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th class="bd_l">Инструмент</th>
				<th class="bd_r">Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="icon_amount arrow">Евро</td>
				<td class="changeVal bg">70.585</td>
			</tr>
			<tr class="col3 profit">
				<td class="icon_amount arrow">Доллар</td>
				<td class="changeVal bg">63.8475</td>
			</tr>
			<tr class="col3 profit">
				<td class="icon_amount arrow">Доллар2</td>
				<td class="changeVal">63.8475</td>
			</tr>
			<tr class="col-data">
				<td colspan="2">
					<time datetime="2016-07-10T14:18">Данные на 10.07.2016 14:18 мск</time>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>










<?php /*
стиль 2
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#f6f6f6';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#f6f6f6';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e8e8e8';
	$tableBorderColor = '#f6f6f6';
	$oddBackgroundTrColor = '#f6f6f6';
	$evenBackgroundTrColor = '#f6f6f6';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$dataTextColor = '#787878';
	$dataBackgroundColor = '#f6f6f6';
	
	$informerLinkTextColor = '#5e5e5e';
	$informerLinkBackgroundColor = '#fff';
?>	

<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:before{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table tbody .col-data time{
	color: <?=$dataTextColor?>;
}
.informer_table tbody .col-data{
	background-color: <?=$dataBackgroundColor?> !important;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}

</style>
	
	<table class="informer_table informer1">
		<thead>
			<th colspan="2">Курсы валют к рублю</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th class="bd_l">Инструмент</th>
				<th class="bd_r">Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="icon_amount arrow">Евро</td>
				<td class="changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="icon_amount arrow">Доллар</td>
				<td class="changeVal bg">63.8475</td>
			</tr>
			<tr class="colLast profit">
				<td class="icon_amount arrow">Доллар2</td>
				<td class="changeVal">63.8475</td>
			</tr>
			<tr class="col-data">
				<td colspan="2">
					<time datetime="2016-07-10T14:18">Данные на 10.07.2016 14:18 мск</time>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
	
*/?>








<?php /*
стиль 3
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#b3b1b1';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#f1f1f1';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e8e8e8';
	$tableBorderColor = '#b3b1b1';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$dataTextColor = '#787878';
	$dataBackgroundColor = '#f1f1f1';
	
	$informerLinkTextColor = '#fff';
	$informerLinkBackgroundColor = '#b3b1b1';
?>	

<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:before{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table tbody .col-data time{
	color: <?=$dataTextColor?>;
}
.informer_table tbody .col-data{
	background-color: <?=$dataBackgroundColor?> !important;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}

</style>
	
	<table class="informer_table informer1">
		<thead>
			<th colspan="2">Курсы валют к рублю</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th class="bd_l">Инструмент</th>
				<th class="bd_r">Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="icon_amount arrow">Евро</td>
				<td class="changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="icon_amount arrow">Доллар</td>
				<td class="changeVal bg">63.8475</td>
			</tr>
			<tr class="colLast profit">
				<td class="icon_amount arrow">Доллар2</td>
				<td class="changeVal">63.8475</td>
			</tr>
			<tr class="col-data">
				<td colspan="2">
					<time datetime="2016-07-10T14:18">Данные на 10.07.2016 14:18 мск</time>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>

*/?>



</div>


<!-- informer_box -->
<div class="informer_box informer_box_example clearfix">

		
		
<? /*
стиль 4
<?php
	$titleTextColor = '#e6e6e6';
	$titleBackgroundColor = '#4d4d4d';
	
	$thTextColor = '#e6e6e6';
	$thBackgroundColor = '#333';
	$symbolTextColor = '#e6e6e6';
	$tableTextColor = '#e6e6e6';
	$borderTdColor = '#4d4d4d';
	$tableBorderColor = '#333';
	$oddBackgroundTrColor = '#333';
	$evenBackgroundTrColor = '#333';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$dataTextColor = '#e6e6e6';
	$dataBackgroundColor = '#333';
	
	$informerLinkTextColor = '#e6e6e6';
	$informerLinkBackgroundColor = '#4d4d4d';
?>	

<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:before{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table tbody .col-data time{
	color: <?=$dataTextColor?>;
}
.informer_table tbody .col-data{
	background-color: <?=$dataBackgroundColor?> !important;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}

</style>
	
	<table class="informer_table informer1">
		<thead>
			<th colspan="2">Курсы валют к рублю</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th class="bd_l">Инструмент</th>
				<th class="bd_r">Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="icon_amount arrow">Евро</td>
				<td class="changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="icon_amount arrow">Доллар</td>
				<td class="changeVal bg">63.8475</td>
			</tr>
			<tr class="colLast profit">
				<td class="icon_amount arrow">Доллар2</td>
				<td class="changeVal">63.8475</td>
			</tr>
			<tr class="col-data">
				<td colspan="2">
					<time datetime="2016-07-10T14:18">Данные на 10.07.2016 14:18 мск</time>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>






<?/*
стиль 5
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#136bad';
	
	$thTextColor = '#fff';
	$thBackgroundColor = '#167ac6';
	$symbolTextColor = '#fff';
	$tableTextColor = '#fff';
	$borderTdColor = '#136bad';
	$tableBorderColor = '#167ac6';
	$oddBackgroundTrColor = '#167ac6';
	$evenBackgroundTrColor = '#167ac6';
	$profitTextColor = '#fff';
	$profitBackgroundColor = '#167ac6';
	$lossTextColor = '#fff';
	$lossBackgroundColor = '#167ac6';
	
	$dataTextColor = '#fff';
	$dataBackgroundColor = '#167ac6';
	
	$informerLinkTextColor = '#fff';
	$informerLinkBackgroundColor = '#136bad';
?>	

<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:before{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table tbody .col-data time{
	color: <?=$dataTextColor?>;
}
.informer_table tbody .col-data{
	background-color: <?=$dataBackgroundColor?> !important;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}

</style>
	
	<table class="informer_table informer1">
		<thead>
			<th colspan="2">Курсы валют к рублю</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th class="bd_l">Инструмент</th>
				<th class="bd_r">Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="icon_amount arrow">Евро</td>
				<td class="changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="icon_amount arrow">Доллар</td>
				<td class="changeVal bg">63.8475</td>
			</tr>
			<tr class="colLast profit">
				<td class="icon_amount arrow">Доллар2</td>
				<td class="changeVal">63.8475</td>
			</tr>
			<tr class="col-data">
				<td colspan="2">
					<time datetime="2016-07-10T14:18">Данные на 10.07.2016 14:18 мск</time>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
	
*/?>
	


<?/*
стиль 6
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#615f8c';
	
	$thTextColor = '#fff';
	$thBackgroundColor = '#7371a6';
	$symbolTextColor = '#fff';
	$tableTextColor = '#fff';
	$borderTdColor = '#615f8c';
	$tableBorderColor = '#7371a6';
	$oddBackgroundTrColor = '#7371a6';
	$evenBackgroundTrColor = '#7371a6';
	$profitTextColor = '#fff';
	$profitBackgroundColor = '#7371a6';
	$lossTextColor = '#fff';
	$lossBackgroundColor = '#7371a6';
	
	$dataTextColor = '#fff';
	$dataBackgroundColor = '#7371a6';
	
	$informerLinkTextColor = '#fff';
	$informerLinkBackgroundColor = '#615f8c';
?>	

<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:before{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table tbody .col-data time{
	color: <?=$dataTextColor?>;
}
.informer_table tbody .col-data{
	background-color: <?=$dataBackgroundColor?> !important;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}

</style>
	
	<table class="informer_table informer1">
		<thead>
			<th colspan="2">Курсы валют к рублю</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th class="bd_l">Инструмент</th>
				<th class="bd_r">Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="icon_amount arrow">Евро</td>
				<td class="changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="icon_amount arrow">Доллар</td>
				<td class="changeVal bg">63.8475</td>
			</tr>
			<tr class="colLast profit">
				<td class="icon_amount arrow">Доллар2</td>
				<td class="changeVal">63.8475</td>
			</tr>
			<tr class="col-data">
				<td colspan="2">
					<time datetime="2016-07-10T14:18">Данные на 10.07.2016 14:18 мск</time>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>

</div>



<!-- informer_box -->
<div class="informer_box informer_box_example clearfix">

	
	
<?/*
стиль 7
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#fff';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#fff';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e8e8e8';
	$tableBorderColor = '#e8e8e8';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$dataTextColor = '#787878';
	$dataBackgroundColor = '#fff';
	
	$informerLinkTextColor = '#5e5e5e';
	$informerLinkBackgroundColor = '#f1f1f1';
?>
<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:after{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:after{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table tbody .col-data time{
	color: <?=$dataTextColor?>;
}
.informer_table tbody .col-data{
	background-color: <?=$dataBackgroundColor?> !important;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}

</style>

	<table class="informer_table bold_bd">
		<thead>
			<th colspan="2">Курсы валют к рублю</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th>Инструмент</th>
				<th>Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="arrow amount">
					<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/flags/flag_eur.png" alt="">
					</span>
					EUR
				</td>
				<td class="changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="arrow amount">
					<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/flags/flag_usd.png" alt="">
					</span>
					USD
				</td>
				<td class="changeVal bg">63.8475</td>
			</tr>
			<tr class="col3 profit">
				<td class="arrow amount">
					<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/flags/flag_usd.png" alt="">
					</span>
					USD
				</td>
				<td class="changeVal">63.8475</td>
			</tr>
			<tr class="col-data">
				<td colspan="2">
					<time datetime="2016-07-10T14:18">Данные на 10.07.2016 14:18 мск</time>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>


<?/*
стиль 8
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#f1f1f1';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#fff';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e8e8e8';
	$tableBorderColor = '#e8e8e8';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$dataTextColor = '#787878';
	$dataBackgroundColor = '#fff';
	
	$informerLinkTextColor = '#5e5e5e';
	$informerLinkBackgroundColor = '#f1f1f1';
?>
<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:after{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:after{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table tbody .col-data time{
	color: <?=$dataTextColor?>;
}
.informer_table tbody .col-data{
	background-color: <?=$dataBackgroundColor?> !important;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.informer_table.bold_bd th, .informer_table.bold_bd td{
	border-width:1px;
}

</style>

	<table class="informer_table bold_bd">
		<thead>
			<th colspan="2">Курсы валют к рублю</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th>Инструмент</th>
				<th>Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="arrow amount">
					<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/flags/flag_eur.png" alt="">
					</span>
					EUR
				</td>
				<td class="changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="arrow amount">
					<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/flags/flag_usd.png" alt="">
					</span>
					USD
				</td>
				<td class="changeVal bg">63.8475</td>
			</tr>
			<tr class="col3 profit">
				<td class="arrow amount">
					<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/flags/flag_usd.png" alt="">
					</span>
					USD
				</td>
				<td class="changeVal">63.8475</td>
			</tr>
			<tr class="col-data">
				<td colspan="2">
					<time datetime="2016-07-10T14:18">Данные на 10.07.2016 14:18 мск</time>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>

<?/*
стиль 9
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#f1f1f1';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#fff';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e8e8e8';
	$tableBorderColor = '#e8e8e8';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#5e5e5e';
	$informerLinkBackgroundColor = '#f1f1f1';
?>
<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:after{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:after{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.informer_table.bold_bd th, .informer_table.bold_bd td{
	border-width:1px;
}
.informer_table thead time{
	font-weight:400;
	color: <?=$titleTextColor?>;
}


</style>
	<table class="informer_table bold_bd">
		<thead>
			<th colspan="2">
				Курсы валют на
				<time datetime="2016-07-10T14:18">10.07.2016 14:18 мск</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th>Инструмент</th>
				<th>Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="arrow amount">
					<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/flags/flag_eur.png" alt="">
					</span>
					EUR
				</td>
				<td class="changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="arrow amount">
					<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/flags/flag_usd.png" alt="">
					</span>
					USD
				</td>
				<td class="changeVal">63.8475</td>
			</tr>
			<tr class="col3 profit">
				<td class="arrow amount">
					<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/flags/flag_usd.png" alt="">
					</span>
					USD
				</td>
				<td class="changeVal bg">63.8475</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>

</div>


<!-- informer_box -->
<div class="informer_box informer_box_example clearfix">


<?/*
стиль 10
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#fff';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#fff';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e3e3e3';
	$tableBorderColor = '#e3e3e3';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#454545';
	$informerLinkBackgroundColor = '#fff';
?>
<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:2px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:before{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.informer_table thead time{
	font-weight:400;
	color: <?=$titleTextColor?>;
}
.informer_table th, .informer_table td{
	border-width:2px;
}
.informer_table td, .informer_table th{
	border:2px solid <?=$borderTdColor?>;
}
</style>
	<table class="informer_table white  wwwhite">
		<thead>
			<th colspan="2">
				Курсы валют на
				<time datetime="2016-07-10T14:18">10.07.2016 14:18 мск</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th>Инструмент</th>
				<th>Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="amount_big">
					<span style=" width: 48px; height: 36px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/table_flag1.jpg" alt="">
					</span>
					USD
				</td>
				<td class="arrow changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="amount_big">
					<span style=" width: 48px; height: 36px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/table_flag1.jpg" alt="">
					</span>
					USD
				</td>
				<td class="arrow changeVal bg">70.585</td>
			</tr>
			<tr class="col3 profit">
				<td class="amount_big">
					<span style=" width: 48px; height: 36px; display: inline-block; vertical-align: middle; margin-right: 10px;">
						<img src="http://web.com/ft/images/table_flag2.jpg" alt="">
					</span>
					EUR
				</td>
				<td class="arrow changeVal">63.847</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>	
	


<?/*	
стиль 11
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#fff';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#fff';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e3e3e3';
	$tableBorderColor = '#e3e3e3';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#454545';
	$informerLinkBackgroundColor = '#fff';
	
	$itemImgBgColor = '#F1F1F1';
?>
<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:2px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:before{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.informer_table thead time{
	font-weight:400;
	color: <?=$titleTextColor?>;
}
.informer_table th, .informer_table td{
	border-width:2px;
}
.informer_table td, .informer_table th{
	border:2px solid <?=$borderTdColor?>;
}
.informer_table td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
</style>		
	<table class="informer_table white wwwhite">
		<thead>
			<th colspan="2">
				Курсы валют на
				<time datetime="2016-07-10T14:18">10.07.2016 14:18 мск</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th>Инструмент</th>
				<th>Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="amount">
					<span class="itemImgWrap">
						<img src="http://web.com/ft/images/informer/icon-eur-silver-square.png" alt="">
					</span>
					EUR
				</td>
				<td class="arrow changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="amount">
					<span class="itemImgWrap">
						<img src="http://web.com/ft/images/informer/icon-eur-silver-square.png" alt="">
					</span>
					EUR
				</td>
				<td class="arrow changeVal bg">70.585</td>
			</tr>
			<tr class="col3 profit">
				<td class="amount">
					<span class="itemImgWrap">
						<img src="http://web.com/ft/images/informer/icon-usd-silver-square.png" alt="">
					</span>
					USD
				</td>
				<td class="arrow changeVal">63.847</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>

		

<?/*
стиль 12
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#ff660a';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#fff';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e3e3e3';
	$tableBorderColor = '#ff660a';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#454545';
	$informerLinkBackgroundColor = '#fff';
	
	$itemImgBgColor='#F1F1F1';
	$itemImgTextColor='#545454';
?>
<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .arrow:before{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.informer_table thead time{
	font-weight:400;
	color: <?=$titleTextColor?>;
}
.informer_table th, .informer_table td{
	border-width:2px;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
</style>		
	<table class="informer_table white wwwhite">
		<thead>
			<th colspan="2">
				Курсы валют на
				<time datetime="2016-07-10T14:18">10.07.2016 14:18 мск</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th>Инструмент</th>
				<th>Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="amount">
					<span class="itemImgWrap">
						<img src="http://web.com/ft/images/informer/icon-eur-silver-square.png" alt="">
					</span>
					EUR
				</td>
				<td class="arrow changeVal bg">70.585</td>
			</tr>
			<tr class="col2 profit">
				<td class="amount">
					<span class="itemImgWrap">
						<img src="http://web.com/ft/images/informer/icon-eur-silver-square.png" alt="">
					</span>
					EUR
				</td>
				<td class="arrow changeVal bg">70.585</td>
			</tr>
			<tr class="col3 profit">
				<td class="amount">
					<span class="itemImgWrap">
						<img src="http://web.com/ft/images/informer/icon-usd-silver-square.png" alt="">
					</span>
					USD
				</td>
				<td class="arrow changeVal">63.847</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>


</div>



<!-- informer_box -->
<div class="informer_box informer_box_example clearfix">


		
		
<?/*
стиль 13	
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#fff';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#fff';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#e3e3e3';
	$tableBorderColor = '#e3e3e3';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#fff';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#fff';
	
	$informerLinkTextColor = '#454242';
	$informerLinkBackgroundColor = '#fff';

	$itemImgBgColor = '#F1F1F1';
?>

<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:2px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.informer_table thead time{
	font-weight:400;
	color: <?=$titleTextColor?>;
}
.informer_table th, .informer_table td{
	border-width:2px;
}
.informer_table td, .informer_table th{
	border:2px solid <?=$borderTdColor?>;
}
.informer_table td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
.informer_table .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
</style>	
		
	<table class="informer_table white tt_upr">
		<thead>
			<th colspan="2">Курсы валют – RUB</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th>Инструмент</th>
				<th>Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="amount_big">
					<span class="itemImgWrap">
						<img src="http://web.com/ft/images/informer/icon-usd-silver-square.png" alt="">
					</span>
					USD
				</td>
				<td>
					<span class="count_red">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
			</tr>
			<tr class="col2 profit">
				<td class="amount_big">
					<span class="itemImgWrap">
						<img src="http://web.com/ft/images/informer/icon-usd-silver-square.png" alt="">
					</span>
					USD
				</td>
				<td>
					<span class="count_red">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
			</tr>
	
			<tr class="col3 profit">
				<td class="amount_big">
					<span class="itemImgWrap">
						<img src="http://web.com/ft/images/informer/icon-eur-silver-square.png" alt="">
					</span>
					EUR
				</td>
				<td>
					<span class="count_green">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>



<?/*
стиль 14
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#fff';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#fff';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#ff1616';
	$tableBorderColor = '#ff1616';
	$oddBackgroundTrColor = '#fff';
	$evenBackgroundTrColor = '#fff';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#fff';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#fff';
	
	$informerLinkTextColor = '#454242';
	$informerLinkBackgroundColor = '#fff';

	$itemImgBgColor = '#F1F1F1';
?>

<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.informer_table thead time{
	font-weight:400;
	color: <?=$titleTextColor?>;
}
.informer_table th, .informer_table td{
	border-width:2px;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
.informer_table .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
</style>	
		
	<table class="informer_table white tt_upr">
		<thead>
			<th colspan="2">Курсы валют – RUB</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th>Инструмент</th>
				<th>Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="amount_big">
					<span class="itemImgWrap">
						ff
					</span>
					USD
				</td>
				<td>
					<span class="count_red">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
			</tr>
			<tr class="col2 profit">
				<td class="amount_big">
					<span class="itemImgWrap">
						ff
					</span>
					USD
				</td>
				<td>
					<span class="count_red">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
			</tr>
	
			<tr class="col3 profit">
				<td class="amount_big">
					<span class="itemImgWrap">
						gg
					</span>
					EUR
				</td>
				<td>
					<span class="count_green">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>

		
<?/*	
стиль 15
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#f1f1f1';
	
	$thTextColor = '#454545';
	$thBackgroundColor = '#f1f1f1';
	$symbolTextColor = '#454545';
	$tableTextColor = '#454545';
	$borderTdColor = '#ccc';
	$tableBorderColor = '#ccc';
	$oddBackgroundTrColor = '#f1f1f1';
	$evenBackgroundTrColor = '#f1f1f1';
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#f1f1f1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f1f1f1';
	
	$informerLinkTextColor = '#454242';
	$informerLinkBackgroundColor = '#f1f1f1';

	$itemImgBgColor = '#545454';
?>

<style>
.informer_table thead th{
	color: <?=$titleTextColor?>;
}
.informer_table thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table tbody th{
	color: <?=$thTextColor?>;
}
.informer_table tbody th{
	background-color: <?=$thBackgroundColor?>;
}
.informer_table tbody tr td{
	color: <?=$tableTextColor?>;
}
.informer_table tbody tr td:nth-child(1){
	color: <?=$symbolTextColor?>;
}
.informer_table tbody [class*="col"] th:not(:last-child),
.informer_table tbody [class*="col"] td:not(:last-child){
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table tbody [class*="col"] th:not(:first-child),
.informer_table tbody [class*="col"] td:not(:first-child){
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table thead th{
	border-top-color: <?=$tableBorderColor?>;;
	border-left-color: <?=$tableBorderColor?>;;
	border-right-color: <?=$tableBorderColor?>;;
}
.informer_table tbody .col-data td{
	border-left-color: <?=$tableBorderColor?>;
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table tfoot td{
	border:1px solid <?=$tableBorderColor?>;
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: <?=$oddBackgroundTrColor?>;
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: <?=$evenBackgroundTrColor?>;
}
.informer_table .loss .arrow:before{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.informer_table thead time{
	font-weight:400;
	color: <?=$titleTextColor?>;
}
.informer_table th, .informer_table td{
	border-width:2px;
}
.informer_table td, .informer_table th{
	border:1px solid <?=$borderTdColor?>;
}
.informer_table td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
.informer_table .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
</style>	
		
	<table class="informer_table white tt_upr">
		<thead>
			<th colspan="2">Курсы валют – RUB</th>
		</thead>
		<tbody>
			<tr class="col1">
				<th>Инструмент</th>
				<th>Предл.</th>
			</tr>
			<tr class="col2 loss">
				<td class="amount_big">
					<span class="itemImgWrap">
						ff
					</span>
					USD
				</td>
				<td>
					<span class="count_red">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
			</tr>
			<tr class="col2 profit">
				<td class="amount_big">
					<span class="itemImgWrap">
						ff
					</span>
					USD
				</td>
				<td>
					<span class="count_red">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
			</tr>
	
			<tr class="col3 profit">
				<td class="amount_big">
					<span class="itemImgWrap">
						gg
					</span>
					EUR
				</td>
				<td>
					<span class="count_green">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
	*/?>
	
	
</div>


<!-- informer_box -->
<div class="informer_box informer_box_example clearfix">

		
<?/*			
стиль 16
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#fff';
	
	$symbolTextColor = '#444';
	$itemImgBgColor = '#F1F1F1';
	
	$tableTextColor = '#444';
	$borderTdColor = '#e3e3e3';
	$tableBorderColor = '#e3e3e3';
	$trBackgroundColor = '#fff';
	
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#454242';
	$informerLinkBackgroundColor = '#fff';
?>
<style>
.informer_table_middle thead th{
	color: <?=$titleTextColor?>;
}
.informer_table_middle thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table_middle tbody tr td .amount_middle{
	color: <?=$tableTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	color: <?=$symbolTextColor?>;
}
.informer_table_middle.white.bold_bd td, .informer_table_middle.white.bold_bd th{
	border:2px solid <?=$borderTdColor?>;
	border-right-color: transparent;
	border-left-color: transparent;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:first-child, .informer_table_middle.white.bold_bd th:first-child{
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:last-child, .informer_table_middle.white.bold_bd th:last-child{
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table_middle tbody tr{
	background-color: <?=$trBackgroundColor?>;
}
.informer_table_middle .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table_middle .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table_middle .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table_middle .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_middle .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table_middle .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
.informer_table_middle .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table_middle.white tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
</style>

	<table class="informer_table_middle white tt_upr bold_bd">
		<thead>
			<th colspan="3">Курсы валют – RUB</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td class="loss">
					<span class="itemImgWrap">
						usd
					</span>
					<span class="amount_middle">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro/dfgf
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal bg">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>

		

		
<?/*
стиль 17		
<?php
	$titleTextColor = '#454545';
	$titleBackgroundColor = '#f1f1f1';
	
	$symbolTextColor = '#fff';
	$itemImgBgColor = '#545454';
	
	$tableTextColor = '#444';
	$borderTdColor = '#ccc';
	$tableBorderColor = '#ccc';
	$trBackgroundColor = '#f1f1f1';
	
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#454242';
	$informerLinkBackgroundColor = '#f1f1f1';
?>
<style>
.informer_table_middle thead th{
	color: <?=$titleTextColor?>;
}
.informer_table_middle thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table_middle tbody tr td .amount_middle{
	color: <?=$tableTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	color: <?=$symbolTextColor?>;
}
.informer_table_middle.white.bold_bd td, .informer_table_middle.white.bold_bd th{
	border:1px solid <?=$borderTdColor?>;
	border-right-color: transparent;
	border-left-color: transparent;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:first-child, .informer_table_middle.white.bold_bd th:first-child{
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:last-child, .informer_table_middle.white.bold_bd th:last-child{
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table_middle tbody tr{
	background-color: <?=$trBackgroundColor?>;
}
.informer_table_middle .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table_middle .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table_middle .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table_middle .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_middle .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table_middle .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
.informer_table_middle .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table_middle.white tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
</style>

	<table class="informer_table_middle white tt_upr bold_bd">
		<thead>
			<th colspan="3">Курсы валют – RUB</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td class="loss">
					<span class="itemImgWrap">
						usd
					</span>
					<span class="amount_middle">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro/dfgf
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal bg">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>	


		

		
		
		
		
		
<?/*	
стиль 18	
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#545454';
	
	$symbolTextColor = '#fff';
	$itemImgBgColor = '#545454';
	
	$tableTextColor = '#444';
	$borderTdColor = '#ccc';
	$tableBorderColor = '#ccc';
	$trBackgroundColor = '#f1f1f1';
	
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#454242';
	$informerLinkBackgroundColor = '#f1f1f1';
?>
<style>
.informer_table_middle thead th{
	color: <?=$titleTextColor?>;
}
.informer_table_middle thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table_middle tbody tr td .amount_middle{
	color: <?=$tableTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	color: <?=$symbolTextColor?>;
}
.informer_table_middle.white.bold_bd td, .informer_table_middle.white.bold_bd th{
	border:1px solid <?=$borderTdColor?>;
	border-right-color: transparent;
	border-left-color: transparent;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:first-child, .informer_table_middle.white.bold_bd th:first-child{
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:last-child, .informer_table_middle.white.bold_bd th:last-child{
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table_middle tbody tr{
	background-color: <?=$trBackgroundColor?>;
}
.informer_table_middle .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table_middle .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table_middle .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table_middle .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_middle .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table_middle .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
.informer_table_middle .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table_middle.white tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
</style>

	<table class="informer_table_middle white tt_upr bold_bd">
		<thead>
			<th colspan="3">Курсы валют – RUB</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td class="loss">
					<span class="itemImgWrap">
						usd
					</span>
					<span class="amount_middle">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro/dfgf
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal bg">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>		
*/?>		
		
		
		
		
		
<?/*
стиль 19
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#ff660a';
	
	$symbolTextColor = '#fff';
	$itemImgBgColor = '#ff660a';
	
	$tableTextColor = '#444';
	$borderTdColor = '#ccc';
	$tableBorderColor = '#ff660a';
	$trBackgroundColor = '#f1f1f1';
	
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#454242';
	$informerLinkBackgroundColor = '#f1f1f1';
?>
<style>
.informer_table_middle thead th{
	color: <?=$titleTextColor?>;
}
.informer_table_middle thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table_middle tbody tr td .amount_middle{
	color: <?=$tableTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	color: <?=$symbolTextColor?>;
}
.informer_table_middle.white.bold_bd td, .informer_table_middle.white.bold_bd th{
	border:1px solid <?=$borderTdColor?>;
	border-right-color: transparent;
	border-left-color: transparent;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:first-child, .informer_table_middle.white.bold_bd th:first-child{
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:last-child, .informer_table_middle.white.bold_bd th:last-child{
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table_middle tbody tr{
	background-color: <?=$trBackgroundColor?>;
}
.informer_table_middle .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table_middle .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table_middle .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table_middle .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_middle .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table_middle .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
}
.informer_table_middle .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table_middle.white tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
</style>

	<table class="informer_table_middle white tt_upr bold_bd">
		<thead>
			<th colspan="3">Курсы валют – RUB</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td class="loss">
					<span class="itemImgWrap">
						usd
					</span>
					<span class="amount_middle">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro/dfgf
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal bg">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>	
		
</div>


<!-- informer_box -->
<div class="informer_box informer_box_example clearfix">

		
		
<?/*	
стиль 20
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#897939';
	
	$symbolTextColor = '#fff';
	$itemImgBgColor = '#897939';
	
	$tableTextColor = '#444';
	$borderTdColor = '#897939';
	$tableBorderColor = '#897939';
	$trBackgroundColor = '#f1f1f1';
	
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#574d24';
	$informerLinkBackgroundColor = '#f1f1f1';
?>
<style>
.informer_table_middle thead th{
	color: <?=$titleTextColor?>;
}
.informer_table_middle time{
	color: <?=$titleTextColor?>;
}
.informer_table_middle thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table_middle tbody tr td .amount_middle{
	color: <?=$tableTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	color: <?=$symbolTextColor?>;
}
.informer_table_middle.white.bold_bd td, .informer_table_middle.white.bold_bd th{
	border:1px solid <?=$borderTdColor?>;
	border-right-color: transparent;
	border-left-color: transparent;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:first-child, .informer_table_middle.white.bold_bd th:first-child{
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:last-child, .informer_table_middle.white.bold_bd th:last-child{
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table_middle tbody tr{
	background-color: <?=$trBackgroundColor?>;
}
.informer_table_middle .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table_middle .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table_middle .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table_middle .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_middle .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table_middle .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
	border-radius:50%;
}
.informer_table_middle .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table_middle.white tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
</style>

	<table class="informer_table_middle white tt_upr bold_bd">
		<thead>
			<th colspan="3">Курсы валют – RUB <time datetime="2016-07-10">10.07.2016</time></th>
		</thead>
		<tbody>
			<tr class="col1">
				<td class="loss">
					<span class="itemImgWrap">
						usd
					</span>
					<span class="amount_middle">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro/dfgf
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal bg">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>

		
		

<?/*
стиль 21
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#ff7e65';
	
	$symbolTextColor = '#fff';
	$itemImgBgColor = '#ff7e65';
	
	$tableTextColor = '#444';
	$borderTdColor = '#ff7e65';
	$tableBorderColor = '#ff7e65';
	$trBackgroundColor = '#ffece5';
	
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#574d24';
	$informerLinkBackgroundColor = '#ffece5';
?>
<style>
.informer_table_middle thead th{
	color: <?=$titleTextColor?>;
}
.informer_table_middle time{
	color: <?=$titleTextColor?>;
}
.informer_table_middle thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table_middle tbody tr td .amount_middle{
	color: <?=$tableTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	color: <?=$symbolTextColor?>;
}
.informer_table_middle.white.bold_bd td, .informer_table_middle.white.bold_bd th{
	border:1px solid <?=$borderTdColor?>;
	border-right-color: transparent;
	border-left-color: transparent;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:first-child, .informer_table_middle.white.bold_bd th:first-child{
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:last-child, .informer_table_middle.white.bold_bd th:last-child{
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table_middle tbody tr{
	background-color: <?=$trBackgroundColor?>;
}
.informer_table_middle .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table_middle .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table_middle .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table_middle .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_middle .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table_middle .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
	border-radius:50%;
}
.informer_table_middle .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table_middle.white tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
</style>

	<table class="informer_table_middle white tt_upr bold_bd">
		<thead>
			<th colspan="3">Курсы валют – RUB <time datetime="2016-07-10">10.07.2016</time></th>
		</thead>
		<tbody>
			<tr class="col1">
				<td class="loss">
					<span class="itemImgWrap">
						usd
					</span>
					<span class="amount_middle">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro/dfgf
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal bg">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>		


	
<?/*	
стиль 22
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#115e99';
	
	$symbolTextColor = '#fff';
	$itemImgBgColor = '#115e99';
	
	$tableTextColor = '#fff';
	$borderTdColor = '#115e99';
	$tableBorderColor = '#115e99';
	$trBackgroundColor = '#167ac6';
	
	$profitTextColor = '#fff';
	$profitBackgroundColor = '#167ac6';
	$lossTextColor = '#fff';
	$lossBackgroundColor = '#167ac6';
	
	$informerLinkTextColor = '#fff';
	$informerLinkBackgroundColor = '#167ac6';
?>
<style>
.informer_table_middle thead th{
	color: <?=$titleTextColor?>;
}
.informer_table_middle time{
	color: <?=$titleTextColor?>;
}
.informer_table_middle thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table_middle tbody tr td .amount_middle{
	color: <?=$tableTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	color: <?=$symbolTextColor?>;
}
.informer_table_middle.white.bold_bd td, .informer_table_middle.white.bold_bd th{
	border:1px solid <?=$borderTdColor?>;
	border-right-color: transparent;
	border-left-color: transparent;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:first-child, .informer_table_middle.white.bold_bd th:first-child{
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:last-child, .informer_table_middle.white.bold_bd th:last-child{
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table_middle tbody tr{
	background-color: <?=$trBackgroundColor?>;
}
.informer_table_middle .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table_middle .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table_middle .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table_middle .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_middle .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table_middle .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
	border-radius:50%;
}
.informer_table_middle .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table_middle.white tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
</style>

	<table class="informer_table_middle white tt_upr bold_bd">
		<thead>
			<th colspan="3">Курсы валют – RUB <time datetime="2016-07-10">10.07.2016</time></th>
		</thead>
		<tbody>
			<tr class="col1">
				<td class="loss">
					<span class="itemImgWrap">
						usd
					</span>
					<span class="amount_middle">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro/dfgf
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal bg">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>
*/?>

		
стиль 23
<?php
	$titleTextColor = '#fff';
	$titleBackgroundColor = '#b3b1b1';
	
	$symbolTextColor = '#fff';
	$itemImgBgColor = '#b3b1b1';
	
	$tableTextColor = '#444';
	$borderTdColor = '#b3b1b1';
	$tableBorderColor = '#b3b1b1';
	$trBackgroundColor = '#f1f1f1';
	
	$profitTextColor = '#89bb50';
	$profitBackgroundColor = '#eaf7e1';
	$lossTextColor = '#ff1616';
	$lossBackgroundColor = '#f6e1e1';
	
	$informerLinkTextColor = '#454242';
	$informerLinkBackgroundColor = '#f1f1f1';
?>
<style>
.informer_table_middle thead th{
	color: <?=$titleTextColor?>;
}
.informer_table_middle time{
	color: <?=$titleTextColor?>;
}
.informer_table_middle thead{
	background-color: <?=$titleBackgroundColor?>;
}
.informer_table_middle tbody tr td .amount_middle{
	color: <?=$tableTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	color: <?=$symbolTextColor?>;
}
.informer_table_middle.white.bold_bd td, .informer_table_middle.white.bold_bd th{
	border:1px solid <?=$borderTdColor?>;
	border-right-color: transparent;
	border-left-color: transparent;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: transparent;
}
.informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:first-child, .informer_table_middle.white.bold_bd th:first-child{
	border-left-color: <?=$tableBorderColor?>;
}
.informer_table_middle.white.bold_bd td:last-child, .informer_table_middle.white.bold_bd th:last-child{
	border-right-color: <?=$tableBorderColor?>;
}
.informer_table_middle tbody tr{
	background-color: <?=$trBackgroundColor?>;
}
.informer_table_middle .loss .changeVal{
	color: <?=$lossTextColor?>;
}
.informer_table_middle .loss .changeVal.bg{
	background-color: <?=$lossBackgroundColor?>;
}
.informer_table_middle .profit .changeVal{
	color: <?=$profitTextColor?>;
}
.informer_table_middle .profit .changeVal.bg{
	background-color: <?=$profitBackgroundColor?>;
}
.informer_table_middle .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid <?=$lossTextColor?>;
}
.informer_table_middle .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid <?=$profitTextColor?>;
}
.informer_table_middle td .itemImgWrap{
	background-color: <?=$itemImgBgColor?>;
	color: <?=$symbolTextColor?>;
	border-radius:50%;
}
.informer_table_middle .instal_link{
	color: <?=$informerLinkTextColor?>;
}
.informer_table_middle.white tfoot td{
	background-color: <?=$informerLinkBackgroundColor?>;
}
</style>

	<table class="informer_table_middle white tt_upr bold_bd">
		<thead>
			<th colspan="3">Курсы валют – RUB <time datetime="2016-07-10">10.07.2016</time></th>
		</thead>
		<tbody>
			<tr class="col1">
				<td class="loss">
					<span class="itemImgWrap">
						usd
					</span>
					<span class="amount_middle">
						64.254
						<span class="angle_l_bottom changeVal bg">0.0064</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro/dfgf
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal">0.4060</span>
					</span>
				</td>
				<td class="profit">
					<span class="itemImgWrap">
						euro
					</span>
					<span class="amount_middle">
						70.567
						<span class="angle_l_bottom changeVal bg">0.4060</span>
					</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<a href="javascript:;" class="instal_link">Установить информер на свой сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>


</div>



<?/*
стиль 24
<!-- informer_box -->
<div class="informer_box informer_box_example clearfix">



	
	<table class="informer_table_small default">
		<thead>
			<th colspan="2">
				Курсы валют на <time datetime="2016-07-10">10.07.16</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td colspan="2">
					<span class="amount_middle">
						<span style="display: inline-block; vertical-align: middle; margin-right: 10px; width: 10px; height: 18px;">
							<img src="http://web.com/ft/images/informer/icon-usd.png" alt="">
						</span>
						USD 64.254
					</span>
					<span class="angle_l_bottom_red">0.0064</span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="amount_middle">
						<span style="display: inline-block; vertical-align: middle; margin-right: 10px; width: 14px; height: 16px;">
							<img src="http://web.com/ft/images/informer/icon-eur.png" alt="">
						</span>
						EUR 70.754
					</span>
					<span class="angle_l_top_green">0.4060</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>


	
стиль 25
	<table class="informer_table_small silver_dark">
		<thead>
			<th colspan="2">
				Курсы валют на <time datetime="2016-07-10">10.07.16</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td colspan="2">
					<span class="amount_middle">
						<span style="display: inline-block; vertical-align: middle; margin-right: 10px; width: 10px; height: 18px;">
							<img src="http://web.com/ft/images/informer/icon-usd.png" alt="">
						</span>
						USD 64.254
					</span>
					<span class="angle_l_bottom_red">0.0064</span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="amount_middle">
						<span style="display: inline-block; vertical-align: middle; margin-right: 10px; width: 14px; height: 16px;">
							<img src="http://web.com/ft/images/informer/icon-eur.png" alt="">
						</span>
						EUR 70.754
					</span>
					<span class="angle_l_top_green">0.4060</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>

	
	
	
стиль 26	
	<table class="informer_table_small orange">
		<thead>
			<th colspan="2">
				Курсы валют на <time datetime="2016-07-10">10.07.16</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td colspan="2">
					<span class="amount_middle">
						<span style="display: inline-block; vertical-align: middle; margin-right: 10px; width: 10px; height: 18px;">
							<img src="http://web.com/ft/images/informer/icon-usd.png" alt="">
						</span>
						USD 64.254
					</span>
					<span class="angle_l_bottom_red">0.0064</span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="amount_middle">
						<span style="display: inline-block; vertical-align: middle; margin-right: 10px; width: 14px; height: 16px;">
							<img src="http://web.com/ft/images/informer/icon-eur.png" alt="">
						</span>
						EUR 70.754
					</span>
					<span class="angle_l_top_green">0.4060</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>


	

стиль 27
	<table class="informer_table_small pink">
		<thead>
			<th colspan="2">
				Курсы валют на <time datetime="2016-07-10">10.07.16</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td colspan="2">
					<span class="amount_middle">
						<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
							<img src="http://web.com/ft/images/flags/flag_usd.png" alt="">
						</span>
						USD 64.254
					</span>
					<span class="angle_l_bottom_red">0.0064</span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="amount_middle">
						<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
							<img src="http://web.com/ft/images/flags/flag_eur.png" alt="">
						</span>
						EUR 70.754
					</span>
					<span class="angle_l_top_green">0.4060</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>


	
	
стиль 28	
	<table class="informer_table_small silver_dark">
		<thead>
			<th colspan="2">
				Курсы валют на <time datetime="2016-07-10">10.07.16</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td colspan="2">
					<span class="amount_middle">
						<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
							<img src="http://web.com/ft/images/flags/flag_usd.png" alt="">
						</span>
						USD 64.254
					</span>
					<span class="angle_l_bottom_red">0.0064</span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span class="amount_middle">
						<span style=" width: 16px; height: 13px; display: inline-block; vertical-align: middle; margin-right: 10px;">
							<img src="http://web.com/ft/images/flags/flag_eur.png" alt="">
						</span>
						EUR 70.754
					</span>
					<span class="angle_l_top_green">0.4060</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>


	
	
стиль 29	
	<table class="informer_table_small silver_dark2">
		<thead>
			<th colspan="2">
				Курсы USD на <time datetime="2016-07-10">10.07.16</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td colspan="2">
					<span>Покупка +0.00</span>
					<span class="amount_middle">64.254</span>
				</td>
			</tr>
			<tr class="col2">
				<td colspan="2">
					<span>Продажа +0.00</span>
					<span class="amount_middle">70.754</span>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>



	
	
стиль 30	
	<table class="informer_table_marquee blue">
		<thead>
			<th colspan="2">
				Курсы валют на <time datetime="2016-07-10T12:30">10.07.16 12:30 мск</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td colspan="2">
					<marquee behavior="scroll" direction="left">
						<span class="amount_middle blue">USD</span>
						<span>Покупка</span>
						<span class="amount_middle">64.84</span>
						<span>Продажа</span>
						<span class="amount_middle">65.78</span>
					</marquee>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>


	
	
	
	
	<table class="informer_table_marquee silver_dark">
		<thead>
			<th colspan="2">
				Курсы валют на <time datetime="2016-07-10T12:30">10.07.16 12:30 мск</time>
			</th>
		</thead>
		<tbody>
			<tr class="col1">
				<td colspan="2">
					<marquee behavior="scroll" direction="left">
						<span class="amount_middle">USD 64.84</span>
						<span class="angle_l_bottom_red">0.0064</span>
						<span class="amount_middle">EUR 70.54</span>
						<span class="angle_l_bottom_red">0.0064</span>
					</marquee>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
					<a href="javascript:;" class="instal_link">Установить информер на сайт</a>
				</td>
			</tr>
		</tfoot>
	</table>







</div>

*/?>
























</div>