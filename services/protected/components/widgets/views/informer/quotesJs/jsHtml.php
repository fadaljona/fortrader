<?php 
if( $category->type == 'quotes' && ( !isset($_GET['disableRealTime']) || !$_GET['disableRealTime'] ) ){ 
    echo file_get_contents('js/autobahn.min.js');
    $quotesSettings = QuotesSettingsModel::getInstance();
    $quotesKey = '';
	$i=0;
	foreach( $items as $item ){
		if( $i==0 ){
			$quotesKey = $item->name;
			$i=1;
		}else{
			$quotesKey  .= ','.$item->name;
		}
	}
?>
(function () {
    var self = {
        fieldsToUpdate: {},
        columnsToUpdate: {},
        availableColumns: {
            ask: 'ask', 
            bid: 'bid', 
            highQuote: 'high', 
            lowQuote: 'low', 
            chg: 'chg', 
            chgPer: 'chgPer', 
            time: 'time'
        },
        options:{
            columns: <?=json_encode($columns)?>,
            connUrl: '<?=$quotesSettings->wsServerUrl?>',
            quotesKey: '<?=$quotesKey?>',
            timeDataOffset: <?=$quotesSettings->realQuotesDataTimeDifference?>,
        },
        items:[
            <?php foreach( $items as $item ){ ?>
            {
                url: '<?=$item->absoluteSingleURL?>',
            },
            <?php } ?>
        ],
        init:function() {
            self.classSelector = 'FT<?=$hash?> FT<?=$marker?>';
            self.$ns = document.getElementsByClassName( self.classSelector )[0];

            if( !self.checkIfAllRight() ) return false;

            self.setUpColumns();

            self.$wQuotesTime = self.$ns.querySelectorAll( '[data-column="time"]' );

            self.listenDomLoaded();
            
            self.subscribeToQuotes();
        },
        checkIfAllRight: function(){
            var allRight = true;
            self.items.forEach(function(item) {
                if( self.$ns.querySelector('a[href="'+item.url+'"]') == null ){
                    allRight = false;
                }
            });
            return allRight;
        },
        subscribeToQuotes:function() {		
            var conn = new ab.Session(self.options.connUrl,
                function() {
                    conn.subscribe(self.options.quotesKey, function(topic, data) {
                        self.updateQuotes(data);
                    });
                    console.warn('WebSocket connection opened for key ' + self.options.quotesKey);
                },
                function() {
                    console.warn('WebSocket connection closed');
                },
                {'skipSubprotocolCheck': true}
            );
        },
        updateQuotes:function(data) {
            for(var key in data) {
                var parsedData = JSON.parse(data[key]);
                self.updateQuotesWithAnimate(key, parsedData);
            }
        },
        updateQuotesWithAnimate:function(key, parsedData) {
            
            var currentTr = self.$ns.querySelector('[data-symbol="'+key+'"]');	
            
            for ( columnKey in self.columnsToUpdate ) {
                self.fieldsToUpdate[ columnKey ] = currentTr.querySelector('[data-column="'+self.columnsToUpdate[columnKey]+'"]');
            }
            
            var oldBid = currentTr.getAttribute('data-oldBid');
            if( oldBid == undefined ){
                currentTr.setAttribute('data-oldBid', parsedData.bid);	
                oldBid = parsedData.bid;
            }
            
            var lastBid = currentTr.getAttribute('data-lastBid');
            if( lastBid == undefined ){
                currentTr.setAttribute('data-lastBid', parsedData.bid);	
                lastBid = parsedData.bid;
            }
            
    
            var lastBidVal = parseFloat(lastBid);
            var oldBidVal = parseFloat(oldBid);
            var newBidVal = parseFloat(parsedData.bid);
            
            if( lastBidVal != newBidVal && self.fieldsToUpdate.chg ){
    
                if( currentTr.getAttribute('data-prev-chg-val') == undefined ){
                    currentTr.setAttribute('data-prev-chg-val', parseFloat( self.fieldsToUpdate.chg.innerText ));
                }
                
                var prevChgVal = parseFloat( currentTr.getAttribute('data-prev-chg-val') );
                
                var newChgVal = newBidVal - lastBidVal;
                var newChgPercentVal = newChgVal / lastBidVal * 100 ;
                var sign = '';
                if( newChgVal > 0 ) sign = '+';
                
                if( self.fieldsToUpdate.chg ){
                    self.fieldsToUpdate.chg.innerText = sign + newChgVal.toFixed(4);
                    if( prevChgVal < 0 && newChgVal > 0 || prevChgVal > 0 && newChgVal < 0 ){
                        self.changeColorAndBg(self.fieldsToUpdate.chg, prevChgVal, newChgVal);
                    }
                }
                if( self.fieldsToUpdate.chgPer ){
                    self.fieldsToUpdate.chgPer.innerText = sign + newChgPercentVal.toFixed(4);
                    if( prevChgVal < 0 && newChgVal > 0 || prevChgVal > 0 && newChgVal < 0 ){
                        self.changeColorAndBg(self.fieldsToUpdate.chgPer, prevChgVal, newChgVal);
                    }
                }
                currentTr.setAttribute('data-prev-chg-val', newChgVal);
            }          
            
            for ( columnKey in self.fieldsToUpdate ) {
                if( columnKey != 'chg' && columnKey != 'chgPer' && columnKey != 'high' && columnKey != 'low' && columnKey != 'time' && columnKey != 'bid' ){
                    self.fieldsToUpdate[columnKey].innerText = parsedData[columnKey];
                }
                if( columnKey == 'time' ){
                    self.fieldsToUpdate[columnKey].innerText = self.getLocalTimeForData(parsedData[columnKey]);
                }
                if( columnKey == 'bid' ){
                    
                    if( self.fieldsToUpdate['bid'].querySelector('.value') ){
                        self.fieldsToUpdate['bid'].querySelector('.value').innerText = parsedData['bid'];
                        
                        if( newBidVal != oldBidVal ){
                            var diff = newBidVal - oldBidVal;
                            
                            if( self.fieldsToUpdate['bid'].querySelector('.changeVal') != null ){
                                self.fieldsToUpdate['bid'].querySelector('.changeVal').innerText = diff.toFixed(4);
                                self.changeColorAndBgBid( self.fieldsToUpdate['bid'].querySelector('.changeVal'), oldBidVal, newBidVal );
                            }
                            
                        }
                        
                    }else{
                        self.fieldsToUpdate['bid'].innerText = parsedData['bid'];
                        self.changeColorAndBgBid( self.fieldsToUpdate['bid'], oldBidVal, newBidVal );
                    }
                    
                }
                if( columnKey == 'high' ){
                    var oldHigh = self.fieldsToUpdate['high'].innerText;
                    if( !isNaN(oldHigh) ){
                        var oldHighVal = parseFloat( oldHigh );
                        if( newBidVal > oldHighVal ){
                            self.fieldsToUpdate['high'].innerText = newBidVal.toFixed(4);
                            self.changeColorAndBg( self.fieldsToUpdate['high'], oldHighVal, newBidVal );
                        }
                    }else{
                        self.fieldsToUpdate['high'].innerText = parsedData['bid'];
                    }
                }
                if( columnKey == 'low' ){
                    var oldLow = self.fieldsToUpdate['low'].innerText;
                    if( !isNaN(oldLow) ){
                        var oldLowVal = parseFloat( oldLow );
                        if( newBidVal < oldLowVal ){
                            self.fieldsToUpdate['low'].innerText = newBidVal.toFixed(4);
                            self.changeColorAndBg( self.fieldsToUpdate['low'], oldLowVal, newBidVal );
                        }
                    }else{
                        self.fieldsToUpdate['low'].innerText = parsedData['bid'];
                    }
                    
                }
            }
            
            if( !self.fieldsToUpdate.bid ){
                if( oldBidVal < newBidVal ){
                    currentTr.classList.remove('loss');
                    currentTr.classList.add('profit');
                }
                if( oldBidVal > newBidVal ){
                    currentTr.classList.remove('profit')
                    currentTr.classList.add('loss');
                }
            }

            currentTr.setAttribute('data-oldBid', parsedData.bid);	
            
            setTimeout(function() {
                if ( newBidVal > oldBidVal || newBidVal < oldBidVal ) {
                    if( currentTr.querySelector('.changeVal') != null ) currentTr.querySelector('.changeVal').classList.remove('bg');
                }
                
            }, 1250);
        
        },
        changeColorAndBg: function(el, prevVal, newVal){
            if( prevVal < newVal ){
                el.classList.remove('lossTextColor');
                el.classLis.add('profitTextColor');
                el.classLis.add('bg');
            }else{
                el.classList.remove('profitTextColor');
                el.classLis.add('lossTextColor');
                el.classLis.add('bg');
            }
        },
        changeColorAndBgBid:function(element, oldBidVal, newBidVal) {
            if( oldBidVal < newBidVal ){
                var tr = element.closest('.trClass')
                tr.classList.remove('loss');
                tr.classList.add('profit');
                element.classList.add('bg');
            }
            if( oldBidVal > newBidVal ){
                var tr = element.closest('.trClass')
                tr.classList.remove('profit')
                tr.classList.add('loss');
                element.classList.add('bg');
            }
        },
        domLoaded: function(){
            self.updateTimeToLocal();
        },
        listenDomLoaded: function(){
            document.readyState !== "complete" ? setTimeout(self.listenDomLoaded, 11) : self.domLoaded();
        },
        updateTimeToLocal:function() {
            var timeRegex = new RegExp('^[0-9]{2}\:[0-9]{2}\:[0-9]{2}');
            self.$wQuotesTime.forEach(function(item) {

                var startTime = item.innerText;
                if( startTime != '' && timeRegex.test( startTime ) ){ 
                    item.innerText = self.getLocalTimeForData( startTime );
                }
            });
        },
        getLocalTimeForData:function(timeStr) {
            var timeArr = timeStr.split(":");
                
            var $date = new Date(2015, 1, 1, timeArr[0], timeArr[1], timeArr[2], 0); //new Date(year, month, day, hours, minutes, seconds, milliseconds)
            var timezoneOffset = $date.getTimezoneOffset() + 60 * self.options.timeDataOffset;
            $date.setMinutes ( $date.getMinutes() + timezoneOffset * (-1) );
            var hours, minutes, seconds;
            
            if( $date.getHours() < 10 ){
                hours = '0' + $date.getHours();
            }else { hours = $date.getHours(); }
            
            if( $date.getMinutes() < 10 ){
                minutes = '0' + $date.getMinutes();
            }else{ minutes = $date.getMinutes(); }
            
            if( $date.getSeconds() < 10 ){
                seconds = '0' + $date.getSeconds();
            }else{ seconds = $date.getSeconds(); }
            
            return hours + ':' + minutes + ':' + seconds;
        },
        setUpColumns:function() {
            for (var i = 0; i < self.options.columns.length; i++) {
                if( self.availableColumns[self.options.columns[i]] ) self.columnsToUpdate[ self.availableColumns[self.options.columns[i]] ] = self.options.columns[i];
            }
        },
        
    };

    self.init();
})();
<?php }?>