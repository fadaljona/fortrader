<?php
if( $category->type == 'quotes' && ( !isset($_GET['disableRealTime']) || !$_GET['disableRealTime'] ) ){
Yii::App()->clientScript->registerScriptFile( Yii::App()->request->getHostInfo() . Yii::app()->baseUrl . "/js/jquery.js" );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/autobahn.min.js" );
	Yii::App()->clientScript->registerScriptFile( Yii::App()->request->getHostInfo() . Yii::app()->baseUrl."/js/widgets/wNewQuotesInformerWidget.js" );
	$quotesSettings = QuotesSettingsModel::getInstance();
	
	$quotesKey = '';
	$i=0;
	foreach( $items as $item ){
		if( $i==0 ){
			$quotesKey = $item->name;
			$i=1;
		}else{
			$quotesKey  .= ','.$item->name;
		}
	}
?>
<script>
	!function( $ ) {
		var ns = ".<?=$ns?>";
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";

		ns.wNewQuotesInformerWidget = wNewQuotesInformerWidgetOpen({
			ns: ns,
			ins: ins,
			connUrl: '<?=$quotesSettings->wsServerUrl?>',
			quotesKey: '<?=$quotesKey?>',
			timeDataOffset: <?=$quotesSettings->realQuotesDataTimeDifference?>,
			selector: nsText+' .<?=$ins?>',
			columns: <?=json_encode($columns)?>,
		});
	}( window.jQuery );
</script>
<?php } ?>