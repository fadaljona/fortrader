(function () {
    var self = {
        init:function() {
            self.classSelector = 'FT<?=$hash?> FT<?=$marker?>';
            self.$wrapper = document.getElementsByClassName( self.classSelector )[0];

            self.setSymbolsLeftPadding();
            setTimeout(function(){self.setSymbolsLeftPadding();}, 500);
            self.listenDomLoaded();
            window.addEventListener('resize', self.windowResized );

        },
        windowResized: function(){
            self.setSymbolsLeftPadding();
        },
        domLoaded: function(){
            self.setSymbolsLeftPadding();
        },
        setSymbolsLeftPadding: function(){
            var symbols = document.getElementsByClassName('symbolContainer<?=$hash?>');
            if( !symbols.length ) return false;
                
            var maxWidth = 0;
            for (var i = 0; i < symbols.length; i++) {
                var elWidth = parseInt( symbols[i].offsetWidth );
                if( elWidth > maxWidth ) maxWidth = elWidth;
            }
            var parentElWidth = parseInt( symbols[0].parentElement.offsetWidth );
            var leftPadding = Math.floor( (parentElWidth - maxWidth - 17) / 2 );
            
            if( leftPadding < 10 ) leftPadding = 10;
            
            for (var i = 0; i < symbols.length; i++) {
                symbols[i].parentElement.style.paddingLeft = leftPadding + 'px';
            }
            
            var maxWidth = 0;
            for (var i = 0; i < symbols.length; i++) {
                var elWidth = parseInt( symbols[i].offsetWidth );
                if( elWidth > maxWidth ) maxWidth = elWidth;
            }
            var parentElWidth = parseInt( symbols[0].parentElement.offsetWidth );
            var leftPadding = Math.floor( (parentElWidth - maxWidth - 17) / 2 );
            
            if( leftPadding < 10 ) leftPadding = 10;
            
            for (var i = 0; i < symbols.length; i++) {
                symbols[i].parentElement.style.paddingLeft = leftPadding + 'px';
            }
        },
        listenDomLoaded: function(){
            document.readyState !== "complete" ? setTimeout(self.listenDomLoaded, 11) : self.domLoaded();
        },

    }
    self.init();

})();