(function () {
    var self = {
        
        init:function() {
            self.classSelector = 'FT<?=$hash?> FT<?=$marker?>';
            self.$wrapper = document.getElementsByClassName( self.classSelector )[0];

            self.equalSymbolsWidth();
        },
        equalSymbolsWidth: function(){
            var symbols = self.$wrapper.getElementsByClassName('symbolContainer');
            
            var maxWidth = 0;
            for (var i = 0; i < symbols.length; i++) {
                var elWidth = parseInt( symbols[i].offsetWidth );
                if( elWidth > maxWidth ) maxWidth = elWidth;
            }
            for (var i = 0; i < symbols.length; i++) {
                symbols[i].style.width = maxWidth + 'px';
                symbols[i].style.display = 'inline-block';
            }
            
            var symbolCodes = self.$wrapper.getElementsByClassName('symbolCode');
            
            var maxCodeWidth = 0;
            var paddingSumm = false;
            
            for (var i = 0; i < symbolCodes.length; i++) {
                var elWidth = parseInt( symbolCodes[i].offsetWidth );
                if( elWidth > maxCodeWidth ) maxCodeWidth = elWidth;
                
                if( paddingSumm === false ){
                    var style = window.getComputedStyle(symbolCodes[i].parentElement);
                    paddingSumm = parseFloat( style.getPropertyValue('padding-left').replace('px', '') ) + parseFloat( style.getPropertyValue('padding-right').replace('px', '') );
                } 
            
            }
            
            var summWidth = maxCodeWidth + paddingSumm;
            
            for (var i = 0; i < symbolCodes.length; i++) {
                symbolCodes[i].parentElement.style.width = summWidth + 'px';
            }
            
            var maxStyle9TrWrapperWidth = 0;
            var style9Trs = self.$wrapper.getElementsByClassName('style9TrWrapper');
            for (var i = 0; i < style9Trs.length; i++) {
                var elWidth = parseInt( style9Trs[i].offsetWidth );
                if( elWidth > maxStyle9TrWrapperWidth ) maxStyle9TrWrapperWidth = elWidth;
            }
            maxStyle9TrWrapperWidth = maxStyle9TrWrapperWidth + 5;
            for (var i = 0; i < style9Trs.length; i++) {
                style9Trs[i].style.width = maxStyle9TrWrapperWidth + 'px';
            }
        },
        

    }
    self.init();
})();