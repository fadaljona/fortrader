(function () {
    var self = {
        options:{
            'lossClass': 'loss',
            'profitClass': 'profit',
            'profitLossElClass': 'trClass',
        },
        items:[
            <?php foreach( $items as $item ){ ?>
            {
                id: <?=$item->id?>,
                informerDiff: <?=$item->getInformerDiff($category->id)?>,
                url: '<?=$item->absoluteSingleURL?>',
                columns:[
                    <?php foreach( $columns as $column ){?>
                    {
                        'name': '<?=$column?>',
                        'value': '<?=$item->{$column}?>'
                    },
                    <?php } ?>
                ]
            },
            <?php } ?>
        ],
        init:function() {
            self.classSelector = 'FT<?=$hash?> FT<?=$marker?>';
            self.$wrapper = document.getElementsByClassName( self.classSelector )[0];

            self.setDateTimeIfExist();
            self.setValues();
        },
        setValues: function(){
            var stopFunc = false;

            self.items.forEach(function(item) {
                if( self.$wrapper.querySelector('a[href="'+item.url+'"]') == null ){
                    alert('<?=Yii::t('*', 'Please use origin Fortrader informer code')?>');
                    stopFunc = true;
                }
            });

            if( stopFunc ) return false;

            self.items.forEach(function(item) {
                var trEl = self.$wrapper.querySelector('.' + self.options.profitLossElClass + '[data-id="' + item.id + '"]');
                if( item.informerDiff < 0 ){
                    trEl.classList.add( self.options.lossClass );
                }
                if( item.informerDiff > 0 ){
                    trEl.classList.add( self.options.profitClass );
                }

                item.columns.forEach(function(column) {
                <?php
                    if( isset( $customSetValsStr ) ){
                        echo $customSetValsStr;
                    }else{
                ?>
                    trEl.querySelector('[data-column="' + column.name + '"]').innerHTML = column.value;
                <?php } ?>
                });
            });
        },
        setDateTimeIfExist: function(){
            var $timeEl = self.$wrapper.getElementsByClassName('ftDateTime')[0];
            if( $timeEl != undefined ){

                var ftD = new Date();
                ftD.setTime( ftD.getTime() + ftD.getTimezoneOffset() * 60 * 1000 );
                var ftDay = ftD.getDate(); if( ftDay < 10 ) ftDay = '0'+ftDay;
                var ftMo = ftD.getMonth() + 1; if( ftMo < 10 ) ftMo = '0' + ftMo;
                var ftYear = ftD.getFullYear();
                var ftHours = ftD.getHours(); if( ftHours < 10 ) ftHours = '0' + ftHours;
                var ftMins = ftD.getMinutes(); if( ftMins < 10 ) ftMins = '0' + ftMins;

                $timeEl.setAttribute('datetime', ftYear + '-' + ftMo + '-' + ftDay + 'UTC' + ftHours + ':' + ftMins );
            }
            var $timeStrEl = self.$wrapper.getElementsByClassName('ftDateTimeStr')[0];
            if( $timeStrEl != undefined ){
                var ftD = new Date();
                ftD.setTime( ftD.getTime() + ftD.getTimezoneOffset() * 60 * 1000 + 60*60*3*1000 );
                var ftDay = ftD.getDate(); if( ftDay < 10 ) ftDay = '0'+ftDay;
                var ftMo = ftD.getMonth() + 1; if( ftMo < 10 ) ftMo = '0' + ftMo;
                var ftYear = ftD.getFullYear();
                /*var ftHours = ftD.getHours(); if( ftHours < 10 ) ftHours = '0' + ftHours;
                var ftMins = ftD.getMinutes(); if( ftMins < 10 ) ftMins = '0' + ftMins;*/
                    
                $timeStrEl.innerText = ftDay + '.' + ftMo + '.' + ftYear/* + ' ' + ftHours + ':' + ftMins*/;
            }
        },

    }
    self.init();
})();