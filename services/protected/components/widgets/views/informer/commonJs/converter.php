<?php
	Yii::App()->clientScript->registerCoreScript( 'jquery.ui' );
	
	$cs=Yii::app()->getClientScript();
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::app()->params['wpThemePath']."/plagins/jQueryFormStyler" );
	$cs->registerScriptFile( $jQueryFormStylerBaseUrl.'/jquery.formstyler.js' );
	$cs->registerCssFile( $jQueryFormStylerBaseUrl.'/jquery.formstyler.css' );
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wConverterInformerWidget.js' ) );
?>
<script>
	!function( $ ) {
		var ns = ".<?=$ns?>";
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";

		ns.wConverterInformerWidget = wConverterInformerWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .<?=$ins?>',
		});
	}( window.jQuery );
</script>