<?php
Yii::app()->clientScript->registerScript('EqualSymbolsPaddings', "

var symbols = document.getElementsByClassName('symbolContainer');
	
var maxWidth = 0;
for (var i = 0; i < symbols.length; i++) {
	var elWidth = parseInt( symbols[i].offsetWidth );
	if( elWidth > maxWidth ) maxWidth = elWidth;
}
var parentElWidth = parseInt( symbols[0].parentElement.offsetWidth );
var leftPadding = Math.floor( (parentElWidth - maxWidth - 17) / 2 );

if( leftPadding < 10 ) leftPadding = 10;

for (var i = 0; i < symbols.length; i++) {
	symbols[i].parentElement.style.paddingLeft = leftPadding + 'px';
}

//подгонка под новые размеры блоков

var maxWidth = 0;
for (var i = 0; i < symbols.length; i++) {
	var elWidth = parseInt( symbols[i].offsetWidth );
	if( elWidth > maxWidth ) maxWidth = elWidth;
}
var parentElWidth = parseInt( symbols[0].parentElement.offsetWidth );
var leftPadding = Math.floor( (parentElWidth - maxWidth - 17) / 2 );

if( leftPadding < 10 ) leftPadding = 10;

for (var i = 0; i < symbols.length; i++) {
	symbols[i].parentElement.style.paddingLeft = leftPadding + 'px';
}

", CClientScript::POS_END);
?>