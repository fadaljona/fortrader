<?php
	$cs=Yii::app()->getClientScript();
	$cs->registerCoreScript( 'jquery.ui' );
	$csBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/monitoringForForum' );
	$cs->registerCssFile($csBaseUrl.'/css/reset.css');
	$cs->registerCssFile($csBaseUrl.'/css/main.css');
	$cs->registerCssFile($csBaseUrl.'/css/media.css');

	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div id="main-container">
	<div class="monitoring-holder clear">
		<!-- user-cart -->
		<div class="user-cart clear">
			<div class="user-cart-col">
				<div class="user-cart-row clear">
					<div class="heading"><p><?=Yii::t($NSi18n, 'Trader')?>:</p></div>
					<div class="value user-string">
						<span  class="user-link clear"><img src="<?=$member->user->getSizeAvatar( 20, 20)?>" alt="#"><?=$member->user->showName?></span>
					</div>
				</div>
				<div class="user-cart-row clear">
					<div class="heading"><p><?=Yii::t($NSi18n, 'Account')?>:</p></div>
					<div class="value user-string">
						<a href="<?=Yii::app()->createUrl('monitoring/single', array('id' => $member->id))?>" class="user-link clear" target='_blank' rel='noopener noreferrer'><span><?=$member->accNameShort?></span></a>
					</div>
				</div>
				<div class="user-cart-row clear">
					<div class="heading"><p><?=Yii::t($NSi18n, 'Broker')?>:</p></div>
					<div class="value"><p><?=CHtml::link('<span>' . $member->server->broker->shortName . '</span>', $member->server->broker->singleUrl, array('class' => 'user-link', 'target' => '_blank', 'rel' => 'noopener noreferrer'))?></p></div>
				</div>
				<div class="user-cart-row clear">
					<div class="heading"><p><?=Yii::t($NSi18n, 'Type')?>:</p></div>
					<div class="value"><p><?=$member->accountType?></p></div>
				</div>
			</div>
			<hr class="user-cart-separator clear">
			<div class="user-cart-col">
				<div class="user-cart-row clear">
					<div class="heading">
						<?=CommonLib::getInfoTextForForumInformer($NSi18n, 'Balance');?>
						<p><?=Yii::t($NSi18n, 'Balance')?>:</p>
					</div>
					<div class="value"><p>$<?=$member->stats->balance?></p></div>
				</div>
				<div class="user-cart-row clear">
					<div class="heading">
						<?=CommonLib::getInfoTextForForumInformer($NSi18n, 'Gain');?>
						<p><?=Yii::t($NSi18n, 'Gain')?>:</p>
					</div>
					<div class="value"><p><?=$member->stats->gain?>%</p></div>
				</div>
				<div class="user-cart-row clear">
					<div class="heading">
						<?=CommonLib::getInfoTextForForumInformer($NSi18n, 'Equity');?>
						<p class="fund"><?=Yii::t($NSi18n, 'Equity')?>:</p>
					</div>
					<div class="value"><p class="fund">$<?=$member->stats->equity?></p></div>
				</div>
			</div>
		</div>
		<!--/ user-cart -->

		<?php
			$this->widget( 'widgets.graphs.ContestMemberGraphForForumWidget', Array(
				'ns' => 'nsActionView',
				'contestMember' => $member,
				'type' => 'monitoring'
			));
		?>
	</div>
	<?php
		$this->widget( 'widgets.lists.ContestMemberLastDealsForForumWidget', Array(
			'ns' => 'nsActionView',
			'contestMember' => $member,
		));
	?>
</div>
<script>
jQuery(document).ready(function($){
	$(window).load(function() {
		var message = document.body.scrollHeight + 'forMonitoringInformer';
		window.parent.postMessage(message,'https://forexsystemsru.com');
	});
});
</script>