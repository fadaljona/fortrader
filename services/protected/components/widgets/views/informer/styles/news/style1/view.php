<?php
	$cs=Yii::app()->getClientScript();
	
	$cs->registerCoreScript( 'jquery' );
	$cs->registerCssFile( Yii::app()->params['wpThemeUrl'] .'/informerNews.css');
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	require_once( 'css.php' );
?>
<div class="informer-news__wrap <?=$ins?>">
	<div class="informer-news__info-block">
		<div class="informer-news__info-title clearfix">
			<div class="informer-news__info-name"><a href="/<?=Yii::app()->baseUrl?>" target="_blank" rel="noopener" class="informer-news__info-link"><?=$category->toolTitle?></a></div>
			
			<?php if($slts){?>
				<a href="/<?=Yii::app()->baseUrl?>" target="_blank" rel="noopener" class="informer-news__info-link"><?=Yii::app()->params['domainInText']?></a>
			<?php }?>
		</div>
		<?php
			foreach( $items as $item ){
				echo CHtml::openTag('a', array( 'class' => 'informer-news__info-item', 'href' => get_permalink($item->ID), 'target' => '_blank', 'rel' => 'noopener' ) );
					echo CHtml::tag('div', array('class' => 'informer-news__item-title'), $item->post_title);
					$this->controller->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $item->post_date, 'class' => 'informer-news__item-time', 'ago' => 'backTextForTime' ));
				echo CHtml::closeTag('a');
			}
		?>
	</div>
</div>

<script>
!function( $ ) {
	
	setMediaStyles();
	
	$(window).on("resize",function(){ setMediaStyles(); });
	
	function setMediaStyles(){
		
		$('#mediaStyles').remove();
		
		var clientWidth = document.body.clientWidth;
		var mediaStyles = '';

		if( clientWidth < 1900 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 1500px;}'; }
		if( clientWidth < 1800 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 1300px;}'; }
		if( clientWidth < 1600 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 1100px;}'; }
		if( clientWidth < 1350 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 1000px;}'; }
		if( clientWidth < 1200 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 900px;}'; }
		if( clientWidth < 1100 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 800px;}'; }
		if( clientWidth < 1000 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 700px;}'; }
		if( clientWidth < 900 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 600px;}'; }
		if( clientWidth < 800 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 500px;}'; }
		if( clientWidth < 700 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 400px;}'; }
		if( clientWidth < 600 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 300px;}'; }
		if( clientWidth < 500 ){ mediaStyles = mediaStyles + '.informer-news__item-title {max-width: 200px;}'; }

		if( clientWidth < 1200 ){
			mediaStyles = mediaStyles + '.inside-menu__gen a {padding-left: 15px;}.inside-menu__sub a {padding-left: 20px;font-size: 14px;}.informer-calendar__tb table td [class*="label_"] {font-size: 10px;}';
		}
		if( clientWidth < 480 ){
			mediaStyles = mediaStyles +  '.informer-news__filter-help {font-size: 12px;line-height: 14px;}';
		}
		if( mediaStyles != '' ){
			mediaStyles = '<style id="mediaStyles">' + mediaStyles + '</style>';
			$('head').append( mediaStyles );
		}
	}
	
}( window.jQuery );
</script>