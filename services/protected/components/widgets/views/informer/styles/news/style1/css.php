<?php
$m2 = ceil( 2 * $mult );
$m3 = ceil( 3 * $mult );
$m5 = ceil( 5 * $mult );
$m10 = ceil( 10 * $mult );
$m12 = ceil( 12 * $mult );
$m13 = ceil( 13 * $mult );
$m14 = ceil( 14 * $mult );
$m32 = ceil( 32 * $mult );
$m35 = ceil( 35 * $mult );


Yii::app()->clientScript->registerCss('informerCss', "


body{
	width: {$width};
}
a{
	font-size: {$m14}px;
}

.informer-news__info-title{
	background-color:{$informerColors['titleBackgroundColor']};
}
.informer-news__info-name, .informer-news__info-link, .informer-news__info-link:hover{
	color:{$informerColors['titleTextColor']};
}
.informer-news__info-item{
	border:1px solid {$informerColors['newsBorderColor']};
	border-left:{$m2}px solid {$informerColors['borderLeftColor']};
	background-color:{$informerColors['newsBackgroundColor']};
	
	height: {$m32}px;
	margin-top: {$m2}px;
	padding: 0 {$m10}px 0 {$m12}px;
	line-height: {$m32}px;
}
.informer-news__item-title{
	color:{$informerColors['newsTextColor']};
	font-size: {$m13}px;
}
.informer-news__item-time{
	color:{$informerColors['timeTextColor']};
}

.informer-news__info-title {
  padding: 0 {$m12}px 0 {$m10}px;
  height: {$m32}px;
  line-height: {$m32}px;
}
.informer-news__info-name, .informer-news__info-name a{
	font-size: {$m12}px;
}

.informer-news__item-time {
  font-size: {$m12}px;
  line-height: {$m35}px;
}

.informer-news__item-time:before {
  margin: 0 {$m5}px 0 {$m3}px;
}


");