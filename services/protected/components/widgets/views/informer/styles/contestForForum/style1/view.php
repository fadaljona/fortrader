<?php
	$cs=Yii::app()->getClientScript();
	$cs->registerCoreScript( 'jquery.ui' );
	$csBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/contestForForum' );
	$cs->registerCssFile($csBaseUrl.'/css/reset.css');
	$cs->registerCssFile($csBaseUrl.'/css/main.css');


	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$mStatus = '';
    if( !Yii::App()->user->isGuest and Yii::App()->user->getModel()->isRegisteredInContest( $contest->id )){
        $mStatus = 'registered';
    }
?>
<div id="main-container">
	<?php
		if( strtotime($contest->begin) < time() ){
	?>
		<div id="leaders-banner">
            <img src="<?=$csBaseUrl?>/img/small-cup.png" alt="#">
            <h3 class="heading"><?=Yii::t($NSi18n, 'Current {br} leaders of the contest', array('{br}' => '<br>'))?></h3>
			<?php
				$members = ContestMemberModel::model()->findAll(array(
					'with' => array('stats', 'user'),
					'condition' => ' `t`.`idContest` = :idContest AND `stats`.`place` IS NOT NULL AND (`t`.`status` = "participates" OR `t`.`status` = "completed") AND (`stats`.`last_error_code` = 0 OR `stats`.`last_error_code` = -1 OR `stats`.`last_error_code` = -2 OR `stats`.`last_error_code` IS NULL)',
					'params' => array(':idContest' => $contest->id),
					'order' => ' `stats`.`gain` DESC ',
					'limit' => 5
				));
			?>
            <ul>
				<?php
					foreach( $members as $member ){
						echo CHtml::openTag('li');
							echo CHtml::openTag('span', array('class' => 'holder'));
								echo CHtml::tag('span', array('class' => 'left-box'), $member->user->showName);
								echo CHtml::tag('span', array('class' => 'right-box'), floor($member->stats->gain) . '%');
							echo CHtml::closeTag('span');
							echo CHtml::tag('span', array('class' => 'percent-line'), ' ');
						echo CHtml::closeTag('li');
					}
				?>
            </ul>
			<?php
				if( $mStatus == 'registered' ){
					$contestMember = ContestModel::getContestMemberByContestId($contest->id);
					echo '<a href="'.ContestMemberModel::getModelSingleURL($contestMember->id).'" target="_blank" rel="noopener" class="register-btn">'.Yii::t($NSi18n, 'You are registered').'</a>';
				}elseif( strtotime($contest->endReg) < time() ){
					echo '<a href="'.$contest->singleUrl.'" target="_blank" rel="noopener" class="register-btn">'.Yii::t($NSi18n, 'Views contest').'</a>';
				}else{
					echo '<a href="'.$contest->singleUrl.'#contestRegistrationForm" target="_blank" rel="noopener" class="register-btn">'.Yii::t($NSi18n, 'Registration').'</a>';
				}
			?>
            
        </div>
	<?php
		}else{
	?>
		<div id="contest-banner">
            <h3 class="heading">
                <span class="subheading"><?=Yii::t($NSi18n, 'Forex contest')?></span>
                <span class="contest-name-string"><?=$contest->name?></span>
            </h3>
            <div class="cup-box">
                <span class="text-string"><?=Yii::t($NSi18n, 'Prize fund')?></span>
                <span class="price">$<?=$contest->sumPrizes?></span>
            </div>
            <?php
				if( $mStatus == 'registered' ){
					$contestMember = ContestModel::getContestMemberByContestId($contest->id);
					echo '<a href="'.ContestMemberModel::getModelSingleURL($contestMember->id).'" target="_blank" rel="noopener" class="register-btn">'.Yii::t($NSi18n, 'You are registered').'</a>';
				}elseif( strtotime($contest->endReg) < time() ){
					echo '<a href="'.$contest->singleUrl.'" target="_blank" rel="noopener" class="register-btn">'.Yii::t($NSi18n, 'Views contest').'</a>';
				}else{
					echo '<a href="'.$contest->singleUrl.'#contestRegistrationForm" target="_blank" rel="noopener" class="register-btn">'.Yii::t($NSi18n, 'Registration').'</a>';
				}
			?>
        </div>
	<?php
		}
	?>
</div>
<script>
jQuery(document).ready(function($){
	$(window).load(function() {
		var message = document.body.scrollHeight + 'forContestInformer';
		window.parent.postMessage(message,'https://forexsystemsru.com');
	});
});
</script>