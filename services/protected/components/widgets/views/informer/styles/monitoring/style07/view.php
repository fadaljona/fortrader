<?php

	$NSi18n = $this->getNSi18n();
	
	$pChartClassDir = Yii::getPathOfAlias('extensions.pChart.class');
	$pChartFontsDir = Yii::getPathOfAlias('extensions.pChart.fonts');
	
	$openSansSemiboldTtf = $pChartFontsDir . "/OpenSansSemibold.ttf";
	$openSansBoldTtf = $pChartFontsDir . "/OpenSansBold.ttf";
	$openSansTtf = $pChartFontsDir . "/OpenSans.ttf";

	include( $pChartClassDir . '/pDraw.class.php' );
	include( $pChartClassDir . '/pImage.class.php' );
	
	
	
//settings	
	$informerWidth = $width;
	
	$leftRightMargin = $settings['leftRightMargin'];
	
	
	$chartWith = $informerWidth-$leftRightMargin*2;
	$chartRatio = @$settings['chartRatio'] ? $settings['chartRatio'] : 1.7;
	$chartHeight = $chartWith / $chartRatio;
	
	$headerHeight = 30;
	$headerFontSize = 9;
	
	$textDataFontSize = $settings['textDataFontSize'];
	$textUpdatedFontSize = $settings['textUpdatedFontSize'];
	
	$backgroundColor = CommonLib::hex2rgbArr( $informerColors['backgroundColor'] );
	$headerBackgroundColor = CommonLib::hex2rgbArr( $informerColors['headerBackgroundColor'] );
	$headerTextColor = CommonLib::hex2rgbArr( $informerColors['headerTextColor'] );
	$informerDataTextColor = CommonLib::hex2rgbArr( $informerColors['informerDataTextColor'] );
	
	$updatedTextColor = CommonLib::hex2rgbArr( $informerColors['updatedTextColor'] );
	
	

	

	
//informer image
	$informer = new pImage( $informerWidth, $chartHeight *4 );


//background gradient
	$informer->drawGradientArea( 
		0, 
		0, 
		$informerWidth, 
		$chartHeight *4, 
		DIRECTION_VERTICAL, 
		array( 
			"StartR" => $backgroundColor['R'], 
			"StartG" => $backgroundColor['G'], 
			"StartB" => $backgroundColor['B'], 
			"EndR" => $backgroundColor['R']+15 < 255 ? $backgroundColor['R']+15 : 255, 
			"EndG" => $backgroundColor['G']+15 < 255 ? $backgroundColor['G']+15 : 255, 
			"EndB" => $backgroundColor['B']+15 < 255 ? $backgroundColor['B']+15 : 255, 
			"Alpha" => 100 
		)
	);


//header
	$informer->drawFilledRectangle( 0, 0, $informerWidth, $headerHeight, $headerBackgroundColor );
	$informer->setFontProperties( array( "FontName" => $openSansBoldTtf, "FontSize" => $headerFontSize ) );
	
	$headerTextXoffset = $leftRightMargin;
	$align = TEXT_ALIGN_MIDDLELEFT;
	
	
	$headerTextPos = $informer->drawText( 
		$headerTextXoffset, 
		$headerHeight/2, 
		strtoupper( $member->imgInformerTitle ), 
		array( "Align" => $align, "R" => $headerTextColor['R'], "G" => $headerTextColor['G'], "B" => $headerTextColor['B'] )
	); 

	
	
//text data
$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textDataFontSize ) );
$fromTop = $headerHeight + $textDataFontSize*2;


	
	$fromTop = $fromTop - $textDataFontSize;
	
	
	
	$dataTextArr = array();
	if( $member->stats->gain ) $dataTextArr[] = array('text' => Yii::t($NSi18n, 'Gain') . ':', 'value' => $member->stats->gain, 'type' => '%  ' );
	if( $member->stats->drowMax ) $dataTextArr[] = array('text' => Yii::t($NSi18n, 'Drow') . ':', 'value' => $member->stats->drowMax, 'type' => '%  ');
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Tradese') . ':', 'value' => $member->stats->countTrades . '  '  );
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Open') . ':', 'value' => $member->stats->countOpen . '  ' );
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Equity') . ':', 'value' => $member->stats->equity . ' $  ' );
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Profit') . ':', 'value' => $member->stats->profit . ' $  ' );
	
	
	
		
		
			
			$fromTop = $headerHeight / 2;

			$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textUpdatedFontSize ) );
			
			$textToPrint = Yii::t($NSi18n, 'Updated:') . ' ' . gmdate('d.m.Y H:i', strtotime($member->stats->trueDT)) . ' GMT';
			
		
			
		
			
			
				$informer->drawText(
					$informerWidth - $leftRightMargin, 
				$fromTop, 
					$textToPrint, 
					array( "Align" => TEXT_ALIGN_MIDDLERIGHT, "R" => $updatedTextColor['R'], "G" => $updatedTextColor['G'], "B" => $updatedTextColor['B'] ) 
				);
			
			
			
			
			$fromTop = $headerHeight + $textDataFontSize;
			$negativeValuesColor = $informerDataTextColor;
			$positiveValuesColor = $informerDataTextColor;
			
			$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textDataFontSize ) );
		
	
	
		$prevTxtXPos = $leftRightMargin;
		$firstTxt = true;
		
		foreach( $dataTextArr as $index => $textArr ){
			
			foreach( $textArr as $key => $value ){
				if( $key == 'type' ) continue;
				
				$neededColorR = $informerDataTextColor['R'];
				$neededColorG = $informerDataTextColor['G'];
				$neededColorB = $informerDataTextColor['B'];
				
				if( $key == 'text' ){
					$textToPrint = $value;
				}elseif( $key == 'value' ){
					if( !isset( $dataTextArr[$index]['type'] ) ){
						$textToPrint = $value;
					}else{
						$textToPrint = $value . $dataTextArr[$index]['type'];
						if( $value > 0 ){
							$textToPrint = '+' . $textToPrint;
							$neededColorR = $positiveValuesColor['R'];
							$neededColorG = $positiveValuesColor['G'];
							$neededColorB = $positiveValuesColor['B'];
						}elseif( $value < 0 ){
							$neededColorR = $negativeValuesColor['R'];
							$neededColorG = $negativeValuesColor['G'];
							$neededColorB = $negativeValuesColor['B'];
						}
					}
				}
				
			
				
				
				
				if( $prevTxtXPos == $leftRightMargin ){
					$informer->drawText( $prevTxtXPos, $fromTop, $textToPrint, array("Align"=>TEXT_ALIGN_TOPLEFT, "R"=>$neededColorR,"G"=>$neededColorG,"B"=>$neededColorB )); 
					$prevTxtPos = $informer->getTextBox($prevTxtXPos, $fromTop, $openSansSemiboldTtf, $textDataFontSize, 0, $textToPrint);
					$prevTxtXPos = $prevTxtPos[1]['X'];
				}else{
					
					$nextTxtPos = $informer->getTextBox( $prevTxtXPos + $textDataFontSize/2, $fromTop, $openSansSemiboldTtf, $textDataFontSize, 0, $textToPrint );
					if( $nextTxtPos[1]['X'] >= $informerWidth-$leftRightMargin ){
						$fromTop = $fromTop + $textDataFontSize*2;
						$xTextOffset = $leftRightMargin;
						
						$nextTxtPos2 = $informer->getTextBox( $xTextOffset, $fromTop, $openSansSemiboldTtf, $textDataFontSize, 0, $textToPrint );
						$prevTxtXPos = $nextTxtPos2[1]['X'];
						
					}else{
						$xTextOffset = $prevTxtXPos + $textDataFontSize;
						$prevTxtXPos = $nextTxtPos[1]['X'];
					}
					
					$informer->drawText( $xTextOffset, $fromTop, $textToPrint, array("Align"=>TEXT_ALIGN_TOPLEFT, "R"=>$neededColorR,"G"=>$neededColorG,"B"=>$neededColorB ));
					
				}
				
			}
			
			

		}
		
		$fromTop = $fromTop + $textDataFontSize;
	

	

	
	
	


//resize

if( $settings['type'] == 7 ) $fromTop += $textDataFontSize*1.5;

	$tmpImg = imagecreatetruecolor( $informerWidth , $fromTop );
	imagecopyresized ( $tmpImg , $informer->Picture , 0, 0, 0, 0, $informerWidth, $fromTop, $informerWidth, $fromTop );
	$informer->Picture =  $tmpImg;


	imagepng( $informer->Picture );
