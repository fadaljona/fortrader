<?php

	$NSi18n = $this->getNSi18n();
	
	$pChartClassDir = Yii::getPathOfAlias('extensions.pChart.class');
	$pChartFontsDir = Yii::getPathOfAlias('extensions.pChart.fonts');
	
	$openSansSemiboldTtf = $pChartFontsDir . "/OpenSansSemibold.ttf";
	$openSansBoldTtf = $pChartFontsDir . "/OpenSansBold.ttf";
	$openSansTtf = $pChartFontsDir . "/OpenSans.ttf";

	include( $pChartClassDir . '/pDraw.class.php' );
	include( $pChartClassDir . '/pImage.class.php' );
	
	
	
//settings	
	$informerWidth = $width;
	
	$leftRightMargin = $settings['leftRightMargin'];
	
	
	$chartWith = $informerWidth-$leftRightMargin*2;
	$chartRatio = @$settings['chartRatio'] ? $settings['chartRatio'] : 1.7;
	$chartHeight = $chartWith / $chartRatio;
	
	$headerHeight = 30;
	$headerFontSize = 9;
	
	$textDataFontSize = $settings['textDataFontSize'];
	$textUpdatedFontSize = $settings['textUpdatedFontSize'];
	
	$backgroundColor = CommonLib::hex2rgbArr( $informerColors['backgroundColor'] );
	$headerBackgroundColor = CommonLib::hex2rgbArr( $informerColors['headerBackgroundColor'] );
	$headerTextColor = CommonLib::hex2rgbArr( $informerColors['headerTextColor'] );
	$informerDataTextColor = CommonLib::hex2rgbArr( $informerColors['informerDataTextColor'] );
	

	$updatedTextColor = CommonLib::hex2rgbArr( $informerColors['updatedTextColor'] );
	
	
	

	
	
//informer image
	$informer = new pImage( $informerWidth, $chartHeight *4 );



//header
	$informer->drawFilledRectangle( 0, 0, $informerWidth, $headerHeight, $headerBackgroundColor );
	$informer->setFontProperties( array( "FontName" => $openSansBoldTtf, "FontSize" => $headerFontSize ) );
	
	$headerTextXoffset = $leftRightMargin;
	$align = TEXT_ALIGN_MIDDLELEFT;
	
	
	$headerTextPos = $informer->drawText( 
		$headerTextXoffset, 
		$headerHeight/2, 
		strtoupper( $member->imgInformerTitle ), 
		array( "Align" => $align, "R" => $headerTextColor['R'], "G" => $headerTextColor['G'], "B" => $headerTextColor['B'] )
	); 

	
	
//text data
$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textDataFontSize ) );
$fromTop = $headerHeight + $textDataFontSize*2;


	
	$fromTop = $fromTop - $textDataFontSize;
	
	
	
	$dataTextArr = array();
	if( $member->stats->gain ) $dataTextArr[] = array('text' => Yii::t($NSi18n, 'Gain') . ':', 'value' => $member->stats->gain, 'type' => '%  ' );
	if( $member->stats->drowMax ) $dataTextArr[] = array('text' => Yii::t($NSi18n, 'Drow') . ':', 'value' => $member->stats->drowMax, 'type' => '%  ');
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Tradese') . ':', 'value' => $member->stats->countTrades . '  '  );
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Open') . ':', 'value' => $member->stats->countOpen . '  ' );
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Equity') . ':', 'value' => $member->stats->equity . ' $  ' );
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Profit') . ':', 'value' => $member->stats->profit . ' $  ' );
	
	
	
		
		$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textUpdatedFontSize ) );
		$fromTop = $headerHeight / 2;
		
		$textPos = $informer->drawText(
			$informerWidth - $leftRightMargin, 
			$fromTop, 
			Yii::t($NSi18n, 'Updated:') . ' ' . gmdate('d.m.Y H:i', strtotime($member->stats->trueDT)) . ' GMT', 
			array( "Align" => TEXT_ALIGN_MIDDLERIGHT, "R" => $updatedTextColor['R'], "G" => $updatedTextColor['G'], "B" => $updatedTextColor['B'] ) 
		);
		
		$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textDataFontSize ) );
		
		$textToPrint = '';
		
		$xTextRightOffset = $informerWidth - $leftRightMargin - $textPos[1]['X'] + $textPos[0]['X'];
		
		foreach( $dataTextArr as $str ){
			if( isset( $str['type'] ) )
				$val = $str['value'] > 0 ? '+' . $str['value'] . $str['type'] : $str['value'] . $str['type'];
			else
				$val = $str['value'];
			
			$txtPos = $informer->getTextBox($headerTextPos[1]['X'], $fromTop, $openSansSemiboldTtf, $textDataFontSize, 0, $textToPrint . $str['text'] . ' ' . $val . '  ');
			
			if( $txtPos[1]['X'] > $xTextRightOffset ) continue;
			
			$textToPrint .= $str['text'] . ' ' . $val . '  ';
		}
		
		
		
		$informer->drawText(
			$xTextRightOffset, 
			$fromTop, 
			$textToPrint, 
			array( "Align" => TEXT_ALIGN_MIDDLERIGHT, "R" => $informerDataTextColor['R'], "G" => $informerDataTextColor['G'], "B" => $informerDataTextColor['B'] ) 
		);
		

	


//resize

if( $settings['type'] == 6 ) $fromTop = $headerHeight;


	$tmpImg = imagecreatetruecolor( $informerWidth , $fromTop );
	imagecopyresized ( $tmpImg , $informer->Picture , 0, 0, 0, 0, $informerWidth, $fromTop, $informerWidth, $fromTop );
	$informer->Picture =  $tmpImg;


	imagepng( $informer->Picture );
