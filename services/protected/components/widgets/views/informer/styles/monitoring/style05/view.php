<?php

	$NSi18n = $this->getNSi18n();
	
	$pChartClassDir = Yii::getPathOfAlias('extensions.pChart.class');
	$pChartFontsDir = Yii::getPathOfAlias('extensions.pChart.fonts');
	
	$openSansSemiboldTtf = $pChartFontsDir . "/OpenSansSemibold.ttf";
	$openSansBoldTtf = $pChartFontsDir . "/OpenSansBold.ttf";
	$openSansTtf = $pChartFontsDir . "/OpenSans.ttf";

	include( $pChartClassDir . '/pDraw.class.php' );
	include( $pChartClassDir . '/pImage.class.php' );
	
	
	
//settings	
	$informerWidth = $width;
	
	$leftRightMargin = $settings['leftRightMargin'];
	
	
	$chartWith = $informerWidth-$leftRightMargin*2;
	$chartRatio = @$settings['chartRatio'] ? $settings['chartRatio'] : 1.7;
	$chartHeight = $chartWith / $chartRatio;
	
	$headerHeight = 30;
	$headerFontSize = 9;
	
	$textDataFontSize = $settings['textDataFontSize'];
	$textUpdatedFontSize = $settings['textUpdatedFontSize'];
	
	$backgroundColor = CommonLib::hex2rgbArr( $informerColors['backgroundColor'] );
	$headerBackgroundColor = CommonLib::hex2rgbArr( $informerColors['headerBackgroundColor'] );
	$headerTextColor = CommonLib::hex2rgbArr( $informerColors['headerTextColor'] );
	$informerDataTextColor = CommonLib::hex2rgbArr( $informerColors['informerDataTextColor'] );
	

	$updatedTextColor = CommonLib::hex2rgbArr( $informerColors['updatedTextColor'] );
	
	
	
	
	

//graph

	$chartData = $this->getMonitoringChartData($member);

	$graphBorderColor = CommonLib::hex2rgbArr( $informerColors['graphBorderColor'] );
	$graphEquityLineColor = $informerColors['graphEquityLineColor'];
	$graphBalanceLineColor = $informerColors['graphBalanceLineColor'];
	$graphGridColor = $informerColors['graphGridColor'];
	$graphLegendTextColor = $informerColors['graphLegendTextColor'];
	$graphBackgroundColor = $informerColors['graphBackgroundColor'];

	$jpgraphDir = Yii::getPathOfAlias('extensions.jpgraph');
	
	define('TTF_DIR',$pChartFontsDir);
	require_once ( $jpgraphDir.'/jpgraph.php' );
	require_once ( $jpgraphDir.'/jpgraph_line.php' );
	require_once ( $jpgraphDir.'/jpgraph_date.php' );
	
	$graph = new Graph( $chartWith, $chartHeight+17 );
	$graph->SetMargin(0,0,0,0);

	$graph->SetScale('datlin',$chartData['minY']-250,$chartData['maxY']+50);

	$balanceLine = new LinePlot($chartData['balance'],$chartData['abscissa']);
	$balanceLine->SetLegend( Yii::t($NSi18n, 'Balance') );

	$equityLine = new LinePlot($chartData['equity'],$chartData['abscissa']);
	
	$equityLine->SetLegend( Yii::t($NSi18n, 'Equity') );
	

	$graph->ygrid->SetFill(false); 

	$graph->ygrid->Show(); 
	$graph->ygrid->SetWeight(1);  
	$graph->ygrid->SetColor( $graphGridColor );
	$graph->xgrid->Show();
	$graph->xgrid->SetWeight(1); 
	$graph->xgrid->SetColor( $graphGridColor );

	$graph->SetUserFont('/OpenSansSemibold.ttf');
	
	$graph->legend->SetFont(FF_USERFONT,FS_NORMAL, 8);
	
	$graph->legend->SetAbsPos(2,$chartHeight-22,'left','top');
	$graph->legend->SetLayout(LEGEND_HOR);
	$graph->legend->SetMarkAbsSize(3);
	$graph->legend->SetColor( $graphLegendTextColor );
	$graph->legend->SetFillColor( $graphBackgroundColor );
	$graph->SetColor( $graphBackgroundColor );

	if( count( $chartData['abscissa'] ) == 1 ) $graph->xaxis->Hide();

	$graph->Add($balanceLine);
	$graph->Add($equityLine);
	
	$balanceLine->SetColor( $graphBalanceLineColor );
	$equityLine->SetColor( $graphEquityLineColor );

	$graph->graph_theme=null;
	$graphImg = $graph->Stroke(_IMG_HANDLER);

	
	
//informer image
	$informer = new pImage( $informerWidth, $chartHeight *4 );


//background gradient
	$informer->drawGradientArea( 
		0, 
		0, 
		$informerWidth, 
		$chartHeight *4, 
		DIRECTION_VERTICAL, 
		array( 
			"StartR" => $backgroundColor['R'], 
			"StartG" => $backgroundColor['G'], 
			"StartB" => $backgroundColor['B'], 
			"EndR" => $backgroundColor['R']+15 < 255 ? $backgroundColor['R']+15 : 255, 
			"EndG" => $backgroundColor['G']+15 < 255 ? $backgroundColor['G']+15 : 255, 
			"EndB" => $backgroundColor['B']+15 < 255 ? $backgroundColor['B']+15 : 255, 
			"Alpha" => 100 
		)
	);


//header
	$informer->drawFilledRectangle( 0, 0, $informerWidth, $headerHeight, $headerBackgroundColor );
	$informer->setFontProperties( array( "FontName" => $openSansBoldTtf, "FontSize" => $headerFontSize ) );
	
	$headerTextXoffset = 70;
	$align = TEXT_ALIGN_MIDDLELEFT;
	
	
	$headerTextPos = $informer->drawText( 
		$headerTextXoffset, 
		$headerHeight/2, 
		strtoupper( $member->imgInformerTitle ), 
		array( "Align" => $align, "R" => $headerTextColor['R'], "G" => $headerTextColor['G'], "B" => $headerTextColor['B'] )
	); 

	
	
//text data
$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textDataFontSize ) );
$fromTop = $headerHeight + $textDataFontSize*2;


	
	$fromTop = $fromTop - $textDataFontSize;
	
	
	
	$dataTextArr = array();
	if( $member->stats->gain ) $dataTextArr[] = array('text' => Yii::t($NSi18n, 'Gain') . ':', 'value' => $member->stats->gain, 'type' => '%  ' );
	if( $member->stats->drowMax ) $dataTextArr[] = array('text' => Yii::t($NSi18n, 'Drow') . ':', 'value' => $member->stats->drowMax, 'type' => '%  ');
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Tradese') . ':', 'value' => $member->stats->countTrades . '  '  );
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Open') . ':', 'value' => $member->stats->countOpen . '  ' );
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Equity') . ':', 'value' => $member->stats->equity . ' $  ' );
	$dataTextArr[] = array('text' => Yii::t($NSi18n, 'Profit') . ':', 'value' => $member->stats->profit . ' $  ' );
	
	
	
		
		
			$negativeValuesColor = CommonLib::hex2rgbArr( $informerColors['negativeValuesColor'] );
			$positiveValuesColor = CommonLib::hex2rgbArr( $informerColors['positiveValuesColor'] );
		
	
	
		$prevTxtXPos = $leftRightMargin;
		$firstTxt = true;
		
		foreach( $dataTextArr as $index => $textArr ){
			
			foreach( $textArr as $key => $value ){
				if( $key == 'type' ) continue;
				
				$neededColorR = $informerDataTextColor['R'];
				$neededColorG = $informerDataTextColor['G'];
				$neededColorB = $informerDataTextColor['B'];
				
				if( $key == 'text' ){
					$textToPrint = $value;
				}elseif( $key == 'value' ){
					if( !isset( $dataTextArr[$index]['type'] ) ){
						$textToPrint = $value;
					}else{
						$textToPrint = $value . $dataTextArr[$index]['type'];
						if( $value > 0 ){
							$textToPrint = '+' . $textToPrint;
							$neededColorR = $positiveValuesColor['R'];
							$neededColorG = $positiveValuesColor['G'];
							$neededColorB = $positiveValuesColor['B'];
						}elseif( $value < 0 ){
							$neededColorR = $negativeValuesColor['R'];
							$neededColorG = $negativeValuesColor['G'];
							$neededColorB = $negativeValuesColor['B'];
						}
					}
				}
				
			
				
				
				
				if( $prevTxtXPos == $leftRightMargin ){
					$informer->drawText( $prevTxtXPos, $fromTop, $textToPrint, array("Align"=>TEXT_ALIGN_TOPLEFT, "R"=>$neededColorR,"G"=>$neededColorG,"B"=>$neededColorB )); 
					$prevTxtPos = $informer->getTextBox($prevTxtXPos, $fromTop, $openSansSemiboldTtf, $textDataFontSize, 0, $textToPrint);
					$prevTxtXPos = $prevTxtPos[1]['X'];
				}else{
					
					$nextTxtPos = $informer->getTextBox( $prevTxtXPos + $textDataFontSize/2, $fromTop, $openSansSemiboldTtf, $textDataFontSize, 0, $textToPrint );
					if( $nextTxtPos[1]['X'] >= $informerWidth-$leftRightMargin ){
						$fromTop = $fromTop + $textDataFontSize*2;
						$xTextOffset = $leftRightMargin;
						
						$nextTxtPos2 = $informer->getTextBox( $xTextOffset, $fromTop, $openSansSemiboldTtf, $textDataFontSize, 0, $textToPrint );
						$prevTxtXPos = $nextTxtPos2[1]['X'];
						
					}else{
						$xTextOffset = $prevTxtXPos + $textDataFontSize;
						$prevTxtXPos = $nextTxtPos[1]['X'];
					}
					
					$informer->drawText( $xTextOffset, $fromTop, $textToPrint, array("Align"=>TEXT_ALIGN_TOPLEFT, "R"=>$neededColorR,"G"=>$neededColorG,"B"=>$neededColorB ));
					
				}
				
			}
			
			

		}
		
		$fromTop = $fromTop + $textDataFontSize;
	

	

	//graph to informer image
	$fromTop = $fromTop + $textDataFontSize*2;
	imagecopyresized( $informer->Picture , $graphImg , $leftRightMargin, $fromTop, 0, 0, $chartWith, $chartHeight, $chartWith, $chartHeight );
	
	//border around graph
	$informer->drawRectangle( $leftRightMargin+1, $fromTop, $leftRightMargin+$chartWith-1, $fromTop+$chartHeight-1, $graphBorderColor );

	
	
	
	

	
	//under chart text
	
	
	
	$fromTop = $fromTop + $chartHeight + $textDataFontSize*2;
	
	
	
	

	$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textUpdatedFontSize ) );

	
	
	$informer->drawText(
			$informerWidth/2, 
			$fromTop, 
			Yii::t($NSi18n, 'Updated:') . ' ' . gmdate('d.m.Y H:i', strtotime($member->stats->trueDT)) . ' GMT', 
			array( "Align" => TEXT_ALIGN_MIDDLEMIDDLE, "R" => $updatedTextColor['R'], "G" => $updatedTextColor['G'], "B" => $updatedTextColor['B'] ) 
		);
	
	
	
	$fromTop = $fromTop + $textUpdatedFontSize*2;
	
	



//resize



	$tmpImg = imagecreatetruecolor( $informerWidth , $fromTop );
	imagecopyresized ( $tmpImg , $informer->Picture , 0, 0, 0, 0, $informerWidth, $fromTop, $informerWidth, $fromTop );
	$informer->Picture =  $tmpImg;


	imagepng( $informer->Picture );
