<?php

	$NSi18n = $this->getNSi18n();
	
	$pChartClassDir = Yii::getPathOfAlias('extensions.pChart.class');
	$pChartFontsDir = Yii::getPathOfAlias('extensions.pChart.fonts');
	
	$openSansSemiboldTtf = $pChartFontsDir . "/OpenSansSemibold.ttf";
	$openSansBoldTtf = $pChartFontsDir . "/OpenSansBold.ttf";
	$openSansTtf = $pChartFontsDir . "/OpenSans.ttf";

	include( $pChartClassDir . '/pDraw.class.php' );
	include( $pChartClassDir . '/pImage.class.php' );
	
	
	
//settings	
	$informerWidth = $width;
	
	$leftRightMargin = $settings['leftRightMargin'];
	
	
	$chartWith = $informerWidth-$leftRightMargin*2;
	$chartRatio = @$settings['chartRatio'] ? $settings['chartRatio'] : 1.7;
	$chartHeight = $chartWith / $chartRatio;
	
	$headerHeight = 30;
	$headerFontSize = 9;
	
	$textDataFontSize = $settings['textDataFontSize'];
	$textUpdatedFontSize = $settings['textUpdatedFontSize'];
	
	$backgroundColor = CommonLib::hex2rgbArr( $informerColors['backgroundColor'] );
	$headerBackgroundColor = CommonLib::hex2rgbArr( $informerColors['headerBackgroundColor'] );
	$headerTextColor = CommonLib::hex2rgbArr( $informerColors['headerTextColor'] );
	$informerDataTextColor = CommonLib::hex2rgbArr( $informerColors['informerDataTextColor'] );
	

	$updatedTextColor = CommonLib::hex2rgbArr( $informerColors['updatedTextColor'] );
	
	
//informer image
	$informer = new pImage( $informerWidth, $chartHeight *4 );


//background gradient
	$informer->drawGradientArea(
		0, 
		0, 
		$informerWidth, 
		$chartHeight *4, 
		DIRECTION_VERTICAL, 
		array( 
			"StartR" => $backgroundColor['R'], 
			"StartG" => $backgroundColor['G'], 
			"StartB" => $backgroundColor['B'], 
			"EndR" => $backgroundColor['R']+15 < 255 ? $backgroundColor['R']+15 : 255, 
			"EndG" => $backgroundColor['G']+15 < 255 ? $backgroundColor['G']+15 : 255, 
			"EndB" => $backgroundColor['B']+15 < 255 ? $backgroundColor['B']+15 : 255, 
			"Alpha" => 100 
		)
	);


//header
	$informer->drawFilledRectangle( 0, 0, $informerWidth, $headerHeight, $headerBackgroundColor );
	$informer->setFontProperties( array( "FontName" => $openSansBoldTtf, "FontSize" => $headerFontSize ) );
	
	
	$headerTextXoffset = $informerWidth/2;
	$align = TEXT_ALIGN_MIDDLEMIDDLE;
	
	
	$headerTextPos = $informer->drawText( 
		$headerTextXoffset, 
		$headerHeight/2, 
		strtoupper( $member->imgInformerTitle ), 
		array( "Align" => $align, "R" => $headerTextColor['R'], "G" => $headerTextColor['G'], "B" => $headerTextColor['B'] )
	); 

	
	
//text data
$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textDataFontSize ) );
$fromTop = $headerHeight + $textDataFontSize*2;

	


		$informer->drawText( 
			$leftRightMargin, 
			$fromTop, 
			Yii::t($NSi18n, 'Profit') . ":", 
			array( "Align" => TEXT_ALIGN_MIDDLELEFT, "R" => $informerDataTextColor['R'], "G" => $informerDataTextColor['G'], "B" => $informerDataTextColor['B'] ) 
		); 
		
		$informer->drawText( 
			$informerWidth-$leftRightMargin, 
			$fromTop, 
			$member->stats->profit . '$', 
			array( "Align" => TEXT_ALIGN_MIDDLERIGHT, "R" => $informerDataTextColor['R'], "G" => $informerDataTextColor['G'], "B" => $informerDataTextColor['B'] ) 
		); 

		$fromTop = $fromTop + $textDataFontSize*2;
	
	

	
	$informer->drawText( 
		$leftRightMargin, 
		$fromTop, 
		Yii::t($NSi18n, 'Balance') . ":", 
		array( "Align" => TEXT_ALIGN_MIDDLELEFT, "R" => $informerDataTextColor['R'], "G" => $informerDataTextColor['G'], "B" => $informerDataTextColor['B'] ) 
	); 
	
	$informer->drawText( 
		$informerWidth-$leftRightMargin, 
		$fromTop, 
		$member->stats->balance . ' $', 
		array( "Align" => TEXT_ALIGN_MIDDLERIGHT, "R" => $informerDataTextColor['R'], "G" => $informerDataTextColor['G'], "B" => $informerDataTextColor['B'] ) 
	); 

	$fromTop = $fromTop + $textDataFontSize*2;
	
	$informer->drawText( 
		$leftRightMargin, 
		$fromTop, 
		Yii::t($NSi18n, 'Equity') . ":", 
		array( "Align" => TEXT_ALIGN_MIDDLELEFT, "R" => $informerDataTextColor['R'], "G" => $informerDataTextColor['G'], "B" => $informerDataTextColor['B'] ) 
	); 
	
	$informer->drawText( 
		$informerWidth-$leftRightMargin, 
		$fromTop, 
		$member->stats->equity . ' $', 
		array( "Align" => TEXT_ALIGN_MIDDLERIGHT, "R" => $informerDataTextColor['R'], "G" => $informerDataTextColor['G'], "B" => $informerDataTextColor['B'] ) 
	); 

	


	
	
	
	

	
	//under chart text
	
	
	
	
		$fromTop = $fromTop + $textUpdatedFontSize*2;
		$informer->drawLine( $leftRightMargin, $fromTop, $informerWidth-$leftRightMargin, $fromTop, CommonLib::hex2rgbArr( $informerColors['lineAboveVerifiedColor'] ) );
		$fromTop = $fromTop + $textUpdatedFontSize*2;
	
	
	
	

	$informer->setFontProperties( array( "FontName" => $openSansSemiboldTtf, "FontSize" => $textUpdatedFontSize ) );

	
	

		$informer->drawText( 
			$informerWidth/2, 
			$fromTop, 
			Yii::t($NSi18n, 'Updated:'), 
			array( "Align" => TEXT_ALIGN_MIDDLEMIDDLE, "R" => $updatedTextColor['R'], "G" => $updatedTextColor['G'], "B" => $updatedTextColor['B'] ) 
		);
		
		$fromTop = $fromTop + $textUpdatedFontSize*2;
		
		$informer->drawText( 
			$informerWidth/2, 
			$fromTop, 
			gmdate('d.m.Y H:i', strtotime($member->stats->trueDT)) . ' GMT', 
			array( "Align" => TEXT_ALIGN_MIDDLEMIDDLE, "R" => $updatedTextColor['R'], "G" => $updatedTextColor['G'], "B" => $updatedTextColor['B'] ) 
		);
	
	
	
	$fromTop = $fromTop + $textUpdatedFontSize*2;
	



//resize


	$tmpImg = imagecreatetruecolor( $informerWidth , $fromTop );
	imagecopyresized ( $tmpImg , $informer->Picture , 0, 0, 0, 0, $informerWidth, $fromTop, $informerWidth, $fromTop );
	$informer->Picture =  $tmpImg;


	imagepng( $informer->Picture );
