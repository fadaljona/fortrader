<?php
	Yii::app()->clientScript->registerCssFile( Yii::app()->params['wpThemeUrl'] .'/informerConverter.css');
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="alignCenter <?=$ins?>">

	<div class="converter_box vertical inlineBlock" data-converter-color="white">
		<h4 class="converter_title"><?=CHtml::link( Yii::t( $NSi18n, 'Currency converter' ), Yii::app()->createAbsoluteUrl('currencyRates/converterPage'), array( 'target' => '_blank' ) )?></h4>
		<form class="converter_form_item clearfix">
                            
			<div class="informer_setings convert_value clearfix">

				<select class="select_type1 converterSelect converterSelectFrom">
				<?php
					$i=1;
					$flagStyles = '';
					foreach( $currenciesForConverter as $key => $val ){
						$selected = false;
						if( $from == $val['id'] ) $selected = true;
						echo CHtml::tag(
							'option',
							array(
								'data-value' => $val['value'],
								'value' => $val['id'],
								'data-nominal' => $val['nominal'],
								'class' => 'lang ' . $val['country'],
								'selected' => $selected,
							),
							$key
						);
						$flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $val['country']);
						if( $flagSrc ) $flagStyles .= ".lang.{$val['country']}, .lang.{$val['country']} .jq-selectbox__select-text{background-image: url({$flagSrc});}";
						if( $i == 2 ){
							$selected = false;
							if( $from == $defaultCurrency->id ) $selected = true;
							echo CHtml::tag(
								'option', 
								array(
									'data-value' => 1,
									'value' => $defaultCurrency->id,
									'data-nominal' => 1,
									'class' => 'lang ' . $defaultCurrency->country->alias,
									'selected' => $selected,
								), 
								$defaultCurrency->code
							);
							$flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $defaultCurrency->country->alias);
							if( $flagSrc ) $flagStyles .= ".lang.{$defaultCurrency->country->alias}, .lang.{$defaultCurrency->country->alias} .jq-selectbox__select-text{background-image: url({$flagSrc});}";
						}
						$i++;
					}
				?>
				</select>

				<div class="form_input_box">
					<input class="form_input converterInpFrom" type="text" value="<?=$amount?>">
				</div><!-- / .form_input_box -->
                    
			</div><!-- / .informer_setings -->

			<div class="convert_arrows flipConverter"></div><!-- / .convert_arrows -->

			<div class="informer_setings convert_value clearfix">
			
				<select class="select_type1 converterSelect converterSelectTo">
				<?php
					$i=1;
					foreach( $currenciesForConverter as $key => $val ){
						$selected = false;
						if( $to == $val['id'] ) $selected = true;
						echo CHtml::tag(
							'option',
							array(
								'data-value' => $val['value'],
								'value' => $val['id'],
								'data-nominal' => $val['nominal'],
								'class' => 'lang ' . $val['country'],
								'selected' => $selected,
							),
							$key
						);
						if( $i == 2 ){
							$selected = false;
							if( $to == $defaultCurrency->id ) $selected = true;
							echo CHtml::tag(
								'option', 
								array(
									'data-value' => 1,
									'value' => $defaultCurrency->id,
									'data-nominal' => 1,
									'class' => 'lang ' . $defaultCurrency->country->alias,
									'selected' => $selected,
								), 
								$defaultCurrency->code
							);
						}
						$i++;
					}
				?>
				</select>
				
				<div class="form_input_box">
					<input class="form_input converterInpTo" type="text" readonly>
				</div><!-- / .form_input_box -->
                    
			</div><!-- / .informer_setings -->

		</form>
		<div class="convert_footer">
			<?=Yii::t( $NSi18n, 'Cbr for' )?> <?=date('m.d.y')?>
			<div class="convert_footer_icon"></div>
		</div>
		
	<?php require_once( 'css.php' ); ?>
	</div>
</div>	
<?php
require_once( dirname(__FILE__) . '/../../../commonJs/converter.php' );
?>