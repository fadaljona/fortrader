﻿<?php
Yii::app()->clientScript->registerCss('flagStyles', $flagStyles);
$themeUrl = Yii::App()->request->getHostInfo() . Yii::app()->params['wpThemeUrl'];
Yii::app()->clientScript->registerCss('informerCss', "

.converter_box.vertical, .inlineBlock{
	width:{$width};
}
.converter_box.vertical .convert_arrows{
	width:100%;
}
.convert_value{
	width:44%;
}
.converter_box.horizontal .convert_value{
	width:43%;
}
.convert_arrows{
	width:12%;
}
.converter_box.horizontal .convert_arrows{
	width:14%;
}
.converter_title, .converter_title a{
	color: {$informerColors['headerTextColor']};
}
.converter_box{
	border-color: {$informerColors['borderColor']};
	background-color: {$informerColors['bgColor']};
}
.jq-selectbox__trigger-arrow{
	border-top-color: {$informerColors['inputArrowsColor']};
}
.select_type1 .jq-selectbox__trigger{
	background-color: {$informerColors['inputArrowsBgColor']};
}
.converter_form_item .select_type1 .jq-selectbox__select, .form_input{
	background-color: {$informerColors['inputBgColor']};
}
.jq-selectbox__select{
	text-shadow:1px 1px {$informerColors['symbolShadowColor']};
}
.form_input, .select_type1 .jq-selectbox__select{
	border-color: {$informerColors['inputBorderColor']};
}
.select_type1 .jq-selectbox__trigger{
	border-left-color: {$informerColors['inputBorderColor']};
}
.form_input{
	color: {$informerColors['inputTextColor']};
}
.select_type1 .jq-selectbox__select{
	color: {$informerColors['symbolTextColor']};
}
.converter_box{
	color: {$informerColors['dateTextColor']};
}
.convert_footer_icon{
	background-image: url({$themeUrl}/{$informerColors['logo']});
}
.change_color_input input[type='text'], .informer_setings input[type='text']{
	font-family:'OpenSansSemiBold';
	font-size:14px;
}
");