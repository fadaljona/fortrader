﻿<?php
Yii::app()->clientScript->registerCss('flagStyles', $flagStyles);
$themeUrl = Yii::App()->request->getHostInfo() . Yii::app()->params['wpThemeUrl'];
Yii::app()->clientScript->registerCss('informerCss', "

.converter_box, .inlineBlock{
	width:{$width};
}
.convert_value{
	width:44%;
}
.convert_arrows{
	width:12%;
}
.converter_title, .converter_title a{
	color: {$informerColors['headerTextColor']};
}
.converter_box{
	border-color: {$informerColors['borderColor']};
	background-color: {$informerColors['bgColor']};
}


.instal_link_box.converter_box{
	border:1px solid {$informerColors['borderColor']};
	border-top:none;
	background-color: {$informerColors['bgColor']};
}
.instal_link_box .instal_link, .instal_link_box .instal_link:before{
	color:{$informerColors['installLinkTextColor']};
}


.jq-selectbox__trigger-arrow{
	border-top-color: {$informerColors['inputArrowsColor']};
}
.select_type1 .jq-selectbox__trigger{
	background-color: {$informerColors['inputArrowsBgColor']};
}
.converter_form_item .select_type1 .jq-selectbox__select, .form_input{
	background-color: {$informerColors['inputBgColor']};
}
.jq-selectbox__select{
	text-shadow:1px 1px {$informerColors['symbolShadowColor']};
}
.form_input, .select_type1 .jq-selectbox__select{
	border-color: {$informerColors['inputBorderColor']};
}
.select_type1 .jq-selectbox__trigger{
	border-left-color: {$informerColors['inputBorderColor']};
}
.form_input{
	color: {$informerColors['inputTextColor']};
}
.select_type1 .jq-selectbox__select{
	color: {$informerColors['symbolTextColor']};
}
.converter_box{
	color: {$informerColors['dateTextColor']};
}


.lang.US, .lang.US .jq-selectbox__select-text{
  background-image: url(/uploads/country/flags/shiny/16/US.png);
}
.lang.RU, .lang.RU .jq-selectbox__select-text{
  background-image: url(/uploads/country/flags/shiny/16/RU.png);
}
.lang.EU, .lang.EU .jq-selectbox__select-text{
  background-image: url(/uploads/country/flags/shiny/16/EU.png);
}
");