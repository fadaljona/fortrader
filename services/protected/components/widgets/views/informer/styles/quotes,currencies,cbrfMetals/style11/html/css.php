<?php
echo '@import "' . Yii::app()->params['wpThemeUrl'] . '/css/informersHtml.css' . '";';

$m5 = ceil( 5 * $mult );
$m6 = ceil( 6 * $mult );
$m8 = ceil( 8 * $mult );
$m9 = ceil( 9 * $mult );
$m10 = ceil( 10 * $mult );
$m11 = ceil( 11 * $mult );
$m12 = ceil( 12 * $mult );
$m13 = ceil( 13 * $mult );
$m14 = ceil( 14 * $mult );
$m16 = ceil( 16 * $mult );
$m17 = ceil( 17 * $mult );
$m18 = ceil( 18 * $mult );
$m19 = ceil( 19 * $mult );
$m22 = ceil( 22 * $mult );
$m36 = ceil( 36 * $mult );
$m48 = ceil( 48 * $mult );
if( $mult < 0.8 )
	$m55 = ceil( 55 * ($mult/2) );
else
	$m55 = ceil( 55 * $mult );



$lastTrBorderColor = '';
if( !$showGetBtn ){
	$lastTrBorderColor = ".FTinformersWrapper.FT{$hash} table tr:last-of-type td{border-bottom-color:{$informerColors['tableBorderColor']} !important;}";
}

echo "

.FTinformersWrapper.FT{$hash} table{max-width: {$width};}

.FTinformersWrapper.FT{$hash} .informer_table_small.default td,
.FTinformersWrapper.FT{$hash} .informer_table_small.default th{
	padding: {$m10}px {$m5}px {$m9}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_marquee thead th,
.FTinformersWrapper.FT{$hash} .informer_table_small thead th,
.FTinformersWrapper.FT{$hash} .informer_table_middle thead th,
.FTinformersWrapper.FT{$hash} .informer_table thead th{
	font-size: {$m12}px;
	padding: {$m14}px {$m5}px {$m13}px;
}
.FTinformersWrapper.FT{$hash} {
	line-height:{$m22}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 .col1 td{
	padding: {$m11}px {$m5}px {$m6}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 .col2 td{
	padding: {$m6}px {$m5}px {$m11}px;
}
.FTinformersWrapper.FT{$hash} .informer_table tfoot td,
.FTinformersWrapper.FT{$hash} .informer_table_middle tfoot td,
.FTinformersWrapper.FT{$hash} .informer_table_small tfoot td{
	padding: {$m11}px {$m5}px;
}
.FTinformersWrapper.FT{$hash} .instal_link{
	font-size: {$m11}px !important;
}
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 tbody span{
	font-size: {$m11}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 .amount_middle{
	font-size: {$m19}px;
}


	  
	  
	  




.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 thead time,
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 thead th, 
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 thead th a{
	color: {$informerColors['titleTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 thead th{
	background: {$informerColors['titleBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small .symbol, 
.FTinformersWrapper.FT{$hash} .informer_table_small .symbol a{
	color: {$informerColors['symbolTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .informer_table_small .value{
	color: {$informerColors['tableTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profit .changeVal{
	color:{$informerColors['profitTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profit .changeVal.bg{
	background-color:{$informerColors['profitBackgroundColor']} !important;
}






.FTinformersWrapper.FT{$hash} .informer_table_small tbody .col-data time, .FTinformersWrapper.FT{$hash} .informer_table_small tbody .col-data time span{
	color: {$informerColors['dataTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small tbody tr.col-data,
.FTinformersWrapper.FT{$hash} .informer_table_small tbody tr.col-data td{
	background-color: {$informerColors['dataBackgroundColor']} !important;
}






.FTinformersWrapper.FT{$hash} .loss .changeVal{
	color:{$informerColors['lossTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .loss .changeVal.bg{
	background-color:{$informerColors['lossBackgroundColor']} !important;
}
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 tbody td, 
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 tfoot td, 
.FTinformersWrapper.FT{$hash} .informer_table_small.silver_dark2 thead th{
	border-color: {$informerColors['borderTdColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small thead tr th{
	border-top-color: {$informerColors['tableBorderColor']} !important;
	border-left-color: {$informerColors['tableBorderColor']} !important;
	border-right-color: {$informerColors['tableBorderColor']} !important;
}
.FTinformersWrapper.FT{$hash} .informer_table_small tbody tr td{
	border-left-color: {$informerColors['tableBorderColor']} !important;
	border-right-color: {$informerColors['tableBorderColor']} !important;
	background-color: {$informerColors['trBackgroundColor']} !important;
}
.FTinformersWrapper.FT{$hash} .informer_table_small tfoot tr td{
	border-left-color: {$informerColors['tableBorderColor']} !important;
	border-right-color: {$informerColors['tableBorderColor']} !important;
	border-bottom-color: {$informerColors['tableBorderColor']} !important;
	background-color: {$informerColors['informerLinkBackgroundColor']} !important;
}
.FTinformersWrapper.FT{$hash} .instal_link{
	color: {$informerColors['informerLinkTextColor']};
}

.FTinformersWrapper.FT{$hash} .informer_table_small th, 
.FTinformersWrapper.FT{$hash} .informer_table_small td{
	border-width: {$model->borderWidth}px !important;
}
$lastTrBorderColor

.FTinformersWrapper.FT{$hash} .lossTextColor{
	color: {$informerColors['lossTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .lossTextColor.bg{
	background-color: {$informerColors['lossBackgroundColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profitTextColor{
	color: {$informerColors['profitTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profitTextColor.bg{
	background-color: {$informerColors['profitBackgroundColor']} !important;
}

";