<?php
echo '@import "' . Yii::app()->params['wpThemeUrl'] . '/css/informersHtml.css' . '";';

$m5 = ceil( 5 * $mult );
$m8 = ceil( 8 * $mult );
$m9 = ceil( 9 * $mult );
$m11 = ceil( 11 * $mult );
$m12 = ceil( 12 * $mult );
$m13 = ceil( 13 * $mult );
$m14 = ceil( 14 * $mult );
$m16 = ceil( 16 * $mult );
$m17 = ceil( 17 * $mult );
$m18 = ceil( 18 * $mult );
$m22 = ceil( 22 * $mult );
$m36 = ceil( 36 * $mult );
$m48 = ceil( 48 * $mult );
if( $mult < 0.8 )
	$m55 = ceil( 55 * ($mult/2) );
else
	$m55 = ceil( 55 * $mult );
	
	
	
$lastTrBorderColor = '';
if( !$showGetBtn ){
	$lastTrBorderColor = ".FTinformersWrapper.FT{$hash} .informer_table_middle tr:last-of-type td{border-bottom-color:{$informerColors['tableBorderColor']} !important;}";
}	

$showDiffCss = '';
if( !$showDiff ){
	$showDiffCss = ".FTinformersWrapper.FT{$hash} .informer_table_middle tbody tr td .amount_middle .value{line-height:{$m36}px;}";
}
	
echo "

.FTinformersWrapper.FT{$hash} table{max-width: {$width};}

.FTinformersWrapper.FT{$hash} .informer_table_marquee thead th,
.FTinformersWrapper.FT{$hash} .informer_table_small thead th,
.FTinformersWrapper.FT{$hash} .informer_table_middle thead th,
.FTinformersWrapper.FT{$hash} .informer_table thead th{
	font-size: {$m12}px;
	padding: {$m14}px {$m5}px {$m13}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.tt_upr thead th{
	font-size: {$m14}px;
	text-transform:none;
}
.FTinformersWrapper.FT{$hash}  {
	line-height:{$m22}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_marquee th,
.FTinformersWrapper.FT{$hash} .informer_table_marquee td,
.FTinformersWrapper.FT{$hash} .informer_table_small th,
.FTinformersWrapper.FT{$hash} .informer_table_small td,
.FTinformersWrapper.FT{$hash} .informer_table_middle th,
.FTinformersWrapper.FT{$hash} .informer_table_middle td,
.FTinformersWrapper.FT{$hash} .informer_table th,
.FTinformersWrapper.FT{$hash} .informer_table td{
	padding: {$m9}px {$m5}px {$m9}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_middle tbody td{
	font-size: {$m18}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_marquee .amount_middle,
.FTinformersWrapper.FT{$hash} .informer_table_small .amount_middle,
.FTinformersWrapper.FT{$hash} .informer_table_middle .amount_middle,
.FTinformersWrapper.FT{$hash} .informer_table .amount_middle{
	font-size: {$m17}px;
}
.FTinformersWrapper.FT{$hash} .angle_l_bottom,
.FTinformersWrapper.FT{$hash} .angle_l_top{
	font-size: {$m13}px;
	line-height: {$m13}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_middle .instal_link{
	font-size: {$m12}px;
}
.FTinformersWrapper.FT{$hash} .informer_table tfoot td,
.FTinformersWrapper.FT{$hash} .informer_table_middle tfoot td,
.FTinformersWrapper.FT{$hash} .informer_table_marquee tfoot td{
	padding: {$m11}px {$m5}px;
}




.FTinformersWrapper.FT{$hash} .informer_table_middle thead th, 
.FTinformersWrapper.FT{$hash} .informer_table_middle thead th time, 
.FTinformersWrapper.FT{$hash} .informer_table_middle thead th a{
	color: {$informerColors['titleTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle thead{
	background-color: {$informerColors['titleBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle tbody tr td .amount_middle{
	color: {$informerColors['tableTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle td .itemImgWrap{
	color: {$informerColors['symbolTextColor']};
	border-radius: 50%;
}
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd td, 
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd th{
	border:2px solid {$informerColors['borderTdColor']};
	border-right-color: transparent;
	border-left-color: transparent;
}
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: transparent;
}
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: transparent;
}
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd tfoot tr:last-child td{
	border-bottom-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd thead tr:first-child th{
	border-top-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd td:first-child, 
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd th:first-child{
	border-left-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd td:last-child, 
.FTinformersWrapper.FT{$hash} .informer_table_middle.white.bold_bd th:last-child{
	border-right-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle tbody tr{
	background-color: {$informerColors['trBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle .loss .changeVal{
	color: {$informerColors['lossTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle .loss .changeVal.bg{
	background-color: {$informerColors['lossBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle .profit .changeVal{
	color: {$informerColors['profitTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle .profit .changeVal.bg{
	background-color: {$informerColors['profitBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid {$informerColors['lossTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid {$informerColors['profitTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle td .itemImgWrap{
	background-color: {$informerColors['itemImgBgColor']};
	color: {$informerColors['symbolTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle .instal_link{
	color: {$informerColors['informerLinkTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_middle.white tfoot td{
	background-color: {$informerColors['informerLinkBackgroundColor']};
}
$lastTrBorderColor









.FTinformersWrapper.FT{$hash} .informer_table_middle td .itemImgWrap{
	background-color: {$informerColors['itemImgBgColor']};
	color: {$informerColors['symbolTextColor']};
	font-size: {$m12}px; 
}
.FTinformersWrapper.FT{$hash} .informer_table_middle td .itemImgWrap{
	background-color: {$informerColors['itemImgBgColor']};
	color: {$informerColors['symbolTextColor']};
	height: {$m36}px;
	min-width: {$m36}px;
	line-height: {$m36}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_middle tbody td a{
	line-height: {$m36}px;
}
{$showDiffCss}
.FTinformersWrapper.FT{$hash} .informer_table_middle td .itemImgWrap .symbolCode{
	color: {$informerColors['itemImgTextColor']};
	font-size:{$m18}px;
}


.FTinformersWrapper.FT{$hash} .informer_table_middle th, 
.FTinformersWrapper.FT{$hash} .informer_table_middle td{
	border-width:{$model->borderWidth}px !important;
}



.FTinformersWrapper.FT{$hash} .lossTextColor{
	color: {$informerColors['lossTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .lossTextColor.bg{
	background-color: {$informerColors['lossBackgroundColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profitTextColor{
	color: {$informerColors['profitTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profitTextColor.bg{
	background-color: {$informerColors['profitBackgroundColor']} !important;
}

";