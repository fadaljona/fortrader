<?php
    $NSi18n = $this->getNSi18n();
    $idInformer = InformersToParamsModel::getInformerId( $_GET );
    $rand = rand();

    ob_start();

?>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->createAbsoluteUrl( 'informers/css', array( 'id' => $idInformer ) )?>">

<div class='FTinformersWrapper FT<?=$hash?> FT<?=$rand?>'><script>document.querySelector(".FT<?=$hash?>.FT<?=$rand?>").style.opacity=0;</script>

    <div class="informer_box informer_box_example clearfix alignCenter <?=$ins?>">
        <div class="inlineBlock">

        <?php $colspanCount = count($columns) + 1;?>
    <?php
        $informerHeader = $category->informerTitle;
        if( $category->linkForHeader ) $informerHeader = CHtml::link( $informerHeader, $category->linkForHeader, array('target' => '_blank') );
    ?>
    <table class="informer_table white tt_upr">
        <?php if(!$hideHeader) {?>
        <thead>
            <th colspan="<?=$colspanCount?>"><?=$informerHeader?></th>
        </thead>
        <?php }?>
        <tbody>
            <tr class="col1">
                <th><?=$category->toolTitle?></th>
                <?php
                    foreach( $columns as $column ){
                        echo CHtml::tag('th', array( ), $category->getColumnName($column) );
                    }
                ?>
            </tr>
            <?php
                $i = 0;
                $itemsCount = count($items);
                foreach( $items as $item ){
                    $lossProfitClass = '';
                    /*if( $item->informerDiff < 0 ) $lossProfitClass = 'loss';
                    if( $item->informerDiff > 0 ) $lossProfitClass = 'profit';*/
                    
                    $colClass = 'col2';
                    if( $i == $itemsCount-1 ) $colClass = 'col3';
                    $i++;
                    
                    echo CHtml::openTag('tr', array( 'class' => $colClass . ' trClass ' . $lossProfitClass, 'data-symbol' => $item->name, 'data-id' => $item->id ));
                    
                        $faSymbolBox = '';
                        if( $item->faSymbol ){
                            $faSymbolBox = '<span class="itemImgWrap">' . $item->faSymbol . '</span>';
                        }
                        
                        $tooltip = '';
                        if( $item->informerTooltip ){
                            $tooltip = $item->informerTooltip;
                        }
                        
                        echo CHtml::tag( 'td', array( 'class' => 'amount_big ' ), $faSymbolBox . CHtml::link($item->informerShortItemName, $item->absoluteSingleURL, array( 'class' => 'symbolContainer', 'target' => '_blank', 'title' => $tooltip ))  );
                    
                        
                        foreach( $columns as $column ){
                            
                            if( $defaultColumn == $column ){
                                $changeValStr = '';
                                if( $showDiff ) $changeValStr = CHtml::tag( 'span', array( 'class' => 'angle_l_bottom changeVal' ), number_format($item->informerDiff, CommonLib::getDecimalPlaces( $item->informerDiff, 4 ), '.', '') );
                                echo CHtml::tag(
                                    'td', array( 'class' => '', 'data-column' => $column ), 
                                    CHtml::tag( 'span', array(), 
                                        CHtml::tag( 'span', array( 'class' => 'value' ), $item->{$column}  ) . 
                                        $changeValStr
                                    )
                                );
                            }else{
                                echo CHtml::tag('td', array( 'data-column' => $column ), '' );
                            }
                            
                            
                        }
                        
                    echo CHtml::closeTag('tr');

                }
            ?>
        </tbody>
        <?php if( $showGetBtn ){?>
        <tfoot>
            <tr>
                <td colspan="<?=$colspanCount?>">
                    <a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
                </td>
            </tr>
        </tfoot>
        <?php }?>
    </table>
        </div>
    </div>
</div>
<script async src="<?=Yii::app()->createAbsoluteUrl( 'informers/js', array( 'id' => $idInformer, 'm' => $rand ) );?>" type="text/javascript"></script>
<?php
    $content = ob_get_clean();
    $content = str_replace(array("\n", "\r"), '', $content);
    echo preg_replace('~>\s+<~', '><', $content);
?>