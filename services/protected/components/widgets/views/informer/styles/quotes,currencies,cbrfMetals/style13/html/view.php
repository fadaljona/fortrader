<?php
    $NSi18n = $this->getNSi18n();
    $idInformer = InformersToParamsModel::getInformerId( $_GET );
    $rand = rand();

    ob_start();

?>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->createAbsoluteUrl( 'informers/css', array( 'id' => $idInformer ) )?>">

<div class='FTinformersWrapper FT<?=$hash?> FT<?=$rand?>'><script>document.querySelector(".FT<?=$hash?>.FT<?=$rand?>").style.opacity=0;</script>

    <div class="informer_box informer_box_example clearfix alignCenter <?=$ins?>">
        <div class="inlineBlock">

        <?php
        $informerHeader = $category->informerTitle;
        if( $category->type == 'currencies' ) $informerHeader .= ' ' . $category->getColumnName('todayCourse');
        if( $category->linkForHeader ) $informerHeader = CHtml::link( $informerHeader, $category->linkForHeader, array('target' => '_blank') );
    ?>
    <table class="informer_table_marquee blue">
        <?php if(!$hideHeader) {?>
        <thead>
            <th colspan="2"><?=$informerHeader?> <?php if (!$hideDate) {?><time class='ftDateTime' datetime="<?=date('Y-m-dTH:i')?>"><span class='ftDateTimeStr'>---</span> <?=Yii::t($NSi18n, 'mskZone')?></time><?php }?></th>
        </thead>
        <?php }?>
        <tbody>
            <tr class="col1">
                <td colspan="2">
                    <marquee behavior="scroll" direction="left"*>
                    <?php
                        $itemsCount = count($items);

                        foreach( $items as $item ){
                            $lossProfitClass = '';
                            /*if( $item->informerDiff < 0 ) $lossProfitClass = 'loss';
                            if( $item->informerDiff > 0 ) $lossProfitClass = 'profit';*/

                            
                            echo CHtml::openTag( 'span', array( 'class' => $lossProfitClass . ' trClass ', 'data-symbol' => $item->name, 'data-id' => $item->id ) );
                                echo CHtml::openTag( 'span', array( 'data-column' => $columns[0] ) );
                                    
                                    echo CHtml::openTag('span', array( 'class' => "amount_middle" ) );
                                        echo CHtml::tag( 'span', array( 'class' => "symbol" ), CHtml::link($item->informerShortItemName, $item->absoluteSingleURL, array( 'target' => '_blank' )) );
                                        echo ' ';
                                        echo '&nbsp;' . CHtml::tag( 'span', array( 'class' => 'value' ), '' );
                                    echo CHtml::closeTag('span');
                                    
                                    if( $showDiff ) echo CHtml::tag( 'span', array( 'class' => "angle_l_bottom changeVal" ), '' );

                                echo CHtml::closeTag( 'span' );
                            echo CHtml::closeTag( 'span' );
                        }
                    ?>	
                    </marquee>
                </td>
            </tr>
        </tbody>
        <?php if( $showGetBtn ){?>
        <tfoot>
            <tr>
                <td colspan="2">
                    <a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
                </td>
            </tr>
        </tfoot>
        <?php }?>
    </table>
        </div>
    </div>
</div>
<script async src="<?=Yii::app()->createAbsoluteUrl( 'informers/js', array( 'id' => $idInformer, 'm' => $rand ) );?>" type="text/javascript"></script>
<?php
    $content = ob_get_clean();
    $content = str_replace(array("\n", "\r"), '', $content);
    echo preg_replace('~>\s+<~', '><', $content);
?>