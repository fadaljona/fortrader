<?php
$m5 = ceil( 5 * $mult );
$m6 = ceil( 6 * $mult );
$m8 = ceil( 8 * $mult );
$m9 = ceil( 9 * $mult );
$m10 = ceil( 10 * $mult );
$m11 = ceil( 11 * $mult );
$m12 = ceil( 12 * $mult );
$m13 = ceil( 13 * $mult );
$m14 = ceil( 14 * $mult );
$m15 = ceil( 15 * $mult );
$m16 = ceil( 16 * $mult );
$m17 = ceil( 17 * $mult );
$m18 = ceil( 18 * $mult );
$m19 = ceil( 19 * $mult );
$m22 = ceil( 22 * $mult );
$m36 = ceil( 36 * $mult );
$m48 = ceil( 48 * $mult );
if( $mult < 0.8 )
	$m55 = ceil( 55 * ($mult/2) );
else
	$m55 = ceil( 55 * $mult );


$lastTrBorderColor = '';
if( !$showGetBtn ){
	$lastTrBorderColor = "table tr:last-of-type td{border-bottom-color:{$informerColors['tableBorderColor']} !important;}";
}

Yii::app()->clientScript->registerCss('informerCss', "

table{max-width: {$width};}

body {
	line-height:{$m22}px;
}
.informer_table_marquee.blue td,
.informer_table_marquee.blue th{
	padding: {$m10}px {$m5}px {$m9}px;
}
.informer_table_marquee thead th,
.informer_table_small thead th,
.informer_table_middle thead th,
.informer_table thead th{
	font-size: {$m12}px;
	padding: {$m14}px {$m5}px {$m13}px;
}
.informer_table_marquee.blue tbody td{
	padding: {$m15}px {$m5}px {$m14}px;
}
.informer_table_marquee.blue tfoot td{
	padding: {$m8}px {$m5}px {$m8}px;
}
.informer_table_marquee.blue .instal_link{
	font-size: {$m12}px;
}
.informer_table_marquee .instal_link:before,
.informer_table .instal_link:before{
	font: normal normal normal {$m18}px/1 FontAwesome;
	left: -{$m30}px;
}
.informer_table_marquee.blue tbody span, .informer_table_marquee.blue tbody span a{
	font-size: {$m14}px;
}
.informer_table_marquee.blue .amount_middle, .informer_table_marquee.blue .amount_middle a{
	font-size: {$m18}px;
}
.informer_table_marquee.blue .amount_middle.blue, .informer_table_marquee.blue .amount_middle.blue a{
	font-size: {$m19}px;
}





	  
.informer_table_marquee.blue thead th{
	background-color: {$informerColors['titleBackgroundColor']};
}
.informer_table_marquee.blue thead time,
.informer_table_marquee.blue thead th,
.informer_table_marquee.blue thead th a{
	color: {$informerColors['titleTextColor']};
}
.informer_table_marquee .symbol, .informer_table_marquee .symbol a{
	color: {$informerColors['symbolTextColor']} !important;
}
.informer_table_marquee .value{
	color: {$informerColors['tableTextColor']} !important;
}
.profit .changeVal{
	color: {$informerColors['profitTextColor']} !important;
}
.profit .changeVal.bg{
	background-color: {$informerColors['profitBackgroundColor']} !important;
}
.loss .changeVal{
	color: {$informerColors['lossTextColor']} !important;
}
.loss .changeVal.bg{
	background-color: {$informerColors['lossBackgroundColor']} !important;
}
.informer_table_marquee tbody td{
	border-bottom-color: {$informerColors['borderTdColor']} !important;
}

.informer_table_marquee thead tr th{
	border-color: {$informerColors['tableBorderColor']} !important;
}
.informer_table_marquee thead tr th{
	border-bottom-color: {$informerColors['borderTdColor']} !important;
}
.informer_table_marquee tbody tr td{
	border-left-color: {$informerColors['tableBorderColor']} !important;
	border-right-color: {$informerColors['tableBorderColor']} !important;
}
.informer_table_marquee tfoot tr td{
	border-bottom-color: {$informerColors['tableBorderColor']} !important;
	border-left-color: {$informerColors['tableBorderColor']} !important;
	border-right-color: {$informerColors['tableBorderColor']} !important;
}
.informer_table_marquee tbody td{
	background-color: {$informerColors['trBackgroundColor']} !important;
}
.informer_table_marquee tfoot tr td{
	background-color: {$informerColors['informerLinkBackgroundColor']} !important;
}
.instal_link{
	color: {$informerColors['informerLinkTextColor']} !important;
}
$lastTrBorderColor	  






.informer_table_marquee th, .informer_table_marquee td{
	border-width: {$model->borderWidth}px !important;
}

.lossTextColor{
	color: {$informerColors['lossTextColor']} !important;
}
.lossTextColor.bg{
	background-color: {$informerColors['lossBackgroundColor']} !important;
}
.profitTextColor{
	color: {$informerColors['profitTextColor']} !important;
}
.profitTextColor.bg{
	background-color: {$informerColors['profitBackgroundColor']} !important;
}

");