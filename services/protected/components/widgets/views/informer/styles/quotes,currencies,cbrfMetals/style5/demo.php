<table class="informer_table white wwwhite">
	<thead>
		<th colspan="2"><?=Yii::t($NSi18n, 'The ruble exchange rates')?> <time datetime="<?=date('Y-m-dTH:i')?>"><?=date('d.m.Y H:i')?> <?=Yii::t($NSi18n, 'mskZone')?></time></th>
	</thead>
	<tbody>
		<tr class="col1">
			<th><?=Yii::t('informerColumnNames', 'Symbol')?></th>
			<th><?=Yii::t('informerColumnNames', 'Ask')?></th>
		</tr>
		<tr class="col2 loss">
			<td class="amount">
				<span class="itemImgWrap">
					<i class="fa fa-euro"></i>
				</span>
				<span class="symbolContainer"><?=Yii::t($NSi18n, 'EUR')?></span>
			</td>
			<td class="arrow changeVal bg">70.585</td>
		</tr>
		<tr class="col3 profit">
			<td class="amount">
				<span class="itemImgWrap">
					<i class="fa fa-dollar"></i>
				</span>
				<span class="symbolContainer"><?=Yii::t($NSi18n, 'USD')?></span>
			</td>
			<td class="arrow changeVal">63.847</td>
		</tr>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="<?=$colspanCount?>">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>