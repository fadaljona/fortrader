<table class="informer_table_marquee blue">
	<thead>
		<th colspan="2"><?=Yii::t($NSi18n, 'The ruble exchange rates')?> <time datetime="<?=date('Y-m-dTH:i')?>"><?=date('d.m.Y H:i')?> <?=Yii::t($NSi18n, 'mskZone')?></time></th>
	</thead>
	<tbody>
		<tr class="col1">
			<td colspan="2">
				<marquee behavior="scroll" direction="left"*>
				
					<span class="profit">
						<span class="amount_middle blue symbol">USD</span>
						<span class="changeVal bg">+100</span>
						<span class="amount_middle value">64.84</span>
					</span>
					
					<span class="loss">
						<span class="amount_middle blue symbol">EUR</span>
						<span class="changeVal">-100</span>
						<span class="amount_middle value">70.78</span>
					</span>
					
				</marquee>
			</td>
		</tr>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="2">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>