<table class="informer_table white tt_upr">
	<thead>
		<th colspan="2"><?=Yii::t($NSi18n, 'The ruble exchange rates')?></th>
	</thead>
	<tbody>
		<tr class="col1">
			<th><?=Yii::t('informerColumnNames', 'Symbol')?></th>
			<th><?=Yii::t('informerColumnNames', 'Ask')?></th>
		</tr>
		<tr class="col2 loss">
			<td class="amount_big">
				<span class="itemImgWrap"><i class="fa fa-dollar"></i></span>
				<span class="symbolContainer"><?=Yii::t($NSi18n, 'USD')?></span>
			</td>
			<td>
				<span class="count_red">
					64.254 <span class="angle_l_bottom changeVal bg">0.0064</span>
				</span>
			</td>			
		</tr>
		<tr class="col3 profit">
			<td class="amount_big">
				<span class="itemImgWrap"><i class="fa fa-euro"></i></span>
				<span class="symbolContainer"><?=Yii::t($NSi18n, 'EUR')?></span>
			</td>
			<td>
				<span class="count_green">
					70.567 <span class="angle_l_bottom changeVal">0.4060</span>
				</span>
			</td>
		</tr>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="<?=$colspanCount?>">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>