<?php $colspanCount = count($items) ;?>
<?php
	$informerHeader = $category->informerTitle;
	if( $category->type == 'currencies' ) $informerHeader .= ' ' . $category->getColumnName('todayCourse');
	if( $category->linkForHeader ) $informerHeader = CHtml::link( $informerHeader, $category->linkForHeader, array('target' => '_blank') );
?>
<table class="informer_table_middle white tt_upr bold_bd">
    <?php if(!$hideHeader) {?>
	<thead>
        <th colspan="<?=$colspanCount?>"><?=$informerHeader?> <?php if (!$hideDate) {?><time datetime="<?=date('Y-m-d')?>"><?=date('d.m.Y')?></time><?php }?></th>
    </thead>
    <?php }?>
	<tbody>
		<tr class="col1">	
		<?php
			foreach( $items as $item ){
				$lossProfitClass = '';
				if( $item->informerDiff < 0 ) $lossProfitClass = 'loss';
				if( $item->informerDiff > 0 ) $lossProfitClass = 'profit';
				
				echo CHtml::openTag( 'td', array( 'class' => $lossProfitClass . ' trClass ', 'data-symbol' => $item->name ) );
				
					$faSymbolBox = '';
					if( $item->faSymbol ){
						$faSymbolBox = '<span class="itemImgWrap">' . $item->faSymbol . '</span>';
					}else{
						$faSymbolBox = '<span class="itemImgWrap">' . $item->informerShortItemName . '</span>';
					}
					
					$tooltip = '';
					if( $item->informerTooltip ){
						$tooltip = $item->informerTooltip;
					}
					
					echo CHtml::link($faSymbolBox, $item->absoluteSingleURL, array('target' => '_blank', 'class' => 'symbolContainer', 'title' => $tooltip ));
					
					echo CHtml::openTag( 'span', array( 'class' => 'amount_middle', 'data-column' => $columns[0] ) );
						echo CHtml::tag( 'span', array( 'class' => 'value' ), $item->{$columns[0]} );
						if( $showDiff ) echo CHtml::tag( 'span', array( 'class' => 'angle_l_bottom changeVal' ), number_format($item->informerDiff, CommonLib::getDecimalPlaces( $item->informerDiff, 4 ), '.', '')  );
					echo CHtml::closeTag( 'span' );
				
				echo CHtml::closeTag( 'td' );
			}
		?>
		</tr>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="<?=$colspanCount?>">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>