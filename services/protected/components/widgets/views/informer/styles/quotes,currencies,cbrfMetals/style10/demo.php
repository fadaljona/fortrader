<table class="informer_table_small default">
	<thead>
		<th colspan="2"><?=Yii::t($NSi18n, 'The ruble exchange rates')?> <time datetime="<?=date('Y-m-d')?>"><?=date('d.m.Y')?></time></th>
	</thead>
	<tbody>
		<tr class="profit">
			<td colspan="2">
				<span class="amount_middle">
					<span class="style10ImgWrap">
						<img src="/uploads/country/flags/shiny/16/EU.png" alt="">
					</span>
					<span class="symbol symbolContainer">EUR</span> <span class="value">70.754</span>
				</span>
				<span class="angle_l_bottom changeVal">0.4060</span>
			</td>
		</tr>
		<tr class="loss">
			<td colspan="2">
				<span class="amount_middle">
					<span class="style10ImgWrap">
						<img src="/uploads/country/flags/shiny/16/US.png" alt="">
					</span>
					<span class="symbol symbolContainer">USD</span> <span class="value">64.254</span>
				</span>
				<span class="angle_l_bottom changeVal bg">0.0064</span>
			</td>
		</tr>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="2">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>