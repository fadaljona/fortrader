<?php $colspanCount = count($columns) + 1;?>
<?php
	$informerHeader = $category->informerTitle;
	if( $category->linkForHeader ) $informerHeader = CHtml::link( $informerHeader, $category->linkForHeader, array('target' => '_blank') );
?>
<table class="informer_table bold_bd">
    <?php if(!$hideHeader) {?>
	<thead>
		<th colspan="<?=$colspanCount?>"><?=$informerHeader?></th>
    </thead>
    <?php }?>
	<tbody>
		<tr class="col1">
			<th><?=$category->toolTitle?></th>
			<?php
				foreach( $columns as $column ){
					echo CHtml::tag('th', array( ), $category->getColumnName($column) );
				}
			?>
		</tr>
		<?php
			$i = 0;
			$itemsCount = count($items);
			foreach( $items as $item ){
				$lossProfitClass = '';
				if( $item->informerDiff < 0 ) $lossProfitClass = 'loss';
				if( $item->informerDiff > 0 ) $lossProfitClass = 'profit';
				
				$colClass = 'col2';
				if( $i == $itemsCount-1 ) $colClass = 'col3';
				$i++;
				
				echo CHtml::openTag('tr', array( 'class' => $colClass . ' trClass ' . $lossProfitClass, 'data-symbol' => $item->name ));
				
					$imgFlag = '';
					if( $item->flagSrc ){
						$imgFlag = '<span class="style2ImgWrap"><img src="'.$item->flagSrc.'" alt=""></span>';
					}
					
					$tooltip = '';
					if( $item->informerTooltip ){
						$tooltip = $item->informerTooltip;
					}
					
					echo CHtml::tag( 'td', array( 'class' => 'amount arrow' ), $imgFlag . CHtml::link($item->informerShortItemName, $item->absoluteSingleURL, array( 'class' => 'symbolContainer', 'target' => '_blank', 'title' => $tooltip ) )  );
				
					
					foreach( $columns as $column ){
						
						$changeValClass = '';
						if( $defaultColumn == $column ) $changeValClass = 'changeVal';
						
						echo CHtml::tag('td', array( 'class' => $changeValClass, 'data-column' => $column ), $item->{$column} );
					}
					
				echo CHtml::closeTag('tr');

			}
        ?>
        <?php if (!$hideDate) {?>
		<tr class="col-data">
			<td colspan="<?=$colspanCount?>">
				<time datetime="<?=date('Y-m-dTH:i')?>"><?=Yii::t($NSi18n, 'Data time')?>  <?=date('d.m.Y H:i')?> <?=Yii::t($NSi18n, 'mskZone')?></time>
			</td>
        </tr>
        <?php }?>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="<?=$colspanCount?>">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>