<?php
$m5 = ceil( 5 * $mult );
$m9 = ceil( 9 * $mult );
$m10 = ceil( 10 * $mult );
$m11 = ceil( 11 * $mult );
$m12 = ceil( 12 * $mult );
$m13 = ceil( 13 * $mult );
$m14 = ceil( 14 * $mult );
$m16 = ceil( 16 * $mult );
$m18 = ceil( 18 * $mult );
$m22 = ceil( 22 * $mult );
if( $mult < 0.8 )
	$m55 = ceil( 55 * ($mult/2) );
else
	$m55 = ceil( 55 * $mult );

$lastTrBorderColor = '';
if( !$showGetBtn ){
	$lastTrBorderColor = ".informer_table tr:last-of-type td{border-bottom-color:{$informerColors['tableBorderColor']};}";
}
	
Yii::app()->clientScript->registerCss('informerCss', "

table{max-width: {$width};}

.informer_table_marquee thead th,
.informer_table_small thead th,
.informer_table_middle thead th,
.informer_table thead th{
	font-size: {$m12}px;
	padding: {$m14}px {$m5}px {$m13}px;
}
.informer_table_marquee th,.informer_table_marquee td,
.informer_table_small th,.informer_table_small td,
.informer_table_middle th,.informer_table_middle td,
.informer_table th,.informer_table td{
	padding: {$m9}px {$m5}px {$m9}px;
}
.informer_table{
	font-size: {$m13}px;
}
.loss .arrow:after{
	font: normal normal normal {$m16}px/1 FontAwesome !important;
}
.profit .arrow:after{
	font: normal normal normal {$m16}px/1 FontAwesome !important;
}
.informer_table [class*='icon_amount']{
	padding-left: {$m10}px;
}
.informer_table tfoot td,.informer_table_middle tfoot td,.informer_table_marquee tfoot td{
	padding: {$m11}px {$m5}px;
}
.informer_table_marquee .instal_link:before,
.informer_table .instal_link:before{
	font: normal normal normal {$m18}px/1 FontAwesome;
}
body {
	line-height:{$m22}px;
}




.informer_table thead th, .informer_table thead th time, .informer_table thead th a{
	color: {$informerColors['titleTextColor']};
}
.informer_table thead{
	background-color: {$informerColors['titleBackgroundColor']};
}
.informer_table tbody th{
	color: {$informerColors['thTextColor']};
}
.informer_table tbody th{
	background-color: {$informerColors['thBackgroundColor']};
}
.informer_table tbody tr td{
	color: {$informerColors['tableTextColor']};
}
.informer_table tbody tr td:nth-child(1), .informer_table tbody tr td:nth-child(1) a{
	color: {$informerColors['symbolTextColor']};
}
.informer_table td, .informer_table th{
	border:1px solid {$informerColors['borderTdColor']};
}
.informer_table tbody [class*='col'] th:not(:last-child),
.informer_table tbody [class*='col'] td:not(:last-child){
	border-left-color: {$informerColors['tableBorderColor']};
}
.informer_table tbody [class*='col'] th:last-child,
.informer_table tbody [class*='col'] td:last-child{
	border-right-color: {$informerColors['tableBorderColor']};
}
.informer_table thead th{
	border-top-color: {$informerColors['tableBorderColor']};;
	border-left-color: {$informerColors['tableBorderColor']};;
	border-right-color: {$informerColors['tableBorderColor']};;
}
.informer_table tbody .col-data td{
	border-left-color: {$informerColors['tableBorderColor']};
	border-right-color: {$informerColors['tableBorderColor']};
}
.informer_table tfoot td{
	border:1px solid {$informerColors['tableBorderColor']};
}
.informer_table tbody tr:nth-child(2n+2){
	background-color: {$informerColors['oddBackgroundTrColor']};
}
.informer_table tbody tr:nth-child(2n+3){
	background-color: {$informerColors['evenBackgroundTrColor']};
}
.informer_table .loss .arrow:after{
	color: {$informerColors['lossTextColor']};
}
.informer_table .loss .changeVal{
	color: {$informerColors['lossTextColor']};
}
.informer_table .loss .changeVal.bg{
	background-color: {$informerColors['lossBackgroundColor']};
}
.informer_table .profit .arrow:after{
	color: {$informerColors['profitTextColor']};
}
.informer_table .profit .changeVal{
	color: {$informerColors['profitTextColor']};
}
.informer_table .profit .changeVal.bg{
	background-color: {$informerColors['profitBackgroundColor']};
}
.informer_table_marquee .instal_link,
.informer_table .instal_link{
	color: {$informerColors['informerLinkTextColor']};
}
.informer_table tfoot td{
	background-color: {$informerColors['informerLinkBackgroundColor']};
}
.informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.informer_table.bold_bd th, .informer_table.bold_bd td{
	border-width:1px;
}

.informer_table th, .informer_table td{
	border-width:{$model->borderWidth}px !important;
}
$lastTrBorderColor


.lossTextColor{
	color: {$informerColors['lossTextColor']} !important;
}
.lossTextColor.bg{
	background-color: {$informerColors['lossBackgroundColor']} !important;
}
.profitTextColor{
	color: {$informerColors['profitTextColor']} !important;
}
.profitTextColor.bg{
	background-color: {$informerColors['profitBackgroundColor']} !important;
}

");