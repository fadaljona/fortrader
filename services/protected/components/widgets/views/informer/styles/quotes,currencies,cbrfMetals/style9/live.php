<?php
	$informerHeader = $category->informerTitle;
	if( $category->type == 'currencies' ) $informerHeader .= ' ' . $category->getColumnName('todayCourse');
	if( $category->linkForHeader ) $informerHeader = CHtml::link( $informerHeader, $category->linkForHeader, array('target' => '_blank') );
?>
<table class="informer_table_small default">
    <?php if(!$hideHeader) {?>
	<thead>
		<th colspan="2"><?=$informerHeader?></th>
    </thead>
    <?php }?>
	<tbody>
	<?php
		foreach( $items as $item ){
			$lossProfitClass = '';
			if( $item->informerDiff < 0 ) $lossProfitClass = 'loss';
			if( $item->informerDiff > 0 ) $lossProfitClass = 'profit';
			
			echo CHtml::openTag( 'tr', array( 'class' => $lossProfitClass . ' trClass', 'data-symbol' => $item->name ) );
				echo CHtml::openTag( 'td', array( 'colspan' => '2', 'data-column' => $columns[0] ) );
				echo CHtml::openTag( 'span', array( 'class' => 'style9TrWrapper' ) );
					
					$faSymbolBox = '';
					if( $item->faSymbol ){
						$faSymbolBox = '<span class="style9ImgWrap">' . $item->faSymbol . '</span>';
					}
					
					$tooltip = '';
					if( $item->informerTooltip ){
						$tooltip = $item->informerTooltip;
					}
					
					echo CHtml::openTag( 'span', array( 'class' => "amount_middle" ) );
						echo $faSymbolBox;
						echo CHtml::tag( 'span', array( 'class' => "symbol" ), CHtml::link($item->informerShortItemName, $item->absoluteSingleURL, array( 'class' => 'symbolContainer', 'target' => '_blank', 'title' => $tooltip )) ) . ' ' . CHtml::tag( 'span', array( 'class' => "value" ), $item->{$columns[0]} );
					echo CHtml::closeTag( 'span' );
					echo ' ';
					if( $showDiff ) echo CHtml::tag( 'span', array( 'class' => 'angle_l_bottom changeVal' ), number_format($item->informerDiff, CommonLib::getDecimalPlaces( $item->informerDiff, 4 ), '.', '')  );
					
				echo CHtml::closeTag( 'span' );
				echo CHtml::closeTag( 'td' );
			echo CHtml::closeTag( 'tr' );
		}
    ?>
        <?php if (!$hideDate) {?>
		<tr class="col-data">
			<td colspan="2">
				<time datetime="<?=date('Y-m-d')?>"><?=date('d.m.Y')?></time>
			</td>
        </tr>
        <?php }?>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="2">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>