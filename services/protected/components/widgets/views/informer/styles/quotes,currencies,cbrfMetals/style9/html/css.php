<?php
echo '@import "' . Yii::app()->params['wpThemeUrl'] . '/css/informersHtml.css' . '";';

$m5 = ceil( 5 * $mult );
$m8 = ceil( 8 * $mult );
$m9 = ceil( 9 * $mult );
$m10 = ceil( 10 * $mult );
$m11 = ceil( 11 * $mult );
$m12 = ceil( 12 * $mult );
$m13 = ceil( 13 * $mult );
$m14 = ceil( 14 * $mult );
$m16 = ceil( 16 * $mult );
$m17 = ceil( 17 * $mult );
$m18 = ceil( 18 * $mult );
$m22 = ceil( 22 * $mult );
$m36 = ceil( 36 * $mult );
$m48 = ceil( 48 * $mult );
if( $mult < 0.8 )
	$m55 = ceil( 55 * ($mult/2) );
else
	$m55 = ceil( 55 * $mult );


$lastTrBorderColor = '';
if( !$showGetBtn ){
	$lastTrBorderColor = ".FTinformersWrapper.FT{$hash} table tr:last-of-type td{border-bottom-color:{$informerColors['tableBorderColor']} !important;}";
}

echo "

.FTinformersWrapper.FT{$hash} table{max-width: {$width};}

.FTinformersWrapper.FT{$hash} .informer_table_small.default td,
.FTinformersWrapper.FT{$hash} .informer_table_small.default th{
	padding: {$m10}px {$m5}px {$m9}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_marquee thead th,
.FTinformersWrapper.FT{$hash} .informer_table_small thead th,
.FTinformersWrapper.FT{$hash} .informer_table_middle thead th,
.FTinformersWrapper.FT{$hash} .informer_table thead th{
	font-size: {$m12}px;
	padding: {$m14}px {$m5}px {$m13}px;
}
.FTinformersWrapper.FT{$hash} {
	line-height:{$m22}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_small.default tbody td{
	font-size: {$m12}px;
}




.FTinformersWrapper.FT{$hash} .informer_table_small .instal_link{
	font-size: {$m10}px !important;
}
.FTinformersWrapper.FT{$hash} .informer_table_small .amount_middle{
	font-size: {$m13}px !important;
}
.FTinformersWrapper.FT{$hash} .style9ImgWrap{
	margin-right: {$m10}px; 
	width: {$m10}px; 
	height: {$m18}px;
}

.FTinformersWrapper.FT{$hash} .style9ImgWrap .symbolCode{
	line-height:{$m13}px;
	color: {$informerColors['symbolTextColor']};
	font-size:{$m18}px;
}
.FTinformersWrapper.FT{$hash} .style9ImgWrap{
	color: {$informerColors['symbolTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small thead time, 
.FTinformersWrapper.FT{$hash} .informer_table_small thead th, 
.FTinformersWrapper.FT{$hash} .informer_table_small thead th a{
	color: {$informerColors['titleTextColor']};
	background-color: {$informerColors['titleBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small .symbol, 
.FTinformersWrapper.FT{$hash} .informer_table_small .symbol a{
	color: {$informerColors['symbolTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small .value{
	color: {$informerColors['tableTextColor']};
}
.FTinformersWrapper.FT{$hash} .angle_l_bottom,
.FTinformersWrapper.FT{$hash} .angle_l_top{
	font-size: {$m13}px;
	line-height: {$m13}px;
	display:inline-block;
	vertical-align:middle;
}
.FTinformersWrapper.FT{$hash} .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid {$informerColors['lossTextColor']};
}
.FTinformersWrapper.FT{$hash} .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid {$informerColors['profitTextColor']};
}
.FTinformersWrapper.FT{$hash} .loss .changeVal{
	color: {$informerColors['lossTextColor']};
}
.FTinformersWrapper.FT{$hash} .loss .changeVal.bg{
	background-color: {$informerColors['lossBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .profit .changeVal{
	color: {$informerColors['profitTextColor']};
}
.FTinformersWrapper.FT{$hash} .profit .changeVal.bg{
	background-color: {$informerColors['profitBackgroundColor']};
}


.FTinformersWrapper.FT{$hash} .informer_table_small tbody .col-data time{
	color: {$informerColors['dataTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small tbody tr.col-data,
.FTinformersWrapper.FT{$hash} .informer_table_small tbody tr.col-data td{
	background-color: {$informerColors['dataBackgroundColor']} !important;
}


.FTinformersWrapper.FT{$hash} .informer_table_small td, 
.FTinformersWrapper.FT{$hash} .informer_table_small th{
	border-color: {$informerColors['borderTdColor']};
}

.FTinformersWrapper.FT{$hash} .informer_table_small thead tr:first-child th{
	border-top-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small thead tr:first-child th:first-child{
	border-left-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small thead tr:first-child th:last-child{
	border-right-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small tfoot tr:last-child td{
	border-bottom-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small tfoot tr:last-child td:first-child{
	border-left-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small tfoot tr:last-child td:last-child{
	border-right-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small tbody tr td:first-child{
	border-left-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small tbody tr td:last-child{
	border-right-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small tbody tr td{
	background-color: {$informerColors['trBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .instal_link{
	color: {$informerColors['informerLinkTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_small tfoot tr td{
	background-color: {$informerColors['informerLinkBackgroundColor']};
}

.FTinformersWrapper.FT{$hash} .informer_table_small th, 
.FTinformersWrapper.FT{$hash} .informer_table_small td{
	border-width: {$model->borderWidth}px !important;
}
$lastTrBorderColor

.FTinformersWrapper.FT{$hash} .lossTextColor{
	color: {$informerColors['lossTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .lossTextColor.bg{
	background-color: {$informerColors['lossBackgroundColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profitTextColor{
	color: {$informerColors['profitTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profitTextColor.bg{
	background-color: {$informerColors['profitBackgroundColor']} !important;
}

";