<?php
	$informerHeader = $category->informerTitle;
	if( $category->type == 'currencies' ) $informerHeader .= ' ' . $category->getColumnName('todayCourse');
	if( $category->linkForHeader ) $informerHeader = CHtml::link( $informerHeader, $category->linkForHeader, array('target' => '_blank') );
?>
<table class="informer_table_marquee blue">
    <?php if(!$hideHeader) {?>
	<thead>
		<th colspan="2"><?=$informerHeader?> <?php if (!$hideDate) {?><time datetime="<?=date('Y-m-dTH:i')?>"><?=date('d.m.Y H:i')?> <?=Yii::t($NSi18n, 'mskZone')?></time><?php }?></th>
    </thead>
    <?php }?>
	<tbody>
		<tr class="col1">
			<td colspan="2">
				<marquee behavior="scroll" direction="left"*>
				<?php
					$itemsCount = count($items);

					foreach( $items as $item ){
						$lossProfitClass = '';
						if( $item->informerDiff < 0 ) $lossProfitClass = 'loss';
						if( $item->informerDiff > 0 ) $lossProfitClass = 'profit';

						
						echo CHtml::openTag( 'span', array( 'class' => $lossProfitClass . ' trClass ', 'data-symbol' => $item->name ) );
							echo CHtml::openTag( 'span', array( 'data-column' => $columns[0] ) );
								
								
								echo CHtml::tag( 'span', array( 'class' => "symbol amount_middle blue" ), CHtml::link($item->informerShortItemName, $item->absoluteSingleURL, array( 'target' => '_blank' )) );
								if( $showDiff ) echo CHtml::tag( 'span', array( 'class' => "changeVal" ), number_format($item->informerDiff, CommonLib::getDecimalPlaces( $item->informerDiff, 4 ), '.', '') );
								echo CHtml::tag( 'span', array( 'class' => 'amount_middle value' ), $item->{$columns[0]} );
					
								
							echo CHtml::closeTag( 'span' );
						echo CHtml::closeTag( 'span' );
					}
				?>
				</marquee>
			</td>
		</tr>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="2">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>