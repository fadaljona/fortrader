<?php
    $NSi18n = $this->getNSi18n();
    $idInformer = InformersToParamsModel::getInformerId( $_GET );
    $rand = rand();

    ob_start();
?>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->createAbsoluteUrl( 'informers/css', array( 'id' => $idInformer ) )?>">

<div class='FTinformersWrapper FT<?=$hash?> FT<?=$rand?>'><script>document.querySelector(".FT<?=$hash?>.FT<?=$rand?>").style.opacity=0;</script>
    <div class="informer_box informer_box_example clearfix alignCenter">
        <div class="inlineBlock">
        <?php $colspanCount = count($columns) + 1;?>
<?php
	$informerHeader = $category->informerTitle;
	if( $category->linkForHeader ) $informerHeader = CHtml::link( $informerHeader, $category->linkForHeader, array('target' => '_blank') );
?>
            <table class="informer_table informer1">
                <?php if(!$hideHeader) {?>
                <thead>
                    <tr><th colspan="<?=$colspanCount?>"><?=$informerHeader?></th></tr>
                </thead>
                <?php }?>
                <tbody>
                    <tr class="col1">
                        <th class="bd_l"><?=$category->toolTitle?></th>
                        <?php
                            foreach( $columns as $column ){
                                echo CHtml::tag('th', array( 'class' => 'bd_r' ), $category->getColumnName($column) );
                            }
                        ?>
                    </tr>
                    <?php
                        $i = 0;
                        foreach( $items as $item ){
                            $lossProfitClass = '';
              
                            /*if( $item->informerDiff < 0 ) $lossProfitClass = 'loss';
                            if( $item->informerDiff > 0 ) $lossProfitClass = 'profit';*/
                            
                            $colClass = 'col3';
                            if( $i == 0 ) $colClass = 'col2';
                            $i = 1;
                            
                            $tooltip = '';
                            if( $item->informerTooltip ){
                                $tooltip = $item->informerTooltip;
                            }
                            
                            echo CHtml::openTag(
                                'tr', 
                                array( 
                                    'class' => $colClass . ' trClass ' . $lossProfitClass, 
                                    'data-symbol' => $item->name,
                                    'data-id' => $item->id 
                                )
                            );
                                echo CHtml::tag( 'td', array( 'class' => 'icon_amount arrow' ), CHtml::link($item->informerItemName, $item->absoluteSingleURL, array('target' => '_blank', 'class' => 'symbolContainer symbolContainer' . $hash, 'title' => $tooltip ))  );
                            
                                foreach( $columns as $column ){
                                    $changeValClass = '';
                                    if( $defaultColumn == $column ) $changeValClass = 'changeVal';
                                    echo CHtml::tag('td', array( 'class' => $changeValClass, 'data-column' => $column ), '' );
                                }
                                
                            echo CHtml::closeTag('tr');
                        }
                    ?>
                    <?php if (!$hideDate) {?>
                    <tr class="col-data">
                        <td colspan="<?=$colspanCount?>">
                            <time class='ftDateTime' datetime="<?=date('Y-m-dTH:i')?>"><?=Yii::t($NSi18n, 'Data time')?>  <span class='ftDateTimeStr'>---</span> <?=Yii::t($NSi18n, 'mskZone')?></time>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
                <?php if( $showGetBtn ){?>
                <tfoot>
                    <tr>
                        <td colspan="<?=$colspanCount?>">
                            <a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
                        </td>
                    </tr>
                </tfoot>
                <?php }?>
            </table>
        </div>
    </div>
</div>
<script async src="<?=Yii::app()->createAbsoluteUrl( 'informers/js', array( 'id' => $idInformer, 'm' => $rand ) );?>" type="text/javascript"></script>

<?php
    $content = ob_get_clean();
    $content = str_replace(array("\n", "\r"), '', $content);
    echo preg_replace('~>\s+<~', '><', $content);
?>