<?php
	Yii::app()->clientScript->registerCssFile( Yii::app()->params['wpThemeUrl'] .'/informers.css');
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="informer_box informer_box_example clearfix alignCenter <?=$ins?>">
	<div class="inlineBlock">

	<?php 
		require_once( 'css.php' );
		if( $mode == 'demo' ){
			require_once( 'demo.php' );
		}else{ 
			require_once( 'live.php' );
			require_once( dirname(__FILE__) . '/../../../quotesJs/js.php' );
		} 
	?>		
	</div>
</div>