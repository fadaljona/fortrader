<table class="informer_table_middle white tt_upr bold_bd">
	<thead>
		<th colspan="2"><?=Yii::t($NSi18n, 'The ruble exchange rates')?> <time datetime="<?=date('Y-m-d')?>"><?=date('d.m.Y')?></time></th>
	</thead>
	<tbody>
		<tr class="col1">
			<td class="loss">
				<span class="itemImgWrap">
					<i class="fa fa-dollar"></i>
				</span>
				<span class="amount_middle">
					64.254
					<span class="angle_l_bottom changeVal bg">0.0064</span>
				</span>
			</td>
			<td class="profit">
				<span class="itemImgWrap">
					<i class="fa fa-euro"></i>
				</span>
				<span class="amount_middle">
					70.567
					<span class="angle_l_bottom changeVal bg">0.4060</span>
				</span>
			</td>
		</tr>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="<?=$colspanCount?>">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>