<table class="informer_table informer1">
	<thead>
		<th colspan="2"><?=Yii::t($NSi18n, 'The ruble exchange rates')?></th>
	</thead>
	<tbody>
		<tr class="col1">
			<th class="bd_l"><?=Yii::t('informerColumnNames', 'Symbol')?></th>
			<th class="bd_r"><?=Yii::t('informerColumnNames', 'Ask')?></th>
		</tr>
		<tr class="col2 loss">
			<td class="icon_amount arrow"><?=Yii::t($NSi18n, 'Euro')?></td>
			<td class="changeVal bg">70.585</td>
		</tr>
		<tr class="col3 profit">
			<td class="icon_amount arrow"><?=Yii::t($NSi18n, 'Dollar')?></td>
			<td class="changeVal">63.8475</td>
		</tr>
		<tr class="col-data">
			<td colspan="2">
				<time datetime="<?=date('Y-m-dTH:i')?>"><?=Yii::t($NSi18n, 'Data time')?>  <?=date('d.m.Y H:i')?> <?=Yii::t($NSi18n, 'mskZone')?></time>
			</td>
		</tr>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="<?=$colspanCount?>">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>