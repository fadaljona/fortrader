<?php
$m5 = ceil( 5 * $mult );
$m6 = ceil( 6 * $mult );
$m8 = ceil( 8 * $mult );
$m9 = ceil( 9 * $mult );
$m10 = ceil( 10 * $mult );
$m11 = ceil( 11 * $mult );
$m12 = ceil( 12 * $mult );
$m13 = ceil( 13 * $mult );
$m14 = ceil( 14 * $mult );
$m16 = ceil( 16 * $mult );
$m17 = ceil( 17 * $mult );
$m18 = ceil( 18 * $mult );
$m19 = ceil( 19 * $mult );
$m22 = ceil( 22 * $mult );
$m36 = ceil( 36 * $mult );
$m48 = ceil( 48 * $mult );
if( $mult < 0.8 )
	$m55 = ceil( 55 * ($mult/2) );
else
	$m55 = ceil( 55 * $mult );



$lastTrBorderColor = '';
if( !$showGetBtn ){
	$lastTrBorderColor = "table tr:last-of-type td{border-bottom-color:{$informerColors['tableBorderColor']} !important;}";
}

Yii::app()->clientScript->registerCss('informerCss', "

table{max-width: {$width};}

.informer_table_small.default td,
.informer_table_small.default th{
	padding: {$m10}px {$m5}px {$m9}px;
}
.informer_table_marquee thead th,
.informer_table_small thead th,
.informer_table_middle thead th,
.informer_table thead th{
	font-size: {$m12}px;
	padding: {$m14}px {$m5}px {$m13}px;
}
body {
	line-height:{$m22}px;
}
.informer_table_small.silver_dark2 .col1 td{
	padding: {$m11}px {$m5}px {$m6}px;
}
.informer_table_small.silver_dark2 .col2 td{
	padding: {$m6}px {$m5}px {$m11}px;
}
.informer_table tfoot td,.informer_table_middle tfoot td,.informer_table_small tfoot td{
	padding: {$m11}px {$m5}px;
}
.instal_link{
	font-size: {$m11}px !important;
}
.informer_table_small.silver_dark2 tbody span{
	font-size: {$m11}px;
}
.informer_table_small.silver_dark2 .amount_middle{
	font-size: {$m19}px;
}


	  
	  
	  




.informer_table_small.silver_dark2 thead time,
.informer_table_small.silver_dark2 thead th, .informer_table_small.silver_dark2 thead th a{
	color: {$informerColors['titleTextColor']};
}
.informer_table_small.silver_dark2 thead th{
	background: {$informerColors['titleBackgroundColor']};
}
.informer_table_small .symbol, .informer_table_small .symbol a{
	color: {$informerColors['symbolTextColor']} !important;
}
.informer_table_small .value{
	color: {$informerColors['tableTextColor']} !important;
}
.profit .changeVal{
	color:{$informerColors['profitTextColor']} !important;
}
.profit .changeVal.bg{
	background-color:{$informerColors['profitBackgroundColor']} !important;
}





.informer_table_small tbody .col-data time, 
.informer_table_small tbody .col-data time span{
	color: {$informerColors['dataTextColor']};
}
.informer_table_small tbody tr.col-data,
.informer_table_small tbody tr.col-data td{
	background-color: {$informerColors['dataBackgroundColor']} !important;
}










.loss .changeVal{
	color:{$informerColors['lossTextColor']} !important;
}
.loss .changeVal.bg{
	background-color:{$informerColors['lossBackgroundColor']} !important;
}
.informer_table_small.silver_dark2 tbody td, .informer_table_small.silver_dark2 tfoot td, .informer_table_small.silver_dark2 thead th{
	border-color: {$informerColors['borderTdColor']};
}
.informer_table_small thead tr th{
	border-top-color: {$informerColors['tableBorderColor']} !important;
	border-left-color: {$informerColors['tableBorderColor']} !important;
	border-right-color: {$informerColors['tableBorderColor']} !important;
}
.informer_table_small tbody tr td{
	border-left-color: {$informerColors['tableBorderColor']} !important;
	border-right-color: {$informerColors['tableBorderColor']} !important;
	background-color: {$informerColors['trBackgroundColor']} !important;
}
.informer_table_small tfoot tr td{
	border-left-color: {$informerColors['tableBorderColor']} !important;
	border-right-color: {$informerColors['tableBorderColor']} !important;
	border-bottom-color: {$informerColors['tableBorderColor']} !important;
	background-color: {$informerColors['informerLinkBackgroundColor']} !important;
}
.instal_link{
	color: {$informerColors['informerLinkTextColor']};
}

.informer_table_small th, .informer_table_small td{
	border-width: {$model->borderWidth}px !important;
}
$lastTrBorderColor

.lossTextColor{
	color: {$informerColors['lossTextColor']} !important;
}
.lossTextColor.bg{
	background-color: {$informerColors['lossBackgroundColor']} !important;
}
.profitTextColor{
	color: {$informerColors['profitTextColor']} !important;
}
.profitTextColor.bg{
	background-color: {$informerColors['profitBackgroundColor']} !important;
}

");