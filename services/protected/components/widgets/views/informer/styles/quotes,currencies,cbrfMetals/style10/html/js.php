<?php require_once( dirname(__FILE__) . '/../../../../quotesJs/jsHtml.php' );?>
<?php 
    ob_start();
?>
    var ft<?=$hash?>FT<?=$marker?>ShowDiff = <?php if( isset($showDiff) && $showDiff ) echo 'true'; else echo 'false';?>;
    if( column.name == '<?=$defaultColumn?>' && ft<?=$hash?>FT<?=$marker?>ShowDiff ){
        trEl.querySelector('[data-column="' + column.name + '"] .value').innerHTML = column.value;
        trEl.querySelector('[data-column="' + column.name + '"] .angle_l_bottom.changeVal').innerHTML = item.informerDiff.toFixed(7);
        
    }else{
        trEl.querySelector('[data-column="' + column.name + '"] .value').innerHTML = column.value;
    }
<?php
    $customSetValsStr = ob_get_clean();
    require_once( dirname(__FILE__) . '/../../../../commonJs/setDataJsHtml.php' );
?>
<?php require_once( dirname(__FILE__) . '/../../../../commonJs/jsHtml.php' );?>
document.querySelector( '.FT<?=$hash?>.FT<?=$marker?>' ).style.opacity=1;