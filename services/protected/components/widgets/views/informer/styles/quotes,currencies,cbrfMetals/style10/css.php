<?php
$m5 = ceil( 5 * $mult );
$m8 = ceil( 8 * $mult );
$m9 = ceil( 9 * $mult );
$m10 = ceil( 10 * $mult );
$m11 = ceil( 11 * $mult );
$m12 = ceil( 12 * $mult );
$m13 = ceil( 13 * $mult );
$m14 = ceil( 14 * $mult );
$m16 = ceil( 16 * $mult );
$m17 = ceil( 17 * $mult );
$m18 = ceil( 18 * $mult );
$m22 = ceil( 22 * $mult );
$m36 = ceil( 36 * $mult );
$m48 = ceil( 48 * $mult );
if( $mult < 0.8 )
	$m55 = ceil( 55 * ($mult/2) );
else
	$m55 = ceil( 55 * $mult );


$lastTrBorderColor = '';
if( !$showGetBtn ){
	$lastTrBorderColor = "table tr:last-of-type td{border-bottom-color:{$informerColors['tableBorderColor']} !important;}";
}

Yii::app()->clientScript->registerCss('informerCss', "

table{max-width: {$width};}

.informer_table_small.default td,
.informer_table_small.default th{
	padding: {$m10}px {$m5}px {$m9}px;
}
.informer_table_marquee thead th,
.informer_table_small thead th,
.informer_table_middle thead th,
.informer_table thead th{
	font-size: {$m12}px;
	padding: {$m14}px {$m5}px {$m13}px;
}
body {
	line-height:{$m22}px;
}
.informer_table_small.default tbody td{
	font-size: {$m12}px;
}





.informer_table_small .instal_link{
	font-size: {$m10}px !important;
}
.informer_table_small .amount_middle{
	font-size: {$m13}px !important;
}
.style10ImgWrap{
	margin-right: {$m10}px; 
	width: {$m16}px; 
	height: {$m16}px;
}
.style10ImgWrap img{
	width:100%;
	height:auto;
}

.informer_table_small thead time, .informer_table_small thead th, .informer_table_small thead th a{
	color: {$informerColors['titleTextColor']};
	background-color: {$informerColors['titleBackgroundColor']};
}
.informer_table_small .symbol, .informer_table_small .symbol a{
	color: {$informerColors['symbolTextColor']};
}
.informer_table_small .value{
	color: {$informerColors['tableTextColor']};
}
.angle_l_bottom,
.angle_l_top{
	font-size: {$m13}px;
	line-height: {$m13}px;
	display:inline-block;
	vertical-align:middle;
}
.loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid {$informerColors['lossTextColor']};
}
.profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid {$informerColors['profitTextColor']};
}
.loss .changeVal{
	color: {$informerColors['lossTextColor']};
}
.loss .changeVal.bg{
	background-color: {$informerColors['lossBackgroundColor']};
}
.profit .changeVal{
	color: {$informerColors['profitTextColor']};
}
.profit .changeVal.bg{
	background-color: {$informerColors['profitBackgroundColor']};
}




.informer_table_small tbody .col-data time{
	color: {$informerColors['dataTextColor']};
}
.informer_table_small tbody tr.col-data,
.informer_table_small tbody tr.col-data td{
	background-color: {$informerColors['dataBackgroundColor']} !important;
}





.informer_table_small td, .informer_table_small th{
	border-color: {$informerColors['borderTdColor']};
}

.informer_table_small thead tr:first-child th{
	border-top-color: {$informerColors['tableBorderColor']};
}
.informer_table_small thead tr:first-child th:first-child{
	border-left-color: {$informerColors['tableBorderColor']};
}
.informer_table_small thead tr:first-child th:last-child{
	border-right-color: {$informerColors['tableBorderColor']};
}
.informer_table_small tfoot tr:last-child td{
	border-bottom-color: {$informerColors['tableBorderColor']};
}
.informer_table_small tfoot tr:last-child td:first-child{
	border-left-color: {$informerColors['tableBorderColor']};
}
.informer_table_small tfoot tr:last-child td:last-child{
	border-right-color: {$informerColors['tableBorderColor']};
}
.informer_table_small tbody tr td:first-child{
	border-left-color: {$informerColors['tableBorderColor']};
}
.informer_table_small tbody tr td:last-child{
	border-right-color: {$informerColors['tableBorderColor']};
}
.informer_table_small tbody tr td{
	background-color: {$informerColors['trBackgroundColor']};
}
.instal_link{
	color: {$informerColors['informerLinkTextColor']};
}
.informer_table_small tfoot tr td{
	background-color: {$informerColors['informerLinkBackgroundColor']};
}
$lastTrBorderColor
.informer_table_small th, .informer_table_small td{
	border-width: {$model->borderWidth}px !important;
}

.lossTextColor{
	color: {$informerColors['lossTextColor']} !important;
}
.lossTextColor.bg{
	background-color: {$informerColors['lossBackgroundColor']} !important;
}
.profitTextColor{
	color: {$informerColors['profitTextColor']} !important;
}
.profitTextColor.bg{
	background-color: {$informerColors['profitBackgroundColor']} !important;
}

");