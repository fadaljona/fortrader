<table class="informer_table bold_bd">
	<thead>
		<th colspan="2"><?=Yii::t($NSi18n, 'The ruble exchange rates')?></th>
	</thead>
	<tbody>
		<tr class="col1">
			<th><?=Yii::t('informerColumnNames', 'Symbol')?></th>
			<th><?=Yii::t('informerColumnNames', 'Ask')?></th>
		</tr>
		<tr class="col2 loss">
			<td class="arrow amount">
				<span class="style2ImgWrap">
					<img src="/uploads/country/flags/shiny/16/EU.png" alt="">
				</span>
				<span class="symbolContainer"><?=Yii::t($NSi18n, 'EUR')?></span>
			</td>
			<td class="changeVal bg">70.585</td>
		</tr>
		<tr class="col3 profit">
			<td class="arrow amount">
				<span class="style2ImgWrap">
					<img src="/uploads/country/flags/shiny/16/US.png" alt="">
				</span>
				<span class="symbolContainer"><?=Yii::t($NSi18n, 'USD')?></span>
			</td>
			<td class="changeVal">63.8475</td>
		</tr>
		<tr class="col-data">
			<td colspan="2">
				<time datetime="<?=date('Y-m-dTH:i')?>"><?=Yii::t($NSi18n, 'Data time')?>  <?=date('d.m.Y H:i')?> <?=Yii::t($NSi18n, 'mskZone')?></time>
			</td>
		</tr>
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="<?=$colspanCount?>">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>