<?php
    $NSi18n = $this->getNSi18n();
    $idInformer = InformersToParamsModel::getInformerId( $_GET );
    $rand = rand();

    ob_start();

?>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->createAbsoluteUrl( 'informers/css', array( 'id' => $idInformer ) )?>">

<div class='FTinformersWrapper FT<?=$hash?> FT<?=$rand?>'><script>document.querySelector(".FT<?=$hash?>.FT<?=$rand?>").style.opacity=0;</script>

    <div class="informer_box informer_box_example clearfix alignCenter <?=$ins?>">
        <div class="inlineBlock">

        <?php
        $informerHeader = $category->informerTitle;
        if( $category->type == 'currencies' ) $informerHeader .= ' ' . $category->getColumnName('todayCourse');
        if( $category->linkForHeader ) $informerHeader = CHtml::link( $informerHeader, $category->linkForHeader, array('target' => '_blank') );
    ?>
    <table class="informer_table_small silver_dark2">
        <?php if(!$hideHeader) {?>
        <thead>
            <th colspan="2"><?=$informerHeader?></th>
        </thead>
        <?php }?>
        <tbody>
        <?php
            $itemsCount = count($items);
            $i=0;
            foreach( $items as $item ){
                $lossProfitClass = '';
                /*if( $item->informerDiff < 0 ) $lossProfitClass = 'loss';
                if( $item->informerDiff > 0 ) $lossProfitClass = 'profit';*/
                
                $colClass = 'col1';
                if( $i == $itemsCount-1 ) $colClass = 'col2';	
                $i++;
                
                echo CHtml::openTag( 'tr', array( 'class' => $lossProfitClass . ' trClass ' . $colClass, 'data-symbol' => $item->name, 'data-id' => $item->id ) );
                    echo CHtml::openTag( 'td', array( 'colspan' => '2', 'data-column' => $columns[0] ) );
                        
                        $changeValStr = '';
                        if( $showDiff ) $changeValStr = CHtml::tag( 'span', array( 'class' => "changeVal" ), '' );
                        
                        $tooltip = '';
                        if( $item->informerTooltip ){
                            $tooltip = $item->informerTooltip;
                        }
                        
                        echo CHtml::openTag( 'span' );
                            echo CHtml::tag( 'span', array( 'class' => "symbol" ), CHtml::link($item->informerShortItemName, $item->absoluteSingleURL, array( 'class' => 'symbolContainer', 'target' => '_blank', 'title' => $tooltip )) ) . ' &nbsp; ' . $changeValStr;
                        echo CHtml::closeTag( 'span' );
                        echo ' ';
                        echo CHtml::tag( 'span', array( 'class' => 'amount_middle value' ), '' );
                        
                    echo CHtml::closeTag( 'td' );
                echo CHtml::closeTag( 'tr' );
            }
        ?>
            <?php if (!$hideDate) {?>
            <tr class="col-data">
                <td colspan="2">
                    <time class='ftDateTime' datetime="<?=date('Y-m-d')?>"><span class='ftDateTimeStr'>---</span></time>
                </td>
            </tr>
            <?php }?>
        </tbody>
        <?php if( $showGetBtn ){?>
        <tfoot>
            <tr>
                <td colspan="2">
                    <a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
                </td>
            </tr>
        </tfoot>
        <?php }?>
    </table>

        </div>
    </div>
</div>
<script async src="<?=Yii::app()->createAbsoluteUrl( 'informers/js', array( 'id' => $idInformer, 'm' => $rand ) );?>" type="text/javascript"></script>
<?php
    $content = ob_get_clean();
    $content = str_replace(array("\n", "\r"), '', $content);
    echo preg_replace('~>\s+<~', '><', $content);
?>