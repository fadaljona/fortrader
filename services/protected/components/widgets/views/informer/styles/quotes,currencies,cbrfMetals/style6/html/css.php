<?php
echo '@import "' . Yii::app()->params['wpThemeUrl'] . '/css/informersHtml.css' . '";';

$m5 = ceil( 5 * $mult );
$m8 = ceil( 8 * $mult );
$m9 = ceil( 9 * $mult );
$m10 = ceil( 10 * $mult );
$m11 = ceil( 11 * $mult );
$m12 = ceil( 12 * $mult );
$m13 = ceil( 13 * $mult );
$m14 = ceil( 14 * $mult );
$m16 = ceil( 16 * $mult );
$m18 = ceil( 18 * $mult );
$m22 = ceil( 22 * $mult );
$m36 = ceil( 36 * $mult );
$m48 = ceil( 48 * $mult );
if( $mult < 0.8 )
	$m55 = ceil( 55 * ($mult/2) );
else
	$m55 = ceil( 55 * $mult );
	

$lastTrBorderColor = '';
if( !$showGetBtn ){
	$lastTrBorderColor = ".FTinformersWrapper.FT{$hash} .informer_table tr:last-of-type td{border-bottom-color:{$informerColors['tableBorderColor']};}";
}
	
echo "

.FTinformersWrapper.FT{$hash} table{max-width: {$width};}

.FTinformersWrapper.FT{$hash} .informer_table_marquee thead th,
.FTinformersWrapper.FT{$hash} .informer_table_small thead th,
.FTinformersWrapper.FT{$hash} .informer_table_middle thead th,
.FTinformersWrapper.FT{$hash} .informer_table thead th{
	font-size: {$m12}px;
	padding: {$m14}px {$m5}px {$m13}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_marquee th,
.FTinformersWrapper.FT{$hash} .informer_table_marquee td,
.FTinformersWrapper.FT{$hash} .informer_table_small th,
.FTinformersWrapper.FT{$hash} .informer_table_small td,
.FTinformersWrapper.FT{$hash} .informer_table_middle th,
.FTinformersWrapper.FT{$hash} .informer_table_middle td,
.FTinformersWrapper.FT{$hash} .informer_table th,
.FTinformersWrapper.FT{$hash} .informer_table td{
	padding: {$m9}px {$m5}px {$m9}px;
}
.FTinformersWrapper.FT{$hash} .informer_table{
	font-size: {$m13}px;
}
.FTinformersWrapper.FT{$hash} .loss .arrow:before{
	font: normal normal normal {$m16}px/1 FontAwesome !important;
}
.FTinformersWrapper.FT{$hash} .profit .arrow:before{
	font: normal normal normal {$m16}px/1 FontAwesome !important;
}
.FTinformersWrapper.FT{$hash} .informer_table [class*='icon_amount']{
	padding-left: {$m10}px;
}
.FTinformersWrapper.FT{$hash} .informer_table tfoot td,
.FTinformersWrapper.FT{$hash} .informer_table_middle tfoot td,
.FTinformersWrapper.FT{$hash} .informer_table_marquee tfoot td{
	padding: {$m11}px {$m5}px;
}
.FTinformersWrapper.FT{$hash} .informer_table_marquee .instal_link:before,
.FTinformersWrapper.FT{$hash} .informer_table .instal_link:before{
	font: normal normal normal {$m18}px/1 FontAwesome;
}
.FTinformersWrapper.FT{$hash}  {
	line-height:{$m22}px;
}
.FTinformersWrapper.FT{$hash} .informer_table.white thead th{
	font-size: {$m14}px;
}
.FTinformersWrapper.FT{$hash} .informer_table.white tbody td{
	font-size: {$m18}px;
}
.FTinformersWrapper.FT{$hash} .style5ImgWrap, 
.FTinformersWrapper.FT{$hash} .style5ImgWrap img{
	width: {$m48}px; 
	height: {$m48}px; 
}
.FTinformersWrapper.FT{$hash} .amount_big.bigFlag a{
	line-height: {$m48}px; 
}




.FTinformersWrapper.FT{$hash} .informer_table thead th, 
.FTinformersWrapper.FT{$hash} .informer_table thead th a{
	color: {$informerColors['titleTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table thead{
	background-color: {$informerColors['titleBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tbody th{
	color: {$informerColors['thTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tbody th{
	background-color: {$informerColors['thBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tbody tr td{
	color: {$informerColors['tableTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tbody tr td:nth-child(1), 
.FTinformersWrapper.FT{$hash} .informer_table tbody tr td:nth-child(1) a{
	color: {$informerColors['symbolTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tbody [class*='col'] th:not(:last-child),
.FTinformersWrapper.FT{$hash} .informer_table tbody [class*='col'] td:not(:last-child){
	border-left-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tbody [class*='col'] th:last-child,
.FTinformersWrapper.FT{$hash} .informer_table tbody [class*='col'] td:last-child{
	border-right-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table thead th{
	border-top-color: {$informerColors['tableBorderColor']};;
	border-left-color: {$informerColors['tableBorderColor']};;
	border-right-color: {$informerColors['tableBorderColor']};;
}
.FTinformersWrapper.FT{$hash} .informer_table tbody .col-data td{
	border-left-color: {$informerColors['tableBorderColor']};
	border-right-color: {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tfoot td{
	border:2px solid {$informerColors['tableBorderColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tbody tr:nth-child(2n+2){
	background-color: {$informerColors['oddBackgroundTrColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tbody tr:nth-child(2n+3){
	background-color: {$informerColors['evenBackgroundTrColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table .loss .arrow:before{
	color: {$informerColors['lossTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table .loss .changeVal{
	color: {$informerColors['lossTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table .loss .changeVal.bg{
	background-color: {$informerColors['lossBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table .profit .changeVal{
	color: {$informerColors['profitTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table .profit .changeVal.bg{
	background-color: {$informerColors['profitBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table_marquee .instal_link,
.FTinformersWrapper.FT{$hash} .informer_table .instal_link{
	color: {$informerColors['informerLinkTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tfoot td{
	background-color: {$informerColors['informerLinkBackgroundColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table tbody .col2 td{
	border-bottom-color:transparent;
}
.FTinformersWrapper.FT{$hash} .informer_table thead time{
	font-weight:400;
	color: {$informerColors['titleTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table th, 
.FTinformersWrapper.FT{$hash} .informer_table td{
	border-width:2px;
}
.FTinformersWrapper.FT{$hash} .informer_table td, 
.FTinformersWrapper.FT{$hash} .informer_table th{
	border:2px solid {$informerColors['borderTdColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table td .itemImgWrap{
	background-color: {$informerColors['itemImgBgColor']};
	color: {$informerColors['symbolTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table .loss .angle_l_bottom:before{
	border: 3px solid transparent;
    border-top: 4px solid {$informerColors['lossTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table .profit .angle_l_bottom:before{
	border: 3px solid transparent;
    border-bottom: 4px solid {$informerColors['profitTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table td .itemImgWrap{
	background-color: {$informerColors['itemImgBgColor']};
	color: {$informerColors['symbolTextColor']};
}
.FTinformersWrapper.FT{$hash} .informer_table td .itemImgWrap{
	background-color: {$informerColors['itemImgBgColor']};
	color: {$informerColors['symbolTextColor']};
	height: {$m36}px;
	width: {$m36}px;

}
.FTinformersWrapper.FT{$hash} .informer_table tbody td a, 
.FTinformersWrapper.FT{$hash} .informer_table tbody td .symbolContainer{
	line-height: {$m36}px;
}
.FTinformersWrapper.FT{$hash} .informer_table td .itemImgWrap{
	line-height: {$m36}px;
}
.FTinformersWrapper.FT{$hash} .informer_table td .itemImgWrap .symbolCode{
	color: {$informerColors['itemImgTextColor']};
	font-size:{$m18}px;
}


.FTinformersWrapper.FT{$hash} .informer_table th, 
.FTinformersWrapper.FT{$hash} .informer_table td{
	border-width:{$model->borderWidth}px !important;
}
$lastTrBorderColor



.FTinformersWrapper.FT{$hash} .lossTextColor{
	color: {$informerColors['lossTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .lossTextColor.bg{
	background-color: {$informerColors['lossBackgroundColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profitTextColor{
	color: {$informerColors['profitTextColor']} !important;
}
.FTinformersWrapper.FT{$hash} .profitTextColor.bg{
	background-color: {$informerColors['profitBackgroundColor']} !important;
}

";