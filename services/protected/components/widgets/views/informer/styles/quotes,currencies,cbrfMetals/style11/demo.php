<table class="informer_table_small silver_dark2">
	<thead>
		<th colspan="2"><?=Yii::t($NSi18n, 'The ruble exchange rates')?> <time datetime="<?=date('Y-m-d')?>"><?=date('d.m.Y')?></time></th>
	</thead>
	<tbody>
		<tr class="col1 loss">
			<td colspan="2">
				<span>
					<span class="symbol symbolContainer">USD</span> <span class="changeVal bg">-0.0064</span>
				</span>
				<span class="amount_middle value">64.254</span>
			</td>
		</tr>
		<tr class="col2 profit">
			<td colspan="2">
				<span>
					<span class="symbol symbolContainer">EUR</span> <span class="changeVal">0.0064</span>
				</span>
				<span class="amount_middle value">70.754</span>
			</td>
		</tr>
		
	</tbody>
	<?php if( $showGetBtn ){?>
	<tfoot>
		<tr>
			<td colspan="2">
				<a href="<?=InformersSettingsModel::getIndexUrl()?>" class="instal_link" target="_blank"><?=Yii::t($NSi18n, 'Install the informer on your website')?></a>
			</td>
		</tr>
	</tfoot>
	<?php }?>
</table>