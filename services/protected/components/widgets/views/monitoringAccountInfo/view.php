<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>


<div class="<?=$ins?>">
	<!-- - - - - - - - - - - - - - Info- - - - - - - - - - - - - - - - -->
	<section class="section_offset monitoring_info">
		<h4 class="toggle_btn blue_color_hight"><?=Yii::t($NSi18n, 'Account Information')?></h4>
		<div class="toggle_content box2 box_informer">
			<?php $this->widget( "widgets.detailViews.MonitoringAccountInfoDetailViewWidget", Array( 'data' => $model ));?>
		</div>
	</section>
	<!-- - - - - - - - - - - - - - End Info - - - - - - - - - - - - - - - - -->

	<!-- - - - - - - - - - - - - - Index- - - - - - - - - - - - - - - - -->
	<section class="section_offset monitoring_info">
		<h4 class="toggle_btn blue_color_hight"><?=Yii::t($NSi18n, 'Indicators')?> 
		<?php
			if( $model->stats->trueDT ){
				echo CHtml::tag('span', array('class'=>'modify_date'), ' / ' . Yii::t($NSi18n, 'Updated') . ': ' . Yii::app()->dateFormatter->format( 'dd MMMM HH:mm',strtotime( $model->stats->trueDT )));
			}
		?></h4>
		<div class="toggle_content box2 box_informer">
			<?php $this->widget( "widgets.detailViews.MonitoringAccountIndicatorsDetailViewWidget", Array( 'data' => $model ));?>
		</div>
	</section>
	<!-- - - - - - - - - - - - - - End Index - - - - - - - - - - - - - - - - -->
</div>