<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();	
	
	$this->widget( 'widgets.lists.ForeignContestsListWidget', Array(
		'ns' => $ns,
		'ajax' => true,
		'showOnEmpty' => true,
	));

	$this->widget( 'widgets.forms.ContestForeignFormWidget', Array(
		'model' => $formModel,
		'ns' => $ns,
		'ajax' => true,
		'mode' => 'front',
	));
?>