<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="exchange-rate-container <?=$ins?>">
	<div class="exchange-rate">
		<span>
			<i class="exchange-currency" <?php if($fromFlag){ echo "style='background: url($fromFlag) no-repeat;'"; }  ?> ></i>
			<?=number_format($model->value, 2, ',', ' ')?> <?=$model->rateFrom->code?> =
		</span>
		<span>
			<i class="exchange-currency" <?php if($toFlag){ echo "style='background: url($toFlag) no-repeat;'"; }  ?> ></i>
			<?=number_format($toVal, CommonLib::getDecimalPlaces( $toVal ), ',', ' ')?> <?=$model->rateTo->code?>
		</span>
	</div>
	<div class="exchange-rate-description">
		1 <?=$model->rateFrom->code?> = <?=number_format($toOneVal, CommonLib::getDecimalPlaces( $toOneVal ), ',', ' ')?> <?=$model->rateTo->code?> <?=$diffValStr?> <?=$infoText?> <?=date('d.m.Y')?>.
	</div>
</div>

