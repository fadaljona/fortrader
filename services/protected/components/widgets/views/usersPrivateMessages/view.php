<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wUsersPrivateMessagesWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$title = $action == 'list' ? Yii::t( $NSi18n, 'Dialogs' ) : Yii::t( $NSi18n, 'Dialog with' )." ".$with->showName;
?>
<div class="widget-box iUsersConversationWidget i01 wUsersPrivateMessagesWidget">
	<div class="widget-header">
		<h4 class="lighter smaller">
			<i class="icon-comment blue"></i>
			<?=$title?>
		</h4>
	</div>
	<div class="widget-body">
		<div class="widget-main no-padding">
			<div class="dialogs <?=$ins?> wMessagesList">
				<?$this->renderMessages()?>
			</div>
			<div class="<?=$ins?> wTplItem" style="display:none; padding-bottom:14px;">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</div>
			<?if( $this->issetMoreMessages()){?>
				<div class="text-center" style="padding-bottom:14px;">
					<button class="btn btn-small btn-yellow no-radius <?=$ins?> wShowMore">
						<i class="icon-arrow-down"></i>
						<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Show more messages' )?></span>
					</button>
				</div>
			<?}?>
			<?if( $action == 'dialog' ){?>
				<div class="<?=$ins?> wAnchorForm">
					<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
						<?=$form->hiddenField( $formModel, 'id', Array( 'class' => "{$ins} wID" ))?>
						<?=CHtml::hiddenField( $formModel->resolveName( 'idTo' ), $with->id )?>
						<div class="form-actions input-append">
							<table style="width:100%;">
								<tr>
									<td>
										<?=$form->textField( $formModel, 'text', Array( 'class' => "iInput i01 {$ins} wText", 'placeholder' => Yii::t( $NSi18n, 'Type your message here ...' )))?>
									</td>
									<td style="width:10px;">
										<button class="btn btn-small btn-info no-radius <?=$ins?> wSubmit" type="submit">
											<i class="icon-share-alt"></i>
											<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Send' )?></span>
										</button>
										<button class="btn btn-small btn-danger no-radius <?=$ins?> wCancel" style="display:none;">
											<i class="icon-ban-circle"></i>
											<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Cancel' )?></span>
										</button>
									</td>
								</tr>
							</table>
						</div>
					<?$this->endWidget()?>
				</div>
			<?}?>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var ins = ".<?=$ins?>";
		var nsText = ".<?=$ns?>";
		var action = "<?=$action?>";
		var idWith = <?= $action == 'list' ? 'null' : $with->id?>;
				
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wUsersPrivateMessagesWidget = wUsersPrivateMessagesWidgetOpen({
			ins: ins,
			selector: nsText+' .wUsersPrivateMessagesWidget',
			action: action,
			idWith: idWith,
			ls: ls,
		});
	}( window.jQuery );
</script>