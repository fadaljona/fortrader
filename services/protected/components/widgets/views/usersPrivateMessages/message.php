<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$userPrivateMessageControl = Yii::App()->user->checkAccess( 'userPrivateMessageControl' );

	switch( $action ) {
		case 'list':{
			$message = $model->message;
			$urlDialog = Yii::App()->createURL( '/userPrivateMessage/dialog', Array( 'idWith' => $model->idWith ));
			//$class = 'iDiv i04';
			$class = '';
			break;
		}
		case 'dialog':{
			$message = $model;
			$class = '';
			break;
		}
	}
?>
<div class="itemdiv dialogdiv <?=$class?>" idMessage="<?=$message->id?>">
	<div class="user">
		<?=$message->from->getHTMLAvatar()?>
	</div>
	<?if( $action == 'list' ){?><a href="<?=$urlDialog?>" class="body" style="display:block">
	<?}else{?><div class="body"><?}?>
		<span class="time">
			<?$this->widget( 'widgets.parts.TimeDiffWidget', Array( 'time' => $message->createdDT ))?>
		</span>
		<span class="name">
			<?if( $action == "list" ){?>
				<span class="link">
					<?=CHtml::encode( $message->from->showName )?>
				</span>
			<?}else{?>
				<?=CHtml::link( CHtml::encode( $message->from->showName ), $message->from->getProfileURL())?>
			<?}?>
			<?foreach( $message->from->groups as $group ){?>
				<?if( $userPrivateMessageControl or $group->name == 'Administrator' ){?>
					<span class="label label-info arrowed arrowed-in-right">
						<?=Yii::t( $NSi18n, $group->name )?>
					</span>
				<?}?>
			<?}?>
			<?if( $action == "list" and $message->from->id == Yii::App()->user->id ){?>
				&nbsp;&nbsp;
				<?=Yii::t( $NSi18n, 'To' )?>:&nbsp;<span class="link"><?=$message->to->showName?></span>
			<?}?>
			<?if( $action == "list" ){?>
				&nbsp;&nbsp;
				<i class="icon-comments light-orange bigger-110"></i>&nbsp;<span class="light-orange"><?=$model->count?></span>
			<?}?>
		</span>
		<?$style = $message->from->id != Yii::App()->user->id && !$message->opened ? ' style="font-weight:bold;"' : '' ?>
		<span class="text"<?=$style?>>
			<?=$message->getHTMLText()?>
		</span>
		<?if( $action == 'dialog' and $message->checkAccess()){?>
			<div class="tools">
				<div class="inline position-relative nowrapTT">
					<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cog icon-only bigger-110"></i>
					</button>
					<ul class="dropdown-menu dropdown-icon-only dropdown-light dropdown-caret dropdown-close">
						<li>
							<a href="#" class="tooltip-success <?=$ins?> wEdit" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Edit' )?>">
								<span class="green">
									<i class="icon-edit"></i>
								</span>
							</a>
						</li>
						<li>
							<a href="#" class="tooltip-error <?=$ins?> wDelete" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Delete' )?>">
								<span class="red">
									<i class="icon-trash"></i>
								</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		<?}?>
	<?if( $action == 'list' ){?></a>
	<?}else{?></div><?}?>
</div>