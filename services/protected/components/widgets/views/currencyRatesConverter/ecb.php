<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$cs=Yii::app()->getClientScript();
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js');
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wEcbCurrencyRatesConverterWidget.js' ) );
?>
<!-- - - - - - - - - - - - - -  Converter - - - - - - - - - - - - - - - - -->
<section class="section_offset <?=$ins?> paddingBottom0" itemscope itemtype="http://schema.org/CurrencyConversionService">
	<?php if(!$withoutTitle){ ?>
	<hr class="separator2">
	<h3 itemprop="name"><?=$model->converterTitle?><i class="icon rates_icon"></i></h3>
	<hr>
	<?php }else{ ?>
	<meta itemprop="name" content="<?=strip_tags($headerTitle)?>" /> 
	<?php } ?>
	<meta itemprop="serviceType" content="Currencies converter" />
	<div class="converter_wr clearfix">
		<div class="converter">
			<input type="text" value="1" class="converter_form_inp converterInpFrom" />
			<div class="">
				<div class="sec  converter_form">
					<select class="form_styler converterSelect converterSelectFrom">
						<option label="-"></option>
					<?php
						$i=1;
						foreach( $currenciesForConverter as $key => $val ){
							if( $i<=2 ) echo CHtml::openTag('optgroup', array( 'label' => $i, 'class' => "stationary stat_$i" ));
							$selected = false;
							if( $key == $model->code ) $selected = true;
							echo CHtml::tag(
								'option',
								array(
									'data-value' => $val['value'],
									'value' => $val['id'],
									'data-title' => $val['name'],
									'selected' => $selected,
								),
								$key
							);
							if( $i<=2 ) echo CHtml::closeTag('optgroup');
							if( $i == 2 ){
								echo CHtml::openTag('optgroup', array( 'label' => 3, 'class' => "stationary stat_3" ));
									echo CHtml::tag(
										'option', 
										array(
											'data-value' => 1,
											'value' => $defaultCurrency->id,
											'data-title' => $defaultCurrency->name,
										), 
										$defaultCurrency->code
									);
								echo CHtml::closeTag('optgroup');
							}
							$i++;
						}
					?>
					</select>
				</div><!-- .sec -->
			</div>
		</div>
		<div class="converter_arrow flipConverter"></div>
		<div class="converter converter_right">
			<?php $rubCount = number_format( 1/$currenciesForConverter[$model->code]['value'], 4, '.', '' );?>
			<input type="text" value="<?=$rubCount?>" class="converter_form_inp converterInpTo" disabled />
			<div class="converter_form">
				<div class="sec  exchange_rates_form">
					<select class="form_styler converterSelect converterSelectTo">
						<option label="-"></option>
					<?php
						$i=1;
						foreach( $currenciesForConverter as $key => $val ){
							if( $i<=2 ) echo CHtml::openTag('optgroup', array( 'label' => $i, 'class' => "stationary stat_$i" ));
							echo CHtml::tag(
								'option',
								array(
									'data-value' => $val['value'],
									'value' => $val['id'],
									'data-title' => $val['name'],
								),
								$key
							);
							if( $i<=2 ) echo CHtml::closeTag('optgroup');
							if( $i == 2 ){
								echo CHtml::openTag('optgroup', array( 'label' => 3, 'class' => "stationary stat_3" ));
									echo CHtml::tag(
										'option', 
										array(
											'data-value' => 1,
											'value' => $defaultCurrency->id,
											'selected' => true,
											'data-title' => $defaultCurrency->name,
										), 
										$defaultCurrency->code
									);
								echo CHtml::closeTag('optgroup');
							}
							$i++;
						}
					?>
					</select>
				</div><!-- .sec -->
			</div>
		</div>
	</div>
	<?php $this->widget( 'widgets.CurrencyRatesExcangePopularLogConvertsWidget', Array( 'ns' => 'nsActionView', 'type' => 'ecb' ));?>
</section>	
<!-- - - - - - - - - - - - - - End of Converter - - - - - - - - - - - - - - - - -->

<script>
!function( $ ) {
	var ns = <?=$ns?>;
	
	ns.wCurrencyRatesConverterWidget = wCurrencyRatesConverterWidgetOpen({
		selector: '.<?=$ns?> .<?=$ins?>',
		sendConverterDataUrl: '<?=Yii::app()->createUrl('currencyRates/ajaxConvertRequest')?>',
		type: '<?=$type?>'
	});
}( window.jQuery );
</script>