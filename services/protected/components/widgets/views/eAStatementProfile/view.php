<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wEAStatementProfileWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lSaidThank' => 'Said thank you',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$countThanks = ThankModel::getCount( 'eaStatement/single', $model->id );
?>
<div class="row-fluid wEAStatementProfileWidget">
	<div class="row-fluid">
		<div class="">
			<?=$model->desc?>
			<?if( strlen( $model->nameFileGraph )){?>
				<div style="display:inline-block; position:relative;">
					<?=$model->getFileGraph()?>
					<div style="position:absolute; right:0px; bottom: 0px;text-align:right;">
						<span class="label label-info arrowed"><?=$model->symbol?> <?=$model->period?></span><br>
						<span class="label label-important"><?=Yii::t( '*', $model->type )?></span>
					</div>
				</div>
			<?}?>
		</div>
		<div class="span9" style="margin-left:0px;">
			<div class="space-6"></div>
			<div class="profile-contact-info">
				<div class="profile-contact-links align-left">
					<?if( strlen( $model->nameFileSet )){?>
						<a class="btn btn-link" href="<?=$model->getDownloadSetURL()?>">
							<i class="icon-download bigger-120 pink"></i>
							<?=Yii::t( '*', 'Download (.set)' )?>
						</a>
					<?}?>
					<?if( strlen( $model->nameFileStatement )){?>
						<a class="btn btn-link" href="<?=$model->getViewStatementURL()?>" target="_blank">
							<i class="icon-globe bigger-125 blue"></i>
							<?=Yii::t( '*', 'View Statement' )?>
						</a>
					<?}?>
				</div>
			</div>
			
			<?if( !Yii::App()->user->isGuest ){?>
				<div class="space-6"></div>
				
				<div class="row-fluid">
					<a href="#" class="iBtn i10 wThank">
						<?=Yii::t( '*', 'Thanks' )?> <?if($countThanks){?>(<?=$countThanks?>)<?}?>
					</a>
				</div>
				<div class="space-6"></div>
				<div class="wThanks iDiv i16"></div>
			<?}?>
			
			<div class="hr hr12 dotted"></div>
			<div class="clearfix">
				<div class="grid2">
					<span class="bigger-175 blue"><?=CommonLib::numberFormat( $model->views )?></span>
					<br>
					<?=Yii::t( '*', 'Views' )?>
				</div>
				<div class="grid2">
					<span class="bigger-175 green"><?=CommonLib::numberFormat( $model->downloads )?></span>
					<br>
					<?=Yii::t( '*', 'Downloads (.sets)' )?>
				</div>
			</div>
			<div class="hr hr12 dotted"></div>
		</div>
		<div class="span9" style="margin-left:0px;">
			<div class="space-12"></div>
			<?$this->controller->widget( "widgets.detailViews.EAStatementProfileDetailViewWidget", Array( 'data' => $model ))?>
			<h3 class="header smaller lighter blue"></h3>
			
			<div class="row-fluid">
				<?
					$this->widget( 'widgets.UsersConversationWidget', Array(
						'ns' => $ns,
						'ajax' => true,
					));
				?>
			</div>
			
			<div class="row-fluid" style="margin-top:20px;">
				<?
					$this->widget( 'widgets.PostInformWidget', Array(
						'ns' => 'nsActionView',
					));
				?>
			</div>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idStatement = <?=$model->id?>;
		
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wEAStatementProfileWidget = wEAStatementProfileWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wEAStatementProfileWidget',
			idStatement: idStatement,
			ls: ls,
		});
	}( window.jQuery );
</script>