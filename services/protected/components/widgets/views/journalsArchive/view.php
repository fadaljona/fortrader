<?
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wJournalsArchiveWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();

?>
<div class="wJournalsArchiveWidget<?=$type?>">

	<hr class="separator2">
	<div class="section_offset">
		<h3><?=$type ? Yii::t( '*', 'Archive digest' ) : Yii::t( '*', 'Archive magazine' )?><i class="fa fa-book"></i></h3>
		<hr class="separator2">
	</div>

	<div class="<?=$ins?> wList wLborder widget_container positionRelative">
        <?$this->renderList()?>
    </div>
	<?if( $this->issetMore()){?>
	
		<div class="section_offset">
			<a href="#" class="load_btn chench_btn <?=$ins?> wShowMore" data-text="<?=Yii::t( $NSi18n, 'Show more' )?>" data-shot-text="<?=Yii::t( $NSi18n, 'Show more' )?>"></a>
		</div>
		
		<div class="spinner-wrap <?=$ins?> wTplItem" style="display:none;">
			<div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div>
			</div>
		</div>
		
	<?}?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		ns.wJournalsArchiveWidget = wJournalsArchiveWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wJournalsArchiveWidget<?=$type?>',
			ajaxLoadListURL: '<?=Yii::app()->createUrl('journal/ajaxLoadArchiveList')?>',
			limit: <?=$limit?>,
			type: <?=$type?>,
		});
	}( window.jQuery );
</script>