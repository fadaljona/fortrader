<?php

$NSi18n = $this->getNSi18n();

if( $i == 0 ){?>
<div class="section_offset paddingBottom30">
	<div class="post_edition clearfix">
<?php }?>

	<figure class="post_col4">
		<a href="<?=$journal->singleURL?>" data-idJournal="<?=$journal->id?>" data-dateJournal="<?=$journal->date?>">
			<?=$journal->getCover(Array( 'width' => 180 ))?>
		</a>
		<figcaption><a href="<?=$journal->singleURL?>"><?=$journal->journalDate?></a></figcaption>
	</figure>


<?if( $i % $this->limit == $this->limit - 1 ){?>
	</div>
</div>
<?}?>

<?php if( ($i+1) % 4 == 0 ){?>
<div class="clear"></div>
<?php }?>