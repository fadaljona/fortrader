<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>

<div class="fx_box-half fx_latest-right fx_latest-mt">
    <div class="fx_half-header clearfix">
        <span class="fx_half-header--left"><?= Yii::t($NSi18n, 'Forum') ?></span>
        <a href="<?= $forumUrl ?>" class="fx_half-header--right"><?= Yii::t($NSi18n, 'Go to forum') ?></a>
    </div>
    <?php
    foreach ($posts as $url => $title) {
        echo CHtml::link($title, $url, array('class' => 'fx_half-right-item'));
    }
    ?>
</div>