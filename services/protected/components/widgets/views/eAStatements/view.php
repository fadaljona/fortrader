<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wEAStatementsWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
		
?>
<div class="row-fluid wEAStatementsWidget">
	<h4 class="header smaller lighter blue" style="margin-top:30px;text-transform:uppercase;"><?=Yii::t( '*', 'Last statements upload' )?></h4>
	<div class="<?=$ins?> wList">
		<?$this->renderList()?>
	</div>
	<?if( $this->issetMore()){?>
		<div class="<?=$ins?> wTplItem" style="display:none; padding-bottom:14px;">
			<div class="progress progress-info progress-striped active">
				<div class="bar" style="width: 100%;"></div>
			</div>
		</div>
		<div class="text-center" style="padding-bottom:14px;">
			<button class="btn btn-small btn-yellow no-radius <?=$ins?> wShowMore">
				<i class="icon-arrow-down"></i>
				<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Show more' )?></span>
			</button>
		</div>
	<?}?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idEA = <?=(int)$this->idEA?>;
		var idEAVersion = <?=(int)$this->idEAVersion?>;
		
		ns.wEAStatementsWidget = wEAStatementsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wEAStatementsWidget',
			idEA: idEA,
			idEAVersion: idEAVersion,
		});
	}( window.jQuery );
</script>