<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wTraderProfileAlertWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wTraderProfileAlertWidget">
	<div class="alert alert-block alert-warning <?=$ins?> wTraderAlert">
		<p>
			<?$params['tabProfileTrader'] = 'yes';?>
			<?$url = $this->controller->createURL( '/user/settings', $params )?>
			<?=Yii::t( '*', "Please, edit you {trader profile}!", Array( '{trader profile}' => CHtml::link( Yii::t( 'lower', 'trader profile' ), $url )))?>
		</p>
		<p>
			<a href="<?=$url?>" class="btn btn-small btn-success"><?=Yii::t( $NSi18n, 'Edit' )?></a>
			<button class="btn btn-small wCancel"><?=Yii::t( $NSi18n, 'Cancel' )?></button>
		</p>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
				
		ns.wTraderProfileAlertWidget = wTraderProfileAlertWidgetOpen({
			ns: ns,
			selector: nsText+' .wTraderProfileAlertWidget',
		});
	}( window.jQuery );
</script>