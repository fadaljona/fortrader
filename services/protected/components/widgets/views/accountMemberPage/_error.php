<?php if( $accountModel->stats->last_error_code == -255 ){?>
	<section class="section_offset paddingBottom30">
		<h5 class="info_title"><?=Yii::t( '*', 'Connection Error!' )?></h5>
		<br />
		<p>
            <?=Yii::t( '*', 'Terminal reports an error connecting to your account:' )?>
            <strong><?=Yii::t( '*', 'Invalid account' )?></strong>
            <br /><?=Yii::t( '*', 'invalid account error desciption' )?>
        </p>
        <p>
			<a class="red_color" href="<?=Yii::App()->createUrl( 'user/profile', Array( 'tabTA' => 'yes', 'idEditMember' => $accountModel->id ))?>"><?=Yii::t( '*', 'Edit' )?></a><br />
            <a class="red_color" href="<?=Yii::App()->createUrl( 'contacts/index', Array())?>"><?=Yii::t( '*', 'Contact the broker' )?></a>
        </p>
	</section>

<?php }?>