<?
	$itCurrentUser = Yii::App()->user->isGuest ? false : isset($accountModel->idUser) && Yii::App()->user->id == $accountModel->idUser;
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<script>
	var nsActionView = {};
	var nsActionView2 = {};
</script>
<div class="nsActionView">

	<?php
		if( $itCurrentUser ) {
			require dirname( __FILE__ )."/_error.php";
		}
		
		$this->widget( 'widgets.lists.ContestMemberInfoVerticalListWidget', Array(
			'ns' => 'nsActionView',
			'idMember' => $accountModel->id,
		));
		
		$this->widget( 'widgets.graphs.ContestMemberGraphWidget', Array(
			'ns' => 'nsActionView',
			'contestMember' => $accountModel,
		));
		
		$this->widget( 'widgets.MemberTradingActivityWidget', Array(
			'ns' => 'nsActionView',
			'accountModel' => $accountModel,
		));
		
		$this->widget( 'widgets.ContestMemberStatementWidget', Array(
			'ns' => 'nsActionView',
			'idMember' => $accountModel->id
		));
				
		$this->widget( 'widgets.graphs.ContestMemberDealsGraphWidget', Array(
			'ns' => 'nsActionView',
			'contestMemberId' => $accountModel->id,
			'ajax' => false,
		));
	?>
</div>