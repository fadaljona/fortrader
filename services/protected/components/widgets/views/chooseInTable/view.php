<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$tableId = $ins . time();
	
	Yii::App()->clientScript->registerCssFile( '//cdn.datatables.net/1.10.12/css/jquery.dataTables.css' );
	Yii::App()->clientScript->registerScriptFile('//cdn.datatables.net/1.10.12/js/jquery.dataTables.js');
	Yii::app()->clientScript->registerScript('DataTableJs', "
!function( $ ) {	
	$('#$tableId').DataTable({
		'order': [[ 0, 'desc' ]]
	});
}( window.jQuery );
	", CClientScript::POS_END);
	
	Yii::app()->clientScript->registerCss('DataTableCss', '
		.dataTables_wrapper .dataTables_length, .dataTables_wrapper .dataTables_filter{float:none;text-align:left;}
	');
	
	$currentId = @$_GET[$param];

?>

<div class="<?=$ins?>">

	<?php if($currentId){ ?>
	<button data-toggle="collapse" data-target="#<?=$tableId?>collapse"><?=$header?> - <?=$options[$currentId]?></button>

	<div id="<?=$tableId?>collapse" class="collapse">
		<br>
	<?php } ?>

		<?php
			echo CHtml::openTag( 'table', array( 'id' => $tableId, 'class' => 'display' ) );
				echo CHtml::openTag( 'thead');
					echo CHtml::openTag( 'tr');
						echo CHtml::tag( 'th', array(), 'id');
						echo CHtml::tag( 'th', array(), $header);
					echo CHtml::closeTag( 'tr');
				echo CHtml::closeTag( 'thead');
				echo CHtml::openTag( 'tbody');
				
					foreach( $options as $key => $option ){
						echo CHtml::openTag( 'tr');
							echo CHtml::tag( 'td', array(), $key);
							echo CHtml::tag( 'td', array(), CHtml::link($option, Yii::app()->createUrl($linkRout, array( $param => $key ) ) ) );
						echo CHtml::closeTag( 'tr');
					}
				
				echo CHtml::closeTag( 'tbody');
			echo CHtml::closeTag( 'table');
		?>

	<?php if($currentId){ ?>
	</div>
	<?php } ?>

</div>
