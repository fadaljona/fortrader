<div class="exchange_rates_box">
	<div class="exchange_rates_h"><strong><?=Yii::t( $NSi18n, 'Cbr course' )?></strong></div>
	<div class="exchange_rates_ins">
		<span><strong><?=number_format( $model->todayData->value, CommonLib::getDecimalPlaces( $model->todayData->value ), ',', ' ' )?></strong></span>
		<p><?=Yii::t( $NSi18n, 'Rubles for' )?> <?=$model->todayData->nominal?></p>
	</div>
</div>
<?php
if( !$model->tomorrowData ){
	$tomorrowPrice = '--';
	$tomorrowText = Yii::t( $NSi18n, 'Tomorrow pending text' );
}else{
	$tomorrowPrice = number_format( $model->tomorrowData->value, CommonLib::getDecimalPlaces( $model->tomorrowData->value ), ',', ' ' );
	$tomorrowText = Yii::t( $NSi18n, 'Rubles for' ) . ' ' . $model->lastData->nominal;
}
?>
<div class="exchange_rates_box">
	<div class="exchange_rates_h">
		<strong><?=Yii::t( $NSi18n, 'Tomorrow price' )?></strong>
	</div>
	<div class="exchange_rates_ins">
		<span><strong><?=$tomorrowPrice?></strong></span>
		<p><?=$tomorrowText?></p>
	</div>
</div>