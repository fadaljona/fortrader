<?php
	$defaultCurrency = CurrencyRatesModel::getDefaultCurrency('ecb');
	$toValue = $defaultCurrency->todayEcbData->value / $model->todayEcbData->value;
	$fromValue = $model->todayEcbData->value / $defaultCurrency->todayEcbData->value;
?>
<?php /*
<div class="exchange_rates_box">
	<div class="exchange_rates_h"><strong><?=Yii::t( $NSi18n, 'Today Ecb course' )?></strong></div>
	<div class="exchange_rates_ins">
		<span><strong><?=number_format( $toValue, CommonLib::getDecimalPlaces( $toValue ), ',', ' ' )?></strong></span>
		<p><?=$defaultCurrency->code?> <?=Yii::t( $NSi18n, 'forOneText' )?> 1 <?=$model->code?></p>
	</div>
</div>*/?>
<div class="exchange_rates_box">
	<div class="exchange_rates_h">
		<strong><?=Yii::t( $NSi18n, 'Today Ecb course' )?></strong>
	</div>
	<div class="exchange_rates_ins">
		<span><strong><?=number_format( $fromValue, CommonLib::getDecimalPlaces( $fromValue ), ',', ' ' )?></strong></span>
		<p><?=$model->code?> <?=Yii::t( $NSi18n, 'forOneText' )?> 1 <?=$defaultCurrency->code?></p>
	</div>
</div>