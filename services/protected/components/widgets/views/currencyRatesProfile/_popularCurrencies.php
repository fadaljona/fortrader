<?php 
	$cs=Yii::app()->getClientScript();
	$formHelpersBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.assets-static.formHelpers') );
	$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers-countries.flags.css');
	
	$count = count($popularCurrencies);
	$ost = $count % 3;
	$contInColumn = ($count - $ost) / 3;
	if( $ost > 0 ) $contInColumn += 1;
?>
<!-- - - - - - - - - - - - - -  Popular currencies - - - - - - - - - - - - - - - - -->
<section class="section_offset">
	<hr class="separator2">
	<h3><?=Yii::t( $NSi18n, 'Popular currencies' )?><i class="icon popular_currencies_icon"></i></h3>
	<div class="popular_currencies clearfix">
	<?php
		$i=1;
		
		foreach( $popularCurrencies as $currency ){
			if( $contInColumn == 1 ) echo CHtml::openTag('div', array( 'class' => "popular_currencies_box" )) . CHtml::openTag('ul');
			if( $i % $contInColumn == 1 ) echo CHtml::openTag('div', array( 'class' => "popular_currencies_box" )) . CHtml::openTag('ul');
			echo CHtml::openTag('li');
				if( $currency->country ){ echo "{$currency->country->icon} "; }
				echo CHtml::link( $currency->name, $currency->singleUrl, array( 'target' => '_blank' ) );
			echo CHtml::closeTag('li');
			if( $i % $contInColumn == 0 ) echo CHtml::closeTag('ul') . CHtml::closeTag('div');
			$i++;
		}
		if( ($i-1) % $contInColumn != 0 ) echo CHtml::closeTag('ul') . CHtml::closeTag('div');
	?>
	</div>
	<div class="clearfix">
	<?php
		echo CHtml::link( 
			Yii::t( $NSi18n, 'All currencies' ), 
			$model->indexURL, 
			array( 'target' => '_blank', 'class' => 'white_btn alignright' ) 
		);
	?>
	</div>
</section>
<!-- - - - - - - - - - - - - - End of Popular currencies - - - - - - - - - - - - - - - - -->