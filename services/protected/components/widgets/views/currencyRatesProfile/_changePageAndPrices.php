<!-- - - - - - - - - - - - - - Exchange Rates - - - - - - - - - - - - - - - - -->
<div class="exchange_rates section_offset clearfix columns_3">
	<div class="exchange_rates_box">                               
		<div class="sec  exchange_rates_form">
			<select class="form_styler changeCurrencyPage" data-placeholder="<?=Yii::t( $NSi18n, 'Choose currency' )?>">
				<option label="-"></option>
			<?php
				$i=1;
				foreach( $currenciesForChangePage as $key => $val ){
					if( $model->code == $key ) continue;
					if( $i<=3 ) echo CHtml::openTag('optgroup', array( 'label' => $i, 'class' => "stationary stat_$i" ));
					echo CHtml::tag(
						'option', 
						array(
							'data-title' => $key,
							'value' => $val['link'],
						), 
						$val['name']
					);
					if( $i<=3 ) echo CHtml::closeTag('optgroup');
					$i++;
				}
			?>
			</select>
		</div><!-- .sec -->
		<div class="exchange_rates_ins">
			<span><strong><?=$model->code?></strong></span>
			<p><?=$model->name?></p>
		</div>
	</div>
	<?php require dirname( __FILE__ ).'/_topPrices_' . $model->dataType . '.php'; ?>
</div>
<!-- - - - - - - - - - - - - - End of Exchange Rates - - - - - - - - - - - - - - - - -->