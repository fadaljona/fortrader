<!-- - - - - - - - - - - - - -  information - - - - - - - - - - - - - - - - -->
<section class="section_offset">
	<hr class="separator2">
	<h3><?=Yii::t( $NSi18n, 'INFO' )?> <span class="sub_title1"> / <?=Yii::t( $NSi18n, 'about' )?> <?=$model->name?></span><i class="icon information_icon"></i></h3>
	<hr>
	<h6><strong><?=Yii::t( $NSi18n, 'CODES AND SYMBOLS' )?></strong></h6>
	<div class="wrapper">
		<table class="inside_table currencyInfo">
			<tbody>
				<?$this->controller->widget( "widgets.detailViews.CurrencyRatesInfoDetailViewWidget", Array( 'data' => $model ))?>          
			</tbody>
		</table>
	</div>
</section>
<!-- - - - - - - - - - - - - - End of  information - - - - - - - - - - - - - - - - -->