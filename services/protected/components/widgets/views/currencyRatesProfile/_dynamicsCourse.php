<!-- - - - - - - - - - - - - - Dynamics course - - - - - - - - - - - - - - - - -->
<section class="section_offset">
	<hr class="separator2">                       
	<h3>
		<?=Yii::t( $NSi18n, 'Dynamics course' )?>
		<span class="sub_title1"> / <?=$model->chartTitle?></span>
		<span class="sub_title_right"><?=$model->rightCode?></span>
	</h3>
	<hr>
	<div class="dynamics_course clearfix">
		<div class="dynamics_course_table">
		<?
			$this->widget( 'widgets.gridViews.CurrencyRatesLeftHistGridViewWidget', Array(
				'NSi18n' => $NSi18n,
				'ins' => $ins,
				'showTableOnEmpty' => true,
				'dataProvider' => $histDp,
				'ajax' => false,
				'dataType' => $model->dataType,
			));
		?>
		</div>
		<div class="chartWrapper">
			<div class="chart_box" id="chartdiv<?=$chartId?>"></div>
			<div class="spinner-wrap">
				<div class="spinner">
					<div class="rect1"></div>
					<div class="rect2"></div>
					<div class="rect3"></div>
					<div class="rect4"></div>
					<div class="rect5"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix periodChangeWrap">
		<ul class="quotes_list_sm marginBottom0 changePeriodLinks">                   
			<li><span><?=Yii::t( $NSi18n, 'Period' )?>:</span></li>
			<li><a href="#" data-val="7"><?=Yii::t( $NSi18n, 'Week' )?></a></li>
			<li><a href="#" data-val="30"><?=Yii::t( $NSi18n, 'Month' )?></a></li>
			<li><a href="#" data-val="91"><?=Yii::t( $NSi18n, 'Quarter' )?></a></li>
			<li class="active"><a href="#" data-val="365"><?=Yii::t( $NSi18n, 'Year' )?></a></li>
			<li><a href="#" data-val="1825">5 <?=Yii::t( $NSi18n, 'CurrYears' )?></a></li>
		</ul>
		<div class="spinner-wrap"></div>
	</div>
	
	<div id="1501232523211" style="margin-top:10px;"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1501232523211");s = document.createElement("script");s.src = "https://fortrader.org/services/15135sz/jsRender?id=58&insertToId=1501232523211";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
</section>
<!-- - - - - - - - - - - - - - End of Dynamics course - - - - - - - - - - - - - - - - -->