<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$languageAlias = LanguageModel::getCurrentLanguageAlias();
	
	$cs=Yii::app()->getClientScript();
	$amchartsBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.amcharts') );
	
	$cs->registerScriptFile($amchartsBaseUrl.'/amcharts.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/serial.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/plugins/export/export.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/lang/'.$languageAlias.'.js');
	$cs->registerCssFile($amchartsBaseUrl.'/plugins/export/export.css');
	
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js');
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wCurrencyRatesProfileWidget.js' ) );

	$jsls = Array(
		'lEror' => 'Error occured',
		'lPrice' => 'Price',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	$chartId = time();
?>

<div class="<?=$ins?>">
	
	<div itemscope itemtype="http://schema.org/FinancialProduct">
		<meta itemprop="name" content="<?=$model->name?>" />
		<meta itemprop="serviceType" content="Currency rate" />
		
		<?php require dirname( __FILE__ ).'/_shortDesc.php'; ?>
		<?php require dirname( __FILE__ ).'/_changePageAndPrices.php'; ?>
		<?php require dirname( __FILE__ ).'/_dynamicsCourse.php'; ?>
	</div>
	
	<?php $this->widget( 'widgets.CurrencyRatesConverterWidget', Array( 'ns' => 'nsActionView', 'currentModel' => $model, 'type' => $model->dataType )); ?>
	
	<?php if($popularCurrencies) require dirname( __FILE__ ).'/_popularCurrencies.php'; ?>
	<?php
		if( Yii::app()->language == 'ru' ){
			$cacheKey = 'currencyRatesFinanceNewsBlock' ;
			if( !Yii::app()->cache->get( $cacheKey ) ){
				ob_start();	
				CommonLib::loadWp();
				require_once Yii::App()->params['wpThemePath'].'/templates/currency-rates-finance-news.php';	
				$blockContent = ob_get_clean();
				Yii::app()->cache->set( $cacheKey, $blockContent, 60*60);
			}
			echo Yii::app()->cache->get( $cacheKey );
		}
	?>
	<?php require dirname( __FILE__ ).'/_currencyInfo.php'; ?>
</div>
<script>
!function( $ ) {
	var ns = <?=$ns?>;
		
	var ls = <?=json_encode( $jsls )?>;
				
	ns.wCurrencyRatesProfileWidget = wCurrencyRatesProfileWidgetOpen({
		selector: '.<?=$ns?> .<?=$ins?>',
		idCurrency: <?=$model->id?>,
		ls: ls,
		loadChartDataUrl: '<?=Yii::app()->createAbsoluteUrl('currencyRates/ajaxLoadChartData')?>',
		chartData: <?=json_encode($chartData)?>,
		chartSelector: 'chartdiv<?=$chartId?>',
		exportLibs: '<?=Yii::app()->getBaseUrl(true)?>/amcharts/plugins/export/libs/',
		lang: '<?=$languageAlias?>',
		label: '<?php echo $model->dataType == 'ecb' ? $model->ecbCode : $model->name; ?>',
		needToUpdateImg: <?=$model->needToUpdateImg() ? 'true' : 'false'?>,
		saveImageUrl: '<?=Yii::app()->createAbsoluteUrl('currencyRates/ajaxSaveImage');?>',
		saveViewsUrl: '<?=Yii::app()->createAbsoluteUrl('currencyRates/ajaxSaveViews');?>',
		dataType: '<?=$model->dataType?>',
	});
}( window.jQuery );
</script>