<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

Yii::app()->getClientScript()->registerScript('hideQuickMenuItems', '
!function( $ ) {
	$(".' . $ins . ' a").each(function(){
        if( $(this).attr("href") != "#" ){
            if (location.pathname.replace(/^\//,"") == this.pathname.replace(/^\//,"")&& location.hostname == this.hostname) {
                var vTarget = $(this.hash);
                vTarget = vTarget.length && vTarget || $("[name=" + this.hash.slice(1) +"]");
                if (!vTarget.length) {
                    $(this).hide();
                }
            }
        }
    });
}( window.jQuery );
', CClientScript::POS_END);
?>

<div class="crypto__menu <?=$ins?>">
    <a href="#cryptoNewsArticlesBlock" class="crypto-menu__item"><?= Yii::t($NSi18n, 'News')?></a>
    <a href="#cryptoCurrenciesBeginer" class="crypto-menu__item"><?= Yii::t($NSi18n, 'For newbie')?></a>
    <a href="#TradingWithBrokerCryptoCurrenciesWidget" class="crypto-menu__item"><?= Yii::t($NSi18n, 'Trade')?></a>
    <a href="#VideoChanelWidget" class="crypto-menu__item"><?= Yii::t($NSi18n, 'Bitcoin TV')?></a>
    <a href="#NewChatWidget" class="crypto-menu__item"><?= Yii::t($NSi18n, 'Chat')?></a>
    <a href="#FaqWidget" class="crypto-menu__item"><?= Yii::t($NSi18n, 'FAQ')?></a>
</div>

