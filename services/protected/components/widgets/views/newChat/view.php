<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>
<div class="fx_section <?=$ins?>" id="NewChatWidget">
    <div class="fx_content-title clearfix">
        <h3 class="fx_content-title-link"><?=Yii::t($NSi18n, 'Chat')?></h3>

        <span class="fx_content-subtitle"><?=Yii::t($NSi18n, 'Discussion in real time')?></span>

        <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/chat_icon.png" alt="" class="fx_title-icon">

    </div>

    <div class="fx_section-box">
        <div class="fx_chat">
            <div id="chatbro"></div>
        </div>
    </div>
</div>