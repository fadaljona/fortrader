<a data-toggle="dropdown" class="dropdown-toggle wTogglePrivateMessagesShortList" href="#">
	<i class="icon-envelope <?=$countNotViewedPrivateMessages?'icon-animated-vertical':''?>"></i>
	<?if( $countNotViewedPrivateMessages ){?>
		<span class="badge badge-success"><?=$countNotViewedPrivateMessages?></span>
	<?}?>
</a>

<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-closer wPrivateMessagesShortList">
	<li class="nav-header">
		<i class="icon-envelope"></i>
		<?=$countPrivateMessages?> <?=Yii::t( $NSi18n, 'Message|Messages', $countPrivateMessages )?>
	</li>

	<?foreach( $privateMessages as $message ){
    if(isset($message->from)) {
  ?>
		<?$urlDialog = Yii::App()->createURL( '/userPrivateMessage/dialog', Array( 'idWith' => $message->idFrom ));?>
		<li>
			<a href="<?=$urlDialog?>">
				<?=$message->from->getHTMLAvatar( Array( 'class' => 'msg-photo' ))?>
				<span class="msg-body">
					<span class="msg-title">
						<span class="blue"><?=htmlspecialchars($message->from->showName)?>:</span>
						<?$style = !$message->opened ? ' style="font-weight:bold;"' : '' ?>
						<span<?=$style?>>
							<?=$message->getHTMLText( 50 )?>
						</span>
					</span>
					<span class="msg-time">
						<?$this->widget( "widgets.parts.TimeDiffWidget", Array( 'time' => $message->createdDT ))?>
					</span>
				</span>
			</a>
		</li>
	<?}}?>
	
	<li>
		<a href="<?=Yii::App()->createUrl( '/userPrivateMessage/list' )?>">
			<?=Yii::t( $NSi18n, 'See all messages', $countPrivateMessages )?>
			<i class="icon-arrow-right"></i>
		</a>
	</li>
</ul>