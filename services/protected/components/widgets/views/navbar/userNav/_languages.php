<?
	$cs=Yii::app()->getClientScript();
	$formHelpersBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.assets-static.formHelpers') );
	$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers.css');
	$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers-countries.flags.css');
	
	$alias = strtoupper( $language );
	if( $alias == 'EN_US' ) $alias = "US";
?>
<a data-toggle="dropdown" class="dropdown-toggle" href="#">
	<i class="ficon-<?=$alias?>"></i><!--<i class="icon-caret-down" style="font-size:12px; color:#5f96b9;"></i>-->
</a>
<ul class="pull-right dropdown-menu dropdown-caret" style="min-width:40px;">
	<?foreach( $languages as $key => $title ){?>
		<?
			$alias = strtoupper( $key );
			if( $alias == 'EN_US' ) $alias = "US";
		?>
		<li>
			<a href="#" class="wChangeLanguage" data-language="<?=$key?>">
				<i class="ficon-<?=$alias?>"></i>
				<?//=$alias?>
			</a>
		</li>
	<?}?>
</ul>