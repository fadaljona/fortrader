<a data-toggle="dropdown" class="dropdown-toggle wToggleNoticesShortList" href="#">
	<i class="icon-bell-alt icon-only <?=$countNotViewedNotices?'icon-animated-bell':''?>"></i>
	<?if( $countNotViewedNotices ){?>
		<span class="badge badge-important"><?=$countNotViewedNotices?></span>
	<?}?>
</a>
<ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-closer wNoticesShortList">
	<li class="nav-header">
		<i class="icon-warning-sign"></i>
		<?=$countNotices?> <?=Yii::t( $NSi18n, 'Notification|Notifications', $countNotices )?>
	</li>

	<?$this->renderNotices()?>

	<?if( $this->issetMoreNotices() ){?>
		<li class="text-center" style="margin-bottom:10px;">
			<button class="btn btn-small btn-yellow no-radius <?=$ins?> wShowMore">
				<i class="icon-arrow-down"></i>
				<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Show more' )?></span>
			</button>
		</li>
		<li class="<?=$ins?> wLoading" style="display:none;">
			<div class="progress progress-info progress-striped active">
				<div class="bar" style="width: 100%;"></div>
			</div>
		</li>
	<?}else{?>
		<li></li>
	<?}?>
</ul>