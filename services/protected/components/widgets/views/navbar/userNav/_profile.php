<?
	$baseUrl = Yii::app()->baseUrl;
	$NSi18n = $this->getNSi18n();
	$model = Yii::App()->user->getModel();
?>
<a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle" style="padding-right:0px;">
	<?=$model->getHTMLAvatar( Array( 'class' => 'nav-user-photo' ))?>

	<span class="user-info">
		<small><?=Yii::t( $NSi18n, 'Welcome' )?>,</small>
		<?=CHtml::encode($model->showName)?>
	</span>

	<i class="icon-caret-down"></i>
</a>

<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer" id="user_menu">
	<li>
		<a href="<?=Yii::App()->createUrl( '/user/settings?tabSettings=yes' )?>">
			<i class="icon-cog"></i>
			<?=Yii::t( $NSi18n, 'Settings' )?>
		</a>
	</li>

	<li>
		<a href="<?=Yii::App()->createUrl( 'user/profile' )?>">
			<i class="icon-user"></i>
			<?=Yii::t( $NSi18n, 'Profile' )?>
		</a>
	</li>
	
	<?if( $model->countTradingAccounts ){?>
		<li>
			<a href="<?=Yii::App()->createUrl( 'user/profile?tabTA=yes' )?>">
				<i class="icon-list"></i>
				<?=Yii::t( $NSi18n, 'Trading accounts' )?>
			</a>
		</li>
	<?}?>
	
	<?if( $model->countEA ){?>
		<li>
			<a href="<?=Yii::App()->createUrl( 'user/profile?tabEA=yes' )?>">
				<i class="icon-cogs"></i>
				<?=Yii::t( $NSi18n, 'EA' )?>
			</a>
		</li>
	<?}?>

	<li class="divider"></li>

	<li>
		<a href="<?=Yii::App()->createUrl( 'default/logout' )?>">
			<i class="icon-off"></i>
			<?=Yii::t( $NSi18n, 'Logout' )?>
		</a>
	</li>
</ul>
