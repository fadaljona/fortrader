<li data-id="<?=$notice->id?>">
	<a href="<?=$notice->getLink()?>" style="white-space: normal">
		<div class="clearfix">
			<?=$notice->getIcon()?>
			<?=$notice->getHTMLMessage()?>
			<?if( $notice->type == 'invite_friend' ){?>
				<button class="btn btn-mini btn-primary wAcceptInviteFriend" data-id="<?=$notice->idObject?>">
					<?=Yii::t( 'lower', 'accept' )?>
				</button>
				<button class="btn btn-mini btn-danger wRejectInviteFriend" data-id="<?=$notice->idObject?>">
					<?=Yii::t( 'lower', 'reject' )?>
				</button>
			<?}?>
		</div>
	</a>
</li>