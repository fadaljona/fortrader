<?
	$NSi18n = $this->getNSi18n();
?>
<ul class="nav ace-nav pull-right">
	<?/*
	<li class="grey">
		<a data-toggle="dropdown" class="dropdown-toggle" href="#">
			<i class="icon-tasks"></i>
			<span class="badge badge-grey">4</span>
		</a>

		<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-closer">
			<li class="nav-header">
				<i class="icon-ok"></i>
				4 Tasks to complete
			</li>

			<li>
				<a href="#">
					<div class="clearfix">
						<span class="pull-left">Software Update</span>
						<span class="pull-right">65%</span>
					</div>

					<div class="progress progress-mini ">
						<div style="width:65%" class="bar"></div>
					</div>
				</a>
			</li>

			<li>
				<a href="#">
					<div class="clearfix">
						<span class="pull-left">Hardware Upgrade</span>
						<span class="pull-right">35%</span>
					</div>

					<div class="progress progress-mini progress-danger">
						<div style="width:35%" class="bar"></div>
					</div>
				</a>
			</li>

			<li>
				<a href="#">
					<div class="clearfix">
						<span class="pull-left">Unit Testing</span>
						<span class="pull-right">15%</span>
					</div>

					<div class="progress progress-mini progress-warning">
						<div style="width:15%" class="bar"></div>
					</div>
				</a>
			</li>

			<li>
				<a href="#">
					<div class="clearfix">
						<span class="pull-left">Bug Fixes</span>
						<span class="pull-right">90%</span>
					</div>

					<div class="progress progress-mini progress-success progress-striped active">
						<div style="width:90%" class="bar"></div>
					</div>
				</a>
			</li>

			<li>
				<a href="#">
					See tasks with details
					<i class="icon-arrow-right"></i>
				</a>
			</li>
		</ul>
	</li>
	*/?>
	<li>
		<? //require dirname( __FILE__ )."/_languages.php"?>
	</li>
	<li class="purple">
		<?require dirname( __FILE__ )."/_notices.php"?>
	</li>
	<li class="green">
		<?require dirname( __FILE__ )."/_privateMessages.php"?>
	</li>
	<li class="light-blue user-profile">
		<?require dirname( __FILE__ )."/_profile.php"?>
	</li>
</ul>