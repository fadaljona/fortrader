<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wNavbarWidget.js" );
	
	$class = $this->getCleanClassName();
	$baseUrl = Yii::app()->baseUrl;
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="navbar navbar-inverse wNavbarWidget">
	<div class="main-container container-fluid">
		<div class="navbar-inner">
			<a href="#" class="brand">
				<small>
					<i class="icon-leaf"></i>
					ForTrader.org 
				</small>
			</a>
			<?require dirname( __FILE__ )."/userNav/_view.php";?>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var countNotViewedNotices = <?=$countNotViewedNotices?>;
		var countNotViewedPrivateMessages = <?=$countNotViewedPrivateMessages?>;
		
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wNavbarWidget = wNavbarWidgetOpen({
			selector: nsText+' .wNavbarWidget',
			countNotViewedNotices: countNotViewedNotices,
			countNotViewedPrivateMessages: countNotViewedPrivateMessages,
			ls: ls,
		});
	}( window.jQuery );
</script>
