<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( $member->idContest ){
		echo CHtml::tag('hr', array('class' => 'separator2'));
	}
?>

<section class="section_offset">
	<?php
		if( $member->idContest ){
			echo CHtml::tag('h3', array(), Yii::t( $NSi18n, 'Statement' ) . CHtml::tag('i', array('class' => 'fa fa-file-text-o', 'aria-hidden' => 'true'), '') ) . 
			CHtml::tag('hr', array('class' => 'separator2'));
		}else{
			echo CHtml::tag('h4', array('class' => 'toggle_btn section_title'), Yii::t( $NSi18n, 'Statement' ));
		}
	?>


	<?php if( $member->idContest ) echo '</section><section class="section_offset">';?>
	<?php if( !$member->idContest ) echo '<div class="toggle_content">';?>

	<?php if($openDeals){ ?>
		<section class="section_offset">
			<h5 class="info_title color2"><?=Yii::t( $NSi18n, 'Open deals' )?></h5>
		</section>
		<!-- - - - - - - - - - - - - - End of Tabs Contant - - - - - - - - - - - - - - - - -->
		<!-- - - - - - - - - - - - - - End of Tabs - - - - - - - - - - - - - - - - -->
		<div class="section_offset">
			<?php $this->widget( "widgets.detailViews.ContestMemberOpenDealsDetailViewWidget", Array( 'data' => $openDeals ));?>
		</div>
	<?php }?>

	<?php if($closedDeals){ ?>
		<section class="section_offset">
			<h5 class="info_title color2"><?=Yii::t( $NSi18n, 'Closed deals' )?></h5>
		</section>
		<!-- inside_table -->
		<div class="section_offset">
			<div class="wrapper">
				<?php $this->widget( "widgets.detailViews.ContestMemberClosedDealsDetailViewWidget", Array( 'data' => $closedDeals ));?>
			</div>
		</div>
		<!-- End of the inside_table -->
	<?php }?>

	<?php if( !$member->idContest ) echo '</div>';?>
</section>