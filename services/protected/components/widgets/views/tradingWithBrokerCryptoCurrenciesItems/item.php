<?php $NSi18n = $this->getNSi18n();?>
<div class="fx_broker-item">
    <div class="fx_broker-image">
        <?=CHtml::image($model->srcImage)?>
    </div>
    <div class="fx_broker-inform">
        <div class="fx_broker-name"><?=$model->shortName?></div>
        <div class="fx_broker-descr">
            <?php if (strlen($model->minDeposit)) {?>
            <div class="fx_broker-descr-item">
                <span><?=Yii::t('*', 'Min deposit')?>:</span>
                <span><?=Yii::t('*', 'from amount')?> <?=$model->minDeposit?>$</span>
            </div>
            <?php }?>

            <?php if ($model->tradePlatforms) {?>
            <div class="fx_broker-descr-item">
                <span><?=Yii::t('*', 'Platforms')?>:</span>
                <span><?=implode(', ', CommonLib::slice($model->tradePlatforms, 'name'))?></span>
            </div>
            <?php }?>

            <?php
            if ($model->type == 'binary') {
                if (strlen($model->contract)) {?>
                    <div class="fx_broker-descr-item">
                        <span><?=Yii::t('*', 'Contract from')?>:</span>
                        <span>$<?=$model->contract?></span>
                    </div>
                <?}
            } else {
                if (strlen($model->maxLeverage)) {?>
                    <div class="fx_broker-descr-item">
                        <span><?=Yii::t('*', 'Leverage')?>:</span>
                        <span><?=$model->maxLeverage?></span>
                    </div>
                <?}
            }
            ?>

            <div class="fx_broker-descr-item">
                <span><?=Yii::t('*', 'Regulator')?>:</span>
                <span>
                <?php
                $regulatorsArr = array();
                foreach ($model->regulators as $regulator) {
                    if (strlen($regulator->link)) {
                        $regulatorsArr[] = CHtml::link(
                            CHtml::encode($regulator->name),
                            CommonLib::getURL($regulator->link),
                            array( 'target' => '_blank')
                        );
                    } else {
                        $regulatorsArr[] = CHtml::encode($regulator->name);
                    }
                }
                echo implode(', ', $regulatorsArr);
                ?>
                </span>
            </div>
        </div>
        <div class="fx_broker-btn">
        <?php
        if ($model->site) {
            echo CHtml::link(
                Yii::t('*', 'Go to the wesite'),
                $model->getRedirectURL('site'),
                array(
                    'data-broker-id' => $model->id,
                    'data-link-type' => 'site',
                    'target' => '_blank'
                )
            );
        } else {
            echo CHtml::link(
                Yii::t('*', 'Go to the broker page'),
                $model->singleURL,
                array('target' => '_blank')
            );
        }
        ?>
        </div>
    </div>
</div>