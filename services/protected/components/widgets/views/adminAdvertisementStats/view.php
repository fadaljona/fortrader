<?
	Yii::import( 'components.sys.CHtmlEx' );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wAdminAdvertisementStatsWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lBlock' => 'Block',
		'lUnBlock' => 'Unblock',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$models = Array( 
		'Ad' => 'Ads', 
		'Campaign' => 'Campaigns',
	);
	if( Yii::App()->user->checkAccess( 'advertisementZoneControl' )) {
		$models[ 'Zone' ] = 'Zones';
	}
	foreach( $models as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="iEditorWidget i01 wAdminAdvertisementStatsWidget">
	<?=Yii::t( $NSi18n, 'Select stats' )?><br>
	<?=CHtml::dropDownList( "model", $model, $models, Array( "class" => "{$ins} wSelectModel" ))?><br>
	
	<?if( $model == 'Ad' ){?>
		<?=Yii::t( $NSi18n, 'Select campaign' )?><br>
		<?=CHtml::dropDownList( "idCampaign", $idCampaign, $campaigns, Array( "class" => "{$ins} wSelectCampaign" ))?><br>
	<?}?>
	<?if( $model == 'Campaign' and $mode == 'Admin' ){?>
		<?=Yii::t( $NSi18n, 'Select broker' )?><br>
		<?=CHtml::dropDownList( "idBroker", $idBroker, $brokers, Array( "class" => "{$ins} wSelectBroker" ))?><br>
	<?}?>

	<?=Yii::t( $NSi18n, 'Begin' )?><br>
	<?=CHtmlEx::dateField( "begin", $begin, Array( "class" => "{$ins} wBegin" ))?><br>
	<?=Yii::t( $NSi18n, 'End' )?><br>
	<?=CHtmlEx::dateField( "end", $end, Array( "class" => "{$ins} wEnd" ))?><br>
	
	<?if( $model == 'Ad' and $idCampaign or ( $model == 'Campaign' and ( $mode != 'Admin' or $idBroker )) or $model == 'Zone' ){?>
		<?
			$gridViewWidget = $this->createGridViewWidget();
			$gridViewWidget->run();
		?>
	<?}?>
	<div class="wClicksContenter">
		<div class="progress progress-info progress-striped active">
			<div class="bar" style="width: 100%;"></div>
		</div>
	</div>
	<div class="modal hide <?=$ins?> wModalAddBlock">
		<div class="modal-body">
			<?
				$this->widget( 'widgets.forms.AdminAdvertisementAddBlockFormWidget', Array(
					'ns' => $ns,
				));
			?>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var model = "<?=$model?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminAdvertisementStatsWidget = wAdminAdvertisementStatsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wAdminAdvertisementStatsWidget',
			model: model,
			ls: ls,
		});
	}( window.jQuery );
</script>