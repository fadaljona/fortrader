<?
	
?>
<ul class="nav nav-pills iUl i03">
	<?$class = $this->controller->id == 'ea' && $this->controller->action->id == 'list' ? 'active' : '' ?>
	<li class="<?=$class?>">
		<a href="<?=$this->controller->createURL( 'ea/list' )?>">
			<i class="iI i22 i22_ratings"></i>
			<?=Yii::t( '*', 'EA Catalog' )?>
		</a>
	</li>
	
	<?$class = $this->controller->id == 'eaTradeAccount' ? 'active' : '' ?>
	<li class="<?=$class?>">
		<a href="<?=$this->controller->createURL( 'eaTradeAccount/list' )?>">
			<i class="iI i22 i22_monitoring"></i>
			<?=Yii::t( '*', 'EA monitoring' )?>
		</a>
	</li>

	<?/*$class = $this->controller->id == 'ea' && $this->controller->action->id == 'laboratory' ? 'active' : '' */?><!--
	<li class="<?/*=$class*/?>">
		<a href="<?/*=$this->controller->createURL( 'ea/laboratory' )*/?>">
			<i class="iI i22 i22_laboratory"></i>
			<?/*=Yii::t( '*', 'EA laboratory' )*/?>
		</a>
	</li>-->

  <li>
    <a href="<?=$this->controller->createURL( 'ea/add' )?>">
      <i class="iI i23"></i>
      <?=Yii::t( '*', 'Add EA' )?>
    </a>
  </li>
</ul>