<?php 
	Yii::App()->clientScript->registerScriptFile( '//code.jquery.com/ui/1.11.4/jquery-ui.js' );
	Yii::app()->clientScript->registerCssFile( Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css' );
	Yii::app()->clientScript->registerCssFile( Yii::App()->request->getHostInfo() . Yii::app()->params['wpThemeUrl'] .'/plagins/colorpicker/css/colorpicker.css');
	Yii::App()->clientScript->registerScriptFile( Yii::App()->request->getHostInfo() . Yii::app()->params['wpThemeUrl'] . '/plagins/colorpicker/js/colorpicker.js', CClientScript::POS_END );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wQuotesInformerSettingsWidget.js" );
	
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="wQuotesInformerSettingsWidget <?=$ins?>">

	
	<h3 class="title6"><?=Yii::t( $NSi18n, 'Informer settings' )?></h3>


	<section class="section_offset chooseColors changeSettings">
		<div class='changeSettingsLayer hide'></div>
		<h4 class="title7"><?=Yii::t( $NSi18n, 'Change colors' )?></h4>
		<div class="box2">
		
			<div class="change_color">
				<h6 class="change_color_title"><?=Yii::t( $NSi18n, 'thTextColor' )?></h6>
 				<div class="colorSelector" data-bg="<?=$defaultColors['thTextColor']?>"></div>
				<input type="hidden" name="thTextColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title">
					<?=Yii::t( $NSi18n, 'thBackgroundColor' )?>
					<?=CHtml::tag('input', array( 'class' => 'inpSettings', 'type' => 'checkbox', 'id' => 'thBackgroundOpacity', 'name' => 'thBackgroundOpacity', 'checked' => false ))?>
					<label for="thBackgroundOpacity" class="square_input small"><i></i><?=Yii::t( $NSi18n, 'thBackgroundOpacity' )?></label>
				</h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['thBackgroundColor']?>"></div>
				<input type="hidden" name="thBackgroundColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title"><?=Yii::t( $NSi18n, 'symbolTextColor' )?></h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['symbolTextColor']?>"></div>
				<input type="hidden" name="symbolTextColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title"><?=Yii::t( $NSi18n, 'tableTextColor' )?></h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['tableTextColor']?>"></div>
				<input type="hidden" name="tableTextColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title"><?=Yii::t( $NSi18n, 'borderTdColor' )?></h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['borderTdColor']?>"></div>
				<input type="hidden" name="borderTdColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title"><?=Yii::t( $NSi18n, 'tableBorderColor' )?></h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['tableBorderColor']?>"></div>
				<input type="hidden" name="tableBorderColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title"><?=Yii::t( $NSi18n, 'oddBackgroundTrColor' )?></h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['oddBackgroundTrColor']?>"></div>
				<input type="hidden" name="oddBackgroundTrColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title"><?=Yii::t( $NSi18n, 'evenBackgroundTrColor' )?></h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['evenBackgroundTrColor']?>"></div>
				<input type="hidden" name="evenBackgroundTrColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title"><?=Yii::t( $NSi18n, 'profitTextColor' )?></h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['profitTextColor']?>"></div>
				<input type="hidden" name="profitTextColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title">
					<?=Yii::t( $NSi18n, 'profitBackgroundColor' )?>
					<input type="checkbox" id="profitBackgroundOpacity" name="profitBackgroundOpacity" class="inpSettings">
					<label for="profitBackgroundOpacity" class="square_input small"><i></i><?=Yii::t( $NSi18n, 'profitBackgroundOpacity' )?></label>
				</h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['profitBackgroundColor']?>"></div>
				<input type="hidden" name="profitBackgroundColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title"><?=Yii::t( $NSi18n, 'lossTextColor' )?></h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['lossTextColor']?>"></div>
				<input type="hidden" name="lossTextColor" class="inpSettings">
			</div>

			<div class="change_color">
				<h6 class="change_color_title">
					<?=Yii::t( $NSi18n, 'lossBackgroundColor' )?>
					<input type="checkbox" id="lossBackgroundOpacity" name="lossBackgroundOpacity" class="inpSettings">
					<label for="lossBackgroundOpacity" class="square_input small"><i></i><?=Yii::t( $NSi18n, 'lossBackgroundOpacity' )?></label>
				</h6>
				<div class="colorSelector" data-bg="<?=$defaultColors['lossBackgroundColor']?>"></div>
				<input type="hidden" name="lossBackgroundColor" class="inpSettings">
			</div>
			<!-- - - - - - - - - - - - - - End of Change Color - - - - - - - - - - - - - - - - -->
		</div>
	</section>
	
	<!-- - - - - - - - - - - - - - End of Change Color Box - - - - - - - - - - - - - - - - -->
	
	<?php 
		$informerSymbols = array();
		$informerSymbolsCatIds = array();
		foreach( $quotesCats as $cat ){
			
			echo CHtml::openTag('section', array( 'class' => 'section_offset chooseSymbols changeSettings', 'data-cat-id' => $cat->id ));
				echo CHtml::tag('div', array( 'class' => 'changeSettingsLayer hide' ), '' );
				echo CHtml::tag('h4', array( 'class' => 'title7' ), Yii::t( $NSi18n, 'Choose symbols from category' ) . ' ' . $cat->name );
				echo CHtml::openTag('div', array( 'class' => 'box2' ));
				
				foreach( $cat->symbols as $symbol ){
					if( $symbol->toInformer ){
						$informerSymbols[] = $symbol->id;
						$informerSymbolsCatIds[$cat->id] = $cat->id;
					} 
					$name = $symbol->nalias ?  $symbol->nalias : $symbol->name;
					echo CHtml::tag(
						'div', 
						array( 'class' => 'checkbox1' ), 
							CHtml::tag('input', array( 'class' => 'inpSettings', 'type' => 'checkbox', 'data-id' => $symbol->id, 'id' => 'symbol' . $symbol->id, 'checked' => $symbol->toInformer ?  true : false )) . 
							CHtml::tag(
								'label', 
								array( 'for' => 'symbol' . $symbol->id, 'class' => 'square_input'), 
								CHtml::tag('i', array(), ' ') . $name
							)
					);
				}
			
				echo CHtml::closeTag('div');
			echo CHtml::closeTag('section');
		}
	?>
	
	<!-- - - - - - - - - - - - - - Columns Show - - - - - - - - - - - - - - - - -->
	<section class="section_offset chooseColumns changeSettings">
		<div class='changeSettingsLayer hide'></div>
		<h4 class="title7"><?=Yii::t( $NSi18n, 'Select columns' )?></h4>
		<div class="box2">
			<div class="checkbox_box">
				<input type="checkbox" id="columnAsk" name="Ask" class="inpSettings">
				<label for="columnAsk" class="square_input"><i></i><?=Yii::t( $NSi18n, 'Ask' )?></label>
			</div>
			<div class="checkbox_box">
				<input type="checkbox" id="columnBid" name="Bid" checked="checked" class="inpSettings">
				<label for="columnBid" class="square_input"><i></i><?=Yii::t( $NSi18n, 'Bid' )?></label>
			</div>
			<div class="checkbox_box">
				<input type="checkbox" id="columnOpen" name="OpenQuote" class="inpSettings">
				<label for="columnOpen" class="square_input"><i></i><?=Yii::t( $NSi18n, 'OpenQuote' )?></label>
			</div>
			<div class="checkbox_box">
				<input type="checkbox" id="columnHigh" name="HighQuote" class="inpSettings">
				<label for="columnHigh" class="square_input"><i></i><?=Yii::t( $NSi18n, 'HighQuote' )?></label>
			</div>
			<div class="checkbox_box">
				<input type="checkbox" id="columnLow" name="LowQuote" class="inpSettings">
				<label for="columnLow" class="square_input"><i></i><?=Yii::t( $NSi18n, 'LowQuote' )?></label>
			</div>
			<div class="checkbox_box">
				<input type="checkbox" id="columnChg" name="Chg" class="inpSettings">
				<label for="columnChg" class="square_input"><i></i><?=Yii::t( $NSi18n, 'Chg' )?></label>
			</div>
			<div class="checkbox_box">
				<input type="checkbox" id="columnChgPer" name="ChgPer" class="inpSettings">
				<label for="columnChgPer" class="square_input"><i></i><?=Yii::t( $NSi18n, 'ChgPer' )?></label>
			</div>
			<div class="checkbox_box">
				<input type="checkbox" id="columnTime" name="Time" class="inpSettings">
				<label for="columnTime" class="square_input"><i></i><?=Yii::t( $NSi18n, 'Time' )?></label>
			</div>
		</div>
	</section>
	
	<section class="section_offset chooseFontSizeAndPaddings changeSettings">
		<div class='changeSettingsLayer hide'></div>
		<h4 class="title7"><?=Yii::t( $NSi18n, 'Change font size and paddings' )?></h4>
		<div class="box2">
			<div class="sliderVal"></div>	
		</div>
	</section>

	<div class="section_offset changeSettings">
		<div class='changeSettingsLayer hide'></div>
		<div class="get_code_box">
			<div class="agreement_box1">
				<div class="checkbox_box">
					<input type="checkbox" id="disableRealTime" name="disableRealTime" class="inpSettings">
					<label for="disableRealTime" class="square_input"><i></i><?=Yii::t( $NSi18n, 'Disable real time update' )?></label>
				</div>	
				<div class="clear"></div>
				<div class="checkbox_box">
					<input type="checkbox" id="hideGetInformerBtn" name="hideGetInformerBtn" class="inpSettings" checked="checked">
					<label for="hideGetInformerBtn" class="square_input"><i></i><?=Yii::t( $NSi18n, 'Hide get informer button' )?></label>
				</div>
			</div>
			<div class="align_center">
				<a href="#" class="edition_btn getCodeBtn"><?=Yii::t( $NSi18n, 'Get the code' )?></a>
			</div>
			<div class="get_code">
				<h3 class="title6"><?=Yii::t( $NSi18n, 'Get the code' )?>:</h3>
				<textarea class="getCodeTextarea" cols="30" rows="10"></textarea>
			</div>
		</div>
	</div>

	<section class="section_offset">
		<h3 class="title6"><?=Yii::t( $NSi18n, 'Preview' )?>:</h3>

		<div class="previewBlock">
			<?php
				$informerCat ='';
				if( count( $informerSymbolsCatIds ) == 1 ){
					$informerCat = current($informerSymbolsCatIds);
				}
			?>
			<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;" src="<?=Yii::app()->createAbsoluteUrl('quotesNew/getInformer', array(
				'symbols' => implode(',', $informerSymbols),
				'cat' => $informerCat,
			) );?>"></iframe>
		</div>

	</section>
	<!-- - - - - - - - - - - - - - End of Columns Show - - - - - - - - - - - - - - - - -->
	
</div>

<script>
var settingsInformerWidget;
	!function( $ ) {
		var ns = ".<?=$ns?>";
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";

		settingsInformerWidget = ns.wQuotesInformerSettingsWidget = wQuotesInformerSettingsWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wQuotesInformerSettingsWidget',
			getInformerUrl: '<?=CommonLib::httpsProtocol( Yii::app()->createAbsoluteUrl('quotesNew/getInformer' ) );?>',

		});
	}( window.jQuery );
</script>