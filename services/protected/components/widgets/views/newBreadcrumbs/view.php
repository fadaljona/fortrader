<?
	$baseUrl = Yii::app()->baseUrl;
	$NSi18n = $this->getNSi18n();
?>
<div class="clearfix">
	<div class="breadcrumbs alignleft">
		<ul class="breadcrumb clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
			<?php
			$i=1;
			foreach( $items as $i=>$item ){
				
					$label = Yii::t( $NSi18n, $item->label );
				?>
				<li>
					<?if( @$item->url ){?>
						<?$url = CHtml::normalizeUrl( $item->url )?>
						<span itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement">
							<a href="<?=$url?>" itemprop="item">
								<span itemprop="name"><?=$label?></span>
								<meta content="<?=$i?>" itemprop="position">
							</a>
						</span>
					<?}else{?>
						<span itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement">
							<a itemprop="item" title="<?=$label?>" href="<?=Yii::App()->request->getUrl()?>">
								<span class="breadcrumb-active" itemprop="name"><?=$label?></span>
							</a>
							<meta content="<?=$i?>" itemprop="position">
						</span>
					<?}?>
				</li>
			<? $i++; } ?>
		</ul>
	</div>
	<?php if($views){ ?>
	<div class="article_info clearfix alignright">
		<span class="views"><?=$views?></span>
	</div>
	<?php } ?>
</div>