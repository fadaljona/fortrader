<?php
Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/assets-static/fileinput/css/fileinput.css" );
Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/assets-static/fileinput/js/fileinput.js" );
?>

<?=$model->getAttributeLabel($field)?>
<input id="<?=$field?>" class="file" type="file">

<?=$form->hiddenField( $model, $field, Array( 'class' => "{$ins} w{$field}" ))?>
<br />

<script>
$('#<?=$field?>').fileinput({
	dropZoneEnabled: false,
	showRemove: false,
	showUpload: false,
	showCancel: false,
	showZoom: false,
	fileActionSettings: {
		removeIcon: '<i class="fa fa-trash text-error"></i> &nbsp;',
		showZoom: false,
		uploadIcon: '<i class="fa fa-upload text-success"></i> &nbsp;',
	},
	uploadUrl: '<?=Yii::app()->createUrl('admin/uploadImages/ajaxImageUpload')?>',
	allowedFileExtensions : ['jpg', 'png','gif'],
	uploadExtraData:{
		nameToSave: '<?=$imgName?>'
	},
<?php if($model->{$field}) echo CommonLib::getInitialPreview($model->{$field}); ?>

});
$('#<?=$field?>').on('fileuploaded', function(event, data, previewId, index) {
    var response = data.response;
	if( response.error != '' ) return false;
	var inpField = $('#<?=$field?>').closest('form').find(".<?=$ins?>.w<?=$field?>");
	inpField.val(response.img);
});
$('#<?=$field?>').on('filedeleted', function(event, key) {
	$('#<?=$field?>').closest('form').find(".<?=$ins?>.w<?=$field?>").val('');
});
</script>