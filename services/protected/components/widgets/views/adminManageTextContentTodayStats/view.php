<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="alert alert-info wAdminManageTextContentTodayStats <?=$ins?>">
	
<?php
	foreach( $todayStats as $stat ){
		echo Yii::t($NSi18n, 'Today added symbols') . ' ' . LanguageModel::getByID( $stat->lang )->name . ' ' . $stat->dayLength . '<br />';
	}
?>

</div>