<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();

  if(isset($activity->user)) {
?>
<div class="profile-activity clearfix iRelative" idActivity="<?=$activity->id?>">
	<div class="iDiv i47">
    <?=$activity->user->getHTMLAvatar( Array( 'class' => 'pull-left' ))?>
	</div>
	<div class="iDiv i48">
		<?=$activity->getIcon()?>
		<a class="user" href="<?=$activity->user->getProfileURL()?>"><?=CHtml::encode($activity->user->showName)?></a>
		<?=$activity->message?><br>
		<?$this->widget( 'widgets.parts.TimeDiffWidget', Array( 'time' => $activity->createdDT ))?>
	</div>
</div>

<? } ?>