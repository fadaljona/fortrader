<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.lists').'/wCategoryRatingWithFilterWidget.js' ), CClientScript::POS_END );

?>

<div class="<?=$ins?> position-relative spinner-margin" id="id<?=$ins?>">

		

	<!-- - - - - - - - - - - - - - Search - - - - - - - - - - - - - - - - -->       
	<div class="quotes_search_box">
		<form>
			<button class="quotes_search_btn"><?=Yii::t($NSi18n, 'Search')?> <i class="fa fa-search"></i></button>
			<input name="searchField" placeholder="<?=Yii::t($NSi18n, 'Search...')?>" type="search" value="<?=$searchTerm?>">
		</form>
	</div>
	<!-- - - - - - - - - - - - - - End of Search - - - - - - - - - - - - - - - - -->	


	<div class="CategoryRatingList">
		<!-- - - - - - - - - - - - - - Tabl Quotes - - - - - - - - - - - - - - - - -->
		<section class="section_offset turn_box">
			<?php /*<div class="tabl_quotes_header clearfix">
			<?php if( $childCats ){ ?>
				<div class="tabl_quotes_btn alignright nav_buttons">
					<a href="javascript:;" class="options_btn customBtnHandler">
						<i class="fa fa-sliders"></i>
						<span class="tooltip"><?=yii::t($NSi18n, 'Settings')?></span>
					</a>
				</div>
			<?php }?>
				<h4 class="tabl_quotes_title"><?=$rating->tableTitle?> <?php if($rating->tableSubTitle) echo '<br><span>'.$rating->tableSubTitle.'</span>';?></h4>
			<?php if( $childCats ){ ?>
				<div class="options_box customBtnHandler">
					<h6 class="options_title"><?=Yii::t($NSi18n, 'Choose categories')?></h6>
					<div class="clearfix">
						<div class="options_item">
							<?php
								$checkedCat = '';
								if( in_array( $rating->catId, $selectedCategories ) ) $checkedCat = 'checked=""';
							?>
							<input value="<?=$rating->catId?>" id="option<?=$rating->catId?>" <?=$checkedCat?> type="checkbox">
							<label for="option<?=$rating->catId?>"><?=Yii::t($NSi18n, 'Uncategorized')?></label>
						</div>
						<?php
							foreach( $childCats as $cat ){
								$checkedCat = '';
								if( in_array( $cat->term_id, $selectedCategories ) ) $checkedCat = 'checked=""';
								echo '<div class="options_item">
								<input value="'.$cat->term_id.'" id="option'.$cat->term_id.'" '.$checkedCat.' type="checkbox">
								<label for="option'.$cat->term_id.'">'.$cat->name.'</label>
							</div>';
							}
						?>                                   
					</div>
				</div>
			<?php }?>
			</div>*/?>
		
			<div class="turn_content">
				<div class="tabl_quotes tabl_inner_contest tabl_inner_sortable">
					<?
						$gridWidget = $this->widget( 'widgets.gridViews.CategoryRatingGridViewWidget', Array(
							'NSi18n' => $NSi18n,
							'ins' => $ins,
							'showTableOnEmpty' => truew,
							'dataProvider' => $DP,
							'ajax' => $ajax,
							'template' => '{items}',
							'pagerCssClass' => 'pagination pagination-right margin-top-10 navigation_box clearfix',
							'sortField' => $sortField,
							'sortType' => $sortType,
							'ratingCatId' => $rating->catId,
							'catsCount' => count( $childCats ),
							'pager' => array(
								'class'=> 'widgets.gridViews.base.WpPager'
							),
						));
					?>
				</div>
			</div>
			<?if( $DP->pagination->getPageCount() > 1 ){?>
				<?=$gridWidget->renderPager()?>
			<?}?>
		
			
			
		</section>
		<!-- - - - - - - - - - - - - - End of Tabl Quotes - - - - - - - - - - - - - - - - -->
	</div>
</div>


<?if( !Yii::App()->request->isAjaxRequest ){
	
Yii::app()->clientScript->registerScript('CategoryRatingWithFilterWidgetJs', '

	!function( $ ) {
		var ns = ' . $ns . ';
		var nsText = ".' . $ns . '";
		var ins = ".' . $ins . '";
		var ajax = true ;
		var hidden =  false;
		var sortField = "' . $sortField . '";
		var sortType = "' . $sortType . '";
		var ls = ' . json_encode( $jsls ) . ';
			
		ns.wCategoryRatingWithFilterWidget = wCategoryRatingWithFilterWidgetOpen({
			ns: ns,
			ins: ins,
			insid: "id'.$ins.'",
			selector: nsText+" .'.$ins.'",
			ajax: ajax,
			nsText: nsText,
			hidden: hidden,
			ls: ls,
			sortField: sortField,
			sortType: sortType,
			ajaxLoadListURL: "' . Yii::app()->createAbsoluteUrl("categoryRatings/ajaxLoadList") . '",
			ajaxAddMarkURL: "' . Yii::app()->createUrl('categoryRatings/ajaxAddMark') . '",
			ratingId: '.$rating->id.',
		});
	}( window.jQuery );

', CClientScript::POS_END);
	
}?>