<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="<?=$ins?>" >
	<hr class="separator2">
	<div class="section_offset">
		<h3><?=Yii::t( $NSi18n, 'Last Edition' )?> <?=$model->journalDate?></h3>
		<hr class="separator2">
	</div>
	<div class="clearfix section_offset">
		<div class="image_box alignleft"><a href="<?=$model->singleURL?>"><?=$model->getCover(Array( 'width' => 243 ))?></a></div>
		<div class="wrapper">
			<ul class="list8">
				<?php
				$inner = $model->getCurrentInner();
				if( !$inner ){
					if($this->beginCache('wp_journal_4_posts_list', array('duration'=>60*60))) {
						CommonLib::loadWp();
						$r = new WP_Query( array(
							'post_status'  => 'publish',
							'post_type' => 'post',
							'orderby' => 'date',
							'order'   => 'DESC',
							'posts_per_page' => 4,
							'meta_query' => array(
												array(
													'key'     => 'journal',
													'value'   => $model->number,
													'compare' => '=',
												),
							),
						) );
					?>
					<?php
						if ($r->have_posts()) :
							while ( $r->have_posts() ) : $r->the_post(); ?>
								<li>
									<a href="<?php the_permalink();?>"><?php the_title();?> <?php if( get_comments_number() ){?> <span class="red_color"><?php comments_number( '', '(1)', '(%)' ); ?></span><?php } ?></a>
								</li>
						<?php 
							endwhile;
							wp_reset_postdata();
						endif;
						
					$this->endCache(); }
				}else{
					foreach( $inner as $i=>$_inner ){
						if( isset($_inner['url']) && $_inner['url'] ){
							$headerInner = CHtml::link( $_inner['header'], $_inner['url'] );
						}else{
							$headerInner = $_inner['header'];
						}
						echo CHtml::openTag('li');
							echo $headerInner;
						echo CHtml::closeTag('li');
					}
				}
				?>
			</ul>
			<div class="clearfix">
				<?php
					if($model->digest){
						echo CHtml::link( Yii::t( $NSi18n, 'Watch Digest' ), $model->digest, array('class' => 'watch_btn', 'target' => '_blank') );
					}
					if( $buyLink && !$model->free ){
						$priceSuffix = '';
						if( $model->price ) $priceSuffix = " {$model->price}\$";
						echo CHtml::link( 
							Yii::t( $NSi18n, 'Buy Journal' ) . $priceSuffix, 
							$buyLink, 
							array('class' => 'download_pdf_btn', 'target' => '_blank') 
						);
					}elseif($model->getDownloadJournalURL()){
						echo CHtml::link( 
							Yii::t( $NSi18n, 'Download PDF Journal' ), 
							$model->getDownloadJournalURL(), 
							array('class' => 'download_pdf_btn') 
						);
					}
					?>
			</div>
		</div>
	</div>
</div>