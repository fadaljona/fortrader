<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wStarsWidget.js" );
	
	$ns = $this->getNS();
	$width = round($currentValue * 100);
?>
<div class="wStarsWidget">
	<div class="wCurrentValue" style="width:<?=$width?>%"></div>
	<?foreach( $values as $value ){?>
		<div class="wValue" data-value="<?=$value?>"></div>
	<?}?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var onChangeValue = <?=$onChangeValue ? '"'.$onChangeValue.'"' : 'null'?>;
				
		ns.wStarsWidget = wStarsWidgetOpen({
			ns: ns,
			selector: nsText+' .wStarsWidget',
			onChangeValue: onChangeValue,
		});
	}( window.jQuery );
</script>