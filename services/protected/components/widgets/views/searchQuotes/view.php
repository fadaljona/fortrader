<?php 
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wSearchQuotesWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>

<div class="<?=$ins?>">
	<!-- - - - - - - - - - - - - - Search - - - - - - - - - - - - - - - - -->       
	<div class="quotes_search_box">
		<form>
			<button class="quotes_search_btn"><?=Yii::t( $NSi18n, 'Search' )?> <i class="fa fa-search"></i></button>
			<input type="search" name="searchField" id="searchField" placeholder="<?=Yii::t( $NSi18n, 'Search for quotes' )?>">
			<ul class="quotes_search_list ">
			</ul>
		</form>
	</div>
	<!-- - - - - - - - - - - - - - End of Search - - - - - - - - - - - - - - - - -->	
</div>

<script>
!function( $ ) {
	var ns = ".<?=$ns?>";
	var nsText = ".<?=$ns?>";
	var ins = ".<?=$ins?>";
	var minInputChars = <?=$minInputChars ? $minInputChars : 0?>;

	ns.wSearchQuotesWidget = wSearchQuotesWidgetOpen({
		ns: ns,
		ins: ins,
		selector: nsText+' .<?=$ins?>',
		minInputChars: minInputChars,
		ajaxSubmitURL: '<?=Yii::app()->createAbsoluteUrl('quotesNew/ajaxSearch');?>'
	});
}( window.jQuery );
</script>