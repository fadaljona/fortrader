<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>

<div class="fx_section turn_box crypto-turn_box <?=$ins?>">

<div class="fx_content-title">
    
    <h3 class="fx_content-title-link"><?= Yii::t($NSi18n, 'Social networks')?></h3>

    <?php /*<div class="nav_buttons fx_slider-nav alignright">
        <a href="#" class="prev_btn fx_slider-prev">
            <span class="tooltip">Предыдущий</span>
        </a>
        <a href="#" class="next_btn fx_slider-next">
            <span class="tooltip">Следующий</span>
        </a>
    </div>*/?>

</div>
    
<div class="fx_slider-wr owl-carousel post_carousel" data-desktop="2" data-large="2" data-medium="2" data-small="1" data-extra-small="1">

    <?php if(Yii::app()->language == 'ru') { ?>
    <div>
        <!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
        <figure class="post">
            <div class="fx_social-post">
                <a class="twitter-timeline"  href="https://twitter.com/hashtag/%D0%B1%D0%B8%D1%82%D0%BA%D0%BE%D0%B8%D0%BD" data-widget-id="988284956567891968">#биткоин Tweets</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>
        </figure>
        <!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
    </div>
    <div>
        <!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
        <figure class="post">
            <div class="fx_social-post">
                <a class="twitter-timeline"  href="https://twitter.com/hashtag/%D0%BA%D1%80%D0%B8%D0%BF%D1%82%D0%BE%D0%B2%D0%B0%D0%BB%D1%8E%D1%82%D0%B0" data-widget-id="988285688142495744">#криптовалюта Tweets</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>
        </figure>
        <!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
    </div>
    <?php } else {?>
    <div>
        <!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
        <figure class="post">
            <div class="fx_social-post">
            <a class="twitter-timeline"  href="https://twitter.com/hashtag/bitcoin" data-widget-id="988300905136128000">#bitcoin Tweets</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>
        </figure>
        <!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
    </div>
    <div>
        <!-- - - - - - - - - - - - - - Post - - - - - - - - - - - - - - - - -->
        <figure class="post">
            <div class="fx_social-post">
            <a class="twitter-timeline"  href="https://twitter.com/hashtag/cryptocurrencies" data-widget-id="988301027601502208">#cryptocurrencies Tweets</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>
        </figure>
        <!-- - - - - - - - - - - - - - End of Post - - - - - - - - - - - - - - - - -->
    </div>
    <?php }?>

</div>

</div>