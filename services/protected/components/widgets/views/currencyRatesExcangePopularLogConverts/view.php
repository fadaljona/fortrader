<?php
	Yii::App()->clientScript->registerCoreScript( 'jquery.ui' );
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/autobahn.min.js' ) );
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wCurrencyRatesExcangePopularLogWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$quotesSettings = QuotesSettingsModel::getInstance();
?>

<div class="exchange-rate-bars-container clearfix <?=$ins?>">

	<div id="1501232523211" style="margin-bottom:10px;"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1501232523211");s = document.createElement("script");s.src = "https://fortrader.org/services/15135sz/jsRender?id=58&insertToId=1501232523211";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
<?php
	if( $populars ){
		echo CHtml::openTag('ul', array( 'class' => "exchange-links" ));
			echo CHtml::tag('li', array( 'class' => 'exchange-links-header' ), Yii::t( $NSi18n, 'Popular queries' ) );
			foreach( $populars as $popular ){
				echo CHtml::tag('li', array(), CHtml::link( $popular->modelTitle, $popular->singleUrl ) );
			}
		echo CHtml::closeTag('ul');
	}
	if($logs) {
		echo CHtml::openTag('ul', array( 'class' => "exchange-links histLog" ));
			echo CHtml::tag('li', array( 'class' => 'exchange-links-header' ), Yii::t( $NSi18n, 'Query history' ) );
			foreach( $logs as $log ){
				if( $log->convert ) echo CHtml::tag('li', array(), CHtml::link( $log->convert->modelTitle, $log->convert->singleUrl ) );
			}
		echo CHtml::closeTag('ul');
	}
?>
</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
	
		ns.wCurrencyRatesExcangePopularLogWidget = wCurrencyRatesExcangePopularLogWidgetOpen({
			ns: ns,
			lang: '<?=Yii::app()->language?>',
			connUrl: '<?=$quotesSettings->wsServerUrl?>',
			wsKey: 'currencyRatesConverterLog',
			selector: '.<?=$ns?> .<?=$ins?>',
		});
	}( window.jQuery );
</script>