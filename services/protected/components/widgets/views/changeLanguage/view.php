<?
	Yii::app()->clientScript->registerCoreScript( 'cookie' );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wChangeLanguageWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	$languageCookieVarName = Yii::App()->params[ 'languageCookieVarName' ];
?>
<div class="control-group">
	<label class="control-label"><?=Yii::t( $NSi18n, 'Change language' )?></label>
	<div class="controls i01">
		<?=CHtml::dropDownList( "language", $language, $languages, Array( "class" => "{$ins} wSelectLanguage" ))?>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var insText = ".<?=$ins?>";
		var nsText = ".<?=$ns?>";
		var languageCookieVarName = "<?=$languageCookieVarName?>";
		
		ns.wChangeLanguageWidget = wChangeLanguageWidgetOpen({
			selector: nsText+' '+insText+'.wSelectLanguage',
			cookieVarName: languageCookieVarName,
		});
	}( window.jQuery );
</script>