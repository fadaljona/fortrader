<?
	$controller_action = Yii::app()->controller->id.'/'.Yii::app()->controller->action->id;
	if( $controller_action == 'contestMember/single' )
		$itCurrentUser = Yii::App()->user->isGuest ? false : isset($accountModel->idUser) && Yii::App()->user->id == $accountModel->idUser;
?>
<script>
	var nsActionView = {};
	var nsActionView2 = {};
</script>
<div class="nsActionView">
    <div class="page-header position-relative row-fluid">
		<?php
			switch ($controller_action) {
				case 'contestMember/single':
					$ContestMemberGraphArgs = Array(
						'ns' => 'nsActionView',
						'contestMember' => $accountModel,

					);
					$TradesListArgs = Array(
						'ns' => 'nsActionView',
						'ajax' => true,
 
					);
					$TradesListWidget = 'widgets.lists.ContestMemberTradesListWidget';
					$OrdersHistoryListWidget = 'widgets.lists.ContestMemberOrdersHistoryListWidget';
					$DealsHistoryListWidget = 'widgets.lists.ContestMemberDealsHistoryListWidget';
				?>
					<h1>
						<?=htmlspecialchars( $accountModel->user->showName )?> - <?=htmlspecialchars( $accountModel->accountNumber )?>
					</h1>
					<p>
						<?=Yii::t( $NSi18n, 'Account Information. Member {contest}.', Array( '{contest}' => $accountModel->contest->currentLanguageI18N->name ))?>
					</p>

					<div>
						<?
						$this->widget( 'widgets.CountersWidget', Array(
								'counters' => Array( 'Ya.share' ),
						));
						?>
					</div>
				<?php
					break;
				case 'eaTradeAccount/single':
					$ContestMemberGraphArgs = Array(
						'ns' => 'nsActionView',
						'accountNumber' => $accountModel->tradeAccount->accountNumber,
						'idServer' => $accountModel->tradeAccount->idServer,
					);
					$TradesListArgs = Array(
						'ns' => 'nsActionView',
						'ajax' => true,
						'idAccount' => $accountModel->idTradeAccount,
					);
					$TradesListWidget = 'widgets.lists.EATradeAccountTradesListWidget';
					$OrdersHistoryListWidget = 'widgets.lists.EATradeAccountOrdersHistoryListWidget';
					$DealsHistoryListWidget = 'widgets.lists.EATradeAccountDealsHistoryListWidget';
				?>
					<div class="pull-left">
						<h1>
							<?=htmlspecialchars( $accountModel->EA->name )?>.
							<?=Yii::t( '*', "Version" )?> <?=htmlspecialchars( $accountModel->version->version )?>
							- <?=htmlspecialchars( $accountModel->tradeAccount->accountNumber )?>
						</h1>
						<?=Yii::t( '*', "Test ea {linkEAVersion} on {typeAccount} account with config {linkConfig}", Array(
							'{linkEAVersion}' => CHtml::link( CHtml::encode( "{$accountModel->EA->name} {$accountModel->version->version}" ), $accountModel->version->getSingleURL() ),
							'{typeAccount}' => Yii::t( $NSi18n, strtolower( $accountModel->tradeAccount->type )),
							'{linkConfig}' => $accountModel->statement ? CHtml::link( CHtml::encode( "№{$accountModel->statement->id}" ), $accountModel->statement->getSingleURL() ) : '',
						))?>
					</div>
					<div class="pull-right">
						<?if( $accountModel->broker ){?>
							<div class="text-center iDiv i49">
								<?=Yii::t( '*', 'Sponsor EA Testing' )?><br>
								<?=Chtml::link( $accountModel->broker->officialName, $accountModel->broker->getSingleURL() )?>
							</div>
						<?}?>
					</div>
				<?php
					break;
			}
		?>
        
    </div>

	<?if( ($controller_action == 'contestMember/single') && $itCurrentUser ) {?>
		<? require dirname( __FILE__ )."/_error.php"; ?>
	<?}?>
	
	<?php
		$ContestMemberGraphWidget = '';
		ob_start();
		$this->widget( 'widgets.graphs.ContestMemberGraphWidget', $ContestMemberGraphArgs);
		$ContestMemberGraphWidget = ob_get_clean();
	?>
	
	<div class="row-fluid iDiv i03 <?php if( !$ContestMemberGraphWidget ) echo 'table-with-ads';?>">
		<?php if( !$ContestMemberGraphWidget ) echo '<div class="ads-table-row">';?>
		<div class="span3">
			<h3 class="header smaller lighter blue"><?=Yii::t( $NSi18n, 'Account Information' )?></h3>
			<?
				switch ($controller_action) {
					case 'contestMember/single':
						$this->widget( 'widgets.lists.ContestMemberInfoVerticalListWidget', Array(
							'ns' => 'nsActionView',
							'ajax' => true,
						));
						break;
					case 'eaTradeAccount/single':
						$this->widget( 'widgets.lists.EATradeAccountInfoListWidget', Array(
							'ns' => 'nsActionView',
							'idAccount' => $accountModel->id,
						));
						break;
				}
			?>
		</div>
		<div class="vspace"></div>
		<div class="span9">
			<div class="row-fluid">
			<?
				if($controller_action == 'contestMember/single'){
					echo $linklist;
					echo '</br>';
				}
				echo $ContestMemberGraphWidget;
			?>
			</div>
			<div class="row-fluid <?php if( !$ContestMemberGraphWidget ) echo 'ads-slot';?>">
			<?
				$this->widget( 'widgets.ads.AdversingBoxWidget', Array(
					'ns' => 'nsActionView',
					'zoneURL' => Yii::app()->controller->createUrl('advertisementZone/jsRender', array('id'=>12,'format'=>'html')),
				));
			?>
			</div>
		</div>
		<?php if( !$ContestMemberGraphWidget ) echo '</div>';?>
	</div>
	<div class="row-fluid iDiv i03">
		<?
			$this->widget( 'widgets.UsersConversationWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
			));
		?>
	</div>
	

	
    <?php
    if($accountModel) {
    ?>
	<div class="row-fluid iDiv i03">
		<h3 class="header smaller lighter blue"><?=Yii::t( $NSi18n, 'Trading Activity' )?></h3>
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tabTrade" data-toggle="tab"><?=Yii::t( $NSi18n, 'Trade' )?></a></li>
			<?
			$tabOrdersHistoryCond = ($controller_action == 'eaTradeAccount/single') || ( ($controller_action == 'contestMember/single') && ($accountModel->server->tradePlatform->name != "MetaTrader 4") );

			if( $tabOrdersHistoryCond ){?>
				<li><a href="#tabOrdersHistory" data-toggle="tab"><?=Yii::t( $NSi18n, 'Orders history' )?></a></li>
			<?}?>
			<li><a href="#tabDealsHistory" data-toggle="tab"><?=Yii::t( $NSi18n, 'Deals History' )?></a></li>
			
			<?php if($controller_action == 'contestMember/single'){
				$checkAccountMonitorData = MT4AccountMonitorModel::hasData( $accountModel->accountNumber, $accountModel->idServer );
				if($checkAccountMonitorData){?>
				<li><a href="#tabAccountMonitor" data-toggle="tab"><?=Yii::t( $NSi18n, 'Account Monitoring' )?></a></li>
			<? }
			} ?>
		
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tabTrade">
				<?
					$contestMemberTradesListWidget = $this->widget( $TradesListWidget, $TradesListArgs);
				?>
			</div>
			<?if( $tabOrdersHistoryCond ){?>
				<div class="tab-pane" id="tabOrdersHistory">
					<?
						$this->widget( $OrdersHistoryListWidget, $TradesListArgs);
					?>
				</div>
			<?}?>
			<div class="tab-pane" id="tabDealsHistory">
				<?
					$this->widget( $DealsHistoryListWidget, $TradesListArgs);
				?>
			</div>
			<?php if($controller_action == 'contestMember/single'&&$checkAccountMonitorData){?>
				<div class="tab-pane" id="tabAccountMonitor">
					<?
						$this->widget( 'widgets.lists.ContestMemberAccountMonitorListWidget', Array(
							'ns' => 'nsActionView',
							'ajax' => true,
							'memberId' => $accountModel->id,
						));
					?>
				</div>
			<? } ?>
			
		</div>
		<?if( !$contestMemberTradesListWidget->DP->getTotalItemCount()){?>
			<script>
				!function( $ ) {
					$( 'a[href="#tabDealsHistory"]' ).tab( 'show' );
				}( window.jQuery );
			</script>
		<?}?>
	</div>
    <?
    }
    ?>
	<?php if( $controller_action=='contestMember/single' ){ ?>
		<div class="row-fluid iDiv i03">
			<?php
				$this->widget( 'widgets.ContestMemberStatementWidget', Array(
					'ns' => 'nsActionView',
					'idMember' => $accountModel->id
				));
			?>
		</div>
		<div class="row-fluid iDiv i03">
			<?php
				$this->widget( 'widgets.graphs.ContestMemberDealsGraphWidget', Array(
					'ns' => 'nsActionView',
					'contestMemberId' => $accountModel->id,
					'ajax' => false,
				));
				
			?>
		</div>
	<?php } ?>
	
	<div class="row-fluid" style="margin-top:20px;">
		<?
			$this->widget( 'widgets.PostInformWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
	</div>
</div>