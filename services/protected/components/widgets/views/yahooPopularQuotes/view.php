<?php // if($yahooQuotes && Yii::App()->user->id == 1  ){?>
<section class="section_offset">
	<h3><?=$title?></h3>
	<?php
		$i=0;
		foreach( $yahooQuotes as $yahooQuote ){
			if( $i ) echo ", ";
			echo CHtml::link( CHtml::encode( $yahooQuote->name ), $yahooQuote->singleURL );
			$i++;
		}
		echo "<br />";
		echo CHtml::link( Yii::t("*", 'Full other quotes list'), Yii::app()->createUrl('quotesNew/otherQuotes'), array( 'class' => 'red_color' ) );
	?>
	
</section>
<?php // }?>