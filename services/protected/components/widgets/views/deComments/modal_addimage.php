<div id="deco_modal_overlay" class="deco_modal_overlayBG" onclick="decom.closeModal(); return false;"></div>
<div id="deco_modal_window">
	<div id="deco_modal_title">
		<div id="deco_modal_ajaxWindowTitle"><?=$decomSettings['texts']['Add a picture']?></div>
		<div id="deco_modal_closeAjaxWindow">
			<a href="#" id="deco_modal_closeWindowButton" onclick="decom.closeModal(); return false;">
				<div class="deco_modal-close-icon"><img class="svg" src="/wp-content/plugins/decomments/templates/decomments/assets/images/svg/close_modal.svg"/></div>
			</a>
		</div>
	</div>
	<div id="deco_modal_ajaxContent">
		<div class="decomments-popup-style">
			<form enctype="multipart/form-data" method="post" action="" id="decomments-add-picture-form" class="decomments-add-picture-form">

				<div class="decomments-load-img">
					<img src="" alt="" />
				</div>

				<div class="decomments-addfile-field" >
					<input type="file" name="decom_pictures[]" />
					<span class="decomments-addfile-cover"><?=$decomSettings['texts']['Choose file']?></span>
				</div>
				<button class="decomments-button decomments-button-addfile-send"><?=$decomSettings['texts']['Submit']?></button>
				<button onclick="decom.closeModal(); return false;" class="decomments-button decomments-button-addfile-cancel"><?=$decomSettings['texts']['Cancel']?></button>
                <button onclick="decom.removeAttachment(this); return false;" class="decomments-button decomments-button-del-image"><i class="icon-bin"></i></button>

			</form>
		</div>
	</div>
</div>