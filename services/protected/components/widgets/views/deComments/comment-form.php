<?php
echo '<span id="decom_default_position_form_add" style="display: none"></span>';

if ( $decomSettings['option_show_avatars'] ) {
	$da_position = $settings['display_avatars_right'] ? ' decomments-avatar-right' : '';
} else {
	$da_position = ' no-avatar';
}
?>
<div class="decomments-form-add-comment decomments-block-form decomments-addform<?php echo $da_position; ?>">
	<div class="decomments-social-login-widget">
		<?php if( !Yii::App()->user->id ) require_once Yii::App()->params['wpThemePath'].'/templates/wsl-comment-widget.php'; ?>
	</div>
	<div class="decomments-addform-title">
		<h3><?=$decomSettings['texts']['Add comment']?></h3>
	</div>
	<?php if ( ! Yii::App()->user->id ) {echo $login_form;} ?>
	<div class="decomments-addform-head"<?php if ( Yii::App()->user->id ) : echo ' data-full="short"'; endif; ?>>
		<?php if ( $decom_settings['output_subscription_rejoin'] || $decom_settings['output_subscription_comments'] ) { ?>
			<?php if ( Yii::App()->user->id ) { ?>
				<a class="decomments-logout-link" href="<?=Yii::App()->createUrl( 'default/logout', array('returnURL' => Yii::App()->request->getUrl()) )?>"><?=$decomSettings['texts']['Log out']?></a>
			<?php } ?>
			<nav class="decomments-subscribe-block">
				<span class="decomments-subscribe-show"><i class="decomments-icon-quick-contacts-mail"></i><?=$decomSettings['texts']['Subscribe']?></span>
				<span class="decomments-subscribe-links">
					<?php $subcribe_block = '';
					if ( $decom_settings['output_subscription_rejoin'] ) {
						$active = $settings['mark_subscription_rejoin'] ? ' active' : '';
						$subcribe_block .= '<a class="decomments-checkbox' . $active . '" href="javascript:void(0)" id="subscribe_my_comment">' . $decomSettings['texts']['Replies to my comments'] . '</a>';
					}
					if ( $decom_settings['output_subscription_comments'] ) {
						$active = $settings['mark_subscription_comments'] ? ' active' : '';
						
						if( Yii::app()->user->id ){
							$currentUser = UserModel::model()->findByPk(Yii::app()->user->id);
							
							$rout = Yii::app()->controller->id . '/' . Yii::app()->controller->action->id;
							
							if( $rout == 'monitoring/single' || $rout == 'contestMember/single' ){
								$member = ContestMemberModel::model()->findByPk( Yii::app()->request->getParam('id') );
							}
							
							if( $rout == 'user/single' && Yii::app()->request->getParam('slug') == $currentUser->user_nicename ){
								if( !$currentUser->settings || $currentUser->settings->receiveCommentsOnMyProfile ){
									$active = ' disabled active';
								}else{
									$active = ' disabled ';
								}
							}elseif( ($rout == 'monitoring/single' || $rout == 'contestMember/single') && $member->idUser == $currentUser->ID ){
								if( !$currentUser->settings || $currentUser->settings->receiveCommentsOnMyMonitoringAccounts ){
									$active = ' disabled active';
								}else{
									$active = ' disabled ';
								}
							}else{
								$postMetaModel = WPPostmetaModel::model()->find(array(
									'condition' => " `t`.`post_id` = :postId AND `t`.`meta_key` = '_decom_subscribers' ",
									'params' => array( ':postId' => $post_id ),
								));
								
								if( $postMetaModel ){
									$subscribedEmails = $postMetaModel->meta_value;
									if( $subscribedEmails ){
										$subscribedEmails = unserialize( $subscribedEmails );
									
										if( is_array( $subscribedEmails ) && in_array( $currentUser->user_email, $subscribedEmails ) ){
											$active = ' active';
										}
									}
								}
							}

							
							
						}
						
						$subcribe_block .= '<a class="decomments-checkbox' . $active . '" href="javascript:void(0)" id="subscribe_all_comments">' . $decomSettings['texts']['All comments'] . '</a>';
					}
					echo $subcribe_block; ?>
				</span>
			</nav>
		<?php } ?>
		<nav class="descomments-form-nav">
			<a title="<?=$decomSettings['texts']['Add a quote']?>" class="decomments-add-blockquote " onclick="decom.showModal(this,jQuery('.decomments-comment-section').attr('data-modal-quote')); return false;"><i class="decomments-icon-format-quote"></i></a>

			<a title="<?=$decomSettings['texts']['Add a picture']?>" class="decomments-add-image " onclick="decom.showModal(this,jQuery('.decomments-block-form-wrapper').attr('data-modal-addimage')); return false;" data-width="500" data-height="120" ><i class="decomments-icon-insert-photo"><img class="svg" src="/wp-content/plugins/decomments/templates/decomments/assets/images/svg/photo.svg" alt="photo" width="28" height="23" /></i></a>

		</nav>
	</div>
	<div class="decomments-addform-body"<?php if ( Yii::App()->user->id ) : echo ' data-full="short"'; endif; ?>>
		<?php if ( Yii::App()->user->id ) : echo $login_success; endif; ?>
		<textarea rows="5" cols="30" class="decomments-editor"></textarea>
		<div class="decomments-commentform-message">
			<i class="decomments-icon-warning"></i>
			<span><?=$decomSettings['texts']['Sorry, you must be logged in to post a comment.']?></span>
		</div>
		<div class="decomments-loading"><div class="loader-ball-scale">
				<div></div>
				<div></div>
				<div></div>
			</div></div>
		<button class="decomments-button decomments-button-send"><?=$decomSettings['texts']['Submit']?></button>
		<button class="decomments-button decomments-button-cancel"><?=$decomSettings['texts']['Cancel']?></button>
	</div>
</div>
<div class="decomments-form-popup">
	<div id="decom_alert_void-block" class="decomments-popup-style" style="display:none;">
		<div class="decomments-popup-style">
			<div id="decom-alert-void-text" class="decom-popup-box decom-quote-box">
				<p></p>
			</div>
		</div>
	</div>
</div>
