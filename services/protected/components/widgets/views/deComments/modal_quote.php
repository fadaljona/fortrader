<div id="deco_modal_overlay" class="deco_modal_overlayBG" onclick="decom.closeModal(); return false;"></div>
<div id="deco_modal_window">
	<div id="deco_modal_title">
		<div id="deco_modal_ajaxWindowTitle"><?=$decomSettings['texts']['Add a quote']?></div>
		<div id="deco_modal_closeAjaxWindow" onclick="decom.closeModal(); return false;">
			<a href="#" id="deco_modal_closeWindowButton">
				<div class="deco_modal-close-icon"><img class="svg" src="/wp-content/plugins/decomments/templates/decomments/assets/images/svg/close_modal.svg"/></div>
			</a>
		</div>
	</div>
	<div id="deco_modal_ajaxContent">
		<div class="decomments-popup-style">

			<div id="decomments-add-blockquote-form">

				<textarea></textarea>

				<button class="decomments-button decomments-button-quote-send" ><?=$decomSettings['texts']['Submit']?></button>
				<button class="decomments-button decomments-button-quote-cancel" onclick="decom.closeModal(); return false;"><?=$decomSettings['texts']['Cancel']?></button>

			</div>

		</div>
	</div>
</div>