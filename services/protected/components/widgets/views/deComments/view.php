<script>
	var chartToImg = <?=$chart?>;
</script>
<?php
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/decom.js' ), CClientScript::POS_END );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$decom_settings = $decomSettings['decom_get_options'];
	$settings = $decom_settings;
?>
	<div class="decomments-comment-section decomments-block-form-wrapper"
		 data-modal-alert='<?php include 'modal_alerts.php'; ?>'
		 data-modal-addimage='<?php include 'modal_addimage.php'; ?>'
		 data-modal-quote='<?php include 'modal_quote.php'; ?>'
		 data-modal-preview='<?php include 'modal_preview.php'; ?>'
		 data-modal-sub='<?php include 'modal_subscribe.php'; ?>'
		>
		<script type="text/javascript">
			var decom_post_id = "<?php echo $post_id; ?>";
			var decom_user_id = "<?php echo Yii::App()->user->id; ?>";
			var decom_plugin_url = "<?php echo '/wp-content/plugins/decomments/components/comments/assets'; ?>";
			var decom_position_form = "<?php echo $settings['comment_form_up'] ? 1 : 0; ?>";
			var decom_comment_single_translate = "<?=$decomSettings['texts']['decom_comment_single_translate']?>";
			var decom_comment_twice_translate = "<?=$decomSettings['texts']['decom_comment_twice_translate']?>";
			var decom_comment_plural_translate = "<?=$decomSettings['texts']['decom_comment_plural_translate']?>";
		</script>

		<?php
		$social_iсon   = '';
		$login_success = '';
		if ( Yii::App()->user->id ) {
			
			$display_round_avatars_class = '';
			$login_success .= '<figure class="decomments-user-thumb' . $display_round_avatars_class . '">';
			$login_success .= '<img src="'.$userModel->getSizeAvatar( 80, 80 ).'" title="'.$userModel->showName.'" alt="'.$userModel->showName.'" />';
			$login_success .= '</figure>';
			
		} else {
			$login_form = '';
			ob_start();
			?>
			<div class="decomments-enter-row">
				<form id="decomments-enterform" class="decomments-enterform" action="#" method="post">
					<fieldset>
						<div class="de-form-field">
							<label for="decom-name-author"><?=$decomSettings['texts']['Name']?><em>*</em></label>
							<input id="decom-name-author" type="text" value="<?=$decomSettings['texts']['decom-name-author-val']?>" />
						</div>
						<div class="de-form-field">
							<label for="decom-mail-author"><?=$decomSettings['texts']['E-mail']?><em>*</em></label>
							<input id="decom-mail-author" type="text" value="<?php echo $_COOKIE['decommentse']; ?>" />
						</div>
						<?php if ( $settings['enable_field_website'] == 1 ) { ?>
							<div class="de-form-field">
								<label for="decom-site-author"><?=$decomSettings['texts']['Website']?></label>
								<input id="decom-site-author" type="text" value="<?php echo $_COOKIE['decommentsu']; ?>" />
							</div>
						<?php } ?>
					</fieldset>
					<p class="decomments-enterform-message decomments-error-message">
						<i class="decomments-icon-warning"></i>
						<?=$decomSettings['texts']['E-mail is already registered']?>
						<a href="#" id="decomments-show-loginform"><?=$decomSettings['texts']['login form']?></a>
						<?=$decomSettings['texts']['or']?>
						<a href="#" id="decomments-show-enterform"><?=$decomSettings['texts']['enter another']?></a>.
					</p>
				</form>
				<form id="decomments-loginform" class="decomments-loginform" action="<?php echo '/wp-login.php' ?>" method="post">
					<p class="decomments-loginform-message decomments-error-message">
						<i class="decomments-icon-warning"></i> <?=$decomSettings['texts']['incorrect username or password']?>
					</p>
					<fieldset>
						<div class="de-form-field">
							<input type="text" name="log" id="log" value="" size="20" />
							<label for="log"><?=$decomSettings['texts']['Name']?>:</label>
						</div>
						<div class="de-form-field">
							<input type="password" name="pwd" id="pwd" size="20" />
							<label for="pwd"><?=$decomSettings['texts']['Password']?>:</label>
						</div>
					</fieldset>
					<button type="submit" class="decomments-button decomments-button-submit"><?=$decomSettings['texts']['Log in']?></button>
					<input id="submit-form" type="hidden" name="submit" />
					<?=$decomSettings['action_login_form']?>
					<input type="hidden" name="redirect_to" value="" />
				</form>
			</div>
			<?php
			$login_form = ob_get_contents();
			ob_end_clean();
		}
		include_once( 'comment-form.php' );
	echo "</div>";


if ($userModel && $userModel->userHaveWpRoles(['editor', 'administrator'])) {
    $sufix = 'admin';
} else {
    $sufix = 'other';
}

$commmentsListCacheKey = 'commmentsList' . $post_id . $sufix . Yii::app()->language;
if( !Yii::app()->cache->get($commmentsListCacheKey) ){
	ob_start();
	CommonLib::loadWp();
	global $wp_query;
	global $post;
	$post = get_post($post_id);
	echo '<div class="decom_dop_bloc">';
	if ( isset( $decom_settings['show_avatars'] ) && intval( $decom_settings['show_avatars'] ) ) {
		$da_position = $decom_settings['display_avatars_right'] ? ' decomments-avatar-right' : '';
	} else {
		$da_position = ' no-avatar';
	}
	$current_user_id = DECOM_Loader_MVC::getComponentModel( 'comments', 'user' )->getCurrentUserId();
	$model_comments = DECOM_Loader_MVC::getComponentModel( 'comments', 'comments' );
	$model_votes    = DECOM_Loader_MVC::getComponentModel( 'comments', 'comments-votes' );
	//print_r( $decom_settings );
	$display_round_avatars = isset( $decom_settings['display_round_avatars'] ) ? intval( $decom_settings['decom_display_round_avatars'] ) : 0;
	$comments_per_page     = - 1;
	$is_comments_open      = comments_open();
	$comments_per_page     = ( isset( $decom_settings['page_comments'] ) && intval( $decom_settings['page_comments'] ) && isset( $decom_settings['comments_per_page'] ) ) ? intval( $decom_settings['comments_per_page'] ) : 0;
	$all_comments          = $model_comments->getComments( $post_id );
	if ( $all_comments ) {
		$all_comments_compact = $model_votes->filter_array_comments( $all_comments );
		$user_voice           = $model_votes->user_voice_like( $all_comments_compact );
	} else {
		$user_voice = array();
	}
	$max_comments_votes_1  = array();
	$max_comments_votes_2  = array();
	$comments_branch_1     = array();
	$comments_branch_2     = array();
	$comments_branch_merge = array();
	//$user_sort             = getUserSort( $current_user_id );
	$user_sort = 'newer';
	if ( $decom_settings['show_two_comments_highest_ranking_top_list'] ) {
		$max_comments_votes = $model_comments->getPostMaxCommentKarma( $post_id, $decom_settings['best_comment_min_likes_count'], $user_sort );
		if ( array_key_exists( 0, $max_comments_votes ) ) {
			$comments_branch_1    = $model_comments->getCommentsBranchByParentId( $post_id, $max_comments_votes[0]->comment_ID );
			$max_comments_votes_1 = $model_comments->getCommentsByIds( $comments_branch_1 );
		}
		if ( array_key_exists( 1, $max_comments_votes ) ) {
			$comments_branch_2    = $model_comments->getCommentsBranchByParentId( $post_id, $max_comments_votes[1]->comment_ID );
			$max_comments_votes_2 = $model_comments->getCommentsByIds( $comments_branch_2 );
		}
		if ( is_array( $comments_branch_1 ) && is_array( $comments_branch_2 ) ) {
			$comments_branch_merge = array_merge( $comments_branch_1, $comments_branch_2 );
		} else {
			if ( is_array( $comments_branch_1 ) ) {
				$comments_branch_merge = $comments_branch_1;
			}
		}
	}
	$comments = $model_comments->getComments( $post_id, $comments_branch_merge );
	if ( $comments ) {
		$pages_count = get_comment_pages_count( $comments, $comments_per_page );
	} else {
		$pages_count = 0;
	}
	?>

	<div id="decomments-comment-section" class="decomments-comment-section">
	<?php
		$number        = get_comments_number($post_id);
		$have_comments = $number > 0 ? true : false;
		if ( $have_comments ) { ?>
			<div class="decomments-head">
				<?php
				if ( $settings['output_total_number_comments_top'] ) {
					$display = ( $number == 0 ) ? ' decomments-hide' : ' decomments-block';
					$comment_str = '<h3><i><span class="decomments-comments-number' . $display . '">' . $number . '</span><span class="decomments-comment-title">' . ' ' . _n( 'comment', 'comments', $number, DECOM_LANG_DOMAIN ) . '</span></i></h3>';
					echo $comment_str;
				}
				?>
				<form class=" decom_comments_sort decomments-comment-sort" action="#comments" method="post" <?php echo $have_comments ? '' : 'style="display:none;"'; ?>>
					<label><?php esc_html_e( 'Sort', DECOM_LANG_DOMAIN ); ?>:</label>
					<dl class="de-select de-select-filter">
						<dt>
						<?php
							if( $user_sort == 'rate' ) esc_html_e( 'by Best', DECOM_LANG_DOMAIN );
							if( $user_sort == 'newer' ) esc_html_e( 'by Newest', DECOM_LANG_DOMAIN );
							if( $user_sort == 'older' ) esc_html_e( 'by Oldest', DECOM_LANG_DOMAIN );
						?>
						</dt>
						<dd id="decom_comments_sort" class="decomments-select">
							<a onclick="decom.sortComments(this)" <?php if( $user_sort == 'rate' ) echo 'class="current"'; ?> href="#" data-sort="rate">
								<i class="decomments-icon-thumb-down"></i> <?php esc_html_e( 'by Best', DECOM_LANG_DOMAIN ); ?>
							</a>
							<a onclick="decom.sortComments(this)" <?php if( $user_sort == 'newer' ) echo 'class="current"'; ?> href="#" data-sort="newer">
								<i class="decomments-icon-thumb-down"></i> <?php esc_html_e( 'by Newest', DECOM_LANG_DOMAIN ); ?>
							</a>
							<a onclick="decom.sortComments(this)" <?php if( $user_sort == 'older' ) echo 'class="current"'; ?> href="#" data-sort="older">
								<i class="decomments-icon-thumb-down"></i> <?php esc_html_e( 'by Oldest', DECOM_LANG_DOMAIN ); ?>
							</a>
						</dd>
					</dl>
				</form>
			</div>
			<div class="decomments-comment-list<?php echo $da_position; ?>">
				<div class="loader-ball-scale lbs-remove">
					<div></div>
					<div></div>
					<div></div>
				</div>
				<?php
				if ( count( $max_comments_votes_1 ) > 0 ) {
					wp_list_comments( array(
						'callback'     => 'decom_render_comment',
						'end-callback' => 'decom_end_comment',
						'style'        => 'div',
						'walker'       => DECOM_Loader_MVC::getComponentClass( 'comments', 'comments-walker' ),
						'settings'     => $settings,
						'votes'        => $votes,
						'user_voice'   => $user_voice,
						'max'          => true
					), $max_comments_votes_1 );
				}
				if ( count( $max_comments_votes_2 ) > 0 ) {
					wp_list_comments( array(
						'callback'     => 'decom_render_comment',
						'end-callback' => 'decom_end_comment',
						'style'        => 'div',
						'walker'       => DECOM_Loader_MVC::getComponentClass( 'comments', 'comments-walker' ),
						'settings'     => $settings,
						'votes'        => $votes,
						'user_voice'   => $user_voice,
						'max'          => true
					), $max_comments_votes_2 );
				}
				wp_list_comments( array(
					'callback'          => 'decom_render_comment',
					'end-callback'      => 'decom_end_comment',
					'style'             => 'div',
					'walker'            => DECOM_Loader_MVC::getComponentClass( 'comments', 'comments-walker' ),
					'settings'          => $settings,
					'votes'             => $votes,
					'user_voice'        => $user_voice,
					'reverse_top_level' => $user_sort,
					'per_page'          => $comments_per_page,
					'page'              => $current_comments_page ? $current_comments_page : '1'
				), $comments );
				?>
			</div><!-- .commentlist -->
			<?php
			$deco_ajax_navy = $settings['deco_ajax_navy'];
			if ( $deco_ajax_navy ) {
				if ( $deco_ajax_navy == 'deco_show_more_comments_prevnext' ) { ?>
					<nav id="comment-nav-below" class="navigation decomments-navigation" role="navigation" data-page="<?php echo $current_comments_page; ?>" data-pages-count="<?php echo $pages_count; ?>">
						<?php otherPagination( $pages_count, $post_id, $current_comments_page ); ?>
					</nav>
					<?php
				} else if ( $deco_ajax_navy == 'deco_show_more_comments_lazy' && $pages_count > 1 ) { ?>
					<div class="decomments-ajax-paginate-lazy" data-cur-page="1" data-comments-perpage="<?php echo $comments_per_page; ?>" data-page-count="<?php echo $pages_count; ?>" data-page="2" data-pages-count="<?php echo $pages_count; ?>">
						<div class="decomments-paginate-loader" style="display: none;">
							<div class="loader-ball-scale">
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
						<a href="#" class="decomments-button decomments-loader-btn"><?php esc_html_e( 'Load more', DECOM_LANG_DOMAIN ); ?>
							<i class="decomments-icon-angle-double-right"></i></a>
					</div>
					<?php
				} else if ( $deco_ajax_navy == 'deco_show_more_comments_onebutton' && $pages_count > 1 ) { ?>
					<div class="decomments-ajax-paginate" data-cur-page="1" data-comments-perpage="<?php echo $comments_per_page; ?>" data-page-count="<?php echo $pages_count; ?>" data-page="2" data-pages-count="<?php echo $pages_count; ?>">
						<div class="decomments-paginate-loader" style="display: none;">
							<div class="loader-ball-scale">
								<div></div>
								<div></div>
								<div></div>
							</div>
							<span style="position: relative;top: -11px;"><?php esc_html_e( 'Loading comments..', DECOM_LANG_DOMAIN ); ?></span>
						</div>
						<a href="#" class="decomments-button decomments-loader-btn"><?php esc_html_e( 'Load more', DECOM_LANG_DOMAIN ); ?>
							<i class="decomments-icon-angle-double-right"></i></a>
					</div>
					<?php
				}
			}
		} // have_comments()
	?>
	</div>
	</div>
	<?php
	$commmentsList = ob_get_clean();
	Yii::app()->cache->set($commmentsListCacheKey, $commmentsList);
}
echo Yii::app()->cache->get($commmentsListCacheKey);
?>