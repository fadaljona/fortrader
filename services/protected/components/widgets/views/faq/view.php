<?php
$cs=Yii::app()->getClientScript();

$handorgelUrl = Yii::app()->getAssetManager()->publish(Yii::App()->params['wpThemePath'] . '/plagins/handorgel-master/lib');
$cs->registerCssFile($handorgelUrl.'/css/handorgel.min.css');
$cs->registerScriptFile($handorgelUrl.'/js/umd/handorgel.min.js', CClientScript::POS_END);
$cs->registerScript('jShandorgel', '
!function( $ ) {
	var accordion = new handorgel(document.querySelector(".fx_accordion"), {
        multiSelectable: false
    });
}( window.jQuery );
', CClientScript::POS_END);

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
?>

<div class="fx_section <?=$ins?>" id="FaqWidget">
    <div class="fx_content-title clearfix">
        <h3 class="fx_content-title-link"><?=$faq->title?></h3>
        <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/ask_icon.png" alt="" class="fx_title-icon">
    </div>
    <div class="fx_section-box">
        <div class="fx_accordion">
        <?php foreach ($models as $index => $model) {?>
            <h3 class="handorgel__header">
                <button class="handorgel__header__button"><?=$model->getPropertyWithReplace('question')?></button>
            </h3>
            <div class="handorgel__content" <?= $index==0 ? 'data-open' : '' ?>>
                <div class="handorgel__content__inner"><?=$model->getPropertyWithReplace('answer')?></div>
            </div>
        <?php }?>
        </div>
    </div>
</div>