<?php

$cs = Yii::app()->getClientScript();

$jscrollpaneBaseUrl = Yii::app()->getAssetManager()->publish(Yii::App()->params['wpThemePath'] . '/plagins/jscrollpane');
$cs->registerScriptFile($jscrollpaneBaseUrl.'/js/jquery.mousewheel.js', CClientScript::POS_END);
$cs->registerScriptFile($jscrollpaneBaseUrl.'/js/jquery.jscrollpane.min.js', CClientScript::POS_END);
$cs->registerCssFile($jscrollpaneBaseUrl.'/css/jquery.jscrollpane.css');
$cs->registerScript('jScrollPane', '
!function( $ ) {
    $(".js_scrolling").jScrollPane({
        autoReinitialise: true
    });
    if($(document).width() <= 767) {
        $(".wide_scroll").removeClass("js_scrolling");
    }
}( window.jQuery );
', CClientScript::POS_END);

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

?>
<div class="fx_section-box clearfix <?= $ins ?>">
    <div class='chartAndList'>
        <?php $this->widget('widgets.CryptoCurrenciesChartWidget', array('ns' => $ns));?>
        <?php $this->widget('widgets.lists.CryptoCurrenciesPageListWidget', array('ns' => $ns));?>
        <div class="crypto__btn">
            <a href="/quotes/cryptorates" target="_blank" class="crypto-currencies__btn">
                <?= Yii::t($NSi18n, 'All crypto-currencies')?>
                <i class="crypto-btn__icon"></i>
            </a>
        </div>
    </div>

    <?php $this->widget('widgets.lists.CryptoExchangeListWidget', array('ns' => $ns));?>
</div>