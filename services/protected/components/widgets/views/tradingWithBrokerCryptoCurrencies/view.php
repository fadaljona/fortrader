<?php
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
if (!$ajax) {
    Yii::app()->clientScript->registerScriptFile(
        CHtml::asset(
            Yii::getPathOfAlias('webroot.js.widgets').'/wTradingWithBrokerCryptoCurrenciesWidget.js'
        )
    );
}

if (!$ajax) {
?>
<div class="fx_section <?=$ins?>" id="TradingWithBrokerCryptoCurrenciesWidget">
    <div class="fx_content-title clearfix">
        <h3 class="fx_content-title-link"><?= Yii::t($NSi18n, 'Trading with the Broker') ?></h3>
        <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/broker_icon.png" alt="" class="fx_title-icon">
    </div>

    <div class="fx_section-box">
        <div class="fx_broker-box clearfix">
<?php }?>


            <?php $this->widget('widgets.TradingWithBrokerCryptoCurrenciesItemsWidget', array('dp' => $DP));?>


<?php if (!$ajax) {?>
        </div>
        <?php if($DP->pagination->getPageCount() > 1) {?>
        <div class="fx_broker-more">
            <a href="#" data-page="1" data-pages="<?=$DP->pagination->getPageCount()?>"><?= Yii::t($NSi18n, 'Show more brokers') ?></a>
        </div>
        <?php } ?>
    </div>
</div>
<script>
!function( $ ) {
    var ns = ".<?=$ns?>";
    ns.wTradingWithBrokerCryptoCurrenciesWidget = wTradingWithBrokerCryptoCurrenciesWidgetOpen({
        selector: '.<?= $ins ?>',
        loadDataUrl: '<?= Yii::app()->createUrl('broker/ajaxLoadTradingWithBrokerCryptoCurrencies') ?>',
    });
}( window.jQuery );
</script>
<?php }?>