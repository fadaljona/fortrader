<?php 
	$languageAlias = LanguageModel::getCurrentLanguageAlias();
	
	$cs=Yii::app()->getClientScript();
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/autobahn.min.js' ), ClientScript::POS_TOP_HEAD );
	
	$amchartsBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.amcharts') );
	$cs->registerScriptFile($amchartsBaseUrl.'/amcharts.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/serial.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/plugins/export/export.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/lang/'.$languageAlias.'.js');
	$cs->registerCssFile($amchartsBaseUrl.'/plugins/export/export.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/download2.js' ) );
	
	$tradingviewChartingLibraryBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.tradingviewChartingLibrary') );
	$cs->registerScriptFile($tradingviewChartingLibraryBaseUrl.'/charting_library.min.js');
	$cs->registerScriptFile($tradingviewChartingLibraryBaseUrl.'/datafeed/udf/datafeed.js');

	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wQuoteChartsWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	if( $model->nalias ){
		$nName = $model->nalias;
	}else{
		$nName = $model->name;
	}
	$periodChartId = time();
	$tickChartId = $periodChartId+1;
	$tradingviewChartId = $periodChartId+2;
?>

<?php if( $preview ){?>
<div class="section_offset">
	<?=$preview?>
</div>
<?php }?>

<div class="<?=$ins?> quoteChartsWrap">

<!-- - - - - - - - - - - - - - Quotes List - - - - - - - - - - - - - - - - -->
	<div class="section_offset">
		<?php /*<div class="dropdown_box PFDregular">
			<div class="dropdown_active dropdown_btn"><?=Yii::t( $NSi18n, 'Charts' )?> <i class="fa fa-angle-down"></i></div>                                  
			<ul class="quotes_list clearfix dropdoun">
				<li class="active"><a href="#"><?=Yii::t( $NSi18n, 'Charts' )?></a></li>
				<li><a href="#"><?=Yii::t( $NSi18n, 'Technical analysis' )?></a></li>
				<li><a href="#"><?=Yii::t( $NSi18n, 'News and Analysis' )?></a></li>
				<li><a href="#"><?=Yii::t( $NSi18n, 'Discussion' )?></a></li>
				<li><a href="#"><?=Yii::t( $NSi18n, 'Brokers' )?></a></li>
			</ul>
		</div>*/?>
		<?php if($periods){?>
		<ul class="quotes_list1 clearfix selectPeriodType">
			<li data-type="amcharts"><a data-type="amcharts" href="#"><?=Yii::t( $NSi18n, 'AmCharts Chart' )?></a></li>
			<li data-type="technical" class="active"><a data-type="technical" href="#"><?=Yii::t( $NSi18n, 'Technical Chart' )?></a></li>
		</ul>
		<?php }?>
	</div>
	<!-- - - - - - - - - - - - - - End of Quotes List - - - - - - - - - - - - - - - - -->

	<!-- - - - - - - - - - - - - - Quotes List - - - - - - - - - - - - - - - - -->
	<div class="section_offset">
		<h2 class="page_title2"><?=$model->chartTitle ? $model->chartTitle : Yii::t( $NSi18n, 'Chart' ) . ' – ' . $nName;?></h2>
		<ul class="overview_list clearfix PFDregular selectChartType hide <?php if( $model->sourceType == 'yahoo' ) echo "hide"; ?>">
		<?php 
			foreach( $periods as $period => $periodData ){ 
				if( isset( $periodData['data'] ) ){
					$defaultPeriod = $period;
				}
		?>		
			<li class="changePeriod <?=isset( $periodData['data'] ) ? 'active' : ''?>" data-period="<?=$period?>"><a href="#"><?=$period?></a></li>
		<?php } ?>
		<?php if($model->tickName){ ?>
			<li class="tick tickChart"><a href="#tick">Tick</a></li>
		<?php } ?>
		</ul>
		<div class="overview_block <?php /*if(!$periods)*/ echo 'hide';?>" id="chartdiv<?=$periodChartId?>"></div>
		<div class="overview_block <?php /*if($periods)*/ echo 'hide';?>" id="chartdiv<?=$tickChartId?>"></div>
		<div class="overview_block techGraphwrapper <?php /*if($periods) echo 'hide'*/;?>">
			<div class="tech_graph <?php /*if($periods) echo 'hide';*/?>" id="chartdiv<?=$tradingviewChartId?>"></div>
			<div class="clear"></div>
			
			<div id="1501232523211" class="startTradeBtnQuotes"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1501232523211");s = document.createElement("script");s.src = "https://fortrader.org/services/15135sz/jsRender?id=58&insertToId=1501232523211";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
			
			<a class="downloadchartImage download1" href="#"><?=Yii::t( $NSi18n, 'Download tech chart image' )?></a>
			<div class="clear"></div>
		</div>

	
		<div class="under-graph-form">	
			<div class="decomments-comment-section under-graph-block-form-wrapper">
				<div class="decomments-addform under-graph-block-form decomments-form-add-comment">
					<form class="ask_question decomments-addform-body" onsubmit="event.preventDefault();">
						<textarea placeholder="<?=Yii::t($NSi18n, 'Type your comment ...')?>"></textarea>
						<i></i>
						<div class="decomments-commentform-message">
							<i class="decomments-icon-warning"></i>
							<span></span>
						</div>
						<div class="decomments-loading"><div class="loader-ball-scale">
							<div></div>
							<div></div>
							<div></div>
						</div></div>
						<button class="ask-question-button button1 alignright decomments-button-send"><?=Yii::t($NSi18n, 'Publish your comment')?></button>
						<div class="clear"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- - - - - - - - - - - - - - End of Quotes List - - - - - - - - - - - - - - - - -->
	
	<div class="spinner-wrap">
		<div class="spinner">
			<div class="rect1"></div>
			<div class="rect2"></div>
			<div class="rect3"></div>
			<div class="rect4"></div>
			<div class="rect5"></div>
		</div>
	</div>
	
</div>

<?php
if( $model->chartLabel ){
	$chartLabel = $model->chartLabel;
}elseif( $model->tooltip ){
	$chartLabel = $model->tooltip;
}else{
	$chartLabel = $model->name;
}

$quotesSettings = QuotesSettingsModel::getInstance();
?>

<script>
var chartWidget;
var chartDetails ={
	periodChart: '',
	periodChartSelector: "<?='#chartdiv'.$periodChartId?>",
	tickChart: '',
	tickChartSelector: "<?='#chartdiv'.$tickChartId?>",
	tradingviewChart: '',
	tradingviewChartSelector: "<?='#chartdiv'.$tradingviewChartId?>",
	imageNotForDecomments: false,
	imageForFb: false,
};

!function( $ ) {
	var ns = ".<?=$ns?>";
	var nsText = ".<?=$ns?>";
	var ins = ".<?=$ins?>";
	var connUrl = '<?=$quotesSettings->wsServerUrl?>';
	var periods = <?=json_encode($periods)?>;

	chartWidget = ns.wQuoteChartsWidget = wQuoteChartsWidgetOpen({
		ns: ns,
		ins: ins,
		connUrl: connUrl,
		selector: nsText+' .<?=$ins?>',
		periods: periods,
		periodChartId: <?=$periodChartId?>,
		lang: '<?=$languageAlias?>',
		symbol: '<?=$model->name?>',
		symbolId: '<?=$model->id?>',
		statsHistoryUrl: '<?=Yii::app()->createAbsoluteUrl('quotesNew/ajaxLoadPeriodChartData');?>',
		needToUpdateImg: <?=$model->needToUpdateImg() ? 'true' : 'false'?>,
		saveImageUrl: '<?=Yii::app()->createAbsoluteUrl('quotesNew/saveImage');?>',
		realQuotesDataTimeDifference: <?=$quotesSettings->realQuotesDataTimeDifference?>,
		tickChartId: <?=$tickChartId?>,
		tradingviewChartId: <?=$tradingviewChartId?>,
		defaultPeriod: '<?=QuotesHistoryDataModel::$resolutionAmcharts[$defaultPeriod]?>',
		exportLibs: '<?=Yii::app()->getBaseUrl(true)?>/amcharts/plugins/export/libs/',
		chartLabel: '<?=$chartLabel?>',
		sourceType: '<?=$model->sourceType?>',
		tvSnapshotUrl: '<?=Yii::app()->createUrl('tradingview/createImage')?>',
	});
}( window.jQuery );

chartDetails.periodChart = chartWidget.$periodChart;
</script>