<?
	$class = $this->getCleanClassName();
	$items = $navlist->getItems();
?>
<div id="nav_scroll_box">
<ul class="nav nav-list">
	<?foreach( $items as $item ){?>
		<?$this->render( "{$class}/_item", Array( 'item' => $item, 'level' => 1 ))?>
	<?}?>
</ul>
</div>
