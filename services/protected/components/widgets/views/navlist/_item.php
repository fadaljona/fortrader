<?
	if( !$item->userHasRight()) return;
	if( $item->getDisable()) return;
	
	$class = $this->getCleanClassName();
	
	$subitems = $item->getSubitems();
	
	$liClass = $item->getClass();
	if( $item->getActive() or $item->hasActiveSubitems() ) {
		$liClass .= " active";
	}
	
	$linkClass = $subitems ? 'dropdown-toggle' : '';
?>
<li class="<?=$liClass?>">
	<a href="<?=$item->getURL()?>" class="<?=$linkClass?>">
		<i class="<?=$item->getIcon()?>"></i>
		<span class="menu-text"><?=$item->getLabel()?></span>
		<?if( $subitems ){?>
			<b class="arrow icon-angle-down"></b>
		<?}?>
	</a>
	<?if( $subitems ){?>
		<ul class="submenu">
			<?foreach( $subitems as $subitem ){?>
				<?$this->render( "{$class}/_item", Array( 'item' => $subitem, 'level' => $level + 1 ))?>
			<?}?>
		</ul>
	<?}?>
</li>