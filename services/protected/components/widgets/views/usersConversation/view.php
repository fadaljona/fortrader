<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wUsersConversationWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lUpdating' => 'Updating...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	switch( $instance ) {
		case 'broker/single':{
			$title = 'Reviews';
			break;
		}
		case 'contest/list':{
			$title = 'Discussion of Forex Contests';
			break;
		}
		case 'user/profile':{
			$title = 'Public messages';
			break;
		}
		default:{
			$title = 'Conversation';
			break;
		}
	}
?>
<a name="wUsersConversationWidget"></a>
<div class="widget-box iUsersConversationWidget i01 wUsersConversationWidget">
	<div class="widget-header">
		<h4 class="lighter smaller">
			<i class="icon-comment blue"></i>
			<?=Yii::t( $NSi18n, $title )?>
		</h4>
		
		<span class="<?=$ins?> UsersConversationOnline">
			<?php
				$this->widget( 'widgets.ConversationWatchWidget', Array(
					'idObj' => $idLinkedObj,
				));
			?>
		</span>
		
		<span class="widget-toolbar">
			<a href="#" class="<?=$ins?> wReload"><i class="icon-refresh"></i></a>
		</span>
	</div>
	<div class="widget-body">
		<div class="widget-main no-padding">
			<div class="dialogs <?=$ins?> wMessagesList">
				<?$this->renderMessages()?>
			</div>
			<div class="<?=$ins?> wTplItem" style="display:none; padding-bottom:14px;">
				<div class="progress progress-info progress-striped active">
					<div class="bar" style="width: 100%;"></div>
				</div>
			</div>
			<?if( $this->issetMoreMessages()){?>
				<div class="text-center" style="padding-bottom:14px;">
					<button class="btn btn-small btn-yellow no-radius <?=$ins?> wShowMore">
						<i class="icon-arrow-down"></i>
						<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Show more messages' )?></span>
					</button>
				</div>
			<?}?>
			<div class="<?=$ins?> wAnchorForm">
				<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
					<?=$form->hiddenField( $formModel, 'id', Array( 'class' => "{$ins} wID" ))?>
					<?=$form->hiddenField( $formModel, 'idMessageQuery', Array( 'class' => "{$ins} wIDMessageQuery" ))?>
					<?=CHtml::hiddenField( $formModel->resolveName( 'instance' ), $instance )?>
					<?=CHtml::hiddenField( $formModel->resolveName( 'idLinkedObj' ), $idLinkedObj )?>
					<div class="form-actions input-append">
						<table style="width:100%;">
							<tr>
								<td>
									<?=$form->textField( $formModel, 'text', Array( 'class' => "iInput i01 {$ins} wText", 'placeholder' => Yii::t( $NSi18n, 'Type your message here ...' )))?>
								</td>
								<td style="width:10px;">
									<button class="btn btn-small btn-info no-radius <?=$ins?> wSubmit" type="submit">
										<i class="icon-share-alt"></i>
										<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Send' )?></span>
									</button>
									<button class="btn btn-small btn-danger no-radius <?=$ins?> wCancel" style="display:none;">
										<i class="icon-ban-circle"></i>
										<span class="hidden-phone"><?=Yii::t( $NSi18n, 'Cancel' )?></span>
									</button>
								</td>
							</tr>
						</table>
					</div>
				<?$this->endWidget()?>
			</div>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var ins = ".<?=$ins?>";
		var nsText = ".<?=$ns?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var instance = "<?=$instance?>";
		var idLinkedObj = <?=json_encode($idLinkedObj)?>;
		var isGuest = <?=CommonLib::boolToStr( Yii::App()->user->isGuest )?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wUsersConversationWidget = wUsersConversationWidgetOpen({
			ins: ins,
			selector: nsText+' .wUsersConversationWidget',
			ajax: ajax,
			instance: instance,
			idLinkedObj: idLinkedObj,
			isGuest: isGuest,
			ls: ls,
		});
	}( window.jQuery );
</script>