<?
	$baseUrl = Yii::app()->baseUrl;
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$userMessageControl = Yii::App()->user->checkAccess( 'userMessageControl' );
	$class = $message->isAnswer() ? ' iAnswer i01' : '';

	switch( $instance ) {
		case 'broker/single':{
			$showAnswerBtn = false;
			$class .= ' withoutLine ';
			break;
		}
		default:{
			$showAnswerBtn = true;
			break;
		}
	}
	
?>
<div class="itemdiv dialogdiv<?=$class?>" idMessage="<?=$message->id?>" authorMessage="<?=htmlspecialchars( $message->user->showName )?>">
	<div class="user">
		<?=$message->user->getHTMLAvatar()?>
	</div>
	<div class="body">
		<div class="time">
			<?$this->widget( 'widgets.parts.TimeDiffWidget', Array( 'time' => $message->createdDT ))?>
		</div>
		<div class="name">
			<?=CHtml::link( CHtml::encode( $message->user->showName ), $message->user->getProfileURL())?>
			<?//if( $userMessageControl ){?>
				<?foreach( $message->user->groups as $group ){?>
					<?if( $userMessageControl or $group->name == 'Administrator' ){?>
						<span class="label label-info arrowed arrowed-in-right">
							<?=Yii::t( $NSi18n, $group->name )?>
						</span>
					<?}?>
				<?}?>
			<?//}?>
		</div>
		<div class="text">
			<?=$message->getHTMLText()?>
		</div>
		<div class="tools">
			<?if( $message->checkAccess( )){?>
				<div class="inline position-relative nowrapTT">
					<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cog icon-only bigger-110"></i>
					</button>
					<ul class="dropdown-menu dropdown-icon-only dropdown-light dropdown-caret dropdown-close">
						<li>
							<a href="#" class="tooltip-success <?=$ins?> wEdit" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Edit' )?>">
								<span class="green">
									<i class="icon-edit"></i>
								</span>
							</a>
						</li>
						<li>
							<a href="#" class="tooltip-error <?=$ins?> wDelete" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Delete' )?>">
								<span class="red">
									<i class="icon-trash"></i>
								</span>
							</a>
						</li>
					</ul>
				</div>
			<?}?>
			<?if( $showAnswerBtn ){?>
				<a href="#" class="btn btn-minier btn-info <?=$ins?> wAnswer">
					<i class="icon-only icon-share-alt"></i>
				</a>
			<?}?>
		</div>
	</div>
</div>