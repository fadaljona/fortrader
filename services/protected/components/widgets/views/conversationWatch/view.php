<?php
	if( $usersOnline ){
		?>
		<span class="UsersOnline"><i class="icon-circle"></i></span>
		<div class="online-users-details">
			<?php
				foreach( $usersOnline as $userOnline ){
					?>
					<div class="itemdiv dialogdiv">
						<div class="user">
							<?=$userOnline->user->getHTMLAvatar()?>
						</div>
						<div class="body">
							<div class="name">
								<?=CHtml::link( CHtml::encode( $userOnline->user->showName ), $userOnline->user->getProfileURL())?>
							</div>
						</div>
					</div>
					<?php
				}
			?>
		</div>	
	<?php			
	}
?>