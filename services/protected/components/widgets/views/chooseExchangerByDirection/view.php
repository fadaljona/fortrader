<?php
$cs=Yii::app()->getClientScript();
$jscrollpaneBaseUrl = Yii::app()->getAssetManager()->publish(Yii::App()->params['wpThemePath'] . '/plagins/jscrollpane');
$cs->registerScriptFile($jscrollpaneBaseUrl.'/js/jquery.mousewheel.js', CClientScript::POS_END);
$cs->registerScriptFile($jscrollpaneBaseUrl.'/js/jquery.jscrollpane.min.js', CClientScript::POS_END);
$cs->registerCssFile($jscrollpaneBaseUrl.'/css/jquery.jscrollpane.css');

$cs->registerScript('jScrollPane', '
!function( $ ) {
	$(".js_scrolling").jScrollPane();
}( window.jQuery );
', CClientScript::POS_END);

$cs->registerScriptFile(
    CHtml::asset(Yii::getPathOfAlias('webroot.js.widgets.lists').'/wChooseExchangerByDirectionWidget.js'),
    CClientScript::POS_END
);
    
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

?>
<div class="fx_section <?=$ins?> position-relative spinner-margin">
                            
    <div class="fx_content-title clearfix">
        <h3 class="fx_content-title-link"><?=Yii::t($NSi18n, 'Direction of exchange')?></h3>
        <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/fx_ic-exchange.png" alt="" class="fx_title-icon" />
    </div>

    <div class="fx_section-box fx_section-exchange clearfix">
        <div class="fx_exchange-box fx_divider-vertical">
            <div class="exchange-box__title clearfix">
                <div><?=Yii::t($NSi18n, 'You give')?></div>
                <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/icon_give.png" alt="">
            </div>
            <div class="exchange-box__wr js_scrolling clearfix fromCurrencyFilter eCurrencyFilter">
                <div class="exchange__item <?= $fromCurrency ? '' : 'exchange-item_active'?>" data-id="0"><?=Yii::t($NSi18n, 'All currencies')?></div>
                <?=$filterContent['from']?>
            </div>
        </div>

        <div class="fx_exchange-box">
            <div class="exchange-box__title clearfix">
                <div><?=Yii::t($NSi18n, 'You get')?></div>
                <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/icon_get.png" alt="">
            </div>
            <div class="exchange-box__wr js_scrolling clearfix toCurrencyFilter eCurrencyFilter">
                <div class="exchange__item <?= $toCurrency ? '' : 'exchange-item_active'?>" data-id="0"><?=Yii::t($NSi18n, 'All currencies')?></div>
                <?=$filterContent['to']?>
            </div>
        </div>

    </div>

    <div class="fx_exchange-btns">
    <?php /*    <a href="#" class="exchange_btn"><i class="fa fa-history"></i>История</a>
        <a href="#" class="exchange_btn"><i class="fa fa-cog"></i>Настройка</a>*/?>
    </div>

    <?php
        $this->widget('widgets.lists.ExchangerWithCoursesAndReservesListWidget', array(
            'ns' => 'nsActionView',
            'fromCurrency' => $fromCurrency,
            'toCurrency' => $toCurrency
        ));
    ?>
</div>
<?php
Yii::app()->clientScript->registerScript('ChooseExchangerByDirectionWidget', '

!function( $ ) {
    var ns = ' . $ns . ';
    var ins = ".' . $ins . '";

    ns.wChooseExchangerByDirectionWidget = wChooseExchangerByDirectionWidgetOpen({
        ns: ns,
        ins: ins,
        selector: ".'.$ins.'",
        ajaxLoadListURL: "' . Yii::app()->createAbsoluteUrl("exchangeECurrency/ajaxLoadListBestCourse") . '",
        ajaxAddMarkURL: "' . Yii::app()->createUrl('exchangeECurrency/ajaxAddMarkToExchanger') . '",
    });
}( window.jQuery );

', CClientScript::POS_END);