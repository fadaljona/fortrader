<?php 
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	$chartId = rand();
?>	
<div class="graph_inner <?=$ins.$chartId?>" data-id="<?=$model->id?>">
<?php	
	$languageAlias = LanguageModel::getCurrentLanguageAlias();
	
	$cs=Yii::app()->getClientScript();
	$amchartsBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.amcharts') );
	$cs->registerScriptFile($amchartsBaseUrl.'/amcharts.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/serial.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/plugins/export/export.js');
	$cs->registerScriptFile($amchartsBaseUrl.'/lang/'.$languageAlias.'.js');
	$cs->registerCssFile($amchartsBaseUrl.'/plugins/export/export.css');
	
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wQuoteOneChartWidget.js' ) );
	
	if( $model->nalias ){
		$nName = $model->nalias;
	}else{
		$nName = $model->name;
	}
?>
	<h6 class="graph_title"><?=Yii::t( $NSi18n, 'Charts' )?> <span class="PFDmedium"><?=CHtml::link($nName, array('quotesNew/single', 'slug' => strtolower($model->name) ))?></span></h6>
	<div class="tabs_box quoteOneChartWrap">
		<ul class="graph_list tabs_list clearfix">
		
		
		<?php foreach( $periods as $period => $periodData ){ ?>		
			<li class="changePeriod <?=isset( $periodData['data'] ) ? 'active' : ''?>" data-period="<?=$period?>">
				<a href="javascript:;"><?=Yii::t( $NSi18n, $periodData['name'] )?></a>
			</li>
		<?php } ?>
		</ul>
		<div class="tabs_contant">
			<div class="active">
				<div class="graph" id="chartdiv<?=$chartId?>"></div>
			</div>
		</div>
		
		<div class="spinner-wrap <?=$ajax ? 'hide' : ''?>">
			<div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div>
			</div>
		</div>
		
	</div>
<script>
!function( $ ) {
	var ns = ".<?=$ins.$chartId?>";
	var periods = <?=json_encode($periods)?>;

	ns.wQuoteOneChartWidget<?=$chartId?> = wQuoteOneChartWidgetOpen({
		selector: '.<?=$ins.$chartId?>',
		periods: periods,
		chartId: 'chartdiv<?=$chartId?>',
		lang: '<?=$languageAlias?>',
		symbol: '<?=$model->name?>',
		symbolId: '<?=$model->id?>',
		statsHistoryUrl: '<?=Yii::app()->createAbsoluteUrl('quotesNew/ajaxLoadPeriodChartData');?>',
		exportLibs: '<?=Yii::app()->getBaseUrl(true)?>/amcharts/plugins/export/libs/',
	});
}( window.jQuery );
</script>

</div>