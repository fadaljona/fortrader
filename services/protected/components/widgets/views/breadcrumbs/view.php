<?
	$baseUrl = Yii::app()->baseUrl;
	$NSi18n = $this->getNSi18n();
?>
<div class="breadcrumbs" id="breadcrumbs">
	<ul class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
		<?foreach( $items as $i=>$item ){?>
			<?
				$label = Yii::t( $NSi18n, $item->label );
			?>
			<li <?if( @$item->url ){?>typeof="v:Breadcrumb"<?}?>>
				<?if( @$item->icon ){?>
					<i class="<?=$item->icon?>"></i>		
				<?}?>
				<?if( @$item->url ){?>
					<?$url = CHtml::normalizeUrl( $item->url )?>
					<a href="<?=$url?>" rel="v:url" property="v:title">
						<?=$label?>
					</a>
				<?}else{?>
					<?=$label?>
				<?}?>
				<?if( $i+1 < count( $items )){?>
					<span class="divider">
						<i class="icon-angle-right"></i>
					</span>		
				<?}?>
			</li>
		<?}?>
	</ul>

	<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="<?=Yii::t( '*', 'Search' )?>" class="input-small search-query nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="icon-search" id="nav-search-icon"></i>
			</span>
		</form>
	</div>
</div>