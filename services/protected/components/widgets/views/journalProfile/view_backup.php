<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wJournalProfileWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSubscribe' => 'Subscribe for magazine',
		'lUnsubscribe' => "You're subscribed",
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
		
	$type = CommonLib::getExtension( $model->pathJournal );
	$size = @filesize( $model->pathJournal );
?>
<div class="row-fluid wJournalProfileWidget">
	<div class="row-fluid">
		<div class="span3" style="max-width:180px;">
			<div class="center">
				<?if( strlen( $model->nameCover )){?>
					<span class="profile-picture">
						<?if( $mode == 'last' ){?>
							<a href="<?=$model->getSingleUrl()?>">
						<?}?>
						<?=$model->getCover(Array( 'style' => "max-width:160px;" ))?>
						<?if( $mode == 'last' ){?>
							</a>
						<?}?>
					</span>
				<?}?>
			
				<?if( $mode == 'single' ){?>
					<div class="space-6"></div>
					<div class="clearfix">
						<div class="grid2">
							<a class="iA i19" href="<?=$model->getDownloadJournalURL()?>">
								<i class="iI i26 i26_1"></i>
								<?=Yii::t( '*', 'Download' )?>
							</a>
						</div>
						<div class="grid2">
							<a class="iA i19" href="<?=$model->getViewJournalURL()?>">
								<i class="iI i26 i26_2"></i>
								<?=Yii::t( '*', 'View' )?>
							</a>
						</div>
					</div>
					<div class="hr hr12 dotted"></div>
					<div class="clearfix">
						<div class="grid2">
							<span class="bigger-175 blue"><?=CommonLib::numberFormat( $model->countDownloads )?></span>
							<br>
							<?=Yii::t( '*', 'Downloads' )?>
						</div>
						<?if( $type == 'pdf' ){?>
							<div class="grid2">
								<span class="bigger-175 green"><?=CommonLib::numberFormat( $model->countDownloadsToday )?></span>
								<br>
								<?=Yii::t( '*', 'Today' )?>
							</div>
						<?}?>
					</div>
					<div class="hr hr12 dotted"></div>
					<? require dirname( __FILE__ ).'/_subscription.php'; ?>
				<?}?>
				<div class="space-6"></div>
			</div>
		</div>
		
		<div class="span9" style="margin-left:10px;">
			<span class="iSpan i03">
				№<?=htmlspecialchars($model->number)?>
			</span>
			
			<?if( $mode == 'single' ){?>
				<span class="iSpan i04">
					<?=$model->name?>
				</span>
			<?}?>
			
			<?if( $mode == 'last' ){?>
				<a href="<?=$model->getSingleURL()?>">
					<span class="iSpan i04">
						<?=$model->name?>
					</span>
				</a>
			<?}?>
			
			<div class="iClear" style="margin:20px 0 0 0"></div>
			
			<?if( $mode == 'single' ){?>
				<div>
					<?
						$this->widget( 'widgets.CountersWidget', Array(
							'counters' => Array( 'Ya.share' ),
						));
					?>
				</div>
			<?}?>
			
			<div class="pull-left" style="margin:5px 30px 0 0">
				<? require dirname( __FILE__ ).'/_mark.php'; ?>
			</div>
			
			<div class="pull-left">
				<div class="position-relative">
					<a href="#" class="iA i18" data-toggle="dropdown">
						<i class="iI i25"></i>
						<span><?=Yii::t( '*', 'Download' )?></span>
					</a>
					<div class="dropdown-menu" style="padding:10px; min-width: 120px; ">
						<i class="iI i24 i24_<?=$type?>"></i>
						<?=CHtml::link( strtoupper( $type ), $model->getDownloadJournalURL() )?> 
						<?=CommonLib::filesizeFormat( $size )?>
					</div>
				</div>
			</div>
			
			<div class="iClear"></div>
			
			<?$this->controller->widget( "widgets.detailViews.JournalProfileDetailViewWidget", Array( 'data' => $model ))?>
			
			<?if( $mode == 'single' ){?>
				<div class="row-fluid">
					<div class="page-header position-relative">
						<h1><?=Yii::t( '*', 'Intresting in issue' )?></h1>
					</div>
					<?$inner = $model->getCurrentInner()?>
					<?foreach( $inner as $i=>$_inner ){?>
						<blockquote class="iBlockquote i01">
							<p><?=$_inner['header']?></p>
							<small>
								<?=$_inner['content']?>
							</small>
						</blockquote>
					<?}?>
				</div>
				
				<div class="row-fluid">
					<?
						$this->widget( 'widgets.UsersConversationWidget', Array(
							'ns' => $ns,
							'ajax' => true,
						));
					?>
				</div>
				
				<div class="row-fluid" style="margin-top:20px;">
					<?
						$this->widget( 'widgets.PostInformWidget', Array(
							'ns' => 'nsActionView',
						));
					?>
				</div>
			<?}?>
			
		</div>
	</div>
	<?
		$this->widget( 'widgets.JournalsArchiveWidget', Array(
			'ns' => $ns,
			'idsSkip' => Array( $model->id ),
		));
	?>
	<?if( $mode == 'last' ){?>
		<div class="row-fluid" style="padding-top:20px;">
			<?
				$this->widget( 'widgets.UserActivitiesWidget', Array( 
					'ns' => $ns, 
				))
			?>
		</div>
		<div class="row-fluid">
			<h3 class="header smaller lighter blue"><?=Yii::t( '*', 'Statistics Magazin' )?></h3>
			
			<?
				$this->widget( 'widgets.lists.JournalsListWidget', Array(
					'ns' => $ns,
					'ajax' => true,
					'showOnEmpty' => true,
				));
			?>
		</div>
	<?}?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idJournal = <?=$model->id?>;
		
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wJournalProfileWidget = wJournalProfileWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wJournalProfileWidget',
			idJournal: idJournal,
			ls: ls,
		});
	}( window.jQuery );
</script>