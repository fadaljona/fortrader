<?	
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wJournalProfileWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSubscribe' => 'Subscribe for magazine',
		'lUnsubscribe' => "You're subscribed",
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
		
	$type = CommonLib::getExtension( $model->pathJournal );
	$size = @filesize( $model->pathJournal );
    $url = isset($_GET['url']) ? $_GET['url'] : null;
?>
<div class="wJournalProfileWidget">
	<? if($mode == 'single' && $model->status == 0){?>
		<? require dirname( __FILE__ ).'/single.php'; ?>
	<? } ?>
    
	<?if( $mode == 'last' ) {?>
		<? require dirname( __FILE__ ).'/last.php'; ?>
	<?}?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idJournal = <?=$model->id?>;
		
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wJournalProfileWidget = wJournalProfileWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wJournalProfileWidget',
			idJournal: idJournal,
			ls: ls,
			ajaxAddMarkURL: '<?=Yii::app()->createUrl('journal/ajaxAddMark')?>',
			ajaxSubscribeURL: '<?=Yii::app()->createUrl('journal/ajaxSubscribe')?>',
			ajaxUnsubscribeURL: '<?=Yii::app()->createUrl('journal/ajaxUnsubscribe')?>',
		});
	}( window.jQuery );
</script>