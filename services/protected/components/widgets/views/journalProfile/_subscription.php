<?
	$isGuest = false;
	if( Yii::App()->user->isGuest ){
		$subscribe = false;
		$isGuest = true;
	}else{
		$settings = Yii::App()->user->getModel()->getSettings();
		$subscribe = $settings->reciveNoticeNewJournal || $settings->reciveEmailNoticeNewJournal;
	}
	
	$set = $subscribe ? "wUnset" : "wSet";
?>

<div class="iDiv i50 wSubscribe <?=$set?>">
	<a class="subscribe_btn <?php if( $isGuest ){ echo 'arcticmodal'; }?>" <?php if( $isGuest ){ echo 'data-modal="#logIn"'; }?> href="#">
		<?=Yii::t( '*', $subscribe ? "You're subscribed" : "Subscribe for magazine" )?>	
	</a>
</div>