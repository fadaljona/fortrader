<div class="JournalRatingVotes"><?php echo Yii::t('*', 'votes');?>: <span><?=$model->getCountMark($model->stats->idJournal)?></span></div>
<?
	$this->widget( 'widgets.WebRatingWidget', Array(
		'ns' => $ns,
		'currentValue' => $model->stats->average,
		'onChangeValue' => "{$ns}.wJournalProfileWidget.onChangeMark",
		'readOnly' => Yii::App()->user->isGuest ? true : false,
	));
?>