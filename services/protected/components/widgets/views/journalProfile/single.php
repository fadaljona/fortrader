<div class="clearfix magazine_inner_btn paddingBottom30">
	<?php
		if( $model->digest ){
			echo CHtml::link( Yii::t( $NSi18n, 'Watch Digest' ), $model->digest, array('class' => 'watch_btn', 'target' => '_blank') );
		}
		if( $buyLink && !$model->free ){
			$priceSuffix = '';
			if( $model->price ) $priceSuffix = " {$model->price}\$";
			echo CHtml::link( 
				Yii::t( '*', 'Buy journal' ) .$priceSuffix, 
				$buyLink, 
				array('class' => 'download_pdf_btn', 'target' => '_blank') 
			);
		}else{
			if($model->getDownloadJournalURL()){
				$urll = isset($_GET['url']) ? $_GET['url'] : '';
				echo CHtml::link( 
					Yii::t( $NSi18n, 'Download PDF Journal' ), 
					$model->getDownloadJournalURL( $urll ), 
					array('class' => 'download_pdf_btn', 'target' => '_blank') 
				);
			}
		}
		echo CHtml::link( 
			Yii::t( '*', 'Questions and suggestions' ), 
			'#comments', 
			array('class' => 'question_btn', 'target' => '_blank') 
		);
	?>
</div>


<div class="clearfix section_offset magazine_inner_page">
	<div class="image_box alignleft">
		<?=$model->getCover(Array( 'width' => 243 ))?>
		<div style="max-width:243px;" >
			<? require dirname( __FILE__ ).'/_subscription.php'; ?>
		</div>
	</div>
	<div class="wrapper">
		<ul class="magazine_inner list8">
			<li>
				<span class="list8_item"><?=Yii::t( '*', 'Number rating' )?>:</span>
				<div class="alignright">
					<? require dirname( __FILE__ ).'/_mark.php'; ?>
				</div>
			</li>
			<li>
				<span class="list8_item"><?=Yii::t('*', 'Number and release date')?>:</span><p class="alignright blue_color_hight"><?="№{$model->number} {$model->date}"?></p>
			</li>
			<li>
				<span class="list8_item"><?=Yii::t('*', 'Count pages')?>:</span><p class="alignright blue_color_hight"><?=$model->countPages?></p>
			</li>
			<li>
				<span class="list8_item"><?=Yii::t('*', 'Share journal')?>:</span>
				<a href="#" class="alignright small-sm-btns share_tw">
					<img src="<?=Yii::App()->params['wpThemeUrl']?>/images/social-button/social-icon-small-tw.jpg" alt="" >
				</a>
				<a href="#" class="alignright small-sm-btns share_gp">
					<img src="<?=Yii::App()->params['wpThemeUrl']?>/images/social-button/social-icon-small-gp.jpg" alt="" >
				</a>
				<a href="#" class="alignright small-sm-btns share_fb">
					<img src="<?=Yii::App()->params['wpThemeUrl']?>/images/social-button/social-icon-small-fb.jpg" alt="" >
				</a>
				<a href="#" class="alignright small-sm-btns share_vk">
					<img src="<?=Yii::App()->params['wpThemeUrl']?>/images/social-button/social-icon-small-vk.jpg" alt="" >
				</a>
			</li>
			<?php
			if($buyLink){
				echo CHtml::openTag('li');
					echo CHtml::openTag('span', array("class"=>"list8_item"));
						echo Yii::t('*', 'App Store') . ':';
						echo CHtml::openTag('span', array("class"=>"alignright appStoreImg"));
							echo CHtml::link( 
								CHtml::tag('img', array('src'=>'/images/appstore.png', 'alt' => Yii::t('*', 'App Store'))),
								$buyLink, 
								array('target' => '_blank') 
							);
						echo CHtml::closeTag('span');
					echo CHtml::closeTag('span');
				echo CHtml::closeTag('li');
			}
			?>
		</ul>
		<div class="clearfix ">
			<div class="counter_box2">
				<div>
					<p><?=Yii::t( '*', 'Downloads' )?></p><strong class="blue_color_hight"><?=CommonLib::numberFormat( $model->countDownloads )?></strong>
				</div>
			</div>
			<?if( $type == 'pdf' ){?>
				<div class="counter_box2">
					<div>
						<p><?=Yii::t( '*', 'Today' )?></p><strong class="green_color"><?=CommonLib::numberFormat( $model->countDownloadsToday )?></strong>
					</div>
				</div>
			<?}?>
			<?php
				if( $buyLink && !$model->free && $model->price ){
					echo CHtml::link( 
						Yii::t( '*', 'Buy journal' ) .' '. $model->price.' $', 
						$model->getViewJournalURL($url), 
						array('class' => 'button3', 'target' => '_blank') 
					);
				}else{
					if($model->getDownloadJournalURL()){
						echo CHtml::link(
							Yii::t( '*', 'View' ), 
							$model->getViewJournalURL($url), 
							array('class' => 'button3', 'target' => '_blank') 
						);
					}
				}
			?>
		</div>
	</div>
</div>

<hr class="separator2">
<div class="section_offset">
	<h3><?=Yii::t( '*', 'Intresting in issue' )?><i class="fa fa-star"></i></h3>
	<hr class="separator2">
</div>
<div class="section_offset">
<?php
	$inner = $model->getCurrentInner();
	if( count($inner) ){
		echo CHtml::openTag('ul', array( 'class' => 'list9' ));
		
		foreach( $inner as $i=>$_inner ){
			if( isset($_inner['url']) && $_inner['url'] ){
				$headerInner = CHtml::link( $_inner['header'], CommonLib::removeProtocol( $_inner['url'] ) );
			}else{
				$headerInner = $_inner['header'];
			}
			echo CHtml::openTag('li');
				echo CHtml::tag(
					'span',
					array( 'class' => 'contentHeader' ),
					$headerInner . CHtml::tag( 'span', array( 'class' => 'contentInner' ), '— ' . $_inner['content'] )
				);
			echo CHtml::closeTag('li');
		}
		
		echo CHtml::closeTag('ul');
	}
?>
</div>

<?
	$this->widget( 'widgets.JournalsArchiveWidget', Array(
		'ns' => $ns,
		'idsSkip' => $mode == 'single' ? Array( $model->id ) : false,
		'limit' => 8
	));
?>