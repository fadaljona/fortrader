<?php 
$defaultWidth = 243;
?>
<hr class="separator2">
<div class="section_offset">
	<h3><?=Yii::t( '*', 'Webmaster rating' )?></h3>
	<hr class="separator2">
</div>

<div class="section_offset">
	<div class="blocksectiondiv clearfix">
		<div class="blocksectiondiv_text">
			<p><?= Yii::t("*", "<span>100 usd</span> per month for the most active distributor!") ?></p>
		</div>
		<div class="blocksectiondiv_link"><a href="#getJournalCover"><?= Yii::t("*", "Place the cover on your site") ?></a></div>
	</div>
</div>

<?
	$this->widget( 'widgets.lists.JournalsWebmasterListWidget', Array(
		'ns' => $ns,
		'ajax' => true,
		'showOnEmpty' => true,
		'journalId'=>$model->id,
	));
?>

<hr class="separator2">
<div class="section_offset" id="getJournalCover">
	<h3><?=Yii::t( '*', 'Journal cover' )?></h3>
	<hr class="separator2">
</div>

<!-- Code cover -->
<div class="section_offset">
	<div class="code_cover clearfix">
		<form class="code_cover_form" action="./">
			<p class="code_cover_form_text"><?=Yii::t( '*', 'Type size' )?>, <?=Yii::t( '*', 'Width' )?></p>
			<input id='jWidth' type="text" value="<?=$defaultWidth?>" class="code_cover_input">
		</form>
	</div>
	<div class="clearfix">
		<div class="code_img" style="width:243px;"><?=$model->getCover()?></div>
		<code class="code_script" title="<?=Yii::t( '*', 'Cover code' )?>">
			<span class="code_cover_title"><?=Yii::t( '*', 'Cover code' )?></span>
			<span class="coverCodeWrapper" id="coverCodeWrapper">
			 <?=$model->getJavascriptCode($defaultWidth)?>
			</span>
		</code>
	</div>
</div>


<?php
Yii::app()->clientScript->registerScript(__FILE__.__LINE__, '
	$(document).on("change", "#jWidth", function() {
		$("#imageForResize").attr("width", $(this).val());
        $(".code_img").css("width", $(this).val());
		updateCode($("#jWidth").val());
	});
	var updateCode = function(w) {
		$("#jsCode").val("");
		$.post(
			"'.Yii::app()->createUrl('journal/code').'",
			{width:w},
			function(data) {
				data = JSON.parse(data);
				$(".coverCodeWrapper").html(data.code);
			}
		);
	}
	function SelectText(element) {
		var doc = document,
			text = doc.getElementById(element),
			range,
			selection;
		if (doc.body.createTextRange) {
			range = document.body.createTextRange();
			range.moveToElementText(text);
			range.select();
		} else if (window.getSelection) {
			selection = window.getSelection();        
			range = document.createRange();
			range.selectNodeContents(text);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	}
	$(document).on("click", ".coverCodeWrapper", function() {
		SelectText("coverCodeWrapper");
	});' . "
	
	$('a[href*=#]').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var \$target = $(this.hash);
			\$target = \$target.length && \$target || $('[name=' + this.hash.slice(1) +']');
			if (\$target.length) {
				var targetOffset = \$target.offset().top;
				$('html,body').animate({scrollTop: targetOffset}, 1000);
				return false;
			}
		}
	});
	
", CClientScript::POS_READY);