<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<!-- - - - - - - - - - - - - - Profit- - - - - - - - - - - - - - - - -->
<section class="section_offset <?=$ins?>">
	<h4 class="toggle_btn"><?=Yii::t($NSi18n, 'Profitability of the account');?></h4>
	<div class="toggle_content tabl_quotes table_monitoring ">
	<?php
		$this->widget( 'widgets.gridViews.ProfitabilityOfTheAccountGridViewWidget', Array(
			'NSi18n' => $NSi18n,
			'ins' => $ins,
			'showTableOnEmpty' => true,
			'dataProvider' => $DP,
			'ajax' => false,
			'template' => '{items}',
		
		));
	?>
	</div>
</section>
<!-- - - - - - - - - - - - - - End Profit - - - - - - - - - - - - - - - - -->