<?php
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();	
?>

<hr class="separator2">
<section class="section_offset">
	<h3><?=Yii::t($NSi18n, 'Trading conditions of contest')?><i class="fa fa-exclamation-circle" aria-hidden="true"></i></h3>
	<hr class="separator2">
</section>
<div class="section_offset <?=$ins?>">
<?php
	foreach( $rules as $index => $rule  ){
		if( $index == 0 ){
			echo CHtml::openTag('ul', array( 'class' => 'clearfix conditions_list_' . $rule->type ));
		}
		$liText = $rule->header;
		if( $rule->val ) $liText .= ':' . CHtml::tag('span', array( 'class' => $rule->type . '_color alignright' ), $rule->val );
		
		$liClass = '';
		
		if( $rule->type == 'green' && ($index % 2 == 0) ) $liClass = 'border_white';
		echo CHtml::tag( 
			'li', 
			array( 'class' => $liClass ),
			CHtml::tag( 'p', array(), $liText )
		);
		
		if( isset( $rules[$index+1] ) && ($rules[$index+1]->type != $rule->type) ){
			echo CHtml::closeTag('ul');
			echo CHtml::openTag('ul', array( 'class' => 'clearfix conditions_list_' . $rules[$index+1]->type ));
		}
	}
	echo CHtml::closeTag('ul');
?>
</div>