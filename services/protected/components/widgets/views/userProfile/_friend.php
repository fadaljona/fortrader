<?
	$friend = UserFriendModel::model()->findByAttributes(Array(
		'idUser' => Yii::App()->user->id,
		'idFriend' => $model->id,
	));
?>
<?if( !$friend ){?>
	<button class="btn btn-small btn-block btn-success wAddFriend">
		<i class="icon-plus-sign bigger-110"></i>
		<?=Yii::t( '*', 'Add as a friend' )?>
	</button>
<?}elseif( $friend->status == 'invite' ){?>
	<button class="btn btn-small btn-block btn-warning">
		<?=Yii::t( '*', 'Invite sended' )?>
	</button>
<?}elseif( $friend->status == 'reject' ){?>
	<button class="btn btn-small btn-block btn-danger">
		<?=Yii::t( '*', 'Invite rejected' )?>
	</button>
<?}elseif( $friend->status == 'accept' ){?>
	<button class="btn btn-small btn-block btn-success">
		<?=Yii::t( '*', 'Invite accepted' )?>
	</button>
<?}?>