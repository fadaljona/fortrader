<?
	$isProfile = (bool)$model->profile;
	$isTrader = $isProfile && $model->profile->isTrader;
	$isManager = $isProfile && $model->profile->isManager;
	$isInvestor = $isProfile && $model->profile->isInvestor;
	$isProgrammer = $isProfile && $model->profile->isProgrammer;
?>
<div class="row-fluid">
	<div class="span3 center">
		<span class="profile-picture">
			<?=$model->getHTMLMiddleAvatar( Array( 'class' => 'wAvatarImage' ))?>
		</span>
		
		<?if( $itCurrentModel or $userControl ){?>
			<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data', 'style' => "margin:0", )))?>
				<?=$form->hiddenField( $avatarFormModel, 'ID' )?>
				<?=$form->fileField( $avatarFormModel, 'avatar', Array( 'class' => "wAvatar", 'style' => 'position:absolute;height:0;width:0;margin:0;padding:0;border:0;' ))?>
				<iframe name="iframeForAvatar" id="iframeForAvatar" style="display:none"></iframe>
			<?$this->endWidget()?>
		<?}?>
		
		<?if( $winers ){?>
			<div style="padding-top:8px;">
				<?foreach( $winers as $winer ){?>
					<?=$winer->getIcon()?>
				<?}?>
			</div>
		<?}?>
		
		<?if( !$itCurrentModel ){?>
			<div class="space space-4"></div>
			<?if( !Yii::App()->user->isGuest ){?>
				<? require dirname( __FILE__ ).'/_friend.php' ?>
			<?}?>
			<?$urlDialog = Yii::App()->createURL( '/userPrivateMessage/dialog', Array( 'idWith' => $model->id ));?>
			<a href="<?=$urlDialog?>" class="btn btn-small btn-block btn-primary">
				<i class="icon-envelope-alt"></i>
				<?=Yii::t( '*', 'Send a message' )?>
			</a>
		<?}?>
		<div class="hr hr12 dotted"></div>
		<div class="clearfix">
			<div class="grid2">
				<span class="bigger-175 blue"><?=CommonLib::numberFormat( $model->countReputation )?></span><br/>
				<?=Yii::t( '*', 'Reputation' )?>
			</div>
			<div class="grid2">
				<span class="bigger-175 green"><?=CommonLib::numberFormat( $model->countActivities )?></span><br/>
				<?=Yii::t( '*', 'Activities' )?>
			</div>
		</div>
		<div class="hr hr12 dotted"></div>
		<?if( $itCurrentModel or $userControl ){?>
			<div style="text-align:left;padding-left:20px;">
				
				<?$params = $itCurrentModel ? Array() : Array( 'id' => (int)@$_GET[ 'id' ])?>
				<?$url = $this->controller->createURL( '/user/settings', $params )?>
				<a href="<?=$url?>" class="iA i08">
					<?=Yii::t( $NSi18n, 'Edit' )?>
				</a>
				<br>

				<?$params['tabSettings'] = 'yes';?>
				<?$url = $this->controller->createURL( '/user/settings', $params )?>
				<a href="<?=$url?>" class="iA i09">
					<?=Yii::t( $NSi18n, 'Settings' )?>
				</a>

			</div>
			<div class="hr hr12 dotted"></div>
		<?}?>
	</div>
	<div class="span9">
		<h4 class="blue">
			<span class="middle">
				<a href="#"><?=htmlspecialchars($model->showName)?></a>
			</span>
			<?if( $model->isOnline()){?>
				<span class="label label-purple arrowed-in-right">
					<i class="icon-circle smaller-80"></i>
					<?=Yii::t( '*', 'online' )?>
				</span>
			<?}?>
		</h4>
		
		<ul class="nav nav-pills iUl i01">
			<li class="active">
				<a href="#tabProfileMain" data-toggle="tab">
					<i class="iI i11 i11_2 i12_main_data"></i>
					<?=Yii::t( $NSi18n, 'General' )?>
				</a>
			</li>
			<?if( $isTrader ){?>
				<li>
					<a href="#tabProfileTrader" data-toggle="tab">
						<i class="iI i11 i11_2 i12_trader"></i>
						<?=Yii::t( $NSi18n, 'Trader' )?>
					</a>
				</li>
			<?}?>
			<?if( $isManager ){?>
				<li>
					<a href="#tabProfileManager" data-toggle="tab">
						<i class="iI i11 i11_2 i12_manager"></i>
						<?=Yii::t( $NSi18n, 'Manager' )?>
					</a>
				</li>
			<?}?>
			<?if( $isInvestor ){?>
				<li>
					<a href="#tabProfileInvestor" data-toggle="tab" style="padding-left:40px;">
						<i class="iI i11 i11_2 i12_investor"></i>
						<?=Yii::t( $NSi18n, 'Investor' )?>
					</a>
				</li>
			<?}?>
			<?if( $isProgrammer ){?>
				<li>
					<a href="#tabProfileProgrammer" data-toggle="tab" style="padding-left:40px;">
						<i class="iI i11 i11_2 i12_programmer"></i>
						<?=Yii::t( $NSi18n, 'Programmer' )?>
					</a>
				</li>
			<?}?>
		</ul>
		
		<div class="pill-content">
			<div class="pill-pane active" id="tabProfileMain">
				<?$this->controller->widget( "widgets.detailViews.UserProfileDetailViewWidget", Array( 'data' => $model ))?>
			</div>
			<?if( $isProfile ){?>
				<div class="pill-pane row-fluid" id="tabProfileTrader">
					<div class="span6 iDiv i26">
						<?$this->controller->widget( "widgets.detailViews.UserProfileTraderLeftDetailViewWidget", Array( 'data' => $model ))?>
					</div>
					<div class="span6 iDiv i27">
						<?$this->controller->widget( "widgets.detailViews.UserProfileTraderRightDetailViewWidget", Array( 'data' => $model ))?>
					</div>
				</div>
				<div class="pill-pane" id="tabProfileManager">
					<div class="span6 iDiv i26 i28">
						<?$this->controller->widget( "widgets.detailViews.UserProfileManagerLeftDetailViewWidget", Array( 'data' => $model ))?>
					</div>
					<div class="span6 iDiv i27 i28">
						<?$this->controller->widget( "widgets.detailViews.UserProfileManagerRightDetailViewWidget", Array( 'data' => $model ))?>
					</div>
				</div>
				<div class="pill-pane" id="tabProfileInvestor">
					<div class="span6 iDiv i26">
						<?$this->controller->widget( "widgets.detailViews.UserProfileInvestorLeftDetailViewWidget", Array( 'data' => $model ))?>
					</div>
					<div class="span6 iDiv i27">
						<?$this->controller->widget( "widgets.detailViews.UserProfileInvestorRightDetailViewWidget", Array( 'data' => $model ))?>
					</div>
				</div>
				<div class="pill-pane" id="tabProfileProgrammer">
					<div class="span6 iDiv i26">
						<?$this->controller->widget( "widgets.detailViews.UserProfileProgrammerLeftDetailViewWidget", Array( 'data' => $model ))?>
					</div>
					<div class="span6 iDiv i27">
						<?$this->controller->widget( "widgets.detailViews.UserProfileProgrammerRightDetailViewWidget", Array( 'data' => $model ))?>
					</div>
				</div>
			<?}?>
		</div>
				
	</div>
</div>
<div class="space-20"></div>
<div class="row-fluid">
	<div class="span6">
		<?if( $winers ){?>
			<div class="widget-box transparent">
				<div class="widget-header">
					<h4 class="bolder">
						<?=Yii::t( '*', 'Awards' )?>
					</h4>
					<span class="widget-toolbar">
						<a href="#" data-action="collapse"><i class="icon-chevron-up"></i></a>
					</span>
				</div>
				<div class="widget-body iDiv i29">
					<?for( $i=0; $i<count( $winers ); $i+=3 ){?>
						<div class="row-fluid">
							<?for( $j=$i; $j<$i+3 and $j<count( $winers ); $j++ ){?>
								<div class="span4 iDiv i30">
									<?=$winers[$j]->getIcon()?>
									&nbsp;<span class="iSpan i02"><?=$winers[$j]->place?> <?=Yii::t( 'lower', 'place' )?></span>
									<div style="padding-left:25px;">
										<?=CHtml::link( CHtml::encode( $winers[$j]->contest->name ), $winers[$j]->contest->getSingleURL() )?>
									</div>
								</div>
							<?}?>
						</div>
					<?}?>
				</div>
			</div>
			<div class="space-20"></div>
		<?}?>
		<?if( strlen( $model->about )){?>
			<div class="widget-box transparent">
				<div class="widget-header widget-header-small">
					<h4 class="smaller">
						<i class="icon-check bigger-110"></i>
						<?=Yii::t( '*', 'Little About Me' )?>
					</h4>
				</div>
				<div class="widget-body">
					<div class="widget-main">
						<?=CommonLib::nl2br( htmlspecialchars( $model->about ))?>
					</div>
				</div>
			</div>
		<?}?>
		
		<?
			$this->widget( 'widgets.UsersConversationWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
				'idLinkedObj' => $model->id,
			));
		?>
	</div>
	<div class="span6">
		<?
			$this->controller->widget( 'widgets.UserActivitiesWidget', Array( 
				'ns' => $ns, 
				'idUser' => $model->id,
			))
		?>
	</div>
</div>