<?

	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets').'/wUserProfileWidget.js' ) );
	
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lInviteSended' => 'Invite sended',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$isProfile = (bool)$model->profile;
	$isTrader = $isProfile && $model->profile->isTrader;
	$isManager = $isProfile && $model->profile->isManager;
	$isInvestor = $isProfile && $model->profile->isInvestor;
	$isProgrammer = $isProfile && $model->profile->isProgrammer;
?>


<div class="<?=$ins?>">
	<div class="profile-inside__menu profile-card_margin">
		<div class="inside-menu__sub clearfix">
			<a href="#" class="inside-menu__main inside-menu_sub-active"><i class="ic_main"></i><?=Yii::t($NSi18n, 'Common')?></a>
			<?if( $isTrader ){?><a href="#" class="inside-menu__trader"><i class="ic_trader"></i><?=Yii::t( $NSi18n, 'Trader' )?></a><?}?>
			<?if( $isManager ){?><a href="#" class="inside-menu__head"><i class="ic_head"></i><?=Yii::t( $NSi18n, 'Manager' )?></a><?}?>
			<?if( $isInvestor ){?><a href="#" class="inside-menu__invest"><i class="ic_invest"></i><?=Yii::t( $NSi18n, 'Investor' )?></a><?}?>
			<?if( $isProgrammer ){?><a href="#" class="inside-menu__proger"><i class="ic_proger"></i><?=Yii::t( $NSi18n, 'Programmer' )?></a><?}?>
		</div>
	</div>
	<div class="profile-card__wr paddingBottom0 clearfix">
		<div class="profile-card__left">
			<div class="profile-card__avatar"><?=$model->getHTMLMiddleAvatar( Array( 'class' => 'profile-card__avatar-img' ))?></div>
			<a href="#comments" class="profile-card__message"><img src="<?=Yii::app()->params['wpThemeUrl']?>/images/icon-msg.png" alt="" class="profile-card_msg-icon"><?=Yii::t($NSi18n, 'Send message')?></a>
			<div class="profile-card__activity clearfix">
				<div class="profile-card_activ-item">
					<div class="profile_repa"><?=CommonLib::numberFormat( $model->countReputation )?></div>
					<?=Yii::t( '*', 'Reputation' )?>
				</div>
				<div class="profile-card_activ-item">
					<div class="profile_active"><?=CommonLib::numberFormat( $model->countActivities )?></div>
					<?=Yii::t( '*', 'Activities' )?>
				</div>
			</div>
		</div>
		<div class="profile-card__right">
			<?$this->controller->widget( "widgets.detailViews.UserProfileDetailViewWidget", Array( 'data' => $model ))?>

			<?if( $isProfile ){?>
				<?$this->controller->widget( "widgets.detailViews.UserProfileTraderDetailViewWidget", Array( 'data' => $model ))?>
				<?$this->controller->widget( "widgets.detailViews.UserProfileManagerDetailViewWidget", Array( 'data' => $model ))?>
				<?$this->controller->widget( "widgets.detailViews.UserProfileInvestorDetailViewWidget", Array( 'data' => $model ))?>
				<?$this->controller->widget( "widgets.detailViews.UserProfileProgrammerDetailViewWidget", Array( 'data' => $model ))?>
			<?}?>
			
			
		</div>
	</div>

	<?php if( $model->description ){ ?>
		<div class="profile-card__about">
			<div class="profile-card_about-title"><?=Yii::t( '*', 'Little About Me' )?></div>
			<div class="profile-card_about-text"><?=CommonLib::nl2br( htmlspecialchars( $model->description ))?></div>
		</div>
	<?php }?>
	
	<?php
		if( $countTradingAccounts ) {
			$this->widget( 'widgets.lists.ContestMemberAcconutsListWidget', Array(
				'ns' => $ns,
				'showOnEmpty' => true,
				'idUser' => $model->id,
			));
		}
		$this->widget( 'widgets.lists.UserWinersListWidget', Array(
			'ns' => $ns,
			'showOnEmpty' => true,
			'idUser' => $model->id,
		));
		$this->widget( 'widgets.lists.UserActivitiesListWidget', Array(
			'ns' => $ns,
			'showOnEmpty' => true,
			'idUser' => $model->id,
		));
	?>

	
	
	
</div>


<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idProfile = <?=$model->id?>;
		var canControl = <?=CommonLib::boolToStr( $canControl )?>;
				
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wUserProfileWidget = wUserProfileWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			idUser: idProfile,
			canControl: canControl,
			ls: ls,
		});
	}( window.jQuery );
</script>