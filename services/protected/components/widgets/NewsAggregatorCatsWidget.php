<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class NewsAggregatorCatsWidget extends WidgetBase {
		public $currentCat = 0;
		
		private function getCats(){
			return NewsAggregatorCatsModel::model()->findAll(array(
				'order' => ' `t`.`order` DESC ',
				'condition' => ' `t`.`enabled` = 1 '
			));
		}

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'currentCat' => $this->currentCat,
				'cats' => $this->getCats(),
			));
		}
	}

?>