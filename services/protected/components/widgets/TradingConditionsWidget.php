<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class TradingConditionsWidget extends WidgetBase {
		public $rules;
		
		function run() {
			if( !$this->rules || !count($this->rules) ) return false;
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'rules' => $this->rules
			));
		}
	}

?>