<?php
Yii::import( 'components.widgets.base.WidgetBase' );
	
final class AdminImageUploadWidget extends WidgetBase {
		
	public $form;
	public $model;
	public $field;
	public $ins;
	public $imgName;

	function run() {
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'form' => $this->form,
			'model' => $this->model,
			'field' => $this->field,
			'ins' => $this->ins,
			'imgName' => $this->imgName,
		));
	}
}

?>