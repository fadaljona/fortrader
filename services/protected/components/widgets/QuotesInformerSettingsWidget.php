<?
Yii::import('components.widgets.base.WidgetBase');

final class QuotesInformerSettingsWidget extends WidgetBase{
	public $defaultColors = array(
		'thTextColor' => '#707070',
		'thBackgroundColor' => '#eeeeee',
		'symbolTextColor' => '#454242',
		'tableTextColor' => '#9a8f8d',
		'borderTdColor' => '#dddddd',
		'tableBorderColor' => '#dddddd',
		'oddBackgroundTrColor' => '#ffffff',
		'evenBackgroundTrColor' => '#eeeeee',
		'profitTextColor' => '#00876a',
		'profitBackgroundColor' => '#eaf7e1',
		'lossTextColor' => '#dc0001',
		'lossBackgroundColor' => '#f6e1e1',
	);
	
	private function getSymbols(){
		return QuotesCategoryModel::model()->findAll(Array(
			'with' => array( 
				'symbols' => array(
					'with' => array('currentLanguageI18N')
				)
			),
			'order' => " `t`.`order` ",
			'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' ",
		));
	}

	function run() {
		$this->render( $this->getCleanClassName() . "/view", Array(
			'quotesCats' => $this->getSymbols(),
			'defaultColors' => $this->defaultColors,
		));
	}
	
}
?>