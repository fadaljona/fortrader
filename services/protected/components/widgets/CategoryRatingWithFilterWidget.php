<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CategoryRatingWithFilterWidget extends WidgetBase {
		public $ratingId;
		
		private $sortField = 'views';
		private $sortType = 'DESC';
		
		private $availableSortField = array('title', 'views', 'reviews', 'downloads', 'rating');
		private $availableSortType = array('DESC', 'ASC');
		
		private $model;
		private $selectedCategories = false;
		
		private function setSortField(){
			if( isset($_GET['sortField']) && $_GET['sortField'] && in_array( $_GET['sortField'], $this->availableSortField ) ){
				$this->sortField = $_GET['sortField'];
			} 
		}
		private function setSortType(){
			if( isset($_GET['sortType']) && $_GET['sortType'] && in_array( $_GET['sortType'], $this->availableSortType ) ){
				$this->sortType = $_GET['sortType'];
			} 
		}
		private function setModel(){
			if( $this->ratingId ){
				$ratingId = $this->ratingId;
			}elseif( isset( $_GET['ratingId'] ) && $_GET['ratingId'] ){
				$ratingId = $_GET['ratingId'];
				
			}else{
				throw new Exception( "Can't find rating" );
			}
			
			$model = CategoryRatingModel::model()->findByPk($ratingId);
			if( !$model ) throw new Exception( "Can't find rating" );
			$this->model = $model;
		}
		private function getChildCats(){
			$categoryChilds = CategoryRatingModel::getChildsOfCategory( $this->model->catId );
			if( !$categoryChilds ) return false;
			return WPTermModel::model()->findAll(array( 'condition' => " `t`.`term_id` IN (".implode(',', $categoryChilds).") "));
		}
		private function setCategories(){
			$categoryChilds = CategoryRatingModel::getChildsOfCategory( $this->model->catId );
			$categoryChilds[] = $this->model->catId;
			if( isset( $_GET['selectedCats'] ) && $_GET['selectedCats'] ){
				$selectedCats = explode(',', urldecode($_GET['selectedCats']));
				$returnArr = array_intersect( $selectedCats, $categoryChilds );
				if( $returnArr ){
					$this->selectedCategories = $returnArr;
					return true;
				} 
			}
			$this->selectedCategories = $categoryChilds;
			return true;
		}
		private function getSearchTerm(){
			if( isset( $_GET['searchTerm'] ) && $_GET['searchTerm'] ){
				return $_GET['searchTerm'];
			}
			return false;
		}
		private function getDp(){

			$c = new CDbCriteria();
			$c->with['termTaxonomy'] = array('with' => 'term');
			$c->with['categoryRatingPost'] = array( 'with' => array('ratingData', 'postsStats', 'stats') );
			
			
			$c->addCondition( " `termTaxonomy`.`taxonomy` = 'category' AND `termTaxonomy`.`term_id` IN (".implode(',', $this->selectedCategories ).") " );
			$c->addCondition( " `ratingData`.`inRating` = 1 OR `ratingData`.`inRating` IS NULL ");
			$c->group = " `categoryRatingPost`.`ID` ";
			
			if( $this->getSearchTerm() ){
				$c->addCondition( "
						`categoryRatingPost`.`post_title` LIKE :termName OR `ratingData`.`title` LIKE :termName
					");
				$c->params[':termName'] = CommonLib::filterSearch( CommonLib::escapeLike( $this->getSearchTerm() ) );
			}
			
			if( $this->sortField == 'views' ){
				$c->order = " `postsStats`.`postViews` " . $this->sortType;
			}elseif( $this->sortField == 'title' ){
				$c->order = " `ratingData`.`title` " . $this->sortType . ", `categoryRatingPost`.`post_title` " . $this->sortType;
			}elseif( $this->sortField == 'reviews' ){
				$c->order = " `postsStats`.`commentCount` " . $this->sortType;
			}elseif( $this->sortField == 'downloads' ){
				$c->order = " `postsStats`.`downloadsFromContent` " . $this->sortType;
			}elseif( $this->sortField == 'rating' ){
				$c->order = " `stats`.`average` " . $this->sortType;
			}
			
			
			
			$DP = new CActiveDataProvider( 'WPTermRelationshipModel', Array(
				'criteria' => $c,
				'pagination' => $this->getDPPagination(),
			));
			
			$DP->pagination->pageVar = "categoryRatingPage";
			$DP->pagination->pageSize = 30;
			return $DP;
		}
	

		function run() {
			$this->setModel();
			$this->setSortField();
			$this->setSortType();
			$this->setCategories();
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'rating' => $this->model,
				'DP' => $this->getDp(),
				'childCats' => $this->getChildCats(),
				'sortType' => $this->sortType,
				'sortField' => $this->sortField,
				'selectedCategories' => $this->selectedCategories,
				'searchTerm' => $this->getSearchTerm(),
			));
		}
	}

?>