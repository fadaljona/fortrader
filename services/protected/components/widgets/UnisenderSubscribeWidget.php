<?php
Yii::import('components.widgets.base.WidgetBase');

final class UnisenderSubscribeWidget extends WidgetBase
{
    public $listId;

    public function run()
    {
        if (!$this->listId) {
            return false;
        }
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'listId' => $this->listId
        ));
    }
}
