<?
Yii::import('components.widgets.base.WidgetBase');

final class NewQuotesWidget extends WidgetBase{
	protected $quotes;
	protected $viewClass;
	public $catId = false;
	
	private function setViewClass(){
		if( !$this->viewClass ) {
			$this->viewClass = $this->getCleanClassName();
		}
	}
	private function setQuotes() {
		if( !$this->quotes ) {
			$c = new CDbCriteria;
			$c->with = array( 
				'symbols' => array(
					'with' => array('currentLanguageI18N')
				),
				'currentLanguageI18N',
			);
			$c->order = " `t`.`order`, `cLI18NQuotesSymbolsModel`.`weightInCat` DESC ";
			if( !$this->catId ){
				$c->condition = " `cLI18NQuotesCategoryModel`.`name` IS NOT NULL AND `cLI18NQuotesCategoryModel`.`name` <> '' AND `cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '' AND `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `symbols`.`category` <> 0 ";
			}else{
				$c->condition = " `cLI18NQuotesCategoryModel`.`name` IS NOT NULL AND `cLI18NQuotesCategoryModel`.`name` <> '' AND `cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '' AND `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `symbols`.`category` <> 0 AND `t`.`id` = :id ";
				$c->params = array( ':id' => $this->catId );
			}
			
			$this->quotes = QuotesCategoryModel::model()->findAll($c);
		}
	}
	private function renderQuotes(){
		$this->setQuotes();
		$this->setViewClass();
		return $this->render(
			$this->viewClass . "/_tables",
			Array(
				'quotesCats' => $this->quotes,
				'catId' => $this->catId,
			),
			true
		);
	}

	function run() {
		$tables = $this->renderQuotes();
		$this->render( $this->viewClass . "/view", Array(
			'tables' => $tables,
		));
	}
	
}
?>