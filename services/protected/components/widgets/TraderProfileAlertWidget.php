<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class TraderProfileAlertWidget extends WidgetBase {
		function run() {
			if( Yii::App()->user->isGuest ) return;
			$dontShowTraderProfileAlert = @$_COOKIE[ 'dontShowTraderProfileAlert' ] == 'on';
			if( $dontShowTraderProfileAlert ) return;
			$user = Yii::App()->user->getModel();
			if( $user->profile and $user->profile->isTrader ) return;
						
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				
			));
		}
	}

?>