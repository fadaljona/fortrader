<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class LastContestWidget extends WidgetBase {
		public $model;
		public $mode = 'last';

		function isCurrentUserInContest( $idContest ) {
			static $idContests;
			if( $idContests === null ) {
				$idContests = Array();
				$userModel = Yii::App()->user->getModel();
				if( $userModel ) {
					$idContests = CommonLib::slice( $userModel->memberContests, 'idContest' );
				}
			}
			return in_array( $idContest, $idContests );
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->model,
				'mode' => $this->mode
			));
		}
	}

?>