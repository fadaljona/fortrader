<?
Yii::import('components.widgets.base.WidgetBase');

final class QuotesInformerWidget extends WidgetBase{
	const colorPattern = "/^[0-9,a-f,A-F]+$/";
	const symbolsPattern = "/^[0-9,\,]+$/";
	const columnsPattern = "/^[a-z,A-Z,\,]+$/";
	
	public $defaultColors = array(
		'thTextColor' => '#707070',
		'thBackgroundColor' => '#eeeeee',
		'symbolTextColor' => '#454242',
		'tableTextColor' => '#9a8f8d',
		'borderTdColor' => '#dddddd',
		'tableBorderColor' => '#dddddd',
		'oddBackgroundTrColor' => '#ffffff',
		'evenBackgroundTrColor' => '#eeeeee',
		'profitTextColor' => '#00876a',
		'profitBackgroundColor' => '#eaf7e1',
		'lossTextColor' => '#dc0001',
		'lossBackgroundColor' => '#f6e1e1',
	);
	public $availableColumns = array( 'Ask', 'Bid', 'OpenQuote', 'HighQuote', 'LowQuote', 'Chg', 'ChgPer', 'Time' );
	public $colors = array();
	public $symbols = array();
	public $columns;
	public $cat;
	public $catLink;
	public $multiplier = 1;
	public $noReal = 0;
	public $showGetBtn = 0;
	
	public function getColorParam( $param ){
		if( isset( $this->defaultColors[$param] ) ){
			if( !isset( $_GET[$param] ) || !$_GET[$param] ) return $this->defaultColors[$param];
			
			if( preg_match( self::colorPattern, $_GET[$param] ) && strlen( $_GET[$param] ) == 6 ){
				return '#' . $_GET[$param];
			}
			return $this->defaultColors[$param];
		}
		return false;
	}
	private function detParams(){
		$this->colors['thTextColor'] = $this->getColorParam('thTextColor');
		$this->colors['thBackgroundColor'] = $this->getColorParam('thBackgroundColor');
		$this->colors['symbolTextColor'] = $this->getColorParam('symbolTextColor');
		$this->colors['tableTextColor'] = $this->getColorParam('tableTextColor');
		$this->colors['borderTdColor'] = $this->getColorParam('borderTdColor');
		$this->colors['tableBorderColor'] = $this->getColorParam('tableBorderColor');
		$this->colors['oddBackgroundTrColor'] = $this->getColorParam('oddBackgroundTrColor');
		$this->colors['evenBackgroundTrColor'] = $this->getColorParam('evenBackgroundTrColor');
		$this->colors['profitTextColor'] = $this->getColorParam('profitTextColor');
		$this->colors['profitBackgroundColor'] = $this->getColorParam('profitBackgroundColor');
		$this->colors['lossTextColor'] = $this->getColorParam('lossTextColor');
		$this->colors['lossBackgroundColor'] = $this->getColorParam('lossBackgroundColor');

		$this->colors['thBackgroundOpacity'] = isset( $_GET['thBackgroundOpacity'] ) && $_GET['thBackgroundOpacity'] ? '1' : '';
		$this->colors['profitBackgroundOpacity'] = isset( $_GET['profitBackgroundOpacity'] ) && $_GET['profitBackgroundOpacity'] ? '1' : '';
		$this->colors['lossBackgroundOpacity'] = isset( $_GET['lossBackgroundOpacity'] ) && $_GET['lossBackgroundOpacity'] ? '1' : '';
		
		if( isset( $_GET['columns'] ) && $_GET['columns'] && preg_match( self::columnsPattern, $_GET['columns'] ) ){
			$this->columns = explode( ',', $_GET['columns'] );
			foreach( $this->columns as $key => $column ){
				if( !in_array( $column, $this->availableColumns ) ) unset( $this->columns[$key] );
			}
			if( !$this->columns ) $this->columns = array( 'Bid' );
		}else{
			$this->columns = array( 'Bid' );
		}
		
		if( isset( $_GET['symbols'] ) && $_GET['symbols'] && preg_match( self::symbolsPattern, $_GET['symbols'] ) ){
			$criteria = new CDbCriteria();
			$criteria->with = array( 'currentLanguageI18N' );
			$criteria->condition = " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND tickName <> '' ";
			$criteria->addInCondition('id', explode(',', $_GET['symbols'] ));
			$criteria->select = array( ' `id` ', ' `name` ', ' `tickName` ' );
			
			$symbols = QuotesSymbolsModel::model()->findAll( $criteria );
		}else{
			$symbols = QuotesSymbolsModel::model()->findAll(array(
				'with' => array( 'currentLanguageI18N' ),
				'select' => array( ' `id` ', ' `name` ', ' `tickName` ' ),
				'condition' => " `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND tickName <> '' AND `cLI18NQuotesSymbolsModel`.`toInformer` = 1 ",
			));
		}

		$symbolIds = array();
		foreach( $symbols as $symbol ){
			$symbolIds[] = $symbol->id;
		}
		
		if( in_array( 'OpenQuote', $this->columns ) || in_array( 'HighQuote', $this->columns ) || in_array( 'LowQuote', $this->columns )  ){
			$lastD1 = QuotesHistoryDataModel::getTodayD1DataForList($symbolIds);
		}
		
		foreach( $symbols as $symbol ){
			$this->symbols[$symbol->id]['name'] = $symbol->name;
			$this->symbols[$symbol->id]['tooltip'] = $symbol->tooltip;
			$this->symbols[$symbol->id]['nalias'] = $symbol->nalias;
			$this->symbols[$symbol->id]['tickName'] = $symbol->tickName;
			$this->symbols[$symbol->id]['lastBid'] = $symbol->lastBid;
			
			$dataExist = true;
			if( !$symbol->tickData ) $dataExist = false;
			$bid = $symbol->tickData->bid;
			
			$this->symbols[$symbol->id]['profit'] = '';
			if( $symbol->lastBid && $dataExist ){
				if( $bid - $symbol->lastBid > 0 ){
					$this->symbols[$symbol->id]['profit'] = 'profit';
				}elseif( $bid - $symbol->lastBid < 0 ){
					$this->symbols[$symbol->id]['profit'] = 'lesion';
				}
			}
			
			if( !isset( $lastD1[$symbol->id] ) ){
				$lastD1[$symbol->id] = array(
					'high' => Yii::t( '*', 'n/a' ),
					'low' => Yii::t( '*', 'n/a' ),
					'open' => Yii::t( '*', 'n/a' ),
				);
			}
			
			$precision = 2;
			if( $dataExist && $symbol->lastBid ){
				if( !$symbol->tickData->precision ){
					$precision = strlen($bid) - strpos( $bid, '.' ) - 1;
				}else{
					$precision = $symbol->tickData->precision;
				}
			}
			$this->symbols[$symbol->id]['precision'] = $precision;
			
			if($dataExist) $this->symbols[$symbol->id]['oldBid'] = $symbol->tickData->bid;
			else $this->symbols[$symbol->id]['oldBid'] = Yii::t( '*', 'n/a' );
			
			if($dataExist && $symbol->lastBid){
				$Chg = $symbol->tickData->bid - $symbol->lastBid;
				$Chg = number_format($Chg, $precision, '.', ' ');
				if( $Chg > 0 ) $Chg = '+'.$Chg;
				$this->symbols[$symbol->id]['prevChgVal'] = $Chg;
			}else{
				$this->symbols[$symbol->id]['prevChgVal'] = Yii::t( '*', 'n/a' );
			}
			
			foreach( $this->columns as $column ){
				
				switch ($column) {
					case 'Ask':
						if($dataExist) $this->symbols[$symbol->id]['Ask'] = $symbol->tickData->ask;
						else $this->symbols[$symbol->id]['Ask'] = Yii::t( '*', 'n/a' );
						break;
					case 'Bid':
						if($dataExist) $this->symbols[$symbol->id]['Bid'] = $symbol->tickData->bid;
						else $this->symbols[$symbol->id]['Bid'] = Yii::t( '*', 'n/a' );
						break;
					case 'Time':
						if($dataExist) $this->symbols[$symbol->id]['Time'] = $symbol->tickData->time;
						else $this->symbols[$symbol->id]['Time'] = Yii::t( '*', 'n/a' );
						break;
					case 'Chg':
						$this->symbols[$symbol->id]['Chg'] = $this->symbols[$symbol->id]['prevChgVal'];
						break;
					case 'ChgPer':
						if($dataExist && $symbol->lastBid){
							$Chg = $symbol->tickData->bid - $symbol->lastBid;
							$ChgPercent = $Chg / $symbol->lastBid * 100 ;
							$ChgPercent = number_format($ChgPercent, $precision, '.', ' ');
							if( $Chg > 0 ) $ChgPercent = '+'.$ChgPercent;
							$this->symbols[$symbol->id]['ChgPer'] = $ChgPercent;
						}else{
							$this->symbols[$symbol->id]['ChgPer'] = Yii::t( '*', 'n/a' );
						}
						break;
					case 'OpenQuote':
						$this->symbols[$symbol->id]['OpenQuote'] = $lastD1[$symbol->id]['open'];
						break;
					case 'HighQuote':
						$this->symbols[$symbol->id]['HighQuote'] = $lastD1[$symbol->id]['high'];
						break;
					case 'LowQuote':
						$this->symbols[$symbol->id]['LowQuote'] = $lastD1[$symbol->id]['low'];
						break;
				}
			}
		}
		if( isset( $_GET['cat'] ) && $_GET['cat'] ){
			$category = QuotesCategoryModel::model()->findByPk( $_GET['cat'] );
			if( $category ){
				$this->cat = $category->name;
				$this->catLink = $category->absoluteSingleURL;
			}else{
				$this->cat = Yii::t( '*', 'Quotes' );
				$this->catLink = Yii::App()->createAbsoluteUrl( 'quotesNew/list');
			}
		}else{
			$this->cat = Yii::t( '*', 'Quotes' );
			$this->catLink = Yii::App()->createAbsoluteUrl( 'quotesNew/list');
		}
		if( isset( $_GET['multiplier'] ) && $_GET['multiplier'] && is_numeric( $_GET['multiplier'] ) ){
			$this->multiplier = $_GET['multiplier'];
		}
		if( isset( $_GET['noReal'] ) && $_GET['noReal'] ){
			$this->noReal = 1;
		}
		if( isset( $_GET['showGetBtn'] ) && $_GET['showGetBtn'] ){
			$this->showGetBtn = 1;
		}
	}

	function run() {	
		$this->detParams();
		
		$this->render( $this->getCleanClassName() . "/view", Array(
			'colors' => $this->colors,
			'symbols' => $this->symbols,
			'columns' => $this->columns,
			'cat' => $this->cat,
			'catLink' => $this->catLink,
			'multiplier' => $this->multiplier,
			'noReal' => $this->noReal,
			'showGetBtn' => $this->showGetBtn
		));
	}	
}
?>