<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestWinnersWidget extends WidgetBase {
		public $model;
        private $winners = null;

		private function getModel() {
			return $this->model;
		}

        private function getWinners() {
            if($this->winners == null)
                $this->winners = $this->getModel()->getWinners();
            return $this->winners;
        }

        private function getTopWinners() {
            return count($this->winners) >= 3 ? array_slice($this->winners, 0, 3) : array();
        }

        private function getOtherWinners() {
            return count($this->winners) > 3 ? array_slice($this->winners, 3, count($this->winners) - 3) : array();
        }

		function run() {
            $this->getWinners();

			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
                'winners' => $this->getOtherWinners(),
                'allPrize' => $this->model->getSumPrizes(),
                'topWinners' => $this->getTopWinners(),
			));
		}
	}

?>