<?
Yii::import('components.widgets.base.WidgetBase');

final class LangSwitcherWidget extends WidgetBase{
	public $availableLangs = false;
	
	private function setAvailableLangs(){
		if( $this->availableLangs === false ) $this->availableLangs = Yii::app()->params['langs'];
	}

	function run() {
		$this->setAvailableLangs();
		if( count( $this->availableLangs ) < 2 ) return false;

		$class = $this->getCleanClassName();
		
		$this->render( 
			"{$class}/view", 
			Array(
				'langs' => $this->availableLangs,
			)
		);
	}
	
}
?>