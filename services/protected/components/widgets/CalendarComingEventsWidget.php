<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CalendarComingEventsWidget extends WidgetBase {
		public $excludeId = false;
		public $count = 5;
		public $fromDt = false;
		public $indicatorId = false;
		public $title;
		
		private $models = false;
		
		private function getModels(){
			if( !$this->models ){
				$c = new CDbCriteria();
				
				$c->with['event'] = array( 'with' => array( 'currentLanguageI18N' ) );
				$c->addCondition( " `cLI18NCalendarEvent`.`indicator_name` <> '' AND `cLI18NCalendarEvent`.`indicator_name` IS NOT NULL " );
				
				$c->order = " `t`.`dt` ASC ";
				
				if( !$this->fromDt ){
					$fromDt = date('Y-m-d H:i:s', time());
				}else{
					$fromDt = $this->fromDt;
				}
				
				if( $this->indicatorId ){
					$c->addCondition( " `t`.`indicator_id` = :indicatorId " );
					$c->params[ ':indicatorId' ] = $this->indicatorId;
				}else{
					$c->limit = $this->count;
				}
				
				$c->addCondition( " `t`.`dt` > :fromDt " );
				$c->params[ ':fromDt' ] = $fromDt;
				
				if( $this->excludeId ){
					$c->addCondition( " `t`.`id` <> :idEvent " );
					$c->params[ ':idEvent' ] = $this->excludeId;
				}
	
				$this->models = CalendarEvent2ValueModel::model()->findAll($c);
			}
			return $this->models;
		}
		public function renderList(){
			
			$models = $this->getModels();
			$list = '';
			foreach( $models as $model ){
				
				$contryImg = '';
				if( $model->event->country ){
					$contryImg = $model->event->country->getImgFlag('shiny', 16);
				}
				
				$list .= CHtml::openTag('li', array('data-dt' => $model->dt ));
				
					$list .= CHtml::link(
						CHtml::tag(
							'span', 
							array('class' => 'exchange-img'), 
							$contryImg
						) . ' ' . $model->event->indicatorName, 
						$model->event->singleURL
					);
					
					$list .= CHtml::tag(
						'span',
						array('class' => 'exchange-time' ),
						CHtml::tag('i', array('class' => 'fa fa-clock-o exchange-time-icon', 'aria-hidden' => 'true'), ' ') . ' ' . $model->getTimeLeft()
					);
				
				$list .= CHtml::closeTag('li');
			}
			
			return $list;
		}
		
		function run() {
			$models = $this->getModels();
			if( !$models ) return false;
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'list' => $this->renderList(),
				'indicatorId' => $this->indicatorId,
				'title' => $this->title,
				'count' => $this->count,
				'modelsCount' => count($models)
			));
		}
	}

?>