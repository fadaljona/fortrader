<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class WebRatingWidget extends WidgetBase {

		function __construct( $array ) {
			if( is_array($array) && count($array) ){
				foreach( $array as $property => $value ){
					$this->{$property} = $value;
				}
			}
		}

		public $values = Array( 1, 2, 3, 4, 5 );
		public $currentValue = 0;
		public $onChangeValue;
		public $numStars = 5;
		public $starWidth = 18;
		public $normalFill = '#dedfe0';
		public $ratedFill = '#f2ba00';
		public $maxValue = 5;
		public $wrapperStyle = false;
		public $returnNs = false;
		
		public $readOnly = false;
		public $type = 'yii';


		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'values' => $this->values,
				'currentValue' => $this->currentValue ? $this->currentValue : 0,
				'onChangeValue' => $this->onChangeValue,
				'readOnly' => $this->readOnly,
				'numStars' => $this->numStars,
				'starWidth' => $this->starWidth,
				'normalFill' => $this->normalFill,
				'ratedFill' => $this->ratedFill,
				'maxValue' => $this->maxValue,
				'wrapperStyle' => $this->wrapperStyle,
				'returnNs' => $this->returnNs,
				'type' => $this->type
			));
		}
	}

?>