<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class StarsWidget extends WidgetBase {
		public $values = Array( 1, 2, 3, 4, 5 );
		public $currentValue = 0;
		public $onChangeValue;
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'values' => $this->values,
				'currentValue' => $this->currentValue,
				'onChangeValue' => $this->onChangeValue,
			));
		}
	}

?>