<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class NewsAggregatorSourcesWidget extends WidgetBase {
		public $ajax = false;
		public $searchKey;
		public $cookieName;
		
		private function getSourcesDp(){
			$this->searchKey = @$_GET['searchKey'];
			$dp = NewsAggregatorSourceModel::getListDp( $this->searchKey );
			$dp->pagination = $this->getDPPagination();
			$dp->pagination->pageSize = 10;
			$dp->pagination->pageVar = 'sourcesPage';
			return $dp;
		}

		function run() {
			
			$disableCount = 0;
			if( isset(Yii::app()->request->cookies[$this->cookieName]) && Yii::app()->request->cookies[$this->cookieName]->value ) $disableCount = count( explode(',', Yii::app()->request->cookies[$this->cookieName]->value) );
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'sourcesDP' => $this->getSourcesDp(),
				'ajax' => $this->ajax,
				'searchKey' => $this->searchKey,
				'disableCount' => $disableCount,
				'cookieName' => $this->cookieName
			));
		}
	}

?>