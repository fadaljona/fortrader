<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class LanguagesTabsWidget extends WidgetBase {
		public $classTabs;
		public $viewPane;
		public $bagPane;
		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'classTabs' => $this->classTabs,
				'viewPane' => $this->viewPane,
				'bagPane' => $this->bagPane,
				'languages' => $this->getLanguages(),
			));
		}
	}

?>