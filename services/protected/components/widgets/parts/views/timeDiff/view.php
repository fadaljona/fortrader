<?
	$time = is_numeric( $time ) ? $time : @strtotime( $time );
	if( !$time ) return;
	
	$currentTime = time();
	$diff = $currentTime - $time;
	$diff = max( $diff , 0 );
	list( $seconds, $minets, $hours, $days, $months, $years ) = CommonLib::explodeTimeDiff( $diff );
	
	$NSi18n = $this->getNSi18n();
	
	$class = $this->class;
	$title = "";
	$value = 0;
	if( $seconds == 0 ) {
		$class .= ' blue';
		$value = Yii::t( $NSi18n, 'Now' );
		$title = '';
		$ago = '';
	}
	elseif( $minets == 0 ) {
		$class .= ' blue';
		$value = $seconds;
		$title = 'sec';
	}
	elseif( $hours == 0 ) {
		$class .= ' green';
		$value = $minets;
		$title = 'min';
	}
	elseif( $days == 0 ) {
		$class .= ' orange';
		$value = $hours;
		$title = Yii::t( $NSi18n, 'hour|hours', $value );
	}
	elseif( $months == 0 ) {
		$class .= ' orange';
		$value = $days;
		$title = Yii::t( $NSi18n, 'day|days', $value );
	}
	elseif( $years == 0 ) {
		$class .= ' grey';
		$value = $months;
		$title = Yii::t( $NSi18n, 'month|months', $value );
	}
	else{
		$class .= ' grey';
		$value = $years;
		$title = Yii::t( $NSi18n, 'year|years', $value );
	}
	if( strlen( $title )) $title = Yii::t( $NSi18n, $title );
?>
<?if( $icon ){?>
	<i class="<?=$icon?>"></i>
<?}?>
<span class="<?=$class?>">
	<?=$value?> 
	<?=$title?> 
	<?if( $ago ){?>
		<?=Yii::t( $NSi18n, $ago )?>
	<?}?>
</span>