<ul class="nav nav-tabs <?=$classTabs?>">
	<?foreach( $languages as $i=>$language ){?>
		<?
			$class = !$i ? ' class="active"' : '';
			$title = strtoupper( $language->alias );
			$title = str_replace( "EN_US", "EN", $title );
			$id = "tab{$classTabs}{$i}";
		?>
		<li<?=$class?>>
			<a href="#<?=$id?>" data-toggle="tab">
				<?=$title?>
			</a>
		</li>
	<?}?>
</ul>
<div class="tab-content">
	<?foreach( $languages as $i=>$language ){?>
		<?
			$class = !$i ? ' active' : '';
			$title = strtoupper( $language->alias );
			$title = str_replace( "EN_US", "EN", $title );
			$id = "tab{$classTabs}{$i}";
		?>
		<div class="tab-pane<?=$class?>" id="<?=$id?>">
			<?require( $viewPane )?>
		</div>
	<?}?>
</div>