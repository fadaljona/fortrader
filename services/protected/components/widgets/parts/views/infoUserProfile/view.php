<?
	$NSi18n = $this->getNSi18n();
	$baseUrl = Yii::app()->baseUrl;
	$currentModel = Yii::App()->user->getModel();
	
	$itCurrentModel = ($currentModel and $currentModel->id == $model->id);
?>
<div class="row-fluid">
	<div class="itemdiv memberdiv">
		<div class="user">
			<?=$model->getHTMLAvatar()?>
		</div>
		<div class="body">
			<div class="name">
				<a href="#"><?=htmlspecialchars($model->showName)?></a>
			</div>
			<div class="time">
				<?$this->controller->widget( 'widgets.parts.TimeDiffWidget', Array( 'time' => $model->usedDT ))?>
			</div>
			<div>
				<?if( $model->isOnline()){?>
					<span class="label label-info arrowed-in arrowed-in-right">online</span>
				<?}?>
			</div>
		</div>
	</div>
	<ul class="unstyled">
		<?if( strlen( $model->user_url )){?>
			<li>
				<i class="icon-globe purple"></i>
				<a href="<?=htmlspecialchars( $model->get_user_url() )?>"><?=htmlspecialchars( $model->user_url )?></a>
			</li>
		<?}?>
		<?if( !$itCurrentModel ){?>
			<li>
				<i class="icon-envelope blue"></i>
				<a href="#">Write a private message</a>
			</li>
		<?}?>
	</ul>
</div>
<div class="row-fluid">
	<dl style="margin-bottom:0">
		<?if( strlen( $model->about )){?>
			<dt><?=Yii::t( $NSi18n, 'About' )?></dt>
			<dd><?=CommonLib::nl2br( htmlspecialchars( $model->about ))?></dd>
		<?}?>
	</dl>
</div>