<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class TimeDiffWidget extends WidgetBase {
		public $time;
		public $class;
		public $icon = 'icon-time';
		public $ago;
		function getTime() {
			return $this->time;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'time' => $this->getTime(),
				'icon' => $this->icon,
				'ago' => $this->ago,
			));
		}
	}

?>