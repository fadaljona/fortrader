<?
	final class AccordionWidget extends WidgetExBase{
		protected $beginedGroup;
		protected $beginedBody;
			// dets
		function detW() {
			$class = $this->getClass();
			$id = $this->getId();
			return "w{$class}_{$id}";
		}
		function begin( $view = null, $data = Array()) {
			if( !strlen( $view )) 
				$view = '//_layouts/accordion/accordion';
			
			if( !array_key_exists( 'class', $data ))
				$data[ 'class' ] = "accordion-style2";
				
			$w = $this->getW();
			$data[ 'class' ] .= " {$w}";
				
			$this->controller->beginContent( $view, $data );
		}
		function end() {
			if( $this->beginedBody )
				$this->endBody();
				
			if( $this->beginedGroup )
				$this->endGroup();
				
			$this->controller->endContent();
		}
		function beginGroup( $view = null, $data = Array()) {
			if( !strlen( $view )) 
				$view = '//_layouts/accordion/accordion-group';
			
			$this->controller->beginContent( $view, $data );
			$this->beginedGroup = true;
		}
		function endGroup() {
			$this->controller->endContent();
			$this->beginedGroup = false;
		}
		function beginHeading( $view = null, $data = Array()) {
			if( !$this->beginedGroup )
				$this->beginGroup();
			
			if( !strlen( $view )) 
				$view = '//_layouts/accordion/accordion-heading';
				
			if( !array_key_exists( 'trigger', $data ))
				$data[ 'trigger' ] = 'manual';
				
			if( !array_key_exists( 'class', $data ))
				$data[ 'class' ] = 'collapsed';
			
			$this->controller->beginContent( $view, $data );
		}
		function endHeading() {
			$this->controller->endContent();
		}
		function beginBody( $view = null, $data = Array()) {
			if( !strlen( $view )) 
				$view = '//_layouts/accordion/accordion-body';
				
			if( !array_key_exists( 'class', $data ))
				$data[ 'class' ] = 'collapse';
				
			$this->controller->beginContent( $view, $data );
			$this->beginedBody = true;
		}
		function endBody() {
			$this->controller->endContent();
			$this->beginedBody = false;
		}
		function init() {
			$this->begin();
		}
		function run() {
			$this->end();
			$this->renderJSConstructor();
		}
	}
?>