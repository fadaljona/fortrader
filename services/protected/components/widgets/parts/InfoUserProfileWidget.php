<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class InfoUserProfileWidget extends WidgetBase {
		public $model;
		function getModel() {
			return $this->model;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
			));
		}
	}

?>