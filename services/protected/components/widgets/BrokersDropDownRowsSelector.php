<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class BrokersDropDownRowsSelector extends WidgetBase {
		public $cookieName = 'BrokersListWidget_countRows';
	
		function run() {
			$selectedCountsRows = 10;
			$cookie = false;
			if( isset(Yii::app()->request->cookies[$this->cookieName]) ) $cookie = Yii::app()->request->cookies[$this->cookieName]->value;
			if( $cookie ){
				$selectedCountsRows = $cookie;
			}
			$countsRows = Array( 10, 20, 50, 100 );
			$countsRows = array_combine( $countsRows, $countsRows );
			echo CHtml::openTag( 'div', array( 'class' => 'search_col25' ) );
				echo CHtml::openTag( 'div', array( 'class' => 'search_number' ) );
					echo CHtml::openTag( 'div', array( 'class' => 'custom_select PFDregular' ) );
						echo CHtml::dropDownList( 'countRows', $selectedCountsRows, $countsRows, Array( 'class' => " wSelectCountRows" ));
					echo CHtml::closeTag('div');
				echo CHtml::closeTag('div');
			echo CHtml::closeTag('div');
		}
	}

?>