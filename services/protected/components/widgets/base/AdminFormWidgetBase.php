<?
	
	class AdminFormWidgetBase extends FormWidgetBase {
		public $modelName;
		public $formModel;
		protected $activeFormWidget;
			// factory
		static protected function getClassByModelName( $modelName ) {
			return "Admin{$modelName}FormWidget";
		}
		static protected function getAliasByClass( $class ) {
			return "widgets.forms.admin.{$class}";
		}
		static protected function getDefAlias() {
			return "widgets.base.".__CLASS__;
		}
		static function createWidgetByModelName( $modelName, $properties = Array(), $_class = __CLASS__ ) {
			return parent::createWidgetByModelName( $modelName, $properties, $_class );
		}
		static function widgetByModelName( $modelName, $properties = Array(), $captureOutput = false, $_class = __CLASS__ ) {
			return parent::widgetByModelName( $modelName, $properties, $captureOutput, $_class );
		}
			// views
		static protected function getPartView( $view ) {
			return "widgets.forms.admin.views._parts.{$view}";
		}
			// dets
		function detModelName() {
			$class = $this->getClass();
			return preg_replace( "#^Admin|FormWidget$#", "", $class );
		}
		function detFormModel() {
			$modelName = $this->getModelName();
			$formModel = AdminFormModelBase::instanceByModelName( $modelName );
			$formModel->load();
			return $formModel;
		}
		function detCSSClass() {
			$w = $this->getW();
			return "iFormWidget i03 {$this->w}";
		}
		function detJSClass() {
			return __CLASS__;
		}
		function detJSInstanceOptions() {
			return CommonLib::mergeAssocs( parent::detJSInstanceOptions(), Array(
				'modelName' => $this->getModelName(),
			));
		}
		function detJSLS() {
			$jsls = CommonLib::explode( 'Deleting..., Deleted, Saving..., Saved, Loading..., Loaded..., Delete?, Refresh, Reseted, Copy, Copied' );
			$jsls[] = 'Sorry. Request failed. Please try again later.';
			$jsls = array_combine( $jsls, $jsls );
			
			$modelName = strtolower( $this->getModelName());
			$jsls[ 'New object' ] = "New {$modelName}";
			$jsls[ 'Editor object' ] = "Editor {$modelName}";
			return CommonLib::mergeAssocs( parent::detJSLS(), $jsls );
		}
			// activeForm
		function beginActiveForm( $widgetAlias = null, $properties = Array()) {
			if( !strlen( $widgetAlias )) 
				$widgetAlias = 'widgets.base.BootstrapExFormWidgetBase';
			return $this->activeFormWidget = $this->beginWidget( $widgetAlias, $properties );
		}
		function endActiveForm() {
			$this->endWidget();
		}
			// accordion
		function beginAccordion( $widgetAlias = null, $properties = Array()) {
			if( !strlen( $widgetAlias )) 
				$widgetAlias = 'widgets.parts.AccordionWidget';
			return $accordionWidget = $this->beginWidget( $widgetAlias, $properties );
		}
		function endAccordion() {
			$this->endWidget();
		}
		function beginSingleAccordion( $widgetAlias = null, $properties = Array()) {
			if( array_key_exists( 'headingContent', $properties )) {
				$headingContent = $properties[ 'headingContent' ];
				unset( $properties[ 'headingContent' ]);
			}
			$accordion = $this->beginAccordion( $widgetAlias, $properties );
			
			if( !empty( $headingContent )) {
				$accordion->beginHeading();
				echo $headingContent;
				$accordion->endHeading();
			}
			
			$accordion->beginBody();
			
			return $accordion;
		}
		function endSingleAccordion() {
			$this->endWidget();
		}
			// variants
		function getFieldVariants( $field, $parameters = Array() ) {
			$formModel = $this->getFormModel();
			return $formModel->getFieldVariants( $field, $parameters );
		}
		function getDropDownListOptions( $field, $parameters = Array() ) {
			return $this->getFieldVariants( $field, $parameters );
		}
			// renders
		function renderTitle( $view = null, $data = Array(), $return = false ) {
			$defData = Array(
				'ins' => $this->getINS(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( 'adminFormTitle' );
			return CWidget::render( $view, $data, $return );
		}
		function renderI18NBlock( $widgetAlias = null, $properties = Array(), $return = false ) {
			if( !strlen( $widgetAlias )) $widgetAlias = "widgets.parts.LanguagesTabsWidget";
			$defProperties = Array(
				'bagPane' => (object)Array(
					'activeFormWidget' => $this->activeFormWidget,
					'formModel' => $this->getFormModel(),
				),
			);
			$properties = CommonLib::mergeAssocsR( $defProperties, $properties );
			return $this->widget( $widgetAlias, $properties, $return );
		}
		function renderControlBlock( $view = null, $data = Array(), $return = false ) {
			$defData = Array(
				'ins' => $this->getINS(),
				'NSi18n' => $this->getNSi18n(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( 'adminFormControlBlock' );
			return CWidget::render( $view, $data, $return );
		}
		function renderSetListRow( $view = null, $data = Array(), $return = false ) {
			$formModel = $this->getFormModel();
			
			$headingContent = '<i class="icon-align-justify"></i>';
			$headingContent .= $formModel->getAttributeLabel( $data[ 'attribute' ]);
			$this->beginSingleAccordion( null, Array( 'headingContent' => $headingContent ));
			
			$defData = Array(
				'ins' => $this->getINS(),
				'NSi18n' => $this->getNSi18n(),
				'formModel' => $formModel,
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( 'adminFormSetListRow' );
			CWidget::render( $view, $data, $return );
			
			$this->endSingleAccordion();
		}
		function render( $view = null, $data = Array(), $return = false ) {
			$defData = Array(
				'cssClass' => $this->getCSSClass(),
				'formModel' => $this->getFormModel(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			return parent::render( $view, $data, $return );
		}
	}

?>