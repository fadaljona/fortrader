<?

	class AdminListWidgetBase extends ListWidgetBase {
		public $selectable = true;
			// factory
		static protected function getClassByModelName( $modelName ) {
			return "Admin{$modelName}ListWidget";
		}
		static protected function getAliasByClass( $class ) {
			return "widgets.lists.admin.{$class}";
		}
		static protected function getDefAlias() {
			return "widgets.base.".__CLASS__;
		}
		static function createWidgetByModelName( $modelName, $properties = Array(), $_class = __CLASS__ ) {
			return parent::createWidgetByModelName( $modelName, $properties, $_class );
		}
		static function widgetByModelName( $modelName, $properties = Array(), $captureOutput = false, $_class = __CLASS__ ) {
			return parent::widgetByModelName( $modelName, $properties, $captureOutput, $_class );
		}
			// views
		static protected function getBaseView( $view ) {
			return "widgets.lists.admin.views._base.{$view}";
		}
		static protected function getPartView( $view ) {
			return "widgets.lists.admin.views._parts.{$view}";
		}
			// dets
		function detModelName() {
			$class = $this->getClass();
			return preg_replace( "#^Admin|ListWidget$#", "", $class );
		}
		function detJSClass() {
			return __CLASS__;
		}
		function detPathXMLConfig() {
			$class = $this->getClass();
			return $path = "widgets.lists.admin.config.{$class}";
		}
	}

?>