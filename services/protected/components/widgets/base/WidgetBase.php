<?
	Yii::import( 'controllers.base.ActionBase' );

	abstract class WidgetBase extends CWidget {
		public $ns;
		public $DPLimit = ActionBase::DPLimit;
		public $DPPageVar = ActionBase::DPPageVar;
		function init() {
			
		}
		protected function getNS() {
			return $this->ns;
		}
		protected function getINS() {
			$class = get_class( $this );
			return "ins{$class}";
		}
		protected function getDPLimit() {
			if( isset( Yii::app()->controller->module ) && Yii::app()->controller->module->id == 'admin' ) return 100;
			return $this->DPLimit;
		}
		protected function setDPLimit( $limit ) {
			$this->DPLimit = $limit;
		}
		protected function getDPPageVar() {
			return $this->DPPageVar;
		}
		protected function setDPPageVar( $var ) {
			$this->DPPageVar = $var;
		}
		protected function getDPPagination( $pageSize = null, $pageVar = null ) {
			return Array(
				'pageSize' => $pageSize === null ? $this->getDPLimit() : $pageSize,
				'pageVar' => $pageVar === null ? $this->getDPPageVar() : $pageVar,
			);
		}
		protected function resolveClassName( $class ) {
			return $class ? $class : get_class( $this );
		}
		function getCleanClassName( $class = null ) {
			$class = $this->resolveClassName( $class );
			$class = preg_replace( "#Widget$#", "", $class );
			$class[0] = strtolower($class[0]);
			return $class;
		}
		function getNSi18n( $class = null ) {
			$class = $this->getCleanClassName( $class );
			return "components/widgets/{$class}";
		}
		protected function throwI18NException( $message, $params = Array(), $class = null ) {
			$NSi18n = $this->getNSi18n( $class );
			throw new Exception( Yii::t( $NSi18n, $message, $params ));
		}
	}

?>