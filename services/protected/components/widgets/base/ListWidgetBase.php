<?

	class ListWidgetBase extends WidgetExBase {
		public $model;
		public $modelName;
		public $modelClass;
		public $DP;
		public $DPCriteria;
		public $data;
		public $columns;
		public $sort;
		public $filters;
		public $availableLimits;
		public $visibleColumns;
		public $hiddenColumns;
		public $demandedColumnsNames;
		public $columnTpls;
		public $extraActionsTpls;
		public $selectable = false;
		protected $DPCriteriaSimpleSort;
			// factory
		static protected function getClassByModelName( $modelName ) {
			return "{$modelName}ListWidget";
		}
		static protected function getAliasByClass( $class ) {
			return "widgets.lists.{$class}";
		}
		static protected function getDefAlias() {
			return "widgets.base.".__CLASS__;
		}
		static function createWidgetByModelName( $modelName, $properties = Array(), $_class = __CLASS__ ) {
			return parent::createWidgetByModelName( $modelName, $properties, $_class );
		}
		static function widgetByModelName( $modelName, $properties = Array(), $captureOutput = false, $_class = __CLASS__ ) {
			return parent::widgetByModelName( $modelName, $properties, $captureOutput, $_class );
		}
			// views
		static protected function getBaseView( $view ) {
			return "widgets.lists.views._base.{$view}";
		}
		static protected function getPartView( $view ) {
			$view = str_replace( '::', '', $view );
			return "widgets.lists.views._parts.{$view}";
		}
		static function getBaseXMLConfig() {
			static $XML;
			if( !$XML ) {
				$path = "widgets.lists.config._base";
				$XML = new DOMDocument( "1.0", "UTF-8" );
				$path = Yii::getPathOfAlias( $path );
				$XML->load( "{$path}.xml" );
			}
			return $XML;
		}
			 // dets
		function detView() {                     // getView
			return self::getBaseView( "list" );
		}
		function detModel() {                    // getModel
			$modelClass = $this->getModelClass();
			return $model = CActiveRecord::model( $modelClass );
		}
		function detModelName() {                // getModelName
			$class = $this->getClass();
			return preg_replace( "#ListWidget$#", "", $class );
		}
		function detModelClass() {               // getModelClass
			$modelName = $this->getModelName();
			return "{$modelName}Model";
		}
		function detCssClass() {                 // getCssClass
			$w = $this->getW();
			$cssClass = "{$w} iListWidget i01";
			if( !$this->selectable )
				$cssClass .= ' i02';
			return $cssClass;
		}
		function detNSi18n() {                   // getNSi18n
			return array_merge( parent::detNSi18n(), (array)$this->getModel()->_getNSi18n(), Array( "components/widgets/".__CLASS__ ));
		}
		function detDPCriteriaSimpleSort() {     // getDPCriteriaSimpleSort
			$simpleSort = Array();
			
			$c = $this->getDPCriteria();
			$orders = CommonLib::explode( $c->order );
			$pattern = "#`\w+`\.`(\w+)` (ASC|DESC)#";
			foreach( $orders as $order ) {
				if( preg_match( $pattern, $order, $matchs )) {
					list( $match, $column, $type ) = $matchs;
					$simpleSort[] = "{$column} {$type}";
				}
			}
			
			return $simpleSort;
		}
		function detDPCriteria( $cDef = null ) { // getDPCriteria
			$model = $this->getModel();
			$modelName = $model->_getName();
			$modelColumnsNames = $model->_getColumnNames();
			$visibleColumns = CommonLib::toAssoc( $this->getVisibleColumns(), 'name' );
			$hiddenColumns = CommonLib::toAssoc( $this->getHiddenColumns(), 'name' );
			
			$c = new CDbCriteria();
			$c->select = Array();
			
			$getRelationCriteria = function( $column ) use( &$model ) {
				$c = new CDbCriteria();
				$relation = $model->_getRelation( $column->relation );
				$relationModelClass = $relation->className;
				$relationModel = CActiveRecord::model( $relationModelClass );
				$relationColumnsNames = $relationModel->_getColumnNames();
				
				if( $relation instanceof CBelongsToRelation ) {
					CommonLib::addToArray( $c->select, $relation->foreignKey );
				}
				
				if( in_array( $column->field, $relationColumnsNames )) {
					CommonLib::addToArray( $c->with[ $column->relation ][ 'select' ], $column->field );
				}
				else{
					$relationModelName = $relationModel->_getName();
					$hasI18N = I18NModelInterface::hasModelRelations( $relationModelName );
					if( $hasI18N ) {
						$I18NModelClass = I18NModelInterface::getI18NModelClass( $relationModelName );
						$I18NColumnsNames = CActiveRecord::model( $I18NModelClass )->_getColumnNames();
						$CLI18NRelationName = I18NModelInterface::getCLI18NRelationName();
						$CLI18NAlias = I18NModelInterface::getCLI18NAlias( $relationModelName );
						
						if( in_array( $column->field, $I18NColumnsNames )) {
							CommonLib::addToArray( $c->with[ $column->relation ][ 'with' ][ $CLI18NRelationName ][ 'select' ], $column->field );
						}
					}	
				}
				
				return $c;
			};
			
				// select
			foreach( array_merge( $visibleColumns, $hiddenColumns ) as $column ) {
				if( @$column->relation ) {
					$relationCriteria = $getRelationCriteria( $column );
					$c->mergeWith( $relationCriteria );
					continue;
				}
				if( in_array( $column->name, $modelColumnsNames )) {
					$c->select[] = $column->name;
					continue;
				}
			}

				// sort
			$sort = $this->getSort();
			if( $sort ) {
				$order = Array();
				foreach( $sort as $part ) {
					$part = str_replace( ':', ' ', $part );
					@list( $name, $type ) = CommonLib::explode( $part, ' ', 2 );
					
					if( !array_key_exists( $name, $visibleColumns ))
						continue;
					
					$column = $visibleColumns[ $name ];
					if( !@$column->field || !@$column->alias || !@$column->sorting == 'yes' )
						continue;
					
					$type = strtoupper( $type );
					if( !in_array( $type, Array( 'ASC', 'DESC' )))
						$type = $column->defSortingType;
						
					$order[] = "`{$column->alias}`.`{$column->field}` {$type}";
				}
				@$c->order .= implode( ',', $order );
			}
			
				// filter
			$filters = $this->getFilters();
			foreach( $filters as $name=>$value ) {
				if( !array_key_exists( $name, $visibleColumns ))
					continue;
				
				$column = $visibleColumns[ $name ];
				if( !@$column->field || !@$column->alias || !@$column->filtering == 'yes' )
					continue;
					
				$key = ":filter-{$column->name}";
				$key = str_replace( '-', '_', $key );
				
				if( $column->filteringOperator == 'LIKE' )
					$value = CommonLib::filterSearch( $value );
					
				switch( $column->filteringOperator ) {
					case 'FIND_IN_SET':{
						$c->addCondition( "FIND_IN_SET( {$key}, `{$column->alias}`.`{$column->field}` ) " );
						break;
					}
					default: {
						$c->addCondition( "`{$column->alias}`.`{$column->field}` {$column->filteringOperator} {$key} " );
					}
				}
					
				
				$c->params[ $key ] = $value;
			}
			
			if( $cDef )
				$c->mergeWith( $cDef );
				
			if( !$c->order ) {
				foreach( $visibleColumns as $column ) {
					if( @$column->sorting == 'yes' ) {
						$type = @$column->defSortingType ? $column->defSortingType : 'ASC';
						$c->order = "`{$column->alias}`.`{$column->field}` {$type}";
						break;
					}
				}
			}
			
			$c = $model->_getLightListCriteria( $c );
			//var_dump( $c ); exit;
			
			return $c;
		}
		function detDP() {                       // getDP
			$modelClass = $this->getModelClass();
			return new CActiveDataProvider( $modelClass, Array(
				'criteria' => $this->getDPCriteria(),
				'pagination' =>$this->getDPPagination(),
			));
		}
		function detAvailableLimits() {          // getAvailableLimits
			$limits = Array( 5, 10, 20, 50, 100 );
			$limits = array_combine( $limits, $limits );
			//$limits['-1'] = $this->translateR( 'All' );
			return $limits;
		}
		function detDPLimit() {                  // getDPLimit
			$state = $this->getQueryState();
			@list( $page, $limit ) = CommonLib::explode( @$state->page, ':', 2 );
			$limits = $this->getAvailableLimits();
						
			if( $limit and array_key_exists( $limit, $limits )) {
				$this->setCookieVar( 'DPLimit', $limit );
				return $limit;
			}
			else{
				$limit = (int)$this->getCookieVar( 'DPLimit' );
				if( $limit and array_key_exists( $limit, $limits )) 
					return $limit;

				return parent::detDPLimit();
			}
		}
		function detDPPageNum() {                // getDPPageNum
			$state = $this->getQueryState();
			@list( $page, $limit ) = CommonLib::explode( @$state->page, ':', 2 );
			return $page > 0 ? $page - 1 : $page;
		}
		function detDPPagination() {             // getDPPagination
			$pagination = parent::detDPPagination();
			return $pagination ? CommonLib::mergeAssocs( $pagination, Array(
				'route' => 'list',
				'params' => Array(),
			)) : $pagination;
		}
		function detJSClass() {                  // getJSClass
			return __CLASS__;
		}
		function detJSInstanceOptions() {        // getJSInstanceOptions
			return CommonLib::mergeAssocs( parent::detJSInstanceOptions(), Array(
				'modelName' => $this->getModelName(),
			));
		}
		function detJSOptions() {                // getJSOptions
			$simpleSort = implode( ',', $this->getDPCriteriaSimpleSort() );
			$simpleSort = str_replace( ' ', ':', $simpleSort );
			$page = $this->getDPPageNum();
			$limit = $this->detDPLimit();
			$filters = $this->getFilters();
			
			$state = Array(
				'sort' => $simpleSort,
			);
			foreach( $filters as $key=>$value ) {
				$state[ "filter-{$key}" ] = $value;
			}
			
			if( $page ) {
				$page++;
				$state[ 'page' ] = "{$page}:{$limit}";
			}
			
			$cols = CommonLib::slice( $this->getVisibleColumns(), 'name' );
			return CommonLib::mergeAssocs( parent::detJSOptions(), Array(
				'state' => $state,
				'hiddenState' => Array(
					'cols' => implode( ',', $cols ),
				),
				'limit' => $this->getDPLimit(),
			));
		}
		function detJSLS() {                     // getJSLS
			$jsls = CommonLib::explode( 'Loading..., No results found., Delete?' );
			$jsls[] = 'Sorry. Request failed. Please try again later.';
			$jsls = array_combine( $jsls, $jsls );
			return CommonLib::mergeAssocs( parent::detJSLS(), $jsls );
		}
		function detColumns() {                  // getColumns 
			$columns = Array();
			$columnTpls = $this->getColumnTpls();
			$extraActionsTpls = $this->getExtraActionsTpls();
						
			$model = $this->getModel();
			$modelName = $model->_getName();
			$modelColumnsNames = $model->_getColumnNames();
			
			$hasI18N = I18NModelInterface::hasModelRelations( $modelName );
			if( $hasI18N ) {
				$I18NModelClass = I18NModelInterface::getI18NModelClass( $modelName );
				$I18NColumnsNames = CActiveRecord::model( $I18NModelClass )->_getColumnNames();
				$CLI18NRelationName = I18NModelInterface::getCLI18NRelationName();
				$CLI18NAlias = I18NModelInterface::getCLI18NAlias( $modelName );
			}			
			
			$XMLConfig = $this->detXMLConfig();
			$XPathConfig = new DOMXPath( $XMLConfig );
						
			$nodes = $XPathConfig->query( '/config/columns/column' );
			foreach( $nodes as $node ) {
				$column = CommonLib::nodeToObj( $node );
								
					// merge with tpl
				if( array_key_exists( @$column->tpl, $columnTpls )) {
					$column = CommonLib::mergeObjsR( new StdClass(), $columnTpls[ $column->tpl ], $column );
				}
				
					// tpls-atributes
				if( $column->name == "{firstPkName}" ) 
					$column->name = $model->_getFirstPkName();
				
					// extract relation, field
				if( @substr_count( $column->name, '-' )) {
					list( $relation, $field ) = CommonLib::explode( $column->name, '-', 2 );
					$column = CommonLib::mergeObjs( (object)compact( 'relation', 'field' ), $column );
				}
				elseif( !isset( $column->field )) 
					if( in_array( $column->name, $modelColumnsNames ))
						$column->field = $column->name;
					elseif( $hasI18N and in_array( $column->name, $I18NColumnsNames )) 
						$column = CommonLib::mergeObjs( (object)Array(
							'field' => $column->name,
							'relation' => $CLI18NRelationName,
							'alias' => $CLI18NAlias,
						), $column );
									
					// det alias
				if( !@$column->alias and @$column->field )	
					if( @$column->relation )
						$column->alias = $column->relation;
					elseif( in_array( @$column->field, $modelColumnsNames ))
						$column->alias = 't';
				
					// standart funcs
				$column = CommonLib::mergeObjs( (object)Array(
					'funcHeader' => 'renderDataHeader',
					'funcFilter' => 'renderDataFilter',
					'funcBody' => 'renderDataBody',
				), $column );
								
					// translate header
				if( isset( $column->header )) {
					$column->header = $this->translateR( $column->header );
				}
				
					// work with extra
				if( @$column->tpl == "extra"	) {
					if( @$column->action ) {
						if( !is_array( $column->action ))
							$column->action = Array( $column->action );
						foreach( $column->action as &$action ) {
							if( array_key_exists( @$action->tpl, $extraActionsTpls )) {
								$action = CommonLib::mergeObjsR( new StdClass(), $extraActionsTpls[ $action->tpl ], $action );
							}
						} unset( $action );
							// filter by rights
						$column->action = array_filter( $column->action, function( $action ) {
							return @$action->right ? Yii::App()->user->checkAccess( $action->right ) : true;
						});
					}
				}
				
				$columns[] = $column;
			}
			return $columns;
		}
		function detColumnTpls() {               // getColumnTpls
			$columnsTpls = Array();
			
			$XMLConfig = $this->getBaseXMLConfig();
			$XPathConfig = new DOMXPath( $XMLConfig );
			
			$nodes = $XPathConfig->query( '/config/columns-tpls/column-tpl' );
			foreach( $nodes as $node ) {
				$columnTpl = CommonLib::nodeToObj( $node );
				
				$columnsTpls[] = $columnTpl;
			}
			
			$columnsTpls = CommonLib::toAssoc( $columnsTpls, 'tpl' );
			return $columnsTpls;
		}
		function detExtraActionsTpls() {         // getExtraActionsTpls
			$actionsTpls = Array();
			
			$XMLConfig = $this->getBaseXMLConfig();
			$XPathConfig = new DOMXPath( $XMLConfig );
			
			$nodes = $XPathConfig->query( '/config/extra-actions-tpls/action-tpl' );
			foreach( $nodes as $node ) {
				$actionTpl = CommonLib::nodeToObj( $node );
				
				$actionsTpls[] = $actionTpl;
			}
			
			$actionsTpls = CommonLib::toAssoc( $actionsTpls, 'tpl' );
			return $actionsTpls;
		}
		function detVisibleColumns() {           // getVisibleColumns
			$visibleColumns = Array();
			$columns = $this->getColumns();
			$demandedColumnsNames = $this->getDemandedColumnsNames();
			$sortColumns = Array();
			$filterColumns = Array();
			
			$sort = $this->getSort();
			if( $sort ) {
				foreach( $sort as $part ) {
					list( $field ) = CommonLib::explode( $part, ":" );
					CommonLib::addToArray( $sortColumns , $field );
				}
			}
			
			$filters = $this->getFilters();
			if( $filters ) {
				foreach( $filters as $field => $value ) {
					CommonLib::addToArray( $filterColumns , $field );
				}			
			}
			
			foreach( $columns as $column ) {
				$demand = @$column->demand == 'yes';
				$required = @$column->required == 'yes';
				$demanded = in_array( $column->name, $demandedColumnsNames );
				$sorted = in_array( $column->name, $sortColumns );
				$filtered = in_array( $column->name, $filterColumns );
				
				$column->visible = $required || $sorted || $filtered ? true : ($demandedColumnsNames ? $demanded : !$demand);
				if( !$column->visible || @$column->hidden == 'yes' )
					continue;
				
				$visibleColumns[] = $column;
			}
			//@$visibleColumns[ count( $visibleColumns ) - 1 ]->last = true;
			return $visibleColumns;
		}
		function detHiddenColumns() {            // getHiddenColumns
			$hiddenColumns = Array();
			$columns = $this->getColumns();
			foreach( $columns as $column ) {
				if( !@$column->hidden == 'yes' )
					continue;
				
				$hiddenColumns[] = $column;
			}
			return $hiddenColumns;
		}
		function detData() {                     // getData
			return $data = $this->getDP()->getData();
		}
		function detSort() {                     // getSort
			$state = $this->getQueryState();
			$sort = @$state->sort ? $state->sort : null;
			$sort = CommonLib::explode( $sort );
			
			if( $sort ) 
				$this->setCookieVar( 'sort', $sort );
			else
				$sort = (array)$this->getCookieVar( 'sort' );
				
			if( $sort )
				return $sort;
			
			$columns = $this->getColumns();
			foreach( $columns as $column ) {
				if( @$column->defSort == 'yes' ) {
					$type = @$column->defSortType ? $column->defSortType : "ASC";
					return Array( "{$column->name} {$type}" );
				}
			}
		}
		function detFilters() {                  // getFilters
			$filters = new StdClass();
			$state = $this->getQueryState();
			foreach( $state as $key=>$value ) {
				if( !preg_match( "#^filter-(.+)$#", $key, $match )) 
					continue;
					
				$name = $match[1];
				$filters->$name = $value;
			}
			return $filters;
		}
		function detDemandedColumnsNames() {     // getDemandedColumnsNames
			$cols = CommonLib::explode(@$_GET[ 'cols' ]);
			
			if( $cols ) 
				$this->setCookieVar( 'demandedColumnsNames', $cols );
						
			if( !$cols ) {
				$cols = $this->getCookieVar( 'demandedColumnsNames' );
				$cols = is_array( $cols ) ? $cols : Array();
			}
			
			$cols = array_intersect( $cols, CommonLib::slice( $this->getColumns(), 'name' ));
			return $cols;
		}
		function detQueryStateName() {           // getQueryStateName
			return 'list-state';
		}
		function detPathXMLConfig() {            // getPathXMLConfig
			$class = $this->getClass();
			return $path = "widgets.lists.config.{$class}";
		}
			// common
		function getColumnSortClass( $column ) {
			if( !@$column->field or !@$column->alias )
				return;
			
			$c = $this->getDPCriteria();
			$orders = CommonLib::explode( $c->order );
			foreach( $orders as $order ) {
				$pattern = sprintf( "#`%s`\.`%s` (ASC|DESC)#", $column->alias, $column->field );
				if( preg_match( $pattern, $order, $matchs )) {
					list( $match, $type ) = $matchs;
					
					return $class = "sorting_".strtolower($type); 
				}
			}
			
			if( @$column->sorting == 'yes' )
				return $class = "sorting";
		}
			// header
		function renderColumnHeader( $view = null, $innerView = null, $data = Array(), $innerData = Array(), $return = false ) {
			$_column = &$innerData['column'];
			$_data = &$innerData['data'];
			
			$defData = Array( 
				'innerView' => $innerView,
				'innerData' => $innerData,
			);
			
			$htmlOptions = &$defData[ 'htmlOptions' ];
			$htmlOptions = Array();
			if( @$_column->htmlOptions )
				$htmlOptions = array_merge( (array)$_column->htmlOptions, $htmlOptions );
			if( @$_column->headerHtmlOptions )
				$htmlOptions = array_merge( $htmlOptions, (array)$_column->headerHtmlOptions );
			
			$htmlOptions[ 'data-name' ] = $_column->name;
			
			if( @$_column->resize == 'yes' )
				$htmlOptions[ 'data-resize' ] = 'yes';
			
			if( @$_column->sorting == 'yes' ) {
				$htmlOptions[ 'data-sorting' ] = 'yes';
				$htmlOptions[ 'data-def-sorting-type' ] = @$_column->defSortingType ? @$_column->defSortingType : 'ASC';
			}
				
			if( $sortClass = $this->getColumnSortClass( $_column ))
				@$htmlOptions[ 'class' ] .= " {$sortClass}";
			
			$innerView = &$defData[ 'innerView' ];
			if( strpos( $innerView, '::' ) === 0 )
				$innerView = self::getPartView( $innerView );
			
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "columns/columnHeader" );
			
			return CWidget::render( $view, $data, $return );
		}
		function renderDataHeader( $view = null, $data = Array(), $return = false ) {
			if( !strlen( $view )) 
				$view = self::getPartView( "columns/dataHeader" );
			return $this->renderColumnHeader( null, $view, Array(), $data, $return );
		}
			// filter
		function renderColumnFilter( $view = null, $innerView = null, $data = Array(), $innerData = Array(), $return = false ) {
			$_column = &$innerData['column'];
			$_data = &$innerData['data'];
			
			$defData = Array( 
				'innerView' => $innerView,
				'innerData' => $innerData,
			);
			
			$htmlOptions = &$defData[ 'htmlOptions' ];
			$htmlOptions = Array();
			if( @$_column->htmlOptions )
				$htmlOptions = array_merge( (array)$_column->htmlOptions, $htmlOptions );
			if( @$_column->filterHtmlOptions )
				$htmlOptions = array_merge( $htmlOptions, (array)$_column->filterHtmlOptions );
			
			$innerView = &$defData[ 'innerView' ];
			if( strpos( $innerView, '::' ) === 0 )
				$innerView = self::getPartView( $innerView );
			
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "columns/columnFilter" );
			return CWidget::render( $view, $data, $return );
		}
		function renderDataFilter( $view = null, $data = Array(), $return = false ) {
			if( !strlen( $view )) 
				$view = self::getPartView( "columns/dataFilter" );
			return $this->renderColumnFilter( null, $view, Array(), $data, $return );
		}
			// body
		function renderColumnBody( $view = null, $innerView = null, $data = Array(), $innerData = Array(), $return = false ) {
			$_column = &$innerData['column'];
			$_data = &$innerData['data'];
			
			$value = null;
			if( @$_column->field ) 
				if( @$_column->relation ) {
					if( isset( $_data->{$_column->relation}))
						if( is_array( $_data->{$_column->relation}))
							$value = CommonLib::slice( $_data->{$_column->relation}, $_column->field );
						else
							$value = $_data->{$_column->relation}->{$_column->field};
				}
				else
					if( isset( $_data->{$_column->field} ))
						$value = $_data->{$_column->field};
			
			if( @$_column->explode ) 
				$value = $value ? CommonLib::explode( $value, $_column->explode ) : Array();
						
			if( @$_column->translateR == 'yes' and $value )
				$value = $this->translateR( $value );
			
			$countValue = is_array( $value ) ? count( $value ) : 1;
			
			if( @$_column->implode ) 
				$value = implode( $_column->implode, $value );
			
			$defInnerData = Array(
				'value' => $value,
				'countValue' => $countValue,
			);
			$innerData = CommonLib::mergeAssocs( $defInnerData, $innerData );
			
			$defData = Array( 
				'innerView' => $innerView,
				'innerData' => $innerData,
			);
			
			$htmlOptions = &$defData[ 'htmlOptions' ];
			$htmlOptions = Array();
			if( @$_column->htmlOptions )
				$htmlOptions = array_merge( (array)$_column->htmlOptions, $htmlOptions );
			if( @$_column->bodyHtmlOptions )
				$htmlOptions = array_merge( $htmlOptions, (array)$_column->bodyHtmlOptions );
			
			$innerView = &$defData[ 'innerView' ];
			if( strpos( $innerView, '::' ) === 0 )
				$innerView = self::getPartView( $innerView );
			
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "columns/columnBody" );
			return CWidget::render( $view, $data, $return );
		}
		function renderDataBody( $view = null, $data = Array(), $return = false ) {
			if( !strlen( $view )) 
				$view = self::getPartView( "columns/dataBody" );
			return $this->renderColumnBody( null, $view, Array(), $data, $return );
		}
			// table parts
		function renderTable( $view = null, $data = Array(), $return = false ) {
			$defData = Array( 
				
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "table" );
			return CWidget::render( $view, $data, $return );
		}
		function renderTableHead( $view = null, $data = Array(), $return = false ) {
			$defData = Array( 
				'columns' => $this->getVisibleColumns(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "tableHead" );
			return CWidget::render( $view, $data, $return );
		}
		function renderTableBody( $view = null, $data = Array(), $return = false ) {
			$defData = Array( 
				'data' => $this->getData(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "tableBody" );
			return CWidget::render( $view, $data, $return );
		}
		function renderTableRow( $view = null, $data = Array(), $return = false ) {
			$defData = Array( 
				'columns' => $this->getVisibleColumns(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "tableRow" );
			return CWidget::render( $view, $data, $return );
		}
		function renderPager( $view = null, $data = Array(), $return = false ) {
			$defData = Array( 
				
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "pager" );
			return CWidget::render( $view, $data, $return );
		}
		function render( $view = null, $data = Array(), $return = false ) {
			$defData = Array(
				'cssClass' => $this->getCSSClass(),
			);
			
			$data = CommonLib::mergeAssocs( $defData, $data );
			return parent::render( $view, $data, $return );
		}
	}

?>