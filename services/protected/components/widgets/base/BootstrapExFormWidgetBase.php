<?
	Yii::import( 'bootstrap.widgets.TbActiveForm' );

	class BootstrapExFormWidgetBase extends TbActiveForm {
		protected $_labelOptions;
		protected $_errorOptions;
		protected $_classSpanInput;
		function colorFieldRow( $model, $attribute, $htmlOptions = Array()) {
			echo $this->labelEx( $model, $attribute );
			CHtml::resolveNameID( $model, $attribute, $htmlOptions );
			$htmlOptions[ 'type' ] = 'color';
			$htmlOptions[ 'value' ] = CHtml::resolveValue( $model, $attribute );
			echo CHtml::tag( 'input', $htmlOptions );
		}
		protected function openRow( $model, $attribute, $htmlOptions ) {
			$classGroup = 'control-group';
			$classGroup .= $model->hasErrors( $attribute ) ? ' '.CHtml::$errorCss : '';
			echo CHtml::openTag( 'div', Array( 'class' => $classGroup ));
			echo $this->labelEx( $model, $attribute, $this->_labelOptions );
			echo '<div class="controls">';
		}
		protected function closeRow( $model, $attribute, $htmlOptions ) {
			echo '</div>';
			echo $this->error( $model, $attribute, $this->_errorOptions );
			echo '</div>';
		}
		protected function resolveHTMLOptions( &$htmlOptions ) {
			$this->_labelOptions = Array();
			if( isset( $htmlOptions[ 'labelOptions' ])) {
				$this->_labelOptions = $htmlOptions[ 'labelOptions' ];
				unset( $htmlOptions[ 'labelOptions' ]);
			}
			@$this->_labelOptions[ 'class' ] .= ' control-label';
			
			$this->_errorOptions = Array();
			if( isset( $htmlOptions[ 'errorOptions' ])) {
				$this->_errorOptions = $htmlOptions[ 'errorOptions' ];
				unset( $htmlOptions[ 'errorOptions' ]);
			}
			
			$this->_classSpanInput = "";
			if( isset( $htmlOptions[ 'classSpanInput' ])) {
				$this->_classSpanInput = $htmlOptions[ 'classSpanInput' ];
				unset( $htmlOptions[ 'classSpanInput' ]);
			}
		}
		function textFieldRowIcon( $model, $attribute, $htmlOptions = Array()) {
			$this->resolveHTMLOptions( $htmlOptions );
			$this->openRow( $model, $attribute, $htmlOptions );
			echo '<span class="input-icon input-icon-right '.$this->_classSpanInput.'">';
			echo $this->textField( $model, $attribute, $htmlOptions );
			echo '</span><span class="note-'.$attribute.'"></span>';
			$this->closeRow( $model, $attribute, $htmlOptions );
		}
		function passwordFieldRowIcon( $model, $attribute, $htmlOptions = Array()) {
			$this->resolveHTMLOptions( $htmlOptions );
			$this->openRow( $model, $attribute, $htmlOptions );
			echo '<span class="input-icon input-icon-right '.$this->_classSpanInput.'">';
			echo $this->passwordField( $model, $attribute, $htmlOptions );
			echo '</span><span class="note-'.$attribute.'"></span>' ;
			$this->closeRow( $model, $attribute, $htmlOptions );
		}
		function dateFieldRow( $model, $attribute, $htmlOptions = Array()) {
			echo $this->labelEx( $model, $attribute );
			CHtml::resolveNameID( $model, $attribute, $htmlOptions );
			$htmlOptions[ 'type' ] = 'date';
			$htmlOptions[ 'value' ] = CHtml::resolveValue( $model, $attribute );
			echo CHtml::tag( 'input', $htmlOptions );
		}
		function datetimeFieldRow( $model, $attribute, $htmlOptions = Array()) {
			echo $this->labelEx( $model, $attribute );
			CHtml::resolveNameID( $model, $attribute, $htmlOptions );
			$htmlOptions[ 'type' ] = 'datetime-local';
			$htmlOptions[ 'value' ] = CHtml::resolveValue( $model, $attribute );
			echo CHtml::tag( 'input', $htmlOptions );
		}
		function pkFieldRow( $model, $attribute, $htmlOptions = Array() ) {
			if( array_key_exists( 'readonly', $htmlOptions )) {
				if( !$htmlOptions[ 'readonly' ]) 
					unset( $htmlOptions[ 'readonly' ]);
			}
			else
				$htmlOptions[ 'readonly' ] = 'readonly';
			
			if( !array_key_exists( 'class', $htmlOptions )) 
				$htmlOptions[ 'class' ] = 'input-medium';
			
			if( !array_key_exists( 'labelOptions', $htmlOptions )) {
				$htmlOptions[ 'labelOptions' ] = Array(
					'style' => 'display: inline-block; vertical-align:top; margin: 5px 10px 0 0;',
				);
			}
			
			return $this->textFieldRow( $model, $attribute, $htmlOptions );
		}
	}

?>