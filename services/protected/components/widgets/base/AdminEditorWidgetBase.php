<?
	
	class AdminEditorWidgetBase extends EditorWidgetBase {
		public $modelName;
		public $modelClass;
		public $modelCLI18NAlias;
		public $pkName;
		public $DPCriteria;
		public $DP;
		protected $formWidget;
		protected $listWidget;
			// factory
		static protected function getClassByModelName( $modelName ) {
			return "Admin{$modelName}EditorWidget";
		}
		static protected function getAliasByClass( $class ) {
			return "widgets.editors.admin.{$class}";
		}
		static protected function getDefAlias() {
			return "widgets.base.".__CLASS__;
		}
		static function createWidgetByModelName( $modelName, $properties = Array(), $_class = __CLASS__ ) {
			return parent::createWidgetByModelName( $modelName, $properties, $_class );
		}
		static function widgetByModelName( $modelName, $properties = Array(), $captureOutput = false, $_class = __CLASS__ ) {
			return parent::widgetByModelName( $modelName, $properties, $captureOutput, $_class );
		}
			// views
		static protected function getPartView( $view ) {
			return "widgets.editors.admin.views._parts.{$view}";
		}
		static protected function getBaseView( $view ) {
			return "widgets.editors.admin.views._base.{$view}";
		}
			// dets
		function detModelName() {
			$class = $this->getClass();
			return preg_replace( "#^Admin|EditorWidget$#", "", $class );
		}
		function detModelClass() {
			$modelName = $this->getModelName();
			return "{$modelName}Model";
		}
		function detPkName() {
			$modelClass = $this->getModelClass();
			return CActiveRecord::model( $modelClass )->_getFirstPkName();
		}
		function detDPCriteria() {
			return null;
		}
		function detDP() {
			return null;
		}
		function detView() {
			return self::getBaseView( "adminEditor" );
		}
		function detJSClass() {
			return __CLASS__;
		}
		function detJSInstanceOptions() {
			return CommonLib::mergeAssocs( parent::detJSInstanceOptions(), Array(
				'modelName' => $this->getModelName(),
			));
		}
		function detJSLS() {
			$jsls = CommonLib::explode( 'Deleting..., Deleted, Delete selected?, Loading...' );
			$jsls[] = 'Sorry. Request failed. Please try again later.';
			$jsls = array_combine( $jsls, $jsls );
			return CommonLib::mergeAssocs( parent::detJSLS(), $jsls );
		}
			// widgets
		function detFormWidget( $properties = Array() ) {
			return $this->createWidgetForm( $properties );
		}
		function detListWidget( $properties = Array() ) {
			$defProperties = Array(
				'DP' => $this->getDP(),
			);
			$properties = CommonLib::mergeAssocs( $defProperties, $properties );
			return $this->createWidgetList( $properties );
		}
		function createWidgetForm( $properties = Array()) {
			$modelName = $this->getModelName();
			return $formWidget = AdminFormWidgetBase::createWidgetByModelName( $modelName, $properties );
		}
		function createWidgetList( $properties = Array()) {
			$modelName = $this->getModelName();
			return AdminListWidgetBase::createWidgetByModelName( $modelName, $properties );
		}
			// renders
		function renderControlBlock( $view = null, $data = Array(), $return = false ) {
			$modelName = strtolower( $this->getModelName( ));
			$defData = Array(
				'titleAddButton' => $this->translateR( "Add {$modelName}" ),
				'NSi18n' => $this->getNSi18n(),
				'ins' => $this->getINS(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "adminEditorControlBlock" );
			return CWidget::render( $view, $data, $return );
		}
		function renderFormWidget( $properties = Array(), $captureOutput = false ) {
			return $this->getFormWidget( $properties )->run( null, Array(), $captureOutput );
		}
		function renderListWidget( $properties = Array(), $captureOutput = false ) {
			return $this->getListWidget( $properties )->run( null, Array(), $captureOutput );
		}
		function renderListWidgetRowByPk( $id, $properties = Array(), $return = false ) {
			$listWidget = $this->getListWidget( $properties );
			$DP = $listWidget->getDP();
						
			$pkName = $this->getPkName();
			$DP->criteria->addColumnCondition(Array(
				" `t`.`{$pkName}` " => $id,
			));
			
			$data = $listWidget->getData();
			if( $data )
				return $listWidget->renderTableRow( null, Array( 'data' => $data[0] ), $return );
		}
		function render( $view = null, $data = Array(), $return = false ) {
			$defData = Array(
				'cssClass' => $this->getCSSClass(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			return parent::render( $view, $data, $return );
		}
	}

?>