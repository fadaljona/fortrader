<?php
	
	class UserProfileFormWidgetBase extends CActiveForm {
		
		public function textFieldInp($model, $attribute, $htmlOptions = array()){
			$labelSettings = $this->getLabelSettings($model,$attribute, $htmlOptions );
			
			$outHtml = CHtml::openTag('label', $labelSettings['htmlOptions']);
				$outHtml .= CHtml::tag('div', array( 'class' => 'inside-form__label-text' ), $labelSettings['text'] . $labelSettings['requiredMarker']) . $this->getErrorTextContainer();
				$outHtml .= $this->textField($model,$attribute,$htmlOptions);
			$outHtml .= CHtml::closeTag('label');
			
			return $outHtml;
		}
		private function getErrorTextContainer(){
			return CHtml::tag('div', array( 'class' => 'form-error' ), '');
		}
		public function textFieldInpWithToolTip($model, $attribute, $toolTip, $htmlOptions = array()){
			$labelSettings = $this->getLabelSettings($model,$attribute, $htmlOptions );
			
			$toolTipHtml = '';
			if( $toolTip ){
				$toolTipHtml = CHtml::tag('div', array('class' => 'inside-form__inform'), CHtml::tag('div', array('class' => 'inside-form__toltip'), $toolTip ) );
			}
			
			$outHtml = CHtml::openTag('label', $labelSettings['htmlOptions']);
				$outHtml .= CHtml::tag('div', array( 'class' => 'inside-form__label-text' ), $labelSettings['text'] . $labelSettings['requiredMarker']) . $this->getErrorTextContainer();
				$outHtml .= $this->textField($model,$attribute,$htmlOptions) . $toolTipHtml;
			$outHtml .= CHtml::closeTag('label');
			
			return $outHtml;
		}
		
		private function getLabelSettings($model,$attribute, &$htmlOptions = array()){
			$labelHtmlOptions = array();
			if( isset( $htmlOptions['labelHtmlOptions'] ) ) $labelHtmlOptions = $htmlOptions['labelHtmlOptions'];
			unset( $htmlOptions['labelHtmlOptions'] );
			
			$inputName=CHtml::resolveName($model,$attribute);
			$labelHtmlOptions['for']=CHtml::getIdByName($inputName);
			$label=$model->getAttributeLabel($attribute);

			if (!isset($htmlOptions['required']))
				$htmlOptions['required']=$model->isAttributeRequired($attribute);
			
			$requiredMarker = '';
			if( $htmlOptions['required'] ){
				$requiredMarker = ' ' . CHtml::tag('span', array( 'class' => 'color_red' ), '*');
			}
			
			return array(
				'text' => $label,
				'htmlOptions' => $labelHtmlOptions,
				'requiredMarker' => $requiredMarker
			);
		}
		
		public function dropDownListInp( $model,$attribute,$data,$htmlOptions=array() ){
			
			if( isset( $htmlOptions['class'] ) && $htmlOptions['class'] == 'inside-form__select' ){
				$cs=Yii::app()->getClientScript();
				$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
				$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
				$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
				Yii::app()->clientScript->registerScript('jsStylerSelectinside-form__select', '
					!function( $ ) {
							
						$(".inside-form__select").styler();
						
					}( window.jQuery );
				', CClientScript::POS_END);
			}
			
			$labelSettings = $this->getLabelSettings($model,$attribute, $htmlOptions );
			
			$outHtml = CHtml::openTag('label', $labelSettings['htmlOptions']);
				$outHtml .= CHtml::tag('div', array( 'class' => 'inside-form__label-text' ), $labelSettings['text'] . $labelSettings['requiredMarker']) . $this->getErrorTextContainer();
				$outHtml .= $this->dropDownList($model,$attribute,$data,$htmlOptions);
			$outHtml .= CHtml::closeTag('label');

			
			return $outHtml;
		}
		private function listCountriesOptions($selection,$data ){
			
			$content = '';
			$flagStyles = '';
			foreach( $data as $id => $country ){
				$flagSrc = CountryModel::getSrcFlagByAlias('shiny', 16, $country->alias);
				$flagClass = '';
				if( $flagSrc ){
					$flagClass = "inside-lang_{$country->alias}";
					$flagStyles .= ".{$flagClass}{background: url({$flagSrc}) center left 12px no-repeat;}.{$flagClass} .jq-selectbox__select-text {background: url({$flagSrc}) center left no-repeat;}";
				}
				
				$content .= CHtml::tag('option', array( 'value' => $id, 'selected' => $selection == $id, 'class' => 'inside_lang ' . $flagClass ), $country->name);
			}
			
			Yii::app()->clientScript->registerCss('dropDownListCountriesFlagStyles', $flagStyles);
			return $content;
		}
		public function dropDownListCountries( $model,$attribute,$data,$htmlOptions=array() ){
			
			$cs=Yii::app()->getClientScript();
			$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
			$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
			$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
			Yii::app()->clientScript->registerScript('jsStylerSelectinside-form__select', '
				!function( $ ) {
						
					$(".inside-form__select").styler();
					
				}( window.jQuery );
			', CClientScript::POS_END);
			
			$labelSettings = $this->getLabelSettings($model,$attribute, $htmlOptions );
			
			CHtml::resolveNameID($model,$attribute,$htmlOptions);
			$selection=CHtml::resolveValue($model,$attribute);
			$options="\n".$this->listCountriesOptions($selection,$data);
			
			$outHtml = CHtml::openTag('label', $labelSettings['htmlOptions']);
				$outHtml .= CHtml::tag('div', array( 'class' => 'inside-form__label-text' ), $labelSettings['text'] . $labelSettings['requiredMarker']) . $this->getErrorTextContainer();
				$outHtml .= CHtml::tag('select',$htmlOptions,$options);
			$outHtml .= CHtml::closeTag('label');
			
			return $outHtml;
		}
		public function textAreaInp($model,$attribute,$htmlOptions=array()){
			$labelSettings = $this->getLabelSettings($model,$attribute, $htmlOptions );
			
			$outHtml = CHtml::openTag('label', $labelSettings['htmlOptions']);
				$outHtml .= CHtml::tag('div', array( 'class' => 'inside-form__label-text' ), $labelSettings['text'] . $labelSettings['requiredMarker']) . $this->getErrorTextContainer();
				$outHtml .= $this->textArea($model,$attribute,$htmlOptions);
			$outHtml .= CHtml::closeTag('label');
			
			return $outHtml;
		}
		public function passwordInp($model,$attribute,$htmlOptions=array()){
			$labelSettings = $this->getLabelSettings($model,$attribute, $htmlOptions );
			
			$outHtml = CHtml::openTag('label', $labelSettings['htmlOptions']);
				$outHtml .= CHtml::tag('div', array( 'class' => 'inside-form__label-text' ), $labelSettings['text'] . $labelSettings['requiredMarker']) . $this->getErrorTextContainer();
				$outHtml .= $this->passwordField($model,$attribute,$htmlOptions);
			$outHtml .= CHtml::closeTag('label');
			
			return $outHtml;
		}
		public function textAreaInpWithToolTip($model,$attribute, $toolTip,$htmlOptions=array()){
			$labelSettings = $this->getLabelSettings($model,$attribute, $htmlOptions );
			
			$toolTipHtml = '';
			if( $toolTip ){
				$toolTipHtml = CHtml::tag('div', array('class' => 'inside-form__inform'), CHtml::tag('div', array('class' => 'inside-form__toltip'), $toolTip ) );
			}
			
			$outHtml = CHtml::openTag('label', $labelSettings['htmlOptions']);
				$outHtml .= CHtml::tag('div', array( 'class' => 'inside-form__label-text' ), $labelSettings['text'] . $labelSettings['requiredMarker']) . $this->getErrorTextContainer();
				$outHtml .= $this->textArea($model,$attribute,$htmlOptions) . $toolTipHtml;
			$outHtml .= CHtml::closeTag('label');
			
			return $outHtml;
		}
		public function checkBoxInp($model,$attribute,$htmlOptions=array()){
			
			if( isset( $htmlOptions['class'] ) && $htmlOptions['class'] == 'inside-form__checkbox' ){
				$cs=Yii::app()->getClientScript();
				$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
				$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
				$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
				Yii::app()->clientScript->registerScript('jsStylercheckboxinside-form__checkbox', '
					!function( $ ) {
							
						$(".inside-form__checkbox").styler();
						
					}( window.jQuery );
				', CClientScript::POS_END);
			}
			
			$labelSettings = $this->getLabelSettings($model,$attribute, $htmlOptions );
			
			$outHtml = CHtml::openTag('label', $labelSettings['htmlOptions']);
				$outHtml .= $this->checkBox($model,$attribute,$htmlOptions);
				$outHtml .= CHtml::tag('div', array( 'class' => 'inside-form__checkbox-text' ), $labelSettings['text'] ) . $this->getErrorTextContainer();
			$outHtml .= CHtml::closeTag('label');
			
			return $outHtml;
		}
		
	}

?>