<?
	
	abstract class WidgetExBase extends CWidget {
		public $class;
		public $w;
		public $INS;
		public $CSSClass;
		public $NSi18n;
		public $view;
		public $DPPagination;
		public $DPLimit;
		public $DPPageNum;
		public $DPPageVar;
		public $JSClass;
		public $JSOptions;
		public $JSInstanceOptions;
		public $JSLS;
		public $cookieKey;
		public $queryStateName;
		public $queryState;
		public $pathXMLConfig;
		public $XMLConfig;
		public $debug;
			// factory
		static protected function getClassByModelName( $modelName ) {
			return __CLASS__;
		}
		static protected function getAliasByClass( $class ) {
			return "widgets.{$class}";
		}
		static protected function getDefAlias() {
			return "widgets.base.".__CLASS__;
		}
		static protected function getExistsAliasByClass( $class, $_class = __CLASS__ ) {
			$alias = call_user_func( Array( $_class, 'getAliasByClass' ), $class );
			$path = Yii::getPathOfAlias( $alias );
			$path = "{$path}.php";
			if( !is_file( $path )) $alias = call_user_func( Array( $_class, 'getDefAlias' ), $class );
			return $alias;
		}
		static function createWidgetByModelName( $modelName, $properties = Array(), $_class = __CLASS__ ) {
			$properties[ 'class' ] = call_user_func( Array( $_class, 'getClassByModelName' ), $modelName );
			$alias = self::getExistsAliasByClass( $properties[ 'class' ], $_class );
			return Yii::app()->controller->createWidget( $alias, $properties );
		}
		static function widgetByModelName( $modelName, $properties = Array(), $captureOutput = false, $_class = __CLASS__ ) {
			$properties[ 'class' ] = call_user_func( Array( $_class, 'getClassByModelName' ), $modelName );
			$alias = self::getExistsAliasByClass( $properties[ 'class' ], $_class );
			return Yii::app()->controller->widget( $alias, $properties, $captureOutput );
		}
			//views
		static protected function getPartView( $view ) {
			return "widgets.views._parts.{$view}";
		}
			// dets
		function detClass() {                       // getClass
			return get_class( $this );
		}
		function detView() {                        // getView
			return "view";
		}
		function detW() {                           // getW
			$class = $this->getClass();
			return "w{$class}";
		}
		function detINS() {                         // getINS
			$class = $this->getClass();
			return "ins{$class}";
		}
		function detCssClass() {                    // getCssClass
			return $this->getW();
		}
		function detNSi18n() {                      // getNSi18n
			$class = $this->getClass();
			return Array( "components/widgets/{$class}" );
		}
		function detJSClass() {                     // getJSClass
			return $jsClass = $this->getClass();
		}
		function detJSLS() {                        // getJSLS
			return Array();
		}
		function detJSOptions() {                   // getJSOptions
			$options = Array();
			
			$jsls = $this->getJSLS();
			if( $jsls ) 
				$options[ 'ls' ] = $this->translateR($jsls);
			
			$ins = $this->getINS();
			if( $ins )
				$options[ 'ins' ] = $ins;
			
			return $options;
		}
		function detJSInstanceOptions() {           // getJSInstanceOptions
			return Array();
		}
		function detCookieKey() {                   // getCookieKey
			$class = $this->getClass();
			return $key = "{$class}_cookie";
		}
		function detPathXMLConfig() {               // getPathXMLConfig
			$this->throwI18NException( "WidgetExBase can't have xml-config file!" );
		}
		function detXMLConfig() {                   // getXMLConfig
			$path = $this->getPathXMLConfig();
			$XML = new DOMDocument( "1.0", "UTF-8" );
			$path = Yii::getPathOfAlias( $path );
			$XML->load( "{$path}.xml" );
			return $XML;
		}
		function detDPLimit() {                     // getDPLimit
			return ActionBase::DPLimit;
		}
		function detDPPageVar() {                   // getDPPageVar
			return ActionBase::DPPageVar;
		}
		function detDPPageNum() {                   // getDPPageNum
			$page = (int)$_REQUEST[ 'page' ];
			return $page > 0 ? $page - 1 : $page;
		}
		function detDPPagination() {                // getDPPagination
			return $this->getDPLimit() >= 0 ? Array(
				'pageSize' => $this->getDPLimit(),
				'pageVar' => $this->getDPPageVar(),
				'currentPage' => $this->getDPPageNum(),
			) : false;
		}
		function detDebug() {                       // getDebug
			return false;
		}
			// for CWidget
		function getViewPath( $checkTheme = false ) {
			$class = $this->getClass();
			$viewPath = parent::getViewPath( $checkTheme );
			return "{$viewPath}/{$class}";
		}
			// common
		function translateR( $array, $params=array() ) {
			$NSi18n = $this->getNSi18n();
			return Yii::t( $NSi18n, $array );	
		}
		protected function throwI18NException( $message, $params = Array(), $class = null ) {
			throw new Exception( $this->translateR( $message, $params ));
		}
			// cookie
		function getCookieObj() {
			$key = $this->getCookieKey();
			$str = CommonLib::fromCookie( $key );
			return $str && ($obj = @json_decode( $str )) ? $obj : new StdClass();
		}
		function setCookieObj( $obj ) {
			$key = $this->getCookieKey();
			$str = json_encode( $obj );
			CommonLib::toCookie( $key, $str );
		}
		function getCookieVar( $key ) {
			$obj = $this->getCookieObj();
			return @$obj->$key;
		}
		function setCookieVar( $key, $value ) {
			$obj = $this->getCookieObj();
			$obj->$key = $value;
			$this->setCookieObj( $obj );
		}
		function pushCookieVar( $key, $value ) {
			$obj = $this->getCookieObj();
			if( !isset( $obj->$key ) or !is_array( $obj->$key ))
				$obj->$key = Array();
			
			CommonLib::addToArray( $obj->$key, $value );
			$this->setCookieObj( $obj );
		}
		function popCookieVar( $key, $value ) {
			$obj = $this->getCookieObj();
			
			if( !isset( $obj->$key ) or !is_array( $obj->$key ))
				return;
				
			CommonLib::removeFromArray( $obj->$key, $value );
			$this->setCookieObj( $obj );
		}
		function inCookieVar( $key, $value ) {
			$obj = $this->getCookieObj();
			return isset( $obj->$key ) && is_array( $obj->$key ) && in_array( $value, $obj->$key );
		}
			// state
		function detQueryStateName() {
			return 'state';
		}
		function detQueryState() {
			$name = $this->getQueryStateName();
			$state = new StdClass();
			
			$var = @$_REQUEST[ $name ];
			if( $var ) {
				$pattern = "#^\{([\s\S]+)\}$#";
				if( preg_match( $pattern, $var, $match )) {
					$var = $match[1];
					foreach( CommonLib::explode( $var, '|' ) as $part ) {
						@list( $key, $value ) = CommonLib::explode( $part, '=', 2 );
						$state->$key = $value;
					}
				}
			}
			
			return $state;
		}
			// render
		function renderJSConstructor( $view = null, $data = Array(), $return = false ) {
			if( Yii::App()->request->isAjaxRequest )
				return;
			
			$defData = Array(
				'w' => $this->getW(),
				'class' => $this->getJSClass(),
				'options' => $this->getJSOptions(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "JSConstructor" );
			return parent::render( $view, $data, $return );
		}
		function renderJSInstance( $view = null, $data = Array(), $return = false ) {
			if( Yii::App()->request->isAjaxRequest )
				return;
				
			$defData = Array(
				'w' => $this->getW(),
				'class' => $this->getJSClass(),
				'instanceOptions' => (object)$this->getJSInstanceOptions(),
				'options' => $this->getJSOptions(),
			);
			$data = CommonLib::mergeAssocs( $defData, $data );
			if( !strlen( $view )) 
				$view = self::getPartView( "JSInstance" );
			return parent::render( $view, $data, $return );
		}
		function render( $view = null, $data = Array(), $return = false ) {
			if( !strlen( $view )) 
				$view = $this->getView();
			return parent::render( $view, $data, $return );
		}
			// run
		function run( $view = null, $data = Array(), $return = false ) {
			$oldDBProfiling = CommonLib::getDBProfiling();
			if( $this->getDebug() && !$oldDBProfiling )
				CommonLib::enableDBProfiling();
							
			$result = $this->render( $view, $data, $return );
			
			if( $this->getDebug() && !$oldDBProfiling )
				CommonLib::disableDBProfiling();
			
			return $result;
		}
	}

?>