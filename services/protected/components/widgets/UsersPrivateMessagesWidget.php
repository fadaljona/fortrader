<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.UserPrivateMessageFormModel' );
	
	final class UsersPrivateMessagesWidget extends WidgetBase {
		public $idUser;
		public $idWith;
		public $with;
		public $action;
		public $limitMessages = 5;
		public $idLastMessage;
		private function getFormModel() {
			$formModel = new UserPrivateMessageFormModel();
			return $formModel;
		}
		private function getIDUser() {
			if( !$this->idUser ) $this->idUser = Yii::App()->user->id;
			return $this->idUser;
		}
		private function getAction() {
			if( !$this->action ) {
				$this->action = $this->controller->action->id;
				if( !in_array( $this->action, Array( 'list', 'dialog' ))) $this->action = 'list'; 
			}
			return $this->action;
		}
		private function getIDWith() {
			if( !$this->idWith ) $this->idWith = abs((int)@$_GET['idWith']);
			return $this->idWith;
		}
		private function getWith() {
			if( !$this->with ) $this->with = UserModel::model()->findByPk( $this->getIDWith());
			if( !$this->with ) $this->throwI18NException( "Can't find User!" );
			return $this->with;
		}
		private function getIDLastMessage() {
			if( !$this->idLastMessage ) $this->idLastMessage = (int)@$_GET['idLastMessage'];
			return $this->idLastMessage;
		}
		private function getMessages() {
			switch( $this->getAction()) {
				case 'list':{
					TempGroupedUserPrivateMessageModel::createTable( $this->getIDUser(), $this->getIDLastMessage(), $this->limitMessages + 1 );
					$messages = TempGroupedUserPrivateMessageModel::model()->findAll(Array(
						'with' => Array(
							'message' => Array(
							),
							'message.from' => Array(
								'select' => Array( 'ID', 'user_login', 'user_nicename', 'display_name', 'firstName', 'lastName' ),
							),
							'message.from.avatar' => Array(
								'select' => Array( 'idUser', 'thumbPath' ),
							),
							'message.to' => Array(
								'select' => Array( 'ID', 'user_login', 'user_nicename', 'display_name', 'firstName', 'lastName' ),
							),
							'message.from.groups' => Array(
								'select' => Array( 'id', 'name' ),
							),
						),
						'order' => "`t`.`id` DESC",
						'limit' => $this->limitMessages,
					));
					return $messages;
				}
				case 'dialog':{
					$idUser = $this->getIDUser();
					$messages = UserPrivateMessageModel::model()->findAll(Array(
						'with' => Array(
							'from' => Array(
								'select' => Array( 'ID', 'user_login', 'user_nicename', 'display_name', 'firstName', 'lastName' ),
							),
							'from.avatar' => Array(
								'select' => Array( 'idUser', 'thumbPath' ),
							),
							'from.groups' => Array(
								'select' => Array( 'id', 'name' ),
							),
						),
						'condition' => "
							( :idLastMessage = 0 OR `t`.`id` < :idLastMessage)
							AND (
								`t`.`idTo` = :idWith AND `t`.`idFrom` = :idUser
								OR `t`.`idFrom` = :idWith AND `t`.`idTo` = :idUser
							)
						",
						'order' => "`t`.`id` DESC",
						'limit' => $this->limitMessages,
						'params' => Array(
							':idLastMessage' => $this->getIDLastMessage(),
							':idUser' => $idUser,
							':idWith' => $this->getIDWith(),
						),
					));
					$ids = Array();
					foreach( $messages as $message ) if( $message->idFrom != $idUser ) $ids[] = $message->id;
					if( $ids ) UserPrivateMessageModel::open( $ids );
					return $messages;
				}
			}
		}
		function run() {
			$class = $this->getCleanClassName();
			$action = $this->getAction();
			$this->render( "{$class}/view", Array(
				'formModel' => $this->getFormModel(),
				'action' => $action,
				'with' => $action == 'dialog' ? $this->getWith() : null,
			));
		}
		function renderMessages() {
			$messages = $this->getMessages();
			foreach( $messages as $message ){
				$this->renderMessage( $message );
			}
		}
		function renderMessage( $message ) {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/message", Array(
				'model' => $message,
				'action' => $this->getAction(),
			));
			$this->idLastMessage = $message->id;
		}
		function issetMoreMessages() {
			switch( $this->getAction()) {
				case 'list':{
					return TempGroupedUserPrivateMessageModel::model()->count() > $this->limitMessages;
				}
				case 'dialog':{
					return UserPrivateMessageModel::model()->exists(Array(
						'condition' => "
							`t`.`id` < :idLastMessage
							AND (
								`t`.`idTo` = :idWith AND `t`.`idFrom` = :idUser
								OR `t`.`idFrom` = :idWith AND `t`.`idTo` = :idUser
							)
						",
						'params' => Array(
							':idLastMessage' => $this->getIDLastMessage(),
							':idUser' => $this->getIDUser(),
							':idWith' => $this->getIDWith(),
						),
					));
				}
			}
		}
	}

?>