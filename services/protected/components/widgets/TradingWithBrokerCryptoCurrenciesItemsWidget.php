<?php
Yii::import('components.widgets.base.WidgetBase');

final class TradingWithBrokerCryptoCurrenciesItemsWidget extends WidgetBase
{
    public $dp;
        
    private function renderItem($model)
    {
        require dirname(__FILE__).'/views/tradingWithBrokerCryptoCurrenciesItems/item.php';
    }
    
    public function run()
    {
        foreach ($this->dp->getData() as $model) {
            $this->renderItem($model);
        }
    }
}
