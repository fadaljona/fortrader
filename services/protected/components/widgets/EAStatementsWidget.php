<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EAStatementsWidget extends WidgetBase {
		public $limit = 4;
		public $idEA;
		public $idEAVersion;
		public $dateLast;
		public $idsSkip;
		private $statements;
		private function getDateLast() {
			if( $this->dateLast ) return $this->dateLast;
			$dateLast = @$_GET['dateLast'];
			return $dateLast ? $dateLast : null;
		}
		private function getIDsSkip() {
			if( $this->idsSkip ) return $this->idsSkip;
			$idsSkip = @$_GET['idsSkip'];
			return $idsSkip ? explode( ',', $idsSkip ) : null;
		}
		private function getCriteria() {
			$c = new CDbCriteria();
			$c->select = "id,nameFileGraph,symbol,period,type,date";
			
			$c->addCondition( " LENGTH(`t`.`nameFileGraph`) " );
			
			if( $this->idEA ) {
				$c->addCondition( " `t`.`idEA` = :idEA " );
				$c->params[ ':idEA' ] = $this->idEA;
			}
			if( $this->idEAVersion ) {
				$c->addCondition( " `t`.`idVersion` = :idVersion " );
				$c->params[ ':idVersion' ] = $this->idEAVersion;
			}
			
			$dateLast = $this->getDateLast();			
			if( $dateLast ) {
				$c->addCondition( " `t`.`date` <= :dateLast " );
				$c->params[ ':dateLast' ] = $dateLast;
			}
			
			$idsSkip = $this->getIDsSkip();			
			if( $idsSkip ) {
				$c->addNotInCondition( "`t`.`id`", $idsSkip );
			}
			
			$c->order = " `t`.`date` DESC, `t`.`id` DESC ";
			$c->limit = $this->limit;
			
			return $c;
		}
		private function getStatements() {
			if( $this->statements === null ) {
				$c = $this->getCriteria();
				$this->statements = EAStatementModel::model()->findAll( $c );
			}
			return $this->statements;
		}
		private function renderStatement( $i, $statement ) {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/statement", Array(
				'i' => $i,
				'statement' => $statement,
			));
			$this->dateLast = $statement->date;
			
			static $holdData;
			if( !$holdData or $holdData != $statement->date ) {
				$holdData = $statement->date;
				$this->idsSkip = Array( $statement->id );
			}
			else{
				$this->idsSkip[] = $statement->id;
			}
		}
		function run() {
			if( !$this->getStatements() ) return;
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
			));
		}
		function renderList() {
			$statements = $this->getStatements();
			foreach( $statements as $i=>$statement ) {
				$this->renderStatement( $i, $statement );
			}
		}
		function issetMore() {
			$c = $this->getCriteria();
			$exists = EAStatementModel::model()->exists( $c );
			return $exists;
		}
	}

?>