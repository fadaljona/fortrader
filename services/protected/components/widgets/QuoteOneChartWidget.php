<?
Yii::import('components.widgets.base.WidgetBase');

final class QuoteOneChartWidget extends WidgetBase{
	public $model;
	public $ajax = false;
	private $periods = array(
		'MN1' => array( 'minPeriod' => "MM", 'categoryBalloonDateFormat' => "MMMM YYYY", 'name' => 'Month', 'periodVal' => 43200 ),
		'W1' => array( 'minPeriod' => "7DD", 'categoryBalloonDateFormat' => "MMMM DD YYYY", 'name' => 'Week', 'periodVal' => 10080 ),
		'D1' => array( 'minPeriod' => "DD", 'categoryBalloonDateFormat' => "MMMM DD YYYY", 'name' => 'Chart Day', 'periodVal' => 1440 ),
		'H1' => array( 'minPeriod' => "hh", 'categoryBalloonDateFormat' => "JJ:NN, MMMM DD YYYY", 'name' => 'Hour', 'periodVal' => 60 ),
	);

	private function getHistory(){
		$existHistory = QuotesHistoryDataModel::hasData( $this->model->id, 10080 );
		$dataFound = 0;
		if( $existHistory ){
			$this->periods['W1']['data'] = QuotesHistoryDataModel::getDataForPeriodChart( $this->model->id, 10080 );
			$dataFound = 1;
		}
		foreach( $this->periods as $period => $periodData ){
			$existHistory = QuotesHistoryDataModel::hasData( $this->model->id, $periodData['periodVal'] );
			if( !$existHistory ){
				unset( $this->periods[$period] );
			}
			if( $existHistory && !$dataFound ){
				$this->periods[$period]['data'] = QuotesHistoryDataModel::getDataForPeriodChart( $this->model->id, $periodData['periodVal'] );
				$dataFound = 1;
			}
		}
		return $this->periods;
	}
	
	function run() {
		$this->render( $this->getCleanClassName() . "/view", Array(
			'model' => $this->model,
			'periods' => $this->getHistory(),
			'ajax' => $this->ajax,
		));
	}
	
}
?>
