<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesArchivePreviewWidget extends WidgetBase {
		public $dataType = 'cbr';
	
		function run() {
			$class = $this->getCleanClassName();
			
			$settings = CurrencyRatesSettingsModel::getModel();
			$title = $settings->getArchiveTitle($this->dataType);
			
			if(!$title){
				$metaTitleTmp = $settings->getArchiveMetaTitle($this->dataType);
				if( $metaTitleTmp ){
					$metaTitle = $metaTitleTmp;
				} else{
					$metaTitle = Yii::t( '*', 'Currency Rates Archive' );
				}
				$title = $metaTitle;
			}

			$this->render( "{$class}/view", Array(
				'years' => CurrencyRatesModel::getYearsAvailable($this->dataType),
				'title' => $title,
				'dataType' => $this->dataType
			));
		}
	}

?>