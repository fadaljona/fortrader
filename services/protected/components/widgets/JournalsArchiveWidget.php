<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class JournalsArchiveWidget extends WidgetBase {
		public $limit = 4;
		public $dateLast;
		public $idsSkip;
		private $journals;
        public $datejournalMax = '';
        public $datejournalMaxKey = '';
		public $type = 0;

		private function setType() {
			$type = @$_GET['type'];
			if( $type && is_int( intval($type) ) ){
				$this->type = $type;
			}
		}
		private function setLimit() {
			$limit = @$_GET['limit'];
			if( $limit && is_int( intval($limit) ) ){
				$this->limit = $limit;
			}
		}
		private function getDateLast() {
			if( $this->dateLast ) return $this->dateLast;
			$dateLast = @$_GET['dateLast'];
			return $dateLast ? $dateLast : null;
		}
		private function getIDsSkip() {
			if( $this->idsSkip ) return $this->idsSkip;
			$idsSkip = @$_GET['idsSkip'];
			return $idsSkip ? explode( ',', $idsSkip ) : null;
		}
		private function getCriteria() {
			$this->setLimit();
			$this->setType();
			$c = new CDbCriteria();
			
			
			$c->addCondition('`t`.`isDigest` = :type');
			$c->params[ ':type' ] = $this->type;

			
			$c->with[ 'i18ns' ] = Array();
			$c->with[ 'currentLanguageI18N' ] = Array();
			
			$dateLast = $this->getDateLast();
			if( $dateLast ) {
				$c->addCondition( " `t`.`date` <= :dateLast " );
				$c->params[ ':dateLast' ] = $dateLast;
			}
			
			$idsSkip = $this->getIDsSkip();			
			if( $idsSkip ) {
				$c->addNotInCondition( "`t`.`id`", $idsSkip );
			}
			
			$c->order = " `t`.`date` DESC, `t`.`id` DESC ";
			$c->limit = $this->limit;
			
			return $c;
		}

        private function getLatestJournalCriteria() {
            $c = new CDbCriteria();
            $c->with[ 'i18ns' ] = Array();
            $c->with[ 'currentLanguageI18N' ] = Array();
            $idsSkip = $this->getIDsSkip();
            if( $idsSkip ) {
                $c->addNotInCondition( "`t`.`id`", $idsSkip );
            }
            $c->order = " `t`.`date` DESC, `t`.`id` DESC ";
            $c->limit = $this->limit;
            return $c;
        }

		private function getJournals() {
			if( $this->journals === null ) {
				$c = $this->getCriteria();
                $c->addCondition('`t`.`status` = \'0\'');
				$this->journals = JournalModel::model()->findAll( $c );
			}
			return $this->journals;
		}
		private function renderJournal( $i, $journal ) {
            $datejournalMax = $this->datejournalMax;
            $datejournalMaxKey = $this->datejournalMaxKey;
			$class = $this->getCleanClassName();
			$this->render( "{$class}/journal", Array(
				'i' => $i,
				'journal' => $journal,
                'dateJournalMax' => $datejournalMax,
                'dateJournalMaxKey' => $datejournalMaxKey
			));
			$this->dateLast = $journal->date;
			
			static $holdData;
			if( !$holdData or $holdData != $journal->date ) {
				$holdData = $journal->date;
				$this->idsSkip = Array( $journal->id );
			}
			else{
				$this->idsSkip[] = $journal->id;
			}
		}
		function run() {
			if( !$this->getJournals() ) return;
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'limit' => $this->limit,
				'type' => $this->type,
			));
		}
		function renderList($mode=null) {
            ///---
            $gLJC = $this->getLatestJournalCriteria();
            $Jo_s = JournalModel::model()->findAll( $gLJC );
            foreach($Jo_s as $k=>$jo){
                $date_i[$k] = $jo->date;
            }
            $this->datejournalMax = max( $date_i );
            $maxs_key = array_keys($date_i, max($date_i));
            $this->datejournalMaxKey = $maxs_key[0];
            ///---

			$journals = $this->getJournals();
			foreach( $journals as $i=>$journal ) {
                $this->renderJournal( $i, $journal, $mode );
			}

		}
		function issetMore() {
			$c = $this->getCriteria();
			$exists = JournalModel::model()->exists( $c );
			return $exists;
		}
	}

?>