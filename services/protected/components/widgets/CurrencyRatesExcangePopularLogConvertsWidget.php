<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesExcangePopularLogConvertsWidget extends WidgetBase {
		
		public $type = 'cbr';

		function run() {
			
			//if( !Yii::App()->user->checkAccess( 'currencyRatesControl' ) ) return false;
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'populars' => CurrencyRatesConvertModel::getPopulars($this->type),
				'logs' => CurrencyRatesConvertHistoryModel::getLastLogs($this->type),
			));
		}
	}

?>