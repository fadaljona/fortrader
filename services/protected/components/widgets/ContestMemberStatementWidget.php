<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberStatementWidget extends WidgetBase {
		private $closedDeals;
		private $openDeals;
		public $idMember;
		public $contestMember;
		private function setMember() {
			$this->contestMember = ContestMemberModel::model()->findByPk( $this->idMember );
		}
		private function loadModels() {
			$this->closedDeals = MT5AccountDataModel::model()->find( Array(
				'select' => ' ACCOUNT_ORDERS_HISTORY_TOTAL, ACCOUNT_ORDERS_HISTORY_PENDING_TOTAL, ACCOUNT_ORDERS_HISTORY_TIME_AVERAGE, ACCOUNT_ORDERS_HISTORY_BEST , ACCOUNT_ORDERS_HISTORY_WORSE, ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE, ACCOUNT_ORDERS_HISTORY_LOSS_AVERAGE, ACCOUNT_ORDERS_HISTORY_BEST_PIPS, ACCOUNT_ORDERS_HISTORY_WORSE_PIPS, ACCOUNT_ORDERS_HISTORY_PROFIT_PERCENT, ACCOUNT_ORDERS_HISTORY_PROFIT_FACTOR, ACCOUNT_ORDERS_HISTORY_MAXLOT, ACCOUNT_ORDERS_HISTORY_MINLOT, ACCOUNT_ORDERS_HISTORY_LOT_AVERAGE, ACCOUNT_ORDERS_HISTORY_LOT_SUM, ACCOUNT_ORDERS_HISTORY_DAY, ACCOUNT_ORDERS_HISTORY_PROFIT_TOTAL, ACCOUNT_ORDERS_HISTORY_LOSS_TOTAL, ACCOUNT_ORDERS_HISTORY_GROSS_LOSS, ACCOUNT_ORDERS_HISTORY_GROSS_PROFIT, ACCOUNT_ORDERS_HISTORY_PROFIT ',
				'condition' => "
					`t`.`ACCOUNT_NUMBER` = :accountNumber and `t`.`AccountServerId` = :accountServerId
				",
				'order'=>'`t`.`timestamp` DESC',
				'params' => Array(
					':accountNumber' => $this->contestMember->accountNumber,
					':accountServerId' => $this->contestMember->idServer,
				)
			));
			if( $this->contestMember->server->tradePlatform->name == 'MetaTrader 4' ){
				$this->closedDeals->accountOrdersHistoryTimeAverage = Yii::App()->db->createCommand( "
					SELECT AVG(`OrderCloseTime`-`OrderOpenTime`) 
					FROM `mt4_account_history` WHERE `AccountNumber` = :accountNumber AND `AccountServerId` = :accountServerId
				" )->queryScalar( Array(
					':accountNumber' => $this->contestMember->accountNumber,
					':accountServerId' => $this->contestMember->idServer
				));
			}else{
				$this->closedDeals->accountOrdersHistoryTimeAverage = '';
			}
			$this->openDeals = MT5AccountDataModel::model()->find( Array(
				'select' => ' ACCOUNT_ORDERS_PENDING_TOTAL, ACCOUNT_ORDERS_TOTAL, ACCOUNT_ORDERS_LOT_SUM, ACCOUNT_ORDERS_LOT_AVERAGE ',
				'condition' => "
					`t`.`ACCOUNT_NUMBER` = :accountNumber and `t`.`AccountServerId` = :accountServerId
				",
				'order'=>'`t`.`timestamp` DESC',
				'params' => Array(
					':accountNumber' => $this->contestMember->accountNumber,
					':accountServerId' => $this->contestMember->idServer,
				),
			));
		}
		function run() {
			$this->setMember();
			$this->loadModels();
			$class = $this->getCleanClassName();
			
			if( $this->closedDeals || $this->openDeals ){
				$this->render( "{$class}/view", Array(
					'closedDeals' => $this->closedDeals,
					'openDeals' => $this->openDeals,
					'member' => $this->contestMember,
				));
			}
		}

	}

?>