<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminManageTextContentFilterWidget extends WidgetBase {
		private $modelName;
		private $modelFormName;
		private $textFields = array();
		
		private function setSettings(){
			$settings = ManageTextContentLib::getItemSettings();
			if( !$settings ) throw new Exception( "Can't find itemSettings" );
			if( !isset($settings['model']) || !$settings['model'] || !isset($settings['modelForm']) || !$settings['modelForm'] ) throw new Exception( "Can't find itemSettings" );
			
			$this->modelName = $settings['model'];
			$this->modelFormName = $settings['modelForm'];
		}

		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}
		private function setTextFields(){
			$modelFormName = $this->modelFormName;
			$formModel = new $modelFormName;
			foreach( $formModel->textFields as $field => $type ){
				$this->textFields[] = $field;
			}
		}
		private function getTextFieldsStats(){
			$langs = $this->getLanguages();
			$statsArr = array();
			
			$modelName = $this->modelName;
	
			$addFilterCondition = $modelName::model()->filterCondition;
			
			foreach( $langs as $lang ){
				foreach( $this->textFields as $field ){
					
					if( $lang->id == 0 ){
						$insRelation = 'enLanguageI18NManageTextContent';
					}elseif( $lang->id == 1 ){
						$insRelation = 'ruLanguageI18NManageTextContent';
					}
					
					$yesRes = $modelName::model()->find(array(
						'select' => array(' COUNT(*) as countRows '),
						'with' => array($insRelation),
						'condition' => ' (`'.$insRelation.'`.`'.$field.'` IS NOT NULL AND `'.$insRelation.'`.`'.$field.'` <> "") AND ' . $addFilterCondition
					));
					
					if( $yesRes ){
						$yesCount = $yesRes->countRows;
					}else{
						$yesCount = 0;
					}
					
					$arrKey = $field . $lang->id . 'yes';
					$statsArr[$arrKey] = $yesCount;
					
					$noRes = $modelName::model()->find(array(
						'select' => array(' COUNT(*) as countRows '),
						'with' => array($insRelation),
						'condition' => ' (`'.$insRelation.'`.`'.$field.'` IS NULL OR `'.$insRelation.'`.`'.$field.'` = "") AND ' . $addFilterCondition
					));
					
					if( $noRes ){
						$noCount = $noRes->countRows;
					}else{
						$noCount = 0;
					}
					
					$arrKey = $field . $lang->id . 'no';
					$statsArr[$arrKey] = $noCount;
					
				}
			}
			return $statsArr;
		}

		function run() {
			$this->setSettings();
			$this->setTextFields();
			
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'langs' => $this->getLanguages(),
				'filedStats' => $this->getTextFieldsStats(),
				'textFields' => $this->textFields
			));
		}
	}

?>