<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminEAFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}
		private function getTradePlatforms() {
			$tradePlatforms = Array();
			$models = TradePlatformModel::model()->findAll(Array(
				'order' => " `t`.`name` ",
			));
			
			foreach( $models as $model ){
				$tradePlatforms[ $model->id ] = $model->name;
			}
			
			return $tradePlatforms;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'languages' => $this->getLanguages(),
				'tradePlatforms' => $this->getTradePlatforms(),
			));
		}
	}

?>