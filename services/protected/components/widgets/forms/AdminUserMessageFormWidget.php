<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminUserMessageFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		public $instance;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getInstance() {
			return $this->instance;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'instance' => $this->getInstance(),
			));
		}
	}

?>