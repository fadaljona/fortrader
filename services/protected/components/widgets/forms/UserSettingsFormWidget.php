<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class UserSettingsFormWidget extends WidgetBase {
		public $ajax = false;
		public $model;
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getMailingTypes() {
			$types = MailingTypeModel::model()->findAll( Array( 
				'with' => Array(
					'currentLanguageI18N' => Array(),
					'currentUserMute' => Array(),
				),
				'order' => " `currentLanguageI18N`.`name` ",
			));
			return $types;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'mailingTypes' => $this->getMailingTypes(),
			));
		}
	}

?>