<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminContestMemberFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		public $idContest;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getIDContest() {
			return $this->idContest;
		}
		private function getServers() {
			$servers = Array();
			$idContest = $this->getIDContest();
			
			$models = ServerModel::model()->findAll( Array(
				'condition' => "
					EXISTS (
						SELECT			*
						FROM 			{{server_to_contest}}
						WHERE			`idServer` = `t`.`id`
							AND			`idContest` = :idContest
						LIMIT			1
					)
				",
				'params' => Array(
					':idContest' => $idContest,
				),
				'order' => "`t`.`name`",
			));
			
			foreach( $models as $model ) {
				$servers[ $model->id ] = $model->name;
			}
			
			return $servers;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'idContest' => $this->getIDContest(),
				'servers' => $this->getServers(),
			));
		}
	}

?>