<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestForeignFormWidget extends WidgetBase {
		public $ajax = false;
		public $model;
		public $mode = 'admin';
		public $hidden = false;
		
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getLangs(){
			if( $this->mode == 'front' ){
				return LanguageModel::getByAlias( Yii::App()->language );
			}
			return CommonLib::getLanguages();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'languages' => $this->getLangs(),
				'mode' => $this->mode
			));
		}
	}

?>