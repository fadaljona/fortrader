<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminUnsubscribeCommentFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getTypes() {
			$models = MailingTypeModel::model()->findAll( Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => '`currentLanguageI18N`.`name`'
			));
			$types = CommonLib::toAssoc( $models, 'id', 'name' );
			return $types;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'types' => $this->getTypes(),
			));
		}
	}

?>