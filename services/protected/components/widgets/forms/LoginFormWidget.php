<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.RegisterUserFormModel' );
	Yii::import( 'models.forms.ResetUserPasswordReguestFormModel' );

	final class LoginFormWidget extends WidgetBase {
		public $model;
		private function getLoginFormModel() {
			return $this->model;
		}
		private function getRegisterUserFormModel() {
			$model = new RegisterUserFormModel();
			return $model;
		}
		private function getResetUserPasswordReguestFormModel() {
			$model = new ResetUserPasswordReguestFormModel();
			return $model;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'loginFormModel' => $this->getLoginFormModel(),
				'registerUserFormModel' => $this->getRegisterUserFormModel(),
				'resetUserPasswordReguestFormModel' => $this->getResetUserPasswordReguestFormModel(),
				'ns' => $this->getNS(),
			));
		}
	}

?>