<?
Yii::import( 'components.widgets.base.WidgetBase' );

final class AdminNewsAggregatorSettingsFormWidget extends WidgetBase {
	public $ajax = false;
	public $model;

	private function getAjax() {
		return $this->ajax;
	}
	private function getModel() {
		return $this->model;
	}
	private function getCats(){
		$models = NewsAggregatorCatsModel::model()->findAll(array(
			'condition' => " `t`.`enabled` = 1 ",
			'order' => " `t`.`order` ASC ",
		));
		$outArr = array();
		foreach( $models as $model ){
			$outArr[$model->id] = $model->title;
		}
		return $outArr;
	}

	function run() {
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'ns' => $this->getNS(),
			'model' => $this->getModel(),
			'ajax' => $this->getAjax(),
			'languages' => CommonLib::getLanguages(),
			'categories' => $this->getCats(),
		));
	}
}
