<?php

Yii::import('components.widgets.base.WidgetBase');

final class AdminReplaceItemsWidget extends WidgetBase
{
    public $model;
    public $field;

    public static $models = [
        'QuotesSymbolsModel',
        'CurrencyRatesModel'
    ];

    private function getModelsOptions()
    {
        $outStr = '';
        foreach (self::$models as $model) {
            $outStr .= '<option value="' . $model . '">'. Yii::t('*', str_replace('Model', '', $model)) .'</option>';
        }
        return $outStr;
    }

    private function getObjectOptions()
    {
        $outArr = [];
        foreach (self::$models as $model) {
            $obects = $model::getModelsListForUseInReplace();
            $outArr[$model] = '<option value="">'. Yii::t('*', 'Empty') .'</option>';
            foreach ($obects as $id => $name) {
                $outArr[$model] .= '<option value="' . $id . '">'. $name .'</option>';
            }
        }

        return $outArr;
    }

    private function getWhereToSearchOptions()
    {
        $outStr = '';
        $arName = $this->model->getARClassName();
        foreach ($arName::getPropertiesForReplace() as $property) {
            $outStr .= '<option value="' . $property . '">'. Yii::t('*', $property) .'</option>';
        }
        return $outStr;
    }

    public function run()
    {
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'model' => $this->model,
            'field' => $this->field,
            'langs' => CommonLib::getLanguages(),
            'modelOptions' => $this->getModelsOptions(),
            'objectOptions' => $this->getObjectOptions(),
            'whereToSearchOptions' => $this->getWhereToSearchOptions(),
        ));
    }
}
