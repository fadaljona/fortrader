<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class UserProfileFormWidget extends WidgetBase {
		public $ajax = false;
		public $model;
		public $itCurrentModel;
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getLanguages() {
			$languages = Array();
			$models = LanguageModel::getAll();
			foreach( $models as $model ) {
				$languages[ $model->alias ] = $model->name;
			}
			return $languages;
		}
		private function getCountries() {
			$countries = CountryModel::getAll();
			return $countries;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'languages' => $this->getLanguages(),
				'itCurrentModel' => $this->itCurrentModel,
				'countries' => $this->getCountries(),
			));
		}
	}

?>
