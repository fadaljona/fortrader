<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminAdvertisementFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		public $mode = 'User';
		public $campaigns;
		public $idCampaign;
		public $type = 'Image';
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getMode() {
			return $this->mode;
		}
		private function getCampaigns() {
			return $this->campaigns;
		}
		private function getIDCampaign() {
			return $this->idCampaign;
		}
		private function getType() {
			return $this->type;
		}
		private function getZones() {
			$type = $this->getType();
			switch( $type ) {
				case 'Text':{ $type = 'Text'; break; }
				case 'Image':{ $type = 'Media'; break; }
				case 'Button':{ $type = 'Buttons'; break; }
				case 'Offer':{ $type = 'Offers'; break; }
			}
			
			$zones = Array();
			$models = AdvertisementZoneModel::model()->findAll( Array(
				'condition' => " `t`.`typeAds` IN ( 'All', '{$type}' ) OR `t`.`mixedZone` = 1 ",
				'order' => '`t`.`name`',
			));
			$NSi18n = $this->getNSi18n();
			foreach( $models as $model ) {
				list($width, $height) = $model->getSizesAd();
				$type = Yii::t( $NSi18n, $model->typeBlock );
				if( $model->typeBlock == 'Fixed' ) {
					$type .= ": {$width}x{$height}";
				}
				if( $model->mixedZone == 1 ) {
					$type .= " " . Yii::t( $NSi18n, 'Mixed zone' );
				}
				switch( $this->getType( )) {
					case 'Text':
					case 'Offer':{
						$zones[$model->id] = $model->name . " ({$type})";
						break;
					}
					case 'Image':
					default:{
						$zones[$model->id] = Array(
							'name' => $model->name . " ({$type})",
							'type' => $model->typeBlock,
							'width' => $width,
							'height' => $height,
						);
						break;
					}
				}
			}
			
			return $zones;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'mode' => $this->getMode(),
				'campaigns' => $this->getCampaigns(),
				'idCampaign' => $this->getIDCampaign(),
				'type' => $this->getType(),
				'zones' => $this->getZones(),
			));
		}
	}

?>