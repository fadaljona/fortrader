<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminEAStatementFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}
		function getEA() {
			$models = EAModel::model()->findAll(Array(
				'order' => " `t`.`name` ",
			));
			$EA = CommonLib::toAssoc( $models, 'id', 'name' );
			return $EA;
		}
		function getVersions( $idEA ) {
			$models = EAVersionModel::model()->findAll( Array(
				'select' => "id,version",
				'condition' => " `t`.`idEA` = :idEA ",
				"params" => Array(
					':idEA' => $idEA,
				),
				'order' => '`t`.`version`',
			));
			
			$versions = CommonLib::toAssoc( $models, 'id', 'version' );
			return $versions;
		}
		function getBrokers() {
			$models = BrokerModel::model()->findAll(Array(
				'select' => "id",
				'with' => Array( 'i18ns', 'currentLanguageI18N' ),
				'order' => " `cLI18NBroker`.`officialName` ",
			));
			$brokers = CommonLib::toAssoc( $models, 'id', 'officialName' );
			return $brokers;
		}
		function getServers( $idBroker ) {
			$models = ServerModel::model()->findAll( Array(
				'select' => "id,name",
				'condition' => " `t`.`idBroker` = :idBroker ",
				"params" => Array(
					':idBroker' => $idBroker,
				),
				'order' => '`t`.`name`',
			));
			
			$servers = CommonLib::toAssoc( $models, 'id', 'name' );
			return $servers;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'languages' => $this->getLanguages(),
				'EA' => $this->getEA(),
				'brokers' => $this->getBrokers(),
			));
		}
	}

?>