<?
Yii::import( 'components.widgets.base.WidgetBase' );
Yii::import( 'models.forms.ResetUserPasswordReguestFormModel' );
Yii::import( 'models.forms.LoginFormModel' );

final class InfoContestMemberFormWidget extends WidgetBase{
	public $model;
	public $UMode = "register";
	public $UModel= null;

	private function getModel() {
		return $this->model;
	}

	private function getCountries() {
		$countries = CountryModel::getAll();

		return $countries;
	}

	function run() {
		$class = $this->getCleanClassName();
		if($this->UMode=='guest'){
			$loginFormModel = new LoginFormModel();
			$resetUserPasswordReguestFormModel = new ResetUserPasswordReguestFormModel();
		}else{
			$loginFormModel=$resetUserPasswordReguestFormModel=null;
		}

		$this->render( "{$class}/view", Array(
			'model'     => $this->getModel(),
			'countries' => $this->getCountries(),
			'UModel'    => $this->UModel,
			'UMode'      => $this->UMode,
			'loginFormModel' => $loginFormModel,
			'resetUserPasswordReguestFormModel' => $resetUserPasswordReguestFormModel
		) );
	}
}