<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.FeedbackFormModel' );
	
	final class FeedbackFormWidget extends WidgetBase {
		public $model;
		public $mode = 'feedback';
		private function getModel() {
			if( !$this->model ) {
				$this->model = new FeedbackFormModel();
				$this->model->load();
			}
			return $this->model;
		}
		private function getTypes() {
			if( $this->mode == 'feedback' ){
				$models = FeedbackTypeModel::model()->findAll( Array(
					'with' => Array( "currentLanguageI18N" ),
					'order' => '`t`.`position` IS NULL, `t`.`position`',
					'condition' => ' `t`.`id` <> 5 '
				));
				
				$types = CommonLib::toAssoc( $models, 'id', 'name' );
			}elseif( $this->mode == 'contest' ){
				$types = 5;
			}
			return $types;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'types' => $this->getTypes(),
				'mode' => $this->mode,
			));
		}
	}

?>