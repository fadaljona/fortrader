<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminAdvertisementAddBlockFormModel' );
	
	final class AdminAdvertisementAddBlockFormWidget extends WidgetBase {
		public $model;
		private function getModel() {
			if( !$this->model ) {
				$this->model = new AdminAdvertisementAddBlockFormModel();
			}
			return $this->model;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
			));
		}
	}

?>