<?
		
	final class AdminStrategyFormWidget extends AdminFormWidgetBase {
		function detJSOptions() {
			return CommonLib::mergeAssocs( parent::detJSOptions(), Array(
				'defModel' => Array(
					'userLogin' => Yii::App()->user->getModel()->user_login,
				),
				'saveInDefModel' => Array( 'userLogin' ),
			));
		}
	}

?>