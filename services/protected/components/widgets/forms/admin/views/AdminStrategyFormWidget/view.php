<div class="<?=$cssClass?>">
	<div class="well">
		<?$activeFormWidget = $this->beginActiveForm()?>
				<? // title ?>
			<?$this->renderTitle()?>
				
				<? // pk ?>
			<?=$activeFormWidget->pkFieldRow( $formModel, $formModel->det_pkName())?>
			
				<? // name, desc ?>
			<?$this->renderI18NBlock( null, Array( 'viewPane' => __DIR__.'/_i18nFields.php' ))?>

				<? // userLogin, price ?>
			<div class="row-fluid">
				<div class="iHalf i01">
					<?=$activeFormWidget->textFieldRow( $formModel, 'userLogin', Array( 'class' => 'iInput i03' ))?>
				</div>
				<div class="iHalf i01">
					<?=$activeFormWidget->textFieldRow( $formModel, 'price', Array( 'class' => 'iInput i03' ))?>
				</div>
			</div>
			
				<? // type, idEA ?>
			<div class="row-fluid">
				<div class="iHalf i01">
					<?=$activeFormWidget->dropDownListRow( $formModel, 'type', $this->getDropDownListOptions( 'type' ))?>
				</div>
				<div class="iHalf i01">
					<?=$activeFormWidget->dropDownListRow( $formModel, 'idEA', $this->getDropDownListOptions( 'idEA' ))?>
				</div>
			</div>
			
				<? // timeframes ?>
			<div class="row-fluid">
				<div class="iHalf i01">
					<?$this->renderSetListRow( null, Array( 'attribute' => 'timeframes', 'title' => 'Add timeframe' ))?>
				</div>
				<div class="iHalf i01">
					
				</div>
			</div>
			
				<? // indicators, instruments ?>
			<div class="row-fluid">
				<div class="iHalf i01">
					<?$this->renderSetListRow( null, Array( 'attribute' => 'linksToIndicatorsString', 'title' => 'Add indicator' ))?>
				</div>
				<div class="iHalf i01">
					<?$this->renderSetListRow( null, Array( 'attribute' => 'linksToInstrumentsString', 'title' => 'Add instrument' ))?>
				</div>
			</div>
			
				<? // controlls ?>
			<?$this->renderControlBlock()?>
		<?$this->endActiveForm()?>
	</div>
</div>
<?$this->renderJSInstance()?>