<div class="<?=$cssClass?>">
	<div class="well">
		<?$activeFormWidget = $this->beginActiveForm()?>
				<? // title ?>
			<?$this->renderTitle()?>
			
				<? // pk ?>
			<?=$activeFormWidget->pkFieldRow( $formModel, $formModel->det_pkName())?>
			
				<? // name, desc ?>
			<?$this->renderI18NBlock( null, Array( 'viewPane' => __DIR__.'/_i18nFields.php' ))?>
			
				<? // controlls ?>		
			<?$this->renderControlBlock()?>
		<?$this->endActiveForm()?>
	</div>
</div>
<?$this->renderJSInstance()?>