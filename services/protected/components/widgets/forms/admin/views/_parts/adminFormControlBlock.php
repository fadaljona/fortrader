<div class="iControlBlock i02 row-fluid">
	<div class="row-fluid">
		<div class="iHalf i02">
			<button type="button" class="btn btn-small btn-danger <?=$ins?> wDelete" style="margin: 0 5px 5px 0;">
				<i class="icon-remove"></i>
				<?=$this->translateR( 'Delete' )?>
			</button>
			<button type="button" class="btn btn-small btn-warning <?=$ins?> wReset" style="margin: 0 5px 5px 0;">
				<i class="icon-repeat"></i>
				<?=$this->translateR( 'Reset' )?>
			</button>
			<button type="button" class="btn btn-small btn-inverse <?=$ins?> wClose" style="margin: 0 5px 5px 0;">
				<i class="icon-arrow-up"></i>
				<?=$this->translateR( 'Close' )?>
			</button>
		</div>
		<div class="iHalf i02" style="text-align:right;">
			<button type="button" class="btn btn-small <?=$ins?> btn-info wCopy" style="margin: 0 5px 5px 0;">
				<i class="icon-edit"></i>
				<?=$this->translateR( 'Copy' )?>
			</button>
			<button type="submit" class="btn btn-small <?=$ins?> btn-success wSubmit" style="margin: 0 0px 5px 0;">
				<i class="icon-ok"></i>
				<?=$this->translateR( 'Save' )?>
			</button>
		</div>
	</div>
</div>