<?
	$options = $this->getDropDownListOptions( $attribute );
?>
<div class="<?=$ins?> wSetList">
	<label>
		<?=Yii::t( $NSi18n, $title )?>
	</label>
	<?=CHtml::dropDownList( $attribute, null, $options, Array( 'class' => "wOptions", 'data-keys' => implode( ',', array_keys( $options ))))?>
	<div class="wHiddenOptions" style="display:none;"></div>
	<br />
	<button type="button" class="btn btn-mini btn-success wAdd" style="margin: 0 5px 0 0;">
		<i class="icon-download-alt"></i>
		<?=$this->translateR( 'Add' )?>
	</button>
	<button type="button" class="btn btn-mini btn-warning wClear" style="margin: 0 5px 0 0;">
		<i class="icon-remove"></i>
		<?=$this->translateR( 'Clear' )?>
	</button>

	<?=$this->activeFormWidget->labelEx( $formModel, $attribute, Array( 'style' => "padding-top:10px;" ))?>
	<?=$this->activeFormWidget->hiddenField( $formModel, $attribute, Array( 'class' => 'fSetListKeys' ))?>
	<div class="wList">
		<div class="wEmpty text-center">
			- <?=Yii::t( $NSi18n, 'Empty list' );?> -
		</div>
		<div class="wTpl row-fluid iDiv i69" style="display: none;">
			<span class="wTitle"></span>
			<div class="pull-right">
				<button type="button" class="btn btn-mini btn-danger wDelete" title="<?=$this->translateR( 'Delete' )?>">
					<i class="icon-remove"></i>
				</button>
			</div>
		</div>
	</div>
</div>