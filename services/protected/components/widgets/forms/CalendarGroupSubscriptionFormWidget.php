<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.CalendarGroupSubscriptionFormModel' );
	
	final class CalendarGroupSubscriptionFormWidget extends WidgetBase {
		private $formModel;
		
		private function getFormModel(){
			$model = new CalendarGroupSubscriptionFormModel;
			$model->load( Yii::app()->user->id );
			return $model;
		}
		
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getFormModel(),
			));
		}
	}

?>