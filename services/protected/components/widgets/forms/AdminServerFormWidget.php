<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminServerFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getBrokers() {
			$brokers = Array();
			
			$models = BrokerModel::model()->findAll( Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => '`cLI18NBroker`.`officialName`'
			));
			foreach( $models as $model ) {
				$brokers[ $model->id ] = $model->officialName;
			}
			
			return $brokers;
		}
		private function getPlatforms() {
			$platforms = Array();
			
			$models = TradePlatformModel::model()->findAll( Array(
				'order' => '`t`.`name`'
			));
			foreach( $models as $model ) {
				$platforms[ $model->id ] = $model->name;
			}
			
			return $platforms;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'brokers' => $this->getBrokers(),
				'platforms' => $this->getPlatforms(),
			));
		}
	}

?>