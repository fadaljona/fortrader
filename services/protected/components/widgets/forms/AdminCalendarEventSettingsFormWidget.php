<?
Yii::import( 'components.widgets.base.WidgetBase' );

final class AdminCalendarEventSettingsFormWidget extends WidgetBase {
	public $ajax = false;
	public $model;

	private function getAjax() {
		return $this->ajax;
	}
	private function getModel() {
		return $this->model;
	}

	function run() {
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'ns' => $this->getNS(),
			'model' => $this->getModel(),
			'ajax' => $this->getAjax(),
			'languages' => CommonLib::getLanguages(),
			'brokers' => BrokerModel::getAllBrokersForSelect()
		));
	}
}
