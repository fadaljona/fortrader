<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminInterestRatesValueFormWidget extends WidgetBase {
		public $ajax = false;
		public $model;
		public $hidden = false;
		public $addLabel;
		public $formModelName;
		public $loadItemRoute;
		public $modelName;
		public $deleteItemRoute;
		public $submitItemRoute;
		
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}

		function run() {
			$this->model->idRate = @$_GET['idRate'] ? @$_GET['idRate'] : 0;
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'addLabel' => $this->addLabel,
				'formModelName' => $this->formModelName,
				'loadItemRoute' => $this->loadItemRoute,
				'modelName' => $this->modelName,
				'deleteItemRoute' => $this->deleteItemRoute,
				'submitItemRoute' => $this->submitItemRoute,
				'languages' => CommonLib::getLanguages(),
			));
		}
	}

?>