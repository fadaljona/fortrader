<?php
Yii::import('components.widgets.base.WidgetBase');

final class AdminCryptoCurrenciesSettingsFormWidget extends WidgetBase
{
    public $ajax = false;
    public $model;

    private function getAjax()
    {
        return $this->ajax;
    }
    private function getModel()
    {
        return $this->model;
    }

    private function getQoutes()
    {
        $models = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'condition' => " `cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '' AND `cLI18NQuotesSymbolsModel`.`visible` = 'Visible' AND `t`.`category` <> 0 "
        ));

        $outArr = array();
        foreach ($models as $model) {
            $outArr[$model->id] = $model->name;
        }
        return $outArr;
    }

    private function getFaqs()
    {
        $models = FaqModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'condition' => " `cLI18NFaq`.`title` IS NOT NULL AND `cLI18NFaq`.`title` <> ''"
        ));

        $outArr = array(0 => Yii::t('*', 'not set'));
        foreach ($models as $model) {
            $outArr[$model->id] = $model->title;
        }
        return $outArr;
    }

    private function getVideoChanels()
    {
        $models = VideoChanelModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'condition' => " `cLI18NVideoChanel`.`title` IS NOT NULL AND `cLI18NVideoChanel`.`title` <> ''"
        ));

        $outArr = array(0 => Yii::t('*', 'not set'));
        foreach ($models as $model) {
            $outArr[$model->id] = $model->title;
        }
        return $outArr;
    }

    public function run()
    {
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'ns' => $this->getNS(),
            'model' => $this->getModel(),
            'ajax' => $this->getAjax(),
            'languages' => CommonLib::getLanguages(),
            'quotes' => $this->getQoutes(),
            'faqs' => $this->getFaqs(),
            'videoChanels' => $this->getVideoChanels()
        ));
    }
}
