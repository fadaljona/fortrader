<?
Yii::import('components.widgets.base.WidgetBase');
Yii::import('models.forms.QuoteForecastFormModel');

final class QuoteForecastFormWidget extends WidgetBase{
	public $symbol;

	function run() {
		$this->render( $this->getCleanClassName() . "/view", Array(
			'symbol' => $this->symbol,
			'mood' => QuotesForecastsModel::getMood( $this->symbol->id ),
			'forecatstCount' => QuotesForecastsModel::getCountForUser(),
			'userMood' => QuotesForecastsModel::getMoodForUser( $this->symbol->id ),
		));
	}
	
}
?>