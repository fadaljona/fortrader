<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminAdvertisementCampaignFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		public $mode = 'User';
		public $idBroker;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getMode() {
			return $this->mode;
		}
		private function getCriteriaUsers() {
			return Array(
				'select' => " ID, user_login ",
				'with' => Array(
					'groups' => Array(
						'select' => false,
						'joinType' => 'INNER JOIN',
					),
					'groups.rights' => Array(
						'select' => false,
						'joinType' => 'INNER JOIN',
						'condition' => "`rights`.`name` IN ( 'advertisementControl', 'ownAdvertisementControl' ) ",
					),
				),
				'order' => "`t`.`user_login`",
			);
		}
		private function getUsers() {
			$users = Array();
			
			$c = $this->getCriteriaUsers();
			$models = UserModel::model()->findAll( $c );
			foreach( $models as $model ) {
				$users[ $model->id ] = $model->user_login;
			}
			
			return $users;
		}
		private function getIDDefaultUser() {
			return Yii::App()->user->id;
		}
		private function getDefBegin() {
			return Yii::App()->db->createCommand(" SELECT CURDATE(); ")->queryScalar();
		}
		private function getDefEnd() {
			return Yii::App()->db->createCommand(" SELECT DATE( DATE_ADD( CURDATE(), INTERVAL 1 MONTH )); ")->queryScalar();
		}
		function run() {
			$class = $this->getCleanClassName();
			$mode = $this->getMode();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'mode' => $mode,
				'users' => $mode == "Admin" ? $this->getUsers() : null,
				'idDefaultUser' => $mode == "Admin" ? $this->getIDDefaultUser() : 0,
				'defBegin' => $this->getDefBegin(),
				'defEnd' => $this->getDefEnd(),
			));
		}
	}

?>