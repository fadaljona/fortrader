<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestMemberFormWidget extends WidgetBase {
		public $hidden = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getModel() {
			return $this->model;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'hidden' => $this->getHidden(),
			));
		}
	}

?>