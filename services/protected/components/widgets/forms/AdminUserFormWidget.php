<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminUserFormWidget extends WidgetBase {
		public $type = 'User';
		public $hidden = false;
		public $ajax = false;
		public $model;
		public $groupsSort = 'id';
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getType() {
			return $this->type;
		}
		private function getGroupsSort() {
			return $this->groupsSort;
		}
		private function sortGroups( $a, $b ) {
			$groupsSort = $this->getGroupsSort();
			if( is_string( $groupsSort )) {
				$field = $groupsSort;
				return ($a->$field < $b->$field) ? -1 : 1;
			}
		}
		private function getGroups() {
			$groups = UserGroupModel::model()->findAll();
			usort( $groups, Array( $this, 'sortGroups' ));
			return $groups;
		}
		private function getLanguages() {
			$languages = Array();
			$models = LanguageModel::getAll();
			$languages[""] = "";
			foreach( $models as $model ) {
				$languages[ $model->alias ] = $model->name;
			}
			return $languages;
		}
		private function getBrokers() {
			$brokers = Array();
			
			$models = BrokerModel::model()->findAll( Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => '`cLI18NBroker`.`officialName`'
			));
			$brokers[ "" ] = "";
			foreach( $models as $model ) {
				$brokers[ $model->id ] = $model->officialName;
			}
			
			return $brokers;
		}
		private function getMailingTypes() {
			$types = MailingTypeModel::model()->findAll( Array( 
				'with' => Array(
					'currentLanguageI18N' => Array(),
					'currentUserMute' => Array(),
				),
				'order' => " `currentLanguageI18N`.`name` ",
			));
			return $types;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'type' => $this->getType(),
				'groups' => $this->getGroups(),
				'languages' => $this->getLanguages(),
				'brokers' => $this->getBrokers(),
				'mailingTypes' => $this->getMailingTypes(),
			));
		}
	}

?>