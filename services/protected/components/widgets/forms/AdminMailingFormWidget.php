<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminMailingFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		public $userGroupsSort = 'id';
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getUserGroupsSort() {
			return $this->userGroupsSort;
		}
		private function sortUserGroups( $a, $b ) {
			$groupsSort = $this->getUserGroupsSort();
			if( is_string( $groupsSort )) {
				$field = $groupsSort;
				return ($a->$field < $b->$field) ? -1 : 1;
			}
		}
		private function getUserGroups() {
			$groups = Array();
			$models = UserGroupModel::model()->findAll();
			usort( $models, Array( $this, 'sortUserGroups' ));
			
			$NSi18n = $this->getNSi18n();
			$groups[0] = Yii::t( $NSi18n, "All" );
			foreach( $models as $model ) {
				$groups[ $model->id ] = Yii::t( $NSi18n, $model->name );
			}
			
			return $groups;
		}
		private function getTypes() {
			$types = Array();
			
			$types = MailingTypeModel::model()->findAll( Array( 
				'with' => Array(
					'currentLanguageI18N' => Array(),
				),
				'order' => " `currentLanguageI18N`.`name` ",
			));
			
			$types = CommonLib::toAssoc( $types, 'id', 'name' );
			$types = CommonLib::mergeAssocs( Array( '' => '' ), $types );
			
			return $types;
		}
		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'userGroups' => $this->getUserGroups(),
				'languages' => $this->getLanguages(),
				'types' => $this->getTypes(),
			));
		}
	}

?>