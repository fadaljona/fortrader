<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class UnsubscribeCommentFormWidget extends WidgetBase {
		public $model;
		public $mailingType;
		private function getType() {
			return $this->mailingType;
		}
		private function getModel() {
			return $this->model;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'type' => $this->getType(),
				'model' => $this->getModel(),
			));
		}
	}

?>