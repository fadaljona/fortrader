<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminRssFeedsFeedFormWidget extends WidgetBase {
		public $ajax = false;
		public $model;
		public $hidden = false;
		public $addLabel;
		public $formModelName;
		public $loadItemRoute;
		public $modelName;
		public $deleteItemRoute;
		public $submitItemRoute;
		
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getLangs(){
			$langs = CommonLib::getLanguages();
			$outArr = array();
			foreach( $langs as $lang ){
				$outArr[$lang->id] = $lang->name;
			}
			return $outArr;
		}
		private function getGroups(){
			$models = RssFeedsGroupModel::model()->findAll(array(
				'condition' => " `t`.`enabled` = 1 ",
				'order' => " `t`.`order` DESC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->id] = $model->title;
			}
			return $outArr;
		}
		private function getCats(){
			$models = WPTermTaxonomyModel::model()->findAll(array(
				'with' => array( 'term' ),
				'condition' => " `t`.`taxonomy` = 'category' AND `term`.`name` IS NOT NULL ",
				'order' => " `t`.`parent` DESC ",
			));
			$outArr = array(0 => Yii::t('*', 'All categories'));
			foreach( $models as $model ){
				$outArr[$model->term_id] = $model->term->name;
			}
			return $outArr;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'languages' => $this->getLangs(),
				'addLabel' => $this->addLabel,
				'formModelName' => $this->formModelName,
				'loadItemRoute' => $this->loadItemRoute,
				'modelName' => $this->modelName,
				'deleteItemRoute' => $this->deleteItemRoute,
				'submitItemRoute' => $this->submitItemRoute,
				'groups' => $this->getGroups(),
				'parents' => RssFeedsFeedModel::getAllForParent(),
				'cats' => $this->getCats()
			));
		}
	}

?>