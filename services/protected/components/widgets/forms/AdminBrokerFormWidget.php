<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminBrokerFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getCountries() {
			$countries = Array();
			$models = CountryModel::getAll();
			
			foreach( $models as $id => $model ) {
				$countries[ $id ] = $model->name;
			}
			
			return $countries;
		}
		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}
		private function getRegulators() {
			$regulators = Array();
			$models = BrokerRegulatorModel::model()->findAll(Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => " `cLI18NBrokerRegulator`.`name` ",
			));
			
			foreach( $models as $model ){
				$regulators[ $model->id ] = $model->name;
			}
			
			return $regulators;
		}
		private function getTradePlatforms() {
			$tradePlatforms = Array();
			$models = TradePlatformModel::model()->findAll(Array(
				'order' => " `t`.`name` ",
			));
			
			foreach( $models as $model ){
				$tradePlatforms[ $model->id ] = $model->name;
			}
			
			return $tradePlatforms;
		}
		private function getIOs() {
			$IOs = Array();
			$models = BrokerIOModel::model()->findAll(Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => " `cLI18NBrokerIO`.`name` ",
			));
			
			foreach( $models as $model ){
				$IOs[ $model->id ] = $model->name;
			}
			
			return $IOs;
		}
		private function getInstruments() {
			$instruments = Array();
			$models = BrokerInstrumentModel::model()->findAll(Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => " `cLI18NBrokerInstrument`.`name` ",
			));
			
			foreach( $models as $model ){
				$instruments[ $model->id ] = $model->name;
			}
			
			return $instruments;
		}
		private function getBinaryTypes() {
			$types = Array();
			$models = BrokerBinaryOptionModel::model()->findAll(Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => " `cLI18NBrokerBinaryOptionModel`.`name` ",
			));
			
			foreach( $models as $model ){
				$types[ $model->id ] = $model->name;
			}
			
			return $types;
		}
		private function getBinaryAssets() {
			$types = Array();
			$models = BrokerBinaryAssetsModel::model()->findAll(Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => " `cLI18NBrokerBinaryAssetsModel`.`name` ",
			));
			
			foreach( $models as $model ){
				$types[ $model->id ] = $model->name;
			}
			
			return $types;
		}
		private function getIdSupportUsers() {
			$group = UserGroupModel::model()->findByAttributes( Array( 'name' => 'Broker Support' ));
			if( $group ) {
				$users = UserModel::model()->findAll(Array(
					'select' => " ID, user_login ",
					'condition' => "
						EXISTS (
							SELECT 	1
							FROM	`{{user_to_user_group}}`
							WHERE	`{{user_to_user_group}}`.`idUser` = `t`.`ID`
								AND	`{{user_to_user_group}}`.`idUserGroup` = :idGroup
							LIMIT	1
						)
					",
					'order' => " `t`.`user_login` ",
					'params' => Array(
						':idGroup' => $group->id,
					),
				));
				$users = CommonLib::toAssoc( $users, 'ID', 'user_login' );
				$users = CommonLib::mergeAssocs( Array( '' => '' ), $users );
				return $users;
			}
			return Array();
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'countries' => $this->getCountries(),
				'languages' => $this->getLanguages(),
				'regulators' => $this->getRegulators(),
				'tradePlatforms' => $this->getTradePlatforms(),
				'IOs' => $this->getIOs(),
				'instruments' => $this->getInstruments(),
				'idSupportUsers' => $this->getIdSupportUsers(),
				'binaryTypes' => $this->getBinaryTypes(),
				'binaryassets' => $this->getBinaryAssets(),
			));
		}
	}

?>