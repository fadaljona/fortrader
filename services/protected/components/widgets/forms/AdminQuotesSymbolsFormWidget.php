<?
Yii::import( 'components.widgets.base.WidgetBase' );

final class AdminQuotesSymbolsFormWidget extends WidgetBase {
    public $hidden = true;
    public $ajax = false;
    public $model;
    public $categories;

    protected function getHidden() {
        return $this->hidden;
    }
    private function getAjax() {
        return $this->ajax;
    }
    private function getModel() {
        return $this->model;
    }

    function run() {
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'ns' => $this->getNS(),
            'model' => $this->getModel(),
            'ajax' => $this->getAjax(),
            'hidden' => $this->getHidden(),
            'default' => array('1' => Yii::t("quotesWidget", 'Да'), '2' => Yii::t("quotesWidget", 'Нет')),
            'panel'=>array('1' => Yii::t("quotesWidget", 'Да'), '2' => Yii::t("quotesWidget", 'Нет')),
            'visible'=>array('1' => Yii::t("quotesWidget", 'Показывать'), '2' => Yii::t("quotesWidget", 'Скрытый')),
            'categories'=>$this->categories,
            'sourceType' => array(
                'own' => Yii::t("quotesWidget", 'own'),
                'yahoo' => Yii::t("quotesWidget", 'yahoo'),
                'bitz' => Yii::t("quotesWidget", 'bit-z.com'),
                'bitfinex' => Yii::t("quotesWidget", 'bitfinex'),
                'okex' => Yii::t("quotesWidget", 'okex'),
                'binance' => Yii::t("quotesWidget", 'binance')
            ),
            'bitzSymbols' => array(
                'btc_usdt', 'eth_usdt', 'bch_btc', 'eth_btc', 'ltc_btc', 'etc_btc', 'zec_btc', 'mzc_btc', 'gxs_btc', 'fct_btc', 'btx_btc', 'lsk_btc', 'dash_btc', 'omg_btc', 'nuls_btc', 'hsr_btc', 'pay_btc', 'eos_btc', 'game_btc', 'xas_btc', 'ppc_btc', 'voise_btc', 'ybct_btc', 'blk_btc', 'xpm_btc', 'qtum_btc', 'viu_btc', 'sss_btc', 'bcd_btc', 'doge_btc', 'dgb_btc', 'btg_btc', 'ark_btc', 'part_btc', 'otn_btc', 'leo_btc', 'gxs_eth', 'zsc_eth', 'doge_eth'
            ),
            'okexSymbols' => array(
                'ltc_btc', 'eth_btc', 'etc_btc', 'bch_btc', 'btc_usdt', 'eth_usdt', 'ltc_usdt', 'etc_usdt', 'bch_usdt', 'etc_eth', 'bt1_btc', 'bt2_btc', 'btg_btc', 'qtum_btc', 'hsr_btc', 'neo_btc', 'gas_btc', 'qtum_usdt', 'hsr_usdt', 'neo_usdt', 'gas_usdt'
            ),
            'languages' => CommonLib::getLanguages(),
        ));
    }
}
