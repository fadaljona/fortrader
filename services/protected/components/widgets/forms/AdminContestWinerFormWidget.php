<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminContestWinerFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getContests() {
			$contests = Array();
			
			$models = ContestModel::model()->findAll( Array(
				'with' => 'currentLanguageI18N',
				'order' => "`t`.`begin` DESC",
			));
			
			foreach( $models as $model ) {
				$contests[ $model->id ] = $model->currentLanguageI18N->name;
			}
			
			return $contests;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'contests' => $this->getContests(),
			));
		}
	}

?>