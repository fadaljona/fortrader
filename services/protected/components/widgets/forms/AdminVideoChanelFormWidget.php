<?php
Yii::import('components.widgets.base.WidgetBase');

final class AdminVideoChanelFormWidget extends WidgetBase
{
    public $ajax = false;
    public $model;
    public $hidden = false;
    public $addLabel;
    public $formModelName;
    public $loadItemRoute;
    public $modelName;
    public $deleteItemRoute;
    public $submitItemRoute;
        
    private function getAjax() {
        return $this->ajax;
    }
    private function getModel() {
        return $this->model;
    }
    private function getLangs()
    {
        return CommonLib::getLanguages();
    }

    public function run()
    {
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'ns' => $this->getNS(),
            'model' => $this->getModel(),
            'ajax' => $this->getAjax(),
            'languages' => $this->getLangs(),
            'addLabel' => $this->addLabel,
            'formModelName' => $this->formModelName,
            'loadItemRoute' => $this->loadItemRoute,
            'modelName' => $this->modelName,
            'deleteItemRoute' => $this->deleteItemRoute,
            'submitItemRoute' => $this->submitItemRoute,
        ));
    }
}
