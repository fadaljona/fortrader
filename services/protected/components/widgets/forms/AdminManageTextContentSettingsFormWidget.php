<?
Yii::import( 'components.widgets.base.WidgetBase' );

final class AdminManageTextContentSettingsFormWidget extends WidgetBase {
	public $ajax = false;
	public $modelFormName;
	public $modelName;

	private function getAjax() {
		return $this->ajax;
	}


	function run() {
		
		
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'ns' => $this->getNS(),
			'modelFormName' => $this->modelFormName,
			'modelName' => $this->modelName,
			'ajax' => $this->getAjax(),
			'languages' => CommonLib::getLanguages(),
		));
	}
}
