<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class RulesContestFormWidget extends WidgetBase {
		public $contest;
		private function getContest() {
			return $this->contest;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'contest' => $this->getContest(),
			));
		}
	}

?>