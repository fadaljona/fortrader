<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminNewsAggregatorNewsFormWidget extends WidgetBase {
		public $ajax = false;
		public $model;
		public $hidden = false;
		public $addLabel;
		public $formModelName;
		public $loadItemRoute;
		public $modelName;
		public $deleteItemRoute;
		public $submitItemRoute;
		
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getLangs(){
			$langs = CommonLib::getLanguages();
			$outArr = array();
			foreach( $langs as $lang ){
				$outArr[$lang->id] = $lang->name;
			}
			return $outArr;
		}

		private function getRss(){
			$models = NewsAggregatorRssModel::model()->findAll(array(
				'condition' => " `t`.`enabled` = 1 ",
				'order' => " `t`.`idSource` DESC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->id] = $model->link;
			}
			return $outArr;
		}
		private function getCats(){
			$models = NewsAggregatorCatsModel::model()->findAll(array(
				'order' => " `t`.`order` DESC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->id] = $model->title;
			}
			return $outArr;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'languages' => $this->getLangs(),
				'addLabel' => $this->addLabel,
				'rsses' => $this->getRss(),
				'cats' => $this->getCats(),
				'formModelName' => $this->formModelName,
				'loadItemRoute' => $this->loadItemRoute,
				'modelName' => $this->modelName,
				'deleteItemRoute' => $this->deleteItemRoute,
				'submitItemRoute' => $this->submitItemRoute,
			));
		}
	}

?>