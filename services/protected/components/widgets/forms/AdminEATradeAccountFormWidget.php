<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminEATradeAccountFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getEA() {
			$EA = Array();
			$models = EAModel::model()->findAll( Array(
				'order' => '`t`.`name`',
			));
			$EA = CommonLib::toAssoc( $models, 'id', 'name' );
			return $EA;
		}
		private function getTradeAccounts() {
			$taradeAccounts = Array();
			$models = TradeAccountModel::model()->findAll( Array(
				'condition' => " FIND_IN_SET( 'EA', `t`.`groups` ) ",
				'order' => '`t`.`name`',
			));
			$taradeAccounts = CommonLib::toAssoc( $models, 'id', 'name' );
			return $taradeAccounts;
		}
		private function getEAMonitoringServers() {
			$EAMonitoringServers = Array();
			$models = EAMonitoringServerModel::model()->findAll( Array(
				'order' => '`t`.`name`',
			));
			$EAMonitoringServers = CommonLib::toAssoc( $models, 'id', 'name' );
			return $EAMonitoringServers;
		}
		function getStatements( $idVersion ) {
			$models = EAStatementModel::model()->findAll( Array(
				'select' => "id",
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'condition' => " `t`.`idVersion` = :idVersion ",
				"params" => Array(
					':idVersion' => $idVersion,
				),
				'order' => '`cLI18NEAStatement`.`title`',
			));
			
			$statements = CommonLib::toAssoc( $models, 'id', 'title' );
			return $statements;
		}
		private function getBrokers() {
			$brokers = Array();
			$models = BrokerModel::model()->findAll( Array(
				'with' => Array( 'currentLanguageI18N', 'i18ns' ),
				'order' => '`cLI18NBroker`.`officialName`',
			));
			$brokers = CommonLib::toAssoc( $models, 'id', 'officialName' );
			$brokers = CommonLib::mergeAssocs( Array( '' => '' ), $brokers );
			return $brokers;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'EA' => $this->getEA(),
				'taradeAccounts' => $this->getTradeAccounts(),
				'EAMonitoringServers' => $this->getEAMonitoringServers(),
				'brokers' => $this->getBrokers(),
			));
		}
	}

?>