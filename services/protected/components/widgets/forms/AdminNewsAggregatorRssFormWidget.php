<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminNewsAggregatorRssFormWidget extends WidgetBase {
		public $ajax = false;
		public $model;
		public $hidden = false;
		public $addLabel;
		public $formModelName;
		public $loadItemRoute;
		public $modelName;
		public $deleteItemRoute;
		public $submitItemRoute;
		
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getLangs(){
			$langs = CommonLib::getLanguages();
			$outArr = array();
			foreach( $langs as $lang ){
				$outArr[$lang->id] = $lang->name;
			}
			return $outArr;
		}
		private function getSources(){
			$models = NewsAggregatorSourceModel::model()->findAll(array(
				'condition' => " `t`.`enabled` = 1 ",
				'order' => " `t`.`order` ASC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->id] = $model->title;
			}
			return $outArr;
		}
		private function getCats(){
			$models = NewsAggregatorCatsModel::model()->findAll(array(
				'condition' => " `t`.`enabled` = 1 ",
				'order' => " `t`.`order` ASC ",
			));
			$outArr = array( 0 => Yii::t('*', 'Auto selection') );
			foreach( $models as $model ){
				$outArr[$model->id] = $model->title;
			}
			return $outArr;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'languages' => $this->getLangs(),
				'addLabel' => $this->addLabel,
				'sources' => $this->getSources(),
				'formModelName' => $this->formModelName,
				'loadItemRoute' => $this->loadItemRoute,
				'modelName' => $this->modelName,
				'deleteItemRoute' => $this->deleteItemRoute,
				'submitItemRoute' => $this->submitItemRoute,
				'categories' => $this->getCats(),
			));
		}
	}

?>