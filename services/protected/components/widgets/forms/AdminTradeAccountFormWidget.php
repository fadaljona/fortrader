<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminTradeAccountFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getServers() {
			$servers = Array();
			
			$models = ServerModel::model()->findAll( Array(
				'order' => '`t`.`name`'
			));
			foreach( $models as $model ) {
				$servers[ $model->id ] = $model->name;
			}
			
			return $servers;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'servers' => $this->getServers(),
			));
		}
	}

?>