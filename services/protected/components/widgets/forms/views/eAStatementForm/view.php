<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wEAStatementFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="wEAStatementFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal', 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id' )?>
		<?=$form->hiddenField( $model, 'idEA' )?>
		<?=$form->dropDownListRow( $model, 'idVersion', $versions )?>
		<?/*
		<div class="control-group">
			<label class="control-label"><?=Yii::t( '*', 'Title' )?>:</label>
			<div class="controls">
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab1<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab1<?=$title?>">
							<?=$form->textField( $model, "titles[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wTitle" ))?>
						</div>
					<?}?>
				</div>
								
			</div>
		</div>
		*/?>
		
		<div class="control-group">
			<label class="control-label"><?=Yii::t( '*', 'Description' )?>:</label>
			<div class="controls">
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab2<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab2<?=$title?>">
							<?=$form->textArea( $model, "desces[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wDesc", 'rows' => 5 ))?>
						</div>
					<?}?>
				</div>
								
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label"></label>
			<div class="controls">
				<label for="<?=$model->resolveID( 'optimization' )?>">
					<?=$form->checkbox( $model, "optimization", Array( 'class' => "{$ins} wOptimizationSwitch" ))?>
					<span class="lbl" style="padding:0px;">
						<?=Yii::t( '*', 'This statement made with optimization parameters' )?>
					</span>
				</label>
			</div>
		</div>
		
		<div class="control-group <?=$ins?> wOptimizationBlock" <?if( !$model->optimization ){?> style="display:none;" <?}?> >
			<label class="control-label"><?=Yii::t( '*', 'Optimization period' )?>:</label>
			<div class="controls">
				<div style="display:inline-block; margin-right:10px;">
					<?=Yii::t( '*', 'Start date' )?>:<br/>
					<?=$form->dateField( $model, "optimizationStartDate" )?>
				</div>
				<div style="display:inline-block">
					<?=Yii::t( '*', 'End date' )?>:<br/>
					<?=$form->dateField( $model, "optimizationEndDate" )?>
				</div>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label"><?=Yii::t( '*', 'Graph statement' )?>:</label>
			<div class="controls">
				<?=$form->fileField( $model, 'graph' )?>
				<?if( $model->graph ){?>
					<br><a href="<?=$model->graph?>" target="_blank"><?=basename($model->graph)?></a>
				<?}?>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label"><?=Yii::t( '*', 'File statement' )?>:</label>
			<div class="controls">
				<?=$form->fileField( $model, 'statement' )?>
				<?if( $model->statement ){?>
					<br><a href="<?=$model->statement?>" target="_blank"><?=basename($model->statement)?></a>
				<?}?>
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label"><?=Yii::t( '*', 'File set' )?>:</label>
			<div class="controls">
				<?=$form->fileField( $model, 'set' )?>
				<?if( $model->set ){?>
					<br><a href="<?=$model->set?>" target="_blank"><?=basename($model->set)?></a>
				<?}?>
			</div>
		</div>
		
		<?
			$this->widget( 'bootstrap.widgets.TbButton', Array( 
				'buttonType' => 'submit', 
				'type' => 'success',
				'label' => Yii::t( $NSi18n, 'Done!' ),
				'htmlOptions' => Array(
					'class' => "pull-right {$ins} wSubmit",
				),
			));
		?>
		<iframe name="iframeForStatement" id="iframeForStatement" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
				
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wEAStatementFormWidget = wEAStatementFormWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wEAStatementFormWidget',
			ls: ls,
		});
	}( window.jQuery );
</script>