<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminJournalWebmasterFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = 'Webmaster';
	
	$jsls = Array(
		'lNew' => 'New journal',
		'lEdit' => 'Editor journal',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	/*foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$years = range( 1971, date( "Y" ) );
	$years = array_combine( $years, $years );*/
?>
<div class="well pull-left iFormWidget i01 wAdminJournalWebmasterFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'idWebmaster', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>

		
		<div class="tab-content" style="position:static">
			<div class="tab-pane active" id="tabJournal">
                <label><?=Yii::t( $NSi18n, "Url" )?></label>
				<?=$form->textField( $model, 'url', Array( 'class' => "{$ins} wUrl" ))?>
                <label><?=Yii::t( $NSi18n, "Description" )?></label>
				<?=$form->textArea( $model, 'description', Array( 'class' => "{$ins} wDescription" ))?>
                
			</div>
		</div>
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				/*$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));*/
			?>
		</div>
		<div class="clear"></div>
        <iframe name="iframeForJournal" id="iframeForJournal" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var idLanguage = <?=LanguageModel::getCurrentLanguageID()?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminJournalWebmasterFormWidget = wAdminJournalWebmasterFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminJournalWebmasterFormWidget',
			ajax: ajax,
			hidden: hidden,
			idLanguage: idLanguage,
			ls: ls,
		});
	}( window.jQuery );
</script>