<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminAdvertisementFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = 'New ad';
	
	switch( $type ) {
		case 'Text':
		default:{
			$lNew = 'New ad';
			$lEdit = 'Editor ad';
			break;
		}
		case 'Image':{
			$lNew = 'New image';
			$lEdit = 'Editor image';
			break;
		}
		case 'Button':{
			$lNew = 'New button';
			$lEdit = 'Editor button';
			break;
		}
		case 'Offer':{
			$lNew = 'New offer';
			$lEdit = 'Editor offer';
			break;
		}
	}
	
	$jsls = Array(
		'lNew' => $lNew,
		'lEdit' => $lEdit,
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminAdvertisementFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		<?=$form->dropDownListRow( $model, 'idCampaign', $campaigns, Array( 'class' => "{$ins} wIDCampaign" ))?>
		<?=$form->textFieldRow( $model, 'header', Array( 'class' => "{$ins} wHeader" ))?>
		
		
		<?if( $type == 'Image' || $type == 'Button' ){?>
			<label for="<?=$model->resolveID( 'htmlBaner' )?>">
				<?=$form->checkBox( $model, 'htmlBaner', Array( 'class' => "{$ins} wHtmlBaner" ))?>
				<span class="lbl">
					<?=$model->getAttributeLabel( 'htmlBaner' )?>
				</span>
			</label>
			
			<div class="<?=$ins?> wuploadIframeFileBlock">
				<label for="<?=$model->resolveID( 'needUploadHtmlFile' )?>" class="labelneedUploadHtmlFile">
					<?=$form->checkBox( $model, 'needUploadHtmlFile', Array( 'class' => "{$ins} wneedUploadHtmlFile" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'needUploadHtmlFile' )?>
					</span>
				</label>
				<div class="<?=$ins?> wuploadIframeFileBlockSettings">
					<?=$form->textFieldRow( $model, 'iframeWidth', Array( 'class' => "{$ins} wiframeWidth" ))?>
					<?=$form->textFieldRow( $model, 'iframeHeight', Array( 'class' => "{$ins} wiframeHeight" ))?>
					<div class="<?=$ins?> uploadedFileSrc"></div>
					<?=$form->fileFieldRow( $model, 'iframeFile', Array( 'class' => "{$ins} wiframeFile" ))?>
				</div>
				<iframe name="iframeForHtmlFile" id="iframeForHtmlFile" style="display:none"></iframe>
			</div>
			
			<div class="<?=$ins?> wTextBlock">
				<?=$form->textAreaRow( $model, 'text', Array( 'class' => "input-xxlarge {$ins} wText", 'rows' => 5 ))?>
			</div>
		<?}?>
		
		
		<?if( in_array( $type, Array( 'Text', 'Offer' ))){?>
			<?=$form->textAreaRow( $model, 'text', Array( 'class' => "input-xxlarge {$ins} wText", 'rows' => 5 ))?>
		<?}?>

		
		<?if( in_array( $type, Array( 'Image', 'Button', 'Offer' ))){?>
			<div class="<?=$ins?> wImageBlock">
				<?=$form->fileFieldRow( $model, 'image', Array( 'class' => "{$ins} wImage" ))?>
				<?=$form->hiddenField( $model, 'holdedImage', Array( 'class' => "{$ins} wHoldedImage" ))?>
				<div class="<?=$ins?> wImageLoading" style="display:none">
					<?=Yii::t( $NSi18n, 'Loading...' )?>
				</div>
				<div class="<?=$ins?> wImageHolder" style="display:none;margin-top:10px;">
				
					<object type='application/x-shockwave-flash' data='' width='200' height='200'>
						<param name='movie' value=''>
					</object>
				
					<img style="max-width:200px;">
					<label><?=Yii::t( $NSi18n, 'Sizes' )?></label>
					<?=$form->textField( $model, 'width', Array( 'class' => "{$ins} wWidth input-very-small", 'placeholder' => Yii::t( $NSi18n, 'Width' )))?>
					&nbsp;x&nbsp;<?=$form->textField( $model, 'height', Array( 'class' => "{$ins} wHeight input-very-small", 'placeholder' => Yii::t( $NSi18n, 'Height' )))?>
					&nbsp;<a href="#" class="btn btn-small <?=$ins?> wPreview" style="margin-bottom:10px;">Ok</a>
				</div>
				<iframe name="iframeForImage" id="iframeForImage" style="display:none"></iframe>
			</div>
		<?}?>
		
		<label><?=Yii::t( $NSi18n, 'Min and Max screen width for popup' )?></label>
		<?=$form->textField( $model, 'minScreenWidth', Array( 'class' => "{$ins} wMinScreenWidth input-very-small" ))?>
		-- 
		<?=$form->textField( $model, 'maxScreenWidth', Array( 'class' => "{$ins} wMaxScreenWidth input-very-small" ))?>

		
		<div class="<?=$ins?> wUrlBlock">
			<?=$form->textFieldRow( $model, 'url', Array( 'class' => "{$ins} wUrl" ))?>
		</div>
		<?if( in_array( $type, Array( 'Offer' ))){?>
			<?=$form->textFieldRow( $model, 'countLikes', Array( 'class' => "{$ins} wLikes" ))?>
		<?}?>
		<label for="<?=$model->resolveID( 'showEverywhere' )?>">
			<?=$form->checkBox( $model, 'showEverywhere', Array( 'class' => "{$ins} wShowEverywhere" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'showEverywhere' )?>
			</span>
		</label>

        <label for="<?=$model->resolveID( 'isDirectAdLink' )?>">
			<?=$form->checkBox( $model, 'isDirectAdLink', Array( 'class' => "{$ins} wIsDirectAdLink" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'isDirectAdLink' )?>
			</span>
		</label>

		<label for="server"><?=Yii::t( $NSi18n, "Add zone" )?></label>
		<?=CHtml::dropDownList( "zones", null, in_array( $type, Array( 'Text', 'Offer' )) ? $zones : Array(), Array( 'class' => "{$ins} wZone" ))?>
		
		<div class="clear"></div>
		<?
			$this->widget( 'bootstrap.widgets.TbButton', Array( 
				'label' => Yii::t( $NSi18n, 'Add' ),
				'size' => 'mini',
				'htmlOptions' => Array(
					'class' => "{$ins} wAddZone",
				),
			));
		?>
		<div class="clear"></div>
		<?=$form->labelEx( $model, 'zones', Array( 'style' => "padding-top:10px;" ))?>
		<?=$form->hiddenField( $model, 'zones', Array( 'class' => "{$ins} wZones" ))?>
		<table class="table i01 table-bordered <?=$ins?> wTabZones">
			<tbody>
				<tr class="wTpl" style="display:none;">
					<td class="wTDName"></td>
					<td class="iActions">
						<a href="#" class="icon-trash wDelete"></a>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="clear"></div>
		
		<label for="<?=$model->resolveID( 'dontShow' )?>">
			<?=$form->checkBox( $model, 'dontShow', Array( 'class' => "{$ins} wDontShow" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'dontShow' )?>
			</span>
		</label>
		
		<?if( $mode == "Admin" ){?>
			<label for="<?=$model->resolveID( 'clearStats' )?>">
				<?=$form->checkBox( $model, 'clearStats', Array( 'class' => "{$ins} wClearStats" ))?>
				<span class="lbl">
					<?=$model->getAttributeLabel( 'clearStats' )?>
				</span>
			</label>
		<?}?>
		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var idCampaign = <?=$idCampaign?>;
		var maxHeaderLen = <?=AdvertisementModel::MAXHeaderLen?>;
		var maxTextLen = <?=AdvertisementModel::MAXTextLen?>;
		var type = "<?=$type?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		<?if( $type != "Text" ){?>
			var zones = <?=json_encode( $zones )?>;
		<?}?>
		
		ns.wAdminAdvertisementFormWidget = wAdminAdvertisementFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminAdvertisementFormWidget',
			ajax: ajax,
			hidden: hidden,
			idCampaign: idCampaign,
			maxHeaderLen: maxHeaderLen,
			maxTextLen: maxTextLen,
			ls: ls,
			type: type,
			<?if( $type != "Text" ){?>
				zones: zones,
			<?}?>
			ajaxSubmitWithHtmlURL: '<?=Yii::app()->createUrl('admin/advertisement/ajaxSubmitWithHtml')?>',
		});
	}( window.jQuery );
</script>