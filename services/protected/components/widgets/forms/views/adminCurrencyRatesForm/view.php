<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminCurrencyRatesFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New object' : 'Editor object';
	
	$jsls = Array(
		'lNew' => 'New object',
		'lEdit' => 'Editor object',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	

?>
<div class="well pull-left iFormWidget i01 wAdminCurrencyRatesFormWidget wAdminQuotesSymbolsFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<?=$form->textFieldRow($model,'code', Array( 'class' => "{$ins} wCode" )) ?>
		<?=$form->textFieldRow($model,'currencyOrder', Array( 'class' => "{$ins} wCurrencyOrder" )) ?>
		<?=$form->textFieldRow($model,'symbol', Array( 'class' => "{$ins} wSymbol" )) ?>

		
		<label for="<?=$model->resolveID( 'popular' )?>">
			<?=$form->checkBox( $model, 'popular', Array( 'class' => "{$ins} wPopular" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'popular' )?>
			</span>
		</label>
		<label for="<?=$model->resolveID( 'topInSelect' )?>">
			<?=$form->checkBox( $model, 'topInSelect', Array( 'class' => "{$ins} wTopInSelect" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'topInSelect' )?>
			</span>
		</label>
		
		<label for="<?=$model->resolveID( 'toInformer' )?>">
			<?=$form->checkBox( $model, 'toInformer', Array( 'class' => "{$ins} wtoInformer" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'toInformer' )?>
			</span>
		</label>
		
		<?=$form->dropDownListRow( $model, 'idCountry', $countries, Array( 'class' => "{$ins} wIDCountry" ))?>
		<br />
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' class="active"' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<li<?=$class?>>
					<a href="#tab<?=$title?>" data-toggle="tab">
						<?=$title?>
					</a>
				</li>
			<?}?>
		</ul>
		
		<div class="tab-content">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' active' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
					<?=$form->textFieldRow( $model, "names[{$language->id}]", Array( 'class' => "{$ins} wName w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "namePlurals[{$language->id}]", Array( 'class' => "{$ins} wnamePlurals wFullWidth w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "toName[{$language->id}]", Array( 'class' => "{$ins} wtoName wFullWidth w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "informerName[{$language->id}]", Array( 'class' => "{$ins} winformerName wFullWidth w{$language->id}" ))?>
					
					<?=$form->textFieldRow( $model, "titles[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "metaTitles[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wMetaTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbMetaTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbMetaTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "metaDescs[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wMetaDesc w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbMetaDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbMetaDesc w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "metaKeyss[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wMetaKeys w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbMetaKeys[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbMetaKeys w{$language->id}" ))?>
					
					<?=$form->textFieldRow( $model, "shortDescs[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wShortDesc w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbShortDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbShortDesc w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "fullDescs[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wFullDesc w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "ecbFullDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbFullDesc w{$language->id}" ))?>
					
					<?=$form->textFieldRow( $model, "chartTitles[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wChartTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbChartTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbChartTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "converterTitles[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wConverterTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbConverterTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbConverterTitle w{$language->id}" ))?>
<hr />
					<?=$form->textFieldRow( $model, "singleArchiveYearTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "singleArchiveYearMetaTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMetaTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMetaTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMetaTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "singleArchiveYearMetaDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMetaDesc w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMetaDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMetaDesc w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "singleArchiveYearMetaKeys[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMetaKeys w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMetaKeys[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMetaKeys w{$language->id}" ))?>
					
					<?=$form->textAreaRow( $model, "singleArchiveYearDescription[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearDescription w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "ecbSingleArchiveYearDescription[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearDescription w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "singleArchiveYearFullDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearFullDesc w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "ecbSingleArchiveYearFullDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearFullDesc w{$language->id}" ))?>
<hr />
					<?=$form->textFieldRow( $model, "singleArchiveYearMoTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMoTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "singleArchiveYearMoMetaTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoMetaTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMoMetaTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoMetaTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "singleArchiveYearMoMetaDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoMetaDesc w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMoMetaDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoMetaDesc w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "singleArchiveYearMoMetaKeys[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoMetaKeys w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMoMetaKeys[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoMetaKeys w{$language->id}" ))?>
					
					<?=$form->textAreaRow( $model, "singleArchiveYearMoDescription[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoDescription w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "ecbSingleArchiveYearMoDescription[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoDescription w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "singleArchiveYearMoFullDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoFullDesc w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "ecbSingleArchiveYearMoFullDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoFullDesc w{$language->id}" ))?>
<hr />
					<?=$form->textFieldRow( $model, "singleArchiveYearMoDayTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoDayTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMoDayTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoDayTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "singleArchiveYearMoDayMetaTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoDayMetaTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMoDayMetaTitle[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoDayMetaTitle w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "singleArchiveYearMoDayMetaDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoDayMetaDesc w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMoDayMetaDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoDayMetaDesc w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "singleArchiveYearMoDayMetaKeys[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoDayMetaKeys w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "ecbSingleArchiveYearMoDayMetaKeys[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoDayMetaKeys w{$language->id}" ))?>
					
					<?=$form->textAreaRow( $model, "singleArchiveYearMoDayDescription[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoDayDescription w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "ecbSingleArchiveYearMoDayDescription[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoDayDescription w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "singleArchiveYearMoDayFullDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wsingleArchiveYearMoDayFullDesc w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "ecbSingleArchiveYearMoDayFullDesc[{$language->id}]", Array( 'class' => "{$ins} wFullWidth wecbSingleArchiveYearMoDayFullDesc w{$language->id}" ))?>
<hr />
				</div>
			<?}?>
		</div>

		
<pre>
для 
	Single Archive Year Title
	Single Archive Year Meta Title
	Single Archive Year MetaDesc
	Single Archive Year MetaKeys
	Single Archive Year Description
	Single Archive Year FullDesc
	Single Archive YearMo Title
	Single Archive YearMo MetaTitle
	Single Archive YearMo MetaDesc
	Single Archive YearMo MetaKeys
	Single Archive YearMo Description
	Single Archive YearMo FullDesc
	Single Archive YearMoDay Title
	Single Archive YearMoDay MetaTitle
	Single Archive YearMoDay MetaDesc
	Single Archive YearMoDay MetaKeys
	Single Archive YearMoDay Description
	Single Archive YearMoDay FullDesc
	
дату можно указать так {$date(yyyy)}
где yyyy - формат даты

<a href="http://www.unicode.org/reports/tr35/tr35-dates.html#Date_Format_Patterns" target="_blank">Date Format Patterns</a>

$_formatters из класса CDateFormatter

private static $_formatters=array(
		'G'=>'formatEra',
		'y'=>'formatYear',
		'M'=>'formatMonth',
		'L'=>'formatMonth',
		'd'=>'formatDay',
		'h'=>'formatHour12',
		'H'=>'formatHour24',
		'm'=>'formatMinutes',
		's'=>'formatSeconds',
		'E'=>'formatDayInWeek',
		'c'=>'formatDayInWeek',
		'e'=>'formatDayInWeek',
		'D'=>'formatDayInYear',
		'F'=>'formatDayInMonth',
		'w'=>'formatWeekInYear',
		'W'=>'formatWeekInMonth',
		'a'=>'formatPeriod',
		'k'=>'formatHourInDay',
		'K'=>'formatHourInPeriod',
		'z'=>'formatTimeZone',
		'Z'=>'formatTimeZone',
		'v'=>'formatTimeZone',
	);
</pre>

		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCurrencyRatesFormWidget = wAdminCurrencyRatesFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminCurrencyRatesFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl('admin/currencyRates/ajaxOptionAdd')?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl('admin/currencyRates/ajaxOptionDelete')?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl('admin/currencyRates/ajaxOptionLoad')?>',
			formModelName: '<?=$formModelName?>',
			modelName: '<?=$modelName?>',
		});
	}( window.jQuery );
</script>