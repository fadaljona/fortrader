<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/uploads/colorpicker/js/bootstrap-colorpicker.js" );
	Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/uploads/colorpicker/css/bootstrap-colorpicker.css" );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminInterestRatesBankFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
		'lNew' => 'New bank',
		'lEdit' => 'Edit bank'
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">
	
	<section class="section_offset">
		<h3 class="<?=$ins?> wTitle"><?=$addLabel?></h3>
		<hr class="separator2">
	</section>
	<div class="section_offset">
		<div class="form_add_in_table">
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
			
				<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
				<?=$form->textFieldRow( $model, 'order', Array( 'class' => "{$ins} worder" ) )?>
				<?=$form->dropDownListRow( $model, 'idCountry', $countries, Array( 'class' => "{$ins} widCountry" ))?>
				
				<label for="<?=$model->resolveID( 'showInFirstChart' )?>">
					<?=$form->checkBox( $model, 'showInFirstChart', Array( 'class' => "{$ins} wshowInFirstChart" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'showInFirstChart' )?>
					</span>
				</label>
				
				<label for="<?=$model->resolveID( 'showInSecondChart' )?>">
					<?=$form->checkBox( $model, 'showInSecondChart', Array( 'class' => "{$ins} wshowInSecondChart" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'showInSecondChart' )?>
					</span>
				</label>

				<?=$form->textFieldRow( $model, 'secondChartColor', Array( 'class' => "{$ins} wsecondChartColor colorSelector" ) )?>

				<br />
				
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?> langTabCats" id="tab<?=$title?>" data-langId="<?=$language->id?>">
							<?php
								foreach( $model->textFields as $field => $data ){
									echo $form->{$data['type']}( $model, "{$field}[{$language->id}]", Array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}", 'disabled' => $data['disabled'] ));
								}	
							?>
							
						</div>
					<?}?>
				</div>
				
				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'type' => 'danger',
							'label' => Yii::t( $NSi18n, 'Delete' ),
							'htmlOptions' => Array(
								'class' => "pull-left {$ins} wDelete",
							),
						));
					?>
				</div>
				<div class="clear"></div>
			<?$this->endWidget()?>


		</div>
	</div>

</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = true;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonFormWidget = wAdminInterestRatesBankFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl($submitItemRoute)?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl($deleteItemRoute)?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl($loadItemRoute)?>',
			errorClass: 'inpError',
			formModelName: '<?=$formModelName?>',
			modelName: '<?=$modelName?>',
		});
	}( window.jQuery );
</script>