<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminServerFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New server' : 'Editor server';
	
	$jsls = Array(
		'lNew' => 'New server',
		'lEdit' => 'Editor server',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminServerFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		<?=$form->textFieldRow( $model, 'name', Array( 'class' => "{$ins} wName" ))?>
		<?=$form->textFieldRow( $model, 'internalName', Array( 'class' => "{$ins} wInternalName" ))?>
		<?=$form->textFieldRow( $model, 'url', Array( 'class' => "{$ins} wURL" ))?>
		<?=$form->dropDownListRow( $model, 'idBroker', $brokers, Array( 'class' => "{$ins} wIDBroker" ))?>
		<?=$form->dropDownListRow( $model, 'idTradePlatform', $platforms, Array( 'class' => "{$ins} wIDTradePlatform" ))?>
		
		<label for="<?=$model->resolveID( 'forReports' )?>">
			<?=$form->checkBox( $model, 'forReports', Array( 'class' => "{$ins} wforReports" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'forReports' )?>
			</span>
		</label>
		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminServerFormWidget = wAdminServerFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminServerFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>