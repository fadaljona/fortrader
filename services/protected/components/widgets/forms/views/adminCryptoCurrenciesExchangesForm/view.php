<?php

Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/widgets/forms/wAdminCryptoCurrenciesExchangesFormWidget.js");

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

$jsls = array(
    'lSaving' => 'Saving...',
    'lSaved' => 'Saved',
    'lLoading' => 'Loading...',
    'lDeleteConfirm' => 'Delete?',
    'lReseted' => 'Reseted',
    'lNew' => 'New exchange',
    'lEdit' => 'Edit exchange'
);
foreach ($jsls as &$ls) {
    $ls = Yii::t($NSi18n, $ls);
}
unset($ls);
?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">

    <section class="section_offset">
        <h3 class="<?=$ins?> wTitle"><?=$addLabel?></h3>
        <hr class="separator2">
    </section>
    <div class="section_offset">
        <div class="form_add_in_table">
            <?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', array( 'htmlOptions' => array( 'enctype' => 'multipart/form-data' )))?>

                <?=$form->hiddenField($model, 'id', array( 'class' => "{$ins} wID" ))?>

                <?=$form->textFieldRow($model, 'title', array( 'class' => "{$ins} wmtitle" ))?>
                <?=$form->textFieldRow($model, 'url', array( 'class' => "{$ins} wurl wFullWidth" ))?>
                <?=$form->textFieldRow($model, 'statsUrl', array( 'class' => "{$ins} wstatsUrl wFullWidth" ))?>



                <div class="clear"></div>
                <span class="errorMess red_color"></span>
                <div class="clear"></div>
                <div class="iControlBlock i01">
                <?
                    $this->widget( 'bootstrap.widgets.TbButton', array( 
                        'buttonType' => 'submit', 
                        'type' => 'success',
                        'label' => Yii::t( $NSi18n, 'Save' ),
                        'htmlOptions' => array(
                            'class' => "pull-right {$ins} wSubmit",
                        ),
                    ));
                ?>
                <?
                    $this->widget( 'bootstrap.widgets.TbButton', array( 
                        'type' => 'danger',
                        'label' => Yii::t( $NSi18n, 'Delete' ),
                        'htmlOptions' => array(
                            'class' => "pull-left {$ins} wDelete",
                        ),
                    ));
                ?>
                </div>
                <div class="clear"></div>

            <?$this->endWidget()?>


        </div>
    </div>

</div>

<script>
!function( $ ) {
    var ns = <?=$ns?>;
    var nsText = ".<?=$ns?>";
    var $ns = $( nsText );
    var ins = ".<?=$ins?>";
    var ajax = <?=CommonLib::boolToStr($ajax)?>;
    var hidden = true;
        
    var ls = <?=json_encode( $jsls )?>;
        
    ns.wAdminCommonFormWidget = wAdminCryptoCurrenciesExchangesFormWidgetOpen({
        ins: ins,
        selector: nsText+' .<?=$ins?>',
        ajax: ajax,
        hidden: hidden,
        ls: ls,
        ajaxSubmitURL: '<?=Yii::app()->createUrl($submitItemRoute)?>',
        ajaxDeleteURL: '<?=Yii::app()->createUrl($deleteItemRoute)?>',
        ajaxLoadURL: '<?=Yii::app()->createUrl($loadItemRoute)?>',
        errorClass: 'inpError',
        formModelName: '<?=$formModelName?>',
        modelName: '<?=$modelName?>',
    });
}( window.jQuery );
</script>