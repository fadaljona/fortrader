<div id="forgot-box" class="forgot-box widget-box no-border">
	<div class="widget-body">
		<div class="widget-main">
			<h4 class="header red lighter bigger">
				<i class="icon-key"></i>
				<?=Yii::t( $NSi18n, 'Reset password' )?>
			</h4>

			<div class="space-6"></div>
			<p>
				<?=Yii::t( $NSi18n, 'Enter your email or login to receive instructions' )?>
			</p>

			<?$form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm' )?>
				<fieldset>
					<label>
						<span class="block input-icon input-icon-right">
							<?=$form->textField( $resetUserPasswordReguestFormModel, 'email', Array( 'class' => 'span12', 'placeholder' => $resetUserPasswordReguestFormModel->getAttributeLabel( 'email' )))?>
							<i class="icon-envelope"></i>
						</span>
					</label>

					<div class="row-fluid">
						<button type="submit" class="span5 offset7 btn btn-small btn-danger">
							<i class="icon-lightbulb"></i>
							<?=Yii::t( $NSi18n, 'Send Me!' )?>
						</button>
					</div>
				</fieldset>
			<?$this->endWidget()?>
		</div>

		<div class="toolbar center">
			<a href="#" class="back-to-login-link wShowLogin">
				<?=Yii::t( $NSi18n, 'Back to login' )?>
				<i class="icon-arrow-right"></i>
			</a>
		</div>
	</div>
</div>