<?
	$sex = Array( '', 'Male', 'Female' );
	$sex = array_combine( $sex, $sex );
	foreach( $sex as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	$sex[''] = '- '.Yii::t( $NSi18n, 'Sex' );
?>
<div id="signup-box" class="signup-box widget-box no-border">
	<div class="widget-body">
		<div class="widget-main">
			<h4 class="header green lighter bigger">
				<i class="icon-group blue"></i>
				<?=Yii::t( $NSi18n, 'New User Registration' )?>
			</h4>

			<div class="space-6"></div>
			<p>
				<?=Yii::t( $NSi18n, 'Enter your details' )?>
			</p>

			<?$form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm' )?>
				<fieldset>
					<label>
						<span class="block input-icon input-icon-right">
							<?=$form->textField( $registerUserFormModel, 'user_login', Array( 'class' => 'span12', 'placeholder' => $registerUserFormModel->getAttributeLabel( 'user_login' )))?>
							<i class="icon-user"></i>
						</span>
					</label>

					<label>
						<span class="block input-icon input-icon-right">
							<?=$form->textField( $registerUserFormModel, 'user_email', Array( 'class' => 'span12', 'placeholder' => $registerUserFormModel->getAttributeLabel( 'user_email' )))?>
							<i class="icon-envelope"></i>
						</span>
					</label>

					<label>
						<span class="block input-icon input-icon-right">
							<?=$form->passwordField( $registerUserFormModel, 'user_pass', Array( 'class' => 'span12', 'placeholder' => $registerUserFormModel->getAttributeLabel( 'user_pass' )))?>
							<i class="icon-lock"></i>
						</span>
					</label>

					<label>
						<span class="block input-icon input-icon-right">
							<?=$form->passwordField( $registerUserFormModel, 'user_pass_repeat', Array( 'class' => 'span12', 'placeholder' => $registerUserFormModel->getAttributeLabel( 'user_pass_repeat' )))?>
							<i class="icon-retweet"></i>
						</span>
					</label>
					
					<?=$form->dropDownList( $registerUserFormModel, 'sex', $sex, Array( 'class' => 'span12' ))?>
					
					<?/*
					<label for="<?=$registerUserFormModel->resolveID( 'agree' )?>">
						<?=$form->checkBox( $registerUserFormModel, 'agree', Array())?>
						<span class="lbl">
							<?=Yii::t( $NSi18n, 'I accept the User Agreement', Array( '{href}' => "#" ))?>
						</span>
					</label>
					*/?>

					
					<div class="row-fluid">
						<script src='https://www.google.com/recaptcha/api.js'></script>
						<div id="registrationForm_grecaptcha" class="g-recaptcha" data-sitekey="6Lf_WxsUAAAAAKksUMKf2cjvKpCKbLZRyi3kg_Ev"></div>
					</div>
					
					<div class="space-24"></div>

					<div class="row-fluid">
						<button type="reset" class="span6 btn btn-small">
							<i class="icon-refresh"></i>
							<?=Yii::t( $NSi18n, 'Reset' )?>
						</button>

						<button type="submit" class="span6 btn btn-small btn-success">
							<?=Yii::t( $NSi18n, 'Register' )?>
							<i class="icon-arrow-right icon-on-right"></i>
						</button>
					</div>
				</fieldset>
			<?$this->endWidget()?>
		</div>

		<div class="toolbar center">
			<a href="#" class="back-to-login-link wShowLogin">
				<i class="icon-arrow-left"></i>
				<?=Yii::t( $NSi18n, 'Back to login' )?>
			</a>
		</div>
	</div>
</div>