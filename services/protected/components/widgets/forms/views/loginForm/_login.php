<div id="login-box" class="login-box visible widget-box no-border">
	<div class="widget-body">
		<div class="widget-main">
			<h4 class="header blue lighter bigger">
				<i class="icon-coffee green"></i>
				<?=Yii::t( $NSi18n, 'Please Enter Your Information' )?>
			</h4>

			<div class="space-6"></div>

			<?$form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm' )?>
				<fieldset>
					<label>
						<span class="block input-icon input-icon-right">
							<?=$form->textField( $loginFormModel, 'login', Array( 'class' => 'span12', 'placeholder' => $loginFormModel->getAttributeLabel( 'login' )))?>
							<i class="icon-user"></i>
						</span>
					</label>

					<label>
						<span class="block input-icon input-icon-right">
							<?=$form->passwordField( $loginFormModel, 'password', Array( 'class' => 'span12', 'placeholder' => $loginFormModel->getAttributeLabel( 'password' )))?>
							<i class="icon-lock"></i>
						</span>
					</label>

					<div class="space"></div>

					<div class="row-fluid">
						<label class="span8" for="LoginFormModel_remember">
							<?=$form->checkBox( $loginFormModel, 'remember' )?>
							<span class="lbl">
								<?=Yii::t( $NSi18n, 'Remember Me' )?>
							</span>
						</label>

						<button type="submit" class="span4 btn btn-small btn-primary">
							<i class="icon-key"></i>
							<?=Yii::t( $NSi18n, 'Enter' )?>
						</button>
					</div>
				</fieldset>
			<?$this->endWidget()?>
		</div>

		<div class="toolbar clearfix">
			<div>
				<a href="#" class="forgot-password-link wShowForgot">
					<i class="icon-arrow-left"></i>
					<?=Yii::t( $NSi18n, 'I forgot my password' )?>
				</a>
			</div>

			<div>
				<a href="#" class="user-signup-link wShowRegister">
					<?=Yii::t( $NSi18n, 'I want to register' )?>
					<i class="icon-arrow-right"></i>
				</a>
			</div>
		</div>
	</div>
</div>