<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wLoginFormWidget.js" );
	
	$dir = dirname( __FILE__ );
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lRegistered' => 'Congrats! You are registered!',
		'lMessageSend' => 'Message send!',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="login-container wLoginForm">
	<div class="row-fluid">
		<div class="center">
			<h1>
				<i class="icon-leaf green"></i>
				<span class="red">Services</span>
				<span class="white">For Traders</span>
			</h1>
			<h4 class="blue">FORTRADER.ORG</h4>
		</div>
	</div>
	<div class="space-6"></div>
	
	<?
		echo CHtml::errorSummary( $loginFormModel, null, null, Array( 
			'class' => 'alert alert-block alert-error'
		));
	?>
	
	<div class="row-fluid">
		<div class="position-relative">
			<?require "{$dir}/_login.php"?>
			<?require "{$dir}/_forgot.php"?>
			<?require "{$dir}/_register.php"?>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wLoginFormWidget = wLoginFormWidgetOpen({
			selector: nsText+' .wLoginForm',
			ls: ls,
		});
	}( window.jQuery );
</script>