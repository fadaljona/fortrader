<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminNewsAggregatorRssFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
		'lNew' => 'New source',
		'lEdit' => 'Edit source'
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">
	
	<section class="section_offset">
		<h3 class="<?=$ins?> wTitle"><?=$addLabel?></h3>
		<hr class="separator2">
	</section>
	<div class="section_offset">
		<div class="form_add_in_table">
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>



				<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
				<?=$form->textFieldRow( $model, 'title', Array( 'class' => "{$ins} wtitle wFullWidth" ) )?>
				<?=$form->textFieldRow( $model, 'link', Array( 'class' => "{$ins} wlink wFullWidth" ) )?>
				<?=$form->textFieldRow( $model, 'regExp', Array( 'class' => "{$ins} wregExp wFullWidth" ) )?>
				
				<?=$form->dropDownListRow( $model, 'idSource', $sources, Array( 'class' => "{$ins} widSource" ))?>
				<?=$form->dropDownListRow( $model, 'idLanguage', $languages, Array( 'class' => "{$ins} widLanguage" ))?>
				<?=$form->dropDownListRow( $model, 'idCat', $categories, Array( 'class' => "{$ins} widCat" ))?>
				
				<?=$form->textFieldRow( $model, 'lastBuildDate', Array( 'class' => "{$ins} wlastBuildDate", 'disabled' => true ) )?>
				
				<label for="<?=$model->resolveID( 'enabled' )?>">
					<?=$form->checkBox( $model, 'enabled', Array( 'class' => "{$ins} wenabled" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'enabled' )?>
					</span>
				</label>
				<br />
				
				<label for="<?=$model->resolveID( 'forceCheck' )?>">
					<?=$form->checkBox( $model, 'forceCheck', Array( 'class' => "{$ins} wforceCheck" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'forceCheck' )?>
					</span>
				</label>
				<br />
				
							
				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'type' => 'danger',
							'label' => Yii::t( $NSi18n, 'Delete' ),
							'htmlOptions' => Array(
								'class' => "pull-left {$ins} wDelete",
							),
						));
					?>
				</div>
				<div class="clear"></div>
			<?$this->endWidget()?>


		</div>
	</div>

</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = true;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonFormWidget = wAdminNewsAggregatorRssFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl($submitItemRoute)?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl($deleteItemRoute)?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl($loadItemRoute)?>',
			errorClass: 'inpError',
			formModelName: '<?=$formModelName?>',
			modelName: '<?=$modelName?>',
		});
	}( window.jQuery );
</script>