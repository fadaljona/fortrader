<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wEAVersionFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="wEAVersionFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal', 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id' )?>
		<?=$form->hiddenField( $model, 'idEA' )?>
		<?=$form->textFieldRow( $model, 'version' )?>
		<div class="control-group">
			<label class="control-label"><?=Yii::t( '*', 'Description' )?>:</label>
			<div class="controls">
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
							<?=$form->textArea( $model, "desces[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wDesc", 'rows' => 5 ))?>
						</div>
					<?}?>
				</div>
								
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label"><?=Yii::t( '*', 'File' )?>:</label>
			<div class="controls">
				<?=$form->fileField( $model, 'file' )?>
				<?if( $model->file ){?>
					<br><a href="<?=$model->file?>" target="_blank"><?=basename($model->file)?></a>
				<?}?>
			</div>
		</div>
		
		<?
			$this->widget( 'bootstrap.widgets.TbButton', Array( 
				'buttonType' => 'submit', 
				'type' => 'success',
				'label' => Yii::t( $NSi18n, 'Done!' ),
				'htmlOptions' => Array(
					'class' => "pull-right {$ins} wSubmit",
				),
			));
		?>
		<iframe name="iframeForVersion" id="iframeForVersion" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
				
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wEAVersionFormWidget = wEAVersionFormWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wEAVersionFormWidget',
			ls: ls,
		});
	}( window.jQuery );
</script>