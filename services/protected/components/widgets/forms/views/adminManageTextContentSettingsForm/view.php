<?php
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/lists/wAdminManageTextContentListWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => Yii::t( $NSi18n,'Saving...'),
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="wAdminManageTextContentSettingsFormWidget <?=$ins?>">
<?php

$formModelName = $this->modelFormName;
$formModel = new $formModelName;

$load = $formModel->load();

if( !$load ) $this->throwI18NException( "Can't load data to form model!" );

$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'class' => 'wAdminFullWidthForm100 wAdminFullWidthForm wAdminManageTextContentForm' )));
	
	
	echo CHtml::tag(
		'div',
		array('class' => 'alert alert-info'),
		CHtml::link(Yii::t('*', 'Link to page'), $formModel->singleUrl, array('target' => '_blank'))
	);
	
	echo CHtml::openTag('table', array('class' => 'table'));
	
		echo CHtml::openTag('tr');
			foreach( $languages as $i=>$language ){
				echo CHtml::tag('th', array(), $language->name);
			}
		echo CHtml::closeTag('tr');
		
		echo CHtml::openTag('tr');
		foreach( $languages as $i=>$language ){
			echo CHtml::openTag('td');
				
				foreach( $formModel->textFields as $field => $inpType ){
					if( $inpType == 'textAreaRow' ){
						echo $form->{$inpType}( $formModel, $field . "[{$language->id}]", Array( 'class' => "wFullWidth", 'rows' => 10 ));
					}else{
						echo $form->{$inpType}( $formModel, $field . "[{$language->id}]", Array( 'class' => "wFullWidth" ));
					}
					
				}
				
			echo CHtml::closeTag('td');
		}
		echo CHtml::closeTag('tr');
	echo CHtml::closeTag('table');

	
	$this->widget( 'bootstrap.widgets.TbButton', Array( 
		'buttonType' => 'submit', 
		'type' => 'success',
		'label' => Yii::t( $NSi18n, 'Done!' ),
		'htmlOptions' => Array(
			'class' => "pull-right {$ins} wSubmit",
		),
	));
$this->endWidget();
?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminManageTextContentListWidget = wAdminManageTextContentListWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			saveUrl: '<?=Yii::app()->createUrl('admin/manageTextContent/ajaxSave')?>'
		});
	}( window.jQuery );
</script>
