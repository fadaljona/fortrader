<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminEAStatementFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New statement' : 'Editor statement';
	
	$jsls = Array(
		'lNew' => 'New statement',
		'lEdit' => 'Editor statement',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$periods = Array( 'M1', 'M5', 'M15', 'M30', 'H1', 'H4', 'D', 'M', 'W' );
	$periods = array_combine( $periods, $periods );
	
	$types = Array( 'Demo', 'Real', 'Backtest' );
	$types = array_combine( $types, $types );
	foreach( $types as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminEAStatementFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<li class="active"><a href="#tabMain" data-toggle="tab"><?=Yii::t( $NSi18n, 'Basic' )?></a></li>
			<li><a href="#tabStatistics" data-toggle="tab"><?=Yii::t( $NSi18n, 'Statistics' )?></a></li>
		</ul>
		
		<div class="tab-content" style="position:static">
			<div class="tab-pane active" id="tabMain">
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
							<?=$form->textFieldRow( $model, "titles[{$language->id}]", Array( 'class' => "{$ins} wTitle w{$language->id}" ))?>
							<?=$form->textAreaRow( $model, "desces[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wDesc w{$language->id}", 'rows' => 20 ))?>
						</div>
					<?}?>
				</div>
				
				<?=$form->dropDownListRow( $model, 'idEA', $EA, Array( 'class' => "{$ins} wIDEA" ))?>
				<?=$form->dropDownListRow( $model, 'idVersion', Array(), Array( 'class' => "{$ins} wIDVersion" ))?>
				<?=$form->textFieldRow( $model, 'nameUser', Array( 'class' => "{$ins} wNameUser" ))?>
				<?=$form->textFieldRow( $model, 'symbol', Array( 'class' => "{$ins} wSymbol" ))?>
				<?=$form->dropDownListRow( $model, 'period', $periods, Array( 'class' => "{$ins} wPeriod" ))?>
				<?=$form->dropDownListRow( $model, 'type', $types, Array( 'class' => "{$ins} wType" ))?>
				<?=$form->dropDownListRow( $model, 'idBroker', $brokers, Array( 'class' => "{$ins} wIDBroker" ))?>
				<?=$form->dropDownListRow( $model, 'idServer', Array(), Array( 'class' => "{$ins} wIDServer" ))?>
				<?=$form->dateFieldRow( $model, 'date', Array( 'class' => "{$ins} wDate" ))?>
				
				<br>
				<a href="#" class="<?=$ins?> wSetHref" target="_blank"></a>
				<?=$form->fileFieldRow( $model, 'set' )?>
			</div>
			<div class="tab-pane" id="tabStatistics">
				<?=$form->textFieldRow( $model, 'gain', Array( 'class' => "{$ins} wGain" ))?>
				<?=$form->textFieldRow( $model, 'drow', Array( 'class' => "{$ins} wDrow" ))?>
				<?=$form->textFieldRow( $model, 'trades', Array( 'class' => "{$ins} wTrades" ))?>
				
				<h4 style="padding-top:20px;"><?=Yii::t( $NSi18n, 'Optimization' )?></h4>
				<?=$form->dateFieldRow( $model, 'optimizationStartDate', Array( 'class' => "{$ins} wOptimizationStartDate" ))?>
				<?=$form->dateFieldRow( $model, 'optimizationEndDate', Array( 'class' => "{$ins} wOptimizationEndDate" ))?>
				
				<h4 style="padding-top:20px;"><?=Yii::t( $NSi18n, 'Test' )?></h4>
				<?=$form->dateFieldRow( $model, 'testStartDate', Array( 'class' => "{$ins} wTestStartDate" ))?>
				<?=$form->dateFieldRow( $model, 'testEndDate', Array( 'class' => "{$ins} wTestEndDate" ))?>
				
				<br>
				<a href="#" class="<?=$ins?> wStatementHref" target="_blank"></a>
				<?=$form->fileFieldRow( $model, 'statement', Array( 'class' => "{$ins} wStatement" ))?>
				<div class="<?=$ins?> wStatementLoading" style="display:none">
					<?=Yii::t( $NSi18n, 'Loading...' )?>
				</div>
				
				<br>
				<a href="#" class="<?=$ins?> wGraphHref" target="_blank"></a>
				<?=$form->fileFieldRow( $model, 'graph' )?>
			</div>
		</div>
		
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
		<iframe name="iframeForStatement" id="iframeForStatement" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var defaultNameUser = "<?=Yii::App()->user->getModel()->user_login?>";
				
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminEAStatementFormWidget = wAdminEAStatementFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminEAStatementFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			defaultNameUser: defaultNameUser,
		});
	}( window.jQuery );
</script>