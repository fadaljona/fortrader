<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminNewsAggregatorNewsFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
		'lNew' => 'New source',
		'lEdit' => 'Edit news'
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">
	
	<section class="section_offset">
		<h3 class="<?=$ins?> wTitle"><?=$addLabel?></h3>
		<hr class="separator2">
	</section>
	<div class="section_offset">
		<div class="form_add_in_table">
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>



				<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
				
				<?=$form->textFieldRow( $model, 'title', Array( 'class' => "{$ins} wtitle wFullWidth" ) )?>
				<?=$form->textFieldRow( $model, 'link', Array( 'class' => "{$ins} wlink wFullWidth" ) )?>
				<?=$form->textAreaRow( $model, 'description', Array( 'class' => "{$ins} wdescription wFullWidth", 'rows' => 10 ) )?>
				<?=$form->textFieldRow( $model, 'gmtTime', Array( 'class' => "{$ins} wgmtTime wFullWidth", 'disabled' => true ) )?>
				<?=$form->dropDownListRow( $model, 'idLanguage', $languages, Array( 'class' => "{$ins} widLanguage", 'disabled' => true ))?>
				
				<?=$form->textAreaRow( $model, 'stemedNewsText', Array( 'class' => "{$ins} wstemedNewsText wFullWidth", 'disabled' => true, 'rows' => 10 ) )?>
				<?=$form->textAreaRow( $model, 'categoryKeys', Array( 'class' => "{$ins} wcategoryKeys wFullWidth", 'disabled' => true ) )?>
				
				<?=$form->dropDownListRow( $model, 'idRss', $rsses, Array( 'class' => "{$ins} widRss wFullWidth", 'disabled' => true ))?>
				<?=$form->dropDownListRow( $model, 'idCat', $cats, Array( 'class' => "{$ins} widCat " ))?>
				

				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'type' => 'danger',
							'label' => Yii::t( $NSi18n, 'Delete' ),
							'htmlOptions' => Array(
								'class' => "pull-left {$ins} wDelete",
							),
						));
					?>
				</div>
				<div class="clear"></div>
			<?$this->endWidget()?>


		</div>
	</div>
</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = true;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonFormWidget = wAdminNewsAggregatorNewsFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl($submitItemRoute)?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl($deleteItemRoute)?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl($loadItemRoute)?>',
			errorClass: 'inpError',
			formModelName: '<?=$formModelName?>',
			modelName: '<?=$modelName?>',
		});
	}( window.jQuery );
</script>