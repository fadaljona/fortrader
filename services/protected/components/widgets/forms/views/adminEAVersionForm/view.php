<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminEAVersionFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New version' : 'Editor version';
	
	$jsls = Array(
		'lNew' => 'New version',
		'lEdit' => 'Editor version',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$statuses = Array( 'Developing', 'Finish', 'Abandoned' );
	$statuses = array_combine( $statuses, $statuses );
	foreach( $statuses as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminEAVersionFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<?=$form->dropDownListRow( $model, 'idEA', $EA,  Array( 'class' => "{$ins} wIDEA" ))?>
		<?=$form->textFieldRow( $model, 'version', Array( 'class' => "{$ins} wVersion" ))?>
		<?=$form->textFieldRow( $model, 'nameUser', Array( 'class' => "{$ins} wNameUser" ))?>
		<?=$form->dropDownListRow( $model, 'status', $statuses,  Array( 'class' => "{$ins} wStatus" ))?>
		<?=$form->dateFieldRow( $model, 'release', Array( 'class' => "{$ins} wRelease" ))?>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' class="active"' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<li<?=$class?>>
					<a href="#tab<?=$title?>" data-toggle="tab">
						<?=$title?>
					</a>
				</li>
			<?}?>
		</ul>
		
		<div class="tab-content">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' active' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
					<?=$form->textAreaRow( $model, "desces[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wDesc w{$language->id}", 'rows' => 20 ))?>
				</div>
			<?}?>
		</div>
		
		<br>
		<a href="#" class="<?=$ins?> wFileHref" target="_blank"></a>
		<?=$form->fileFieldRow( $model, "file" )?>
		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
		<iframe name="iframeForVersion" id="iframeForVersion" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var defaultNameUser = "<?=Yii::App()->user->getModel()->user_login?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminEAVersionFormWidget = wAdminEAVersionFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminEAVersionFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			defaultNameUser: defaultNameUser,
		});
	}( window.jQuery );
</script>