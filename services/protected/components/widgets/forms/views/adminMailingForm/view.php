<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminMailingFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New mailing' : 'Editor mailing';
	
	$jsls = Array(
		'lNew' => 'New mailing',
		'lEdit' => 'Editor mailing',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lNumSendSMS' => 'Will be sent {d} SMS',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminMailingFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<ul class="nav nav-tabs <?=$ins?> wTabsNav">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' class="active"' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<li<?=$class?>>
					<a href="#tab<?=$title?>" data-toggle="tab">
						<?=$title?>
					</a>
				</li>
			<?}?>
		</ul>
		
		<div class="tab-content">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' active' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
					<?=$form->textFieldRow( $model, "names[{$language->id}]", Array( 'class' => "{$ins} wName w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "messages[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wMessage w{$language->id}", 'rows' => '5' ))?>
					<div class="clear"></div>
					<span class="<?=$ins?> wStat<?=$language->id?>"></span>
					<br><br>
				</div>
			<?}?>
		</div>
		
		<?=$form->dropDownListRow( $model, 'usersGroup', $userGroups, Array( 'class' => "{$ins} wUsersGroup" ))?>
		<?=$form->dropDownListRow( $model, 'idType', $types, Array( 'class' => "{$ins} wIDType" ))?>
		
		<label for="<?=$model->resolveID( 'isSMS' )?>">
			<?=$form->checkBox( $model, 'isSMS', Array( 'class' => "{$ins} wIsSMS" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'isSMS' )?>
			</span>
		</label>
		
		<label for="<?=$model->resolveID( 'isEmail' )?>">
			<?=$form->checkBox( $model, 'isEmail', Array( 'class' => "{$ins} wIsEmail" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'isEmail' )?>
			</span>
		</label>
		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminMailingFormWidget = wAdminMailingFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminMailingFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>