<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminContactFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New contact' : 'Editor contact';
	
	$jsls = Array(
		'lNew' => 'New contact',
		'lEdit' => 'Editor contact',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
?>
<div class="well pull-left iFormWidget i01 wAdminContactFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<a href="#" class="<?=$ins?> wImageHref " target="_blank"></a>
		<?=$form->fileFieldRow( $model, 'image', Array( 'class' => "{$ins} wImage" ))?>
				
		<label for="<?=$model->resolveID( 'deleteImage' )?>">
			<?=$form->checkBox( $model, 'deleteImage', Array( 'class' => "{$ins} wDeleteImage" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'deleteImage' )?>
			</span>
		</label>
		
		<?=$form->textFieldRow( $model, 'email', Array( 'class' => "{$ins} wEmail" ))?>
		<?=$form->textFieldRow( $model, 'skype', Array( 'class' => "{$ins} wSkype" ))?>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' class="active"' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<li<?=$class?>>
					<a href="#tab<?=$title?>" data-toggle="tab">
						<?=$title?>
					</a>
				</li>
			<?}?>
		</ul>
		
		<div class="tab-content">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' active' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
					<?=$form->textFieldRow( $model, "posts[{$language->id}]", Array( 'class' => "{$ins} wPost w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "names[{$language->id}]", Array( 'class' => "{$ins} wName w{$language->id}" ))?>
					<?=$form->textAreaRow( $model, "descs[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wDesc w{$language->id}", "rows" => 5 ))?>
				</div>
			<?}?>
		</div>
				
		<?=$form->textFieldRow( $model, 'position', Array( 'class' => "{$ins} wPosition" ))?>
		
		<label for="<?=$model->resolveID( 'reciveFeedback' )?>">
			<?=$form->checkBox( $model, 'reciveFeedback', Array( 'class' => "{$ins} wReciveFeedback" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'reciveFeedback' )?>
			</span>
		</label>
				
		<label for="<?=$model->resolveID( 'hidden' )?>">
			<?=$form->checkBox( $model, 'hidden', Array( 'class' => "{$ins} wHidden" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'hidden' )?>
			</span>
		</label>
				
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
		<iframe name="iframeForContact" id="iframeForContact" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminContactFormWidget = wAdminContactFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminContactFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>