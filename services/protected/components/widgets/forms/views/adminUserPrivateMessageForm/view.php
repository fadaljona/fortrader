<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminUserPrivateMessageFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New message' : 'Editor message';
	
	$jsls = Array(
		'lNew' => 'New message',
		'lEdit' => 'Editor message',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminUserPrivateMessageFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<?=$form->textFieldRow( $model, "from", Array( 'class' => "{$ins} wFrom" ))?>
		<?=$form->textFieldRow( $model, "to", Array( 'class' => "{$ins} wTo" ))?>
		<?=$form->textAreaRow( $model, "text", Array( 'class' => "{$ins} wText" ))?>
		
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var defaultFrom = "<?=Yii::App()->user->getModel()->user_login?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminUserPrivateMessageFormWidget = wAdminUserPrivateMessageFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminUserPrivateMessageFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			defaultFrom: defaultFrom,
		});
	}( window.jQuery );
</script>