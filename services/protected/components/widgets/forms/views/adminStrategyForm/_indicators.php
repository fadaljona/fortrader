<label for="indicator"><?=Yii::t( $NSi18n, "Add indicator" )?></label>
<?=CHtml::dropDownList( "indicator", null, $indicators, Array( 'class' => "{$ins} wIndicator" ))?>
<div class="clear"></div>
<?
	$this->widget( 'bootstrap.widgets.TbButton', Array( 
		'label' => Yii::t( $NSi18n, 'Add' ),
		'size' => 'mini',
		'htmlOptions' => Array(
			'class' => "{$ins} wAddIndicator",
		),
	));
?>
<div class="clear"></div>
<?=$form->labelEx( $model, 'indicators', Array( 'style' => "padding-top:10px;" ))?>
<?=$form->hiddenField( $model, 'indicators', Array( 'class' => "{$ins} wIndicators" ))?>
<table class="table i01 table-bordered <?=$ins?> wTabIndicators">
	<tbody>
		<tr class="wTpl" style="display:none;">
			<td class="wTDName"></td>
			<td class="iActions">
				<a href="#" class="icon-trash wDelete"></a>
			</td>
		</tr>
	</tbody>
</table>
<div class="clear"></div>