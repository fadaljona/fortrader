<label for="timeframe"><?=Yii::t( $NSi18n, "Add timeframe" )?></label>
<?=CHtml::dropDownList( "timeframe", null, $timeframes, Array( 'class' => "{$ins} wTimeframe" ))?>
<div class="clear"></div>
<?
	$this->widget( 'bootstrap.widgets.TbButton', Array( 
		'label' => Yii::t( $NSi18n, 'Add' ),
		'size' => 'mini',
		'htmlOptions' => Array(
			'class' => "{$ins} wAddTimeframe",
		),
	));
?>
<div class="clear"></div>
<?=$form->labelEx( $model, 'timeframes', Array( 'style' => "padding-top:10px;" ))?>
<?=$form->hiddenField( $model, 'timeframes', Array( 'class' => "{$ins} wTimeframes" ))?>
<table class="table i01 table-bordered <?=$ins?> wTabTimeframes">
	<tbody>
		<tr class="wTpl" style="display:none;">
			<td class="wTDName"></td>
			<td class="iActions">
				<a href="#" class="icon-trash wDelete"></a>
			</td>
		</tr>
	</tbody>
</table>
<div class="clear"></div>