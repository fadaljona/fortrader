<label for="instrument"><?=Yii::t( $NSi18n, "Add instrument" )?></label>
<?=CHtml::dropDownList( "instrument", null, $instruments, Array( 'class' => "{$ins} wInstrument" ))?>
<div class="clear"></div>
<?
	$this->widget( 'bootstrap.widgets.TbButton', Array( 
		'label' => Yii::t( $NSi18n, 'Add' ),
		'size' => 'mini',
		'htmlOptions' => Array(
			'class' => "{$ins} wAddInstrument",
		),
	));
?>
<div class="clear"></div>
<?=$form->labelEx( $model, 'instruments', Array( 'style' => "padding-top:10px;" ))?>
<?=$form->hiddenField( $model, 'instruments', Array( 'class' => "{$ins} wInstruments" ))?>
<table class="table i01 table-bordered <?=$ins?> wTabInstruments">
	<tbody>
		<tr class="wTpl" style="display:none;">
			<td class="wTDName"></td>
			<td class="iActions">
				<a href="#" class="icon-trash wDelete"></a>
			</td>
		</tr>
	</tbody>
</table>
<div class="clear"></div>