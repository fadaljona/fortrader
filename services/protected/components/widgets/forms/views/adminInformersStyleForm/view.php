<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminInformersStyleFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
		'lNew' => 'New style',
		'lEdit' => 'Edit style'
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);

?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">
	
	<section class="section_offset">
		<h3 class="<?=$ins?> wTitle"><?=$addLabel?></h3>
		<hr class="separator2">
	</section>
	<div class="section_offset">
		<div class="form_add_in_table">
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
			
				<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
				
				<?=$form->dropDownListRow( $model, 'categoryTypes', $categoryTypes, Array( 'class' => "{$ins} wcategoryTypes", 'id' => 'categoryTypes', 'size' => 4, 'multiple' => true ))?>
				
				<?=$form->dropDownListRow( $model, 'viewFile', $viewFiles, Array( 'class' => "{$ins} wviewFile" ))?>
				
				<label for="<?=$model->resolveID( 'oneColumn' )?>">
					<?=$form->checkBox( $model, 'oneColumn', Array( 'class' => "{$ins} woneColumn" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'oneColumn' )?>
					</span>
				</label>
				
				<label for="<?=$model->resolveID( 'defaultStyle' )?>">
					<?=$form->checkBox( $model, 'defaultStyle', Array( 'class' => "{$ins} wdefaultStyle" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'defaultStyle' )?>
					</span>
				</label>
				
				<label for="<?=$model->resolveID( 'hasDiffValue' )?>">
					<?=$form->checkBox( $model, 'hasDiffValue', Array( 'class' => "{$ins} whasDiffValue" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'hasDiffValue' )?>
					</span>
				</label>

				<label for="<?=$model->resolveID( 'hidden' )?>">
					<?=$form->checkBox( $model, 'hidden', Array( 'class' => "{$ins} whidden" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'hidden' )?>
					</span>
				</label>
				
				<?=$form->textFieldRow( $model, 'order', Array( 'class' => "{$ins} worder" ) )?>
				
				<?=$form->textFieldRow( $model, 'width', Array( 'class' => "{$ins} wwidth" ) )?>
				<?=$form->textFieldRow( $model, 'borderWidth', Array( 'class' => "{$ins} wborderWidth" ) )?>
				<?=$form->textAreaRow( $model, 'settings', Array( 'class' => "{$ins} wsettings wFullWidth", 'rows' => 20 ) )?>
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?> langTabCats" id="tab<?=$title?>" data-langId="<?=$language->id?>">
							<?php
								foreach( $model->textFields as $field => $data ){
									echo $form->{$data['type']}( $model, "{$field}[{$language->id}]", Array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}", 'disabled' => $data['disabled'] ));
								}	
							?>
							
						</div>
					<?}?>
				</div>
				
				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'type' => 'danger',
							'label' => Yii::t( $NSi18n, 'Delete' ),
							'htmlOptions' => Array(
								'class' => "pull-left {$ins} wDelete",
							),
						));
					?>
				</div>
				<div class="clear"></div>
			<?$this->endWidget()?>


		</div>
	</div>

</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = true;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonFormWidget = wAdminInformersStyleFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl($submitItemRoute)?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl($deleteItemRoute)?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl($loadItemRoute)?>',
			errorClass: 'inpError',
			formModelName: '<?=$formModelName?>',
			modelName: '<?=$modelName?>',
		});
	}( window.jQuery );
</script>