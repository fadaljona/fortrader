<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wUserPasswordFormWidget.js" );
	
	$class = $this->getCleanClassName();
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lReseted' => 'Reseted',
		'lError' => 'Error!',
		'lPasswordSaved' => 'Password saved',
		'lSave' => 'Save',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="inside-form__pass clearfix <?=$ins?> spinner-margin position-relative">
	<div class="inside-form__title"><?=Yii::t($NSi18n, 'Change password')?></div>
	<?php
		$form = $this->beginWidget('widgets.base.UserProfileFormWidgetBase', Array( 'htmlOptions' => array('class' => 'inside-form__pass-wr') ));	
			echo $form->hiddenField( $model, 'ID' );
			echo $form->passwordInp( $model, 'oldPassword', array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__input' ) );
			echo $form->passwordInp( $model, 'password', array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__input' ) );
			echo $form->passwordInp( $model, 'repeatPassword', array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__input' ) );
			echo CHtml::htmlButton( Yii::t($NSi18n, 'Save'), array( 'class' => 'inside-form__btn-blue wSubmit' ));
		$this->endWidget();
	?>
</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
				
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wUserPasswordFormWidget = wUserPasswordFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl('user/ajaxUpdatePassword')?>',
		});
	}( window.jQuery );
</script>