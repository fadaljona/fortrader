<?php

Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/widgets/forms/wAdminPaymentExchangeDirectionFormWidget.js");

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

$jsls = array(
    'lSaving' => 'Saving...',
    'lSaved' => 'Saved',
    'lLoading' => 'Loading...',
    'lDeleteConfirm' => 'Delete?',
    'lReseted' => 'Reseted',
    'lNew' => 'New exchanger',
    'lEdit' => 'Edit exchanger'
);
foreach ($jsls as &$ls) {
    $ls = Yii::t($NSi18n, $ls);
}
unset($ls);

error_reporting(E_ALL);
ini_set('display_errors', 1);
?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">

    <section class="section_offset">
        <h3 class="<?=$ins?> wTitle"><?=$addLabel?></h3>
        <hr class="separator2">
    </section>
    <div class="section_offset">
        <div class="form_add_in_table">
            <?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', array( 'htmlOptions' => array( 'enctype' => 'multipart/form-data' )))?>

                <?=$form->hiddenField($model, 'id', array( 'class' => "{$ins} wID" ))?>

                <?=$form->textFieldRow($model, 'direction', array( 'class' => "{$ins} wdirection", 'disabled' => true ))?>
                <br>
                <div class="<?=$ins?> imageWrap "></div>
                <?=$form->fileFieldRow($model, "ogImageFile")?>


                <br /><br /><br>

                <ul class="nav nav-tabs <?=$ins?> wTabs">
                    <?php foreach ($languages as $i => $language) {?>
                        <?$class = !$i ? ' class="active"' : ''?>
                        <?$title = strtoupper($language->alias)?>
                        <?$title = str_replace("EN_US", "EN", $title)?>
                        <li<?=$class?>>
                            <a href="#tab<?=$title?>" data-toggle="tab"><?=$title?></a>
                        </li>
                    <?}?>
                </ul>

                <div class="tab-content">
                    <?foreach ($languages as $i => $language) {?>
                        <?$class = !$i ? ' active' : ''?>
                        <?$title = strtoupper($language->alias)?>
                        <?$title = str_replace("EN_US", "EN", $title)?>
                        <div class="tab-pane<?=$class?> langTabCats" id="tab<?=$title?>" data-langId="<?=$language->id?>">
                        <?php
                            foreach ($model->textFields as $field => $data) {
                                echo $form->{$data['type']}($model, "{$field}[{$language->id}]", array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}", 'disabled' => $data['disabled'] ));
                            }
                        ?>
                        </div>
                    <?}?>
                </div>

                <div class="clear"></div>
                <span class="errorMess red_color"></span>
                <div class="clear"></div>
                <div class="iControlBlock i01">
                <?
                    $this->widget( 'bootstrap.widgets.TbButton', array( 
                        'buttonType' => 'submit', 
                        'type' => 'success',
                        'label' => Yii::t( $NSi18n, 'Save' ),
                        'htmlOptions' => array(
                            'class' => "pull-right {$ins} wSubmit",
                        ),
                    ));
                ?>
                <?
                    $this->widget( 'bootstrap.widgets.TbButton', array( 
                        'type' => 'danger',
                        'label' => Yii::t( $NSi18n, 'Delete' ),
                        'htmlOptions' => array(
                            'class' => "pull-left {$ins} wDelete",
                        ),
                    ));
                ?>
                </div>
                <div class="clear"></div>
                <iframe name="iframePaymentSystem" id="iframePaymentSystem" style="display:none"></iframe>
            <?$this->endWidget()?>


        </div>
    </div>

</div>

<script>
!function( $ ) {
    var ns = <?=$ns?>;
    var nsText = ".<?=$ns?>";
    var $ns = $( nsText );
    var ins = ".<?=$ins?>";
    var ajax = <?=CommonLib::boolToStr($ajax)?>;
    var hidden = true;

    var ls = <?= json_encode($jsls) ?>;

    ns.wAdminCommonFormWidget = wAdminPaymentExchangeDirectionFormWidgetOpen({
        ins: ins,
        selector: nsText+' .<?=$ins?>',
        ajax: ajax,
        hidden: hidden,
        ls: ls,
        ajaxSubmitURL: '<?=Yii::app()->createUrl('admin/exchangersMonitoring/ajaxSubmitPaymentSystem')?>',
        ajaxDeleteURL: '<?=Yii::app()->createUrl($deleteItemRoute)?>',
        ajaxLoadURL: '<?=Yii::app()->createUrl($loadItemRoute)?>',
        errorClass: 'inpError',
        formModelName: '<?=$formModelName?>',
        modelName: '<?=$modelName?>',
    });
}( window.jQuery );
</script>