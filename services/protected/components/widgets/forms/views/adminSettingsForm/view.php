<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminSettingsFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$importAccountDataVariants = Array( 'Enabled', 'Disabled' );
	$importAccountDataVariants = array_combine( $importAccountDataVariants, $importAccountDataVariants );
	foreach( $importAccountDataVariants as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$logImportAccountData = Array( 'Extended', 'Short' );
	$logImportAccountData = array_combine( $logImportAccountData, $logImportAccountData );
	foreach( $logImportAccountData as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminSettingsFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, 'Editor settings' )?></h3>
		
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tabCommon" data-toggle="tab"><?=Yii::t( $NSi18n, 'Common' )?></a></li>
			<li><a href="#tabSMTP" data-toggle="tab">SMTP</a></li>
			<li><a href="#tabClickatell" data-toggle="tab">Clickatell</a></li>
			<li><a href="#tabImportMembers" data-toggle="tab"><?=Yii::t( $NSi18n, 'Import members' )?></a></li>
			<li><a href="#tabSm" data-toggle="tab"><?=Yii::t( $NSi18n, 'Social media' )?></a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active" id="tabCommon">
				<?=$form->textFieldRow( $model, 'common_mailForMailing' )?>
				<?=$form->textFieldRow( $model, 'common_adClickCost' )?>
				<?=$form->textFieldRow( $model, 'common_delimiter' )?>
				<?=$form->dropDownListRow( $model, 'common_importAccountData', $importAccountDataVariants )?>
				<?=$form->dropDownListRow( $model, 'common_logImportAccountData', $logImportAccountData )?>
                <?=$form->textFieldRow( $model, 'common_eaPerPage' )?>
                <?=$form->textFieldRow( $model, 'common_numberOfNotifications' )?>
				<?=$form->textFieldRow( $model, 'servicesPostsCat' )?>
			</div>
			<div class="tab-pane" id="tabSMTP">
				<?=$form->textFieldRow( $model, 'smtp_host' )?>
				<?=$form->textFieldRow( $model, 'smtp_port' )?>
				<?=$form->textFieldRow( $model, 'smtp_username' )?>
				<?=$form->passwordFieldRow( $model, 'smtp_password', Array( 'class' => "{$ins} w_smtp_password", 'placeholder' => Yii::t( $NSi18n, 'Fill to change' )))?>
			</div>
			<div class="tab-pane" id="tabClickatell">
				<?=$form->textFieldRow( $model, 'clickatell_api_id' )?>
				<?=$form->textFieldRow( $model, 'clickatell_user' )?>
				<?=$form->passwordFieldRow( $model, 'clickatell_password', Array( 'class' => "{$ins} w_clickatell_password", 'placeholder' => Yii::t( $NSi18n, 'Fill to change' )))?>
			</div>
			<div class="tab-pane" id="tabImportMembers">
				<?=$form->textFieldRow( $model, 'importMembers_loginForMessage' )?>
				<?=$form->textFieldRow( $model, 'importMembers_mail_host' )?>
				<?=$form->textFieldRow( $model, 'importMembers_mail_login' )?>
				<?=$form->passwordFieldRow( $model, 'importMembers_mail_pass', Array( 'class' => "{$ins} w_importMembers_mail_pass", 'placeholder' => Yii::t( $NSi18n, 'Fill to change' )))?>
			</div>
			<div class="tab-pane" id="tabSm">
				<?=$form->textFieldRow( $model, 'vkAppId' )?>
				<?=$form->textFieldRow( $model, 'vkAppSecretKey' )?>
				<?=$form->textFieldRow( $model, 'fbAppId' )?>
				<?=$form->textFieldRow( $model, 'fbAppSecretKey' )?>
			</div>
		</div>	
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Save' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Reset' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wReset",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminSettingsFormWidget = wAdminSettingsFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminSettingsFormWidget',
			ajax: ajax,
			ls: ls,
		});
	}( window.jQuery );
</script>