<?
$cs=Yii::app()->getClientScript();
$formHelpersBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.assets-static.formHelpers') );
$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers.css');
$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers-countries.flags.css');
$cs->registerScriptFile($formHelpersBaseUrl.'/js/bootstrap-formhelpers-selectbox.js');

$ns     = $this->getNS();
$ins    = $this->getINS();
$NSi18n = $this->getNSi18n();
?>
<div class="wInfoContestMemberFormWidget">
	<? $form = $this->beginWidget( 'widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal' ) ) ?>
	<h3 class="header smaller lighter blue"><?=Yii::t( $NSi18n, 'Your information' )?></h3>
	<? if ( $UMode == "guest" ) {
		echo $form->textFieldRowIcon( $UModel, 'user_login', Array(
			'class'          => 'span12',
			'classSpanInput' => 'span6'
		) );
		echo $form->textFieldRowIcon( $UModel, 'user_email', Array(
			'class'          => 'span12',
			'classSpanInput' => 'span6'
		) );
		echo $form->passwordFieldRowIcon( $UModel, 'user_pass', Array(
			'class'          => 'span12',
			'classSpanInput' => 'span6'
		) );
		echo $form->passwordFieldRowIcon( $UModel, 'user_pass_repeat', Array(
			'class'          => 'span12',
			'classSpanInput' => 'span6'
		) );
		echo $form->hiddenField( $UModel, 'sex', Array( 'value' => 'Male' ) );
		echo $form->hiddenField( $UModel, 'user_id', Array( 'value' => 0 ) );
	} ?>
	<?=$form->textFieldRowIcon( $model, 'firstName', Array(
		'class'          => 'span12',
		'classSpanInput' => 'span6'
	) )?>
	<?=$form->textFieldRowIcon( $model, 'lastName', Array(
		'class'          => 'span12',
		'classSpanInput' => 'span6'
	) )?>
	<?=$form->textFieldRowIcon( $model, 'phone', Array(
		'class'          => 'span12',
		'classSpanInput' => 'span6'
	) )?>
	<div class="control-group i01">
		<?=$form->labelEx( $model, 'idCountry', Array( 'class' => 'control-label' ) )?>
		<div class="controls">
			<div class="bfh-selectbox bfh-countries">
				<?=$form->hiddenField( $model, 'idCountry' )?>
				<a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
						<span class="bfh-selectbox-option input-medium">
							<? if ( $model->idCountry and $obj = @$countries[ $model->idCountry ] ) { ?>
								<i class="ficon-<?=strtoupper( $obj->alias )?>"></i>
								<?=$obj->name?>
							<? } else { ?>
								<span class="grey"><?=Yii::t( $NSi18n, 'Select...' )?></span>
							<? } ?>
						</span>
					<b class="caret"></b>
				</a>

				<div class="bfh-selectbox-options">
					<input type="text" class="bfh-selectbox-filter">

					<div role="listbox">
						<ul role="option">
							<? foreach ( $countries as $id => $obj ) { ?>
								<li>
									<a href="#" data-option="<?=$id?>">
										<i class="ficon-<?=strtoupper( $obj->alias )?>"></i>
										<?=$obj->name?>
									</a>
								</li>
							<? } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<? $this->endWidget() ?>
	<? if ( $UMode == "guest" ) { ?>
	<div id="loginForgot" style="display: none">
		<div id="login-box" class="login-box visible widget-box no-border">
			<div class="widget-body">
				<div class="widget-main">
					<? $form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm',array('action'=>Yii::app()->createUrl('/default/login',array('returnURL'=>$_SERVER['REQUEST_URI']))) ) ?>
					<fieldset>
						<?=$form->textField( $loginFormModel, 'login', Array(
							'class'       => 'span6',
							'placeholder' => $loginFormModel->getAttributeLabel( 'login' )
						) )?>
						</label>
						<label>
							<?=$form->passwordField( $loginFormModel, 'password', Array(
								'class'       => 'span6',
								'placeholder' => $loginFormModel->getAttributeLabel( 'password' )
							) )?>
						</label>
						<div class="space"></div>
						<div class="row-fluid">
							<label class="span2" for="LoginFormModel_remember" style="left:0;top:0;">
								<?=$form->checkBox( $loginFormModel, 'remember' )?>
								<span class="lbl">
								<?=Yii::t( $NSi18n, 'Remember Me' )?>
							</span>
							</label>
							<div class="span2 btn btn-small btn-danger" style="left:0;top:0;" id="back_from_login">
								<a href="#" class="back-to-login-link wShowLogin" style="color: #fe9;font-size: 14px;font-weight: bold;text-shadow: 1px 0 1px rgba(0, 0, 0, 0.25);">
									<i class="icon-arrow-left"></i>
									<?=Yii::t( $NSi18n, 'Back' )?>
								</a>
							</div>
							<button type="submit" class="span2 btn btn-small btn-primary" style="left:0;top:0;">
								<i class="icon-key"></i>
								<?=Yii::t( $NSi18n, 'Enter' )?>
							</button>
						</div>
					</fieldset>
					<? $this->endWidget() ?>
				</div>
			</div>
		</div>
		<div id="forgot-box" class="forgot-box widget-box no-border">
			<div class="widget-body">
				<div class="widget-main">
					<p>
						<?=Yii::t( $NSi18n, 'Enter your email or login to receive instructions' )?>
					</p>

					<? $form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm' ) ?>
					<fieldset>
						<label>
							<?=$form->textField( $resetUserPasswordReguestFormModel, 'email', Array(
								'class'       => 'span6',
								'placeholder' => $resetUserPasswordReguestFormModel->getAttributeLabel( 'email' )
							) )?>
						</label>

						<div class="row-fluid">
							<div class="span2 btn btn-small btn-danger left" style="left:0;top:0;"  id="back_from_forgot">
								<a href="#" class="back-to-login-link wShowLogin">
									<i class="icon-arrow-left"></i>
									<?=Yii::t( $NSi18n, 'Back' )?>
								</a>
							</div>
							<button type="submit" class="span2 btn btn-small btn-danger" style="left:1px;top:1px;">
								<i class="icon-lightbulb"></i>
								<?=Yii::t( $NSi18n, 'Send Me!' )?>
							</button>
						</div>
					</fieldset>
					<? $this->endWidget() ?>
				</div>
			</div>
		</div>
	</div>
	<?} ?>
</div>