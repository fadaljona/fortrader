<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminUserGroupFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New group' : 'Editor group';
	
	$jsls = Array(
		'lNew' => 'New group',
		'lEdit' => 'Editor group',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	$icons = UserGroupModel::getAvailableIconClasses();
	sort( $icons );
	$icons = array_combine( $icons, $icons );
?>
<div class="well pull-left iFormWidget i01 wAdminUserGroupFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		<?=$form->textFieldRow( $model, 'name', Array( 'class' => "{$ins} wName" ))?>
		
		<?=$form->labelEx( $model, 'iconClass' )?>
		<i class="<?=$model->iconClass?> <?=$ins?> iIcon i01 wIcon"></i>
		<?=$form->dropDownList( $model, 'iconClass', $icons, Array( 'class' => "iSelect i01 {$ins} wIconClass" ))?>
		<?=$form->textAreaRow( $model, 'additionalInformation', Array( 'class' => "{$ins} wAdditionalInformation" ))?>
		<div class="clear"></div>
		<div class="iRightsBlock i01 <?=$ins?> wRights">
			<?=Yii::t( $NSi18n, 'Group rights' )?>
			
			<ul class="nav nav-tabs <?=$ins?> wRightTabs" style="margin-bottom:10px;">
				<li class="active"><a href="#tabControl" data-toggle="tab"><?=Yii::t( $NSi18n, 'Control' )?></a></li>
				<li><a href="#tabBan" data-toggle="tab"><?=Yii::t( $NSi18n, 'Ban' )?></a></li>
				<li><a href="#tabOther" data-toggle="tab"><?=Yii::t( $NSi18n, 'Other' )?></a></li>
			</ul>
			
			<div class="tab-content">
				<?foreach( Array( 'Control', 'Ban', 'Other' ) as $i=>$type ){?>
					<div class="tab-pane <?=$i?'':'active'?>" id="tab<?=$type?>">
						<?foreach( $rights as $right ){?>
							<?if( $right->type != $type ) continue;?>
							<label>
								<?=CHtml::checkBox( $model->resolveName( 'rights[]' ), false, Array( 'id' => '', 'value' => $right->id ))?>
								<span class="lbl">
									<?=Yii::t( $NSi18n, $right->title )?>
								</span>
							</label>
						<?}?>
					</div>	
				<?}?>
			</div>
						
		</div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminUserGroupFormWidget = wAdminUserGroupFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminUserGroupFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>