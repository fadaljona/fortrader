<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminAdvertisementZoneFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New zone' : 'Editor zone';
	
	$jsls = Array(
		'lNew' => 'New zone',
		'lEdit' => 'Editor zone',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$typesAds = Array( 'Text','Media','Buttons', 'Offers' );
	$typesAds = array_combine( $typesAds, $typesAds );
	foreach( $typesAds as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$typesBlock = Array( 'Horizontal', 'Vertical', 'Fixed', 'Popup' );
	$typesBlock = array_combine( $typesBlock, $typesBlock );
	foreach( $typesBlock as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$countsAds = range( 1, 20 );
	$countsAds = array_combine( $countsAds, $countsAds );
	
	$typesBorder = Array( 
		'None' => 'None border', 
		'All' => 'All ads', 
		'Each' => 'Each ad',
	);
	foreach( $typesBorder as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$fonts = Array( 'Arial', 'Tahoma' );
	$fonts = array_combine( $fonts, $fonts );
	
	$sizesFont = range( 10, 20 );
	$sizesFont = array_combine( $sizesFont, $sizesFont );
?>
<div class="wAdminAdvertisementZoneFormWidget">
	<div class="well pull-left iFormWidget i01">
		<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase')?>
			<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
			<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
			<?=$form->textFieldRow( $model, 'name', Array( 'class' => "{$ins} wName" ))?>
			<?=$form->textFieldRow( $model, 'site', Array( 'class' => "{$ins} wSite" ))?>
			<?=$form->dropDownListRow( $model, 'typeAds', $typesAds, Array( 'class' => "{$ins} wTypeAds" ))?>
			
			<label for="<?=$model->resolveID( 'mixedZone' )?>">
				<?=$form->checkBox( $model, 'mixedZone', Array( 'class' => "{$ins} wMixedZone" ))?>
				<span class="lbl">
					<?=$model->getAttributeLabel( 'mixedZone' )?>
				</span>
			</label>
			
			<?=$form->dropDownListRow( $model, 'typeBlock', $typesBlock, Array( 'class' => "{$ins} wTypeBlock" ))?>
			<div>
				<?=$form->dropDownListRow( $model, 'sizeBlock', $sizesBlock, Array( 'class' => "{$ins} wSizeBlock" ))?>
			</div>
			<div>
				<?=$form->dropDownListRow( $model, 'countAds', $countsAds, Array( 'class' => "{$ins} wCountAds" ))?>
			</div>
			<?=$form->dropDownListRow( $model, 'typeBorder', $typesBorder, Array( 'class' => "{$ins} wTypeBorder" ))?>
			<?=$form->colorFieldRow( $model, 'colorBorder', Array( 'class' => "{$ins} wColorBorder" ))?>
			<label for="<?=$model->resolveID( 'roundedShapeBlocks' )?>">
				<?=$form->checkBox( $model, 'roundedShapeBlocks', Array( 'class' => "{$ins} wRoundedShapeBlocks" ))?>
				<span class="lbl">
					<?=$model->getAttributeLabel( 'roundedShapeBlocks' )?>
				</span>
			</label>
			<?=$form->colorFieldRow( $model, 'colorHeader', Array( 'class' => "{$ins} wColorHeader" ))?>
			<?=$form->colorFieldRow( $model, 'colorHoverHeader', Array( 'class' => "{$ins} wColorHoverHeader" ))?>
			<?=$form->colorFieldRow( $model, 'colorBG', Array( 'class' => "{$ins} wColorBG" ))?>
			<?=$form->colorFieldRow( $model, 'colorText', Array( 'class' => "{$ins} wColorText" ))?>
			<?=$form->colorFieldRow( $model, 'colorURL', Array( 'class' => "{$ins} wColorURL" ))?>
			<?=$form->colorFieldRow( $model, 'colorHoverURL', Array( 'class' => "{$ins} wColorHoverURL" ))?>
			<?=$form->dropDownListRow( $model, 'font', $fonts, Array( 'class' => "{$ins} wFont" ))?>
			<?=$form->dropDownListRow( $model, 'sizeHeader', $sizesFont, Array( 'class' => "{$ins} wSizeHeader" ))?>
			<?=$form->dropDownListRow( $model, 'sizeText', $sizesFont, Array( 'class' => "{$ins} wSizeText" ))?>
			
			<label for="<?=$model->resolveID( 'clearStats' )?>">
				<?=$form->checkBox( $model, 'clearStats', Array( 'class' => "{$ins} wClearStats" ))?>
				<span class="lbl">
					<?=$model->getAttributeLabel( 'clearStats' )?>
				</span>
			</label>
			
			<div class="clear"></div>
			<a href="#" class="<?=$ins?> wReloadPreview"><?=Yii::t( $NSi18n, 'Reload preview' )?></a>
			<div class="iControlBlock i01">
				<?
					$this->widget( 'bootstrap.widgets.TbButton', Array( 
						'buttonType' => 'submit', 
						'type' => 'success',
						'label' => Yii::t( $NSi18n, 'Done!' ),
						'htmlOptions' => Array(
							'class' => "pull-right {$ins} wSubmit",
						),
					));
				?>
				<?
					$this->widget( 'bootstrap.widgets.TbButton', Array( 
						'type' => 'danger',
						'label' => Yii::t( $NSi18n, 'Delete' ),
						'htmlOptions' => Array(
							'class' => "pull-left {$ins} wDelete",
						),
					));
				?>
			</div>
			<div class="clear"></div>
		<?$this->endWidget()?>
	</div>
	<div class="iClear"></div>
	<div class="<?=$ins?> wBlockPreview">
		<div class="progress progress-info progress-striped active">
			<div class="bar" style="width: 100%;"></div>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminAdvertisementZoneFormWidget = wAdminAdvertisementZoneFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminAdvertisementZoneFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>