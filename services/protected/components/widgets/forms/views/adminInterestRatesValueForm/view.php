<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminInterestRatesValueFormWidget.js" );
	Yii::app()->clientScript->registerCss('dataPickerCss', '
		.wmodDate, .wfutDate{
			position: relative;
			z-index: 10;
		}
	');
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
		'lNew' => 'New value',
		'lEdit' => 'Edit value'
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">
	
	<section class="section_offset">
		<h3 class="<?=$ins?> wTitle"><?=$addLabel?></h3>
		<hr class="separator2">
	</section>
	<div class="section_offset">
		<div class="form_add_in_table">
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
			
				<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
				<?=$form->hiddenField( $model, 'idRate', Array( 'class' => "{$ins} widRate" ))?>
				
				<label for="<?=$model->resolveID( 'modDate' )?>"><?=$model->getAttributeLabel( 'modDate' )?></label>
				<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker',array(
						'name'=>'AdminInterestRatesValueFormModel[modDate]',
						'htmlOptions'=>array(
							'class' => "{$ins} wmodDate"
						),
						'options' => array(
							'dateFormat'=>'yy-mm-dd',
						),
					));			
				?>
				
				<label for="<?=$model->resolveID( 'futDate' )?>"><?=$model->getAttributeLabel( 'futDate' )?></label>
				<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker',array(
						'name'=>'AdminInterestRatesValueFormModel[futDate]',
						'htmlOptions'=>array(
							'class' => "{$ins} wfutDate"
						),
						'options' => array(
							'dateFormat'=>'yy-mm-dd',
						),
					));			
				?>
				

				<?=$form->textFieldRow( $model, 'value', Array( 'class' => "{$ins} wvalue" ) )?>
				
			
				<br />
				<br />
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?> langTabCats" id="tab<?=$title?>" data-langId="<?=$language->id?>">
							<?php
								foreach( $model->textFields as $field => $data ){
									echo $form->{$data['type']}( $model, "{$field}[{$language->id}]", Array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}", 'disabled' => $data['disabled'] ));
								}	
							?>
							
						</div>
					<?}?>
				</div>
				

				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'type' => 'danger',
							'label' => Yii::t( $NSi18n, 'Delete' ),
							'htmlOptions' => Array(
								'class' => "pull-left {$ins} wDelete",
							),
						));
					?>
				</div>
				<div class="clear"></div>
			<?$this->endWidget()?>


		</div>
	</div>

</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = true;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonFormWidget = wAdminInterestRatesValueFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl($submitItemRoute)?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl($deleteItemRoute)?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl($loadItemRoute)?>',
			errorClass: 'inpError',
			formModelName: '<?=$formModelName?>',
			modelName: '<?=$modelName?>',
		});
	}( window.jQuery );
</script>