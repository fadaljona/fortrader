<?php
	$cs=Yii::app()->getClientScript();

	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js');
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');

	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.forms').'/wCalendarGroupSubscriptionFormWidget.js' ));


	$cs->registerScript('jsCalendarGroupSlideToggleSubscribeBlock', '
		!function( $ ) {
			$(".calendarGroupSubscribeWidget .colorInformer").click(function(){ $(this).parent(".section_offset").find(".box2").slideToggle("slow"); });
		}( window.jQuery );
	', CClientScript::POS_END);

	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();

	$jsls = Array(
		'lSubscribe' => 'Subscribe',
		'lUpdate' => 'Update',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
            
?>

<section class="section_offset position-relative spinner-margin paddingBottom0 calendarGroupSubscribeWidget <?=$ins?>">
	<h4 class="title7 colorInformer"><?=Yii::t($NSi18n, 'Subscribe to group of events')?></h4>
	<div class="box2" style="display:none">
		<?php $form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal', 'htmlOptions' => array( 'class' => 'regestration clearfix' ) ));?>
			<?php echo $form->hiddenField( $model, 'timeBeforeEvent', Array( 'class' => "{$ins} wtimeBeforeEvent" )); ?>
			<?php echo $form->hiddenField( $model, 'countriesInp', Array( 'class' => "{$ins} wcountries" )); ?>
			<?php echo $form->hiddenField( $model, 'serious', Array( 'class' => "{$ins} wserious" )); ?>

			<div class="country_top clearfix">
				<h5 class="country_title"><?=Yii::t($NSi18n, 'Select countries')?></h5>
				<div class="country_btn_box clearfix">
					<button class="country_btn"><?=Yii::t($NSi18n, 'Select all / Reset')?></button>
				</div>
			</div>
			<div class="country_content clearfix paddingBottom0">

			<?php

				$countries = CountryModel::getAllExist();
				$selectedCountries = false;
				if( $model->countriesInp ) $selectedCountries = explode(',', $model->countriesInp);

				$countriesCount = count( $countries );
				if( $countriesCount % 3 != 0 ) $countriesCount += 3;
				$countForColumn = ($countriesCount - $countriesCount % 3) / 3;
				$i=1;
				foreach( $countries as  $country ){
					if( $i % $countForColumn == 1 ) echo '<div class="country_col">';
						
						$checked = '';
						if( $selectedCountries && in_array( $country->alias, $selectedCountries ) ) $checked = ' checked ';

						echo '<input type="checkbox" id="country'.$i.'" name="country_filters[]" value="'.$country->alias.'" '.$checked.'>';
						echo '<label for="country'.$i.'" class="square_input"><i></i>';
							echo '<span class="checkbox_img"><img src="'.$country->img16.'" alt="'.$country->name.'"></span>' . $country->name;
						echo '</label>';

					if( $i % $countForColumn == 0 ) echo '</div>';
					$i++;
				}
				if( $i % $countForColumn != 1 ) echo '</div>';
			?>
			</div>

			<h4><?=Yii::t($NSi18n, 'Select event Importance')?></h4>

			<?php
				$selectedSeriuos = false;
				if( $model->serious ) $selectedSeriuos = explode(',', $model->serious);
			?>

			<ul class="informer-calendar__event clearfix impactTabs">
				<li class="calendar__event-high <?php if( $selectedSeriuos && in_array( 3, $selectedSeriuos ) ) echo 'active'; ?>" data-val="3">
					<?=Yii::t( '*', ucfirst( CalendarEvent2ValueModel::getModelTitleSerious( 3 ) ))?>
				</li>
				<li class="calendar__event-medium <?php if( $selectedSeriuos && in_array( 2, $selectedSeriuos ) ) echo 'active'; ?>" data-val="2">
					<?=Yii::t( '*', ucfirst( CalendarEvent2ValueModel::getModelTitleSerious( 2 ) ))?>
				</li>
				<li class="calendar__event-low <?php if( $selectedSeriuos && in_array( 1, $selectedSeriuos ) ) echo 'active'; ?>" data-val="1">
					<?=Yii::t( '*', ucfirst( CalendarEvent2ValueModel::getModelTitleSerious( 1 ) ))?>
				</li>
			</ul>

			<?php
				$hoursDisabled = $minsDisabled = $minsChecked = $hoursChecked = '';
				if( $model->idUser && $model->timeBeforeEvent ){
					if( $model->timeBeforeEvent % 3600 == 0 ){
						$minsVal = 30;
						$hoursVal = $model->timeBeforeEvent / 3600;
						$hoursChecked = 'checked';
						$minsDisabled  = 'disabled';
					}else{
						$minsVal = $model->timeBeforeEvent / 60;
						$hoursVal = 1;
						$hoursDisabled = 'disabled';
						$minsChecked = 'checked';
					}
				}else{
					$unsubscribeClass = ' hide ';
					$hoursDisabled = 'disabled';
					$minsChecked = 'checked';
					$minsVal = 30;
					$hoursVal = 1;
				}
			?>

			<div class="informer-calendar__time ">
				<label for="time_minutes" class="informer-calendar__time-item">
					<input type="radio" id="time_minutes" name="calendar_time" <?=$minsChecked?> class="informer-calendar__time-radio">
					<input type="text" maxlength="2" value="<?=$minsVal?>" class="informer-calendar__time-input" <?=$minsDisabled?>>
					<span class="informer-calendar__time-descr"><?=Yii::t($NSi18n, 'Minutes to start')?></span>
				</label>
				<label for="time_hourse" class="informer-calendar__time-item">
					<input type="radio" id="time_hourse" name="calendar_time" <?=$hoursChecked?> class="informer-calendar__time-radio">
					<input type="text" maxlength="2" value="<?=$hoursVal?>" class="informer-calendar__time-input" <?=$hoursDisabled?>>
					<span class="informer-calendar__time-descr"><?=Yii::t($NSi18n, 'Hours to start')?></span>
				</label>
			</div>

			<div class="subscribeBtns">
				<input class="regestration_btn wSubscribe  "  value="<?=$model->timeBeforeEvent ? $jsls['lUpdate'] : $jsls['lSubscribe'] ?>" type="button">
				<input class="regestration_btn wUnsubscribe <?=!$model->timeBeforeEvent ? 'hide' : '' ?>" value="<?=Yii::t($NSi18n, 'Unsubscribe')?>" type="button">
			</div>

		<?php $this->endWidget(); ?>

	</div>
	<?php if( !Yii::app()->user->id ){ ?><div data-modal="#logIn" class="disableBlock arcticmodal"></div><?php }?>
</section>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ls = <?=json_encode( $jsls )?>;

		ns.wCalendarGroupSubscriptionFormWidget = wCalendarGroupSubscriptionFormWidgetOpen({
			selector: nsText+' .<?=$ins?>',
			subscribeUrl: '<?=Yii::app()->createUrl('calendarEvent/ajaxSubscribeGroupEvent')?>',
			ls: ls,
			unSubscribeUrl: '<?=Yii::app()->createUrl('calendarEvent/ajaxUnSubscribeGroupEvent')?>',
		});

		var $informerCalendarTimeRadio = $(".informer-calendar__time-radio");
		if( $informerCalendarTimeRadio.length ){ $informerCalendarTimeRadio.styler(); }
		$informerCalendarTimeRadio.change(function(){
			$(this).closest(".informer-calendar__time").find("input[type=\'text\']").attr({"disabled": true});
			$(this).closest(".informer-calendar__time-item").find("input[type=\'text\']").attr({"disabled": false});
		});
	}( window.jQuery );

</script>