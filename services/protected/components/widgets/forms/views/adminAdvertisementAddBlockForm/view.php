<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = 'Add block';
	
?>
<div class="wAdminAdvertisementAddBlockFormWidget">
	<div class="well iFormWidget i01 i02">
		<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase')?>
			<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
			<?=$form->textFieldRow( $model, 'ip', Array( 'class' => "span12 {$ins} wIP" ))?>
			<?=$form->textAreaRow( $model, 'agent', Array( 'class' => "span12 {$ins} wAgent", 'rows' => 5 ))?>
			<div class="iClear"></div>
			<div class="iControlBlock i01">
				<?
					$this->widget( 'bootstrap.widgets.TbButton', Array( 
						'buttonType' => 'submit', 
						'type' => 'success',
						'label' => Yii::t( $NSi18n, 'Done!' ),
						'htmlOptions' => Array(
							'class' => "pull-right {$ins} wSubmit",
						),
					));
				?>
				<?
					$this->widget( 'bootstrap.widgets.TbButton', Array( 
						'type' => 'danger',
						'label' => Yii::t( $NSi18n, 'Cancel' ),
						'htmlOptions' => Array(
							'class' => "pull-left {$ins} wCancel",
						),
					));
				?>
			</div>
			<div class="iClear"></div>
		<?$this->endWidget()?>
	</div>
</div>
