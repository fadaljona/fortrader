<?php $commonHtmlOptions = array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__input' ); ?>
<div class="inside-form__title"><?=Yii::t( $NSi18n, 'Trader' )?></div>
<div class="inside-form__column">
<?
	echo $form->textFieldInpWithToolTip( $model, 'profile_trader_market', Yii::t( $NSi18n, 'Where do you trade? FOREX, CDF, Metal, MMVB, etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_trader_tradingPlatforms', Yii::t( $NSi18n, 'You trading platform  MetaTrader4,MetaTrader5, etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_trader_brokers', Yii::t( $NSi18n, 'What companies do you have accounts?  Alpari, FxPro, Exness, ACFX etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_trader_financeInstruments', Yii::t( $NSi18n, 'Your financial instrument: EURUSD, GOLD, GAZP, etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_trader_tradingExperience', Yii::t( $NSi18n, 'How long do you trade? 1 year, 5 years, 10 years, etc.' ), $commonHtmlOptions );
?>

</div>
<div class="inside-form__column">
<?php
	echo $form->textFieldInpWithToolTip( $model, 'profile_trader_analyseMethod', Yii::t( $NSi18n, 'You analyses type. Tehnical, Fundamental, EA, Genetic, etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_trader_tradingType', Yii::t( $NSi18n, 'You trade style. Scalping, Interday, medium-term, long-term.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_trader_useEA', Yii::t( $NSi18n, 'Do you use EA for your trade?' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_trader_commercialProduct', Yii::t( $NSi18n, 'Do you use commercial product for your trade? What?' ), $commonHtmlOptions );
	echo $form->textAreaInpWithToolTip( $model, 'profile_trader_aboutTrading', Yii::t( $NSi18n, 'Please write a couple of lines about your trading.' ), array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__textarea' ) );
?>
</div>