<?
	$sex = Array( 'Male', 'Female' );
	if( !strlen( $model->sex )) array_unshift( $sex, '' );
	$sex = array_combine( $sex, $sex );
	foreach( $sex as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$commonHtmlOptions = array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__input' );
	$commonListInpHtmlOptions = array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__select' );
?>
<div class="inside-form__user">
	<div class="inside-form__title"><?=Yii::t( $NSi18n, 'User' )?></div>
	<?=$form->textFieldInp( $model, 'user_login', $commonHtmlOptions )?>
	<?=$form->textFieldInp( $model, 'display_name', $commonHtmlOptions )?>
	<?=$form->textFieldInp( $model, 'user_email', $commonHtmlOptions )?>
	<?=$form->dropDownListInp( $model, 'sex', $sex, $commonListInpHtmlOptions )?>
	<?=$form->dropDownListInp( $model, 'language', $languages,  array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__select wSelectLanguage' ))?>
	<?=$form->textFieldInp( $model, 'city', $commonHtmlOptions )?>
	
	<?=$form->dropDownListCountries( $model, 'idCountry', $countries, array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__select inside-form__select-lang' ) )?>

	<? $model->utc = UserProfileModel::getTimezone(); ?>
	<?=$form->dropDownListInp( $model, 'utc', array(
		'-12' => '(UTC -12:00)',
		'-11' => '(UTC -11:00)',
		'-10' => '(UTC -10:00)',
		'-9'  => '(UTC -9:00)',
		'-8'  => '(UTC -8:00)',
		'-7'  => '(UTC -7:00)',
		'-6'  => '(UTC -6:00)',
		'-5'  => '(UTC -5:00)',
		'-4'  => '(UTC -4:00)',
		'-3'  => '(UTC -3:00)',
		'-2'  => '(UTC -2:00)',
		'-1'  => '(UTC -1:00)',
		'0'   => '(UTC +0:00)',
		'1'   => '(UTC +1:00)',
		'2'   => '(UTC +2:00)',
		'3'   => '(UTC +3:00)',
		'4'   => '(UTC +4:00)',
		'5'   => '(UTC +5:00)',
		'6'   => '(UTC +6:00)',
		'7'   => '(UTC +7:00)',
		'8'   => '(UTC +8:00)',
		'9'   => '(UTC +9:00)',
		'10'  => '(UTC +10:00)',
		'11'  => '(UTC +11:00)',
		'12'  => '(UTC +12:00)',
	), array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__select', 'onchange'=>'time.settz($(this).val(),1,false);',  ) )?>

	<?=$form->textAreaInp( $model, 'description', array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__textarea' ) )?>
	
</div>
<div class="inside-form__contact">
	<div class="inside-form__title"><?=Yii::t( $NSi18n, 'Contact' )?></div>
	
	<?=$form->textFieldInp( $model, 'firstName', $commonHtmlOptions )?>
	<?=$form->textFieldInp( $model, 'lastName', $commonHtmlOptions )?>
	<?=$form->textFieldInp( $model, 'phone', $commonHtmlOptions )?>
	<?=$form->textFieldInp( $model, 'user_url', $commonHtmlOptions )?>
	<?=$form->textFieldInp( $model, 'icq', $commonHtmlOptions )?>
	<?=$form->textFieldInp( $model, 'skype', $commonHtmlOptions )?>
	
</div>