<?php $commonHtmlOptions = array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__input' ); ?>
<div class="inside-form__title"><?=Yii::t( $NSi18n, 'Programmer' )?></div>
<div class="inside-form__column">
<?
	echo $form->textFieldInpWithToolTip( $model, 'profile_programmer_availability', Yii::t( $NSi18n, 'Do you provide service of programming now? Yes or no.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_programmer_programmingExperience', Yii::t( $NSi18n, 'How long do you program? 1 month, 6  month,  1 year, 5 years, etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_programmer_programmingLangauge', Yii::t( $NSi18n, 'What programming languages do you know? Mql4, mql5, c++, c#, Pascal, etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_programmer_typeOfPrograms', Yii::t( $NSi18n, 'What kind of programs can you make? EA, indicators, scripts, DLL etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_programmer_hourlyRate', Yii::t( $NSi18n, 'What is your hourly rate? 1$, 5$, 10$, 20$, 50$, 100$ etc.' ), $commonHtmlOptions );
?>

</div>
<div class="inside-form__column">
<?php
	echo $form->textFieldInpWithToolTip( $model, 'profile_programmer_minimalOrder', Yii::t( $NSi18n, 'What is your minimum price for order? No limits, 10$, 20$, 50$, 100$, etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_programmer_mql4Profile', Yii::t( $NSi18n, 'Insert  link to your profile on mql4 site, if you have it.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_programmer_mql5Profile', Yii::t( $NSi18n, 'Insert  link to your profile on mql5 site, if you have it.' ), $commonHtmlOptions );
	echo $form->textAreaInpWithToolTip( $model, 'profile_programmer_additionally', Yii::t( $NSi18n, 'If you want, you can add a couple of lines about your service.' ), array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__textarea' ) );
?>
</div>