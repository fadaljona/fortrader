<?php $commonHtmlOptions = array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__input' ); ?>
<div class="inside-form__title"><?=Yii::t( $NSi18n, 'Investor' )?></div>
<div class="inside-form__column">
<?
	echo $form->textFieldInpWithToolTip( $model, 'profile_investor_availability', Yii::t( $NSi18n, 'Do you do investment now? Yes or no.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_investor_investmentAmount', Yii::t( $NSi18n, 'How much money you want to invest? 100$, 500$, 1000$, 10 000$ etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_investor_investmentPeriod', Yii::t( $NSi18n, 'How long do you prefer to invest?  1 month, 3 month, 6 month, 1 year, 5 years, etc.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_investor_expectedAnnualRateOfReturn', Yii::t( $NSi18n, 'How many profit (in percentage) do you want in year for start of investment? 10%, 50%, 100%, 300%, etc.' ), $commonHtmlOptions );
?>

</div>
<div class="inside-form__column">
<?php
	echo $form->textFieldInpWithToolTip( $model, 'profile_investor_maximumRiskOfInvestment', Yii::t( $NSi18n, 'How much of your investment assets can lose trader? 10%, 30%, 60%, 100% ect.' ), $commonHtmlOptions );
	echo $form->textFieldInpWithToolTip( $model, 'profile_investor_agreement', Yii::t( $NSi18n, 'What type of trust management agreement do you prefer? Public offer, official agreement, verbal agreement etc.' ), $commonHtmlOptions );
	echo $form->textAreaInpWithToolTip( $model, 'profile_investor_additionally', Yii::t( $NSi18n, 'If you want, you can add a couple of lines about yourself.' ), array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__textarea' ) );
?>
</div>