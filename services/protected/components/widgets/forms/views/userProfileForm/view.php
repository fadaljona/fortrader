<?
	$cs=Yii::app()->getClientScript();
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.forms').'/wUserProfileFormWidget.js' ) );
	
	$class = $this->getCleanClassName();
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lReseted' => 'Reseted',
		'lError' => 'Error!',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	$languageCookieVarName = Yii::App()->params[ 'languageCookieVarName' ];
?>
<div class="<?=$ins?> spinner-margin position-relative">
	<?$form = $this->beginWidget('widgets.base.UserProfileFormWidgetBase', Array( 'htmlOptions' => array('class' => 'profile-inside__form clearfix') ))?>
		<?=$form->hiddenField( $model, 'ID' )?>
		<div class="inside-menu__sub clearfix mainSettingsProfileTabs">
			<a href="#" class="inside-menu__main inside-menu_sub-active"><i class="ic_main"></i><?=Yii::t( $NSi18n, 'Common' )?></a>
			<a href="#" class="inside-menu__trader"><i class="ic_trader"></i><?=Yii::t( $NSi18n, 'Trader' )?></a>
			<a href="#" class="inside-menu__head"><i class="ic_head"></i><?=Yii::t( $NSi18n, 'Manager' )?></a>
			<a href="#" class="inside-menu__invest"><i class="ic_invest"></i><?=Yii::t( $NSi18n, 'Investor' )?></a>
			<a href="#" class="inside-menu__proger"><i class="ic_proger"></i><?=Yii::t( $NSi18n, 'Programmer' )?></a>
		</div>
		<div class="settingsContentTabs">
			<div class="clearfix">
				<?require __DIR__.'/_main.php';?>
			</div>
			<div class="clearfix">
				<?require __DIR__.'/_trader.php';?>
			</div>
			<div class="clearfix">
				<?require __DIR__.'/_manager.php';?>
			</div>
			<div class="clearfix">
				<?require __DIR__.'/_investor.php';?>
			</div>
			<div class="clearfix">
				<?require __DIR__.'/_programmer.php';?>
			</div>
		</div>
		
		<div class="inside-form__buttons">
			<button class="inside-form__btn-blue wSubmit"><?=Yii::t( $NSi18n, 'Save' )?></button>
		</div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var itCurrentModel = <?=CommonLib::boolToStr($itCurrentModel)?>;
		var languageCookieVarName = "<?=$languageCookieVarName?>";
				
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wUserProfileFormWidget = wUserProfileFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			itCurrentModel: itCurrentModel,
			cookieVarName: languageCookieVarName,
			ajaxSubmitURL: '<?=Yii::app()->createUrl('user/ajaxUpdateProfile')?>',
			ls: ls,
		});
	}( window.jQuery );
</script>