<?php $commonHtmlOptions = array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__input' ); ?>
<div class="inside-form__title"><?=Yii::t( $NSi18n, 'Manager' )?></div>
<div class="inside-form__column">
<?
	echo $form->textFieldInpWithToolTip( 
		$model, 
		'profile_manager_availability', 
		Yii::t( $NSi18n, 'Do you provide service now? Yes or no.' ), 
		$commonHtmlOptions 
	);
	echo $form->textFieldInpWithToolTip( 
		$model, 
		'profile_manager_trustManagementType',
		Yii::t( $NSi18n, 'What is your trust management type? PAMM, trade on investor account, reception investments into the trader account, etc.' ), 
		$commonHtmlOptions 
	);
	echo $form->textFieldInpWithToolTip( 
		$model, 
		'profile_manager_agreement', 
		Yii::t( $NSi18n, 'What type of trust management agreement do you use? Public offer, official agreement, verbal agreement etc.' ), 
		$commonHtmlOptions 
	);
	echo $form->textFieldInpWithToolTip( 
		$model, 
		'profile_manager_managementExperience', 
		Yii::t( $NSi18n, 'How long do you provide trust management? Less then 1 year, 1 year, 5 years, 10 years, etc.' ), 
		$commonHtmlOptions 
	);
	
?>

</div>
<div class="inside-form__column">
<?php
	echo $form->textFieldInpWithToolTip( 
		$model, 
		'profile_manager_investmentRisk', 
		Yii::t( $NSi18n, 'What is your maximum loss risk? 10%, 20%, 50%, 100%' ), 
		$commonHtmlOptions 
	);
	echo $form->textFieldInpWithToolTip( 
		$model, 
		'profile_manager_minimumInvestmentAmount', 
		Yii::t( $NSi18n, 'What is minimum share in your trust management? 100$, 500$, 1000$, 10 000$ etc.' ), 
		$commonHtmlOptions 
	);
	echo $form->textAreaInpWithToolTip( 
		$model, 
		'profile_manager_additionally', 
		Yii::t( $NSi18n, 'If you want, you can add a couple of lines about your services.' ), 
		array( 'labelHtmlOptions' => array('class' => 'inside-form__label' ), 'class' => 'inside-form__textarea' ) 
	);
?>
</div>