<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminBrokerFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New broker' : 'Editor broker';
	
	$jsls = Array(
		'lNew' => 'New broker',
		'lEdit' => 'Editor broker',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$years = range( 1971, date( "Y" ) );
	$years = array_combine( $years, $years );
?>
<div class="well pull-left iFormWidget i01 wAdminBrokerFormWidget wAdminFullWidthForm">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<?=$form->hiddenField( $model, 'type', Array( 'class' => "{$ins} wType" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<li class="active"><a href="#tabInfo" data-toggle="tab"><?=Yii::t( $NSi18n, 'Basic information' )?></a></li>
			<li><a href="#tabConditions" data-toggle="tab"><?=Yii::t( $NSi18n, 'Trading conditions' )?></a></li>
			<li><a href="#tabLinks" data-toggle="tab"><?=Yii::t( $NSi18n, 'Links' )?></a></li>
			<?php if( $model->type == 'binary' ){?><li><a href="#binarySettings" data-toggle="tab"><?=Yii::t( $NSi18n, 'Binary options settings' )?></a></li><?php }?>
		</ul>
		
		<div class="tab-content" style="position:static">
			<div class="tab-pane active" id="tabInfo">
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
							<?=$form->textFieldRow( $model, "officialNames[{$language->id}]", Array( 'class' => "{$ins} wOfficialName w{$language->id}" ))?>
							<?=$form->textFieldRow( $model, "brandNames[{$language->id}]", Array( 'class' => "{$ins} wBrandName w{$language->id}" ))?>
							<?=$form->textFieldRow( $model, "shortNames[{$language->id}]", Array( 'class' => "{$ins} wShortName w{$language->id}" ))?>
							
							<?=$form->textAreaRow( $model, "metaTitles[{$language->id}]", Array( 'class' => "{$ins} wMetaTitle w{$language->id} wFullWidth" ))?>
							<?=$form->textAreaRow( $model, "metaDescs[{$language->id}]", Array( 'class' => "{$ins} wMetaDesc w{$language->id} wFullWidth" ))?>
							<?=$form->textAreaRow( $model, "metaKeyss[{$language->id}]", Array( 'class' => "{$ins} wMetaKeys w{$language->id} wFullWidth" ))?>
							
							<?=$form->textAreaRow( $model, "shortDesc[{$language->id}]", Array( 'class' => "{$ins} wshortDesc w{$language->id} wFullWidth" ))?>
							<?=$form->textAreaRow( $model, "fullDesc[{$language->id}]", Array( 'class' => "{$ins} wfullDesc w{$language->id} wFullWidth" ))?>
							
						</div>
					<?}?>
				</div>
				<br>
				
				<?=$form->textFieldRow( $model, 'slug', Array( 'class' => "{$ins} wSlug" ))?>
				
				<label for="<?=$model->resolveID( 'showOnHome' )?>">
					<?=$form->checkBox( $model, 'showOnHome', Array( 'class' => "{$ins} wShowOnHome" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'showOnHome' )?>
					</span>
				</label>
				<?=$form->fileFieldRow( $model, 'homeImage', Array( 'class' => "{$ins} wHomeImage" ))?>
				<img class="<?=$ins?> wHomeImageToAdmin hide" src="" style="height:50px;" />
				
				<br><br>
				
				<label for="<?=$model->resolveID( 'showInRating' )?>">
					<?=$form->checkBox( $model, 'showInRating', Array( 'class' => "{$ins} wShowInRating" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'showInRating' )?>
					</span>
                </label>
                
                <label for="<?=$model->resolveID( 'toCryptoCurrencies' )?>">
					<?=$form->checkBox( $model, 'toCryptoCurrencies', Array( 'class' => "{$ins} wtoCryptoCurrencies" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel('toCryptoCurrencies')?>
					</span>
				</label>
				
				<?=$form->fileFieldRow( $model, 'nameImage', Array( 'class' => "{$ins} wNameImage" ))?>
				<img class="<?=$ins?> wNameImageToAdmin hide" src="" style="height:50px;" />
				<br><br>
				<?=$form->dropDownListRow( $model, 'year', $years, Array( 'class' => "{$ins} wYear" ))?>
				<?=$form->dropDownListRow( $model, 'idCountry', $countries, Array( 'class' => "{$ins} wIDCountry" ))?>
				<?=$form->dropDownListRow( $model, 'idSupportUser', $idSupportUsers, Array( 'class' => "{$ins} wIDSupportUsers" ))?>
				
				<label><?=Yii::t( $NSi18n, 'Regulators' )?></label>
				<?=$form->checkBoxList( $model, 'idsRegulators', $regulators, Array( 'class' => "{$ins} wIDsRegulators" ))?>
			</div>
			<div class="tab-pane" id="tabConditions">
				<?=$form->textFieldRow( $model, 'minDeposit', Array( 'class' => "{$ins} wMinDeposit" ))?>
				<?=$form->textFieldRow( $model, 'maxLeverage', Array( 'class' => "{$ins} wMaxLeverage" ))?>
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab2<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab2<?=$title?>">
							<?=$form->textFieldRow( $model, "AMs[{$language->id}]", Array( 'class' => "{$ins} wAM w{$language->id}" ))?>
							<?php if( $model->type == 'broker' ){
								echo $form->textFieldRow( $model, "demoAccounts[{$language->id}]", Array( 'class' => "{$ins} wDemoAccount w{$language->id}" ));
							}?>
						</div>
					<?}?>
				</div>
				<br>
				<div class="accordion" id="accordionConditions">
					<div class="accordion-group">
						<div class="accordion-heading">
							<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordionConditions" data-target="#collapseTradePlatforms">
								<b><?=Yii::t( $NSi18n, 'Trade platforms' )?></b>
							</div>
						</div>
						<div id="collapseTradePlatforms" class="accordion-body collapse in">
							<div style="padding-left:10px;">
								<?=$form->checkBoxList( $model, 'idsTradePlatforms', $tradePlatforms, Array( 'class' => "{$ins} wIDsTradePlatforms" ))?>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordionConditions" data-target="#collapseIO">
								<b><?=Yii::t( $NSi18n, 'Input/Output' )?></b>
							</div>
						</div>
						<div id="collapseIO" class="accordion-body collapse" style="max-height:250px; overflow-y:auto">
							<div style="padding-left:10px;">
								<?=$form->checkBoxList( $model, 'idsIOs', $IOs, Array( 'class' => "{$ins} wIDsIOs" ))?>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordionConditions" data-target="#collapseInstruments">
								<b><?=Yii::t( $NSi18n, 'Instruments' )?></b>
							</div>
						</div>
						<div id="collapseInstruments" class="accordion-body collapse">
							<div style="padding-left:10px;">
								<?=$form->checkBoxList( $model, 'idsInstruments', $instruments, Array( 'class' => "{$ins} wIDsInstruments" ))?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="tabLinks">
				<?=$form->textFieldRow( $model, 'conditions', Array( 'class' => "{$ins} wConditions" ))?>
				<?=$form->textFieldRow( $model, 'terms', Array( 'class' => "{$ins} wTerms" ))?>
				<?=$form->textFieldRow( $model, 'site', Array( 'class' => "{$ins} wSite" ))?>
				<?=$form->textFieldRow( $model, 'openAccount', Array( 'class' => "{$ins} wOpenAccount" ))?>
				<?=$form->textFieldRow( $model, 'openDemoAccount', Array( 'class' => "{$ins} wOpenDemoAccount" ))?>
			</div>
			
			<?php if( $model->type == 'binary' ){?>
			<div class="tab-pane" id="binarySettings">
			
				<?=$form->textFieldRow( $model, 'contract', Array( 'class' => "{$ins} wContract" ))?>
				
				<label for="<?=$model->resolveID( 'earlyClosure' )?>">
					<?=$form->checkBox( $model, 'earlyClosure', Array( 'class' => "{$ins} wEarlyClosure" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'earlyClosure' )?>
					</span>
				</label>
				
				<label for="<?=$model->resolveID( 'tradeOnWeekends' )?>">
					<?=$form->checkBox( $model, 'tradeOnWeekends', Array( 'class' => "{$ins} wTradeOnWeekends" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'tradeOnWeekends' )?>
					</span>
				</label>
				
				<label for="<?=$model->resolveID( 'binnaryDemoAccount' )?>">
					<?=$form->checkBox( $model, 'binnaryDemoAccount', Array( 'class' => "{$ins} wBinnaryDemoAccount" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'binnaryDemoAccount' )?>
					</span>
				</label>
				
				<br />
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab3<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab3<?=$title?>">
							<?=$form->textFieldRow( $model, "maxProfits[{$language->id}]", Array( 'class' => "{$ins} wMaxProfit w{$language->id}" ))?>
						</div>
					<?}?>
				</div>
				
				<div class="accordion" id="accordionBinaryConditions">
					<div class="accordion-group">
						<div class="accordion-heading">
							<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordionBinaryConditions" data-target="#collapseBinaryassets">
								<b><?=Yii::t( $NSi18n, 'Bynary Assets' )?></b>
							</div>
						</div>
						<div id="collapseBinaryassets" class="accordion-body collapse in">
							<div style="padding-left:10px;">
								<?=$form->checkBoxList( $model, 'idsBinaryAssets', $binaryassets, Array( 'class' => "{$ins} wIDsBinaryAssets" ))?>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<div class="accordion-toggle" data-toggle="collapse" data-parent="#accordionBinaryConditions" data-target="#collapseBinarytypes">
								<b><?=Yii::t( $NSi18n, 'Binary types' )?></b>
							</div>
						</div>
						<div id="collapseBinarytypes" class="accordion-body collapse" style="max-height:500px; overflow-y:auto">
							<div style="padding-left:10px;">
								<?=$form->checkBoxList( $model, 'idsBinaryTypes', $binaryTypes, Array( 'class' => "{$ins} wIDsBinaryTypes" ))?>
							</div>
						</div>
					</div>
				
				</div>
				
			</div>
			<?php }?>
			
		</div>
				
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
		<iframe name="iframeForBroker" id="iframeForBroker" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminBrokerFormWidget = wAdminBrokerFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminBrokerFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>
