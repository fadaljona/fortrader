<?
Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminQuotesCategoryFormWidget.js" );

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

$title =Yii::t( "quotesWidget",$model->getAR()->isNewRecord ? 'Новый алиас' : 'Редактирование алиса');

$jsls = Array(
	'lNew' => Yii::t( "quotesWidget",'New category'),
	'lEdit' => Yii::t( "quotesWidget",'Edit category'),
	'lDeleting' => Yii::t( "quotesWidget",'Deleting...'),
	'lDeleted' => Yii::t( "quotesWidget",'Deleted'),
	'lSaving' => Yii::t( "quotesWidget",'Saving...'),
	'lSaved' => Yii::t( "quotesWidget",'Saved'),
	'lLoading' => Yii::t( "quotesWidget",'Loading...'),
	'lDeleteConfirm' => Yii::t( "quotesWidget",'Delete?')
);
foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);


?>
<div class="well pull-left iFormWidget i01 wAdminQuotesCategoryFormWidget wAdminQuotesSymbolsFormWidget wAdminCommonSettingsFormWidget wAdminFullWidthForm">

<section class="section_offset">
		<h3 class="<?=$ins?> wTitle"></h3>
		<hr class="separator2">
	</section>
	<div class="section_offset">
		<div class="form_add_in_table">
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
					
				<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
				<?=$form->textFieldRow($model,'slug', Array( 'class' => "{$ins} wslug" )) ?>
				<?=$form->textFieldRow($model,'order', Array( 'class' => "{$ins} worder" )) ?>
				<?=$form->dropDownListRow($model,'parent', $categories, Array( 'class' => "{$ins} wParent" )) ?>
				<?=$form->textFieldRow($model,'ogImageUrl', Array( 'class' => "{$ins} wFullWidth wogImageUrl" )) ?>
				<label for="<?=$model->resolveID( 'is_def' )?>">
					<?=$form->checkBox( $model, 'is_def', Array( 'class' => "{$ins} wis_def" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'is_def' )?>
					</span>
				</label>
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?> langTabCats" id="tab<?=$title?>" data-langId="<?=$language->id?>">
							<?php
								foreach( $model->textFields as $field => $data ){
									echo $form->{$data['type']}( $model, "{$field}[{$language->id}]", Array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}", 'disabled' => $data['disabled'] ));
								}	
							?>
						</div>
					<?}?>
				</div>
				
				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'type' => 'danger',
							'label' => Yii::t( $NSi18n, 'Delete' ),
							'htmlOptions' => Array(
								'class' => "pull-left {$ins} wDelete",
							),
						));
					?>
				</div>
				<div class="clear"></div>
			<?$this->endWidget()?>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;

		var ls = <?=json_encode( $jsls )?>;

		ns.wAdminQuotesCategoryFormWidget = wAdminQuotesCategoryFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminQuotesCategoryFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls
		});
	}( window.jQuery );
</script>
