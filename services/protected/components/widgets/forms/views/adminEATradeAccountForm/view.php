<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminEATradeAccountFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New trade account' : 'Editor trade account';
	
	$jsls = Array(
		'lNew' => 'New trade account',
		'lEdit' => 'Editor trade account',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminEATradeAccountFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<?=$form->dropDownListRow( $model, 'idEA', $EA, Array( 'class' => "{$ins} wIDEA" ))?>
		<?=$form->dropDownListRow( $model, 'idVersion', Array(), Array( 'class' => "{$ins} wIDVersion" ))?>
		<?=$form->dropDownListRow( $model, 'idStatement', Array(), Array( 'class' => "{$ins} wIDStatement" ))?>
		<?=$form->dropDownListRow( $model, 'idTradeAccount', $taradeAccounts, Array( 'class' => "{$ins} wIDTradeAccount" ))?>
		<?=$form->dropDownListRow( $model, 'idEAMonitoringServer', $EAMonitoringServers, Array( 'class' => "{$ins} wIDEAMonitoringServer" ))?>
		<?=$form->dropDownListRow( $model, 'idBroker', $brokers, Array( 'class' => "{$ins} wIDBroker" ))?>
		<?=$form->textFieldRow( $model, 'planeDrow', Array( 'class' => "{$ins} wPlaneDrow" ))?>
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminEATradeAccountFormWidget = wAdminEATradeAccountFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminEATradeAccountFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>