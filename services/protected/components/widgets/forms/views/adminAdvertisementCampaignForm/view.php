<?
	$cs=Yii::app()->getClientScript();
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.forms').'/wAdminAdvertisementCampaignFormWidget.js' ) );

	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	//$title = $model->getAR()->isNewRecord ? 'New campaign' : 'Editor campaign';
	$title = 'New campaign';
	
	$jsls = Array(
		'lNew' => 'New campaign',
		'lEdit' => 'Editor campaign',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$types = Array( 'Text', 'Image', 'Buttons', 'Offers', 'Rating Brokers' );
	$types = array_combine( $types, $types );
	foreach( $types as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$statuses = Array( 'Active', 'Pause', 'Block' );
	$statuses = array_combine( $statuses, $statuses );
	foreach( $statuses as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminAdvertisementCampaignFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<li class="active"><a href="#tabInfo" data-toggle="tab"><?=Yii::t( $NSi18n, 'Information' )?></a></li>
			<li><a href="#tabSettings" data-toggle="tab"><?=Yii::t( $NSi18n, 'Settings' )?></a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="tabInfo">
				<?=$form->textFieldRow( $model, 'name', Array( 'class' => "{$ins} wName" ))?>
				<?=$form->checkBoxListRow( $model, 'type', $types, Array( 'class' => "{$ins} wType" ))?>
				
				<?=$form->textFieldRow( $model, 'begin', Array( 'class' => "{$ins} wBegin" ))?>
				<?=$form->textFieldRow( $model, 'end', Array( 'class' => "{$ins} wEnd" ))?>
				
				<?=$form->textFieldRow( $model, 'weight', Array( 'class' => "{$ins} wWeight" ))?>
				
				<label for="<?=$model->resolveID( 'hideOnMobiles' )?>">
					<?=$form->checkBox( $model, 'hideOnMobiles', Array( 'class' => "{$ins} wHideOnMobiles" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'hideOnMobiles' )?>
					</span>
				</label>
				
				<?if( $mode == 'Admin' ){?>
					<?=$form->textFieldRow( $model, 'clickCost', Array( 'class' => "{$ins} wClickCost" ))?>
					<label for="<?=$model->resolveID( 'marketClickCost' )?>">
						<?=$form->checkBox( $model, 'marketClickCost', Array( 'class' => "{$ins} wMarketClickCost" ))?>
						<span class="lbl">
							<?=$model->getAttributeLabel( 'marketClickCost' )?>
						</span>
					</label>
					<?=$form->dropDownListRow( $model, 'status', $statuses, Array( 'class' => "{$ins} wStatus" ))?>
					<?=$form->dropDownListRow( $model, 'idUser', $users, Array( 'class' => "{$ins} wIDUser" ))?>
					
					<label for="<?=$model->resolveID( 'clearStats' )?>">
						<?=$form->checkBox( $model, 'clearStats', Array( 'class' => "{$ins} wClearStats" ))?>
						<span class="lbl">
							<?=$model->getAttributeLabel( 'clearStats' )?>
						</span>
					</label>
				<?}?>
			</div>
			<div class="tab-pane" id="tabSettings">
				<?=$form->textFieldRow( $model, 'totalLimitShows', Array( 'class' => "{$ins} wTotalLimitShows" ))?>
				<?=$form->textFieldRow( $model, 'dayLimitShows', Array( 'class' => "{$ins} wDayLimitShows" ))?>
				<?=$form->textFieldRow( $model, 'dayLimitClicks', Array( 'class' => "{$ins} wDayLimitClicks" ))?>
				<?=$form->textFieldRow( $model, 'minPopup', Array( 'class' => "{$ins} wMinPopup" ))?>

				<label for="<?=$model->resolveID( 'oneAd' )?>">
					<?=$form->checkBox( $model, 'oneAd', Array( 'class' => "{$ins} wOneAd" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'oneAd' )?>
					</span>
				</label>
				
				<?=$form->textFieldRow( $model, 'startTime', Array( 'class' => "{$ins} wStartTime" ))?>
				<?=$form->textFieldRow( $model, 'endTime', Array( 'class' => "{$ins} wEndTime" ))?>
				
			</div>
		</div>
		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var idDefaultUser = <?=$idDefaultUser?>;
		var adClickCost = <?=SettingsModel::getModel()->common_adClickCost?>;
		var defBegin = "<?=$defBegin?>";
		var defEnd = "<?=$defEnd?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminAdvertisementCampaignFormWidget = wAdminAdvertisementCampaignFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminAdvertisementCampaignFormWidget',
			ajax: ajax,
			hidden: hidden,
			idDefaultUser: idDefaultUser,
			ls: ls,
			adClickCost: adClickCost,
			defBegin: defBegin,
			defEnd: defEnd,
		});
	}( window.jQuery );
</script>