<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wUserAvatarFormWidget.js" );
	
	$class = $this->getCleanClassName();
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lDeleting' => 'Deleting...',
		'lSaved' => 'Saved',
		'lError' => 'Error!',
		'lDeleteConfirm' => 'Delete?',
		'lNoFile' => 'No file ...',
		'lChoose' => 'Choose',
		'lChange' => 'Change',
		'lSet' => 'Set avatar',
		'lChange' => 'Change avatar',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>

<div class="inside-form__avatar clearfix <?=$ins?> spinner-margin position-relative">
	<?$pathImg = $model->avatar ? Yii::app()->baseUrl."/{$model->avatar->middlePath}" : ''?>
	<div class="inside-form__title"><?=Yii::t( $NSi18n, 'Your avatar' )?></div>
	<div class="inside-form__avatar-pr wAvatarHolder"><img src="<?=$pathImg?>" alt=""></div>
	<div class="inside-form__avatar-btns">
		<button class="inside-form__btn-blue wChange"><?=Yii::t( $NSi18n, 'Change avatar' )?></button>
		<?php/*<button class="inside-form__btn-gray wDelete"><?=Yii::t( $NSi18n, 'Delete' )?></button>*/?>
	</div>
	
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data', 'style' => 'display:none;' )))?>
		<?=$form->hiddenField( $model, 'ID' )?>
		<?=$form->fileFieldRow( $model, 'avatar', Array( 'class' => "{$ins} wAvatar" ))?>
		<iframe name="iframeForAvatar" id="iframeForAvatar" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
				
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wUserAvatarFormWidget = wUserAvatarFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			ls: ls,
			idUser: <?=Yii::app()->user->id?>,
			ajaxDeleteURL: '<?=Yii::app()->createUrl('user/ajaxDeleteAvatar')?>',
			ajaxSubmitURL: '<?=Yii::app()->createUrl('user/iframeUploadAvatar')?>',
		});
	}( window.jQuery );
</script>