<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminCommonSettingsFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
?>
<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, 'Editor settings' )?></h3>
		
		<?=$form->textFieldRow( $model, 'inactiveDays', Array( 'class' => "{$ins}" ) )?>
		<br />
		<?php
			$this->widget( 'components.widgets.AdminImageUploadWidget', Array(
				'form' => $form,
				'model' => $model,
				'field' => 'ogImage',
				'ins' => $ins,
				'imgName' => 'admin.monitoring.settings.ogImage',
			));
		?>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' class="active"' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<li<?=$class?>>
					<a href="#tab<?=$title?>" data-toggle="tab">
						<?=$title?>
					</a>
				</li>
			<?}?>
		</ul>
		
		<div class="tab-content">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' active' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
					<?php
						foreach( $model->textFields as $field => $fieldType ){
							echo $form->{$fieldType}( $model, "{$field}[{$language->id}]", Array( 'class' => "{$ins} wFullWidth w{$field} w{$language->id}" ));
						}	
					?>
				</div>
			<?}?>
		</div>
		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Save' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Reset' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wReset",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonSettingsFormWidget = wAdminCommonSettingsFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminCommonSettingsFormWidget',
			ajax: ajax,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl('admin/monitoring/ajaxSettingsUpdate')?>',
		});
	}( window.jQuery );
</script>