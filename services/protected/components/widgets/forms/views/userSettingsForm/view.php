<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wUserSettingsFormWidget.js" );
	
	$class = $this->getCleanClassName();
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lReseted' => 'Reseted',
		'lError' => 'Error!',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	$languageCookieVarName = Yii::App()->params[ 'languageCookieVarName' ];
	
	$commonHtmlOptions = array( 'labelHtmlOptions' => array('class' => 'inside-form__label clearfix' ), 'class' => 'inside-form__checkbox' );
?>

<div class="<?=$ins?> spinner-margin position-relative">
	<?php $form = $this->beginWidget('widgets.base.UserProfileFormWidgetBase');	?>
		<div class="inside-form__settings clearfix">
			<div class="inside-form__title"><?=Yii::t($NSi18n, 'Common')?></div>
			
			<div class="inside-form__label clearfix"><div class="inside-form__checkbox-text form-error userSettingsFormError"></div></div>
			
			
			<?=$form->hiddenField( $model, 'idUser' )?>
			
			<?php/*=$form->checkBoxInp( $model, 'receiveNoticeAnswerMessage', $commonHtmlOptions )?>
			<?=$form->checkBoxInp( $model, 'receiveEmailNoticeAnswerPrivateMessage', $commonHtmlOptions )?>
			<?=$form->checkBoxInp( $model, 'receiveNoticeMessage', $commonHtmlOptions )*/?>
			
			<?=$form->checkBoxInp( $model, 'receiveCommentsOnMyArticles', $commonHtmlOptions )?>
			<?=$form->checkBoxInp( $model, 'receiveCommentsOnMyProfile', $commonHtmlOptions )?>
			<?=$form->checkBoxInp( $model, 'receiveCommentsOnMyMonitoringAccounts', $commonHtmlOptions )?>
			
			<?=$form->checkBoxInp( $model, 'reciveNoticeNewJournal', $commonHtmlOptions )?>
			<?=$form->checkBoxInp( $model, 'reciveEmailNoticeNewJournal', $commonHtmlOptions )?>
			
			<?if( $mailingTypes ){?>
				<div class="inside-form__title"><?=Yii::t( $NSi18n, 'Mailings' )?></div>
				<?foreach( $mailingTypes as $mailingType ){?>
					<label class="inside-form__label clearfix" for="UserSettingsFormModel_mailingTypesAccept_<?=$mailingType->id?>">
						<?=CHtml::checkBox( "UserSettingsFormModel[mailingTypesAccept][]", !$mailingType->currentUserMute, Array( 'id' => "UserSettingsFormModel_mailingTypesAccept_{$mailingType->id}", 'value' => $mailingType->id, 'class' => 'inside-form__checkbox' ))?>
						<div class="inside-form__checkbox-text"><?=$mailingType->name?></div>
					</label>
				<?}?>
			<?}?>
			
		</div>
		<div class="inside-form__buttons">
			<button class="inside-form__btn-blue wSubmit"><?=Yii::t( $NSi18n, 'Save' )?></button>
			<button class="inside-form__btn-gray wReset"><?=Yii::t( $NSi18n, 'Reset' )?></button>
		</div>
	<?php $this->endWidget();?>
</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
				
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wUserSettingsFormWidget = wUserSettingsFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl('user/ajaxUpdateSettings')?>',
		});
	}( window.jQuery );
</script>