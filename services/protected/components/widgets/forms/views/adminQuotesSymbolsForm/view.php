<?
Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminQuotesSymbolsFormWidget.js" );

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

$title =Yii::t( "quotesWidget",$model->getAR()->isNewRecord ? 'Новый алиас' : 'Редактирование алиса');

$jsls = array(
    'lNew' => Yii::t( "quotesWidget", 'Новый алиас'),
    'lEdit' => Yii::t( "quotesWidget", 'Редактирование алиса'),
    'lDeleting' => Yii::t( "quotesWidget", 'Удаление...'),
    'lDeleted' => Yii::t( "quotesWidget", 'Удалено'),
    'lSaving' => Yii::t( "quotesWidget", 'Сохранение...'),
    'lSaved' => Yii::t( "quotesWidget", 'Сохранено'),
    'lLoading' => Yii::t( "quotesWidget", 'Загрузка...'),
    'lDeleteConfirm' => Yii::t( "quotesWidget", 'Удалить?')
);
foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);

?>
<div class="well pull-left iFormWidget i01 wAdminQuotesSymbolsFormWidget">
    <? $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array( 'htmlOptions' => array( 'enctype' => 'multipart/form-data' ))); ?>
    <h3 class="<?=$ins?> wTitle"></h3>
    <div class="tab-content">
            <div id="tabCommon">
                <?= $form->hiddenField($model, 'id', array('class' => "{$ins} wID")) ?>
                <?= $form->textFieldRow($model, 'name', array( 'class' => "{$ins} wName" )) ?>
                <?=$form->dropDownListRow($model, 'category', $categories, array( 'class' => "{$ins} wCategory" )) ?>
                <?=$form->dropDownListRow($model, 'panel', $panel, array( 'class' => "{$ins} wPanel" )) ?>
                <?= $form->textFieldRow($model, 'tickName', array( 'class' => "{$ins} wTickName" )) ?>
                
                <?= $form->textFieldRow($model, 'histName', array( 'class' => "{$ins} wHistName" )) ?>
                <?= $form->textFieldRow($model, 'yahooHistName', array( 'class' => "{$ins} wyahooHistName" )) ?>
                
                <div class="jsAddRows histAliases">
                    <label for="histAliases"><?= Yii::t( $NSi18n, "Add history alias" )?></label>
                    <?=CHtml::textField("histAliases", null, array( 'class' => "{$ins} whistAliases" )) ?>
                    
                    <div class="clear"></div>
                    <?
                        $this->widget( 'bootstrap.widgets.TbButton', array( 
                            'label' => Yii::t( $NSi18n, 'Add' ),
                            'size' => 'mini',
                            'htmlOptions' => array(
                                'class' => "{$ins} wAddKeyword",
                            ),
                        ));
                    ?>
                    <?=$form->labelEx( $model, 'histAliases', array( 'style' => "padding-top:10px;" ))?>
                    <?=$form->hiddenField( $model, 'histAliases', array( 'class' => "{$ins} whistAliases" ))?>
                    <table class="table i01 table-bordered <?=$ins?> wTabKeywords">
                        <tbody>
                            <tr class="wTpl" style="display:none;">
                                <td class="wTDName"></td>
                                <td class="iActions">
                                    <a href="#" class="icon-trash wDelete"></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="clear"></div>
                </div>

                <?=$form->fileFieldRow( $model, 'nameIcon', array( 'class' => "{$ins} wnameIcon" ))?>
                <img class="<?=$ins?> wnameIconToAdmin hide" src="" style="height:50px;" />
                <br><br>
                
                
                <?=$form->dropDownListRow($model, 'sourceType', $sourceType, array( 'class' => "{$ins} wSourceType" )) ?>
                
                <div class='bitzData' style='display:none;'>
                    <?=$form->dropDownListRow($model, 'bitzSymbol', array_combine( $bitzSymbols, $bitzSymbols ), array( 'class' => "{$ins} wbitzSymbol" )) ?>
                </div>

                <div class='convertToUsdWrap' style='display:none;'>
                    <label for="<?=$model->resolveID( 'convertToUsd' )?>">
                        <?=$form->checkBox( $model, 'convertToUsd', array( 'class' => "{$ins} wconvertToUsd" ) )?>
                        <span class="lbl">
                            <?=$model->getAttributeLabel( 'convertToUsd' )?>
                        </span>
                    </label>
                </div>

                <div class='okexData' style='display:none;'>
                    <?=$form->dropDownListRow($model, 'okexSymbol', array_combine($okexSymbols, $okexSymbols), array( 'class' => "{$ins} wokexSymbol" )) ?>
                </div>


                <label for="<?=$model->resolveID( 'toCryptoCurrenciesPage' )?>">
                    <?=$form->checkBox( $model, 'toCryptoCurrenciesPage', array( 'class' => "{$ins} wtoCryptoCurrenciesPage" ) )?>
                    <span class="lbl">
                        <?=$model->getAttributeLabel( 'toCryptoCurrenciesPage' )?>
                    </span>
                </label>
                <br>

                
                <ul class="nav nav-tabs <?=$ins?> wTabs">
                    <?foreach( $languages as $i=>$language ){?>
                        <?$class = !$i ? ' class="active"' : ''?>
                        <?$title = strtoupper( $language->alias )?>
                        <?$title = str_replace( "EN_US", "EN", $title )?>
                        <li<?=$class?>>
                            <a href="#tab<?=$title?>" data-toggle="tab">
                                <?=$title?>
                            </a>
                        </li>
                    <?}?>
                </ul>
                
                <div class="tab-content">
                    <?foreach( $languages as $i=>$language ){?>
                        <?$class = !$i ? ' active' : ''?>
                        <?$title = strtoupper( $language->alias )?>
                        <?$title = str_replace( "EN_US", "EN", $title )?>
                        <div class="tab-pane<?=$class?> langTabCats" id="tab<?=$title?>" data-langId="<?=$language->id?>">
                            <?php
                                foreach( $model->textFields as $field => $data ){
                                    if( $data['type'] == 'dropDownListRow' ){
                                        
                                        echo $form->dropDownListRow($model,"{$field}[{$language->id}]", ${$field}, array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}", 'disabled' => $data['disabled'] ));
                                        
                                    }elseif( $data['type'] == 'checkBox' ){
                                        
                                        echo "<label for=".$model->resolveID( "{$field}[{$language->id}]" ).">";
                                            echo $form->checkBox( $model, "{$field}[{$language->id}]", array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}", 'disabled' => $data['disabled'] ));
                                            echo "<span class='lbl'>";
                                                echo $model->getAttributeLabel( "{$field}[{$language->id}]" );
                                            echo "</span>";
                                        echo "</label>";
                                        
                                    }else{
                                        
                                        echo $form->{$data['type']}( $model, "{$field}[{$language->id}]", array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}", 'disabled' => $data['disabled'] ));
                                        
                                    }
                                    
                                }    
                            ?>
                            
                            
                            <div class="jsAddRows keys">
                                <label for="keyword"><?=Yii::t( $NSi18n, "Add keyword" )?></label>
                                <?=CHtml::textField("keyword", null, array( 'class' => "{$ins} wKeyword" )) ?>
                                
                                <div class="clear"></div>
                                <?
                                    $this->widget( 'bootstrap.widgets.TbButton', array( 
                                        'label' => Yii::t( $NSi18n, 'Add' ),
                                        'size' => 'mini',
                                        'htmlOptions' => array(
                                            'class' => "{$ins} wAddKeyword",
                                        ),
                                    ));
                                ?>
                                <?=$form->labelEx( $model, 'keywords', array( 'style' => "padding-top:10px;" ))?>
                                <?=$form->hiddenField( $model, "keywords[{$language->id}]", array( 'class' => "{$ins} wKeywords" ))?>
                                <table class="table i01 table-bordered <?=$ins?> wTabKeywords">
                                    <tbody>
                                        <tr class="wTpl" style="display:none;">
                                            <td class="wTDName"></td>
                                            <td class="iActions">
                                                <a href="#" class="icon-trash wDelete"></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="clear"></div>
                            </div>
                            
                            
                        </div>
                    <?}?>
                </div>
                
            
            </div>
        </div>

        <div class="clear"></div>
        <div class="iControlBlock i01">
            <?
            $this->widget( 'bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'success',
                'label' => Yii::t( "quotesWidget", 'Продолжить' ),
                'htmlOptions' => array(
                    'class' => "pull-right {$ins} wSubmit",
                ),
            ));
            ?>
            <?
            $this->widget( 'bootstrap.widgets.TbButton', array(
                'type' => 'danger',
                'label' => Yii::t( "quotesWidget", 'Удалить' ),
                'htmlOptions' => array(
                    'class' => "pull-left {$ins} wDelete",
                ),
            ));
            ?>
        </div>
    <div class="clear"></div>
    <iframe name="iframeForSymbol" id="iframeForSymbol" style="display:none"></iframe>
    <? $this->endWidget() ?>
</div>
<script>
    !function( $ ) {
        var ns = <?=$ns?>;
        var nsText = ".<?=$ns?>";
        var $ns = $( nsText );
        var ins = ".<?=$ins?>";
        var ajax = <?=CommonLib::boolToStr($ajax)?>;
        var hidden = <?=CommonLib::boolToStr($hidden)?>;

        var ls = <?=json_encode( $jsls )?>;

        ns.wAdminQuotesSymbolsFormWidget = wAdminQuotesSymbolsFormWidgetOpen({
            ins: ins,
            selector: nsText+' .wAdminQuotesSymbolsFormWidget',
            ajax: ajax,
            hidden: hidden,
            ls: ls
        });
    }( window.jQuery );
</script>
