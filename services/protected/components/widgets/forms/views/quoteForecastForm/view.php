<?php
	Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.forms').'/wQuoteForecastFormWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	Yii::app()->clientScript->registerCss('QuoteForecastWidgetCss', '
		.add_mood>a:hover{
			color:#fff;
		}
	');
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Forecast saved'
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	$model = new QuoteForecastFormModel();
?>
<!-- - - - - - - - - - - - - - Quotes Box 1 - - - - - - - - - - - - - - - - -->
<div class="<?=$ins?> section_offset">
	<!-- - - - - - - - - - - - - - Tabs - - - - - - - - - - - - - - - - -->
	<div class="tabs_box forecast_tabs">
		<!-- - - - - - - - - - - - - - Tabs List - - - - - - - - - - - - - - - - -->
		<ul class="tabs_list clearfix PFDregular">
			<li><a href="javascript:;"><?=Yii::t( $NSi18n, 'Make your forecast' )?></a></li>
			<li><a href="javascript:;"><?=Yii::t( $NSi18n, 'Add mood' )?></a></li>
			<li class="rating_traders_link"><a target="_blank" href="<?=Yii::app()->controller->createUrl('quotesNew/tradersRating')?>"><?=Yii::t( $NSi18n, 'Rating of traders' )?></a></li>
			<?php /*<li class="trader_link"><a href="#"><?=Yii::t( $NSi18n, 'Go to trade' )?></a></li>*/?>
		</ul>
		<!-- - - - - - - - - - - - - - End of Tabs List  - - - - - - - - - - - - - - - - -->
		<!-- - - - - - - - - - - - - - Tabs Contant - - - - - - - - - - - - - - - - -->
		<div class="tabs_contant">
			<!-- - - - - - - - - - - - - - Tabs Item 1 - - - - - - - - - - - - - - - - -->
			<div class="wForecast">
			
			
				<?php
				$forecatsLinkClass = '';
				if( !$forecatstCount ) $forecatsLinkClass = 'hide';
				echo CHtml::link(
					Yii::t( $NSi18n, 'Your forecats' ) . ' <span class="red_color">('.$forecatstCount.')</span>', 
					array('quotesNew/forecats', 'userId' => Yii::App()->user->id ), 
					array('target' => '_blank', 'class' => 'forecatsLink '.$forecatsLinkClass )
				); 
				?>
			
				<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array('class' => 'make_forecast clearfix' )))?>
					<?=$form->hiddenField( $model, 'symbolId', Array( 'value' => $symbol->id, 'class' => 'wSymbolId' ))?>
					<?=$form->hiddenField( $model, 'userId', Array( 'value' => Yii::App()->user->id, 'class' => 'wUserId' ))?>
					<?php
						$periods = array(
							'12' => '12 '.Yii::t( $NSi18n, 'hours' ),
							'24' => '1 '.Yii::t( $NSi18n, 'day' ),
							'168' => '1 '.Yii::t( $NSi18n, 'week' ),
						);
					?>
					<div class="make_forecast_left">
						<label for="<?=$model->resolveID( 'startDate' )?>"><?=Yii::t( $NSi18n, 'Period' )?></label>
						<div class="custom_select PFDregular">
							<?=$form->dropDownList($model,'startDate', $periods, array('options' => array('12'=>array('selected'=>true)), 'class' => 'wStartDate' ) ); ?>
						</div>
					</div>
					<div class="make_forecast_center">
						<?=$form->textFieldRow( $model, 'value', array( 'placeholder' => Yii::t( $NSi18n, 'Your goal' ), 'class' => 'wValue' ) )?>
					</div>
					<div class="make_forecast_right">
						<button type="submit" class="wSubmit"><?=Yii::t( $NSi18n, 'Add' )?></button>
					</div>
				<?$this->endWidget()?>
				<span class="wFormError red_color"></span>
				<p class="required_message PFDregular"><?=Yii::t( $NSi18n, 'To account for ranking goal should be achieved within a specified period' )?><span class="red_color">*</span></p>
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs Item 1 - - - - - - - - - - - - - - - - -->
   
			<!-- - - - - - - - - - - - - - Tabs Item 2 - - - - - - - - - - - - - - - - -->
			<div class="wMood">
				<?php
					$yourMoodClass = '';
					$yourMoodText = '';
					$canAddMood = 0;
					$wMoodButtonClass = 'clicked';
					if( $userMood == 0 ){
						$yourMoodClass = 'hide';
						$canAddMood = 1;
						$wMoodButtonClass = '';
					} 
					if( $userMood == 1 ) $yourMoodText = Yii::t( $NSi18n, 'Bullish' );
					if( $userMood == -1 ) $yourMoodText = Yii::t( $NSi18n, 'Bearish' );
				?>
				<div class="yourMood <?=$yourMoodClass?>"><?=Yii::t( $NSi18n, 'Your mood is' )?> <span><?=$yourMoodText?></span></div>
				<div class="add_mood_box">
					<div class="add_mood clearfix">
  						<a href="#" class="bullish_forecast PFDregular wBullish wMoodButton <?=$wMoodButtonClass?>"><span><?=Yii::t( $NSi18n, 'Bullish forecast' )?></span><i></i></a>
						<a href="#" class="bear_forecast PFDregular wBearish wMoodButton <?=$wMoodButtonClass?>"><span><?=Yii::t( $NSi18n, 'Bearish forecast' )?></span><i></i></a>
						<span><?=Yii::t( $NSi18n, 'or' )?></span>
					</div>
					<div class="mood_chart_box clearfix <?php if(!$mood) echo 'hide';?>">
						<div class="mood_chart_label alignleft"><?=Yii::t( $NSi18n, 'Bullish' )?></div>
						<div class="mood_chart_label alignright"><?=Yii::t( $NSi18n, 'Bearish' )?></div>
						<div class="mood_chart">
							<div class="mood_chart_bull" style="width: <?=$mood['bull']?>%;"></div>
							<div class="mood_chart_bear" style="width: <?=$mood['bear']?>%;"></div>
						</div>
						<div class="mood_chart_label1 bull PFDbold alignleft"><?=$mood['bull']?>%</div>
						<div class="mood_chart_label1 bear PFDbold alignright"><?=$mood['bear']?>%</div>
					</div>
				</div>
				<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array('class' => 'hide' )))?>
					<?=$form->hiddenField( $model, 'symbolId', Array( 'value' => $symbol->id, 'class' => 'wSymbolId', 'id' => 'moodSymbolId' ))?>
					<?=$form->hiddenField( $model, 'userId', Array( 'value' => Yii::App()->user->id, 'class' => 'wUserId', 'id' => 'moodUserId' ))?>
					<?=$form->hiddenField( $model, 'status', Array( 'value' => 2, 'class' => 'wStatus', 'id' => 'moodStatus' ))?>
					<?=$form->hiddenField( $model, 'value', Array( 'value' => 0, 'class' => 'wValue', 'id' => 'moodValue' ))?>
				<?$this->endWidget()?>
				<span class="wFormError red_color"></span>
			</div>
			<!-- - - - - - - - - - - - - - End of Tabs Item 2 - - - - - - - - - - - - - - - - -->
		</div><!-- / .tabs_contant -->
		<!-- - - - - - - - - - - - - - End of Tabs Contant - - - - - - - - - - - - - - - - -->
	</div>
	<!-- - - - - - - - - - - - - - End of Tabs - - - - - - - - - - - - - - - - -->
</div>
<!-- - - - - - - - - - - - - - End of Quotes Box 1 - - - - - - - - - - - - - - - - -->
<script>
	!function( $ ) {
		var ns = ".<?=$ns?>";
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var ls = <?=json_encode( $jsls )?>;
		var ajaxSubmitURL = "<?=Yii::App()->controller->createAbsoluteUrl( '/quotesNew/ajaxAddForecast' )?>";

		ns.wQuoteForecastFormWidget = wQuoteForecastFormWidgetOpen({
			ns: ns,
			ajaxSubmitURL: ajaxSubmitURL,
			ins: ins,
			ls: ls,
			selector: nsText+' .<?=$ins?>',
			canAddMood: <?=$canAddMood?>,
		});
	}( window.jQuery );
</script>