<?php
Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/widgets/forms/wAdminCryptoCurrenciesExchangesSymbolToOurSymbolFormWidget.js");
	
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();
	
$jsls = array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
		'lNew' => 'New currency',
		'lEdit' => 'Edit currency'
);
foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);

?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">
	
	<section class="section_offset">
		<h3 class="<?=$ins?> wTitle"><?=$addLabel?></h3>
		<hr class="separator2">
	</section>
	<div class="section_offset">
		<div class="form_add_in_table">
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
			
				<?=$form->hiddenField($model, 'id', Array( 'class' => "{$ins} wID" ))?>
                <?=$form->hiddenField($model, 'idExchange', Array( 'class' => "{$ins} widExchange" ))?>
                <?=$form->textFieldRow($model, 'symbol', array( 'class' => "{$ins} wsymbol", 'disabled' => true))?>
                <?=$form->dropDownList($model, 'symbolId', $ourSymbols, array('class' => "{$ins} wsymbolId"))?>


				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
				</div>
				<div class="clear"></div>
			<?$this->endWidget()?>


		</div>
	</div>

</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = true;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonFormWidget = wAdminCryptoCurrenciesExchangesSymbolToOurSymbolFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl($submitItemRoute)?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl($deleteItemRoute)?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl($loadItemRoute)?>',
			errorClass: 'inpError',
			formModelName: '<?=$formModelName?>',
			modelName: '<?=$modelName?>',
		});
	}( window.jQuery );
</script>