<?
	$cs=Yii::app()->getClientScript();
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.forms').'/wFeedbackFormWidget.js' ) );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lCancel' => 'Cancel',
		'lDeleteConfirm' => 'Delete?',
		'lSended' => 'Message sent!',
	);
	
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="wFeedbackFormWidget iDiv i61">
	<?php if( $mode == 'feedback' ){ ?>
		<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal', 'htmlOptions' => Array( 'style' => 'margin-bottom:0;' )))?>
			<div class="controls" style="margin-bottom:20px;">
				<span class="iSpan i20">
					<?=Yii::t( '*', 'Feedback' )?>
				</span>
			</div>
		
			<?=$form->textFieldRow( $model, 'user' )?>
			<?=$form->textFieldRow( $model, 'email' )?>
			<?=$form->dropDownListRow( $model, 'idType', $types )?>
			<?=$form->textAreaRow( $model, 'message', Array( 'rows' => '5' ))?>
			
			<div class="controls">
				<button class="btn btn-info <?=$ins?> wSubmit" type="submit">
					<?=Yii::t( $NSi18n, 'Send' )?>
				</button>
			</div>
		<?$this->endWidget()?>
	<?php } ?>
	
	<?php 
		if( $mode == 'contest' ){
			$arcticmodalBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/arcticmodal' );
			$cs->registerCssFile($arcticmodalBaseUrl.'/jquery.arcticmodal-0.3.css');
			$cs->registerScriptFile($arcticmodalBaseUrl.'/jquery.arcticmodal-0.3.min.js', CClientScript::POS_END);
	?>
		<!-- - - - - - - - - - - - - - Modal Window - - - - - - - - - - - - - - - - -->
		<div class="hide">
			<div id="conductCompetition" class="modal_box">
				<div class="arcticmodal-close modal_close"><i class="fa fa-times"></i></div>
				<div class="modal_header">
					<div class="modal_logo">FT</div>
					<h6 class="modal_header_title"><?=Yii::t( $NSi18n, 'Submit a proposal for the contest' )?></h6>
					<h6 class="modal_header_title"><?=Yii::t( $NSi18n, 'For companies' )?></h6>
				</div>
				<div class="modal_inner">
				<?php
					$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal', 'htmlOptions' => Array( 'id' => 'conductCompetitionForm' )));
						echo CHtml::tag( 'span', array( 'class' => 'form-error errorMess' ), '' );
						echo $form->hiddenField( $model, 'idType', Array( 'value' => $types ));
						echo $form->textField( $model, 'user', array( 'class' => "modal_input user-name", 'placeholder' => Yii::t( $NSi18n, 'Your name' ) ) );
						echo $form->emailField( $model, 'email', array( 'class' => "modal_input user-email", 'placeholder' => $model->getAttributeLabel( 'email' ) ) );
						echo $form->textArea( $model, 'message', Array( 'rows' => '5', 'class' => 'modal_input user-email', 'placeholder' => Yii::t( $NSi18n, 'Full information about the contest' ) ));
						echo CHtml::tag( 'button', array( 'class' => "modal_button {$ins} wSubmit", 'type' => 'submit' ), Yii::t( $NSi18n, 'Send' ) );						
					$this->endWidget();
				?>
				</div>
			</div>
		</div>
		<!-- - - - - - - - - - - - - - End of Modal Window - - - - - - - - - - - - - - - - -->
	<?php } ?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
				
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wFeedbackFormWidget = wFeedbackFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wFeedbackFormWidget',
			ls: ls,
			mode: '<?=$mode?>',
			errorClass: 'invalid',
			ajaxSubmitURL: '<?=Yii::app()->createUrl('contacts/ajaxAddFeedback')?>',
		});
	}( window.jQuery );
</script>