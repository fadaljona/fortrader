<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminCounterFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New counter' : 'Editor counter';
	
	$jsls = Array(
		'lNew' => 'New counter',
		'lEdit' => 'Editor counter',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminCounterFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		<?=$form->textFieldRow( $model, 'name', Array( 'class' => "{$ins} wName" ))?>
		<?=$form->textAreaRow( $model, 'code', Array( 'class' => "input-xxlarge {$ins} wCode", 'rows' => 5 ))?>
		
		<label for="<?=$model->resolveID( 'enabled' )?>">
			<?=$form->checkBox( $model, 'enabled', Array( 'class' => "{$ins} wEnabled" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'enabled' )?>
			</span>
		</label>
		
		<label for="<?=$model->resolveID( 'common' )?>">
			<?=$form->checkBox( $model, 'common', Array( 'class' => "{$ins} wCommon" ))?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'common' )?>
			</span>
		</label>
		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCounterFormWidget = wAdminCounterFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminCounterFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>