<?php
Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/widgets/forms/wAdminCommonSettingsFormWidget.js");

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

$jsls = array(
    'lSaving' => 'Saving...',
    'lSaved' => 'Saved',
    'lLoading' => 'Loading...',
    'lDeleteConfirm' => 'Delete?',
    'lReseted' => 'Reseted',
);
foreach ($jsls as &$ls) {
    $ls = Yii::t($NSi18n, $ls);
}
unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm">
    <?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
        <h3 class="<?=$ins?> wTitle"><?=Yii::t($NSi18n, 'Editor settings')?></h3>

        <br />
        <div class="row-fluid">
            <div class="span3">
            <?php
                $this->widget('components.widgets.AdminImageUploadWidget', array(
                    'form' => $form,
                    'model' => $model,
                    'field' => 'ogImage',
                    'ins' => $ins,
                    'imgName' => 'admin.cryptoCurrencies.settings.ogImage',
                ));
            ?>
            </div>
            <div class="span3">

            </div>
            <div class="span3">
            
            </div>
            <div class="span3">
            
            </div>
            
        </div>

        <?=$form->dropDownListRow($model, 'bitcoin', $quotes, Array( 'class' => "{$ins} wbitcoin" ))?>
        <?=$form->dropDownListRow($model, 'videoChanel', $videoChanels, Array( 'class' => "{$ins} wvideoChanel" ))?>
        <?=$form->dropDownListRow($model, 'faq', $faqs, Array( 'class' => "{$ins} wfaq" ))?>


        <ul class="nav nav-tabs <?=$ins?> wTabs">
            <?php foreach ($languages as $i => $language) {?>
                <?$class = !$i ? ' class="active"' : ''?>
                <?$title = strtoupper($language->alias)?>
                <?$title = str_replace("EN_US", "EN", $title)?>
                <li<?=$class?>>
                    <a href="#tab<?=$title?>" data-toggle="tab">
                        <?=$title?>
                    </a>
                </li>
            <?php }?>
        </ul>

        <div class="tab-content">
            <?foreach ($languages as $i => $language) {?>
                <?$class = !$i ? ' active' : ''?>
                <?$title = strtoupper($language->alias)?>
                <?$title = str_replace("EN_US", "EN", $title)?>
                <div class="tab-pane<?=$class?>" id="tab<?=$title?>">
                <?php
                $startDefaultExchangeDirection = false;
                foreach ($model->textFields as $field => $fieldType) {
                    if (!$startDefaultExchangeDirection && strpos($field, 'defaultExchangeDirection') !== false) {
                        echo '<hr><hr><h4>' . Yii::t($NSi18n, 'Available vars {$from}, {$to}') . '</h4>';
                        $startDefaultExchangeDirection = true;
                    }
                    echo $form->{$fieldType}($model, "{$field}[{$language->id}]", array('class' => "{$ins} wFullWidth w{$field} w{$language->id}"));
                }
                ?>
                </div>
            <?}?>
        </div>

        <div class="clear"></div>
        <div class="iControlBlock i01">
        <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'success',
                'label' => Yii::t($NSi18n, 'Save'),
                'htmlOptions' => array(
                    'class' => "pull-right {$ins} wSubmit",
                ),
            ));
        ?>
        <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'type' => 'danger',
                'label' => Yii::t($NSi18n, 'Reset'),
                'htmlOptions' => array(
                    'class' => "pull-left {$ins} wReset",
                ),
            ));
        ?>
        </div>
        <div class="clear"></div>
    <?$this->endWidget()?>
</div>
<script>
!function( $ ) {
    var ns = <?=$ns?>;
    var nsText = ".<?=$ns?>";
    var $ns = $( nsText );
    var ins = ".<?=$ins?>";
    var ajax = <?=CommonLib::boolToStr($ajax)?>;

    var ls = <?=json_encode($jsls)?>;

    ns.wAdminCommonSettingsFormWidget = wAdminCommonSettingsFormWidgetOpen({
        ins: ins,
        selector: nsText+' .wAdminCommonSettingsFormWidget',
        ajax: ajax,
        ls: ls,
        ajaxSubmitURL: '<?=Yii::app()->createUrl('admin/cryptoCurrencies/ajaxSettingsUpdate')?>',
    });
}( window.jQuery );
</script>