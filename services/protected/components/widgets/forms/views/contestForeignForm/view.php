<?
	$cs=Yii::app()->getClientScript();
	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.forms').'/wContestForeignFormWidget.js' ) );
	
	$inputmaskBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/inputmask' );

	$cs->registerScriptFile($inputmaskBaseUrl.'/inputmask.js', CClientScript::POS_END);
	$cs->registerScriptFile($inputmaskBaseUrl.'/inputmask.date.extensions.js', CClientScript::POS_END);
	$cs->registerScriptFile($inputmaskBaseUrl.'/jquery.inputmask.js', CClientScript::POS_END);

	Yii::app()->clientScript->registerScript('maskJs', '
	!function( $ ) {	
		if($(".date_to").length){
			$(".date_to").inputmask("y-m-d", { "placeholder": "yyyy-mm-dd" });
		}
	}( window.jQuery );
	', CClientScript::POS_END);
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
		'lCCreated' => 'Your contest has been sent for moderation, please wait.'
	);
	if( Yii::app()->user->checkAccess('contestControl') ){
		$jsls['lCCreated'] = 'Contest added';
	}
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">
	
	<section class="section_offset">
		<h3><?=Yii::t( $NSi18n, 'Add contest' )?><?php if($mode != 'admin'){ ?><i class="fa fa-table" aria-hidden="true"></i><?php }?></h3>
		<hr class="separator2">
	</section>
	<div class="section_offset">
		<div class="form_add_in_table">
		<?php if($mode == 'admin'){ ?>
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
			
				<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
				<?=$form->textFieldRow( $model, 'prize', Array( 'class' => "{$ins} wprize" ) )?>
				<?=$form->textFieldRow( $model, 'begin', array( 'class' => "date_to {$ins} wbegin" ) )?>
				
				<label for="<?=$model->resolveID( 'approved' )?>">
					<?=$form->checkBox( $model, 'approved', Array( 'class' => "{$ins} wapproved" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'approved' )?>
					</span>
				</label>
				<br />
				
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
							<?php
							
								foreach( $model->textFields as $field => $data ){

									echo $form->{$data['type']}( $model, "{$field}[{$language->id}]", Array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}" ));
								}	
							?>
						</div>
					<?}?>
				</div>
				
				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'type' => 'danger',
							'label' => Yii::t( $NSi18n, 'Delete' ),
							'htmlOptions' => Array(
								'class' => "pull-left {$ins} wDelete",
							),
						));
					?>
				</div>
				<div class="clear"></div>
			<?$this->endWidget()?>
		<?php 
		}
		if( $mode == 'front' ){?>
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
				<div class="clearfix">
					<?php	
						echo $form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ));
						
						echo CHtml::openTag('label', array( 'class' => 'box_name' ));	
							echo $model->getAttributeLabel( "title[{$languages->id}]" );
							echo CHtml::tag('br');
							echo $form->textField( $model, "title[{$languages->id}]", Array( 'class' => "name {$ins} wFullWidth wtitle w{$languages->id}" ));
						echo CHtml::closeTag('label');	
					
						echo CHtml::openTag('label', array( 'class' => 'box_prize' ));	
							echo $model->getAttributeLabel( 'prize' );
							echo CHtml::tag('br');
							echo $form->textField( $model, 'prize', array( 'class' => "prize {$ins} wprize" ) );
						echo CHtml::closeTag('label');
						
						echo CHtml::openTag('label', array( 'class' => 'box_date_to' ));	
							echo $model->getAttributeLabel( 'begin' );
							echo CHtml::tag('br');
							echo $form->textField( $model, 'begin', array( 'class' => "date_to {$ins} wbegin" ) );
						echo CHtml::closeTag('label');
					?>
				</div>
				<div class="clearfix">
					<?php
						echo CHtml::openTag('label', array( 'class' => 'box_sponsor' ));	
							echo $model->getAttributeLabel( "sponsor[{$languages->id}]" );
							echo CHtml::tag('br');
							echo $form->textField( $model, "sponsor[{$languages->id}]", Array( 'class' => "sponsor {$ins} wFullWidth wsponsor w{$languages->id}" ));
						echo CHtml::closeTag('label');	
						
						echo CHtml::openTag('label', array( 'class' => 'box_link' ));	
							echo $model->getAttributeLabel( "link[{$languages->id}]" );
							echo CHtml::tag('br');
							echo $form->textField( $model, "link[{$languages->id}]", Array( 'class' => "link {$ins} wFullWidth wlink w{$languages->id}" ));
						echo CHtml::closeTag('label');	
					?>
				</div>
				<div class="clearfix">
					<span class="errorMess red_color"></span>
				</div>
				<div class="clearfix">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Add' ),
							'htmlOptions' => Array(
								'class' => "add_btn {$ins} wSubmit",
							),
						));
					?>
				</div>
			
			<?$this->endWidget()?>
		<?php } ?>
		</div>
	</div>

</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr( $mode=='admin' ? true : false )?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wContestForeignFormWidget = wContestForeignFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl('contest/ajaxAddForeignContest')?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl('admin/contest/ajaxLoadForeignContestDelete')?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl('admin/contest/ajaxLoadForeignContest')?>',
			mode: '<?=$mode?>',
			errorClass: 'inpError',
		});
	}( window.jQuery );
</script>