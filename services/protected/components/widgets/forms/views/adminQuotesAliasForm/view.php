<?
Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/widgets/forms/wAdminQuotesAliasFormWidget.js");

$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

$title =Yii::t( "quotesWidget",$model->getAR()->isNewRecord ? 'Новый алиас' : 'Редактирование алиса');

$jsls = Array(
	'lNew' => Yii::t( "quotesWidget",'Новый алиас'),
	'lEdit' => Yii::t( "quotesWidget",'Редактирование алиса'),
	'lDeleting' => Yii::t( "quotesWidget",'Удаление...'),
	'lDeleted' => Yii::t( "quotesWidget",'Удалено'),
	'lSaving' => Yii::t( "quotesWidget",'Сохранение...'),
	'lSaved' => Yii::t( "quotesWidget",'Сохранено'),
	'lLoading' => Yii::t( "quotesWidget",'Загрузка...'),
	'lDeleteConfirm' => Yii::t( "quotesWidget",'Удалить?')
);
foreach($jsls as &$ls) $ls = Yii::t($NSi18n,$ls);
unset($ls);

$languages = LanguageModel::getAll();
?>
<div class="well pull-left iFormWidget i01 wAdminQuotesAliasFormWidget">
	<? $form = $this->beginWidget('bootstrap.widgets.TbActiveForm') ?>
	<div class="tab-content">
			<div id="tabCommon">
				<?= $form->hiddenField($model,'id',Array('class' => "{$ins} wID")) ?>
				<?= $form->textFieldRow($model,'name',Array('class' => "{$ins} wName")) ?>
				<?= $form->textAreaRow($model,'desc',Array('class' => "{$ins} wDesc")) ?>
				<p>
					<span><?=Yii::t($NSi18n,'Symbol')?></span>
					<br>
				<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',array('model' => $model,'attribute' => 'symbol_suggest',
					'source' => Yii::app()->createUrl('admin/quotesAlias/suggest'),
					'options' => array(
						'delay' => 300,
						'minLength' => 1,
						'showAnim' => 'fold',
						'multiple' => false,
						'select' => "js:function(event, ui) {
						this.value = ui.item.label;
						$('#AdminQuotesAliasFormModel_symbol').val(ui.item.id);
						return false;}"
					),
					'htmlOptions' => array(
						'size' => '40'
					)
				));
				?>
					<?= $form->hiddenField($model,'symbol',Array('class' => "{$ins} wID")) ?>
				</p>
			</div>
		</div>

		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
			$this->widget('bootstrap.widgets.TbButton',Array(
				'buttonType' => 'submit',
				'type' => 'success',
				'label' => Yii::t( "quotesWidget",'Продолжить' ),
				'htmlOptions' => Array('class' => "pull-right {$ins} wSubmit",)
			));
			?>
			<?
			$this->widget('bootstrap.widgets.TbButton',Array(
				'type' => 'danger',
				'label' => Yii::t( "quotesWidget",'Удалить' ),
				'htmlOptions' => Array('class' => "pull-left {$ins} wDelete",)
			));
			?>
		</div>
	<div class="clear"></div>
	<? $this->endWidget() ?>
</div>
<script>
	!function ($) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $(nsText);
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;

		var ls = <?=json_encode( $jsls )?>;

		ns.wAdminQuotesAliasFormWidget = wAdminQuotesAliasFormWidgetOpen({
			ins: ins,
			selector: nsText + ' .wAdminQuotesAliasFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls
		});
	}(window.jQuery);
</script>
