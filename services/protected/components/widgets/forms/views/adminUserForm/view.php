<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminUserFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New user' : 'Editor user';
	
	$nameGroups = 'groups[]';
	$nameGroups = CHtml::resolveName( $model, $nameGroups );
	
	$jsls = Array(
		'lNewUser' => 'New user',
		'lEditUser' => 'Editor user',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminUserFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'ID', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<li class="active"><a href="#tabInfo" data-toggle="tab"><?=Yii::t( $NSi18n, 'Information' )?></a></li>
			<li><a href="#tabSettings" data-toggle="tab"><?=Yii::t( $NSi18n, 'Settings' )?></a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="tabInfo">
				<?=$form->textFieldRow( $model, 'ID', Array( 'class' => "{$ins} wID", 'disabled' => 'disabled' ))?>
				<?=$form->textFieldRow( $model, 'user_login', Array( 'class' => "{$ins} wLogin" ))?>
				<?=$form->textFieldRow( $model, 'user_nicename', Array( 'class' => "{$ins} wNicename" ))?>
				<?=$form->textFieldRow( $model, 'display_name', Array( 'class' => "{$ins} wDisplayName" ))?>
				<?=$form->textFieldRow( $model, 'user_email', Array( 'class' => "{$ins} wEmail" ))?>
				<?=$form->passwordFieldRow( $model, 'user_pass', Array( 'class' => "{$ins} wPassword", 'placeholder' => Yii::t( $NSi18n, 'Fill to change' )))?>
				<div class="clear"></div>
				<div class="iGroupsBlock i01 <?=$ins?> wGroups">
					<?=Yii::t( $NSi18n, 'User group' )?>
					<?foreach( $groups as $group ){?>
						<label>
							<?=CHtml::checkBox( $nameGroups, false, Array( 'id' => '', 'value' => $group->id ))?>
							<span class="lbl">
								<?=Yii::t( $NSi18n, $group->name )?>
							</span>
						</label>
					<?}?>
				</div>
				<br>
				<label for="<?=$model->resolveID( 'sendNotice' )?>">
					<?=$form->checkBox( $model, 'sendNotice', Array( 'class' => "{$ins} wSendNotice" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'sendNotice' )?>
					</span>
				</label>
			</div>
			<div class="tab-pane" id="tabSettings">
				<?=$form->dropDownListRow( $model, 'language', $languages, Array( 'class' => "{$ins} wLanguage" ))?>
				<?=$form->dropDownListRow( $model, 'idBroker', $brokers, Array( 'class' => "{$ins} wBroker" ))?>
				
				<?if( $mailingTypes ){?>
					<h4><?=Yii::t( $NSi18n, 'Mailings' )?></h4>
					<?foreach( $mailingTypes as $mailingType ){?>
						<label for="AdminUserFormModel_mailingTypesAccept_<?=$mailingType->id?>">
							<?=CHtml::checkBox( "AdminUserFormModel[mailingTypesAccept][]", false, Array( 'id' => "AdminUserFormModel_mailingTypesAccept_{$mailingType->id}", 'class' => "{$ins} wMailingTypesAccept", 'value' => $mailingType->id ))?>
							<span class="lbl">
								<?=$mailingType->name?>
							</span>
						</label>
					<?}?>
				<?}?>
				
			</div>
		</div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var type = "<?=$type?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminUserFormWidget = wAdminUserFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminUserFormWidget',
			ajax: ajax,
			hidden: hidden,
			type: type,
			ls: ls,
		});
	}( window.jQuery );
</script>