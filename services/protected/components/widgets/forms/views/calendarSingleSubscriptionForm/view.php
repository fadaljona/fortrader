<?php
	$cs=Yii::app()->getClientScript();
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');

	$cs->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.forms').'/wCalendarSingleSubscriptionFormWidget.js' ));
	
	$cs->registerScript('jsStyler', '
		!function( $ ) {
			var $informerCalendarTimeRadio = $(".informer-calendar__time-radio");
			if( $informerCalendarTimeRadio.length ){ $informerCalendarTimeRadio.styler(); }
			$informerCalendarTimeRadio.change(function(){
				$(this).closest(".informer-calendar__time").find("input[type=\'text\']").attr({"disabled": true});
				$(this).closest(".informer-calendar__time-item").find("input[type=\'text\']").attr({"disabled": false});
			});
		}( window.jQuery );
	', CClientScript::POS_END);
	
	$cs->registerScript('jsSlideToggleSubscribeBlock', '
		!function( $ ) {
			$(".colorInformer").click(function(){ $(this).parent(".section_offset").find(".box2").slideToggle("slow"); });
		}( window.jQuery );
	', CClientScript::POS_END);

	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
?>

<section class="section_offset position-relative spinner-margin paddingBottom30 calendarSubscribeWidget <?=$ins?>">
	<h4 class="title7 colorInformer"><?=Yii::t($NSi18n, 'Subscribe to notifications by email')?></h4>
	<div class="box2 clearfix">
	<?php
		$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal', 'htmlOptions' => array( 'class' => 'regestration clearfix' ) ));
		
			echo $form->hiddenField( $model, 'idEvent', Array( 'class' => "{$ins} widEvent" ));
			echo $form->hiddenField( $model, 'timeBeforeEvent', Array( 'class' => "{$ins} wtimeBeforeEvent" ));
			
			$selectTimeClass = $subscribeClass = $unsubscribeClass = $hoursDisabled = $minsDisabled = $minsChecked = $hoursChecked = '';
			if( $model->id ){
				$selectTimeClass = $subscribeClass = ' hide ';
				if( $model->timeBeforeEvent % 3600 == 0 ){
					$minsVal = 30;
					$hoursVal = $model->timeBeforeEvent / 3600;
					$hoursChecked = 'checked';
					$minsDisabled  = 'disabled';
				}else{
					$minsVal = $model->timeBeforeEvent / 60;
					$hoursVal = 1;
					$hoursDisabled = 'disabled';
					$minsChecked = 'checked';
				}
			}else{
				$unsubscribeClass = ' hide ';
				$hoursDisabled = 'disabled';
				$minsChecked = 'checked';
				$minsVal = 30;
				$hoursVal = 1;
			}

	?>
		<div class="informer-calendar__time alignleft <?=$selectTimeClass?>">
			<label for="time_minutes" class="informer-calendar__time-item">
				<input type="radio" id="time_minutes" name="calendar_time" <?=$minsChecked?> class="informer-calendar__time-radio">
				<input type="text" maxlength="2" value="<?=$minsVal?>" class="informer-calendar__time-input" <?=$minsDisabled?>>
				<span class="informer-calendar__time-descr"><?=Yii::t($NSi18n, 'Minutes to start')?></span>
			</label>
			<label for="time_hourse" class="informer-calendar__time-item">
				<input type="radio" id="time_hourse" name="calendar_time" <?=$hoursChecked?> class="informer-calendar__time-radio">
				<input type="text" maxlength="2" value="<?=$hoursVal?>" class="informer-calendar__time-input" <?=$hoursDisabled?>>
				<span class="informer-calendar__time-descr"><?=Yii::t($NSi18n, 'Hours to start')?></span>
			</label>
		</div>

	<?php
			
			echo CHtml::openTag('div', array( 'class' => 'alignleft regestration_box_50_padding_left' ));	
				echo CHtml::button( Yii::t($NSi18n, 'Subscribe'), array('class'=>'regestration_btn wSubscribe ' . $subscribeClass, 'style' => 'margin-top:0;') );
				echo CHtml::button( Yii::t($NSi18n, 'Unsubscribe'), array('class'=>'regestration_btn wUnsubscribe ' . $unsubscribeClass, 'style' => 'margin-top:0;') );
			echo CHtml::closeTag('div');
		
		
		$this->endWidget(); 
	?>
	</div>
	<?php if( !Yii::app()->user->id ){ ?><div data-modal="#logIn" class="disableBlock arcticmodal"></div><?php } ?>
</section>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";

		ns.wCalendarSingleSubscriptionFormWidget = wCalendarSingleSubscriptionFormWidgetOpen({
			selector: nsText+' .<?=$ins?>',
			subscribeUrl: '<?=Yii::app()->createUrl('calendarEvent/ajaxSubscribeSingleEvent')?>',
			unSubscribeUrl: '<?=Yii::app()->createUrl('calendarEvent/ajaxUnSubscribeSingleEvent')?>',
		});
	}( window.jQuery );
</script>