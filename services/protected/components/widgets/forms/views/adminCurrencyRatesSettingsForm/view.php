<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminCommonSettingsFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
?>
<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, 'Editor settings' )?></h3>

		<?=$form->textFieldRow( $model, 'ogListImage', Array( 'class' => "{$ins} wFullWidth" ) )?>
		<?=$form->textFieldRow( $model, 'tomorrowOgListImage', Array( 'class' => "{$ins} wFullWidth" ) )?>
		<?=$form->textFieldRow( $model, 'archiveOgImage', Array( 'class' => "{$ins} wFullWidth" ) )?>
		<?=$form->textFieldRow( $model, 'converterOgImage', Array( 'class' => "{$ins} wFullWidth" ) )?>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' class="active"' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<li<?=$class?>>
					<a href="#tab<?=$title?>" data-toggle="tab">
						<?=$title?>
					</a>
				</li>
			<?}?>
		</ul>
		
		<div class="tab-content">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' active' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<?php $accordionParentId = "tab$title"; ?>
				<div class="tab-pane<?=$class?> accordion" id="tab<?=$title?>">
					<?php
						foreach( $model->textFields as $field => $fieldType ){
							if( $field == 'archiveTitle' || $field == 'defaultSingleArchiveYearTitle' || $field == 'title' || $field == 'tomorrowTitle' || $field == 'converterTitle' || $field == 'ecbConverterTitle' || $field == 'ecbTitle' || $field == 'ecbArchiveTitle' || $field == 'ecbDefaultSingleArchiveYearTitle' ) {
								
								if( $field == 'archiveTitle' ) $accordionTitle = Yii::t( $NSi18n, 'Archive settings' );
								if( $field == 'defaultSingleArchiveYearTitle' ) $accordionTitle = Yii::t( $NSi18n, 'Single archive settings' );
								if( $field == 'title' ) $accordionTitle = Yii::t( $NSi18n, 'Index page settings' );
								if( $field == 'tomorrowTitle' ) $accordionTitle = Yii::t( $NSi18n, 'Tomorrow page settings' );
								if( $field == 'converterTitle' ) $accordionTitle = Yii::t( $NSi18n, 'CBR Converter page settings' );
								if( $field == 'ecbConverterTitle' ) $accordionTitle = Yii::t( $NSi18n, 'ECB Converter page settings' );
								if( $field == 'ecbTitle' ) $accordionTitle = Yii::t( $NSi18n, 'Ecb list page settings' );
								if( $field == 'ecbArchiveTitle' ) $accordionTitle = Yii::t( $NSi18n, 'Ecb Archive settings' );
								if( $field == 'ecbDefaultSingleArchiveYearTitle' ) $accordionTitle = Yii::t( $NSi18n, 'Ecb Single archive settings' );
								
								echo CHtml::openTag( 'div', array( 'class' => 'accordion-group' ) );
									echo CHtml::openTag( 'div', array( 'class' => 'accordion-heading' ) );
										echo CHtml::tag( 
											'div', 
											array( 'data-target' => '#accordion' . $field . $language->id, 'data-toggle' => 'collapse', 'class' => 'accordion-toggle', 'data-parent' => '#' . $accordionParentId ), 
											CHtml::tag('b', array(), $accordionTitle) 
										);
									echo CHtml::closeTag('div');
									echo CHtml::openTag( 'div', array( 'class' => 'accordion-body collapse', 'id' => 'accordion' . $field . $language->id ) );
										echo CHtml::openTag( 'div', array( 'style' => 'padding-left:10px;' ) );
							};
							
							echo $form->{$fieldType}( $model, "{$field}[{$language->id}]", Array( 'class' => "{$ins} wFullWidth w{$field} w{$language->id}" ));
							
							if( $field == 'archiveYearMoDayFullDesc' || $field == 'defaultSingleArchiveYearMoDayFullDesc' || $field == 'fullDesc' || $field == 'tomorrowFullDesc' || $field == 'converterSubTitle2' || $field == 'ecbConverterSubTitle2' || $field == 'ecbFullDesc' || $field == 'ecbArchiveYearMoDayFullDesc' || $field == 'ecbDefaultSingleArchiveYearMoDayFullDesc' ) {
								echo CHtml::closeTag('div');
								echo CHtml::closeTag('div');
								echo CHtml::closeTag('div');
							}
							
						}	
					?>
				</div>
			<?}?>
		</div>
		
		


<div class="accordion-group">
	<div class="accordion-heading">
		<div data-target="#collapseTradePlatforms"  data-toggle="collapse" class="accordion-toggle">
			<b>Информация по заменам</b>
		</div>
	</div>
	<div class="accordion-body collapse" id="collapseTradePlatforms" style="">
		<div style="padding-left:10px;">
		
<pre>
для 
	Archive Year Title
	Archive Year Meta Title
	Archive Year Meta Desc
	Archive Year Meta Keys
	Archive Year Description
	Archive Year Full Desc
	Archive Year Mo Title
	Archive Year Mo Meta Title
	Archive Year Mo Meta Desc
	Archive Year Mo Meta Keys
	Archive Year Mo Description
	Archive Year Mo Full Desc
	Archive Year Mo Day Title
	Archive Year Mo Day Meta Title
	Archive Year Mo Day Meta Desc
	Archive Year Mo Day Meta Keys
	Archive Year Mo Day Description
	Archive Year Mo Day Full Desc
	
	Default Single Archive Year Title
	Default Single Archive Year Meta Title
	Default Single Archive Year Meta Desc
	Default Single Archive Year Meta Keys
	Default Single Archive Year Description
	Default Single Archive Year Full Desc
	Default Single Archive Year Mo Title
	Default Single Archive Year Mo Meta Title
	Default Single Archive Year Mo Meta Desc
	Default Single Archive Year Mo Meta Keys
	Default Single Archive Year Mo Description
	Default Single Archive Year Mo Full Desc
	Default Single Archive Year Mo Day Title
	Default Single Archive Year Mo Day Meta Title
	Default Single Archive Year Mo Day Meta Desc
	Default Single Archive Year Mo Day Meta Keys
	Default Single Archive Year Mo Day Description
	Default Single Archive Year Mo Day Full Desc
	
дату можно указать так {$date(yyyy)}
где yyyy - формат даты

<a href="http://www.unicode.org/reports/tr35/tr35-dates.html#Date_Format_Patterns" target="_blank">Date Format Patterns</a>

$_formatters из класса CDateFormatter

private static $_formatters=array(
		'G'=>'formatEra',
		'y'=>'formatYear',
		'M'=>'formatMonth',
		'L'=>'formatMonth',
		'd'=>'formatDay',
		'h'=>'formatHour12',
		'H'=>'formatHour24',
		'm'=>'formatMinutes',
		's'=>'formatSeconds',
		'E'=>'formatDayInWeek',
		'c'=>'formatDayInWeek',
		'e'=>'formatDayInWeek',
		'D'=>'formatDayInYear',
		'F'=>'formatDayInMonth',
		'w'=>'formatWeekInYear',
		'W'=>'formatWeekInMonth',
		'a'=>'formatPeriod',
		'k'=>'formatHourInDay',
		'K'=>'formatHourInPeriod',
		'z'=>'formatTimeZone',
		'Z'=>'formatTimeZone',
		'v'=>'formatTimeZone',
	);
	

для
	Default Single Archive Year Title
	Default Single Archive Year Meta Title
	Default Single Archive Year Meta Desc
	Default Single Archive Year Meta Keys
	Default Single Archive Year Description
	Default Single Archive Year Full Desc
	Default Single Archive Year Mo Title
	Default Single Archive Year Mo Meta Title
	Default Single Archive Year Mo Meta Desc
	Default Single Archive Year Mo Meta Keys
	Default Single Archive Year Mo Description
	Default Single Archive Year Mo Full Desc
	Default Single Archive Year Mo Day Title
	Default Single Archive Year Mo Day Meta Title
	Default Single Archive Year Mo Day Meta Desc
	Default Single Archive Year Mo Day Meta Keys
	Default Single Archive Year Mo Day Description
	Default Single Archive Year Mo Day Full Desc	
	
доступны замены
{$code}	USD
{$name}	Доллар США

</pre>
		
		</div>
	</div>
</div>
		

		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Save' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Reset' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wReset",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonSettingsFormWidget = wAdminCommonSettingsFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminCommonSettingsFormWidget',
			ajax: ajax,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl('admin/currencyRates/ajaxSettingsUpdate')?>',
		});
	}( window.jQuery );
</script>