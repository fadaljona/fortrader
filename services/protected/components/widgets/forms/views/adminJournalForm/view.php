<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminJournalFormWidget.js" );
	
	Yii::app()->clientScript->registerCss('JournalFormWidgetCss', '
		.wTabInner tr:nth-of-type(2) .iActions .wMoveUp, .wTabInner tr:last-of-type .iActions .wMoveDown{
			display:none;
		}
	');
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New journal' : 'Editor journal';
	
	$jsls = Array(
		'lNew' => 'New journal',
		'lEdit' => 'Editor journal',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lEditInner' => 'Edit',
		'lAddInner' => 'Add',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$years = range( 1971, date( "Y" ) );
	$years = array_combine( $years, $years );
?>
<div class="well pull-left iFormWidget i01 wAdminJournalFormWidget wAdminFullWidthForm">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<li class="active"><a href="#tabJournal" data-toggle="tab"><?=Yii::t( $NSi18n, 'Journal' )?></a></li>
			<li><a href="#tabInIssue" data-toggle="tab"><?=Yii::t( $NSi18n, 'In issue' )?></a></li>
		</ul>
		
		<div class="tab-content" style="position:static">
			<div class="tab-pane active" id="tabJournal">
				<?=$form->textFieldRow( $model, 'number', Array( 'class' => "{$ins} wNumber" ))?>
				<?=$form->dateFieldRow( $model, 'date', Array( 'class' => "{$ins} wDate" ))?>
				<?=$form->textFieldRow( $model, 'slug', Array( 'class' => "wFullWidth {$ins} wslug" ))?>
				
				<label for="<?=$model->resolveID( "isDigest" )?>">
					<?=$form->checkBox( $model, "isDigest", Array( 'class' => "{$ins} wisDigest " ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( "isDigest" )?>
					</span>
				</label><br />
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?php
					$formTextFields = AdminJournalFormModel::getTextFields();
					foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
							
							<?php
								foreach( $formTextFields as $key => $settings ){
									$classW = isset( $settings['class'] ) ? $settings['class'] : '';
									echo $form->{$settings['type']}( $model, $key . "[{$language->id}]", Array( 'class' => "$classW {$ins} w$key w{$language->id}" ));
								}
							?>
											
							<br>
						
							<a href="#" class="<?=$ins?> wCoverHref w<?=$language->id?>" target="_blank"></a>
							<?=$form->fileFieldRow( $model, "covers[{$language->id}]" )?>
							
							<br>
							<a href="#" class="<?=$ins?> wJournalHref w<?=$language->id?>" target="_blank"></a>
							<?=$form->fileFieldRow( $model, "journals[{$language->id}]" )?>
							
							<br><br>
							<label for="<?=$model->resolveID( "frees[{$language->id}]" )?>">

								<?=$form->checkBox( $model, "frees[{$language->id}]", Array( 'class' => "{$ins} wFree w{$language->id}" ))?>
								<span class="lbl">
									<?=$model->getAttributeLabel( "frees[{$language->id}]" )?>
								</span>
							</label>
							
						</div>
					<?}?>
				</div>
				
			</div>
			<div class="tab-pane" id="tabInIssue">
				<ul class="nav nav-tabs <?=$ins?> wTabsInner">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab2<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab2<?=$title?>">
							<label><?=Yii::t( $NSi18n, "Header" )?></label>
							<?=CHtml::textField( "headers[{$language->id}]", "", Array( 'class' => "{$ins} wHeader w{$language->id}" ))?>
							<label><?=Yii::t( $NSi18n, "Content" )?></label>
							<?=CHtml::textField( "contents[{$language->id}]", "", Array( 'class' => "{$ins} wContent w{$language->id}" ))?>
							
							<label><?=Yii::t( $NSi18n, "Inner url" )?></label>
							<?=CHtml::textField( "innerUrl[{$language->id}]", "", Array( 'class' => "{$ins} wInnerUrl w{$language->id}" ))?>
							
							<?//=$form->hiddenField( $model, 'inner', Array( 'class' => "{$ins} wInner" ))?>
							
							<?=$form->hiddenField( $model, "inner[{$language->id}]", Array( 'class' => "{$ins} wInner w{$language->id}" ))?>
							
							
							<table class="table i01 table-bordered <?=$ins?> wTabInner w<?=$language->id?>" style="margin-top:10px;">
								<tbody>
									<tr class="wTpl" style="display:none;">
										<td class="wTDName"></td>
										<td class="iActions">
											<a href="#" class="icon-arrow-up wMoveUp wMoveInner" data-langId="<?=$language->id?>"></a>
											<a href="#" class="icon-arrow-down wMoveDown wMoveInner" data-langId="<?=$language->id?>"></a>
										</td>
										<td class="iActions">
											<a href="#" class="icon-edit wEdit" data-langId="<?=$language->id?>"></a>
										</td>
										<td class="iActions">
											<a href="#" class="icon-trash wDelete" data-langId="<?=$language->id?>"></a>
										</td>
									</tr>
								</tbody>
							</table>
							<?
								$this->widget( 'bootstrap.widgets.TbButton', Array( 
									'label' => Yii::t( $NSi18n, 'Add' ),
									'size' => 'mini',
									'htmlOptions' => Array(
										'class' => "{$ins} wAddInner",
										'data-langId' => $language->id,
									),
								));
							?>
							<?
								$this->widget( 'bootstrap.widgets.TbButton', Array( 
									'label' => Yii::t( $NSi18n, 'Cancel' ),
									'size' => 'mini',
									'htmlOptions' => Array(
										'class' => "{$ins} wCancelEditInner",
										'data-langId' => $language->id,
										'style' => "display:none;"
									),
								));
							?>
						</div>
					<?}?>
					
					<div class="clear"></div>
				</div>
			
			</div>
		</div>
        <label for="<?=$model->resolveID( "status" )?>">
            <?=$form->checkBox( $model, "status", Array( 'class' => "{$ins} wStatus w{$language->id}" ))?>
            <span class="lbl">
                <?=$model->getAttributeLabel( "status" )?>
            </span>
        </label>

        <label for="<?=$model->resolveID( "push_notification" )?>">
            <?=$form->checkBox( $model, "push_notification", Array( 'class' => "{$ins} wPush_notification w{$language->id}" ))?>
            <span class="lbl">
                    <?=$model->getAttributeLabel( "push_notification" )?>
                </span>
        </label>
		
		<label for="AdminJournalFormModel_send_now">
            <input type="checkbox" value="1" id="AdminJournalFormModel_send_now" name="AdminJournalFormModel[send_now]" class="insAdminJournalFormWidget w0">
            <span class="lbl">
                    <?=Yii::t( '*', 'Send E-mail notification' )?>
                </span>
        </label>

		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
		<iframe name="iframeForJournal" id="iframeForJournal" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var idLanguage = <?=LanguageModel::getCurrentLanguageID()?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminJournalFormWidget = wAdminJournalFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminJournalFormWidget',
			ajax: ajax,
			hidden: hidden,
			idLanguage: idLanguage,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl('admin/journal/iframeAdd')?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl('admin/journal/ajaxDelete')?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl('admin/journal/ajaxLoad')?>',
			langs: <?= '[' . implode(',', array_keys($languages)) . ']'?>,
			textFields: <?=json_encode(array_keys($formTextFields))?>,
		});
	}( window.jQuery );
</script>
