<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminEAFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New EA' : 'Editor EA';
	
	$types = Array( 'Commercial', 'Free' );
	$types = array_combine( $types, $types );
	foreach( $types as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$statuses = Array( 'Running', 'Discontinued' );
	$statuses = array_combine( $statuses, $statuses );
	foreach( $statuses as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$params = Array( 'MON', 'LAB', 'DS', 'RS' );
	$params = array_combine( $params, $params );
	foreach( $params as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$jsls = Array(
		'lNew' => 'New EA',
		'lEdit' => 'Editor EA',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminEAFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<?=$form->textFieldRow( $model, 'name', Array( 'class' => "{$ins} wName" ))?>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' class="active"' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<li<?=$class?>>
					<a href="#tab<?=$title?>" data-toggle="tab">
						<?=$title?>
					</a>
				</li>
			<?}?>
		</ul>
		<div class="tab-content">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' active' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
					<?=$form->textAreaRow( $model, "abouts[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wAbout w{$language->id}", 'rows' => 20 ))?>
				</div>
			<?}?>
		</div>
		
		<?=$form->textFieldRow( $model, 'nameUser', Array( 'class' => "{$ins} wNameUser" ))?>
		<?=$form->textFieldRow( $model, 'creator', Array( 'class' => "{$ins} wCreator" ))?>
		<?=$form->textFieldRow( $model, 'license', Array( 'class' => "{$ins} wLicense" ))?>
		<?=$form->dropDownListRow( $model, 'type', $types, Array( 'class' => "{$ins} wType" ))?>
		<?=$form->dropDownListRow( $model, 'status', $statuses, Array( 'class' => "{$ins} wStatus" ))?>
		
		<br>
		<a href="#" class="<?=$ins?> wImageHref" target="_blank"></a>
		<?=$form->fileFieldRow( $model, 'image' )?>
		
		<?=$form->dateFieldRow( $model, 'date', Array( 'class' => "{$ins} wDate" ))?>
		<?=$form->textFieldRow( $model, 'desc', Array( 'class' => "{$ins} wDesc" ))?>
		
		<?=$form->checkBoxListRow( $model, 'idsTradePlatforms', $tradePlatforms, Array( 'class' => "{$ins} wIDsTradePlatforms" ))?>
		<br>
		<?=$form->checkBoxListRow( $model, 'params', $params, Array( 'class' => "{$ins} wParams" ))?>
				
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
		<iframe name="iframeForEA" id="iframeForEA" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		var defaultNameUser = "<?=Yii::App()->user->getModel()->user_login?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminEAFormWidget = wAdminEAFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminEAFormWidget',
			ajax: ajax,
			hidden: hidden,
			defaultNameUser: defaultNameUser,
			ls: ls,
		});
	}( window.jQuery );
</script>