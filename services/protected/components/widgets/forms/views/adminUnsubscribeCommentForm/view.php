<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminUnsubscribeCommentFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New comment' : 'Editor comment';
	
	$statuses = Array(	'New', 'Done', 'Need answer' );
	$statuses = array_combine( $statuses, $statuses );
	foreach( $statuses as &$ls ) $ls = Yii::t( 'models/unsubscribeComment', $ls ); unset($ls);
	
	$jsls = Array(
		'lNew' => 'New comment',
		'lEdit' => 'Editor comment',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminUnsubscribeCommentFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		<?=$form->textFieldRow( $model, 'login', Array( 'class' => "{$ins} wLogin" ))?>
		<?=$form->dropDownListRow( $model, 'idMailingType', $types, Array( 'class' => "{$ins} wIDMailingType" ))?>
		<?=$form->textAreaRow( $model, 'comment', Array( 'class' => "input-xxlarge {$ins} wComment", 'rows' => "5" ))?>
		<?=$form->dropDownListRow( $model, 'status', $statuses, Array( 'class' => "{$ins} wStatus" ))?>
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminUnsubscribeCommentFormWidget = wAdminUnsubscribeCommentFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminUnsubscribeCommentFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>