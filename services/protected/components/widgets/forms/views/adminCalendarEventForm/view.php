<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminCalendarEventFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New event' : 'Editor event';
	
	$jsls = Array(
		'lNew' => 'New event',
		'lEdit' => 'Editor event',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminCalendarEventFormWidget  wAdminFullWidthForm">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>

		<?=$form->textFieldRow( $model, 'currency', Array( 'class' => "{$ins} wCurrency" ))?>
		<?=$form->textFieldRow( $model, 'slug', Array( 'class' => "{$ins} wslug" ))?>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' class="active"' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<li<?=$class?>>
					<a href="#tab<?=$title?>" data-toggle="tab">
						<?=$title?>
					</a>
				</li>
			<?}?>
		</ul>
		<div class="tab-content">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' active' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
					<?=$form->textFieldRow( $model, "indicators[{$language->id}]", Array( 'class' => "{$ins} wIndicator wFullWidth w{$language->id}" ))?>
					
					<?=$form->textFieldRow( $model, "pageTitle[{$language->id}]", Array( 'class' => "{$ins} wpageTitle wFullWidth w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "metaTitle[{$language->id}]", Array( 'class' => "{$ins} wmetaTitle wFullWidth w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "metaDesc[{$language->id}]", Array( 'class' => "{$ins} wmetaDesc wFullWidth w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "metaKeys[{$language->id}]", Array( 'class' => "{$ins} wmetaKeys wFullWidth w{$language->id}" ))?>
					
					<?=$form->textFieldRow( $model, "chartTitle[{$language->id}]", Array( 'class' => "{$ins} wchartTitle wFullWidth w{$language->id}" ))?>
					<?=$form->textFieldRow( $model, "chartSubTitle[{$language->id}]", Array( 'class' => "{$ins} wchartSubTitle wFullWidth w{$language->id}" ))?>
					
					<?=$form->textAreaRow( $model, "fullDesc[{$language->id}]", Array( 'class' => "{$ins} wfullDesc wFullWidth w{$language->id}" ))?>
					
					<?=$form->textAreaRow( $model, "descriptions[{$language->id}]", Array( 'class' => "{$ins} wDescription w{$language->id} input-xxlarge", 'rows' => 5 ))?>
				</div>
			<?}?>
		</div>
		
		<?=$form->hiddenField( $model, 'indicator_id', Array( 'class' => "{$ins} wIndicatorId" ))?>
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
		/*		$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));*/
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCalendarEventFormWidget = wAdminCalendarEventFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminCalendarEventFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>
