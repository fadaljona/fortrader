<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminAdvertisementSettingsFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminAdvertisementSettingsFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, 'Editor settings' )?></h3>
		
		<label for="<?=$model->resolveID( 'endBanalce' )?>">
			<?=$form->checkBox( $model, 'endBanalce' )?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'endBanalce' )?>
			</span>
		</label>
		<label for="<?=$model->resolveID( 'endDate' )?>">
			<?=$form->checkBox( $model, 'endDate' )?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'endDate' )?>
			</span>
		</label>
		<label for="<?=$model->resolveID( 'sendEmail' )?>">
			<?=$form->checkBox( $model, 'sendEmail' )?>
			<span class="lbl">
				<?=$model->getAttributeLabel( 'sendEmail' )?>
			</span>
		</label>
		
		<?=$form->textFieldRow( $model, 'emails' )?>
		
		<?=$form->textFieldRow( $model, 'ad_prefix' )?>
		
		<?=$form->textFieldRow( $model, 'popupDelay' )?>
		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Save' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Reset' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wReset",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminAdvertisementSettingsFormWidget = wAdminAdvertisementSettingsFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminAdvertisementSettingsFormWidget',
			ajax: ajax,
			ls: ls,
		});
	}( window.jQuery );
</script>