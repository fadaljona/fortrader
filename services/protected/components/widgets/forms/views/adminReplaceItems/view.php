<?php

Yii::App()->clientScript->registerScriptFile(Yii::app()->baseUrl."/js/widgets/forms/wAdminReplaceItemsWidget.js");
$ns = $this->getNS();
$ins = $this->getINS();
$NSi18n = $this->getNSi18n();

$langOptions = '';
foreach ($langs as $language) {
    $title = str_replace("EN_US", "EN", strtoupper($language->alias));
    $langOptions .= '<option value="' . $language->id . '">' . $title . '</option>';
}

$tableStr = '
<tr class="multiple-input-list__item">
    <td class="list-cell__modelClass">
        <div class="form-group field-' . get_class($model) . '-'. $field .'-{{index}}-modelClass">
            <select id="'. get_class($model) .'-'. $field .'-{{index}}-modelClass" class="form-control selectModelClass" name="'. get_class($model) .'['. $field .'][{{index}}][modelClass]" style="width:100%;">
                <option value="">'. Yii::t($NSi18n, 'Empty') .'</option>
                ' . $modelOptions . '
            </select>
        </div>
    </td>
    <td class="list-cell__object">
        <div class="form-group field-' . get_class($model) . '-'. $field .'-{{index}}-object">
            <select id="'. get_class($model) .'-'. $field .'-{{index}}-object" class="form-control selectObject" name="'. get_class($model) .'['. $field .'][{{index}}][object]"  style="width:100%;">
                <option value="">'. Yii::t($NSi18n, 'Empty') .'</option>
            </select>
        </div>
    </td>
    <td class="list-cell__objectName">
        <div class="form-group field-'. get_class($model) .'-'. $field .'-{{index}}-objectName">
            <input name="'. get_class($model) .'['. $field .'][{{index}}][objectName]" id="'. get_class($model) .'-'. $field .'-{{index}}-objectName" type="text"  style="width:95%;" class="objectNameInp">
        </div>
    </td>
    <td class="list-cell__whereToSearch">
        <div class="form-group field-'. get_class($model) .'-'. $field .'-{{index}}-whereToSearch">
            <select id="'. get_class($model) .'-'. $field .'-{{index}}-whereToSearch" class="form-control selectWhereToSearch" name="'. get_class($model) .'['. $field .'][{{index}}][whereToSearch]"  style="width:100%;">
                ' . $whereToSearchOptions . '
            </select>
        </div>
    </td>
    <td class="list-cell__lang">
        <div class="form-group field-'. get_class($model) .'-'. $field .'-{{index}}-lang">
            <select id="'. get_class($model) .'-'. $field .'-{{index}}-lang" class="form-control selectLang" name="'. get_class($model) .'['. $field .'][{{index}}][lang]"  style="width:100%;">
                ' . $langOptions . '
            </select>
        </div>
    </td>
    <td class="list-cell__button">
        <div class="btn multiple-input-list__btn js-input-plus btn btn-danger">
            <i class="fa fa-remove"></i>
        </div>
    </td>
</tr>';

?>

<div class="<?= get_class($model) ?>-<?= $field ?>-wrapper">
    <h3><?= Yii::t($NSi18n, 'Replaces') ?></h3>

    <div class="form-group field-<?= $field ?>">
        <input name="<?= get_class($model) ?>[<?= $field ?>]" type="hidden">
        <div class="multiple-input">
            <table class="multiple-input-list table table-condensed table-renderer">
                <thead>
                    <tr>
                        <th class="list-cell__modelClass"><?= Yii::t($NSi18n, 'Model') ?></th>
                        <th class="list-cell__object">Object</th>
                        <th class="list-cell__objectName">Object name</th>
                        <th class="list-cell__whereToSearch">Where to search</th>
                        <th class="list-cell__lang">Lang</th>
                        <th class="list-cell__button"></th>
                    </tr>
                </thead>
                <tbody>
                    <?= str_replace(['{{index}}', 'fa fa-remove', 'btn-danger'], ['0', 'fa fa-plus', 'btn-default'], $tableStr) ?>
                </tbody>
            </table>
        </div>
    </div>    
</div>


<script>
!function( $ ) {
    var ns = <?=$ns?>;
    ns.wAdminReplaceItemsWidget = wAdminReplaceItemsWidget({
        row: '<?= str_replace(PHP_EOL, '', $tableStr)?>',
        selector: '.<?= get_class($model) ?>-<?= $field ?>-wrapper',
        objectOptions: <?= json_encode($objectOptions) ?>,
        field: '<?= $field ?>',
        emptyOption: '<option value=""><?= Yii::t($NSi18n, 'Empty') ?></option>',
    });
}( window.jQuery );
</script>