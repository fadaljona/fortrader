<?php
	$cs=Yii::app()->getClientScript();
	$inputmaskBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/inputmask' );
	$cs->registerScriptFile($inputmaskBaseUrl.'/inputmask.js', CClientScript::POS_END);
	$cs->registerScriptFile($inputmaskBaseUrl.'/inputmask.date.extensions.js', CClientScript::POS_END);
	$cs->registerScriptFile($inputmaskBaseUrl.'/jquery.inputmask.js', CClientScript::POS_END);

	Yii::app()->clientScript->registerScript('accountNumJs', '
	!function( $ ) {
		if($(".number_card").length){
			$(".number_card ").inputmask("9{3,15}");
		}
	}( window.jQuery );
	', CClientScript::POS_END);
	
	$jQueryFormStylerBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/jQueryFormStyler' );
	$cs->registerScriptFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.js', CClientScript::POS_END);
	$cs->registerCssFile($jQueryFormStylerBaseUrl.'/jquery.formstyler.css');
	
	Yii::app()->clientScript->registerScript('jsStyler', '
		!function( $ ) {
			var $forms = $(".wAccountContestMemberFormWidget .select_type_idServer");
			if( $forms.length ){
				$forms.styler({});
			}
			
			var $reportFile = $(".wAccountContestMemberFormWidget .file_style1");
			if( $reportFile.length ){
				$reportFile.styler({});
			}
			
			var $accountType = $(".wAccountContestMemberFormWidget .select_type_accountType");
			if( $accountType.length ){
				var accountTypePrevVal = $accountType.val();
				$accountType.styler({
					onSelectClosed: function() {
						if( $accountType.val() !=  accountTypePrevVal ){
							accountTypePrevVal = $accountType.val();
							nsActionView.wRegistrationContestMemberWizardWidget.accountTypeChanged();
							setPaddingLeft10();
						}
					}
				});
			}
			
			setPaddingLeft10();
			
			function setPaddingLeft10(){
				$(".wAccountContestMemberFormWidget .mainFildsWrapper .regestration_box_50").removeClass("paddingLeft10");
				
				var i=0;
				$(".wAccountContestMemberFormWidget .mainFildsWrapper .regestration_box_50").each(function() {
					if( $(this).css("display") != "none" ){
						if( i == 0  ){
							i = 1;
						}else if( i == 1 ){
							$(this).addClass("paddingLeft10");
							i = 0;
						}
					}
					
				});
			}
			
		}( window.jQuery );
	', CClientScript::POS_END);
	
	
$ns     = $this->getNS();
$ins    = $this->getINS();
$NSi18n = $this->getNSi18n();

if( $UMode == 'editRegister' ){
	$buttonText = Yii::t( $NSi18n, 'Edit' );
}else{
	$buttonText = Yii::t( $NSi18n, 'Register' );
}
if( !$contest ) $buttonText = Yii::t( $NSi18n, 'Add account' );
?>
<div class="wAccountContestMemberFormWidget">
	<?php
		$form = $this->beginWidget( 'widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal', 'htmlOptions' => array( 'class' => 'regestration clearfix' ) ) );
			if( $contest )
				echo Yii::t( $NSi18n, 'Next, you need to manually open an account under the terms of the competition and enter the account information below.' );
			
			echo CHtml::tag('div', array( 'class' => 'form-error' ), '');
			
			echo CHtml::openTag('div', array( 'class' => 'mainFildsWrapper' ));	
			
				if( !$contest && $type != 'frameForForum'  ){
					echo CHtml::openTag('div', array( 'class' => 'regestration_box_50' ));	
						echo CHtml::tag('p', array('class'=>'name_input'), $model->getAttributeLabel( "accountType" ) );
						echo CHtml::tag( 'div', array('class'=>''), $form->dropDownList( $model, 'accountType', $accountTypes, array('class'=>'select_type1 select_type_accountType') ) );
					echo CHtml::closeTag('div');	
					
					echo CHtml::openTag('div', array( 'class' => 'regestration_box_50', 'style' => 'display:none;' ));	
						echo CHtml::tag('p', array('class'=>'name_input'), $model->getAttributeLabel( "reportFile" ) );
						echo CHtml::tag( 'div', array('class'=>''), $form->fileField( $model, 'reportFile', Array( 'class' => "file_style1" )) );
						echo '<iframe name="iframeForReportSubmit" id="iframeForReportSubmit" style="display:none"></iframe>';
					echo CHtml::closeTag('div');	

				}
				
				echo CHtml::openTag('div', array( 'class' => 'regestration_box_50' ));	
					echo CHtml::tag('p', array('class'=>'name_input'), $model->getAttributeLabel( "accountNumber" ) );
					echo $form->textField( $model, "accountNumber", Array( 'class' => 'number_card' ));
				echo CHtml::closeTag('div');	
				
				echo CHtml::openTag('div', array( 'class' => 'regestration_box_50' ));	
					echo CHtml::tag('p', array('class'=>'name_input'), $model->getAttributeLabel( "investorPassword" ) );
					echo $form->textField( $model, "investorPassword", Array( 'class' => 'password' ));
				echo CHtml::closeTag('div');	
					
				echo CHtml::openTag('div', array( 'class' => 'regestration_box_50' ));	
					echo CHtml::tag('p', array('class'=>'name_input'), $model->getAttributeLabel( "idServer" ) );
					echo CHtml::tag( 'div', array('class'=>''), $form->dropDownList( $model, 'idServer', $servers, array('class'=>'select_type1 select_type_idServer') ) );
				echo CHtml::closeTag('div');	
				
				if( $contest ){
					echo CHtml::openTag('div', array( 'class' => 'regestration_box_50 conditions_checkbox_box' ));	
						echo CHtml::openTag('div', array( 'class' => 'checkbox_box' ));
							echo CHtml::checkBox('conditions', false, array( 'id' => 'columns_show1', 'class' => 'wconditions' ));
							echo CHtml::tag( 'label', array( 'for' => 'columns_show1', 'class' => 'square_input' ), '<i></i>' . Yii::t( $NSi18n, '<a href="{link}" target="_blank">The terms</a> have read', array( '{link}' => $contest->urlRules ) ) );
						echo CHtml::closeTag('div');
					echo CHtml::closeTag('div');
					
					echo $form->hiddenField( $model, 'monitoringType', Array( 'value' => 'contest' ) );
				}
				
				if( !$contest ){
					echo CHtml::openTag('div', array( 'class' => 'regestration_box_50 ' ));	
						echo CHtml::tag('p', array('class'=>'name_input'), $model->getAttributeLabel( "accountTitle" ) );
						echo $form->textField( $model, "accountTitle", Array( 'class' => 'password' ));
					echo CHtml::closeTag('div');

					echo $form->hiddenField( $model, 'monitoringType', Array( 'value' => 'monitoring' ) );
				}
			
			echo CHtml::closeTag('div');
			

			

			echo CHtml::openTag('div', array( 'class' => 'regestration_box_50 regestration_box_button' ));	
				echo CHtml::button( $buttonText, array('class'=>'regestration_btn alignright wSubmit') );
			echo CHtml::closeTag('div');

			if($UMode == "guest") {
				echo $form->hiddenField( $model, 'user_id', Array( 'value' => 0 ) );
			}else{
				echo $form->hiddenField( $model, 'user_id', Array( 'value' => Yii::app()->user->id ) );
			}
			echo $form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ));
		$this->endWidget();	
	?>
</div>