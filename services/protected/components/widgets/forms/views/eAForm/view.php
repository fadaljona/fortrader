<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wEAFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$types = Array( 'Commercial', 'Free' );
	$types = array_combine( $types, $types );
	foreach( $types as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$statuses = Array( 'Running', 'Discontinued' );
	$statuses = array_combine( $statuses, $statuses );
	foreach( $statuses as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$jsls = Array(
		'lSaving' => 'Saving...',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="wEAFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal', 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id' )?>
		<?=$form->textFieldRow( $model, 'name' )?>
		
		<div class="control-group">
			<label class="control-label"><?=Yii::t( 'models/forms/adminEA', 'About' )?>:</label>
			<div class="controls">
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
							<?=$form->textArea( $model, "abouts[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wAbout", 'rows' => 5 ))?>
						</div>
					<?}?>
				</div>
								
			</div>
		</div>
		
		<?=$form->textFieldRow( $model, 'creator' )?>
		<?=$form->dropDownListRow( $model, 'type', $types, Array( 'class' => "{$ins} wType" ))?>
		<div class="<?=$ins?> wLicenseBlock">
			<?=$form->textFieldRow( $model, 'license' )?>
		</div>
		<?=$form->dropDownListRow( $model, 'status', $statuses )?>
		<?=$form->fileFieldRow( $model, 'image' )?>
		<?=$form->textFieldRow( $model, 'desc' )?>
		
		<?=$form->checkBoxListRow( $model, 'idsTradePlatforms', $tradePlatforms )?>
		
		<?
			$this->widget( 'bootstrap.widgets.TbButton', Array( 
				'buttonType' => 'submit', 
				'type' => 'success',
				'label' => Yii::t( $NSi18n, 'Done!' ),
				'htmlOptions' => Array(
					'class' => "pull-right {$ins} wSubmit",
				),
			));
		?>
		<iframe name="iframeForEA" id="iframeForEA" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
				
		var ls = <?=json_encode( $jsls )?>;
				
		ns.wEAFormWidget = wEAFormWidgetOpen({
			ns: ns,
			ins: ins,
			selector: nsText+' .wEAFormWidget',
			ls: ls,
		});
	}( window.jQuery );
</script>