<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wUnsubscribeCommentFormWidget.js" );
	
	$class = $this->getCleanClassName();
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lCancel' => 'Cancel',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	$languageCookieVarName = Yii::App()->params[ 'languageCookieVarName' ];
	
	$baseUrl = Yii::app()->baseUrl;
	$alias = LanguageModel::getCurrentLanguageAlias();
	
	$key = @$_GET['key'];
?>
<div class="wUnsubscribeCommentFormWidget">
	<div class="<?=$ins?> wStage1">
		<div class="text-center">
			<?$src = "{$baseUrl}/assets-static/images/unsubscribe01_{$alias}.png"?>
			<?=CHtml::image( $src )?>
		</div>
		<div>
			<?=Yii::t( $NSi18n, 'unsubscribe_message', Array( '{nameMailingType}' => $type->name ))?>
		</div>

		<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase')?>
			<?if( strlen( $key )){?>
				<?=CHtml::hiddenField( "key", $key )?>
			<?}?>
			<?=$form->hiddenField( $model, 'idMailingType' )?>
			<?=$form->textArea( $model, 'comment', Array( 'class' => 'iSized', 'style' => 'width:100%;', 'rows' => '5' ))?>
						
			<div class="pull-left">
				<button class="btn btn-inverse <?=$ins?> wCancel">
					<?=Yii::t( $NSi18n, 'Changed his mind' )?>
				</button>
			</div>
			
			<div class="pull-right">
				<button class="btn btn-warning <?=$ins?> wSubmit" type="submit">
					<?=Yii::t( $NSi18n, 'Send' )?>
				</button>
			</div>
			
			<div class="iClear"></div>
		<?$this->endWidget()?>
	</div>
	<div class="<?=$ins?> wStage2" style="display:none;">
		<div class="text-center">
			<?$src = "{$baseUrl}/assets-static/images/motivation01.png"?>
			<?=CHtml::image( $src )?>
		</div>
		<div>
			<?=Yii::t( $NSi18n, 'unsubscribe_comment_message' )?>
		</div>
		
		<div class="pull-left">
			<button class="btn btn-inverse <?=$ins?> wCancel">
				<?=Yii::t( $NSi18n, 'Changed his mind' )?>
			</button>
		</div>
		<div class="iClear"></div>
	</div>
	<div class="<?=$ins?> wStage3" style="display:none;">
		<div class="text-center">
			<?$src = "{$baseUrl}/assets-static/images/thanks01.png"?>
			<?=CHtml::image( $src )?>
		</div>
		<div>
			<?=Yii::t( $NSi18n, 'unsubscribe_cancel_thanks' )?>
		</div>
		<div class="iClear"></div>
	</div>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		var idType = <?=$type->id?>;
		var key = "<?=$key?>";
				
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wUnsubscribeCommentFormWidget = wUnsubscribeCommentFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wUnsubscribeCommentFormWidget',
			ls: ls,
			idType: idType,
			key: key,
		});
	}( window.jQuery );
</script>