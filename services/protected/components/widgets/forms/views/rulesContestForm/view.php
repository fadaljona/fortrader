<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$cmbRule = $contest->currentLanguageRule ? ContestModel::parseRule( $contest->currentLanguageRule->text ) : null;
?>
<div class="wRulesContestFormWidget">
	<?if( $cmbRule ){?>
		<form>
			<h3 class="header smaller lighter blue"><?=Yii::t( $NSi18n, 'Contest Rules' )?></h3>
			<?if( is_array( $cmbRule )){?>
				<div id="accordionRules" class="accordion">
					<?foreach( $cmbRule as $i=>$rule ){?>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a href="#collapseRule_<?=$i?>" data-parent="#accordionRules" data-toggle="collapse" class="accordion-toggle collapsed">
									<?=$rule->group?>
								</a>
							</div>
							<div class="accordion-body collapse" id="collapseRule_<?=$i?>">
								<div class="accordion-inner">
									<?=$rule->content?>
								</div>
							</div>
						</div>
					<?}?>
				</div>
			<?}else{?>
				<?=$cmbRule?>
			<?}?>
			<label>
				<input name="agree" type="checkbox" class="wAgree"/>
				<span class="lbl">
					<?=Yii::t( $NSi18n, 'I agree to the contest rules' )?>
				</span>
			</label>
		</form>
	<?}?>
</div>