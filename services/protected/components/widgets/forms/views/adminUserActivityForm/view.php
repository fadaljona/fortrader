<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminUserActivityFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New activity' : 'Editor activity';
	
	$jsls = Array(
		'lNew' => 'New activity',
		'lEdit' => 'Editor activity',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	$icons = UserGroupModel::getAvailableIconClasses();
	sort( $icons );
	$icons = CommonLib::mergeAssocs( Array( "" => "" ), array_combine( $icons, $icons ));
?>
<div class="well pull-left iFormWidget i01 wAdminUserActivityFormWidget">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<?=$form->textFieldRow( $model, "user", Array( 'class' => "{$ins} wUser" ))?>
		
		<?=$form->labelEx( $model, 'icon' )?>
		<i class="<?=$model->icon?> <?=$ins?> iIcon i01 wIcon"></i>
		<?=$form->dropDownList( $model, 'icon', $icons, Array( 'class' => "iSelect i01 {$ins} wIconSelect" ))?>
						
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' class="active"' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<li<?=$class?>>
					<a href="#tab<?=$title?>" data-toggle="tab">
						<?=$title?>
					</a>
				</li>
			<?}?>
		</ul>
		
		<div class="tab-content">
			<?foreach( $languages as $i=>$language ){?>
				<?$class = !$i ? ' active' : ''?>
				<?$title = strtoupper( $language->alias )?>
				<?$title = str_replace( "EN_US", "EN", $title )?>
				<div class="tab-pane<?=$class?>" id="tab<?=$title?>">
					<?=$form->textAreaRow( $model, "messages[{$language->id}]", Array( 'class' => "input-xxlarge {$ins} wMessage w{$language->id}", 'rows' => 5 ))?>
				</div>
			<?}?>
		</div>
				
		<?=$form->textFieldRow( $model, "count", Array( 'class' => "{$ins} wCount" ))?>
		<?=$form->textFieldRow( $model, "url", Array( 'class' => "{$ins} wURL" ))?>
		
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
				
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminUserActivityFormWidget = wAdminUserActivityFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminUserActivityFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>