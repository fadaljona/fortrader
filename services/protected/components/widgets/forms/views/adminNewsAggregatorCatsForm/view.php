<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminNewsAggregatorCatsFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
		'lNew' => 'New category',
		'lEdit' => 'Edit category'
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">
	
	<section class="section_offset">
		<h3 class="<?=$ins?> wTitle"><?=$addLabel?></h3>
		<hr class="separator2">
	</section>
	<div class="section_offset">
		<div class="form_add_in_table">
			<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
			
				<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
				<?=$form->textFieldRow( $model, 'slug', Array( 'class' => "{$ins} wslug" ) )?>
				<?=$form->textFieldRow( $model, 'ogImage', Array( 'class' => "{$ins} wogImage" ) )?>
				<?=$form->textFieldRow( $model, 'order', Array( 'class' => "{$ins} worder" ) )?>
				
				<label for="<?=$model->resolveID( 'enabled' )?>">
					<?=$form->checkBox( $model, 'enabled', Array( 'class' => "{$ins} wenabled" ) )?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'enabled' )?>
					</span>
				</label>
				<br />
				
				
				<ul class="nav nav-tabs <?=$ins?> wTabs">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tab<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?> langTabCats" id="tab<?=$title?>" data-langId="<?=$language->id?>">
							<?php
								foreach( $model->textFields as $field => $data ){
									echo $form->{$data['type']}( $model, "{$field}[{$language->id}]", Array( 'class' => "{$ins} {$data['class']} w{$field} w{$language->id}", 'disabled' => $data['disabled'] ));
								}	
							?>
							
							<hr>
								<h3><?=Yii::t($NSi18n, 'Keywords')?></h3>
								<? //=$form->hiddenField( $model, "keys[{$language->id}]", Array( 'class' => "{$ins} wkeys{$language->id}" ))?>
								
								<label><?=Yii::t( $NSi18n, "Keyword" )?></label>
								<?=CHtml::textField( "key[{$language->id}]", "", Array( 'class' => "{$ins} wkey{$language->id} wFullWidth" ))?>


								<div class="clear"></div>
								<?
									$this->widget( 'bootstrap.widgets.TbButton', Array( 
										'label' => Yii::t( $NSi18n, 'Add' ),
										'size' => 'mini',
										'htmlOptions' => Array(
											'class' => "{$ins} wAddKey",
											'data-langId' => $language->id,
										),
									));
								?>

								<div class="clearfix"></div><br>
								
								<ul class="<?=$ins?> keysUl keysUl<?=$language->id?> pull-left span7 unstyled row-fluid clearfix" data-langid="<?=$language->id?>">
								</ul>

								<div class="clearfix"></div>

							<hr>
							
							
						</div>
					<?}?>
				</div>
				
				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'type' => 'danger',
							'label' => Yii::t( $NSi18n, 'Delete' ),
							'htmlOptions' => Array(
								'class' => "pull-left {$ins} wDelete",
							),
						));
					?>
				</div>
				<div class="clear"></div>
			<?$this->endWidget()?>


		</div>
	</div>

</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = true;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonFormWidget = wAdminNewsAggregatorCatsFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl($submitItemRoute)?>',
			ajaxDeleteURL: '<?=Yii::app()->createUrl($deleteItemRoute)?>',
			ajaxLoadURL: '<?=Yii::app()->createUrl($loadItemRoute)?>',
			errorClass: 'inpError',
			formModelName: '<?=$formModelName?>',
			modelName: '<?=$modelName?>',
		});
	}( window.jQuery );
</script>