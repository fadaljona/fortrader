<?
	Yii::App()->clientScript->registerScriptFile('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js');
	Yii::App()->clientScript->registerCssFile( 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' );
	
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminInformersCategoryNewRawSettingsFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lReseted' => 'Reseted',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);

?>

<div class="well pull-left iFormWidget i01 wAdminCommonSettingsFormWidget wAdminFullWidthForm <?=$ins?>" id="addFrontForm">
	<style>.select2-search:after{content:"";}</style>
	<div class="section_offset">
		<div class="form_add_in_table">
			<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
			
				<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
	
				<?=$form->dropDownListRow( $model, 'rawSettings', $cats, Array( 'class' => "{$ins} wrawSettings", "multiple" => "multiple" ))?>
				
				<div class="clear"></div>
					<span class="errorMess red_color"></span>
				<div class="clear"></div>
				<div class="iControlBlock i01">
					<?
						$this->widget( 'bootstrap.widgets.TbButton', Array( 
							'buttonType' => 'submit', 
							'type' => 'success',
							'label' => Yii::t( $NSi18n, 'Save' ),
							'htmlOptions' => Array(
								'class' => "pull-right {$ins} wSubmit",
							),
						));
					?>
				</div>
				<div class="clear"></div>
				<iframe name="iframeForCategories" id="iframeForCategories" style="display:none"></iframe>
			<?$this->endWidget()?>


		</div>
	</div>

</div>

<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = false;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminCommonFormWidget = wAdminInformersCategoryNewRawSettingsFormWidgetOpen({
			ins: ins,
			selector: nsText+' .<?=$ins?>',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
			ajaxSubmitURL: '<?=Yii::app()->createUrl('admin/informers/ajaxSubmitCategory')?>',
			
			errorClass: 'inpError',
			formModelName: '<?=$formModelName?>',
			modelName: '<?=$modelName?>',
			idInTypes: <?=json_encode($idInTypes)?>,
		});
	}( window.jQuery );
</script>