<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminUserActivitySettingsFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
		'lReseted' => 'Reseted',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminUserActivitySettingsFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, 'Editor settings' )?></h3>
		
		<?=$form->textFieldRow( $model, 'count_user' )?>
		<?=$form->textFieldRow( $model, 'count_contestWiner' )?>
		<?=$form->textFieldRow( $model, 'count_contestMember' )?>
		<?=$form->textFieldRow( $model, 'eval_contestMemberPlace' )?>
		<?=$form->textFieldRow( $model, 'count_message' )?>
		<?=$form->textFieldRow( $model, 'count_brokerReview' )?>
		<?=$form->textFieldRow( $model, 'count_brokerMark' )?>
		<?=$form->textFieldRow( $model, 'count_brokerLink' )?>
		<?=$form->textFieldRow( $model, 'count_EAReview' )?>
		<?=$form->textFieldRow( $model, 'count_EAMark' )?>
		<?=$form->textFieldRow( $model, 'count_downloadEAVersion' )?>
		<?=$form->textFieldRow( $model, 'count_EA' )?>
		<?=$form->textFieldRow( $model, 'count_EAVersion' )?>
		<?=$form->textFieldRow( $model, 'count_EAStatement' )?>
		<?=$form->textFieldRow( $model, 'count_JournalComment' )?>
		<?=$form->textFieldRow( $model, 'count_JournalDownload' )?>
		<?=$form->textFieldRow( $model, 'count_JournalView' )?>
		
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Save' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Reset' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wReset",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminUserActivitySettingsFormWidget = wAdminUserActivitySettingsFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminUserActivitySettingsFormWidget',
			ajax: ajax,
			ls: ls,
		});
	}( window.jQuery );
</script>