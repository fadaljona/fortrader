<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminContestFormWidget.js" );
	Yii::App()->clientScript->registerCssFile( Yii::app()->baseUrl."/css/adminContestsConditions.css" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New contest' : 'Editor contest';
	
	$jsls = Array(
		'lNew' => 'New contest',
		'lEdit' => 'Editor contest',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$statuses = Array(
		'registration' => 'Registration',
		'registration stoped' => 'Registration stoped',
		'started' => 'Started',
		'completed' => 'Completed',
	);
	foreach( $statuses as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$modesEA = Array( 'Allowed', 'Disallowed' );
	$modesEA = array_combine( $modesEA, $modesEA );
	foreach( $modesEA as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$accountOpenings = Array(
		'MetaTrader API' => 'MetaTrader API', 
		'internal mail' => 'Internal mail', 
		'manually trader' => 'Manually trader',
		'list of accounts' => 'List of accounts',
	);
	foreach( $accountOpenings as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminContestFormWidget wAdminFullWidthForm">
	<?$form = $this->beginWidget('widgets.base.BootstrapExFormWidgetBase', Array( 'htmlOptions' => Array( 'enctype' => 'multipart/form-data' )))?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<li class="active"><a href="#tabMain" data-toggle="tab"><?=Yii::t( $NSi18n, 'Main' )?></a></li>
			<li><a href="#tabSettings" data-toggle="tab"><?=Yii::t( $NSi18n, 'Settings' )?></a></li>
			<li><a href="#tabRules" data-toggle="tab"><?=Yii::t( $NSi18n, 'Rules' )?></a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active" id="tabMain">
				<ul class="nav nav-tabs <?=$ins?> wTabsI18N">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tabI18N<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				<div class="tab-content">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tabI18N<?=$title?>">
							<?=$form->textFieldRow( $model, "names[{$language->id}]", Array( 'class' => "{$ins} wName{$language->id}" ))?>
							<?=$form->textAreaRow( $model, "descriptions[{$language->id}]", Array( 'class' => "{$ins} wDescription{$language->id} wFullWidth" ))?>
							
							<?=$form->textAreaRow( $model, "fullDesc[{$language->id}]", Array( 'class' => "{$ins} wfullDesc{$language->id} wFullWidth" ))?>
							
							<?=$form->textFieldRow( $model, "pageTitle[{$language->id}]", Array( 'class' => "{$ins} wpageTitle{$language->id} wFullWidth" ))?>
							<?=$form->textFieldRow( $model, "pageSeoTitle[{$language->id}]", Array( 'class' => "{$ins} wpageSeoTitle{$language->id} wFullWidth" ))?>
							<?=$form->textFieldRow( $model, "seoKeys[{$language->id}]", Array( 'class' => "{$ins} wseoKeys{$language->id} wFullWidth" ))?>
							<?=$form->textFieldRow( $model, "seoDesc[{$language->id}]", Array( 'class' => "{$ins} wseoDesc{$language->id} wFullWidth" ))?>
							
							<br>
							<a href="#" class="<?=$ins?> wImageHref w<?=$language->id?>" target="_blank"></a>
							<?=$form->fileFieldRow( $model, "images[{$language->id}]" )?>
							
							<div class="clear"></div>
						</div>
					<?}?>
				</div>
				<?=$form->textFieldRow( $model, "slug", Array( 'class' => "{$ins} wslug" ))?>
				<?=$form->dropDownListRow( $model, "status", $statuses, Array( 'class' => "{$ins} wStatus" ))?>
				<?=$form->textFieldRow( $model, "startDeposit", Array( 'class' => "{$ins} wStartDeposit" ))?>
				<?=$form->dropDownListRow( $model, "modeEA", $modesEA, Array( 'class' => "{$ins} wModeEA" ))?>
				
				<label for="server"><?=Yii::t( $NSi18n, "Add server" )?></label>
				<?=CHtml::dropDownList( "server", null, $servers, Array( 'class' => "{$ins} wServer" ))?>
				<div class="clear"></div>
				<?
					$this->widget( 'bootstrap.widgets.TbButton', Array( 
						'label' => Yii::t( $NSi18n, 'Add' ),
						'size' => 'mini',
						'htmlOptions' => Array(
							'class' => "{$ins} wAddServer",
						),
					));
				?>
				<div class="clear"></div>
				<?=$form->labelEx( $model, 'servers', Array( 'style' => "padding-top:10px;" ))?>
				<?=$form->hiddenField( $model, 'servers', Array( 'class' => "{$ins} wServers" ))?>
				<table class="table i01 table-bordered <?=$ins?> wTabServers">
					<tbody>
						<tr class="wTpl" style="display:none;">
							<td class="wTDName"></td>
							<td class="iActions">
								<a href="#" class="icon-trash wDelete"></a>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="clear"></div>
				
				<?=$form->dateFieldRow( $model, 'endReg', Array( 'class' => "{$ins} wEndReg" ))?>
				<?=$form->dateFieldRow( $model, 'begin', Array( 'class' => "{$ins} wBegin" ))?>
				<?=$form->dateFieldRow( $model, 'end', Array( 'class' => "{$ins} wEnd" ))?>
				<div class="clear"></div>
				
				<label for="server"><?=Yii::t( $NSi18n, "Add prize" )?></label>
				<?=CHtml::textField( 'prize', null, Array( 'class' => "input-small {$ins} wPrize" ))?>
				<?
					$this->widget( 'bootstrap.widgets.TbButton', Array( 
						'label' => Yii::t( $NSi18n, 'Add' ),
						'size' => 'mini',
						'htmlOptions' => Array(
							'class' => "pull-right {$ins} wAddPrize",
						),
					));
				?>
				<div class="clear"></div>
				
				<?=$form->labelEx( $model, 'prizes' )?>
				<?=$form->hiddenField( $model, 'prizes', Array( 'class' => "{$ins} wPrizes" ))?>
				<div style="max-height:200px; overflow-y:auto;">
					<table class="table i01 table-bordered <?=$ins?> wTabPrizes">
						<tbody>
							<tr class="wTpl" style="display:none;">
								<td class="wTDPrize"></td>
								<td class="iActions">
									<a href="#" class="icon-trash wDelete"></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
			<div class="tab-pane" id="tabSettings">
				<?=$form->radioButtonListRow( $model, "accountOpening", $accountOpenings, Array( 'class' => "{$ins} wAccountOpening" ))?>
				<br>
				<label for="<?=$model->resolveID( 'showGraphLeaders' )?>">
					<?=$form->checkBox( $model, 'showGraphLeaders', Array( 'class' => "{$ins} wShowGraphLeaders" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'showGraphLeaders' )?>
					</span>
				</label>
				<br>
				
				<label for="<?=$model->resolveID( 'stopImport' )?>">
					<?=$form->checkBox( $model, 'stopImport', Array( 'class' => "{$ins} wStopImport" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'stopImport' )?>
					</span>
				</label>
				<br>
				
				<label for="<?=$model->resolveID( 'stopDeleteMMonitoring' )?>">
					<?=$form->checkBox( $model, 'stopDeleteMMonitoring', Array( 'class' => "{$ins} wStopDeleteMMonitoring" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'stopDeleteMMonitoring' )?>
					</span>
				</label>
				<br>
				
				<label><?=Yii::t( $NSi18n, "Control contest members" )?>:</label>
				<label for="<?=$model->resolveID( 'completedMembers' )?>">
					<?=$form->checkBox( $model, 'completedMembers', Array( 'class' => "{$ins} wCompleteMembers" ))?>
					<span class="lbl">
						<?=$model->getAttributeLabel( 'completedMembers' )?>
					</span>
				</label>
			</div>
			<div class="tab-pane" id="tabRules">
				<ul class="nav nav-tabs <?=$ins?> wTabsRules">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' class="active"' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<li<?=$class?>>
							<a href="#tabRule<?=$title?>" data-toggle="tab">
								<?=$title?>
							</a>
						</li>
					<?}?>
				</ul>
				<div class="tab-content <?=$ins?> wTabsRulesContent">
					<?foreach( $languages as $i=>$language ){?>
						<?$class = !$i ? ' active' : ''?>
						<?$title = strtoupper( $language->alias )?>
						<?$title = str_replace( "EN_US", "EN", $title )?>
						<div class="tab-pane<?=$class?>" id="tabRule<?=$title?>" data-langId="<?=$language->id?>">

							<?=$form->hiddenField( $model, "rules[{$language->id}]", Array( 'class' => "{$ins} wrules{$language->id}" ))?>
							
							<hr>
							<label><?=Yii::t( $NSi18n, "Rule" )?></label>
							<?=CHtml::textField( "ruleHeader[{$language->id}]", "", Array( 'class' => "{$ins} wruleHeader{$language->id} wFullWidth" ))?>
							<label><?=Yii::t( $NSi18n, "Rule Value" )?></label>
							<?=CHtml::textField( "ruleValue[{$language->id}]", "", Array( 'class' => "{$ins} wruleValue{$language->id}" ))?>
		
							<?=CHtml::dropDownList( 
								"ruleType[{$language->id}]", 
								'ruleType', 
								array( 'red' => Yii::t( $NSi18n, 'red'), 'green' => Yii::t( $NSi18n, 'green') ), 
								Array( 'options' => array('green'=>array('selected'=>true)), "class" => "{$ins} wruleType{$language->id}" )
							)?>
						

							<div class="clear"></div>
							<?
								$this->widget( 'bootstrap.widgets.TbButton', Array( 
									'label' => Yii::t( $NSi18n, 'Add' ),
									'size' => 'mini',
									'htmlOptions' => Array(
										'class' => "{$ins} wAddRule",
										'data-langId' => $language->id,
									),
								));
							?>

							<div class="clear"></div><br>

							<h5><?=Yii::t( $NSi18n, 'Trading rules' )?></h5>
							
							<ul class="conditions_list_green <?=$ins?> greenList<?=$language->id?> clearfix" data-langid="<?=$language->id?>" id="sortableGreen<?=$language->id?>">
							</ul>

							<ul class="conditions_list_red <?=$ins?> redList<?=$language->id?> clearfix" data-langId="<?=$language->id?>" id="sortableRed<?=$language->id?>">
							</ul>

							<script>$("#sortableGreen<?=$language->id?>").sortable();$("#sortableRed<?=$language->id?>").sortable();</script>

							<div class="clear"></div><br>
							<?=$form->textFieldRow( $model, "urlsRules[{$language->id}]", Array( 'class' => "{$ins} wUrlRules{$language->id}" ))?>
						</div>
					<?}?>
				</div>
			</div>
		</div>
		
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
		<iframe name="iframeForContest" id="iframeForContest" style="display:none"></iframe>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminContestFormWidget = wAdminContestFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminContestFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>