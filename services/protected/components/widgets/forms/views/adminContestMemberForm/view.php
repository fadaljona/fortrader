<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminContestMemberFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New member' : 'Editor member';
	
	$jsls = Array(
		'lNew' => 'New member',
		'lEdit' => 'Editor member',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$statuses = Array( 
		'participates' => 'Participates', 
		'disqualified' => 'Disqualified', 
		'quitted' => 'Quitted',
		'completed' => 'Completed',
	);
	foreach( $statuses as &$name ) $name = Yii::t( $NSi18n, $name ); unset( $name );
?>
<div class="well pull-left iFormWidget i01 wAdminContestMemberFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<?=CHtml::hiddenfield( $model->resolveName( 'idContest' ), $idContest )?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		
		<ul class="nav nav-tabs <?=$ins?> wTabs">
			<li class="active"><a href="#tabUser" data-toggle="tab"><?=Yii::t( $NSi18n, 'User' )?></a></li>
			<li><a href="#tabAccount" data-toggle="tab"><?=Yii::t( $NSi18n, 'Account' )?></a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="tabUser">
				<?=$form->textFieldRow( $model, 'login', Array( 'class' => "{$ins} wLogin" ))?>
				<?=$form->dropDownListRow( $model, 'status', $statuses, Array( 'class' => "{$ins} wStatus" ))?>
			</div>
			<div class="tab-pane" id="tabAccount">
				<?=$form->textFieldRow( $model, 'accountNumber', Array( 'class' => "{$ins} wAccountNumber" ))?>
				<?=$form->textFieldRow( $model, 'investorPassword', Array( 'class' => "{$ins} wInvestorPassword" ))?>
				<?=$form->dropDownListRow( $model, 'idServer', $servers, Array( 'class' => "{$ins} wServer" ))?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminContestMemberFormWidget = wAdminContestMemberFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminContestMemberFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>