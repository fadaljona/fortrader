<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/forms/wAdminTradeAccountFormWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$title = $model->getAR()->isNewRecord ? 'New trade account' : 'Editor trade account';
	
	$jsls = Array(
		'lNew' => 'New trade account',
		'lEdit' => 'Editor trade account',
		'lDeleting' => 'Deleting...',
		'lDeleted' => 'Deleted',
		'lSaving' => 'Saving...',
		'lSaved' => 'Saved',
		'lLoading' => 'Loading...',
		'lDeleteConfirm' => 'Delete?',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$statusues = Array( 'On', 'Off' );
	$statusues = array_combine( $statusues, $statusues );
	foreach( $statusues as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
	
	$types = Array( 'Demo', 'Real' );
	$types = array_combine( $types, $types );
	foreach( $types as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
		
	$groups = Array( 'Contests', 'EA' );
	$groups = array_combine( $groups, $groups );
	foreach( $groups as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>
<div class="well pull-left iFormWidget i01 wAdminTradeAccountFormWidget">
	<?$form = $this->beginWidget('bootstrap.widgets.TbActiveForm')?>
		<?=$form->hiddenField( $model, 'id', Array( 'class' => "{$ins} wID" ))?>
		<h3 class="<?=$ins?> wTitle"><?=Yii::t( $NSi18n, $title )?></h3>
		<?=$form->textFieldRow( $model, 'name', Array( 'class' => "{$ins} wName" ))?>
		<?=$form->textFieldRow( $model, 'accountNumber', Array( 'class' => "{$ins} wAccountNumber" ))?>
		<?=$form->textFieldRow( $model, 'password', Array( 'class' => "{$ins} wPassword" ))?>
		<?=$form->dropDownListRow( $model, 'idServer', $servers, Array( 'class' => "{$ins} wIDServer" ))?>
		<?=$form->dropDownListRow( $model, 'status', $statusues, Array( 'class' => "{$ins} wStatus" ))?>
		<?=$form->dropDownListRow( $model, 'type', $types, Array( 'class' => "{$ins} wType" ))?>
		<?=$form->checkBoxListRow( $model, 'groups', $groups, Array( 'class' => "{$ins} wGroups" ))?>
		<div class="clear"></div>
		<div class="iControlBlock i01">
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'buttonType' => 'submit', 
					'type' => 'success',
					'label' => Yii::t( $NSi18n, 'Done!' ),
					'htmlOptions' => Array(
						'class' => "pull-right {$ins} wSubmit",
					),
				));
			?>
			<?
				$this->widget( 'bootstrap.widgets.TbButton', Array( 
					'type' => 'danger',
					'label' => Yii::t( $NSi18n, 'Delete' ),
					'htmlOptions' => Array(
						'class' => "pull-left {$ins} wDelete",
					),
				));
			?>
		</div>
		<div class="clear"></div>
	<?$this->endWidget()?>
</div>
<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var $ns = $( nsText );
		var ins = ".<?=$ins?>";
		var ajax = <?=CommonLib::boolToStr($ajax)?>;
		var hidden = <?=CommonLib::boolToStr($hidden)?>;
		
		var ls = <?=json_encode( $jsls )?>;
		
		ns.wAdminTradeAccountFormWidget = wAdminTradeAccountFormWidgetOpen({
			ins: ins,
			selector: nsText+' .wAdminTradeAccountFormWidget',
			ajax: ajax,
			hidden: hidden,
			ls: ls,
		});
	}( window.jQuery );
</script>