<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EAVersionFormWidget extends WidgetBase {
		public $model;
		private function getModel() {
			return $this->model;
		}
		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'languages' => $this->getLanguages(),
			));
		}
	}

?>