<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminInformersCategoryNewRawSettingsFormWidget extends WidgetBase {
		public $ajax = false;
		public $model;
		public $hidden = false;
		public $formModelName;
		public $modelName;
		
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getCats(){
			$models = WPTermTaxonomyModel::model()->findAll(array(
				'with' => array( 'term' ),
				'condition' => " `t`.`taxonomy` = 'category' AND `term`.`name` IS NOT NULL ",
				'order' => " `t`.`parent` DESC ",
			));
			$outArr = array();
			foreach( $models as $model ){
				$outArr[$model->term_id] = $model->term->name;
			}
			return $outArr;
		}

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'formModelName' => $this->formModelName,
				'modelName' => $this->modelName,
				'cats' => $this->getCats()
			));
		}
	}

?>