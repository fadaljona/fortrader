<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminAdvertisementZoneFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getSizesBlock() {
			$NSi18n = $this->getNSi18n();
			$source = AdvertisementZoneModel::getZoneSizesBlock();
			$sizes = Array();
			foreach( $source as $group=>$array ) {
				$group = Yii::t( $NSi18n, $group );
				$sizes[ $group ] = Array();
				foreach( $array as $size ) {
					$lCountAds = Yii::t( $NSi18n, '{n} ad|{n} ads', AdvertisementZoneModel::getZoneCountAdsBySizeBlock( $size ));
					$sizes[ $group ][ $size ] = "{$size} ({$lCountAds})";
				}
			}
			return $sizes;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'sizesBlock' => $this->getSizesBlock(),
			));
		}
	}

?>