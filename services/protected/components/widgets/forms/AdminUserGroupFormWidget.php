<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminUserGroupFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $model;
		public $rightsSort = 'id';
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function getRightsSort() {
			return $this->rightsSort;
		}
		private function filterRight( $a ) {
			$except = Array( 'companyControl' );
			return !in_array( $a->name, $except );
		}
		private function sortRights( $a, $b ) {
			$sort = $this->getRightsSort();
			if( is_string( $sort )) {
				$field = $sort;
				return ($a->$field < $b->$field) ? -1 : 1;
			}
		}
		private function getRights() {
			$rights = UserRightModel::model()->findAll();
			$rights = array_filter( $rights, Array( $this, 'filterRight' ));
			usort( $rights, Array( $this, 'sortRights' ));
			return $rights;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'rights' => $this->getRights(),
			));
		}
	}

?>