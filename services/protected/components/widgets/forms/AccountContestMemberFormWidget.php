<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AccountContestMemberFormWidget extends WidgetBase {
		public $model;
		public $contest;
		public $UModel=null;
		public $UMode='register';
		public $type = 'ft';
		private function getModel() {
			return $this->model;
		}
		private function getContest() {
			return $this->contest;
		}
		private function getServers() {
			$contest = $this->getContest();
			$servers = Array();
			if( $contest ){
				foreach( $contest->servers as $model ) {
					$servers[ $model->id ] = $model->name;
				}
			}else{
				foreach( ServerModel::model()->findAll(array('condition' => ' `t`.`forReports` = 0 ', 'order' => ' `t`.`name` ASC ')) as $model ) {
					$servers[ $model->id ] = $model->name;
				}
			}
			
			return $servers;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
				'servers' => $this->getServers(),
				'UMode'=>$this->UMode,
				'UModel'=>$this->UModel,
				'contest' => $this->getContest(),
				'type' => $this->type,
				'accountTypes' => array(
					'account' => Yii::t('*', 'Account number'),
					'report' => Yii::t('*', 'Report Metatrader4'),
				),
			));
		}
	}