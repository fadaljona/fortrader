<?php
    
Yii::import( 'components.widgets.base.WidgetBase' );

final class AdminCryptoCurrenciesExchangesSymbolToOurSymbolFormWidget extends WidgetBase
{
    public $ajax = false;
    public $model;
    public $hidden = false;
    public $addLabel;
    public $formModelName;
    public $loadItemRoute;
    public $modelName;
    public $deleteItemRoute;
    public $submitItemRoute;

    private function getAjax()
    {
        return $this->ajax;
    }
    private function getModel()
    {
        return $this->model;
    }
    private function getLangs()
    {
        return CommonLib::getLanguages();
    }

    private function getOurSymbols()
    {
        $models = QuotesSymbolsModel::model()->findAll(array(
            'with' => array('currentLanguageI18N'),
            'condition' => " `cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '' AND `t`.`toCryptoCurrenciesPage` = 1 ",
            'order' => "`cLI18NQuotesSymbolsModel`.`weightInCat` DESC ",
        ));

        $outArr = array(
            0 => 'not set'
        );
        foreach ($models as $model) {
            $outArr[$model->id] = $model->cryptoCurrencyTitle;
        }
        return $outArr;
    }

    public function run()
    {
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'ns' => $this->getNS(),
            'model' => $this->getModel(),
            'ajax' => $this->getAjax(),
            'languages' => $this->getLangs(),
            'addLabel' => $this->addLabel,
            'formModelName' => $this->formModelName,
            'loadItemRoute' => $this->loadItemRoute,
            'modelName' => $this->modelName,
            'deleteItemRoute' => $this->deleteItemRoute,
            'submitItemRoute' => $this->submitItemRoute,
            'ourSymbols' => $this->getOurSymbols()
        ));
    }
}
