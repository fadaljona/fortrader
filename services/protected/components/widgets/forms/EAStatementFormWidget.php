<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class EAStatementFormWidget extends WidgetBase {
		public $model;
		private function getModel() {
			return $this->model;
		}
		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}
		private function getVersions() {
			$models = EAVersionModel::model()->findAll(Array(
				'condition' => " `t`.`idEA` = :idEA ",
				'order' => " `t`.`release` DESC ",
				'params' => Array(
					':idEA' => $this->model->idEA,
				),
			));
			$versions = CommonLib::toAssoc( $models, 'id', 'version' );
			return $versions;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'languages' => $this->getLanguages(),
				'versions' => $this->getVersions(),
			));
		}
	}

?>