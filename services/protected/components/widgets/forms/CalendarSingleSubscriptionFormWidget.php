<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.CalendarSingleSubscriptionFormModel' );
	
	final class CalendarSingleSubscriptionFormWidget extends WidgetBase {
		public $model;
		private $formModel;
		

		private function getFormModel(){
			$model = new CalendarSingleSubscriptionFormModel;
			$model->loadByIdUserIdEvent( Yii::app()->user->id, $this->model->id );
			$model->idEvent = $this->model->id;
			return $model;
		}
		
		function run() {
			if( !$this->model ) return false;
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getFormModel(),
			));
		}
	}

?>