<?php
Yii::import('components.widgets.base.WidgetBase');

final class AdminPaymentExchangerSettingsFormWidget extends WidgetBase
{
    public $ajax = false;
    public $model;

    private function getAjax()
    {
        return $this->ajax;
    }
    private function getModel()
    {
        return $this->model;
    }

    public function run()
    {
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'ns' => $this->getNS(),
            'model' => $this->getModel(),
            'ajax' => $this->getAjax(),
            'languages' => CommonLib::getLanguages(),
        ));
    }
}
