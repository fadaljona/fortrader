<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class AdminCurrencyRatesFormWidget extends WidgetBase {
		public $hidden = false;
		public $ajax = false;
		public $modelName;
		public $formModelName;
		public $model;
		protected function getHidden() {
			return $this->hidden;
		}
		private function getAjax() {
			return $this->ajax;
		}
		private function getModel() {
			return $this->model;
		}
		private function sortLanguages( $a, $b ) {
			$defaultLanguageAlias = Yii::App()->language;
			return $a->alias == $defaultLanguageAlias ? -1 : 1;
		}
		private function getLanguages() {
			$languages = LanguageModel::getAll();
			usort( $languages, Array( $this, 'sortLanguages' ));
			return $languages;
		}
		private function getCountries() {
			$countries = Array();
			$models = CountryModel::getAll();
			
			foreach( $models as $id => $model ) {
				$countries[ $id ] = $model->name;
			}
			
			return $countries;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ns' => $this->getNS(),
				'model' => $this->getModel(),
				'ajax' => $this->getAjax(),
				'hidden' => $this->getHidden(),
				'languages' => $this->getLanguages(),
				'countries' => $this->getCountries(),
				'formModelName' => $this->formModelName,
				'modelName' => $this->modelName,
			));
		}
	}

?>