<?
Yii::import('components.widgets.base.WidgetBase');

final class SearchCurrencyRatesWidget extends WidgetBase{
	public $minInputChars;
	public $type = 'cbr';

	function run() {
		$class = $this->getCleanClassName();
		$this->render( "{$class}/view", Array(
			'minInputChars' => $this->minInputChars,
			'type' => $this->type,
		));
	}
	
}
?>