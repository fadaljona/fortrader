<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.UserProfileFormModel' );
	Yii::import( 'models.forms.UserAvatarFormModel' );
	Yii::import( 'models.forms.UserPasswordFormModel' );
	Yii::import( 'models.forms.UserSettingsFormModel' );
	
	final class UserSettingsWidget extends WidgetBase {
		public $model;
		private function getGeneralFormModel( $id ) {
			$model = new UserProfileFormModel();
			$model->load( $id );
			return $model;
		}
		private function getAvatarFormModel( $id ) {
			$model = new UserAvatarFormModel();
			$model->load( $id );
			return $model;
		}
		private function getPasswordFormModel( $id ) {
			$model = new UserPasswordFormModel();
			$model->load( $id );
			return $model;
		}
		private function getSettingsFormModel( $id ) {
			$model = new UserSettingsFormModel();
			$model->load( $id );
			return $model;
		}
		private function getModel() {
			if( !$this->model ) {
				$id = (int)@$_GET['id'];
				if( $id ) {
					$this->model = UserModel::model()->findByPk( $id );
				}
				else{
					$this->model = Yii::App()->user->getModel();
					if( !$this->model ) {
						Yii::App()->user->loginRequired();
					}
				}
				if( !$this->model ) {
					$this->redirect( Array( '/' ));
				}
			}
			return $this->model;
		}
		private function getItCurrentModel() {
			$model = $this->getModel();
			$currentModel = Yii::App()->user->getModel();
			$itCurrentModel = ($currentModel and $currentModel->id == $this->model->id);
			return $itCurrentModel;
		}
		function run() {
			$class = $this->getCleanClassName();
			if( !$this->getItCurrentModel() and !Yii::App()->user->checkAccess( 'userControl' )) $this->throwI18NException( "Access denied!" );
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
				'generalFormModel' => $this->getGeneralFormModel( $this->model->id ),
				'avatarFormModel' => $this->getAvatarFormModel( $this->model->id ),
				'passwordFormModel' => $this->getPasswordFormModel( $this->model->id ),
				'settingsFormModel' => $this->getSettingsFormModel( $this->model->id ),
				'itCurrentModel' => $this->getItCurrentModel(),
			));
		}
	}

?>
