<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ShortCurrencyRatesExcangeInfoWidget extends WidgetBase {
		public $model;
		
		private function getToVal( $val, $fromData, $toData ){
			if( $this->model->type == 'cbr' ){
				if( $this->model->rateFrom->defaultCurrency ){
					$fromCourse = $fromNominal = 1;
				}else{
					$fromCourse = $fromData->value;
					$fromNominal = $fromData->nominal;
				}
				if( $this->model->rateTo->defaultCurrency ){
					$toCourse = $toNominal = 1;
				}else{
					$toCourse = $toData->value;
					$toNominal = $toData->nominal;
				}
				return $val * ( $fromCourse / $fromNominal) / ( $toCourse / $toNominal) ;
			}elseif( $this->model->type == 'ecb' ){
				if( $this->model->rateFrom->defaultCurrencyEcb ){
					$fromCourse = 1;
				}else{
					$fromCourse = $fromData->value;
				}
				if( $this->model->rateTo->defaultCurrencyEcb ){
					$toCourse = 1;
				}else{
					$toCourse = $toData->value;
				}
				return $val * $toCourse / $fromCourse;
			}
		}
		private function getDiffValStr(){
			if( $this->model->type == 'cbr' ){
				if( (!$this->model->rateFrom->prevDayData && !$this->model->rateFrom->defaultCurrency) || (!$this->model->rateTo->prevDayData && !$this->model->rateTo->defaultCurrency) ) return '';
				$todayVal = $this->getToVal( 1, $this->model->rateFrom->todayData, $this->model->rateTo->todayData );
				$prevDayVal = $this->getToVal( 1, $this->model->rateFrom->prevDayData, $this->model->rateTo->prevDayData );
				$dif = $todayVal - $prevDayVal;
				$percentVal = $dif  / $prevDayVal * 100;
				
				if( $dif <=0 ){
					return CHtml::tag('span', array( 'class' => 'red_color' ), number_format($dif, CommonLib::getDecimalPlaces( $dif ), ',', ' ') . ' (' . number_format($percentVal, CommonLib::getDecimalPlaces( $percentVal ), ',', ' ') . '%)' );
				}else{
					return CHtml::tag('span', array( 'class' => 'green_color' ), '+' . number_format($dif, CommonLib::getDecimalPlaces( $dif ), ',', ' ') . ' (+' . number_format($percentVal, CommonLib::getDecimalPlaces( $percentVal ), ',', ' ') . '%)' );
				}
			}elseif( $this->model->type == 'ecb' ){
				
				if( (!$this->model->rateFrom->prevEcbDayData && !$this->model->rateFrom->defaultCurrencyEcb) || (!$this->model->rateTo->prevEcbDayData && !$this->model->rateTo->defaultCurrencyEcb) ) return '';
				$todayVal = $this->getToVal( 1, $this->model->rateFrom->todayEcbData, $this->model->rateTo->todayEcbData );
				$prevDayVal = $this->getToVal( 1, $this->model->rateFrom->prevEcbDayData, $this->model->rateTo->prevEcbDayData );
				$dif = $todayVal - $prevDayVal;
				$percentVal = $dif  / $prevDayVal * 100;
				
				if( $dif <=0 ){
					return CHtml::tag('span', array( 'class' => 'red_color' ), number_format($dif, CommonLib::getDecimalPlaces( $dif ), ',', ' ') . ' (' . number_format($percentVal, CommonLib::getDecimalPlaces( $percentVal ), ',', ' ') . '%)' );
				}else{
					return CHtml::tag('span', array( 'class' => 'green_color' ), '+' . number_format($dif, CommonLib::getDecimalPlaces( $dif ), ',', ' ') . ' (+' . number_format($percentVal, CommonLib::getDecimalPlaces( $percentVal ), ',', ' ') . '%)' );
				}	
				
			}
		}

		function run() {
			$class = $this->getCleanClassName();
			
			if( $this->model->type == 'cbr' ){
				$rateFromTodayData = $this->model->rateFrom->todayData;
				$rateToTodayData = $this->model->rateTo->todayData;
				$infoText = Yii::t($NSi18n, 'Information by CBR for');
			}
			if( $this->model->type == 'ecb' ){
				$rateFromTodayData = $this->model->rateFrom->todayEcbData;
				$rateToTodayData = $this->model->rateTo->todayEcbData;
				$infoText = Yii::t($NSi18n, 'Information by ECB for');
			}

		
			$this->render( "{$class}/view", Array(
				'fromFlag' => $this->model->rateFrom->country->getSrcFlag('shiny', 48),
				'toFlag' => $this->model->rateTo->country->getSrcFlag('shiny', 48),
				'toVal' => $this->getToVal( $this->model->value, $rateFromTodayData, $rateToTodayData ),
				'toOneVal' => $this->getToVal( 1, $rateFromTodayData, $rateToTodayData ),
				'diffValStr' => $this->getDiffValStr(),
				'model' => $this->model,
				'infoText' => $infoText
			));
		}
	}

?>