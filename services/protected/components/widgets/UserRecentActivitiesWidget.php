<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.UserMessageFormModel' );
	
	final class UserRecentActivitiesWidget extends WidgetBase {
		public $ajax = true;
		public $idUser;
		public $limitActivities = 5;
		public $idLastActivity;
		private function getIDUser() {
			return $this->idUser;
		}
		private function getIDLastActivity() {
			if( $this->idLastActivity ) return $this->idLastActivity;
			$id = (int)@$_GET['idLastActivity'];
			return $id ? $id : null;
		}
		private function getCriteriaActivities( $forCount = false ) {
			$c = new CDbCriteria();
			
			if( !$forCount ) {
				$c->with[ 'i18ns' ] = Array();
			}
			$c->addCondition( " `t`.`idUser` = :idUser " );
			$c->params[ ':idUser' ] = $this->getIDUser();
			
			$idLastActivity = $this->getIDLastActivity();			
			if( $idLastActivity ) {
				$c->addCondition( " `t`.`id` < :idLastActivity " );
				$c->params[ ':idLastActivity' ] = $idLastActivity;
			}
			
			if( !$forCount ) {
				$c->order = " `t`.`id` DESC ";
				$c->limit = $this->limitActivities;
			}
			
			return $c;
		}
		private function getActivities() {
			$c = $this->getCriteriaActivities();
			$activities = UserActivityModel::model()->findAll( $c );
			return $activities;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'ajax' => $this->ajax,
				'idUser' => $this->getIDUser(),
			));
		}
		function renderActivities() {
			$activities = $this->getActivities();
			foreach( $activities as $activity ){
				$this->renderActivity( $activity );
			}
		}
		function renderActivity( $activity ) {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/activity", Array(
				'activity' => $activity,
			));
			$this->idLastActivity = $activity->id;
		}
		function issetMoreActivities() {
			$c = $this->getCriteriaActivities( true );
			$exists = UserActivityModel::model()->exists( $c );
			return $exists;
		}
	}

?>