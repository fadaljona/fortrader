<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CalendarEventProfileWidget extends WidgetBase {
		public $model;
		private function getModel() {
			return $this->model;
		}
		private function getNews() {
			/*$model = $this->getModel();
			$language = LanguageModel::getByAlias( 'ru' );
			$i18nRU = $model->getI18N( $language->id );
			$index = $i18nRU->indicator;
			$country = $i18nRU->country;
			
			$posts = WPPostModel::model()->findAll( Array( 
				'select' => " ID, post_title, post_content, post_date ",
				'with' => Array(
					"categories" => Array(
						"together" => false,
					),
					"index" => Array(),
					"country" => Array(),
					"author" => Array(),
				),
				'condition' => "
					`index`.`meta_value` = :index
					AND `country`.`meta_value` = :country
				",
				'params' => Array(
					':index' => $index,
					':country' => $country,
				),
			));
			return $posts;*/
			
		}
		function run() {
			
			$cs=Yii::app()->getClientScript();
			$formHelpersBaseUrl = Yii::app()->getAssetManager()->publish( Yii::getPathOfAlias('webroot.assets-static.formHelpers') );
			$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers.css');
			$cs->registerCssFile($formHelpersBaseUrl.'/css/bootstrap-formhelpers-countries.flags.css');
			
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->getModel(),
				'news' => $this->getNews(),
			));
		}
	}

?>
