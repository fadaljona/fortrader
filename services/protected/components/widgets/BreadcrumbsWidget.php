<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class BreadcrumbsWidget extends WidgetBase {
		function getItems() {
			$NSi18n = $this->getNSi18n();
			$items = isset($this->controller->commonBag->breadcrumbs) ? $this->controller->commonBag->breadcrumbs : Array();
			
			if( Yii::app()->controller->id == 'default' && Yii::app()->controller->action->id == 'index' ){
				$services_link=(object)Array( 
				'label' => 'Services'
				);
			}else{
				$services_link=(object)Array( 
				'label' => 'Services',
				'url' => Array( '/' ),
				);
			}
			
			array_unshift( $items, (object)Array( 
				'label' => 'Home',
				'url' =>  '/',
				'icon' => 'icon-home home-icon',
				),
				$services_link
			);
			
			return $items;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'items' => $this->getItems(),
			));
		}
	}

?>