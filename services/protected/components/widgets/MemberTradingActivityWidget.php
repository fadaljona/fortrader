<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class MemberTradingActivityWidget extends WidgetBase {
		public $accountModel;

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'accountModel' => $this->accountModel,
			));
		}
	}

?>