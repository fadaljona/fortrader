<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.AdminBrokerMarkFormModel' );
	
	final class BrokerProfileWidget extends WidgetBase {
		public $model;
		/*private function getMarkModel() {
			$formModel = new AdminBrokerMarkFormModel();
			$formModel->setIDBroker( $this->model->id );
			$formModel->load();
			return $formModel;
		}*/
		private function checkBrokerAds($id) {

			$array_AdsIds = Yii::app()->db->createCommand()
				->select('users.idBroker')
				->from('ft_advertisement_campaign campaign')  //Your Table name
				->join('wp_users users', 'users.ID = campaign.idUser')
				->where( "campaign.type='Rating Brokers' and campaign.status='Active' and CURDATE() BETWEEN campaign.begin and campaign.end and users.idBroker=".$id )
				->queryRow(); //Will get the all selected rows from table
				
			if( isset( $array_AdsIds['idBroker'] )  && $array_AdsIds['idBroker'] )
				return true;
			else
				return false;

		}
		private function getModel() {
			return $this->model;
		}
		function run() {
			$class = $this->getCleanClassName();
			
			$model = $this->getModel();
			
			$ftSettings = SettingsModel::getModel();
			
			if( $this->checkBrokerAds($model->id) )
				$model->stats->place=0;
			
			$this->render( "{$class}/view", Array(
				'model' => $model,
				//'markModel' => $this->getMarkModel(),
				'vkAppId' => $ftSettings->vkAppId,
			));
		}
	}

?>