<?
Yii::import( 'components.widgets.base.WidgetBase' );
Yii::import( 'models.forms.InfoContestMemberFormModel' );
Yii::import( 'models.forms.AccountContestMemberFormModel' );
Yii::import( 'models.forms.RegisterUserFormModel' );

final class RegistrationContestMemberWizardWidget extends WidgetBase{
	public $infoFormModel;
	public $accountFormModel;
	public $contest;
	public $UMode = "register";
	public $type = 'ft';
	private $member = false;
	
	
	private function detUmode() {
		if(Yii::app()->user->id){
			
			if( $this->contest ){
				$member = ContestMemberModel::model()->find(array(
					'select' => array( 'id' ),
					'condition' => " `t`.`idContest` = :idContest AND `t`.`idUser` = :idUser ",
					'params' => array( ':idContest' => $this->getIdContest(), ':idUser' => Yii::app()->user->id ),
				));
				
				if( !$member ){
					$this->UMode='register';
				}else{
					$this->UMode='editRegister';
					$this->member = $member;
				}
			}else{
				$this->UMode='register';
			}
			
		}else{
			$this->UMode='guest';
		}
	}

	private function getUser() {
		if($this->UMode=='guest'){
			$user= new RegisterUserFormModel();
		}else
			$user = Yii::App()->user->getModel();
		if ( ! $user ) {
			throw new Exception( "Can't find User! Relogin!" );
		}

		return $user;
	}

	private function getIdUser() {
		if($this->UMode=='guest'){
			$id=0;
		}else{
			$user = $this->getUser();
			$id=$user->id;
		}
		return $id;
	}

	private function detInfoFormModel() {
		$this->infoFormModel = new InfoContestMemberFormModel();
		$idUser              = $this->getIdUser();
		$this->infoFormModel->load( $idUser );
		$obj = ContestMemberModel::instanceInfoDataObject( $this->getIdContest(), $this->getIdUser() );
		if ( $obj->load() ) {
			$this->infoFormModel->loadFromObject( $obj->data );
		}
	}

	private function getInfoFormModel() {
		if ( ! $this->infoFormModel ) {
			$this->detInfoFormModel();
		}

		return $this->infoFormModel;
	}

	private function detAccountFormModel() {
		$this->accountFormModel = new AccountContestMemberFormModel();
		if( $this->UMode != 'editRegister' ){
			$obj                    = ContestMemberModel::instanceAccountDataObject( $this->getIdContest(), $this->getIdUser() );
			$obj->load();
			if ( $obj->load() ) {
				$this->accountFormModel->loadFromObject( $obj->data );
			}
		}else{
			$this->accountFormModel->load( $this->member->id );
		}
	}

	private function getAccountFormModel() {
		if ( ! $this->accountFormModel ) {
			$this->detAccountFormModel();
		}
		return $this->accountFormModel;
	}

	private function getContest() {
		return $this->contest;
	}

	private function getIdContest() {
		$contest = $this->getContest();
		return $contest->id;
	}

	function run() {
		$class = $this->getCleanClassName();
		$this->detUmode();
		$user    = $this->getUser();
		$contest = $this->getContest();
		
		if( $contest && $contest->status != 'registration' ) return false;
		
		$registeredInContest = false;
		if( $this->getIdUser() ) $registeredInContest = $user->isRegisteredInContest( $contest->id );

		if ( $this->UMode != 'guest' && $user->isRoot() ) {
			throw new Exception( "Root can't register in contest!" );
		} else{
			if ( $contest->accountOpening == "list of accounts" ) {
				$this->render( "{$class}/listOfAccountsView", Array(
					'infoFormModel' => $this->getInfoFormModel(),
					'contest'       => $this->getContest(),
					'UMode'         => $this->UMode,
					'UModel'        => $user
				) );
			} else {
				$this->render( "{$class}/view", Array(
					'infoFormModel'    => $this->getInfoFormModel(),
					'accountFormModel' => $this->getAccountFormModel(),
					'contest'          => $this->getContest(),
					'UMode'            => $this->UMode,
					'UModel'           => $user,
					'type'             => $this->type,
				) );
			}
		}
	}
}
