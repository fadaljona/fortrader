<?
Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.assets-static.js.fuelux').'/fuelux.wizard.min.js' ) );
Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.assets-static.js').'/bootbox.min.js' ) );
Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js.widgets.wizards').'/wRegistrationContestMemberWizardWidget.js' ), CClientScript::POS_END );


$ns     = $this->getNS();
$ins    = $this->getINS();
$NSi18n = $this->getNSi18n();

$jsls = Array(
	'lError'      => 'Error!',
	'lLoading'    => 'Loading...',
	'lSaved'      => 'Saved',
	'lErrorAgree' => 'You must agree to the rules of the contest!',
	'login' 	  => 'Log in',
	'forgotPass'  => 'Forgot your password?',
	'lMessageSend' => 'Please check your email for reset password instructions'
);
foreach ( $jsls as &$ls ) {
	$ls = Yii::t( $NSi18n, $ls );
}
unset( $ls );
?>

<div <?php if($UMode == 'editRegister') echo 'style="display:none;"'?> class="wRegistrationContestMemberWizardWidget spinner-margin position-relative">

	<hr class="separator2">
	<section class="section_offset" id="contestRegistrationForm">
		<h3>
			<?php
				if($UMode == 'editRegister'){
					echo Yii::t( $NSi18n, 'Edit your information' );
				}else{
					echo Yii::t( $NSi18n, 'Registration Wizard' );
				}
			?>
			<i class="fa fa-key" aria-hidden="true"></i></h3>
		<hr class="separator2">
	</section>
	<!-- form -->
	<div class="section_offset">
		
		<?
			$this->widget( 'widgets.forms.AccountContestMemberFormWidget', Array(
				'model'   => $accountFormModel,
				'contest' => $contest,
				'UModel'=> $UModel,
				'UMode'  => $UMode,
				'ns'      => $ns,
				'type'    => $type
			) );
		?>
							
		<div class="step-pane hide" id="stepFinish">
			<div class="center">
				<h3 class="green"><?=Yii::t( $NSi18n, 'Congrats!' )?></h3>
				<?php
				if( $contest ){
					echo Yii::t( $NSi18n, 'Your registered in contest!' );
				}else{
					echo Yii::t( $NSi18n, 'Your account registered' );
				}
				?>
				<span id="forward_message"><?=Yii::t( $NSi18n, 'You will be forwarded to the account page after !t! seconds. If you are still on this page, click <a href="!link!">here</a>' )?></span>
			</div>
		</div>
		
		
		
		
		
		
		<!-- - - - - - - - - - - - - - Modal Window - - - - - - - - - - - - - - - - -->
		<div class="hide">
			<div id="contestModal" class="modal_box">
				<div class="arcticmodal-close modal_close"><i class="fa fa-times"></i></div>
				<div class="modal_header">
					<div class="modal_logo">FT</div>
					<h6 class="modal_header_title"><?php
					if( $contest ){
						echo Yii::t( $NSi18n, 'Account with this number is already registered in the competition. Go to page account?' );
					}else{
						echo Yii::t( $NSi18n, 'Account with this number is already registered. Go to page account?' );
					}
					?></h6>
				</div>
				<div class="modal_inner">
					<div class="modal_social_box">
						<ul class="modal_social choice clearfix">
							<li><a href="javascript:void(0);" class="fb goToAccount"><?=Yii::t( $NSi18n, 'Yes' )?></a></li>
							<li><a href="javascript:void(0);" class="google_plus arcticmodal-close"><?=Yii::t( $NSi18n, 'No' )?></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- - - - - - - - - - - - - - End of Modal Window - - - - - - - - - - - - - - - - -->
		
		<?php
			if( !Yii::app()->user->id ){
				echo CHtml::tag('div', array( 'class' => "disableBlock arcticmodal", 'data-modal' => "#logIn" ), '');
			}
		?>	
		
		
		
	</div>

</div>

<?php
	
$contestId = $contest ? $contest->id : 0;
	
$memberUrl = $contest ? Yii::app()->createAbsoluteUrl('contestMember') : Yii::app()->createAbsoluteUrl('monitoring');

Yii::app()->clientScript->registerScript('RegistrationContestMemberWizardJs', '

	!function( $ ) {
		var ns = ' . $ns . ';
		var nsText = ".' . $ns . '";
		var ins = ".' . $ins . '";
		var ls = '.json_encode( $jsls ).';
		var idContest = '. $contestId .';
		
		var textYes = "'. Yii::t( $NSi18n, 'Yes' ) .'";
		var textNo = "'. Yii::t( $NSi18n, 'No' ) .'";
		var textContestMemberExist = "'. Yii::t( $NSi18n, 'Account with this number is already registered in the competition. Go to page account?' ) .'";

		ns.wRegistrationContestMemberWizardWidget = wRegistrationContestMemberWizardWidgetOpen({
			ns: ns,
			ins: ins,
			mode: "'. $UMode .'",
			selector: nsText + " .wRegistrationContestMemberWizardWidget",
			ls: ls,
			idContest: idContest,
			textYes: textYes,
			textNo: textNo,
			acceptTermsText: "'. Yii::t( $NSi18n, 'You must agree to the rules of the contest!' ) .'",
			textContestMemberExist: textContestMemberExist,
			registerMemberUrl: "'. Yii::app()->createUrl('contest/ajaxRegisterMember', array( 'idContest' => $contestId, 'step' => 'account' ) ) .'",
			editAccountMemberUrl: "'. Yii::app()->createUrl('contest/ajaxRegisterMember', array( 'idContest' => $contestId, 'step' => 'account', 'act' => 'edit' ) ) .'",
			getCurrentUserInfoUrl: "'. Yii::app()->createUrl('contest/ajaxLoadCurrentUserInfo', array( 'idContest' => $contestId ) ) .'",
			memberUrl: "'.  $memberUrl .'",
			addAccountToMonitoringUrl: "'. Yii::app()->createUrl('contest/ajaxAddAccountToMonitoring') .'",
			checkAccountStatusUrl: "'. Yii::app()->createUrl('contest/ajaxCheckAccountStatus') .'",
			accountWillBeCheckdLater: "'. Yii::t($NSi18n, "Your account will be checked later") .'",
			type: "' . $type . '",
			exisAccountErrorMessage: "' . Yii::t( $NSi18n, 'Account with this number is already registered.' ) . '",
			iframeAddReportUrl: "'. Yii::app()->createUrl('monitoring/iframeAddReport' ) .'",
		});
	}( window.jQuery );

', CClientScript::POS_END);
	
?>