<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/js/widgets/wizards/wRegistrationListOfAccountsContestMemberWizardWidget.js" );
	
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
	
	$jsls = Array(
		'lError' => 'Error!',
		'lLoading' => 'Loading...',
		'lSaved' => 'Saved',
		'lErrorAgree' => 'You must agree to the rules of the contest!',
	);
	foreach( $jsls as &$ls ) $ls = Yii::t( $NSi18n, $ls ); unset($ls);
?>

<hr class="separator2">
<section class="section_offset" id="contestRegistrationForm">
	<h3><?=Yii::t( $NSi18n, 'Registration Wizard' )?><i class="fa fa-key" aria-hidden="true"></i></h3>
	<hr class="separator2">
</section>
<!-- form -->
<div class="section_offset wRegistrationListOfAccountsContestMemberWizardWidget spinner-margin position-relative">
	
	<?php
		$this->beginWidget( 'widgets.base.BootstrapExFormWidgetBase', Array( 'type' => 'horizontal', 'htmlOptions' => array( 'class' => 'regestration clearfix' ) ) );
		
			
			echo CHtml::openTag('div', array( 'class' => 'regestration_box_50 conditions_checkbox_box' ));	
				echo CHtml::openTag('div', array( 'class' => 'checkbox_box' ));
					echo CHtml::checkBox('conditions', false, array( 'id' => 'columns_show1', 'class' => 'wconditions '.$ins ));
					echo CHtml::tag( 'label', array( 'for' => 'columns_show1', 'class' => 'square_input' ), '<i></i>' . Yii::t( $NSi18n, 'The terms have read' ) );
				echo CHtml::closeTag('div');
			echo CHtml::closeTag('div');
			

			echo CHtml::openTag('div', array( 'class' => 'regestration_box_50 regestration_box_button' ));	
				echo CHtml::button( Yii::t( $NSi18n, 'Register' ), array('class'=>'regestration_btn alignright wSubmit '.$ins) );
			echo CHtml::closeTag('div');

			
		$this->endWidget();	
	?>


	<div class="step-content row-fluid position-relative hide" id="stepFinish">
		<section class="section_offset paddingBottom30">
			<h5 class="info_title"><?=Yii::t( $NSi18n, 'You are registered successfully' )?></h5>
		</section>
		<div class="section_offset ">
			<div class="wrapper">
				<table id="yw0" class=" inside_table table table-striped table-condensed table1">
					<tbody>
						<tr class="odd">
							<td class="inside_col_1 inside_col_blue"><?=Yii::t( $NSi18n, 'Number of you account' )?></td>
							<td class="inside_col_2 "><a href="" id="accountModelLogin" class="blue_color"></a></td>
						</tr>
						<tr class="even">
							<td class="inside_col_1 inside_col_blue"><?=Yii::t( $NSi18n, 'Password' )?></td>
							<td class="inside_col_2 "><span id="accountModelPassword"></span></td>
						</tr>
						<tr class="odd">
							<td class="inside_col_1 inside_col_blue"><?=Yii::t( $NSi18n, 'Password of a investor' )?></td>
							<td class="inside_col_2 "><span id="accountModelInvPassword"></span></td>
						</tr>
						<tr class="even">
							<td class="inside_col_1 inside_col_blue"><?=Yii::t( $NSi18n, 'Server name' )?></td>
							<td class="inside_col_2 "><?=$contest->servers[0]->name;?></td>
						</tr>
						<tr class="odd">
							<td class="inside_col_1 inside_col_blue"><?=Yii::t( $NSi18n, 'Server IP' )?></td>
							<td class="inside_col_2 "><?=$contest->servers[0]->url;?></td>
						</tr>
					</tbody>
				</table>		
			</div>
		</div>
	</div>
	<?php
		if( !Yii::app()->user->id ){
			echo CHtml::tag('div', array( 'class' => "disableBlock arcticmodal", 'data-modal' => "#logIn" ), '');
		}
	?>	
</div>


<script>
	!function( $ ) {
		var ns = <?=$ns?>;
		var nsText = ".<?=$ns?>";
		var ins = ".<?=$ins?>";
		
		var ls = <?=json_encode( $jsls )?>;
		
		var idContest = <?=$contest->id?>;
		
		ns.wRegistrationListOfAccountsContestMemberWizardWidget = wRegistrationListOfAccountsContestMemberWizardWidgetOpen({
			ns: ns,
			ins: ins,
			mode: '<?=$UMode?>',
			selector: nsText+' .wRegistrationListOfAccountsContestMemberWizardWidget',
			ls: ls,
			idContest: idContest,
			errorMess: '<?=Yii::t( $NSi18n, 'An error occurred')?>',
			ajaxRegisterUrl: '<?=Yii::app()->createUrl('contest/ajaxRegisterMember', array( 'idContest' => $contest->id, 'step' => 'info', 'flag' => 'save_account_from_list' ))?>',
			acceptTermsText: "<?=Yii::t( $NSi18n, 'You must agree to the rules of the contest!' )?>",
			memberUrl: '<?=Yii::app()->createUrl('contestMember/single')?>',
		});
	}( window.jQuery );
</script>
