<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wRegistrationContestMemberWizardWidget">
	<div class="alert alert-error">
		<?=Yii::t( $NSi18n, 'You are already registered in this contest!' )?>
	</div>
</div>
