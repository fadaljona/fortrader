<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesListWidget extends WidgetBase {
		public $type = 'today';
		public $dataType = 'cbr';

		private function getToCurrencies(){
			if( $this->dataType == 'cbr' ){
				return CurrencyRatesModel::getCurrenciesForConverter();
			}elseif( $this->dataType == 'ecb' ){
				return CurrencyRatesModel::getEcbCurrenciesForConverter();
			}
		}
		private function getDefaultCurrency(){
			return CurrencyRatesModel::getDefaultCurrency( $this->dataType );
		}
		function run() {
			$class = $this->getCleanClassName();
			
			$daySecondsToNewData = 0;
			if( $this->dataType == 'cbr' && $this->type == 'tomorrow' ) $daySecondsToNewData = CurrencyRatesHistoryModel::getDaySecondsToNewData();
			
			$this->render( "{$class}/view", Array(
				'daySecondsToNewData' => $daySecondsToNewData,
				'type' => $this->type,
				'dataType' => $this->dataType,
				'toCurrencies' => $this->getToCurrencies(),
				'defaultCurrency' => $this->getDefaultCurrency(),
			));
		}
	}

?>