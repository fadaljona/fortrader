<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.ContestForeignFormModel' );
	
	final class ForeignContestsWidget extends WidgetBase {

		private $formModel;
		private function detFormModel() {
			$this->formModel = new ContestForeignFormModel();
			$load = $this->formModel->load();
			if( !$load ) $this->throwI18NException( "Can't find foreign contest!" );
		}
		private function getFormModel() {
			return $this->formModel;
		}
		
		function run() {
			$this->detFormModel();
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'formModel' => $this->getFormModel(),
			));
		}
	}

?>