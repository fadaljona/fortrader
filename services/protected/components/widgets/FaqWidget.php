<?php

Yii::import('components.widgets.base.WidgetBase');

final class FaqWidget extends WidgetBase
{
    public $faq;

    private function getModels()
    {
        if (!$this->faq) {
            return false;
        }
        return FaqItemModel::model()->findAll(array(
            'with' => array(
                'currentLanguageI18N',
                'faq' => array('with' => 'currentLanguageI18N')
            ),
            'condition' => " `cLI18NFaqItem`.`question` IS NOT NULL AND `cLI18NFaqItem`.`question` <> '' AND `cLI18NFaqItem`.`answer` IS NOT NULL AND `cLI18NFaqItem`.`answer` <> '' AND `cLI18NFaq`.`title` IS NOT NULL AND `cLI18NFaq`.`title` <> '' ",
            'order' => " `t`.`order` DESC ",
            'limit' => 30,
        ));
    }

    public function run()
    {
        $models = $this->getModels();
        if (!$models) {
            return false;
        }
        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'faq' => $models[0]->faq,
            'models' => $models
        ));
    }
}
