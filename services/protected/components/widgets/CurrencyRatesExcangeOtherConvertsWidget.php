<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CurrencyRatesExcangeOtherConvertsWidget extends WidgetBase {
		public $model;

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'model' => $this->model,
				'similars' => $this->model->similars,
				'inOtherCurrencies' => $this->model->inOtherCurrencies,
			));
		}
	}

?>