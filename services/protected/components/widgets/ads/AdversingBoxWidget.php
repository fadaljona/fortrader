<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	Yii::import( 'models.forms.UserMessageFormModel' );
	
	final class AdversingBoxWidget extends WidgetBase {
		public $title = 'Adversing';
		public $zoneURL = '';
		public $showMover = false;
		public $style;
		function getZoneURL() {
			return $this->zoneURL;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'title' => $this->title,
				'zoneURL' => $this->getZoneURL(),
				'showMover' => $this->showMover,
				'style' => $this->style,
			));
		}
	}

?>
