<?
	$NSi18n = $this->getNSi18n();
?>
<a href="<?=ContestModel::getListURL()?>" class="iA i01 i04">
	<i class="iI i05"></i>
	<span class="iSpan i13"><?=Yii::t( '*', 'Contests' )?></span>
	<?if( $registered ){?>
		<div class="iDiv i06">
			<i class="iI i04 wToggle iOpened"></i>
			<i class="iI i06">+<?=count($registered)?></i>
		</div>
	<?}?>
</a>
<?if( $registered ){?>
	<div class="iDiv i09 i14" style="display:block">
		<?foreach( $registered as $i=>$contest ){?>
			<?if( $contest->servers ){?>
				<?$img = $contest->servers[0]->broker->getThumbImage( Array( 'class' => 'iImg i01' ))?>
				<?=CHtml::link( $img, $contest->getSingleURL() )?>
			<?}?>
			<?=CHtml::link( CHtml::encode( $contest->name ), $contest->getSingleURL())?><br>
			<?=Yii::t( '*', 'Begin' )?> <?=$this->formatDate( $contest->begin )?>
			
			<div style="clear:both"></div>
			
			<?if( $i+1 < count( $registered )){?>
				<div class="iDiv i10"></div>
			<?}?>
		<?}?>
	</div>
<?}?>