<?
	$NSi18n = $this->getNSi18n();
?>
<a href="<?=UserModel::getListURL()?>" class="iA i01 i15">
	<i class="iI i20"></i>
	<span class="iSpan i13"><?=Yii::t( '*', 'Members' )?></span>
	<br>
	
	<span class="iSpan i16"><?=Yii::t( '*', 'Total' )?>: </span>
	<span class="iSpan i17"><?=CommonLib::numberFormat( $countUsers, 0, ".", "," )?></span>

	&nbsp;&nbsp;
	<span class="iSpan i16"><?=Yii::t( '*', 'Today' )?>: </span>
	<span class="iSpan i17"><?=CommonLib::numberFormat( $countUsersToday, 0, ".", "," )?></span>
	
	<div class="iDiv i06">
		<i class="iI i04 wToggle"></i>
	</div>
</a>
<div class="iDiv i09 i42">
	<a class="iA i16" href="<?=UserModel::getListURL( Array( 'traders' => 'on' ))?>">
		<div class="iDiv i43">
			<i class="iI i21 i21_01"></i>
		</div>
		<?=Yii::t( '*', 'Rating Traders' )?>
	</a>
	<a class="iA i16" href="<?=UserModel::getListURL( Array( 'managers' => 'on' ))?>">
		<div class="iDiv i43">
			 <i class="iI i21 i21_02"></i>
		</div>
		<?=Yii::t( '*', 'Rating Managers' )?>
	</a>
	<a class="iA i16" href="<?=UserModel::getListURL( Array( 'investors' => 'on' ))?>">
		<div class="iDiv i43">
			<i class="iI i21 i21_03"></i>
		</div>
		<?=Yii::t( '*', 'Rating Investors' )?>
	</a>
	<a class="iA i16" href="<?=UserModel::getListURL( Array( 'programmers' => 'on' ))?>" style="padding-bottom:0">
		<div class="iDiv i43">
			<i class="iI i21 i21_04"></i>
		</div>
		<?=Yii::t( '*', 'Rating Programmers' )?>
	</a>
</div>