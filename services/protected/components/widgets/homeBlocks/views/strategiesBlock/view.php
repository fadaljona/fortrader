<?
	$NSi18n = $this->getNSi18n();
?>
<a href="<?=StrategyModel::_getModelListURL()?>" class="iA i01 i28">
	<i class="iI i43"></i>
	<span class="iSpan i13"><?=Yii::t( '*', 'Strategies' )?></span>
	<?if( $strategies ){?>
		<div class="iDiv i06">
			<i class="iI i04 wToggle"></i>
		</div>
	<?}?>
</a>
<?if( $strategies ){?>
	<div class="iDiv i09 i64">
		<?foreach( $strategies as $strategy ){?>
			<div class="iDiv i53">
				<?=CHtml::link( CHtml::encode( $strategy->name ), "#" )?><br>
				<?$this->widget( 'widgets.parts.TimeDiffWidget', Array( 'time' => $strategy->createdDT, 'icon' => null, 'ago' => 'ago' ))?>
			</div>
		<?}?>
		<div class="text-center">
			<?=CHtml::link( Yii::t( '*', 'All strategies' ), StrategyModel::_getModelListURL())?>
			<i class="icon-chevron-down" style="color:grey"></i>
		</div>
	</div>
<?}?>