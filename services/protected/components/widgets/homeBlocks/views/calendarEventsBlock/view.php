<?
	$NSi18n = $this->getNSi18n();
?>
<a href="<?=CalendarEvent2Model::getListURL()?>" class="iA i01 i22">
	<i class="iI i35"></i>
	<span class="iSpan i13"><?=Yii::t( '*', 'Calendar' )?></span>
	<?if( $events ){?>
		<div class="iDiv i06">
			<i class="iI i04 wToggle"></i>
		</div>
	<?}?>
</a>
<?if( $events ){?>
	<div class="iDiv i09 i54">
		<?foreach( $events as $event ){?>
			<div class="iDiv i53">
				<?=CHtml::link( CHtml::encode( $event->event->indicatorName ), "#", Array( 'class' => "iA i23_{$event->serious}" ))?><br>
				<?=Yii::t( "lower", 'time left' )?>: <?=$event->getTimeLeft()?><br>
			</div>
		<?}?>
		<div class="text-center">
			<?=CHtml::link( Yii::t( '*', 'All events' ), CalendarEvent2Model::getListURL())?>
			<i class="icon-chevron-down" style="color:grey"></i>
		</div>
	</div>
<?}?>