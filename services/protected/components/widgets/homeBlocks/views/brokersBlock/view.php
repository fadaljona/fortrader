<?
	$NSi18n = $this->getNSi18n();
?>
<a href="<?=BrokerModel::getListURL()?>" class="iA i01 i03">
	<i class="iI i03"></i>
	<span class="iSpan i13"><?=Yii::t( '*', 'Brokers' )?></span>
	<div class="iDiv i06">
		<i class="iI i04 wToggle"></i>
	</div>
</a>
<div class="iDiv i09 i13">
	<?=Yii::t( '*', 'Best brokers' );?><br>
	<?=$best?>
	<div class="space-6"></div>
	<?=Yii::t( '*', 'New brokers' );?><br>
	<?=$new?>
</div>