<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="wActivityBlockWidget">
	<a class="iA i01 i05">
		<i class="iI i07"></i>
		<span class="iSpan i14"><?=Yii::t( '*', 'Activity' )?></span>
		<div class="iDiv i06">
			<i class="iI i04 wToggle iOpened"></i>
		</div>
	</a>

	<div class="iDiv i09 i15" style="display:block;">
		<?
			$this->widget( 'widgets.UserActivitiesWidget', Array( 
				'ns' => $ns, 
			))
		?>
	</div>
</div>