<?
	$ns = $this->getNS();
	$ins = $this->getINS();
	$NSi18n = $this->getNSi18n();
?>
<div class="" idActivity="<?=$activity->id?>">
	<div style="float:left">
		<?=$activity->user->getHTMLAvatar( Array( 'class' => 'iImg i02' ))?>
	</div>
				
	<div style="padding-left:50px;">
		<?=$activity->getIcon()?>
		<?=CHtml::link( CHtml::encode( $activity->user->showName ), $activity->user->getProfileURL())?>
		<?=$activity->message?><br>
		<?$this->widget( 'widgets.parts.TimeDiffWidget', Array( 'time' => $activity->createdDT ))?>
	</div>

	<div style="clear:both"></div>

	<div class="iDiv i10"></div>
</div>