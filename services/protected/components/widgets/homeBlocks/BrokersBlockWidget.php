<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class BrokersBlockWidget extends WidgetBase {
		private function getBest() {
			$models = BrokerModel::model()->findAll(Array(
				'with' => Array( 'i18ns', 'currentLanguageI18N', 'stats' ),
				'condition' => " `t`.`showInRating` ",
				'order' => " `stats`.`place` ",
				'limit' => 3,
			));
			$best = Array();
			foreach( $models as $model ) {
				$best[] = CHtml::link( CHtml::encode( $model->shortName ), $model->getSingleURL());
			}
			return implode( ', ', $best );
		}
		private function getNew() {
			$models = BrokerModel::model()->findAll(Array(
				'with' => Array( 'i18ns', 'currentLanguageI18N' ),
				'condition' => " `t`.`showInRating` ",
				'order' => " `t`.`createdDT` DESC ",
				'limit' => 3,
			));
			$new = Array();
			foreach( $models as $model ) {
				$new[] = CHtml::link( CHtml::encode( $model->shortName ), $model->getSingleURL());
			}
			return implode( ', ', $new );
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'best' => $this->getBest(),
				'new' => $this->getNew(),
			));
		}
	}

?>