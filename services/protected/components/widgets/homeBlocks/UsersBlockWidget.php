<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class UsersBlockWidget extends WidgetBase {
		private function getCountUsers() {
			return UserModel::model()->count();
		}
		private function getCountUsersToday() {
			$c = new CDbCriteria();
			$c->addCondition( " DATE( `t`.`user_registered` ) = CURDATE() " );
			return UserModel::model()->count( $c );
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'countUsers' => $this->getCountUsers(),
				'countUsersToday' => $this->getCountUsersToday(),
			));
		}
	}

?>