<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class CalendarEventsBlockWidget extends WidgetBase {
		public $limit = 3;
		private function getEvents() {
			return CalendarEvent2ValueModel::model()->findAll(Array(
				'with' => Array( 
					'event', 
				),
				'condition' => "
					`t`.`dt` > NOW()
				",
				'order' => " `t`.`dt` ",
				'limit' => $this->limit,
			));
		}
		
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'events' => $this->getEvents(),
			));
		}
	}

?>