<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class StrategiesBlockWidget extends WidgetBase {
		public $limit = 3;
		private function getStrategies() {
			return StrategyModel::model()->findAll(Array(
				'order' => " `t`.`createdDT` DESC ",
				'limit' => $this->limit,
			));
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'strategies' => $this->getStrategies(),
			));
		}
	}

?>