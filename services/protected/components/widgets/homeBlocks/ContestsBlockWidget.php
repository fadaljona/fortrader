<?
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class ContestsBlockWidget extends WidgetBase {
		private function getRegistered() {
			return ContestModel::model()->findAll(Array(
				'with' => Array( 
					'i18ns', 
					'currentLanguageI18N', 
					'servers' => Array(
						'with' => Array( 'broker' ),
					),
				),
				'condition' => " `t`.`endReg` >= CURDATE() ",
				'order' => " `t`.`createdDT` DESC ",
			));
		}
		function formatDate( $date ) {
			$time = strtotime( $date );
			$Y = date( "Y", $time );
			$M = date( "F", $time );
			$M = strtolower( $M );
			$M = Yii::t( '*', $M );
			$d = date( "d", $time );
			$date = "{$d} {$M}";
			
			$yearNow = date( "Y" );
			if( $Y != $yearNow ) $date = "{$date} {$Y}";
			
			return $date;
		}
		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'registered' => $this->getRegistered(),
			));
		}
	}

?>