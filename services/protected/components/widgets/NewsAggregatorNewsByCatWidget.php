<?php
	Yii::import( 'components.widgets.base.WidgetBase' );
	
	final class NewsAggregatorNewsByCatWidget extends WidgetBase {
		public $currentCat = 0;
		
		private function getNews(){
			return NewsAggregatorNewsModel::getListDp(0, $this->currentCat);
		}

		function run() {
			$class = $this->getCleanClassName();
			$this->render( "{$class}/view", Array(
				'currentCat' => $this->currentCat,
				'newsDP' => $this->getNews(),
			));
		}
	}

?>