<?php
Yii::import('components.widgets.base.WidgetBase');

final class BitcoinStatsWidget extends WidgetBase
{
    public $bitcoin;

    public function getChangeClass()
    {
        $bid = $this->bitcoin->bid;
        if ($this->bitcoin->lastBid) {
            if ($bid - $this->bitcoin->lastBid > 0) {
                return 'green';
            } elseif ($bid - $this->bitcoin->lastBid < 0) {
                return 'red';
            }
        }
        return '';
    }

    public function run()
    {
        $model = CryptoCurrenciesBitcoinStatsModel::getLastStats();
        if (!$model) {
            return false;
        }

        $settings = CryptoCurrenciesSettingsModel::getModel();
        if (!$settings->bitcoinSymbol) {
            return false;
        }

        $this->bitcoin = $settings->bitcoinSymbol;

        $class = $this->getCleanClassName();
        $this->render("{$class}/view", array(
            'model' => $model
        ));
    }
}
