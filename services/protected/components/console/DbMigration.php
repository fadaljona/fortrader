<?php
class DbMigration extends CDbMigration {
  public function execute ($sql, $params = array(), $log = false)
  {
    if ( $log )
      $this->_logExecute($sql, $params);
    parent::execute($sql, $params);
  }

  private function _logExecute($sql, $params = array()) {
    $msg = array();
    $msg[] = __METHOD__ . ' :';
    $msg[] = $sql;
    $msg[] = "dbConnection: '{$this->dbConnection->connectionString}'";
    if ( !empty($params) ) {
      $msg[] = 'Params:';
      $msg[] = print_r($params, 1);
    }
    Yii::log(join("\n", $msg) . "\n", CLogger::LEVEL_INFO, 'migrate.execute');
  }

} 