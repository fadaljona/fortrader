<?
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'components.MailerComponent' );
		
	final class AdvertisementCampaignMonitorComponent extends ComponentBase {
		const idAdmin = 1;
		private static function getMailer() {
			$factoryMailer = new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			return $mailer;
		}
		private static function getMailTpl() {
			$key = 'Adverster campaign finished';
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			if( !$mailTpl ) self::throwI18NException( "Can't load mail template! ({key})", Array( '{key}' => $key ));
			return $mailTpl;
		}
		private static function getMailTplI18N( $mailTpl ) {
			$i18n = $mailTpl->currentLanguageI18N;
			if( $i18n and strlen( $i18n->message )) return $i18n;
			foreach( $mailTpl->i18ns as $i18n ) if( strlen( $i18n->message )) return $i18n;

			self::throwI18NException( "Can't load i18n for mail template!" );
		}
		private static function renderMessage( $model, $message ) {
			$url = Yii::App()->createAbsoluteURL( '/admin/advertisementCampaign/list', Array( 'idBroker' => $model->owner->idBroker , 'idEdit' => $model->id ));
			return strtr( $message, Array(
				'%name%' => $model->name,
				'%link%' => $url,
			));
		}
		private static function sendMailEndDate( $emails, $model ) {
			$mailer = self::getMailer();
			$mailTpl = self::getMailTpl();
			$i18n = self::getMailTplI18N( $mailTpl );
						
			$message = self::renderMessage( $model, $i18n->message );
			
			$emails = explode( ",", $emails );
			$emails = array_map( 'trim', $emails );
			foreach( $emails as $email ) {
				$mailer->AddAddress( $email );
			}
			$mailer->Subject = $i18n->subject;
			if( substr_count( $message, '<' )) {
				$message = CommonLib::nl2br( $message, true );
				$mailer->MsgHTML( $message );
			}
			else{
				$mailer->Body = $message;
			}
			
			$result = $mailer->Send();
			$error = $result ? null : $mailer->ErrorInfo;
			if( $error ) throw new Exception( $error );
		}
		private static function createNoticeEndDate( $model ) {
			UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_END_ADVERTISEMENT_CAMPAIGN, Array(
				'idUser' => self::idAdmin,
				'idCampaign' => $model->id,
			));
		}
		private static function checkEndDate() {
			$settings = AdvertisementSettingsModel::getModel();
			$models = AdvertisementCampaignModel::model()->findAll(Array(
				'with' => Array(
					'owner' => Array(
						'select' => "ID,idBroker",
					),
				),
				'condition' => " DATE( DATE_ADD(`t`.`end`, INTERVAL -1 DAY )) = CURDATE() ",
			));
			foreach( $models as $model ) {
				if( $settings->sendEmail and strlen( $settings->emails )) {
					self::sendMailEndDate( $settings->emails, $model );
				}
				self::createNoticeEndDate( $model );
			}
		}
		private static function sendPrevMoCampaings(){
			$settings = AdvertisementSettingsModel::getModel();
			$models = AdvertisementCampaignModel::model()->findAll(Array(
				'with' => Array(
					'owner' => Array(
						'select' => "ID,idBroker",
					),
				),
				'condition' => " `t`.`end` <= :endMo AND `t`.`end` >= :startMo ",
				'order' => " `t`.`end` DESC",
				'params' => array( ':startMo' => date('Y-m-d', strtotime('first day of previous month')), ':endMo' => date('Y-m-d', strtotime('last day of previous month')) ),
			));
			$bodyMsg = '';
			foreach( $models as $model ){
				$url = Yii::App()->createAbsoluteURL( '/admin/advertisementCampaign/list', Array( 'idBroker' => $model->owner->idBroker , 'idEdit' => $model->id ));
				$bodyMsg .= $model->name . "\t" . $url . "\t" . $model->end . "\n\r";
			}
			if( $bodyMsg ){
				$mailer = self::getMailer();
				$emails = explode( ",", $settings->emails );
				$emails = array_map( 'trim', $emails );
				foreach( $emails as $email ) {
					$mailer->AddAddress( $email );
				}
				$mailer->Subject = "Завершенные рекламные кампании в прошлом месяце";
				$mailer->Body = $bodyMsg;
				
				
				$result = $mailer->Send();
				$error = $result ? null : $mailer->ErrorInfo;
				if( $error ) throw new Exception( $error );
			}
		}
		static function monitor() {
			$settings = AdvertisementSettingsModel::getModel();
			if( !$settings->endDate ) return false;
			
			self::checkEndDate();
			if( date('j') == 1 ) self::sendPrevMoCampaings();
		}
	}
			
?>