<?
	
	final class NavlistComponent extends ComponentExBase {
		const pathNavlist = 'protected/data/navlist.xml';
		const pathAdminNavlist = 'protected/data/adminNavlist.xml';
		static function getNavlist() {
			static $navlist;
			
			if( $navlist === null ) {
				$navlist = new Navlist();
				$navlist->load( self::pathNavlist );
			}
			
			return $navlist;
		}
		static function getAdminNavlist() {
			static $navlist;
			
			if( $navlist === null ) {
				$navlist = new Navlist();
				$navlist->load( self::pathAdminNavlist );
			}
			
			return $navlist;
		}
	}
	
	final class Navlist {
		public $XML; 
		function __construct() {
			$this->XML = new DOMDocument( "1.0", "UTF-8" );
		}
		function getXML() {
			return $this->XML;
		}
		function getXPath() {
			return new DOMXPath( $this->getXML() );
		}
		function getItemsByStartsUrls( $urls, $additionalConditions = Array() ) {
			$XPath = $this->getXPath();
			
			$selectors = Array();
			foreach( (array)$urls as $url ) {
				$selector = "//item[ starts-with( @url, '{$url}' )]";
				foreach( (array)$additionalConditions as $condition ) {
					$selector .= "[{$condition}]";
				}
				$selectors[] = $selector;
			}
			$selectors = implode( "|", $selectors );
			$nodes = $XPath->query( $selectors );
			
			$items = Array();
			foreach( $nodes as $node ) {
				$items[] = new NavlistItem( $node );
			}
			
			return $items;
		}
		function getItemsByCurrentUrl( $additionalConditions = Array()) {
			$idController = Yii::App()->controller->id;
			$idAction = Yii::App()->controller->action->id;
			
			$url = "{$idController}/{$idAction}";
			
			$module = Yii::App()->controller->getModule();
			if( $module ) $url = "{$module->id}/{$url}";
			
			$urls = Array(
				"{$url}/",
				"{$url}?",
			);
			return $this->getItemsByStartsUrls( $urls, $additionalConditions );
		}
		function getActiveItems() {
			$XPath = $this->getXPath();
			$nodes = $XPath->query( "//item[@active='yes']" );
			
			$items = Array();
			foreach( $nodes as $node ) {
				$items[] = new NavlistItem( $node );
			}
			return $items;
		}
		function autoSetActiveItems() {
			$items = $this->getItemsByCurrentUrl( "not(@active)" );
			foreach( $items as $item ) {
				$item->setActive( true );
			}
		}
		function load( $path ) {
			$this->getXML()->load( $_SERVER['DOCUMENT_ROOT'].'/services/'.$path );
			
			$this->autoSetActiveItems();
		}
		function getItems() {
			$items = Array();
			
			$XPath = $this->getXPath();
			$nodes = $XPath->query( "/navlist/item" );
			
			foreach( $nodes as $node ) {
				$items[] = new NavlistItem( $node );
			}
			
			return $items;
		}
		function getItemsSubmenu() {
			$itemsSubmenu = Array();
			
			$XPath = $this->getXPath();
			$nodes = $XPath->query( "//item[@hasSubmenu='yes']/item[@active='yes']" );
			$activeNode = $nodes->length ? $nodes->item( 0 ) : null;
			if( $activeNode ) {
				$activeItem = new NavlistItem( $activeNode );
				$itemsSubmenu = $activeItem->getSiblings();
			}			
			
			return $itemsSubmenu;
		}
		function findItemById( $id ) {
			$XPath = $this->getXPath();
			$query = "//item[ @id='{$id}' ]";
			$nodes = $XPath->query( $query );
			
			if( !$nodes->length ) return null;
			$item = new NavlistItem( $nodes->item( 0 ));
			
			return $item;
		}
		function createItemAfter( $before ) {
			$beforeNode = $before->getNode();
			
			$newNode = $beforeNode->ownerDocument->createElement( "item" );
			
			$nextSibling = $beforeNode->nextSibling;
			if( $nextSibling ) {
				$newNode = $beforeNode->parentNode->insertBefore( $newNode, $nextSibling );
			}
			else{
				$newNode = $beforeNode->parentNode->appendChild( $newNode );
			}
			
			$newItem = new NavlistItem( $newNode );
			return $newItem;
		}
	}
		
	final class NavlistItem {
		private $node;
		function __construct( $node ) {
			$this->setNode( $node );
		}
		function setNode( $node ) {
			$this->node = $node;
		}
		function getNode() {
			return $this->node;
		}
		function getXPath() {
			return new DOMXPath( $this->getNode()->ownerDocument );
		}
		function getAttribute( $key ) {
			return $this->getNode()->getAttribute( $key );
		}
		function setAttribute( $key, $value ) {
			return $this->getNode()->setAttribute( $key, $value );
		}
		function removeAttribute( $key ) {
			$this->getNode()->removeAttribute( $key );
		}
		function getLabel() {
			$label = $this->getAttribute( 'label' );
			$label = Yii::t( '*', $label );
			return $label;
		}
		function setLabel( $value ) {
			$this->setAttribute( 'label', $value );
		}
		function getIcon() {
			$icon = $this->getAttribute( 'icon' );
			return $icon;
		}
		function getClass() {
			$class = $this->getAttribute( 'class' );
			return $class;
		}
		function getURL() {
			$strictUrl = $this->getAttribute( 'strictUrl' );
			if( strlen( $strictUrl )) return $strictUrl;
			
			$url = $this->getAttribute( 'url' );
			if( !strlen( $url )) {
				if( $this->hasSubmenu()) {
					$subitems = $this->getSubitems();
					foreach( $subitems as $subitem ) {
						if( $subitem->getDisable()) continue;
						if( !$subitem->userHasRight()) continue;
						
						$url = $subitem->getURL();
						if( strlen( $url )) return $url;
					}
				}
			}
			
			$url = $url ? Yii::App()->createURL( $url ) : "";
			return $url;
		}
		function setURL( $value ) {
			$this->setAttribute( 'url', $value );
		}
		function setStrictURL( $value ) {
			$this->setAttribute( 'strictUrl', $value );
		}
		function getActive() {
			$value = $this->getAttribute( 'active' );
			return $value === "yes"; 
		}
		function setActive( $flag = true ) {
			if( $flag ) {
				$this->setAttribute( 'active', "yes" );
			}
			else{
				$this->removeAttribute( 'active' );
			}
		}
		function getRight() {
			$right = $this->getAttribute( 'right' );
			return $right;
		}
		function getRightOperator() {
			$rightOperator = $this->getAttribute( 'rightOperator' );
			if( !strlen( $rightOperator )) $rightOperator = "or";
			return $rightOperator;
		}
		function getDisable() {
			$value = $this->getAttribute( 'disable' );
			return $value === "yes"; 
		}
		function hasActiveSubitems() {
			$XPath = $this->getXPath();
			
			$query = "descendant::item[@active='yes']";
			$nodes = $XPath->query( $query, $this->getNode() );
			
			return !!$nodes->length;
		}
		function hasSubmenu() {
			$value = $this->getAttribute( 'hasSubmenu' );
			return $value === "yes"; 
		}
		function userHasRight() {
			$right = $this->getRight();
						
			if( $right === '{admin}' ) {
				return Yii::App()->user->hasAdminRight();
			}
			
			if( !strlen( $right )) {
				$subitems = $this->getSubitems();
				if( $subitems ) {
					foreach( $subitems as $subitem ) {
						if( $subitem->userHasRight()) return true;
					}
					return false;
				}
				else{
					return true;
				}
			}
			
			$rights = explode( ',', $right );
			$rights = array_map( 'trim', $rights );
			$rightOperator = $this->getRightOperator();
			return Yii::App()->user->checkAccessArray( $rights, $rightOperator );
		}
		function getSubitems() {
			$items = Array();
			
			$XPath = $this->getXPath();
			$nodes = $XPath->query( "item", $this->getNode() );
			
			foreach( $nodes as $node ) {
				$items[] = new NavlistItem( $node );
			}
			
			return $items;
		}
		function getSiblings() {
			$items = Array();
			
			$XPath = $this->getXPath();
			$nodes = $XPath->query( "parent::*/item", $this->getNode() );
			
			foreach( $nodes as $node ) {
				$items[] = new NavlistItem( $node );
			}
			
			return $items;
		}
	}

?>
