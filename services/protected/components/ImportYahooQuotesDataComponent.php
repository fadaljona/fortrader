<?php
Yii::import('components.base.ComponentBase');
        
final class ImportYahooQuotesDataComponent extends ComponentBase
{
    private $prefixToRemovePatterns = array();
    private $symbolsToStrip = array();
    private $groupedDateQuotesArr = array();
    private $idToHistnameArr = array();
    private $quotesDataToSave = array();
    public $perRequestForSearchNewQuotes = 100;
    public $perRequestForGetNewData = 100;
    public $daysPerRequestForGetNewData = 40;
    public $firstDaysOffset = 1460;

    private $moToSetDoNotCheckYahoo = 4;
        
    public function __construct()
    {
        $quotesSettings = QuotesSettingsModel::getInstance();
        $prefixies = explode('||', $quotesSettings->prefixToRemove);
        foreach ($prefixies as $prefix) {
            $this->prefixToRemovePatterns[] = '/^' . preg_quote($prefix) . '/';
        }
        $this->symbolsToStrip = $quotesSettings->symbolsToStrip;
    }

    private function getQuotesSearchUrl($symbolsStr)
    {
        return "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%20in%20(" . urlencode($symbolsStr) . ")&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    }

        /*private function getQuotesDataUrl($symbolsStr, $startDate){
            return "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%20in%20(" . urlencode( $symbolsStr ) . ")%20and%20startDate%20%3D%20%22" . $startDate . "%22%20and%20endDate%20%3D%20%22" . date( 'Y-m-d', strtotime( $startDate ) + $this->daysPerRequestForGetNewData * 60*60*24    ) . "%22&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        }*/
        
    private function stripChars($symbol)
    {
        return preg_replace($this->prefixToRemovePatterns, '', str_replace($this->symbolsToStrip, '', $symbol));
    }
    
    private function getAllQuotes()
    {
        $summArr = array();
        $quotesSettings = QuotesSettingsModel::getInstance();
        $sources = $quotesSettings->quotesTickSource;
        foreach ($sources as $source) {
            $quotesStr = trim(CommonLib::downloadFileFromWww($source));
            $quotes = explode("\r\n", $quotesStr);

            foreach ($quotes as $quote) {
                $delimerPos = strpos($quote, ';');
                $symbol = substr($quote, 0, $delimerPos);
                $summArr[$symbol] = $this->stripChars($symbol);
            }
        }
        return array_unique($summArr);
    }
        
    private function getExistQuotes()
    {
        $models = QuotesSymbolsModel::model()->findAll(array(
            'select' => array( " `t`.`tickName` ", " `t`.`histName` " ),
        ));
        $outArr = array();
        foreach ($models as $model) {
            if ($model->tickName) {
                $outArr[$model->tickName] = $this->stripChars($model->tickName);
            }
            if ($model->histName) {
                $outArr[$model->histName] = $this->stripChars($model->histName);
            }
        }
        return array_unique($outArr);
    }

        /*private function getIters( $arrCount, $perRequest ){
            if( $arrCount < $perRequest ) return 1;
            $ost = $arrCount % $perRequest;
            if( $ost == 0 ) return $arrCount / $perRequest;
            return ($arrCount-$ost) / $perRequest + 1;
        }*/

    private function setGroupedDateQuotesArr()
    {
        $models = QuotesSymbolsModel::model()->findAll(array(
            'select' => array( " `t`.`id` ", " `t`.`histName` " ),
            'condition' => " `t`.`sourceType` = 'yahoo' AND `t`.`histName` <> '' AND `t`.`yahooCheckedInCircle` = 0 AND `t`.`doNotCheckYahoo` = 0 ",
            'limit' => 50
        ));

        foreach ($models as $model) {
            if ($model->lastDayData) {
                $date = date('Y-m-d', $model->lastDayData->barTime + 60*60*24);
            } else {
                $date = date('Y-m-d', time() - $this->firstDaysOffset * 60*60*24);
            }
            if ($date == date('Y-m-d', time())) {
                $model->yahooCheckedInCircle = 1;
                $model->save();
                continue;
            }
            $this->groupedDateQuotesArr[$date][] = $model->histName;
            $this->idToHistnameArr[$model->id] = $model->histName;
        }
        if (!count($this->groupedDateQuotesArr)) {
            $sql =  " UPDATE `{{quotes_symbols}}` SET `yahooCheckedInCircle` = 0 ";
            Yii::App()->db->createCommand($sql)->query();
        }
    }
    private function setGroupedDateQuotesArrForOwnQuotes()
    {
        $models = QuotesSymbolsModel::model()->findAll(array(
            'select' => array( " `t`.`id` ", " `t`.`yahooHistName` " ),
            'condition' => " `t`.`sourceType` = 'own' AND `t`.`histName` <> '' AND `t`.`yahooHistName` <> '' ",
        ));
        foreach ($models as $model) {
            if ($model->lastDayData) {
                $date = date('Y-m-d', $model->lastDayData->barTime + 60*60*24);
            } else {
                $date = date('Y-m-d', time() - $this->firstDaysOffset * 60*60*24);
            }
            if ($date == date('Y-m-d', time())) {
                continue;
            }
            $this->groupedDateQuotesArr[$date][] = $model->yahooHistName;
            $this->idToHistnameArr[$model->id] = $model->yahooHistName;
        }
    }

    private function downloadFileFromWww($url, $connectTimeout = 10, $timeout = 120)
    {
        $header[] = "Accept: */*";
        $header[] = "Accept-Encoding: identity";
        $header[] = "Connection: Keep-Alive";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connectTimeout);
        curl_setopt($ch, CURLOPT_USERAGENT, "Wget/1.17.1 (linux-gnu)");
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        $data = curl_exec($ch);
        
        curl_close($ch);
        return $data;
    }

    private function downloadCsvFileFromWww($url, $bCookieVal, $referer)
    {
        $connectTimeout=10;
        $timeout=120;
        $header[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Accept-Language: en-US,en;q=0.9";
        $header[] = "Pragma: ";
        $header[] = "Cookie: B={$bCookieVal}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connectTimeout);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.89 Chrome/62.0.3202.89 Safari/537.36");
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    
    private function setQuotesDataToSave()
    {
        foreach ($this->groupedDateQuotesArr as $date => $symbols) {
            foreach ($symbols as $symbol) {
                $frontUrl = 'https://finance.yahoo.com/quote/'.$symbol.'/history';

                echo $frontUrl . "\n";

                $frontData = $this->downloadFileFromWww($frontUrl);
                if (!$frontData) {
                    echo "Can't get data\n";
                    echo $frontData . "dd\n";
                    continue;
                }

                preg_match('/^Set-Cookie:\s*B=([^;]*)/mi', $frontData, $matches);

                if (!$matches || !isset($matches[1])) {
                    echo "Can't find Set-Cookie\n";
                    continue;
                }

                $bCookieVal = $matches[1];

                preg_match('/CrumbStore":{"crumb":"([^"]+)"/i', $frontData, $matches);
                    
                if (!$matches || !isset($matches[1])) {
                    echo "Can't find CrumbStore\n";
                    continue;
                }

                $crumbVal = $matches[1];

                $period1 = strtotime($date) - 60*60*24;
                $period2 = time();
                $csvUrl = "https://query1.finance.yahoo.com/v7/finance/download/{$symbol}?period1={$period1}&period2={$period2}&interval=1d&events=history&crumb={$crumbVal}";
                    
                    
                $csvData = $this->downloadCsvFileFromWww($csvUrl, $bCookieVal, $frontUrl);

                if (!$csvData) {
                    echo "Empty data for $csvUrl\n";
                    continue;
                }

                $csvArr = array_map('str_getcsv', explode(PHP_EOL, $csvData));
                if (!$csvArr) {
                    echo "Can't parse csv\n";
                    continue;
                }
                
                unset($csvArr[0]);
                    
                if (!count($csvArr)) {
                    echo "Rows count in csv 0\n";
                    continue;
                }

                $this->quotesDataToSave[$symbol] = $csvArr;
            }
        }
    }

    private function saveQuotesDataToDb()
    {
        foreach ($this->quotesDataToSave as $histName => $data) {
            $quoteId = array_search($histName, $this->idToHistnameArr);
            $insertSql = 'INSERT IGNORE INTO `{{quotes_history_data}}` (`symbolId`, `barTime`, `high`, `low`, `open`, `close`, `volume`, `resolution`) VALUES ';
            $insertParams = array( ':symbolId' => $quoteId, ':resolution' => 1440 );
            $i=0;

            foreach ($data as $oneData) {
                if ($i) {
                    $insertSql .= ',';
                }
                $insertSql .= "( :symbolId, :barTime$i, :high$i, :low$i, :open$i, :close$i, :volume$i, :resolution)";
                $insertParams[":barTime$i"] = strtotime($oneData[0]);
                $insertParams[":open$i"] = $oneData[1];
                $insertParams[":high$i"] = $oneData[2];
                $insertParams[":low$i"] = $oneData[3];
                $insertParams[":close$i"] = $oneData[4];
                $insertParams[":volume$i"] = $oneData[6];
                $i++;
            }
            if (Yii::App()->db->createCommand($insertSql)->query($insertParams)) {
                echo "data for $histName inserted. rows ". count($data) ." \n";
            }
                /*echo $insertSql . "\n\n\n";
                print_r( $insertParams );
                echo "\n\n\n" . $histName . "\n\n\n";
                print_r( $data );*/
        }
        foreach ($this->idToHistnameArr as $quoteId => $histname) {
            $model = QuotesSymbolsModel::model()->findByPk($quoteId);
            $model->yahooCheckedInCircle = 1;
            $model->save();
        }
    }

    private function setUnvisibleYahooQuotes()
    {
        $sql =  " 
SELECT `symbols`.`id` 
FROM `{{quotes_symbols}}` `symbols` 
LEFT JOIN `{{quotes_history_data}}` `data` ON `data`.`symbolId` = `symbols`.`id` 
WHERE `data`.`symbolId` IS NULL AND `symbols`.`sourceType` = 'yahoo' AND `symbols`.`histName` <> '' AND `symbols`.`visible` = 'Visible'
GROUP BY `symbols`.`id` ";
        $result = Yii::App()->db->createCommand($sql)->queryAll();

        foreach ($result as $row) {
            $command = Yii::app()->db->createCommand();
            $command->update('{{quotes_symbols}}', array(
                'visible' => 'Unvisible',
            ), ' id = :id ', array( ':id' => $row['id'] ));
        }
    }

    private function setupMessages()
    {
        $models = QuotesSymbolsModel::model()->findAll(array(
            'condition' => " `t`.`sourceType` = 'yahoo' AND `t`.`title` = '' AND `t`.`nalias` <> '' "
        ));
            
        foreach ($models as $model) {
            $translatedNalias = CommonLib::translitStrToRu($model->nalias);
            
            $model->metaTitle = "Акции и график {$translatedNalias} / {$model->nalias} / котировки, цена и стоимость компании сегодня";
            $model->title = "Акции {$translatedNalias} / {$model->nalias} ({$model->name})";
            $model->preview = "На этой странице вы можете узнать цену на акции компании {$translatedNalias} , а так же посмореть график и увидеть динамику изменения котировки.";
            $model->searchPhrases = "{$translatedNalias}, {$model->nalias}";
            $model->tooltip = $model->nalias;
            $model->metaKeywords = "Акции {$translatedNalias}, Акции {$model->nalias}";
            $model->chartTitle = "График и динамика акций {$translatedNalias}";
            $model->wpPostsTitle = "Прогнозы по {$translatedNalias}";
                
            if ($model->validate() && $model->save()) {
                echo "texts saved for {$model->name}\n";
            }
        }
    }

    private function getNotCheckedQuotes()
    {
        $models = QuotesYahooToCheckModel::model()->findAll(array(
            'condition' => " `t`.`checked` = 0 ",
            'limit' => 1000,
        ));
        $outArr = array();
        foreach ($models as $model) {
            $outArr[] = $model->symbol;
        }
        return $outArr;
    }

    private function createNewYahooQuotes($inpArr)
    {
        if (!$inpArr) {
            return false;
        }
        $arrayForSearch = array_values($inpArr);
        $iters = $this->getIters(count($arrayForSearch), $this->perRequestForSearchNewQuotes);
            
        for ($i=0; $i<$iters; $i++) {
            $symbolsStr = '';
            $max = ($i+1) * $this->perRequestForSearchNewQuotes < count($arrayForSearch) ? ($i+1) * $this->perRequestForSearchNewQuotes : count($arrayForSearch);

            for ($j=$i * $this->perRequestForSearchNewQuotes; $j<$max; $j++) {
                if ($j > $i * $this->perRequestForSearchNewQuotes) {
                    $symbolsStr .= ',';
                }
                $symbolsStr .= '"' . trim($arrayForSearch[$j]) . '"';
            }

            $url = $this->getQuotesSearchUrl($symbolsStr);
            $xmlStr = file_get_contents($url);
            $xmlObj = simplexml_load_string($xmlStr);

            if (!$xmlObj) {
                continue;
            }

            foreach ($xmlObj->results->quote as $quote) {
                $symbol = $quote->attributes()->symbol->__toString();
                $symbolName = $quote->Name->__toString();
                $tickName = array_search($symbol, $inpArr);
                    
                //echo "found $symbol $symbolName  $symbol\n";
                    
                if ($symbolName && $tickName) {
                    if (QuotesSymbolsModel::createYahooQuote($symbol, $symbolName, $symbol)) {
                        echo "quote created $symbol\n";
                    } else {
                        echo "quote not created $symbol - error occured\n";
                    }
                }/*else{
                        if( QuotesNotFoundInYahooModel::saveNotFoundSymbol( $symbol ) ){
                            echo "not found quote saved $symbol\n";
                        }else{
                            echo "not found quote not saved $symbol - error occured\n";
                        }    
                    }*/
            }
        }
        foreach ($inpArr as $sym) {
            $model = QuotesYahooToCheckModel::model()->find(array(
                'condition' => " `t`.`symbol` = :symbol ",
                'params' => array(':symbol'=>$sym),
            ));
            $model->checked = 1;
            $model->save();
        }
    }
        
    public function importNewQuotes()
    {
        $notCheckedQuotes = $this->getNotCheckedQuotes();
            
        $this->createNewYahooQuotes($notCheckedQuotes);
            
        //print_r( $notCheckedQuotes );
        /*$allQuotes = $this->getAllQuotes();
        $existQuotes = $this->getExistQuotes();
        $notFoundQuotes = QuotesNotFoundInYahooModel::getSimbolsArray();
        $newQuotes = array_diff( $allQuotes, $existQuotes, $notFoundQuotes );
        $this->createNewYahooQuotes( $newQuotes );
        $this->setupMessages();*/
    }
        
    public function importYahooData()
    {
        date_default_timezone_set('UTC');
        if (isset($_GET['setUnvisibleYahooQuotes'])) {
            $this->setUnvisibleYahooQuotes();
            echo "UnvisibleYahooQuotes setuped\n";
            return false;
        }
            
        $this->setGroupedDateQuotesArr();
            
        /*print_r($this->idToHistnameArr);
        print_r($this->groupedDateQuotesArr);*/
            
            
        $this->setQuotesDataToSave();
        $this->saveQuotesDataToDb();
    }

    public function importYahooDataForOwnQuotes()
    {
        $quotesSettings = QuotesSettingsModel::getInstance();
        date_default_timezone_set($quotesSettings->statsDataTimeZone);

        $this->setGroupedDateQuotesArrForOwnQuotes();
        $this->setQuotesDataToSave();
        $this->saveQuotesDataToDb();
    }

    public function setDoNotCheckYahoo()
    {
        $sql =  "SELECT `symbol`.`id` FROM `{{quotes_symbols}}` `symbol`
        LEFT JOIN (SELECT `symbolId`, MAX(`barTime`) `lastTime` FROM `{{quotes_history_data}}` WHERE `resolution` = 1440 GROUP BY `symbolId`) `lastDay`
            ON `lastDay`.`symbolId` = `symbol`.`id`
        WHERE `symbol`.`sourceType` = 'yahoo' 
            AND `symbol`.`histName` <> '' 
            AND `symbol`.`doNotCheckYahoo` = 0 
            AND `symbol`.`histName` IS NOT NULL 
            AND `lastDay`.`lastTime` < (UNIX_TIMESTAMP() - 60*60*24*30*{$this->moToSetDoNotCheckYahoo})
        LIMIT 200";

        $result = Yii::App()->db->createCommand($sql)->queryAll();

        foreach ($result as $row) {
            $command = Yii::app()->db->createCommand();
            $command->update('{{quotes_symbols}}', array(
                'doNotCheckYahoo' => 1,
            ), ' id = :id ', array( ':id' => $row['id'] ));
            echo "doNotCheckYahoo setted for {$row['id']}\n";
        }
    }
}
