<?
	
	abstract class DataColumnBase extends CDataColumn {
		protected function resolveClassName( $class ) {
			return $class ? $class : get_class( $this );
		}
		function getCleanClassName( $class = null ) {
			$class = $this->resolveClassName( $class );
			$class = preg_replace( "#DataColumn$#", "", $class );
			$class[0] = strtolower($class[0]);
			return $class;
		}
		function getNSi18n( $class = null ) {
			$class = $this->getCleanClassName( $class );
			return "components/gridColumns/{$class}";
		}
		function getViewPath( $classFile, $class = null ) {
			$class = $this->getCleanClassName( $class );
			$dir = dirname( $classFile );
			return "{$dir}/views/{$class}";
		}
	}

?>