<?
Yii::import( 'dataColumns.base.DataColumnBase' );

final class NewsAggregatorRssSourceDataColumn extends DataColumnBase {
	function init() {
		$sources = NewsAggregatorSourceModel::model()->findAll();	
		$this->filter = Array();
		foreach( $sources as $source ){
			$this->filter[$source->id] = $source->title;
		}
		parent::init();
	}
}
