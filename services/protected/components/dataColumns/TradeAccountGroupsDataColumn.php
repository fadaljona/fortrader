<?
	Yii::import( 'dataColumns.base.DataColumnBase' );

	final class TradeAccountGroupsDataColumn extends DataColumnBase {
		function init() {
			$this->filter = Array();
			$groups = Array( 'Contests', 'EA' );
			foreach( $groups as $group ) {
				$this->filter[ $group ] = Yii::t( '*', $group );
			}
			parent::init();
		}
	}

?>