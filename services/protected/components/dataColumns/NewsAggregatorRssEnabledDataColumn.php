<?
Yii::import( 'dataColumns.base.DataColumnBase' );

final class NewsAggregatorRssEnabledDataColumn extends DataColumnBase {
	function init() {
		$this->filter = Array();
		$this->filter[0] = Yii::t('*', 'No');
		$this->filter[1] = Yii::t('*', 'Yes');
		parent::init();
	}
}
