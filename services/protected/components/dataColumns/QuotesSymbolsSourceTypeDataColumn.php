<?
Yii::import( 'dataColumns.base.DataColumnBase' );

final class QuotesSymbolsSourceTypeDataColumn extends DataColumnBase {
	function init() {
		$this->filter = Array();
		$this->filter[ 'own' ]=Yii::t( "*", 'own');
		$this->filter[ 'yahoo' ]=Yii::t( "*", 'yahoo');
        $this->filter[ 'bitz' ]=Yii::t( "*", 'bit-z.com');
        $this->filter[ 'bitfinex' ]=Yii::t( "*", 'bitfinex');
        $this->filter['okex'] = Yii::t("*", 'okex');
        $this->filter['binance'] = Yii::t("*", 'binance');
		
		parent::init();
	}
}
