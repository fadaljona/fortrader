<?
	Yii::import( 'dataColumns.base.DataColumnBase' );

	final class UserGroupsDataColumn extends DataColumnBase {
		private function sortGroups( $a, $b ) {
			return ($a->name < $b->name) ? -1 : 1;
		}
		private function getGroups() {
			$groups = UserGroupModel::model()->findAll();
			usort( $groups, Array( $this, 'sortGroups' ));
			return $groups;
		}
		function init() {
			$this->filter = Array();
			$groups = $this->getGroups();
			foreach( $groups as $group ) {
				$this->filter[ $group->id ] = Yii::t( '*', $group->name );
			}
			parent::init();
		}
		protected function renderDataCellContent( $row, $model ) {
			$groups = $model->groups;
			$viewPath = $this->getViewPath( __FILE__ );
			require "{$viewPath}/view.php";
		}
	}

?>