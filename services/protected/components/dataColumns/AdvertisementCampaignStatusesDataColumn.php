<?
	Yii::import( 'dataColumns.base.DataColumnBase' );

	final class AdvertisementCampaignStatusesDataColumn extends DataColumnBase {
		function init() {
			$this->filter = Array();
			$statuses = Array( 'Active', 'Pause', 'Block', 'Limit is settled', 'Not begun', 'Ended', 'End balance' );
			foreach( $statuses as $status ) {
				$this->filter[ $status ] = Yii::t( '*', $status );
			}
			parent::init();
		}
	}
	
?>