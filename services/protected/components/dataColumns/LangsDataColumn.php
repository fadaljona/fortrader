<?
Yii::import( 'dataColumns.base.DataColumnBase' );

final class LangsDataColumn extends DataColumnBase {
	function init() {
		$langs = CommonLib::getLanguages();	
		$this->filter = Array();
		foreach( $langs as $lang ){
			$this->filter[$lang->id] = $lang->name;
		}
		parent::init();
	}
}
