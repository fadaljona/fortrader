<?
Yii::import( 'dataColumns.base.DataColumnBase' );

final class QuotesSymbolsCatDataColumn extends DataColumnBase {
	function init() {
		$this->filter = Array();
		
		$aCatsFromModel=QuotesCategoryModel::getAll();

		if($aCatsFromModel){
			foreach($aCatsFromModel as $oCategory){
				if( $oCategory->id == 20 ) continue;
				$this->filter[$oCategory->id]=$oCategory->name;
			}
		}
		
		parent::init();
	}
}
