<?
	$alias = LanguageModel::getCurrentLanguageAlias();
	
	$letters = Array();
	if( $alias == 'ru' ) {
		$letters = str_split( "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ", 2 );
	}
	if( $alias == 'en_us' ) {
		$letters = str_split( "ABCDEFGHIJKLMNOPQRSTUVXYZ" );
	}
?>
<div class="filter-container">
	<?=CHtml::activeTextField( $this->grid->filter, $this->name, array( 'class' => "wName", 'id' => false ))?>
	<?$fActive=false;?>
	<?foreach( $letters as $letter ){?>
		<?$active = $this->grid->filter->indicatorForSearch == "{$letter}*" ? "active" : ""; ?>
		<?if( $active ) $fActive = true;?>
		<a class="iA i26 wLetter <?=$active?>" href="#" data-letter="<?=$letter?>"><?=$letter?></a>
	<?}?>
	<?if( $fActive ){?>
		<button class="btn btn-mini wLetterClear">
			[x]
		</button>
	<?}?>
</div>