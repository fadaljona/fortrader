<?
	$ip = long2ip( $model->ip );
?>
<?=$ip?>
<?if( Yii::App()->user->checkAccess( 'advertisementBlockControl' )){?>
	<?
		if( $model->blockIP ) {
			$type = 'success';
			$title = 'Unblock';
			$class = 'wUnBlockIP';
		}
		else{
			$type = 'danger';
			$title = 'Block';
			$class = 'wBlockIP';
		}
		$this->grid->widget( 'bootstrap.widgets.TbButton', Array( 
			'type' => $type,
			'size' => 'small',
			'label' => Yii::t( '*', $title ),
			'htmlOptions' => Array(
				'class' => "pull-right {$class}",
				'data-ip' => $ip
			),
		));
	?>
<?}?>