<?if( strlen( $model->agent )){?>
	<?=CommonLib::shorten( $model->agent, 50 )?>
	<?if( Yii::App()->user->checkAccess( 'advertisementBlockControl' )){?>
		<?
			if( $model->blockAgent ) {
				$type = 'success';
				$title = 'Unblock';
				$class = 'wUnBlockAgent';
			}
			else{
				$type = 'danger';
				$title = 'Block';
				$class = 'wBlockAgent';
			}
			$this->grid->widget( 'bootstrap.widgets.TbButton', Array( 
				'type' => $type,
				'size' => 'small',
				'label' => Yii::t( '*', $title ),
				'htmlOptions' => Array(
					'class' => "pull-right {$class}",
					'data-agent' => htmlspecialchars($model->agent),
				),
			));
		?>
	<?}?>
<?}?>