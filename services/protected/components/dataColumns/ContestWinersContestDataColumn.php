<?
	Yii::import( 'dataColumns.base.DataColumnBase' );

	final class ContestWinersContestDataColumn extends DataColumnBase {
		function init() {
			$this->filter = Array();
			
			$contests = Yii::app()->db->createCommand()
					->select('contest_winer.idContest, contest_i18n.name')
					->from('ft_contest_winer contest_winer') 
					->join('ft_contest_i18n contest_i18n', 'contest_i18n.idContest = contest_winer.idContest')
					->where( 'contest_i18n.idLanguage=:id', array(':id'=>LanguageModel::getCurrentLanguageID()) )
					->group('contest_winer.idContest') 
					->queryAll(); //Will get the all selected rows from table
	
	
			
			foreach( $contests as $contest ) {
				$this->filter[ $contest['idContest'] ] = $contest['name'];
			}
			
			parent::init();
		}
	}

?>