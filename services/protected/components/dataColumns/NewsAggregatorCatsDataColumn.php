<?
Yii::import( 'dataColumns.base.DataColumnBase' );

final class NewsAggregatorCatsDataColumn extends DataColumnBase {
	function init() {
		$sources = NewsAggregatorCatsModel::model()->findAll();	
		$this->filter = Array();
		foreach( $sources as $source ){
			$this->filter[$source->id] = $source->title;
		}
		parent::init();
	}
}
