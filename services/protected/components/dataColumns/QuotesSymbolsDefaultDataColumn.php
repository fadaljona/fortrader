<?
Yii::import( 'dataColumns.base.DataColumnBase' );

final class QuotesSymbolsDefaultDataColumn extends DataColumnBase {
	function init() {
		$this->filter = Array();
		$this->filter[ 'Yes' ]=Yii::t( "quotesWidget", 'Да');
		$this->filter[ 'No' ]=Yii::t( "quotesWidget", 'Нет');
		
		parent::init();
	}
}
