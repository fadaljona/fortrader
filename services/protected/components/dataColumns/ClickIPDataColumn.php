<?
	Yii::import( 'dataColumns.base.DataColumnBase' );

	final class ClickIPDataColumn extends DataColumnBase {
		protected function renderDataCellContent( $row, $model ) {
			$viewPath = $this->getViewPath( __FILE__ );
			require "{$viewPath}/view.php";
		}
	}

?>