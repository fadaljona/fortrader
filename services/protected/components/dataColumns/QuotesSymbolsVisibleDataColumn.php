<?
Yii::import( 'dataColumns.base.DataColumnBase' );

final class QuotesSymbolsVisibleDataColumn extends DataColumnBase {
	function init() {
		$this->filter = Array();
		$this->filter[ 'Visible' ]=Yii::t( "quotesWidget",'Показывать');
		$this->filter[ 'Unvisible' ]=Yii::t( "quotesWidget",'Скрыть');
		parent::init();
	}
}
