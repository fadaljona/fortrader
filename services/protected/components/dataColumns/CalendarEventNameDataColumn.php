<?
	Yii::import( 'dataColumns.base.DataColumnBase' );

	final class CalendarEventNameDataColumn extends DataColumnBase {
		protected function renderFilterCellContent() {
			$viewPath = $this->getViewPath( __FILE__ );
			require "{$viewPath}/filter.php";
		}
	}

?>