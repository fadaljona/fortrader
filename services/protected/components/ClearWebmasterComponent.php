<?php

Yii::import( 'components.base.ComponentBase' );
class ClearWebmasterComponent extends ComponentBase {
    
    const DAYS = 30;
    
    public function clear() {
        $criteria = new CDbCriteria();
        $criteria->addCondition('((TO_DAYS(NOW()) - TO_DAYS(clear_date)) >= :d) OR clear_date IS NULL', 'AND');
        $criteria->params = array(':d'=>self::DAYS);
        $models = JournalModel::model()->findAll($criteria);
        
        foreach($models as $model) {
            /**$i18ns = $model->currentLanguageI18N;
            if($i18ns instanceof JournalI18NModel) {
                $i18ns->countDownloads = 0;
                $i18ns->save(false, array('countDownloads'));
            }*/
            $model->clear_date = new CDbExpression('NOW()');
            $model->save(false, array('clear_date'));
            Yii::app()->db->createCommand()
                    ->delete(JournalWebmasterDownloadsModel::model()->tableName(), 'idJournal=:j', array(':j'=>$model->id));
        }
                
    }
}

?>
