<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class ContestStatusesComponent extends ComponentBase {
		function __construct() {
			
		}
		function update() {
			//update ft_contest set status='completed' where end < now()
			$endedContestModels = ContestModel::model()->findAll(Array(
				'condition' => " `t`.`end` < now() and `status`<>'completed' ",
			));
			foreach( $endedContestModels as $model ){
				$model->status = 'completed';
				$model->save();
			}
		
			//update ft_contest set status='started' where endReg < now() and status='registration'
			$startedContestModels = ContestModel::model()->findAll(Array(
				'condition' => " `t`.`endReg` < now() and `t`.`status`='registration' ",
			));
			foreach( $startedContestModels as $model ){
				$model->status = 'started';
				$model->save();
			}
						
			echo 'обновлено конкурсов со статусом заверщено '. count($endedContestModels);
			echo '<br>обновлено конкурсов со статусом проходит '. count( $startedContestModels );
			
		}
	}
			
?>