<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class ContestMemberStatsComponent extends ComponentBase {
		function __construct() {
			
		}
		function getContests() {

			$contests = ContestModel::model()->findAll(Array(
				'condition' => "
					`t`.`status` IN ( 'registration', 'started', 'completed' )
					AND `t`.`stopImport` = 0 
					AND `t`.`stopDeleteMMonitoring` = 0 
					AND `t`.`completedMembers` = 0 
				"
			));
			return $contests;
		}
		
		function updateStats() {
			$contests = $this->getContests();
			foreach( $contests as $contest ) {
				ContestMemberModel::updateStats( $contest->id );
			}
			ContestMemberModel::updateStats( 0 );
			ContestMemberModel::updateMonitoringStats();
		}
	}
			
?>