<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class ClosedTransactionsStatsComponent extends ComponentBase {
		function __construct() {
			
		}
		function updateAll() {
			Yii::app()->db->createCommand()
				->delete('ft_closed_transactions_stats',
						'1=1'
					);
			
			Yii::app()->db->createCommand(
				"INSERT INTO ft_closed_transactions_stats (idContest, orderSymbol, sumOrderLots, orderType)
					SELECT cmember.idContest, mt4his.OrderSymbol, SUM(mt4his.OrderLots), mt4his.OrderType 
					FROM `mt4_account_history` as mt4his 
					LEFT JOIN ft_contest_member as cmember ON cmember.accountNumber = mt4his.AccountNumber and cmember.idServer = mt4his.AccountServerId
					where OrderType = 0 and cmember.idContest > 0
					group by cmember.idContest, mt4his.OrderSymbol"
			)->execute();
			Yii::app()->db->createCommand(
				"INSERT INTO ft_closed_transactions_stats (idContest, orderSymbol, sumOrderLots, orderType)
					SELECT cmember.idContest, mt4his.OrderSymbol, SUM(mt4his.OrderLots), mt4his.OrderType 
					FROM `mt4_account_history` as mt4his 
					LEFT JOIN ft_contest_member as cmember ON cmember.accountNumber = mt4his.AccountNumber and cmember.idServer = mt4his.AccountServerId
					where OrderType = 1 and cmember.idContest > 0
					group by cmember.idContest, mt4his.OrderSymbol"
			)->execute();
		}
		
		function update() {
			
			Yii::app()->db->createCommand(
				"delete from ft_closed_transactions_stats where idContest in ( select id from ft_contest where status <> 'completed' )"
			)->execute();
			
			Yii::app()->db->createCommand(
				"INSERT INTO ft_closed_transactions_stats (idContest, orderSymbol, sumOrderLots, orderType)
					SELECT cmember.idContest, mt4his.OrderSymbol, SUM(mt4his.OrderLots), mt4his.OrderType 
					FROM `mt4_account_history` as mt4his 
					LEFT JOIN ft_contest_member as cmember ON cmember.accountNumber = mt4his.AccountNumber and cmember.idServer = mt4his.AccountServerId
					where OrderType = 0 and cmember.idContest in ( select id from ft_contest where status <> 'completed' )
					group by cmember.idContest, mt4his.OrderSymbol"
			)->execute();
			
			Yii::app()->db->createCommand(
				"INSERT INTO ft_closed_transactions_stats (idContest, orderSymbol, sumOrderLots, orderType)
					SELECT cmember.idContest, mt4his.OrderSymbol, SUM(mt4his.OrderLots), mt4his.OrderType 
					FROM `mt4_account_history` as mt4his 
					LEFT JOIN ft_contest_member as cmember ON cmember.accountNumber = mt4his.AccountNumber and cmember.idServer = mt4his.AccountServerId
					where OrderType = 1 and cmember.idContest in ( select id from ft_contest where status <> 'completed' )
					group by cmember.idContest, mt4his.OrderSymbol"
			)->execute();
			
			
		}
	}
			
?>