<?
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'extensions.PHPMailer' );
	Yii::import( 'extensions.SMTP' );
	
	final class MailerComponent extends ComponentBase {
		public $defaultFromName = "Fortrader.org";
		function instanceMailer() {
			$mailer = new PHPMailer( true );
							
			$from = SettingsModel::getModel()->common_mailForMailing;
			list( $fromName, $fromEmail ) = CommonLib::explodeComplexEmail( $from );
			
			$mailer->addReplyTo( $fromEmail, strlen( $fromName ) ? $fromName : $this->defaultFromName );
			$mailer->setFrom( $fromEmail, strlen( $fromName ) ? $fromName : $this->defaultFromName );
			
			$mailer->CharSet = 'UTF-8';
			
				# SMTP
			$smtp_host = SettingsModel::getModel()->smtp_host;
			$smtp_port = SettingsModel::getModel()->smtp_port;
			$smtp_username = SettingsModel::getModel()->smtp_username;
            $smtp_password = SettingsModel::getModel()->smtp_password;
            

            
			
			if( strlen( $smtp_host )) {
				$mailer->isSMTP();
				$mailer->Host = $smtp_host;
                if( $smtp_port ) $mailer->Port = $smtp_port;
                
                if ($mailer->Port == 465) {
                    $mailer->SMTPSecure = 'ssl';
                }
				
				if( strlen( $smtp_username ) && strlen( $smtp_password )) {
					$smtp_password = ModelBase::decrypt( $smtp_password );
					$mailer->SMTPAuth = true;
					$mailer->Username = $smtp_username;
                    $mailer->Password = $smtp_password;
				}
				$mailer->SMTPOptions = array(
					'ssl' => array(
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
					)
				);
			}
			return $mailer;
		}
	}
	
?>