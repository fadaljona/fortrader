<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class ContestStatsComponent extends ComponentBase {
		function __construct() {
			
		}
		function updateAll() {
			$contestModels = ContestModel::model()->findAll();
			foreach( $contestModels as $model ){
				ContestStatsModel::updateStats( $model->id );
			}
		}
		
		function update() {
			$contestModels = ContestModel::model()->findAll(Array(
				'condition' => " `t`.`status` <> 'completed' ",
			));
			foreach( $contestModels as $model ){
				ContestStatsModel::updateStats( $model->id );
			}
		}
	}
			
?>