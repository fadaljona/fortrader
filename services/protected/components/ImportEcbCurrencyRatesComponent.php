<?
	Yii::import( 'components.base.ComponentBase' );

	final class ImportEcbCurrencyRatesComponent extends ComponentBase {
		public $daysToFullCheck = 30;
		public $dayData = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
		protected $currencyModels = false;
		
		protected function setupDataForLastFullCheckTable(){
			if( CurrencyRatesEcbLastFullCheckModel::getAllCount() ) return false;
			
			$listData = file_get_contents( 'https://www.ecb.europa.eu/stats/exchange/eurofxref/html/index.en.html' );
			if( !$listData ) throw new CException( Yii::t( '*', "Can't load https://www.ecb.europa.eu/stats/exchange/eurofxref/html/index.en.html") );
			
			$listData=str_replace("\n"," ",$listData);
			$listData=str_replace("\r"," ",$listData);

			preg_match_all(
				'/<td id="([^"]+)" class="currency"><a href="([^"]+)"/',
				$listData,
				$matches,
				PREG_PATTERN_ORDER
			);
			
			$urls = array();
			
			foreach( $matches[1] as $key => $val ){
				$urls[$val] = $matches[2][$key];
			}
			
			foreach( $this->currencyModels as $currency ){
				$model = new CurrencyRatesEcbLastFullCheckModel;
				$model->idCurrency = $currency->id;
				$model->fullUrl = 'https://www.ecb.europa.eu/stats/exchange/eurofxref/html/' . $urls[$currency->code];
				$model->save();
				unset( $model );
			}
			CurrencyRatesEcbLastFullCheckModel::model()->updateAll(array('updatedDT'=>'1980-11-16 15:26:07'));
		}
		protected function setCurrencyModels(){
			$models = CurrencyRatesModel::model()->findAll(array(
				'condition' => ' `t`.`ecb` = 1 '
			));
			$this->currencyModels = array();
			foreach( $models as $model ){
				$this->currencyModels[$model->code] = $model;
			}
		}
		protected function importFullData(){
			$models = CurrencyRatesEcbLastFullCheckModel::model()->findAll(array(
				'condition' => " `t`.`updatedDT` < :date ",
				'params' => array( ':date' => date( 'Y-m-d H:i:s', time() - 60*60*24 * $this->daysToFullCheck ) )
			));
			foreach( $models as $model ){
				echo $model->fullUrl . "\n";
				
				$data = file_get_contents($model->fullUrl);
				if( !$data ) throw new CException( Yii::t( '*', "Can't load ") . $model->fullUrl );
				
				preg_match_all(
					'/chartData.push\(\{ date\: new Date\(([^\)]+)\), rate\: ([^\s]+) \}\)/',
					$data,
					$matches,
					PREG_PATTERN_ORDER
				);
				
				if( !$matches ) throw new CException( Yii::t( '*', "No matches") );
				
				$sql = "INSERT IGNORE INTO {{currency_rates_history_ecb}} ( `idCurrency`, `date`, `value`, `createdTime` ) VALUES ";
				$i=0;
				$params = array( ':id' => $model->idCurrency, ':time' => time() );
				
				foreach( $matches[1] as $index => $dateStr ){
					if( !isset( $matches[2][$index] ) || !$matches[2][$index] ) throw new CException( Yii::t( '*', "No value") );
					
					$dateArr = explode(',', $dateStr);
					$mo = $dateArr[1] + 1;
					$moStr = $mo < 10 ? '0'.$mo : $mo;
					$dayStr = $dateArr[2] < 10 ? '0'.$dateArr[2] : $dateArr[2];
					$trueDateStr = $dateArr[0] . '-' . $moStr . '-' . $dayStr;
					
					if( $i ) $sql .= ',';
					$sql .= "(:id, :date$i, :value$i, :time )";
					
					$params[":date$i"] =  $trueDateStr;
					$params[":value$i"] = $matches[2][$index];
					$i++;	

				}
				
				if( count($params) != 1 ){
					$insertResult = Yii::App()->db->createCommand( $sql )->query( $params );
					echo " inserted rows {$insertResult->rowCount} for currency {$model->idCurrency}\n";
				}
				
				$model->updatedDT = date('Y-m-d H:i:s');
				if( !$model->save() ) throw new CException( Yii::t( '*', "Can't save CurrencyRatesEcbLastFullCheckModel") );
			}
		}
		protected function importDayData(){
			$xmlStr = CommonLib::downloadFileFromWww( $this->dayData );
			if( !$xmlStr ) throw new CException( Yii::t( '*', "Can't load ") . $this->dayData );
			$xml = simplexml_load_string( $xmlStr );
			if( !$xml ) throw new CException( Yii::t( '*', "Can't load xml") );
			
			$dayDate = $xml->Cube->Cube->attributes()->time->__toString();
			
			$sql = "INSERT IGNORE INTO {{currency_rates_history_ecb}} ( `idCurrency`, `date`, `value`, `createdTime` ) VALUES ";
			$i=0;
			$params = array( ':date' => $dayDate, ':time' => time() );
			
			foreach($xml->Cube->Cube->Cube as $rate){
				if( $i ) $sql .= ',';
				$sql .= "(:id$i, :date, :value$i, :time )";
					
				$params[":id$i"] =  $this->currencyModels[$rate->attributes()->currency->__toString()]->id;
				$params[":value$i"] = $rate->attributes()->rate->__toString();
				$i++;	
			}
			
			if( count($params) != 1 ){
				$insertResult = Yii::App()->db->createCommand( $sql )->query( $params );
				echo " inserted rows {$insertResult->rowCount}\n";
			}
		}
		
		function import() {
			$this->setCurrencyModels();
			$this->setupDataForLastFullCheckTable();
			$this->importFullData();
			$this->importDayData();
		}
	}
?>