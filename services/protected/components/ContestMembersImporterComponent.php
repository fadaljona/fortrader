<?
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'extensions.POP3Protocol' );
	
	final class ContestMembersImporterComponent extends ComponentBase {
		public $trace = false;
		protected function getProtocol() {
			$host = SettingsModel::getModel()->importMembers_mail_host;
			$user = SettingsModel::getModel()->importMembers_mail_login;
			$password = SettingsModel::getModel()->getCrypt( 'importMembers_mail_pass' );
			$protocol = new POP3Protocol( $host, $user, $password );
			return $protocol;
		}
		function import() {
			$loginForMessage = SettingsModel::getModel()->importMembers_loginForMessage;
			$userForMessage = UserModel::model()->findByAttributes( Array( 'user_login' => $loginForMessage ));
			if( !$userForMessage ) throw new Exception( "Can't find user for messages" );
		
			$protocol = $this->getProtocol();
			
			$statistics = $protocol->getStat();
			for( $i=1; $i<=$statistics->count; $i++ ) {
				$top = $protocol->getTop( $i, 50 );
				$charset = preg_match( "#charset=(.*)#", $top->headers['Content-Type'], $matchs ) ? $matchs[1] : null;
				if( $charset and strtolower( $charset ) != 'utf-8' ) $top->body = iconv( $charset, "utf-8", $top->body );
								
					# parse fields
				$fields = ContestMembersImporterComponentUtils::parseFields( $top->body );
				
					# validate fields
				if( !ContestMembersImporterComponentUtils::validateFields( $fields )) {
					if( $this->trace ) echo "{$i}: !validateFields <br/>\r\n";
					continue;
				}
				
					# search exists member
				if( ContestMemberModel::model()->findByAttributes( Array( 'idContest' => $fields['contestsid'], 'accountNumber' => $fields['mtlogin']))) {
					if( $this->trace ) echo "{$i}: ContestMemberModel::findByAttributes {$fields['contestsid']} {$fields['mtlogin']}<br/>\r\n";
					continue;
				}
				
					# search contest
				$contest = ContestModel::model()->find( Array(
					'with' => Array( 'servers', 'currentLanguageI18N' ),
					'condition' => " `t`.`id` = :id ",
					'params' => Array(
						':id' => $fields['contestsid'],
					),
				));
				if( !$contest ) {
					if( $this->trace ) echo "{$i}: ContestModel::!find {$fields['contestsid']} <br/>\r\n";
					continue;
				}
				
					# search user
				if( !$user = UserModel::model()->findByAttributes( Array( 'user_email' => $fields['user-email']))) {
					if( $this->trace ) echo "{$i}: UserModel::!findByAttributes {$fields['user-email']} <br/>\r\n";
					continue;
				}
				
					# search server
				$server = isset( $fields['server-id'] ) ? ServerModel::model()->findByPk( $fields['server-id'] ) : null;

					# create member
				$servers = $contest->servers;
				$firstServer = @array_shift( $servers );
				$member = ContestMemberModel::instance( $contest->id, $user->id );
				$member->accountNumber = $fields['mtlogin'];
				$member->investorPassword = $fields['mtinvpass'];
				$member->idServer = $server ? $server->id : (int)@$firstServer->id;
				$member->save();
				
					# create notice
				UserNoticeModel::notice( UserNoticeModel::TYPE_NOTICE_CREATE_CONTEST_MEMBER, Array(
					'idUser' => $user->id,
					'idMember' => $member->id,
				));
				
					# create private message
				$messageText = ContestMembersImporterComponentUtils::renderMessage( $contest, $fields );
				$message = UserPrivateMessageModel::instance( $user->id, $userForMessage->id, $messageText );
				$message->save();
			}
		}
	}
	
	abstract class ContestMembersImporterComponentUtils{
		static function validateFields( &$fields ) {
			$needFields = explode( ",", "contestsid, user-email, mtlogin, mtpass, mtinvpass" );
			foreach( $needFields as $field ) {
				if( !array_key_exists( trim( $field ), $fields )) return false;
			}
			return true;
		}
		static function parseFields( $text ) {
			$fields = Array();
			$key = "";
			foreach( explode( "\r\n", $text ) as $line ) {
				list($key, $field) = array_pad( explode( ":", $line, 2 ), 2, '' );
				if( $field ) {
					$fields[ $key ] = trim( $field, "; " );
				}
			}
			return $fields;
		}
		static function renderNotice() {
			$notice = "";
			return $notice;
		}
		static function renderMessage( $contest, &$fields ) {
			$message = "
				Для вас открыт торговый счет для участия в конкурсе {$contest->currentLanguageI18N->name}. 

				login: {$fields['mtlogin']}
				Password: {$fields['mtpass']}
				Investor Password: {$fields['mtinvpass']}

				".@$fields['brokermessage']."
			";
			$message = preg_replace( "#^\s+|\s+$#m", '', $message );
			return $message;
		}
	}
	
?>