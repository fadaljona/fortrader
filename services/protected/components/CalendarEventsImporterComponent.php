<?
	Yii::import( 'components.base.ComponentBase' );
	
	final class CalendarEventsImporterComponent extends ComponentBase {
		public $daysRadiusLeft = 30;
		public $daysRadiusRight = 30;
		const APIURL = "http://infos.fxteam.ru/3.1/apiecalend/events2.php?dt={date}&key={key}&lang={lang}";
		const pathLog = 'log/importCalendarEvents';
		public $mode = 'Default';
		private function getCronLogs() {
			$models = CalendarEventCronModel::model()->findAll(Array(
				'condition' => " DATEDIFF( `date`, CURDATE() ) BETWEEN :left AND :right ",
				'order' => " `date` ",
				'params' => Array(
					':left' => -$this->daysRadiusLeft,
					':right' => $this->daysRadiusRight,
				),
			));
			$models = CommonLib::toAssoc( $models, 'date' );
			return $models;
		}
		private static function sortCronLogs( $a, $b ) {
			if( $a->updatedDT == $b->updatedDT ) return 0;
			return $a->updatedDT > $b->updatedDT ? 1 : -1;
		}
		private static function saveCronLog( $date ) {
			$model = CalendarEventCronModel::model()->findByPk( $date );
			if( !$model ) $model = CalendarEventCronModel::instance( $date );
			$model->save();
		}
		private function getWorkDate() {
			if( $this->mode == 'Today' ) {
				return date( "Y-m-d" );
			}
			
			$cronLogs = $this->getCronLogs();
			
			for( $i = 0; $i <= $this->daysRadiusLeft + $this->daysRadiusRight; $i++ ) {
				$daysDiff = $i - $this->daysRadiusLeft;
				$time = time() + $daysDiff * 86400;
				$date = date( "Y-m-d", $time );
				if( empty( $cronLogs[ $date ])) return $date;
			}
			
			uasort( $cronLogs, Array( 'CalendarEventsImporterComponent', 'sortCronLogs' ));
			$keys = array_keys( $cronLogs );
			if( $keys ) return $keys[0];
			exit;
		}
		private static function getAPIContent( $date, $lang ) {
			if( $lang == 'en_us' ) {
				 $lang = 'en';
			}
			$url = strtr( self::APIURL, Array(
				'{date}' => $date,
				'{key}' => Yii::App()->params[ 'keyAPI_fxteam.ru' ],
				'{lang}' => $lang,
			));
			$content = file_get_contents( $url );
			self::logAPIContent( $date, $lang, $content );
			return $content;
		}
		private static function logAPIContent( $date, $lang, $content ) {
			$year = substr( $date, 0, strpos( $date, '-' ));
			$dir = self::pathLog."/{$lang}/{$year}";
			if( !is_dir( $dir )) mkdir( $dir );
			$file = "{$dir}/{$date}.xml";
			file_put_contents( $file, $content );
		}
		private static function getEventsFromAPI( $date, $lang ) {
			$content = self::getAPIContent( $date, $lang );
			//$content = file_get_contents( "log/importCalendarEvents/2014-03-09.xml" );
			
			$XML = new DOMDocument( "1.0", "UTF-8" );
			$XML->loadXML( $content );
			$XPath = new DOMXPath( $XML );
			
			$events = Array();
			
			$nodes = $XPath->query( "//event" );
			foreach( $nodes as $nodeEvent ) {
				$event = Array();
				foreach( $nodeEvent->childNodes as $node ) {
					if( $node->nodeType == XML_ELEMENT_NODE ) {
						$event[ $node->nodeName ] = $node->textContent !== "" ? $node->textContent : null;
					}
				}
				if( @$event['id'] and @$event['dt'] ) $events[] = $event;
			}
			return $events;
		}
		private static function updateEvents( $date, $lang ) {
			$fieldsEvent = ModelBase::getModelColumns( 'CalendarEventModel' );
			$fieldsEventI18N = ModelBase::getModelColumns( 'CalendarEventI18NModel' );
			
			$events = self::getEventsFromAPI( $date, $lang->alias );
						
			foreach( $events as $event ) {
				$model = CalendarEventModel::model()->findByPk( $event[ 'id' ]);
				if( !$model ) {
					$model = new CalendarEventModel();
				}
				foreach( $event as $key => $value ) {
					if( in_array( $key, $fieldsEvent )) {
						$model->$key = $value;
					}
				}
				
				$model->save();
				$modelI18N = CalendarEventI18NModel::model()->findByAttributes(Array(
					'idEvent' => $model->id,
					'idLanguage' => $lang->id,
				));
				if( !$modelI18N ) {
					$modelI18N = CalendarEventI18NModel::instance( $model->id, $lang->id, $event['indicator'] );
				}
				foreach( $event as $key => $value ) {
					if( in_array( $key, $fieldsEventI18N )) {
						$modelI18N->$key = $value;
					}
				}
				
				$modelI18N->indicator = strip_tags( $modelI18N->indicator );
				$modelI18N->save();
			}
			self::saveCronLog( $date );
		}

		function import() {
			$date = $this->getWorkDate();
			$langs = LanguageModel::getAll();
			foreach( $langs as $lang ) {
				self::updateEvents( $date, $lang );
			}
		}
	}
	
?>
