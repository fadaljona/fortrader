<?php

class ManageTextContentBehavior extends CActiveRecordBehavior{
	
	public $filterCondition = ' 1=1 ';
	
	public function getManageTextContentDp(){
		
		$c = new CDbCriteria();
			
		$c->addCondition( $this->filterCondition);

		$lang = Yii::app()->request->getParam('lang');
		$field = Yii::app()->request->getParam('field');
		$type = Yii::app()->request->getParam('type');
		
		
		$settings = ManageTextContentLib::getItemSettings();
		if( !$settings ) throw new Exception( "Can't find itemSettings" );
		if( !isset($settings['model']) || !$settings['model'] || !isset($settings['modelForm']) || !$settings['modelForm'] ) throw new Exception( "Can't find itemSettings" );
			
		$modelForm = $settings['modelForm'];
			
		$formModel = new $modelForm;
			
		if( $field && ($lang == 0 || $lang == 1) && ($type == 'no' || $type == 'yes') && array_key_exists( $field, $formModel->textFields ) ){

			if( $type == 'no' ){

				if( $lang == 1 ){
					$c->with = array('ruLanguageI18NManageTextContent');
					$c->addCondition('  `ruLanguageI18NManageTextContent`.`'.$field.'` IS NULL OR `ruLanguageI18NManageTextContent`.`'.$field.'` = "" ');
				}else{
					$c->with = array('enLanguageI18NManageTextContent');
					$c->addCondition('  `enLanguageI18NManageTextContent`.`'.$field.'` IS NULL OR `enLanguageI18NManageTextContent`.`'.$field.'` = "" ');
				}
					
			}else{
					
				if( $lang == 1 ){
					$c->with = array('ruLanguageI18NManageTextContent');
					$c->addCondition('  `ruLanguageI18NManageTextContent`.`'.$field.'` IS NOT NULL AND `ruLanguageI18NManageTextContent`.`'.$field.'` <> "" ');
				}else{
					$c->with = array('enLanguageI18NManageTextContent');
					$c->addCondition('  `enLanguageI18NManageTextContent`.`'.$field.'` IS NOT NULL AND `enLanguageI18NManageTextContent`.`'.$field.'` <> "" ');
				}
					
			}
			
		}
			
		return new CActiveDataProvider( get_class($this->owner), Array(
			'criteria' => $c,
		));
	}
	
}