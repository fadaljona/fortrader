<?
	Yii::import( 'components.base.ComponentBase' );
	
	final class JournalApiComponent extends ComponentBase {
		public $formatRender;
		public $limitPage;
		public $page;
		public $languages;
		public $idsLanguages;
		public $dumpResult;
		const modelName = 'JournalModel';
		const limitPageDef = 1000;
		const version = "1.0";
		private function getParam( $key, $def = null, $data = null ) {
			if( @$this->$key ) return @$this->$key;
			if( $data === null ) $data = $_GET;
			if( @$data[ $key ]) return @$data[ $key ];
			return $def; 
		}
		function getFormatRender() {
			return $this->getParam( 'formatRender', 'json' );
		}
		function getLimitPage() {
			$limitPage = $this->getParam( 'limitPage', self::limitPageDef );
			$limitPage = (int)$limitPage;
			$limitPage = CommonLib::minimax( $limitPage, 1, self::limitPageDef );
			return $limitPage;
		}
		function getPage() {
			$page = $this->getParam( 'page', 0 );
			$page = (int)$page;
			$page = max( $page, 0 );
			return $page;
		}
		function getLanguages() {
			$languages = $this->getParam( 'languages' );
			return $languages;
		}
		function getIDsLanguages() {
			if( $this->idsLanguages === null ) {
				$this->idsLanguages = Array();
				$languages = $this->getLanguages();
				if( $languages ) {
					$languages = explode( ",", $languages );
					$languages = array_map( 'trim', $languages );
					
					$modelsLanguages = LanguageModel::getAll();
					foreach( $modelsLanguages as $modelLanguages ) {
						if( in_array( $modelLanguages->alias, $languages )) {
							$this->idsLanguages[] = $modelLanguages->id;
						}
					}
				}
			}
			
			return $this->idsLanguages;
		}
		function getDumpResult() {
			return $this->getParam( 'dumpResult' );
		}
		private function getDPListCriteria() {
			$self = $this;
			$getI18NCriteria = function() use( $self ) {
				$c = new CDbCriteria();
				$c->together = false;
				
				$idsLanguages = $self->getIDsLanguages();
				if( $idsLanguages ) 
					$c->addInCondition( "`i18nsJournal`.`idLanguage`", $idsLanguages );	
					
				return $c;
			};
			
			$c = new CDbCriteria();
			$c->with[ 'i18ns' ] = $getI18NCriteria()->toArray();
			
			$c->order = " `t`.`date` ";
			
			$idsLanguages = $this->getIDsLanguages();
			if( $idsLanguages ) {
				$c->addInCondition( "`i18nsJournal`.`idLanguage`", $idsLanguages );
			}
			
			return $c;
		}
		private function getDPList() {
			$limitPage = $this->getLimitPage();
			$page = $this->getPage();
			
			$DP = new CActiveDataProvider( self::modelName, Array(
				'criteria' => $this->getDPListCriteria(),
			));
			
			$DP->pagination->setPageSize( $limitPage );
			$DP->pagination->setCurrentPage( $page );
			return $DP;
		}
		private function echoETag() {
			$url = Yii::App()->getRequest()->getUrl();
			$updated = JournalModel::getMaxUpdatedDT();
			$etag = md5( "{$url}{$updated}".self::version );
			header( "ETag: {$etag}" );
		}
		function exportList() {
			$this->echoETag();
			$output = JournalApiOutput::instance( 'list', $this );
			
			$DP = $this->getDPList();
			$data = $DP->getData();
			
			$output->appendAttribute( 'page', $this->getPage() );
			$output->appendAttribute( 'limitPage', $DP->pagination->getPageSize() );
			$output->appendAttribute( 'countPages', $DP->pagination->getPageCount() );
			
			if( $this->getPage() == $DP->pagination->getCurrentPage()) {
				foreach( $data as $journal ) {
					$output->addJournal( $journal );
				}
			}
			
			$output->render();
		}
	}
	
	
	abstract class JournalApiOutput {
		protected $api;
		static function instanceList( $api ) {
			switch( $api->getFormatRender() ) {
				case 'xml':{
					return new JournalApiOutputListXML( $api );
					break;
				}
				case 'json':
				default: {
					return new JournalApiOutputListJson( $api );
					break;
				}
			}
		}
		static function instance( $type, $api ) {
			switch( $type ) {
				case 'list':{
					return self::instanceList( $api );
					break;
				}
			}
		}
		function __construct( $api ) {
			$this->api = $api;
		}
		abstract function appendAttribute( $key, $value );
		abstract function render();
	}
	
	abstract class JournalApiOutputList extends JournalApiOutput{
		function addJournal( $journal ) {
			$languages = LanguageModel::getAll();
			$languages = CommonLib::toAssoc( $languages, 'id', 'alias' );
			
			$newJournal = $this->createJournal();
			
			foreach( Array( 'id', 'number', 'date' ) as $key ) {
				$this->appendJournalAttribute( $newJournal, $key, $journal->$key );
			}
			
			$this->appendJournalI18Ns( $newJournal );
			
			foreach( $journal->i18ns as $i18n ) {
				$alias = $languages[ $i18n->idLanguage ];
				
				$this->appendJournalI18NAlias( $newJournal, $alias );
				$this->appendJournalI18NAttribute( $newJournal, $alias, 'wideID', $i18n->getWideID() );
				$this->appendJournalI18NAttribute( $newJournal, $alias, 'srcCover', $i18n->getSrcCover( true ));
				foreach( Array( 'name', 'digest', 'countPages', 'countDownloads' ) as $key ) {
					$this->appendJournalI18NAttribute( $newJournal, $alias, $key, $i18n->$key );
				}
				$this->appendJournalI18NAttribute( $newJournal, $alias, 'srcJournal', $i18n->getSrcJournal( true ));
				//$this->appendJournalI18NAttribute( $newJournal, $alias, 'free', CommonLib::boolToStr( $i18n->free ));
				$this->appendJournalI18NAttribute( $newJournal, $alias, 'price', $i18n->free ? null : $i18n->price );
				$this->appendJournalI18NAttribute( $newJournal, $alias, 'description', trim( $i18n->description ));
			}
			
			$this->appendJournal( $newJournal );
		}
		abstract protected function createJournal();
		abstract protected function appendJournal( $journal );
		abstract protected function appendJournalAttribute( $journal, $key, $value );
		abstract protected function appendJournalI18Ns( $journal );
		abstract protected function appendJournalI18NAlias( $journal, $alias );
		abstract protected function appendJournalI18NAttribute( $journal, $alias, $key, $value );
	}
	
	final class JournalApiOutputListJson extends JournalApiOutputList{
		private $object;
		function __construct( $api ) {
			parent::__construct( $api );
			$this->object = new StdClass();
		}
		function appendAttribute( $key, $value ) {
			$this->object->$key = $value;
		}
		protected function createJournal() {
			return new StdClass();
		}
		protected function appendJournal( $journal ) {
			$this->object->journals[] = $journal;
		}
		protected function appendJournalAttribute( $journal, $key, $value ) {
			$journal->$key = $value;
		}
		protected function appendJournalI18Ns( $journal ) {
			if( !@$journal->i18ns ) $journal->i18ns = Array();
		}
		protected function appendJournalI18NAlias( $journal, $alias ) {
			$journal->i18ns[ $alias ] = new StdClass();
		}
		protected function appendJournalI18NAttribute( $journal, $alias, $key, $value ) {
			$journal->i18ns[ $alias ]->$key = $value;
		}
		function render() {
			if( $this->api->getDumpResult()) {
				var_dump( $this->object );
			}
			else{
				header( "Content-Type: application/json" );
				echo json_encode( $this->object );
			}
		}
	}
	
	final class JournalApiOutputListXML extends JournalApiOutputList{
		private $XML;
		private $nodeRoot;
		private $nodeJournals;
		function __construct( $api ) {
			parent::__construct( $api );
			$this->XML = new DOMDocument( "1.0", "UTF-8" );
			
			$this->nodeRoot = $this->XML->appendChild( $this->createElement( 'listJournals' ));
		}
		protected function trValue( $value ) {
			return strtr( $value, Array(
				'&' => '&amp;',
			));
		}
		protected function createElement( $name, $value = null ) {
			if( $value !== null ) {
				$value = $this->trValue( $value );
				return $this->XML->createElement( $name, $value );
			}
			return $this->XML->createElement( $name );
		}
		function appendAttribute( $key, $value ) {
			$this->nodeRoot->appendChild( $this->createElement( $key, $value ) );
		}
		protected function createJournal() {
			$journal = $this->createElement( 'journal' );
			return $journal;
		}
		protected function appendJournal( $journal ) {
			if( !$this->nodeJournals ) {
				$this->nodeJournals = $this->nodeRoot->appendChild( $this->createElement( 'journals' ));
			}
			$this->nodeJournals->appendChild( $journal );
		}
		protected function appendJournalAttribute( $journal, $key, $value ) {
			$journal->appendChild( $this->createElement( $key, $value ));
		}
		protected function appendJournalI18Ns( $journal ) {
			$journal->appendChild( $this->createElement( 'i18ns' ));
		}
		protected function appendJournalI18NAlias( $journal, $alias ) {
			$i18nss = $journal->getElementsByTagName( 'i18ns' );
			$i18ns = $i18nss->item(0);
			
			$nodeAlias = $i18ns->appendChild( $this->createElement( $alias ));		
		}
		protected function appendJournalI18NAttribute( $journal, $alias, $key, $value ) {
			$i18nss = $journal->getElementsByTagName( 'i18ns' );
			$i18ns = $i18nss->item(0);
			
			$nodesAlias = $i18ns->getElementsByTagName( $alias );
			$nodeAlias = $nodesAlias->item(0);
			
			$nodeAlias->appendChild( $this->createElement( $key, $value ) );
		}
		function render() {
			if( $this->api->getDumpResult()) {
				$this->XML->formatOutput = true;
			}
			header( "Content-Type: application/xml" );
			echo $this->XML->saveXML();
		}
	}
?>