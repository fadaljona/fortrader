<?
	Yii::import( 'gridColumns.base.GridColumnBase' );

	final class ContestMembersActionsGridColumn extends GridColumnBase {
		protected function renderDataCellContent( $row, $model ) {
			$viewPath = $this->getViewPath( __FILE__ );
			require "{$viewPath}/view.php";
		}
	}

?>