<?
	Yii::import( 'gridColumns.base.GridColumnBase' );

	final class AdminInterestRatesBankColorGridColumn extends GridColumnBase {
		public $styleExpression;
		public function renderDataCell($row){
			$data=$this->grid->dataProvider->data[$row];
			$options=$this->htmlOptions;
			if($this->styleExpression!==null)
			{
				$style=$this->evaluateExpression($this->styleExpression,array('row'=>$row,'data'=>$data));
				if(!empty($style))
				{
					$options['style']="background-color:$style";
				}
			}
			echo CHtml::openTag('td',$options);
			$this->renderDataCellContent($row,$data);
			echo '</td>';
		}
	}

?>