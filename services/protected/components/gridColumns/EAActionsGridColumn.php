<?
	Yii::import( 'gridColumns.base.GridColumnBase' );

	final class EAActionsGridColumn extends GridColumnBase {
		protected function renderDataCellContent( $row, $model ) {
			$viewPath = $this->getViewPath( __FILE__ );
			require "{$viewPath}/view.php";
		}
	}

?>