<?php

final class CryptoCurrenciesExchangesColumn extends CDataColumn
{
    public function renderHeaderCell()
    {
        $this->headerHtmlOptions['id']=$this->id;
        echo CHtml::openTag('td', $this->headerHtmlOptions);
        $this->renderHeaderCellContent();
        echo "</td>";
    }
}
