<?
	Yii::import( 'gridColumns.base.GridColumnBase' );

	final class UserGroupRightsGridColumn extends GridColumnBase {
		protected function renderDataCellContent( $row, $model ) {
			$rights = $model->rights;
			
			$viewPath = $this->getViewPath( __FILE__ );
			require "{$viewPath}/view.php";
		}
	}

?>