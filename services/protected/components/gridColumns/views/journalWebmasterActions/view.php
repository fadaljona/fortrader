<?
	$NSi18n = $this->getNSi18n();
?>
<div class="inline position-relative nowrapTT">
	<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
		<i class="icon-cog icon-only bigger-110"></i>
	</button>
	<ul class="dropdown-menu dropdown-icon-only dropdown-light pull-right dropdown-caret dropdown-close">
		<li>
			<?$url = $this->grid->controller->createUrl( '/admin/journal/webmasterList', Array( 'idEdit' => $model->idWebmaster ))?>
			<a href="<?=$url?>" class="tooltip-success" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Edit' )?>">
				<span class="green">
					<i class="icon-edit"></i>
				</span>
			</a>
		</li>
		<li>
			<a href="#" class="tooltip-error wDelete" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Delete' )?>">
				<span class="red">
					<i class="icon-trash"></i>
				</span>
			</a>
		</li>
        <li>
			<a href="#" class="tooltip-error wBlock" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Block' )?>">
				<span class="red">
					<i class="icon-minus-sign"></i>
				</span>
			</a>
		</li>
	</ul>
</div>