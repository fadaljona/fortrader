<?
	$NSi18n = $this->getNSi18n();
	
	$contestMemberControl = Yii::App()->user->checkAccess( 'contestMemberControl' );
?>
<div class="inline position-relative nowrapTT">
	<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
		<i class="icon-cog icon-only bigger-110"></i>
	</button>
	<ul class="dropdown-menu dropdown-icon-only dropdown-light pull-right dropdown-caret dropdown-close">
		<li>
			<?$url = $contestMemberControl ? $this->grid->controller->createUrl( '/admin/contestMember/list', Array( 'idContest' => $model->idContest, 'idEdit' => $model->id )) : '#'?>
			<a href="<?=$url?>" class="tooltip-success wEdit" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Edit' )?>">
				<span class="green">
					<i class="icon-edit"></i>
				</span>
			</a>
		</li>
		<li>
			<a href="#" class="tooltip-error wDelete" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Delete' )?>">
				<span class="red">
					<i class="icon-trash"></i>
				</span>
			</a>
		</li>
	</ul>
</div>