<?
	$NSi18n = $this->getNSi18n();
?>
<div class="inline position-relative nowrapTT">
	<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
		<i class="icon-cog icon-only bigger-110"></i>
	</button>
	<ul class="dropdown-menu dropdown-icon-only dropdown-light pull-right dropdown-caret dropdown-close">
		<li>
			<?$url = $this->grid->controller->createUrl( '/admin/contest/list', Array( 'idEdit' => $model->id ))?>
			<a href="<?=$url?>" class="tooltip-success" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Edit' )?>">
				<span class="green">
					<i class="icon-edit"></i>
				</span>
			</a>
		</li>
		<?if( $model->status == 'registration' ){?>
			<li>
				<a href="#" class="wChangeStatusRegistrationStoped" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Stop Registration' )?>">
					<i class="icon-flag"></i>
				</a>
			</li>
		<?}?>
		<?if( $model->status == 'registration stoped' ){?>
			<li>
				<a href="#" class="wChangeStatusRegistration" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Continue registration' )?>">
					<i class="icon-flag"></i>
				</a>
			</li>
		<?}?>
		<?if( in_array( $model->status, Array( 'registration', 'registration stoped' ))){?>
			<li>
				<a href="#" class="wChangeStatusStarted" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Start' )?>">
					<i class="icon-flag"></i>
				</a>
			</li>
		<?}?>
		<?if( $model->status == 'started' ){?>
			<li>
				<a href="#" class="wChangeStatusCompleted" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Complete' )?>">
					<i class="icon-flag"></i>
				</a>
			</li>
		<?}?>
		<li>
			<a href="#" class="tooltip-error wDelete" data-rel="tooltip" title="" data-placement="left" data-original-title="<?=Yii::t( $NSi18n, 'Delete' )?>">
				<span class="red">
					<i class="icon-trash"></i>
				</span>
			</a>
		</li>
	</ul>
</div>