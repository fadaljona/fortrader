<?

	class ACECheckBoxGridColumn extends CCheckBoxColumn {
		function init() {
			if( !$this->name ) $this->name = 'id';
			if( !$this->checkBoxHtmlOptions ) $this->checkBoxHtmlOptions = Array( 'name' => $this->name );
			parent::init();
		}
		protected function renderHeaderCellContent() {
			parent::renderHeaderCellContent();
			if( trim( $this->headerTemplate ) !== '' ) {
				echo '<span class="lbl"></span>';
			}
		}
		protected function renderDataCellContent( $row, $data ) {
			parent::renderDataCellContent( $row, $data );
			echo '<span class="lbl"></span>';
		}
	}

?>