<?
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'components.MailerComponent' );
		
	final class CalendarNotificationsComponent extends ComponentBase {
        private $searchTime = 600;

        private static function getMailer() {
			$factoryMailer = new MailerComponent();
			$mailer = $factoryMailer->instanceMailer();
			return $mailer;
		}
		private static function getMailTpl( $key ) {
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			if( !$mailTpl ) self::throwI18NException( "Can't load mail template! ({key})", Array( '{key}' => $key ));
			return $mailTpl;
		}
		private static function getMailTplI18N( $mailTpl ) {
			$i18n = $mailTpl->currentLanguageI18N;
			if( $i18n and strlen( $i18n->message )) return $i18n;
			foreach( $mailTpl->i18ns as $i18n ) if( strlen( $i18n->message )) return $i18n;
			self::throwI18NException( "Can't load i18n for mail template!" );
		}
        private function formatResult( $results ){
            $dataArr = array();
            
            foreach( $results as $resultsArr ){
                foreach( $resultsArr as $result ){
                    if( !isset( $dataArr[$result['idLanguage']] ) ){
                        $dataArr[$result['idLanguage']] = array(
                            $result['user_email'] => array(
                                $result['eventId'] => array(
                                    'indicator_name' => $result['indicator_name'],
                                    'slug' => $result['slug'],
                                    'dt' => $result['dt'],
                                )
                            )
                        );
                    }elseif( isset( $dataArr[$result['idLanguage']] ) && !isset( $dataArr[$result['idLanguage']][$result['user_email']] ) ){
                        $dataArr[$result['idLanguage']][$result['user_email']] = array(
                            $result['eventId'] => array(
                                'indicator_name' => $result['indicator_name'],
                                'slug' => $result['slug'],
                                'dt' => $result['dt'],
                            )
                        );     
                    }else{
                        $dataArr[$result['idLanguage']][$result['user_email']][$result['eventId']] = array(
                            'indicator_name' => $result['indicator_name'],
                            'slug' => $result['slug'],
                            'dt' => $result['dt'],
                        );
                    }
                }
            }
            return $dataArr;
        }
        private function getSingleSubscriptionsNotificationsSql(){
            $sql = "SELECT `eventi18n`.`indicator_name`, `event`.`slug`, `value`.`dt`, `user`.`user_email`, `subscription`.`idLanguage`, `event`.`id` `eventId` FROM `{{calendar_single_subscription}}` `subscription` 
            LEFT JOIN `{{calendar_event2}}` `event` ON `event`.`id` = `subscription`.`idEvent`
            LEFT JOIN `{{calendar_event2_i18n}}` `eventi18n` ON `eventi18n`.`indicator_id` = `event`.`indicator_id`
            LEFT JOIN `{{calendar_event2_value}}` `value` ON `event`.`indicator_id` = `value`.`indicator_id`
            LEFT JOIN `wp_users` `user` ON `user`.`ID` = `subscription`.`idUser`
            WHERE 
                `eventi18n`.`idLanguage` = `subscription`.`idLanguage` 
                AND UNIX_TIMESTAMP(`value`.`dt`) > ( UNIX_TIMESTAMP() + `subscription`.`timeBeforeEvent` ) 
                AND UNIX_TIMESTAMP(`value`.`dt`) < ( UNIX_TIMESTAMP() + `subscription`.`timeBeforeEvent` + :searchTime ) 
                AND `eventi18n`.`indicator_name` <> '' 
                AND `eventi18n`.`indicator_name` IS NOT NULL 
            ORDER BY `subscription`.`idLanguage` ASC, `user`.`ID` ASC, `value`.`dt` ASC";

            return $sql;
        }
        private function getGroupSubscriptionsNotificationsSql(){
            $sql = "SELECT `eventi18n`.`indicator_name`, `event`.`slug`, `value`.`dt`, `user`.`user_email`, `subscription`.`idLanguage`, `event`.`id` `eventId` FROM `{{calendar_group_subscription}}` `subscription` 
            LEFT JOIN `{{calendar_group_subscription_user_to_country}}` `sCountry` ON `sCountry`.`idUser` = `subscription`.`idUser`
            LEFT JOIN `{{calendar_group_subscription_user_to_serious}}` `sSerious` ON `sSerious`.`idUser` = `subscription`.`idUser`
            LEFT JOIN `{{calendar_event2_value}}` `value` ON `value`.`serious` = `sSerious`.`serious`
            LEFT JOIN `{{calendar_event2}}` `event` ON `event`.`indicator_id` = `value`.`indicator_id`
            LEFT JOIN `{{calendar_event2_i18n}}` `eventi18n` ON `eventi18n`.`indicator_id` = `event`.`indicator_id`
            LEFT JOIN `wp_users` `user` ON `user`.`ID` = `subscription`.`idUser`
            WHERE 
                ( `sCountry`.`countryCode` IS NULL OR `sCountry`.`countryCode` = `event`.`countrycode` )
                AND `eventi18n`.`idLanguage` = `subscription`.`idLanguage` 
                AND UNIX_TIMESTAMP(`value`.`dt`) > ( UNIX_TIMESTAMP() + `subscription`.`timeBeforeEvent` ) 
                AND UNIX_TIMESTAMP(`value`.`dt`) < ( UNIX_TIMESTAMP() + `subscription`.`timeBeforeEvent` + :searchTime ) 
                AND `eventi18n`.`indicator_name` <> '' 
                AND `eventi18n`.`indicator_name` IS NOT NULL 
            ORDER BY `subscription`.`idLanguage` ASC, `user`.`ID` ASC, `value`.`dt` ASC";

            return $sql;
        }
        private function sendNotifications( $sql ){

            $sql1 = $this->getSingleSubscriptionsNotificationsSql();
            $results1 =  Yii::App()->db->createCommand( $sql1 )->queryAll(true, array( ':searchTime' => $this->searchTime ) );

            $sql2 = $this->getGroupSubscriptionsNotificationsSql();
            $results2 =  Yii::App()->db->createCommand( $sql2 )->queryAll(true, array( ':searchTime' => $this->searchTime ) );
           

            if( !$results1 && !$results2 ) return false;

            $dataArr = $this->formatResult( array($results1, $results2) );

            foreach( $dataArr as $idLang => $langData ){
                $langAlias = LanguageModel::getAliasById( $idLang );
                if( !$langAlias ) continue;
                Yii::app()->language = $langAlias;

                $mailTpl = self::getMailTpl( 'calendarNotification' );
                $i18n = self::getMailTplI18N( $mailTpl );

                foreach( $langData as $email => $data ){
                    $mailer = self::getMailer();
                    $mailer->Subject = $i18n->subject;
                    $message = $i18n->message;
                    $eventList = "";
                    foreach( $data as $eventData ){
                        $eventList .= "\n<a href='".Yii::app()->createAbsoluteUrl('calendarEvent/single', Array( 'slug' => $eventData['slug'] ))."'>{$eventData['indicator_name']}</a>\t " . Yii::t('*', 'beforeEventTimeLeft') . " " . CalendarEvent2ValueModel::getStaticTimeLeft( $eventData['dt'] );
                        
                    }
                    $mailer->AddAddress( $email );
                    $mailer->MsgHTML( CommonLib::nl2br( str_replace('%eventList%', $eventList, $message), true ) );
                    $result = $mailer->Send();
                    $error = $result ? null : $mailer->ErrorInfo;
                    if( $error ) throw new Exception( $error );
                }
            }
        }
        
		public function run() {
            $this->sendNotifications( );
		}
	}
			
?>