<?php

final class FileHelper extends CFileHelper{
	
	public static function copyDirectory($src,$dst,$options=array())
	{
		$fileTypes=array();
		$exclude=array();
		$level=-1;
		extract($options);
		if(!is_dir($dst))
			self::createDirectory($dst,isset($options['newDirMode'])?$options['newDirMode']:null,true);

		self::copyDirectoryRecursive($src,$dst,'',$fileTypes,$exclude,$level,$options);
	}
	public static function minifyCssJs($path){
		$fileExtension = strtolower(substr($path, strrpos($path, '.')+1));
		if( $fileExtension == 'css' ){
			$minifier = new MatthiasMullie\Minify\CSS($path);
			$minifier->minify($path);
		}elseif( $fileExtension == 'js' ){
			$minifier = new MatthiasMullie\Minify\JS($path);
			$minifier->minify($path);
		}
		unset($minifier);
	}
	protected static function copyDirectoryRecursive($src,$dst,$base,$fileTypes,$exclude,$level,$options)
	{
		if(!is_dir($dst))
			self::createDirectory($dst,isset($options['newDirMode'])?$options['newDirMode']:null,false);

		$folder=opendir($src);
		if($folder===false)
			throw new Exception('Unable to open directory: ' . $src);
		while(($file=readdir($folder))!==false)
		{
			if($file==='.' || $file==='..')
				continue;
			$path=$src.DIRECTORY_SEPARATOR.$file;
			$isFile=is_file($path);
			if(self::validatePath($base,$file,$isFile,$fileTypes,$exclude))
			{
				if($isFile)
				{
					copy($path,$dst.DIRECTORY_SEPARATOR.$file);
					if(isset($options['newFileMode']))
						@chmod($dst.DIRECTORY_SEPARATOR.$file,$options['newFileMode']);
					
					self::minifyCssJs($dst.DIRECTORY_SEPARATOR.$file);
				}
				elseif($level)
					self::copyDirectoryRecursive($path,$dst.DIRECTORY_SEPARATOR.$file,$base.'/'.$file,$fileTypes,$exclude,$level-1,$options);
			}
		}
		closedir($folder);
	}
	
}