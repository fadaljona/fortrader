<?php

final class ClientScript extends CClientScript
{
    const CACHE_KEY_PREFIX = 'ClientScript.';
    const POS_TOP_HEAD = 5;
    const POS_BOTTOM_HEAD = 6;

    public $cachingDuration = 86400;
    public $cacheID = 'cache';

    public function render(&$output)
    {
        if (!$this->hasScripts) {
            return;
        }
            
        $this->renderCoreScripts();

        if (!empty($this->scriptMap)) {
            $this->remapScripts();
        }
            
        $this->unifyScripts();

        $this->renderHead($output);
        $this->renderTopHead($output);
        $this->renderBottomHead($output);

        if ($this->enableJavaScript) {
            $this->renderBodyBegin($output);
            $this->renderBodyEnd($output);
        }
    }

    public function renderTopHead(&$output)
    {
        $html='';
        if ($this->enableJavaScript) {
            if (isset($this->scriptFiles[self::POS_TOP_HEAD])) {
                foreach ($this->scriptFiles[self::POS_TOP_HEAD] as $scriptFile) {
                    $html.=CHtml::scriptFile($scriptFile)."\n";
                }
            }

            if (isset($this->scripts[self::POS_TOP_HEAD])) {
                $html .= CHtml::script(implode("\n", $this->scripts[self::POS_TOP_HEAD]))."\n";
            }
        }

        if ($html!=='') {
            $count=0;
            $output = preg_replace('/(<\/title\s*>|<\\/head\s*>)/is', '$1'."\n\n".'<###head###>', $output, 1, $count);
            if ($count) {
                $output = str_replace('<###head###>', $html, $output);
            } else {
                $output = $html.$output;
            }
        }
    }

    public function renderBottomHead(&$output)
    {
        $this->addHashesToAssets();
        $html='';

        foreach ($this->cssFiles as $url => $media) {
            if ($media == self::POS_BOTTOM_HEAD) {
                $html .= CHtml::cssFile($url)."\n";
            }
        }

        if ($html !== '') {
            $count = 0;
            $output = preg_replace('#</head>#i', '<###head###>' . '$0' . "\n", $output, 1, $count);
            if ($count) {
                $output = str_replace('<###head###>', $html, $output);
            } else {
                $output = $html.$output;
            }
        }
    }

    public function renderHead(&$output)
    {
        $this->addHashesToAssets();
        
        $html='';
        foreach ($this->metaTags as $meta) {
            $html .= CHtml::metaTag($meta[ 'content' ], null, null, $meta)."\n";
        }
            
        foreach ($this->linkTags as $link) {
            $html .= CHtml::linkTag(null, null, null, null, $link)."\n";
        }
            
        foreach ($this->cssFiles as $url => $media) {
            if ($media == self::POS_BOTTOM_HEAD) {
                continue;
            }
            $html .= CHtml::cssFile($url, $media)."\n";
        }
            
        foreach ($this->css as $css) {
            $html .= CHtml::css($css[0], $css[1])."\n";
        }
            
        if ($this->enableJavaScript) {
            if (isset($this->scriptFiles[ self::POS_HEAD ])) {
                foreach ($this->scriptFiles[ self::POS_HEAD ] as $scriptFile) {
                    $html .= CHtml::scriptFile($scriptFile)."\n";
                }
            }

            if (isset($this->scripts[ self::POS_HEAD ])) {
                $html .= CHtml::script(implode("\n", $this->scripts[ self::POS_HEAD ]))."\n";
            }
        }

        if ($html !== '') {
            $count = 0;
            $output = preg_replace('#</title>#i', '$0'."\n".'<###head###>', $output, 1, $count);
            if ($count) {
                $output = str_replace('<###head###>', $html, $output);
            } else {
                $output = $html.$output;
            }
        }
    }

    private function getAssetsHashesCache()
    {
        $cache = Yii::app()->getComponent($this->cacheID);
        $cacheKey = self::CACHE_KEY_PREFIX.'AssetsHashes';
        if ($this->cachingDuration > 0 && $cache) {
            if (($data = $cache->get($cacheKey)) !== false) {
                return unserialize($data);
            }
        }
        return array();
    }

    private function setAssetsHashesCache($hashes)
    {
        $cache = Yii::app()->getComponent($this->cacheID);
        $cacheKey = self::CACHE_KEY_PREFIX.'AssetsHashes';
        if ($this->cachingDuration > 0 && $cache) {
            $cache->set($cacheKey, serialize($hashes), $this->cachingDuration);
        }
    }

    private function isAsset($url)
    {
        return preg_match('#^/[^/]#', $url);
    }

    private function getMTimeHash($mtime)
    {
        $hash = md5($mtime);
        $hash = substr($hash, 0, 8);
        return $hash;
    }

    private function addHashToAsset(&$url, &$hashes)
    {
        $changeHashes = false;
        
        if (empty($hashes[ $url ])) {
            $baseUrl = Yii::app()->baseUrl;
            $path = preg_replace("#^{$baseUrl}/#", '', $url);
            $mtime = @filemtime($path);
            $hashes[ $url ] = $mtime ? $this->getMTimeHash($mtime) : '';
            $changeHashes = true;
        }
        if (strlen($hashes[ $url ])) {
            $url .= ( substr_count($url, '?') ? '&' : '?' ) . $hashes[ $url ];
        }
        
        return $changeHashes;
    }

    private function addHashesToAssets()
    {
        $hashes = $this->getAssetsHashesCache();
        $changeHashes = !$hashes;
            
            # css
        $cssFiles = array();
        foreach ($this->cssFiles as $url => $media) {
            if ($this->isAsset($url)) {
                $changeHashes |= $this->addHashToAsset($url, $hashes);
            }
            $cssFiles[ $url ] = $media;
        }
        $this->cssFiles = $cssFiles;
        
            # js
        if ($this->enableJavaScript) {
            foreach (array( self::POS_HEAD, self::POS_BEGIN, self::POS_END ) as $index) {
                if (isset($this->scriptFiles[ $index ])) {
                    $scriptFiles = array();
                    foreach ($this->scriptFiles[ $index ] as $url => $val) {
                        if ($this->isAsset($url)) {
                            $changeHashes |= $this->addHashToAsset($url, $hashes);
                        }
                        $scriptFiles[$val] = $url;
                    }
                    $this->scriptFiles[ $index ] = $scriptFiles;
                }
            }
        }
                    
        if ($changeHashes) {
            $this->setAssetsHashesCache($hashes);
        }
    }
}
