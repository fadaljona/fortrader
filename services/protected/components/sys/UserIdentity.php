<?
	Yii::import( 'components.sys.CustomException' );

	final class UserIdentity extends CUserIdentity {
		public $email;
		protected $model;
		function __construct( $email, $password ) {
			parent::__construct( $email, $password );
			$this->email = $email;
		}
		function setModel( $model ) {
			$this->model = $model;
		}
		function getModel() {
			return $this->model;
		}
		function isRoot() {
			$model = $this->getModel();
			return $model ? $model->isRoot() : null;
		}
		function authenticate() {
			$this->errorCode = self::ERROR_NONE;
			try{
				$user = UserModel::model()->findForAuthenticate( $this->username );
				if( !$user ) 
					throw new CustomException( '', self::ERROR_USERNAME_INVALID );
				
				if( !$user->isPassword( $this->password ))
					throw new CustomException( '', self::ERROR_PASSWORD_INVALID );
					
				$this->setModel( $user );
			}
			catch( CustomException $e ) {
				$this->errorCode = $e->getCode();
			}
			catch( Exception $e ) {
				throw $e;
			}
			return $this->errorCode == self::ERROR_NONE;
		}
		function getId() {
			$model = $this->getModel();
			return $model ? $model->id : null;
		}
		function getName() {
			$model = $this->getModel();
			return $model ? $model->showName : null;
		}
	}

?>