<?
	require_once __DIR__."/CComponent.php";

	class WebApplication extends CWebApplication {
		protected function init() {
			parent::init();
		}
		function setLanguage( $language ) {
			parent::setLanguage( $language );
			if( defined( 'LanguageModelLoaded' )) 
				LanguageModel::setIDCurrentLanguage();
		}
		public function createUrl($route,$params=array(),$ampersand='&'){
			$routeExp = explode('/', $route);
			
			if( isset(Yii::app()->controller) ){
				$moduleName = @Yii::app()->controller->module->id;
				if( 
					(
						Yii::app()->controller->id == 'quotesNew' || 
						Yii::app()->controller->id == 'broker' || 
						Yii::app()->controller->id == 'binary' || 
						Yii::app()->controller->id == 'currencyRates' || 
						Yii::app()->controller->id == 'journal' || 
						Yii::app()->controller->id == 'contest' || 
						Yii::app()->controller->id == 'contestMember' || 
						Yii::app()->controller->id == 'newsAggregator' || 
						Yii::app()->controller->id == 'feeds' || 
						Yii::app()->controller->id == 'interestRates' || 
						Yii::app()->controller->id == 'informers' || 
						Yii::app()->controller->id == 'calendarEvent' || 
						Yii::app()->controller->id == 'monitoring' || 
						Yii::app()->controller->id == 'categoryRatings' || 
						Yii::app()->controller->id == 'user' || 
						Yii::app()->controller->id == 'commentsUnsubscribe' || 
						Yii::app()->controller->id == 'cryptoCurrencies'
					) && 
					$moduleName == '' 
				){
					if( 
						$routeExp[0] != 'quotesNew' && 
						$routeExp[0] != 'broker' && 
						$routeExp[0] != 'binary' && 
						$routeExp[0] != 'currencyRates' && 
						$routeExp[0] != 'journal'  && 
						$routeExp[0] != 'contest' && 
						$routeExp[0] != 'contestMember' && 
						$routeExp[0] != 'newsAggregator' && 
						$routeExp[0] != 'feeds' && 
						$routeExp[0] != 'interestRates' && 
						$routeExp[0] != 'informers' && 
						$routeExp[0] != 'calendarEvent' && 
						$routeExp[0] != 'monitoring' && 
						$routeExp[0] != 'categoryRatings' && 
						$routeExp[0] != 'user' && 
						$routeExp[0] != 'commentsUnsubscribe' && 
						$routeExp[0] != 'cryptoCurrencies'
					){
						$outUrl = '/services' . $this->getUrlManager()->createUrl($route,$params,$ampersand);
						$outUrl = str_replace('/en', '', $outUrl);
					}else{
						$outUrl = $this->getUrlManager()->createUrl($route,$params,$ampersand);
					}
				}else{
					$outUrl = $this->getUrlManager()->createUrl($route,$params,$ampersand);
				}
			}else{
				$outUrl = $this->getUrlManager()->createUrl($route,$params,$ampersand);
			}
			
			
			if( 
				$routeExp[0] == 'quotesNew' || 
				$routeExp[0] == 'broker' || 
				$routeExp[0] == 'binary' || 
				$routeExp[0] == 'currencyRates' || 
				$routeExp[0] == 'journal'  || 
				$routeExp[0] == 'contest' || 
				$routeExp[0] == 'contestMember' || 
				$routeExp[0] == 'newsAggregator' || 
				$routeExp[0] == 'feeds' || 
				$routeExp[0] == 'interestRates' || 
				$routeExp[0] == 'informers' || 
				$routeExp[0] == 'calendarEvent' || 
				$routeExp[0] == 'monitoring' || 
				$routeExp[0] == 'categoryRatings' || 
				$routeExp[0] == 'user' || 
				$routeExp[0] == 'commentsUnsubscribe' || 
				$routeExp[0] == 'cryptoCurrencies'
			){
				return str_replace( '/services', '', $outUrl );
			}
			return $outUrl;
		}
	}

?>