<?
	
	final class DbMessageSource extends CMessageSource {
		const commonCategory = '*';
		const CACHE_KEY_PREFIX = 'DbMessageSource.';
		public $cachingDuration = 86400;
		public $cacheID = 'cache';
		private $messages = Array();
		public $forceTranslation = true;
		function clearCache( $language ) {
			$cache = Yii::app()->getComponent( $this->cacheID );
			$cacheKey = self::CACHE_KEY_PREFIX.$language;
			if( $cache ) {
				return $cache->delete( $cacheKey );
			}
			return false;
		}
		protected function loadMessages( $category, $language ){
			return Array();
		}
		protected function loadLanguageMessages( $language ){
			$messages = Array();
			$languageModel = LanguageModel::model()->findByAttributes( Array( 'alias' => $language ));
			$models = MessageModel::model()->findAll( Array(
				'with' => Array(
					'currentLanguageI18N' => Array(
						'condition' => "`currentLanguageI18N`.`idLanguage` = :idLanguage",
						'params' => Array(
							':idLanguage' => $languageModel ? $languageModel->id : 0,
						),
					),
				),
			));
			foreach( $models as $message ) {
				$nss = explode( ',', $message->ns );
				$nss = array_map( 'trim', $nss );
				foreach( $nss as $ns ) {
					$messages[ $ns ][ $message->key ] = $message->currentLanguageI18N->value;
				}
			}
			return $messages;
		}
		protected function getLanguageMessages( $language ){
			$cache = Yii::app()->getComponent( $this->cacheID );
			$cacheKey = self::CACHE_KEY_PREFIX.$language;
			
			if( $this->cachingDuration > 0 && $cache ) {
				if(( $data = $cache->get( $cacheKey )) !== false ) {
					return unserialize( $data );
				}
			}
			
			$messages = $this->loadLanguageMessages( $language );
			if( $this->cachingDuration > 0 && $cache ) {
				$dependency = new CDbCacheDependency( " SELECT MAX( `updatedDT` ), COUNT( * ) FROM `{{message}}` " );
				$cache->set( $cacheKey, serialize( $messages ), $this->cachingDuration, $dependency );
			}
			return $messages;
		}
		protected function translateMessage( $categories, $message, $language ){
			if( is_array( $message )) {
				$result = Array();
				foreach( $message as $key=>$_message )
					$result[$key] = $this->translateMessage( $categories, $_message, $language );
				return $result;				
			}
			
			if( !isset( $this->messages[$language])) 
				$this->messages[$language] = $this->getLanguageMessages( $language );
			
			foreach( (array)$categories as $category ) {
				$link = &$this->messages[$language][$category][$message];
				if( isset( $link )) 
					return $link;
			}
			
			$link = &$this->messages[$language][self::commonCategory][$message];
			if( isset( $link )) 
				return $link;
			
			return $message;
		}
	}
	
?>