<?
	Yii::import( 'models.UserModel' );
	final class WebUser extends CWebUser {
		public $allowWPLogin = true;
		function init() {

			parent::init();
			if( $this->getIsGuest() && $this->allowWPLogin && !headers_sent() ) {
				$this->restoreFromWP();
			}
			$model = $this->getModel();
			if( $model ) {
				$model->usedDT = new CDbExpression( 'NOW()' );
				$model->update( Array( 'usedDT' ));
				$model->usedDT = date( "Y-m-d H:i:s" );
			}
		}
		function restoreFromWP() {
			if( empty( $_COOKIE['user_loged_in'])) return false;
			
			CommonLib::loadWp();
			
			if( !defined( 'LOGGED_IN_COOKIE' )) return false;
			if( empty( $_COOKIE[LOGGED_IN_COOKIE])) return false;
			
			$id = wp_validate_auth_cookie( $_COOKIE[LOGGED_IN_COOKIE], 'logged_in' );
			if( !$id ) return false;
			$cookie_elements = wp_parse_auth_cookie( $_COOKIE[LOGGED_IN_COOKIE], 'logged_in' );
			
			$name = $cookie_elements[ 'username' ];
			$states = Array();
			if($this->beforeLogin( $id, $states, true )){
				$this->changeIdentity( $id, $name, $states );
				$this->afterLogin( true );
			}
		}
		function login( $identity, $duration = 0 ) {
			if( parent::login( $identity, $duration )) {
				if( $this->allowWPLogin && !headers_sent() ) {
					$id=$identity->getId();
					
					CommonLib::loadWp();
					
					wp_set_auth_cookie( $id, $duration > 0 );
				}
			}
		}
		function logout( $destroySession = true ) {
			parent::logout( $destroySession );
			
			CommonLib::loadWp();
			
			if( !isset( Yii::app()->params['wpLogOut'] ) || !Yii::app()->params['wpLogOut'] ){
				Yii::app()->params['wpLogOut'] = false;
				wp_logout();
			}
		}
		function getModel() {
			static $model;
			
			if( !$this->id ) return null;
			if( $model === null ) {
				$model = UserModel::model()->findByPk( $this->id );
				if( !$model ) $model = false;
			}
			return $model;
		}
		function getRights() {
			static $rights;
			
			if( $rights === null ) {
				$model = $this->getModel();
				if( !$model ) return Array();
				
				$rights = Array();
				$groups = $model->groups( Array( 
					'select' => Array( '`groups`.`id`' ),
					'with' => Array(
						'rights' => Array(
							'select' => Array( '`rights`.`name`' ),
						),
					)
				));
				foreach( $groups as $group ) {
					foreach( $group->rights as $right ) {
						$rights[] = $right->name;
					}
				}
				$rights = array_values(array_unique( $rights ));
			}
			return $rights;
		}
		function isRoot() {
			$model = $this->getModel();
			return $model ? $model->isRoot() : false;
		}
		function getIsGuest() {
			return !$this->id or !$this->getModel();
		}
		function checkRight( $right ) {
			if( $this->isRoot()) return true;
			$rights = $this->getRights();
			return in_array( $right, $rights );
		}
		function checkAccess( $operation, $params=Array(), $allowCaching=true ) {
			return $this->checkRight( $operation );
		}
		function checkAccessArray( $arrayRights, $operator = 'OR' ) {
			if( !$arrayRights ) return false;
			$operator = strtoupper( $operator );
			switch( $operator ) {
				case 'OR':{
					foreach( $arrayRights as $right ) {
						if( $this->checkAccess( $right )) return true;
					}
					break;
				}
				case 'AND':{
					foreach( $arrayRights as $right ) {
						if( !$this->checkAccess( $right )) return false;
					}
					return true;
					break;
				}
			}
			return false;
		}
		function hasAdminRight() {
			$rights = "
				userControl, userGroupControl, languageControl, settings, serverControl, contestControl, 
				brokerControl, countryControl, contestWinerControl, contestMemberControl, userBalanceControl,
				advertisementControl, ownAdvertisementControl, advertisementZoneControl, tradePlatformControl, 
				ownBalanceView
			";
			$rights = explode( ',', $rights );
			$rights = array_map( 'trim', $rights );
			return $this->checkAccessArray( $rights );
		}
	}

?>