<?php

require_once Yii::getPathOfAlias('ext.minify') . '/vendor/autoload.php';
Yii::import( 'components.sys.FileHelper' );

final class AssetManager extends CAssetManager{
	
	private $_basePath;
	private $_baseUrl;
	private $_published=array();
	
	public function publish($path,$hashByName=false,$level=-1,$forceCopy=null)
	{
		if($forceCopy===null)
			$forceCopy=$this->forceCopy;
		if($forceCopy && $this->linkAssets)
			throw new CException(Yii::t('yii','The "forceCopy" and "linkAssets" cannot be both true.'));
		if(isset($this->_published[$path]))
			return $this->_published[$path];
		elseif(is_string($path) && ($src=realpath($path))!==false)
		{
			$dir=$this->generatePath($src,$hashByName);
			$dstDir=$this->getBasePath().DIRECTORY_SEPARATOR.$dir;
			if(is_file($src))
			{
				$fileName=basename($src);
				$dstFile=$dstDir.DIRECTORY_SEPARATOR.$fileName;

				if(!is_dir($dstDir))
				{
					mkdir($dstDir,$this->newDirMode,true);
					@chmod($dstDir,$this->newDirMode);
				}

				if($this->linkAssets && !is_file($dstFile)) symlink($src,$dstFile);
				elseif(@filemtime($dstFile)<@filemtime($src))
				{
					copy($src,$dstFile);
					@chmod($dstFile,$this->newFileMode);

					FileHelper::minifyCssJs($dstFile);
				}

				return $this->_published[$path]=$this->getBaseUrl()."/$dir/$fileName";
			}
			elseif(is_dir($src))
			{
				if($this->linkAssets && !is_dir($dstDir))
				{
					symlink($src,$dstDir);
				}
				elseif(!is_dir($dstDir) || $forceCopy)
				{
					FileHelper::copyDirectory($src,$dstDir,array(
						'exclude'=>$this->excludeFiles,
						'level'=>$level,
						'newDirMode'=>$this->newDirMode,
						'newFileMode'=>$this->newFileMode,
					));
				}
				return $this->_published[$path]=$this->getBaseUrl().'/'.$dir;
			}
		}
		throw new CException(Yii::t('yii','The asset "{asset}" to be published does not exist.',
			array('{asset}'=>$path)));
	}
}