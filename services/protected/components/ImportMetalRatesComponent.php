<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class ImportMetalRatesComponent extends ComponentBase {
		private $dataUrl = 'http://www.cbr.ru/scripts/xml_metall.asp?date_req1={dateFrom}&date_req2={dateTo}';
		private $firstDate = 852058800; //'01/01/1997'
		private $perOneRequestTime = 60*60*24*365;
	
		private function getFirstTime(){
			$model = MetalRatesHistoryModel::model()->find(array(
				'order' => ' `t`.`date` DESC ',
			));
			if( !$model ) return $this->firstDate;
			
			return strtotime( $model->date );
		}
		private function getLastTime( $firstTime ){
			if( time() - $firstTime > $this->perOneRequestTime ) return $firstTime + $this->perOneRequestTime;
			return time();
		}
		private function importFromUrl($dataUrl){
			if( !$dataUrl ) return false;
			echo "start import from $dataUrl\n";
			
			$xmlStr = CommonLib::downloadFileFromWww($dataUrl);
			if( !$xmlStr ) throw new CException("Can't load $dataUrl\n");
			
			echo "loaded $dataUrl\n";
			
			$xml = simplexml_load_string( $xmlStr );
			if( !$xml ) throw new CException("simplexml can't load string from $dataUrl\n");
			
	
			foreach( $xml->Record as $item ){
				try{
					$model = new MetalRatesHistoryModel;
				
					$model->idRate = $item->attributes()->Code->__toString();
					$model->date = $this->normalizeDate( $item->attributes()->Date->__toString() );
					$model->valueBuy = str_replace(',', '.', $item->Buy->__toString() );
					$model->valueSell = str_replace(',', '.', $item->Sell->__toString() );
					$model->createdTime = time();
				  
					if( $model->save() ){
						echo "data saved {$model->idRate}\t{$model->date}\t{$model->valueBuy}\t{$model->valueSell}\n";
					}
				}catch( Exception $e ) {
					//echo $e->getMessage() . "\n";
				}
			}
			
		}
		private function normalizeDate( $dateStr ){
			$dateArr = explode('.', $dateStr);
			if( count($dateArr) < 3 ) throw new CException("date format changed\n");
			return $dateArr[2] . '-' . $dateArr[1] . '-' . $dateArr[0];
		}


		function import() {
			$firstTime = $this->getFirstTime();
			$lastTime = $this->getLastTime( $firstTime );
			$dataUrl = str_replace( array('{dateFrom}', '{dateTo}'), array( date('d/m/Y', $firstTime), date('d/m/Y', $lastTime) ), $this->dataUrl );
			$this->importFromUrl($dataUrl);	
		}
	}
?>