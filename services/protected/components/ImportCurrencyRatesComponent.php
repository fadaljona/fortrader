<?
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'models.forms.AdminCurrencyRatesFormModel' );
		
	final class ImportCurrencyRatesComponent extends ComponentBase {
		public $currenciesUrl = 'http://www.cbr.ru/scripts/XML_val.asp?d=0';
		public $chartCodesUrl = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=';
		public $dataUrl = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=';
		public $daysToSearchCharCode = 45;
		
	
		private function updateCurrenciesList(){
			echo "start load new currencies\n";
			
			$xmlStr = CommonLib::downloadFileFromWww($this->currenciesUrl);
			$xml = simplexml_load_string( $xmlStr );
			
			if( !$xml ) throw new CException("Can't load {$this->currenciesUrl} ");
			foreach( $xml->Item as $item ){
				$cbrId = $item->attributes()->ID;
				$id = CurrencyRatesModel::getIdByCbrId( $cbrId );
				if( $id ) continue;
				
				$data = Array();
				try{
					$formModel = new AdminCurrencyRatesFormModel();
					$loaded = $formModel->load();
					if( !$loaded ) throw new CException( "Can't load formModel" );
					$formModel->code = $cbrId;
					$formModel->cbrId = $cbrId;
					$formModel->noData = 0;
					$formModel->names = array(
						0 => $item->EngName,
						1 => $item->Name,
					);
					
					if( $formModel->validate()) {
						$id = $formModel->save();
						if( !$id ) throw new CException( "Can't save AR!" );
						$data[ 'id' ] = $id;
					}else{
						list( $data[ 'errorField' ], $data[ 'error' ]) = $formModel->getFirstError();
					}
				}
				catch( Exception $e ) {
					$data[ 'error' ] = $e->getMessage();
				}
				print_r( $data );
			}
			echo "new currencies loaded\n";
		}
		private function setCharCodes(){
			echo "start setCharCodes\n";
			$models = CurrencyRatesModel::model()->findAll(array(
				'condition' => " `t`.`code` = `t`.`cbrId` AND `t`.`noData` = 0 ",
			));
			$timeStamp = time();
			$receivedDays = 0;
			$chartCodeArr = array();
			
			foreach( $models as $model ){
				
				if( isset( $chartCodeArr[$model->cbrId] ) ){
					$model->code = $chartCodeArr[$model->cbrId]['code'];
					$model->cbrDigitCode = $chartCodeArr[$model->cbrId]['cbrDigitCode'];
					
					if( $model->validate() ) $model->save();
					continue;
				}
				for( $i = $receivedDays; $i < $this->daysToSearchCharCode; $i++ ){
					echo $this->chartCodesUrl . date( 'd/m/Y', $timeStamp - 60*60*24*$i ) . "\n";
					
					$xmlStr = CommonLib::downloadFileFromWww( $this->chartCodesUrl . date( 'd/m/Y', $timeStamp - 60*60*24*$i ) );
					$xml = simplexml_load_string( $xmlStr );
					
					if( !$xml ) {
						echo "Can't load " . $this->chartCodesUrl . date( 'd/m/Y', $timeStamp - 60*60*24*$i ) . "\n";
						continue;
					}
					
					foreach( $xml->Valute as $item ){
						$cbrId = $item->attributes()->ID->__toString();
						$code = $item->CharCode->__toString();
						$chartCodeArr[$cbrId] = array(
							'code' => $code,
							'cbrDigitCode' => $item->NumCode->__toString(),
						);
					}
					$receivedDays++;
					if( isset( $chartCodeArr[$model->cbrId] ) ){
						$model->code = $chartCodeArr[$model->cbrId]['code'];
						$model->cbrDigitCode = $chartCodeArr[$model->cbrId]['cbrDigitCode'];
						
						if( $model->validate() ) {
							if( $model->save() ){
								echo "model charCode updated {$model->code} \n";
							}
						}
						$i = $this->daysToSearchCharCode;
					}
				}
				if( $model->code == $model->cbrId ){
					$model->noData = 1;
					if( $model->validate() ) {
						if( $model->save() ){
							echo "model noData setUp {$model->code} \n";
						}
					}
				}
			}
			echo "end setCharCodes\n";
		}
		private function importAllData(){
			$models = CurrencyRatesModel::model()->findAll(array(
				'condition' => " `t`.`noData` = 0 ",
			));
			$dataArr = array();
			$endDatesArr = array();
			$datesToReceiveArr = array();
			foreach( $models as $model ){
				
				if( $model->lastDataDateForImport == date( 'Y-m-d', time() + 60*60*24 ) ) continue;
				$dateForReceiveData = strtotime( $model->lastDataDateForImport . ' 00:00:00' ) + 60*60*24;
				
				$endDateForReceiveData = $dateForReceiveData + 60*60*24 * 50;
				if( $endDateForReceiveData  > time() ) $endDateForReceiveData = time() + 60*60*24;
				
				$endDatesArr[$model->cbrId] = $endDateForReceiveData;
				
				while( $dateForReceiveData < $endDateForReceiveData ){
					$datesToReceiveArr[] = date( 'd/m/Y', $dateForReceiveData );
					$dateForReceiveData += 60*60*24;
				}
			}
			
			$datesToReceiveArr = array_unique( $datesToReceiveArr );
			echo "receiving data from cbr\n";
			foreach( $datesToReceiveArr as $date ){
				$dataUrl = $this->dataUrl . $date;
				
				echo $dataUrl . "\n";
				
				$xmlStr = CommonLib::downloadFileFromWww( $dataUrl );
				$xml = simplexml_load_string( $xmlStr );
				
				if( !$xml ) throw new CException("Can't load {$dataUrl} ");
				
				$urlDate = $xml->attributes()->Date->__toString();
				$tmpDate = date_create_from_format('d.m.Y H:i:s', $urlDate. ' 00:00:01'); 
				$urlDateTimestamp = $tmpDate->getTimestamp();
				$urlDate = $tmpDate->format('Y-m-d');

				foreach( $xml->Valute as $item ){
					$charId = $item->attributes()->ID->__toString();
					
					if( isset( $endDatesArr[$charId] ) && $urlDateTimestamp > $endDatesArr[$charId] ) continue;
					
					$dataArr[$charId][$urlDate] = array(
						'nominal' => $item->Nominal->__toString(),
						'value' => str_replace( ',', '.', $item->Value->__toString())
					);
				}

			}
			echo "data received cbr\n";
			/*print_r( $endDatesArr );
			print_r( $datesToReceiveArr );
			print_r( $dataArr );*/
			
			foreach( $dataArr as $cbrId => $data ){
				$this->importOneCurrencyData( $cbrId, $data );
			}
		}
		private function importOneCurrencyData($cbrId, $data){
			$model = CurrencyRatesModel::findByCbrId($cbrId);
			if(!$model) return false;
			
			$query = " INSERT IGNORE INTO `{{currency_rates_history}}` (`idCurrency`, `date`, `value`, `nominal`, `createdTime`) VALUES ";
			$i=0;
			$params = array( ':id' => $model->id );
			foreach( $data as $dataDate => $values ){
				
				if( $model->lastDataDateForImport == $dataDate ) continue;
				
				if( $i ) $query .= ',';
				$query .= "(:id, :date$i, :value$i, :nominal$i, :createdTime$i )";
				$params[":date$i"] =  $dataDate;
				$params[":value$i"] = $values['value'];
				$params[":nominal$i"] = $values['nominal'];
				$params[":createdTime$i"] = time();
				$i++;		
			}
			//echo $query;
			//print_r( $params );
			if( count($params) != 1 ){
				$insertResult = Yii::App()->db->createCommand( $query )->query( $params );
				echo " inserted rows {$insertResult->rowCount} for currency {$model->id}\n";
			}
		}
		function import() {
			$this->updateCurrenciesList();
			$this->setCharCodes();
			$this->importAllData();
			CurrencyRatesHistoryModel::setUpDiffValues();
		}
	}
?>