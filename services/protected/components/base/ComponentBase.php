<?

	abstract class ComponentBase {
		function init() {
		}
		protected function resolveClassName( $class ) {
			return $class ? $class : get_class( $this );
		}
		function getCleanClassName( $class = null ) {
			$class = $this->resolveClassName( $class );
			$class = preg_replace( "#Widget$#", "", $class );
			$class[0] = strtolower($class[0]);
			return $class;
		}
		function getNSi18n( $class = null ) {
			$class = $this->getCleanClassName( $class );
			return "components/{$class}";
		}
		protected function throwI18NException( $message, $params = Array(), $class = null ) {
			$NSi18n = $this->getNSi18n( $class );
			throw new Exception( Yii::t( $NSi18n, $message, $params ));
		}
	}

?>
