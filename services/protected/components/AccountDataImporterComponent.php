<?
	/*
		Компонент для импорта данных по аккаунту с внешнего мониторинга
		Компонент запускается в скрипте externalImportAccountData.php - AccountDataImporterComponent::import();
	*/
		// импорт связей
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'components.MailerComponent' );
	
		// класс компонента
	final class AccountDataImporterComponent extends ComponentBase {
			// путь для сохранения логов
		const pathSave = 'log/importAccountData';
			
			// ID'шник админа для уведомлений
		const idAdmin = 1;
		
		public $affectedRowsHIST = 0;
		public $affectedRowsOPEN = 0;
			
			/*
				Функция проверки настроек проека.
				Импорт данных можно отключить в настройках - "Импорт данных об аккаунтах"
			*/
		private static function checkSettings() {
			$settings = SettingsModel::model()->getModel();
			if( $settings->common_importAccountData == 'Disabled' ) exit;
		}
			
			/*
				Функция для получения части строки по ключу <-key=inner/-key>    ->    inner
			*/
		private static function getPart( $text, $key ) {
			return preg_match( "#<-{$key}=(.*)/-{$key}>#Us", $text, $match ) ? $match[1] : null;
		}
		
			/*
				Функция для парсинга одной строки в массив по ключам и значениям <key=value	/>   ->   Array( key => value )
			*/
		private static function parseRow( $row ) {
			$assoc = Array();
			$row = trim( $row );
			if( preg_match_all( "#<(.+)=(.*)[\t, ]+/>#U", $row, $matchs )) {
				for( $i=0; $i<count( $matchs[0] ); $i++ ) {
					$assoc[ $matchs[1][$i] ] = $matchs[2][$i];
				}
			}
			return $assoc;
		}
			
			/*
				Функция для парсинга блока текста по строкам
			*/
		private static function parseRows( $text ) {
			$array = Array();
				// делим на строки
			$rows = explode( PHP_EOL, $text );
				// парсим в цикле
			foreach( $rows as $row ) {
				$row = trim( $row );
				if( strlen( $row )) {
					$array[] = self::parseRow( $row );
				}
			}
			return $array;
		}
		private static function parseStats( $text ) {
			$array = Array();
				// делим на строки
			$rows = explode( PHP_EOL, $text );
				// парсим в цикле

			foreach( $rows as $row ) {
				$row = trim( $row );
				if( strlen( $row )) {
					$varVal = explode( "=", $row );
					$array[$varVal[0]] = $varVal[1];
				}
			}
			return $array;
		}
		
			/*
				Функция получания объекта-сервера по его адресу из ключа srvMt4
			*/
		static function getServer( $assocACC ) {
			$server = null;
			if( isset($assocACC['srvMt4']) ) {
				// поиск сервера по url = srvMt4
				$server = ServerModel::model()->findByAttributes( Array( 'url' => $assocACC['srvMt4'] ));
				if( $server ) return $server;
			}elseif( isset($assocACC['last_srvMt4']) ){
					// поиск сервера по url = srvMt4
				$server = ServerModel::model()->findByAttributes( Array( 'url' => $assocACC['last_srvMt4'] ));
				if( $server ) return $server;
			}
			
				// если сервер не найден генерируем ошибку в модель лога и выходим
			if( isset($assocACC['srvMt4']) ) {
				$message = 'Обнаружен Новый IP сервера брокера: {ip}';
				$message = strtr( $message, Array(
					'{ip}' => $assocACC['srvMt4'],
				));
				$log = ContestMemberLogModel::instance( $message );
				$log->save();
			}elseif( isset($assocACC['last_srvMt4']) ){
				$message = 'Обнаружен Новый IP сервера брокера: {ip}';
				$message = strtr( $message, Array(
					'{ip}' => $assocACC['last_srvMt4'],
				));
				$log = ContestMemberLogModel::instance( $message );
				$log->save();
			}
			
			exit;
		}
		
			/*
				Функция обновления имени сервера по ключу i_server
			*/
		static function updateServer( $assocACC, $server ) {
			if( @$assocACC['i_server'] ) {
					// если у сервера другое имя - обновляем
				if( $assocACC['i_server'] != $server->internalName ) {
					$server->name = $assocACC['i_server'];
					$server->internalName = $assocACC['i_server'];
					$server->save();
				}
			}
		}
		
			/*
				Функция поиска объекта мониторинга по aid (участник конкурса или торговый счёт советников)
			*/
		static function getModel( $assocACC ) {
				// ищем участника конкурса
			$contestMember = ContestMemberModel::model()->findByPk( $assocACC[ 'aid' ]);
			if( $contestMember ) return $contestMember;
			
				// ищем торговый счёт
			$tradeAccount = TradeAccountModel::model()->findByPk( $assocACC[ 'aid' ]);
			if( $tradeAccount ) return $tradeAccount;
				
				// выходим если ничего не нашли
			exit;
		}
		
			/*
				Функция импорта данных из секции ACC в таблицу mt5_account_data
			*/
		static function importMT5AccountData( $assocACC, $server, $arraySTATS ) {
			if( !isset( $assocACC[ 'i_bal' ])) return;
			$assocACC[ 'ServerId' ] = $server->id;
			$paramVals = array_merge($assocACC, $arraySTATS);
			
			$prevModel = MT5AccountDataModel::model()->find(array(
				'condition' => ' `t`.`ACCOUNT_NUMBER` = :ACCOUNT_NUMBER AND `t`.`AccountServerId` = :AccountServerId ',
				'order' => ' `t`.`timestamp` DESC ',
				'params' => array( ':ACCOUNT_NUMBER' => $paramVals['login'], ':AccountServerId' => $paramVals['ServerId'] )
			));
			
			if( $prevModel ){
				if( 
$prevModel->ACCOUNT_BALANCE == intval( @$paramVals['i_bal'] ) && 
$prevModel->ACCOUNT_EQUITY == intval( @$paramVals['i_equi'] ) && 
$prevModel->ACCOUNT_MARGIN == @$paramVals['i_marg'] && 
$prevModel->ACCOUNT_PROFIT == @$paramVals['i_prof'] && 
$prevModel->ACCOUNT_LEVERAGE == @$paramVals['i_level'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_TOTAL == @$paramVals['ACCOUNT_ORDERS_HISTORY_TOTAL'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_PENDING_TOTAL == @$paramVals['ACCOUNT_ORDERS_HISTORY_PENDING_TOTAL'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_BEST_PIPS == @$paramVals['ACCOUNT_ORDERS_HISTORY_BEST_PIPS'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_WORSE_PIPS == @$paramVals['ACCOUNT_ORDERS_HISTORY_WORSE_PIPS'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_BEST == @$paramVals['ACCOUNT_ORDERS_HISTORY_BEST'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_WORSE == @$paramVals['ACCOUNT_ORDERS_HISTORY_WORSE'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_PROFIT_PERCENT == @$paramVals['ACCOUNT_ORDERS_HISTORY_PROFIT_PERCENT'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_PROFIT_FACTOR == @$paramVals['ACCOUNT_ORDERS_HISTORY_PROFIT_FACTOR'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE == @$paramVals['ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE_PIPS == @$paramVals['ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE_PIPS'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_MAXLOT == @$paramVals['ACCOUNT_ORDERS_HISTORY_MAXLOT'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_MINLOT == @$paramVals['ACCOUNT_ORDERS_HISTORY_MINLOT'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_AVERAGELOT == @$paramVals['ACCOUNT_ORDERS_HISTORY_AVERAGELOT'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_LOSS_AVERAGE == @$paramVals['ACCOUNT_ORDERS_HISTORY_LOSS_AVERAGE'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_RESULT_AVERAGE == @$paramVals['ACCOUNT_ORDERS_HISTORY_RESULT_AVERAGE'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_LOT_AVERAGE == @$paramVals['ACCOUNT_ORDERS_HISTORY_LOT_AVERAGE'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_LOT_SUM == @$paramVals['ACCOUNT_ORDERS_HISTORY_LOT_SUM'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_DAY == @$paramVals['ACCOUNT_ORDERS_HISTORY_DAY'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_PROFIT_TOTAL == @$paramVals['ACCOUNT_ORDERS_HISTORY_PROFIT_TOTAL'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_LOSS_TOTAL == @$paramVals['ACCOUNT_ORDERS_HISTORY_LOSS_TOTAL'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_GROSS_PROFIT == @$paramVals['ACCOUNT_ORDERS_HISTORY_GROSS_PROFIT'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_GROSS_LOSS == @$paramVals['ACCOUNT_ORDERS_HISTORY_GROSS_LOSS'] && 
$prevModel->ACCOUNT_ORDERS_HISTORY_PROFIT == @$paramVals['ACCOUNT_ORDERS_HISTORY_PROFIT'] && 
$prevModel->ACCOUNT_ORDERS_SUMLOT == @$paramVals['ACCOUNT_ORDERS_SUMLOT'] && 
$prevModel->ACCOUNT_ORDERS_PENDING_TOTAL == @$paramVals['ACCOUNT_ORDERS_PENDING_TOTAL'] && 
$prevModel->ACCOUNT_ORDERS_LOT_SUM == @$paramVals['ACCOUNT_ORDERS_LOT_SUM'] && 
$prevModel->ACCOUNT_ORDERS_TOTAL == @$paramVals['ACCOUNT_ORDERS_TOTAL'] && 
$prevModel->ACCOUNT_ORDERS_LOT_AVERAGE == @$paramVals['ACCOUNT_ORDERS_LOT_AVERAGE']

				){
					return;
				}
				
			}
			
			$query = "
				INSERT IGNORE INTO `mt5_account_data`
				SET
					`ACCOUNT_NUMBER`           = :login,
					`AccountServerId`          = :ServerId,
					`ACCOUNT_BALANCE`          = :i_bal,
					`ACCOUNT_EQUITY`           = :i_equi,
					`ACCOUNT_MARGIN`           = :i_marg,
					`ACCOUNT_PROFIT`           = :i_prof,
					`ACCOUNT_LEVERAGE`         = :i_level,
					`ACCOUNT_EQUITY_DROW`      = ROUND( GREATEST( :i_bal - :i_equi, 0 ), 2 ),
					`ACCOUNT_EQUITY_DROW_PERC` = ROUND( GREATEST( ( :i_bal - :i_equi ) / :i_bal * 100, 0 ), 2 ),
					
					`ACCOUNT_ORDERS_HISTORY_TOTAL`			= :ACCOUNT_ORDERS_HISTORY_TOTAL,
					`ACCOUNT_ORDERS_PENDING_TOTAL`			= :ACCOUNT_ORDERS_PENDING_TOTAL,
					`ACCOUNT_ORDERS_HISTORY_PENDING_TOTAL`	= :ACCOUNT_ORDERS_HISTORY_PENDING_TOTAL,
					`ACCOUNT_ORDERS_TOTAL`					= :ACCOUNT_ORDERS_TOTAL,
					`ACCOUNT_ORDERS_HISTORY_TIME_AVERAGE`		= :ACCOUNT_ORDERS_HISTORY_TIME_AVERAGE,
					`ACCOUNT_ORDERS_HISTORY_BEST_PIPS`		= :ACCOUNT_ORDERS_HISTORY_BEST_PIPS,
					`ACCOUNT_ORDERS_HISTORY_WORSE_PIPS`		= :ACCOUNT_ORDERS_HISTORY_WORSE_PIPS,
					`ACCOUNT_ORDERS_HISTORY_BEST`				= :ACCOUNT_ORDERS_HISTORY_BEST,
					`ACCOUNT_ORDERS_HISTORY_WORSE`			= :ACCOUNT_ORDERS_HISTORY_WORSE,
					`ACCOUNT_ORDERS_HISTORY_PROFIT_PERCENT`	= :ACCOUNT_ORDERS_HISTORY_PROFIT_PERCENT,
					`ACCOUNT_ORDERS_HISTORY_PROFIT_FACTOR`	= :ACCOUNT_ORDERS_HISTORY_PROFIT_FACTOR,
					`ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE`	= :ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE,
					`ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE_PIPS` = :ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE_PIPS,
					`ACCOUNT_ORDERS_HISTORY_LOSS_AVERAGE`		= :ACCOUNT_ORDERS_HISTORY_LOSS_AVERAGE,
					`ACCOUNT_ORDERS_HISTORY_RESULT_AVERAGE`	= :ACCOUNT_ORDERS_HISTORY_RESULT_AVERAGE,
					`ACCOUNT_ORDERS_HISTORY_MAXLOT`			= :ACCOUNT_ORDERS_HISTORY_MAXLOT,
					`ACCOUNT_ORDERS_HISTORY_MINLOT`			= :ACCOUNT_ORDERS_HISTORY_MINLOT,
					`ACCOUNT_ORDERS_HISTORY_AVERAGELOT`		= :ACCOUNT_ORDERS_HISTORY_AVERAGELOT,
					`ACCOUNT_ORDERS_HISTORY_LOT_AVERAGE`		= :ACCOUNT_ORDERS_HISTORY_LOT_AVERAGE,
					`ACCOUNT_ORDERS_LOT_SUM`					= :ACCOUNT_ORDERS_LOT_SUM,
					`ACCOUNT_ORDERS_HISTORY_DAY`				= :ACCOUNT_ORDERS_HISTORY_DAY,
					`ACCOUNT_ORDERS_HISTORY_PROFIT_TOTAL`		= :ACCOUNT_ORDERS_HISTORY_PROFIT_TOTAL,
					`ACCOUNT_ORDERS_HISTORY_LOSS_TOTAL`		= :ACCOUNT_ORDERS_HISTORY_LOSS_TOTAL,
					`ACCOUNT_ORDERS_HISTORY_GROSS_PROFIT`		= :ACCOUNT_ORDERS_HISTORY_GROSS_PROFIT,
					`ACCOUNT_ORDERS_HISTORY_GROSS_LOSS`		= :ACCOUNT_ORDERS_HISTORY_GROSS_LOSS,
					`ACCOUNT_ORDERS_HISTORY_LOT_SUM`			= :ACCOUNT_ORDERS_HISTORY_LOT_SUM,
					`ACCOUNT_ORDERS_LOT_AVERAGE`				= :ACCOUNT_ORDERS_LOT_AVERAGE,
					`ACCOUNT_ORDERS_HISTORY_PROFIT`				= :ACCOUNT_ORDERS_HISTORY_PROFIT,
					`ACCOUNT_ORDERS_SUMLOT`						= :ACCOUNT_ORDERS_SUMLOT
			";
			$params = Array( ':login', ':ServerId', ':i_bal', ':i_equi', ':i_marg', ':i_prof', ':i_level', ':ACCOUNT_ORDERS_HISTORY_TOTAL', ':ACCOUNT_ORDERS_PENDING_TOTAL', ':ACCOUNT_ORDERS_HISTORY_PENDING_TOTAL', ':ACCOUNT_ORDERS_TOTAL', ':ACCOUNT_ORDERS_HISTORY_TIME_AVERAGE', ':ACCOUNT_ORDERS_HISTORY_BEST_PIPS', ':ACCOUNT_ORDERS_HISTORY_WORSE_PIPS', ':ACCOUNT_ORDERS_HISTORY_BEST', ':ACCOUNT_ORDERS_HISTORY_WORSE', ':ACCOUNT_ORDERS_HISTORY_PROFIT_PERCENT', ':ACCOUNT_ORDERS_HISTORY_PROFIT_FACTOR', ':ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE', ':ACCOUNT_ORDERS_HISTORY_PROFIT_AVERAGE_PIPS', ':ACCOUNT_ORDERS_HISTORY_LOSS_AVERAGE', ':ACCOUNT_ORDERS_HISTORY_RESULT_AVERAGE', ':ACCOUNT_ORDERS_HISTORY_MAXLOT', ':ACCOUNT_ORDERS_HISTORY_MINLOT', ':ACCOUNT_ORDERS_HISTORY_AVERAGELOT', ':ACCOUNT_ORDERS_HISTORY_LOT_AVERAGE', ':ACCOUNT_ORDERS_LOT_SUM', ':ACCOUNT_ORDERS_HISTORY_DAY', ':ACCOUNT_ORDERS_HISTORY_PROFIT_TOTAL', ':ACCOUNT_ORDERS_HISTORY_LOSS_TOTAL', ':ACCOUNT_ORDERS_HISTORY_GROSS_PROFIT', ':ACCOUNT_ORDERS_HISTORY_GROSS_LOSS', ':ACCOUNT_ORDERS_HISTORY_LOT_SUM', ':ACCOUNT_ORDERS_LOT_AVERAGE', ':ACCOUNT_ORDERS_HISTORY_PROFIT', ':ACCOUNT_ORDERS_SUMLOT');
			$params = array_combine( $params, $params );
			
			foreach( $params as &$param ) {
				$param = str_replace( ':', '', $param );
				$param = @$paramVals[ $param ];
			}
			Yii::App()->db->createCommand( $query )->query( $params );
		}
		
			/*
				Функция импорта данных из секции ACC в таблицу mt5_account_info
			*/
		static function importMT5AccountInfo( $assocACC, $server ) {
			$prevData = MT5AccountInfoModel::model()->find(array(
				'condition' => ' `ACCOUNT_NUMBER` = :login AND `AccountServer` = :i_server ',
				'params' => array( ':login' => @$assocACC[ 'login' ], ':i_server' => $server->internalName )
			));
			
			$sameErrorCount = 0;
			if( $prevData && $prevData->last_error_code == @$assocACC[ 'stat' ] && @$assocACC[ 'stat' ] !=0 ){
				$sameErrorCount = $prevData->sameErrorCount + 1;
			}

			
			$query = "
				REPLACE `mt5_account_info`
				SET
					`ACCOUNT_NUMBER`   = :login,
					`AccountServer`    = :i_server,
					`ServerUrl`    = :srvMt4,
					`AccountCompany`   = :i_firma,
					`AccountName`      = :i_fio,
					`ACCOUNT_UPDATE`   = :time_last_update,
					`sGmt`   = :sGmt,	
					`IsDemo`           = :i_dr,
					`last_error_code`  = :stat,
					`last_error_descr` = :last_logMt4,
					`sameErrorCount` = :sameErrorCount
			";
			if( !isset( $assocACC['i_dr'] ) ){
				
				$assocACC['i_dr'] = $prevData->IsDemo;
			}elseif( $assocACC['i_dr'] == 'demo' ){
				$assocACC['i_dr'] = 1;
			}elseif( $assocACC['i_dr'] == 'real' ){
				$assocACC['i_dr'] = 0;
			}else{
				$assocACC['i_dr'] = $prevData->IsDemo;
			}
			
			$params = Array( ':login', ':srvMt4', ':i_firma', ':i_fio', ':time_last_update', ':sGmt', ':i_dr', ':stat', ':last_logMt4' );
			$params = array_combine( $params, $params );
			foreach( $params as &$param ) {
				$param = str_replace( ':', '', $param );
				$param = @$assocACC[ $param ];
			}
			$params[':sGmt'] = $params[':sGmt'] ? $params[':sGmt'] : 0;
			$params[':i_server'] = $server->internalName;
			$params[':sameErrorCount'] = $sameErrorCount;
			if( !$params[':i_firma']) $params[':i_firma'] = $server->broker->officialName;
			if( !$params[':i_fio']) $params[':i_fio'] = '';
			Yii::App()->db->createCommand( $query )->query( $params );
			
			/*
				удаление из мониторинга счетов с ошибкой -255
			*/
			if( $assocACC['stat'] == -255 && $sameErrorCount > 4 ){
				$url = 'http://159.69.219.218:18466/master.api?cmd=del_acc&aid='.$assocACC['aid'].'&login='.$assocACC['login'];
				$deleteResult = CommonLib::downloadFileFromWww( $url );
				if( !$deleteResult ) echo "<br />Empty delete result, please contact admin<br />" ;
				
				preg_match_all('/aid\:\s*'.$assocACC['aid'].'\s*login\:\s*'.$assocACC['login'].'\s*delete/',
					$deleteResult,
					$found,
					PREG_PATTERN_ORDER
				);
				if( !isset($found[0][0]) || !$found[0][0] ) 
					echo "<br />Wrong delete response, please contact admin<br />" ;
			}
		}
		
			/*
				Функция обновления данных об ошибках счета в таблице ft_contest_member
			*/
		static function update_ft_contest_member( $assocACC, $server ) {
			if( $assocACC[ 'stat' ] ==-255) {$assocACC[ 'status' ] = "invalid password";}
			if( $assocACC[ 'stat' ] ==0) {$assocACC[ 'status' ] = "connect";}
			$assocACC[ 'ServerId' ] = $server->id;	
			$query = "
				UPDATE `ft_contest_member`
				SET
					`statusconnect`    = :status
				WHERE 
					`accountNumber`   = :login
				AND
					`idServer`  = :ServerId
			";
			$params = Array( ':status', ':login', ':ServerId' );
			$params = array_combine( $params, $params );
			foreach( $params as &$param ) {
				$param = str_replace( ':', '', $param );
				$param = @$assocACC[ $param ];
			}
			Yii::App()->db->createCommand( $query )->query( $params );
		}
		
		
			/*
				Функция импорта данных из секции OPEN в таблицу mt5_account_trades
			*/
		private function importMT5AccountInfoTrades( $assocACC, $arrayOPEN, $server ) {
			$assocACC[ 'ServerId' ] = $server->id;
			if ( !isset($assocACC['s_newrev']) or $assocACC['s_newrev'] != 1) {
				// удаляем старые записи
				$query = "
					DELETE FROM `mt5_account_trades`
					WHERE `ACCOUNT_NUMBER` = :login 
				";
				Yii::App()->db->createCommand( $query )->query( Array(
					':login' => $assocACC['login'],
				));
			}
			$affectedRows = 0;
			foreach( $arrayOPEN as $row ) {
				$assocM = array_merge( $assocACC, $row );
				$query = "
					INSERT INTO `mt5_account_trades`
					SET
						`ACCOUNT_NUMBER`  = :login,
						`AccountServerId` = :ServerId,
						`DEAL_TIME`       = :OpTim,
						`DEAL_TICKET`     = :Tick,
						`SYMBOL`          = :Sym,
						`DEAL_TYPE`       = :Type,
						`DEAL_VOLUME`     = :Lots,
						`DEAL_PRICE`      = :OPrice,
						`DEAL_SL`         = :SL,
						`DEAL_TP`         = :TP,
						`DEAL_SWAP`       = :Swap,
						`DEAL_COMMISSION` = :Commis,
						`DEAL_PROFIT`     = :Prof
				";
				$params = Array( ':login', ':ServerId', ':OpTim', ':Tick', ':Sym', ':Type', ':Lots', ':OPrice', ':SL', ':TP', ':Swap', ':Commis', ':Prof' );
				$params = array_combine( $params, $params );
				foreach( $params as &$param ) {
					$param = str_replace( ':', '', $param );
					if ( !isset($assocM[ $param ]) ) {
						$param = 0;
					}else{
						$param = @$assocM[ $param ];
					}
				}

				$insertResult = Yii::App()->db->createCommand( $query )->query( $params );
				$affectedRows +=$insertResult->rowCount;
			}
			$this->affectedRowsOPEN = $affectedRows;
		}
		
		
		private function importMT5AccountInfoTradesNewrev( $assocACC, $arrayOPEN, $server, $s_newrev ) {
			$assocACC[ 'ServerId' ] = $server->id;
			if ( $s_newrev ) {
				// удаляем старые записи
				$query = "
					DELETE FROM `mt5_account_trades`
					WHERE `ACCOUNT_NUMBER` = :login and `AccountServerId` = :ServerId
				";
				Yii::App()->db->createCommand( $query )->query( Array(
					':login' => $assocACC['login'],
					':ServerId' => $assocACC[ 'ServerId' ],
				));
			}
			$affectedRows = 0;
			foreach( $arrayOPEN as $row ) {
				$assocM = array_merge( $assocACC, $row );
				$query = "
					INSERT INTO `mt5_account_trades`
					SET
						`ACCOUNT_NUMBER`  = :login,
						`AccountServerId` = :ServerId,
						`DEAL_TIME`       = :OpTim,
						`DEAL_TICKET`     = :Tick,
						`SYMBOL`          = :Sym,
						`DEAL_TYPE`       = :Type,
						`DEAL_VOLUME`     = :Lots,
						`DEAL_PRICE`      = :OPrice,
						`DEAL_SL`         = :SL,
						`DEAL_TP`         = :TP,
						`DEAL_SWAP`       = :Swap,
						`DEAL_COMMISSION` = :Commis,
						`DEAL_PROFIT`     = :Prof
				";
				$params = Array( ':login', ':ServerId', ':OpTim', ':Tick', ':Sym', ':Type', ':Lots', ':OPrice', ':SL', ':TP', ':Swap', ':Commis', ':Prof' );
				$params = array_combine( $params, $params );
				foreach( $params as &$param ) {
					$param = str_replace( ':', '', $param );
					if ( !isset($assocM[ $param ]) ) {
						$param = 0;
					}else{
						$param = @$assocM[ $param ];
					}
				}
				$insertResult = Yii::App()->db->createCommand( $query )->query( $params );
				$affectedRows +=$insertResult->rowCount;
			}
			$this->affectedRowsOPEN = $affectedRows;
		}
		
			/*
				Функция импорта данных из секции HIST в таблицу mt4_account_history
			*/
		private function importMT4AccountHistoryNewrev( $assocACC, $arrayHIST, $server, $s_newrev ) {
			if( !$server ) return;
			
			if ( $s_newrev ) {
				// удаляем старые записи
				$query = "
					DELETE FROM `mt4_account_history`
					WHERE `AccountNumber` = :login and `AccountServerId` = :ServerId
				";
				Yii::App()->db->createCommand( $query )->query( Array(
					':login' => $assocACC['login'],
					':ServerId' => $server->id,
				));
			}
			
			$affectedRows = 0;
			foreach( $arrayHIST as $row ) {
				$assocM = array_merge( $assocACC, $row );
				$assocM[ 'ServerId' ] = $server->id;
				$query = "
					INSERT IGNORE INTO `mt4_account_history`
					SET 
						`AccountNumber`    = :login,
						`AccountServerId`  = :ServerId,
						`OrderTicket`      = :Tick,
						`OrderType`        = :Type,
						`OrderOpenTime`    = :OpTim,
						`OrderCloseTime`   = :ClTim,
						`OrderExpiration`  = :Expir,
						`OrderMagicNumber` = :Magic,
						`OrderLots`        = :Lots,
						`OrderSymbol`      = :Sym,
						`OrderOpenPrice`   = :OPrice,
						`OrderStopLoss`    = :SL,
						`OrderTakeProfit`  = :TP,
						`OrderClosePrice`  = :CPrice,
						`OrderSwap`        = :Swap,
						`OrderCommission`  = :Commis,
						`OrderProfit`      = :Prof,
						`OrderComment`     = :Com
				";
				$params = Array( ':login', ':ServerId', ':Tick', ':Type', ':OpTim', ':ClTim', ':Expir', ':Magic', ':Lots', ':Sym', ':OPrice', ':SL', ':TP', ':CPrice', ':Swap', ':Commis', ':Prof', ':Com' );
				$params = array_combine( $params, $params );
				foreach( $params as &$param ) {
					$param = str_replace( ':', '', $param );
					$param = @$assocM[ $param ];
				}
				$insertResult = Yii::App()->db->createCommand( $query )->query( $params );
				$affectedRows +=$insertResult->rowCount;
			}
			$this->affectedRowsHIST = $affectedRows;
		}
		
		
		private function importMT4AccountHistory( $assocACC, $arrayHIST, $server ) {
			if( !$server ) return;
			
			$affectedRows = 0;
			foreach( $arrayHIST as $row ) {
				$assocM = array_merge( $assocACC, $row );
				$assocM[ 'ServerId' ] = $server->id;
				$query = "
					INSERT IGNORE INTO `mt4_account_history`
					SET 
						`AccountNumber`    = :login,
						`AccountServerId`  = :ServerId,
						`OrderTicket`      = :Tick,
						`OrderType`        = :Type,
						`OrderOpenTime`    = :OpTim,
						`OrderCloseTime`   = :ClTim,
						`OrderExpiration`  = :Expir,
						`OrderMagicNumber` = :Magic,
						`OrderLots`        = :Lots,
						`OrderSymbol`      = :Sym,
						`OrderOpenPrice`   = :OPrice,
						`OrderStopLoss`    = :SL,
						`OrderTakeProfit`  = :TP,
						`OrderClosePrice`  = :CPrice,
						`OrderSwap`        = :Swap,
						`OrderCommission`  = :Commis,
						`OrderProfit`      = :Prof,
						`OrderComment`     = :Com
				";
				$params = Array( ':login', ':ServerId', ':Tick', ':Type', ':OpTim', ':ClTim', ':Expir', ':Magic', ':Lots', ':Sym', ':OPrice', ':SL', ':TP', ':CPrice', ':Swap', ':Commis', ':Prof', ':Com' );
				$params = array_combine( $params, $params );
				foreach( $params as &$param ) {
					$param = str_replace( ':', '', $param );
					$param = @$assocM[ $param ];
				}
				$insertResult = Yii::App()->db->createCommand( $query )->query( $params );
				$affectedRows +=$insertResult->rowCount;
			}
			$this->affectedRowsHIST = $affectedRows;
		}
		
			/*
				Функция сохранения логов
			*/
		static function saveLog( $assocACC, $post ) {
			$settings = SettingsModel::getModel();
				// расширенный лог
			if( $settings->common_logImportAccountData == 'Extended' ) {
				$date = date( "Y.m.d" );
				$time = date( "H-i-s" );
				
				$dir = self::pathSave."/extended/{$date}";
				is_dir( $dir ) or mkdir( $dir );
				
				$dir .= "/{$assocACC['login']}";
				is_dir( $dir ) or mkdir( $dir );
				
				$file = "{$dir}/{$time}.txt";
				file_put_contents( $file, $post );
			}
				// простой лог
			if( $settings->common_logImportAccountData == 'Short' ) {
				$dir = self::pathSave."/short";
				$file = "{$dir}/{$assocACC['login']}.txt";
				file_put_contents( $file, $post );
			}
		}
		
		
			/*
				Основная функция компонента. Точка входа.
			*/
		public function import() {
			
				// проверяем настройки
			self::checkSettings();
			
				// получаем POST
			$post = file_get_contents( "php://input" );
			//$post = file_get_contents( 'http://fortrader.org/services/log/importAccountData/short/500679.txt' );
			
	
				// конвертируем POST windows-1251 -> utf-8
			$post = iconv( 'windows-1251', 'utf-8', $post );
				
				// если пустой POST - выходим
			if( !strlen( $post )) return;

				// разбираем POST на части ACC, OPEN, HIST
			$ACC = self::getPart( $post, "ACC" );
			$OPEN = self::getPart( $post, "OPEN" );
			$HIST = self::getPart( $post, "HIST" );
			$STATS = self::getPart( $post, "STATISTICS" );
						
				// парсим части на массивы
			$assocACC = self::parseRow( $ACC );
			$arrayOPEN = self::parseRows( $OPEN );
			$arrayHIST = self::parseRows( $HIST );
			$arraySTATS = self::parseStats( $STATS );

			echo '<pre>$assocACC';
			print_r($assocACC);
			echo '</pre>';
			echo '<hr/>';
			echo '<pre>$arrayOPEN';
			print_r($arrayOPEN);
			echo '</pre>';
			echo '<hr/>';
			echo '<pre>$arrayHIST';
			print_r($arrayHIST);
			echo '</pre>';
			echo '<hr/>';
			echo '<pre>$arraySTATS';
			print_r($arraySTATS);
			echo '</pre>';
			echo '<hr/>';
			//die;
				
				// если пустой login в ACC - выходим
			if( empty( $assocACC[ 'login' ])) return;

			$s_newrev=0;
	
			if ( isset($assocACC['s_newrev']) and $assocACC['s_newrev'] == 1) {
				$s_newrev=1;	
			}
			$assocACC['revision'] = $s_newrev;

			echo 1;
				// сохраняем POST в лог
			self::saveLog( $assocACC, $post );
			
			echo 2;
				// ищем сервер
			$server = self::getServer( $assocACC );
			
			echo 3;
			
				// ищем объект мониторинга
			$model = self::getModel( $assocACC );
			
			if( $model instanceof ContestMemberModel && $model->idContest != 0 ){
				$contestModel = ContestModel::model()->findByPk( $model->idContest );
				
				if( $contestModel->stopImport ) return;
				if( strtotime($contestModel->end) + 60*60*24-1 < time() ){
					if( $model->stopMonitor ) return;
					$model->stopMonitor = 1;
					$model->save();
				}
			}
			
			
			echo 4;
				// обновляем имя сервера (если надо)
			self::updateServer( $assocACC, $server );
			
			/*echo 5;
				// если объект мониторинга - участник конкурса и он имеет статус completed - выходим
			if( $model instanceof ContestMemberModel and $model->status == 'completed' ) return;*/
			
			echo 6;
				// определяем stat
			$stat = (int)@$assocACC[ 'stat' ];
			
			echo 7;
			
			if( !isset($assocACC['__pack']) || !$assocACC['__pack'] ){
				$assocACC['__pack'] = 1;
			}
			// удаляем и вставляем если новая ревизия и $assocACC['__pack'] == 1 потом выход
			if( isset($assocACC['__pack']) && $assocACC['__pack'] == 1 && $s_newrev ) {
				$this->importMT5AccountInfoTradesNewrev( $assocACC, $arrayOPEN, $server, $s_newrev );
				$this->importMT4AccountHistoryNewrev( $assocACC, $arrayHIST, $server, $s_newrev );
				return;
			}
			
			//если __pack > 1( второй и следующий пакет ) импорт только HIST и выход
			if ( isset($assocACC['__pack']) and $assocACC['__pack'] > 1) {
				
				//if( $stat >= 0 && !$s_newrev ) {
				$this->importMT4AccountHistory( $assocACC, $arrayHIST, $server );
				//}
				
				if( (isset($contestModel) && $contestModel && !$contestModel->stopDeleteMMonitoring ) || ( $model instanceof ContestMemberModel && $model->idContest == 0 ) ){
					$histUnique = array();
					foreach($arrayHIST as $hist){
						$histUnique[]=$hist['Tick'];
					}
					$this->affectedRowsHIST = count( array_unique($histUnique) );
					$this->importMT4AccountMonitor( $assocACC['login'], $server->id, $assocACC['i_bal'], $assocACC['i_equi'], 1 );
				}
				return;
				
			}
			
				// если stat >= 0 - импорт в mt5_account_data
			if( $stat >= 0 ) {
				self::importMT5AccountData( $assocACC, $server, $arraySTATS );
			}
			
			echo 8;
				// импорт в mt5_account_info
			self::importMT5AccountInfo( $assocACC, $server );
			
			echo 9;
				// если stat >= 0 - импорт в mt5_account_trades и mt4_account_history
			if( $stat >= 0 && !$s_newrev ) {
				$this->importMT5AccountInfoTrades( $assocACC, $arrayOPEN, $server );
				$this->importMT4AccountHistory( $assocACC, $arrayHIST, $server );
				if( (isset($contestModel) && $contestModel && !$contestModel->stopDeleteMMonitoring ) || ( $model instanceof ContestMemberModel && $model->idContest == 0 ) ) $this->importMT4AccountMonitor( $assocACC['login'], $server->id, $assocACC['i_bal'], $assocACC['i_equi'], 0 );
			}
			
			echo 10;
			
			/*if( $s_newrev ) {
				$this->importMT5AccountInfoTradesNewrev( $assocACC, $arrayOPEN, $server, $s_newrev );
				$this->importMT4AccountHistoryNewrev( $assocACC, $arrayHIST, $server, $s_newrev );
				if(!$contestModel->stopDeleteMMonitoring)$this->importMT4AccountMonitor( $assocACC['login'], $server->id, $assocACC['i_bal'], $assocACC['i_equi'], 1 );
			}*/
			
			echo 11;
				// обновляем состояние коннекта счета в ft_contest_member
			//self::update_ft_contest_member( $assocACC, $server );
			
			echo 12;
		}
		private function importMT4AccountMonitor( $accountNumber, $serverId, $balance, $equity, $packMarker ){
					
			$lastData = MT4AccountMonitorModel::getLastData( $accountNumber, $serverId );
					
			if($lastData){
				$comment = 'update';
			}
			if($packMarker&&$lastData){
				$comment = 'history';
				$this->affectedRowsOPEN =0;
				$balance=0;
				$equity=0;
			}
			if(!$lastData){
				$comment = 'start';
			}

			if(
				$lastData && 
				$lastData->comment == $comment && 
				$lastData->accountHistory == $this->affectedRowsHIST && 
				$lastData->openDeals == $this->affectedRowsOPEN && 
				$lastData->balance == intval($balance) && 
				$lastData->equity == intval($equity) 
			){
				return false;
			}
			
			$model = new MT4AccountMonitorModel;
			$model->accountNumber = $accountNumber;
			$model->accountServerId = $serverId;
			$model->comment = $comment;
			$model->accountHistory = $this->affectedRowsHIST;
			$model->openDeals = $this->affectedRowsOPEN;
			$model->balance = $balance;
			$model->equity = $equity;
			$model->save();
			
		}
	}
	
?>