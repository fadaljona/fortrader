<?
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'extensions.ClickatellXMLAPI' );
	
	final class ClickatellComponent extends ComponentBase {
		public $sessionStateName = "clickatellSession";
		function instanceAPI() {
			static $api;
			if( !$api ) {
				$api_id = SettingsModel::getModel()->clickatell_api_id;
				if( !strlen( $api_id )) throw new Exception( 'api_id not set in Settings project!' );
				
				$user = SettingsModel::getModel()->clickatell_user;
				if( !strlen( $user )) throw new Exception( 'user not set in Settings project!' );
				
				$password = SettingsModel::getModel()->clickatell_password;
				if( !strlen( $password )) throw new Exception( 'password not set in Settings project!' );
				$password = ModelBase::decrypt( $password );
				
				$api = new ClickatellXMLAPI();
				$api->UTF8 = 1;
				$idSession = Yii::App()->user->getState( $this->sessionStateName );
				$idSession = $api->pingOrAuth( $idSession, $api_id, $user, $password );
				Yii::App()->user->setState( $this->sessionStateName, $idSession );
			}
			return $api;
		}
	}
	
?>