<?php

class ShortAccountDataComponent
{
    private function log($text){
        echo $text."\n";
        flush();
    }
    
    public function run()
    {
        //$this->log('start');
        set_time_limit(0);

        $table_name = 'mt5_account_data';

        $today = date('Y-m-d');
        $for_delete = array();
        
        //$this->log('today: '.$today);
        
        $db = Yii::App()->db;

        $command = $db->createCommand("SELECT * FROM $table_name GROUP BY `ACCOUNT_NUMBER`, `AccountServerId`");

        $rows = $command->queryAll();
        //$this->log('found '.(count($rows)).' rows for processing');
        
        foreach ($rows as $row) {
            $account_number = $row['ACCOUNT_NUMBER'];
            $account_server_id = $row['AccountServerId'];
            
            //$this->log('start new row');

            $sql = "SELECT * FROM `$table_name` WHERE `ACCOUNT_NUMBER` = \"$account_number\" AND `AccountServerId` = \"$account_server_id\"";
            
            //$this->log($sql);
            
            $command = $db->createCommand($sql);
            $result = $command->queryAll();
            
            if(!$result)continue;

            //$this->log($account_number.'-'.$account_server_id.' '.count($result).' rows founded');
            
            $min_id = null;
            $dates_result = array();
            foreach ($result as $result_row) {
                $date = substr($result_row['timestamp'], 0, 10);

                if ($date == $today) {
                    continue;
                }

                $row_amount = $result_row['ACCOUNT_EQUITY'];

                if (!isset($dates_result[$date])) {
                    $dates_result[$date] = array(
                        'min' => $row_amount,
                        'max' => $row_amount,
                        'rows' => array()
                    );
                } else {
                    $dates_result[$date]['min'] = min($dates_result[$date]['min'], $row_amount);
                    $dates_result[$date]['max'] = max($dates_result[$date]['max'], $row_amount);
                }
                $dates_result[$date]['rows'][] = $result_row;

                $min_id = $min_id == null ? $result_row['id'] : min($min_id, $result_row['id']);
            }

            //$this->log('found '.count($dates_result).' dates');
            
            foreach ($dates_result as $date => $date_result) {
                
                //$this->log($date.': '.count($date_result['rows']).' ('.$date_result['min'].','.$date_result['max'].')');
                
                if (count($date_result['rows']) <= 2) {
                    continue;
                }

                foreach ($date_result['rows'] as $date_row) {
                    $row_amount = $date_row['ACCOUNT_EQUITY'];
                    if($date_row['id'] != $min_id && $row_amount > $date_result['min'] && $row_amount < $date_result['max']){
                        $for_delete[] = $date_row['id'];
                    }
                }
            }
        }
        
        //$this->log('finish processing, next deleting');

        //$this->log('for delete found '.count($for_delete).' rows');
        
        if(empty($for_delete)){
            return;
        }

        foreach($for_delete as $id){
            $db->createCommand("DELETE FROM `$table_name` WHERE `id` = $id")->execute();
        }

        return;
    }
}
