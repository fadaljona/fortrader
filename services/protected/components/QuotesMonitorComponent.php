<?
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'components.MailerComponent' );
		
	final class QuotesMonitorComponent extends ComponentBase {
		private $factoryMailer;
		private $path;
		private $timeZone;
		public $timeToWatch;
		private $periods = array(
			'MN1' => 43200,
			'W1' => 10080,
			'D1' => 1440,
			'H4' => 240,
			'H1' => 60,
			'30M' => 30,
			'15M' => 15,
			'5M' => 5,
			'1M' => 1,
		);
		
		function __construct() {
			$quotesSettings = QuotesSettingsModel::getInstance();
			$this->timeToWatch = 60*60*24;
			$this->timeZone = $quotesSettings->statsDataTimeZone;
			date_default_timezone_set( $this->timeZone );
			$this->factoryMailer = new MailerComponent();
			$this->path = Yii::getPathOfAlias('webroot') .'/..'. $quotesSettings->phpServersPath . '/log';
		}
		
		private function getQuotesHistoryServerAppStats(){
			$file = file( $this->path . '/quotes-history-server-application.log' );
			if( !count($file) ) return false;
			
			$outArr = array();
			foreach( array_reverse($file) as $str ){
				$str = trim( $str );
				$exploded = explode('] ', $str);
				
				$date = str_replace('[', '', $exploded[0]);
				if( strtotime( $date ) < (time() - $this->timeToWatch ) ) break;
				
				$expMsg = explode( ' [url=', $exploded[1] );
				$url = str_replace( ']', '', $expMsg[1] );
				
				if( isset( $outArr[$expMsg[0]] ) ){
					$outArr[$expMsg[0]]['count'] += 1;
				}else{
					$dataDate = '';
					if( $exploded[2] ){
						$dataDate = str_replace( array('[dataDate=', ']'), '', $exploded[2] );
					}
					$outArr[$expMsg[0]] = array(
						'count' => 1,
						'date' => $date,
						'url' => $url,
						'dataDate' => $dataDate
					);
				}
			}
			ksort( $outArr );
			if( !count($outArr) ) return false;
			
			return $outArr;
		}
		private function getQuotesServerAppStats(){
			$file = file( $this->path . '/quotes-server-application.log' );
			if( !count($file) ) return false;
			
			$lastDate = '';
			$count = 0;
			foreach( array_reverse($file) as $str ){
				$str = trim( $str );
				$exploded = explode('] ', $str);
				$date = str_replace('[', '', $exploded[0]);
				if( strtotime( $date ) < (time() - $this->timeToWatch ) ) break;
				
				if( $lastDate != '' ){
					$count += 1;
				}else{
					$lastDate = $date;
					$count = 1;
				}
			}
	
			if( $count == 0 ) return false;
			return array(
				'count' => $count,
				'date' => $lastDate
			);
		}


		private function send( ) {
			$quotesHistoryServerAppStats = $this->getQuotesHistoryServerAppStats();
			$quotesServerAppStats = $this->getQuotesServerAppStats();
			$quotesSettings = QuotesSettingsModel::getInstance();
			
			$subject = "Quotes app log stats for " . date('Y-m-d H:i:s', time() - $this->timeToWatch) . " - " . date('Y-m-d H:i:s');
			
			$MsgHTML = '<h2>time zone '.$this->timeZone.'</h2><hr />';
			
			if( $quotesServerAppStats ){
				$MsgHTML .= '<h2>Tick Quotes</h2>';
				$MsgHTML .= 'strs ' . $quotesServerAppStats['count'] . '<br />';
				$MsgHTML .= 'last str date ' . $quotesServerAppStats['date'] . '<br /><hr />';
			}
			if($quotesHistoryServerAppStats ){
				
				$countRows = 0;
				$MsgHTML .= '<h2>History Quotes</h2>';
				$MsgHTML .= '<table><tr><td style="padding:0 5px;">MSG</td><td style="padding:0 5px;">LAST DATA DATE</td><td style="padding:0 5px;">COUNT</td><td style="padding:0 5px;">LAST DATE</td></tr>';
				foreach( $quotesHistoryServerAppStats as $msg => $data ){
					/*if( strpos($msg, 'no update. 1M' ) && $data['count'] < 1000 ){
						continue;
					}else*/
					/*if( !strpos($msg, 'no update. 1M' ) && strpos($msg, 'no update.' ) && $data['count'] < 250 ){
						continue;
					}*/
					
					if( strpos($msg, 'changed alias to' ) !== false ) continue;
					if( strpos($msg, 'no data' ) !== false ){
						
						if( isset( $data['count'] ) && $data['count'] && $data['count'] < 30 ) continue;
						
						$quoteName = trim(substr($msg,  strrpos( $msg, ' ' ) ));
						
						if( $quoteName && isset( $data['url'] ) && $data['url'] ){
							$symbolInUrl = str_replace( $quotesSettings->statsUrlData, '', $data['url'] );
							$symbolInUrl = substr($symbolInUrl,  0, strpos( $symbolInUrl, '&' ) );
							
							$symbolModel = QuotesSymbolsModel::findByName($quoteName);
							
							if( $symbolModel ){
								if( $symbolModel->histName != $symbolInUrl ) continue;
							}
						}
					}
					if( strpos($msg, 'no update' ) !== false ){
						$period = str_replace('WARNING - no update. ', '', $msg);
						$period = trim( substr($period,  0, strpos( $period, '.' ) ) );
						if( $period && isset( $this->periods[$period] ) && isset( $data['dataDate'] ) && $data['dataDate'] ){
	
							$dataTime = strtotime( $data['dataDate'] );

							if( $period == '15M' && $dataTime > time() - 60*60*24 ) continue;
							if( $period == '30M' && $dataTime > time() - 60*60*24 ) continue;
							if( $period == 'H1' && $dataTime > time() - 60*60*24 ) continue;
							if( $period == 'H4' && $dataTime > time() - 60*60*24 ) continue;
							if( $period == 'D1' && $dataTime > time() - 60*60*36 ) continue;
							if( $period == 'W1' && $dataTime > strtotime('monday this week') - 60*60*24*2 ) continue;
							if( $period == 'MN1' && $dataTime > strtotime('first day of this month') - 60*60*24*2 ) continue;
							
						}
					}
					
					$countRows ++;
					if( isset( $data['url'] ) && $data['url'] ){
						$msg = $msg . ' <span style="font-size:8px;">[' . $data['url'] . ']</span>';
					}
					$MsgHTML .= '<tr><td>'.$msg.'</td><td>'.$data['dataDate'].'</td><td align="center">'.$data['count'].'</td><td>'.$data['date'].'</td></tr>';
				}
				$MsgHTML .= '</table><hr />';
			}
			if( !$countRows && !$quotesServerAppStats ) return false;
			
			//echo $MsgHTML;
			
			$mailer = $this->factoryMailer->instanceMailer();
			
			$quotesSettings = QuotesSettingsModel::getInstance();
			
			foreach( explode( ",", $quotesSettings->notificationEmails ) as $email ) {
				$email = trim( $email );
				$mailer->addAddress( $email );
			}
			
			$mailer->Subject = $subject;
			$mailer->MsgHTML( $MsgHTML );

			$mailer->send();
		}

		function monitor() {	
			$this->send();
		}
	}
			
?>