<?

	final class PushNotificationComponent extends ComponentExBase {

        private function getJournalWithNotificationsCriteria() {
            $c = new CDbCriteria();
            $c->with[ 'i18ns' ] = Array();
            $c->with[ 'currentLanguageI18N' ] = Array();
            $c->addCondition('`t`.`push_notification` = \'1\'');
            $c->addCondition('`t`.`status` IS NULL OR `t`.`status` = \'0\'');
            $c->order = " `t`.`date` DESC, `t`.`id` DESC ";
            return $c;
        }

        public function cleanTokens(){
            $result = 0;
            $apns = Yii::app()->apns;

            $env = $apns->environment == 'production' ? ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION : ApnsPHP_Abstract::ENVIRONMENT_SANDBOX;
            $apnsFeedback = new ApnsPHP_Feedback($env, $apns->pemFile);
            $apnsFeedback->connect();
            $outdatedTokens = $apnsFeedback->receive();
            $apnsFeedback->disconnect();

            if(count($outdatedTokens) > 0) {
                $criteria = new CDbCriteria;
                $criteria->addInCondition('token', $outdatedTokens);
                $result = ApnsDevices::model()->deleteAll($criteria);
            }
            return $result;
        }

        public function push( $idJournal, $message = 'New journal available', $limit = 100)
        {
            if(!isset($idJournal)){
                throw new CException('Journal ID is required.');
            }

            $apns = Yii::app()->apns;
            $page = 0;

            do {
                $tokens = CHtml::listData(ApnsDevices::model()->findAll(array(
                    'order' => 'createdDT',
                    'offset' => $page*$limit,
                    'limit' => $limit
                )), 'id', 'token');
                $page++;

                if(count($tokens) > 0){
                    $apns->sendMulti($tokens, $message,
                        array(
                            'idJournal' => $idJournal,
                        ),
                        array(
                            'ContentAvailable' => true,
                            'sound' => 'default'
                        )
                    );
                }
            } while(count($tokens) > 0);
        }

        public function sendNotifications() {
            $this->cleanTokens();

            $limit = SettingsModel::getModel()->common_numberOfNotifications;
            $c = $this->getJournalWithNotificationsCriteria();
            $journals = JournalModel::model()->findAll( $c );
            foreach($journals as $key => $journal){
                $message = 'New journal available';
                $idJournal= 'ru.fortrader.services.journal.ru.'.$journal->id;
                echo "<br>".$idJournal;
                $this->push($idJournal, $message, $limit);
            }
        }
	}
?>
