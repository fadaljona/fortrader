<?
	Yii::import( 'components.base.ComponentBase' );
	Yii::import( 'components.MailerComponent' );
		
	final class ContestMonitorComponent extends ComponentBase {
		private $factoryMailer;
		function __construct() {
			$this->factoryMailer = new MailerComponent();
		}
		private function getMailTpl() {
			$key = 'monitoring stopping message';
			$mailTpl = MailTplModel::model()->findByAttributes( Array( 'key' => $key ));
			return $mailTpl;
		}
		private function renderSubject( $subject, $contest ) {
			$subject = strtr( $subject, Array(
				'{name}' => $contest->name,
			));
			return $subject;
		}
		private function renderMessage( $message, $contest ) {
			$message = strtr( $message, Array(
				'{link}' => CHtml::link( $contest->name, $contest->getAbsoluteSingleURL() ),
			));
			return $message;
		}
		private function send( $contest ) {
			$settings = ContestSettingsModel::getModel();
			$mailer = $this->factoryMailer->instanceMailer();
			
			foreach( explode( ",", $settings->emails ) as $email ) {
				$email = trim( $email );
				$mailer->addAddress( $email );
			}
			
			$mailTpl = $this->getMailTpl();
			
			$mailer->Subject = $this->renderSubject( $mailTpl->subject, $contest );
			$mailer->MsgHTML( $this->renderMessage( $mailTpl->message, $contest ));

			$mailer->send();
		}
		function getContests() {
			$settings = ContestSettingsModel::getModel();
			$contests = ContestModel::model()->findAll(Array(
				'condition' => "
					`t`.`status` IN ( 'registration', 'started' )
					AND `t`.`begin` <= CURDATE()
					AND `t`.`end` >= CURDATE()
					AND DATE_ADD( `t`.`updatedStats`, INTERVAL :timeToSend MINUTE ) < NOW()
				",
				'params' => Array(
					':timeToSend' => $settings->timeToSend,
				),
			));
			return $contests;
		}
		function monitor() {
			$settings = ContestSettingsModel::getModel();
			if( $settings->sendNotices ) {
					# d1
				$contests = $this->getContests();
				if( $contests ) {
						# d2
					$contests = $this->getContests();
					foreach( $contests as $contest ) {
						$monitor = ContestMonitorModel::model()->findByAttributes( Array( 'idContest' => $contest->id ));
						if( !$monitor ) {
							$this->send( $contest );
							
							$monitor = ContestMonitorModel::instance( $contest->id );
							$monitor->save();
						}
					}
				}
					# clean
				$c = new CDbCriteria();
				if( $contests ) {
					$IDsContests = CommonLib::slice( $contests, 'id' );
					$c->addNotInCondition( 'idContest', $IDsContests );
				}
				$monitors = ContestMonitorModel::model()->findAll( $c );
				foreach( $monitors as $monitor ) {
					$monitor->delete();
				}
			}
		}
	}
			
?>