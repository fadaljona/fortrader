<?php

Yii::import('components.base.ComponentBase');

class AssignOurSymbolToCryptoCurrenciesExchangesSymbolComponent extends ComponentBase
{
    public function run()
    {
        $exchanges = CryptoCurrenciesExchangesModel::model()->findAll();

        foreach ($exchanges as $exchange) {
            echo "\n\n" . $exchange->title . "\n\n";

            $exchangeSymbolsToAssign = CryptoCurrenciesExchangesSymbolToOurSymbolModel::model()->findAll([
                'condition' => ' `t`.`idExchange` = :idExchange AND (`t`.`symbolId` IS NULL OR `t`.`symbolId` = 0) ',
                'params' => [':idExchange' => $exchange->id]
            ]);

            $exchangeSymbolsToExclude = CryptoCurrenciesExchangesSymbolToOurSymbolModel::model()->findAll([
                'condition' => ' `t`.`idExchange` = :idExchange AND `t`.`symbolId` IS NOT NULL AND `t`.`symbolId` <> 0 ',
                'params' => [':idExchange' => $exchange->id]
            ]);

            $excludeSymbols = [];
            foreach ($exchangeSymbolsToExclude as $s) {
                $excludeSymbols[] = $s->symbolId;
            }

            $addNotIn = '';
            if ($excludeSymbols) {
                $addNotIn = " AND `t`.`id` NOT IN(" . implode(',', $excludeSymbols) . ")";
            }

            $avaliableSymbols = QuotesSymbolsModel::model()->findAll(array(
                'with' => array('currentLanguageI18N'),
                'condition' => " `cLI18NQuotesSymbolsModel`.`nalias` IS NOT NULL AND `cLI18NQuotesSymbolsModel`.`nalias` <> '' AND `t`.`toCryptoCurrenciesPage` = 1" . $addNotIn,
            ));

            $this->assignSymbols($exchangeSymbolsToAssign, $avaliableSymbols);
        }
    }

    private function assignSymbols($exchangeSymbolsToAssign, $avaliableSymbols)
    {
        foreach ($exchangeSymbolsToAssign as $exchangeSymbol) {
            foreach ($avaliableSymbols as $id => $avaliableSymbol) {
                similar_text($exchangeSymbol->symbol, $avaliableSymbol->cryptoCurrencyTitle, $perc);
                if ($perc >= 90) {
                    echo "found\t $exchangeSymbol->symbol\tto\t$avaliableSymbol->cryptoCurrencyTitle\n";

                    $exchangeSymbol->symbolId = $avaliableSymbol->id;
                    $exchangeSymbol->save();
                    
                    unset($avaliableSymbols[$id]);
                }
            }
        }
    }
}
