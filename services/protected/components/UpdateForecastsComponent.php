<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class UpdateForecastsComponent extends ComponentBase {
		private $forecastsToUpdate;
		private $currDate;
		private $timezone = 'UTC';
		
		function __construct() {
			date_default_timezone_set( $this->timezone );
			$this->currDate = date( 'Y-m-d H:i:s', time() );
			$this->setForecasts();
		}
		private function setForecasts(){
			$this->forecastsToUpdate = QuotesForecastsModel::model()->findAll(Array(
				'condition' => " `t`.`status` = 0 AND `t`.`endDate` < :endDate ",
				'params' => array(':endDate' => $this->currDate),
			));
		}
		private function updateOneForecast( $forecast ){
			$hasData = QuotesHistoryDataModel::model()->find(Array(
				'condition' => " `t`.`symbolId` = :symbolId AND `t`.`barTime` >= :barTime AND `t`.`resolution` = 15 ",
				'params' => array(':symbolId' => $forecast->symbolId, ':barTime' => strtotime($forecast->endDate) ),
			));
			if( !$hasData ) return false;

			$confirmed = QuotesHistoryDataModel::model()->find(Array(
				'condition' => " `t`.`symbolId` = :symbolId AND `t`.`barTime` >= :startDate AND `t`.`barTime` <= :endDate AND `t`.`high` >= :value AND `t`.`low` <= :value ",
				'params' => array(':symbolId' => $forecast->symbolId, ':startDate' => strtotime($forecast->startDate), ':endDate' => strtotime($forecast->endDate), ':value' => $forecast->value),
			));
			if( $confirmed ){
				$forecast->status=1;
			}else{
				$forecast->status=-1;
			}
			if( $forecast->save() ){
				echo $forecast->id ." => updated\n<br />";
			}
		}
		private function updateAllForecasts(){
			$usersArr = array();
			foreach( $this->forecastsToUpdate as $forecast ){
				$this->updateOneForecast($forecast);
				$usersArr[] = $forecast->userId;
			}
			foreach( $usersArr as $userId ){
				UserKarmaModel::updateKarma($userId);
			}
		}
		
		function update() {
			
			$this->updateAllForecasts();
		}
	}
			
?>