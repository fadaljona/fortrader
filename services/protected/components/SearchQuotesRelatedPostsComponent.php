<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class SearchQuotesRelatedPostsComponent extends ComponentBase {
		public $keywordsPerSearch = 10;
		
		function __construct() {
			
		}
		private function firstSearchKeywords(){
			$keys = QuotesSymbolKeywordModel::model()->findAll(Array(
				'condition' => " `t`.`firstSearch` = 1 AND `t`.`idLanguage` = 1 ",
				'limit' => $this->keywordsPerSearch,
			));
			foreach( $keys as $key ){
				echo $key->keyword . ' ' . $key->idLanguage  . '<br />';
				
				$relatedPosts = WPPostModel::model()->findAll(Array(
					'condition' => " ( `t`.`post_title` LIKE :keyword OR `t`.`post_content` LIKE :keyword ) AND `t`.`post_type` = 'post' AND `t`.`post_status` = 'publish' ",
					'order' => " `t`.`post_date` DESC ",
					'limit' => 4,
					'params' => array( ':keyword' => '%'.$key->keyword.'%' ),
				));
				foreach( $relatedPosts as $post ){
					$keywordPost = new QuotesKeywordPostsModel;
					$keywordPost->keywordId = $key->id;
					$keywordPost->postId = $post->ID;

					if( $keywordPost->validate() ) $keywordPost->save();
				}
				$key->firstSearch = 0;
				if( $key->validate() ) $key->save();
			}
		}
		private function newUpdatedPostsSearch(){
			$postsToSearch = QuotesPostsForKeywordsSearchModel::model()->findAll();
			
			$keys = QuotesSymbolKeywordModel::model()->findAll(Array(
				'condition' => " `t`.`idLanguage` = 1 ",
				'order' => ' `t`.`id` ASC ',
			));
			
			foreach( $postsToSearch as $postToSearch ){
				$postModel = WPPostModel::model()->find(Array(
					'condition' => " `t`.`ID` = :id AND `t`.`post_type` = 'post' AND `t`.`post_status` = 'publish' ",
					'params' => array( ':id' => $postToSearch->id ),
				));
				if( $postModel ){
					$content = '------------' . $postModel->post_title . '------------' . $postModel->post_content;
		
					foreach( $keys as $key ){
						if( strpos( $content, $key->keyword ) ){
							echo $key->keyword . '<br />';
							$keywordPost = new QuotesKeywordPostsModel;
							$keywordPost->keywordId = $key->id;
							$keywordPost->postId = $postModel->ID;
							if( $keywordPost->validate() ) $keywordPost->save();
						}
					}
					
				}
				$postToSearch->delete();
			}
		}
		
		function search() {
			$this->firstSearchKeywords();
			$this->newUpdatedPostsSearch();
		}
	}
			
?>