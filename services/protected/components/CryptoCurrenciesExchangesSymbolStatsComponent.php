<?php

Yii::import('components.base.ComponentBase');

class CryptoCurrenciesExchangesSymbolStatsComponent extends ComponentBase
{
    const STATS_URL = 'https://coinmarketcap.com/';

    public function update()
    {
        $models = CryptoCurrenciesExchangesModel::model()->findAll(array(
            'with' => array(
                'symbolsToSymbols' => array(
                    'with' => array('ourSymbol', 'stats')
                )
            ),
            'condition' => '`symbolsToSymbols`.`symbolId` <> 0 AND `symbolsToSymbols`.`symbolId` IS NOT NULL AND `ourSymbol`.`id` IS NOT NULL'
        ));

        foreach ($models as $model) {
            if ($model->symbolsToSymbols) {
                $strToParse = CommonLib::downloadFileFromWww($model->statsUrl);

                if (!$strToParse) {
                    throw new Exception("Can't get content for {$model->statsUrl}");
                }

                echo "\nstart parsing data for {$model->statsUrl}\n";

                $strToParse = str_replace(array("\n", "\r"), '', $strToParse);
                preg_match_all(
                    '/>([A-Z]+\/[A-Z]+)<.*?<span class="volume" data-usd="([^"]+)".*?<span class="price" data-usd="([^"]+)".*?data-format-value="([^"]+)"/',
                    $strToParse,
                    $symbolsData
                );

                if (!isset($symbolsData) ||
                    !isset($symbolsData[1]) ||
                    !isset($symbolsData[2]) ||
                    !isset($symbolsData[3]) ||
                    !isset($symbolsData[4])
                ) {
                    throw new Exception("Can't parse {$model->statsUrl}");
                }

                $this->importData($model->symbolsToSymbols, $symbolsData);
            }
        }
    }

    public function updateBitcoinStats()
    {
        $strToParse = CommonLib::downloadFileFromWww(self::STATS_URL);

        if (!$strToParse) {
            throw new Exception("Can't get content for " . self::STATS_URL);
        }

        $strToParse = str_replace(array("\n", "\r"), '', $strToParse);

        preg_match_all(
            '/Bitcoin<.*?market-cap text-right" data-usd="([^"]+)".*?class="price" data-usd="([^"]+)".*?class="volume" data-usd="([^"]+)".*?data-supply="([^"]+)".*?data-percentusd="([^"]+)"/',
            $strToParse,
            $symbolsData
        );

        if (!isset($symbolsData) ||
            !isset($symbolsData[1]) ||
            !isset($symbolsData[2]) ||
            !isset($symbolsData[3]) ||
            !isset($symbolsData[4]) ||
            !isset($symbolsData[5])
        ) {
            throw new Exception("Can't parse " . self::STATS_URL);
        }

        $model = CryptoCurrenciesBitcoinStatsModel::model()->find(array(
            'condition' => '`t`.`date` = :date',
            'params' => array(':date' => date('Y-m-d'))
        ));
        if (!$model) {
            $model = new CryptoCurrenciesBitcoinStatsModel;
            $model->date = date('Y-m-d');
        }

        $model->marketCap = intval(floatval($symbolsData[1][0]));
        $model->price = $symbolsData[2][0];
        $model->volume = $symbolsData[3][0];
        $model->circulatingSupply = $symbolsData[4][0];
        $model->change = $symbolsData[5][0];
        if ($model->validate()) {
            $model->save();
        }
    }

    private function importData($symbols, $data)
    {
        foreach ($symbols as $symbol) {
            if (!in_array($symbol->symbol, $data[1])) {
                continue;
            }

            if ($symbol->stats) {
                $symbol->stats->delete();
            }

            $key = array_search($symbol->symbol, $data[1]);

            $newStats = CryptoCurrenciesExchangesSymbolStatsModel::instance(
                $symbol->id,
                $data[2][$key],
                $data[3][$key],
                $data[4][$key],
                date('Y-m-d')
            );

            if ($newStats->validate()) {
                $newStats->save();
                echo "data for {$symbol->symbol} inserted\n";
            }
        }
    }
}
