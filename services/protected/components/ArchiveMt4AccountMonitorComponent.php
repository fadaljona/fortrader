<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class ArchiveMt4AccountMonitorComponent extends ComponentBase {
		
		private function archive(){
			$models = MT4AccountMonitorModel::model()->findAll(array(
				'with' => array( 'member' => array( 'with' => 'contest' ) ),
				'condition' => ' `contest`.`end` < :end AND `contest`.`status` = "completed" ',
				'params' => array( ':end' => date('Y-m-d', time() - 60*60*24*30*2) ),
				'group' => ' `member`.`id` ',
				'limit' => 500
			));

			foreach( $models as $model ){
				$this->archiveAccount(array(
					'member' => $model->member->id,
					'accountNumber' => $model->accountNumber,
					'accountServerId' => $model->accountServerId,
				));
			}
			
		}
		private function archiveAccount( array $accout ){

			$models = MT4AccountMonitorModel::model()->findAll(array(
				'condition' => ' `t`.`accountNumber` = :accountNumber AND `t`.`accountServerId` = :accountServerId ',
				'params' => array( ':accountNumber' => $accout['accountNumber'], ':accountServerId' => $accout['accountServerId'] ),
				'order' => ' `t`.`dateAdd` ASC ',
			));
			
			$localFileName = "{$accout['member']}_{$accout['accountNumber']}_monitor.csv";
			$csvFileName = MT4AccountMonitorModel::PATHStoreFilesDir . "/$localFileName";
			$zipFileName = MT4AccountMonitorModel::PATHStoreFilesDir . "/{$accout['member']}_{$accout['accountNumber']}_monitor.zip";
			
			clearstatcache();
			if( file_exists($csvFileName) ) unlink($csvFileName);
			if( file_exists($zipFileName) ) unlink($zipFileName);
			
			$fp = fopen( $csvFileName, 'w');
			
			fputcsv($fp, array(
				'Date',
				'Comment',
				'Account History',
				'Open Deals',
				'Balance',
				'Equity'
			));
			
			foreach( $models as $model ){
				fputcsv($fp, array(
					$model->dateAdd,
					$model->comment,
					$model->accountHistory,
					$model->openDeals,
					$model->balance,
					$model->equity
				));
			}
			
			fclose($fp);
			
			$zip = new ZipArchive();

			if ($zip->open($zipFileName, ZipArchive::CREATE)!==TRUE) throw new CException("unable create zip $zipFileName");

			$fileAdded = $zip->addFile( $csvFileName, $localFileName);
			if( !$fileAdded ) throw new CException("can't add file to archive $zipFileName");
			
			$zipClosed = $zip->close();
			if( !$zipClosed ) throw new CException("can't close archive $zipFileName");
			
			clearstatcache();
			if( file_exists($csvFileName) ) unlink($csvFileName);
			
			MT4AccountMonitorModel::model()->deleteAll(
				' `accountNumber` = :accountNumber AND `accountServerId` = :accountServerId ', 
				array( ':accountNumber' => $accout['accountNumber'], ':accountServerId' => $accout['accountServerId'] ) 
			);
			
			echo "monitoring for account {$accout['accountNumber']} {$accout['accountServerId']} for member {$accout['member']} archived\n";
		}
		
		public function run() {
			$this->archive();
		}
	}
			
?>