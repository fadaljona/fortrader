<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class WpPopularCatsComponent extends ComponentBase {
		function __construct() {
			
		}
		function update() {
			$dataCats = Yii::app()->db->createCommand()
					->select('count( p.ID ) postCount, sum( pmeta.meta_value ) postViews, terms.term_id catId, terms.name catName')
					->from('wp_posts p')
					->join('wp_postmeta pmeta', 'p.ID = pmeta.post_id')
					->join('wp_term_relationships r', 'p.ID = r.object_ID')
					->join('wp_term_taxonomy t', 'r.term_taxonomy_id = t.term_taxonomy_id')
					->join('wp_terms terms', 't.term_id = terms.term_id')
					->where( 'p.post_status = "publish" and p.post_type = "post" and p.post_author > 0 and pmeta.meta_key = "views" and t.taxonomy = "category" and terms.term_id IS NOT NULL' )
					->group('terms.term_id') 
					->order('postViews DESC')
					->limit(5)
					->queryAll(); 
			print_r($dataCats);
			$outData = '<ul class="list1 semibold">';
			$today = getdate();
			foreach( $dataCats as $dataCat ){
	
				/*$posts = WPPostModel::model()->findAll(Array(
					'select'=>" ID, post_title, post_date ",
					'with'=>Array(
						"categories"
					),
					'condition'=>"
						 `categories`.`term_id`=:catId and `t`.`post_date` BETWEEN :statDate and :endDate ",

					'params' => array( 
						':catId' => $dataCat['catId'], 
						':statDate' => date('Y-m-d 00:00:00', time()), 
						':endDate' => date('Y-m-d 23:59:59', time()), 
					),
				));
				
				print_r( count($posts) );
				

				$todayNumber = '';
				if( count($posts) ){
					$todayNumber = '<span class="grey_color regular">+'.count($posts).'</span>';
				}
				$catModel = WPTermModel::model()->findByPk( $dataCat['catId'] );
				print_r( $catModel->getCategoryURL() );
				$outData .= '<li><a href="'. $catModel->getCategoryURL() .'">'.$dataCat['catName'].' <span class="red_color">('.$dataCat['postCount'].')</span> '.$todayNumber.'</a></li>';*/
				
				
				$r = new WP_Query( array(
					'post_status'  => 'publish',
					'post_type' => 'post',
					'year' => $today["year"],
					'monthnum' => $today["mon"],
					'day' => $today["mday"],
					'cat' => $dataCat['catId'],
				) );
				$todayNumber = '';
				if( $r->found_posts ){
					$todayNumber = '<span class="grey_color regular">+'.$r->found_posts.'</span>';
				}
				$outData .= '<li><a href="'.esc_url( get_category_link( $dataCat['catId'] ) ).'">'.$dataCat['catName'].' <span class="red_color">('.$dataCat['postCount'].')</span> '.$todayNumber.'</a></li>';
				
				
			}
			echo $outData .= '</ul>';
			file_put_contents( Yii::app()->basePath.'/data/wpPopularCats.txt', $outData );
	
		}
	}
			
?>