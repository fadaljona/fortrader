<?php

Yii::import('components.exchangersParsers.ExchangerParserComponentBase');

final class ObmennikWsParser extends ExchangerParserComponentBase
{
    const RESERVES_URL = 'https://obmennik.ws/';
    const COURSES_URL = 'https://obmennik.ws/rate.xml';

    protected $enabled = true;

    protected function getDataToImport()
    {
        $coursesContent = $this->getCfContnet(self::COURSES_URL);

        if (!$coursesContent) {
            $this->log("Can't get cf protected content for " . self::COURSES_URL, 'warning');
            return false;
        }
        $coursesContent = mb_strtolower(mb_convert_encoding($coursesContent, "UTF-8", "Windows-1251"));

        $reservesContent = $this->getCfContnet(self::RESERVES_URL);
        if (!$reservesContent) {
            $this->log("Can't get cf protected content for " . self::RESERVES_URL, 'warning');
            return false;
        }
        $reservesContent = mb_strtolower(mb_convert_encoding($reservesContent, "UTF-8", "Windows-1251"));

        preg_match_all('/<td style="color: #ffffff;"><span[^>]+>([^<]+)<\/span>\:<\/td>[^<]+<td[^>]+><nobr>([^ ]+) /', $reservesContent, $reservesMatches);

        if (!isset($reservesMatches) || !$reservesMatches || !isset($reservesMatches[1]) || !isset($reservesMatches[2])) {
            $this->log("Can't find data in content for " . self::RESERVES_URL, 'warning');
            return false;
        }

        $startDataSearchStr = '<description><![cdata[';
        $startDataPos = strpos($coursesContent, $startDataSearchStr);

        if (!$startDataPos) {
            $this->log("Can't find data in content for " . self::COURSES_URL, 'warning');
            return false;
        }

        $startDataPos += strlen($startDataSearchStr);
        $endDataPos = strpos($coursesContent, '<br> ]]></description>');

        if (!$endDataPos) {
            $this->log("Can't find data in content for " . self::COURSES_URL, 'warning');
            return false;
        }

        $coursesContent = substr($coursesContent, $startDataPos, $endDataPos - $startDataPos);
        $coursesContentArr = explode('<br>', $coursesContent);

        $outArr = array(
            'courses' => array(),
            'reserves' => array(),
        );
        $allAliases = PaymentSystemCurrencyAliasModel::getAllAliases();
        $notFoundCurrency = array();

        foreach ($coursesContentArr as $courseStr) {
            $delimiterPos = strpos($courseStr, ':');
            $directionStr = substr($courseStr, 0, $delimiterPos);
            $courseValStr = substr($courseStr, $delimiterPos+1);
            
            $delimiterPos = strpos($directionStr, ' &#8594; ');
            $sourceCurrency = trim(substr($directionStr, 0, $delimiterPos));
            $targetCurrency = trim(substr($directionStr, $delimiterPos + strlen(' &#8594; ')));

            if (!isset($allAliases[$sourceCurrency]) || !$allAliases[$sourceCurrency]) {
                $notFoundCurrency[$sourceCurrency] = $sourceCurrency;
                continue;
            }
            if (!isset($allAliases[$targetCurrency]) || !$allAliases[$targetCurrency]) {
                $notFoundCurrency[$targetCurrency] = $targetCurrency;
                continue;
            }

            $sourceCurrencyName = $sourceCurrency;
            $targetCurrencyName = $targetCurrency;
            $sourceCurrency = $allAliases[$sourceCurrency];
            $targetCurrency = $allAliases[$targetCurrency];

            $delimiterPos = strpos($courseValStr, ' = ');
            $sourceAmount = trim(substr($courseValStr, 0, $delimiterPos));
            $sourceAmount = substr($sourceAmount, 0, strpos($sourceAmount, ' '));
            $targetAmount = trim(substr($courseValStr, $delimiterPos + strlen(' = ')));
            $targetAmount = substr($targetAmount, 0, strpos($targetAmount, ' '));

            if (!$sourceAmount ||
                !$sourceCurrency ||
                !$targetAmount ||
                !$targetCurrency ||
                !$reservesMatches[2][array_search($targetCurrencyName, $reservesMatches[1])]
            ) {
                $this->log("Can't define course val for $sourceCurrencyName $targetCurrencyName " . self::COURSES_URL, 'warning');
                continue;
            }

            $outArr['courses'][] = array(
                'fromAmount' => $sourceAmount,
                'fromCurrency' => $sourceCurrency,
                'toAmount' => $targetAmount,
                'toCurrency' => $targetCurrency,
            );
            $outArr['reserves'][$targetCurrency] = $reservesMatches[2][array_search($targetCurrencyName, $reservesMatches[1])];
        }

        if ($notFoundCurrency) {
            $this->log("Not found ObmennikWs currencies, please add aliases " . implode(', ', $notFoundCurrency), 'warning');
        }

        return $outArr;
    }
}
