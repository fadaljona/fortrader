<?php

Yii::import('components.exchangersParsers.ExchangerParserComponentBase');

final class QuickchangeParser extends ExchangerParserComponentBase
{
    const COURSES_URL = 'https://quickchange.cc/xrates/';
    const RESERVES_URL = 'https://quickchange.cc';

    protected $enabled = true;

    protected function getDataToImport()
    {
        $coursesContent = CommonLib::downloadFileFromWww(self::COURSES_URL);
        if (!$coursesContent) {
            $this->log("Can't get content for " . self::COURSES_URL, 'warning');
            return false;
        }
        $coursesContent = mb_strtolower($coursesContent);

        $reservesContent = CommonLib::downloadFileFromWww(self::RESERVES_URL);
        if (!$reservesContent) {
            $this->log("Can't get content for " . self::RESERVES_URL, 'warning');
            return false;
        }
        $reservesContent = mb_strtolower($reservesContent);

        preg_match_all('/<div class="exchange_row">\s*<div[^>]+><strong class="red">([^<]+)<[^>]+>([^<]+)<\/div>[^<]*<div class="arrow"><\/div>[^<]*<[^>]+><strong class="red">([^<]+)<[^>]+>([^<]+)</', $coursesContent, $coursesMatches);

        if (!isset($coursesMatches)
            || !isset($coursesMatches[1])
            || !isset($coursesMatches[2])
            || !isset($coursesMatches[3])
            || !isset($coursesMatches[4])
            || !((count($coursesMatches[1]) == count($coursesMatches[2])) && (count($coursesMatches[2]) == count($coursesMatches[3])) && (count($coursesMatches[3]) == count($coursesMatches[4])))
        ) {
            $this->log("Can't get courses from content for " . self::COURSES_URL, 'warning');
            return false;
        }

        preg_match_all('/<div class="ps_reserv clear">[^<]+<[^>]+>[^<]+<[^>]+>([^<]+)<[^>]+>[^<]+<[^>]+>([^<]+)</', $reservesContent, $reservesMatches);
        
        if (!isset($reservesMatches)
            || !isset($reservesMatches[1])
            || !isset($reservesMatches[2])
            || !(count($reservesMatches[1]) == count($reservesMatches[2]))
        ) {
            $this->log("Can't get reserves from content for " . self::RESERVES_URL, 'warning');
            return false;
        }

        $outArr = array(
            'courses' => array(),
            'reserves' => array(),
        );
        $allAliases = PaymentSystemCurrencyAliasModel::getAllAliases();
        $notFoundCurrency = array();

        foreach ($coursesMatches[1] as $index => $val) {
            $sourceCurrency = trim($coursesMatches[2][$index]);
            $targetCurrency = trim($coursesMatches[4][$index]);

            if (!isset($allAliases[$sourceCurrency]) || !$allAliases[$sourceCurrency]) {
                $notFoundCurrency[$sourceCurrency] = $sourceCurrency;
                continue;
            }
            if (!isset($allAliases[$targetCurrency]) || !$allAliases[$targetCurrency]) {
                $notFoundCurrency[$targetCurrency] = $targetCurrency;
                continue;
            }

            $sourceCurrency = $allAliases[$sourceCurrency];
            $targetCurrency = $allAliases[$targetCurrency];

            $outArr['courses'][] = array(
                'fromAmount' => trim($val),
                'fromCurrency' => $sourceCurrency,
                'toAmount' => trim($coursesMatches[3][$index]),
                'toCurrency' => $targetCurrency,
            );
        }

        foreach ($reservesMatches[1] as $index => $val) {
            $currency = trim($val);
            if (!isset($allAliases[$currency]) || !$allAliases[$currency]) {
                $notFoundCurrency[$currency] = $currency;
                continue;
            }
            $currency = $allAliases[$currency];
            $outArr['reserves'][$currency] = trim($reservesMatches[1][$index]);
        }

        if ($notFoundCurrency) {
            $this->log("Not found Quickchange currencies, please add aliases " . implode(', ', $notFoundCurrency), 'warning');
        }

        return $outArr;

    }
}
