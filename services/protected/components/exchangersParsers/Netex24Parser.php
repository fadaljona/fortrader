<?php

Yii::import('components.exchangersParsers.ExchangerParserComponentBase');

final class Netex24Parser extends ExchangerParserComponentBase
{
    const ID_TO_NAME_URL = 'https://www.netex24.net/netex24.min.js';
    const DATA_URL = 'https://www.netex24.net/api/exchangeDirection/getAll';
    const NETEX24_ID_TO_NAMES = 'Netex24Parser.idToNames';

    protected $cfDelay = 2000;//milliseconds

    protected $enabled = true;

    protected function getNetex24IdToNames()
    {
        if ($returnArr = Yii::app()->cache->get(self::NETEX24_ID_TO_NAMES)) {
            return $returnArr;
        }
        $content = $this->getCfContnet(self::ID_TO_NAME_URL);

        if (!isset($content) || !$content) {
            $this->log("Can't get content for " . self::ID_TO_NAME_URL, 'warning');
            return false;
        }

        $matches = array();
        preg_match('/"nxCurrency",function\(\){var n={([^}]+)}/', $content, $matches);
        
        if (!$matches || !isset($matches[1]) || !$matches[1]) {
            $this->log("Can't find id to names array", 'warning');
            return false;
        }
        
        $returnArr = json_decode('{' . $matches[1] . '}');
        if (!$returnArr) {
            $this->log("Can't decode id to names array", 'warning');
            return false;
        }

        Yii::app()->cache->set(self::NETEX24_ID_TO_NAMES, $returnArr, 60*60*6);
        return $returnArr;
    }

    protected function getDataToImport()
    {
        $netex24IdToNames = $this->getNetex24IdToNames();
        if (!$netex24IdToNames) {
            return false;
        }

        $content = $this->getCfContnet(self::DATA_URL);
        if (!isset($content) || !$content) {
            $this->log("Can't get content for " . self::DATA_URL, 'warning');
            return false;
        }

        $xml = simplexml_load_string($content);

        echo $content;
        print_r($xml);

        die();

        if (!$xml) {
            $this->log("Can't load xml from " . self::DATA_URL, 'warning');
            return false;
        }

        $outArr = array(
            'courses' => array(),
            'reserves' => array(),
        );

        $allAliases = PaymentSystemCurrencyAliasModel::getAllAliases();

        $notFoundCurrency = array();

        foreach ($xml as $one) {
            if ($one->IsDisabled->__toString() == 'true') {
                continue;
            }

            $sourceCurrency = mb_strtolower(trim($netex24IdToNames->{$one->SourceCurrencyId->__toString()}));
            $targetCurrency = mb_strtolower(trim($netex24IdToNames->{$one->TargetCurrencyId->__toString()}));

            if (!isset($allAliases[$sourceCurrency]) || !$allAliases[$sourceCurrency]) {
                $notFoundCurrency[$sourceCurrency] = $sourceCurrency;
                continue;
            }
            if (!isset($allAliases[$targetCurrency]) || !$allAliases[$targetCurrency]) {
                $notFoundCurrency[$targetCurrency] = $targetCurrency;
                continue;
            }

            $sourceCurrency = $allAliases[$sourceCurrency];
            $targetCurrency = $allAliases[$targetCurrency];

            $outArr['courses'][] = array(
                'fromAmount' => $one->SourceAmount->__toString(),
                'fromCurrency' => $sourceCurrency,
                'toAmount' => $one->TargetAmount->__toString(),
                'toCurrency' => $targetCurrency,
            );
            $outArr['reserves'][$targetCurrency] = $one->TargetAvailable->__toString();
        }

        if ($notFoundCurrency) {
            $this->log("Not found Netex24 currencies, please add aliases " . implode(', ', $notFoundCurrency), 'warning');
        }
        
        return $outArr;
    }
}
