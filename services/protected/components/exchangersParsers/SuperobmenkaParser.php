<?php

Yii::import('components.exchangersParsers.ExchangerParserComponentBase');

final class SuperobmenkaParser extends ExchangerParserComponentBase
{
    const DATA_URL = 'http://www.superobmenka.com/ru/export/xml';

    protected $enabled = true;

    protected function getDataToImport()
    {
        $content = $this->getCfContnet(self::DATA_URL);

        if (!$content) {
            $this->log("Can't get cf protected content for " . self::DATA_URL, 'warning');
            return false;
        }
        $content = strtolower($content);

        $xml = simplexml_load_string($content);
        if (!$xml) {
            $this->log("Can't load xml from " . self::DATA_URL, 'warning');
            return false;
        }

        $outArr = array(
            'courses' => array(),
            'reserves' => array(),
        );
        $allAliases = PaymentSystemCurrencyAliasModel::getAllAliases();
        $notFoundCurrency = array();

        foreach ($xml as $one) {

            $sourceCurrency = trim($one->from->__toString());
            $targetCurrency = trim($one->to->__toString());

            if (!isset($allAliases[$sourceCurrency]) || !$allAliases[$sourceCurrency]) {
                $notFoundCurrency[$sourceCurrency] = $sourceCurrency;
                continue;
            }
            if (!isset($allAliases[$targetCurrency]) || !$allAliases[$targetCurrency]) {
                $notFoundCurrency[$targetCurrency] = $targetCurrency;
                continue;
            }

            $sourceCurrency = $allAliases[$sourceCurrency];
            $targetCurrency = $allAliases[$targetCurrency];

            $outArr['courses'][] = array(
                'fromAmount' => $one->in->__toString(),
                'fromCurrency' => $sourceCurrency,
                'toAmount' => $one->out->__toString(),
                'toCurrency' => $targetCurrency,
            );
            $outArr['reserves'][$targetCurrency] = $one->amount->__toString();
        }

        if ($notFoundCurrency) {
            $this->log("Not found Superobmenka currencies, please add aliases " . implode(', ', $notFoundCurrency), 'warning');
        }

        return $outArr;
    }
}
