<?php

Yii::import('components.base.ComponentBase');

abstract class ExchangerParserComponentBase extends ComponentBase
{
    const CF_GET_PAGE_SCRIPT = 'getPageContent.js';

    protected $cfDelay = 10000;//milliseconds

    protected $exchangerId;
    protected $enabled = true;

    public function __construct($exchangerId)
    {
        $this->exchangerId = $exchangerId;
    }

    protected function getCfExecCmd($url)
    {
        return 'phantomjs ' . Yii::getPathOfAlias('components') . '/' . self::CF_GET_PAGE_SCRIPT . ' ' . $url . ' ' . $this->cfDelay;
    }

    protected function getCfContnet($url)
    {
        exec($this->getCfExecCmd($url), $result);
        if (!isset($result) || !isset($result[0])) {
            return false;
        }
        return implode(PHP_EOL, $result);
    }

    protected function log($message, $level = 'info')
    {
        Yii::log($message, $level, 'exchangerParser');
    }
    protected function logNotFound($message, $level = 'info')
    {
        Yii::log($message, $level, 'exchangerParserNotFoundCurrencies');
    }

    /**
     * get data from exchanger for import
     *
     * @return array
     * [courses] => Array
     *  (
     *      [0] => Array
     *          (
     *              [fromAmount] => 1
     *              [fromCurrency] => 5
     *              [toAmount] => 57.0285
     *              [toCurrency] => 2
     *           )
     *   )
     * [reserves] => Array
     *  (
     *      [2] => 247.41
     *      [7] => 337905.30
     *      [14] => 3.1790
     *   )
     */
    abstract protected function getDataToImport();

    public function import()
    {
        if (!$this->enabled) {
            return false;
        }
        
        $dataToImport = $this->getDataToImport();

        if (!$dataToImport['courses'] || !$dataToImport['reserves']) {
            return false;
        }

        $date = date('Y-m-d');

        Yii::app()->db->createCommand(
            "DELETE FROM `{{payment_exchanger_course}}` WHERE `idExchanger` = :id AND `date` = :date"
        )->query(array(
            ':id' => $this->exchangerId,
            ':date' => date('Y-m-d')
        ));

        Yii::app()->db->createCommand(
            "DELETE FROM `{{payment_exchanger_reserve}}` WHERE `idExchanger` = :id AND `date` = :date"
        )->query(array(
            ':id' => $this->exchangerId,
            ':date' => date('Y-m-d')
        ));


        $query = " INSERT IGNORE INTO `{{payment_exchanger_course}}` (`idExchanger`, `fromCurrency`, `fromAmount`, `toCurrency`, `toAmount`, `toReserve`, `date`) VALUES ";
        $i=0;
        $params = array( ':id' => $this->exchangerId, ':date' => $date );


        $savedDirections = PaymentExchangeDirectionModel::getSavedDirections();
        $directionQuery = " INSERT IGNORE INTO `{{payment_exchange_direction}}` (`from`, `to`) VALUES ";
        $directionParams = array();
        
        foreach ($dataToImport['courses'] as $course) {
            if ($i) {
                $query .= ',';
            }

            $query .= "(:id, :fromCurrency$i, :fromAmount$i, :toCurrency$i, :toAmount$i, :toReserve$i, :date )";
            $params[":fromCurrency$i"] =  $course['fromCurrency'];
            $params[":fromAmount$i"] =  $course['fromAmount'];
            $params[":toCurrency$i"] =  $course['toCurrency'];
            $params[":toAmount$i"] =  $course['toAmount'];
            $params[":toReserve$i"] =  isset($course['toReserve']) ? $course['toReserve'] : null;




            if (!isset($directionParams[':fromToAll' . $course['fromCurrency']]) &&
                !in_array($course['fromCurrency'] . '_0', $savedDirections)) {
                $directionQuery .= "(:fromToAll{$course['fromCurrency']}, :toAll),";
                $directionParams[':fromToAll' . $course['fromCurrency']] =  $course['fromCurrency'];
                $directionParams[':toAll'] = 0;
            }

            if (!isset($directionParams[':fromAllto' . $course['toCurrency']]) &&
                !in_array('0_' . $course['toCurrency'], $savedDirections)) {
                $directionQuery .= "(:fromAll, :fromAllto{$course['toCurrency']}),";
                $directionParams[':fromAllto' . $course['toCurrency']] =  $course['toCurrency'];
                $directionParams[':fromAll'] = 0;
            }

            if (!in_array($course['fromCurrency'] . '_' . $course['toCurrency'], $savedDirections)) {
                $directionQuery .= "(:from$i, :to$i),";
                $directionParams[":from$i"] =  $course['fromCurrency'];
                $directionParams[":to$i"] =  $course['toCurrency'];
            }


            $i++;
        }

        if (count($params) > 2) {
            $insertResult = Yii::App()->db->createCommand($query)->query($params);
            echo " inserted rows {$insertResult->rowCount} for courses exchanger {$this->exchangerId}\n";
        }
        
        if (count($directionParams)) {
            $directionQuery = substr($directionQuery, 0, -1);

            $insertResultDirection = Yii::App()->db->createCommand($directionQuery)->query($directionParams);
            echo " inserted new directions {$insertResultDirection->rowCount}\n";
        }
        

        $query = " INSERT IGNORE INTO `{{payment_exchanger_reserve}}` (`idExchanger`, `idCurrency`, `amount`, `date`) VALUES ";
        $i=0;
        $params = array( ':id' => $this->exchangerId, ':date' => $date );
        foreach ($dataToImport['reserves'] as $idCurrency => $reserve) {
            if ($i) {
                $query .= ',';
            }
            $query .= "(:id, :idCurrency$i, :amount$i, :date )";
            $params[":idCurrency$i"] =  $idCurrency;
            $params[":amount$i"] =  $reserve;
            $i++;
        }
        $insertResult = Yii::App()->db->createCommand($query)->query($params);
        echo " inserted rows {$insertResult->rowCount} for reserves exchanger {$this->exchangerId}\n";
        

        
    }
}
