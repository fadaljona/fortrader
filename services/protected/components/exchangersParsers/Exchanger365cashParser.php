<?php

Yii::import('components.exchangersParsers.ExchangerParserComponentBase');

final class Exchanger365cashParser extends ExchangerParserComponentBase
{
    const DATA_URL = 'http://365cash.co/api/v1/rates/bestchange';

    protected $enabled = true;

    protected function getDataToImport()
    {
        $content = CommonLib::downloadFileFromWww(self::DATA_URL);
        if (!isset($content) || !$content) {
            $this->log("Can't get content for " . self::DATA_URL, 'warning');
            return false;
        }

        $xml = simplexml_load_string($content);
        if (!$xml) {
            $this->log("Can't load xml from " . self::DATA_URL, 'warning');
            return false;
        }

        $outArr = array(
            'courses' => array(),
            'reserves' => array(),
        );
        $allAliases = PaymentSystemCurrencyAliasModel::getAllAliases();
        $notFoundCurrency = array();

        foreach ($xml as $one) {
            $sourceCurrency = mb_strtolower(trim($one->from->__toString()));
            $targetCurrency = mb_strtolower(trim($one->to->__toString()));

            if (!isset($allAliases[$sourceCurrency]) || !$allAliases[$sourceCurrency]) {
                $notFoundCurrency[$sourceCurrency] = $sourceCurrency;
                continue;
            }
            if (!isset($allAliases[$targetCurrency]) || !$allAliases[$targetCurrency]) {
                $notFoundCurrency[$targetCurrency] = $targetCurrency;
                continue;
            }

            $sourceCurrency = $allAliases[$sourceCurrency];
            $targetCurrency = $allAliases[$targetCurrency];

            $outArr['courses'][] = array(
                'fromAmount' => $one->in->__toString(),
                'fromCurrency' => $sourceCurrency,
                'toAmount' => $one->out->__toString(),
                'toCurrency' => $targetCurrency,
                'toReserve' => $one->amount->__toString(),
            );
            if (isset($outArr['reserves'][$targetCurrency]) &&
                $outArr['reserves'][$targetCurrency] < floatval($one->amount->__toString())
            ) {
                $outArr['reserves'][$targetCurrency] = floatval($one->amount->__toString());
            } elseif (!isset($outArr['reserves'][$targetCurrency])) {
                $outArr['reserves'][$targetCurrency] = floatval($one->amount->__toString());
            }
        }

        if ($notFoundCurrency) {
            $this->log("Not found 365cash currencies, please add aliases " . implode(', ', $notFoundCurrency), 'warning');
        }
        
        return $outArr;
    }
}
