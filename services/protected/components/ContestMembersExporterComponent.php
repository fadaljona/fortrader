<?
	/*
		Компонент для экспорта данных по аккаунтам для внешнего мониторинга
		Компонент запускается в скрипте по крону cronContestMembersExport.php - ContestMembersExporterComponent::import();
	*/
		
		// импорт связей
	Yii::import( 'components.base.ComponentBase' );
	
		// класс компонента
	final class ContestMembersExporterComponent extends ComponentBase {
			// путь для сохранения данных
		const pathExport = 'export/account_list.txt';
			
			/*
				Функция получения итератора по объектам-участникам-конкурсов
			*/
		static function getIteratorContestMembers() {
				// получаем DataProvider по модели ContestMemberModel
			$DP = new CActiveDataProvider( 'ContestMemberModel', Array(
				'criteria' => Array(
					'select' => Array(
						"`t`.*",
						"
							(
								SELECT 	COUNT( * )
								FROM	`mt4_account_history`
								WHERE	`mt4_account_history`.`AccountNumber` = `t`.`accountNumber`
							) AS countRowsMT4AccountHistory
						",
						"
							(
								SELECT 	MAX( `OrderCloseTime` )
								FROM	`mt4_account_history`
								WHERE	`mt4_account_history`.`AccountNumber` = `t`.`accountNumber`
							) AS maxOrderCloseTimeMT4AccountHistory
						",
						"
							(
								SELECT 	MAX( `OrderTicket` )
								FROM	`mt4_account_history`
								WHERE	`mt4_account_history`.`AccountNumber` = `t`.`accountNumber`
							) AS maxOrderTicketMT4AccountHistory
						"
					),
					'with' => Array(
						'stats' => Array(
							'select' => "idMember,gain,equity",
						), 
						'contest' => Array(
							'select' => "end",
						), 
						'server' => Array(
							'select' => "url",
						),
						'mt5AccountInfo' => array()
					),
					'condition' => "					
						`contest`.`stopImport` = 0 
						AND `contest`.`stopDeleteMMonitoring` = 0 
						AND `contest`.`completedMembers` = 0 
						AND `t`.`stopMonitor` = 0 
						AND ( `mt5AccountInfo`.`last_error_code` IS NULL OR `mt5AccountInfo`.`last_error_code` <> -255 OR ( `mt5AccountInfo`.`last_error_code` = -255 AND `mt5AccountInfo`.`sameErrorCount` < 5 ) )
						
					",
					'order' => " `t`.`id` ",
				),
			));
			
				// получаем Iterator
			$iterator = new CDataProviderIterator( $DP, 1000 );
			return $iterator;
		}
		
		static function getIteratorMonitoringAccounts() {
				// получаем DataProvider по модели ContestMemberModel
			$DP = new CActiveDataProvider( 'ContestMemberModel', Array(
				'criteria' => Array(
					'select' => Array(
						"`t`.*",
						"
							(
								SELECT 	COUNT( * )
								FROM	`mt4_account_history`
								WHERE	`mt4_account_history`.`AccountNumber` = `t`.`accountNumber`
							) AS countRowsMT4AccountHistory
						",
						"
							(
								SELECT 	MAX( `OrderCloseTime` )
								FROM	`mt4_account_history`
								WHERE	`mt4_account_history`.`AccountNumber` = `t`.`accountNumber`
							) AS maxOrderCloseTimeMT4AccountHistory
						",
						"
							(
								SELECT 	MAX( `OrderTicket` )
								FROM	`mt4_account_history`
								WHERE	`mt4_account_history`.`AccountNumber` = `t`.`accountNumber`
							) AS maxOrderTicketMT4AccountHistory
						"
					),
					'with' => Array(
						'stats' => Array(
							'select' => "idMember,gain,equity",
						), 
						'server' => Array(
							'select' => "url",
						),
						'mt5AccountInfo' => array()
					),
					'condition' => "					
						`t`.`idContest` = 0
						AND ( `mt5AccountInfo`.`last_error_code` IS NULL OR `mt5AccountInfo`.`last_error_code` <> -255 OR ( `mt5AccountInfo`.`last_error_code` = -255 AND `mt5AccountInfo`.`sameErrorCount` < 5 ) ) 
						AND `server`.`forReports` = 0
						
					",
					'order' => " `t`.`id` ",
				),
			));
			
				// получаем Iterator
			$iterator = new CDataProviderIterator( $DP, 1000 );
			return $iterator;
		}
		
			/*
				Функция получения итератора по объектам-торговым счетам
			*/
		static function getIteratorTradeAccounts() {
				// получаем DataProvider по модели TradeAccountModel
			$DP = new CActiveDataProvider( 'TradeAccountModel', Array(
				'criteria' => Array(
					'select' => Array(
						"`t`.*",
						"
							(
								SELECT 	COUNT( * )
								FROM	`mt4_account_history`
								WHERE	`mt4_account_history`.`AccountNumber` = `t`.`accountNumber`
							) AS countRowsMT4AccountHistory
						",
						"
							(
								SELECT 	MAX( `OrderCloseTime` )
								FROM	`mt4_account_history`
								WHERE	`mt4_account_history`.`AccountNumber` = `t`.`accountNumber`
							) AS maxOrderCloseTimeMT4AccountHistory
						",
						"
							(
								SELECT 	MAX( `OrderTicket` )
								FROM	`mt4_account_history`
								WHERE	`mt4_account_history`.`AccountNumber` = `t`.`accountNumber`
							) AS maxOrderTicketMT4AccountHistory
						"
					),
					'with' => Array(
						'stats' => Array(
							'select' => "idAccount,gain,equity",
						), 
						'server' => Array(
							'select' => "url",
						),
					),
					'condition' => "
						FIND_IN_SET( 'EA', `t`.`groups` )
					",
					'order' => " `t`.`id` ",
				),
			));
				
				// получаем Iterator
			$iterator = new CDataProviderIterator( $DP, 1000 );
			return $iterator;
		}
			
			/*
				Функция для форматирования строки из массива: Array( key => value )  ->  <key=value	/>
			*/
		static function formatObject( $object ) {
			$out = Array();
			$fields = Array( 'aid', 'login', 'pass', 'srvMt4', 'h_count', 'h_closetime', 'h_ordertick' );
			foreach( $fields as $field ) {
				$out[] = sprintf( '<%s=%s	/>', $field, $object->$field );
			}
			return implode( " ", $out );
		}
			
			/*
				Функция обратного вызова для сортировки объектов в списке по значению gain
			*/
		static function cbSort( $a, $b ) {
			if( $a->end == $b->end ){
				if( $a->equity == $b->equity ) return 0;
				return $a->equity > $b->equity ? -1 : 1;
			}
			return $a->end > $b->end ? -1 : 1;
		}
			
			/*
				Функция сохранения списка объектов в account_list.txt
			*/
		static function save( $objects ) {
			$f = fopen( self::pathExport, "w" );
			if( $f ) {
				foreach( $objects as $object ) {
					$row = self::formatObject( $object );
					fputs( $f, "{$row}\r\n" );
				}
				fclose( $f );
			}
		}
			
			/*
				Основная функция компонента. Точка входа.
			*/
		static function export() {
			$objects = Array();
			
				// получаем итератор для участников конкурса
			$iterator = self::getIteratorContestMembers();
				// цикл по итератору
			foreach( $iterator as $contestMember ) {
					// формируем объект для списка
				$objects[] = (object)Array(
					'aid' => $contestMember->id,
					'login' => $contestMember->accountNumber,
					'pass' => $contestMember->investorPassword,
					'srvMt4' => $contestMember->server->url,
					'h_count' => (int)$contestMember->countRowsMT4AccountHistory,
					'h_closetime' => (int)$contestMember->maxOrderCloseTimeMT4AccountHistory,
					'h_ordertick' => (int)$contestMember->maxOrderTicketMT4AccountHistory,
					'gain' => (float)$contestMember->stats->gain,
					'equity' => (float)$contestMember->stats->equity,
					'end' => strtotime($contestMember->contest->end),
				);
			}
			
			
				// получаем итератор для торговых счетов
			$iterator = self::getIteratorTradeAccounts();
				
			$lastContest = ContestModel::model()->find(Array(
				'order' => " `t`.`end` DESC",
				'limit' => 1,
			));

				// цикл по итератору
			foreach( $iterator as $tradeAccount ) {
					// формируем объект для списка
				$objects[] = (object)Array(
					'aid' => $tradeAccount->id,
					'login' => $tradeAccount->accountNumber,
					'pass' => $tradeAccount->password,
					'srvMt4' => $tradeAccount->server->url,
					'h_count' => (int)$tradeAccount->countRowsMT4AccountHistory,
					'h_closetime' => (int)$tradeAccount->maxOrderCloseTimeMT4AccountHistory,
					'h_ordertick' => (int)$tradeAccount->maxOrderTicketMT4AccountHistory,
					'gain' => (float)$tradeAccount->stats->gain,
					'equity' => (float)$contestMember->stats->equity,
					'end' => strtotime($lastContest->end)-60*60*24*30,
				);
			}
			
				// сортируем список функцией ContestMembersExporterComponent::cbSort
			usort( $objects, Array( 'ContestMembersExporterComponent', 'cbSort' ));
			
			
				// получаем итератор для участников мониторинга
			$iterator = self::getIteratorMonitoringAccounts();
				// цикл по итератору
			foreach( $iterator as $contestMember ) {
					// формируем объект для списка
				$objects[] = (object)Array(
					'aid' => $contestMember->id,
					'login' => $contestMember->accountNumber,
					'pass' => $contestMember->investorPassword,
					'srvMt4' => $contestMember->server->url,
					'h_count' => (int)$contestMember->countRowsMT4AccountHistory,
					'h_closetime' => (int)$contestMember->maxOrderCloseTimeMT4AccountHistory,
					'h_ordertick' => (int)$contestMember->maxOrderTicketMT4AccountHistory,
					'gain' => (float)$contestMember->stats->gain,
					'equity' => (float)$contestMember->stats->equity,
					'end' => strtotime($contestMember->contest->end),
				);
			}
			
				// сохраняем список
			self::save( $objects );
		}
	}
	
?>