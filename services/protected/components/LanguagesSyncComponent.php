<?
	Yii::import( 'components.base.ComponentBase' );
	
	final class LanguagesSyncComponent extends ComponentBase {
		const STATUS_ERROR = 0;
		const STATUS_OK = 1;
		function import( $data, $idsLanguages ) {
			$languagesInData = @$data['languages'];
			if( !$languagesInData ) return self::STATUS_ERROR;
			
			$languagesImport = Array();
			foreach( $idsLanguages as $id ) {
				$languagesImport[ $id ] = $id ? $languagesInData[ $id ] : 0;
			}
			
			foreach( $languagesImport as &$languageAssoc ) {
				if( $languageAssoc ) {
					$languageModel = LanguageModel::model()->findByAttributes( Array( 'alias' => $languageAssoc[ 'alias' ]));
					if( !$languageModel ) {
						$languageModel = LanguageModel::instance( $languageAssoc[ 'name' ], $languageAssoc[ 'alias' ]);
						$languageModel->save();
					}
					$languageAssoc[ 'idInSystem' ] = $languageModel->id;
				}
			}
			unset( $languageAssoc );
			LanguageModel::getAll( true );
				
			$messages = @$data['messages'];
			if( !$messages ) return self::STATUS_ERROR;
			
			foreach( $messages as $message ) {
				$messageKey = MessageModel::model()->findByAttributes( Array( 'key' => $message['key'], 'ns' => $message['ns'] ));
				if( !$messageKey ) {
					$messageKey = MessageModel::instance( $message['key'], $message['ns'] );
					$messageKey->type = $message['type'];
					$messageKey->save();
				}
				if( @$message['values'] ) {
					$values = Array();
					foreach( $message['values'] as $idLanguageInData=>$value ) {
						if( $idLanguageInData ) {
							$values[ $languagesImport[ $idLanguageInData ][ 'idInSystem' ]] = $value;
						}
						else{
							$values[ 0 ] = $value;
						}
					}
					$messageKey->addI18Ns( $values );
				}
			}
			
			foreach( $languagesImport as &$languageAssoc ) {
				if( $languageAssoc ) {
					Yii::App()->messages->clearCache( $languageAssoc[ 'alias' ] );
				}
				else{
					Yii::App()->messages->clearCache( "en_us" );					
				}
			}
			
			return self::STATUS_OK;
		}
		function export( $idsLanguages, &$data ) {
			$data = Array();
			
			if( !$idsLanguages ) return self::STATUS_ERROR;
			$impIDSLanguages = implode( ',', $idsLanguages );
			
			$languagesModels = LanguageModel::model()->findAll( Array(
				'condition' => "`t`.`id` IN ({$impIDSLanguages})",
				'order' => '`t`.`createdDT` ASC, `t`.`id` ASC',
			));
			
			foreach( $languagesModels as $language ) {
				$data[ 'languages' ][ $language->id ] = Array( 
					'name' => $language->name,
					'alias' => $language->alias
				);
			}
			
			$messages = MessageModel::model()->findAll( Array(
				'with' => Array(
					'i18ns' => Array(
						'condition' => "`i18ns`.`idLanguage` IN ({$impIDSLanguages})",
					),
				),
				'order' => '`t`.`createdDT` ASC, `t`.`id` ASC',
			));
			
			foreach( $messages as $message ) {
				$values = Array();
				foreach( $message->i18ns as $i18n ) {
					$values[ $i18n->idLanguage ] = $i18n->value;
				}
				$data[ 'messages' ][] = Array(
					'key' => $message->key,
					'ns' => $message->ns,
					'type' => $message->type,
					'values' => $values,
				);
			}
						
			return self::STATUS_OK;
		}
	}

?>