<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class UpdatepostsStatsComponent extends ComponentBase {
		public $homeUrl = 'http://fortrader.org';
		private $ftSettings;
		function __construct() {
			
		}
		private function getFbLikesShares( $url, $fbAccessToken ){
			$url = trim( $url );
			$url = urlencode( $url );
			$apiUrl =  "https://graph.facebook.com/v2.5/?id=" . $url  ;
			$result = file_get_contents( "$apiUrl&fields=og_object{engagement{count}}&$fbAccessToken" );
			if( !$result ) return false;
			
			$decodedResult = json_decode($result);
			return $decodedResult->og_object->engagement->count;
		}
		private function getVkShares( $url ){
			$url = urlencode( $url );
			$vkApi = "https://vk.com/share.php?act=count&index=1&url=$url";
			//echo "vkApi $vkApi\n";
			$vkShares = file_get_contents( $vkApi );
			if( $vkShares ){
				preg_match_all(
					'/VK.Share.count\(1, ([0-9]+)\)/',
					$vkShares,
					$vkSharesNumArr,
					PREG_PATTERN_ORDER
				);
				return $vkSharesNumArr[1][0];
			}
			return false;
		}
		private function getTwShares( $url ){
			$twApi = "https://cdn.api.twitter.com/1/urls/count.json?url=$url";
			$twShares = file_get_contents( $twApi );
			if( $twShares ){
				$twSharesDecoded = json_decode( $twShares );
				return $twSharesDecoded->count;
			}
			return false;
		}
		private function getGpShares( $url ){
			$url = urlencode( $url );
			$gpApi = "https://plusone.google.com/_/+1/fastbutton?url=$url";
			$gpShares = file_get_contents( $gpApi );
			if( $gpShares ){
				$doc = new DOMDocument();
				$doc->loadHTML( $gpShares );
				$counter=$doc->getElementById('aggregateCount');
				return $counter->nodeValue;
			}
			return false;
		}
		private function updateSm() {
			$postsStats = PostsStatsCommonModel::model()->findAll(Array(
				'condition' => " `t`.`updateSm` = 1 ",
				'limit' => 50,
				'order' => " `t`.`postDate` DESC",
			));
			$this->ftSettings = $ftSettings = SettingsModel::getModel();
			$fbAccessToken = file_get_contents( "https://graph.facebook.com/oauth/access_token?client_id={$ftSettings->fbAppId}&client_secret={$ftSettings->fbAppSecretKey}&grant_type=client_credentials" );
			
			if( !$fbAccessToken ) die("<b>Error:</b> can't get Fb token");
			
			foreach( $postsStats as $postsStat ){
				$url = $this->homeUrl.$postsStat->url;
				echo 'url '. $url ."\n";
				
				$fbLikesShares = $this->getFbLikesShares( $url, $fbAccessToken );
				$fbLikesShares = $fbLikesShares + $this->getFbLikesShares( CommonLib::httpsProtocol($url), $fbAccessToken );
				if( $fbLikesShares === false ) die('<b>Error:</b> no Facebook response');
				echo "fblikes $fbLikesShares\n";
				
				$vkShares = $this->getVkShares( $url );
				if( $vkShares === false ) die('<b>Error:</b> no VK response');
				echo "vkShares $vkShares\n";
				
				/*$twShares = $this->getTwShares( $url );
				if( $twShares === false ) die('<b>Error:</b> no TW response');*/
				
				$gpShares = $this->getGpShares( $url );
				$gpShares = $gpShares + $this->getGpShares( CommonLib::httpsProtocol($url) );
				if( $gpShares === false ) die('<b>Error:</b> no GP response');
				echo "gpShares $gpShares\n";
				
				
				$postsStat->fbLikes = $fbLikesShares;

				$postsStat->vkShare = $vkShares;
				//$postsStat->tw = $twShares;
				$postsStat->gp = $gpShares;
				$postsStat->updateSm = 0;
				$postsStat->save();
				
				@ob_end_flush();
				flush();
			}
			echo "<hr><b>Sm updated</b><hr>";
		}

		function update() {
			$this->updateSm();
		}
	}
			
?>