<?
	Yii::import( 'components.base.ComponentBase' );
		
	final class UpdateBrokerSmStatsComponent extends ComponentBase {
		private $ftSettings;
		private $fbAccessToken;
		
		private function getMotCheckedBrokers(){
			$brokers = BrokerModel::model()->findAll(array(
				'with' => array( 'smData' ),
				'condition' => " `smChecked` = 0 ",
				'limit' => 10
			));
			if( !$brokers ) {
				$sql =  " UPDATE `{{broker}}` SET `smChecked` = 0 ";
				Yii::App()->db->createCommand( $sql )->query( );
				$brokers = BrokerModel::model()->findAll(array(
					'with' => array( 'smData' ),
					'condition' => " `smChecked` = 0 ",
					'limit' => 10
				));
			}

			return $brokers;
		}
		private function getFbLikes( $url ){
			$url = trim( $url );
			$url = urlencode( $url );
			$apiUrl =  "https://graph.facebook.com/v2.5/?id=" . $url  ;
			
			//echo "$apiUrl&fields=og_object{engagement{count}}&{$this->fbAccessToken}\n";
			
			$result = file_get_contents( "$apiUrl&fields=og_object{engagement{count}}&{$this->fbAccessToken}" );
			if( !$result ) return false;
			
			$decodedResult = json_decode($result);
			return $decodedResult->og_object->engagement->count;
		}
		private function getVkLikes( $url ){
			$url = urlencode( $url );
			
			//echo "https://api.vk.com/method/likes.getList?owner_id={$this->ftSettings->vkAppId}&type=sitepage&friends_only=0&extended=0&count=1&offset=0&page_url=$url\n\n";
			
			$result = file_get_contents( "https://api.vk.com/method/likes.getList?owner_id={$this->ftSettings->vkAppId}&type=sitepage&friends_only=0&extended=0&count=1&offset=0&page_url=$url" );
			if( !$result ) return false;
			
			$decodedResult = json_decode($result);
			if( isset($decodedResult->error) ){
				$count = 0;
			}else{
				$count = $decodedResult->response->count;
			}
			return $count;
		}
		private function checkSmData(){
			$brokers = $this->getMotCheckedBrokers();
			
			$this->ftSettings = SettingsModel::getModel();
			$this->fbAccessToken = file_get_contents( "https://graph.facebook.com/oauth/access_token?client_id={$this->ftSettings->fbAppId}&client_secret={$this->ftSettings->fbAppSecretKey}&grant_type=client_credentials" );
			if( !$this->fbAccessToken ) $this->throwI18NException( "can't get Fb token" );
			
			foreach( $brokers as $broker ){
				if( !$broker->smData ){
					$broker->smData = BrokerSmDataModel::instance( $broker->id );
				}
				$url = str_replace('/services', '', $broker->absoluteSingleURL);
				$fbLikes = $this->getFbLikes( $url );
				$fbLikes2 = $this->getFbLikes( CommonLib::httpsProtocol($url) );
				$fbLikes = $fbLikes + $fbLikes2;
				if( $fbLikes ) $broker->smData->fb = $fbLikes;
				$vkLikes = $this->getVkLikes( $url );
				if( $vkLikes ) $broker->smData->vk = $vkLikes;
				if( $broker->smData->validate() ) $broker->smData->save();
				
				$broker->smChecked = 1; 
				$broker->save();
				
				echo "$url\t$fbLikes\t$vkLikes\n";
			}
		}
		function update() {
			$this->checkSmData();
			
			$newRating = BrokerSmDataModel::getNewRating( 'broker' );
			if( $newRating ){
				BrokerStatsSmModel::saveNewRating( $newRating );
				echo '<pre>';
				print_r( $newRating );
				echo '</pre>';
			}
			
			$newRating = BrokerSmDataModel::getNewRating( 'binary' );
			if( $newRating ){
				BrokerStatsSmModel::saveNewRating( $newRating );
				echo '<pre>';
				print_r( $newRating );
				echo '</pre>';
			}
			
		}
	}
?>