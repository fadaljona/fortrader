<?
	Yii::import( 'components.base.ComponentBase' );
	
	final class UsersSyncComponent extends ComponentBase {
		const STATUS_ERROR = 0;
		const STATUS_OK = 1;
		function import( $data, $exportType ) {
			$rightsInData = @$data[ 'rights' ];
			$groupsInData = @$data[ 'groups' ];
			$usersInData = @$data[ 'users' ];
			
			if( $rightsInData ) {
				foreach( $rightsInData as &$rightAssoc ) {
					$right = UserRightModel::model()->findByAttributes( Array( 'name' => $rightAssoc[ 'name' ]));
					if( !$right ) {
						$right = UserRightModel::fromAssoc( $rightAssoc );
						$right->save();
					}
					$rightAssoc[ 'idInSystem' ] = $right->id;
				}
				unset( $rightAssoc );
			}
						
			if( $groupsInData ) {
				foreach( $groupsInData as &$groupAssoc ) {
					$group = UserGroupModel::model()->findByAttributes( Array( 'name' => $groupAssoc[ 'name' ]));
					if( !$group ) {
						$group = UserGroupModel::fromAssoc( $groupAssoc );
						$group->save();
					}
					$groupAssoc[ 'idInSystem' ] = $group->id;
					$rights = Array();
					foreach( $groupAssoc['rights'] as $id ) {
						$rights[] = $rightsInData[ $id ][ 'idInSystem' ];
					}
					$group->linkToRights( $rights );
				}
				unset( $groupAssoc );
			}
			
			if( $exportType == 'users_and_groups' and $usersInData ) {
				foreach( $usersInData as &$userAssoc ) {
					$user = UserModel::model()->findByAttributes( Array( 'email' => $userAssoc[ 'email' ]));
					if( !$user ) {
						$user = UserModel::fromAssoc( $userAssoc );
						$user->save();
					}
					$groups = Array();
					foreach( $userAssoc['groups'] as $id ) {
						$groups[] = $groupsInData[ $id ][ 'idInSystem' ];
					}
					$user->linkToGroups( $groups );
				}
				unset( $userAssoc );
			}
						
			return self::STATUS_OK;
		}
		function export( $exportType, &$data ) {
			$data = Array();

			if( !in_array( $exportType, Array( 'users_and_groups', 'groups' ))) return self::STATUS_ERROR;
			
			$rights = UserRightModel::model()->findAll( Array(
				'order' => '`t`.`id` ASC',
			));
			
			foreach( $rights as $right ) {
				$data[ 'rights' ][ $right->id ] = $right->toAssoc( null, Array( 'id' ));
			}
			
			$groups = UserGroupModel::model()->findAll( Array(
				'with' => 'rights',
				'order' => '`t`.`createdDT` ASC, `t`.`id` ASC',
			));
			
			foreach( $groups as $group ) {
				$data[ 'groups' ][ $group->id ] = $group->toAssoc( null, Array( 'id', 'createdDT' ));
				$data[ 'groups' ][ $group->id ][ 'rights' ] = $group->getIDsRights();
			}
			
			if( $exportType == 'users_and_groups' ) {
				$users = UserModel::model()->findAll( Array(
					'with' => 'groups',
					'order' => '`t`.`createdDT` ASC, `t`.`id` ASC',
				));
				foreach( $users as $user ) {
					$assoc = $user->toAssoc( null, Array( 'id', 'createdDT' ));
					$assoc[ 'groups' ] = $user->getIDsGroups();
					$data[ 'users' ][] = $assoc;
				}
			}
			
			return self::STATUS_OK;
		}
	}

?>