<?
	Yii::import( 'components.base.ComponentBase' );
	
	final class CalendarEventApiComponent extends ComponentBase {
			// params
		public $formatRender;
		public $limitPage;
		public $page;
		public $dumpResult;
		public $ids;
		public $serious;
		public $languages;
		public $idsLanguages;
		public $idsIndicators;
		public $fields;
		public $unstructI18N;
		public $date;
		public $begin;
		public $end;
		public $sort;
		public $group;
			// common
		const modelName = 'CalendarEventModel';
		const I18NModelName = 'CalendarEventI18NModel';
		const limitPageDef = 1000;
		const version = "1.0";
			// common
		private function getParam( $key, $def = null, $data = null ) {
			if( isset( $this->$key )) return @$this->$key;
			if( $data === null ) $data = $_GET;
			if( @$data[ $key ]) return @$data[ $key ];
			return $def; 
		}
		private function getArrayParam( $key, $def = null, $data = null ) {
			$value = $this->getParam( $key, $def, $data );
			if( $value ) {
				if( !is_array( $value )) {
					$value = explode( ",", $value );
					$value = array_map( 'trim', $value );
				}
			}
			else
				$value = Array();
			
			return $value;
		}
		function getColumnNames( $modelName ) {
			return CActiveRecord::model( $modelName )->getMetaData()->tableSchema->getColumnNames();
		}
		function getTotalFields() {
			$fieldsEvents = $this->getColumnNames( self::modelName );
			$fieldsI18NEvents = $this->getColumnNames( self::I18NModelName );
			return array_merge( $fieldsEvents, $fieldsI18NEvents );
		}
			// params
		function getFormatRender() {
			return $this->getParam( 'formatRender', 'json' );
		}
		function getLimitPage() {
			$limitPage = $this->getParam( 'limitPage', self::limitPageDef );
			$limitPage = (int)$limitPage;
			$limitPage = CommonLib::minimax( $limitPage, 1, self::limitPageDef );
			return $limitPage;
		}
		function getPage() {
			$page = $this->getParam( 'page', 0 );
			$page = (int)$page;
			$page = max( $page, 0 );
			return $page;
		}
		function getDumpResult() {
			return $this->getParam( 'dumpResult' );
		}
		function getIDs() {
			$ids = &$this->ids;
			if( $ids === null ) 
				$ids = $this->getArrayParam( 'ids' );
			
			return $ids;
		}
		function getSerious() {
			$serious = &$this->serious;
			if( $serious === null ) 
				$serious = $this->getArrayParam( 'serious' );
			return $serious;
		}
		function getLanguages() {
			$languages = &$this->languages;
			if( $languages === null ) {
				$languages = $this->getArrayParam( 'languages' );
				
				$aliases = LanguageModel::getExistsAliases();
				$languages = array_intersect( $languages, $aliases );
				
				if( !$languages )
					$languages = $aliases;
					
				if( $this->getUnstructI18N()) {
					$languages = array_slice( $languages, 0, 1 );
				}
			}
			return $languages;
		}
		function getIDsLanguages() {
			$ids = &$this->idsLanguages;
			if( $ids === null ) {
				$ids = Array();
				$languages = $this->getLanguages();
				if( $languages ) {
					$modelsLanguages = LanguageModel::getAll();
					foreach( $modelsLanguages as $modelLanguages ) {
						if( in_array( $modelLanguages->alias, $languages )) {
							$ids[] = $modelLanguages->id;
						}
					}
				}
			}
			return $ids;
		}
		function getIDsIndicators() {
			$ids = &$this->idsIndicators;
			if( $ids === null ) 
				$ids = $this->getArrayParam( 'idsIndicators' );
			return $ids;
		}
		function getFields() {
			$fields = &$this->fields;
			if( $fields === null ) {
				$fields = $this->getArrayParam( 'fields' );
				
				$totalFields = $this->getTotalFields();
				$fields = array_intersect( $fields, $totalFields );
				
				if( !$fields )
					$fields = Array( '*' );
			}
			return $fields;
		}
		function getUnstructI18N() {
			return $this->getParam( 'unstructI18N' );
		}
		function getDate() {
			return $this->getParam( 'date' );
		}
		function getBegin() {
			return $this->getParam( 'begin' );
		}
		function getEnd() {
			return $this->getParam( 'end' );
		}
		function getSort() {
			$sort = &$this->sort;
			if( $sort === null ) {
				$sort = $this->getArrayParam( 'sort' );
								
				$totalFields = $this->getTotalFields();
				$types = Array( 'ASC', 'DESC' );
				
				$filter = function( $el ) use( $totalFields, $types ) {
					$ex = explode( " ", $el, 2 );
					$ex = array_map( 'trim', $ex );
					$ex = array_filter( $ex );
					
					if( count( $ex ) == 1 ) {
						list( $field ) = $ex;
						return in_array( $field, $totalFields );
					}
					else{
						list( $field, $type ) = $ex;
						return in_array( $field, $totalFields ) and in_array( ucfirst( $type ), $types );
					}
				};
				
				$sort = array_filter( $sort, $filter );
				if( !$sort )
					$sort = Array( 'id' );
			}
			return $sort;
		}
		function getGroup() {
			$group = &$this->group;
			if( $group === null ) {
				$group = $this->getArrayParam( 'group' );
								
				$totalFields = $this->getTotalFields();
				$group = array_intersect( $group, $totalFields );
			}
			return $group;
		}
		function inFields( $field ) {
			$fields = $this->getFields();
			return in_array( '*', $fields ) or in_array( $field, $fields );
		}
		private function getDPListCriteria() {
			$self = $this;
						
			$getSelect = function() use( $self ){
				$fields = $self->getFields();
				$fieldsEvents = $self->getColumnNames( CalendarEventApiComponent::modelName );
				$totalFields = array_merge( $fieldsEvents, Array( '*' ));
				$fields = array_intersect( $fields, $totalFields );
				return $fields;
			};
			$getI18NCriteria = function() use( $self ) {
				$getSelect = function() use( $self ){
					$fields = $self->getFields();
					$fieldsI18NEvents = $self->getColumnNames( CalendarEventApiComponent::I18NModelName );
					$totalFields = array_merge( $fieldsI18NEvents, Array( '*' ));
					$fields = array_intersect( $fields, $totalFields );
					return $fields;
				};
				
				$c = new CDbCriteria();
				$c->select = $getSelect();
				$c->together = false;
				
				$idsLanguages = $self->getIDsLanguages();
				if( $idsLanguages ) 
					$c->addInCondition( "`i18nsCalendarEvent`.`idLanguage`", $idsLanguages );	
					
				return $c;
			};
			
			
			$c = new CDbCriteria();
			$c->select = $getSelect();
			$c->with[ 'i18ns' ] = $getI18NCriteria()->toArray();
				
				// conditions
			$ids = $this->getIDs();
			if( $ids ) 
				$c->addInCondition( " `t`.`id` ", $ids );
			
			
			$serious = $this->getSerious();
			if( $serious ) 
				$c->addInCondition( " `t`.`serious` ", $serious );
			
			
			$date = $this->getDate();
			if( $date ) {
				$c->addCondition( " DATE( `t`.`dt` ) = DATE( :date ) " );
				$c->params[ ':date' ] = $date;
			}
			
			$begin = $this->getBegin();
			if( $begin ) {
				$c->addCondition( " `t`.`dt` >= :begin " );
				$c->params[ ':begin' ] = $begin;
			}
			
			$end = $this->getEnd();
			if( $end ) {
				$c->addCondition( " `t`.`dt` <= :end " );
				$c->params[ ':end' ] = $end;
			}
			
			$idsIndicators = $this->getIDsIndicators();
			if( $idsIndicators ) 
				$c->addInCondition( " `t`.`indicator_id` ", $idsIndicators );
			
				// group
			$group = $this->getGroup();
			if( $group )
				$c->group = implode( ',', $group );
			
				// order
			$sort = $this->getSort();
			$c->order = implode( ',', $sort );
			
			return $c;
		}
		private function getDPList() {
			$limitPage = $this->getLimitPage();
			$page = $this->getPage();
						
			$DP = new CalendarEventApiListDP( self::modelName, Array(
				'criteria' => $this->getDPListCriteria(),
			));
			
			$DP->pagination->setPageSize( $limitPage );
			$DP->pagination->setCurrentPage( $page );
			
			return $DP;
		}
		function exportList() {
			$output = CalendarEventApiOutput::instance( 'list', $this );
			
			$DP = $this->getDPList();
			$data = $DP->getData();
			
			$output->appendAttribute( 'limitPage', $DP->pagination->getPageSize() );
			$output->appendAttribute( 'page', $this->getPage() );
			$output->appendAttribute( 'languages', implode( ', ', $this->getLanguages()));
			
			if( !$this->getGroup()) {
				$output->appendAttribute( 'countPages', $DP->pagination->getPageCount() );
				$output->appendAttribute( 'totalCountEvents', (int)$DP->pagination->getItemCount() );
			}
			$output->appendAttribute( 'fields', implode( ', ', $this->getFields()));
			$output->appendAttribute( 'sort', implode( ', ', $this->getSort()));
			
			if( $this->getGroup()) {
				$output->appendAttribute( 'group', implode( ', ', $this->getGroup()));
			}
			
			if( $this->getPage() == $DP->pagination->getCurrentPage()) {
				foreach( $data as $event ) {
					$output->addEvent( $event );
				}
			}

			$output->render();
		}
	}
	
	
	abstract class CalendarEventApiOutput {
		protected $api;
		static function instanceList( $api ) {
			switch( $api->getFormatRender() ) {
				case 'xml':{
					return new CalendarEventApiOutputListXML( $api );
					break;
				}
				case 'json':
				default: {
					return new CalendarEventApiOutputListJson( $api );
					break;
				}
			}
		}
		static function instance( $type, $api ) {
			switch( $type ) {
				case 'list':{
					return self::instanceList( $api );
					break;
				}
			}
		}
		function __construct( $api ) {
			$this->api = $api;
		}
		abstract function appendAttribute( $key, $value );
		abstract function render();
	}
	
	abstract class CalendarEventApiOutputList extends CalendarEventApiOutput{
		function addEvent( $event ) {
			$fields = $this->api->getColumnNames( CalendarEventApiComponent::modelName );
			$fields = array_diff( $fields, Array( 'fact_color', 'fact_color2', 'smq' ));
			
			$fieldsI18N = $this->api->getColumnNames( CalendarEventApiComponent::I18NModelName );
			$fieldsI18N = array_diff( $fieldsI18N, Array( 'idEvent', 'idLanguage', 'idCountry', 'before_value_sec', 'mb_value_sec', 'fact_value_sec' ));
			
			$unstructI18N = $this->api->getUnstructI18N();

			$languages = LanguageModel::getAll();
			$languages = CommonLib::toAssoc( $languages, 'id', 'alias' );
			
			$newEvent = $this->createEvent();
						
			foreach( $fields as $key ) {
				if( !$this->api->inFields( $key )) continue;
				$inted = in_array( $key, Array( 'id', 'serious', 'indicator_id' ));
				$this->appendEventAttribute( $newEvent, $key, $inted ? (int)$event->$key : $event->$key );
			}
			
			foreach( $event->i18ns as $i18n ) {
				$alias = $languages[ $i18n->idLanguage ];
				
				foreach( $fieldsI18N as $key ) {
					if( !$this->api->inFields( $key )) continue;
					if( $unstructI18N ) {
						$this->appendEventAttribute( $newEvent, $key, $i18n->$key );
					}
					else {
						$this->appendEventI18NAttribute( $newEvent, $alias, $key, $i18n->$key );
					}
				}
			}
			
			$this->appendEvent( $newEvent );
		}
		abstract protected function createEvent();
		abstract protected function appendEvent( $event );
		abstract protected function appendEventAttribute( $event, $key, $value );
		abstract protected function appendEventI18NAttribute( $event, $alias, $key, $value );
	}
	
	final class CalendarEventApiOutputListJson extends CalendarEventApiOutputList{
		private $object;
		function __construct( $api ) {
			parent::__construct( $api );
			$this->object = new StdClass();
		}
		function appendAttribute( $key, $value ) {
			$this->object->$key = $value;
		}
		protected function createEvent() {
			return new StdClass();
		}
		protected function appendEvent( $event ) {
			$this->object->events[] = $event;
		}
		protected function appendEventAttribute( $event, $key, $value ) {
			$event->$key = $value;
		}
		protected function appendEventI18Ns( $event ) {
			if( !isset( $event->i18ns )) 
				$event->i18ns = Array();
		}
		protected function appendEventI18NAlias( $event, $alias ) {
			$this->appendEventI18Ns( $event );
			if( !isset( $event->i18ns[ $alias ])) 
				$event->i18ns[ $alias ] = new StdClass();
		}
		protected function appendEventI18NAttribute( $event, $alias, $key, $value ) {
			$this->appendEventI18NAlias( $event, $alias );
			$event->i18ns[ $alias ]->$key = $value;
		}
		function render() {
			if( $this->api->getDumpResult()) {
				var_dump( $this->object );
			}
			else{
				header( "Content-Type: application/json" );
				echo json_encode( $this->object );
			}
		}
	}
	
	final class CalendarEventApiOutputListXML extends CalendarEventApiOutputList{
		private $XML;
		private $nodeRoot;
		private $nodeEvents;
		function __construct( $api ) {
			parent::__construct( $api );
			$this->XML = new DOMDocument( "1.0", "UTF-8" );
			
			$this->nodeRoot = $this->XML->appendChild( $this->createElement( 'listEvents' ));
		}
		protected function trValue( $value ) {
			return strtr( $value, Array(
				'&' => '&amp;',
			));
		}
		protected function createElement( $name, $value = null ) {
			if( $value !== null ) {
				$value = $this->trValue( $value );
				return $this->XML->createElement( $name, $value );
			}
			return $this->XML->createElement( $name );
		}
		function appendAttribute( $key, $value ) {
			$this->nodeRoot->appendChild( $this->createElement( $key, $value ) );
		}
		protected function createEvent() {
			return $this->createElement( 'event' );
		}
		protected function appendEvent( $event ) {
			if( !$this->nodeEvents ) {
				$this->nodeEvents = $this->nodeRoot->appendChild( $this->createElement( 'events' ));
			}
			$this->nodeEvents->appendChild( $event );
		}
		protected function appendEventAttribute( $event, $key, $value ) {
			$event->appendChild( $this->createElement( $key, $value ));
		}
		protected function getEventI18NNode( $event ) {
			$i18nss = $event->getElementsByTagName( 'i18ns' );
			if( $i18nss->length ) 
				return $i18nss->item(0);
			return $event->appendChild( $this->createElement( 'i18ns' ));
		}
		protected function getEventI18NNodeAlias( $event, $alias ) {
			$nodeI18N = $this->getEventI18NNode( $event );
			$aliases = $event->getElementsByTagName( $alias );
			if( $aliases->length ) 
				return $aliases->item(0);
			return $nodeI18N->appendChild( $this->createElement( $alias ));
		}
		protected function appendEventI18NAttribute( $event, $alias, $key, $value ) {
			$nodeAlias = $this->getEventI18NNodeAlias( $event, $alias );
			$nodeAlias->appendChild( $this->createElement( $key, $value ) );
		}
		function render() {
			if( $this->api->getDumpResult()) {
				$this->XML->formatOutput = true;
			}
			header( "Content-Type: application/xml" );
			echo $this->XML->saveXML();
		}
	}
	
	
	class CalendarEventApiListDP extends CActiveDataProvider{
		//protected function calculateTotalItemCount(){
		//	return 0;
		//}
	}
?>