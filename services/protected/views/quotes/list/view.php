<?
Yii::import('controllers.base.ViewBase');
?>
<script>
	var nsActionView = {};
	var nsActionView2 = {};
</script>

<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<h1>
			<?= Yii::t( "quotesWidget",'Котировки') ?>
		</h1>
		<div class="iClear"></div>
		<p>
			<?=Yii::t('*','[Text for Quotes]') ?>
		</p>
		<div class="iClear"></div>
		<div>
			<? $this->widget('widgets.quotes.QuotesWidget',Array('view' => 'list','symbols_model'=>$symbols_model)); ?>
		</div>
	</div>
</div>