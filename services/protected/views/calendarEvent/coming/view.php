<div class="page-header border_n" style="margin-bottom:0px;">
	<h1><?=Yii::t( '*', 'Upcoming events' )?></h1>
</div>
<? foreach($model as $one): ?>
<div class="events_main clearfix">
	<div class="events_main_item clearfix d_ib pull-left">
		<?=$one->formatName()?>
	</div>
	<div class="events_main_item2 clearfix pull-left">
		<span class="d_ib events_span pull-left"><?=Yii::t( '*', 'Remaining' )?>:
			<span class="tab_text"><?=$one->getTimeLeft()?></span>
		</span>
	</div>
</div>
<? endforeach; ?>

<div 
	id="c_loader"
	class="iDummyReload i02"
	style="height:20px;margin:10px 0; position:relative; display:none;"
></div>

<button 
	id="coming_more_btn"
	onclick="coming.render(<?=$limit?>); return false;"
	class="btn btn-small btn-yellow no-radius wShowMore"
>
	<span class="hidden-phone"><?=Yii::t( '*', 'Show more' )?></span>
</button>
