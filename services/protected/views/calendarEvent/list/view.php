<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'calendarEvent/list/view' );
$watchNow = UserRequestModel::getCountSessions( Array( 
	Array(
		'instance' => Array( 'calendarEvent/list', 'calendarEvent/single' ),
	),
));
?>

<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<section class="section_offset"> 
	<hr>
	<h1 class="page_title1 calendar_title clearfix" itemprop="name">
		<?=$this->action->title?> <?php //if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?>
		<p class="support_txt">
		<?php
			if( $settings->brokerId ){
				$broker = BrokerModel::model()->findByPk($settings->brokerId);
				if( $broker ){
					echo Yii::t( '*', "Supported by" ) . ' ';
					echo Chtml::link($broker->singleTitle, $broker->singleURL, array('class' => 'support_link'));
					echo Chtml::openTag('span', array('class' => 'support_img'));
						echo $broker->getThumbImage( array('width' => 31, 'height' => 31) );
					echo Chtml::closeTag('span');
				}
			}
		?>
		</p>
	</h1>
	<?php 
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; 
		
		if($settings->firstDesc) echo CHtml::tag('p', array( 'class' => 'bold_txt1' ), $settings->firstDesc);
		if($settings->secondDesc) echo CHtml::tag('p', array( 'class' => 'normal_txt1' ), $settings->secondDesc);
	?>
	<div class="nsActionView">
		<?php /*<div class="wiev_page clearfix">
			<p class="wiev_page_txt clearfix">
				<i class="fa fa-eye wiev_page_icon" aria-hidden="true"></i>
				<?=Yii::t( '*', 'Calendar watch now: <strong><span class="count_wiev">{n}</span> people</strong>', Array( '{n}' => $watchNow ))?>
			</p>
			<a class="yellow_btn" href="<?=Yii::App()->createURL( 'question/list', Array( 'id' => UserMessageModel::ID_QUESTION_LIST_CALENDAR_EVENTS ))?>"><?=Yii::t( '*', 'Questions and suggestions' )?></a>
		</div>*/?>
		
		<?php
			$this->widget( 'widgets.forms.CalendarGroupSubscriptionFormWidget', Array(
				'ns' => 'nsActionView',
			));

			$this->widget( 'widgets.ExportCalendarEventsWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
		
		<?php
			$this->widget( 'widgets.lists.CalendarEventsList2Widget', Array(
				'ns' => 'nsActionView',
			));
		?>
		
		<?php
			$cacheKey = 'calendarEventCommonList' . Yii::app()->language;
			if( !Yii::app()->cache->get( $cacheKey ) ){
				ob_start();	
				CommonLib::loadWp();
				require_once Yii::App()->params['wpThemePath'].'/templates/calendar-event-common-news.php';	
				$blockContent = ob_get_clean();
				Yii::app()->cache->set( $cacheKey, $blockContent, 60*60 * 6);
			}
			echo Yii::app()->cache->get( $cacheKey );
		?>
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
		
		<?=Yii::t( $NSi18n, 'faq_conomic_calendar' )!='faq_conomic_calendar' ? Yii::t( $NSi18n, 'faq_conomic_calendar' ) : ''?>
		
		<? $this->widget('widgets.lists.DeCommentsListWidget',Array( 'ns' => 'nsActionView', 'cat' => 'singleClendarEvent', 'modelName' => 'CalendarEvent2Model', 'limit' => 10, 'label'  => Yii::t('*', 'Recent comments') )); ?>

	</div>
</section>