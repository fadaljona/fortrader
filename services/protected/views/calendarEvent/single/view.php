<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'calendarEvent/single/view' );
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<section class="section_offset">
	<h1 class="page_title1 title_flag clearfix" itemprop="name">
		<?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?>
		<?php if( $event->country ){ echo CHtml::tag('span', array('class' => 'title_flag_img'), $event->country->getImgFlag('shiny', 48)); }?>
	</h1>
	
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<div class="nsActionView">
	
		<?php
			$this->widget( 'widgets.forms.CalendarSingleSubscriptionFormWidget', Array(
				'ns' => 'nsActionView',
				'model' => $event,
			));
		?>
	
		<?php
			$this->widget( 'widgets.CalendarEventProfileWidget', Array(
				'ns' => 'nsActionView',
				'model' => $event,
			));
		?>

		<?php
			$cacheKey = 'calendarEventSingleList' . $event->id  . 'lang' . Yii::app()->language;
			if( !Yii::app()->cache->get( $cacheKey ) ){
				ob_start();	
				CommonLib::loadWp();
				require_once Yii::App()->params['wpThemePath'].'/templates/calendar-event-single-news.php';	
				$blockContent = ob_get_clean();
				Yii::app()->cache->set( $cacheKey, $blockContent, 60*60 * 6);
			}
			echo Yii::app()->cache->get( $cacheKey );
		?>
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
		
		<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
		
		<?php if($event->fullDesc) echo $event->fullDesc; ?>
	</div>
</section>