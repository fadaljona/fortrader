<table id="hist_res" class="table_main pull-left" style="margin-top: 3px;">
	<thead class="table_head">
		<tr>
			<th colspan="3"><?=Yii::t( '*', 'History' )?></th>
		</tr>
	</thead>
	<tbody class="tbody_history">
		<tr class="table_head_history">
			<td><?=Yii::t( '*', 'Period' )?></td>
			<? if($s != ''): ?>
			<td><?=$model0->getTitleFirst()?></td>
			<? endif; ?>
			<? if($s1 != ''): ?>
			<td><?=$model0->getTitleSel()?></td>
			<? endif; ?>
		</tr>
		<? foreach($model as $one): ?>
			<? if($s != '' and $one->getFactValue() != ''): ?>
				<tr class="cf4f4f4">
					<td class="tab_month tab_text"><?=$one->getPeriod()?></td>
					<td class="tab_number"><?=$one->getFactValue()?></td>
				</tr>
			<? endif; ?>
			<? if($s1 != '' and $one->getFactValueSel() != ''): ?>
				<tr class="cf4f4f4">
					<td class="tab_month tab_text"><?=$one->getPeriod()?></td>
					<td class="tab_text"><?=$one->getFactValueSel()?></td>
				</tr>
			<? endif; ?>
		<? endforeach; ?>
		<tr class="tr_th_links">
			<th colspan="3">
				<div 
					id="h_loader"
					class="iDummyReload i02"
					style="height:20px;margin:10px 0; position:relative; display:none;"
				></div>
				<a 
					id="history_more_btn" 
					href="#"
					onclick="cahistory.render(<?=$limit?>); return false;"
				><?=Yii::t( '*', 'Show more' )?></a>
			</th>
		</tr>
	</tbody>
</table>
