<?
Yii::import('controllers.base.ViewBase');
Yii::App()->clientScript->registerScriptFile( CHtml::asset( Yii::getPathOfAlias('webroot.js').'/autobahn.min.js' ), ClientScript::POS_TOP_HEAD );

if ($symbol->sourceType != 'yahoo') {
	$quotesSettings = QuotesSettingsModel::getInstance();
	
	Yii::app()->clientScript->registerScript('InitTickSession', "

	var tickSessionOpend = new Event('tickSessionOpend');
	
	var tickSession = new ab.Session('".$quotesSettings->wsServerUrl."',
		function() {
			console.warn('Ws tick session opened');
			
			document.addEventListener('DOMContentLoaded', function(){
				document.dispatchEvent(tickSessionOpend);
			});
		},
		function() {
			console.warn('Ws tick session closed');
		},
		{'skipSubprotocolCheck': true}
	);

	", ClientScript::POS_TOP_HEAD);
}else{
	Yii::app()->clientScript->registerScript('InitTickSession', "var tickSession = '';", ClientScript::POS_TOP_HEAD);
}
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?></h1>
	<hr class="separator1">
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<div class="nsActionView">
		<? $this->widget('widgets.SingleQuoteWidget',Array( 'ns' => 'nsActionView', 'model' => $symbol )); ?>
		<? //$this->widget('widgets.forms.QuoteForecastFormWidget',Array( 'ns' => 'nsActionView', 'symbol' => $symbol )); ?>
		
		<? $this->widget('widgets.QuoteChartsWidget',Array( 'ns' => 'nsActionView', 'model' => $symbol )); ?>
		
		<?php
			if( $symbol->wpTerm ){
				if( !Yii::app()->cache->get( 'quotesWpPostBlock'.$symbol->id ) ){
					ob_start();
					
					CommonLib::loadWp();
					require_once Yii::App()->params['wpThemePath'].'/templates/quotes-tags-block.php';
					
					$quotesWpPostBlock = ob_get_clean();
					Yii::app()->cache->set('quotesWpPostBlock'.$symbol->id, $quotesWpPostBlock, 60*60);
				}
				echo Yii::app()->cache->get( 'quotesWpPostBlock'.$symbol->id );
			}
		?>
		
		
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
	
		<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat, 'postId' => 0, 'chart' => 1 )); ?></div>

        <?php if( $symbol->desc ){ ?>
			<p itemprop="text"><?=$symbol->desc?></p>
		<?php } ?>
        
		<? $this->widget('widgets.YahooPopularQuotesWidget',Array( 'title'  => Yii::t('*', 'Popular quotes') )); ?>
	</div>
	
	<?php if(Yii::app()->language == 'ru'){?>
	<div id="1480603085687"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1480603085687");s = document.createElement("script");s.src = "//fortrader.org/services/15135sz/jsRender?id=45&insertToId=1480603085687";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
	<?php } ?>
	
</section>