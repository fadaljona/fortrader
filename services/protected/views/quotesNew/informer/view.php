<? Yii::import('controllers.base.ViewBase');?>
<script>
	var nsActionView = {};
</script>

<hr>
<section class="section_offset"> 
	<h1 class="page_title1"><?=$this->action->title?><i class="fa fa-info-circle"></i></h1>
	<hr class="separator1">
	
	<div class="nsActionView">
		<? $this->widget('widgets.QuotesInformerSettingsWidget',Array( 'ns' => 'nsActionView')); ?>
	</div>
	
	<?php
		$informerDesc = Yii::t('*', 'quotesInformerDescText');
		if( $informerDesc == 'quotesInformerDescText' ) $informerDesc = false;
		if( $informerDesc ){
			echo Chtml::tag('div', array('class' => 'section_offset'), $informerDesc);
		}
	?>

</section>