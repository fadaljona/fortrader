<?
Yii::import('controllers.base.ViewBase');
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 

<hr>
<section class="section_offset"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?></h1>
	<hr class="separator1">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php 
		if( $model->shortDesc ){
			echo '<p>'.$model->shortDesc.'</p>';
		}
	?>
	
	<div class="nsActionView">
	
		<? $this->widget('widgets.SearchQuotesWidget',Array( 'ns' => 'nsActionView', 'minInputChars' => 3 )); ?>
		<? $this->widget('widgets.NewQuotesWidget',Array( 'ns' => 'nsActionView', 'catId' => $model->id )); ?>
		
		<?php 
			if( $model->fullDesc ){
				echo '<p itemprop="text">'.$model->fullDesc.'</p>';
			}
		?>
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
		
		
		
	</div>
	
	<?php if(Yii::app()->language == 'ru'){?>
	<div id="1480603054856"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1480603054856");s = document.createElement("script");s.src = "//fortrader.org/services/15135sz/jsRender?id=45&insertToId=1480603054856";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
	<?php } ?>
	
</section>