<?
Yii::import('controllers.base.ViewBase');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl .'/css/wNewQuotesWidget.css');


?>
<script>
	var nsActionView = {};
</script>

<hr>
<section class="section_offset"> 
	<h1 class="page_title1"><?=$this->action->title?></h1>
	<hr class="separator1">
	
	<div class="nsActionView">
		<? $this->widget('widgets.lists.TradersRatingListWidget',Array( 'ns' => 'nsActionView')); ?>
		<? $this->widget('widgets.DeCommentsWidget', Array( 'ns' => 'nsActionView', 'cat' => 'tradersRatingQuote' )); ?>
	</div>


</section>