<?
Yii::import('controllers.base.ViewBase');
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 

<hr>
<section class="section_offset"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?></h1>
	<hr class="separator1">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if($this->action->shortDesc){?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$this->action->shortDesc?>
		</blockquote>
	</div>
	<?php }?>
	
	<div class="nsActionView">
	
		<? $this->widget('widgets.SearchQuotesWidget',Array( 'ns' => 'nsActionView', 'minInputChars' => 3 )); ?>
		<? $this->widget('widgets.NewQuotesWidget',Array( 'ns' => 'nsActionView', )); ?>
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
		
		<?php 
			if( $this->action->desc ){
				echo '<div class="section_offset" itemprop="text">'.$this->action->desc.'</div>';
			}
		?>
		
		<? $this->widget('widgets.lists.DeCommentsListWidget',Array( 'ns' => 'nsActionView', 'cat' => 'singleQuote', 'modelName' => 'QuotesSymbolsModel', 'limit' => 10, 'label'  => Yii::t('*', 'Recent comments') )); ?>
		
	</div>
	
	<?php if(Yii::app()->language == 'ru'){?>
	<div id="1480603085687"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1480603085687");s = document.createElement("script");s.src = "//fortrader.org/services/15135sz/jsRender?id=45&insertToId=1480603085687";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
	<?php } ?>
	
</section>