<?
Yii::import('controllers.base.ViewBase');
?>
<script>
	var nsActionView = {};
</script>


<hr>
<section class="section_offset"> 
	<h1 class="page_title1"><?=$this->action->title?></h1>
	<hr class="separator1">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<div class="nsActionView">
		<? $this->widget('widgets.SearchQuotesWidget',Array( 'ns' => 'nsActionView', 'minInputChars' => 3 )); ?>
		<? $this->widget('widgets.lists.OtherQuotesListWidget',Array( 'ns' => 'nsActionView' )); ?>
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
		
		<?php 
			if( $this->action->desc ){
				echo '<p>'.$this->action->desc.'</p>';
			}
		?>
	</div>
</section>