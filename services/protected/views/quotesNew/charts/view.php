<?
Yii::import('controllers.base.ViewBase');
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?></h1>
	<hr class="separator1">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if($this->action->shortDesc){?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$this->action->shortDesc?>
		</blockquote>
	</div>
	<?php }?>
	
	<div class="nsActionView">
		<? $this->widget('widgets.SearchQuotesWidget',Array( 'ns' => 'nsActionView', 'minInputChars' => 3 )); ?>
		<? $this->widget('widgets.lists.QuotesChartListWidget',Array( 'ns' => 'nsActionView' )); ?>
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
		
		<?php 
			if( $this->action->desc ){
				echo '<p itemprop="text">'.$this->action->desc.'</p>';
			}
		?>
	</div>
</section>