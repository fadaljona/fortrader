<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'contest/registerMember/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<?
	$this->widget('widgets.wizards.RegistrationContestMemberWizardWidget', Array(
		'ns'=>'nsActionView',
		'contest'=>$modelContest,
		'UMode'=>$UMode
	));
	?>
</div>
