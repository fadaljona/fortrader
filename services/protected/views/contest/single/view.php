<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'contest/single/view' );
    $contestStatus = $contest->getStatus();
?>
<script>
	var nsActionView = {};
	var nsActionView2 = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<div class="section_offset">
	<div class="clearfix">
		<div class="box1">
			<h1 class="page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?></h1>
		</div><!--/ .box1 -->
		<a href="#" class="competition_btn alignright arcticmodal" data-modal="#conductCompetition"><?=Yii::t( $NSi18n, 'Conduct competition' )?><span><?=Yii::t( $NSi18n, 'for companies' )?></span></a>
	</div>
</div>

<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>

<?php if( $contest->currentLanguageI18N->getHTMLDescription() ){?>
<div class="section_offset">
	<blockquote class="thesis2">
		<?=$contest->currentLanguageI18N->getHTMLDescription()?>
	</blockquote>
</div>
<?php }?>

<div class="nsActionView">
	<?php
		$this->widget( 'widgets.forms.FeedbackFormWidget', Array(
			'ns' => 'nsActionView',
			'mode' => 'contest'
		));
		
		echo CHtml::openTag('div', array( 'itemscope' => true, 'itemtype' => 'http://schema.org/BusinessEvent' ));
			echo '<meta itemprop="startDate" content="'.$contest->begin.'" />';
			echo '<meta itemprop="endDate" content="'.$contest->end.'" />';
			echo '<meta itemprop="description" content="'.$contest->description.'" />';
			echo '<div itemprop="location" itemscope itemtype="http://schema.org/Place"><meta itemprop="telephone" content="'.WpOptionsModel::getOptionVal('fortraderTelephone').'" /><meta itemprop="address" content="ForTrader" /><meta itemprop="name" content="ForTrader" /></div>';
			
			$this->widget( 'widgets.LastContestWidget', Array(
				'ns' => 'nsActionView',
				'model' => $contest,
				'mode' => 'single',
			));
		echo CHtml::closeTag('div');
		
		$this->widget( 'widgets.TradingConditionsWidget', Array(
			'ns' => 'nsActionView',
			'rules' => $contest->rules
		));
		/*$this->widget( 'widgets.OtherContestsSliderWidget', Array(
			'ns' => 'nsActionView',
			'excludeModel' => $contest->id
		));*/
		$this->widget( 'widgets.lists.CurrentUserContestMemberInfoListWidget', Array(
			'ns' => 'nsActionView',
			'ajax' => false,
			'idContest' => $contest->id
		));	
		$this->widget('widgets.wizards.RegistrationContestMemberWizardWidget', Array(
		'ns'=>'nsActionView',
			'contest'=>$contest,
		));
		$this->widget( 'widgets.lists.ContestMembersListWidget', Array(
			'ns' => 'nsActionView',
			'ajax' => true,
			'showOnEmpty' => true,
			'idContest' => $contest->id
		));
	?>
	<div id="1480602993040"></div><script type="text/javascript">(function(s, t) {t = document.getElementById("1480602993040");s = document.createElement("script");s.src = "//fortrader.org/services/15135sz/jsRender?id=47&insertToId=1480602993040";s.type = "text/javascript";s.async = true;t.parentNode.insertBefore(s, t);})(window, document );</script>
	<div id="comments"><?php $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
</div>