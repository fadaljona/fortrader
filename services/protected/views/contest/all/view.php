<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'contest/all/view' );
?>
<script>
	var nsActionView = {};
	var nsActionView2 = {};
</script>

<div class="section_offset">
	<div class="clearfix">
		<div class="box1">
			<h1 class="page_title1"><?=$this->action->title?><i class="fa fa-trophy"></i></h1>
		</div><!--/ .box1 -->
		<a href="#" class="competition_btn alignright arcticmodal" data-modal="#conductCompetition"><?=Yii::t( $NSi18n, 'Conduct competition' )?><span><?=Yii::t( $NSi18n, 'for companies' )?></span></a>
	</div>
</div>

<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>

<?php if( $settings->shortDesc ){?>
<div class="section_offset">
	<blockquote class="thesis2">
		<?=$settings->allShortDesc?>
	</blockquote>
</div>
<?php }?>

<div class="nsActionView">

	<?php
		$this->widget( 'widgets.forms.FeedbackFormWidget', Array(
			'ns' => 'nsActionView',
			'mode' => 'contest'
		));

		$this->widget( 'widgets.lists.ContestsListWidget', Array(
			'ns' => 'nsActionView',
			'ajax' => true,
			'showOnEmpty' => true,
		));

		
		$this->widget( 'widgets.ForeignContestsWidget', Array(
			'ns' => 'nsActionView',
		));
		
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php';
		
		if( $settings->allFullDesc ){
			echo $settings->allFullDesc;
		}
	?>

</div>