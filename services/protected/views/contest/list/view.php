<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'contest/list/view' );
?>
<script>
	var nsActionView = {};
	var nsActionView2 = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<div class="nsActionView">
	<div class="fx_section marginBottom20">
		<div class="fx_content-title16 flex_title clearfix">
			<span class="fx_content-title-link"><?=$this->action->title?> <span class="count_red"><?=$contestsCount?></span></span>
			<a href="#" class="content-title-btn button-red arcticmodal" data-modal="#conductCompetition"><?=Yii::t($NSi18n, 'Conduct competition')?></a>
		</div>
		
		<div class="fx_section-box25 fx_section-exchange clearfix">
			<div class="contest-table__box">
				<?php $this->widget( 'widgets.lists.ContestsTradersListWidget', Array(
					'ns' => 'nsActionView',
					'ajax' => true,
					'showOnEmpty' => true,
				));?>
			</div>
		</div>
	</div>

	<div class="fx_section">
		<div class="fx_content-title16 flex_title clearfix">
			<span class="fx_content-title-link">
				<?=Yii::t( $NSi18n, 'Table contests friendly resources' )?>
				<span class="count_red"><?=$foreignContestsCount?></span>
			</span>
			<a href="#" class="content-title-btn button-red"><?=Yii::t($NSi18n, 'Add')?></a>
		</div>

		<div class="fx_section-box25 fx_section-exchange clearfix">
			<div class="contest-table__box">
				<?php $this->widget( 'widgets.lists.ContestsPartnersListWidget', Array(
					'ns' => 'nsActionView',
					'ajax' => true,
					'showOnEmpty' => true,
				));?>
			</div>
		</div>
	</div>
</div>

<?php /*<div class="section_offset">
	<div class="clearfix">
		<div class="box1">
			<h1 class="page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?><i class="fa fa-trophy"></i></h1>
		</div><!--/ .box1 -->
		<a href="#" class="competition_btn alignright arcticmodal" data-modal="#conductCompetition"><?=Yii::t( $NSi18n, 'Conduct competition' )?><span><?=Yii::t( $NSi18n, 'for companies' )?></span></a>
	</div>
</div>*/?>

<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>

<?php /*if( $settings->shortDesc ){?>
<div class="section_offset">
	<blockquote class="thesis2">
		<?=$settings->shortDesc?>
	</blockquote>
</div>
<?php }*/?>

<div class="nsActionView">
	<?php
		/*$this->widget( 'widgets.forms.FeedbackFormWidget', Array(
			'ns' => 'nsActionView',
			'mode' => 'contest'
		));*/

		/*$this->widget( 'widgets.LastContestWidget', Array(
			'ns' => 'nsActionView',
			'model' => $model
		));*/
		/*$this->widget( 'widgets.OtherContestsSliderWidget', Array(
			'ns' => 'nsActionView',
			'excludeModel' => $model->id
		));*/
		/*$this->widget( 'widgets.lists.ContestMembersRatingListWidget', Array(
			'ns' => 'nsActionView',
			'ajax' => true,
			'showOnEmpty' => true,
		));*/
		
		/*$this->widget( 'widgets.ForeignContestsWidget', Array(
			'ns' => 'nsActionView',
		));*/
		
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php';
		
		//if( $settings->fullDesc ){ echo CHtml::tag( 'div', array( 'itemprop' => 'text' ), $settings->fullDesc ); }
	?>
	<div id="comments"><?php //$this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
</div>