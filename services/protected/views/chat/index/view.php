<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'chat/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<h1>
			<?=Yii::t( $NSi18n, 'Chat' )?>
		</h1>
	</div>
	<div class="row-fluid">
		<?
			$this->widget( 'widgets.ChatWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
	</div>
</div>