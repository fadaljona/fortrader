<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'default/index/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<div class="pull-left">
			<?
				$this->widget( 'widgets.CountersWidget', Array(
					'counters' => Array( 'Ya.share' ),
				));
			?>
		</div>
		<a class="btn btn-small btn-yellow pull-right" href="<?=Yii::App()->createURL( 'question/list' )?>"><?=Yii::t( '*', 'Questions and suggestions' )?></a>
	</div>

	<div class="wHomeBlocks">
		<div class="row-fluid">
			<div class="iDiv i05">
				<?$this->widget( 'widgets.homeBlocks.JournalsBlockWidget', Array( 'ns' => 'nsActionView' ));?>
				<div class="iDiv i08"></div>
				<?$this->widget( 'widgets.homeBlocks.ContestsBlockWidget', Array( 'ns' => 'nsActionView' ));?>
				<div class="iDiv i08"></div>

			</div>
			<div class="iDiv i05">
				<?$this->widget( 'widgets.homeBlocks.CalendarEventsBlockWidget', Array( 'ns' => 'nsActionView' ));?>
				<div class="iDiv i08"></div>
				<?$this->widget( 'widgets.homeBlocks.BrokersBlockWidget', Array( 'ns' => 'nsActionView' ));?>
				<div class="iDiv i08"></div>
			</div>
			<div class="iDiv i05">
				<?$this->widget( 'widgets.homeBlocks.UsersBlockWidget', Array( 'ns' => 'nsActionView' ));?>
				<div class="iDiv i08"></div>
				<?$this->widget( 'widgets.homeBlocks.EABlockWidget', Array( 'ns' => 'nsActionView' ));?><br>
				<div class="iDiv i08"></div>
			</div>
			<div class="iDiv i05">
				<?$this->widget( 'widgets.homeBlocks.ContactsBlockWidget', Array( 'ns' => 'nsActionView' ));?>
				<div class="iDiv i08"></div>
				<?if( Yii::App()->user->hasAdminRight()){?>
					<?$this->widget( 'widgets.homeBlocks.StrategiesBlockWidget', Array( 'ns' => 'nsActionView' ));?>
					<div class="iDiv i08"></div>
				<?}?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="iDiv i05">
				<?php
					$this->renderDynamic(
						'widget', 
						'widgets.homeBlocks.ActivityBlockWidget', 
						array( 'ns' => 'nsActionView' ), 
						true
					);
					//$this->widget( 'widgets.homeBlocks.ActivityBlockWidget', Array( 'ns' => 'nsActionView' ));
				?>
				<div class="iDiv i08"></div>
			</div>
		</div>
	</div>
</div>
<script>
	!function( $ ) {
		$( '.wHomeBlocks .wToggle' ).click( function() {
			var $i = $(this)
			var $a = $i.closest( 'a' );
			var $div = $a.next( 'div' );
			$div.slideToggle();
			$i.toggleClass( 'iOpened' );
			return false;
		});
	}( window.jQuery );
</script>
