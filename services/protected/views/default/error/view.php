<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'default/error/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<div class="pull-left">
			<?
				$this->widget( 'widgets.CountersWidget', Array(
					'counters' => Array( 'Ya.share' ),
				));
			?>
		</div>
		<a class="btn btn-small btn-yellow pull-right" href="<?=Yii::App()->createURL( 'question/list' )?>"><?=Yii::t( '*', 'Questions and suggestions' )?></a>
	</div>
	
	<div class="error-container">
		<div class="well">
			<h1 class="grey lighter smaller">
				<span class="blue bigger-125">
					<i class="icon-sitemap"></i>
					<?php echo $error['code']; ?>
				</span>
				<?=Yii::t( '*', CHtml::encode($error['message']) )?>
			</h1>

			<hr />
			<h3 class="lighter smaller"><?=Yii::t( '*', 'Please try find it use this search!' )?></h3>

			<div>

				
					<div id='cse-search-form' style='width: 100%;'>Loading</div>
					<script src='//www.google.com/jsapi' type='text/javascript'></script>
					<script type='text/javascript'>
					google.load('search', '1', {language: 'ru', style: google.loader.themes.V2_DEFAULT});
					google.setOnLoadCallback(function() {
					  var customSearchOptions = {};
					  customSearchOptions['adoptions'] = {'layout' : 'noTop'};
					  var customSearchControl =   new google.search.CustomSearchControl('015824500756150665004:koszn_wgu4c', customSearchOptions);
					  customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
					  var options = new google.search.DrawOptions();
					  options.enableSearchboxOnly('//fortrader.org/poisk-po-sajtu.html', 'q');
					  options.setAutoComplete(true);
					  customSearchControl.draw('cse-search-form', options);
					}, true);
					</script>
					<style type='text/css'>
					  input.gsc-input, .gsc-input-box, .gsc-input-box-hover, .gsc-input-box-focus {
					    border-color: #D9D9D9;
					  }
					  input.gsc-search-button, input.gsc-search-button:hover, input.gsc-search-button:focus {
					    border-color: #999999;
					    background-color: #000000;
					    background-image: none;
					    filter: none;
					
					  }
					</style>

				<div class="space"></div>
				<h4 class="smaller"><?=Yii::t( '*', 'Try one of the following' )?>:</h4>

				<ul class="unstyled spaced inline bigger-110">
					<li>
						<a href="//fortrader.org/services/calendarEvent/list">
						<i class="icon-hand-right blue"></i>
						<?=Yii::t( '*', 'Economic calendar' )?>
						</a>
					</li>

					<li>
						<a href="//fortrader.org/services/contest/list">
						<i class="icon-hand-right blue"></i>
						<?=Yii::t( '*', 'Contests' )?>
						</a>
					</li>

					<li>
						<a href="//fortrader.org/services/journal/last">
						<i class="icon-hand-right blue"></i>
						<?=Yii::t( '*', 'Journal' )?>
						</a>
					</li>
					
						<li>
						<a href="//fortrader.org/services/broker/list">
						<i class="icon-hand-right blue"></i>
						<?=Yii::t( '*', 'Brokers' )?>
						</a>
					</li>
				</ul>
			</div>

			<hr />
			<div class="space"></div>

			<div class="row-fluid">
				<div class="center">
					<a href="//fortrader.org/" class="btn btn-grey">
						<i class="icon-arrow-left"></i>
						<?=Yii::t( '*', 'На главную' )?>
					</a>

					<a href="//fortrader.org/services/" class="btn btn-primary">
						<i class="icon-dashboard"></i>
							<?=Yii::t( '*', 'Dashboard' )?>
					</a>
				</div>
			</div>
		</div>
	</div><!-- PAGE CONTENT ENDS -->
<?php //print_r($error);?>

</div>

