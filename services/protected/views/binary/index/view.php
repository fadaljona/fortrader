<?
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'broker/list/view' );
Yii::app()->clientScript->registerCoreScript('cookie'); 
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="page_title1" itemprop="name"><?=Yii::t( $NSi18n, 'Binary options rating' )?></h1>
	<hr class="separator1">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if($settings->binaryOptionsListShortDesc){?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$settings->binaryOptionsListShortDesc?>
		</blockquote>
	</div>
	<?php }?>
	
	
	<div class="nsActionView">
		<!-- - - - - - - - - - - - - - Search - - - - - - - - - - - - - - - - -->
		<div class="search_box_wr">
			<? $this->widget('widgets.BrokersDropDownRowsSelector',Array( 'ns' => 'nsActionView', 'cookieName' => 'BinaryOptionsListWidget_countRows' )); ?>
			<? $this->widget('widgets.SearchBrokersWidget',Array( 'ns' => 'nsActionView', 'minInputChars' => 3, 'type' => 'binary' )); ?>
		</div>
		<!-- - - - - - - - - - - - - - End of Search - - - - - - - - - - - - - - - - -->
		<?
			$this->widget( 'widgets.lists.BrokersListWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
				'showOnEmpty' => true,
				'type' => 'binary',
				'cookieRowsCount' => 'BinaryOptionsListWidget_countRows'
			));
		?>
		
		<?php
			$cacheKey = 'brokersRatingNewsBlock';
			if( !Yii::app()->cache->get( $cacheKey ) ){
				ob_start();	
				CommonLib::loadWp();
				require_once Yii::App()->params['wpThemePath'].'/templates/brokers-rating-page-news.php';	
				$blockContent = ob_get_clean();
				Yii::app()->cache->set( $cacheKey, $blockContent, 60*60 * 6);
			}
			echo Yii::app()->cache->get( $cacheKey );
		?>
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
		
		<?php
		if( $settings->binaryOptionsListFullDesc ){
				echo Chtml::tag('div', array('class' => 'section_offset', 'itemprop' => 'text'), $settings->binaryOptionsListFullDesc);
			}
		?>
		
		<? $this->widget('widgets.lists.DeCommentsListWidget',Array( 'ns' => 'nsActionView', 'cat' => 'singleBinaryBroker', 'modelName' => 'BrokerModel', 'limit' => 10, 'label'  => Yii::t('*', 'Recent reviews') )); ?>

	</div>
</section>