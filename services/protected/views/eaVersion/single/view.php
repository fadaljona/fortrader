<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'eaVersion/single/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative">
		<h1>
			<?=htmlspecialchars( $version->EA->name )?>
			<small>
				<i class="icon-double-angle-right"></i>
				<?=Yii::t( '*', 'Version' )?> <?=htmlspecialchars( $version->version )?>
			</small>
		</h1>
	</div>
	<?
		$this->widget( 'widgets.EAVersionProfileWidget', Array(
			'ns' => 'nsActionView',
			'model' => $version,
		));
	?>
</div>