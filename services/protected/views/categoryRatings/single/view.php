<?
Yii::import('controllers.base.ViewBase');
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 

<hr>
<section class="section_offset"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?></h1>
	<hr class="separator1">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if($model->shortDesc){?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$model->shortDesc?>
		</blockquote>
	</div>
	<?php }?>
	
	<div class="nsActionView">
	
		<?
			$this->widget('widgets.CategoryRatingWithFilterWidget',Array( 
				'ns' => 'nsActionView', 
				'ratingId' => $model->id
			));
		?>
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
		
		<?php 
			if( $model->fullDesc ){
				echo '<div class="section_offset">'.$model->fullDesc.'</div>';
			}
		?>
		

	</div>
	

	
</section>