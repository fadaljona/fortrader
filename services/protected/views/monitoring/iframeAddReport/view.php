<?
	Yii::app()->clientScript->registerCoreScript( 'jquery' );
?>
<script>
	!function( $ ) {
		<?if( !empty( $data['error'] )){?>
			var error = <?=json_encode( $data['error'] )?>;
			var errorField = <?=json_encode( @$data['errorField'] )?>;
			parent.nsActionView.wRegistrationContestMemberWizardWidget.submitReportError( error, errorField );
		<?}else{?>
			var id = <?=$data['id']?>;
			parent.nsActionView.wRegistrationContestMemberWizardWidget.submitReportSuccess( id );
		<?}?>
	}( window.jQuery );
</script>