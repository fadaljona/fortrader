<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'monitoring/single/view' );
	
Yii::app()->clientScript->registerScript('saveViewsMonitoringAccount', '
	!function( $ ) { 
		$.post( "'.Yii::app()->createUrl('monitoring/ajaxSaveViews').'", {"id": '.$model->id.'}, function ( data ) {} ); 
		$(".toggle_btn").click(function(){
            $(this).parent(".section_offset").find(".toggle_content").slideToggle("slow");
        });
	}( window.jQuery );
', CClientScript::POS_END);
	
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?></h1>
	<hr class="separator1 with_social_pl">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<div class="nsActionView">
	
	
	
	
	<?
	
		$this->widget( 'widgets.MonitoringAccountInfoWidget', Array(
			'ns' => 'nsActionView',
			'idMember' => $model->id,
		));
		
		if( !$model->server->forReports ){
			$this->widget( 'widgets.ProfitabilityOfTheAccountWidget', Array(
				'ns' => 'nsActionView',
				'idMember' => $model->id,
			));
		}
	?>



                

<?php /*
	<!-- - - - - - - - - - - - - - Conditions- - - - - - - - - - - - - - - - -->
	<section class="section_offset ">
		<h4 class="toggle_btn">Условия трейдера</h4>
		<div class="toggle_content tabl_quotes table_monitoring ">
			<table data-item='2'>
				<thead>
					<tr>
						<th><span>НОмер сета</span></th>
						<th><span>Минимальная инвестиция</span></th>
						<th><span>Комиссия</span></th>
						<th><span>Минимальный срок</span></th>
						<th><span>Штраф за дострочный возврат</span></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td data-title="НОмер сета" class=""><span class="">0.00%</span></td>
						<td data-title="Минимальная инвестиция">1450.2 USD</td>
						<td data-title="Комиссия"><span class="">50%</span></td>
						<td data-title="Минимальный срок"><span class="">7 Дней 0 часов</span></td>
						<td data-title="Штраф за дострочный возврат"><span class="">17%</span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>
	<!-- - - - - - - - - - - - - - End Conditions - - - - - - - - - - - - - - - - -->
	<!-- - - - - - - - - - - - - - Agress- - - - - - - - - - - - - - - - -->
	<section class="section_offset ">
		<h4 class="toggle_btn">Агреcсивность торговли</h4>
		<div class="toggle_content box2 box_informer">
			<div class="progress-wrap">
				<span class="progress-title">Низкая</span>
				<span class="progress-value">79,25%</span>
				<div class="progress-bar low_color" style="width:79.25%"></div>
			</div>
			<div class="progress-wrap">
				<span class="progress-title">Средняя</span>
				<span class="progress-value">35%</span>
				<div class="progress-bar mid_color" style="width:35%"></div>
			</div>
			<div class="progress-wrap">
				<span class="progress-title">Высокая</span>
				<span class="progress-value">15,25%</span>
				<div class="progress-bar high_color" style="width:15%"></div>
			</div>
			<div class="progress-wrap">
				<span class="progress-title">Очень высокая</span>
				<span class="progress-value">25%</span>
				<div class="progress-bar tout_color" style="width:25%"></div>
			</div>
		</div>
	</section>
	<!-- - - - - - - - - - - - - - End Agress - - - - - - - - - - - - - - - - -->
*/?>

	<?php
		$this->widget( 'widgets.graphs.ContestMemberGraphWidget', Array(
			'ns' => 'nsActionView',
			'contestMember' => $model,
			'type' => 'monitoring'
		));
	?>

			<div class="participant_page">
				<?php
					$this->widget( 'widgets.MemberTradingActivityWidget', Array(
						'ns' => 'nsActionView',
						'accountModel' => $model,
					));
					
					$this->widget( 'widgets.graphs.ContestMemberClosedDealsCountGraphWidget', Array(
						'ns' => 'nsActionView',
						'contestMember' => $model,
					));
					
					if( !$model->server->forReports ){
						$this->widget( 'widgets.ContestMemberStatementWidget', Array(
							'ns' => 'nsActionView',
							'idMember' => $model->id
						));
					
						$this->widget( 'widgets.InformerSettingsWidget', Array(
							'ns' => 'nsActionView',
							'category' => InformersCategoryModel::model()->findBySlugWithLang('monitoring'),
							'idMember' => $model->id,
						));
					}
		
				?>
			</div>
	
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
		<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
		
	</div>
</section>