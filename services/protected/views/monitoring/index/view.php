<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'monitoring/index/view' );
?>

<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset"> 
	<h1 class="clearfix page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?><i class="icon clip_icon"></i></h1>
	<hr class="separator1 inform-separator">

	<?php 
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; 
		
		if($settings->shortDesc){
			echo CHtml::tag('div', array( 'class' => 'inform-thesis_text thesis_text second mb71' ), $settings->shortDesc);
		}
	?>
	
	<div class="nsActionView">
		<?php 
			$this->widget( 'widgets.lists.MonitoringListWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
	</div>
	
	<?php
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; 
	
		if($settings->fullDesc){ 
			echo CHtml::tag('div', array( 'itemprop' => 'text' ), $settings->fullDesc);
		}
	?>
	<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
</section>		