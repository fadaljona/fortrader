<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'frameForForum/index/view' );
?>

<script>
	var nsActionView = {};
</script>

<section class="section_offset"> 
	<h1 class="clearfix page_title1" itemprop="name"><?=$this->action->title?> </h1>
	
	<div class="nsActionView">
		<?php
			$this->widget('widgets.wizards.RegistrationContestMemberWizardWidget', Array(
				'ns' => 'nsActionView',
				'contest'=>false,
				'type' => 'frameForForum',
			));
		?>
		<?php 
			$this->widget( 'widgets.lists.YourMonitoringAccountsListWidget', Array(
				'ns' => 'nsActionView',
				'type' => 'frameForForum',
				'forumId' => $forumId,
			));
		?>
	</div>
	

</section>		