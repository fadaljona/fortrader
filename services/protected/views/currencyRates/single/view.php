<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'currencyRates/single/view' );
	
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?></h1>
	<hr class="separator1 with_social_pl">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<div class="nsActionView">
	
	<?
		$this->widget( 'widgets.CurrencyRatesProfileWidget', Array(
			'ns' => 'nsActionView',
			'model' => $model,
		));
	?>
	
	<?php if($model->fullDesc){?>	
		<h5><strong class="blue_color"><?=Yii::t( $NSi18n, 'HISTORICAL BACKGROUND' )?></strong></h5>
		<?=CHtml::tag('div', array( 'itemprop' => 'text' ), $model->fullDesc)?>
	<?php }?>
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>

	<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
		
	</div>
</section>