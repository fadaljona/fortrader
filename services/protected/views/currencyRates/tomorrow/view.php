<?
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'currencyRates/index/view' );
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?></h1>
	<hr class="separator1">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<!-- - - - - - - - - - - - - - Сontent Block - - - - - - - - - - - - - - - - -->
	<div class="clearfix section_offset">
		<div class="author_box clearfix">
			<?=$settings->tomorrowDescription?>
		</div><!--/ .author_box -->
	</div><!--/ .clearfix -->
	<!-- - - - - - - - - - - - - - End of Сontent Block - - - - - - - - - - - - - - - - -->
	
	<div class="nsActionView">
		<?
			$this->widget( 'widgets.CurrencyRatesListWidget', Array(
				'ns' => 'nsActionView',
				'type' => 'tomorrow',
			));
		?>
		
		<?php $this->widget( 'widgets.CurrencyRatesArchivePreviewWidget', Array('ns' => 'nsActionView')); ?>
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
	</div>
	
	<?php if($settings->tomorrowFullDesc){ echo CHtml::tag('div', array( 'itemprop' => 'text' ), $settings->tomorrowFullDesc);} ?>
	
</section>