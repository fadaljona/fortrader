<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'currencyRates/exchangePage/view' );
	
	Yii::app()->clientScript->registerScript('saveConverterViews', "
		!function( $ ) {	
			$.post( '".Yii::app()->createAbsoluteUrl('currencyRates/ajaxSaveExchangeViews')."', {id: {$model->id}}, function ( data ) {} );
		}( window.jQuery );
	", CClientScript::POS_END);
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?></h1>
	<hr class="separator1 with_social_pl">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	
	<div class="nsActionView">
		<?php
			$this->widget( 'widgets.ShortCurrencyRatesExcangeInfoWidget', Array(
				'ns' => 'nsActionView',
				'model' => $model,
			));
			$this->widget( 'widgets.graphs.CurrencyRatesExcangeChartWidget', Array(
				'ns' => 'nsActionView',
				'model' => $model,
			));
			$this->widget( 'widgets.CurrencyRatesExcangeOtherConvertsWidget', Array(
				'ns' => 'nsActionView',
				'model' => $model,
			));
			
			require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php';
			
			if( $model->fullDesc ) echo CHtml::tag('div', array( 'class' => 'exchange-bottom-text-container', 'itemprop' => 'text' ), $model->fullDesc);
			
		?>

		<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
	</div>
</section>