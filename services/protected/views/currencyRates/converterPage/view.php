<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'currencyRates/archive/view' );
	
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset"> 
	<h1 class="page_title1">
		<span class="main_text" itemprop="name"><?=$this->action->title?></span>
		<?php
			if( $settings->converterSubTitle1 ) echo CHtml::tag('span', array( 'class' => 'sub_title1 aligncenter' ), $settings->converterSubTitle1 );
		?>
		<i class="icon converter_icon"></i></h1>
	<hr class="separator1">
	<?php 
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php';
		if( $settings->converterDescription )
			echo Chtml::tag('div', array( 'class' => 'thesis_text second' ), $settings->converterDescription );
	?>
	
	<?php
		if( $settings->converterSubTitle2 ){
			echo CHtml::tag('hr');
			echo CHtml::tag('h3', array( ), $settings->converterSubTitle2 . '<i class="icon arrows_icon"></i>' );
		} 
	?>
	
	<hr class="mb30">
	
	<div class="nsActionView">

		<div class="tabs_box tabs_rating">
			<ul class="tabs_list clearfix PFDregular">
				<li class="active tabs_list_1" data-id="tabs_id_1"><a href="<?=Yii::app()->createUrl('currencyRates/converterPage')?>"><?=Yii::t('converterPage', 'CBR course')?></a></li>
				<li class="tabs_list_2" data-id="tabs_id_2"><a href="<?=Yii::app()->createUrl('currencyRates/converterEcbPage')?>"><?=Yii::t('converterPage', 'ECB course')?></a></li>
			</ul>
		</div>

		<?php $this->widget( 'widgets.CurrencyRatesConverterWidget', Array( 'ns' => 'nsActionView', 'withoutTitle' => true, 'headerTitle' => $settings->converterSubTitle2, 'type' => 'cbr' )); ?>
	</div>
	
	<?php 
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php';
		if( $settings->converterFullDesc )
			echo Chtml::tag('div', array( 'class' => 'after_table_text', 'itemprop' => 'text' ), $settings->converterFullDesc );
	?>
</section>