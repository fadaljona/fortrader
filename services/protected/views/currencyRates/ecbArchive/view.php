<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'currencyRates/archive/view' );
	
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 

<hr>
<section class="section_offset"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?><i class="icon clip_icon"></i></h1>
	<hr class="separator1">
	<?php 
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php';
		if( $shortDesc )
			echo Chtml::tag('div', array( 'class' => 'thesis_text second' ), $shortDesc );
	?>
	
	<div class="nsActionView">
	<?php
		$this->widget( 'widgets.CurrencyRatesArchiveWidget', Array(
			'ns' => 'nsActionView',
			'year' => $year,
			'mo' => $mo,
			'day' => $day,
			'dataType' => 'ecb',
		));
	?>
	</div>
	
	<?php 
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php';
		if( $fullDesc )
			echo Chtml::tag('div', array( 'class' => 'after_table_text', 'itemprop' => 'text' ), $fullDesc );
	?>
</section>