<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'journal/last/view' );
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?></h1>
	<hr class="separator1">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$settings->description?>
		</blockquote>
	</div>
	<!-- - - - - - - - - - - - - - End of Сontent Block - - - - - - - - - - - - - - - - -->
	
	<div class="nsActionView">
		<?
			$this->widget( 'widgets.JournalProfileWidget', Array(
				'mode' => 'last',
				'ns' => 'nsActionView',
				'model' => $journal,
			));
		?>
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
	</div>
	
	<?php
		if( $settings->fullDescription ){
			echo Chtml::tag('div', array('class' => 'section_offset', 'itemprop' => 'text'), $settings->fullDescription);
		}
	?>
	
	<? $this->widget('widgets.lists.DeCommentsListWidget',Array( 'ns' => 'nsActionView', 'cat' => 'singleJournal', 'modelName' => 'JournalModel', 'limit' => 10, 'label'  => Yii::t('*', 'Recent comments') )); ?>
	
</section>