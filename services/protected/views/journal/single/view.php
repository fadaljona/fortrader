<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'journal/single/view' );
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />
<meta itemprop="datePublished" content="<?=$journal->date?>" />

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?></h1>
	<hr class="separator1 with_social_pl">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if($journal->shortDescription){?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$journal->shortDescription?>
		</blockquote>
	</div>
	<?php }?>
	
	<div class="nsActionView">
		<?
			$this->widget( 'widgets.JournalProfileWidget', Array(
				'ns' => 'nsActionView',
				'model' => $journal,
			));
		?>
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>

	<?php if( $journal->description ){ echo CHtml::tag( 'div', array( 'itemprop' => 'text' ), $journal->description ); } ?>
	
	<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>

	</div>
</section>