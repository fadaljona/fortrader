<?if( $member->stats->last_error_code == -255 ){?>
    <div class="alert alert-block alert-error">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>
        <p>
            <i class="icon-cogs"></i>
            <?=Yii::t( '*', 'Connection Error!' )?><br><br><?=Yii::t( '*', 'Terminal reports an error connecting to your account:' )?>
            <strong><?=Yii::t( '*', 'Invalid account' )?></strong>
            <br><br><?=Yii::t( '*', 'invalid account error desciption' )?>
        </p>
        <p><a class="btn btn-small btn-success" href="<?=Yii::App()->createUrl( 'user/profile', Array( 'tabTA' => 'yes', 'idEditMember' => $member->id ))?>">
                <?=Yii::t( '*', 'Edit' )?></a>
            <a class="btn btn-small" href="<?=Yii::App()->createUrl( 'contacts/index', Array())?>"><?=Yii::t( '*', 'Contact the broker' )?></a>
        </p>
    </div>
<?}?>