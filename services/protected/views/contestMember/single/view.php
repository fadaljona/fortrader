<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'contestMember/single/view' );
?>

<hr class="">
<section class="section_offset">
	<div class="clearfix">
		<h2 class="page_title1"><?=$this->action->title?></h2>
		<p class="user_team"><?=Yii::t( $NSi18n, 'The participant of the contest' )?> <?=$member->contest->currentLanguageI18N->name?></p>
		
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-small-top-buttons.php'; ?>
		
	</div>
	<hr class="separator2">
</section>


<?php	
	$this->widget( 'widgets.AccountMemberPageWidget', Array(
		'NSi18n' => $NSi18n,
		'accountModel' => $member,
		'rev' => $rev,
		'revlist' => $revlist,
		'linklist' => $linklist,
	));	
?>
<script>
	var nsActionView3 = {};
</script>
<div id="comments" class="nsActionView3"><?php $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView3', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>