<?
	$lang = Yii::App()->language;

	$baseUrl = Yii::app()->baseUrl;
	$language = Yii::App()->language;
	
	$bodyClass = $this->getLayoutBodyClass();
	$bodyClass = !empty( $bodyClass ) ? ' class="'.$bodyClass.'"' : '';
?>
<!DOCTYPE html>
<html lang="<?=$lang?>">
	<head>
		<script>
			var yiiBaseURL = "<?=$baseUrl?>";
			var yiiLanguage = "<?=$language?>";
			var yiiDebug = <?=CommonLib::boolToStr( YII_DEBUG )?>;
		</script>
		<meta charset="utf-8">
		<title></title>
		<meta name="author" content="">
	</head>
	<body<?=$bodyClass?>>
		<?=$content?>
	</body>
</html>