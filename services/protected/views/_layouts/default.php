<?
	$baseUrl = Yii::app()->baseUrl;
	
	Yii::app()->bootstrap->register();
	Yii::App()->clientScript->registerCssFile( "{$baseUrl}/assets-static/css/font-awesome-4.3.0/css/font-awesome.min.css" );
	Yii::App()->clientScript->registerCssFile( "{$baseUrl}/assets-static/css/font-awesome.min.css" );
	Yii::App()->clientScript->registerCssFile( "//fonts.googleapis.com/css?family=PT+Sans:400,700|Open+Sans:400,300&subset=cyrillic-ext,cyrillic" );
	Yii::App()->clientScript->registerCssFile( "{$baseUrl}/assets-static/css/ace.min.css" );
	Yii::App()->clientScript->registerCssFile( "{$baseUrl}/assets-static/css/ace-responsive.min.css" );
	Yii::App()->clientScript->registerCssFile( "{$baseUrl}/css/default.css" );
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/js/jquery.scrollTo.js" );
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/js/jquery.mCustomScrollbar.js" );
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/assets-static/js/ace-elements.min.js" );
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/assets-static/js/ace.min.js" );
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/assets-static/js/jquery.autosize-min.js" );
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/assets-static/js/jquery-ui-1.10.3.custom.min.js" );
	
	Yii::App()->clientScript->registerCoreScript( 'jquery.ui' );
	Yii::app()->clientScript->registerCssFile( Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css' );
	
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/js/commonLib.js" );
	Yii::App()->clientScript->registerScriptFile( "{$baseUrl}/js/engine.js" );
	Yii::app()->clientScript->registerCoreScript('cookie'); 

?>
<?$this->beginContent( '//_layouts/cleanPage' )?>
	<?=$content?>
	<script>
		$('[data-rel=tooltip]').tooltip();
		$('textarea[class*=autosize]').autosize({append: "\n"});
		
		$(document).on( 'click', '.dropdown-menu', function( e ) {
            $(this).hasClass( 'iKeepOpen' ) && e.stopPropagation();
        }); 
		
		!function( $ ) {
			if( $(document).width() <= 979 ) {
				var selector; 
				if( $( '.wPageSubmenu' ).length ) selector = '.wPageSubmenu';
				else selector = '.nsActionView';
		
				$.scrollTo( selector, 0, { offset: -10 });
			}
		}( window.jQuery );
	</script>
<?$this->endContent()?>
