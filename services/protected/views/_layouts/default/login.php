<?$this->beginContent( '/_layouts/default' )?>
	<div class="container-fluid" id="main-container">
		<div id="main-content">
			<div class="row-fluid">
				<div class="span12">
					<?=$content?>
				</div>
			</div>
		</div>
	</div>
	<?$this->widget( "components.widgets.CountersWidget", Array( 'counters' => 'common' ))?>
<?$this->endContent()?>