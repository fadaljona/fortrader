<?$this->beginContent( '/_layouts/default' )?>
	<script>
		var nsLayoutDefaultIndex = {};
	</script>
	<div class="nsLayoutDefaultIndex">
		<?php
			$this->renderDynamic('widget', 'components.widgets.NavbarWidget', array( 'ns' => 'nsLayoutDefaultIndex' ), true);
			//$this->widget( "components.widgets.NavbarWidget", Array( 'ns' => 'nsLayoutDefaultIndex' ));
		?>
		<div class="main-container container-fluid" id="main-container">
			<a class="menu-toggler" id="menu-toggler" href="#" class="">
				<span class="menu-text"></span>
			</a>
			<div class="sidebar menu-min" id="sidebar">
				<?$this->widget( "components.widgets.SidebarShortcutsWidget" )?>
				<?$this->widget( "components.widgets.NavlistWidget" )?>
				<div class="sidebar-collapse" id="sidebar-collapse"><i class="icon-double-angle-left icon-double-angle-right"></i></div>
			</div>
			<div class="main-content" id="main-content">
				<div>
					<?php 
						$this->renderDynamic('widget', 'widgets.quotes.QuotesPanelWidget', array( 'view' => 'panel' ), true);
						//$this->widget('widgets.quotes.QuotesPanelWidget',Array('view' => 'panel')); 
					?>
				</div>
				<?$this->widget( "components.widgets.BreadcrumbsWidget" )?>
				<div class="iClear"></div>
				<div id="page-content" class="page-content">
					<div class="row-fluid">
						<?$this->widget( "components.widgets.TraderProfileAlertWidget", Array( 'ns' => 'nsLayoutDefaultIndex' ))?>
					</div>
					<div class="row-fluid">
						<?=$content?>
					</div>
					<h3 class="header smaller lighter blue"></h3>
					<?$this->widget( "components.widgets.CountersWidget", Array( 'counters' => Array( 'LiveInternet' )))?>
					<div class="pull-right">
						<?$this->widget( "components.widgets.ChangeLanguageWidget", Array(
							'ns' => 'nsLayoutDefaultIndex',
						))?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?$this->widget( "components.widgets.CountersWidget", Array( 'counters' => 'common' ))?>
<?$this->endContent()?>
