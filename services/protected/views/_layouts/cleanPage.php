<?
	$lang = Yii::App()->language;
	$title = $this->getLayoutTitle();
	$description = $this->getLayoutDescription();
	$keywords = $this->getLayoutKeywords();
	$baseUrl = Yii::app()->baseUrl;
	$language = Yii::App()->language;
	
	$bodyClass = $this->getLayoutBodyClass();
	$bodyClass = !empty( $bodyClass ) ? ' class="'.$bodyClass.'"' : '';
?>
<!DOCTYPE html>
<html lang="<?=$lang?>">
	<head>
		<script>
			var yiiBaseURL = "<?=$baseUrl?>";
			var yiiLanguage = "<?=$language?>";
			var yiiDebug = <?=CommonLib::boolToStr( YII_DEBUG )?>;
		</script>
		<meta charset="utf-8">
		<title><?=$title?></title>
		<meta name="description" content="<?=$description?>">
		<meta name="keywords" content="<?=$keywords?>">
		<meta name="author" content="">
	</head>
	<body<?=$bodyClass?>>
		<?=$content?>
	</body>
</html>