<?
	$lang = Yii::App()->language;
	$title = $this->getLayoutTitle();
	$description = $this->getLayoutDescription();
	$keywords = $this->getLayoutKeywords();
	$baseUrl = Yii::app()->baseUrl;
	$language = Yii::App()->language;
	$bodyClass = $this->getLayoutBodyClass();
	$bodyClass = !empty( $bodyClass ) ? ' class="'.$bodyClass.'"' : '';
	
	$siteName = WpOptionsModel::getSiteName();
	$ogSection = $this->getLayoutOgSection();
	$ogImage = $this->getLayoutOgImage();
	
	$itemscopeItemtype = $this->getMainItemscopeItemtype();
	
	$langSwitcher = $this->getLangSwitcher();
	
	require_once Yii::App()->params['wpThemePath'].'/header-yii.php';
?>
	<!-- - - - - - - - - - - - - - Left Part - - - - - - - - - - - - - - - - -->
	<div class="left_part servicesContent" itemscope itemtype="http://schema.org/<?=$itemscopeItemtype?>">
		<?php require_once Yii::App()->params['wpThemePath'].'/templates/banner-728x90.php';?>
		<?=$content?>
	</div>
	
	<!-- - - - - - - - - - - - - - End of Left Part - - - - - - - - - - - - - - - - -->
	<?php 
	if($this->beginCache('wp_get_sidebar' . Yii::app()->language , array('duration'=>3600))) {
		CommonLib::loadWp();
		get_sidebar('yii'); 
	$this->endCache(); }
	
	if(!Yii::App()->user->id){
		require_once Yii::App()->params['wpThemePath']. '/templates/popup-login-yii.php'; 
	} 
	
	$cs=Yii::app()->getClientScript();
	$arcticmodalBaseUrl = Yii::app()->getAssetManager()->publish( Yii::App()->params['wpThemePath'] . '/plagins/arcticmodal' );
	$cs->registerCssFile($arcticmodalBaseUrl.'/jquery.arcticmodal-0.3.css');
	$cs->registerScriptFile($arcticmodalBaseUrl.'/jquery.arcticmodal-0.3.min.js', CClientScript::POS_END);
	
	


	if($this->beginCache('wp_get_footer' . Yii::app()->language . CommonLib::sslRequestMarker(), array('duration'=>60*60))) {
		CommonLib::loadWp();
		ob_start();
			require_once Yii::App()->params['wpThemePath'].'/footer-yii.php';
		$footerContent = ob_get_clean();
		$footerContent = preg_replace ( "/<script[^>]+wp\-includes\/js\/jquery\/jquery.js[^>]+><\/script>/" , '' , $footerContent );
		echo $footerContent = preg_replace ( "/<link[^>]+decom.css[^>]+\/>/" , '' , $footerContent );
	$this->endCache(); }
?>