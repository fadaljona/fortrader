<?
	$baseUrl = Yii::app()->baseUrl;
	Yii::App()->clientScript->registerCoreScript( 'jquery' );
	Yii::app()->clientScript->registerCoreScript('cookie'); 
	
	Yii::app()->clientScript->registerScript('animateAnchorScroll', "
	!function( $ ) {	
		$('a[href*=#]').click(function() {
			if( $(this).attr('href') != '#' ){
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')&& location.hostname == this.hostname) {
					var vTarget = $(this.hash);
					vTarget = vTarget.length && vTarget || $('[name=' + this.hash.slice(1) +']');
					if (vTarget.length) {
						var targetOffset = vTarget.offset().top;
						$('html,body').animate({scrollTop: targetOffset}, 1000);
						return false;
					}
				}
			}
		});
	}( window.jQuery );
	", CClientScript::POS_END);
	
	
?>
<?$this->beginContent( '//_layouts/wp/main' )?>
	<?=$content?>

<?$this->endContent()?>
