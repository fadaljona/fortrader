<div class="accordion accordion-style2 <?=@$class?>">
	<div class="accordion-group">
		<div class="accordion-heading">
			<a href="#collapse_<?=$idCollapse=mt_rand()?>" data-toggle="collapse" class="accordion-toggle collapsed">
				<?=$title?>
			</a>
		</div>
		<div class="accordion-body collapse" id="collapse_<?=$idCollapse?>">
			<div class="accordion-inner">
				<?=$content?>
			</div>
		</div>
	</div>
</div>