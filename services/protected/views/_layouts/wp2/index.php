<?php $this->beginContent('/_layouts/wp2/default'); ?>

<script>var nsLayoutDefaultIndex = {};</script>

<div class="nsLayoutDefaultIndex">
    <?php $this->widget("components.widgets.NewBreadcrumbsWidget"); ?>
    <div class="row-fluid">
        <?=$content?>
    </div>
</div>
<?php $this->endContent(); ?>