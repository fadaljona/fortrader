<?=CHtml::openTag( 'div', Array(
	'class' => 'accordion-body '.@$class,
	'id' => isset( $id ) ? "collapse_{$id}" : null,
))?>
	<div class="accordion-inner">
		<?=$content?>
	</div>
</div>