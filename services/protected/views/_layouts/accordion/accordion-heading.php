<div class="accordion-heading">
	<?=CHtml::openTag( 'a', Array(
		'class' => 'accordion-toggle '.@$class,
		'href' => isset( $id ) ? "#collapse_{$id}" : "#",
		'data-toggle' => @$trigger !== 'manual' ? 'collapse' : '',
	))?>
		<?=$content?>
	</a>
</div>