<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/assets-static/ckeditor/ckeditor.js" );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/assets-static/ckeditor/adapters/jquery.js" );
	
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'ea/add/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative">
		<h1>
			<?=Yii::t( '*', 'EA Catalog' )?>
			<small>
				<i class="icon-double-angle-right"></i>
				<?=Yii::t( '*', 'Adding EA' )?>
			</small>
		</h1>
	</div>
	<div style="max-width:800px;">
		<?
			$this->widget( 'widgets.forms.EAFormWidget', Array(
				'ns' => 'nsActionView',
				'model' => $formModel,
			));
		?>
	</div>
</div>