<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'ea/laboratory/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<div class="pull-right" style="margin-top:5px">
			<?$this->widget( 'widgets.EANavWidget', Array( 'ns' => 'nsActionView' ));?>
		</div>
		<h1>
			<?=Yii::t( $NSi18n, 'EA laboratory' )?>
		</h1>
		
		<?//=Yii::t( '*', 'text_description_rating_ea' )?>
		
		<?/*<a class="btn btn-small btn-yellow pull-right" href="<?=Yii::App()->createURL( 'question/list', Array( 'id' => UserMessageModel::ID_QUESTION_LIST_EA ))?>"><?=Yii::t( '*', 'Questions and suggestions' )?></a>*/?>
	</div>
	<div class="row-fluid">
		<?
			$this->widget( 'widgets.lists.EAListWidget', Array(
				'instance' => 'laboratory',
				'ns' => 'nsActionView',
				'ajax' => true,
				'showOnEmpty' => true,
			));
		?>
	</div>
	<div class="row-fluid" style="padding-top:20px;">
		<?
			$this->widget( 'widgets.UserActivitiesWidget', Array( 
				'ns' => 'nsActionView', 
			))
		?>
	</div>
	<div class="row-fluid">
		<h3 class="header smaller lighter blue"></h3>
		<?
			$this->widget( 'widgets.UsersConversationWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
			));
		?>
	</div>
	
	<div class="row-fluid" style="margin-top:20px;">
		<?
			$this->widget( 'widgets.PostInformWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
	</div>
</div>