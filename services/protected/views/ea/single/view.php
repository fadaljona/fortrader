<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/assets-static/ckeditor/ckeditor.js" );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/assets-static/ckeditor/adapters/jquery.js" );

	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'ea/single/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative">
		<h1>
			<?=htmlspecialchars( $ea->name )?>
		</h1>
	</div>
	<?
		$this->widget( 'widgets.EAProfileWidget', Array(
			'ns' => 'nsActionView',
			'model' => $ea,
		));
	?>
</div>