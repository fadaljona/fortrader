<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'ea/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
    <h1>
      <?=Yii::t( $NSi18n, 'EA Catalog' )?>
    </h1>
		
		<?//=Yii::t( '*', 'text_description_rating_ea' )?>
		
		<?/*<a class="btn btn-small btn-yellow pull-right" href="<?=Yii::App()->createURL( 'question/list', Array( 'id' => UserMessageModel::ID_QUESTION_LIST_EA ))?>"><?=Yii::t( '*', 'Questions and suggestions' )?></a>*/?>
    <?$buttons = array(
      array('buttonType'=>'button',
        'label' => Yii::t($NSi18n, 'EA all'),
        'htmlOptions' => array (
          'data-href' => 'all',
          'onclick' => 'js:changePlatform(this)',
          'class' => 'first',
        ),
        'active' =>true,
      ),
    )?>
    <?foreach (EAModel::getEaPlatforms() as $platform) {
      $buttons[] = array(
        'buttonType' => 'button',
        'label' => $platform,
        'htmlOptions' => array (
          'data-href' => $platform,
          'onclick' => 'js:changePlatform(this)',
        ),
      );
    }
    $buttons[count($buttons) - 1]['htmlOptions']['class'] = 'last';
    ?>
<!--    <div class="iClear"></div>-->
    <div class="row-fluid">
      <?$this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type' => 'info', // 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'toggle' => 'radio', // 'checkbox' or 'radio'
        'buttons' => $buttons,
        'htmlOptions' => array('class' => 'eaPlatform')
      ));?>

      <div class="pull-right" style="margin-top: 10px">
        <?$this->widget( 'widgets.EANavWidget', Array( 'ns' => 'nsActionView' ));?>
      </div>
	  </div>
	</div> 
	
  <div class="table-row">
    <div class="table-col center-columns">
      <?
      ob_start();
      $tab = $this->widget( 'widgets.lists.EAListWidget', Array(
        'ns' => 'nsActionView',
        'ajax' => true,
        'showOnEmpty' => true,
      ));
      $tabContent = ob_get_clean();
      $ns = $tab->getNS();
      $ins = $tab->getINS();
      ?>
    
<!--    <div class="pull-left" style="margin-top:5px">-->
      <?$this->widget( 'bootstrap.widgets.TbTabs', Array(
          'id' => 'eATabs',
          'type'=>'tabs',
          'placement'=>'top',
          'tabs'=>array(
            array('label'=>Yii::t( $NSi18n, 'EA last' ), 'content'=> $tabContent, 'active'=>true),
            array('label'=>Yii::t( $NSi18n, 'EA popular' ), 'content'=>''),
          ),
          'events'=>array('shown' => 'js:loadTabContent'),
        ));?>
    </div>
    <div class="table-col right-columns">
      <?$this->widget('widgets.lists.EALastColumnWidget', array(
        'ajax' => true,
        'showOnEmpty' => true,
      ))?>
      <?$this->widget('widgets.lists.EACommentColumnWidget', array(
        'ajax' => true,
        'showOnEmpty' => true,
      ))?>
      <?$this->widget('widgets.lists.EAPopularColumnWidget', array(
        'ajax' => true,
        'showOnEmpty' => true,
      ))?>
      <?$this->widget('widgets.lists.EARatingColumnWidget', array(
        'ajax' => true,
        'showOnEmpty' => true,
      ))?>
    </div>
	</div>
	<div class="row-fluid" style="padding-top:20px;">
		<?
			$this->widget( 'widgets.UserActivitiesWidget', Array( 
				'ns' => 'nsActionView', 
			))
		?>
	</div>
	<div class="row-fluid">
		<h3 class="header smaller lighter blue"></h3>
		<?
			$this->widget( 'widgets.UsersConversationWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
			));
		?>
	</div>
	
	<div class="row-fluid" style="margin-top:20px;">
		<?
			$this->widget( 'widgets.PostInformWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
	</div>
</div>

<?if( !Yii::App()->request->isAjaxRequest ){?>
  <script>
    function loadTabContent(e) {
      e.preventDefault();
      
      var tabId = e.target.getAttribute("href");
      var sort = nsActionView.wEAListWidget.$form.find('input[name=sortField]');
      $(sort).val(tabId == '#eATabs_tab_2' ? 'popular' : 'last');
      $('#eATabs_tab_2').removeClass('active in');
      $('#eATabs_tab_1').addClass('active in');

      nsActionView.wEAListWidget.changeFilter();
    }

    function changePlatform(el) {
      var platform = nsActionView.wEAListWidget.$form.find('input[name=platform]');
      $(platform).val($(el).data('href'));

      $('#eATabs > ul > li').removeClass('active');
      $('#eATabs > ul > li:first').addClass('active');
      
      nsActionView.wEAListWidget.changeFilter();
    }

  </script>
<?}?>