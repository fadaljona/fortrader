<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'contacts/index/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<h1>
			<?=Yii::t( '*', 'Contacts' )?>
		</h1>
	</div>
	
	<div class="pull-left" style="margin-right:20px;">
		<?$this->widget( 'widgets.lists.ContactsListWidget' );?>
	</div>
	
	<div class="pull-left">
		<?
			$this->widget( 'widgets.forms.FeedbackFormWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
	</div>
</div>