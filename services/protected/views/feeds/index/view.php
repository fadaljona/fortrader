<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'newsAggregator/index/view' );
?>

<script>
	var nsActionView = {};
</script>

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="clearfix mb15 last_news_title page_title1">
		<?=$this->action->title?>
	</h1>
	<hr>
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if($settings->shortDesc){ ?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$settings->shortDesc?>
		</blockquote>
	</div>
	<?php } ?>
	
	
	<div class="nsActionView main-post-content rssFeedsList">

		<?php 
			if( $feeds ){
				echo CHtml::openTag('div', array('class' => 'post_box'));
					foreach( $feeds as $block ){
						echo CHtml::openTag('div', array('class' => 'post_col2 post_col_sm1'));
							echo CHtml::tag('h3', array(), $block['groupTitle'] );
							if( isset( $block['feeds'] ) ){
								RssFeedsFeedModel::renderFeedTree( $block['feeds'] );
							}
						echo CHtml::closeTag('div');
					}
				echo CHtml::closeTag('div');
			}
		?>

	</div>

	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
</section>

<?php if($settings->fullDesc){ ?>
	<div class="section_offset">
		<?=$settings->fullDesc?>
	</div>
<?php }?>