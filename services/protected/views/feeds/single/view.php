<?php 

date_default_timezone_set( 'UTC' );
header("content-type: application/rss+xml; charset=UTF-8");


$homeUrl = Yii::app()->request->getHostInfo();
$logo = $homeUrl. "/images/ftlogo-100.png";




if($this->beginCache('rssFeed' . $model->lastPostDate . $model->cats , array('duration'=>60*60*6))) {

CommonLib::loadWp();

echo ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">
	<channel>
		<title>{$model->chanelTitle}</title>
		<atom:link href=\"{$model->absoluteSingleUrl}\" rel=\"self\" type=\"application/rss+xml\" />
		<link>{$homeUrl}</link>
		<description>{$model->description}</description>
		<image>
			<url>{$logo}</url>
			<link>{$homeUrl}</link>
			<title>{$model->chanelTitle}</title>
		</image>
		<lastBuildDate>".date("r", strtotime( $model->lastPostDate ) - 3600*4 )."</lastBuildDate>
");

if( $model->cats == 0 ){
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'modified',
		'order'   => 'DESC',
		'posts_per_page' => $model->newsCount,
		'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
	) );
}else{
	$r = new WP_Query( array(
		'post_status'  => 'publish',
		'post_type' => 'post',
		'orderby' => 'modified',
		'order'   => 'DESC',
		'posts_per_page' => $model->newsCount,
		'category__in' => explode(',', $model->cats )
	) );
}



global $post;
foreach( $r->posts as $post ){
	setup_postdata($post);

	$chen_title = '<![CDATA[' . $post->post_title . ']]>';
	$chen_link = get_the_permalink();
		
	$text = apply_filters('the_content', $post->post_content );
	$text = html_entity_decode(strip_tags( $text ));
		
	$text = wp_trim_words($text, 100, '...');
	$text = str_replace("\n"," ",$text);
	$text = '<![CDATA[' . str_replace("\r"," ",$text) . ']]>';

	$date = date("r", strtotime( $post->post_date ) - 3600*4 );

	echo ("
		<item>
			<title>" . $chen_title . "</title>
			<link>" . $chen_link . "</link>
			<pubDate>$date</pubDate>
			<description>" . $text . "</description>
			<guid>" . $chen_link . "</guid>
		</item>
	");
}


echo ("
	</channel>
</rss> ");

$this->endCache(); }


?>