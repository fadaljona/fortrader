<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'userPrivateMessages/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative">
		<h1>
			<?=Yii::t( $NSi18n, 'Private messages' )?>
		</h1>
	</div>
	<?
		$this->widget( 'widgets.UsersPrivateMessagesWidget', Array(
			'ns' => 'nsActionView',
		));
	?>
</div>