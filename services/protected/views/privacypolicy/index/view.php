<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'privacypolicy/index/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<h1>
			<?=Yii::t( '*', 'Privacy policy' )?>
		</h1>
	</div>
	
	<?=Yii::t( '*', 'privacy_policy_text' )?>
</div>