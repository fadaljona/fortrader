<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'newsAggregator/category/view' );
?>

<script>
	var nsActionView = {};
</script>

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="clearfix mb15 last_news_title page_title1">
		<?=$this->action->title?>
		<?=CHtml::link( Yii::t($NSi18n, 'Sources'), Yii::app()->createUrl('newsAggregator/sources'), array( 'class' => 'btn_red abs', 'data-ico' => 'speaker' ) )?>
	</h1>
	<hr>
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if($model->shortDesc){ ?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$model->shortDesc?>
		</blockquote>
	</div>
	<?php } ?>
	
	
	<div class="nsActionView">
		<?php
			$this->widget( 'widgets.NewsAggregatorCatsWidget', Array(
				'ns' => 'nsActionView',
				'currentCat' => $model->id,
			));
		?>
		<?php
			$this->widget( 'widgets.NewsAggregatorNewsByCatWidget', Array(
				'ns' => 'nsActionView',
				'currentCat' => $model->id,
			));
		?>
	</div>

	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
</section>

<?php if($model->fullDesc){ ?>
	<div class="section_offset">
		<?=$model->fullDesc?>
	</div>
<?php }?>