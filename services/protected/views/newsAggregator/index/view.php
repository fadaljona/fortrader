<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'newsAggregator/index/view' );
?>

<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="clearfix mb15 last_news_title page_title1" itemprop="name">
		<?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?>
		<?=CHtml::link( Yii::t($NSi18n, 'Sources'), Yii::app()->createUrl('newsAggregator/sources'), array( 'class' => 'btn_red abs', 'data-ico' => 'speaker' ) )?>
	</h1>
	<hr>
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if($settings->newsShortDesc){ ?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$settings->newsShortDesc?>
		</blockquote>
	</div>
	<?php } ?>
	
	
	<div class="nsActionView">
		<?php
			$this->widget( 'widgets.NewsAggregatorCatsWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
		<?php
			$this->widget( 'widgets.NewsAggregatorNewsByCatWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
	</div>

	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
</section>

<?php if($settings->newsFullDesc){ ?>
	<div class="section_offset" itemprop="text">
		<?=$settings->newsFullDesc?>
	</div>
<?php }?>

<div id="comments"><?php $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>