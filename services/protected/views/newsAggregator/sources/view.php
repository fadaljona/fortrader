<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'newsAggregator/sources/view' );
?>

<script>
	var nsActionView = {};
</script>



<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="clearfix mb15 last_news_title page_title1">
		<?=$this->action->title?>
		<?=CHtml::link( Yii::t($NSi18n, 'Sources'), Yii::app()->createUrl('newsAggregator/sources'), array( 'class' => 'btn_red abs', 'data-ico' => 'speaker' ) )?>
	</h1>
	<hr>
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if($settings->sourceShortDesc){ ?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$settings->sourceShortDesc?>
		</blockquote>
	</div>
	<?php } ?>
	
	
	<div class="nsActionView">
		<?php
			$this->widget( 'widgets.NewsAggregatorCatsWidget', Array(
				'ns' => 'nsActionView',
				'currentCat' => -1
			));
		?>
		
		<?php
			$this->widget( 'widgets.NewsAggregatorSourcesWidget', Array(
				'ns' => 'nsActionView',
				'cookieName' => 'disabledSourcesFT',
			));
		?>
	</div>

	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
</section>

<?php if($settings->sourceFullDesc){ ?>
	<div class="section_offset">
		<?=$settings->sourceFullDesc?>
	</div>
<?php }?>