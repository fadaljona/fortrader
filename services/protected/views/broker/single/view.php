<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'broker/single/view' );
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 

<hr>
<section class="section_offset paddingBottom0"> 
	<h1 class="page_title1" itemprop="name">
		<?=$this->action->title?> <?php if($commentsCount){?><a href="#comments">(<?=$commentsCount?>)</a><?php }?>
		<?php
			if(Yii::app()->user->checkAccess('brokerControl')){
				echo CHtml::link( '<i> </i>', Yii::app()->createUrl('admin/broker/list', array('id' => $broker->id)), array( 'class' => 'post-edit-link', 'target' => '_blank' ));
			}
		?>
	</h1>
	<hr class="separator1 with_social_pl">
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
	
	<?php if( $broker->shortDesc ){?>
	<div class="section_offset">
		<blockquote class="thesis2">
			<?=$broker->shortDesc?>
		</blockquote>
	</div>
	<?php }?>
	
	<div class="nsActionView">
	
	<?
		$this->widget( 'widgets.BrokerProfileWidget', Array(
			'ns' => 'nsActionView',
			'model' => $broker,
		));
	?>
	
	<?php if( $broker->fullDesc ){ ?>
		<div class="section_offset" itemprop="text"><br>
			<?=$broker->fullDesc;?>
		</div>
	<?php }?>

	<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
		
	</div>
</section>