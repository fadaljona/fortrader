<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n('cryptoCurrencies/index/view');

$cs=Yii::app()->getClientScript();

$cssUrl = Yii::app()->getAssetManager()->publish(Yii::App()->params['wpThemePath'] . '/css');
$cs->registerCssFile($cssUrl.'/fx_cryptocurrencies.css', $cs::POS_BOTTOM_HEAD);
$cs->registerCssFile($cssUrl.'/fx_cryptocurrencies-responsive.css', $cs::POS_BOTTOM_HEAD);
?>

<script>var nsActionView = {};</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>

<div class="fx_section marginBottom20">                        
    <div class="fx_content-title">
        <h1 class="fx_content-title-link"><?=$this->action->title?> <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/crypto_icon.png" alt="" class="fx_title-icon"></h1>
    </div>
    <div class='fx_section-box'>
        <?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php';?>
    </div>
</div>
<div class="nsActionView">
    <?php $this->widget('widgets.CryptoQuickMenuWidget', array('ns' => 'nsActionView'));?>

    <?php $this->widget('widgets.CryptoCurrenciesAndListWidget', array('ns' => 'nsActionView'));?>

    <?php if(Yii::app()->language == 'ru') { ?>
    <div class="fx_section marginTop55" id="cryptoNewsArticlesBlock">
        <div class="fx_content-title clearfix">
            <h3 class="fx_content-title-link"><?= Yii::t($NSi18n, 'News Crypto-currency') ?></h3>
            <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/news_icon.png" alt="" class="fx_title-icon">
        </div>
        <div class="fx_section-box clearfix">
            <?= CommonLib::getWpTemplateBlock('cryptoArticlesBlock', 'templates/crypto-articles-block.php');?>
            <?= CommonLib::getWpTemplateBlock('cryptoNewsBlock', 'templates/crypto-news-block.php');?>
            <?php $this->widget('widgets.LastForumUpdatesWidget', array(
                'ns' => 'nsActionView',
                'url' => 'https://forexsystemsru.com/bitkoin-i-drugie-kriptovalyuty-f292/'
            ));?>
        </div>
    </div>
    <?php } else {
        echo '<div class="fx_section marginTop55"></div>';
    }?>


    <?= CommonLib::getWpTemplateBlock('cryptoCurrenciesBeginer', 'templates/crypto_currencies_beginer.php');?>

    <?php $this->widget('widgets.BitcoinStatsWidget', array('ns' => 'nsActionView'));?>

    <?php $this->widget('widgets.TradingWithBrokerCryptoCurrenciesWidget', array('ns' => 'nsActionView'));?>
    <?php $this->widget('widgets.SmByHashTagWidget', array('ns' => 'nsActionView'));?>
    <?php $this->widget('widgets.VideoChanelWidget', array('ns' => 'nsActionView', 'chanel' => $settings->videoChanel));?>
    <?php $this->widget('widgets.NewChatWidget', array('ns' => 'nsActionView'));?>
    <?php $this->widget('widgets.FaqWidget', array('ns' => 'nsActionView', 'faq' => $settings->faq));?>

</div>