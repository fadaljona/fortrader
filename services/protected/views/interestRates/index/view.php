<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'interestRates/index/view' );
?>

<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset"> 
	<h1 class="clearfix page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?><i class="icon clip_icon"></i></h1>
	<hr class="separator1">

	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; ?>
                 
	<?php if($settings->shortDesc){ ?>
	<div class="thesis_text second mb71">
			<?=$settings->shortDesc?>
	</div>
	<?php } ?>				 

	<div class="nsActionView">

		<div class="diagrams_container">
			<?php $this->widget( 'widgets.graphs.InterestRatesColumnChartWidget', Array('ns' => 'nsActionView',)); ?>
			<?php $this->widget( 'widgets.graphs.InterestRatesLineChartWidget', Array('ns' => 'nsActionView',)); ?>
		</div>
		
		<?php
			$this->widget( 'widgets.lists.InterestRatesListWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
				'showOnEmpty' => true,
			));
		?>
		<?php $this->widget( 'widgets.lists.InterestRatesReviewsWidget', Array('ns' => 'nsActionView')); ?>
	
	</div>
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
	
	<?php if($settings->fullDesc){ ?>
		<div class="after_table_text second">
			<p itemprop="text">
				<?=$settings->fullDesc?>
			</p>
		</div>
	<?php }?>
	
	<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
	
</section>