<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'informers/single/view' );
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset hrNone">
	<h1 class="page_title1" itemprop="name"><?=$this->action->title?> <?php if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?></h1>
	<hr class="separator1">
	
	<?php 
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; 
		if( $catModel->shortDesc ){
			echo CHtml::tag( 'div', array( 'class' => 'inform-thesis_text thesis_text second mb71' ), $catModel->shortDesc );
		}
	?>
	
	<div class="nsActionView" itemscope itemtype="http://schema.org/WebApplication">
		<meta itemprop="applicationCategory" content="<?=$catModel->title?>" />
		<meta itemprop="operatingSystem" content="Windows, OSX, Android, Linux, iOS, BlackBerry" />
		<?php
			$this->widget( 'widgets.InformerSettingsWidget', Array(
				'ns' => 'nsActionView',
				'category' => $catModel,
			));
		?>
	</div>

	<?php 
		if( $catModel->fullDesc ){
			echo CHtml::openTag('section', array( 'class' => 'section_offset' ));
				echo CHtml::tag('div', array( 'class' => 'desc', 'itemprop' => 'text' ), $catModel->fullDesc);
			echo CHtml::closeTag('section');
		}
	?>
	
	<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php'; ?>
	
	<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>
	
</section>