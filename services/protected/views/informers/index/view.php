<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'informers/index/view' );
?>

<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>
<section class="section_offset"> 
	<h1 class="clearfix page_title1" itemprop="name"><?=$this->action->title?> <?php //if($commentsCount) echo CHtml::link( "($commentsCount)", '#comments' );?><i class="icon clip_icon"></i></h1>
	<hr class="separator1 inform-separator">

	<?php 
		require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php'; 
		
		if($settings->shortDesc){
			echo CHtml::tag('div', array( 'class' => 'inform-thesis_text thesis_text second mb71' ), $settings->shortDesc);
		}
	?>
	
	<div class="nsActionView">
		<?php 
			$this->widget( 'widgets.lists.InformersListWidget', Array(
				'ns' => 'nsActionView',
				'title' => $settings->catsListTitle
			)); 
		?>
	
	
        <?php
            if( $settings->infoLink ){
                echo CHtml::openTag('div', array( 'class' => 'informer-bottom-text' ));
                    echo CHtml::link( $settings->infoLinkText, $settings->infoLink );
                echo CHtml::closeTag('div');
            }
            
            require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php';

            $this->widget('widgets.UnisenderSubscribeWidget', array(
                'ns' => 'nsActionView',
                'listId' => 12531533
            ));
        
            if($settings->fullDesc){ 
                echo CHtml::tag('div', array( 'itemprop' => 'text' ), $settings->fullDesc);
            }
        ?>
    </div>
	<?php /*<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>*/ ?>
</section>		