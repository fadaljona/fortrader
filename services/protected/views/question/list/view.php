<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'question/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<h1>
			<?=Yii::t( $NSi18n, 'Questions and suggestions' )?>
		</h1>
		<?if( $obj->id ){?>
			<?=Yii::t( $NSi18n, $obj->name )?>
		<?}?>
	</div>
	<div class="row-fluid">
		<?
			$this->widget( 'widgets.UsersConversationWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
			));
		?>
	</div>
	
	<div class="row-fluid" style="margin-top:20px;">
		<?
			$this->widget( 'widgets.PostInformWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
	</div>
</div>