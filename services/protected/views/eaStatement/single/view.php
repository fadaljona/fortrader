<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'eaStatemetn/single/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative">
		<h1>
			<?=htmlspecialchars( $statement->title )?>
		</h1>
	</div>
	<?
		$this->widget( 'widgets.EAStatementProfileWidget', Array(
			'ns' => 'nsActionView',
			'model' => $statement,
		));
	?>
</div>