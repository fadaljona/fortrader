<?
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/assets-static/ckeditor/ckeditor.js" );
	Yii::App()->clientScript->registerScriptFile( Yii::app()->baseUrl."/assets-static/ckeditor/adapters/jquery.js" );
	
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'eaStatement/single/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative">
		<h1>
			<?=htmlspecialchars( $EA->name )?>
			<small>
				<i class="icon-double-angle-right"></i>
				<?=Yii::t( '*', 'Adding statement' )?>
			</small>
		</h1>
	</div>
	<div style="max-width:800px;">
		<?
			$this->widget( 'widgets.forms.EAStatementFormWidget', Array(
				'ns' => 'nsActionView',
				'model' => $formModel,
			));
		?>
	</div>
</div>