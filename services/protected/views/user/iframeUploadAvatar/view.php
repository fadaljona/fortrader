<?
	Yii::app()->clientScript->registerCoreScript( 'jquery' );
?>
<script>
	!function( $ ) {
		<?if( !empty( $data->error )){?>
			var error = <?=json_encode( $data->error )?>;
			parent.nsActionView.wUserAvatarFormWidget ? parent.nsActionView.wUserAvatarFormWidget.submitError( error )
													  : parent.nsActionView.wUserProfileWidget.submitAvatarError( error );
		<?}else{?>
			var id = <?=$data->model->ID?>;
			var thumbPath = <?=json_encode($data->model->avatar->thumbPath)?>;
			var middlePath = <?=json_encode($data->model->avatar->middlePath)?>;
			parent.nsActionView.wUserAvatarFormWidget ? parent.nsActionView.wUserAvatarFormWidget.submitSuccess( id, thumbPath, middlePath )
													  : parent.nsActionView.wUserProfileWidget.submitAvatarSuccess( id, thumbPath, middlePath );
		<?}?>
	}( window.jQuery );
</script>