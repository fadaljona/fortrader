<?
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n( 'user/settings/view' );
?>
<script>
	var nsActionView = {};
</script>


<section class="section_offset"> 
	<div class="profile__title-wr clearfix">
		<h1 class="page_title1 profile__title" itemprop="name"><?=$this->action->title?></h1>
		<a href="<?=$model->singleURL?>" class="profile-btn_settings"><?=Yii::t('*', 'View profile')?> <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/icon-head-w.png" alt="" class="profile-btn_settings-img"></a>
	</div>
	
	<div class="nsActionView">
		
		<?
			$this->widget( 'widgets.UserSettingsWidget', Array(
				'ns' => 'nsActionView',
			));
		?>
	</div>
	
</section>