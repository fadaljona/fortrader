<?
Yii::import('controllers.base.ViewBase');
?>
<script>
	var nsActionView = {};
</script>

<meta itemprop="description" content="<?=$metaDesc?>" /> 


<section class="section_offset"> 
	<div class="profile__title-wr clearfix">
		<h1 class="page_title1 profile__title" itemprop="name"><?=$this->action->title?></h1>
		<?php if($canControl){?><a href="<?=Yii::app()->createUrl('user/settings', array('id' => $model->id))?>" class="profile-btn_settings"><?=Yii::t('*', 'Profile settings')?> <img src="<?=Yii::app()->params['wpThemeUrl']?>/images/icon-edit.png" alt="" class="profile-btn_settings-img"></a><?php }?>
	</div>
	
	<div class="nsActionView">
		
		<?php
			$this->widget( 'widgets.UserProfileWidget', Array(
				'ns' => 'nsActionView',
				'model' => $model,
			));
		?>
		
		<?php
			$cacheKey = 'userProfileWpContent.lang.' . Yii::app()->language . '.user.'. $model->id;
			if( !Yii::app()->cache->get( $cacheKey ) ){
				ob_start();	
				CommonLib::loadWp();
				require_once Yii::App()->params['wpThemePath'].'/templates/author-wp-content.php';	
				$blockContent = ob_get_clean();
				Yii::app()->cache->set( $cacheKey, $blockContent, 60*60 * 6);
			}
			echo Yii::app()->cache->get( $cacheKey );
		?>
		<div id="comments"><? $this->widget('widgets.DeCommentsWidget',Array( 'ns' => 'nsActionView', 'paramStr' => $paramStr, 'cat' => $cat )); ?></div>

	</div>
	
</section>