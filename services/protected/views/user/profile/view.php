<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'user/profile/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative">
		<h1>
			<?=Yii::t( $NSi18n, 'User profile' )?>
		</h1>
	</div>
	<?
		$this->widget( 'widgets.UserProfileWidget', Array(
			'ns' => 'nsActionView',
			'model' => $model,
		));
	?>
</div>
