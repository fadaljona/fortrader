<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'user/unsubscribeMailingType/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative">
		<h1>
			<?=Yii::t( $NSi18n, 'Unsubscribe our mailing list' )?>
		</h1>
	</div>
	
	<?
		$this->widget( 'widgets.forms.UnsubscribeCommentFormWidget', Array(
			'ns' => 'nsActionView',
			'model' => $formModel,
			'mailingType' => $type,
		));
	?>
</div>
