<?
	Yii::import( 'controllers.base.ViewBase' );
	$NSi18n = ViewBase::getNSi18n( 'eaTradeAccount/list/view' );
?>
<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<div class="pull-right" style="margin-top:5px">
			<?$this->widget( 'widgets.EANavWidget', Array( 'ns' => 'nsActionView' ));?>
		</div>
		<h1>
			<?=Yii::t( $NSi18n, 'EA monitoring' )?>
		</h1>
	</div>
	<div class="row-fluid">
		<?/*
			$this->widget( 'widgets.graphs.EATradeAccountsGraphWidget', Array(
				'ns' => 'nsActionView',
			));
		*/?>
	</div>
	<h3 class="header smaller lighter blue">
		<?=Yii::t( $NSi18n, 'EA' )?>
	</h3>
	<div class="row-fluid">
		<?
			$this->widget( 'widgets.lists.EATradeAccountsListWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
				'showOnEmpty' => true,
			));
		?>
	</div>
</div>