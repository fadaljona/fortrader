<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n('exchangeECurrency/exchangersRating/view');
?>

<script>var nsActionView = {};</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>

<div class="fx_section">                        
    <div>
        <h1 class="fx_content-title-link"><?=$this->action->title?></h1>
    </div>
    <div class='fx_section-box'>
        <?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php';?>
    </div>
    <?php
    if ($settings->exchangersRatingShortDesc) {
        echo CHtml::tag('div', array( 'class' => 'fx_section-box' ), $settings->exchangersRatingShortDescWithFormat);
    }
    ?>
</div>
<div class="nsActionView">
    <?php
    $this->widget('widgets.lists.ExchangersRatingListWidget', array(
        'ns' => 'nsActionView'
    ));
    ?>
</div>
<div class="fx_section">
    <?php
    if ($settings->exchangersRatingFullDescTitle) {
        echo CHtml::tag('div', array(), CHtml::tag('h3', array('class' => 'fx_content-title-link'), $settings->exchangersRatingFullDescTitle));
    }
    if ($settings->exchangersRatingFullDesc) {
        echo CHtml::tag('div', array( 'class' => 'fx_section-box' ), $settings->exchangersRatingFullDescWithFormat);
    }
    ?>
</div>


<?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php';?>