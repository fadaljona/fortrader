<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n('exchangeECurrency/index/view');
?>

<script>var nsActionView = {};</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>

<div class="fx_section">                        
    <div>
        <h1 class="fx_content-title-link"><?=$this->action->title?></h1>
    </div>
    <div class='fx_section-box'>
        <?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php';?>
    </div>
    <?php
    if ($model->shortDesc) {
        echo CHtml::tag('div', array( 'class' => 'fx_section-box' ), $model->shortDescWithFormat);
    }
    ?>
</div>
<div class="nsActionView">
    <?php
    $this->widget('widgets.ChooseExchangerByDirectionWidget', array(
        'ns' => 'nsActionView',
        'fromCurrency' => $model->from,
        'toCurrency' => $model->to
    ));
    ?>
</div>
<div class="fx_section">
    <?php
    if ($model->fullDescTitle) {
        echo CHtml::tag('div', array(), CHtml::tag('h3', array('class' => 'fx_content-title-link'), $model->fullDescTitle));
    }
    if ($model->fullDesc) {
        echo CHtml::tag('div', array( 'class' => 'fx_section-box' ), $model->fullDescWithFormat);
    }
    ?>
</div>


<?php
require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php';
?>