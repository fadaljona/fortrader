<?php
Yii::import('controllers.base.ViewBase');
$NSi18n = ViewBase::getNSi18n('exchangeECurrency/index/view');
?>

<script>var nsActionView = {};</script>

<meta itemprop="description" content="<?=$metaDesc?>" />

<hr>

<div class="fx_section">                        
    <div>
        <h1 class="fx_content-title-link"><?=$this->action->title?></h1>
    </div>
    <div class='fx_section-box'>
        <?php require_once Yii::App()->params['wpThemePath'].'/templates/sm-top-post-buttons.php';?>
    </div>
    <?php
    if ($settings->mainShortDesc) {
        echo CHtml::tag('div', array( 'class' => 'fx_section-box' ), $settings->mainShortDescWithFormat);
    }
    ?>
</div>
<div class="nsActionView">
    <?php
    $this->widget('widgets.ChooseExchangerByDirectionWidget', array(
        'ns' => 'nsActionView'
    ));
    ?>
</div>
<div class="fx_section">
    <?php
    if ($settings->mainFullDescTitle) {
        echo CHtml::tag('div', array(), CHtml::tag('h3', array('class' => 'fx_content-title-link'), $settings->mainFullDescTitle));
    }
    if ($settings->mainFullDesc) {
        echo CHtml::tag('div', array( 'class' => 'fx_section-box' ), $settings->mainFullDescWithFormat);
    }
    ?>
</div>


<?php
require_once Yii::App()->params['wpThemePath'].'/templates/sm-bottom-post-buttons-yii.php';

$cacheKey = 'servicesLatestForexNews' . Yii::app()->language;
if (!Yii::app()->cache->get($cacheKey)){
    ob_start();
    CommonLib::loadWp();
    require_once Yii::App()->params['wpThemePath'].'/templates/latest-forex-news.php';
    $blockContent = ob_get_clean();
    Yii::app()->cache->set($cacheKey, $blockContent, 60*30);
}
echo Yii::app()->cache->get($cacheKey);
?>