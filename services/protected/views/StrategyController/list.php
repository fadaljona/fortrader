<script>
	var nsActionView = {};
</script>
<div class="nsActionView">
	<div class="page-header position-relative row-fluid">
		<h1>
			<?=$this->translateR( 'Strategies' )?>
		</h1>
	</div>
	<div class="row-fluid">
		<? ListWidgetBase::widgetByModelName( 'Strategy' ) ?>
	</div>
	<div class="row-fluid" style="padding-top:20px;">
		<?
			$this->widget( 'widgets.UserActivitiesWidget', Array( 
				'ns' => 'nsActionView', 
			))
		?>
	</div>
	<div class="row-fluid">
		<h3 class="header smaller lighter blue"></h3>
		<?
			$this->widget( 'widgets.UsersConversationWidget', Array(
				'ns' => 'nsActionView',
				'ajax' => true,
			));
		?>
	</div>
</div>