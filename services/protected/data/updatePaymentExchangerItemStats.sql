DROP TEMPORARY TABLE IF EXISTS `temp_payment_exchanger_item_stats`;

CREATE TEMPORARY TABLE `temp_payment_exchanger_item_stats` (
	`idExchanger` INT(10) UNSIGNED NOT NULL,
	`average` DOUBLE UNSIGNED NULL,
	`votesCount` INT(10) UNSIGNED NOT NULL
)
ENGINE=Memory;

-- collect stats
INSERT IGNORE INTO 	`temp_payment_exchanger_item_stats` ( `idExchanger` )
SELECT				`{{payment_exchanger_item_mark}}`.`idExchanger`
FROM				`{{payment_exchanger_item_mark}}`
GROUP BY `{{payment_exchanger_item_mark}}`.`idExchanger`;

-- det average
UPDATE 			`temp_payment_exchanger_item_stats`
SET				`average` = (
					SELECT 	AVG( `value` )
					FROM	`{{payment_exchanger_item_mark}}`
					WHERE	`{{payment_exchanger_item_mark}}`.`idExchanger` = `temp_payment_exchanger_item_stats`.`idExchanger`
				);
				
-- det votesCount

UPDATE 			`temp_payment_exchanger_item_stats`
SET				`votesCount` = (
					SELECT 	COUNT( `value` )
					FROM	`{{payment_exchanger_item_mark}}`
					WHERE	`{{payment_exchanger_item_mark}}`.`idExchanger` = `temp_payment_exchanger_item_stats`.`idExchanger`
				);
	
-- return stats
REPLACE INTO 	`{{payment_exchanger_item_stats}}` ( `idExchanger`, `average`, `updatedDT`, `votesCount` )
SELECT			`idExchanger`,
				ROUND( `average`, 1 ),
				NOW(),
				`votesCount`
FROM			`temp_payment_exchanger_item_stats`;

-- drop temp struct
DROP TEMPORARY TABLE `temp_payment_exchanger_item_stats`;