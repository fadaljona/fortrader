-- create temp struct
DROP TEMPORARY TABLE IF EXISTS `temp_ea_stats`;

CREATE TEMPORARY TABLE `temp_ea_stats` (
	`idEA` INT(10) UNSIGNED NOT NULL,
	`average` TINYINT(3) UNSIGNED NULL,
	`countMarks` INT(10) UNSIGNED NULL
)
COLLATE='utf8_general_ci'
ENGINE=Memory;

-- collect stats
INSERT INTO 	`temp_ea_stats` ( `idEA` )
SELECT			`{{ea_stats}}`.`idEA`
FROM			`{{ea_stats}}`
WHERE			`{{ea_stats}}`.`updatedDT` IS NULL
	OR 			`{{ea_stats}}`.`updatedDT` != CURDATE();

-- det average
UPDATE 			`temp_ea_stats`
SET				`average` = (
					SELECT 	ROUND(AVG( `average` ))
					FROM	`{{ea_mark}}`
					WHERE	`{{ea_mark}}`.`idEA` = `temp_ea_stats`.`idEA`
				);
	
-- det countMarks
UPDATE 			`temp_ea_stats`
SET				`countMarks` = (
					SELECT 	COUNT(*)
					FROM	`{{ea_mark}}`
					WHERE	`{{ea_mark}}`.`idEA` = `temp_ea_stats`.`idEA`
				);	

-- return stats
REPLACE INTO 	`{{ea_stats}}` ( `idEA`, `average`, `countMarks`, `updatedDT` )
SELECT			`idEA`,
				`average`,
				`countMarks`,
				NOW()
FROM			`temp_ea_stats`;

-- drop temp struct
DROP TEMPORARY TABLE `temp_ea_stats`;