-- create temp struct
DROP TEMPORARY TABLE IF EXISTS `temp_contest_member_stats_d1`;
DROP TEMPORARY TABLE IF EXISTS `temp_contest_member_stats_d2`;

CREATE TEMPORARY TABLE `temp_contest_member_stats_d1` (
	`idMember` INT(10) UNSIGNED NOT NULL,
	`accountNumber` INT(10) UNSIGNED NOT NULL,
	`idServer` INT(10) UNSIGNED NOT NULL,
	`nameServer` VARCHAR( 255 ) NOT NULL,
	`urlServer` VARCHAR( 255 ) NOT NULL,
	`tradePlatform` VARCHAR(255) NOT NULL,
	`countOpenDeals` INT(10) UNSIGNED NOT NULL DEFAULT 0,
	`idLastMT5AccountData` INT(10) UNSIGNED NULL,
	`balance` DOUBLE NULL DEFAULT NULL,
	`deposit` DOUBLE NULL DEFAULT NULL,
	`withdrawals` DOUBLE NULL DEFAULT NULL,
	`equity` DOUBLE NULL DEFAULT NULL,
	`profit` DOUBLE NULL DEFAULT NULL,
	`minEquity` DOUBLE NULL DEFAULT NULL,
	`margin` DOUBLE NULL DEFAULT NULL,
	`freeMargin` DOUBLE NULL DEFAULT NULL,
	`countTrades` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`countOpen` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`gain` DOUBLE NULL DEFAULT NULL,
	`drowAbs` DOUBLE NULL DEFAULT NULL,
	`drowMax` DOUBLE NULL DEFAULT NULL,
	`last_error_code` INT(11) NULL DEFAULT NULL,
	`trueDT` DATETIME NULL DEFAULT NULL,
	
-- new fields
	`leverage` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`leverageCganged` TINYINT(1) NOT NULL DEFAULT '0',
	`leverageCgangedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`avgTradeTimeSec` INT(15) UNSIGNED NOT NULL DEFAULT '0',
	`bestTrade` DOUBLE NULL DEFAULT NULL,
	`worstTrade` DOUBLE NULL DEFAULT NULL,
	`profitableTrades`  DOUBLE NULL DEFAULT NULL,
	`allTrades` DOUBLE NULL DEFAULT NULL,
-- new fields end

	`status` enum('participates','disqualified','quitted','completed','invalid password') NOT NULL DEFAULT 'participates',

	PRIMARY KEY (`idMember`)
)
COLLATE='utf8_general_ci'
ENGINE=Memory;

CREATE TEMPORARY TABLE `temp_contest_member_stats_d2` (
	`newPlace` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`idMember` INT(10) UNSIGNED NOT NULL,
	`balance` DOUBLE NULL DEFAULT NULL,
	`deposit` DOUBLE NULL DEFAULT NULL,
	`withdrawals` DOUBLE NULL DEFAULT NULL,
	`equity` DOUBLE NULL DEFAULT NULL,
	`profit` DOUBLE NULL DEFAULT NULL,
	`minEquity` DOUBLE NULL DEFAULT NULL,
	`margin` DOUBLE NULL DEFAULT NULL,
	`freeMargin` DOUBLE NULL DEFAULT NULL,
	`countTrades` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`countOpen` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`gain` DOUBLE NULL DEFAULT NULL,
	`drowAbs` DOUBLE NULL DEFAULT NULL,
	`drowMax` DOUBLE NULL DEFAULT NULL,
	`last_error_code` INT(11) NULL DEFAULT NULL,
	`trueDT` DATETIME NULL DEFAULT NULL,
	
-- new fields
	`leverage` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`leverageCganged` TINYINT(1) NOT NULL DEFAULT '0',
	`leverageCgangedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`avgTradeTimeSec` INT(15) UNSIGNED NOT NULL DEFAULT '0',
	`bestTrade` DOUBLE NULL DEFAULT NULL,
	`worstTrade` DOUBLE NULL DEFAULT NULL,
	`profitableTrades`  DOUBLE NULL DEFAULT NULL,
	`allTrades` DOUBLE NULL DEFAULT NULL,
-- new fields end	
	
	PRIMARY KEY (`newPlace`)
)
COLLATE='utf8_general_ci'
ENGINE=Memory;

-- collect d1
INSERT INTO 	`temp_contest_member_stats_d1` ( `idMember`, `accountNumber`, `idServer`, `nameServer`, `urlServer`, `tradePlatform`, `status` )
SELECT			`{{contest_member}}`.`id`,
				`{{contest_member}}`.`accountNumber`,
				`{{contest_member}}`.`idServer`,
				`{{server}}`.`internalName`,
				`ft_server`.`url`,
				`{{trade_platform}}`.`name`,
				`{{contest_member}}`.`status`
FROM			`{{contest_member}}` 
INNER JOIN		`{{server}}` ON `{{server}}`.`id` = `{{contest_member}}`.`idServer`
INNER JOIN		`{{trade_platform}}` ON `{{trade_platform}}`.`id` = `{{server}}`.`idTradePlatform`
WHERE			`{{contest_member}}`.`idContest` = :idContest AND `{{server}}`.`forReports` = 0;

-- det idLastMT5AccountData
UPDATE			`temp_contest_member_stats_d1`
SET				`idLastMT5AccountData` = (
					SELECT 			MAX( `id` )
					FROM			`mt5_account_data`
					WHERE			`mt5_account_data`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt5_account_data`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
				);
				



				
				
				
				
				
				
				
				
				


-- det leverage
		
UPDATE			`temp_contest_member_stats_d1`
SET				`leverage` = (
					SELECT `mt5data`.`ACCOUNT_LEVERAGE` FROM 
					(
						SELECT	`mt5_account_data`.*
						FROM	`mt5_account_data`
						ORDER BY	`mt5_account_data`.`timestamp` ASC
					) as	`mt5data`
					WHERE	`mt5data`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
							AND	`mt5data`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
					GROUP BY	`mt5data`.`ACCOUNT_LEVERAGE`
					order by	`mt5data`.`timestamp` DESC
					LIMIT 1
				);		
				
			
-- det leverageCgangedDate
		
UPDATE			`temp_contest_member_stats_d1`
SET				`leverageCgangedDate` = (
					SELECT `mt5data`.`timestamp` FROM 
					(
						SELECT	`mt5_account_data`.*
						FROM	`mt5_account_data`
						ORDER BY	`mt5_account_data`.`timestamp` ASC
					) as	`mt5data`
					WHERE	`mt5data`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
							AND	`mt5data`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
					GROUP BY	`mt5data`.`ACCOUNT_LEVERAGE`
					order by	`mt5data`.`timestamp` DESC
					LIMIT 1
				);				
				
-- det leverageCganged
		
UPDATE			`temp_contest_member_stats_d1`
SET				`leverageCganged` = (
					SELECT IF( count(`mt5data`.`ACCOUNT_NUMBER`) = 1 ,0,1) AS leverageCganged from 
					(
						SELECT `mt5_account_data`.*
						FROM `mt5_account_data`
						GROUP BY `mt5_account_data`.`ACCOUNT_NUMBER`, `mt5_account_data`.`ACCOUNT_LEVERAGE`
					) AS `mt5data`
					WHERE	`mt5data`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
							AND	`mt5data`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
				);		
				
-- det avgTradeTimeSec

UPDATE			`temp_contest_member_stats_d1`
SET				`avgTradeTimeSec` = (
					SELECT	ROUND( AVG ( ABS ( `mt4_account_history`.`OrderOpenTime` - `mt4_account_history`.`OrderCloseTime` ) ) )  AS `avgtime`
					FROM	`mt4_account_history`
					WHERE	`mt4_account_history`.`AccountNumber` = `temp_contest_member_stats_d1`.`accountNumber`
							AND	`mt4_account_history`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
							AND `mt4_account_history`.`OrderType` = (0 OR 1)
				);		

-- det bestTrade

UPDATE			`temp_contest_member_stats_d1`
SET				`bestTrade` = (
					SELECT	MAX( `mt4_account_history`.`OrderProfit` )  AS `maxProfit`
					FROM	`mt4_account_history`
					WHERE	`mt4_account_history`.`AccountNumber` = `temp_contest_member_stats_d1`.`accountNumber`
							AND	`mt4_account_history`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
							AND	`mt4_account_history`.`OrderSymbol` IS NOT NULL
							AND `mt4_account_history`.`OrderType` <> 6
				);	
				
-- det worstTrade

UPDATE			`temp_contest_member_stats_d1`
SET				`worstTrade` = (
					SELECT	MIN( `mt4_account_history`.`OrderProfit` )  AS `maxProfit`
					FROM	`mt4_account_history`
					WHERE	`mt4_account_history`.`AccountNumber` = `temp_contest_member_stats_d1`.`accountNumber`
							AND	`mt4_account_history`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
							AND	`mt4_account_history`.`OrderSymbol` IS NOT NULL
							AND `mt4_account_history`.`OrderType` <> 6
				);	
				
-- det profitableTrades
				
UPDATE			`temp_contest_member_stats_d1`
SET				`profitableTrades` = (
					SELECT	count( `mt4_account_history`.`id` )  AS `countProfitTrades`
					FROM	`mt4_account_history`
					WHERE	`mt4_account_history`.`AccountNumber` = `temp_contest_member_stats_d1`.`accountNumber`
							AND	`mt4_account_history`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
							AND `mt4_account_history`.`OrderType` = (0 OR 1) 
							AND `mt4_account_history`.`OrderProfit` > 0
				);	
				
-- det allTrades
				
UPDATE			`temp_contest_member_stats_d1`
SET				`allTrades` = (
					SELECT	count( `mt4_account_history`.`id` )  AS `allTrades`
					FROM	`mt4_account_history`
					WHERE	`mt4_account_history`.`AccountNumber` = `temp_contest_member_stats_d1`.`accountNumber`
							AND	`mt4_account_history`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
							AND `mt4_account_history`.`OrderType` = (0 OR 1) 
				);	

















				

-- det countOpenDeals
UPDATE			`temp_contest_member_stats_d1`
SET				`countOpenDeals` = (
					SELECT 			COUNT( * )
					FROM			`mt5_account_trades`
					WHERE			`mt5_account_trades`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt5_account_trades`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
				);
			
-- det balance
UPDATE			`temp_contest_member_stats_d1`
SET				`balance` = (
					SELECT			`ACCOUNT_BALANCE`
					FROM			`mt5_account_data`
					WHERE			`mt5_account_data`.`id` = `temp_contest_member_stats_d1`.`idLastMT5AccountData`
					LIMIT			1
				)
WHERE			`temp_contest_member_stats_d1`.`idLastMT5AccountData` IS NOT NULL;

-- det deposit
UPDATE			`temp_contest_member_stats_d1`
SET				`deposit` = (
					SELECT			SUM( `DEAL_PROFIT` )
					FROM 			`mt5_account_history_deals`
					WHERE 			`mt5_account_history_deals`.`DEAL_TYPE` = 'balance' 
						AND 		`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt5_account_history_deals`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
				)
WHERE			`tradePlatform` = 'MetaTrader 5';

UPDATE			`temp_contest_member_stats_d1`
SET				`deposit` = (
					SELECT			SUM( `OrderProfit` )
					FROM 			`mt4_account_history`
					WHERE 			`mt4_account_history`.`OrderType` = 6
						AND 		`mt4_account_history`.`AccountNumber` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt4_account_history`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
				)
WHERE			`tradePlatform` = 'MetaTrader 4';

UPDATE			`temp_contest_member_stats_d1`
SET				`deposit` = (
					SELECT			`startDeposit`
					FROM 			`ft_contest`
					WHERE 			`ft_contest`.`id` = :idContest
				)
WHERE			`deposit` IS NULL;


-- det withdrawals
UPDATE			`temp_contest_member_stats_d1`
SET				`withdrawals` = (
					SELECT				SUM( `DEAL_PROFIT` )
					FROM 				`mt5_account_history_deals`
					WHERE 				`mt5_account_history_deals`.`DEAL_TYPE` = 'balance' 
						AND 			`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND				`mt5_account_history_deals`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
						AND 			`mt5_account_history_deals`.`DEAL_PROFIT` < 0
				)
WHERE			`tradePlatform` = 'MetaTrader 5';

UPDATE			`temp_contest_member_stats_d1`
SET				`withdrawals` = (
					SELECT				SUM( `OrderProfit` )
					FROM 				`mt4_account_history`
					WHERE 				`mt4_account_history`.`OrderType` = 6 
						AND 			`mt4_account_history`.`AccountNumber` = `temp_contest_member_stats_d1`.`accountNumber`
						AND				`mt4_account_history`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
						AND 			`mt4_account_history`.`OrderProfit` < 0
				)
WHERE			`tradePlatform` = 'MetaTrader 4';

-- det profit
UPDATE			`temp_contest_member_stats_d1`
SET				`profit` = `balance` - `deposit`;	
				
-- det equity
UPDATE			`temp_contest_member_stats_d1`
SET				`equity` = `balance`
WHERE			NOT `countOpenDeals`;

UPDATE			`temp_contest_member_stats_d1`
SET				`equity` = (
					SELECT			`ACCOUNT_EQUITY`
					FROM			`mt5_account_data`
					WHERE			`mt5_account_data`.`id` = `temp_contest_member_stats_d1`.`idLastMT5AccountData`
					LIMIT			1
				)
WHERE			`countOpenDeals`;

-- det margin
UPDATE			`temp_contest_member_stats_d1`
SET				`margin` = 0
WHERE			NOT `countOpenDeals`;

UPDATE			`temp_contest_member_stats_d1`
SET				`margin` = (
					SELECT			`ACCOUNT_MARGIN`
					FROM			`mt5_account_data`
					WHERE			`mt5_account_data`.`id` = `temp_contest_member_stats_d1`.`idLastMT5AccountData`
					LIMIT			1
				)
WHERE			`countOpenDeals`;

-- det freeMargin
UPDATE			`temp_contest_member_stats_d1`
SET				`freeMargin` = 0
WHERE			NOT `countOpenDeals`;

UPDATE			`temp_contest_member_stats_d1`
SET				`freeMargin` = (
					SELECT			`ACCOUNT_FREEMARGIN`
					FROM			`mt5_account_data`
					WHERE			`mt5_account_data`.`id` = `temp_contest_member_stats_d1`.`idLastMT5AccountData`
					LIMIT			1
				)
WHERE			`countOpenDeals`;

-- det minEquity
UPDATE			`temp_contest_member_stats_d1`
SET				`minEquity` = (
					SELECT			MIN( `ACCOUNT_EQUITY` )
					FROM 			`mt5_account_data`
					WHERE 			`mt5_account_data`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt5_account_data`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
				);
				
-- det countTrades
UPDATE			`temp_contest_member_stats_d1`
SET				`countTrades` = (
					SELECT 			COUNT(*)
					FROM 			`mt5_account_history_deals`
					WHERE			`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt5_account_history_deals`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
						AND			`mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
				)
WHERE			`tradePlatform` = 'MetaTrader 5';

UPDATE			`temp_contest_member_stats_d1`
SET				`countTrades` = (
					SELECT 			COUNT(*)
					FROM 			`mt4_account_history`
					WHERE			`mt4_account_history`.`AccountNumber` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt4_account_history`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
						AND			`mt4_account_history`.`OrderType` IN ( 0, 1 )
				)
WHERE			`tradePlatform` = 'MetaTrader 4';
	
	
-- det countOpen
UPDATE			`temp_contest_member_stats_d1`
SET				`countOpen` = (
					SELECT 			COUNT(*)
					FROM 			`mt5_account_trades`
					WHERE			`mt5_account_trades`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt5_account_trades`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
				);
	
-- det gain
UPDATE			`temp_contest_member_stats_d1`
SET				`gain` = ( `balance` - `deposit` ) / `deposit` * 100;	

-- det drowAbs
UPDATE			`temp_contest_member_stats_d1`
SET				`drowAbs` = - ABS( ( `deposit` - `minEquity` ) / `deposit` * 100  );	

-- det drowMax
UPDATE			`temp_contest_member_stats_d1`
SET				`drowMax` = (
					SELECT			- GREATEST( MAX( `ACCOUNT_EQUITY_DROW_PERC` ), 0 )
					FROM			`mt5_account_data`
					WHERE 			`mt5_account_data`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt5_account_data`.`AccountServerId` = `temp_contest_member_stats_d1`.`idServer`
				);	
				
-- det last_error_code
UPDATE			`temp_contest_member_stats_d1`
SET				`last_error_code` = ((
					SELECT			`last_error_code`
					FROM			`mt5_account_info`
					WHERE			`mt5_account_info`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt5_account_info`.`ServerUrl` = `temp_contest_member_stats_d1`.`urlServer`
					LIMIT			1
				));				
				
-- det trueDT
UPDATE			`temp_contest_member_stats_d1`
SET				`trueDT` = FROM_UNIXTIME((
					SELECT			`ACCOUNT_UPDATE`
					FROM			`mt5_account_info`
					WHERE			`mt5_account_info`.`ACCOUNT_NUMBER` = `temp_contest_member_stats_d1`.`accountNumber`
						AND			`mt5_account_info`.`ServerUrl` = `temp_contest_member_stats_d1`.`urlServer`
					LIMIT			1
				));


-- collect d2
INSERT INTO 	`temp_contest_member_stats_d2` 
			  ( `idMember`, `balance`, `deposit`, `withdrawals`, `equity`, `profit`, `minEquity`, `margin`, `freeMargin`, `countTrades`, `countOpen`, `gain`, `drowAbs`, `drowMax`, `last_error_code`, `trueDT`, `leverage`, `leverageCganged`, `leverageCgangedDate`, `avgTradeTimeSec`, `bestTrade`, `worstTrade`, `profitableTrades`, `allTrades` )
SELECT			`idMember`, `balance`, `deposit`, `withdrawals`, `equity`, `profit`, `minEquity`, `margin`, `freeMargin`, `countTrades`, `countOpen`, `gain`, `drowAbs`, `drowMax`, `last_error_code`, `trueDT`, `leverage`, `leverageCganged`, `leverageCgangedDate`, `avgTradeTimeSec`, `bestTrade`, `worstTrade`, `profitableTrades`, `allTrades`
FROM			`temp_contest_member_stats_d1` 
WHERE			(`status` = 'participates' OR `status` = 'completed') AND (`last_error_code` = 0 OR `last_error_code` = -1 OR `last_error_code` = -2 OR `last_error_code` IS NULL)
ORDER BY		`equity` DESC;

-- collect d2
INSERT INTO 	`temp_contest_member_stats_d2` 
			  ( `idMember`, `balance`, `deposit`, `withdrawals`, `equity`, `profit`, `minEquity`, `margin`, `freeMargin`, `countTrades`, `countOpen`, `gain`, `drowAbs`, `drowMax`, `last_error_code`, `trueDT`, `leverage`, `leverageCganged`, `leverageCgangedDate`, `avgTradeTimeSec`, `bestTrade`, `worstTrade`, `profitableTrades`, `allTrades` )
SELECT			`idMember`, `balance`, `deposit`, `withdrawals`, `equity`, `profit`, `minEquity`, `margin`, `freeMargin`, `countTrades`, `countOpen`, `gain`, `drowAbs`, `drowMax`, `last_error_code`, `trueDT`, `leverage`, `leverageCganged`, `leverageCgangedDate`, `avgTradeTimeSec`, `bestTrade`, `worstTrade`, `profitableTrades`, `allTrades`
FROM			`temp_contest_member_stats_d1` 
WHERE			(`status` = 'disqualified') OR (`last_error_code` = -255)
ORDER BY		`equity` DESC;
				
-- return stats				
REPLACE INTO 	`{{contest_member_stats}}` 
			  ( 
				`idMember`, 
				`balance`, 
				`place`,    
				`deposit`, 
				`withdrawals`, 
				`equity`, 
				`profit`, 
				`minEquity`, 
				`margin`, 
				`freeMargin`, 
				`countTrades`, 
				`countOpen`, 
				`gain`, 
				`drowAbs`, 
				`drowMax`, 
				`last_error_code`, 
				`DT`,  
				`trueDT`,
				`leverage`, `leverageCganged`, `leverageCgangedDate`, `avgTradeTimeSec`, `bestTrade`, `worstTrade`, `profitableTrades`, `allTrades`
			  )
SELECT			`idMember`, 
				ROUND( `balance`, 2 ), 
				`newPlace`, 
				ROUND( `deposit`, 2 ),
				ROUND( `withdrawals`, 2 ), 
				ROUND( `equity`, 2 ), 
				ROUND( `profit`, 2 ), 
				ROUND( `minEquity`, 2 ), 
				ROUND( `margin`, 2 ), 
				ROUND( `freeMargin`, 2 ), 
				`countTrades`, 
				`countOpen`, 
				ROUND( `gain`, 2 ), 
				ROUND( `drowAbs`, 2 ), 
				ROUND( `drowMax`, 2 ), 
				`last_error_code`, 
				NOW(), 
				`trueDT`,
				`leverage`, `leverageCganged`, `leverageCgangedDate`, `avgTradeTimeSec`, `bestTrade`, `worstTrade`, `profitableTrades`, `allTrades`
FROM			`temp_contest_member_stats_d2`;

-- det updatedStats
UPDATE			`{{contest}}`
SET				`updatedStats` = (
					SELECT		MAX( `trueDT` )
					FROM		`temp_contest_member_stats_d2`
				)
WHERE			`id` = :idContest
LIMIT			1;
								
-- drop temp struct
DROP TEMPORARY TABLE IF EXISTS `temp_contest_member_stats_d1`;
DROP TEMPORARY TABLE IF EXISTS `temp_contest_member_stats_d2`;