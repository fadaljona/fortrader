DROP TEMPORARY TABLE IF EXISTS `temp_journal_stats`;

CREATE TEMPORARY TABLE `temp_journal_stats` (
	`idJournal` INT(10) UNSIGNED NOT NULL,
	`average` DOUBLE UNSIGNED NULL
)
ENGINE=Memory;

-- collect stats
INSERT IGNORE INTO 	`temp_journal_stats` ( `idJournal` )
SELECT				`{{journal}}`.`id`
FROM				`{{journal}}`;

-- det average
UPDATE 			`temp_journal_stats`
SET				`average` = (
					SELECT 	AVG( `value` )
					FROM	`{{journal_mark}}`
					WHERE	`{{journal_mark}}`.`idJournal` = `temp_journal_stats`.`idJournal`
				);
	
-- return stats
REPLACE INTO 	`{{journal_stats}}` ( `idJournal`, `average`, `updatedDT` )
SELECT			`idJournal`,
				ROUND( `average`, 1 ),
				NOW()
FROM			`temp_journal_stats`;

-- drop temp struct
DROP TEMPORARY TABLE `temp_journal_stats`;