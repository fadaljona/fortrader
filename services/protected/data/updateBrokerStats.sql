-- consts
SET @m = 10;
SET @d = :daysToWeightReductionReview;

-- create temp struct
DROP TEMPORARY TABLE IF EXISTS `temp_broker_mark`;
DROP TEMPORARY TABLE IF EXISTS `temp_broker_stats_d1`;
DROP TEMPORARY TABLE IF EXISTS `temp_broker_stats_d2`;

CREATE TEMPORARY TABLE `temp_broker_mark` (
	`idBroker` INT(10) UNSIGNED NOT NULL,
	`Y` DOUBLE NOT NULL,
	`X` INT(10) UNSIGNED NOT NULL,
	`k` DOUBLE NULL,
	INDEX (`idBroker`)
)
ENGINE=Memory;

CREATE TEMPORARY TABLE `temp_broker_stats_d1` (
	`idBroker` INT(10) UNSIGNED NOT NULL,
	`place` INT(10) UNSIGNED NULL,
	`oldPlace` INT(10) UNSIGNED NULL,
	`changePlaceDT` DATETIME NULL,
	`countMarks` INT UNSIGNED NULL,
	`Rch` DOUBLE NULL,
	`Rz` DOUBLE NULL,
	`R` DOUBLE NULL,
	`v` DOUBLE NULL,
	`S` DOUBLE NULL,
	`W` DOUBLE NULL
)
ENGINE=Memory;

CREATE TEMPORARY TABLE `temp_broker_stats_d2` (
	`idBroker` INT(10) UNSIGNED NOT NULL,
	`newPlace` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`place` INT(10) UNSIGNED NULL,
	`oldPlace` INT(10) UNSIGNED NULL,
	`changePlaceDT` DATETIME NULL,
	`trust` TINYINT UNSIGNED NULL,
	`countMarks` INT UNSIGNED NULL,
	PRIMARY KEY (`newPlace`)
)
ENGINE=Memory;

-- collect marks
INSERT INTO 	`temp_broker_mark` ( `idBroker`, `Y`, `X` )
SELECT			`{{broker_mark}}`.`idBroker`, 
				`{{broker_mark}}`.`trust`,
				DATEDIFF( CURDATE(), `{{broker_mark}}`.`createdDT` )
FROM			`{{broker_mark}}`
INNER JOIN		`wp_users` ON `wp_users`.`ID` = `{{broker_mark}}`.`idUser`
WHERE			DATE_SUB( NOW(), INTERVAL 1 DAY ) > `wp_users`.`user_registered`
	AND			`{{broker_mark}}`.`trust` BETWEEN :minTrust AND :maxTrust;

-- det k
UPDATE 			`temp_broker_mark`
SET 			`k` = 1.106 * EXP( -0.001697 * `X` )
WHERE 			`X` > @d;

UPDATE 			`temp_broker_mark`
SET 			`k` = 1
WHERE 			`X` <= @d;

-- det Cch
SET @Cch = (
	SELECT SUM( `Y` * `k` )
	FROM `temp_broker_mark`
);

-- det Cz
SET @Cz = (
	SELECT SUM( `k` )
	FROM `temp_broker_mark`
);

-- det C
SET @C = @Cch / @Cz;

-- collect d1
INSERT INTO 	`temp_broker_stats_d1` ( `idBroker`, `place`, `oldPlace`, `changePlaceDT` )
SELECT			`{{broker_stats}}`.`idBroker`, 
				`{{broker_stats}}`.`place`, 
				`{{broker_stats}}`.`oldPlace`, 
				`{{broker_stats}}`.`changePlaceDT`
FROM			`{{broker_stats}}` 
INNER JOIN		`{{broker}}` ON `{{broker}}`.`id` = `{{broker_stats}}`.`idBroker`
WHERE			`{{broker}}`.`showInRating`;

-- det countMarks
UPDATE			`temp_broker_stats_d1` 
SET				`countMarks` = (
					SELECT		COUNT( * )
					FROM		`temp_broker_mark`
					WHERE		`temp_broker_mark`.`idBroker` = `temp_broker_stats_d1`.`idBroker`
				);

-- det Rch in d1
UPDATE			`temp_broker_stats_d1` 
SET				`Rch` = (
					SELECT		SUM( `Y` * `k` )
					FROM		`temp_broker_mark`
					WHERE		`temp_broker_mark`.`idBroker` = `temp_broker_stats_d1`.`idBroker`
				);
				
-- det Rz in d1
UPDATE			`temp_broker_stats_d1` 
SET				`Rz` = (
					SELECT		SUM( `k` )
					FROM		`temp_broker_mark`
					WHERE		`temp_broker_mark`.`idBroker` = `temp_broker_stats_d1`.`idBroker`
				);
				
-- det R in d1
UPDATE			`temp_broker_stats_d1` 
SET				`R` = `Rch` / `Rz`;

-- det v in d1
UPDATE			`temp_broker_stats_d1` 
SET				`v` = (
					SELECT		SUM( `k` )
					FROM		`temp_broker_mark`
					WHERE		`temp_broker_mark`.`idBroker` = `temp_broker_stats_d1`.`idBroker`
				);
				
-- det S in d1
UPDATE			`temp_broker_stats_d1` 
SET				`S` = ( `R` * `v` + @C * @m ) / ( `v` + @m );

-- det W in d1
UPDATE			`temp_broker_stats_d1` 
SET				`W` = ( `S` - 1 ) * 25;

-- collect d2
INSERT INTO 	`temp_broker_stats_d2` ( `idBroker`, `place`, `oldPlace`, `changePlaceDT`, `trust`, `countMarks` )
SELECT			`temp_broker_stats_d1`.`idBroker`, 
				`temp_broker_stats_d1`.`place`, 
				`temp_broker_stats_d1`.`oldPlace`, 
				`temp_broker_stats_d1`.`changePlaceDT`,
				LEAST( `temp_broker_stats_d1`.`S`, 100 ),
				`temp_broker_stats_d1`.`countMarks`
FROM			`temp_broker_stats_d1`
ORDER BY		`S` IS NULL, `S` DESC, `idBroker`;

-- update DT in d2
UPDATE			`temp_broker_stats_d2`
SET				`oldPlace` = IFNULL( `place`, `newPlace` ),
				`changePlaceDT` = NOW()
WHERE			`changePlaceDT` IS NULL OR `place` != `newPlace`;

-- return stats
REPLACE INTO 	`{{broker_stats}}` ( `idBroker`, `place`, `oldPlace`, `changePlaceDT`, `trust`, `countMarks` )
SELECT			`idBroker`,
				`newPlace`,
				`oldPlace`,
				`changePlaceDT`, 
				`trust`,
				`countMarks`
FROM			`temp_broker_stats_d2`;

-- add stats for NOT showInRating
REPLACE INTO 	`{{broker_stats}}` ( `idBroker` )
SELECT			`{{broker}}`.`id`
FROM			`{{broker}}`
WHERE			NOT `{{broker}}`.`showInRating`;

-- update DT in stats
UPDATE			`{{broker_stats}}`
SET				`updatedDT` = NOW();

-- drop temp struct
DROP TEMPORARY TABLE `temp_broker_mark`;
DROP TEMPORARY TABLE `temp_broker_stats_d1`;
DROP TEMPORARY TABLE `temp_broker_stats_d2`;