INSERT IGNORE INTO `{{contest_member_monitoring_stats}}` (`idMember`)
SELECT `id` FROM `{{contest_member}}` WHERE `idContest` = 0;


UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `closedDealsToday` = (
	SELECT COUNT(*)
	FROM	`mt5_account_history_deals`
	WHERE	`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `member`.`accountNumber`
		AND `mt5_account_history_deals`.`AccountServerId` = `member`.`idServer`
		AND `mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
		AND DATE_FORMAT(FROM_UNIXTIME(`mt5_account_history_deals`.`DEAL_TIME`), '%Y-%m-%d') = CURDATE()
)
WHERE `platform`.`name` = 'MetaTrader 5';



UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `closedDealsToday` = (
	SELECT COUNT(*)
	FROM	`mt4_account_history`
	WHERE	`mt4_account_history`.`AccountNumber` = `member`.`accountNumber`
		AND `mt4_account_history`.`AccountServerId` = `member`.`idServer`
		AND `mt4_account_history`.`OrderType` IN ( 0, 1 )
		AND DATE_FORMAT(FROM_UNIXTIME(`mt4_account_history`.`OrderCloseTime`), '%Y-%m-%d') = CURDATE()
)
WHERE `platform`.`name` = 'MetaTrader 4';


UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `balanceToday` = (
	SELECT SUM(`mt5_account_history_deals`.`DEAL_PROFIT`)
	FROM	`mt5_account_history_deals`
	WHERE	`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `member`.`accountNumber`
		AND `mt5_account_history_deals`.`AccountServerId` = `member`.`idServer`
		AND `mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
		AND DATE_FORMAT(FROM_UNIXTIME(`mt5_account_history_deals`.`DEAL_TIME`), '%Y-%m-%d') = CURDATE()
)
WHERE `platform`.`name` = 'MetaTrader 5';


UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `balanceToday` = (
	SELECT SUM(`mt4_account_history`.`OrderProfit`)
	FROM	`mt4_account_history`
	WHERE	`mt4_account_history`.`AccountNumber` = `member`.`accountNumber`
		AND `mt4_account_history`.`AccountServerId` = `member`.`idServer`
		AND `mt4_account_history`.`OrderType` IN ( 0, 1 )
		AND DATE_FORMAT(FROM_UNIXTIME(`mt4_account_history`.`OrderCloseTime`), '%Y-%m-%d') = CURDATE()
)
WHERE `platform`.`name` = 'MetaTrader 4';






UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `dayBalance` = (
	SELECT SUM(`mt5_account_history_deals`.`DEAL_PROFIT`)
	FROM	`mt5_account_history_deals`
	WHERE	`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `member`.`accountNumber`
		AND `mt5_account_history_deals`.`AccountServerId` = `member`.`idServer`
		AND `mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
		AND `mt5_account_history_deals`.`DEAL_TIME` > UNIX_TIMESTAMP()-60*60*24*1
)
WHERE `platform`.`name` = 'MetaTrader 5';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `dayBalance` = (
	SELECT SUM(`mt4_account_history`.`OrderProfit`)
	FROM	`mt4_account_history`
	WHERE	`mt4_account_history`.`AccountNumber` = `member`.`accountNumber`
		AND `mt4_account_history`.`AccountServerId` = `member`.`idServer`
		AND `mt4_account_history`.`OrderType` IN ( 0, 1 )
		AND `mt4_account_history`.`OrderCloseTime` > UNIX_TIMESTAMP()-60*60*24*1
)
WHERE `platform`.`name` = 'MetaTrader 4';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `weekBalance` = (
	SELECT SUM(`mt5_account_history_deals`.`DEAL_PROFIT`)
	FROM	`mt5_account_history_deals`
	WHERE	`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `member`.`accountNumber`
		AND `mt5_account_history_deals`.`AccountServerId` = `member`.`idServer`
		AND `mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
		AND `mt5_account_history_deals`.`DEAL_TIME` > UNIX_TIMESTAMP()-60*60*24*7
)
WHERE `platform`.`name` = 'MetaTrader 5';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `weekBalance` = (
	SELECT SUM(`mt4_account_history`.`OrderProfit`)
	FROM	`mt4_account_history`
	WHERE	`mt4_account_history`.`AccountNumber` = `member`.`accountNumber`
		AND `mt4_account_history`.`AccountServerId` = `member`.`idServer`
		AND `mt4_account_history`.`OrderType` IN ( 0, 1 )
		AND `mt4_account_history`.`OrderCloseTime` > UNIX_TIMESTAMP()-60*60*24*7
)
WHERE `platform`.`name` = 'MetaTrader 4';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `monthBalance` = (
	SELECT SUM(`mt5_account_history_deals`.`DEAL_PROFIT`)
	FROM	`mt5_account_history_deals`
	WHERE	`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `member`.`accountNumber`
		AND `mt5_account_history_deals`.`AccountServerId` = `member`.`idServer`
		AND `mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
		AND `mt5_account_history_deals`.`DEAL_TIME` > UNIX_TIMESTAMP()-60*60*24*30
)
WHERE `platform`.`name` = 'MetaTrader 5';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `monthBalance` = (
	SELECT SUM(`mt4_account_history`.`OrderProfit`)
	FROM	`mt4_account_history`
	WHERE	`mt4_account_history`.`AccountNumber` = `member`.`accountNumber`
		AND `mt4_account_history`.`AccountServerId` = `member`.`idServer`
		AND `mt4_account_history`.`OrderType` IN ( 0, 1 )
		AND `mt4_account_history`.`OrderCloseTime` > UNIX_TIMESTAMP()-60*60*24*30
)
WHERE `platform`.`name` = 'MetaTrader 4';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `threeMonthsBalance` = (
	SELECT SUM(`mt5_account_history_deals`.`DEAL_PROFIT`)
	FROM	`mt5_account_history_deals`
	WHERE	`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `member`.`accountNumber`
		AND `mt5_account_history_deals`.`AccountServerId` = `member`.`idServer`
		AND `mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
		AND `mt5_account_history_deals`.`DEAL_TIME` > UNIX_TIMESTAMP()-60*60*24*91
)
WHERE `platform`.`name` = 'MetaTrader 5';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `threeMonthsBalance` = (
	SELECT SUM(`mt4_account_history`.`OrderProfit`)
	FROM	`mt4_account_history`
	WHERE	`mt4_account_history`.`AccountNumber` = `member`.`accountNumber`
		AND `mt4_account_history`.`AccountServerId` = `member`.`idServer`
		AND `mt4_account_history`.`OrderType` IN ( 0, 1 )
		AND `mt4_account_history`.`OrderCloseTime` > UNIX_TIMESTAMP()-60*60*24*91
)
WHERE `platform`.`name` = 'MetaTrader 4';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `sixMonthsBalance` = (
	SELECT SUM(`mt5_account_history_deals`.`DEAL_PROFIT`)
	FROM	`mt5_account_history_deals`
	WHERE	`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `member`.`accountNumber`
		AND `mt5_account_history_deals`.`AccountServerId` = `member`.`idServer`
		AND `mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
		AND `mt5_account_history_deals`.`DEAL_TIME` > UNIX_TIMESTAMP()-60*60*24*182
)
WHERE `platform`.`name` = 'MetaTrader 5';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `sixMonthsBalance` = (
	SELECT SUM(`mt4_account_history`.`OrderProfit`)
	FROM	`mt4_account_history`
	WHERE	`mt4_account_history`.`AccountNumber` = `member`.`accountNumber`
		AND `mt4_account_history`.`AccountServerId` = `member`.`idServer`
		AND `mt4_account_history`.`OrderType` IN ( 0, 1 )
		AND `mt4_account_history`.`OrderCloseTime` > UNIX_TIMESTAMP()-60*60*24*182
)
WHERE `platform`.`name` = 'MetaTrader 4';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `nineMonthsBalance` = (
	SELECT SUM(`mt5_account_history_deals`.`DEAL_PROFIT`)
	FROM	`mt5_account_history_deals`
	WHERE	`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `member`.`accountNumber`
		AND `mt5_account_history_deals`.`AccountServerId` = `member`.`idServer`
		AND `mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
		AND `mt5_account_history_deals`.`DEAL_TIME` > UNIX_TIMESTAMP()-60*60*24*273
)
WHERE `platform`.`name` = 'MetaTrader 5';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `nineMonthsBalance` = (
	SELECT SUM(`mt4_account_history`.`OrderProfit`)
	FROM	`mt4_account_history`
	WHERE	`mt4_account_history`.`AccountNumber` = `member`.`accountNumber`
		AND `mt4_account_history`.`AccountServerId` = `member`.`idServer`
		AND `mt4_account_history`.`OrderType` IN ( 0, 1 )
		AND `mt4_account_history`.`OrderCloseTime` > UNIX_TIMESTAMP()-60*60*24*273
)
WHERE `platform`.`name` = 'MetaTrader 4';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `allDayBalance` = (
	SELECT SUM(`mt5_account_history_deals`.`DEAL_PROFIT`)
	FROM	`mt5_account_history_deals`
	WHERE	`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `member`.`accountNumber`
		AND `mt5_account_history_deals`.`AccountServerId` = `member`.`idServer`
		AND `mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
)
WHERE `platform`.`name` = 'MetaTrader 5';

UPDATE `{{contest_member_monitoring_stats}}` `stats`
JOIN `{{contest_member}}` `member` ON `member`.`id` = `stats`.`idMember`
JOIN `{{server}}` `server` ON `server`.`id` = `member`.`idServer`
JOIN `{{trade_platform}}` `platform` ON `platform`.`id` = `server`.`idTradePlatform`
SET `allDayBalance` = (
	SELECT SUM(`mt4_account_history`.`OrderProfit`)
	FROM	`mt4_account_history`
	WHERE	`mt4_account_history`.`AccountNumber` = `member`.`accountNumber`
		AND `mt4_account_history`.`AccountServerId` = `member`.`idServer`
		AND `mt4_account_history`.`OrderType` IN ( 0, 1 )
)
WHERE `platform`.`name` = 'MetaTrader 4';










