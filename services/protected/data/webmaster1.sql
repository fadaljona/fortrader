ALTER TABLE `ft_journal` ADD COLUMN `clear_date` DATETIME NULL DEFAULT NULL;
ALTER TABLE `ft_journal_webmasters` ADD COLUMN `is_blocked` TINYINT(1) NOT NULL DEFAULT 0;
