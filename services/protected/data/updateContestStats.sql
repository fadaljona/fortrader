-- create temp struct
DROP TEMPORARY TABLE IF EXISTS `temp_contest_stats`;
DROP TEMPORARY TABLE IF EXISTS `temp_mt5data_last`;

CREATE TEMPORARY TABLE `temp_contest_stats` (
	`idContest` int(15) unsigned NOT NULL UNIQUE,
	`leaderMember` int(15) NOT NULL,
	`totalProfit` double NOT NULL,
	`totalLoss` double NOT NULL,
	`committedDeals` int(15) NOT NULL,
	`bestDeal` double NOT NULL,
	`bestProfitRiskMember` int(15) NOT NULL,
	`lastDealOrderOpenPrice` double DEFAULT NULL,
	`lastDealOrderClosePrice` double DEFAULT NULL,
	`lastDealOrderType` int(11) NOT NULL,
	`lastDealOrderSymbol` varchar(255) DEFAULT NULL,
	`activeTraders` int(15) NOT NULL,
	`profitTraders` int(15) NOT NULL,
	`lossTraders` int(15) NOT NULL,
	`totalTraders` int(15) NOT NULL,
	PRIMARY KEY (idContest)
)
COLLATE='utf8_general_ci'
ENGINE=Memory;

CREATE TEMPORARY TABLE `temp_mt5data_last` (
	`idContest` int(15) unsigned NOT NULL,
	`idMember` int(15) NOT NULL,
	`ACCOUNT_NUMBER` int(15) NOT NULL,
	`ACCOUNT_EQUITY` double NOT NULL,
	`ACCOUNT_BALANCE` double NOT NULL,
	`timestamp` timestamp NOT NULL,
	`ACCOUNT_EQUITY_DROW` int(15) NOT NULL
)
COLLATE='utf8_general_ci'
ENGINE=Memory;


INSERT INTO 	`temp_mt5data_last` ( `idContest`, `idMember`, `ACCOUNT_NUMBER`, `ACCOUNT_EQUITY`, `ACCOUNT_BALANCE`, `timestamp`, `ACCOUNT_EQUITY_DROW` )
	SELECT `member`.`idContest`, `member`.`id`, `mt5data`.`ACCOUNT_NUMBER`, `mt5data`.`ACCOUNT_EQUITY`,`mt5data`.`ACCOUNT_BALANCE`, `mt5data`.`timestamp`, `mt5data`.`ACCOUNT_EQUITY_DROW` 
	FROM `mt5_account_data` `mt5data`
	LEFT JOIN `{{contest_member}}` `member` 
		ON `mt5data`.`ACCOUNT_NUMBER` = `member`.`accountNumber` AND `mt5data`.`AccountServerId` = `member`.`idServer`
	LEFT JOIN (SELECT `ACCOUNT_NUMBER`, `AccountServerId`, MAX( `timestamp` ) `maxTime` from `mt5_account_data`  GROUP BY `ACCOUNT_NUMBER`) `mt5dataMaxDate` 
		ON `mt5dataMaxDate`.`ACCOUNT_NUMBER` = `member`.`accountNumber` AND `mt5dataMaxDate`.`AccountServerId` = `member`.`idServer` AND `mt5data`.`timestamp` = `mt5dataMaxDate`.`maxTime`
	WHERE `member`.`idContest` = :idContest AND ( `member`.`status` = 'participates' OR `member`.`status` = 'completed' ) AND `mt5dataMaxDate`.`ACCOUNT_NUMBER` IS NOT NULL;


INSERT INTO 	`temp_contest_stats` ( `idContest`, `leaderMember` )
	SELECT `idContest`, `idMember` FROM `temp_mt5data_last`
	ORDER BY `ACCOUNT_BALANCE` DESC
	LIMIT 1;

update `temp_contest_stats` set `totalProfit` = (
	SELECT sum( `mt4his`.`OrderSwap` + `mt4his`.`OrderCommission` + `mt4his`.`OrderProfit` ) 
	FROM `mt4_account_history` AS `mt4his`
	LEFT JOIN `{{contest_member}}` ON `mt4his`.`AccountNumber` = `{{contest_member}}`.`accountNumber` AND `mt4his`.`AccountServerId` = `{{contest_member}}`.`idServer`
	WHERE `{{contest_member}}`.`idContest` = :idContest AND `mt4his`.`OrderProfit` > 0  AND ( `mt4his`.`OrderType` = 0 or `mt4his`.`OrderType` = 1 ) AND ( `{{contest_member}}`.`status` = 'participates' OR `{{contest_member}}`.`status` = 'completed' )
);

update `temp_contest_stats` set `totalLoss` = (
	SELECT sum( `mt4his`.`OrderSwap` + `mt4his`.`OrderCommission` + `mt4his`.`OrderProfit` ) 
	FROM `mt4_account_history` AS `mt4his`
	LEFT JOIN `{{contest_member}}` ON `mt4his`.`AccountNumber` = `{{contest_member}}`.`accountNumber` AND `mt4his`.`AccountServerId` = `{{contest_member}}`.`idServer`
	WHERE `{{contest_member}}`.`idContest` = :idContest AND `mt4his`.`OrderProfit` < 0 AND ( `mt4his`.`OrderType` = 0 or `mt4his`.`OrderType` = 1 ) AND ( `{{contest_member}}`.`status` = 'participates' OR `{{contest_member}}`.`status` = 'completed' )
);

update `temp_contest_stats` set `committedDeals` = (
	SELECT count( `mt4his`.`id` ) FROM `mt4_account_history` AS `mt4his`
	LEFT JOIN `{{contest_member}}` ON `mt4his`.`AccountNumber` = `{{contest_member}}`.`accountNumber` AND `mt4his`.`AccountServerId` = `{{contest_member}}`.`idServer`
	WHERE `{{contest_member}}`.`idContest` = :idContest AND ( `mt4his`.`OrderType` = 0 or `mt4his`.`OrderType` = 1 ) AND ( `{{contest_member}}`.`status` = 'participates' OR `{{contest_member}}`.`status` = 'completed' )
);

update `temp_contest_stats` set `bestDeal` = (
	SELECT max( abs( `mt4his`.`OrderOpenPrice` - `mt4his`.`OrderClosePrice` )) FROM `mt4_account_history` AS `mt4his`
	LEFT JOIN `{{contest_member}}` ON `mt4his`.`AccountNumber` = `{{contest_member}}`.`accountNumber` AND `mt4his`.`AccountServerId` = `{{contest_member}}`.`idServer`
	WHERE `{{contest_member}}`.`idContest` = :idContest AND ( `mt4his`.`OrderType` = 1 or `mt4his`.`OrderType` = 0 ) AND ( `{{contest_member}}`.`status` = 'participates' OR `{{contest_member}}`.`status` = 'completed' )
);


update `temp_contest_stats` set `bestProfitRiskMember` = (
	SELECT `member`.`id`
	FROM `mt5_account_data` `mt5data`
	LEFT JOIN `{{contest_member}}` `member` 
		ON `mt5data`.`ACCOUNT_NUMBER` = `member`.`accountNumber` AND `mt5data`.`AccountServerId` = `member`.`idServer`
	LEFT JOIN (SELECT `ACCOUNT_NUMBER`, `AccountServerId`, MAX( `timestamp` ) `maxTime` from `mt5_account_data`  GROUP BY `ACCOUNT_NUMBER`) `mt5dataMaxDate` 
		ON `mt5dataMaxDate`.`ACCOUNT_NUMBER` = `member`.`accountNumber` AND `mt5dataMaxDate`.`AccountServerId` = `member`.`idServer` AND `mt5data`.`timestamp` = `mt5dataMaxDate`.`maxTime`
	WHERE `member`.`idContest` = :idContest AND  `mt5data`.`ACCOUNT_EQUITY_DROW` <> 0 AND `mt5data`.`ACCOUNT_EQUITY` > 0 AND ( `member`.`status` = 'participates' OR `member`.`status` = 'completed' ) AND `mt5data`.`ACCOUNT_EQUITY` > ( SELECT `startDeposit` FROM `{{contest}}` WHERE `id` = :idContest ) AND `mt5dataMaxDate`.`ACCOUNT_NUMBER` IS NOT NULL
	ORDER BY ( `mt5data`.`ACCOUNT_EQUITY` / `mt5data`.`ACCOUNT_EQUITY_DROW` ) DESC
	LIMIT 1
);

update `temp_contest_stats` set `lastDealOrderOpenPrice` = (
	SELECT `mt4his`.`OrderOpenPrice` FROM `mt4_account_history` AS `mt4his`
	LEFT JOIN `{{contest_member}}` ON `mt4his`.`AccountNumber` = `{{contest_member}}`.`accountNumber` AND `mt4his`.`AccountServerId` = `{{contest_member}}`.`idServer`
	WHERE `{{contest_member}}`.`idContest` = :idContest AND ( `mt4his`.`OrderType` = 0 or `mt4his`.`OrderType` = 1 ) AND ( `{{contest_member}}`.`status` = 'participates' OR `{{contest_member}}`.`status` = 'completed' )
	ORDER BY `mt4his`.`RowDateAdd` DESC
	LIMIT 1
);

update `temp_contest_stats` set `lastDealOrderClosePrice` = (
	SELECT `mt4his`.`OrderClosePrice` FROM `mt4_account_history` AS `mt4his`
	LEFT JOIN `{{contest_member}}` ON `mt4his`.`AccountNumber` = `{{contest_member}}`.`accountNumber` AND `mt4his`.`AccountServerId` = `{{contest_member}}`.`idServer`
	WHERE `{{contest_member}}`.`idContest` = :idContest AND ( `mt4his`.`OrderType` = 0 or `mt4his`.`OrderType` = 1 ) AND ( `{{contest_member}}`.`status` = 'participates' OR `{{contest_member}}`.`status` = 'completed' )
	ORDER BY `mt4his`.`RowDateAdd` DESC
	LIMIT 1
);

update `temp_contest_stats` set `lastDealOrderType` = (
	SELECT `mt4his`.`OrderType` FROM `mt4_account_history` AS `mt4his`
	LEFT JOIN `{{contest_member}}` ON `mt4his`.`AccountNumber` = `{{contest_member}}`.`accountNumber` AND `mt4his`.`AccountServerId` = `{{contest_member}}`.`idServer`
	WHERE `{{contest_member}}`.`idContest` = :idContest AND ( `mt4his`.`OrderType` = 0 or `mt4his`.`OrderType` = 1 ) AND ( `{{contest_member}}`.`status` = 'participates' OR `{{contest_member}}`.`status` = 'completed' )
	ORDER BY `mt4his`.`RowDateAdd` DESC
	LIMIT 1
);

update `temp_contest_stats` set `lastDealOrderSymbol` = (
	SELECT `mt4his`.`OrderSymbol` FROM `mt4_account_history` AS `mt4his`
	LEFT JOIN `{{contest_member}}` ON `mt4his`.`AccountNumber` = `{{contest_member}}`.`accountNumber` AND `mt4his`.`AccountServerId` = `{{contest_member}}`.`idServer`
	WHERE `{{contest_member}}`.`idContest` = :idContest AND ( `mt4his`.`OrderType` = 0 or `mt4his`.`OrderType` = 1 ) AND ( `{{contest_member}}`.`status` = 'participates' OR `{{contest_member}}`.`status` = 'completed' )
	ORDER BY `mt4his`.`RowDateAdd` DESC
	LIMIT 1
);

update `temp_contest_stats` set `activeTraders` = (
	SELECT count( `activeTradersCount`.`countAccountNumber` ) FROM 
		( 
			SELECT count( `mt4his`.`AccountNumber` ) `countAccountNumber`, `mt4his`.`AccountNumber` FROM `mt4_account_history` AS `mt4his`
			LEFT JOIN `{{contest_member}}` ON `mt4his`.`AccountNumber` = `{{contest_member}}`.`accountNumber` AND `mt4his`.`AccountServerId` = `{{contest_member}}`.`idServer`
			WHERE `{{contest_member}}`.`idContest` = :idContest AND ( `{{contest_member}}`.`status` = 'participates' OR `{{contest_member}}`.`status` = 'completed' )
			GROUP BY `mt4his`.`AccountNumber`
			HAVING count( `mt4his`.`AccountNumber` ) > 1 
		) AS `activeTradersCount`
);

update `temp_contest_stats` set `profitTraders` = (
	SELECT count( `ACCOUNT_NUMBER` ) FROM `temp_mt5data_last`
	WHERE `ACCOUNT_EQUITY` > ( SELECT `startDeposit` FROM `{{contest}}` WHERE `id` = :idContest )
);

update `temp_contest_stats` set `lossTraders` = (
	SELECT count( `ACCOUNT_NUMBER` ) FROM `temp_mt5data_last`
	WHERE `ACCOUNT_EQUITY` < ( SELECT `startDeposit` FROM `{{contest}}` WHERE `id` = :idContest )
);

update `temp_contest_stats` set `totalTraders` = (
	SELECT count( `{{contest_member}}`.`id` ) FROM `{{contest_member}}` WHERE `{{contest_member}}`.`idContest` = :idContest
);



REPLACE INTO 	`{{contest_stats}}` ( `idContest`, `leaderMember`, `totalProfit`, `totalLoss`, `committedDeals`, `bestDeal`, `bestProfitRiskMember`, `lastDealOrderOpenPrice`, `lastDealOrderClosePrice`, `lastDealOrderType`, `lastDealOrderSymbol`, `activeTraders`, `profitTraders`, `lossTraders`, `totalTraders` )
SELECT			*
FROM			`temp_contest_stats`;



DROP TEMPORARY TABLE IF EXISTS `temp_contest_stats`;
DROP TEMPORARY TABLE IF EXISTS `temp_mt5data_last`;













