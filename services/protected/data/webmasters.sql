-- -----------------------------------------------------
-- Table `fortrader`.`ft_journal_webmasters`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ft_journal_webmasters` (
  `idWebmaster` INT NOT NULL AUTO_INCREMENT ,
  `url` VARCHAR(255) NOT NULL ,
  `createdDT` DATETIME NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`idWebmaster`) ,
  UNIQUE INDEX `index3` (`url` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8;



-- -----------------------------------------------------
-- Table `fortrader`.`ft_journal_webmaster_downloads`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ft_journal_webmaster_downloads` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `idWebmaster` INT NOT NULL ,
  `ip` VARCHAR(20) NOT NULL ,
  `createdDT` DATETIME NOT NULL ,
  `idJournal` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `index2` (`ip` ASC, `idJournal` ASC, `idWebmaster` ASC) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8;