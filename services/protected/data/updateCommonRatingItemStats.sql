DROP TEMPORARY TABLE IF EXISTS `temp_common_rating_item_stats`;

CREATE TEMPORARY TABLE `temp_common_rating_item_stats` (
	`idModel` bigint(20) UNSIGNED NOT NULL,
    `modelName` varchar(255) NOT NULL,
	`average` DOUBLE UNSIGNED NULL,
	`votesCount` bigint(20) UNSIGNED NOT NULL
)
ENGINE=Memory;

-- collect stats
INSERT IGNORE INTO 	`temp_common_rating_item_stats` ( `idModel`, `modelName` )
SELECT				`{{common_rating_item_mark}}`.`idModel`, `{{common_rating_item_mark}}`.`modelName`
FROM				`{{common_rating_item_mark}}`
GROUP BY `{{common_rating_item_mark}}`.`idModel`, `{{common_rating_item_mark}}`.`modelName`;

-- det average
UPDATE 			`temp_common_rating_item_stats`
SET				`average` = (
					SELECT 	AVG( `value` )
					FROM	`{{common_rating_item_mark}}`
					WHERE	`{{common_rating_item_mark}}`.`idModel` = `temp_common_rating_item_stats`.`idModel` AND `{{common_rating_item_mark}}`.`modelName` = `temp_common_rating_item_stats`.`modelName`
				);
				
-- det votesCount

UPDATE 			`temp_common_rating_item_stats`
SET				`votesCount` = (
					SELECT 	COUNT( `value` )
					FROM	`{{common_rating_item_mark}}`
					WHERE	`{{common_rating_item_mark}}`.`idModel` = `temp_common_rating_item_stats`.`idModel` AND `{{common_rating_item_mark}}`.`modelName` = `temp_common_rating_item_stats`.`modelName`
				);
	
-- return stats
REPLACE INTO 	`{{common_rating_item_stats}}` ( `idModel`, `modelName`, `average`, `updatedDT`, `votesCount` )
SELECT			`idModel`,
                `modelName`,
				ROUND( `average`, 1 ),
				NOW(),
				`votesCount`
FROM			`temp_common_rating_item_stats`;

-- drop temp struct
DROP TEMPORARY TABLE `temp_common_rating_item_stats`;