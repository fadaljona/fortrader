DROP TEMPORARY TABLE IF EXISTS `temp_category_rating_item_stats`;

CREATE TEMPORARY TABLE `temp_category_rating_item_stats` (
	`idPost` INT(10) UNSIGNED NOT NULL,
	`average` DOUBLE UNSIGNED NULL,
	`votesCount` INT(10) UNSIGNED NOT NULL
)
ENGINE=Memory;

-- collect stats
INSERT IGNORE INTO 	`temp_category_rating_item_stats` ( `idPost` )
SELECT				`{{category_rating_item_mark}}`.`idPost`
FROM				`{{category_rating_item_mark}}`
GROUP BY `{{category_rating_item_mark}}`.`idPost`;

-- det average
UPDATE 			`temp_category_rating_item_stats`
SET				`average` = (
					SELECT 	AVG( `value` )
					FROM	`{{category_rating_item_mark}}`
					WHERE	`{{category_rating_item_mark}}`.`idPost` = `temp_category_rating_item_stats`.`idPost`
				);
				
-- det votesCount

UPDATE 			`temp_category_rating_item_stats`
SET				`votesCount` = (
					SELECT 	COUNT( `value` )
					FROM	`{{category_rating_item_mark}}`
					WHERE	`{{category_rating_item_mark}}`.`idPost` = `temp_category_rating_item_stats`.`idPost`
				);
	
-- return stats
REPLACE INTO 	`{{category_rating_item_stats}}` ( `idPost`, `average`, `updatedDT`, `votesCount` )
SELECT			`idPost`,
				ROUND( `average`, 1 ),
				NOW(),
				`votesCount`
FROM			`temp_category_rating_item_stats`;

-- drop temp struct
DROP TEMPORARY TABLE `temp_category_rating_item_stats`;