-- create temp struct
DROP TEMPORARY TABLE IF EXISTS `temp_trade_account_stats_d1`;

CREATE TEMPORARY TABLE `temp_trade_account_stats_d1` (
	`idAccount` INT(10) UNSIGNED NOT NULL,
	`accountNumber` INT(10) UNSIGNED NOT NULL,
	`idServer` INT(10) UNSIGNED NOT NULL,
	`nameServer` VARCHAR( 255 ) NOT NULL,
	`tradePlatform` VARCHAR(255) NOT NULL,
	`countOpenDeals` INT(10) UNSIGNED NOT NULL DEFAULT 0,
	`idLastMT5AccountData` INT(10) UNSIGNED NULL,
	`balance` DOUBLE NULL DEFAULT NULL,
	`deposit` DOUBLE NULL DEFAULT NULL,
	`withdrawals` DOUBLE NULL DEFAULT NULL,
	`equity` DOUBLE NULL DEFAULT NULL,
	`profit` DOUBLE NULL DEFAULT NULL,
	`minEquity` DOUBLE NULL DEFAULT NULL,
	`margin` DOUBLE NULL DEFAULT NULL,
	`freeMargin` DOUBLE NULL DEFAULT NULL,
	`countTrades` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`gain` DOUBLE NULL DEFAULT NULL,
	`drowAbs` DOUBLE NULL DEFAULT NULL,
	`drowMax` DOUBLE NULL DEFAULT NULL,
	`trueDT` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`idAccount`)
)
COLLATE='utf8_general_ci'
ENGINE=Memory;

-- collect d1
INSERT INTO 	`temp_trade_account_stats_d1` ( `idAccount`, `accountNumber`, `idServer`, `nameServer`, `tradePlatform` )
SELECT			`{{trade_account}}`.`id`,
				`{{trade_account}}`.`accountNumber`,
				`{{trade_account}}`.`idServer`,
				`{{server}}`.`internalName`,
				`{{trade_platform}}`.`name`
FROM			`{{trade_account}}`
INNER JOIN		`{{server}}` ON `{{server}}`.`id` = `{{trade_account}}`.`idServer`
INNER JOIN		`{{trade_platform}}` ON `{{trade_platform}}`.`id` = `{{server}}`.`idTradePlatform`;

-- det idLastMT5AccountData
UPDATE			`temp_trade_account_stats_d1`
SET				`idLastMT5AccountData` = (
					SELECT 			MAX( `id` )
					FROM			`mt5_account_data`
					WHERE			`mt5_account_data`.`ACCOUNT_NUMBER` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt5_account_data`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
				);

-- det countOpenDeals
UPDATE			`temp_trade_account_stats_d1`
SET				`countOpenDeals` = (
					SELECT 			COUNT( * )
					FROM			`mt5_account_trades`
					WHERE			`mt5_account_trades`.`ACCOUNT_NUMBER` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt5_account_trades`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
				);				
				
-- det balance
UPDATE			`temp_trade_account_stats_d1`
SET				`balance` = (
					SELECT			SUM( IFNULL( `OrderProfit`, 0 ) + IFNULL( `OrderSwap`, 0 ) + IFNULL( `OrderCommission`, 0 ) )
					FROM			`mt4_account_history`
					WHERE			`mt4_account_history`.`AccountNumber` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt4_account_history`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
				)
WHERE			`temp_trade_account_stats_d1`.`idLastMT5AccountData` IS NOT NULL;

-- det deposit
UPDATE			`temp_trade_account_stats_d1`
SET				`deposit` = (
					SELECT			SUM( `DEAL_PROFIT` )
					FROM 			`mt5_account_history_deals`
					WHERE 			`mt5_account_history_deals`.`DEAL_TYPE` = 'balance' 
						AND 		`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt5_account_history_deals`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
				)
WHERE			`tradePlatform` = 'MetaTrader 5';

UPDATE			`temp_trade_account_stats_d1`
SET				`deposit` = (
					SELECT			SUM( `OrderProfit` )
					FROM 			`mt4_account_history`
					WHERE 			`mt4_account_history`.`OrderType` = 6 
						AND 		`mt4_account_history`.`AccountNumber` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt4_account_history`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
				)
WHERE			`tradePlatform` = 'MetaTrader 4';
				
-- det withdrawals
UPDATE			`temp_trade_account_stats_d1`
SET				`withdrawals` = (
					SELECT				SUM( `DEAL_PROFIT` )
					FROM 				`mt5_account_history_deals`
					WHERE 				`mt5_account_history_deals`.`DEAL_TYPE` = 'balance' 
						AND 			`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `temp_trade_account_stats_d1`.`accountNumber`
						AND 			`mt5_account_history_deals`.`DEAL_PROFIT` < 0
				)
WHERE			`tradePlatform` = 'MetaTrader 5';
				
UPDATE			`temp_trade_account_stats_d1`
SET				`withdrawals` = (
					SELECT				SUM( `OrderProfit` )
					FROM 				`mt4_account_history`
					WHERE 				`mt4_account_history`.`OrderType` = 6 
						AND 			`mt4_account_history`.`AccountNumber` = `temp_trade_account_stats_d1`.`accountNumber`
						AND				`mt4_account_history`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
						AND 			`mt4_account_history`.`OrderProfit` < 0
				)
WHERE			`tradePlatform` = 'MetaTrader 4';

-- det profit
UPDATE			`temp_trade_account_stats_d1`
SET				`profit` = `balance` - `deposit`;	
				
-- det equity
UPDATE			`temp_trade_account_stats_d1`
SET				`equity` = `balance`
WHERE			NOT `countOpenDeals`;

UPDATE			`temp_trade_account_stats_d1`
SET				`equity` = (
					SELECT			`ACCOUNT_EQUITY`
					FROM			`mt5_account_data`
					WHERE			`mt5_account_data`.`id` = `temp_trade_account_stats_d1`.`idLastMT5AccountData`
					LIMIT			1
				)
WHERE			`countOpenDeals`;

-- det margin
UPDATE			`temp_trade_account_stats_d1`
SET				`margin` = 0
WHERE			NOT `countOpenDeals`;

UPDATE			`temp_trade_account_stats_d1`
SET				`margin` = (
					SELECT			`ACCOUNT_MARGIN`
					FROM			`mt5_account_data`
					WHERE			`mt5_account_data`.`id` = `temp_trade_account_stats_d1`.`idLastMT5AccountData`
					LIMIT			1
				)
WHERE			`countOpenDeals`;				
				
-- det freeMargin
UPDATE			`temp_trade_account_stats_d1`
SET				`freeMargin` = 0
WHERE			NOT `countOpenDeals`;

UPDATE			`temp_trade_account_stats_d1`
SET				`freeMargin` = (
					SELECT			`ACCOUNT_FREEMARGIN`
					FROM			`mt5_account_data`
					WHERE			`mt5_account_data`.`id` = `temp_trade_account_stats_d1`.`idLastMT5AccountData`
					LIMIT			1
				)
WHERE			`countOpenDeals`;
				
-- det minEquity
UPDATE			`temp_trade_account_stats_d1`
SET				`minEquity` = (
					SELECT			MIN( `ACCOUNT_EQUITY` )
					FROM 			`mt5_account_data`
					WHERE 			`mt5_account_data`.`ACCOUNT_NUMBER` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt5_account_data`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
				);
				
-- det countTrades
UPDATE			`temp_trade_account_stats_d1`
SET				`countTrades` = (
					SELECT 			COUNT(*)
					FROM 			`mt5_account_history_deals`
					WHERE			`mt5_account_history_deals`.`ACCOUNT_NUMBER` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt5_account_history_deals`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
						AND			`mt5_account_history_deals`.`DEAL_TYPE` IN ( 'buy', 'sell' )
				)
WHERE			`tradePlatform` = 'MetaTrader 5';

UPDATE			`temp_trade_account_stats_d1`
SET				`countTrades` = (
					SELECT 			COUNT(*)
					FROM 			`mt4_account_history`
					WHERE			`mt4_account_history`.`AccountNumber` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt4_account_history`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
						AND			`mt4_account_history`.`OrderType` IN ( 0, 1 )
				)
WHERE			`tradePlatform` = 'MetaTrader 4';
	
-- det gain
UPDATE			`temp_trade_account_stats_d1`
SET				`gain` = ( `balance` - `deposit` ) / `deposit` * 100;	

-- det drowAbs
UPDATE			`temp_trade_account_stats_d1`
SET				`drowAbs` = - ABS( ( `deposit` - `minEquity` ) / `deposit` * 100  );	

-- det drowMax
UPDATE			`temp_trade_account_stats_d1`
SET				`drowMax` = (
					SELECT			- GREATEST( MAX( `ACCOUNT_EQUITY_DROW_PERC` ), 0 )
					FROM			`mt5_account_data`
					WHERE 			`mt5_account_data`.`ACCOUNT_NUMBER` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt5_account_data`.`AccountServerId` = `temp_trade_account_stats_d1`.`idServer`
				);	
				
-- det trueDT
UPDATE			`temp_trade_account_stats_d1`
SET				`trueDT` = FROM_UNIXTIME((
					SELECT			`ACCOUNT_UPDATE`
					FROM			`mt5_account_info`
					WHERE			`mt5_account_info`.`ACCOUNT_NUMBER` = `temp_trade_account_stats_d1`.`accountNumber`
						AND			`mt5_account_info`.`AccountServer` = `temp_trade_account_stats_d1`.`nameServer`
					LIMIT			1
				));


-- return stats				
REPLACE INTO 	`{{trade_account_stats}}` 
			  ( 
				`idAccount`, 
				`balance`, 
				`deposit`, 
				`withdrawals`, 
				`equity`, 
				`profit`, 
				`minEquity`, 
				`margin`, 
				`freeMargin`, 
				`countTrades`, 
				`gain`, 
				`drowAbs`, 
				`drowMax`, 
				`DT`,  
				`trueDT`
			)
SELECT			`idAccount`, 
				ROUND( `balance`, 2 ),
				ROUND( `deposit`, 2 ),
				ROUND( `withdrawals`, 2 ),
				ROUND( `equity`, 2 ),
				ROUND( `profit`, 2 ),
				ROUND( `minEquity`, 2 ),
				ROUND( `margin`, 2 ),
				ROUND( `freeMargin`, 2 ),
				`countTrades`, 
				ROUND( `gain`, 2 ),
				ROUND( `drowAbs`, 2 ),
				ROUND( `drowMax`, 2 ),
				NOW(), 
				`trueDT`
FROM			`temp_trade_account_stats_d1`;


-- drop temp struct
DROP TEMPORARY TABLE IF EXISTS `temp_trade_account_stats_d1`;
