<?
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
	//defined('YII_DEBUG') or define('YII_DEBUG',true);
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;
	
	define( 'PATH_TO_WP', $pathWP );

	if( $loadWP ) { 
		define( 'WP_FOR_YII', true );
		$er = error_reporting();
		require_once "{$pathWP}/wp-load.php";
		error_reporting( $er );
	}
	
	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug ) || $debug === false ) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	define( 'YII_LOG_ERRORS', @$log_errors );
		
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	require_once "{$dir}/protected/components/sys/WebApplication.php";
	$config = "{$dir}/protected/config/default.php";
	$app = new WebApplication( $config );
	
	if( $loadWP ) {
		Yii::app()->params['wpLoaded'] = true;
	}
	
    $app->run();
