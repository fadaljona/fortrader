#!/bin/sh

cat /var/www/fortrader/domains/fortrader.org/services/protected/data/dealsSymbolsList.txt | while read line
do
 wget $line
done

rm /var/www/fortrader/domains/fortrader.org/services/protected/data/dealsOnGraph/*
cp /var/www/fortrader/domains/fortrader.org/services/protected/data/dealsOnGraphTmp/* /var/www/fortrader/domains/fortrader.org/services/protected/data/dealsOnGraph
rm /var/www/fortrader/domains/fortrader.org/services/protected/data/dealsOnGraphTmp/*
