<?php

global $wpFromConsole;

$wpFromConsole = true;

$documentRoot = dirname(__FILE__) . '/../';

require_once $documentRoot . '/wp-load.php';

clearstatcache();
if( file_exists( $documentRoot . '/wp-content/plugins/fortrader-settings/removeWpFileCache.txt' ) ){
	exec("rm -rf {$documentRoot}/tmp/*");
	transient_theme_flush();
	echo 'cache removed';
	unlink($documentRoot . '/wp-content/plugins/fortrader-settings/removeWpFileCache.txt');
}
