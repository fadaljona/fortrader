CKEDITOR.editorConfig = function( config ) {
	config.allowedContent = true;
	config.contentsCss = [
		'/services/assets-static/css/bootstrap.min.css',
		'/services/assets-static/css/ace.min.css',
		'//fonts.googleapis.com/css?family=Open+Sans:400,300&amp;subset=cyrillic-ext,cyrillic',
		'/services/css/forCKEditor.css',
	];
	config.toolbar = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ '-', /*'Save',*/ 'NewPage', 'Preview'/*, 'Print', '-', 'Templates'*/ ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-'/*, 'Scayt'*/ ] },
		/*{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },*/
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', /*'CreateDiv'*/, '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', /*'-', 'BidiLtr', 'BidiRtl'*/ ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', /*'Flash', */'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'/*, 'PageBreak', 'Iframe'*/ ] },
		'/',
		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
		{ name: 'others', items: [ 'Source' ] },
		/*{ name: 'about', items: [ 'About' ] }*/
	];
	
	config.resize_dir = 'both';
};