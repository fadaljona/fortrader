#!/bin/sh

#aria2c --input-file=/media/sf_data/list.txt

#newsRssDownloadList.txt

documentRoot="$(realpath ${0%/*})/../";

fileName="newsRssDownloadList.txt";
filePath="$documentRoot/services/protected/data/";
tmpDir="newsRss/tmp/";
storageDir="newsRss/";

consolePhp="/opt/plesk/php/7.0/bin/php";
importPhpScript="$documentRoot/services/cronImportRssNews.php";


psContent=$(ps -u fortrader -o pid,command);
findPsRows=$(echo "$psContent" | grep rssDownload.sh);
runedScripts=$(echo "$findPsRows" | wc -l);



if [ $runedScripts -gt 1 ]
  then
    echo 'already runed';
  else
    for i in `seq 1 4`;
    do
      
      
      aria2c --input-file=$filePath$fileName

      runImport=0;

      for file in $filePath$tmpDir*; 
	do 
	  newSize=$(wc -c <"$file");
	  baseFileName=$(basename $file);
	  
	  oldFile=$filePath$storageDir$baseFileName
	  
	  if [ ! -f $oldFile ]
	    then
	      runImport=1;
	    else
	      oldSize=$(wc -c <"$oldFile");
	      if [ $newSize -ne $oldSize ]
		then
		  runImport=1;
	      fi
	    fi
      done

      rm $filePath$storageDir*.txt
      cp -a $filePath$tmpDir. $filePath$storageDir
      rm $filePath$tmpDir*.txt

      if [ $runImport -eq 1 ]
	then
	  echo 'runImport';
	  $consolePhp $importPhpScript &> /dev/null
      fi
      
      sleep 10
    done 
fi