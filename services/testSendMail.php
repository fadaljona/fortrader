<?
	$start = microtime(true);
	
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
		
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;
	
	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$config = "{$dir}/protected/config/default.php";
	$app = Yii::createWebApplication( $config );
	
	Yii::import( 'components.MailerComponent' );
	$factoryMailer = new MailerComponent();
	$mailer = $factoryMailer->instanceMailer();
			
	$mailer->addAddress( 'akadem87@gmail.com' );
			
	$mailer->Subject = 'test sub';
	$mailer->Body = 'tset mess';


	
	if(!$mailer->send()) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mailer->ErrorInfo;
	} else {
		echo 'Message has been sent';
	}
	
?>
