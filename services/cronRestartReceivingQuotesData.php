<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require dirname(__DIR__) . '/ratchet/vendor/autoload.php';


use Quotes\Helper;

$dir = dirname(__FILE__);
chdir($dir);
    
$boot = "{$dir}/boot-dist.php";
if (is_file($boot)) {
    require_once $boot;
}
    
$boot = "{$dir}/boot-local.php";
if (is_file($boot)) {
    require_once $boot;
}
    
if (!empty($timezone)) {
    date_default_timezone_set($timezone);
}
if (!empty($debug)) {
    define('YII_DEBUG', $debug);
}
if (!empty($mb_encoding)) {
    mb_internal_encoding($mb_encoding);
}
if (!empty($profiling_action)) {
    define('PROFILING_ACTION', true);
}
    
require_once $pathYii;
    
$dir = dirname(__FILE__);
chdir($dir);
    
$config = "{$dir}/protected/config/default.php";
$app = Yii::createWebApplication($config);



$quotesSettings = QuotesSettingsModel::getInstance();
$path = Yii::getPathOfAlias('webroot') .'/..'. $quotesSettings->phpServersPath;

$bitfinex = array('quotes-bitfinex-server.php', 'quotes-bitfinex-history-server.php');
$bitz = array('quotes-bitz-history-server.php', 'quotes-bitz-server.php');
$okex = 'quotes-okex.php';
$binance = 'quotes-binance.php';

$phpPath = Yii::App()->params['consolePhpPath'];

if ($quotesSettings->autoStartBitfinex) {
    foreach ($bitfinex as $one) {
        $pid = Helper::getPidByScriptName($one);
        if ($pid) {
            exec("kill ".$pid);
            exec("nohup {$phpPath} {$path}/{$one} >/dev/null 2>/dev/null &");
        } else {
            exec("nohup {$phpPath} {$path}/{$one} >/dev/null 2>/dev/null &");
        }
    }
}

if ($quotesSettings->autoStartBitz) {
    foreach ($bitz as $one) {
        $pid = Helper::getPidByScriptName($one);
        if ($pid) {
            exec("kill ".$pid);
            exec("nohup {$phpPath} {$path}/{$one} >/dev/null 2>/dev/null &");
        } else {
            exec("nohup {$phpPath} {$path}/{$one} >/dev/null 2>/dev/null &");
        }
    }
}

if ($quotesSettings->autoStartOkex) {
    $pid = Helper::getPidByScriptName($okex);
    if ($pid) {
        exec("kill ".$pid);
        exec("nohup {$phpPath} {$path}/{$okex} >/dev/null 2>/dev/null &");
    } else {
        exec("nohup {$phpPath} {$path}/{$okex} >/dev/null 2>/dev/null &");
    }
}

if ($quotesSettings->autoStartBinance) {
    $pid = Helper::getPidByScriptName($binance);
    if ($pid) {
        exec("kill ".$pid);
        exec("nohup {$phpPath} {$path}/{$binance} >/dev/null 2>/dev/null &");
    } else {
        exec("nohup {$phpPath} {$path}/{$binance} >/dev/null 2>/dev/null &");
    }
}

echo "finish\n";
