<?php
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
		
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;

	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	define( 'YII_LOG_ERRORS', @$log_errors );
	
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$monthsToStoreData = 6;
	
	require_once "{$dir}/protected/components/sys/WebApplication.php";
	$config = "{$dir}/protected/config/default.php";
	$app = new WebApplication( $config );
	
	
	$deletedInformersStats = InformersStatsModel::model()->deleteAll(
		" accessDate < :accessDate ", 
		array(':accessDate' => date( 'Y-m-d H:i:s', time() - 60*60*24*30 * 1 ) )
	);
	
	echo "deleted rows from ft_informers_stats $deletedInformersStats \n";
	
	$deletedPostsStatsTraffic = PostsStatsTrafficModel::model()->deleteAll(
		" visitDate < :visitDate ", 
		array(':visitDate' => date( 'Y-m-d H:i:s', time() - 60*60*24*30 * $monthsToStoreData ) )
	);
	
	echo "deleted rows from ft_posts_stats_traffic $deletedPostsStatsTraffic \n";
	
	$deletedQuotesInformerStats = QuotesInformerStatsModel::model()->deleteAll(
		" accessDate < :accessDate ", 
		array(':accessDate' => date( 'Y-m-d H:i:s', time() - 60*60*24*30 * 1 ) )
	);
	
	echo "deleted rows from ft_quotes_informer_stats $deletedQuotesInformerStats \n";
	
	$deletedStatDays = Yii::App()->db->createCommand()->delete(
		'stat_days', 
		" date < :date ", 
		array(':date' => date( 'Y-m-d H:i:s', time() - 60*60*24*30 * $monthsToStoreData ) )
	);
	
	echo "deleted rows from stat_days $deletedStatDays \n";
	
	
?>