<?
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
		
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;
	
	$loadWP = true;
	if( $loadWP ) {
		define( 'WP_FOR_YII', true );
		$er = error_reporting();
		require_once "{$pathWP}/wp-load.php";
		error_reporting( $er );
	}
	
	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$config = "{$dir}/protected/config/default.php";
	$app = Yii::createWebApplication( $config );
	
	if( $loadWP ) {
		Yii::app()->params['wpLoaded'] = true;
	}
	
	Yii::import( 'components.WpPopularCatsComponent' );
	$component = new WpPopularCatsComponent();
	$component->update();
?>