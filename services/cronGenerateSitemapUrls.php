<?
	$start = microtime(true);
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
		
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;

	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	define( 'YII_LOG_ERRORS', @$log_errors );
	
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$config = "{$dir}/protected/config/default.php";
	
	require_once(dirname(__FILE__).'/protected/components/sys/WebApplication.php');
	$app = new WebApplication( $config );
	
	$_SERVER['SCRIPT_FILENAME'] = 'index.php';
	$_SERVER['SCRIPT_NAME'] =  '/index.php';
	$_SERVER['REQUEST_URI'] = 'index.php';

	
	$sitemapModule = Yii::app()->getModule('sitemap');
	$types = $sitemapModule->oneSiteMapTypeToController;
	$types['other'] = '';
	
	foreach( $types as $type => $route ){
		$sitemapModule->getAllUrls( $type, 0 );
	}
	
	$time = microtime(true) - $start;
	printf('time %.4F s', $time);

?>