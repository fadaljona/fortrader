<?
	$start = microtime(true);
	
	error_reporting( E_ALL );
	ini_set( 'display_errors', 1 );
		
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$boot = "{$dir}/boot-dist.php";
	if( is_file( $boot )) require_once $boot;
	
	$boot = "{$dir}/boot-local.php";
	if( is_file( $boot )) require_once $boot;
	
	if( !empty( $timezone )) date_default_timezone_set( $timezone );
	if( !empty( $debug )) define( 'YII_DEBUG', $debug );
	if( !empty( $mb_encoding )) mb_internal_encoding( $mb_encoding );
	if( !empty( $profiling_action )) define( 'PROFILING_ACTION', true );
	
	require_once $pathYii;
	
	$dir = dirname(__FILE__);
	chdir( $dir );
	
	$config = "{$dir}/protected/config/default.php";
	$app = Yii::createWebApplication( $config );
	
	$avatarsToUpdate = Yii::app()->db->createCommand()
		->select('user_id, photourl')
		->from('wp_wslusersprofiles smprofiles')  
		->queryAll(); 
		
	foreach( $avatarsToUpdate as $avatarToUpdate ){
		userAvatarUpdate( $avatarToUpdate['user_id'], $avatarToUpdate['photourl'] );
	//	echo $avatarToUpdate['user_id']. '_' . $avatarToUpdate['photourl'] . "<br />";
		@ob_end_flush();
		flush();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function userAvatarUpdate( $userId, $photoUrl ) {
		
		$documentRoot = 'D:/GitHub/test/dev.fortrader.ru';
		
		$tempDir = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();
		$tempName = md5( time() . $userId );
		$tempFile = $tempDir . '/' . $tempName .'.img';

		file_put_contents( $tempFile, file_get_contents($photoUrl) );
		$photoUrlType = exif_imagetype( $tempFile );

		$imageTypes = array(
			1 => 'gif',
			2 => 'jpg',
			3 => 'png'
		);
		$tempFileNew = $tempDir . '/' . $tempName . '.' . $imageTypes[$photoUrlType];
		rename( $tempFile, $tempFileNew );

		$userModel = UserModel::model()->findByPk( $userId );
		if( $userModel ){
			echo $avatarFile = Yii::app()->basePath . '/../' . $userModel->avatar->path;
			echo "\n";

			echo "\n";
		
			if( filesize( $avatarFile ) != filesize( $tempFileNew ) ){
				$userModel->uploadAvatar( $tempFileNew );
			}
		}
		
		unlink( $tempFileNew );
	}
	
?>
