var wContestMemberAccountMonitorListWidgetOpen;
!function( $ ) {
	wContestMemberAccountMonitorListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wContestMemberAccountMonitorListWidget',
			ajax: false,
			hidden: false,
			ajaxLoadListURL: '/contestMember/ajaxLoadMemberAccountMonitorList',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wContestMemberAccountMonitorGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idContest="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( !$li.hasClass( 'disabled' ) && !$li.hasClass( 'active' ) ) {
					var $href = $link.attr( 'href' );
					var matchs = /accountMonitorPage=(\d+)/.exec( $href );
					var accountMonitorPage = matchs ? matchs[1] : 1;
					self.disable();
					$.getJSON( yiiBaseURL+options.ajaxLoadListURL, {id:options.idMember, accountMonitorPage:accountMonitorPage}, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $content = $( data.list );
							self.$ns.replaceWith( $content );
							self.init();
						}
					});
				}
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );