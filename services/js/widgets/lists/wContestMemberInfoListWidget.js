var wContestMemberInfoListWidgetOpen;
!function( $ ) {
	wContestMemberInfoListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wContestMemberInfoGridView',
			ajax: false,
			ajaxDeleteURL: '/user/ajaxDeleteContestMember',
			editURL: '/user/profile?tabTA=yes&idEditMember=%idEditMember%',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wContestMemberInfoGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				
				self.$gridTable.on( 'click', '.wDelete', self.deleteSpecificRow );
				self.$gridTable.on( 'click', '.wEdit', self.edit );
				
				self.$gridTable.on( 'click', 'a', self.onClickLink );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idMember="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			deleteSpecificRow:function() {
				self.hideDropdowns();
				var $row = $(this).closest( "tr[idMember]" );
				var id = $row.attr( 'idMember' );
				var $row = self.$gridTable.find( '> tbody > tr[idMember="'+id+'"]' );
				if( $row.length ) {
					if( !confirm( options.ls.lDeleteConfirm )) return false;
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							$rowLoading.remove();
							self.resetSelection();
							self.hide();
						}
					});
				}
				return false;
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			edit:function() {
				var $tr = $(this).closest( 'tr' );
				var idMember = $tr.attr( 'idMember' );
				var url = options.editURL.replace( '%idEditMember%', idMember );
				document.location.assign( yiiBaseURL + url );
				return false;
			},
			onClickLink:function() {
				var $link = $(this);
				var href = $link.attr( 'href' );
				if( href != '#' ) {
					document.location.assign( href );
					return false;
				}
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );