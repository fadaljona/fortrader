var wContestsListWidgetOpen;
!function( $ ) {
	wContestsListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wContestsListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wContestsGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$gridTable.on( 'click', '.wDelete', self.deleteSpecificRow );
				self.$gridTable.on( 'click', '.wChangeStatusRegistrationStoped', { status: 'registration stoped' }, self.changeStatusSpecificRow );
				self.$gridTable.on( 'click', '.wChangeStatusRegistration', { status: 'registration' }, self.changeStatusSpecificRow );
				self.$gridTable.on( 'click', '.wChangeStatusStarted', { status: 'started' }, self.changeStatusSpecificRow );
				self.$gridTable.on( 'click', '.wChangeStatusCompleted', { status: 'completed' }, self.changeStatusSpecificRow );
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			hideIfEmpty:function( immediately ) {
				if( self.$gridTable.find( '> tbody > tr[idContest]' ).length ) return false;
				if( self.$ns.find( '.pagination' ).length ) return false;
				
				self.hide();
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idContest="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			delete:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idContest="'+id+'"]' );
				$row.remove();
				self.issetRemovedRows = true;
				self.hideIfEmpty();
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( $li.hasClass( 'disabled' )) return false;
				if( !$li.hasClass( 'active' ) || self.issetRemovedRows ) {
					var $href = $link.attr( 'href' );
					var matchs = /contestsPage=(\d+)/.exec( $href );
					var contestsPage = matchs ? matchs[1] : 1;
					self.disable();
					$.getJSON( options.ajaxLoadListURL, {contestsPage:contestsPage}, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $content = $( data.list );
							self.$ns.replaceWith( $content );
							self.init();
						}
					});
				}
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );