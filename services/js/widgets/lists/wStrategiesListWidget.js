var wStrategiesListWidgetOpen;
!function( $ ) {
	wStrategiesListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wStrategiesListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxDeleteURL: '/admin/strategy/ajaxDelete',
			ajaxUserDeleteURL: '/strategy/ajaxDelete',
			ajaxLoadListURL: '/strategy/ajaxLoadList',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( '> form' );
				self.$grid = self.$ns.find( options.ins+'.wStrategiesGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				self.$wDummReload = self.$ns.find( options.ins+'.wDummReload' );
				self.$wDummReloadText = self.$ns.find( options.ins+'.wDummReloadText' );
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
				self.$ns.on( 'change', 'form input, form select', self.changeFilter );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			isEmpty:function() {
				if( self.$gridTable.find( '> tbody > tr[idEA]' ).length ) return false;
				if( self.$ns.find( '.pagination' ).length ) return false;
				return true;
			},
			hideIfEmpty:function( immediately ) {
				if( self.isEmpty()) self.hide();
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idEA="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			disable:function() {
				self.$wDummReload.show();
				self.$wDummReloadText.show();
				self.$wDummReloadText.height( self.$ns.height() );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			updateTooltips:function() {
				self.$ns.find('[data-rel=tooltip]').tooltip();
			},
			load:function( query ) {
				self.disable();
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, query, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
						self.updateTooltips();
					}
				});
			},
			changeFilter:function() {
				var query = self.$form.serialize();
				self.load( query );
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				
				var href = $link.attr( 'href' );
				var query = commonLib.getQueryString( href );
				
				self.load( query );
				
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );