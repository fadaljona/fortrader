var wAdminAdvertisementsListWidgetOpen;
!function( $ ) {
	wAdminAdvertisementsListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminAdvertisementsListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxLoadURL: '/admin/advertisement/ajaxLoadRow',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wAdvertisementsGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$ns.on( 'click', 'th[class*=sorting]', self.changeSorting );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idAdvertisement="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			hideEmptyRow:function() {
				self.$gridTable.find( 'td.empty' ).closest( 'tr' ).hide();
			},
			add:function( id ) {
				self.show();
				var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
				self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
				$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
					if( data.error ) {
						alert( data.error );
						$rowLoading.remove();
					}
					else{
						var $row = $( data.row );
						$row.replaceAll( $rowLoading );
						self.resetSelection();
						self.hideEmptyRow();
					}
				});
			},
			update:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idAdvertisement="'+id+'"]' );
				if( $row.length ) {
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							var $row = $( data.row );
							$row.replaceAll( $rowLoading );
							self.resetSelection();
						}
					});
				}
			},
			delete:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idAdvertisement="'+id+'"]' );
				$row.remove();
				
				if( !self.$gridTable.find( '> tbody > tr[idAdvertisement]' ).length ) {
					self.hide();
				}
			},
			changeSorting:function() {
				var $th = $(this);
				var idCampaign = commonLib.getVar( 'idCampaign' );
				var sortField = $th.attr( 'sortField' );
				var sortType = $th.hasClass( 'sorting_asc' ) ? 'DESC' : 'ASC';
				var urlBase = commonLib.getAbsoluteUrl();
				var url = urlBase + '?idCampaign=' + idCampaign + '&sortField=' + sortField + '&sortType=' + sortType;
				document.location.assign( url );
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );