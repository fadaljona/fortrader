var wAdminMessagesListWidgetOpen;
!function( $ ) {
	wAdminMessagesListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminMessagesListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxLoadURL: '/admin/message/ajaxLoadRow',
			inSearch: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wMessagesGridView' );
				self.$gridTable = self.$grid.find( '> form > table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				
				if( options.hidden ) {
					self.hide( true );
				}
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idMessageKey="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			hideEmptyRow:function() {
				self.$gridTable.find( 'td.empty' ).closest( 'tr' ).hide();
			},
			add:function( id ) {
				self.show();
				var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
				self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
				$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
					if( data.error ) {
						alert( data.error );
						$rowLoading.remove();
					}
					else{
						var $row = $( data.row );
						$row.replaceAll( $rowLoading );
						//self.setSelection( [ id ] );
						self.resetSelection();
						self.hideEmptyRow();
					}
				});
			},
			update:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idMessageKey="'+id+'"]' );
				if( $row.length ) {
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							var $row = $( data.row );
							$row.replaceAll( $rowLoading );
							//self.setSelection( [ id ] );
							self.resetSelection();
						}
					});
				}
			},
			delete:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idMessageKey="'+id+'"]' );
				$row.remove();
				
				if( !self.$gridTable.find( '> tbody > tr[idMessageKey]' ).length && !options.inSearch ) {
					self.hide();
				}
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );