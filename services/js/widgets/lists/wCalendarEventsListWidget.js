var wCalendarEventsListWidgetOpen;
!function( $ ) {
	wCalendarEventsListWidgetOpen = function( options ) {
		var defLS = {
			lDone: 'Done',
			lSec: 'sec',
			lMin: 'min',
			lH: 'h',
			lDay_ip: 'day',
			lDay_rp: 'day',
			lDay_mrp: 'days',
			lMonth_ip: 'month',
			lMonth_rp: 'month',
			lMonth_mrp: 'months',
			lYear_ip: 'year',
			lYear_rp: 'year',
			lYear_mrp: 'years',
			lDeleteConfirm: 'Delete?',
			lSelectEvents: 'Select events!',
			lLoading: 'Loading...',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wCalendarEventsListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			singleUrl: '/calendarEvent/single',
			cookieFilterTypesVarName: 'wCalendarEventsList_filterTypes',
			expires: 365,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			prevZone: false,
			rangeSliderVar: false,
			prevDaySelected: '',
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
				self.$timezoneSelect = self.$ns.find('.timezone_select');
				self.initTimeZoneSelect();
				self.initUtcCookie();
				self.$selectDay = self.$ns.find('#calendarSelectDate');
				
				self.prevDaySelected = self.$selectDay.val();
				
				setInterval(function(){
					if( self.prevDaySelected != self.$selectDay.val() ){
						self.prevDaySelected = self.$selectDay.val();
						self.daySelected();
					}
				}, 1000);
				
				self.$filterBtn = self.$ns.find('.showCountrySliderFilter');
				self.$filterBtn.click( self.showHideFilter );
				self.$countryBtnSelect = self.$ns.find('.country_btn');
				self.$countryBtnSelect.click( self.checkResetCountries );
				self.$rangeSlider = self.$ns.find('.range_slider1');
				self.initRangeSlider();
				self.$filterApplyBtn = self.$ns.find('.load_btn');
				self.$filterApplyBtn.click( self.applyFilter );
				
				self.$grid = self.$ns.find( options.ins+'.wCalendarEventsGridView' );
				self.$gridTable = self.$grid.find( ' table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				self.$wProgress = self.$ns.find( options.ins+'.wProgress' );
				self.$wFilterTypes = self.$ns.find( options.ins+'.wFilterType' );
				self.$wSaveFilterTypes = self.$ns.find( options.ins+'.wSaveFilterTypes' );
				
				self.$gridTable.on( 'click', '.wShowMore', self.showMore );
				self.$wSaveFilterTypes.on( 'click', self.saveFilterTypes );
				
				setInterval( self.refresh, 1000 * 60 );
				self.initFilterTypes();
				
				self.$visibleFilterBox = self.$ns.find('.showCalendarFilterData');
				self.showFilterData();
				
				self.calendarMenu = self.$ns.find('.calendar_form');
				
				self.$resetFilterBtn = self.$ns.find('.calendar_btn_reload');
				self.$resetFilterBtn.click( self.resetFilter );
				
			},
			resetFilter: function(){
				self.$ns.find('.country_content .country_col input[type="checkbox"]').each( function() { $(this).prop( 'checked', false ); });
				self.createCookie('ftCalendarCountries', '');
				self.createCookie('ftVolatility', 0 );
				self.$rangeSlider.val(0);
				self.rangeSliderVar.reset();
				self.applyFilter();
			},
			showFilterData: function(){
				var selectedCountries = self.getCookie('ftCalendarCountries');
				var selectedVolatility = self.getCookie('ftVolatility');
				if( selectedCountries || ( selectedVolatility && selectedVolatility != 0) ){
					self.$visibleFilterBox.html('');
					self.$visibleFilterBox.addClass('country_content');
					if( selectedCountries ){
					//	self.$visibleFilterBox.append( options.ls.lFilteredByCountry + ': ' );
						$.each( selectedCountries.split(','), function( key, value ) {
							self.$visibleFilterBox.append( '<span class="oneCountryWrap"><span class="checkbox_img"><img src="' + options.countriesFilter[value].img16 + '" alt="' + options.countriesFilter[value].name + '"></span>' + options.countriesFilter[value].name + '</span>' );
						});
					}
					if( selectedVolatility && selectedVolatility != 0 ){
						if( selectedCountries ) self.$visibleFilterBox.append('<br />');
						self.$visibleFilterBox.append( options.ls.lFilteredByVolatility + ': ' + selectedVolatility );
					}
				}else{
					self.$visibleFilterBox.html('');
					self.$visibleFilterBox.removeClass('country_content');
				}
			},
			applyFilter: function(){
				var countriesStr = '';
				self.$ns.find('.country_content .country_col input[type="checkbox"]:checked').each( function( index ) {
					if( index == 0 ) 
						countriesStr = $(this).val();
					else
						countriesStr = countriesStr + ',' + $(this).val();
				});
				
				self.createCookie('ftCalendarCountries', countriesStr);
				self.createCookie('ftVolatility', self.$rangeSlider.val() );
				self.showFilterData();
				
				self.closeFilter();
				
				$('html,body').animate({scrollTop: self.calendarMenu.offset().top-30}, 500);
				
				self.updateList();
				
				return false;
			},
			closeFilter: function(){
				self.$ns.find(".country_wrapp").slideUp();
            	self.$ns.find(".turn_box").slideDown();
            	self.$filterBtn.removeClass("active");
			},
			updateList:function() {
				self.disable();

				var data = {};
				
				data.period = options.period;
				if( data.period == 'date_v' ) data.d_view = options.d_view;
				
				$.getJSON( options.ajaxLoadUpdatedList, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$gridTable.find('tbody').html( data.list.replace('<tbody>', '').replace('</tbody>', '') );
						options.moreDate = data.moreDate;
					}
					self.enable();
				});
			},
			initRangeSlider: function(){
				self.$rangeSlider.ionRangeSlider({
					type: "single",
					grid: true,
					from: options.selectedVolatility,
					values: [0, 1, 2, 3]
				});
				self.rangeSliderVar = self.$rangeSlider.data("ionRangeSlider");
			},
			checkResetCountries: function(){
				if( $(this).hasClass('check') ){
					self.$ns.find('.country_content input[type="checkbox"]').prop( 'checked', true );
				}else{
					self.$ns.find('.country_content input[type="checkbox"]').prop( 'checked', false );
				}
			},
			showHideFilter:function(){
				self.$ns.find(".country_wrapp").slideToggle();
            	self.$ns.find(".turn_box").slideToggle();
            	$(this).toggleClass("active");
			},
			daySelected: function(){
				window.location.replace( options.dayUrl.replace('date_v', self.$selectDay.val()) ); 
			},
			initTimeZoneSelect: function(){
				self.prevZone = self.$timezoneSelect.val();
				self.$timezoneSelect.styler({
					onSelectClosed: function() {
						if( self.$timezoneSelect.val() != self.prevZone ){
							self.prevZone = self.$timezoneSelect.val();
							self.zoneSelected(self.prevZone);
						}
					}
				});
			},
			initUtcCookie: function(){
				if( self.getCookie('utc') == null ){
					var offset = ( (new Date().getTimezoneOffset() * -1) - (new Date().getTimezoneOffset() * -1 % 60) ) / 60;
					self.$timezoneSelect.val(offset).trigger('refresh');
					self.zoneSelected(offset);
				}
			},
			getCookie: function(c_name) {
				if (document.cookie.length > 0) {
					c_start = document.cookie.indexOf(c_name + "=");
					if (c_start != -1) {
						c_start = c_start + c_name.length + 1;
						c_end = document.cookie.indexOf(";", c_start);
						if (c_end == -1) {
							c_end = document.cookie.length;
						}
						return unescape(document.cookie.substring(c_start, c_end));
					}
				}
				return null;
			},
			createCookie: function(c_name, c_val) {
				var expires;
				var date = new Date();
				date.setTime(date.getTime() + (100000 * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();	
				document.cookie = c_name + "=" + c_val + expires + "; path=/";
			},
			zoneSelected: function(zone){
				self.createCookie('utc', zone);
				self.updateList();
			},
			initFilterTypes:function() {
				var types = $.cookie( options.cookieFilterTypesVarName );
				types = types && types.length ? types.split( ',' ) : [];
				
				self.$wFilterTypes.each( function () {
					var value = $(this).attr( 'value' );
					if ( !types.length || $.inArray( value, types ) != -1 ) $(this).prop( 'checked', true );
				});
			},
			saveFilterTypes:function() {
				var types = [];
				self.$wFilterTypes.each( function () {
					var value = $(this).attr( 'value' );
					$(this).prop( 'checked' ) && types.push( value );
				});
				
				if( !types.length ) {
					alert( options.ls.lSelectEvents );
					return;
				}
				
				types = types.join( "," );
				$.cookie( options.cookieFilterTypesVarName, types, { expires: options.expires, path: "/" });
				self.$wSaveFilterTypes.html( options.ls.lLoading );
				self.$wSaveFilterTypes.attr( 'disabled', 'disabled' );
				document.location.reload();
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find( '.spinner-wrap' ).remove();
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tr[data-idEvent="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
				var $tr = self.getSelection();
				var id = $tr.attr( 'data-idEvent' );
				if( !id ) return;
				document.location.assign( yiiBaseURL+options.singleUrl+'?id='+id );
			},
			showMore:function() {
				var $td = $(this).closest( 'td' );
				var $tr = $td.closest( 'tr' );
				$td.html( '' );
				self.disable();

				
				var data = {};
				if( options.period == 'forthcoming' ) {
					data.period = options.period;
					var idsSkip = [];
					var lastTimeEvent = false;
					self.$gridTable.find( 'tr[data-idEvent]' ).each( function() {
						var idEvent = $(this).attr( 'data-idEvent' );
						idsSkip.push( idEvent );
						lastTimeEvent =  $(this).attr( 'data-timeEvent' );
					});
					data.idsSkip = idsSkip.join( ',' );
					data.date = lastTimeEvent;
				}
				else{
					data.period = 'day';
					data.date = options.moreDate;
				}
				
				$.getJSON( options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var content = data.list.replace('<tbody>', '').replace('</tbody>', '');
						content = $(content);
						var trToRemove = false;
						
						if( options.period == 'forthcoming' ) {
							if(self.$gridTable.find('tbody .calendarDateBtn .btn_date').length){
								var lastBtnText = self.$gridTable.find('tbody .calendarDateBtn .btn_date').last().text();
								if( content.find('.calendarDateBtn .btn_date').length ){
									
									var firstBnt = content.find('.calendarDateBtn .btn_date').first();
									if( firstBnt.text() == lastBtnText ){
										trToRemove = firstBnt.closest('tr');
									}
								}
							}
						}
						
						$tr.after( content );
						$tr.remove();
						if( trToRemove !== false ) trToRemove.remove();
						options.moreDate = data.moreDate;
					}
					self.enable();
				});
			},
			refresh: function() {
				self.refreshTimeLeft();
				self.refreshHard();
			},
			refreshTimeLeft: function(){
				var date = new Date();
				var time = Math.round( date.getTime() / 1000 );
				self.$gridTable.find( 'tr[data-timeEvent].needUpdate' ).each( function( index ) {
					var timeEvent = parseInt( $(this).attr( 'data-timeEvent' ));
					var timeLeft = Math.abs( timeEvent - time );
					$(this).find('.timeLeft').text( self.parseTimeToStr(timeLeft) );
				});
			},
			addRowBg: function( row ){
				if( row.attr('data-seriousEvent') == 1 ) row.addClass('green_bg2');
				if( row.attr('data-seriousEvent') == 2 ) row.addClass('cream_bg');
				if( row.attr('data-seriousEvent') == 3 ) row.addClass('red_bg2');
			},
			refreshHard: function() {
				var radius = 60 * 5;
				var date = new Date();
				var time = Math.round( date.getTime() / 1000 );
				var idsEvents = [];
				
				self.$gridTable.find( 'tr[data-timeEvent].needUpdate' ).each( function( index ) {
					var idEvent = $(this).attr( 'data-idEvent' );
					var timeEvent = parseInt( $(this).attr( 'data-timeEvent' ));
					var d = Math.abs( timeEvent - time );
					if( d <= radius ) {
						idsEvents.push( idEvent );
					}
				});
				
				if( idsEvents.length ) {
					$.post( options.ajaxLoadRowsURL, {ids:idsEvents.join( ',' )}, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							if( data.rows ) {
								for( var idEvent in data.rows ) {
									var $row = self.$gridTable.find( 'tr[data-idEvent="'+idEvent+'"]' );
									if( $row.length ) {
										$row.replaceWith( data.rows[ idEvent ] );
										self.addRowBg( self.$gridTable.find( 'tr[data-timeEvent].needUpdate' ).eq( 0 ) );
									}
								}
							}
						}
					}, "JSON" );
				}
			},
			parseTimeToStr: function( time ){
				time = parseInt(time);
				var minets = Math.floor( time / 60 );
				var hours = Math.floor( minets / 60 );
				var days = Math.floor( hours / 24 );
				var months = Math.floor( days / 30 );
				var years = Math.floor( months / 12 );
				
				if( minets == 0 ) {
					var sec = Yii.t("app", "sec" );
					return time + ' ' + sec;
				}else if( hours == 0 ) {
					var minT = Yii.t("app", "min" );
					return minets + ' ' + minT;
				}else if( days == 0 ) {
					var hoursT = Yii.t("app", "h" );
					var minT = Yii.t("app", "min" );
					var minetsOst = minets % 60;
					return hours + ' ' + hoursT  + ' ' + minetsOst + ' ' + minT;
				}else if( months == 0 ) {
					return Yii.t("app", "{n} day|{n} days", days);
				}else if( years == 0 ) {
					return Yii.t("app", "{n} month|{n} months", months);
				}else{
					return Yii.t("app", "{n} year|{n} years", years);
				}
			},
			
			
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );
