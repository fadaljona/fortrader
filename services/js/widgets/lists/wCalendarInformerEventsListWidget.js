var wCalendarInformerEventsListWidgetOpen;
!function( $ ) {
	wCalendarInformerEventsListWidgetOpen = function( options ) {
		var defLS = {
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wCalendarInformerEventsListWidget',
			ajax: false,
			hidden: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			prevZone: false,
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				self.$timeZoneSelect = self.$ns.find('.calendar-header__select');
				self.initSelectStyle();
				
				setInterval( self.refresh, 500 * 60 );
				
				self.$ns.on( 'click', '.informer-calendar__page a', self.changePage );
			},
			refresh: function() {
				self.refreshTimeLeft();
				self.refreshHard();
			},
			refreshTimeLeft: function(){
				var date = new Date();
				var time = Math.round( date.getTime() / 1000 );
				self.$ns.find( 'tr[data-timeEvent].needUpdate' ).each( function( index ) {
					var timeEvent = parseInt( $(this).attr( 'data-timeEvent' ));
					var timeLeft = Math.abs( timeEvent - time );
					$(this).find('.timeLeft').text( self.parseTimeToStr(timeLeft) );
				});
			},
			refreshHard: function() {
				var radius = 60 * 5;
				var date = new Date();
				var time = Math.round( date.getTime() / 1000 );
				var idsEvents = [];
				
				self.$ns.find( 'tr[data-timeEvent].needUpdate' ).each( function( index ) {
					var idEvent = $(this).attr( 'data-idEvent' );
					var timeEvent = parseInt( $(this).attr( 'data-timeEvent' ));
					var d = Math.abs( timeEvent - time );
					if( d <= radius ) {
						idsEvents.push( idEvent );
					}
				});
				
				if( idsEvents.length ) {
					$.post( options.ajaxLoadRowsURL, {ids:idsEvents.join( ',' )}, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							if( data.rows ) {
								for( var idEvent in data.rows ) {
									var $row = self.$ns.find( 'tr[data-idEvent="'+idEvent+'"]' );
									if( $row.length ) {
										$row.replaceWith( data.rows[ idEvent ] );
									}
								}
							}
						}
					}, "JSON" );
				}
			},
			parseTimeToStr: function( time ){
				time = parseInt(time);
				var minets = Math.floor( time / 60 );
				var hours = Math.floor( minets / 60 );
				var days = Math.floor( hours / 24 );
				var months = Math.floor( days / 30 );
				var years = Math.floor( months / 12 );
				
				if( minets == 0 ) {
					var sec = Yii.t("app", "sec" );
					return time + ' ' + sec;
				}else if( hours == 0 ) {
					var minT = Yii.t("app", "min" );
					return minets + ' ' + minT;
				}else if( days == 0 ) {
					var hoursT = Yii.t("app", "h" );
					var minT = Yii.t("app", "min" );
					var minetsOst = minets % 60;
					return hours + ' ' + hoursT  + ' ' + minetsOst + ' ' + minT;
				}else if( months == 0 ) {
					return Yii.t("app", "{n} day|{n} days", days);
				}else if( years == 0 ) {
					return Yii.t("app", "{n} month|{n} months", months);
				}else{
					return Yii.t("app", "{n} year|{n} years", years);
				}
			},
			initSelectStyle: function(){
				self.prevZone = self.$timeZoneSelect.val();
				self.$timeZoneSelect.styler({
					onSelectClosed: function() {
						if( self.$timeZoneSelect.val() != self.prevZone ){
							self.prevZone = self.$timeZoneSelect.val();
							self.zoneSelected(self.prevZone);
						}
					}
				})
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
		
			loadPage:function( page ) {
				self.disable();
			
				var data = { 
					page:page,
					impact: options.impact,
					symbols: options.symbols,
				};

				$.getJSON( options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
						TableSlider.tablQuotesSlider.init();
					}
				});
			},
			changePage:function() {
				var $link = $(this);
				if( $link.hasClass( 'disabled' )) return false;
				if( !$link.hasClass( 'calendar_page-active' ) ) {
					var $href = $link.attr( 'href' );
					var matchs = /page=(\d+)/.exec( $href );
					var page = matchs ? matchs[1] : 1;
					self.loadPage( page );
				}
				return false;
			},
			createCookie: function(c_name, c_val) {
				var expires;
				var date = new Date();
				date.setTime(date.getTime() + (100000 * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();	
				document.cookie = c_name + "=" + c_val + expires + "; path=/";
			},
			zoneSelected: function(zone){
				self.createCookie('utc', zone);
				self.loadPage(1);
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );