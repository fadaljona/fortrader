var wJournalsListWidgetOpen;
!function( $ ) {
	wJournalsListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '.insJournalsListWidget',
			selector: '.wJournalsListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxLoadListURL: '/journal/ajaxLoadList',
			ajaxDeleteURL: '/admin/journal/ajaxDelete',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
                
				self.$grid = self.$ns.find( options.ins+'.wJournalsGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				//self.$selectCountRows = self.$ns.find( options.ins+'.wSelectCountRows' );
				//self.$selectName = self.$ns.find( options.ins+'.wName' );
				self.$wDummReload = self.$ns.find( options.ins+'.wDummReload' );
				self.$wDummReloadText = self.$ns.find( options.ins+'.wDummReloadText' );
				
				if( options.hidden ) {
					self.hide( true );
				}
				console.log(self.deleteSpecificRow);
				self.$gridTable.on( 'click', '.wDelete', self.deleteSpecificRow );
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
				//self.$ns.on( 'click', 'th[class*=sorting]', self.changeSorting );
				//self.$selectCountRows.change( self.changeCountRows );
				//self.$selectName.keydown( self.keydownName );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			isEmpty:function() {
				if( self.$gridTable.find( '> tbody > tr[idJournal]' ).length ) return false;
				if( self.$ns.find( '.pagination' ).length ) return false;
				return true;
			},
			hideIfEmpty:function( immediately ) {
				if( self.isEmpty()) self.hide();
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idBroker="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			disable:function() {
				self.$wDummReload.show();
				self.$wDummReloadText.show();
				self.$wDummReloadText.height( self.$ns.height() );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			updateTooltips:function() {
				self.$ns.find('[data-rel=tooltip]').tooltip();
			},
			loadPage:function( journalsPage ) {
				self.disable();
				//var name = self.$selectName.val();
				var data = {id: options.idContest, journalsPage:journalsPage };
				//if( name ) data.name = name;
				//data.sortField = options.sortField;
				//data.sortType = options.sortType;
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
						self.updateTooltips();
					}
				});
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( $li.hasClass( 'disabled' )) return false;
				if( !$li.hasClass( 'active' ) || self.issetRemovedRows ) {
					var $href = $link.attr( 'href' );
					var matchs = /journalsPage=(\d+)/.exec( $href );
					var journalsPage = matchs ? matchs[1] : 1;
					self.loadPage( journalsPage );
				}
				return false;
			},
			deleteSpecificRow:function() {
				self.hideDropdowns();
				var $row = $(this).closest( "tr[idJournal]" );
				var id = $row.attr( 'idJournal' );
				//var $row = self.$gridTable.find( '> tbody > tr[idJournal="'+id+'"]' );
				if( $row.length ) {
					if( !confirm( options.ls.lDeleteConfirm )) return false;
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							$rowLoading.remove();
							self.issetRemovedRows = true;
							self.resetSelection();
							self.hideIfEmpty();
						}
					});
				}
				return false;
			},
			/*
			changeCountRows:function() {
				self.hideDropdowns();
				var $select = $(this);
				var countRows = $select.val();
				$.cookie( 'BrokersStats2ListWidget_countRows', countRows, {path: '/'} );
				self.loadPage( 1 );
			},
			keydownName:function( event ) {
				if( event.which == 13 ) {
					self.loadPage( 1 );
				}
			},
			changeSorting:function() {
				if( self.isEmpty() ) return false;
				var $th = $(this);
				options.sortField = $th.attr( 'sortField' );
				options.sortType = $th.hasClass( 'sorting_asc' ) ? 'DESC' : 'ASC';
				self.loadPage( 1 );
				return false;
			},
			*/
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );