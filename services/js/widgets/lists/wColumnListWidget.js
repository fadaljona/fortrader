var wColumnListWidgetOpen;
!function( $ ) {
	wColumnListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wColumnListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxLoadListURL: '/default/ajaxLoadColumn',
      limit: 3,
      step: 3
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
//		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
        $classParts = options.selector.split(' ');
        self.$class = $classParts[$classParts.length - 1].replace('.w', '');
				self.$ns = $( options.selector );
        self.$moreLink = self.$ns.find( '.moreLink > a' );
				self.$columns = self.$ns.find('.columns');
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				self.$wDummReload = self.$ns.find( options.ins+'.wDummReload' );
				self.$wDummReloadText = self.$ns.find( options.ins+'.wDummReloadText' );
        self.limit = options.limit;
        self.step = options.step;
				
				if( options.hidden ) {
					self.hide( true );
				}

        self.$ns.on( 'click', '.moreLink a', self.loadMore );
			},
      
			show:function() {
				eval( "self.$ns."+options.showType );
			},
      
			disable:function() {
				self.$wDummReload.show();
				self.$wDummReloadText.show();
				self.$wDummReloadText.height( self.$ns.height() );
			},

			load:function( query ) {
				self.disable();
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, query + '&class=' + self.$class, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else {
            var limit = self.limit;
						var $content = $( data.list );
            console.log($content);
            console.log(self.$ns);
						self.$ns.replaceWith( $content );
						self.init();
            self.limit = limit;

            if (options.afterAjaxUpdate !== undefined && typeof(options.afterAjaxUpdate) == 'function') {
              options.afterAjaxUpdate();
            }
					}
				});
			},
      
      loadMore: function(e) {
        e.preventDefault();
        
        self.limit = self.limit + self.step;
        self.load('limit=' + self.limit);
      }
		};
		self.init();
		return self;
	}
}(window.jQuery);
