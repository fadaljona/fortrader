var wContestInfoListWidgetOpen;
!function( $ ) {
	wContestInfoListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wContestInfoListWidget',
			ajax: false,
			ajaxDeleteURL: '/admin/contest/ajaxDelete',
			ajaxChangeStatusURL: '/admin/contest/ajaxChangeStatus',
			ajaxLoadURL: '/contest/ajaxLoadSingleRow',
			listURL: '/contest/list',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wContestInfo' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				
				self.$gridTable.on( 'click', '.wDelete', self.deleteSpecificRow );
				self.$gridTable.on( 'click', '.wChangeStatusRegistrationStoped', { status: 'registration stoped' }, self.changeStatusSpecificRow );
				self.$gridTable.on( 'click', '.wChangeStatusRegistration', { status: 'registration' }, self.changeStatusSpecificRow );
				self.$gridTable.on( 'click', '.wChangeStatusStarted', { status: 'started' }, self.changeStatusSpecificRow );
				self.$gridTable.on( 'click', '.wChangeStatusCompleted', { status: 'completed' }, self.changeStatusSpecificRow );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idContest="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			update:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idContest="'+id+'"]' );
				if( $row.length ) {
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							var $row = $( data.row );
							$row.replaceAll( $rowLoading );
							self.resetSelection();
							$('[data-rel=tooltip]').tooltip();
						}
					});
				}
			},
			delete:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idContest="'+id+'"]' );
				$row.remove();
				self.issetRemovedRows = true;
				self.hideIfEmpty();
			},
			deleteSpecificRow:function() {
				self.hideDropdowns();
				var $row = $(this).closest( "tr[idContest]" );
				var id = $row.attr( 'idContest' );
				var $row = self.$gridTable.find( '> tbody > tr[idContest="'+id+'"]' );
				if( $row.length ) {
					if( !confirm( options.ls.lDeleteConfirm )) return false;
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							document.location.assign( yiiBaseURL+options.listURL );
						}
					});
				}
				return false;
			},
			changeStatusSpecificRow:function( event ) {
				self.hideDropdowns();
				var $row = $(this).closest( "tr[idContest]" );
				var id = $row.attr( 'idContest' );
				$.getJSON( yiiBaseURL+options.ajaxChangeStatusURL, {id: id, status: event.data.status}, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.update( id );
					}
				});
				return false;
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			updateTooltips:function() {
				self.$ns.find('[data-rel=tooltip]').tooltip();
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );