var wContestMembersListWidgetOpen;
!function( $ ) {
	wContestMembersListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wContestMembersListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wContestMembersGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				self.$selectCountRows = self.$ns.find( options.ins+'.wSelectCountRows' );
				self.$selectAccountNumber = self.$ns.find( options.ins+'.wAccountNumber' );
				
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$gridTable.on( 'click', '.wDelete', self.deleteSpecificRow );
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
				self.$ns.on( 'click', 'th[class*=sorting]', self.changeSorting );
				self.$selectCountRows.change( self.changeCountRows );
				self.$selectAccountNumber.keydown( self.keydownAccountNumber );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			isEmpty:function() {
				if( self.$gridTable.find( '> tbody > tr[data-idMember]' ).length ) return false;
				if( self.$ns.find( '.pagination' ).length ) return false;
				return true;
			},
			hideIfEmpty:function( immediately ) {
				if( self.isEmpty()) self.hide();
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[data-idMember="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			deleteSpecificRow:function() {
				self.hideDropdowns();
				var $row = $(this).closest( "tr[data-idMember]" );
				var id = $row.attr( 'data-idMember' );
				var $row = self.$gridTable.find( '> tbody > tr[data-idMember="'+id+'"]' );
				if( $row.length ) {
					if( !confirm( options.ls.lDeleteConfirm )) return false;
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					$rowLoading.replaceAll( $row );
					$.getJSON( options.ajaxDeleteURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							$rowLoading.remove();
							self.issetRemovedRows = true;
							self.resetSelection();
							self.hideIfEmpty();
						}
					});
				}
				return false;
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			loadPage:function( membersPage ) {
				self.disable();
				var accountNumber = self.$selectAccountNumber.val();
				var data = {id: options.idContest, membersPage:membersPage };
				if( accountNumber ) data.accountNumber = accountNumber;
				data.sortField = options.sortField;
	
				data.sortType = options.sortType;
				$.getJSON( options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
					}
				});
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( $li.hasClass( 'disabled' )) return false;
				if( !$li.hasClass( 'active' ) || self.issetRemovedRows ) {
					var $href = $link.attr( 'href' );
					var matchs = /membersPage=(\d+)/.exec( $href );
					var membersPage = matchs ? matchs[1] : 1;
					self.loadPage( membersPage );
				}
				return false;
			},
			changeCountRows:function() {
				self.hideDropdowns();
				var $select = $(this);
				var countRows = $select.val();
				$.cookie( 'ContestMembersListWidget_countRows', countRows, {path: '/'} );
				self.loadPage( 1 );
			},
			keydownAccountNumber:function( event ) {
				if( event.which == 13 ) {
					self.loadPage( 1 );
				}
			},
			changeSorting:function() {
				if( self.isEmpty() ) return false;
				var $th = $(this);
				options.sortField = $th.attr( 'data-sortfield' );
				options.sortType = $th.hasClass( 'sorting_asc' ) ? 'DESC' : 'ASC';
				self.loadPage( 1 );
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );