var wContestMembersTAListWidgetOpen;
!function( $ ) {
	wContestMembersTAListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wContestMembersTAListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxDeleteURL: '/user/ajaxDeleteContestMember',
			ajaxLoadURL: '/user/ajaxLoadContestMemberRow',
			ajaxLoadListURL: '/user/ajaxLoadContestMembersList',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wContestMembersGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				self.$wDummReload = self.$ns.find( options.ins+'.wDummReload' );
				self.$wDummReloadText = self.$ns.find( options.ins+'.wDummReloadText' );
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$gridTable.on( 'click', '.wDelete', self.deleteSpecificRow );
				self.$gridTable.on( 'click', '.wEdit', self.edit );
				self.$gridTable.on( 'click', 'a', self.onClickLink );
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			isEmpty:function() {
				if( self.$gridTable.find( '> tbody > tr[idMember]' ).length ) return false;
				if( self.$ns.find( '.pagination' ).length ) return false;
				return true;
			},
			hideIfEmpty:function( immediately ) {
				if( self.isEmpty()) self.hide();
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idMember="'+this+'"]' ).addClass( 'selected' ).find( '.select-on-check' ).prop( 'checked', true );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' ).find( '.select-on-check' ).prop( 'checked', false );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			deleteSpecificRow:function() {
				self.hideDropdowns();
				var $row = $(this).closest( "tr[idMember]" );
				var id = $row.attr( 'idMember' );
				var $row = self.$gridTable.find( '> tbody > tr[idMember="'+id+'"]' );
				if( $row.length ) {
					if( !confirm( options.ls.lDeleteConfirm )) return false;
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							$rowLoading.remove();
							self.issetRemovedRows = true;
							self.resetSelection();
							self.selectionChanged();
							self.hideIfEmpty();
						}
					});
				}
				return false;
			},
			disable:function() {
				self.$wDummReload.show();
				self.$wDummReloadText.show();
				self.$wDummReloadText.height( self.$ns.height() );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			updateTooltips:function() {
				self.$ns.find('[data-rel=tooltip]').tooltip();
			},
			loadPage:function( membersPage ) {
				self.disable();
				var data = {idUser: options.idUser, membersPage:membersPage };
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
						self.updateTooltips();
					}
				});
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( $li.hasClass( 'disabled' )) return false;
				if( !$li.hasClass( 'active' ) || self.issetRemovedRows ) {
					var $href = $link.attr( 'href' );
					var matchs = /membersPage=(\d+)/.exec( $href );
					var membersPage = matchs ? matchs[1] : 1;
					self.loadPage( membersPage );
				}
				return false;
			},
			update:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idMember="'+id+'"]' );
				if( $row.length ) {
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							var $row = $( data.row );
							$row.replaceAll( $rowLoading );
							self.resetSelection();
						}
					});
				}
			},
			delete:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idMember="'+id+'"]' );
				$row.remove();
				
				self.hideIfEmpty();
			},
			edit:function() {
				var $link = $(this);
				var href = $link.attr( 'href' );
				if( href == '#' ) {
					self.hideDropdowns();
					var $tr = $(this).closest( 'tr' );
					$tr.trigger( 'click' );
					return false;
				}
			},
			onClickLink:function() {
				var $link = $(this);
				var href = $link.attr( 'href' );
				if( href != '#' ) {
					document.location.assign( href );
					return false;
				}
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );