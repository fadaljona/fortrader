var wUserEAListWidgetOpen;
!function( $ ) {
	wUserEAListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wUserEAListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxDeleteURL: '/admin/ea/ajaxDelete',
			ajaxUserDeleteURL: '/ea/ajaxDelete',
			ajaxLoadListURL: '/user/ajaxLoadEAList',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( '> form' );
				self.$grid = self.$ns.find( options.ins+'.wBrokersGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				//self.$selectCountRows = self.$ns.find( options.ins+'.wSelectCountRows' );
				//self.$selectName = self.$ns.find( options.ins+'.wName' );
				self.$wDummReload = self.$ns.find( options.ins+'.wDummReload' );
				self.$wDummReloadText = self.$ns.find( options.ins+'.wDummReloadText' );
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$gridTable.on( 'click', '.wDelete', self.deleteSpecificRow );
				self.$gridTable.on( 'click', '.wUserDelete', self.userDeleteSpecificRow );
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
				self.$ns.on( 'change', 'form input, form select', self.changeFilter );
				//self.$ns.on( 'click', 'th[class*=sorting]', self.changeSorting );
				//self.$selectCountRows.change( self.changeCountRows );
				//self.$selectName.keydown( self.keydownName );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			isEmpty:function() {
				if( self.$gridTable.find( '> tbody > tr[idEA]' ).length ) return false;
				if( self.$ns.find( '.pagination' ).length ) return false;
				return true;
			},
			hideIfEmpty:function( immediately ) {
				if( self.isEmpty()) self.hide();
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idEA="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			deleteSpecificRow:function() {
				self.hideDropdowns();
				var $row = $(this).closest( "tr[idEA]" );
				var id = $row.attr( 'idEA' );
				var $row = self.$gridTable.find( '> tbody > tr[idEA="'+id+'"]' );
				if( $row.length ) {
					if( !confirm( options.ls.lDeleteConfirm )) return false;
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							$rowLoading.remove();
							self.issetRemovedRows = true;
							self.resetSelection();
							self.hideIfEmpty();
						}
					});
				}
				return false;
			},
			userDeleteSpecificRow:function() {
				self.hideDropdowns();
				var $row = $(this).closest( "tr[idEA]" );
				var id = $row.attr( 'idEA' );
				var $row = self.$gridTable.find( '> tbody > tr[idEA="'+id+'"]' );
				if( $row.length ) {
					if( !confirm( options.ls.lDeleteConfirm )) return false;
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxUserDeleteURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							$rowLoading.remove();
							self.issetRemovedRows = true;
							self.resetSelection();
							self.hideIfEmpty();
						}
					});
				}
				return false;
			},
			disable:function() {
				self.$wDummReload.show();
				self.$wDummReloadText.show();
				self.$wDummReloadText.height( self.$ns.height() );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			updateTooltips:function() {
				self.$ns.find('[data-rel=tooltip]').tooltip();
			},
			load:function( query ) {
				self.disable();
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, query, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
						self.updateTooltips();
					}
				});
			},
			changeFilter:function() {
				var query = self.$form.serialize();
				self.load( query );
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				
				var href = $link.attr( 'href' );
				var query = commonLib.getQueryString( href );
				if( query.indexOf( 'idUser' ) == -1 ) query += '&idUser=' + options.idUser;
				
				self.load( query );
				
				return false;
			},
			/*
			changeCountRows:function() {
				self.hideDropdowns();
				var $select = $(this);
				var countRows = $select.val();
				$.cookie( 'BrokersListWidget_countRows', countRows, {path: '/'} );
				self.loadPage( 1 );
			},
			keydownName:function( event ) {
				if( event.which == 13 ) {
					self.loadPage( 1 );
				}
			},
			changeSorting:function() {
				if( self.isEmpty() ) return false;
				var $th = $(this);
				options.sortField = $th.attr( 'sortField' );
				options.sortType = $th.hasClass( 'sorting_asc' ) ? 'DESC' : 'ASC';
				self.loadPage( 1 );
				return false;
			},
			*/
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );