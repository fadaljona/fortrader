var wCryptoCurrenciesPageListWidgetOpen;
!function( $ ) {
	wCryptoCurrenciesPageListWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wCryptoCurrenciesPageListWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
            loadCryptoExchange: false,
			init:function() {
                self.$ns = $( options.selector );
                
				self.$wTablQuotes = self.$ns.find( '.crypto__table' );

				self.$wWindow = $(window);
				self.$wDocument = $(document);


				self.$wWindow.on('load', function(){
                    self.subscribeToQuotes();
                });

            },
            loadCryptoExchangeList: function(id) {
                if (self.loadCryptoExchange) {
                    return false;
                }
                self.loadCryptoExchange = true;

                $.getJSON( options.loadCryptoExchangeUrl, {id: id}, function (data) {
                    self.loadCryptoExchange = false;
                    if (data.error) {
                        alert( data.error );
                    } else {
                        $('.insCryptoExchangeListWidget').replaceWith(data.content);

                        $(".insCryptoExchangeListWidget .js_scrolling").jScrollPane({
                            autoReinitialise: true
                        });
                        if($(document).width() <= 767) {
                            $(".wide_scroll").removeClass("js_scrolling");
                        }
                    }
                });
                return false;
            },
			subscribeToQuotes:function() {		
				var conn = new ab.Session(options.connUrl,
					function() {
						conn.subscribe(options.quotesKey, function(topic, data) {
							self.updateQuotes(data);
						});
						console.warn('subscribed');
					},
					function() {
						console.warn('WebSocket connection closed');
					},
					{'skipSubprotocolCheck': true}
				);
			},
			updateQuotes:function(data) {
				for(var key in data) {
					var parsedData = JSON.parse(data[key]);
					self.updateQuotesWithAnimate(key, parsedData);
				}
			},
			updateQuotesWithAnimate:function(key, parsedData) {
				var pageBid = $(options.selector+' .pid-' + key + '-bid'),
					pageChg = $(options.selector+' .pid-' + key + '-chg');
					
				var newBid = parseFloat(parsedData.bid),
					precision = pageBid.data('precision'),
					lastBid = pageBid.data('lastBid'),
					oldBid = parseFloat(pageBid.text());
					
				if( !isNaN(lastBid) ){
					var lastBidVal = parseFloat( lastBid );
					var newChgVal = parsedData.bid - lastBidVal;
					var newChgPercentVal = newChgVal / lastBidVal * 100 ;
					var sign = '';
					if( newChgVal > 0 ) sign = '+';
				}

                pageChg.html(sign + newChgPercentVal.toFixed(2) + '%' );
				pageBid.html(parsedData.bid);
				if (parsedData.bid > lastBidVal) {
                    pageBid.removeClass('crypto-pos_fall').addClass('crypto-val_rise_color crypto-val_rise');
                    pageChg.removeClass('crypto-pos_fall').addClass('crypto-pos_rise');
                } else {
                    pageBid.removeClass('crypto-pos_rise').addClass('crypto-val_fall_color crypto-val_fall');
                    pageChg.removeClass('crypto-pos_rise').addClass('crypto-pos_fall');
                }

                setTimeout(function() {
					pageBid.removeClass('crypto-val_rise crypto-val_fall');
				}, 1250);

                if (options.bitcoinKey && key == options.bitcoinKey) {
                    var bitcoinEl = $('.insBitcoinStatsWidget .fx_statistic-item .pid-' + key + '-bid');
                    if (bitcoinEl.length) {
                        bitcoinEl.html(parseFloat(parsedData.bid).toFixed(precision).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                        if (parsedData.bid > lastBidVal) {
                            bitcoinEl.closest('.fx_statistic-item').removeClass('fx_statistic-red').addClass('fx_statistic-green');
                        } else {
                            bitcoinEl.closest('.fx_statistic-item').removeClass('fx_statistic-green').addClass('fx_statistic-red');
                        }
                    }
                }
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );