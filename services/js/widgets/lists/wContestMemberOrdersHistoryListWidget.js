var wContestMemberOrdersHistoryListWidgetOpen;
!function( $ ) {
	wContestMemberOrdersHistoryListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wContestMemberOrdersHistoryListWidget',
			ajax: false,
			hidden: false,
			ajaxLoadListURL: '/contestMember/ajaxLoadMemberOrdersHistoryList',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wContestMemberOrdersHistoryGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				self.$wDummReload = self.$ns.find( options.ins+'.wDummReload' );
				self.$wDummReloadText = self.$ns.find( options.ins+'.wDummReloadText' );
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idOrder="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			disable:function() {
				self.$wDummReload.show();
				self.$wDummReloadText.show();
				self.$wDummReloadText.height( self.$ns.height() );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			updateTooltips:function() {
				self.$ns.find('[data-rel=tooltip]').tooltip();
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( !$li.hasClass( 'disabled' ) && !$li.hasClass( 'active' ) ) {
					var $href = $link.attr( 'href' );
					var matchs = /ordersHistoryPage=(\d+)/.exec( $href );
					var ordersHistoryPage = matchs ? matchs[1] : 1;
					self.disable();
					$.getJSON( yiiBaseURL+options.ajaxLoadListURL, {id:options.idMember, ordersHistoryPage:ordersHistoryPage}, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $content = $( data.list );
							self.$ns.replaceWith( $content );
							self.init();
							self.updateTooltips();
						}
					});
				}
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );