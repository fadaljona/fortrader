var wCurrentUserContestMemberInfoListWidgetOpen;
!function( $ ) {
	wCurrentUserContestMemberInfoListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wCurrentUserContestMemberInfo',
			ajax: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				self.$ns = $( options.selector );
				
				self.editLink = self.$ns.find( '.fa.fa-pencil-square-o' );
				self.deleteLink = self.$ns.find( '.fa.fa-times' );
				
				self.editLink.css({'cursor':'pointer'});
				self.deleteLink.css({'cursor':'pointer'});
				
				self.editLink.click( self.edit ); 
				self.deleteLink.click( self.deleteMember ); 
				
			},
			showWidget: function(){
				self.$ns.show(500);
			},
			updateContent:function( content ){
				self.$ns.html(content);
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			deleteMember:function() {
				
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				$.getJSON( options.ajaxDeleteURL, {}, function ( data ) {
					if( data.error ) {
						alert( data.error );
						self.$ns.find('.spinner-wrap').remove();
					}
					else{
						self.hide(true);
						location.reload();
					}
				});
				return false;
			},

			edit:function() {
				options.ns.wRegistrationContestMemberWizardWidget.showWidget();
				self.$ns.hide(500);
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );