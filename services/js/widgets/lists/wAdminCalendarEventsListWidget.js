var wAdminCalendarEventsListWidgetOpen;
!function( $ ) {
	wAdminCalendarEventsListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminCalendarEventsListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxLoadURL: '/admin/calendarEvent/ajaxLoadRow',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wCalendarEventsGridView' );
				self.$gridTable = self.$grid.find( '> form > table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				self.$wName = self.$ns.find( '.wName' );
				self.$wForm = self.$ns.find( 'form' );
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				commonLib.initDatepicker();
				self.$wInputDate = self.$grid.find( 'input[name="CalendarEventModel[dateForSearch]"]' );
				self.$wInputDate.datepicker();
				self.$wInputDate.change( function() { $(this).closest( 'form' ).submit(); } );
				
				self.$grid.on( 'click', '.wLetter', self.changeLetter );
				self.$grid.on( 'click', '.wLetterClear', self.clearLetter );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idEvent="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			hideEmptyRow:function() {
				self.$gridTable.find( 'td.empty' ).closest( 'tr' ).hide();
			},
			add:function( id ) {
				self.show();
				var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
				self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
				$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
					if( data.error ) {
						alert( data.error );
						$rowLoading.remove();
					}
					else{
						var $row = $( data.row );
						$row.replaceAll( $rowLoading );
						self.resetSelection();
						self.hideEmptyRow();
					}
				});
			},
			update:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idEvent="'+id+'"]' );
				if( $row.length ) {
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							var $row = $( data.row );
							$row.replaceAll( $rowLoading );
							self.resetSelection();
						}
					});
				}
			},
			delete:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idEvent="'+id+'"]' );
				$row.remove();
				
				if( !self.$gridTable.find( '> tbody > tr[idEvent]' ).length ) {
					self.hide();
				}
			},
			changeLetter:function() {
				var $letter = $(this);
				var letter = $letter.attr( 'data-letter' );
				
				self.$wName.val( letter + '*' );
				self.$wForm.submit();
				
				return false;
			},
			clearLetter:function() {
				self.$wName.val( '' );
				self.$wForm.submit();
				
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );