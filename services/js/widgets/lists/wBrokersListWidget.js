var wBrokersListWidgetOpen;
!function( $ ) {
	wBrokersListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wBrokersListWidget',
			ajax: false,
			hidden: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wBrokersGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				
				self.$selectCountRows = $( options.nsText + ' .wSelectCountRows' );
		
				self.$wDummReload = self.$ns.find( '.spinner-wrap' );
							
				if( options.hidden ) {
					self.hide( true );
				}	
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
				self.$ns.on( 'click', 'th[class*=sorting]', self.changeSorting );
				self.$selectCountRows.change( self.changeCountRows );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			isEmpty:function() {
				if( self.$gridTable.find( '> tbody > tr[data-idBroker]' ).length ) return false;
				if( self.$ns.find( '.pagination' ).length ) return false;
				return true;
			},
			hideIfEmpty:function( immediately ) {
				if( self.isEmpty()) self.hide();
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[data-idBroker="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},

			disable:function() {
				self.$wDummReload.removeClass('hide');
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			loadPage:function( brokersPage ) {
				self.disable();
			
				var data = { brokersPage:brokersPage };

				data.sortField = options.sortField;
				data.sortType = options.sortType;
				data.type = options.type;
				$.getJSON( options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
					}
				});
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( $li.hasClass( 'disabled' )) return false;
				if( !$li.hasClass( 'active' ) || self.issetRemovedRows ) {
					var $href = $link.attr( 'href' );
					var matchs = /brokersPage=(\d+)/.exec( $href );
					var brokersPage = matchs ? matchs[1] : 1;
					self.loadPage( brokersPage );
				}
				return false;
			},
			changeCountRows:function() {
				self.hideDropdowns();
				var $select = $(this);
				var countRows = $select.val();
				
				var expires;
				var date = new Date();
				date.setTime(date.getTime() + (100000 * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();	
				document.cookie = options.cookieRowsCount + "=" + countRows + expires + "; path=/";

				self.loadPage( 1 );
			},

			changeSorting:function() {
				if( self.isEmpty() ) return false;
				var $th = $(this);
				options.sortField = $th.attr( 'data-sortField' );
				options.sortType = $th.hasClass( 'sorting_asc' ) ? 'DESC' : 'ASC';
				self.loadPage( 1 );
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );