var wUsersListWidgetOpen;
!function( $ ) {
	wUsersListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wUsersListWidget',
			ajaxLoadListURL: '/user/ajaxLoadList',
			profileTraderURL: '/user/settings?tabProfileTrader=yes',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
				
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$wTraderAlert = self.$ns.find( options.ins+'.wTraderAlert' );
				self.$form = self.$ns.find( '> form' );
				self.$grid = self.$ns.find( options.ins+'.wUsersListWidget' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$wDummReload = self.$ns.find( options.ins+'.wDummReload' );
				self.$wDummReloadText = self.$ns.find( options.ins+'.wDummReloadText' );
				self.$selectName = self.$ns.find( options.ins+'.wName' );
				
				self.$wTraderAlert.on( 'click', '.wRedirect', self.redirectProfileTrader );
				self.$wTraderAlert.on( 'click', '.wCancel', self.cancelProfileTrader );
				self.$ns.on( 'click', '.pagination a', self.changePage );
				self.$ns.on( 'change', 'form input, form select', self.changeFilter );
				self.$selectName.keydown( self.keydownName );
			},
			redirectProfileTrader:function() {
				document.location.assign( yiiBaseURL+options.profileTraderURL );
			},
			cancelProfileTrader:function() {
				self.$wTraderAlert.hide();
				$.cookie( 'dontShowTraderProfileAlert', 'on', {path:'/', expires: 365} );
			},
			disable:function() {
				self.$wDummReload.show();
				self.$wDummReloadText.show();
				self.$wDummReloadText.height( self.$ns.height() );
			},
			load:function( query ) {
				self.disable();
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, query, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
						$.scrollTo( self.$ns, 500, { offset: -60 } );
					}
				});
			},
			keydownName:function( event ) {
				var query = self.$form.serialize();
				if( event.which == 13 ) {
					self.load( query );
					return false;
				}
			},
			changeFilter:function() {
				var query = self.$form.serialize();
				self.load( query );
			},
			changePage:function() {
				var $link = $(this);
				
				var href = $link.attr( 'href' );
				var query = commonLib.getQueryString( href );
				
				self.load( query );
				
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );