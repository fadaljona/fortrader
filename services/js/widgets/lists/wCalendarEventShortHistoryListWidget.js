var wCalendarEventShortHistoryListWidgetOpen;
!function( $ ) {
	wCalendarEventShortHistoryListWidgetOpen = function( options ) {
		var defOption = {

		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			blocker: '<div class="blocker" style="position:absolute;width:100%;height:100%;z-index:5;top:0;left:0;background:rgba(241, 241, 241, 0.5) none repeat scroll 0 0;"></div>',
			init:function() {
				self.$ns = $( options.selector );
				self.$showMoreBtn = self.$ns.find('.description_history_btn');
				self.$tbody = self.$ns.find('table tbody');
				self.$showMoreBtn.click( self.loadHistory );
			},
			disable: function(){
				self.$ns.append(self.blocker);
			},
			enable: function(){
				self.$ns.find('.blocker').remove();
			},
			loadHistory: function(){
				if( self.loading ) return false;
				self.loading = true;
				self.disable();
				var lastTr = self.$tbody.find('tr').last();
				
				$.getJSON( options.loadUrl, {id:lastTr.attr('data-id'), dt: lastTr.attr('data-dt')}, function ( data ) {
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.$tbody.append( data.list );
							if( data.count < options.pageSize ) self.$showMoreBtn.remove();
						}
						self.loading = false;
						self.enable();
					});
				return false;
			}
		};
		self.init();
		return self;
	}
}( window.jQuery );