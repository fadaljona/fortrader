var wCategoryRatingWithFilterWidgetOpen;
!function( $ ) {
	wCategoryRatingWithFilterWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wCategoryRatingWithFilterWidget',
			ajax: false,
			hidden: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			selectedCats: false,
			searchFormUsed: false,
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
				
				self.selectedCats = self.getSelectedCats();
				
				self.$serchForm = self.$ns.find('.quotes_search_box form');
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
				self.$ns.on( 'click', 'th[class*=sorting]', self.changeSorting );
				self.$serchForm.submit( self.serchTermEntered );
				
				self.optionsButtonClick();
				self.closeOptions();
				
			},
			serchTermEntered: function(){
				if( self.searchFormUsed ){
					self.loadPage( 1 );
					return false;
				}
				if( self.$serchForm.find('input').val() ){
					self.searchFormUsed = true;
					self.loadPage( 1 );
				}
				return false;
			},
			getSelectedCats: function(){
				if( self.$ns.find('.options_box.customBtnHandler').length ){
					var selectedCats = '';
					self.$ns.find('.options_box.customBtnHandler input[type="checkbox"]:checked').each(function(){
						if( selectedCats != '' ){
							selectedCats = selectedCats + ',' + $(this).val();
						}else{
							selectedCats = $(this).val();
						}
					});
					
					if( selectedCats != '' ){
						return selectedCats;
					}

				}
				return false;
			},
			optionsButtonClick:function() {
				self.$ns.find('.turn_box .options_btn.customBtnHandler').click(function(e){
			
					e.preventDefault();
					var $this = $(this),
						parent = $this.closest(".turn_box");
						
					var marker = 1;
					if( $this.hasClass("active") ) marker = 0;
					if( marker ){
						self.$ns.find('.options_box.customBtnHandler').removeClass('opened');
						self.$ns.find('.options_btn.customBtnHandler').removeClass('active');	
					}else{
						if( self.getSelectedCats() != self.selectedCats ){
							self.selectedCats = self.getSelectedCats();
							self.loadPage( 1 );
						}
					}
					$this.toggleClass("active");
					parent.find('.options_box').toggleClass("opened");	
				});
			},
			closeOptions:function() {
				$(document).click(function(event){
					if(!$(event.target).closest('.options_box.customBtnHandler,.options_btn.customBtnHandler').length){
						if( self.$ns.find('.options_box.customBtnHandler').hasClass('opened') &&  self.$ns.find('.options_btn.customBtnHandler').hasClass('active') ){
							if( self.getSelectedCats() != self.selectedCats ){
								self.selectedCats = self.getSelectedCats();
								self.loadPage( 1 );
							}
						}
						self.$ns.find('.options_box.customBtnHandler').removeClass('opened');
						self.$ns.find('.options_btn.customBtnHandler').removeClass('active');
					}
				});
			},
	
			disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			loadPage:function( pageNum ) {
				self.disable();
			
				var data = { categoryRatingPage:pageNum, ratingId:options.ratingId };

				data.sortField = options.sortField;
				data.sortType = options.sortType;
				data.type = options.type;
				data.selectedCats = self.getSelectedCats();
				data.searchTerm = self.$serchForm.find('input').val();
				$.getJSON( options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						
					/*execute js in $content*/
						var el = document.getElementById( options.insid ); 
						var scripts = el.getElementsByTagName('script'); 

						for (var i = 0; i < scripts.length; i++) { 
							var parentDiv = scripts[i].parentNode;
							var markerEl = document.createElement('div');
							var dateId = new Date(); var markerElId = dateId.getTime();
							markerEl.id = markerElId;
							parentDiv.insertBefore(markerEl, scripts[i]);
									
							var scriptAtts = scripts[i].attributes;
							var scriptForReplace = document.createElement('script');
									
							for (var j = 0; j < scriptAtts.length; j++){
								var attVal = scriptAtts[j].value;
								if( attVal == '' ) attVal = true;
								scriptForReplace.setAttribute(scriptAtts[j].nodeName,attVal);
							}
							if( scripts[i].innerHTML != '' ){ scriptForReplace.innerHTML = scripts[i].innerHTML; }
									
							parentDiv.removeChild(scripts[i]);
							parentDiv.insertBefore(scriptForReplace, markerEl);
							parentDiv.removeChild( document.getElementById(markerElId) );	
						} 
						
						
						self.init();
					}
				});
			},
			changePage:function() {
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( $li.hasClass( 'disabled' )) return false;
				if( !$li.hasClass( 'active' ) ) {
					var $href = $link.attr( 'href' );
					var matchs = /categoryRatingPage=(\d+)/.exec( $href );
					var pageNum = matchs ? matchs[1] : 1;
					self.loadPage( pageNum );
				}
				return false;
			},
			onChangeMark:function( value, selector ) {
				var data = {
					postId: selector.replace('row-id', ''),
					value: value,
				};
		
				$.getJSON( options.ajaxAddMarkURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
				});
			},

			changeSorting:function() {
				var $th = $(this);
				options.sortField = $th.attr( 'data-sortField' );
				options.sortType = $th.hasClass( 'sorting_asc' ) ? 'DESC' : 'ASC';
				self.loadPage( 1 );
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );