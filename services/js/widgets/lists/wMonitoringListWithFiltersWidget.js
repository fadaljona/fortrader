var wMonitoringListWithFiltersWidgetOpen;
!function( $ ) {
	wMonitoringListWithFiltersWidgetOpen = function( options ) {
		var defLS = {
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wMonitoringListWithFiltersWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			mode: 'loadMore',
			sortField: false,
			sortType: false,
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
				self.$loadMoreBtn = self.$ns.find('.load_btn');
				self.$tbody = self.$ns.find('table tbody');
				self.$accountPeriod = self.$ns.find('.accountPeriod');
				self.$sortType = self.$ns.find('.sortType');
				self.$secondarySortType = self.$ns.find('.secondarySortType');
				self.$accountSearchInput = self.$ns.find('.accountSearchInput');
				self.$sortingByTabs = self.$ns.find('.sortingByTabs');
				self.$negativFilter = self.$ns.find('.negativFilter');
				
				self.prevPeriodVal = self.$accountPeriod.val();
				self.prevSortTypeVal = self.$sortType.val();
				self.prevSecondarySortTypeVal = self.$secondarySortType.val();
				
				self.disableSortingOptions();
				self.initTabsSorting();
				
				self.initSelectStyler();
				self.initPeriodSelectStyler();
				self.initFirstSortSelectStyler();
				self.initSecondSortSelectStyler();
				self.$ns.find('.monitoring_btn_sort').click( self.clickedOrderBtn );
				self.$ns.find('.accountTypeBtns button').click( self.clickedOrderBtn );
				self.$ns.find('.accountSearchForm').submit( self.searchFormSubmit );
				self.$ns.find('.clearAccountSearchInput').click( self.clearAccountSearchInput );
				self.$sortingByTabs.find('li').click( self.sortingTabsClicked );
				self.$loadMoreBtn.click( self.loadList );
				self.$negativFilter.change( self.loadList );
				self.$ns.on( 'click', 'th[class*=sorting]', self.changeSorting );
			},
			changeSorting:function() {
				var $th = $(this);
				self.sortType = $th.attr( 'data-sortField' );
				self.sortTypeOrder = $th.hasClass( 'sorting_asc' ) ? 'DESC' : 'ASC';

				self.$sortingByTabs.find('li').removeClass('active');
				if( self.$sortingByTabs.find('li[data-type="'+self.sortType+'"]').length ){
					self.$sortingByTabs.find('li[data-type="'+self.sortType+'"]').addClass('active');
				}else{
					self.$sortingByTabs.find('li.ortherSorting').addClass('active');
					self.$sortingByTabs.find('li.ortherSorting').attr({'data-type':self.sortType});
					self.$sortingByTabs.find('li.ortherSorting a').text( $th.find('span').text() );
				}
				
				self.loadList();
				return false;
			},
			initTabsSorting: function(){
				var sortTypeVal = options.sortType;
				self.$sortingByTabs.find('li').removeClass('active');
				if( self.$sortingByTabs.find('li[data-type="'+sortTypeVal+'"]').length ){
					
					self.$sortingByTabs.find('li[data-type="'+sortTypeVal+'"]').addClass('active');
				}else{
					self.$sortingByTabs.find('li.ortherSorting').addClass('active');
					self.$sortingByTabs.find('li.ortherSorting').attr({'data-type':sortTypeVal});
					self.$sortingByTabs.find('li.ortherSorting a').text( self.$sortType.find('option[value="'+sortTypeVal+'"]').text() );
				}
			},
			sortingTabsClicked: function(){
				if( $(this).hasClass('active') ) return false;
				
				self.$sortingByTabs.find('li').removeClass('active');
				$(this).addClass('active');
			
				self.sortType = $(this).attr('data-type');
				self.sortTypeOrder = 'ASC';
				
				self.loadList();
				return false;
			},
			disableSortingOptions: function(){
				var sortTypeVal = self.$sortType.val();
				var secondarySortTypeVal = self.$secondarySortType.val();
				
				if( sortTypeVal === null ){
					self.$secondarySortType.val( self.$secondarySortType.find('option[value="' +secondarySortTypeVal+ '"]').next().attr('value') );
					secondarySortTypeVal = self.$secondarySortType.val();
				}

				
				
				self.$secondarySortType.find('option').attr('disabled', false);
				self.$sortType.find('option').attr('disabled', false);
				
				self.$secondarySortType.find('option[value="' +sortTypeVal+ '"]').attr('disabled', true);
				self.$sortType.find('option[value="' +secondarySortTypeVal+ '"]').attr('disabled', true);
			},
			refreshSortingSelects: function(){
				self.$secondarySortType.trigger('refresh');
				self.$sortType.trigger('refresh');
			},
			clearAccountSearchInput: function(){
				if( self.$accountSearchInput.val() == '' ) return false;
				self.$accountSearchInput.val('');
				self.loadList();
				return false;
			},
			searchFormSubmit: function(){
				if( self.$accountSearchInput.val() == '' ) return false;
				self.loadList();
				return false;
			},
			clickedOrderBtn: function(){
				if( $(this).hasClass('active') ) return false;
				$(this).closest('div').find('button').removeClass('active');
				$(this).addClass('active');
				self.loadList();
			},
			initSelectStyler: function(){
				var $selects = self.$ns.find(".hzfilter");
				if( $selects.length ){
					$selects.styler({
						onSelectClosed: function() {
							var $this = $(this);
						//	self.loadList();
						}
					});
				}
			},
			initPeriodSelectStyler: function(){
				var $selects = self.$ns.find(".accountPeriod");
				if( $selects.length ){
					$selects.styler({
						onSelectClosed: function() {
							if( self.$accountPeriod.val() != self.prevPeriodVal ){
								self.prevPeriodVal = self.$accountPeriod.val();
								self.loadList();
							}
						}
					});
				}
			},
			initFirstSortSelectStyler: function(){
				var $selects = self.$ns.find(".sortType");
				if( $selects.length ){
					$selects.styler({
						onSelectClosed: function() {
							if( self.$sortType.val() == self.prevSortTypeVal ) return false;
							self.prevSortTypeVal = self.$sortType.val();
							
							self.initTabsSorting();
							self.disableSortingOptions();
							self.refreshSortingSelects();
							self.loadList();
						}
					});
				}
			},
			initSecondSortSelectStyler: function(){
				var $selects = self.$ns.find(".secondarySortType");
				if( $selects.length ){
					$selects.styler({
						onSelectClosed: function() {
							if( self.$secondarySortType.val() == self.prevSecondarySortTypeVal ) return false;
							self.prevSecondarySortTypeVal = self.$secondarySortType.val();
							
							self.disableSortingOptions();
							self.refreshSortingSelects();
							self.loadList();
						}
					});
				}
			},
			getSortTypeOrder: function(){
				return self.$ns.find('.sortTypeOrder button.active').attr('data-type');
			},
			getSecondarySortTypeOrder: function(){
				return self.$ns.find('.secondarySortTypeOrder button.active').attr('data-type');
			},
			getAccountType: function(){
				return self.$ns.find('.accountTypeBtns button.active').attr('data-type');
			},
			loadList: function(){	
				var data = { monitoringAccountsListPage:0, pagesCount:1 };
				data.period = self.$accountPeriod.val();
				data.sortType = self.sortType;
				data.sortTypeOrder = self.sortTypeOrder;
				/*data.secondarySortType = self.$secondarySortType.val();
				data.secondarySortTypeOrder = self.getSecondarySortTypeOrder();*/
				data.accountSearchInput = self.$accountSearchInput.val();
				if( self.$negativFilter.is(":checked") ){
					data.negativFilter = 1;
				}else{
					data.negativFilter = 0;
				}
				data.accountType = self.getAccountType();
					
				if( $(this).hasClass('load_btn') ){
					data.monitoringAccountsListPage = parseInt($(this).attr('data-page')) + 1;
					self.mode = 'loadMore';
				}else{
					self.mode = 'replace';
					if( self.$loadMoreBtn.length ) data.pagesCount = self.$loadMoreBtn.attr('data-page');
				}
		
				self.disable();
				$.getJSON( options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						self.enable();
						alert( data.error );
					}
					else{
	
						if( self.sortTypeOrder == 'DESC' ){
							self.$ns.find('th[data-sortField="'+self.sortType+'"]').removeClass('sorting_asc').addClass('sorting_desc');
						}else if( self.sortTypeOrder == 'ASC' ){
							self.$ns.find('th[data-sortField="'+self.sortType+'"]').removeClass('sorting_desc').addClass('sorting_asc');
						}
						
						
						self.enable();
						var dataCount = Math.floor(parseInt(data.itemsCount) / options.pageSize);
						if( parseInt(data.itemsCount) / options.pageSize > dataCount ) dataCount = dataCount+1;
						self.$loadMoreBtn.attr({'data-count': dataCount});
						
						if( self.mode == 'loadMore' ){
							self.$tbody.append( data.list );
							var nextPage = parseInt(self.$loadMoreBtn.attr('data-page')) + 1;
							self.$loadMoreBtn.attr({
								'data-page': nextPage
							});
							if( nextPage == parseInt(self.$loadMoreBtn.attr('data-count')) ){
								self.$loadMoreBtn.addClass('hide');
							}
						}else if( self.mode == 'replace' ){
							self.$tbody.html( data.list );
							if( parseInt(self.$loadMoreBtn.attr('data-page')) >= parseInt(self.$loadMoreBtn.attr('data-count')) ){
								self.$loadMoreBtn.addClass('hide');
							}else{
								self.$loadMoreBtn.removeClass('hide');
							}
						}
					}
				});
				
				return false;
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find('.spinner-wrap').remove();
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );