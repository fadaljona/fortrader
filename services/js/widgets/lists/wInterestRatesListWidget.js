var wInterestRatesListWidgetOpen;
!function( $ ) {
	wInterestRatesListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wInterestRatesListWidget',
			ajax: false,
			hidden: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wInterestRatesGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			
			disable:function() {
				self.$ns.append( self.spinner );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( !$li.hasClass( 'disabled' ) && !$li.hasClass( 'active' ) ) {
					var $href = $link.attr( 'href' );
					var matchs = /interestRatePage=(\d+)/.exec( $href );
					var interestRatePage = matchs ? matchs[1] : 1;
					self.disable();
					$.getJSON( options.ajaxLoadListURL, {interestRatePage:interestRatePage}, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $content = $( data.list );
							self.$ns.replaceWith( $content );
							self.init();
						}
					});
				}
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );