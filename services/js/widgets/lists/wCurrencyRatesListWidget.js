var wCurrencyRatesListWidgetOpen;
!function( $ ) {
	wCurrencyRatesListWidgetOpen = function( options ) {
		var defOption = {
			ajax: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			issetRemovedRows: false,
			prevToCurrencyVal: false,
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				self.$currencyList = self.$ns.find('.tabl_quotes.table_currency');
				
				
				self.$toCurrency = self.$ns.find( 'select.currency_filter_change_value' );
				
				self.initToCurrencySelect();
				
				
			},
			initToCurrencySelect: function(){
				self.prevToCurrencyVal = self.$toCurrency.val();
				self.$toCurrency.styler({
					onSelectClosed: function() {
						if( self.$toCurrency.val() != self.prevToCurrencyVal && self.checkIfDataWasPrinted() ){
							self.prevToCurrencyVal = self.$toCurrency.val();
							self.changedToCurrency();
						}
					}
				});
			},
			checkIfDataWasPrinted: function(){
				if( self.$currencyList.find('td.empty').length ) return false;
				return true;
			},
			changedToCurrency: function(){
				self.disable();
				$.getJSON( options.ajaxLoadListURL, {type: options.type, dataType: options.dataType, toCurrency: self.$toCurrency.val() }, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$currencyList.html( data.content );
					}
					self.enable();
				});
				return false;
			},
			disable: function(){
				self.$ns.append( self.spinner );
			},
			enable: function(){
				self.$ns.find('.spinner-wrap').remove();
			},


		};
		self.init();
		return self;
	}
}( window.jQuery );