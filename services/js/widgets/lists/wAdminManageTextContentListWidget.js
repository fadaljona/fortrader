var wAdminManageTextContentListWidgetOpen;
!function( $ ) {
	wAdminManageTextContentListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$ns.find('input[type="text"],textarea').on('focusout', function() { self.inputChanged( $(this) ); });
				self.$ns.find('.wAdminManageTextContentForm').on('submit', function() { self.inputChanged( $(this) ); return false; });
			},
			inputChanged: function( el ){
				self.submit( el );
				return false;
			},
			disable: function(el){
				el.closest('form').find('.wSubmit').attr( 'disabled', 'disabled' );
			},
			submit:function( el ) {
				if( self.loading ) return false;
				
				var $form = el.closest('form');
				var $submitBtn = $form.find('.wSubmit');
				$submitBtn.attr( 'disabled', 'disabled' );
				var lSubmit = $submitBtn.html();
				$submitBtn.html( options.ls.lSaving );
				self.loading = true;
					
				$.post( options.saveUrl, $form.serialize(), function ( data ) {
					self.loading = false;
					$submitBtn.html( lSubmit );
					$submitBtn.removeAttr( 'disabled' );
					if( data.error ) {
						alert( data.error );
					}
				}, "json" );
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );