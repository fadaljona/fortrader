var wChooseExchangerByDirectionWidgetOpen;
!function( $ ) {
	wChooseExchangerByDirectionWidgetOpen = function( options ) {
		var defLS = {
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
                self.$ns = $( options.selector );
                self.$fromCurrencyFilter = self.$ns.find('.fromCurrencyFilter');
                self.$toCurrencyFilter = self.$ns.find('.toCurrencyFilter');

				self.$ns.on( 'click', '.pagination a', self.changePage );
                self.$fromCurrencyFilter.find('.exchange__item').click(self.clickedFilter);
                self.$toCurrencyFilter.find('.exchange__item').click(self.clickedFilter);
                
            },
            clickedFilter: function() {
                if ($(this).hasClass('exchange-item_disable') || $(this).hasClass('exchange-item_active')) {
                    return false;
                }
                $(this).closest('.eCurrencyFilter').find('.exchange__item').removeClass('exchange-item_active');
                $(this).addClass('exchange-item_active');
                self.loadPage(1);
            },
            loadPage:function( pageNum ) {
				self.disable();
                var data = { selectBestCoursePage:pageNum };
                
                if (self.$fromCurrencyFilter.find('.exchange-item_active').length) {
                    data.fromCurrency = self.$fromCurrencyFilter.find('.exchange-item_active').attr('data-id');
                }
                if (self.$toCurrencyFilter.find('.exchange-item_active').length) {
                    data.toCurrency = self.$toCurrencyFilter.find('.exchange-item_active').attr('data-id');
                }

				$.getJSON( options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
                        self.$ns.find('.wExchangersWithCoursesAndReservesGridView').replaceWith($content);

						self.enable();
					}
				});
			},
			changePage:function() {
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( $li.hasClass( 'disabled' )) return false;
				if( !$li.hasClass( 'active' ) ) {
					var $href = $link.attr( 'href' );
					var matchs = /selectBestCoursePage=(\d+)/.exec( $href );
					var pageNum = matchs ? matchs[1] : 1;
					self.loadPage( pageNum );
				}
				return false;
            },
            disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			onChangeMark:function( value, selector ) {;
                if (self.$ns.find('.' + selector + ' .insWebRatingWidget').length > 1) {
                    self.$ns.find('.' + selector + ' .insWebRatingWidget').each(function(){
                        $(this).rateYo("silentRating", value);
                    });
                }
                self.$ns.find('.' + selector + ' .exchangerVoteCount').text(value);
                
				var data = {
					idExchanger: selector.replace('data-exchanger', ''),
					value: value,
				};
		
				$.getJSON( options.ajaxAddMarkURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
				});
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );