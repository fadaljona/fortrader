var wQuotesChartListWidgetOpen;
!function( $ ) {
	wQuotesChartListWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wQuotesChartListWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$wCheckedQuotes = self.getCheckedQuotes();
				self.$wOptionCheckbox = self.$ns.find( '.options_box .options_item input:checkbox' );
				self.$wOptionCheckbox.click( self.changeChartsList );

			},
			changeChartsList:function() {
				self.$wOptionCheckbox.prop('disabled', true);
				
				var clickedCheckBox = $(this);
				self.$wCheckedQuotes = self.getCheckedQuotes();
					
				var turnContent = clickedCheckBox.closest('section.section_offset.turn_box').find('.turn_content');
				
				if( clickedCheckBox.prop('checked') ){
					
					self.loadOneChart(turnContent, clickedCheckBox.data('id'));
					
					//turnContent.append('<div class="graph_inner">new graph</div>');
	
				}else{
					var elToRemove = turnContent.find('.graph_inner[data-id='+clickedCheckBox.data('id')+']');
					elToRemove.remove();
					self.$wOptionCheckbox.prop('disabled', false);
				}
				
				var quotesStr = '';
				self.$wCheckedQuotes.each(function(index, element){
					var delimiter = '';
					if( index > 0 ) delimiter = ',';
					quotesStr = quotesStr + delimiter + $(this).val();
				});
				self.setCheckedQuotesToCookie( quotesStr );
			},
			loadOneChart:function( parentEl, symbol ) {
				if( self.loading ) return false;
				self.loading = true;
				$.post( options.ajaxSubmitURL, {"symbol": symbol}, function ( data ) {
					if( data == '' ){
						alert('Error');
						self.loading = false;
						return false;
					}
					parentEl.append(data);
					self.$wOptionCheckbox.prop('disabled', false);
					self.loading = false;
				});
			},
			setCheckedQuotesToCookie:function(value) {
				var expires;
				var date = new Date();
				date.setTime(date.getTime() + (100000 * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();	
				var name = 'checkedQuotesOnChartsList' + options.lang;
				document.cookie = name + "=" + value + expires + "; path=/";
			},
			getCheckedQuotes:function() {
				return self.$ns.find( '.options_box .options_item input:checkbox:checked' );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );