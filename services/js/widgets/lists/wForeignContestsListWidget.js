var wForeignContestsListWidgetOpen;
!function( $ ) {
	wForeignContestsListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wForeignContestsListWidget',
			ajax: false,
			hidden: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				
				self.$grid = self.$ns.find( options.ins+'.wForeignContestsGridView' );
				self.$gridTable = self.$grid.find( '> table' );

				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
			
				self.$wLoadBtn = self.$ns.find( '.download_more' );
				self.$wLoadBtn.click( self.loadNextPage );
				
				self.$ns.find('.add_btn').click( self.scrollToForm );
				
			},
			scrollToForm:function( ) {
				$('html, body').animate({
					scrollTop: $('#addFrontForm').offset().top
				}, 'slow');
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			
			disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find( '.spinner-wrap' ).remove();
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			loadNextPage:function(){
				var $this = $(this);
				var page = $this.attr('data-page');
				
				var $wTabbleBody = self.$ns.find( 'table tbody' );
				
				self.disable();
				$.getJSON( options.ajaxLoadListURL, {page: page }, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						$wTabbleBody.append( data.rows );
						var nextPage = parseFloat( page ) + 1;
						$this.attr({'data-page': nextPage});
						if( options.pagesCount == nextPage ) $this.remove();
						self.enable();
					}
				});
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );