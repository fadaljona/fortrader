var wYourMonitoringAccountsListWidgetOpen;
!function( $ ) {
	wYourMonitoringAccountsListWidgetOpen = function( options ) {
		var defLS = {
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wYourMonitoringAccountsListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wContestMembersGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$registerFormWidget = options.ns.wRegistrationContestMemberWizardWidget;
				self.$registerFormBoxButton = self.$registerFormWidget.$ns.find('.regestration_box_button');
				self.$registerFormWidget.$ns.on( 'click', '.cancelEditAccount', self.returnRegisterFormToPreviousState );
				self.registerFormPrevMode = self.$registerFormWidget.mode;
				
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$gridTable.on( 'click', '.fa.fa-times', self.deleteSpecificRow );
				self.$gridTable.on( 'click', '.fa.fa-pencil-square-o', self.editAccount );
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
				
				self.$ns.on( 'click', '.linkForForumFrame', self.useAccountForThread );
			},
			scrollToRegisterForm:function(){
				var targetOffset = self.$registerFormWidget.$ns.offset().top;
				$('html,body').animate({scrollTop: targetOffset}, 500);
				return false;		
			},
			scrollToAccountsTable:function(){
				var targetOffset = self.$grid.offset().top;
				$('html,body').animate({scrollTop: targetOffset}, 500);
				return false;		
			},
			editAccount:function(){
				var $row = $(this).closest( "tr[data-id]" );
				var account = $row.attr('data-account');
				var accountTitle = $row.attr('data-accountTitle');
				var server = $row.attr('data-server');
				var rowId = $row.attr('data-id');
				
				if( self.$registerFormWidget.mode == 'editRegister' && self.$registerFormWidget.$ns.find('#AccountContestMemberFormModel_id').val() == rowId ){
					return false;
				}else{
					self.returnRegisterFormToPreviousState();
				}
				
				self.$registerFormWidget.$ns.find('#AccountContestMemberFormModel_accountNumber').val(account);
				self.$registerFormWidget.$ns.find('#AccountContestMemberFormModel_id').val(rowId);
				self.$registerFormWidget.$ns.find('#AccountContestMemberFormModel_accountTitle').val(accountTitle);
				self.$registerFormWidget.$ns.find('#AccountContestMemberFormModel_idServer').val(server).trigger('refresh');
				self.$registerFormWidget.$ns.find('input[type="button"]').val(options.ls.lEditBtnText);
				self.$registerFormWidget.$ns.attr({'data-id': rowId});
				self.scrollToRegisterForm();
				self.$registerFormWidget.mode = 'editRegister';
				self.$registerFormWidget.type = 'monitoring';
				self.$registerFormBoxButton.append('<input type="button" class="regestration_btn cancelEditAccount alignleft wSubmit" name="yt0" value="'+options.ls.lCancelBtnText+'">');
			},
			updateRow: function(rowId){
				$.getJSON( options.ajaxLoadOneAccountInfo, {id: rowId}, function ( data ) {
					if( data.error ) {
						alert( data.error );
						self.$registerFormWidget.enableAll();
					}
					else{
						self.$gridTable.find( '> tbody > tr[data-id="'+rowId+'"]' ).replaceWith( data['row'] );
						self.returnRegisterFormToPreviousState();
					}
				});
			},
			returnRegisterFormToPreviousState: function(){
				self.$registerFormWidget.enableAll();
				self.$registerFormWidget.$ns.find('input[type="text"]').val('');
				self.$registerFormWidget.$ns.find('input[type="hidden"]').val('');
				self.$registerFormWidget.$ns.find('input[type="button"]').val(options.ls.lAddAccountBtnText);
				self.$registerFormWidget.$ns.attr({'data-id': ''});
				self.$registerFormWidget.mode = self.registerFormPrevMode;
				self.$registerFormWidget.type = 'contest';
				self.$registerFormBoxButton.find('.cancelEditAccount').remove();
			},
			deleteSpecificRow:function() {
				var $row = $(this).closest( "tr[data-id]" );
				var id = $row.attr( 'data-id' );
				var $row = self.$gridTable.find( '> tbody > tr[data-id="'+id+'"]' );
				if( $row.length ) {
					if( !confirm( options.ls.lDeleteConfirm )) return false;
					$.getJSON( options.ajaxDeleteURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							$row.remove();
							self.issetRemovedRows = true;
						}
					});
				}
				return false;
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find( '.spinner-wrap' ).remove();;
			},
			loadPage:function( yourMonitoringAccountsListPage ) {
				self.disable();
				var data = { yourMonitoringAccountsListPage:yourMonitoringAccountsListPage, type: options.type };
				$.getJSON( options.ajaxLoadListURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
					}
				});
			},
			changePage:function() {
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( $li.hasClass( 'disabled' )) return false;
				if( !$li.hasClass( 'active' ) || self.issetRemovedRows ) {
					var $href = $link.attr( 'href' );
					var matchs = /yourMonitoringAccountsListPage=(\d+)/.exec( $href );
					var yourMonitoringAccountsListPage = matchs ? matchs[1] : 1;
					self.loadPage( yourMonitoringAccountsListPage );
				}
				return false;
			},
			clearRegisterForm: function(){
				self.$registerFormWidget.enableAll();
				self.$registerFormWidget.$ns.find('input[type="text"]').val('');
			
			},
			updateList: function(){
				self.loadPage( 1 );
				self.clearRegisterForm();
				self.scrollToAccountsTable();
			},
			useAccountForThread: function(){
				if( options.forumId == '' ) return false;
				var accId = $(this).closest('tr').attr('data-id');
				
				self.disable();
				$.getJSON( options.ajaxSendAccIdToForumUrl, {accId: accId, forumId: options.forumId}, function ( data ) {
					if( data.error ) {
						alert( data.error );
						self.enable();
					}
				});
				

				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );