var wInterestRatesReviewsWidgetOpen;
!function( $ ) {
	wInterestRatesReviewsWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$loadBtn = self.$ns.find('.load_btn');
				self.$postsWrapper = self.$ns.find('.load-more-wrapper');

				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';

				self.$loadBtn.click( self.loadMorePosts );

			},
			loadMorePosts: function(){
				if( self.loading ) return false;
				
				self.loading = true;
				self.disable();
				
				$.getJSON( options.ajaxLoadListURL, {num: options.num, lastDate: options.lastDate, lastId: options.lastId}, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$postsWrapper.append(data.content);
						$("img.lazy").lazyload({effect : "fadeIn"}).removeClass('lazy');
						if( parseInt( data.total ) <= options.num ) self.$loadBtn.remove();
						options.lastDate = data.lastDate;
						options.lastId = data.lastId;
					}
					self.loading = false;
					self.enable();
				});
				
				return false;
			},
			enable: function(){
				self.$ns.find('.spinner-wrap').remove();
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},

			changePage:function() {
				self.hideDropdowns();
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( !$li.hasClass( 'disabled' ) && !$li.hasClass( 'active' ) ) {
					var $href = $link.attr( 'href' );
					var matchs = /interestRatePage=(\d+)/.exec( $href );
					var interestRatePage = matchs ? matchs[1] : 1;
					self.disable();
					$.getJSON( options.ajaxLoadListURL, {interestRatePage:interestRatePage}, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $content = $( data.list );
							self.$ns.replaceWith( $content );
							self.init();
						}
					});
				}
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );