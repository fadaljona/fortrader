var wAdminCountersListWidgetOpen;
!function( $ ) {
	wAdminCountersListWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminCountersListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxLoadURL: '/admin/counter/ajaxLoadRow',
			ajaxChangeEnabledURL: '/admin/counter/ajaxChangeEnabled',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wCountersGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$gridTable.on( "click", "button.wEnable", { enabled: '1' }, self.changeEnabled );
				self.$gridTable.on( "click", "button.wDisable", { enabled: '0' }, self.changeEnabled );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idCounter="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			hideEmptyRow:function() {
				self.$gridTable.find( 'td.empty' ).closest( 'tr' ).hide();
			},
			changeEnabled:function( event ) {
				//if( self.loading ) return false;
				var $btn = $(this);
				var btnHtml = $btn.html();
				var $tr = $btn.closest( 'tr' );
				var id = $tr.attr( 'idCounter' );
				$btn.attr( 'disabled', 'disabled' );
				$btn.html( options.ls.lSaving );
				self.loading = true;
				self.resetSelection();
				self.selectionChanged();
				
				$.getJSON( yiiBaseURL+options.ajaxChangeEnabledURL, { id: id, enabled: event.data.enabled }, function ( data ) {
					self.loading = false;
					$btn.removeAttr( 'disabled' );
					$btn.html( btnHtml );
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.update( id );
					}
				});
				return false;
			},
			add:function( id ) {
				self.show();
				var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
				self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
				$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
					if( data.error ) {
						alert( data.error );
						$rowLoading.remove();
					}
					else{
						var $row = $( data.row );
						$row.replaceAll( $rowLoading );
						self.resetSelection();
						self.hideEmptyRow();
					}
				});
			},
			update:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idCounter="'+id+'"]' );
				if( $row.length ) {
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							var $row = $( data.row );
							$row.replaceAll( $rowLoading );
							self.resetSelection();
						}
					});
				}
			},
			delete:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idCounter="'+id+'"]' );
				$row.remove();
				
				if( !self.$gridTable.find( '> tbody > tr[idCounter]' ).length ) {
					self.hide();
				}
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );