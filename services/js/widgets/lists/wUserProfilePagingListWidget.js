var wUserProfilePagingListWidgetOpen;
!function( $ ) {
	wUserProfilePagingListWidgetOpen = function( options ) {
		var defLS = {
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wUserProfilePagingListWidget',
			ajax: false,
			hidden: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			issetRemovedRows: false,
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns.on( 'click', '.pagination a', self.changePage );
			},
			
			
			
			
			disable:function() {
				self.$ns.append( self.spinner );
			},
		
			loadPage:function( pageNum ) {
				self.disable();
				var postData = {
					idUser: options.idUser
				}
				postData[options.pageVar] = pageNum;

				$.getJSON( options.ajaxLoadListURL, postData, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
					}
				});
			},
			changePage:function() {
				
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( $li.hasClass( 'disabled' )) return false;
				if( !$li.hasClass( 'active' ) || self.issetRemovedRows ) {
					var $href = $link.attr( 'href' );
					var re = new RegExp(options.pageVar+'=(\\d+)', 'i');
					var matchs = re.exec( $href );
					var pageNum = matchs ? matchs[1] : 1;
					self.loadPage( pageNum );
				}
				return false;
			},
			
			
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );