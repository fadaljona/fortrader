var wAdminUserBalancesListWidgetOpen;
!function( $ ) {
	wAdminUserBalancesListWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminUserBalancesListWidget',
			ajax: false,
			showType: 'fadeIn()',
			hideType: 'fadeOut()',
			ajaxLoadURL: '/admin/userBalance/ajaxLoadRow',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wUserBalancesGridView' );
				self.$gridTable = self.$grid.find( '> form > table, > table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idUser="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			update:function( id ) {
				var $row = self.$gridTable.find( '> tbody > tr[idUser="'+id+'"]' );
				if( $row.length ) {
					var $rowLoading = self.$tabTpl.find( 'tr'+options.ins+'.wLoading' ).clone();
					self.$gridTable.find( 'tbody' ).prepend( $rowLoading );
					$rowLoading.replaceAll( $row );
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						if( data.error ) {
							alert( data.error );
							$row.replaceAll( $rowLoading );
						}
						else{
							var $row = $( data.row );
							$row.replaceAll( $rowLoading );
							self.resetSelection();
						}
					});
				}
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );