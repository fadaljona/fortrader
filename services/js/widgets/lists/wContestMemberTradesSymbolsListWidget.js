var wContestMemberTradesSymbolsListWidgetOpen;
!function( $ ) {
	wContestMemberTradesSymbolsListWidgetOpen = function( options ) {
		var defOption = {
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			sortField: false,
			sortType: false,
			init:function() {
				
				self.$ns = $( options.selector );
				self.$grid = self.$ns.find( options.ins+'.wContestMemberTradesSymbolsGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				self.$tabTpl = self.$ns.find( options.ins+'.wTabTpl' );
				self.$byDirection = self.$ns.find( '.byDirection' + options.type );
				
				self.$bySell = self.$ns.find( '.bySell' + options.type );
				self.$byBuy = self.$ns.find( '.byBuy' + options.type );
				self.$symbolSearchInput = self.$ns.find( '.symbolSearchInput' );
				self.$symbolSearchForm = self.$ns.find( '.symbolSearchForm' );
				
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				if( options.hidden ) {
					self.hide( true );
				}
				
				self.$byDirection.change( self.changeByDirection );
				self.$bySell.change( self.changeBySell );
				self.$byBuy.change( self.changeByBuy );
				self.$symbolSearchForm.submit( self.reloadWithFilters );
				
				self.$ns.on( 'click', '.pagination a', self.changePage );
				self.$ns.on( 'click', 'th[class*=sorting]', self.changeSorting );
			},
			changeSorting:function() {
				var $th = $(this);
				self.sortField = $th.attr( 'data-sortField' );
				self.sortType = $th.hasClass( 'sorting_asc' ) ? 'DESC' : 'ASC';
				self.reloadWithFilters();
				return false;
			},
			hide:function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					eval( "self.$ns."+options.hideType );
				}
			},
			show:function() {
				eval( "self.$ns."+options.showType );
			},
			getSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			setSelection:function( ids ) {
				self.resetSelection();
				$.each( ids, function() {
					self.$gridTable.find( '> tbody > tr[idContest="'+this+'"]' ).addClass( 'selected' );
				});
			},
			resetSelection:function() {
				self.getSelection().removeClass( 'selected' );
			},
			selectionChanged:function() {
				$.each( self.onSelectionChanged, function() { this(); });
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			getByDirection: function(){
				if( self.$byDirection.is(":checked") )
					return 1;
				else
					return 0;
			},
			getBySell: function(){
				if( self.$bySell.is(":checked") )
					return 1;
				else
					return 0;
			},
			getByBuy: function(){
				if( self.$byBuy.is(":checked") )
					return 1;
				else
					return 0;
			},
			changeBySell:function() {
				if( self.$bySell.is(":checked") ){
					self.$byBuy.prop( 'checked', false );
					self.$byDirection.prop( 'checked', true );
				}
				self.reloadWithFilters();
			},
			changeByBuy:function() {
				if( self.$byBuy.is(":checked") ){
					self.$bySell.prop( 'checked', false );
					self.$byDirection.prop( 'checked', true );
				}
				self.reloadWithFilters();
			},
			changeByDirection:function() {
				if( !self.$byDirection.is(":checked") ){
					self.$bySell.prop( 'checked', false );
					self.$byBuy.prop( 'checked', false );
					/*self.$bySell.attr({'disabled': true});
					self.$byBuy.attr({'disabled': true});*/
				}/*else{
					self.$bySell.attr({'disabled': false});
					self.$byBuy.attr({'disabled': false});
				}*/
				self.reloadWithFilters();
			},
			getPostData: function(){
				return {
					id: options.idMember,
					byDirection: self.getByDirection(), 
					byBuy: self.getByBuy(), 
					bySell: self.getBySell(),
					type: options.type,
					search: self.$symbolSearchInput.val(),
					sortField: self.sortField,
					sortType: self.sortType,
				};
				
			},
			reloadWithFilters:function() {
				var postData = self.getPostData();
				postData[options.pageVar] = 1;
				
				self.disable();
				$.getJSON( options.ajaxLoadListURL,  postData, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $content = $( data.list );
						self.$ns.replaceWith( $content );
						self.init();
					}
				});
				return false;
			},
			changePage:function() {
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( !$li.hasClass( 'disabled' ) && !$li.hasClass( 'active' ) ) {
					var $href = $link.attr( 'href' );
					var re = new RegExp(options.pageVar+'=(\\d+)', 'i');
					var matchs = re.exec( $href );
					var pageNum = matchs ? matchs[1] : 1;
					self.disable();
					
					var postData = self.getPostData();
					postData[options.pageVar] = pageNum;
					
					$.getJSON( options.ajaxLoadListURL, postData, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $content = $( data.list );
							self.$ns.replaceWith( $content );
							self.init();
						}
					});
				}
				return false;
			},
			onSelectionChanged:[],
		};
		self.init();
		return self;
	}
}( window.jQuery );