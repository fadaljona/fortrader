var wBrokerLinksStats;
!function( $ ) {
	wBrokerLinksStats = function( options ) {
		var defOption = {
			linkSelector: '.saveBrokerLinksStats',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				var $links = $( options.linkSelector );
				$links.click( self.sendStats );
			},
			sendStats:function() {
				var $this = $(this);
				var dataTosend = {
					brokerId: $this.attr('data-broker-id'),
					type: $this.attr('data-link-type'),
				}
				$.post( options.statsUrl, dataTosend, function ( data ) {}, "json" );
			},

		};
		self.init();
		return self;
	}
}( window.jQuery );