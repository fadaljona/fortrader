var wTraderProfileAlertWidgetOpen;
!function( $ ) {
	wTraderProfileAlertWidgetOpen = function( options ) {
		var defOption = {
			ns: nsLayoutDefaultIndex,
			ins: '',
			selector: '.wTraderProfileAlertWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
				
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$wTraderAlert = self.$ns.find( options.ins+'.wTraderAlert' );
				
				self.$wTraderAlert.on( 'click', '.wCancel', self.cancelProfileTrader );
			},
			cancelProfileTrader:function() {
				self.$wTraderAlert.hide();
				$.cookie( 'dontShowTraderProfileAlert', 'on', {path:'/', expires: 365} );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );