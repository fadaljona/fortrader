var wCurrencyRatesArchivePreviewWidgetOpen;
!function( $ ) {
	wCurrencyRatesArchivePreviewWidgetOpen = function( options ) {
		var defOption = {
			ajax: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.chooseDate();
			},
			chooseDate: function(){
				self.$ns.find('.choose_date_box .choose_date_list').each(function( ) {
					var $this = $(this);
					var totalWidth = $this.find('ul').width();

					$this.stop().animate({
						'scrollLeft': totalWidth,
					});

				});
				self.$ns.on('click', '.choose_date .navigation_box_2>a', function(){

					var $this = $(this),
						parent = $this.closest('.choose_date'),
						box = parent.find('.choose_date_list'),
						boxWidth = box.width(),
						boxScroll = box.scrollLeft();

					if($this.hasClass('next')){

						box.stop().animate({
							'scrollLeft': boxScroll+boxWidth/2
						});

					}
					else{

						box.stop().animate({
							'scrollLeft': boxScroll - boxWidth/2
						});

					}

				});

			},
		};
		self.init();
		return self;
	}
}( window.jQuery );