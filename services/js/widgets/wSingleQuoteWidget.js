var wSingleQuoteWidgetOpen;
!function( $ ) {
	wSingleQuoteWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wSingleQuoteWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$wWampSession = tickSession;
				self.$wWindow = $(window);
				self.$wDocument = $(document);
				self.$wDigitalWatchEl = self.$ns.find( '#digital_watch' );
				self.$wArrowIcon = self.$ns.find( '.arrow-icon' );
				self.$wRealTimeLabel = self.$ns.find( '.square_input' );
				self.$wRealTimeCheckbox = self.$ns.find( '#real_time' );
				self.$wWindow.load( self.digitalWatch( self.$wDigitalWatchEl ) );
				
				self.subscribeToQuotes();
			},
			digitalWatch:function(el) {
				if(el.length){
					var self = this;
					var date = new Date();
					var hours = date.getHours();
					var minutes = date.getMinutes();
					var seconds = date.getSeconds();
					if (hours < 10) hours = "0" + hours;
					if (minutes < 10) minutes = "0" + minutes;
					if (seconds < 10) seconds = "0" + seconds;
					document.getElementById("digital_watch").innerHTML = hours + ":" + minutes + ":" + seconds;
					setTimeout(function(){
						self.digitalWatch(el);
					},1000);
				}
			},
			subscribeToQuotes:function() {
				if( options.sourceType == 'yahoo' ) return false;

				document.addEventListener('tickSessionOpend', function(){	
					self.$wWampSession.subscribe(options.quotesKey, function(topic, data) {
						if( !self.$wRealTimeCheckbox.is(':checked') ) return false;
						self.updateQuotes(data);
					});
					console.warn('single subscribed to tick data for ' + options.quotesKey);
				});
					
			},
			updateQuotes:function(data) {
				for(var key in data) {
					var parsedData = JSON.parse(data[key]);	
					self.updateQuotesWithAnimate(key, parsedData);
				}
			},
			updateQuotesWithAnimate:function(key, parsedData) {
				var pageBid = $(options.selector+' .pid-' + key + '-bid'),
					pageChg = $(options.selector+' .pid-' + key + '-chg'),
					pageChgPercent = $(options.selector+' .pid-' + key + '-chg-percent'),
					pageChgPercentWrap = $(options.selector+' .' + key + '-chg-percent-wrap'),
					pageTime = $(options.selector+' .pid-' + key + '-time');
					
				var newBid = parseFloat(parsedData.bid),
					precision = pageBid.data('precision'),
					lastBid = pageBid.data('lastBid'),
					oldBid = parseFloat(pageBid.text());
					
				if( lastBid != 'n/a' ){
					var lastBidVal = parseFloat( lastBid );
					var prevChgVal = parseFloat(pageChg.text());
					var newChgVal = parsedData.bid - lastBidVal;
					var newChgPercentVal = newChgVal / lastBidVal * 100 ;
					var sign = '';
					if( newChgVal > 0 ) sign = '+';
					pageChg.html( sign + newChgVal.toFixed(precision) );
					
					pageChgPercent.html( sign + newChgPercentVal.toFixed(precision) );
					
					if( prevChgVal < 0 && newChgVal > 0 ){
						pageChgPercentWrap.removeClass('red_color');
						pageChgPercentWrap.addClass('green_color');
					}else if( prevChgVal > 0 && newChgVal < 0 ){
						pageChgPercentWrap.removeClass('green_color');
						pageChgPercentWrap.addClass('red_color');
					}
				}

				pageBid.html(parsedData.bid);
				
				if( newBid > oldBid ){
					self.$wArrowIcon.removeClass('fa-long-arrow-down red_color');
					self.$wArrowIcon.addClass('fa-long-arrow-up green_color');
				}else if( newBid < oldBid ){
					self.$wArrowIcon.removeClass('fa-long-arrow-up green_color');
					self.$wArrowIcon.addClass('fa-long-arrow-down red_color');
				}
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );