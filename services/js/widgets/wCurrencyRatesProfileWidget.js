var wCurrencyRatesProfileWidgetOpen;
!function( $ ) {
	wCurrencyRatesProfileWidgetOpen = function( options ) {
		var defOption = {
			ajax: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.saveViews();
				
				self.$ns = $( options.selector );
				self.$wChangeCurrencyPageSelect = self.$ns.find( 'select.changeCurrencyPage' );
				
				self.$wSpinner = self.$ns.find( '.spinner-wrap' );
				self.$wPeriodLinks = self.$ns.find( 'ul.changePeriodLinks li a' );
				self.$wPeriodLinksLI = self.$ns.find( 'ul.changePeriodLinks li' );

				self.initFormStyler();
				
				self.$chartData = self.parseCSV( options.chartData );
				
				self.$chart = self.makeChart(options.lang);
				
				self.$chart.addListener("rendered", self.enable);
				self.$chart.addListener("dataUpdated", self.enable);
				self.$chart.addListener("rendered", self.saveImage);
				
				self.$wPeriodLinks.click( self.changePeriod );
			
			},
			
			saveViews:function(){
				$.post( options.saveViewsUrl, {"id": options.idCurrency}, function ( data ) {

				} );
			},
			saveImage:function() {
				if( options.needToUpdateImg ){
					var interval = setInterval( function() {
						if ( window.fabric ) {
							clearTimeout( interval );
							self.$chart.export.capture( {}, function() {
								this.toJPG( {}, function( base64 ) {
									self.sendImage( base64 );
								} );
							} );
						}
					}, 100 );
				}
			},
			sendImage:function( base64 ) {
				if( self.loading ) return false;
				self.loading = true;
				$.post( options.saveImageUrl, {"id": options.idCurrency, "img": base64}, function ( data ) {

					self.loading = false;
				} );
			},
			
			initFormStyler:function(){
				var $forms = self.$ns.find('.form_styler');
				if( $forms.length ){
					$forms.styler({
						selectSearch: true,
						onSelectClosed: function() {
							var $this = $(this);
							if( $this.hasClass('changeCurrencyPage') ){
								var newUrl = self.$wChangeCurrencyPageSelect.val();
								if( document.location.href.indexOf(newUrl) == -1 ) document.location.assign( newUrl );
							}
						}
					});
				}
			},
			changePeriod:function(){
				var $this = $(this);
				if( $this.closest('li').hasClass('active') ) return false;
				
				self.disable();
				var periodVal = $this.attr('data-val');
				
				$.getJSON( options.loadChartDataUrl, {period: periodVal, id:options.idCurrency, type: options.dataType }, function ( data ) {
					if( data.error ) {
						alert( data.error );
						self.enable();
					}else{
						self.$chartData = self.parseCSV( data.data );
						/*console.log( self.$chartData );*/
						self.$chart.dataProvider = self.$chartData;
						
						var lastVal = self.$chartData[self.$chartData.length - 1].value;
						self.$chart.valueAxes[0].guides[0].label = lastVal;
						self.$chart.valueAxes[0].guides[0].balloonText = lastVal;
						self.$chart.valueAxes[0].guides[0].value = lastVal;
						self.$chart.validateData();
						
						self.$wPeriodLinksLI.removeClass('active');
						$this.closest('li').addClass('active');
					}
				});
				
				

				return false;
			},
			makeChart:function( lang) {
				var lastVal = self.$chartData[self.$chartData.length - 1].value;
				return AmCharts.makeChart( options.chartSelector, {
					"type": "serial",
					//new theme 
					"theme": 'none',
					"zoomOutText": '',
					"autoMargins": false,
					"marginRight": 10,
					"marginLeft": 10,
					"marginBottom": 0,
					"marginTop": 30,
					"plotAreaBorderAlpha": 1,
					"plotAreaBorderColor": '#e6e6e6',
					//new theme end
					"allLabels":[{
						"text": options.label,
						"size": 15,
						"bold": true,
						"x": 15,
						"y": 35,
						"color": '#555',
					}],
					"valueAxes": [{
						//new theme 
						"position": "left",
						"id": 'v1',
						"axisAlpha": 0,
						"inside": true,
						//new theme end
						"guides": [{
							"dashLength": 2,
							"inside": false,
							"lineAlpha": 1,
							"above": true,
							"lineColor": "#2c88cb",
							"fillColor": "#2c88cb",
							"balloonColor": "#2c88cb",
							"position": "right",
							"angle": 20,

							"label": lastVal,
							"balloonText": lastVal,
							"value": lastVal,
						}],
					}],
					//new theme 
					"balloon": {
						"shadowAlpha": 0,
						"fillAlpha": 1
					},
					//new theme end
					"language": lang,
					"dataDateFormat":"YYYY-MM-DD",
					"graphs": [{
						"useNegativeColorIfDown": true,
						"title": options.ls.lPrice,
						"negativeLineColor": "#ff2d2e",
						"lineColor": "#2c88cb",
						"lineAlpha": "1",
						"lineThickness": 2,
						"fillColors": "#2c88cb",
						"fillAlphas": 0,
						"valueField": "value",
						"type": "line",
						"id": "g1",
						"balloonText": options.ls.lPrice + ": [[value]]",
						"bullet": "round",
						"bulletSize": 5,
						"bulletColor": "#FFFFFF",
						"bulletBorderThickness": 1,
						"bulletBorderAlpha": 1,
						"balloonColor": "#2c88cb",
						"hideBulletsCount": 50,
						"useLineColorForBulletBorder": true
					}],
					//new theme 
					"chartScrollbar": {
						"graph": 'g1',
						"scrollbarHeight": 54,
						"backgroundAlpha": 1,
						"backgroundColor": '#fafafa',
						"selectedBackgroundAlpha": 1,
						"selectedBackgroundColor": '#cdcdcd',
						"graphFillAlpha": 1,
						"graphLineAlpha": 0.5,
						"selectedGraphFillAlpha": 1,
						"selectedGraphLineAlpha": 1,
						"autoGridCount": true,
						"color": '#fff',
						"dragIconWidth": 25
					},
					//new theme end
					"chartCursor": {
						//new theme 
						"cursorAlpha": 1,
						//new theme end
						"pan": true,
						//"valueLineEnabled": true,
						//"valueLineBalloonEnabled": true,
						"categoryBalloonDateFormat": "MMMM DD YYYY",
						"categoryBalloonColor": "#ff2d2e",
						"cursorColor": "#ff2d2e",
					},
					"categoryField": "date",
					"categoryAxis": {
						//new theme 
						"dashLength": 5,
						"minorGridEnabled": true,
						"position": 'top',
						"labelOffset": 4,
						"axisAlpha": 0,
						"gridColor": '#e6e6e6',
						"gridAlpha": 1,
						//new theme end
						"parseDates": true,
						"minPeriod": "DD",
						"equalSpacing": true,
						//"inside": true,
					},
					"dataProvider": self.$chartData,
					"export": {
						"enabled": true,
						"libs": {
							"path": options.exportLibs
						},
						"menu": [{
						  "format": "UNDEFINED",
						  "label": "",
						  "class": "export-main",
						  "menu": ["JPG", "PNG"]
						}],
					},
				});
			},
			disable:function(){
				self.$wSpinner.removeClass('hide');
			},
			enable:function(){
				self.$wSpinner.addClass('hide');
				/*console.log( self.$chart.export );*/
			},
			parseCSV:function(data) {
				var returnArr = [];
				if(!data) return returnArr;
				data = data.replace (/\r\n/g, "\n");
				data = data.replace (/\r/g, "\n");
				var rows = data.split("\n");
				for (var i = 0; i < rows.length; i++) {
					if (rows[i]) {
						var column = rows[i].split(";");
						
						if( column[1].indexOf('E') != -1 ){
							column[1] = parseFloat(column[1]);
							column[1] = column[1] + '';
						}

						var splitedStr = column[1].split('.');
						
						if( splitedStr.length > 1 ){
							if( splitedStr[1].length > 4 ){
								var lastZeroIndex = splitedStr[1].search(/[1-9]/);
								if( lastZeroIndex == -1 ){
									var dateValue = splitedStr[0] + '.' + splitedStr[1].substr(0, 4);
								}else{
									var dateValue = splitedStr[0] + '.' + splitedStr[1].substr(0, parseInt(lastZeroIndex) + 4);
								}
								
							}else{
								var dateValue = column[1];
							}
						}else{
							var dateValue = column[1];
						}
						
						var dataObject = {
							date: column[0],
							value: dateValue,
						};
						returnArr.push(dataObject);
					}
				}
				return returnArr;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );