var wCalendarComingEventsWidgetOpen;
!function( $ ) {
	wCalendarComingEventsWidgetOpen = function( options ) {
		var defOption = {

		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			blocker: '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>',
			init:function() {
				self.$ns = $( options.selector );
				self.$ul = self.$ns.find('ul');
				self.$showMoreBtn = self.$ns.find('.load_btn');
				self.$showMoreBtn.click( self.loadData );
			},
			disable: function(){
				self.$ns.append(self.blocker);
			},
			enable: function(){
				self.$ns.find('.spinner-wrap').remove();
			},
			loadData: function(){
				if( self.loading ) return false;
				self.loading = true;
				self.disable();
				var lastLi = self.$ns.find('li').last();
				
				$.getJSON( options.loadUrl, {dt: lastLi.attr('data-dt')}, function ( data ) {
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							if( data.list == '' ) self.$showMoreBtn.remove();
							self.$ul.append( data.list );
						}
						self.loading = false;
						self.enable();
					});
				return false;
			}
		};
		self.init();
		return self;
	}
}( window.jQuery );