var wChangeLanguageWidgetOpen;
!function( $ ) {
	wChangeLanguageWidgetOpen = function( options ) {
		var defOption = {
			selector: ".wSelectLanguage",
			cookieVarName: "language",
			expires: 365
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$selectLanguage = $( options.selector );
				self.$selectLanguage.change( function () {
					$.cookie( options.cookieVarName, $(this).val(), { expires: options.expires, path: "/" });
					document.location.reload();
				});
			}
		};
		self.init();
		return self;
	}
}( window.jQuery );