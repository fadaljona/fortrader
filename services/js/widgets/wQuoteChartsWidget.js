var wQuoteChartsWidgetOpen;
!function( $ ) {
	wQuoteChartsWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wQuoteChartsWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			needToUpdatePeriodChart: false,
			loadingAmchartsTech: false,
			init:function() {
				
				self.$ns = $( options.selector );
				self.$wWampSessionPeriod;
				self.$wWampSessionTick = tickSession;
				
				self.$tradingviewChart;
				
				self.$periodChartSelector = 'chartdiv'+options.periodChartId;
				self.$tickChartSelector = 'chartdiv'+options.tickChartId;
				
				self.$spinner = self.$ns.find( '.spinner-wrap' );
				self.$TechGraphwrapper = self.$ns.find( '.techGraphwrapper' );
				self.$TradingViewDownload = self.$TechGraphwrapper.find('.downloadchartImage');
				self.$TradingViewDownload.click( self.downloadTechChart );
				
				self.$periodChartWrapper = self.$ns.find( '#'+self.$periodChartSelector );
				self.$tickChartWrapper = self.$ns.find( '#'+self.$tickChartSelector );
				
				self.$tickChart;
				
				self.$periodZoomStartIndex;
				self.$periodZoomEndIndex;
				self.$tickZoomStartIndex;
				self.$tickZoomEndIndex;
				
				
				self.$currentPeriod = self.$ns.find( 'li.changePeriod.active a' ).text();
				
				self.$periodChartData = self.parseCSV( options.periods[self.$currentPeriod]['data'] );

				self.$periodChart = self.makePeriodChart( options.lang, options.periods[self.$currentPeriod]['categoryBalloonDateFormat'], options.periods[self.$currentPeriod]['minPeriod'] );
				self.subscribeToTickQuotes();
				//self.subscribeToPeriodQuotes();
				self.updatePeriodChartTimer();
				
				self.$periodChart.addListener("rendered", self.periodZoomChart);
				self.$periodChart.addListener("rendered", self.removeSpinner);
				
				//self.$periodChart.addListener("rendered", self.saveImage);
				
				self.$periodChart.addListener("dataUpdated", self.removeSpinner);
				self.$periodChart.addListener("zoomed", self.periodHandleZoom);
				
				self.$wSelectChartTypeUL = self.$ns.find( 'ul.selectChartType' );
				self.$wChangePeriodsLi = self.$ns.find( 'ul.selectChartType li' );
				self.$wChangePeriod = self.$ns.find( 'li.changePeriod a' );
				self.$wChangePeriod.click( self.changeChartPeriod );
				
				self.$wSelectAmchartsTech = self.$ns.find( 'ul.selectPeriodType a' );
				self.$wSelectAmchartsTechLi = self.$ns.find( 'ul.selectPeriodType li' );
				self.$wSelectAmchartsTechUl = self.$ns.find( 'ul.selectPeriodType' );
				
				self.$wSelectAmchartsTech.click( self.changeAmchartsTech );
				
				//init tradingviewChart
				if( !self.$tradingviewChart ){
					self.$tradingviewChart = self.makeTradingViewChart( options.symbol, options.defaultPeriod, 'chartdiv'+options.tradingviewChartId );
					chartDetails.tradingviewChart = self.$tradingviewChart;
					
					self.$tradingviewChart.onChartReady(function( ){
							self.saveTvImage();
					});
					
				} 
				
				self.$wTickChartButton = self.$ns.find( 'li.tickChart' );
				
				self.$wTickChartButton.click( self.clickTickChartButton );
				

			},
			saveTvImage:function(){
				if( options.needToUpdateImg ){
					if( !self.$tradingviewChart ) return false;
					chartDetails.imageForFb = true;
					self.$tradingviewChart.executeActionById("takeScreenshot");	
					
					self.$tradingviewChart.onScreenshotReady(function( base64 ){
						if( chartDetails.imageForFb == true ){
							if( self.loading ) return false;
							self.loading = true;
							$.post( options.saveImageUrl, {"symbolId": options.symbolId, "img": base64}, function ( data ) {

								self.loading = false;
							} );
							chartDetails.imageForFb = false;
						}
					});
				}
				return false;
			},
			downloadTechChart:function() {
				if( !self.$tradingviewChart ) return false;
				chartDetails.imageNotForDecomments = true;
				self.$tradingviewChart.executeActionById("takeScreenshot");
				
				self.$tradingviewChart.onScreenshotReady(function( base64 ){
					if( chartDetails.imageNotForDecomments == true && chartDetails.imageForFb == false ){
						download( base64, "TechnicalChart_"+options.symbol+".png", "image/png" );
						chartDetails.imageNotForDecomments = false;
					}
				});
						
				return false;
			},
			saveImage:function() {
				if( options.needToUpdateImg ){
					var interval = setInterval( function() {
						if ( window.fabric ) {
							clearTimeout( interval );
							self.$periodChart.export.capture( {}, function() {
								this.toJPG( {}, function( base64 ) {
									self.sendImage( base64 );
								} );
							} );
						}
					}, 100 );
				}
			},
			sendImage:function( base64 ) {
				if( self.loading ) return false;
				self.loading = true;
				$.post( options.saveImageUrl, {"symbolId": options.symbolId, "img": base64}, function ( data ) {

					self.loading = false;
				} );
			},
			hideTradingviewChart:function() {
				$( '#chartdiv'+options.tradingviewChartId ).addClass('hide');
				self.$TechGraphwrapper.addClass('hide');
			},
			showTradingviewChart:function() {
				$( '#chartdiv'+options.tradingviewChartId ).removeClass('hide');
				self.$TechGraphwrapper.removeClass('hide');
			},
			changeAmchartsTech:function() {
				
				if( self.loadingAmchartsTech ) return false;
				self.loadingAmchartsTech = true;
				
				if( $(this).closest('li').hasClass('active') ){
					self.loadingAmchartsTech = false;
					return false;
				}
				
				if( $(this).data('type') == 'technical' ){
					if( self.$wWampSessionPeriod ) self.$wWampSessionPeriod.close();
	
					self.$wSelectAmchartsTechLi.removeClass('active');
					$(this).closest('li').addClass('active');
					self.showTradingviewChart();
					$( '#chartdiv'+options.periodChartId ).addClass('hide');
					$( '#chartdiv'+options.tickChartId ).addClass('hide');
					self.$wSelectChartTypeUL.addClass('hide');
					
					if( !self.$tradingviewChart ){
						self.$tradingviewChart = self.makeTradingViewChart( options.symbol, options.defaultPeriod, 'chartdiv'+options.tradingviewChartId );
						chartDetails.tradingviewChart = self.$tradingviewChart;
					} 
					
				}else{			
					
					self.hideTradingviewChart();
					if(options.sourceType != 'yahoo')self.$wSelectChartTypeUL.removeClass('hide');
					self.$wSelectAmchartsTechLi.removeClass('active');
					
					$(this).closest('li').addClass('active');
					
					if( self.$currentPeriod == 'tick' ){
						$( '#chartdiv'+options.tickChartId ).removeClass('hide');
						
						self.loadingAmchartsTech = false;
						return false;
					}else{
						$( '#chartdiv'+options.periodChartId ).removeClass('hide');
					}
				
					self.loadDataForPeriodChart( self.$currentPeriod );
					self.subscribeToPeriodQuotes();
				}
				self.loadingAmchartsTech = false;
				
				return false;
				
			},
			updatePeriodChartTimer:function() { //обновление тиковых данных на периодическом графике по таймеру

				var interval = setInterval( function() {
					if( self.needToUpdatePeriodChart && self.$currentPeriod != 'tick' && self.$wSelectAmchartsTechUl.find('li[data-type="amcharts"]').hasClass('active') ){
						self.periodChartValidateData( );
						self.periodRefreshZoomChart();
					}
					self.needToUpdatePeriodChart = false;
				}, 1 * 1000 );

			},
			removeSpinner:function() {
				self.$spinner.addClass('hide');
			},
			clickTickChartButton:function() {
				if( $(this).hasClass('active') ){
					return false;
				}else{
					if( self.$wWampSessionPeriod ) self.$wWampSessionPeriod.close();
					self.$currentPeriod = 'tick';
					
					self.$wChangePeriodsLi.removeClass('active');
					$(this).addClass('active');
					
					self.$periodChartWrapper.addClass('hide');
					self.$tickChartWrapper.removeClass('hide');
					
					if( self.$tickChart == undefined ){
						self.$tickChart = self.makeTickChart(options.lang);
						self.$tickChart.addListener("rendered", self.removeSpinner);
						self.$tickChart.addListener("zoomed", self.tickHandleZoom);
						chartDetails.tickChart = self.$tickChart;
					}
				}
				
				return false;
			},
			subscribeToTickQuotes:function() {
				if( options.sourceType == 'yahoo') return false;
				
				var waitOpendConnection = setInterval(function(){
					
					if( self.$wWampSessionTick._websocket_connected ){
						
						clearInterval(waitOpendConnection);
						
						self.$wWampSessionTick.subscribe(options.symbol, function(topic, data) {
							if( self.$tickChart != undefined ) self.updateTickChart(data);
							if( self.$periodChart != undefined ) self.updatePeriodChartWithTickData(data);
						});
						console.warn('subscribed to tick data for ' + options.symbol);
					}
				}, 500);
			

			},
			subscribeToPeriodQuotes:function() {
				if( options.sourceType == 'yahoo') return false;
				self.$wWampSessionPeriod = new ab.Session(options.connUrl,
					function() {
						self.$wWampSessionPeriod.subscribe(options.symbol+'_'+self.$currentPeriod, function(topic, data) {
							self.updatePeriodChart(data);
						});
						console.warn('WebSocket connection opened for '+options.symbol+' period ' + self.$currentPeriod);
					},
					function() {
						console.warn('WebSocket connection closed for period');
					},
					{'skipSubprotocolCheck': true}
				);
			},
			updatePeriodChartWithTickData:function(data) { // пишем постоянно данные в dataProvider
				if( self.$currentPeriod == 'tick' ) return false;
				
				self.needToUpdatePeriodChart = true;
				
				for(var key in data) {	// 1 строка данных для символа
					var parsedData = JSON.parse(data[key]);	
					
					var dataDate = self.getLocalTimeForTickDataForBars( parsedData.time );
					
					var currentTime = self.jsStrToTime( dataDate );
					var lastDate = self.$periodChart.dataProvider[self.$periodChart.dataProvider.length-1].date;
					var periodVal = parseInt( options.periods[self.$currentPeriod]['periodVal'] ) * 60 * 1000;
					var lastTime = self.jsStrToTime( lastDate );
					var diffTime = currentTime - lastTime;
					
					if( diffTime < periodVal ){
						dataDate = lastDate;
					}else if( diffTime > periodVal ){
						var ost = diffTime % periodVal;
						var multiplier = ( diffTime - ost ) / periodVal;
						var newTime = lastTime + periodVal * multiplier;
						
					}
					
					var index = self.searchByDateInDataProvider( self.$periodChart.dataProvider, dataDate );
					
					if( index ){
						
						if( self.$periodChart.dataProvider[index].high < parsedData.bid ) self.$periodChart.dataProvider[index].high = parsedData.bid;
						if( self.$periodChart.dataProvider[index].low > parsedData.bid ) self.$periodChart.dataProvider[index].low = parsedData.bid;
						self.$periodChart.dataProvider[index].close = parsedData.bid;
						self.$periodChart.dataProvider[index].volume = self.$periodChart.dataProvider[index].volume + 1;
						
					}else{
						
						self.$periodChart.dataProvider.push( {
							date: dataDate,
							open: parsedData.bid,
							high: parsedData.bid,
							low: parsedData.bid,
							close: parsedData.bid,
							volume: 1
						
						} );
					}
					
				}
			},
			updateTickChart:function(data) {
				for(var key in data) {
					var parsedData = JSON.parse(data[key]);	
					self.$tickChart.dataProvider.push({
						ask: parsedData.ask,
						bid: parsedData.bid,
						date: self.getLocalTimeForTickData( parsedData.time )
					});
					
					var lastAsk = self.$tickChart.dataProvider[self.$tickChart.dataProvider.length - 1].ask;
					var lastBid = self.$tickChart.dataProvider[self.$tickChart.dataProvider.length - 1].bid;
						
					self.$tickChart.valueAxes[0].guides[0].label = lastAsk;
					self.$tickChart.valueAxes[0].guides[0].balloonText = lastAsk;
					self.$tickChart.valueAxes[0].guides[0].value = lastAsk;
					
					self.$tickChart.valueAxes[0].guides[1].label = lastBid;
					self.$tickChart.valueAxes[0].guides[1].balloonText = lastBid;
					self.$tickChart.valueAxes[0].guides[1].value = lastBid;
					
					if( self.$currentPeriod == 'tick' && self.$wSelectAmchartsTechUl.find('li[data-type="amcharts"]').hasClass('active') ){
						self.tickChartValidateData();
						self.tickRefreshZoomChart();
					}
				}
				
			},
			updatePeriodChart:function(data) {
				var parsedData = self.parseCSV(data);
				for (var i = 0; i < parsedData.length; i++) {
					var dateNew = parsedData[i].date;
					var index = self.searchByDateInDataProvider( self.$periodChart.dataProvider, dateNew );	
					if( index ){
						self.$periodChart.dataProvider[index] = parsedData[i];
					}else{
						self.$periodChart.dataProvider.push( parsedData[i] );
					}
				}
				if( self.$wSelectAmchartsTechUl.find('li[data-type="amcharts"]').hasClass('active') ){
					self.periodChartValidateData( );
					self.periodRefreshZoomChart();
				}
			},
			periodChartValidateData:function( ) {
				var lastClose = self.$periodChart.dataProvider[self.$periodChart.dataProvider.length - 1].close;
				self.$periodChart.valueAxes[0].guides[0].label = lastClose;
				self.$periodChart.valueAxes[0].guides[0].balloonText = lastClose;
				self.$periodChart.valueAxes[0].guides[0].value = lastClose;
				var zoomStartIndex = self.$periodZoomStartIndex;
				var zoomEndIndex = self.$periodZoomEndIndex;
				self.$periodChart.validateData(); // сброс self.$periodZoomStartIndex
				self.$periodZoomStartIndex = zoomStartIndex;
				self.$periodZoomEndIndex = zoomEndIndex;
			},
			tickChartValidateData:function() {
				var zoomStartIndex = self.$tickZoomStartIndex;
				var zoomEndIndex = self.$tickZoomEndIndex;
				self.$tickChart.validateData(); // сброс self.$tickZoomStartIndex
				self.$tickZoomStartIndex = zoomStartIndex;
				self.$tickZoomEndIndex = zoomEndIndex;
			},
			searchByDateInDataProvider:function( dataProvider, date ) {
				if( self.jsStrToTime( date ) > self.jsStrToTime( dataProvider[dataProvider.length-1].date ) ) return false;
				for (var i = dataProvider.length-1; i >= 0; i--) {			
					if( dataProvider[i].date == date ){
						return i;
					}
				}
				return false;
			},
			jsStrToTime:function( dateStr ){
				var dataTime = dateStr.split(" ");	// 0 year, 1 month
				var dataArr = dataTime[0].split("-");
				var timeArr = dataTime[1].split(":");

				var $date = new Date( parseInt(dataArr[0]) , parseInt(dataArr[1])-1, parseInt(dataArr[2]),  parseInt(timeArr[0]), parseInt(timeArr[1]), 0, 0); //new Date(year, month, day, hours, minutes, seconds, milliseconds)
				
				return $date.getTime();
			},
			periodHandleZoom:function(event) {	
				self.$periodZoomStartIndex = event.startIndex;
				self.$periodZoomEndIndex = event.endIndex;
			},
			tickHandleZoom:function(event) {	
				self.$tickZoomStartIndex = event.startIndex;
				self.$tickZoomEndIndex = event.endIndex;
			},
			getLocalTimeForData:function(timeStr) {					
				var $date = new Date( timeStr * 1000 ); //new Date(year, month, day, hours, minutes, seconds, milliseconds)
				
				var year, month, day, hours, minutes;
				
				year = $date.getFullYear();
				
				month = $date.getMonth() + 1;
				if( month < 10 ){ month = '0' + month;}
				
				day = $date.getDate();
				if( day < 10 ){ day = '0' + day;}
				
				hours = $date.getHours();
				if( hours < 10 ){ hours = '0' + hours;}
				
				minutes = $date.getMinutes();
				if( minutes < 10 ){ minutes = '0' + minutes;}
				
				return year+'-'+month+'-'+day+' '+hours+':'+minutes;
			},
			getLocalTimeForTickData:function(timeStr) {
				var timeArr = timeStr.split(":");
					
				var $date = new Date(2015, 1, 1, timeArr[0], timeArr[1], timeArr[2], 0); //new Date(year, month, day, hours, minutes, seconds, milliseconds)
				var timezoneOffset = $date.getTimezoneOffset() + 60 * options.realQuotesDataTimeDifference;
				$date.setMinutes ( $date.getMinutes() + timezoneOffset * (-1) );
				var hours, minutes, seconds;
				
				hours = $date.getHours();
				if( hours < 10 ){ hours = '0' + hours; }
				
				minutes = $date.getMinutes();
				if( minutes < 10 ){ minutes = '0' + minutes;}
				
				seconds = $date.getSeconds();
				if( seconds < 10 ){ seconds = '0' + seconds;}
				
				return hours + ':' + minutes + ':' + seconds;
			},
			getLocalTimeForTickDataForBars:function(timeStr) {
				var timeArr = timeStr.split(":");
				
				var currentTime = new Date();
					
				var $date = new Date(2015, 1, 1, timeArr[0], timeArr[1], timeArr[2], 0); //new Date(year, month, day, hours, minutes, seconds, milliseconds)
				var timezoneOffset = $date.getTimezoneOffset() + 60 * options.realQuotesDataTimeDifference;
				$date.setMinutes ( $date.getMinutes() + timezoneOffset * (-1) );
				var hours, minutes, seconds, year, month, day;
				
				hours = $date.getHours();
				if( hours < 10 ){ hours = '0' + hours;}
				
				minutes = $date.getMinutes();
				if( minutes < 10 ){ minutes = '0' + minutes;}
				
				day = currentTime.getDate();
				if( day < 10 ){ day = '0' + day;}
				
				month = currentTime.getMonth() + 1;
				if( month < 10 ){ month = '0' + month;}
				year = currentTime.getFullYear();
				
				return year+'-'+month+'-'+day+' '+hours+':'+minutes;
			},
			changeChartPeriod:function() {
				if( $(this).text() != self.$currentPeriod ){
					
					self.$spinner.removeClass('hide');
					
					if( self.$currentPeriod == 'tick' && self.$wWampSessionPeriod ) self.$wWampSessionPeriod.close();
					
					self.$periodChartWrapper.removeClass('hide');
					self.$tickChartWrapper.addClass('hide');
					
					self.loadDataForPeriodChart( $(this).text() );
	
					self.$wWampSessionPeriod.close();
					self.subscribeToPeriodQuotes();
					
				}
				return false;
			},
			loadDataForPeriodChart:function( period ) {
				if( self.loading ) return false;
				self.loading = true;
				$.get( options.statsHistoryUrl, {symbolId: options.symbolId, period: options.periods[period]['periodVal'] }, function ( data ) {
					if( data ){
						self.$periodChart.allLabels[0].text = options.chartLabel + ', ' + period;
						self.$periodChart.dataProvider = self.parseCSV( data );
						
						self.$periodChart.categoryAxis.minPeriod = options.periods[period]['minPeriod'];
						self.$periodChart.chartCursor.categoryBalloonDateFormat = options.periods[period]['categoryBalloonDateFormat'];
						
						self.periodChartValidateData();
						
						if( self.$currentPeriod != period ){
							self.periodZoomChart();
						}else{
							self.periodRefreshZoomChart();
						}
						self.$currentPeriod = period;
						
						self.$wChangePeriodsLi.removeClass('active');
						self.$ns.find( 'li.changePeriod[data-period='+period+']' ).addClass('active');
					}
					self.loading = false;
				} );
				return false;
			},
			// this method is called when chart is first inited as we listen for "dataUpdated" event
			periodZoomChart:function() {
				// different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
				self.$periodChart.zoomToIndexes(1, self.$periodChart.dataProvider.length - 1);
			},
			periodRefreshZoomChart:function() {
				var endIndex, startIndex;
				var dataEndIndex = self.$periodChart.dataProvider.length - 1;
				self.$periodZoomEndIndex = self.$periodZoomEndIndex-1;
				if( !self.$periodZoomStartIndex ) self.$periodZoomStartIndex = 1
				
				if( self.$periodZoomEndIndex <= dataEndIndex && self.$periodZoomStartIndex <= dataEndIndex ){
					startIndex = self.$periodZoomStartIndex;
					if( dataEndIndex - self.$periodZoomEndIndex < 10 ){
						endIndex = dataEndIndex;
					}else{
						endIndex = self.$periodZoomEndIndex;
					}
				}else{
					endIndex = dataEndIndex;
					startIndex = 1;
				}
				self.$periodChart.zoomToIndexes( startIndex, endIndex );
			},
			tickRefreshZoomChart:function() {
				var endIndex, startIndex;
				var dataEndIndex = self.$tickChart.dataProvider.length - 1;
				self.$tickZoomEndIndex = self.$tickZoomEndIndex-1;
				
				if( self.$tickZoomEndIndex <= dataEndIndex && self.$tickZoomStartIndex <= dataEndIndex ){
					startIndex = self.$tickZoomStartIndex;
					if( dataEndIndex - self.$tickZoomEndIndex < 10 ){
						endIndex = dataEndIndex;
					}else{
						endIndex = self.$tickZoomEndIndex;
					}
				}else{
					endIndex = dataEndIndex;
					startIndex = 1;
				}
				self.$tickChart.zoomToIndexes( startIndex, endIndex );
			},
			
			makePeriodChart:function(lang, categoryBalloonDateFormat, minPeriod) {
				var lastClose = self.$periodChartData[self.$periodChartData.length - 1].close;
				return AmCharts.makeChart( self.$periodChartSelector, {
						"type": "serial",
						//new theme 
						"theme": 'none',
						"zoomOutText": '',
						"autoMargins": false,
						/*"marginRight": 60,
						"marginLeft": 60,
						"marginBottom": 0,
						"marginTop": 30,*/
						"marginRight": 10,
						"marginLeft": 10,
						"marginBottom": 0,
						"marginTop": 30,
						"plotAreaBorderAlpha": 1,
						"plotAreaBorderColor": '#e6e6e6',
						//new theme end
						"allLabels":[{
							"text": options.chartLabel + ', ' + self.$currentPeriod,
							"size": 15,
							"bold": true,
							/*"x": 70,
							"y": 35,*/
							"x": 70,
							"y": 35,
							"color": '#555',
						}],
						"valueAxes": [{
							//new theme 
							"position": "left",
							"id": 'v1',
							"axisAlpha": 0,
							/*"inside": false,*/
							"inside": true,
							//new theme end
							"guides": [{
								"dashLength": 2,
								"inside": false,
								"lineAlpha": 1,
								"above": true,
								"lineColor": "#2c88cb",
								"fillColor": "#2c88cb",
								"balloonColor": "#2c88cb",
								"position": "right",


								"label": lastClose,
								"balloonText": lastClose,
								"value": lastClose,
							}],
						}],
						//new theme 
						"balloon": {
							"shadowAlpha": 0,
							"fillAlpha": 1
						},
						//new theme end
						"language": lang,
						"dataDateFormat":"YYYY-MM-DD JJ:NN",
						"graphs": [{
							"id": "g1",
							"balloonText": "Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>",
							"balloonColor": "#ff2d2e",
							"closeField": "close",
							"fillColors": "#2c88cb",
							"highField": "high",
							"lineColor": "#2c88cb",
							"lineAlpha": 1,
							"lowField": "low",
							"fillAlphas": 1,
							"negativeFillColors": "#ff2d2e",
							"negativeLineColor": "#ff2d2e",
							"openField": "open",
							"title": "Price:",
							"type": "candlestick",
							"valueField": "close"
						}],
						//new theme 
						"chartScrollbar": {
							"graph": 'g1',
							"scrollbarHeight": 54,
							"backgroundAlpha": 1,
							"backgroundColor": '#fafafa',
							"selectedBackgroundAlpha": 1,
							"selectedBackgroundColor": '#cdcdcd',
							"graphFillAlpha": 1,
							"graphLineAlpha": 0.5,
							"selectedGraphFillAlpha": 1,
							"selectedGraphLineAlpha": 1,
							"autoGridCount": true,
							"color": '#fff',
							"dragIconWidth": 25
						},
						//new theme end
						"chartCursor": {
							//new theme 
							"cursorAlpha": 1,
							//new theme end
							"pan": true,
							//"valueLineEnabled": true,
							//"valueLineBalloonEnabled": true,
							"categoryBalloonDateFormat": categoryBalloonDateFormat,
							"categoryBalloonColor": "#ff2d2e",
							"cursorColor": "#ff2d2e",
						},
						"categoryField": "date",
						"categoryAxis": {
							//new theme 
							"dashLength": 5,
							"minorGridEnabled": true,
							"position": 'top',
							"labelOffset": 4,
							"axisAlpha": 0,
							"gridColor": '#e6e6e6',
							"gridAlpha": 1,
							//new theme end
							"parseDates": true,
							"minPeriod": minPeriod,
							"equalSpacing": true,
						},
						"dataProvider": self.$periodChartData,
						"export": {
							"enabled": true,
							"libs": {
								"path": options.exportLibs
							},
							"menu": [{
							  "format": "UNDEFINED",
							  "label": "",
							  "class": "export-main",
							  "menu": ["JPG", "PNG"]
							}],
						},
					});
			},
			makeTickChart:function( lang) {
				return AmCharts.makeChart( self.$tickChartSelector, {
						"type": "serial",
						//new theme 
						"theme": 'none',
						"zoomOutText": '',
						"autoMargins": false,
						/*"marginRight": 60,
						"marginLeft": 60,
						"marginBottom": 0,
						"marginTop": 30,*/
						"marginRight": 10,
						"marginLeft": 10,
						"marginBottom": 0,
						"marginTop": 30,
						"plotAreaBorderAlpha": 1,
						"plotAreaBorderColor": '#e6e6e6',
						//new theme end
						"allLabels":[{
							"text": options.chartLabel + ', ' + self.$wTickChartButton.find('a').text(),
							"size": 15,
							"bold": true,
							"x": 70,
							"y": 35,
							"color": '#555',
						}],
						"valueAxes": [{
							//new theme 
							"position": "left",
							"id": 'v1',
							"axisAlpha": 0,
							"inside": true,
							//new theme end
							"guides": [{
								"dashLength": 2,
								"inside": false,
								"lineAlpha": 1,
								"above": true,
								"lineColor": "#2c88cb",
								"fillColor": "#2c88cb",
								"balloonColor": "#2c88cb",
								"position": "right",
								"angle": 20,

								"label": 0,
								"balloonText": 0,
								"value": 0,
							},{
								"dashLength": 2,
								"inside": false,
								"lineAlpha": 1,
								"above": true,
								"lineColor": "#ff2d2e",
								"fillColor": "#ff2d2e",
								"balloonColor": "#ff2d2e",
								"position": "right",
								"angle": 20,

								"label": 0,
								"balloonText": 0,
								"value": 0,
							}],
						}],
						//new theme 
						"balloon": {
							"shadowAlpha": 0,
							"fillAlpha": 1
						},
						//new theme end
						"language": lang,
						"dataDateFormat":"JJ:NN:SS",
						"graphs": [{
							"title": "Ask",
							"lineColor": "#2c88cb",
							"lineAlpha": "1",
							"lineThickness": 2,
							"fillColors": "#2c88cb",
							"fillAlphas": 0,
							"valueField": "ask",
							"type": "line",
							"id": "ask",
							"balloonText": "Ask: [[value]]",
							"bullet": "round",
							"bulletSize": 5,
							"bulletColor": "#FFFFFF",
							"bulletBorderThickness": 1,
							"bulletBorderAlpha": 1,
							"balloonColor": "#2c88cb",
							"hideBulletsCount": 50,
							"useLineColorForBulletBorder": true
						},{
							"title": "Bid",
							"lineColor": "#ff2d2e",
							"lineAlpha": "1",
							"lineThickness": 2,
							"fillColors": "#2c88cb",
							"fillAlphas": 0,
							"valueField": "bid",
							"type": "line",
							"id": "bid",
							"balloonText": "bid: [[value]]",
							"bullet": "round",
							"bulletSize": 5,
							"bulletColor": "#FFFFFF",
							"bulletBorderThickness": 1,
							"bulletBorderAlpha": 1,
							"balloonColor": "#ff2d2e",
							"hideBulletsCount": 50,
							"useLineColorForBulletBorder": true
						}],
						//new theme 
						"chartScrollbar": {
							"graph": 'bid',
							"scrollbarHeight": 54,
							"backgroundAlpha": 1,
							"backgroundColor": '#fafafa',
							"selectedBackgroundAlpha": 1,
							"selectedBackgroundColor": '#cdcdcd',
							"graphFillAlpha": 1,
							"graphLineAlpha": 0.5,
							"selectedGraphFillAlpha": 1,
							"selectedGraphLineAlpha": 1,
							"autoGridCount": true,
							"color": '#fff',
							"dragIconWidth": 25
						},
						//new theme end
						"chartCursor": {
							//new theme 
							"cursorAlpha": 1,
							//new theme end
							"pan": true,
							//"valueLineEnabled": true,
							//"valueLineBalloonEnabled": true,
							"categoryBalloonDateFormat": "JJ:NN:SS",
							"categoryBalloonColor": "#ff2d2e",
							"cursorColor": "#ff2d2e",
						},
						"categoryField": "date",
						"categoryAxis": {
							//new theme 
							"dashLength": 5,
							"minorGridEnabled": true,
							"position": 'top',
							"labelOffset": 4,
							"axisAlpha": 0,
							"gridColor": '#e6e6e6',
							"gridAlpha": 1,
							//new theme end
							"parseDates": true,
							"minPeriod": "ss",
						},
						"dataProvider": [],
						"export": {
							"enabled": true,
							"libs": {
								"path": options.exportLibs
							},
							"menu": [{
							  "format": "UNDEFINED",
							  "label": "",
							  "class": "export-main",
							  "menu": ["JPG", "PNG"]
							}],
						},
					});
			},
			makeTradingViewChart:function( symbol, interval, container_id ) {
				var wWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
	
				if( wWidth < 700 ) var preset = "mobile";
				else var preset = "";
				
				return new TradingView.widget({
					fullscreen: false,
					allow_symbol_change: true,
					autosize: true,
					symbol: symbol,
					interval: interval,
					container_id: container_id,
					debug: false,
					datafeed: new Datafeeds.UDFCompatibleDatafeed("/services/tradingview", 30 * 1000, symbol ),
					library_path: "/services/tradingviewChartingLibrary/",
					locale: 'en',
					drawings_access: { type: 'black', tools: [ { name: "Regression Trend" } ] },
					disabled_features: ["use_localstorage_for_settings", "show_dialog_on_snapshot_ready", "header_screenshot"],
					enabled_features: ["header_resolutions"],
					charts_storage_url: 'http://saveload.tradingview.com',
					charts_storage_api_version: "1.1",
					client_id: 'tradingview.com',
					user_id: 'public_user_id',
					snapshot_url: options.tvSnapshotUrl,
					preset:  preset
				});
			},
			
			parseCSV:function(data) {
				var returnArr = [];
				if(!data) return returnArr;
				data = data.replace (/\r\n/g, "\n");
				data = data.replace (/\r/g, "\n");
				var rows = data.split("\n");
				for (var i = 0; i < rows.length; i++) {
					if (rows[i]) {
						var column = rows[i].split(";");
						var dataObject = {
							date: self.getLocalTimeForData(column[0]),
							open: column[1],
							high: column[2],
							low: column[3],
							close: column[4],
							volume: column[5]
						};
						returnArr.push(dataObject);
					}
				}
				return returnArr;
			},

		};
		self.init();
		return self;
	}
}( window.jQuery );