var wBrokerProfileWidgetOpen;
var vkAsyncInitDone = new Event('vkAsyncInitDone');
!function( $ ) {
	wBrokerProfileWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lITradeHere: 'I trade here',
			lINotTradeHere: 'I do not trade here',
			lYouAnd: 'You and',
			lTerribly: 'Terribly',
			lBad: 'Bad',
			lNormal: 'Normal',
			lGood: 'Good',
			lExcellent: 'Excellent',
		};
		var defOption = {
			selector: '.wBrokerProfileWidget',
			ins: '',
			delayTooltips: 1000,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$wCountLinkedUsersSpan = self.$ns.find( '.wCountLinkedUsers span.red_color' );
				
				self.$wMarkForm = self.$ns.find( options.ins+'.wMarkForm' );
				self.$wMarkForm.$bSubmit = self.$wMarkForm.find( '[type=submit]' );
				
				self.$ns.on( 'click', '.wLinkToUser, .wUnLinkFromUser', self.linkToUser );
				
				self.$wMarksGraph = self.$ns.find( '.wMarksGraph' );
				self.$wMarksGraph.hide();
				
				self.bindSmEvents();
				
			},
			bindSmEvents: function() {

				window.vkAsyncInit = function() {
					VK.init({apiId: options.vkAppId, onlyWidgets: true});
					VK.Widgets.Like('vk_like_custom', {type: 'button'});
					document.dispatchEvent(vkAsyncInitDone);
					/*VK.Observer.subscribe("widgets.like.liked", function f( likes ){
						self.sendSm( null, 1 );
					});

					VK.Observer.subscribe("widgets.like.unliked", function f( likes ){
						self.sendSm( null, 0 );
					});*/
				};

				/*window.fbAsyncInit = function() {
					FB.Event.subscribe('edge.create',
						function(url, html_element) {
							var currentButton = jQuery(html_element);
							if( currentButton.hasClass('sendRating') ){
								self.sendSm( 1, null );
							}
						}
					);
					FB.Event.subscribe('edge.remove',
						function(url, html_element) {
							var currentButton = jQuery(html_element);
							if( currentButton.hasClass('sendRating') ){
								self.sendSm( 0, null );
							}
						}
					);
				};*/
			},
			/*sendSm: function( fb, vk ) {
				var dataToSend = {};
				if( fb != null ){
					dataToSend.fb = fb;
				}
				if( vk != null ){
					dataToSend.vk = vk;
				}
				dataToSend.id = options.idBroker;
				$.post( options.sendSmUrl, dataToSend, function(data){}, "json" );
				
			},*/
			linkToUser:function() {
				if( options.logedInUser === false ) return false;
				if( self.loading ) return false;
				var $b = $(this);
				var $topLayer = $b.find('.activeBtnDisabled');
				$topLayer.removeClass('hide');
				var holdHtml = $b.find( 'span' ).html();
				$b.find( 'span' ).html( options.ls.lSaving );
				var url = $b.hasClass( 'wLinkToUser' ) ? options.ajaxLinkToUserURL : options.ajaxUnLinkFromUserURL;
				$.getJSON( url, {id:options.idBroker}, function ( data ) {
					$topLayer.addClass('hide');
					if( data.error ) {
						alert( data.error );
						$b.find( 'span' ).html( holdHtml );
					}
					else{
						if( $b.hasClass( 'wLinkToUser' )) {
							$b.removeClass( 'wLinkToUser' ).addClass( 'wUnLinkFromUser' );
							$b.find( 'span' ).html( options.ls.lINotTradeHere );
							var count = parseInt( self.$wCountLinkedUsersSpan.html() );
							count = count+1;
							if( self.$wCountLinkedUsersSpan.length ) self.$wCountLinkedUsersSpan.html( count );
						}
						else{
							$b.removeClass( 'wUnLinkFromUser' ).addClass( 'wLinkToUser' );
							$b.find( 'span' ).html( options.ls.lITradeHere );
							var count = parseInt( self.$wCountLinkedUsersSpan.html() );
							count = count-1;
							if( self.$wCountLinkedUsersSpan.length ) self.$wCountLinkedUsersSpan.html( count );
						}
					}
				});
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );