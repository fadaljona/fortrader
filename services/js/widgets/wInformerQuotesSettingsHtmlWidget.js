var wInformerSettingsWidgetOpen;
!function( $ ) {
	wInformerSettingsWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wInformerSettingsWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			informerParams: {},
			informerUrl: '',
			loading:false,
			windowLoaded:true,
			changedDefaultColors: false,
			hasDiffValue: false,
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				$(window).load( self.setWindowLoaded );
				
				self.$stylesList = self.$ns.find( '.stylesList' );
				self.$styleIframeWrap = self.$ns.find( '.styleIframeWrap' );
				self.$informerColorsBox = self.$ns.find( '.informerColorsBox' );
				self.$informerItemsBox = self.$ns.find( '.informerItemsBox' );
				self.$informerColumsBox = self.$ns.find( '.informerColumsBox' );
				self.$informerCodeWrap = self.$ns.find( '.informerCodeWrap' );
				self.$previewInformerWrap = self.$ns.find( '.previewInformerWrap' );
				self.$informerColorsPreview = self.$ns.find( '.informerColorsPreview' );
				self.$hideDiffValuesBox = self.$ns.find( '.hideDiffValuesBox' );
				self.$stylesAndJsFromInformer = self.$ns.find( '.stylesAndJsFromInformer' );
				
				self.$informerTitle = self.$ns.find('#informerTitle');
                self.$informerTextsSettings = self.$ns.find('.informerTextsSettings');
                self.$texts = self.$informerTextsSettings.find('.informerTexts');
				
				self.$fixedWidthBtn = self.$ns.find( '#fixedWidthBtn' );
				self.$sliderWidthBox = self.$ns.find( '.sliderWidthBox' );
				
				self.styleSelected();
				self.$stylesList.on('change', function() { self.styleSelected(); });
				self.$informerItemsBox.find('li').click( self.itemClicked );
				self.$informerColumsBox.find('li').click( self.columnClicked );
				
				self.$fixedWidthBtn.click( self.changeWidthType );

				self.$wSlider = self.$ns.find( '.sliderVal' );
				self.$wSliderWidthVal = self.$ns.find( '.sliderWidthVal' );
				self.$widthVal = self.$ns.find( '.widthVal' );
				self.initSlider();
				self.$widthVal.on('change', function() { self.changeWidth(); });
				self.$widthVal.keydown( self.filterInpWidth );
				
				$('.colorInformer').click(function(){
					$(this).parent('.section_offset').find('.box2').slideToggle('slow');
				})
				
				self.$informerTitle.on('change', function() { self.changedSettings(); });
                self.$texts.on('change', function() { self.changedSettings(); });
				
				self.$ns.find( '#hideGetInformerBtn:checkbox:checked' ).click( self.changedSettings );
				self.$ns.find( '#currenciesByCode:checkbox:checked' ).click( self.changedSettings );
				self.$ns.find( '#hideDiffValues:checkbox' ).click( self.changedSettings );
				self.$ns.find( '#disableRealTime:checkbox' ).click( self.changedSettings );
				

				new Clipboard('.copyCode1');
				new Clipboard('.copyCode2');

				$(window).scroll( self.pageScrolled );
				
			},
			pageScrolled:function(){
				clearTimeout(self.scrollTimeout);
				self.scrollTimeout = setTimeout(function(){
					var topWindowsOffset = self.$informerColorsPreview.offset().top - $(window).scrollTop() + self.$informerColorsPreview.outerHeight()*1.75;
					var bottomWindowsOffset = self.$informerColorsPreview.offset().top - $(window).scrollTop() - window.innerHeight - self.$informerColorsPreview.outerHeight()*1.25;

					var $visibleSection = self.findVisibleSection();

					if( $visibleSection ){
						if( topWindowsOffset < 0 ){
							$visibleSection.prev('section').after(self.$informerColorsPreview);
	
						}else if( bottomWindowsOffset > 0 ){
							$visibleSection.prev('section').after(self.$informerColorsPreview);
						}
					}
					
				},10);
			},
			findVisibleSection: function(){
				var el = false;
				self.$ns.find('>section').each(function( index ) {
					if( (index > 0) && ($(this).offset().top - $(window).scrollTop() > 0) ){
						el = $(this);
						return false;
					}
				});
				return el;
			},
			disable: function(){
				self.$ns.append( self.spinner );
			},
			enable: function(){
				self.$ns.find('.spinner-wrap').remove();
			},
			setWindowLoaded: function(){
				self.windowLoaded = true;
			},
			styleSelected: function(){
				self.updateInformer();

				/*if( self.loading ) return false;
				self.loading = true;
				self.disable();
				
				self.informerParams = {};
				self.informerParams['st'] = self.$stylesList.val();
				self.informerParams['cat'] = options.catId;
				self.informerParams['mult'] = 1;
				self.informerParams['showGetBtn'] = 0;
				self.informerParams['w'] = 0;
				
				self.informerParams['items'] = options.defaultItems;
				self.informerParams['columns'] = options.defaultColumns;
				
				self.informerParams['demoPreview'] = 1;
				
                self.informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(self.informerParams);
                
                $.get( self.informerUrl, function( data ) {
                    self.$styleIframeWrap.html( data );
                });*/
				
                /*var frameCode = '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:100%" scrolling="no" src="' + self.informerUrl + '"></iframe>';
                self.$styleIframeWrap.html( frameCode );*/
				
				self.$informerColorsPreview.html( '' );

				
				self.$informerColorsBox.html('');
                self.changedDefaultColors = false;
                self.setDefaultStyleColors();
				
				//self.$styleIframeWrap.find( 'iframe' ).load( self.correctIframeHeight );
								
			},
			setDefaultStyleColors: function(){
				$.getJSON( options.getInformerColors, {st:self.$stylesList.val()}, function ( data ) {
				
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$informerColorsBox.html( data.data );
						if( self.windowLoaded ){
							self.initColorPicker();
						}else{
							$(window).load( self.initColorPicker );
						}
						if( data.oneColumn == '1' ){
							self.$ns.find('.informerColumsSection').addClass('hide');
						}else{
							self.$ns.find('.informerColumsSection').removeClass('hide');
						}
						if( data.width == '0' ){
							self.$fixedWidthBtn.prop( 'checked', false );
						}else{
							self.$fixedWidthBtn.prop( 'checked', false );
							self.$widthVal.val( data.width );
							self.$wSliderWidthVal.slider( "value", data.width );
						}
						if( data.hasDiffValue == '1' ){
							self.hasDiffValue = true;
							self.$hideDiffValuesBox.slideDown('slow');
							self.$ns.find( '#hideDiffValues').prop( 'checked', true );
						}else{
							self.hasDiffValue = false;
							self.$hideDiffValuesBox.slideUp('slow');
						}
						self.changeWidthType(false);
					}
					self.changedSettings();
					self.loading = false;
					self.enable();
				});
			},
			
			correctIframeHeight:function() {
				var iframeHeight = this.contentWindow.document.body.scrollHeight + "px";
				this.style.height = iframeHeight;
				self.setDefaultStyleColors();
			},
			initColorPicker: function(){
				if( self.$ns.find(".colorSelector").length){
					self.$ns.find(".colorSelector").each(function(){
						var $this = $(this);

						$this.css('background-color', $this.attr('data-bg') );
						$this.attr({'data-cpp-color': $this.attr('data-bg')});
						
						$this.closest('.change_color_input').find('.col3 input').val( $this.attr('data-bg') );
						
						$this.colorpickerplus();
						
						$this.on('changeColor', function(e, color){					
							if(color!=null){
								$(this).css('background-color', color);
								$(this).closest('.change_color_input').find('.col3 input').val( color );
							}
						});
						$this.on('hidePicker', function(e, color){					
							self.changedDefaultColors = true;
							self.showInformerColorsPreview();
							self.changedSettings();
						});
						
					});
				}
			},
			showInformerColorsPreview: function(){

				/*self.updateInformer();

				var informerColorsPreviewParams = {};
				informerColorsPreviewParams['st'] = self.$stylesList.val();
				informerColorsPreviewParams['cat'] = options.catId;
		
			
				informerColorsPreviewParams['mult'] = 1;
				informerColorsPreviewParams['showGetBtn'] = 0;
				informerColorsPreviewParams['w'] = 0;

				informerColorsPreviewParams['columns'] = options.defaultColumns;
				informerColorsPreviewParams['items'] = options.defaultItems;
				
				informerColorsPreviewParams['colors'] = self.getColors();
				
				if( self.hasDiffValue ){
					if( self.$ns.find( '#hideDiffValues:checkbox:checked' ).length != 0 ){
						informerColorsPreviewParams['hideDiff'] = 1;
					}
				}
				
                var informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(informerColorsPreviewParams);
                
                $.get( informerUrl, function( data ) {
                    self.$informerColorsPreview.html( data );
                });*/
				
				/*var frameCode = '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:100%;margin-top:20px;" scrolling="no" src="' + informerUrl + '"></iframe>';
				
				self.$informerColorsPreview.html( frameCode );
				
				self.$informerColorsPreview.find( 'iframe' ).load( self.correctinformerColorsPreviewIframeHeight );*/
			},
			correctinformerColorsPreviewIframeHeight:function() {
				var iframeHeight = this.contentWindow.document.body.scrollHeight + "px";
				this.style.height = iframeHeight;
			},
			encodeQueryData:function( informerParams, frame ){
				frame = typeof frame !== 'undefined' ?  frame : false;


                if( !frame ){
					informerParams['lType'] = 'html';
				}
				var ret = [];
				for ( var param in informerParams ){
					ret.push( encodeURIComponent( param ) + "=" + encodeURIComponent( informerParams[param] ) );
				}
				return ret.join("&");
			},
			changedSettings:function() {	
				self.disable();
				self.updateInformer();
			},
			updateInformer:function() {				
				self.informerParams = {};
				self.informerParams['st'] = self.$stylesList.val();
				self.informerParams['cat'] = options.catId;
                
                self.informerParams['title'] = self.$informerTitle.val();
                if (self.$texts.length) {
                    var textsArr = {};
                    self.$texts.each(function(){
                        if (!$(this).closest('.box_informer').hasClass('hide')) {
                            var name = $(this).attr('name').replace(/texts\[/, '').replace(/\]/, '');
                            textsArr[name] = $(this).val();
                        }
                    });
                    self.informerParams['texts'] = JSON.stringify(textsArr);
                }
				

				if( self.$wSlider != undefined ){
					self.informerParams['mult'] = self.$wSlider.slider("value");
				}else{
					self.informerParams['mult'] = 1;
				}
					
				
				if( self.$ns.find( '#hideGetInformerBtn:checkbox:checked' ).length == 0 ){
					self.informerParams['showGetBtn'] = 1;
				}else{
					self.informerParams['showGetBtn'] = 0;
				}
				
				if( self.$ns.find( '#fixedWidthBtn:checkbox:checked' ).length == 0 ){
					self.informerParams['w'] = 0;
				}else{
					self.informerParams['w'] = self.$widthVal.val();
				}
				if( self.hasDiffValue ){
					if( self.$ns.find( '#hideDiffValues:checkbox:checked' ).length != 0 ){
						self.informerParams['hideDiff'] = 1;
					}
				}
				if( self.$ns.find( '#disableRealTime:checkbox:checked' ).length != 0 ){
					self.informerParams['disableRealTime'] = 1;
				}
				if( options.catType == 'currencies' ){
					if( self.$ns.find( '#currenciesByCode:checkbox:checked' ).length == 0 ){
						self.informerParams['codes'] = 0;
					}else{
						self.informerParams['codes'] = 1;
					}
				}
				
				self.informerParams['colors'] = self.getColors();
				self.getSymbols();
				self.getColumns();
				
                self.informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(self.informerParams);
				
                $.get( self.informerUrl, function( data ) {
					self.$stylesAndJsFromInformer.html( data.substr(0, data.search( />/i ) +1 ) );

					self.$informerColorsPreview.html( data.substr( data.search( />/i ) +1 ) );

					
					self.$informerCodeWrap.val( data );


					setTimeout(function(){
						if( self.informerParams['w'] == 0 ){
							var width = '100%';
						}else{
							var width = self.informerParams['w'] + 'px';
						}
						delete self.informerParams['lType']
						informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(self.informerParams, true);

						self.$informerColorsPreview.css({
							height: 'auto',
						});

						self.$informerColorsPreview.css({
							height: self.$informerColorsPreview.outerHeight() + 'px'
						});

						self.$ns.find( '.informerCodeWrap2' ).val( '<iframe style="width:'+width+';border:0;overflow:hidden;background-color:transparent;height:'+self.$informerColorsPreview.outerHeight()+'px" scrolling="no" src="' + informerUrl + '"></iframe>' );
						self.enable();
					}, 500);
					
                });
					
				/*var frameCode = '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:100%" scrolling="no" src="' + self.informerUrl + '"></iframe>';
				
				self.$previewInformerWrap.html( frameCode )
				self.$previewInformerWrap.find( 'iframe' ).load( self.correctIframeHeightForPreview );*/

				return false;
			},
			correctIframeHeightForPreview:function( ) {	
				var iframeHeight = this.contentWindow.document.body.scrollHeight + "px";
				this.style.height = iframeHeight;
				if( self.informerUrl != '' ){
					if( self.informerParams['w'] == 0 ){
						var width = '100%';
					}else{
						var width = self.informerParams['w'] + 'px';
					}
					self.$informerCodeWrap.val( '<iframe style="width:'+width+';border:0;overflow:hidden;background-color:transparent;height:'+iframeHeight+'" scrolling="no" src="' + self.informerUrl + '"></iframe>' );
				}
				self.enable();
			},
			getSymbols:function() {
				
				var $els = self.$informerItemsBox.find('li.columnActive')
				var strVal = '';
				$els.each(function(index, element){
					var delimiter = '';
					if( index > 0 ) delimiter = ',';
					strVal = strVal + delimiter + $(this).data('item');
				});
				self.informerParams['items'] = strVal;
			},
			getColumns:function() {	
				if( self.$ns.find('.informerColumsSection').hasClass('hide') ){
					self.informerParams['columns'] = '';
				}else{
					var $els = self.$informerColumsBox.find('li.columnActive')
					var strVal = '';
					$els.each(function(index, element){
						var delimiter = '';
						if( index > 0 ) delimiter = ',';
						strVal = strVal + delimiter + $(this).data('colum');
					});
					self.informerParams['columns'] = strVal;
				}
			},
			getColors:function() {	
				if( !self.changedDefaultColors ) return false;
				var strVal = '';
				self.$informerColorsBox.find( 'input' ).each(function(index, element){
					var delimiter = '';
					var val = $(this).val();
					if( index > 0 ) delimiter = ',';
					if( val.charAt(0) === '#' ) val = val.substr(1);
					strVal = strVal + delimiter + $(this).attr('name') + '=' + val;
				});
				return strVal;	
			},
			itemClicked: function(){
				$(this).toggleClass('columnActive');
				self.changedSettings();
				return false;
			},
			columnClicked: function(){
                $(this).toggleClass('columnActive');
                self.openHideColumnTitles();
				self.changedSettings();
				return false;
            },
            openHideColumnTitles: function() {
                self.$informerColumsBox.find('li').each(function(){
                    var $boxOfInput = self.$informerTextsSettings.find('[name="texts[' + $(this).attr('data-colum') + ']"]').closest('.box_informer');
                    if ($(this).hasClass('columnActive')) {
                        $boxOfInput.removeClass('hide');
                    } else {
                        $boxOfInput.addClass('hide');
                    }
                });
            },
			initSlider:function() {		
				self.$wSlider.slider({
					min : 0.6,
					max : 1.5,
					step: 0.01,
					value: 1,
					stop: function( event, ui ) {
						self.changedSettings();
					},
				});
				
				self.$wSliderWidthVal.slider({
					min : options.minWidth,
					max : options.maxWidth,
					step: 10,
					value: 320,
					stop: function( event, ui ) {
						self.changedSettings();
					},
					slide: function( event, ui ) {
						self.$widthVal.val( ui.value );
					},
				});
			},
			changeWidth: function(){
				var value = parseInt( self.$widthVal.val() );
				if( value < options.minWidth ){
					value = options.minWidth;
					self.$widthVal.val( value );
				}else if( value > options.maxWidth ){
					value = options.maxWidth;
					self.$widthVal.val( value );
				}
				self.$wSliderWidthVal.slider( "value", value );
				self.changedSettings();
			},
			filterInpWidth: function(e){
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
					// Allow: Ctrl+A
					(e.keyCode == 65 && e.ctrlKey === true) ||
					// Allow: Ctrl+C
					(e.keyCode == 67 && e.ctrlKey === true) ||
					// Allow: Ctrl+X
					(e.keyCode == 88 && e.ctrlKey === true) ||
					// Allow: home, end, left, right
					(e.keyCode >= 35 && e.keyCode <= 39)) {
					// let it happen, don't do anything
					return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			},
			changeWidthType: function( marker = true ){
				if( self.$ns.find( '#fixedWidthBtn:checkbox:checked' ).length == 0 ){
					self.$sliderWidthBox.slideUp('slow');
				}else{
					self.$sliderWidthBox.slideDown('slow');
				}
				if( marker ) self.changedSettings();
			}
			
			

			
			
		};
		self.init();
		return self;
	}
}( window.jQuery );