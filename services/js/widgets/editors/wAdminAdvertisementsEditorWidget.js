var wAdminAdvertisementsEditorWidgetOpen;
!function( $ ) {
	wAdminAdvertisementsEditorWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminAdvertisementsEditorWidget',
			type: 'Text',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		switch( options.type ) {
			case 'Text':{ options.URLReload = '/admin/advertisement/list'; break; }
			case 'Image':{ options.URLReload = '/admin/advertisement/listBanners'; break; }
			case 'Button':{ options.URLReload = '/admin/advertisement/listButtons'; break; }
			case 'Offer':{ options.URLReload = '/admin/advertisement/listOffers'; break; }
		}
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$alert = self.$ns.find( options.ins+".wAlert" );
				self.$bAdd = self.$ns.find( options.ins+".wAdd" );
				self.$selectCampaign = self.$ns.find( options.ins+".wSelectCampaign" );
				self.wList = options.ns.wAdminAdvertisementsListWidget;
				self.wForm = options.ns.wAdminAdvertisementFormWidget;
				
				self.$bAdd.click( self.showAdd );
				self.$selectCampaign.change( self.onChangeCampaign );
				if( self.wList && self.wForm ) {
					self.wList.onSelectionChanged.push( self.onListSelectionChanged );
					self.wForm.onAdd.push( self.onFormAdd );
					self.wForm.onEdit.push( self.onFormEdit );
					self.wForm.onDelete.push( self.onFormDelete );
				}
			},
			showAdd:function() {
				var showed = self.wForm.showAdd();
				showed ? self.$alert.slideUp() : self.$alert.slideDown();
				if( showed ) {
					self.wList.resetSelection();
				}
				return false;
			},
			onChangeCampaign:function() {
				var idCampaign = self.$selectCampaign.val();
				if( idCampaign ) {
					var url = yiiBaseURL + options.URLReload + '?idCampaign=' + idCampaign;
					var sortField = commonLib.getVar( 'sortField' );
					var sortType = commonLib.getVar( 'sortType' );
					if( sortField ) url += '&sortField=' + sortField;
					if( sortType ) url += '&sortType=' + sortType;
					document.location.assign( url );
				}
			},
			onListSelectionChanged:function() {
				var $selection = self.wList.getSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					var id = $row.attr( 'idAdvertisement' );
					self.wForm.showEdit( id );
				}
				else{
					self.wForm.hide();
				}
			},
			onFormAdd:function( id ) {
				self.wList.add( id );
			},
			onFormEdit:function( id ) {
				self.wList.update( id );
			},
			onFormDelete:function( id ) {
				self.wList.delete( id );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );