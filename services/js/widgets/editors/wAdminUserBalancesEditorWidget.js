var wAdminUserBalancesEditorWidgetOpen;
!function( $ ) {
	wAdminUserBalancesEditorWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminUserBalancesEditorWidget',
			URLLoadChanges: '/admin/userBalance/ajaxLoadChanges',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var htmlLoadChanges = '';
		
		var self = {
			idUserLoadedChanges: 0,
			init:function() {
				self.$ns = $( options.selector );
				self.wList = options.ns.wAdminUserBalancesListWidget;
				self.wForm = options.ns.wAdminChangeUserBalanceFormWidget;
				self.$wChanges = self.$ns.find( '.wAdminUserBalanceChanges' );
				htmlLoadChanges = self.$wChanges.html();
				self.clearChanges();
				
				self.wList.onSelectionChanged.push( self.onListSelectionChanged );
				if( self.wForm ) {
					self.wForm.onEdit.push( self.onFormEdit );
				}
				self.$wChanges.on( 'click', '.pagination a', self.onChangesPaginationClick );
			},
			onListSelectionChanged:function() {
				var $selection = self.wList.getSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					var id = $row.attr( 'idUser' );
					if( self.wForm ) self.wForm.showEdit( id );
					self.loadChanges( id );
				}
				else{
					if( self.wForm ) self.wForm.hide();
					self.clearChanges();
				}
			},
			onFormEdit:function( id ) {
				self.wList.update( id );
				self.clearChanges();
			},
			loadChanges:function( id ) {
				self.$wChanges.html( htmlLoadChanges );
				self.$wChanges.load( yiiBaseURL + options.URLLoadChanges, {id:id} );
				self.idUserLoadedChanges = id;
			},
			clearChanges:function() {
				self.$wChanges.html( '' );
				self.idUserLoadedChanges = 0;
			},
			onChangesPaginationClick:function() {
				var $link = $(this);
				if( !$link.parent().hasClass( 'active' ) &&  !$link.parent().hasClass( 'disabled' )) {
					var href = $link.attr( 'href' );
					self.$wChanges.html( htmlLoadChanges );
					$.scrollTo( self.$wChanges, options.scrollSpeed, { offset: options.scrollOffset } );
					self.$wChanges.load( href, {id:self.idUserLoadedChanges} );
				}
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );