var wTradingAccountsEditorWidgetOpen;
!function( $ ) {
	wTradingAccountsEditorWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wTradingAccountsEditorWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
	
				self.wList = options.ns.wContestMembersTAListWidget;
				self.wForm = options.ns.wContestMemberFormWidget;
				
				if( self.wList && self.wForm ) {
					self.wList.onSelectionChanged.push( self.onListSelectionChanged );
					self.wForm.onEdit.push( self.onFormEdit );
					self.wForm.onDelete.push( self.onFormDelete );
				}
			},
			showAdd:function() {
				var showed = self.wForm.showAdd();
				if( showed ) {
					self.wList.resetSelection();
				}
				return false;
			},
			onListSelectionChanged:function() {
				var $selection = self.wList.getSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					var id = $row.attr( 'idMember' );
					self.wForm.showEdit( id );
				}
				else{
					self.wForm.hide();
				}
			},
			onFormEdit:function( id ) {
				self.wList.update( id );
			},
			onFormDelete:function( id ) {
				self.wList.delete( id );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );