var wAdminAdvertisementCampaignsEditorWidgetOpen;
!function( $ ) {
	wAdminAdvertisementCampaignsEditorWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminAdvertisementCampaignsEditorWidget',
			URLReload: '/admin/advertisementCampaign/list',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$alert = self.$ns.find( options.ins+".wAlert" );
				self.$bAdd = self.$ns.find( options.ins+".wAdd" );
				self.$selectBroker = self.$ns.find( options.ins+".wSelectBroker" );
				self.wList = options.ns.wAdminAdvertisementCampaignsListWidget;
				self.wForm = options.ns.wAdminAdvertisementCampaignFormWidget;
				
				self.$bAdd.click( self.showAdd );
				self.$selectBroker.change( self.onChangeBroker );
				if( self.wList ) {
					self.wList.onSelectionChanged.push( self.onListSelectionChanged );
				}
				if( self.wForm ) {
					self.wForm.onAdd.push( self.onFormAdd );
					self.wForm.onEdit.push( self.onFormEdit );
					self.wForm.onDelete.push( self.onFormDelete );
				}
			},
			showAdd:function() {
				var showed = self.wForm.showAdd();
				showed ? self.$alert.slideUp() : self.$alert.slideDown();
				if( showed ) {
					self.wList.resetSelection();
				}
				return false;
			},
			onChangeBroker:function() {
				var idBroker = self.$selectBroker.val();
				if( idBroker ) {
					document.location.assign( yiiBaseURL + options.URLReload + '?idBroker=' + idBroker );
				}
			},
			onListSelectionChanged:function() {
				var $selection = self.wList.getSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					var id = $row.attr( 'idCampaign' );
					self.wForm.showEdit( id );
				}
				else{
					self.wForm.hide();
				}
			},
			onFormAdd:function( id ) {
				self.wList.add( id );
			},
			onFormEdit:function( id ) {
				self.wList.update( id );
			},
			onFormDelete:function( id ) {
				self.wList.delete( id );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );