var wAdminCalendarEventsEditorWidgetOpen;
!function( $ ) {
	wAdminCalendarEventsEditorWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminCalendarEventsEditorWidget',
			URLReload: '/admin/calendarEvent/list',
			URLFront: '/calendarEvent/single',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$alert = self.$ns.find( options.ins+".wAlert" );
				self.$bAdd = self.$ns.find( options.ins+".wAdd" );
				self.wList = options.ns.wAdminCalendarEventsListWidget;
				self.wForm = options.ns.wAdminCalendarEventFormWidget;
				self.wFront = $( '.wAdminNavbarWidget .wFront a' );
				self.$selectCountry = self.$ns.find( options.ins+".wSelectCountry" );

				self.$bAdd.click( self.showAdd );
				
				self.$selectCountry.change( self.onChangeFilter );
				self.wList.onSelectionChanged.push( self.onListSelectionChanged );
				self.wForm.onAdd.push( self.onFormAdd );
				self.wForm.onEdit.push( self.onFormEdit );
				self.wForm.onDelete.push( self.onFormDelete );
			},
			onChangeFilter:function() {
				var request = "";
				
				var idCountry = self.$selectCountry.val();
				if( idCountry ) {
					request = request + ( request.length ? '&' : '' ) + 'idCountry=' + idCountry;
				}
				
				var url = yiiBaseURL + options.URLReload;
				if( request.length ) {
					url = url + '?' + request;
				}
				
				document.location.assign( url );
			},
			showAdd:function() {
				var showed = self.wForm.showAdd();
				showed ? self.$alert.slideUp() : self.$alert.slideDown();
				if( showed ) {
					self.wList.resetSelection();
				}
				self.wFront.attr( 'href', yiiBaseURL );
				return false;
			},
			onListSelectionChanged:function() {
				var $selection = self.wList.getSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					var id = $row.attr( 'idEvent' );
					self.wForm.showEdit( id );
					self.wFront.attr( 'href', yiiBaseURL + options.URLFront + '?id=' + id );
				}
				else{
					self.wForm.hide();
					self.wFront.attr( 'href', yiiBaseURL );
				}
			},
			onFormAdd:function( id ) {
				self.wList.add( id );
			},
			onFormEdit:function( id ) {
				self.wList.update( id );
			},
			onFormDelete:function( id ) {
				self.wList.delete( id );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );