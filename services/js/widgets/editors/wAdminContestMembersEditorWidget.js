var wAdminContestMembersEditorWidgetOpen;
!function( $ ) {
	wAdminContestMembersEditorWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminContestMembersEditorWidget',
			URLReload: '/admin/contestMember/list',
			URLFront: '/contestMember/single',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$alert = self.$ns.find( options.ins+".wAlert" );
				self.$bAdd = self.$ns.find( options.ins+".wAdd" );
				self.$selectContest = self.$ns.find( options.ins+".wSelectContest" );
				self.wList = options.ns.wAdminContestMembersListWidget;
				self.wForm = options.ns.wAdminContestMemberFormWidget;
				self.wFront = $( '.wAdminNavbarWidget .wFront a' );
				
				self.$bAdd.click( self.showAdd );
				self.$selectContest.change( self.onChangeContest );
				if( self.wList && self.wForm ) {
					self.wList.onSelectionChanged.push( self.onListSelectionChanged );
					self.wForm.onAdd.push( self.onFormAdd );
					self.wForm.onEdit.push( self.onFormEdit );
					self.wForm.onDelete.push( self.onFormDelete );
				}
			},
			showAdd:function() {
				var showed = self.wForm.showAdd();
				showed ? self.$alert.slideUp() : self.$alert.slideDown();
				if( showed ) {
					self.wList.resetSelection();
				}
				self.wFront.attr( 'href', yiiBaseURL );
				return false;
			},
			onChangeContest:function() {
				var idContest = self.$selectContest.val();
				if( idContest ) {
					document.location.assign( yiiBaseURL + options.URLReload + '?idContest=' + idContest );
				}
			},
			onListSelectionChanged:function() {
				var $selection = self.wList.getSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					var id = $row.attr( 'idMember' );
					self.wForm.showEdit( id );
					self.wFront.attr( 'href', yiiBaseURL + options.URLFront + '?id=' + id );
				}
				else{
					self.wForm.hide();
					self.wFront.attr( 'href', yiiBaseURL );
				}
			},
			onFormAdd:function( id ) {
				self.wList.add( id );
			},
			onFormEdit:function( id ) {
				self.wList.update( id );
			},
			onFormDelete:function( id ) {
				self.wList.delete( id );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );