var wAdminMailingsEditorWidgetOpen;
!function( $ ) {
	wAdminMailingsEditorWidgetOpen = function( options ) {
		var defLS = {
			lRestartConfirm: 'Restart mailing?',
			lRunConfirm: 'Run mailing?',
			lStageInitialization: 'Initialization',
			lStageFinish: 'Finish',
			lStageSMSMailing: 'SMS mailing',
			lStageEmailMailing: 'EMail mailing',
			lError: 'Error!',
			lConnectionError: 'Connection error! Repeat...',
			lPause: 'Pause',
			lStop: 'Stop',
			lRestart: 'Restart',
			lStoping: 'Stoping...',
			lRestarting: 'Restarting...',
			lStoped: 'Mailing stoped!',
			lQueryRestart: 'Mailing progress: (%stage%) %offset% / %count% \r\nResume?',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminMailingsEditorWidget',
			ls: defLS,
			ajaxRunURL: '/admin/mailing/ajaxRun',
			delayAlertClose: 2000,
			delayRunRequests: 1000,
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			idRuned: 0,
			runed:false,
			stoping: false,
			pausing: false,
			restarting: false,
			holdedData: {},
			init:function() {
				self.$ns = $( options.selector );
				self.$alert = self.$ns.find( options.ins+".wAlert" );
				self.$bAdd = self.$ns.find( options.ins+".wAdd" );
				
				self.$wRunAlert = self.$ns.find( options.ins+".wRunAlert" );
				self.$wRunProgress = self.$ns.find( options.ins+".wRunProgress" );
				self.$wTextProgress = self.$ns.find( options.ins+".wTextProgress" );
				self.$wRunStage = self.$ns.find( options.ins+".wRunStage" );
				self.$wRunWarnings = self.$ns.find( options.ins+".wRunWarnings" );
				self.$wResume = self.$ns.find( options.ins+".wResume" );
				self.$wPause = self.$ns.find( options.ins+".wPause" );
				self.$wStop = self.$ns.find( options.ins+".wStop" );
				self.$wRestart = self.$ns.find( options.ins+".wRestart" );
				
				self.wList = options.ns.wAdminMailingsListWidget;
				self.wForm = options.ns.wAdminMailingFormWidget;
				
				self.$wRunAlert.hide();
				self.$wRunWarnings.hide();
				
				self.$bAdd.click( self.showAdd );
				
				self.wList.onSelectionChanged.push( self.onListSelectionChanged );
				self.wForm.onAdd.push( self.onFormAdd );
				self.wForm.onEdit.push( self.onFormEdit );
				self.wForm.onDelete.push( self.onFormDelete );
				
				self.$wResume.click( self.resume );
				self.$wPause.click( self.pause );
				self.$wStop.click( self.stop );
				self.$wRestart.click( self.restart );
			},
			showAdd:function() {
				if( self.runed ) return;
				var showed = self.wForm.showAdd();
				if( showed ) {
					self.$alert.slideUp();
					self.wList.resetSelection();
				}
				return false;
			},
			disable:function() {
				self.$wResume.attr( 'disabled', 'disabled' );
				self.$wPause.attr( 'disabled', 'disabled' );
				self.$wStop.attr( 'disabled', 'disabled' );
				self.$wRestart.attr( 'disabled', 'disabled' );
			},
			enable:function() {
				self.$wResume.removeAttr( 'disabled' );
				self.$wPause.removeAttr( 'disabled' );
				self.$wStop.removeAttr( 'disabled' );
				self.$wRestart.removeAttr( 'disabled' );
				
				self.$wPause.html( options.ls.lPause );
				self.$wStop.html( options.ls.lStop );
				self.$wRestart.html( options.ls.lRestart );
				
				if( self.pausing ) {
					self.$wResume.show();
					self.$wPause.hide();
				}
				else{
					self.$wResume.hide();
					self.$wPause.show();
				}
			},
			onListSelectionChanged:function() {
				if( self.runed ) {
					self.wList.resetSelection();
					return;
				}
				var $selection = self.wList.getSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					var id = $row.attr( 'idMailing' );
					self.wForm.showEdit( id );
				}
				else{
					self.wForm.hide();
				}
			},
			setRunProgress:function( data ) {
				function progressEval( data ) {
					if( !parseInt( data.count )) return 100;
					return Math.ceil( parseInt( data.offset ) / parseInt( data.count ) * 100);
				}
				var progress = progressEval( data );
				
				switch( data.stage ) {
					case 'initialization':
					case 'resume':
					case 'finish': {
						self.$wTextProgress.html( "*" );
						break;
					}
					default:{
						self.$wTextProgress.html( data.offset * data.limit + " / " + data.count * data.limit );
					}
				}
				
				self.$wRunProgress.find( '.bar' ).css( 'width', progress+'%' );
			},
			setRunStage:function( text, className ) {
				self.$wRunStage.html( text );
				self.$wRunStage.removeClass( 'text-success text-error' );
				if( className ) self.$wRunStage.addClass( className );
			},
			addWarning: function( text ) {
				self.$wRunWarnings.slideDown();
				self.$wRunWarnings.find( 'ol' ).append( '<li>'+text+'</li>' );
			},
			clearWarnings: function( text ) {
				self.$wRunWarnings.slideUp();
				self.$wRunWarnings.find( 'ol' ).html( '' );
			},
			onProgressStop:function() {
				setTimeout( function () {
					self.$wRunAlert.slideUp();
					self.runed = false;
					self.stoping = false;
					self.pausing = false;
					self.wList.update( self.idRuned );
				}, options.delayAlertClose );
			},
			onProgressRun:function( data ) {
				self.holdedData = data;
				
				if( self.stoping ) {
					self.pausing = false;
					self.setRunStage( options.ls.lStoped, 'text-error' );
					self.onProgressStop();
					return;
				}
				
				if( self.restarting ) {
					self.pausing = false;
					self.restarting = false;
					self.enable();
					self.onProgressRun({
						stage: 'initialization',
						restart: '1',
					});
					return;
				}
				
				if( self.pausing ) {
					self.setRunStage( options.ls.lPause );
					self.enable();
					return;
				}
				
				try{
					if( !data ) throw 'Empty data!';
					if( data.error ) throw data.error;
					if( data.warnings ) {
						$.each( data.warnings, function () {
							self.addWarning( this );
						});
					}
					
					switch( data.stage ) {
						case 'initialization':{
							self.setRunProgress( data );
							self.setRunStage( options.ls.lStageInitialization );
							break;
						}
						case 'resume':{
							var lQuery = options.ls.lQueryRestart;
							var lQuery = lQuery.replace( '%stage%', data.progressData.stage );
							var lQuery = lQuery.replace( '%offset%', data.progressData.offset * data.progressData.limit );
							var lQuery = lQuery.replace( '%count%', data.progressData.count * data.progressData.limit );
							if( confirm( lQuery )) {
								self.onProgressRun( data.progressData );
							}
							else{
								self.restarting = true;
								self.onProgressRun( self.holdedData );
							}
							return;
							break;
						}
						case 'SMSMailing':{	
							self.setRunProgress( data );
							self.setRunStage( options.ls.lStageSMSMailing );
							break;
						}
						case 'EmailMailing':{	
							self.setRunProgress( data );
							self.setRunStage( options.ls.lStageEmailMailing );
							break;
						}
						case 'finish':{
							self.setRunProgress( data );
							self.setRunStage( options.ls.lStageFinish, 'text-success' );
							throw '';
							break;
						}
						default: {
							throw 'Unknown stage '+data.stage+'!';
							break;
						}
					}
					
					setTimeout( function () {
						var sendData = {
							id: self.idRuned,
							stage: data.stage,
						};
						if( typeof data.offset != 'undefined' ) sendData.offset = data.offset;
						if( typeof data.restart != 'undefined' ) sendData.restart = data.restart;
						var jqxhr = $.getJSON( yiiBaseURL+options.ajaxRunURL, sendData, self.onProgressRun );
						jqxhr.fail( function() {
							self.setRunStage( options.ls.lConnectionError );
							setTimeout( function () {
								self.onProgressRun( data );
							}, options.delayRunRequests * 5 );
						});
					}, options.delayRunRequests );
				}
				catch( message ) {
					if( message ) {
						alert( message );
						self.setRunStage( options.ls.lError, 'text-error' );
					}
					self.onProgressStop();
				}
			},
			run:function( id ) {
				if( self.runed ) return false;
				
				self.idRuned = id;
				
				self.clearWarnings();
				self.$wRunAlert.slideDown();
				
				$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear' });
				
				self.runed = true;
				
				self.enable();
				
				self.onProgressRun({
					stage: 'initialization',
				});
			},
			resume:function() {
				self.pausing = false;
				self.enable();
				self.onProgressRun( self.holdedData );
			},
			pause:function() {
				self.disable();
				self.$wPause.html( options.ls.lStoping );
				self.pausing = true;
			},
			stop:function() {
				self.disable();
				self.$wStop.html( options.ls.lStoping );
				self.stoping = true;
				if( self.pausing ) {
					self.onProgressRun( self.holdedData );
				}
			},
			restart:function() {
				if( !confirm( options.ls.lRestartConfirm )) return;
				
				self.disable();
				self.$wRestart.html( options.ls.lRestarting );
				self.restarting = true;
				if( self.pausing ) {
					self.onProgressRun( self.holdedData );
				}
			},
			onFormAdd:function( id ) {
				self.wList.add( id );
				if( confirm( options.ls.lRunConfirm )) self.run( id );
			},
			onFormEdit:function( id ) {
				self.wList.update( id );
				if( confirm( options.ls.lRunConfirm )) self.run( id );
			},
			onFormDelete:function( id ) {
				self.wList.delete( id );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );