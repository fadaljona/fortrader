var wAdminTradePlatformsEditorWidgetOpen;
!function( $ ) {
	wAdminTradePlatformsEditorWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminTradePlatformsEditorWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$alert = self.$ns.find( options.ins+".wAlert" );
				self.$bAdd = self.$ns.find( options.ins+".wAdd" );
				self.wList = options.ns.wAdminTradePlatformsListWidget;
				self.wForm = options.ns.wAdminTradePlatformFormWidget;
				
				self.$bAdd.click( self.showAdd );
				
				self.wList.onSelectionChanged.push( self.onListSelectionChanged );
				self.wForm.onAdd.push( self.onFormAdd );
				self.wForm.onEdit.push( self.onFormEdit );
				self.wForm.onDelete.push( self.onFormDelete );
			},
			showAdd:function() {
				var showed = self.wForm.showAdd();
				showed ? self.$alert.slideUp() : self.$alert.slideDown();
				if( showed ) {
					self.wList.resetSelection();
				}
				return false;
			},
			onListSelectionChanged:function() {
				var $selection = self.wList.getSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					var id = $row.attr( 'idPlatform' );
					self.wForm.showEdit( id );
				}
				else{
					self.wForm.hide();
				}
			},
			onFormAdd:function( id ) {
				self.wList.add( id );
			},
			onFormEdit:function( id ) {
				self.wList.update( id );
			},
			onFormDelete:function( id ) {
				self.wList.delete( id );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );