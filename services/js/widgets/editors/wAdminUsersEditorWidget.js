var wAdminUsersEditorWidgetOpen;
!function( $ ) {
	wAdminUsersEditorWidgetOpen = function( options ) {
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminUsersEditorWidget',
			type: 'Users',
			URLFront: '/user/profile',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$alert = self.$ns.find( options.ins+".wAlert" );
				self.$bAdd = self.$ns.find( options.ins+".wAdd" );
				self.wList = options.ns.wAdminUsersListWidget;
				self.wForm = options.ns.wAdminUserFormWidget;
				self.wFront = $( '.wAdminNavbarWidget .wFront a' );
				
				self.$bAdd.click( self.showAdd );
				
				self.wList.onSelectionChanged.push( self.onListSelectionChanged );
				self.wForm.onAdd.push( self.onFormAdd );
				self.wForm.onEdit.push( self.onFormEdit );
				self.wForm.onDelete.push( self.onFormDelete );
			},
			showAdd:function() {
				var showed = self.wForm.showAdd();
				showed ? self.$alert.slideUp() : self.$alert.slideDown();
				if( showed ) {
					self.wList.resetSelection();
				}
				self.wFront.attr( 'href', yiiBaseURL );
				return false;
			},
			onListSelectionChanged:function() {
				var $selection = self.wList.getSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					var idUser = $row.attr( 'idUser' );
					self.wForm.showEdit( idUser );
					self.wFront.attr( 'href', yiiBaseURL + options.URLFront + '?id=' + idUser );
				}
				else{
					self.wForm.hide();
					self.wFront.attr( 'href', yiiBaseURL );
				}
			},
			onFormAdd:function( id ) {
				self.wList.add( id );
			},
			onFormEdit:function( id ) {
				self.wList.update( id );
			},
			onFormDelete:function( id ) {
				self.wList.delete( id );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );