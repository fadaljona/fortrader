var wEAProfileWidgetOpen;
!function( $ ) {
	wEAProfileWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lITradeHere: 'I trade here',
			lINotTradeHere: 'I do not trade here',
			lYouAnd: 'You and',
			lTerribly: 'Terribly',
			lBad: 'Bad',
			lNormal: 'Normal',
			lGood: 'Good',
			lExcellent: 'Excellent',
		};
		var defOption = {
			selector: '.wEAProfileWidget',
			ins: '',
			//ajaxLinkToUserURL: '/broker/ajaxLinkToUser',
			//ajaxUnLinkFromUserURL: '/broker/ajaxUnLinkFromUser',
			ajaxSubmitMarkURL: '/ea/ajaxAddMark',
			delayTooltips: 1000,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				//self.$wCountLinkedUsersSpan = self.$ns.find( '.wCountLinkedUsers span' );
				
				self.$wMarkForm = self.$ns.find( options.ins+'.wMarkForm' );
				self.$wMarkForm.$bSubmit = self.$wMarkForm.find( '[type=submit]' );
				
				self.$wConcept = self.$ns.find( options.ins+'.wConcept' );
				self.$wResult = self.$ns.find( options.ins+'.wResult' );
				self.$wStability = self.$ns.find( options.ins+'.wStability' );
				self.$wAverage = self.$ns.find( options.ins+'.wAverage' );
				
				self.$wConceptInput = self.$ns.find( options.ins+'.wConceptInput' );
				self.$wResultInput = self.$ns.find( options.ins+'.wResultInput' );
				self.$wStabilityInput = self.$ns.find( options.ins+'.wStabilityInput' );
								
				self.$wConceptSlider = self.$ns.find( options.ins+'.wConceptSlider' );
				self.$wResultSlider = self.$ns.find( options.ins+'.wResultSlider' );
				self.$wStabilitySlider = self.$ns.find( options.ins+'.wStabilitySlider' );
				
				self.$wConceptSlider
					.add( self.$wResultSlider )
					.add( self.$wStabilitySlider )
					.slider({ min: 0, max: 100, range: 'min', animate:true });
				
				self.$wConceptSlider.slider( 'value', parseInt( self.$wConceptInput.val()));
				self.$wResultSlider.slider( 'value', parseInt( self.$wResultInput.val()));
				self.$wStabilitySlider.slider( 'value', parseInt( self.$wStabilityInput.val()));
				
				self.$wConceptSlider
					.add( self.$wResultSlider )
					.add( self.$wStabilitySlider )
					.on( 'slide slidechange', self.changeSlider );
													
				self.changeSlider();
				self.$wMarkForm.on( 'submit', self.submitMark );
				
				//self.$ns.on( 'click', '.wLinkToUser, .wUnLinkFromUser', self.linkToUser );
			},
			changeTitleSlider: function( slider, value ) {
				if( value < 20 ) {
					slider.html( options.ls.lTerribly );
				}
				else if( value < 40 ) {
					slider.html( options.ls.lBad );
				}
				else if( value < 60 ) {
					slider.html( options.ls.lNormal );
				}
				else if( value < 80 ) {
					slider.html( options.ls.lGood );
				}
				else{
					slider.html( options.ls.lExcellent );
				}
			},
			changeSlider: function() {
				var concept = self.$wConceptSlider.slider( 'value' );
				var result = self.$wResultSlider.slider( 'value' );
				var stability = self.$wStabilitySlider.slider( 'value' );
				var average = Math.round( ( concept + result + stability ) / 3 );
				
				self.changeTitleSlider( self.$wConcept, concept );
				self.changeTitleSlider( self.$wResult, result );
				self.changeTitleSlider( self.$wStability, stability );
				
				self.$wConceptInput.val( concept );
				self.$wResultInput.val( result );
				self.$wStabilityInput.val( stability );
								
				self.$wAverage.html( average + '%' );
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submitMark:function() {
				var lSubmit = self.$wMarkForm.$bSubmit.html();
				self.$wMarkForm.$bSubmit.html( options.ls.lSaving );
				$.post( yiiBaseURL+options.ajaxSubmitMarkURL, self.$wMarkForm.serialize(), function ( data ) {
					self.$wMarkForm.$bSubmit.html( lSubmit );
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.showTooltip( self.$wMarkForm.$bSubmit, options.ls.lSaved, 'top', options.delayTooltips );
					}
				}, "json" );
				return false;
			},
			/*
			linkToUser:function() {
				var $b = $(this);
				$b.attr( 'disabled', 'disabled' );
				var holdHtml = $b.find( 'span' ).html();
				$b.find( 'span' ).html( options.ls.lSaving );
				var url = $b.hasClass( 'wLinkToUser' ) ? options.ajaxLinkToUserURL : options.ajaxUnLinkFromUserURL;
				$.getJSON( yiiBaseURL+url, {id:options.idBroker}, function ( data ) {
					$b.removeAttr( 'disabled' );
					if( data.error ) {
						alert( data.error );
						$b.find( 'span' ).html( holdHtml );
					}
					else{
						if( $b.hasClass( 'wLinkToUser' )) {
							$b.removeClass( 'wLinkToUser' ).addClass( 'wUnLinkFromUser' );
							$b.find( 'span' ).html( options.ls.lINotTradeHere );
							if( self.$wCountLinkedUsersSpan.length ) self.$wCountLinkedUsersSpan.html( options.ls.lYouAnd + ' ' + self.$wCountLinkedUsersSpan.html() );
						}
						else{
							$b.removeClass( 'wUnLinkFromUser' ).addClass( 'wLinkToUser' );
							$b.find( 'span' ).html( options.ls.lITradeHere );
							if( self.$wCountLinkedUsersSpan.length ) self.$wCountLinkedUsersSpan.html( self.$wCountLinkedUsersSpan.html().replace( options.ls.lYouAnd, '' ) );
						}
					}
				});
			},
			*/
		};
		self.init();
		return self;
	}
}( window.jQuery );