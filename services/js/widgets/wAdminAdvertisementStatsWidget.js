var wAdminAdvertisementStatsWidgetOpen;
!function( $ ) {
	wAdminAdvertisementStatsWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lBlock: 'Block',
			lUnBlock: 'Unblock',
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wAdminAdvertisementStatsWidget',
			URLReload: '/admin/advertisement/stats',
			URLLoadClicks: '/admin/advertisement/ajaxLoadClicks',
			ajaxSubmitBlockURL: '/admin/advertisementBlock/ajaxAddBlock',
			ajaxDeleteBlockIPURL: '/admin/advertisementBlock/ajaxDeleteIP',
			ajaxDeleteBlockAgentURL: '/admin/advertisementBlock/ajaxDeleteAgent',
			sortField: 'createdDT',
			sortType: 'ASC',
			sortDayField: 'clickDate',
			sortDayType: 'ASC',
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var htmlLoadClicks = '';
		
		var self = {
			loadingDelete: false,
			idLoadedClicks: 0,
			urlLoadedClicks:'',
			dataLoadedClicks:{},
			init:function() {
				self.$ns = $( options.selector );
				self.$selectModel = self.$ns.find( options.ins+".wSelectModel" );
				self.$selectCampaign = self.$ns.find( options.ins+".wSelectCampaign" );
				self.$selectBroker = self.$ns.find( options.ins+".wSelectBroker" );
				self.$selectBegin = self.$ns.find( options.ins+".wBegin" );
				self.$selectEnd = self.$ns.find( options.ins+".wEnd" );
				
				self.$grid = self.$ns.find( options.ins+'.iGridView' );
				self.$gridTable = self.$grid.find( '> table' );
				
				self.$wClicksContenter = self.$ns.find( '.wClicksContenter' );
				htmlLoadClicks = self.$wClicksContenter.html();
				
				self.$selectModel.change( self.onChangeLocation );
				self.$selectCampaign.change( self.onChangeLocation );
				self.$selectBroker.change( self.onChangeLocation );
				self.$selectBegin.change( self.onChangeLocation );
				self.$selectEnd.change( self.onChangeLocation );
				
				self.$modalAddBlock = self.$ns.find( options.ins+'.wModalAddBlock' );
				self.$modalAddBlock.ins = '.insAdminAdvertisementAddBlockFormWidget';
				self.$formAddBlock = self.$modalAddBlock.find( 'form' );
				self.$formAddBlock.loading = false;
				self.$formAddBlock.$inputIP = self.$formAddBlock.find( self.$modalAddBlock.ins+'.wIP' );
				self.$formAddBlock.$inputAgent = self.$formAddBlock.find( self.$modalAddBlock.ins+'.wAgent' );
				self.$formAddBlock.$wCancel = self.$formAddBlock.find( self.$modalAddBlock.ins+'.wCancel' );
				self.$formAddBlock.$wSubmit = self.$formAddBlock.find( self.$modalAddBlock.ins+'.wSubmit' );
				
				self.clearClicks();
				self.$wClicksContenter.on( 'click', '.pagination a', self.onClicksPaginationClick );
				self.$wClicksContenter.on( 'click', 'th[class*=sorting]', self.onClicksSortingClick );
				self.$wClicksContenter.on( 'click', '.wBlockIP', self.onClicksBlockIPClick );
				self.$wClicksContenter.on( 'click', '.wUnBlockIP', self.onClicksUnBlockIPClick );
				self.$wClicksContenter.on( 'click', '.wBlockAgent', self.onClicksBlockAgentClick );
				self.$wClicksContenter.on( 'click', '.wUnBlockAgent', self.onClicksUnBlockAgentClick );
				self.$formAddBlock.$wCancel.click( self.closeModalAddBlock );
				self.$formAddBlock.$wSubmit.click( self.addBlock );
			},
			onChangeLocation:function() {
				var model = self.$selectModel.val();
				var idCampaign = self.$selectCampaign.val();
				var idBroker = self.$selectBroker.val();
				var begin = self.$selectBegin.val();
				var end = self.$selectEnd.val();
				
				var params = '?model=' + model;
				if( begin ) params += '&begin='+begin;
				if( end ) params += '&end='+end;
				
				switch( model ) {
					case 'Ad':
					default:{
						if( idCampaign ) params += '&idCampaign='+idCampaign;
						break;
					}
					case 'Campaign':{
						if( idBroker ) params += '&idBroker='+idBroker;
						break;
					}
				}
				
				document.location.assign( yiiBaseURL + options.URLReload + params );
			},
			loadClicks:function( id ) {
				var begin = self.$selectBegin.val();
				var end = self.$selectEnd.val();
				var data = {
					model:options.model, 
					id:id, 
					begin:begin, 
					end:end,
				};
				var params = '?sortField='+options.sortField+'&sortType='+options.sortType+'&sortDayField='+options.sortDayField+'&sortDayType='+options.sortDayType;
				self.$wClicksContenter.html( htmlLoadClicks );
				self.$wClicksContenter.load( yiiBaseURL + options.URLLoadClicks + params, data );
				self.idLoadedClicks = id;
				self.urlLoadedClicks = yiiBaseURL + options.URLLoadClicks + params;
				self.dataLoadedClicks = data;
			},
			clearClicks:function() {
				self.$wClicksContenter.html( '' );
			},
			reloadClicks:function() {
				self.$wClicksContenter.html( htmlLoadClicks );
				self.$wClicksContenter.load( self.urlLoadedClicks, self.dataLoadedClicks );
			},
			getGridSelection:function() {
				return self.$gridTable.find( '> tbody > tr.selected' );
			},
			selectionGridChanged:function() {
				var $selection = self.getGridSelection();
				if( $selection.length ) {
					var $row = $($selection[0]);
					switch( options.model ) {
						case 'Ad':{
							var id = $row.attr( 'idAdvertisement' );
							break;
						}
						case 'Campaign':{
							var id = $row.attr( 'idCampaign' );
							break;
						}
						case 'Zone':{
							var id = $row.attr( 'idZone' );
							break;
						}
					}
					self.loadClicks( id );
				}
				else{
					self.clearClicks();
				}
			},
			onClicksPaginationClick:function() {
				var $link = $(this);
				if( !$link.parent().hasClass( 'active' ) &&  !$link.parent().hasClass( 'disabled' )) {
					var href = $link.attr( 'href' );
					self.$wClicksContenter.html( htmlLoadClicks );
					$.scrollTo( self.$wChanges, options.scrollSpeed, { offset: options.scrollOffset } );
					var begin = self.$selectBegin.val();
					var end = self.$selectEnd.val();
					var data = {model:options.model, id:self.idLoadedClicks, begin:begin, end:end};
					self.$wClicksContenter.load( href, data );
					self.urlLoadedClicks = href;
					self.dataLoadedClicks = data;
				}
				return false;
			},
			onClicksSortingClick:function() {
				var $th = $(this);
				options.sortField = $th.attr( 'sortField' );
				options.sortType = $th.hasClass( 'sorting_asc' ) ? 'DESC' : 'ASC';
				options.sortDayField = $th.attr( 'sortDayField' );
				options.sortDayType = $th.hasClass( 'sorting_day_asc' ) ? 'DESC' : 'ASC';
				self.loadClicks( self.idLoadedClicks );
			},
			closeModalAddBlock:function() {
				if( self.$formAddBlock.loading ) return false;
				self.$modalAddBlock.modal( 'hide' );
				return false;
			},
			openModalAddBlock:function() {
				self.$modalAddBlock.modal({
					backdrop:'static',
				});
				return false;
			},
			disableModalAddBlock:function() {
				self.$formAddBlock.$wCancel
				.add( self.$formAddBlock.$wSubmit )
				.attr( 'disabled', 'disabled' );
			},
			enableModalAddBlock:function() {
				self.$formAddBlock.$wCancel
				.add( self.$formAddBlock.$wSubmit )
				.removeAttr( 'disabled' );
			},
			onClicksBlockIPClick:function() {
				var $btn = $(this);
				var ip = $btn.attr( 'data-ip' );
				self.openModalAddBlock();
				self.$formAddBlock.$inputIP.val( ip ).focus();
				self.$formAddBlock.$inputAgent.val( '' );
				return false;
			},
			onClicksUnBlockIPClick:function() {
				if( self.loadingDelete ) return false;
				var $btn = $(this);
				var ip = $btn.attr( 'data-ip' );
				var lUnBlock = $btn.html();
				$btn.html( options.ls.lSaving );
				$btn.attr( 'disabled', 'disabled' );
				self.loadingDelete = true;
				
				$.getJSON( yiiBaseURL+options.ajaxDeleteBlockIPURL, {ip:ip}, function ( data ) {
					self.loadingDelete = false;
					$btn.html( lUnBlock );
					$btn.removeAttr( 'disabled', 'disabled' );
					if( data.error ) {
						alert( data.error );
					}
					else{
						//self.reloadClicks();
						self.$wClicksContenter.find( '.wUnBlockIP[data-ip="'+ip+'"]' )
						.html( options.ls.lBlock )
						.removeClass( 'wUnBlockIP btn-success' )
						.addClass( 'wBlockIP btn-danger' );
					}
				});
				
				return false;
			},
			onClicksUnBlockAgentClick:function() {
				if( self.loadingDelete ) return false;
				var $btn = $(this);
				var agent = $btn.attr( 'data-agent' );
				var lUnBlock = $btn.html();
				$btn.html( options.ls.lSaving );
				$btn.attr( 'disabled', 'disabled' );
				self.loadingDelete = true;
				
				$.getJSON( yiiBaseURL+options.ajaxDeleteBlockAgentURL, {agent:agent}, function ( data ) {
					self.loadingDelete = false;
					$btn.html( lUnBlock );
					$btn.removeAttr( 'disabled', 'disabled' );
					if( data.error ) {
						alert( data.error );
					}
					else{
						//self.reloadClicks();
						self.$wClicksContenter.find( '.wUnBlockAgent[data-agent="'+agent+'"]' )
						.html( options.ls.lBlock )
						.removeClass( 'wUnBlockAgent btn-success' )
						.addClass( 'wBlockAgent btn-danger' );
					}
				});
				
				return false;
			},
			onClicksBlockAgentClick:function() {
				var $btn = $(this);
				var agent = $btn.attr( 'data-agent' );
				self.openModalAddBlock();
				self.$formAddBlock.$inputAgent.val( agent ).focus();
				self.$formAddBlock.$inputIP.val( '' );
				return false;
			},
			addBlock:function() {
				if( self.$formAddBlock.loading ) return false;
				self.disableModalAddBlock();
				
				self.$formAddBlock.find( '.'+options.errorClass ).removeClass( options.errorClass );
				var lSubmit = self.$formAddBlock.$wSubmit.html();
				self.$formAddBlock.$wSubmit.html( options.ls.lSaving );
				self.$formAddBlock.loading = true;
				var ip = self.$formAddBlock.$inputIP.val();
				var agent = self.$formAddBlock.$inputAgent.val();
				$.post( yiiBaseURL+options.ajaxSubmitBlockURL, self.$formAddBlock.serialize(), function ( data ) {
					self.$formAddBlock.loading = false;
					self.$formAddBlock.$wSubmit.html( lSubmit );
					self.enableModalAddBlock();
					if( data.error ) {
						alert( data.error );
						if( data.errorField ) {
							var $errorField = self.$formAddBlock.find( '*[name="'+data.errorField+'"]' );
							$errorField.addClass( options.errorClass );
							$errorField.focus();
						}
					}
					else{
						self.closeModalAddBlock();
						//self.reloadClicks();
						if( ip ) {
							self.$wClicksContenter.find( '.wBlockIP[data-ip="'+ip+'"]' )
							.html( options.ls.lUnBlock )
							.removeClass( 'wBlockIP btn-danger' )
							.addClass( 'wUnBlockIP btn-success' );
						}
						if( agent ) {
							self.$wClicksContenter.find( '.wBlockAgent[data-agent="'+agent+'"]' )
							.html( options.ls.lUnBlock )
							.removeClass( 'wBlockAgent btn-danger' )
							.addClass( 'wUnBlockAgent btn-success' );
						}
					}
				}, "JSON" );
				
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );