var wQuotesInformerSettingsWidgetOpen;
!function( $ ) {
	wQuotesInformerSettingsWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wQuotesInformerSettingsWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			informerParams: {},
			informerUrl: '',
			init:function() {
				self.$ns = $( options.selector );
				self.$wGetCodeBtn = self.$ns.find( '.getCodeBtn' );
				self.$wInpGetCodeAgreement = self.$ns.find( '#getCodeAgreement' );
				self.$wInpGetCodeTextarea = self.$ns.find( '.getCodeTextarea' );
				self.$wPreviewBlock = self.$ns.find( '.previewBlock' );
				self.$wColorsWrap = self.$ns.find( '.chooseColors' );
				self.$wSymbolsWrap = self.$ns.find( '.chooseSymbols' );	
				self.$wColumnsWrap = self.$ns.find( '.chooseColumns' );	
				self.$wPreviewWrap = self.$ns.find( '.previewBlock' );	
				self.$wSettingInputs = self.$ns.find( 'input.inpSettings' );
				self.$wSettingLayer = self.$ns.find( '.changeSettings .changeSettingsLayer' );
				self.$wSlider = self.$ns.find( '.sliderVal' );
				self.wIframe = self.$wPreviewWrap.find( 'iframe' );	
				
				self.initSlider();
				
				self.wIframe.load( self.correctIframeHeight );	
				
				self.$wGetCodeBtn.click( self.getCode );
				
				self.$wSettingInputs.change( self.changedSettings );
				
				self.disableSettings();
				
				$(window).load( self.initColorPicker );

			},
			initColorPicker: function(){
			
				if( self.$ns.find(".colorSelector").length){

					self.$ns.find(".colorSelector").each(function(){
						var $this = $(this),
							color = $this.attr('data-bg');

						$this.css({
							"background-color": color
						}).next().val(color);
						
						$this.ColorPicker({
							color: color,
							onSubmit: function(hsb, hex, rgb, el) {
								$(el).css({
									"background-color": "#"+hex
								});
								$(el).next().val("#"+hex);
								//$(el).ColorPickerHide();
							},
							onShow: function(colpkr){
								$(colpkr).fadeIn(500);
								return false;
							},
							onHide: function(colpkr){
								$(colpkr).fadeOut(500);
								self.changedSettings();
								return false;
							},
							onChange: function (hsb, hex, rgb) {
								$this.css({
									"background-color": "#"+hex
								});
								$this.next().val("#"+hex);
							}
						});
					});
				
				}


			},
			initSlider:function() {		
				self.$wSlider.slider({
					min : 0.6,
					max : 1.5,
					step: 0.01,
					value: 1,
					stop: function( event, ui ) {
						self.changedSettings();
					},
				});
			},
			disableSettings:function() {		
				self.$wSettingLayer.removeClass('hide');
			},
			enableSettings:function() {		
				self.$wSettingLayer.addClass('hide');
			},
			changedSettings:function() {		
				self.disableSettings();
				self.updateInformer();
			},
			updateInformer:function() {				
				self.informerParams = {};
				self.informerParams['multiplier'] = self.$wSlider.slider("value");
				if( self.$ns.find( '#disableRealTime:checkbox:checked' ).length != 0 ) self.informerParams['noReal'] = 1;
				if( self.$ns.find( '#hideGetInformerBtn:checkbox:checked' ).length == 0 ) self.informerParams['showGetBtn'] = 1;
				self.getColors();
				self.getSymbols();
				self.getColumns();
				
				self.informerUrl = options.getInformerUrl + '?' + self.encodeQueryData();
					
				var frameCode = '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:100%" src="' + self.informerUrl + '"></iframe>';
				
				self.$wPreviewWrap.html( frameCode );
				self.$wPreviewWrap.find( 'iframe' ).load( self.correctIframeHeight );	
				return false;
			},
			correctIframeHeight:function( ) {	
				var iframeHeight = this.contentWindow.document.body.scrollHeight + "px";
				this.style.height = iframeHeight;
				if( self.informerUrl != '' ){
					self.$wInpGetCodeTextarea.val( '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:'+iframeHeight+'" src="' + self.informerUrl + '"></iframe>' );
				}
				self.enableSettings();
			},
			getCode:function() {		
				if( self.informerUrl == '' || self.$wInpGetCodeTextarea.closest('.get_code:visible').length == 0 ){
					self.$wInpGetCodeTextarea.closest('.get_code').slideDown();
					if( self.$wInpGetCodeTextarea.val() == '' ){
						self.disableSettings();
						self.updateInformer();
					} 
				}
				return false;
			},
			getColors:function() {		
				self.$wColorsWrap.find( 'input[type="hidden"]' ).each(function(index, element){
					var val = $(this).val();;
					if( val.charAt(0) === '#' ) val = val.substr(1);
					self.informerParams[$(this).attr('name')] = val;
				});
				self.$wColorsWrap.find( 'input:checkbox:checked' ).each(function(index, element){
					var val = $(this).val();;
					if( val.charAt(0) === '#' ) val = val.substr(1);
					self.informerParams[$(this).attr('name')] = val;
				});
			},
			getSymbols:function() {	
				var $els = self.$wSymbolsWrap.find( 'input:checkbox:checked' );		
				var strVal = '';
				var cats = [];
				$els.each(function(index, element){
					var delimiter = '';
					if( index > 0 ) delimiter = ',';
					strVal = strVal + delimiter + $(this).data('id');
					var catId = $(this).closest('.chooseSymbols').attr('data-cat-id');
					if ( $.inArray(catId, cats) == -1 ) cats.push( catId );
				});
				self.informerParams['cat'] = '';
				if( cats.length == 1 ){
					self.informerParams['cat'] = cats[0];
				}
				self.informerParams['symbols'] = strVal;
			},
			getColumns:function() {	
				var $els = self.$wColumnsWrap.find( 'input:checkbox:checked' );		
				var strVal = '';
				$els.each(function(index, element){
					var delimiter = '';
					if( index > 0 ) delimiter = ',';
					strVal = strVal + delimiter + $(this).attr('name');
				});
				self.informerParams['columns'] = strVal;
			},
			encodeQueryData:function(){
				var ret = [];
				for ( var param in self.informerParams ){
					ret.push( encodeURIComponent( param ) + "=" + encodeURIComponent( self.informerParams[param] ) );
				}
				return ret.join("&");
			},
			
		};
		self.init();
		return self;
	}
}( window.jQuery );