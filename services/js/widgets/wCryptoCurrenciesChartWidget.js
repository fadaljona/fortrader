var wCryptoCurrenciesChartWidgetOpen;
!function( $ ) {
	wCryptoCurrenciesChartWidgetOpen = function( options ) {
		var self = {
            currencyId:0,
            periodChanged: false,
			init:function() {
                self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
                self.$ns = $( options.selector );
                self.$ns.closest('.chartAndList').addClass('position-relative spinner-margin');

                self.$chartWrapper = $('#' + options.chartContainer);
                self.$periods = self.$ns.find('.contrtoller__item');

                self.$periods.click(self.periodClicked);
                self.setLocaleData();
            },
            scrollToChart: function() {
                $('html,body').animate({scrollTop: self.$ns.offset().top}, 500);
            },
            disable:function() {
				self.$ns.closest('.chartAndList').append( self.spinner );
			},
			enable:function(){
				self.$ns.closest('.chartAndList').find('.spinner-wrap').remove();
			},
            periodClicked: function() {
                if (!self.currencyId) {
                    return false;
                }
                if ($(this).hasClass('contrtoller__item--active')) {
                    return false;
                }
                self.$periods.removeClass('contrtoller__item--active');
                $(this).addClass('contrtoller__item--active');

                self.periodChanged = true;
                self.reDrawChart(self.currencyId);
                return false;
            },
            setLocaleData: function() {
                Highcharts.setOptions({
                    lang: options.langOptions
                });
            },
            reDrawChart: function(id) {
                if (self.currencyId == id && !self.periodChanged) {
                    return false;
                }
                self.periodChanged = false;
                self.currencyId = id;
                if (typeof self.$chartWrapper.highcharts() != 'undefined') {
                    self.$chartWrapper.highcharts().destroy();
                }
                self.disable();
                self.initChart(id);
            },
            getChartPeriod: function() {
                return self.$ns.find('.contrtoller__item--active').attr('data-type');
            },
            initChart: function(id) {
                $.getJSON(
                    options.loadDataUrl + '?id=' + id + '&type=' + self.getChartPeriod(),
                    function (data) {
                        self.enable();
                        self.$chartWrapper.highcharts({
                            chart: {
                                zoomType: 'x'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                type: 'datetime'
                            },
                            yAxis: {
                                title: {
                                    text: ''
                                }
                            },
                            legend: {
                                enabled: false
                            },
                            plotOptions: {
                                area: {
                                    fillColor: {
                                        linearGradient: {
                                            x1: 0,
                                            y1: 0,
                                            x2: 0,
                                            y2: 1
                                        },
                                        stops: [
                                            [0, Highcharts.getOptions().colors[0]],
                                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                        ]
                                    },
                                    marker: {
                                        radius: 2
                                    },
                                    lineWidth: 1,
                                    states: {
                                        hover: {
                                            lineWidth: 1
                                        }
                                    },
                                    threshold: null
                                }
                            },
                            series: [{
                                type: 'area',
                                name: data.name,
                                data: JSON.parse(data.data)
                            }]
                        });
                    }
                );
            },
		};
		self.init();
		return self;
	}
}( window.jQuery );