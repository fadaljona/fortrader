var wWebRatingWidgetOpen;
!function( $ ) {
	wWebRatingWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wWebRatingWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			ratingSetted: false,
			currentVal: false,
			init:function() {
				self.$ns = $( options.selector );
				self.initRating();
			},
			initRating:function() {
				if( options.onChangeValue != null ){
					self.$ns.rateYo({
						rating 		: options.currentValue,
						starWidth	: options.starWidth + 'px',
						normalFill	: options.normalFill,
						ratedFill	:  options.ratedFill,
						numStars: options.numStars,
						maxValue: options.maxValue,
						onSet: function (rating, rateYoInstance) {
							if( !self.ratingSetted ) {
								self.ratingSetted = true;
								self.currentVal = options.currentValue;
								return false;
								
							}
				
							if( options.onChangeValue && self.currentVal != rating ) {

								if( options.readOnly ){
									$('#logIn').arcticmodal();
								}else{
									if( !options.returnNs ){
										eval( options.onChangeValue + "( rating );" );
									}else{
										eval( options.onChangeValue + "( rating, options.nsToReturn );" );
									}
									
								}
								self.currentVal = rating;
							}
						}
					});
				}else{
					self.$ns.rateYo({
						rating 		: options.currentValue,
						starWidth	: options.starWidth + 'px',
						normalFill	: options.normalFill,
						ratedFill	:  options.ratedFill,
						readOnly: options.readOnly,
						numStars: options.numStars,
						maxValue: options.maxValue,
						onSet: function (rating, rateYoInstance) {
							if( !self.ratingSetted ) {
								self.ratingSetted = true;
								return false;
							}
							if( options.onChangeValue ) {
								if( options.readOnly ){
									$('#topLogin').trigger('click');
								}else{
									if( !options.returnNs ){
										eval( options.onChangeValue + "( rating );" );
									}else{
										eval( options.onChangeValue + "( rating, options.nsToReturn );" );
									}
								}
							}
						}
					});
				}
				
			},
			
		};
		self.init();
		return self;
	}
}( window.jQuery );