var wTradingWithBrokerCryptoCurrenciesWidgetOpen;
!function( $ ) {
	wTradingWithBrokerCryptoCurrenciesWidgetOpen = function( options ) {
		var self = {
            currencyId:0,
            periodChanged: false,
			init:function() {
                self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
                self.$ns = $( options.selector );
                self.$ns.addClass('position-relative spinner-margin');
                self.$boxMore = self.$ns.find('.fx_broker-more');
                self.$showMore = self.$boxMore.find('a');
                self.itemsContainer = self.$ns.find('.fx_broker-box');
                self.totalPages = parseInt(self.$showMore.attr('data-pages'));

                self.$showMore.click(self.loadMore);
                
            },
            disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function(){
				self.$ns.find('.spinner-wrap').remove();
			},
            loadMore: function() {
                var currentPage = parseInt(self.$showMore.attr('data-page'));
                if (currentPage >= self.totalPages) {
                    return false;
                }
                var data = {
                    pageTradingWithBrokerCryptoCurrencies: currentPage+1
                }
                self.disable();
                $.getJSON( options.loadDataUrl, data, function ( data ) {
                    self.enable();
					if (data.error) {
						alert( data.error );
					} else {
                        self.itemsContainer.append(data.list);
                        self.$showMore.attr({'data-page': currentPage+1});
                        if (currentPage+1 >= self.totalPages) {
                            self.$boxMore.hide();
                        }
					}
                });
                return false;
            },

		};
		self.init();
		return self;
	}
}( window.jQuery );