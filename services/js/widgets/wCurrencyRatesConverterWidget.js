var wCurrencyRatesConverterWidgetOpen;
!function( $ ) {
	wCurrencyRatesConverterWidgetOpen = function( options ) {
		var defOption = {
			ajax: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			prevFromVal: 1,
			timeoutFromVar: false,
			timeoutToVar: false,
			timeoutTime: 1000,
			init:function() {
	
				self.$ns = $( options.selector );
				self.$wFlipConverter = self.$ns.find( '.flipConverter' );
	
				self.$wConverterSelectFrom = self.$ns.find( 'select.converterSelectFrom' );
				self.$wConverterSelectTo = self.$ns.find( 'select.converterSelectTo' );
				self.$wConverterInpFrom = self.$ns.find( 'input.converterInpFrom' );
				self.$wConverterInpTo = self.$ns.find( 'input.converterInpTo' );
				
				self.prevToVal = parseFloat(self.$wConverterInpTo.val());
				
				self.$wConverterInps = self.$ns.find( 'input.converter_form_inp' );
				
				self.initFormStyler();

				self.converterInpBindKeyup();
				
				self.$wFlipConverter.click( self.changeConverterFromTo );
				
				self.$wConverterInpFrom.focusout( self.saveConverterRequestFrom );
				self.$wConverterInpTo.focusout( self.saveConverterRequestTo );
				
			},
			saveConverterRequest: function( dest ){
				if( self.loading ) return false;
				
				if( dest == 'From' ){
					var fromCur = self.$wConverterSelectFrom.val();
					var toCur = self.$wConverterSelectTo.val();
					var fromVal = self.$wConverterInpFrom.val();
				}else{
					var fromCur = self.$wConverterSelectTo.val();
					var toCur = self.$wConverterSelectFrom.val();
					var fromVal = self.$wConverterInpTo.val();
				}
				
				self.loading = true;
				$.post( options.sendConverterDataUrl, {fromCur:fromCur, toCur:toCur, fromVal:fromVal, type: options.type}, function ( data ) {
					
				}, "json" );
				self.loading = false;
			},
			saveConverterRequestTo: function(){
				if( self.$wConverterInpTo.val() == '' ) return false;
				var newVal = parseFloat(self.$wConverterInpTo.val());
				if( newVal != self.prevToVal ){
					self.saveConverterRequest('To');
				}
				self.prevToVal = newVal;
			},
			saveConverterRequestFrom: function(){
				if( self.$wConverterInpFrom.val() == '' ) return false;
				var newVal = parseFloat(self.$wConverterInpFrom.val());
				if( newVal != self.prevFromVal ){
					self.saveConverterRequest('From');
				}
				self.prevFromVal = newVal;
			},
			changeConverterFromTo:function(){
				if( self.loading ) return false;
				self.loading = true;
				
				var fromInpVal = self.$wConverterInpFrom.val();
				self.$wConverterInpFrom.val( self.$wConverterInpTo.val() );
				self.$wConverterInpTo.val( fromInpVal );
				
				var fromSelectVal = self.$wConverterSelectFrom.val();
				self.$wConverterSelectFrom.val( self.$wConverterSelectTo.val() );
				self.$wConverterSelectTo.val( fromSelectVal );
				
				self.$wConverterSelectFrom.trigger('refresh');
				self.$wConverterSelectTo.trigger('refresh');
				
				self.loading = false;
				
				return false;
			},

			converterInpBindKeyup:function(e){
				self.$wConverterInps.keyup(function(e) {
					var $this = $(this);
					var val = $this.val();
					val = val.replace(/[^\d.]*/g, '')
							.replace(/([.])[.]+/g, '$1')
							.replace(/^[^\d]*(\d+([.]\d{0,15})?).*$/g, '$1');
					$this.val( val );
					if( val != '' ){
						self.recalculateConverter('From', 'To');
						/*if( $this.hasClass('converterInpFrom') ){
							self.recalculateConverter('From', 'To');
						}
						if( $this.hasClass('converterInpTo') ){
							self.recalculateConverter('To', 'From');
						}*/
					}
				});
			},
			initFormStyler:function(){
				var $forms = self.$ns.find('.form_styler');
				if( $forms.length ){
					$forms.styler({
						selectSearch: true,
						onSelectClosed: function() {
							var $this = $(this);
							if( $this.hasClass('converterSelect') ){
								self.recalculateConverter('From', 'To');
								self.saveConverterRequest('From');
								/*if( $this.hasClass('converterSelectFrom') ){
									self.recalculateConverter('From', 'To');
									self.saveConverterRequest('From');
								}else{
									self.recalculateConverter('To', 'From');
									self.saveConverterRequest('To');
								}*/
							}
						}
					});
				}
			},
			recalculateConverter:function( fromCur, toCur ){
				var $toSelect = self.$ns.find( 'select.converterSelect' + toCur );
				var $selectOptDest = $toSelect.find(":selected");
				var destNominal = parseInt( $selectOptDest.attr( 'data-nominal' ) );
				var destVal = parseFloat( $selectOptDest.attr( 'data-value' ) );
									
				var $fromInp = self.$ns.find( 'input.converterInp' + fromCur );
				var $toInp = self.$ns.find( 'input.converterInp' + toCur );
				var $fromSelect = self.$ns.find( 'select.converterSelect' + fromCur );					
				var $selectOpt = $fromSelect.find(":selected");
				var toVal = parseFloat( $fromInp.val() );
				var newNominal = parseInt( $selectOpt.attr( 'data-nominal' ) );
				var newVal = parseFloat( $selectOpt.attr( 'data-value' ) );
				
				$toInp.val( (( newVal/newNominal   ) / ( destVal / destNominal ) * toVal).toFixed(4) );			
				
				clearTimeout(self.timeoutFromVar);	
				clearTimeout(self.timeoutToVar);
				if( fromCur == 'From' ){
					self.timeoutFromVar = setTimeout( self.saveConverterRequestFrom, self.timeoutTime);
				}else{
					self.timeoutToVar = setTimeout( self.saveConverterRequestTo, self.timeoutTime);
				}
				
				
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );