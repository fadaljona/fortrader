var wActivityBlockWidgetOpen;
!function( $ ) {
	wActivityBlockWidgetOpen = function( options ) {
		var defOption = {
			ins: '',
			selector: '.wActivityBlockWidget',
			ajax: false,
			ajaxLoadListURL: '/default/ajaxLoadActivitiesList',
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
			isGuest: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				
				self.$wActivitiesList = self.$ns.find( options.ins+'.wActivitiesList' );
				self.$tplItem = self.$ns.find( options.ins+'.wTplItem' );
								
				self.$wReload = self.$ns.find( options.ins+'.wReload' );
				self.$wShowMore = self.$ns.find( options.ins+'.wShowMore' );
				
				self.$wReload.click( self.reload );
				self.$wShowMore.click( self.showMore );
			},
			reload: function() {
				if( self.loading ) return false;
				self.$wShowMore.parent().hide();
			
				self.$wActivitiesList.html( '' );
				var $tplItem = self.$tplItem.clone();
				self.$wActivitiesList.append( $tplItem );
				$tplItem.show();
				var data = {};
				self.loading = true;
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
					self.loading = false;
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $list = $( data.list );
						$tplItem.replaceWith( $list );
						if( data.more ) {
							self.$wShowMore.parent().show();
						}
					}
					$tplItem.remove();
				});
				return false;
			},
			showMore:function() {
				if( self.loading ) return false;
				self.$wShowMore.parent().hide();

				var $tplItem = self.$tplItem.clone();
				self.$wActivitiesList.append( $tplItem );
				$tplItem.show();
				self.loading = true;
				var idLastActivity = self.$wActivitiesList.find( '[idActivity]:last' ).attr( 'idActivity' );
				var data = {};
				if( idLastActivity ) data.idLastActivity = idLastActivity;
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
					self.loading = false;
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $list = $( data.list );
						$tplItem.replaceWith( $list );
						if( data.more ) {
							self.$wShowMore.parent().show();
						}
					}
					$tplItem.remove();
				});
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );