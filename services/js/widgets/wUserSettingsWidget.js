var wUserSettingsWidgetOpen;
!function( $ ) {
	wUserSettingsWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wUserSettingsWidget',
			ins: '',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				
				self.$profileTabsWrapper = self.$ns.find('.topSettingsTabs');
				self.$profileTabsLinks = self.$profileTabsWrapper.find('a');
				self.$profileTabsContests = self.$ns.find('.topSettingsContentTabs > div');
				self.hideInactiveProfileTabs();	
				self.$profileTabsLinks.click( self.changeProfileTab );
				
				
			},
			hideInactiveProfileTabs: function(){
				var activeIndex = self.$profileTabsLinks.index( self.$ns.find('.inside-menu__active') );
				
				self.$profileTabsContests.each(function( index, element ) {
					if( index == activeIndex ) 
						$(this).show();
					else
						$(this).hide();
				});
			},
			changeProfileTab: function(){
				self.$profileTabsLinks.removeClass('inside-menu__active');
				$(this).addClass('inside-menu__active');
				self.hideInactiveProfileTabs();
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );