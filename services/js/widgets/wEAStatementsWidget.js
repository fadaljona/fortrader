var wEAStatementsWidgetOpen;
!function( $ ) {
	wEAStatementsWidgetOpen = function( options ) {
		var defOption = {
			ins: '',
			selector: '.wEAStatementsWidget',
			ajax: false,
			ajaxLoadListURL: '/ea/ajaxLoadStatementsList',
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
			isGuest: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				
				self.$wList = self.$ns.find( options.ins+'.wList' );
				self.$tplItem = self.$ns.find( options.ins+'.wTplItem' );
								
				self.$wShowMore = self.$ns.find( options.ins+'.wShowMore' );
				
				self.$wShowMore.click( self.showMore );
			},
			collectIDsSkip: function( date ) {
				var ids = [];
				self.$wList.find( '[dateStatment="'+date+'"]' ).each( function() {
					ids.push( $(this).attr( 'idStatment' ));
				});
				return ids;
			},
			showMore:function() {
				if( self.loading ) return false;
				self.$wShowMore.parent().hide();
				
				var $tplItem = self.$tplItem.clone();
				self.$wList.append( $tplItem );
				$tplItem.show();
				self.loading = true;
				var dateLast = self.$wList.find( '[idStatment]:last' ).attr( 'dateStatment' );
				var data = {
					idEA: options.idEA,
					idEAVersion: options.idEAVersion,
				};
				if( dateLast ) {
					data.dateLast = dateLast;
					var idsSkip = self.collectIDsSkip( dateLast );
					if( idsSkip.length ) {
						data.idsSkip = idsSkip.join( ',' );
					}
				}
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
					self.loading = false;
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $list = $( data.list );
						$tplItem.replaceWith( $list );
						if( data.more ) {
							self.$wShowMore.parent().show();
						}
					}
					$tplItem.remove();
				});
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );