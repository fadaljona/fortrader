var wEAStatementProfileWidgetOpen;
!function( $ ) {
	wEAStatementProfileWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lSaidThank: 'Said thank you',
		};
		var defOption = {
			selector: '.wEAStatementProfileWidget',
			ins: '',
			ajaxThankURL: '/thank/ajaxAdd',
			delayTooltips: 1000,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$wThank = self.$ns.find( '.wThank' );
				self.$wThanks = self.$ns.find( '.wThanks' );
				
				self.$wThank.click( self.thank );
			},
			thank:function() {
				var $link = $(this);
				var holdHtml = $link.html();
				$link.html( options.ls.lSaving );
				
				var data = {
					instance: 'eaStatement/single',
					idObject: options.idStatement,
				};
				$.getJSON( yiiBaseURL+options.ajaxThankURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
						$link.html( holdHtml );
					}
					else{
						holdHtml = /\d+/.test( holdHtml ) ? holdHtml.replace( /\d+/, data.count ) : holdHtml + '(' + data.count + ')';
						$link.html( holdHtml );
						
						var thanks = '<span>'+options.ls.lSaidThank+'</span>';
						thanks += ":<br>";
						thanks += data.thanks;
						
						self.$wThanks.html( thanks );
						self.$wThanks.show();
					}
				});
				
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );