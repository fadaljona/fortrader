var wJournalsArchiveWidgetOpen;
!function( $ ) {
	wJournalsArchiveWidgetOpen = function( options ) {
		var defOption = {
			ins: '',
			selector: '.wJournalsArchiveWidget',
			ajax: false,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
			isGuest: false
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				
				self.$wList = self.$ns.find( options.ins+'.wList' );
				self.$tplItem = self.$ns.find( options.ins+'.wTplItem' );
								
				self.$wShowMore = self.$ns.find( options.ins+'.wShowMore' );
				
				self.$wShowMore.click( self.showMore );
				
				$(document).ready( self.equalHeight );
				$( window ).resize( self.reEqualHeight );
			},
			reEqualHeight: function( ) {
				var imgs = self.$ns.find('.post_col4 img');
				imgs.each(function( index ) {
					$( this ).closest('a').css({
						height: 'auto',
					});
				});
				self.equalHeight();
			},
			equalHeight: function( ) {
				var imgs = self.$ns.find('.post_col4 img');
				var maxHeight = 0;
				imgs.each(function( index ) {
					if( parseInt( $( this ).innerHeight() ) > maxHeight ) maxHeight = parseInt( $( this ).innerHeight() );
				});
				imgs.each(function( index ) {
					$( this ).closest('a').css({
						display: 'block',
						height: maxHeight,
					});
				});
			},
			collectIDsSkip: function( date ) {
				var ids = [];
				self.$wList.find( '[data-dateJournal="'+date+'"]' ).each( function() {
					ids.push( $(this).attr( 'data-idJournal' ));
				});
				return ids;
			},
			showMore:function() {
				if( self.loading ) return false;
				//self.$wShowMore.parent().hide();
				
				var $tplItem = self.$tplItem.clone();
				self.$wList.append( $tplItem );
				$tplItem.show();
				self.loading = true;
				var dateLast = self.$wList.find( '[data-idJournal]:last' ).attr( 'data-dateJournal' );
				var data = {
					idEA: options.idEA,
					idEAVersion: options.idEAVersion
				};
				if( dateLast ) {
					data.dateLast = dateLast;
					var idsSkip = self.collectIDsSkip( dateLast );
					if( idsSkip.length ) {
						data.idsSkip = idsSkip.join( ',' );
					}
				}
				data.limit = options.limit;
				data.type = options.type;
				$.getJSON( options.ajaxLoadListURL, data, function ( data ) {
					self.loading = false;
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $list = $( data.list );
						$tplItem.replaceWith( $list );
						if( data.more == false ) {
							self.$wShowMore.parent().hide();
						}
					}
					$tplItem.remove();
					self.equalHeight();
				});
				return false;
			}
		};
		self.init();
		return self;
	}
}( window.jQuery );