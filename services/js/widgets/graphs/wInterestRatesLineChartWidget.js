var wwInterestRatesLineChartWidgetOpen;
!function( $ ) {
	wwInterestRatesLineChartWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wwInterestRatesLineChartWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {	
				self.$ns = $( options.selector );
				self.$chartWrapper = self.$ns.find( '#'+options.chartId );
				self.$chart = self.makeChart( );
				self.$chart.addListener("rendered", self.zoomChart);
			},
			zoomChart:function() {
				var now = new Date();
				var nowTime = now.getTime();
				var time12mo = ((nowTime - ( nowTime % 1000 )) / 1000) - 60*60*24*31*18;
				var startIndex = 0;
				for( i in self.$chart.dataProvider ){
					if( parseInt( self.$chart.dataProvider[i].time ) >= time12mo ){
						startIndex = i;
						break;
					}
				}
				self.$chart.zoomToIndexes(startIndex, self.$chart.dataProvider.length - 1);
			},

			makeChart:function() {	
				return AmCharts.makeChart( options.chartId, {
					"type": "serial",
					"theme": "light",
					"language": options.lang,
					"legend": {
						//"useGraphSettings": true
					},
					"dataProvider": options.dp,
					"valueAxes": [{
						"gridColor": "#000000",
						"gridAlpha": 0.05,
						"dashLength": 0,
					}],
					"gridAboveGraphs": true,
					"graphs": options.graphs,
					"mouseWheelZoomEnabled": true,
					"chartScrollbar": {
						"autoGridCount": true,
						"graph": "g1",
						"scrollbarHeight": 40
					},
					"chartCursor": {
						"cursorAlpha": 1,
						"zoomable": true,
						"pan": true,
						"valueBalloonsEnabled": false,
					},
					"categoryField": "date",
					"categoryAxis": {
						"gridPosition": "start",
						"gridAlpha": 0,
						"parseDates": true,
						"position": 'top',
					},
					"export": {
						"enabled": true
					}

				} );
			},

		};
		self.init();
		return self;
	}
}( window.jQuery );