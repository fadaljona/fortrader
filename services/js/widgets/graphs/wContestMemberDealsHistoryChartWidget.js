var wContestMemberDealsHistoryChartWidgetOpen;
!function( $ ) {
	wContestMemberDealsHistoryChartWidgetOpen = function( options ) {
		var defOption = {
		
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$historySymbolSelect = self.$ns.find('.historySymbolForHighcharts');
			
				self.initChart();
			},
			historySymbolChanged: function(){
				Highcharts.charts[0].xAxis[0].setExtremes(null,null);
				self.afterSetExtremes( {min:0,max:0} );
			},
			afterSetExtremes: function(e){
				var chart = Highcharts.charts[0];

				chart.showLoading('Loading data from server...');
				$.getJSON( options.getDataUrl + '?symbolId=' + self.$historySymbolSelect.val() +'&symbolName='+self.$historySymbolSelect.find('option[value="'+self.$historySymbolSelect.val()+'"]').text()+'&memberId='+ options.memberId +'&start=' + Math.round(e.min) + '&end=' + Math.round(e.max) + '&callback=?', function (data) {
			
					chart.series[0].setData(data[0]);
					chart.series[0].name= self.$historySymbolSelect.find('option[value="'+self.$historySymbolSelect.val()+'"]').text();
		
					chart.series[1].setData(data[1]);
					chart.series[2].setData(data[2]);
		
					chart.setTitle({ text: self.$historySymbolSelect.find('option[value="'+self.$historySymbolSelect.val()+'"]').text() });
		
					chart.hideLoading();
				});
			},
			initChart: function(){
				$.getJSON( options.getDataUrl + '?symbolId=' + self.$historySymbolSelect.val() +'&symbolName='+self.$historySymbolSelect.find('option[value="'+self.$historySymbolSelect.val()+'"]').text()+'&memberId='+ options.memberId +'&callback=?', function (data) {

					Highcharts.stockChart('highChartsContestMemberDealsHistory', {
						chart: {
							zoomType: 'x'
						},
						navigator: {
							adaptToUpdatedData: false,
							series: {
								data: data[0]
							}
						},
						scrollbar: {
							liveRedraw: false
						},
						title: {
							text: self.$historySymbolSelect.find('option[value="'+self.$historySymbolSelect.val()+'"]').text()
						},
						subtitle: {
							text: ''
						},
						rangeSelector: {
							buttons: [{
								type: 'hour',
								count: 12,
								text: '12h'
							}, {
								type: 'day',
								count: 1,
								text: '1d'
							},, {
								type: 'week',
								count: 1,
								text: '1w'
							}, {
								type: 'month',
								count: 1,
								text: '1m'
							}, {
								type: 'year',
								count: 1,
								text: '1y'
							}, {
								type: 'all',
								text: 'All'
							}],
							inputEnabled: false, // it supports only days
							selected: 6
						},
						xAxis: {
							events: {
								afterSetExtremes: self.afterSetExtremes
							},
							minRange: 15*60 * 1000 // 15 mins
						},
						yAxis: {
							floor: 0
						},

						series: [{
							name: self.$historySymbolSelect.find('option[value="'+self.$historySymbolSelect.val()+'"]').text(),
							type: 'ohlc',
							data: data[0],
							color: '#000',
							dataGrouping: {
								enabled: false
							}
						},{
							dashStyle: 'LongDash',
							color: 'red',
							lineWidth: 0.5,
							yAxis: 0,
							type: 'line',
							name: 'Sell',
							data: data[1]

						},{
							dashStyle: 'LongDash',
							color: 'blue',
							lineWidth: 0.5,
							yAxis: 0,
							type: 'line',
							name: 'Buy',
							data: data[2]

						}]
					});
				});
			},

		};
		self.init();
		return self;
	}
}( window.jQuery );