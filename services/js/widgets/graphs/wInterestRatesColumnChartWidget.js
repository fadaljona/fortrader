var wInterestRatesColumnChartWidgetOpen;
!function( $ ) {
	wInterestRatesColumnChartWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wInterestRatesColumnChartWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			needToUpdatePeriodChart: false,
			loadingAmchartsTech: false,
			init:function() {	
				self.$ns = $( options.selector );
				self.$chartWrapper = self.$ns.find( '#'+options.chartId );
				self.$chart = self.makeChart( );
			},

			makeChart:function() {	
				return AmCharts.makeChart( options.chartId, {
					"type": "serial",
					"theme": "light",
					"language": options.lang,
					"dataProvider": options.dp,
					"valueAxes": [ {
						"gridColor": "#FFFFFF",
						"gridAlpha": 0.2,
						"dashLength": 0,
				
						"guides": [{
								"dashLength": 1,
								"lineAlpha": 1,
								"lineColor": "#167AC6",
								"fillColor": "#167AC6",
								"value": 0,
							},{
								"lineColor": "#000000",
								"fillColor": "#000000",
								"fillAlpha": 0.05,
								"lineAlpha": 0,
								"toValue": -10000,
								"value": 0,
						}],			
					} ],
					"gridAboveGraphs": true,
					"graphs": [ {
						"balloonText": "<div style='line-height:16px;'><img width='16' src='[[img]]'> [[category]]: <b>[[value]]%</b></div>[[difDate]]<br />[[contryName]]",
						"fillAlphas": 1,
						"fillColors": "#167AC6",
						"negativeFillColors": "#F4402D",
						"lineAlpha": 0.2,
						"type": "column",
						"valueField": "value"
					},{
						"showBalloon": false,
						"bulletOffset": -16,
						"bulletSize": 32,
						"customBulletField": "img",
						"fillAlphas": 0,
						"lineAlpha": 0,
						"type": "column",
						"visibleInLegend": false,
						"clustered": false,
						"stackable": false,
						"valueField": "flag"
					} ],
					"chartCursor": {
						"categoryBalloonEnabled": false,
						"cursorAlpha": 0,
						"zoomable": false
					},
					"categoryField": "bank",
					"categoryAxis": {
						"gridPosition": "start",
						"gridAlpha": 0,
					},
					"export": {
						"enabled": true
					}

				} );
			},

		};
		self.init();
		return self;
	}
}( window.jQuery );