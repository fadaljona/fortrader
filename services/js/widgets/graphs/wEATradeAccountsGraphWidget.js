var wEATradeAccountsGraphWidgetOpen;
!function( $ ) {
	wEATradeAccountsGraphWidgetOpen = function( options ) {
		var defLS = {
			lAccount: 'Account',
			lTime: 'Time',
			lGain: 'Gain',
			lEA: 'EA',
		};
		var defOption = {
			ins: '',
			selector: '.wEATradeAccountsGraphWidget',
			cuteOutlets: true,
			accountURL: '/eaTradeAccount/single',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init: function() {
				self.$ns = $( options.selector );
				self.$wGraph = self.$ns.find( options.ins + '.wGraph' );
				self.$wGraph.$svg = self.$wGraph.find( 'svg' );
				self.$wBabl = self.$ns.find( options.ins + '.wBabl' );
				
				self.detVars();
				
				$(function(){
					self.drawGraph();
					$( window ).on( 'resize', self.drawGraph );
				});
			},
			detVars: function() {
				function getDay( date ) {
					var year = date.getFullYear();
					var month = date.getMonth();
					var day = date.getDate();
					return new Date( year, month, day, 0, 0, 0, 0 );
				}
				function getCurrDayTS( ts ) {
					var currDate = new Date( ts * 1000 );
					var currDay = getDay( currDate );
					return currDay.getTime() / 1000;
				}
				function getNextDayTS( ts ) {
					var currDate = new Date( ts * 1000 );
					var currDay = getDay( currDate );
					return currDay.getTime() / 1000 + 60*60*24;
				}
				options.lengthData = 0;
				for( account in options.data ) {
					options.lengthData++;
					for( ts in options.data[ account ] ) {
						var value = options.data[ account ][ ts ];
						if( !options.minTS || ts < options.minTS ) options.minTS = ts;
						if( !options.maxTS || ts > options.maxTS ) options.maxTS = ts;
						if( typeof options.minValue == 'undefined' || value < options.minValue ) options.minValue = value;
						if( typeof options.maxValue == 'undefined' || value > options.maxValue ) options.maxValue = value;
					}
				}
				options.trueMaxTS = options.maxTS;
				options.minTS = getCurrDayTS( parseInt( options.minTS ));
				options.maxTS = getNextDayTS( parseInt( options.maxTS ));
				options.widthTS = options.maxTS - options.minTS;
				options.heightValue = options.maxValue - options.minValue;
				
				options.days = {};
				options.daysKeys = [];
				for( var ts = options.minTS; ts<options.maxTS; ts+=86400 ) {
					var date = new Date( ts * 1000 );
					var dateStr = commonLib.getDate( date );
					var day = date.getDay();
					var outlet = day == 6 || day == 0;
					
					options.daysKeys.push( dateStr );
					
					options.days[dateStr] = {
						date: date,
						outlet: outlet,
						show: options.cuteOutlets ? !outlet : true,
					};
				}
				if( options.cuteOutlets ) {
					if( options.daysKeys.length > 0 ) {
						var keyEnd1 = options.daysKeys[ options.daysKeys.length - 1 ];
						if( options.days[ keyEnd1 ].outlet ) {
							options.days[ keyEnd1 ].show = true
							if( options.daysKeys.length > 1 ) {
								var keyEnd2 = options.daysKeys[ options.daysKeys.length - 2 ];
								if( options.days[ keyEnd2 ].outlet ) {
									options.days[ keyEnd2 ].show = true
								}
							}
						}
					}
				}
				
				options.countDays = 0;
				options.countDaysShow = 0;
				options.countDaysHide = 0;
				options.daysKeysShow = [];
				options.countDays = options.daysKeys.length;
				for( dateStr in options.days ) {
					if( options.days[dateStr].show ) {
						options.countDaysShow++;
						options.daysKeysShow.push( dateStr );
					}
					else options.countDaysHide++;
				}
			},
			markMouseover: function( event ) {
				var $mark = $(this);
				var offset = $mark.offset();
				self.$wBabl.html( event.data.content );
				offset.top -= self.$wBabl.height() + 10;
				offset.left -= Math.round( ( self.$wBabl.width() + 16 ) / 2 );
				$mark.attr( 'stroke-opacity', 1 );
				$mark.attr( 'fill-opacity', 1 );
				self.$wBabl.offset( offset );
				self.$wBabl.css( 'visibility', 'visible' );
			},
			markMouseout:function() {
				var $mark = $(this);
				self.$wBabl.css( 'visibility', 'hidden' );
				
				$mark.attr( 'stroke-opacity', 0 );
				$mark.attr( 'fill-opacity', 0 );
			},
			markClick:function( event ) {
				document.location.assign( yiiBaseURL + options.accountURL + '?id=' + event.data.id );
			},
			makeSVG: function( tag, attrs, inner ) {
				var el = document.createElementNS('http://www.w3.org/2000/svg', tag);
				for( var k in attrs ) {
					el.setAttribute( k, attrs[ k ]);
				}
				if( inner ) {
					el.appendChild( document.createTextNode( inner ));
				}
				return el;
			},
			drawGraph: function() {
				self.$wGraph.html( '' );
				var svg = self.makeSVG( 'svg', { width:'100%', height:'100%' });
				self.$wGraph[0].appendChild( svg );
				
				var graph = {
					width: self.$wGraph.width(),
					height: self.$wGraph.height(),
					padding: 10,
				};
				graph.halfHeight = Math.round( graph.height / 2 );
				var inPhone = graph.width <= 480;
				
				var legendBlock = {
					width: inPhone ? 50 : 150,
					height: inPhone ? 30 : 60,
				};
				legendBlock.halfWidth = Math.round( legendBlock.width / 2 );
				var legend = {
					//width: inPhone ? graph.width - graph.padding * 2 : legendBlock.width,
					width: 0,
					top: inPhone ? graph.height - 150 : graph.padding,
				};
				legend.left = graph.width - legend.width - graph.padding;
				
				var asixY = {};
								
				var asixX = {
					top: inPhone ? legend.top - 30 : graph.height - graph.padding - 10,
				};
				
				var net = {
					top: graph.padding,
					bottom: asixX.top,
				};
				
				net.height = net.bottom - net.top;
				
				function getSizeStep( sizeStep ) {
					if( !sizeStep ) return 0;
					var pow10 = Math.floor(Math.log( sizeStep ) / Math.log( 10 ));
					var step = Math.pow( 10, pow10 );
					sizeStep = Math.ceil( sizeStep / step ) * step;
					if( sizeStep > 5000 && sizeStep < 10000 ) sizeStep = 10000;
					return sizeStep;
				}
				
				var heightStep = 30;
				var nSteps = Math.floor( net.height / heightStep );
				var sizeStep = Math.ceil( options.heightValue / nSteps );
				var sizeStep = getSizeStep( sizeStep );
				var maxValue = sizeStep ? Math.ceil( options.maxValue / sizeStep ) * sizeStep : 0;
				var minValue = sizeStep ? Math.floor( options.minValue / sizeStep ) * sizeStep : 0;
				var heightValue = maxValue - minValue;
				var nSteps = sizeStep ? heightValue / sizeStep : 0;
				var heightStep = nSteps ? net.height / nSteps : 0;

				var widthAsixY = Math.max( maxValue.toString().length, minValue.toString().length ) * 7 + 10;
				
				asixY.left = widthAsixY + graph.padding;
				asixX.left = asixY.left;
					
				net.left = asixY.left;
				net.right = inPhone ? graph.width - graph.padding : legend.left - graph.padding;
				net.width = net.right - net.left;
								
				!function drawNet() {
					!function drawVerticalNet() {
						var gLines = self.makeSVG( 'g', { fill:'none', stroke:'Gainsboro', 'stroke-dasharray': 5 });
						svg.appendChild( gLines );
						var gTexts = self.makeSVG( 'g', { fill:'DarkGray', 'text-anchor': 'middle' });
						svg.appendChild( gTexts );
						
						if( options.countDaysShow ) {
							var widthDay = net.width / options.countDaysShow;
						
							var stepI = 1;
							var widthDayTmp = widthDay;
							while( widthDayTmp < 40 ) {
								stepI++;
								widthDayTmp = widthDay * stepI;
							}		
							
							for( i = 0; i<options.countDaysShow+1; i++ ) {
								if( i<options.countDaysShow && i % stepI == 0 || widthDay>10 ) {
									var x = net.left + widthDay * i;	
									x = Math.round( x );	
									var line = self.makeSVG( 'line', {
										x1:x + 0.5, 
										x2:x + 0.5, 
										y1:net.bottom, 
										y2:net.top,
									});
									gLines.appendChild( line );
								}
								
								if( i<options.countDaysShow && i % stepI == 0 ) {
									var dateStr = options.daysKeysShow[i];
									var date = commonLib.getShortDate( options.days[ dateStr ].date );
									
									var xTitle = Math.round( x + widthDay/2 );
									var yTitle = net.bottom + 16;
									
									if( xTitle + 15 <= net.right || !inPhone ) {
										var title = self.makeSVG( 'text', {
											x:xTitle, 
											y:yTitle, 
										}, date );
										gTexts.appendChild( title );
									}
								}
							}
						}
						
					}();
					!function drawGorizontalNet() {
						var gLines = self.makeSVG( 'g', { stroke:'Gainsboro', 'stroke-dasharray': 5 });
						svg.appendChild( gLines );
						var gLineZero = self.makeSVG( 'g', { stroke:'Silver', 'stroke-dasharray': 10, 'stroke-width': 2 });
						svg.appendChild( gLineZero );
						var gTexts = self.makeSVG( 'g', { fill:'DarkGray', 'text-anchor': 'end' });
						svg.appendChild( gTexts );
						var gTextZero = self.makeSVG( 'g', { fill:'DarkGray', 'text-anchor': 'end', 'font-weight': 'bold' });
						svg.appendChild( gTextZero );
						
						for( i=0; i<nSteps+1; i++ ) {
							var y = Math.round( net.bottom - heightStep * i );
							var value = minValue + sizeStep * i;
							
							var line = self.makeSVG( 'line', {
								x1:net.left, 
								x2:net.right, 
								y1:y + 0.5, 
								y2:y + 0.5,
							});
							value == 0 ? gLineZero.appendChild( line ) : gLines.appendChild( line );
							
							var xTitle = net.left - 5;
							var yTitle = y + 4;
							var title = self.makeSVG( 'text', {
								x:xTitle, 
								y:yTitle, 
							}, value.toString()+'%' );
							value == 0 ? gTextZero.appendChild( title ) : gTexts.appendChild( title );
						}
					}();
				}();
				!function drawData() {
					function getCountDaysHide( dateStr ) {
						var count = 0;
						for( var _dateStr in options.days ) {
							if( _dateStr == dateStr ) break;
							if( !options.days[ _dateStr ].show ) count++;
						}
						return count;
					}
					var gLines = self.makeSVG( 'g', { fill: 'none', 'stroke-width': inPhone ? 1 : 2 });
					svg.appendChild( gLines );
					var gCircles = self.makeSVG( 'g', { stroke: 'black', 'stroke-width' : 1, 'stroke-opacity': 0, 'fill-opacity': 0, });
					svg.appendChild( gCircles );
					
					var widthTS = options.countDaysShow * 86400;
					
					for( i = options.accounts.length - 1; i >= 0; i-- ) {
						var account = options.accounts[ i ];
						var points = [];
						var x, y, xPrev, yPrev;
						
						var n = 0;
						for( ts in options.data[ account ] ) {
							n++;
						}

						var j = -1;
						for( ts in options.data[ account ] ) {
							j++;
							var last = (j+1) == n;
							var value = options.data[ account ][ts];
							ts = parseInt( ts );
							x = (ts - options.minTS) / widthTS * net.width + net.left;
							y = net.bottom - ( heightValue ? ( value - minValue ) / heightValue * net.height : 0 );
							x = Math.round( x );
							y = Math.round( y );
							
							if( !last ) {
								if( xPrev && yPrev ) {
									var r = Math.sqrt( Math.pow( xPrev - x, 2 ) + Math.pow( yPrev - y, 2 ));
									if( r < 3 ) continue;
								}
								xPrev = x;
								yPrev = y;
							}
							
							var date = new Date( ts * 1000 );
							var dateStr = commonLib.getDate( date );
							if( !options.days[ dateStr ].show ) continue;
							
							var countDaysHide = getCountDaysHide( dateStr );
							var tsHide = countDaysHide * 86400;
							
							x = (ts - tsHide - options.minTS) / widthTS * net.width + net.left;
							x = Math.round( x );
							
							points.push( x + ',' + y );
							
							if( !inPhone ) {
								var circle = self.makeSVG( 'circle', {
									cx:x, 
									cy:y, 
									r: 4,
									fill:options.colors[account],
									style: ' cursor:pointer; ',
								});
								var hint = "";
								hint += options.ls.lAccount + ': ' + account + "<br>";
								hint += options.ls.lEA + ': ' + options.EA[account] + "<br>";
								hint += options.ls.lTime + ': ' + commonLib.getDT( date ) + "<br>";
								hint += options.ls.lGain + ': ' + value + '%';
								$( circle ).mouseover( { content: hint }, self.markMouseover );
								$( circle ).mouseout( self.markMouseout );
								$( circle ).click( { id: options.EATradeAccounts[account] }, self.markClick );
								gCircles.appendChild( circle );
							}
						}
						
						var date = new Date( options.trueMaxTS * 1000 );
						var dateStr = commonLib.getDate( date );
						var countDaysHide = getCountDaysHide( dateStr );
						var tsHide = countDaysHide * 86400;
						x = (options.trueMaxTS - tsHide - options.minTS) / widthTS * net.width + net.left;
						x = Math.round( x );
						points.push( x + ',' + y );
						
						var polyline = self.makeSVG( 'polyline', {
							points: points.join( ' ' ),
							stroke: options.colors[account],
						});
						gLines.appendChild( polyline );
					}
				}();
				/*
				!function drawLegend() {
					var gLines = self.makeSVG( 'g', { 'stroke-width': 2 });
					svg.appendChild( gLines );
					var gCircles = self.makeSVG( 'g' );
					svg.appendChild( gCircles );
					var gTexts = self.makeSVG( 'g', { 'text-anchor': inPhone ? 'start' : 'middle', 'font-weight': 'bold' });
					svg.appendChild( gTexts );
						
					function drawBlock( x, y, color, account ) {
						var line = self.makeSVG( 'line', {
							x1:x - legendBlock.halfWidth, 
							x2:x + legendBlock.halfWidth, 
							y1:y, 
							y2:y,
							stroke:color,
						});
						gLines.appendChild( line );
						
						if( !inPhone ) {
							var circle = self.makeSVG( 'circle', {
								cx:x, 
								cy:y, 
								r: 4,
								fill:color,
								stroke: 'black', 
							});
							gCircles.appendChild( circle );
						}
						
						var titleAccount = self.makeSVG( 'text', {
							x:inPhone ? x + legendBlock.halfWidth + 10 : x, 
							y:inPhone ? y + 5 : y + 15, 
							fill:color,
						}, inPhone ? account+' ' + options.EA[ account ] : account );
						gTexts.appendChild( titleAccount );
						
						if( !inPhone ) {
							var titleEA = self.makeSVG( 'text', {
								x:inPhone ? x + legendBlock.halfWidth + 10 : x, 
								y:inPhone ? y + 25 : y + 35, 
								fill:color,
							}, options.EA[ account ] );
							gTexts.appendChild( titleEA );
						}
					}
					
					for( i in options.accounts ) {
						var account = options.accounts[ i ];
						if( inPhone ) {
							var x = i <= 4 ? legend.left + legendBlock.halfWidth : legend.left + legendBlock.width + graph.padding + legendBlock.halfWidth;
							var y = i <= 4 ? legend.top + legendBlock.height*i : legend.top + legendBlock.height*( i - 5 );
						}
						else{
							var x = legend.left + legendBlock.halfWidth;
							var y = legend.top + legendBlock.height*i;
							
						}
						drawBlock( x, y, options.colors[account], account );
					}
				}();
				*/
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );