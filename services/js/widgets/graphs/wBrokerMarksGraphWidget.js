var wBrokerMarksGraphWidgetOpen;
!function( $ ) {
	wBrokerMarksGraphWidgetOpen = function( options ) {
		var defLS = {
			
		};
		var defOption = {
			ins: '',
			selector: '.wBrokerMarksGraphWidget',
			colors: [ '#3A87AD' ],
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init: function() {
				self.$ns = $( options.selector );
				self.$wGraph = self.$ns.find( options.ins + '.wGraph' );
				self.$wGraph.$svg = self.$wGraph.find( 'svg' );
				self.$wBabl = self.$ns.find( options.ins + '.wBabl' );
				
				self.detVars();
				
				$(function(){
					$( window ).on( 'resize', self.drawGraph );
				});
			},
			detVars: function() {
				function getDay( date ) {
					var year = date.getFullYear();
					var month = date.getMonth();
					var day = date.getDate();
					return new Date( year, month, day, 0, 0, 0, 0 );
				}
				function getCurrDayTS( ts ) {
					var currDate = new Date( ts * 1000 );
					var currDay = getDay( currDate );
					return currDay.getTime() / 1000;
				}
				function getNextDayTS( ts ) {
					var currDate = new Date( ts * 1000 );
					var currDay = getDay( currDate );
					return currDay.getTime() / 1000 + 60*60*24;
				}
			
				var i = 0;
				var s = 0;
				for( var ts in options.data ) {
					i++;
					s+=options.data[ts].trust;
					var trustAVG = Math.round(s / i);
					options.data[ts].trustAVG = trustAVG;
					
					if( !options.minTS || ts < options.minTS ) options.minTS = ts;
					if( !options.maxTS || ts > options.maxTS ) options.maxTS = ts;
					if( typeof options.minValue == 'undefined' || trustAVG < options.minValue ) options.minValue = trustAVG;
					if( typeof options.maxValue == 'undefined' || trustAVG > options.maxValue ) options.maxValue = trustAVG;
				}
				
				options.trueMaxTS = options.maxTS;
				options.minTS = getCurrDayTS( parseInt( options.minTS ));
				options.maxTS = getNextDayTS( parseInt( options.maxTS ));
				options.widthTS = options.maxTS - options.minTS;
				options.heightValue = options.maxValue - options.minValue;
				
				options.days = {};
				options.daysKeys = [];
				for( var ts = options.minTS; ts<options.maxTS; ts+=86400 ) {
					var date = new Date( ts * 1000 );
					var dateStr = commonLib.getDate( date );
					var day = date.getDay();
					var outlet = day == 6 || day == 0;
					
					options.daysKeys.push( dateStr );
					
					options.days[dateStr] = {
						date: date,
						outlet: outlet,
						show: true,
					};
				}
				
				options.countDays = 0;
				options.countDaysShow = 0;
				options.countDaysHide = 0;
				options.daysKeysShow = [];
				options.countDays = options.daysKeys.length;
				for( dateStr in options.days ) {
					if( options.days[dateStr].show ) {
						options.countDaysShow++;
						options.daysKeysShow.push( dateStr );
					}
					else options.countDaysHide++;
				}
			},
			markMouseover: function( event ) {
				var $mark = $(this);
				
				self.$wBabl.find( '.wShowName' ).html( event.data.showName );
				self.$wBabl.find( '.wDT' ).html( commonLib.getDT( new Date( event.data.ts * 1000 )));
				self.$wBabl.find( '.wConditions' ).html( Math.round( event.data.conditions / 10 ));
				self.$wBabl.find( '.wExecution' ).html( Math.round( event.data.execution / 10 ));
				self.$wBabl.find( '.wSupport' ).html( Math.round( event.data.support / 10 ));
				self.$wBabl.find( '.wIO' ).html( Math.round( event.data.IO / 10 ));
				self.$wBabl.find( '.wSite' ).html( Math.round( event.data.site / 10 ));
				self.$wBabl.find( '.wTrust' ).html( event.data.trust + '%' );
				
				var offset = $mark.offset();
				
				$mark.attr( 'stroke', '#5ebd54' );
				self.$wBabl.show();
				offset.top -= self.$wBabl.height() + 20;
				if( offset.left < 200 ) {
				}
				else if( offset.left + 200 > $(document).width()) {
					offset.left -= Math.round( ( self.$wBabl.width() + 16 ));
				}
				else{
					offset.left -= Math.round( ( self.$wBabl.width() + 16 ) / 2 );
				}
				self.$wBabl.offset( offset );
			},
			markMouseout:function() {
				var $mark = $(this);
				self.$wBabl.hide();
				
				$mark.attr( 'stroke', options.colors[0] );
			},
			makeSVG: function( tag, attrs, inner ) {
				var el = document.createElementNS('http://www.w3.org/2000/svg', tag);
				for( var k in attrs ) {
					el.setAttribute( k, attrs[ k ]);
				}
				if( inner ) {
					el.appendChild( document.createTextNode( inner ));
				}
				return el;
			},
			drawGraph: function() {
				self.$wGraph.html( '' );
				var svg = self.makeSVG( 'svg', { width:'100%', height:'100%' });
				self.$wGraph[0].appendChild( svg );
				
				var graph = {
					width: self.$wGraph.width(),
					height: self.$wGraph.height(),
					padding: 10,
				};
				
				graph.halfHeight = Math.round( graph.height / 2 );
				var inPhone = $(document).width() <= 480;
				
				var asixY = {};
				var asixX = {
					top: graph.height - graph.padding - 10,
				};
				
				var net = {
					top: graph.padding,
					bottom: asixX.top,
				};
				
				net.height = net.bottom - net.top;
				
				function getSizeStep( sizeStep ) {
					if( !sizeStep ) return 0;
					var pow10 = Math.floor(Math.log( sizeStep ) / Math.log( 10 ));
					var step = Math.pow( 10, pow10 );
					sizeStep = Math.ceil( sizeStep / step ) * step;
					if( sizeStep < 10 ) return 10;
					return sizeStep;
				}
				
				var heightStep = 30;
				var nSteps = Math.floor( net.height / heightStep );
				var sizeStep = Math.ceil( options.heightValue / nSteps );
				var sizeStep = getSizeStep( sizeStep );
				if( !sizeStep ) sizeStep = 100;
				var maxValue = sizeStep ? Math.ceil( options.maxValue / sizeStep ) * sizeStep : 0;
				var minValue = sizeStep ? Math.floor( options.minValue / sizeStep ) * sizeStep : 0;
				var heightValue = maxValue - minValue;
				var nSteps = sizeStep ? heightValue / sizeStep : 0;
				var heightStep = nSteps ? net.height / nSteps : 0;

				var widthAsixY = Math.max( maxValue.toString().length, minValue.toString().length ) * 7;
				
				asixY.left = widthAsixY + graph.padding;
				asixX.left = asixY.left;
					
				net.left = asixY.left;
				net.right = graph.width - graph.padding;
				net.width = net.right - net.left;
								
				!function drawNet() {
					!function drawVerticalNet() {
						var gLines = self.makeSVG( 'g', { fill:'none', stroke:'Gainsboro' });
						svg.appendChild( gLines );
						var gTexts = self.makeSVG( 'g', { 'font-size': 11, fill:'Black', 'text-anchor': 'middle' });
						svg.appendChild( gTexts );
						
						if( options.countDaysShow ) {
							var widthDay = net.width / options.countDaysShow;
						
							var stepI = 1;
							var widthDayTmp = widthDay;
							while( widthDayTmp < 40 ) {
								stepI++;
								widthDayTmp = widthDay * stepI;
							}		
							
							for( i = 0; i<options.countDaysShow+1; i++ ) {
								if( i % stepI == 0 ) {
									var x = net.left + widthDay * i;	
									x = Math.round( x );	
									var line = self.makeSVG( 'line', {
										x1:x + 0.5, 
										x2:x + 0.5, 
										y1:net.bottom, 
										y2:net.top,
										'stroke-dasharray': i && i<options.countDaysShow ? 5 : 0,
									});
									gLines.appendChild( line );
								}
								if( i<options.countDaysShow && i % stepI == 0 ) {
									var dateStr = options.daysKeysShow[i];
									var date = commonLib.getShortDate( options.days[ dateStr ].date );
									
									var xTitle = Math.round( x + widthDay/2 );
									var yTitle = net.bottom + 16;
									
									if( xTitle + 15 <= net.right || !inPhone ) {
										var title = self.makeSVG( 'text', {
											x:xTitle, 
											y:yTitle, 
										}, date );
										gTexts.appendChild( title );
									}
								}
							}
						}
					}();
					!function drawGorizontalNet() {
						var gLines = self.makeSVG( 'g', { stroke:'Gainsboro' });
						svg.appendChild( gLines );
						var gTexts = self.makeSVG( 'g', { 'font-size': 11, fill:'Black', 'text-anchor': 'end' });
						svg.appendChild( gTexts );
						
						for( i=0; i<nSteps+1; i++ ) {
							var y = Math.round( net.bottom - heightStep * i );
							
							var line = self.makeSVG( 'line', {
								x1:net.left, 
								x2:net.right, 
								y1:y + 0.5, 
								y2:y + 0.5,
								'stroke-dasharray': i && i<nSteps ? 5 : 0,
							});
							gLines.appendChild( line );
							
							var xTitle = net.left - 5;
							var yTitle = y + 4;
							var title = self.makeSVG( 'text', {
								x:xTitle, 
								y:yTitle, 
							}, (minValue + sizeStep * i).toString() );
							gTexts.appendChild( title );
						}
					}();
				}();
				!function drawData() {
					var gLines = self.makeSVG( 'g', { fill: 'none', 'stroke-width': inPhone ? 2 : 4 });
					svg.appendChild( gLines );
					var gCircles = self.makeSVG( 'g', { stroke: options.colors[0], fill: 'white', 'stroke-width' : 3, 'stroke-opacity': 1, 'fill-opacity': 1, });
					svg.appendChild( gCircles );
					
					var widthTS = options.countDaysShow * 86400;
					
					for( i = 0; i <= 0; i++ ) {
						var points = [];
						var x, y, xPrev, yPrev;
						
						for( ts in options.data ) {
							var value = options.data[ts].trustAVG;
							
							ts = parseInt( ts );
							x = (ts - options.minTS) / widthTS * net.width + net.left;
							y = net.bottom - ( heightValue ? ( value - minValue ) / heightValue * net.height : 0 );
							x = Math.round( x );
							y = Math.round( y );
							
							if( xPrev && yPrev ) {
								var r = Math.sqrt( Math.pow( xPrev - x, 2 ) + Math.pow( yPrev - y, 2 ));
								if( r < 6 ) continue;
							}
							xPrev = x;
							yPrev = y;
							
							var date = new Date( ts * 1000 );
							var dateStr = commonLib.getDate( date );
							
							points.push( x + ',' + y );
							
							if( !inPhone ) {
								var circle = self.makeSVG( 'circle', {
									cx:x, 
									cy:y, 
									r: 6,
									style: ' cursor:pointer; ',
								});
								var data = {
									showName: options.data[ts].showName,
									ts: ts,
									conditions: options.data[ts].conditions,
									execution: options.data[ts].execution,
									support: options.data[ts].support,
									IO: options.data[ts].IO,
									site: options.data[ts].site,
									trust: options.data[ts].trust,
								};
								$( circle ).mouseover( data, self.markMouseover );
								$( circle ).mouseout( self.markMouseout );
								gCircles.appendChild( circle );
							}
						}
						
						var polyline = self.makeSVG( 'polyline', {
							points: points.join( ' ' ),
							stroke: options.colors[0],
						});
						gLines.appendChild( polyline );
					}
				}();
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );