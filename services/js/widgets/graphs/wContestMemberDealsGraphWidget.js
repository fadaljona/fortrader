var wContestMemberDealsGraphWidgetOpen;
!function( $ ) {
	wContestMemberDealsGraphWidgetOpen = function( options ) {

		var defOption = {
			ins: '',
			selector: '.wContestMemberDealsGraphWidget',
			ajaxLoadGraphURL: '/contestMember/ajaxLoadMemberDealsGraph',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init: function() {
				self.$ns = $( options.selector );
				self.$wShowGraph = self.$ns.find( options.ins+'.wShowGraph' );
				self.$wShowGraph.click( self.showGraph );
				
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			showGraph:function() {
				self.disable();
				var data = {};
				data.contestMemberId = options.contestMemberId;
				$.getJSON( yiiBaseURL+options.ajaxLoadGraphURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$ns.html( data.graph );
					}
				});
				return false;
			},
			
		};
		self.init();
		return self;
	}
}( window.jQuery );