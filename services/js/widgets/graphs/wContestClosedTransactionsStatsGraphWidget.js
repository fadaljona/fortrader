var wContestMemberDealsGraphWidgetOpen;
!function( $ ) {
	wContestClosedTransactionsStatsGraphWidgetOpen = function( options ) {

		var defOption = {
			ins: '',
			selector: '.wContestClosedTransactionsStatsGraphWidget',
			ajaxLoadGraphURL: '/contest/ajaxLoadClosedTransactionsStatsGraph',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init: function() {
				self.$ns = $( options.selector );
				self.$wShowGraph = self.$ns.find( options.ins+'.wShowGraph' );
				self.$wShowGraph.click( self.showGraph );
				self.$tplItem = self.$ns.find( options.ins+'.wTplItem' );
			},
			showGraph:function() {
				
				self.$wShowGraph.parent().hide();
				self.$tplItem.show();
				var data = {};
				data.idContest = options.idContest;
				$.getJSON( yiiBaseURL+options.ajaxLoadGraphURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$ns.html( data.graph );
					}
				});
				return false;
			},
			
		};
		self.init();
		return self;
	}
}( window.jQuery );