var wQuoteOneChartWidgetOpen;
!function( $ ) {
	wQuoteOneChartWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wQuoteOneChartWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$chartSelector = options.chartId;
				self.$currentPeriod = self.$ns.find( 'li.changePeriod.active' ).data('period');
				self.$chartData = self.parseCSV( options.periods[self.$currentPeriod]['data'] );
				
				self.$spinner = self.$ns.find( '.spinner-wrap' );
				
				self.$chart = self.makeChart();
				self.$chart.addListener("rendered", self.zoomChart);
				
				self.$chart.addListener("rendered", self.removeSpinner);
				self.$chart.addListener("dataUpdated", self.removeSpinner);
				
				self.$wChangePeriod = self.$ns.find( 'li.changePeriod' );
				self.$wChangePeriod.click( self.changeChartPeriod );
			},
			removeSpinner:function() {
				self.$spinner.addClass('hide');
			},
			chartValidateData:function() {
				self.$chart.validateData(); 
			},
			getLocalTimeForData:function(timeStr) {
				var $date = new Date( timeStr * 1000 ); //new Date(year, month, day, hours, minutes, seconds, milliseconds)
				//var timezoneOffset = $date.getTimezoneOffset() + 60 * (2+options.timeDataOffset);
				//$date.setMinutes ( $date.getMinutes() + timezoneOffset * (-1) );
				
				var year, month, day, hours, minutes;
				
				year = $date.getFullYear();
				
				if( $date.getMonth() < 9 ){
					var tmpMo = $date.getMonth() + 1;
					month = '0' + tmpMo;
				}else{ month = $date.getMonth() + 1; }
				
				if( $date.getDate() < 10 ){
					day = '0' + $date.getDate();
				}else{ day = $date.getDate(); }
				
				if( $date.getHours() < 10 ){
					hours = '0' + $date.getHours();
				}else { hours = $date.getHours(); }
				
				if( $date.getMinutes() < 10 ){
					minutes = '0' + $date.getMinutes();
				}else{ minutes = $date.getMinutes(); }
				
				return year+'-'+month+'-'+day+' '+hours+':'+minutes;
			},
			changeChartPeriod:function() {
				var period = $(this).data('period');
				if( period != self.$currentPeriod ){
					
					self.$spinner.removeClass('hide');
					
					self.loadDataForChart( period );
				}
				return false;
			},
			loadDataForChart:function( period ) {
				if( self.loading ) return false;
				self.loading = true;
				$.get( options.statsHistoryUrl, {symbolId: options.symbolId, period: options.periods[period]['periodVal'] }, function ( data ) {
					if( data ){
						self.$ns.find( 'li.changePeriod' ).removeClass('active');
						self.$chart.dataProvider = self.parseCSV( data );
						
						self.$chart.categoryAxis.minPeriod = options.periods[period]['minPeriod'];
						self.$chart.chartCursor.categoryBalloonDateFormat = options.periods[period]['categoryBalloonDateFormat'];
						
						self.chartValidateData();
						
						self.zoomChart();
						self.$currentPeriod = period;
					
						self.$ns.find( 'li.changePeriod[data-period='+period+']' ).addClass('active');
					}
					self.loading = false;
				} );
				return false;
			},
			zoomChart:function() {
				self.$chart.zoomToIndexes(self.$chart.dataProvider.length - 50, self.$chart.dataProvider.length - 1);
			},
			makeChart:function() {
				return AmCharts.makeChart( self.$chartSelector, {
						"type": "serial",
						"zoomOutText": "",
						"fontSize": 1,
						"creditsPosition": "bottom-left",
						"autoMargins": false,
						"marginTop": 0,
						"marginRight": 0,
						"marginBottom": 0,
						"marginLeft": 0,
						"valueAxes": [{
							"position": "left"
						}],
						"language": options.lang,
						"dataDateFormat":"YYYY-MM-DD JJ:NN",
						"graphs": [{
							"id": "g1",
							"balloonText": "Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>",
							"balloonColor": "#ff2d2e",
							"closeField": "close",
							"fillColors": "#2c88cb",
							"highField": "high",
							"lineColor": "#2c88cb",
							"lineAlpha": 1,
							"lowField": "low",
							"fillAlphas": 1,
							"negativeFillColors": "#ff2d2e",
							"negativeLineColor": "#ff2d2e",
							"openField": "open",
							"title": "Price:",
							"type": "candlestick",
							"valueField": "close"
						}],
						"chartScrollbar": {
							"graph": "g1",
							"graphType": "line",
							"scrollbarHeight": 30
						},
						"chartCursor": {
							"pan": true,
							"valueLineEnabled": true,
							"valueLineBalloonEnabled": true,
							"categoryBalloonDateFormat": options.periods[self.$currentPeriod]['categoryBalloonDateFormat'],
							"categoryBalloonColor": "#ff2d2e",
							"cursorColor": "#ff2d2e",
						},
						"categoryField": "date",
						"categoryAxis": {
							"parseDates": true,
							"minPeriod": options.periods[self.$currentPeriod]['minPeriod'],
							"equalSpacing": true,
							"inside": true,
							"fontSize": 8,
						},
						"valueAxes": [{
							"inside": true,
							"fontSize": 8,
						}],
						"balloon": {
							"fontSize": 9,
						}, 
						"dataProvider": self.$chartData,
					});
			},
			parseCSV:function(data) {
				var returnArr = [];
				if(!data) return returnArr;
				data = data.replace (/\r\n/g, "\n");
				data = data.replace (/\r/g, "\n");
				var rows = data.split("\n");
				for (var i = 0; i < rows.length; i++) {
					if (rows[i]) {
						var column = rows[i].split(";");
						var dataObject = {
							date: self.getLocalTimeForData(column[0]),
							open: column[1],
							high: column[2],
							low: column[3],
							close: column[4],
							volume: column[5]
						};
						returnArr.push(dataObject);
					}
				}
				return returnArr;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );