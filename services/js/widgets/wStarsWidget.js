var wStarsWidgetOpen;
!function( $ ) {
	wStarsWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wStarsWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				
				self.$wCurrentValue = self.$ns.find( '.wCurrentValue' );
				self.$wValue = self.$ns.find( '.wValue' );
				
				self.$ns.on( 'mouseenter', self.onEnter );
				self.$ns.on( 'mouseleave', self.onLeave );
				self.$ns.on( 'mouseenter', '.wValue', self.onEnterValue );
				self.$ns.on( 'click', '.wValue', self.onClickValue );
				
				if( options.onChangeValue ) {
					self.$wValue.css( 'cursor', 'pointer' );
				}
			},
			onEnter:function() {
				if( options.onChangeValue ) {
					self.$wCurrentValue.hide();
					self.$wValue.show();
				}
			},
			onLeave:function() {
				if( options.onChangeValue ) {
					self.$wCurrentValue.show();
					self.$wValue.hide();
				}
			},
			onEnterValue:function() {
				$(this).prevAll( '.wValue' ).andSelf().addClass( 'active' );
				$(this).nextAll( '.wValue' ).removeClass( 'active' );
			},
			onClickValue:function() {
				var value = $(this).attr( 'data-value' );
				var i = $(this).prevAll( '.wValue' ).andSelf().length;
				var width = Math.round(i / self.$wValue.length * 100);
				self.$wCurrentValue.css( 'width', width.toString() + '%' );
				
				if( options.onChangeValue ) {
					eval( options.onChangeValue + "( value );" );
				}
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );