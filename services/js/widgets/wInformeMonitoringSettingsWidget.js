var wInformeMonitoringSettingsWidgetOpen;
!function( $ ) {
	wInformeMonitoringSettingsWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wInformeMonitoringSettingsWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			informerParams: {},
			informerUrl: '',
			loading:false,
			windowLoaded:false,
			changedDefaultColors: false,
			prevStyleListVal: false,
			defaultColors: false,
			informerWidgetLoaded: false,
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				$(window).load( self.setWindowLoaded );
				
				self.$ns.find('.toggle_btn').click( self.loadInformerSettingsWidget );
				
			},
			loadInformerSettingsWidget: function(){
				if( self.informerWidgetLoaded ) return false;
				self.disable();
				$.getJSON( options.loadContentUrl, {id:options.idMember}, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$ns.find('.toggle_content').html(data.content);
						self.informerWidgetLoaded = true;
						self.initInformerSettingsContent();
					}
					
					self.loading = false;
					self.enable();
				});
			},
			initInformerSettingsContent: function(){
				self.$informerColorsBox = self.$ns.find( '.informerColorsBox' );
				
				self.$previewInformerWrap = self.$ns.find( '.previewInformerWrap' );
				self.$informerCodeWrap = self.$ns.find( '.informerCodeWrap' );
				self.$informerColorsPreview = self.$ns.find( '.informerColorsPreview' );
				
				self.$stylesList = self.$ns.find( '.stylesList' );
			
				self.$sliderWidthBox = self.$ns.find( '.sliderWidthBox' );
				self.$widthVal = self.$ns.find( '.widthVal' );
	
				self.styleSelected();
				
				self.$ns.find('.imgMonitoringInformer').click( self.styleClicked );

				self.$widthVal.on('change', function() { self.changeWidth(); });
				self.$widthVal.keydown( self.filterInpWidth );

				$('.colorInformer').click(function(){
					$(this).parent('.section_offset').find('.box2').slideToggle('slow');
				})
				
				var clipboard = new Clipboard('.copyCode');
				clipboard.on('success', function(e) {
				});
				clipboard.on('error', function(e) {
				});
			},
			styleClicked: function(){
				if( $(this).find('div.selected').length ) return false;
				self.$stylesList.find('div.selected').remove();
				$(this).append('<div class="selected"></div>');
				self.styleSelected();
				self.$informerColorsPreview.html('');
				return false;
			},
			disable: function(){
				self.$ns.append( self.spinner );
			},
			enable: function(){
				self.$ns.find('.spinner-wrap').remove();
			},
		
			
	
			
			getSelectedStyleId: function(){
				return self.$stylesList.find('.imgMonitoringInformer .selected').closest('.imgMonitoringInformer').attr('data-styleId');
			},
			styleSelected: function(){
				if( self.loading ) return false;
				self.loading = true;
				self.disable();
				self.changedDefaultColors = false;
				self.setDefaultStyleColors();					
			},
			
			setDefaultStyleColors: function(){
				$.getJSON( options.getInformerColors, {st:self.getSelectedStyleId()}, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$informerColorsBox.html( data.data );
						self.defaultColors = data.data;
						
						if( self.windowLoaded ){
							self.initColorPicker();
						}else{
							$(window).load( self.initColorPicker );
						}
						self.$widthVal.val( data.width );

					}
					self.changedSettings();
					self.loading = false;
					self.enable();
				});
			},
			setWindowLoaded: function(){
				self.windowLoaded = true;
			},
			initColorPicker: function(){
				if( self.$ns.find(".colorSelector").length){
					self.$ns.find(".colorSelector").each(function(){
						var $this = $(this);

						$this.css('background-color', $this.attr('data-bg') );
						$this.attr({'data-cpp-color': $this.attr('data-bg')});
						
						$this.closest('.change_color_input').find('.col3 input').val( $this.attr('data-bg') );
						
						$this.colorpickerplus();
						
						$this.on('changeColor', function(e, color){					
							if(color!=null){
								$(this).css('background-color', color);
								$(this).closest('.change_color_input').find('.col3 input').val( color );
							}
						});
						$this.on('hidePicker', function(e, color){					
							self.changedDefaultColors = true;
							self.showInformerColorsPreview();
							self.changedSettings();
						});
						
					});
				}
			},
			
			
			showInformerColorsPreview: function(){

				var informerColorsPreviewParams = {};
				informerColorsPreviewParams['st'] = self.getSelectedStyleId();
				informerColorsPreviewParams['accId'] = options.idMember;
				informerColorsPreviewParams['colors'] = self.getColors();
				informerColorsPreviewParams['cat'] = options.catId;
				informerColorsPreviewParams['w'] = self.$widthVal.val();
				var informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(informerColorsPreviewParams);
		
				var frameCode = '<img src="' + informerUrl + '"></img>';
				
				self.$informerColorsPreview.html( frameCode );
			},
			
			encodeQueryData:function( informerParams ){
				var ret = [];
				for ( var param in informerParams ){
					ret.push( encodeURIComponent( param ) + "=" + encodeURIComponent( informerParams[param] ) );
				}
				return ret.join("&");
			},
			changedSettings:function() {	
				self.disable();
				self.updateInformer();
			},
			updateInformer:function() {				
				self.informerParams = {};
				self.informerParams['st'] = self.getSelectedStyleId();
				self.informerParams['cat'] = options.catId;
				self.informerParams['accId'] = options.idMember;
				
				self.informerParams['w'] = self.$widthVal.val();
				

				var colorsSelected = self.getColors();
				if( colorsSelected ) self.informerParams['colors'] = colorsSelected;
			
				
				self.informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(self.informerParams);
					
				var frameCode = '<img src="' + self.informerUrl + '"></img>';
				
				self.$previewInformerWrap.html( frameCode )
				
				self.$informerCodeWrap.val( '<a href="'+options.memberUrl+'" rel="nofollow noopener noreferrer" target="_blank">' + frameCode + '</a>' );
				
				self.enable();

				return false;
			},

			getColors:function() {	
				if( !self.changedDefaultColors ) return false;
				var strVal = '';
				self.$informerColorsBox.find( 'input' ).each(function(index, element){
					var delimiter = '';
					var val = $(this).val();
					if( index > 0 ) delimiter = ',';
					if( val.charAt(0) === '#' ) val = val.substr(1);
					strVal = strVal + delimiter + $(this).attr('name') + '=' + val;
				});
				
				return strVal;	
			},

		
			changeWidth: function(){
				var value = parseInt( self.$widthVal.val() );
				if( value < options.minWidth ){
					value = options.minWidth;
					self.$widthVal.val( value );
				}else if( value > options.maxWidth ){
					value = options.maxWidth;
					self.$widthVal.val( value );
				}
				self.changedSettings();
			},
			filterInpWidth: function(e){
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
					// Allow: Ctrl+A
					(e.keyCode == 65 && e.ctrlKey === true) ||
					// Allow: Ctrl+C
					(e.keyCode == 67 && e.ctrlKey === true) ||
					// Allow: Ctrl+X
					(e.keyCode == 88 && e.ctrlKey === true) ||
					// Allow: home, end, left, right
					(e.keyCode >= 35 && e.keyCode <= 39)) {
					// let it happen, don't do anything
					return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			},

		};
		self.init();
		return self;
	}
}( window.jQuery );