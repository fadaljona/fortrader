var wQuotesInformerWidgetOpen;
!function( $ ) {
	wQuotesInformerWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wQuotesInformerWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			fieldsToUpdate: {},
			columnsToUpdate: {},
			availableColumns: {
				Ask: 'ask', 
				Bid: 'bid', 
				HighQuote: 'high', 
				LowQuote: 'low', 
				Chg: 'chg', 
				ChgPer: 'chgPer', 
				Time: 'time'
			},
			init:function() {
				self.$ns = $( options.selector );
				self.$wTablQuotes = self.$ns.find( '.ticker_quotes_table' );
				self.$wQuotesTime = self.$ns.find( '.Time' );
				
				self.$wWindow = $(window);
				self.$wDocument = $(document);
				self.$wDocument.ready( self.updateTimeToLocal );

				self.setUpColumns();
				
				self.$wDigitalWatchEl = self.$ns.find( '.timeTr span' );
				
				if( options.noReal == 0 ){
					self.$wWindow.load( self.subscribeToQuotes );
					self.$wWindow.load( self.digitalWatch( self.$wDigitalWatchEl ) );
				}
				

			},
			digitalWatch:function(el) {
				if(el.length){
					var self = this;
					var date = new Date(Date.now()+3*60*60*1000);
					var year = date.getUTCFullYear();
					var month = date.getUTCMonth() + 1;
					var day = date.getUTCDate();
					var hours = date.getUTCHours();
					var minutes = date.getUTCMinutes();
					if (month < 10) month = "0" + month;
					if (day < 10) day = "0" + day;
					if (hours < 10) hours = "0" + hours;
					if (minutes < 10) minutes = "0" + minutes;
					self.$wDigitalWatchEl.html( day + '.' + month + '.' + year + ' ' + hours + ":" + minutes );
					setTimeout(function(){
						self.digitalWatch(el);
					},15 * 1000);
				}
			},
			setUpColumns:function() {
				for (var i = 0; i < options.columns.length; i++) {
					if( self.availableColumns[options.columns[i]] ) self.columnsToUpdate[ self.availableColumns[options.columns[i]] ] = options.columns[i];
				}
			},
			subscribeToQuotes:function() {		
				var conn = new ab.Session(options.connUrl,
					function() {
						conn.subscribe(options.quotesKey, function(topic, data) {
							self.updateQuotes(data);
						});
						console.warn('WebSocket connection opened for key ' + options.quotesKey);
					},
					function() {
						console.warn('WebSocket connection closed');
					},
					{'skipSubprotocolCheck': true}
				);
			},
			updateQuotes:function(data) {
				for(var key in data) {
					var parsedData = JSON.parse(data[key]);
					self.updateQuotesWithAnimate(key, parsedData);
				}
			},
			updateQuotesWithAnimate:function(key, parsedData) {
				
				for ( columnKey in self.columnsToUpdate ) {
					self.fieldsToUpdate[ columnKey ] = $(options.selector+' .pid-' + key + '-' + self.columnsToUpdate[columnKey] );
				}		
				
				var pageBid = $(options.selector+' .pid-' + key + '-bid'),
					pageChg = $(options.selector+' .pid-' + key + '-chg'),
					pageChgPercent = $(options.selector+' .pid-' + key + '-chg-percent'),
					pageTime = $(options.selector+' .pid-' + key + '-time');
					
				
				var currentTr = self.$wTablQuotes.find('tr[data-symbol="' + key + '"]');
					newBid = parseFloat(parsedData.bid),
					precision = currentTr.data('precision'),
					lastBid = currentTr.data('lastBid'),
					oldBid = parseFloat( currentTr.attr('data-old-bid') );


				if( !isNaN(lastBid) ){
					var lastBidVal = parseFloat( lastBid );
					var prevChgVal = parseFloat( currentTr.attr('data-prev-chg-val') );
					
					var newChgVal = parsedData.bid - lastBidVal;
					var newChgPercentVal = newChgVal / lastBidVal * 100 ;
					var sign = '';
					if( newChgVal > 0 ) sign = '+';
					
					if( self.fieldsToUpdate.chg ){
						self.fieldsToUpdate.chg.html( sign + newChgVal.toFixed(precision) );
						if( prevChgVal < 0 && newChgVal > 0 || prevChgVal > 0 && newChgVal < 0 ){
							self.changeColorAndBg(self.fieldsToUpdate.chg, prevChgVal, newChgVal);
						}
					}
					if( self.fieldsToUpdate.chgPer ){
						self.fieldsToUpdate.chgPer.html( sign + newChgPercentVal.toFixed(precision) );
						if( prevChgVal < 0 && newChgVal > 0 || prevChgVal > 0 && newChgVal < 0 ){
							self.changeColorAndBg(self.fieldsToUpdate.chgPer, prevChgVal, newChgVal);
						}
					}
					currentTr.attr({'data-prev-chg-val': newChgVal});
				}
				
				for ( columnKey in self.fieldsToUpdate ) {
					if( columnKey != 'chg' && columnKey != 'chgPer' && columnKey != 'high' && columnKey != 'low' && columnKey != 'time' ){
						self.fieldsToUpdate[columnKey].html(parsedData[columnKey]);
					}
					if( columnKey == 'time' ){
						self.fieldsToUpdate[columnKey].html( self.getLocalTimeForData(parsedData[columnKey]) );
					}
					if( columnKey == 'high' ){
						var oldHigh = self.fieldsToUpdate['high'].text();
						if( !isNaN(oldHigh) ){
							var oldHighVal = parseFloat( oldHigh );
							if( newBid > oldHighVal ){
								self.fieldsToUpdate['high'].html( newBid.toFixed(precision) );
								self.changeColorAndBg( self.fieldsToUpdate['high'], oldHighVal, newBid );
							}
						}else{
							self.fieldsToUpdate['high'].html(parsedData['bid']);
						}
						
					}
					if( columnKey == 'low' ){
						var oldLow = self.fieldsToUpdate['low'].text();
						if( !isNaN(oldLow) ){
							var oldLowVal = parseFloat( oldLow );
							if( newBid < oldLowVal ){
								self.fieldsToUpdate['low'].html( newBid.toFixed(precision) );
								self.changeColorAndBg( self.fieldsToUpdate['low'], oldLowVal, newBid );
							}
						}else{
							self.fieldsToUpdate['low'].html(parsedData['bid']);
						}
						
					}
					if( columnKey == 'bid' ){
						self.changeColorAndBg( self.fieldsToUpdate['bid'], oldBid, newBid );
					}
				}

					
				self.changeProfit(currentTr, oldBid, newBid);

	
					
				setTimeout(function() {
					if ( newBid > oldBid || newBid < oldBid ) {
						currentTr.find('td').removeClass('loss_bg profit_bg');
					}
					
				}, 1250);
				
				
				currentTr.attr({'data-old-bid': parsedData.bid});
				
			},
			changeProfit:function(element, oldVal, newVal) {

				if ( newVal > oldVal ){
					element.removeClass('lesion');
					element.addClass('profit');
				}else if ( newVal < oldVal ){
					element.removeClass('profit');
					element.addClass('lesion');
				}
			},
			changeColorAndBg:function(element, oldVal, newVal) {
				if ( newVal > oldVal ){
					element.removeClass('lesionColor loss_bg');
					element.addClass('profitColor profit_bg');
				}else if ( newVal < oldVal ){
					element.removeClass('profitColor profit_bg');
					element.addClass('lesionColor loss_bg');
				}
			},


			updateTimeToLocal:function() {
				var timeRegex = new RegExp('^[0-9]{2}\:[0-9]{2}\:[0-9]{2}');
				self.$wQuotesTime.each(function(index, element){
					var $this = $(this);
					var startTime = $this.text();
					if( startTime != '' && timeRegex.test( startTime ) ){ 
						$this.html( self.getLocalTimeForData( startTime ) );
					}
				});
			},
			getLocalTimeForData:function(timeStr) {
				var timeArr = timeStr.split(":");
					
				var $date = new Date(2015, 1, 1, timeArr[0], timeArr[1], timeArr[2], 0); //new Date(year, month, day, hours, minutes, seconds, milliseconds)
				var timezoneOffset = $date.getTimezoneOffset() + 60 * options.timeDataOffset;
				$date.setMinutes ( $date.getMinutes() + timezoneOffset * (-1) );
				var hours, minutes, seconds;
				
				if( $date.getHours() < 10 ){
					hours = '0' + $date.getHours();
				}else { hours = $date.getHours(); }
				
				if( $date.getMinutes() < 10 ){
					minutes = '0' + $date.getMinutes();
				}else{ minutes = $date.getMinutes(); }
				
				if( $date.getSeconds() < 10 ){
					seconds = '0' + $date.getSeconds();
				}else{ seconds = $date.getSeconds(); }
				
				return hours + ':' + minutes + ':' + seconds;
			},
			
		};
		self.init();
		return self;
	}
}( window.jQuery );