var wNewsAggregatorWidgetOpen;
!function( $ ) {
	wNewsAggregatorWidgetOpen = function( options ) {
		var defOption = {
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$newsList = self.$ns.find( '.rssNewsList ul');
				self.$loadBtn = self.$ns.find( '.loadOldNews');
				self.$loadNewNewsBtn = self.$ns.find( '.loadNewNews');
				self.$newNewsWrap = self.$ns.find( '.newNews');
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$loadBtn.click( self.showMore );
				self.$loadNewNewsBtn.click( self.showNewNews );
				self.subscribeToRssNews();
				
			},
			showNewNews: function(){
				var newNews = self.$newNewsWrap.find('li.news_big_list_item');
				if( !newNews.length ) return false;
				newNews.css({display:'none'});
				newNews.addClass('newsToInsert');
				newNews.find('.news_top_box').css({'background-color':'#ff1616'});
				self.$newsList.prepend( self.$newNewsWrap.html() );
				self.$newNewsWrap.html('');
				self.$loadNewNewsBtn.slideUp(500);
				self.$newsList.find('.newsToInsert').slideDown(500);
				self.$newsList.find('.newsToInsert .news_top_box').animate({
					backgroundColor: '#f6f6f6',
				}, 1500, function() {
				});
				self.$newsList.find('li.news_big_list_item').removeClass('newsToInsert');
				self.$loadNewNewsBtn.html('');
				return false;
			},
			getNewsShowText:function( count, lang ){
				if( lang != 'ru' ) return 'Show <span class="red_color">' + count + ' new</span> news';
				var intCount = parseInt(count);
				var ostH = intCount % 100;
				var ostD = intCount % 10;
				if( ostH >= 11 && ostH <= 19 || ostD == 0 || ostD >= 5 && ostD <= 9 ){
					return 'Показать <span class="red_color">'+count+' новых</span> новостей';
				}else if( ostD == 1 ){
					return 'Показать <span class="red_color">'+count+' новую</span> новость';
				}else if( ostD >= 2 && ostD <= 4 ){
					return 'Показать <span class="red_color">'+count+' новые</span> новости';
				}
			},
			insertNewNews:function(data){
				self.$newNewsWrap.append(data);
				if( self.$loadNewNewsBtn.html() == '' ) self.$loadNewNewsBtn.slideDown(500);
				self.$loadNewNewsBtn.html( self.getNewsShowText( self.$newNewsWrap.find('li.news_big_list_item').length, options.lang ) );
			},
			subscribeToRssNews:function() {		
				var conn = new ab.Session(options.connUrl,
					function() {
						console.warn('WS connection opened');
						conn.subscribe(options.wsKey, function(topic, data) {
							self.insertNewNews(data);
						});
					},
					function() {
						console.warn('WebSocket connection closed');
					},
					{'skipSubprotocolCheck': true}
				);
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			showMore:function() {
				if( self.loading ) return false;
				self.disable();
				self.loading = true;
				var lastTime = self.$newsList.find( 'li').last().attr('data-time');

				$.getJSON( options.ajaxLoadListURL, {lastTime: lastTime, currentCat: options.currentCat}, function ( data ) {
					self.loading = false;
					if( parseFloat(data.pagesCount) == 1 ) self.$loadBtn.remove();
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$newsList.append( data.list );
					}
					self.enable();
				});
				return false;
			}
		};
		self.init();
		return self;
	}
}( window.jQuery );