var wCurrencyRatesArchiveWidgetOpen;
!function( $ ) {
	wCurrencyRatesArchiveWidgetOpen = function( options ) {
		var defOption = {
			ajax: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				
				
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
				
				self.$yearsBlock = self.$ns.find('.choose_years');
				self.$monthsBlock = self.$ns.find('.chooseMonth');
				self.$monthsList = self.$monthsBlock.find('ul.choose_date_bottom');
				self.$daysBlock = self.$ns.find('.chooseDay');
				self.$daysList = self.$daysBlock.find('ul.choose_date_bottom');
				
				self.$list = self.$ns.find('.CurrencyRatesArchiveListWidget');
				
				/*self.$yearsBlock.find('.choose_date_bottom a').click( self.chooseYear );
				self.$monthsBlock.on('click', 'li a', self.chooseMonth);
				self.$daysBlock.on('click', 'li a', self.chooseDay);*/
	
				self.chooseDate();
				
			},
			chooseDay: function(){
				if( $(this).closest('li').hasClass('current') ) return false;
				if( self.loading ) return false;
				
				self.loading = true;
				self.disable();
				$.getJSON( options.archiveDataUrl, {year:self.$yearsBlock.find('ul li.current a').text(), mo:self.$monthsBlock.find('ul li.current').attr('data-mo'), day:$(this).closest('li').attr('data-day') }, function ( data ) {
					self.loading = false;
					self.enable();
					if( data.error ) {
						alert( data.error );
					}else{
						self.$list.html( data.list );
					}
				});
				
			},
			chooseMonth: function(){
				if( self.loading ) return false;
				
				self.loading = true;
				self.disable();
				self.$daysBlock.slideUp('fast');
				
				$.getJSON( options.archiveDataUrl, {year:self.$yearsBlock.find('ul li.current a').text(), mo:$(this).closest('li').attr('data-mo') }, function ( data ) {
					self.loading = false;
					self.enable();
					if( data.error ) {
						alert( data.error );
					}else{
						self.$daysList.html( data.days );
						self.$daysBlock.find('.choosedItem').text('');
						self.$daysBlock.slideDown('fast');
						self.$list.html( data.list );
					}
				});
				
			},
			chooseYear: function(){
				if( self.loading ) return false;
				
				self.loading = true;
				self.disable();
				self.$monthsBlock.slideUp('fast');
				self.$daysBlock.slideUp('fast');
				$.getJSON( options.archiveDataUrl, {year:$(this).text() }, function ( data ) {
					self.loading = false;
					self.enable();
					if( data.error ) {
						alert( data.error );
					}else{
						self.$monthsList.html( data.months );
						self.$monthsBlock.find('.choosedItem').text('');
						self.$monthsBlock.slideDown('fast');
						self.$list.html( data.list );
					}
				});
				return false;
			},
			disable:function() {
				self.$ns.find('.CurrencyRatesArchiveListWidget').append( self.spinner );
				self.$ns.find('.choose_date_box').append( self.spinner );
			},
			enable:function(){
				self.$ns.find( '.spinner-wrap' ).remove();
			},
			chooseDate: function(){
				self.$ns.find('.choose_date_box .choose_date_list').each(function( ) {
					var $this = $(this);
					var boxWidth = $this.width();
					var $currentLi = $this.find('li.current');
					
					if( $currentLi.length ){
						var activeOffset = $currentLi.position();
						$this.stop().animate({
							'scrollLeft': activeOffset.left - boxWidth/2,
						});
					}	
				});
				self.$ns.on('click', '.choose_date .navigation_box_2>a', function(){

					var $this = $(this),
						parent = $this.closest('.choose_date'),
						box = parent.find('.choose_date_list'),
						boxWidth = box.width(),
						boxScroll = box.scrollLeft();

					if($this.hasClass('next')){

						box.stop().animate({
							'scrollLeft': boxScroll+boxWidth/2
						});

					}
					else{

						box.stop().animate({
							'scrollLeft': boxScroll - boxWidth/2
						});

					}

				});

			},
		};
		self.init();
		return self;
	}
}( window.jQuery );