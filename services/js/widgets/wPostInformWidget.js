var wPostInformWidgetOpen;
!function( $ ) {
	wPostInformWidgetOpen = function( options ) {
		var defLS = {
			
		};
		var defOption = {
			selector: '.wPostInformWidget',
			ins: '',
			minWidthPost: 300,
			padding:12,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			moving: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$wLeft = self.$ns.find( options.ins+'.wLeft' );
				self.$wRight = self.$ns.find( options.ins+'.wRight' );
				self.$wPosts = self.$ns.find( options.ins+'.wPosts' );
				self.$wPost = self.$ns.find( options.ins+'.wPost' );
				
				self.$wLeft.click( {direction: 'left'}, self.move );
				self.$wRight.click( {direction: 'right'}, self.move );
				
				$(window).on( 'resize', self.resize );
				self.resize();
			},
			getCountVisible: function() {
				var widthBlock = self.$wPosts.width();
				return Math.floor( widthBlock / options.minWidthPost );
			},
			move: function( event ) {
				if( self.moving ) return false;
				var right = event.data.direction == 'right';
				var left = !right;
								
				var d = right ? '-=' : '+=';
				var count = self.getCountVisible();
				var widthBlock = self.$wPosts.width();
				var widthPost = widthBlock / count;
				var widthMove = Math.round( widthPost * count );
				
				var $old = self.$wPost.filter( ':visible' );
				if( right && $old.length < count ) return false;
				
				
				var $new = right
					? $($old[count-1]).nextAll( ':lt('+count+')' ) 
					: $($old[0]).prevAll( ':lt('+count+')' );
				
				if( $new.length == 0 ) return false;
				$new.show();

				var $visible = self.$wPost.filter( ':visible' );

				if( left ) {
					$visible.css( 'left', '-'+widthMove+'px' );
				}
				
				self.moving = true;
				var i = 0;
				$visible.animate( { 'left': d+widthMove+'px' }, function() {
					i++;
					if( i == $visible.length ) {
						$visible.css( 'left', '0px' );
						$old.hide();
						self.moving = false;
					}
				});
				return false;
			},
			resize: function() {
				var count = self.getCountVisible();
				var width = commonLib.round( 100 / count, 2 ) + '%';
				self.$wPost.css( { left: 0, width: width } );
				
				self.$wPost.filter( ':gt('+(count-1)+')' ).hide();
				self.$wPost.filter( ':lt('+count+')' ).show();
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );