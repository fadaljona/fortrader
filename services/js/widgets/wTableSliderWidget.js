var TableSlider
;(function($){

	"use strict";

	TableSlider = {

		DOMReady: function(){

			var self = this;
			self.tablQuotesSlider.init();
			
		},

		tablQuotesSlider : {

			init: function(){

				var self = this;
				
				self.body = $('body');
				
				self.ww = $(window);
				self.table = self.body.find('.tabl_quotes table');
				self.tableNav = self.body.find('.tabl_quotes_nav');
				self.nav = self.body.find('tabl_quotes_nav');
				self.itemWidth;
				self.itemQt = self.table.attr('data-item') ? self.table.attr('data-item') : 4;

				self.body.find('.tabl_quotes tr').addClass('tabl_quotes_item');

				self.createBtn();
				self.WidthSlideItem();
				self.moveSlider();

				$(window).on("resize", function(){

					self.WidthSlideItem();
				
				});

			},

			WidthSlideItem: function(){

				var self = this,
					wrapBox = self.body.find('.tabl_quotes').parents('.tabs_contant').length ? self.body.find('.tabl_quotes').parents('.tabs_contant').width() : self.body.find('.tabl_quotes').width();

				self.itemWidth = wrapBox/self.itemQt;

				if(self.ww.width() < 620){

					self.body.find('.tabl_quotes tr').width(self.itemWidth);

				}

				self.table.each(function(){

						var $this = $(this),
							qItem = $(this).find("tbody>tr").length;
						$(this).width(qItem*self.itemWidth +1);

				});
				
				if(self.ww.width() < 620){

					/*set first td height*/
					
					self.table.each(function(){

						var $this = $(this),
							firstTdHeight = 0,
							$trs = $(this).find("tbody>tr");
							
						$trs.each(function(){
							$(this).find("td:first").css({
								'height': 'auto',
							});
						});
						
						$trs.each(function(){
							var tmpHeight = $(this).find("td:first").outerHeight();
							if( firstTdHeight < tmpHeight ) firstTdHeight = tmpHeight;
						});
						
						$trs.each(function(){
							$(this).find("td:first").css({
								'height': firstTdHeight + 'px',
							});
						});
						
					});

				}else{
					self.table.each(function(){

						var $this = $(this),
							$trs = $(this).find("tbody>tr");
							
						$trs.each(function(){
							$(this).find("td:first").css({
								'height': 'auto',
							});
						});
					});
				}
			},

			createBtn : function(){

				var self = this;

				self.table.each(function(){

					var $this = $(this),
					template = '<div class="tabl_quotes_nav clearfix"><button class="prev disable"><i class="fa fa-angle-left"></i></button><button class="next"><i class="fa fa-angle-right"></i></button></div>',
						qItem = $this.find("tbody>tr").length;

					if(qItem > self.itemQt){
						$this.closest('.turn_content').prepend(template);
					}

				});

			},

			moveSlider : function(){

				var self = this;

				$('body').on("click",".tabl_quotes_nav>*:not(.process)",function(){

					if($(this).hasClass('disable')) return false;

					var $this = $(this),
						wrapSlider = $this.closest('.turn_content').find('.tabl_quotes'),
						slider = wrapSlider.find('table'),
						sliderWidth = slider.width(),
						wrapOffset = wrapSlider.offset().left,
						sliderOffset = slider.offset().left,
						lastOffset = wrapSlider.width() - slider.width(),
						position = wrapOffset - sliderOffset,
						prevItem = - position + self.itemWidth,
						nextItem = - position - self.itemWidth;


					if($this.hasClass("next")){

						$this.addClass('process');
						slider.css({
							'left' : nextItem
						});
						setTimeout(function(){
							$this.removeClass('process');
						},500);

						$this.siblings().removeClass('disable');

						if((nextItem - self.itemWidth) < lastOffset){

							$this.addClass('disable');
						
						}

					}
					else{

						$this.addClass('process');
						slider.css({
							'left' : prevItem
						});
						setTimeout(function(){
							$this.removeClass('process');
						},500);

						$this.siblings().removeClass('disable');

						if((prevItem + self.itemWidth) > 0 ){

							$this.addClass('disable');
						
						}

					}

				});

			},

		},

	}


	$(function(){

		TableSlider.DOMReady();

	});
	



})(jQuery);