var wAdversingBoxWidgetOpen;
!function( $ ) {
	wAdversingBoxWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lLoading: 'Loading...',
			lLike: 'Like',
			lDisLike: 'Dislike',
		};
		var defOption = {
			ins: '',
			selector: '.wUsersConversationWidget',
			ajaxLikeURL: yiiBaseURL+'/advertisement/ajaxLike',
			ajaxLoadLikesURL: yiiBaseURL+'/advertisement/ajaxLoadLikes',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var htmlLoading;
		
		var self = {
			loading: false,
			moving: false,
			idTimeoutHideLikes: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$wBody = self.$ns.find( options.ins+'.wBody' );
				htmlLoading = self.$wBody.html();
				
				self.$wLeft = self.$ns.find( options.ins+'.wLeft' );
				self.$wRight = self.$ns.find( options.ins+'.wRight' );
				self.$wReload = self.$ns.find( options.ins+'.wReload' );
				self.$wTooltip = self.$ns.find( options.ins+'.wTooltip' );
				
				self.$wLeft.click( {direction: 'left'}, self.move );
				self.$wRight.click( {direction: 'right'}, self.move );
				self.$wReload.click( self.reload );
				self.reload();
				$(window).on( 'resize', self.resize );
				self.$wBody.on( 'click', '.jsOfferLike', self.like );
				self.$wBody.on( 'mouseenter', '.jsOfferLike', self.showLikes );
				self.$wBody.on( 'mouseleave', '.jsOfferLike', self.hideLikes );
				self.$wTooltip.on( 'mouseenter', self.holdLikes );
				self.$wTooltip.on( 'mouseleave', self.hideLikes );
			},
			getCount: function() {
				var $conts = self.$wBody.find( '.jsCont:not( .ft_hidden )' );
				var widthBody = self.$wBody.width();
				var widthCont = $conts.width();
				var countCont = Math.round( widthBody / widthCont );
				return countCont;
			},
			move: function( event ) {
				self.hideLikes();
				if( self.moving ) return false;
				var countCont = self.getCount();
				var $conts = self.$wBody.find( '.jsCont' );
				var right = event.data.direction == 'right';
				var left = !right;
				var d = right ? '-=' : '+=';
				var widthBody = self.$wBody.width();
				
				var $oldConsts = $conts.filter( ':not( .ft_hidden )' );
				
				if( right && $oldConsts.length < countCont ) return false;
				
				var $newConsts = right
					? $($oldConsts[countCont-1]).nextAll( ':lt('+countCont+')' ) 
					: $($oldConsts[0]).prevAll( ':lt('+countCont+')' );
				
				if( $newConsts.length == 0 ) return false;
				$newConsts.removeClass( 'ft_hidden' );
				
				var $visibleConsts = $conts.filter( ':not( .ft_hidden )' );
				
				if( left ) {
					$visibleConsts.css( 'left', '-'+widthBody+'px' );
				}
				
				self.moving = true;
				var i = 0;
				$visibleConsts.animate( { 'left': d+widthBody+'px' }, function() {
					i++;
					if( i == $visibleConsts.length ) {
						$visibleConsts.css( 'left', '0px' );
						$oldConsts.addClass( 'ft_hidden' );
						self.moving = false;
					}
				});
				return false;
			},
			reload: function() {
				self.hideLikes();
				self.$wBody.html( htmlLoading );
				self.$wBody.load( options.zoneURL, function() { self.resize(); } );
				return false;
			},
			resize: function() {
				if( !options.mover ) return false;
				self.hideLikes();
				var countCont = self.getCount();
				var $conts = self.$wBody.find( '.jsCont' );
				if( !$conts.length ) return false;
				$conts.css( 'left', '0px' );
				$conts.filter( ':gt('+(countCont-1)+')' ).addClass( 'ft_hidden' );
				$conts.filter( ':lt('+countCont+')' ).removeClass( 'ft_hidden' );
				return false;
			},
			like: function() {
				self.hideLikes();
				if( !options.idUser ) return false;
				if( self.loading ) return false;
				
				var $link = $(this);
				var id = $link.attr( 'data-id' );
				
				//$link.html( options.ls.lSaving );
				$link.find( 'span.saving' ).html( options.ls.lSaving );
				$link.blur();
				
				self.loading = true;
				$.getJSON( options.ajaxLikeURL, {id:id}, function ( data ) {
					$link.find( 'span.saving' ).html( '' );
					self.loading = false;
					if( data.error ) {
						alert( data.error );
					}
					else{
						if( data.liked ) {
							$link.find( '.jsHeart' ).addClass( 'jsHeartHover' );
						}
						else{
							$link.find( '.jsHeart' ).removeClass( 'jsHeartHover' );
						}
						$link.find( 'span.likes' ).html( data.likes );
						$link.attr( 'data-likes', data.likes );
						$link.attr( 'data-likesHTML', '' );
					}
				});
				
				return false;
			},
			showLikes: function() {
				if( self.idTimeoutHideLikes ) clearTimeout( self.idTimeoutHideLikes );
				if( self.moving ) return false;
				var $link = $(this);
				
				var id = $link.attr( 'data-id' );
				var likes = $link.attr( 'data-likes' );
				var likesHTML = $link.attr( 'data-likesHTML' );
				
				if( likes == '0' ) {
					self.hideTooltip();
					return false;
				}
				
				if( likesHTML ) {
					self.showTooltip( $link, likesHTML );
				}
				else{
					self.showTooltip( $link, options.ls.lLoading );
					$.getJSON( options.ajaxLoadLikesURL, {id:id}, function ( data ) {
						self.loading = false;
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.showTooltip( $link, data.likes );
							$link.attr( 'data-likesHTML', data.likes );
						}
					});
				}
			},
			hideLikes: function() {
				if( self.idTimeoutHideLikes ) clearTimeout( self.idTimeoutHideLikes );
				self.idTimeoutHideLikes = setTimeout( self.hideTooltip, 500 );
			},
			holdLikes: function() {
				if( self.idTimeoutHideLikes ) clearTimeout( self.idTimeoutHideLikes );
			},
			showTooltip: function( $object, title ) {
				self.$wTooltip.html( title );
				self.$wTooltip.show();
				var offset = $object.offset();
				offset.top+=20;
				self.$wTooltip.offset( offset );
			},
			hideTooltip: function() {
				self.$wTooltip.hide();
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );