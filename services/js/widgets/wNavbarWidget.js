var wNavbarWidgetOpen;
!function( $ ) {
	wNavbarWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
		};
		var defOption = {
			selector: '.wNavbarWidget',
			countNotViewedNotices: 0,
			countNotViewedPrivateMessages: 0,
			ajaxViewAllNotices: '/userNotice/ajaxViewAll',
			ajaxViewAllPrivateMessages: '/userPrivateMessage/ajaxViewAll',
			ajaxLoadNoticesListURL: '/user/ajaxLoadNoticesList',
			ajaxAcceptInviteFriendURL: '/user/ajaxAcceptInviteFriend',
			ajaxRejectInviteFriendURL: '/user/ajaxRejectInviteFriend',
			cookieLanguageVarName: "language",
			mp3File: yiiBaseURL + '/uploads/audio/beep.mp3',
			oggFile: yiiBaseURL + '/uploads/audio/beep.ogg',
			ajaxUpdateNavBar: yiiBaseURL + '/navBar/update',
			expires: 365,
			iTimer:30,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			viewedNoticesShortList: false,
			viewedPrivateMessagesShortList: false,
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$wToggleNoticesShortList = self.$ns.find( '.wToggleNoticesShortList' );
				self.$wTogglePrivateMessagesShortList = self.$ns.find( '.wTogglePrivateMessagesShortList' );
				
				self.$wNoticesShortList = self.$ns.find( '.wNoticesShortList' );
				self.$wNoticesShortList.$wShowMore = self.$wNoticesShortList.find( '.wShowMore' );
				self.$wNoticesShortList.$wLoading = self.$wNoticesShortList.find( '.wLoading' );
				
				self.$wNoticesShortList.$wShowMore.click( self.showMoreNotices );
				self.$wNoticesShortList.on( 'click', '.wAcceptInviteFriend', self.acceptInviteFriend );
				self.$wNoticesShortList.on( 'click', '.wRejectInviteFriend', self.rejectInviteFriend );
				
				self.$wToggleNoticesShortList.click( self.onToggleNoticesShortList );
				self.$wTogglePrivateMessagesShortList.click( self.onTogglePrivateMessagesShortList );
				
				self.$ns.on( 'click', '.wChangeLanguage', self.changeLanguage );
				
				self.sInterval = setInterval(function () {
						self.updateNavBar();
					},
					options.iTimer * 1000
				);
			},
			onToggleNoticesShortList:function() {
				if( options.countNotViewedNotices && !self.viewedNoticesShortList ) {
					$.get( yiiBaseURL+options.ajaxViewAllNotices );
					self.viewedNoticesShortList = true;
				}
			},
			onTogglePrivateMessagesShortList:function() {
				if( options.countNotViewedPrivateMessages && !self.viewedPrivateMessagesShortList ) {
					$.get( yiiBaseURL+options.ajaxViewAllPrivateMessages );
					self.viewedPrivateMessagesShortList = true;
				}
			},
			soundPlay:function() {
				var audio = new Audio();
				audio.innerHTML = '<source src="'+options.mp3File+'" type="audio/mp3"></source><source src="'+options.oggFile+'" type="audio/ogg"></source>';
				audio.autoplay = true;
			},
			updateNavBar:function() {
				$.get( options.ajaxUpdateNavBar, '', function ( data ) {
					
					if( $('.wToggleNoticesShortList span').text() == '' ) 
						var oldNoticeCount=0;
					else
						var oldNoticeCount = $('.wToggleNoticesShortList span').text();
					
					if( $('.wTogglePrivateMessagesShortList span').text() == '' ) 
						var oldMessCount=0;
					else
						var oldMessCount = $('.wTogglePrivateMessagesShortList span').text();
					
					$( '#navbar-wrapper' ).html(data);
					
					if( $('.wToggleNoticesShortList span').text() == '' ) 
						var newNoticeCount=0;
					else
						var newNoticeCount = $('.wToggleNoticesShortList span').text();
					
					if( $('.wTogglePrivateMessagesShortList span').text() == '' ) 
						var newMessCount=0;
					else
						var newMessCount = $('.wTogglePrivateMessagesShortList span').text();
					
					if( oldNoticeCount < newNoticeCount || oldMessCount < newMessCount )
						self.soundPlay();
					
					
				});
				self.restartTimer();
			},
			restartTimer:function(){
				clearInterval(self.sInterval);
			},
			showMoreNotices:function() {
				if( self.loading ) return false;
				self.$wNoticesShortList.$wShowMore.parent().hide();
				
				self.$wNoticesShortList.$wLoading.show();
				self.loading = true;
				var idLastNotice = self.$wNoticesShortList.find( '[data-id]:last' ).attr( 'data-id' );
				var data = {
					idLastNotice: idLastNotice,
				};
				
				$.getJSON( yiiBaseURL+options.ajaxLoadNoticesListURL, data, function ( data ) {
					self.loading = false;
					self.$wNoticesShortList.$wLoading.hide();
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $list = $( data.list );
						self.$wNoticesShortList.find( '[data-id]:last' ).after( $list );
						if( data.more ) {
							self.$wNoticesShortList.$wShowMore.parent().show();
						}
					}
				});
				return false;
			},
			acceptInviteFriend:function() {
				var $button = $(this);
				var id = $button.attr( 'data-id' );
				$button.html( options.ls.lSaving );
				$button.attr( 'disabled', 'disabled' );
				
				$.get( yiiBaseURL+options.ajaxAcceptInviteFriendURL, { idFriend: id }, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						$button.closest( 'li' ).remove();
					}
				});
				
				return false;
			},
			rejectInviteFriend:function() {
				var $button = $(this);
				var id = $button.attr( 'data-id' );
				$button.html( options.ls.lSaving );
				$button.attr( 'disabled', 'disabled' );
				
				$.get( yiiBaseURL+options.ajaxRejectInviteFriendURL, { idFriend: id }, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						$button.closest( 'li' ).remove();
					}
				});
				
				return false;
			},
			changeLanguage:function() {
				var language = $(this).attr( 'data-language' );
				
				$.cookie( options.cookieLanguageVarName, language, { expires: options.expires, path: "/" });
				document.location.reload();
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );