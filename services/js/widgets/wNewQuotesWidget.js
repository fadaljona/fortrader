var wNewQuotesWidgetOpen;
!function( $ ) {
	wNewQuotesWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wNewQuotesWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$wTablQuotes = self.$ns.find( '.tabl_quotes' );
				self.$wCheckedQuotes = self.getCheckedQuotes();
				//self.$wOptionsButton = self.$ns.find( '.options_btn' );
				self.$wOptionCheckbox = self.$ns.find( '.options_box .options_item input:checkbox' );
				self.$wQuotesTime = self.$ns.find( '.pid-time' );
				self.$wWindow = $(window);
				self.$wDocument = $(document);
				self.$wDocument.ready( self.updateTimeToLocal );
				//self.$wWindow.load( self.bindClickToOptions );
				self.$wWindow.load( self.subscribeToQuotes );
				self.$wOptionCheckbox.click( self.changeQuotesList );
			},
			subscribeToQuotes:function() {		
				var conn = new ab.Session(options.connUrl,
					function() {
						conn.subscribe(options.quotesKey, function(topic, data) {
							self.updateQuotes(data);
						});
						console.warn('subscribed');
					},
					function() {
						console.warn('WebSocket connection closed');
					},
					{'skipSubprotocolCheck': true}
				);
			},
			updateQuotes:function(data) {
				for(var key in data) {
					var parsedData = JSON.parse(data[key]);
					self.updateQuotesWithAnimate(key, parsedData);
				}
			},
			updateQuotesWithAnimate:function(key, parsedData) {
				var pageBid = $(options.selector+' .pid-' + key + '-bid'),
					pageChg = $(options.selector+' .pid-' + key + '-chg'),
					pageChgPercent = $(options.selector+' .pid-' + key + '-chg-percent'),
					pageTime = $(options.selector+' .pid-' + key + '-time');
					
				var newBid = parseFloat(parsedData.bid),
					precision = pageBid.data('precision'),
					lastBid = pageBid.data('lastBid'),
					oldBid = parseFloat(pageBid.text());
					
				if( !isNaN(lastBid) ){
					var lastBidVal = parseFloat( lastBid );
					var prevChgVal = parseFloat(pageChg.text());
					var newChgVal = parsedData.bid - lastBidVal;
					var newChgPercentVal = newChgVal / lastBidVal * 100 ;
					var sign = '';
					if( newChgVal > 0 ) sign = '+';
					pageChg.html( sign + newChgVal.toFixed(precision) );
					pageChgPercent.html( sign + newChgPercentVal.toFixed(precision) );
					
					if( prevChgVal < 0 && newChgVal > 0 || prevChgVal > 0 && newChgVal < 0 ){
						self.changeColorAndBg(pageChg, prevChgVal, newChgVal);
						self.changeColorAndBg(pageChgPercent, prevChgVal, newChgVal);
					}
				}

				pageBid.html(parsedData.bid);
				
				
				pageTime.html( self.getLocalTimeForData(parsedData.time) );
					

				self.changeColorAndBg(pageBid, oldBid, newBid);
					
				setTimeout(function() {
					if ( newBid > oldBid || newBid < oldBid ) {
						pageBid.removeClass('red_bg green_bg');
						pageChg.removeClass('red_bg green_bg');
						pageChgPercent.removeClass('red_bg green_bg');
					}
				}, 1250);
			},
			changeColorAndBg:function(element, oldVal, newVal) {
				if ( newVal > oldVal ){
					element.removeClass('red_color');
					element.addClass('green_color');
					element.removeClass('red_bg');
					element.addClass('green_bg');
				}else if ( newVal < oldVal ){
					element.removeClass('green_color');
					element.addClass('red_color');
					element.removeClass('green_bg');
					element.addClass('red_bg');
				}
			},
			/*bindClickToOptions:function() {
				self.$wOptionsButton.click( self.optionsButtonClick );
				self.$wDocument.click( self.closeOptions );
			},
			optionsButtonClick:function() {
				var $this = $(this),
					parent = $this.closest(".turn_box");
				var marker = 1;
				if( $this.hasClass("active") ) marker = 0;
				if( marker ){
					$(options.selector+' .options_box').removeClass('opened');
					$(options.selector+' .options_btn').removeClass('active');	
				}
				$this.toggleClass("active");
				parent.find('.options_box').toggleClass("opened");	
			},
			closeOptions:function(event) {
				if(!$(event.target).closest('.options_box,.options_btn').length){
					$(options.selector+' .options_box').removeClass('opened');
					$(options.selector+' .options_btn').removeClass('active');
				}
			},*/
			getCheckedQuotes:function() {
				return self.$ns.find( '.options_box .options_item input:checkbox:checked' );
			},
			changeQuotesList:function() {
				self.$wCheckedQuotes = self.getCheckedQuotes();
				var quotesStr = '';
				self.$wCheckedQuotes.each(function(index, element){
					var delimiter = '';
					if( index > 0 ) delimiter = ',';
					quotesStr = quotesStr + delimiter + $(this).val();
				});
				self.setCheckedQuotesToCookie( quotesStr );
				self.showHideQuotesRows();
			},
			showHideQuotesRows:function() {
				self.$wOptionCheckbox.each(function(index, element){
					var rowForSymbol = self.$wTablQuotes.find('tr[data-symbol=' + $(this).val() + ']');
					if( $(this).is(':checked') ){
						rowForSymbol.removeClass('hide');
					}else{
						rowForSymbol.addClass('hide');
					}
				});
			},
			updateTimeToLocal:function() {
				self.$wQuotesTime.each(function(index, element){
					var startTime = $(this).data('startTime');
					if( startTime != '' ){ 
						$(this).html( self.getLocalTimeForData( $(this).data('startTime') ) );
					}
				});
			},
			getLocalTimeForData:function(timeStr) {
				var timeArr = timeStr.split(":");
				
					
				var $date = new Date(2015, 1, 1, timeArr[0], timeArr[1], timeArr[2], 0); //new Date(year, month, day, hours, minutes, seconds, milliseconds)
				var timezoneOffset = $date.getTimezoneOffset() + 60 * options.timeDataOffset;
				$date.setMinutes ( $date.getMinutes() + timezoneOffset * (-1) );
				var hours, minutes, seconds;
				
				if( $date.getHours() < 10 ){
					hours = '0' + $date.getHours();
				}else { hours = $date.getHours(); }
				
				if( $date.getMinutes() < 10 ){
					minutes = '0' + $date.getMinutes();
				}else{ minutes = $date.getMinutes(); }
				
				if( $date.getSeconds() < 10 ){
					seconds = '0' + $date.getSeconds();
				}else{ seconds = $date.getSeconds(); }
				
				return hours + ':' + minutes + ':' + seconds;
			},
			setCheckedQuotesToCookie:function(value) {
				var expires;
				var date = new Date();
				date.setTime(date.getTime() + (100000 * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();	
				var name = 'checkedQuotesOnFullList' + options.lang;
				document.cookie = name + "=" + value + expires + "; path=/";
			},
			
		};
		self.init();
		return self;
	}
}( window.jQuery );