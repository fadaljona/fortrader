var wEANewsWidgetOpen;
!function( $ ) {
	wEANewsWidgetOpen = function( options ) {
		var defOption = {
			ins: '',
			selector: '.wEANewsWidget',
			ajax: false,
			ajaxLoadListURL: '/ea/ajaxLoadNewsList',
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
			isGuest: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				
				self.$wNewsList = self.$ns.find( options.ins+'.wNewsList' );
				self.$tplItem = self.$ns.find( options.ins+'.wTplItem' );
								
				self.$wReload = self.$ns.find( options.ins+'.wReload' );
				self.$wShowMore = self.$ns.find( options.ins+'.wShowMore' );
				
				self.$wReload.click( self.reload );
				self.$wShowMore.click( self.showMore );
			},
			collectIDsSkip: function( date ) {
				var ids = [];
				self.$wNewsList.find( '[dateNews="'+date+'"]' ).each( function() {
					ids.push( $(this).attr( 'idNews' ));
				});
				return ids;
			},
			reload: function() {
				if( self.loading ) return false;
				self.$wShowMore.parent().hide();
				if( options.ajax ) {
					self.$wNewsList.html( '' );
					var $tplItem = self.$tplItem.clone();
					self.$wNewsList.append( $tplItem );
					$tplItem.show();
					var data = {
						idUser: options.idUser,
					};
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
						self.loading = false;
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $list = $( data.list );
							$tplItem.replaceWith( $list );
							if( data.more ) {
								self.$wShowMore.parent().show();
							}
						}
						$tplItem.remove();
					});
				}
				return false;
			},
			showMore:function() {
				if( self.loading ) return false;
				self.$wShowMore.parent().hide();
				if( options.ajax ) {
					var $tplItem = self.$tplItem.clone();
					self.$wNewsList.append( $tplItem );
					$tplItem.show();
					self.loading = true;
					var dateLast = self.$wNewsList.find( '[idNews]:last' ).attr( 'dateNews' );
					var data = {};
					if( dateLast ) {
						data.dateLast = dateLast;
						var idsSkip = self.collectIDsSkip( dateLast );
						if( idsSkip.length ) {
							data.idsSkip = idsSkip.join( ',' );
						}
					}
					$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
						self.loading = false;
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $list = $( data.list );
							$tplItem.replaceWith( $list );
							if( data.more ) {
								self.$wShowMore.parent().show();
							}
						}
						$tplItem.remove();
					});
					return false;
				}
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );