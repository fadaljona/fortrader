var wRegistrationContestMemberWizardWidgetOpen;
!function( $ ) {
	wRegistrationContestMemberWizardWidgetOpen = function( options ) {
		var defLS = {
			lError: 'Error!',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lErrorAgree: 'You must agree to the rules of the contest!',
            displayingTimeoutTimer: undefined,
            displayingTimeout: 0
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			mode:'register',
			user:0,
			selector: '.wRegistrationContestMemberWizardWidget',
			delayTooltips: 1000,
			errorClass: 'inpError',
			delayRedirect: 5000
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			memberPageUrl: '',
			registerMemberDownTimer: 0,
			registerMemeberUrl: options.registerMemberUrl,
			init:function() {
				self.mode = options.mode;
				self.type = 'contest';
				
				self.spinner = '<div class="spinner-wrap sloader"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.timerHtml = '<div class="spinner-wrap stimer"><div class="countDownRegister">60</div></div>';
				self.$ns = $( options.selector );
				self.$accountFormWidget = self.$ns.find( '.wAccountContestMemberFormWidget' );
				self.$accountForm = self.$accountFormWidget.find( 'form' );
				self.$accountFormSubmit = self.$accountForm.find('.wSubmit');
				self.$accountFormError = self.$accountForm.find('.form-error');
				self.$finBlock = self.$ns.find('#stepFinish');
				self.$accountFormSubmit.click( self.sendForm );
			},
			getRegisterMemeberUrl:function(){
				if( self.mode == 'editRegister' ){
					return options.editAccountMemberUrl;
				}
				return self.registerMemeberUrl;
			},
			initTimer:function(){
				self.$ns.append( self.timerHtml );
				self.registerMemberDownTimer = setInterval( function(){
					var $timer = self.$ns.find('.spinner-wrap .countDownRegister');
					var timeLeft = parseInt( $timer.text() );
					if( timeLeft > 1 ){
						timeLeft = timeLeft - 1;
						$timer.text( timeLeft );
					}else{
						alert( options.accountWillBeCheckdLater );
						self.disableTimer();
						self.showFinBlock();
					}
				}, 1000 );
			},
			disableTimer:function(){
				clearInterval( self.registerMemberDownTimer );
				self.$ns.find('.spinner-wrap.stimer').remove();
			},
			enable:function() {
				self.$ns.find('.spinner-wrap.sloader').remove();
			},
			enableAll:function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			showFinBlock:function(){
				if( self.mode != 'editRegister' ){
					
					if( options.type != 'frameForForum' ){
						self.$accountForm.hide();
						self.$finBlock.removeClass('hide');
								
						var $forwardMessage = self.$finBlock.find( 'span#forward_message' );
						$forwardMessageText = $($forwardMessage).html();
						$forwardMessageText = $forwardMessageText.replace(/(!t!)/, '<span id="displayedTime">' + options.delayRedirect / 1000 + '</span>');
						$forwardMessageText = $forwardMessageText.replace(/(!link!)/, self.memberPageUrl);

						$($forwardMessage).html($forwardMessageText);

						defLS.displayingTimeoutTimer = setInterval( function(){
							if( options.delayRedirect > 0 ){
								options.delayRedirect = options.delayRedirect - 1000;
								$( 'span#displayedTime').text(options.delayRedirect / 1000);
							}else{
								clearInterval(defLS.displayingTimeoutTimer);
								window.location.assign( self.memberPageUrl );
							}
						}, 1000 );
					}else{
						options.ns.wYourMonitoringAccountsListWidget.updateList( );
					}
					
				}else{
					self.disable();
					if( self.type == 'contest' ){
						$.getJSON( options.getCurrentUserInfoUrl, {}, function ( data ) {
							if( data.error ) {
								alert( data.error );
								self.$ns.find('.spinner-wrap').remove();
							}
							else{
								options.ns.wCurrentUserContestMemberInfoListWidget.updateContent(data.content);
								options.ns.wCurrentUserContestMemberInfoListWidget.init();
								self.enable();
								self.$ns.hide(500);
								options.ns.wCurrentUserContestMemberInfoListWidget.showWidget();
							}
						});
					}else if( self.type == 'monitoring' ){
						options.ns.wYourMonitoringAccountsListWidget.updateRow( self.$ns.attr('data-id') );
					}
					
				}
			},
			showWidget: function(){
				self.$ns.show(500);
			},
			addAccountToMonitoring: function( memberId ){
                self.showFinBlock(); return false;

				self.initTimer();
				$.post( options.addAccountToMonitoringUrl, {memberId: memberId}, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}else{
						self.checkAccountStatus( memberId );
					}
					
				}, 'json' );
			},
			checkAccountStatus: function( memberId ){
				$.post( options.checkAccountStatusUrl, {memberId: memberId}, function ( data ) {
					if( data.errorNo == -123156 ) {
						setTimeout( function(){ self.checkAccountStatus( memberId ); }, 2000);
					}else{
						self.disableTimer();
						

						if( data.title == '' && data.description == '' ){
							errMessage = data.errorDesc;
						}else if( data.description != '' ){
							errMessage = data.description;
						}else{
							errMessage = data.title;
						}
						
						if( data.errorNo == 0 ){
							self.showFinBlock();
						}else{
							self.$accountFormError.text(errMessage);
							self.registerMemeberUrl = options.editAccountMemberUrl;
						}
					}
					
				}, 'json' );
			},
			accountTypeChanged: function(){
				var $accountType = self.$accountForm.find('#AccountContestMemberFormModel_accountType');
				if( $accountType.val() == 'account' ){
					self.$accountForm.find('#AccountContestMemberFormModel_accountNumber').closest('.regestration_box_50').fadeIn(0);
					self.$accountForm.find('#AccountContestMemberFormModel_investorPassword').closest('.regestration_box_50').fadeIn(0);
					self.$accountForm.find('#AccountContestMemberFormModel_idServer').closest('.regestration_box_50').fadeIn(0);
					self.$accountForm.find('#AccountContestMemberFormModel_reportFile').closest('.regestration_box_50').fadeOut(0);
				}
				if( $accountType.val() == 'report' ){
					self.$accountForm.find('#AccountContestMemberFormModel_accountNumber').closest('.regestration_box_50').fadeOut(0);
					self.$accountForm.find('#AccountContestMemberFormModel_investorPassword').closest('.regestration_box_50').fadeOut(0);
					self.$accountForm.find('#AccountContestMemberFormModel_idServer').closest('.regestration_box_50').fadeOut(0);
					self.$accountForm.find('#AccountContestMemberFormModel_reportFile').closest('.regestration_box_50').fadeIn(0);
					self.$accountForm.find('.turn_content').css({'display':'block'});
				}
			},
			submitReportError: function( error, errorField ){
				self.enable();
				self.$accountFormError.html('');
				var $errorField = self.$ns.find( '*[name="'+errorField+'"]' );
				$errorField.addClass( options.errorClass );
				self.$accountFormError.html( error );
	
			},
			submitReportSuccess: function( id ){
				self.enable();
				self.$accountForm.find('input').removeClass( options.errorClass );
				self.$accountFormError.html('');
				options.ns.wYourMonitoringAccountsListWidget.updateList( );
			},
			sendForm: function() {
				
				
				var $accountType = self.$accountForm.find('#AccountContestMemberFormModel_accountType');
				
				if( $accountType.length && $accountType.val() == 'report' ){
					
					self.disable();
					self.$accountForm[0].action = options.iframeAddReportUrl;
					self.$accountForm[0].enctype = 'multipart/form-data';
					self.$accountForm[0].target = 'iframeForReportSubmit';
					self.$accountForm[0].submit();
					
				}else{
				
				
					if( options.idContest != 0 ){
						if( !self.$accountForm.find( '.wconditions' ).is(":checked") ){
							alert( options.acceptTermsText );
							return false;
						}
					}
					

					self.disable();
					self.$accountForm.find('input').removeClass( options.errorClass );
					self.$accountFormError.html('');

					$.post( self.getRegisterMemeberUrl(), self.$accountForm.serialize(), function ( data ) {
					
						self.enable();
			
						if( data.error ) {
							if( data.existMember > 0 ){
								if( options.type != 'frameForForum' ){
									$('#contestModal').find('.goToAccount').attr({ 'href': options.memberUrl + '/' + data.existMember });
									$('#contestModal').arcticmodal();
								}else{
									alert( options.exisAccountErrorMessage );
								}
								
							}else{
								alert( data.error );
							}
						}
						else if( data.errors && data.errors.length > 0 ) {
							data.errors.reverse();
							$.each( data.errors, function ( i, el ) {
								var $errorField = self.$ns.find( '*[name="'+el.field+'"]' );
								$errorField.addClass( options.errorClass );
								self.$accountFormError.html( self.$accountFormError.html() + el.error + '<br />' );
							});
						}
						else {
							self.$accountForm.find('input').removeClass( options.errorClass );
							self.memberPageUrl = options.memberUrl + '/' + data.contestMemberId;
							self.addAccountToMonitoring( data.contestMemberId );
						}
					}, 'json' );
				
				}
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );
