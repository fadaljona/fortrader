var wRegistrationListOfAccountsContestMemberWizardWidgetOpen;
!function( $ ) {
    wRegistrationListOfAccountsContestMemberWizardWidgetOpen = function( options ) {
		var defLS = {
			lError: 'Error!',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lErrorAgree: 'You must agree to the rules of the contest!',
			login:'Log in',
			forgotPass:'Forgot your password?',
            displayingTimeoutTimer: undefined,
            displayingTimeout: 0
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
			selector: '.wRegistrationContestMemberWizardWidget',
			mode:'register',
			user:0,
			delayTooltips: 1000,
			errorClass: 'error',
			delayRedirect: 5000
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$finBlock = self.$ns.find('#stepFinish');
				self.$submit = self.$ns.find( options.ins + ".wSubmit");
				self.$submit.click( self.sendForm );
			},
            enable:function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
   
 
			sendForm: function( ) {
				
				if( !self.$ns.find( '.wconditions' ).is(":checked") ){
					alert( options.acceptTermsText );
					return false;
				}
				self.disable();
				
				$.post( options.ajaxRegisterUrl, {}, function ( data ) {
					

					self.enable();
					if( data.error ) {
						alert( options.errorMess );
					}
					else if( data.errors ) {
						alert( options.errorMess );
					}
					else {
						self.$ns.find('form').hide();
						self.$finBlock.removeClass('hide');
						
						var memberUrl = options.memberUrl + '?id=' + data.contestMemberId;
						
						self.$ns.find('#accountModelLogin').attr({'href':memberUrl});
						self.$ns.find('#accountModelLogin').text(data.accountNumber);
						
						self.$ns.find('#accountModelPassword').text(data.password);
						self.$ns.find('#accountModelInvPassword').text(data.investorPassword);
						
					}
				}, 'json' );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );
