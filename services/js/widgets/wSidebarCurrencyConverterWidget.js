var wSidebarCurrencyConverterWidgetOpen;
!function( $ ) {
	wSidebarCurrencyConverterWidgetOpen = function( options ) {
		var defOption = {
			
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			prevFromVal: 1,
			timeoutFromVar: false,
			timeoutToVar: false,
			timeoutTime: 1000,
			init:function() {
	
                self.$ns = $( options.selector );

                self.$wConverterSelectFrom = self.$ns.find( 'select.converterSelectFrom' );
                self.$wConverterSelectTo = self.$ns.find( 'select.converterSelectTo' );
                self.$wConverterInpTo = self.$ns.find( 'input.converterInpTo' );
                self.$wConverterInpFrom = self.$ns.find( 'input.converterInpFrom' );

                self.$wConverterResultInp = self.$ns.find( '.fx_sb-result-convert-input span');
                self.$wConverterResultOutput = self.$ns.find( '.fx_sb-result-convert-output');


                self.prevToVal = parseFloat(self.$wConverterInpTo.val());

                self.initFormStyler();

                self.$wConverterInps = self.$ns.find( 'input.converter_form_inp' );
                self.converterInpBindKeyup();

                self.recalculateConverter('From', 'To', false);
				
            },
            initFormStyler:function(){
				var $forms = self.$ns.find('.js_fx-sb-select');
				if( $forms.length ){
					$forms.styler({
                        selectVisibleOptions: 6,
						onSelectClosed: function() {
							var $this = $(this);
							if( $this.hasClass('converterSelect') ){
								self.recalculateConverter('From', 'To');
								
							}
						}
					});
				}
            },
            recalculateConverter:function( fromCur, toCur, saveRequest = true ){
				var $toSelect = self.$ns.find( 'select.converterSelect' + toCur );
				var $selectOptDest = $toSelect.find(":selected");
				var destVal = parseFloat( $selectOptDest.attr( 'data-value' ) );
									
				var $fromInp = self.$ns.find( 'input.converterInp' + fromCur );
				var $toInp = self.$ns.find( 'input.converterInp' + toCur );
				var $fromSelect = self.$ns.find( 'select.converterSelect' + fromCur );					
				var $selectOpt = $fromSelect.find(":selected");
				var toVal = parseFloat( $fromInp.val() );
				var newVal = parseFloat( $selectOpt.attr( 'data-value' ) );
                
                var toInpVal = ( destVal / newVal * toVal).toFixed(2);
                $toInp.val( toInpVal );	
                
                self.$wConverterResultInp.text( toVal + ' ' + $selectOpt.attr('data-restitle') );
                self.$wConverterResultOutput.text( toInpVal + ' ' + $selectOptDest.attr('data-restitle') );
				
				if( saveRequest ){
					clearTimeout(self.timeoutFromVar);	
					clearTimeout(self.timeoutToVar);
					if( fromCur == 'From' ){
						self.timeoutFromVar = setTimeout( self.saveConverterRequestFrom, self.timeoutTime);
					}else{
						self.timeoutToVar = setTimeout( self.saveConverterRequestTo, self.timeoutTime);
					}
				}
				
				
				
			},



			saveConverterRequest: function( dest ){
                if( self.loading ) return false;
				
				if( dest == 'From' ){
					var fromCur = self.$wConverterSelectFrom.val();
					var toCur = self.$wConverterSelectTo.val();
					var fromVal = self.$wConverterInpFrom.val();
				}else{
					var fromCur = self.$wConverterSelectTo.val();
					var toCur = self.$wConverterSelectFrom.val();
					var fromVal = self.$wConverterInpTo.val();
				}
				
				self.loading = true;
				$.post( options.sendConverterDataUrl, {fromCur:fromCur, toCur:toCur, fromVal:fromVal, type: options.type}, function ( data ) {
					
				}, "json" );
				self.loading = false;
			},
		
			saveConverterRequestFrom: function(){

				if( self.$wConverterInpFrom.val() == '' ) return false;
				var newVal = parseFloat(self.$wConverterInpFrom.val());
				if( newVal != self.prevFromVal ){
					self.saveConverterRequest('From');
				}
				self.prevFromVal = newVal;
            },
            


			converterInpBindKeyup:function(e){
                self.$wConverterInps.keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                         // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                         // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                             // let it happen, don't do anything
                             return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });
				self.$wConverterInps.keyup(function(e) {
					var $this = $(this);
					var val = $this.val();
					val = val.replace(/[^\d.]*/g, '')
							.replace(/([.])[.]+/g, '$1')
							.replace(/^[^\d]*(\d+([.]\d{0,15})?).*$/g, '$1');
					$this.val( val );
					if( val != '' ){
						self.recalculateConverter('From', 'To');
					}
				});
			},
			
			
		};
		self.init();
		return self;
	}
}( window.jQuery );