var wHomeQuotesSliderWidgetOpen;
!function( $ ) {
	wHomeQuotesSliderWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wHomeQuotesSliderWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
                self.$ns = $( options.selector );

                self.$wWindow = $(window);
                self.$wWindow.load( self.subscribeToQuotes );
                
			},
			subscribeToQuotes:function() {		
				var conn = new ab.Session(options.connUrl,
					function() {
						conn.subscribe(options.quotesKey, function(topic, data) {
							self.updateQuotes(data);
						});
						console.warn('subscribed');
					},
					function() {
						console.warn('WebSocket connection closed');
					},
					{'skipSubprotocolCheck': true}
				);
            },
            
			updateQuotes:function(data) {

				for(var key in data) {
					var parsedData = JSON.parse(data[key]);
					self.updateQuotesWithAnimate(key, parsedData);
				}
			},
			updateQuotesWithAnimate:function(key, parsedData) {
				var pageBid = $(options.selector+' .pid-' + key + '-bid');
					
				var newBid = parseFloat(parsedData.bid),
					precision = pageBid.data('precision'),
					lastBid = pageBid.data('lastBid'),
					oldBid = parseFloat(pageBid.text());
					
				if( !isNaN(lastBid) ){
					var lastBidVal = parseFloat( lastBid );
				}

				pageBid.html(parsedData.bid);

				self.changeColorAndBg(pageBid, oldBid, newBid);
				
            },
			changeColorAndBg:function(element, oldVal, newVal) {
				if ( newVal > oldVal ){
					element.removeClass('fx_data-red');
                    element.removeClass('fx_data-gray');
					element.addClass('fx_data-green');
				}else if ( newVal < oldVal ){
                    element.removeClass('fx_data-green');
                    element.removeClass('fx_data-gray');
					element.addClass('fx_data-red');
				}
			},

		};
		self.init();
		return self;
	}
}( window.jQuery );