var wSearchCurrencyRatesWidgetOpen;
!function( $ ) {
	wSearchCurrencyRatesWidgetOpen = function( options ) {
		var defOption = {
			
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				
				self.$wSearchButton = self.$ns.find( 'form .quotes_search_btn' );
				self.$wSearchField = self.$ns.find( 'form .typeInSearchField' );
				self.$wQuotesSearchResults = self.$ns.find( 'form .quotes_search_list' );
				self.$wDocument = $(document);
				
				self.$ns.click( self.topSelectorClick );
				self.$wSearchField.keyup( self.keyupSearch );
				self.$wSearchButton.click( self.searchButtonClick );
				self.$wDocument.click( self.documentClick );
				
			},
			documentClick:function() {
				self.$wQuotesSearchResults.removeClass('search_results');
			},
			topSelectorClick:function(e) {
				e.stopPropagation();
			},
			keyupSearch:function() {
				var search = self.$wSearchField.val();
				if( search.length > 0 && ( search.length >= options.minInputChars || !options.minInputChars ) ){
					self.ajaxSearch(search);
				}
				return false;
			},
			ajaxSearch:function(search) {
				if( self.loading ) return false;
				self.loading = true;
				$.post( options.ajaxSubmitURL, {"search": search, "type": options.type}, function ( data ) {
					self.$wQuotesSearchResults.addClass('search_results');
					self.$wQuotesSearchResults.html(data.result);
					self.loading = false;
				}, "json" );
			},
			searchButtonClick:function() {
				var search = self.$wSearchField.val();
				if( search.length > 0 ){
					self.ajaxSearch(search);
				}
				return false;
			},		
		};
		self.init();
		return self;
	}
}( window.jQuery );