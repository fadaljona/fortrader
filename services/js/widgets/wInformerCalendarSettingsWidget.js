var wInformerSettingsWidgetOpen;
!function( $ ) {
	wInformerSettingsWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wInformerSettingsWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			informerParams: {},
			informerUrl: '',
			loading:false,
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				
				self.$previewInformerWrap = self.$ns.find( '.previewInformerWrap' );
				self.$informerCodeWrap = self.$ns.find( '.informerCodeWrap' );
				
				self.$realTimeUpdate = self.$ns.find('.realTimeUpdate');
				self.$realTimeUpdate.find('li').click( self.realTimeUpdateLiClicked );
				self.$impactTabs = self.$ns.find('.impactTabs');
				self.$impactTabs.find('li').click( self.impactTabClicked );
				self.$symbolsTabs = self.$ns.find('.symbolsTabs');
				self.$symbolsTabs.find('li').click( self.symbolClicked );
				self.$informerColorsPreview = self.$ns.find( '.informerColorsPreview' );
				
				self.changedSettings();

				var clipboard = new Clipboard('.copyCode');
				
				$(window).scroll( self.pageScrolled );
			
			},
			pageScrolled:function(){
				clearTimeout(self.scrollTimeout);
				self.scrollTimeout = setTimeout(function(){
					var topWindowsOffset = self.$informerColorsPreview.offset().top - $(window).scrollTop() + self.$informerColorsPreview.outerHeight()*1.75;
					var bottomWindowsOffset = self.$informerColorsPreview.offset().top - $(window).scrollTop() - window.innerHeight - self.$informerColorsPreview.outerHeight()*1.25;

					var $visibleSection = self.findVisibleSection();

					if( $visibleSection ){
						if( topWindowsOffset < 0 ){
							$visibleSection.prev('section').after(self.$informerColorsPreview);
	
						}else if( bottomWindowsOffset > 0 ){
							$visibleSection.prev('section').after(self.$informerColorsPreview);
						}
					}
					
				},10);
			},
			findVisibleSection: function(){
				var el = false;
				self.$ns.find('section>section').each(function( index ) {
					if( (index > 0) && ($(this).offset().top - $(window).scrollTop() > 0) ){
						el = $(this);
						return false;
					}
				});
				return el;
			},
			realTimeUpdateLiClicked: function(){
				if( $(this).hasClass('columnActive') ) return false;
				self.$realTimeUpdate.find('li').removeClass('columnActive');
				$(this).addClass('columnActive')
				
				self.changedSettings();
				return false;
			},
			impactTabClicked: function(){
				$(this).toggleClass("active");
				
				self.changedSettings();
				return false;
			},
			symbolClicked: function(){
				$(this).toggleClass("columnActive");
				
				self.changedSettings();
				return false;
			},

	
			disable: function(){
				self.$ns.append( self.spinner );
			},
			enable: function(){
				self.$ns.find('.spinner-wrap').remove();
			},
			
			correctIframeHeight:function() {
				var iframeHeight = this.contentWindow.document.body.scrollHeight + "px";
				this.style.height = iframeHeight;
				self.setDefaultStyleColors();
			},

			encodeQueryData:function( informerParams ){
				var ret = [];
				for ( var param in informerParams ){
					ret.push( encodeURIComponent( param ) + "=" + encodeURIComponent( informerParams[param] ) );
				}
				return ret.join("&");
			},
			changedSettings:function() {	
				self.disable();
				self.updateInformer();
			},
			getImpactVals: function(){
				var returnArr = [];
				
				if( self.$impactTabs.find('li.active').length == self.$impactTabs.find('li').length ) return 'all';
				
				self.$impactTabs.find('li.active').each(function(index, element){
					returnArr.push($(this).attr('data-val'));
				});
				return returnArr;
			},
			getSymbols: function(){
				var returnArr = [];
				
				if( self.$symbolsTabs.find('li.columnActive').length == self.$symbolsTabs.find('li').length ) return 'all';
				
				self.$symbolsTabs.find('li.columnActive').each(function(index, element){
					returnArr.push($(this).text());
				});
				return returnArr;
			},
			updateInformer:function() {				
				self.informerParams = {};
				self.informerParams['st'] = options.styleId;
				self.informerParams['cat'] = options.catId;
				self.informerParams['update'] = self.$realTimeUpdate.find('li.columnActive').attr('data-val');
				self.informerParams['impact'] = self.getImpactVals();
				self.informerParams['symbols'] = self.getSymbols();
				
				self.informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(self.informerParams);
					
				var frameCode = '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:100%" scrolling="no" src="' + self.informerUrl + '"></iframe>';
				
				self.$informerColorsPreview.html( frameCode )
				self.$informerColorsPreview.find( 'iframe' ).load( self.correctIframeHeightForPreview );

				return false;
			},
			correctIframeHeightForPreview:function( ) {	
				var iframeHeight = this.contentWindow.document.body.scrollHeight + "px";
				this.style.height = iframeHeight;
				if( self.informerUrl != '' ){
					if( self.informerParams['w'] == 0 ){
						var width = '100%';
					}else{
						var width = self.informerParams['w'] + 'px';
                    }
                    
                    self.$informerColorsPreview.css({ height: iframeHeight });

					self.$informerCodeWrap.val( '<iframe style="width:'+width+';border:0;overflow:hidden;background-color:transparent;height:'+iframeHeight+'" scrolling="no" src="' + self.informerUrl + '"></iframe>' );
				}
				self.enable();
			},
	
			
		};
		self.init();
		return self;
	}
}( window.jQuery );