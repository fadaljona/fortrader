var wContestSliderWidgetOpen;
!function( $ ) {
	wContestSliderWidgetOpen = function( options ) {
		var defLS = {

		};
		var defOption = {

		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.buttonSlider();
				
			},
			sliderLoad: false,
			spinnerWrap: '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>',
			buttonSlider : function(){
				var self = this;
				self.$ns.find('.nav_buttons>a').on('click',function(event){

					var parent = $(this).parents('.turn_box'),
						turn = $(this).parents('.turn_box').find('.turn_content'),
						owl = parent.find(".owl-carousel");

					if($(this).hasClass('prev_btn')){

						owl.trigger('owl.prev');
						event.preventDefault();

					}
					else if($(this).hasClass('next_btn')){
		
						if( self.sliderLoad ) return false;
						
						var carouselWrapper = owl.closest('.carousel-wrapper');
						var owlWrapper = owl.closest('.owl-carousel');
						var currentItem = parseInt(owlWrapper.attr('data-currentItem'));
						var maximumItem = parseInt(owlWrapper.attr('data-maximumItem'));
						var itemsToLoad = 3;

						if( parseInt(owlWrapper.attr('data-pages-to-load')) == 1 ){
							owl.trigger('owl.next');
						}else if( currentItem == 0 || maximumItem == 0 ){
							self.sliderLoad = true;

							carouselWrapper.append( self.spinnerWrap );
							$.ajax({
								type: 'POST',
								url: options.ajaxLoadListURL,
								dataType: 'json',
								data: {
									offset: owlWrapper.attr('data-offset'),
									exclude: options.exclude,
								},
								success: function(data){
									if( data.content != '' ){
										owl.data('owlCarousel').addItem( data.content );
										owl.trigger('owl.next');
										owlWrapper.attr({
											'data-offset': parseInt(owlWrapper.attr('data-offset')) + itemsToLoad,
										});
									}else{
										owlWrapper.attr({
											'data-pages-to-load': 1
										});
										owl.trigger('owl.next');
									}
									self.sliderLoad = false;
									carouselWrapper.find( '.spinner-wrap' ).remove();
								},
								error: function(MLHttpRequest, textStatus, errorThrown){
									alert('error loading');
									self.sliderLoad = false;
								}
							});

						}else if( currentItem == maximumItem || currentItem < 0 || currentItem > maximumItem ){
							self.sliderLoad = true;
							if( currentItem < 0 )currentItem = maximumItem;
							
							carouselWrapper.append( self.spinnerWrap );
							$.ajax({
								type: 'POST',
								url: options.ajaxLoadListURL,
								dataType: 'json',
								data: {
									offset: owlWrapper.attr('data-offset'),
									exclude: options.exclude,
								},
								success: function(data){
									if( data.content != '' ){
										owl.data('owlCarousel').addItem(data.content);
										owlWrapper.attr({
											'data-currentItem':currentItem+1,
											'data-maximumItem':maximumItem + itemsToLoad
										});
										owl.data('owlCarousel').jumpTo(currentItem+1);
										owlWrapper.attr({
											'data-offset': parseInt(owlWrapper.attr('data-offset')) + itemsToLoad,
											'data-pages-to-load': parseInt( data.pages )
										});
									}else{
										owlWrapper.attr({
											'data-pages-to-load': 1
										});
										owl.trigger('owl.next');
									}
									self.sliderLoad = false;
									carouselWrapper.find( '.spinner-wrap' ).remove();
								},
								error: function(MLHttpRequest, textStatus, errorThrown){
									alert('error loading');
									self.sliderLoad = false;
								}
							});

						}else{
							owl.trigger('owl.next');
						}

						event.preventDefault();

					}
					else if($(this).hasClass('turn_btn')){

						turn.slideToggle(500);
						$(this).toggleClass("active");
						event.preventDefault();
					}

				});
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );