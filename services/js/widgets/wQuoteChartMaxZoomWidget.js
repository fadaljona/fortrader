var wQuoteChartMaxZoomWidgetOpen;
!function( $ ) {
	wQuoteChartMaxZoomWidgetOpen = function( options ) {
		var self = {
            currencyId:0,
            periodChanged: false,
			init:function() {
                self.$ns = $( options.selector );

                self.$chartWrapper = $('#' + options.chartContainer);
    
                self.setLocaleData();
                self.initChart();
            },
            setLocaleData: function() {
                Highcharts.setOptions({
                    lang: options.langOptions
                });
            },
            initChart: function() {
                $.getJSON(
                    options.loadDataUrl + '?id=' + options.symbolId + '&type=my',
                    function (data) {
                        self.$chartWrapper.highcharts({
                            chart: {
                                zoomType: 'x'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                type: 'datetime'
                            },
                            yAxis: {
                                title: {
                                    text: ''
                                },
                                opposite: true
                            },
                            legend: {
                                enabled: false
                            },
                            plotOptions: {
                                area: {
                                    marker: {
                                        radius: 2
                                    },
                                    lineWidth: 1,
                                    states: {
                                        hover: {
                                            lineWidth: 1
                                        }
                                    },
                                    threshold: null
                                }
                            },
                            series: [{
                                type: 'line',
                                name: data.name,
                                data: JSON.parse(data.data)
                            }],
                            exporting: {
                                buttons: {
                                    contextButton: {
                                        align: 'left',
                                        x: 0,
                                        y: 0,
                                        verticalAlign: 'top'
                                    }
                                }
                            },
                        });
                    }
                );
            },
		};
		self.init();
		return self;
	}
}( window.jQuery );