var wNewQuotesInformerWidgetOpen;
!function( $ ) {
	wNewQuotesInformerWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wNewQuotesInformerWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			fieldsToUpdate: {},
			columnsToUpdate: {},
			availableColumns: {
				ask: 'ask', 
				bid: 'bid', 
				highQuote: 'high', 
				lowQuote: 'low', 
				chg: 'chg', 
				chgPer: 'chgPer', 
				time: 'time'
			},
			init:function() {
				self.$ns = $( options.selector );
				
				self.setUpColumns();
				
				self.$wQuotesTime = self.$ns.find( '[data-column="time"]' );
				
				self.$wWindow = $(window);
				self.$wDocument = $(document);
				self.$wDocument.ready( self.updateTimeToLocal );
		
				self.$wWindow.load( self.subscribeToQuotes );
			},
			setUpColumns:function() {
				for (var i = 0; i < options.columns.length; i++) {
					if( self.availableColumns[options.columns[i]] ) self.columnsToUpdate[ self.availableColumns[options.columns[i]] ] = options.columns[i];
				}
			},
			subscribeToQuotes:function() {		
				var conn = new ab.Session(options.connUrl,
					function() {
						conn.subscribe(options.quotesKey, function(topic, data) {
							self.updateQuotes(data);
						});
						console.warn('WebSocket connection opened for key ' + options.quotesKey);
					},
					function() {
						console.warn('WebSocket connection closed');
					},
					{'skipSubprotocolCheck': true}
				);
			},
			updateQuotes:function(data) {
				for(var key in data) {
					var parsedData = JSON.parse(data[key]);
					self.updateQuotesWithAnimate(key, parsedData);
				}
			},
			updateQuotesWithAnimate:function(key, parsedData) {
				
				var currentTr = self.$ns.find('[data-symbol="'+key+'"]');	
				
				for ( columnKey in self.columnsToUpdate ) {
					self.fieldsToUpdate[ columnKey ] = currentTr.find('[data-column="'+self.columnsToUpdate[columnKey]+'"]');
				}
				
				var oldBid = currentTr.attr('data-oldBid');
				if( oldBid == undefined ){
					currentTr.attr({'data-oldBid':parsedData.bid});	
					oldBid = parsedData.bid;
				}
				
				var lastBid = currentTr.attr('data-lastBid');
				if( lastBid == undefined ){
					currentTr.attr({'data-lastBid':parsedData.bid});	
					lastBid = parsedData.bid;
				}
				
		
				var lastBidVal = parseFloat(lastBid);
				var oldBidVal = parseFloat(oldBid);
				var newBidVal = parseFloat(parsedData.bid);
				
				if( lastBidVal != newBidVal && self.fieldsToUpdate.chg ){
		
					if( currentTr.attr('data-prev-chg-val') == undefined ){
						currentTr.attr({'data-prev-chg-val': parseFloat( self.fieldsToUpdate.chg.text() )});
					}
					
					var prevChgVal = parseFloat( currentTr.attr('data-prev-chg-val') );
					
					var newChgVal = newBidVal - lastBidVal;
					var newChgPercentVal = newChgVal / lastBidVal * 100 ;
					var sign = '';
					if( newChgVal > 0 ) sign = '+';
					
					if( self.fieldsToUpdate.chg ){
						self.fieldsToUpdate.chg.html( sign + newChgVal.toFixed(4) );
						if( prevChgVal < 0 && newChgVal > 0 || prevChgVal > 0 && newChgVal < 0 ){
							self.changeColorAndBg(self.fieldsToUpdate.chg, prevChgVal, newChgVal);
						}
					}
					if( self.fieldsToUpdate.chgPer ){
						self.fieldsToUpdate.chgPer.html( sign + newChgPercentVal.toFixed(4) );
						if( prevChgVal < 0 && newChgVal > 0 || prevChgVal > 0 && newChgVal < 0 ){
							self.changeColorAndBg(self.fieldsToUpdate.chgPer, prevChgVal, newChgVal);
						}
					}
					currentTr.attr({'data-prev-chg-val': newChgVal});
				}
							
				
				for ( columnKey in self.fieldsToUpdate ) {
					if( columnKey != 'chg' && columnKey != 'chgPer' && columnKey != 'high' && columnKey != 'low' && columnKey != 'time' && columnKey != 'bid' ){
						self.fieldsToUpdate[columnKey].html(parsedData[columnKey]);
					}
					if( columnKey == 'time' ){
						self.fieldsToUpdate[columnKey].html( self.getLocalTimeForData(parsedData[columnKey]) );
					}
					if( columnKey == 'bid' ){
						
						if( self.fieldsToUpdate['bid'].find('.value').length ){
							self.fieldsToUpdate['bid'].find('.value').html(parsedData['bid']);
							
							if( newBidVal != oldBidVal ){
								var diff = newBidVal - oldBidVal;
								self.fieldsToUpdate['bid'].find('.changeVal').html( diff.toFixed(4) );
								self.changeColorAndBgBid( self.fieldsToUpdate['bid'].find('.changeVal'), oldBidVal, newBidVal );
							}
							
						}else{
							self.fieldsToUpdate['bid'].html(parsedData['bid']);
							self.changeColorAndBgBid( self.fieldsToUpdate['bid'], oldBidVal, newBidVal );
						}
						
					}
					if( columnKey == 'high' ){
						var oldHigh = self.fieldsToUpdate['high'].text();
						if( !isNaN(oldHigh) ){
							var oldHighVal = parseFloat( oldHigh );
							if( newBidVal > oldHighVal ){
								self.fieldsToUpdate['high'].html( newBidVal.toFixed(4) );
								self.changeColorAndBg( self.fieldsToUpdate['high'], oldHighVal, newBidVal );
							}
						}else{
							self.fieldsToUpdate['high'].html(parsedData['bid']);
						}
					}
					if( columnKey == 'low' ){
						var oldLow = self.fieldsToUpdate['low'].text();
						if( !isNaN(oldLow) ){
							var oldLowVal = parseFloat( oldLow );
							if( newBidVal < oldLowVal ){
								self.fieldsToUpdate['low'].html( newBidVal.toFixed(4) );
								self.changeColorAndBg( self.fieldsToUpdate['low'], oldLowVal, newBidVal );
							}
						}else{
							self.fieldsToUpdate['low'].html(parsedData['bid']);
						}
						
					}
				}
				
				if( !self.fieldsToUpdate.bid ){
					if( oldBidVal < newBidVal ){
						currentTr.removeClass('loss');
						currentTr.addClass('profit');
					}
					if( oldBidVal > newBidVal ){
						currentTr.removeClass('profit')
						currentTr.addClass('loss');
					}
				}

				currentTr.attr({'data-oldBid':parsedData.bid});	
				
				setTimeout(function() {
					if ( newBidVal > oldBidVal || newBidVal < oldBidVal ) {
						currentTr.find('.changeVal').removeClass('bg');
					}
					
				}, 1250);
			
			},
			changeColorAndBg: function(el, prevVal, newVal){
				if( prevVal < newVal ){
					el.removeClass('lossTextColor');
					el.addClass('profitTextColor');
					el.addClass('bg');
				}else{
					el.removeClass('profitTextColor');
					el.addClass('lossTextColor');
					el.addClass('bg');
				}
			},
			changeColorAndBgBid:function(element, oldBidVal, newBidVal) {
				if( oldBidVal < newBidVal ){
					var tr = element.closest('.trClass')
					tr.removeClass('loss');
					tr.addClass('profit');
					element.addClass('bg');
				}
				if( oldBidVal > newBidVal ){
					var tr = element.closest('.trClass')
					tr.removeClass('profit')
					tr.addClass('loss');
					element.addClass('bg');
				}
			},

			updateTimeToLocal:function() {
				var timeRegex = new RegExp('^[0-9]{2}\:[0-9]{2}\:[0-9]{2}');
				self.$wQuotesTime.each(function(index, element){
					var $this = $(this);
					var startTime = $this.text();
					if( startTime != '' && timeRegex.test( startTime ) ){ 
						$this.html( self.getLocalTimeForData( startTime ) );
					}
				});
			},
			getLocalTimeForData:function(timeStr) {
				var timeArr = timeStr.split(":");
					
				var $date = new Date(2015, 1, 1, timeArr[0], timeArr[1], timeArr[2], 0); //new Date(year, month, day, hours, minutes, seconds, milliseconds)
				var timezoneOffset = $date.getTimezoneOffset() + 60 * options.timeDataOffset;
				$date.setMinutes ( $date.getMinutes() + timezoneOffset * (-1) );
				var hours, minutes, seconds;
				
				if( $date.getHours() < 10 ){
					hours = '0' + $date.getHours();
				}else { hours = $date.getHours(); }
				
				if( $date.getMinutes() < 10 ){
					minutes = '0' + $date.getMinutes();
				}else{ minutes = $date.getMinutes(); }
				
				if( $date.getSeconds() < 10 ){
					seconds = '0' + $date.getSeconds();
				}else{ seconds = $date.getSeconds(); }
				
				return hours + ':' + minutes + ':' + seconds;
			},
			
		};
		self.init();
		return self;
	}
}( window.jQuery );