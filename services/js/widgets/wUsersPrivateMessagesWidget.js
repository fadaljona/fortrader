var wUsersPrivateMessagesWidgetOpen;
!function( $ ) {
	wUsersPrivateMessagesWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wUsersConversationWidget',
			ls: defLS,
			loginURL: '/default/login',
			ajaxSubmitURL: '/userPrivateMessage/ajaxAdd',
			ajaxDeleteURL: '/userPrivateMessage/ajaxDelete',
			ajaxLoadURL: '/userPrivateMessage/ajaxLoad',
			ajaxLoadItemURL: '/userPrivateMessage/ajaxLoadItem',
			ajaxLoadListURL: '/userPrivateMessage/ajaxLoadList',
			action: 'list',
			idWith: 0,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				self.$wMessagesList = self.$ns.find( options.ins+'.wMessagesList' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputText = self.$ns.find( options.ins+'.wText' );
				self.$tplItem = self.$ns.find( options.ins+'.wTplItem' );
				
				self.$wShowMore = self.$ns.find( options.ins+'.wShowMore' );
				self.$wShowMore.click( self.showMore );
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bCancel = self.$ns.find( options.ins+'.wCancel' );
				
				self.$bSubmit.click( self.submit );
				
				self.$wMessagesList.on( 'click', options.ins+'.wEdit', self.edit );
				self.$wMessagesList.on( 'click', options.ins+'.wDelete', self.delete );
				self.$wMessagesList.on( 'mouseenter mouseleave', '.itemdiv', self.hideDropdowns );
			},
			load:function( $form, model ) {
				$form.find( options.ins+'.wID' ).val( model.id );
				$form.find( options.ins+'.wText' ).val( model.text );
			},
			loadMessage: function( idMessage ) {
				if( self.loading ) return false;
				var $itemdiv = self.$wMessagesList.find( '[idMessage='+idMessage+']' );
				self.disable();
				var $tplItem = self.$tplItem.clone();
				$itemdiv.length ? $itemdiv.replaceWith( $tplItem ) : self.$wMessagesList.prepend( $tplItem );
				$tplItem.show();
				self.loading = true;
				if( !$itemdiv.length ) $.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear' });
				$.getJSON( yiiBaseURL+options.ajaxLoadItemURL, {id:idMessage}, function ( data ) {
					self.loading = false;
					self.enable();
					if( data.error ) {
						alert( data.error );
						if( $itemdiv.length ) $tplItem.replaceWith( $itemdiv )
					}
					else{
						var $itemdiv = $( data.item );
						$tplItem.replaceWith( $itemdiv );
						self.updateTooltips();
					}
					$tplItem.remove();
				});
			},
			disable: function() {
				self.$ns.find( options.ins+'.wSubmit' ).add( options.ins+'.wCancel' ).add( self.$wShowMore ).attr( 'disabled', 'disabled' );
			},
			enable: function() {
				self.$ns.find( options.ins+'.wSubmit' ).add( options.ins+'.wCancel' ).add( self.$wShowMore ).removeAttr( 'disabled' );
			},
			showMore:function() {
				if( self.loading ) return false;
				self.disable();
				self.$wShowMore.parent().hide();

				var $tplItem = self.$tplItem.clone();
				self.$wMessagesList.append( $tplItem );
				$tplItem.show();
				self.loading = true;
				var idLastMessage = self.$wMessagesList.find( '[idMessage]:last' ).attr( 'idMessage' );
				var data = {};
				data.action = options.action;
				if( options.action == 'dialog' ) data.idWith = options.idWith;
				if( idLastMessage ) data.idLastMessage = idLastMessage;
				$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
					self.loading = false;
					self.enable();
					if( data.error ) {
						alert( data.error );
					}
					else{
						var $list = $( data.list );
						$tplItem.replaceWith( $list );
						self.updateTooltips();
						if( data.more ) {
							self.$wShowMore.parent().show();
						}
					}
					$tplItem.remove();
				});
				return false;
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			updateTooltips:function() {
				self.$ns.find('[data-rel=tooltip]').tooltip();
			},
			returnForm:function() {
				self.$bCancel.hide();
				self.$inputID.val( '' );
				self.$inputText.val( '' );
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
			
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				var lSubmit = self.$bSubmit.html();
				self.$bSubmit.html( options.ls.lSaving );
				self.loading = true;
				$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
					self.loading = false;
					self.$bSubmit.html( lSubmit );
					self.enable();
					if( data.error ) {
						alert( data.error );
						if( data.errorField ) {
							var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
							$errorField.addClass( options.errorClass );
							$errorField.focus();
						}
					}
					else{
						self.returnForm();
						self.loadMessage( data.id );
					}
				}, "json" );
				return false;
			},
			edit:function() {
				self.hideDropdowns();
				if( self.loading ) return false;
				self.disable();
				
				var $link = $(this);
				var $itemdiv = $link.closest( '.itemdiv' );
				var idMessage = $itemdiv.attr( 'idMessage' );
				var $tplItem = self.$tplItem.clone();
				$itemdiv.find( '.tooltip' ).remove();
				$itemdiv.replaceWith( $tplItem );
				$tplItem.show();
				self.loading = true;
				$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:idMessage}, function ( data ) {
					self.loading = false;
					self.enable();
					if( data.error ) {
						alert( data.error );
						$tplItem.replaceWith( $itemdiv );
					}
					else{
						var $form = self.$form.clone();
						$form.$inputText = $form.find( options.ins+'.wText' );
						$form.$bSubmit = $form.find( options.ins+'.wSubmit' );
						$form.$bCancel = $form.find( options.ins+'.wCancel' );
						
						$tplItem.replaceWith( $form );
						self.load( $form, data.model );
						
						$form.$inputText.focus();
						
						$form.$bSubmit.click( function() {
							if( self.loading ) return false;
							self.disable();
							$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
							
							var lSubmit = $form.$bSubmit.html();
							$form.$bSubmit.html( options.ls.lSaving );
							self.loading = true;
															
							$.post( yiiBaseURL+options.ajaxSubmitURL, $form.serialize(), function ( data ) {
								self.loading = false;
								$form.$bSubmit.html( lSubmit );
								self.enable();
								if( data.error ) {
									alert( data.error );
									if( data.errorField ) {
										var $errorField = $form.find( '*[name="'+data.errorField+'"]' );
										$errorField.addClass( options.errorClass );
										$errorField.focus();
									}
								}
								else{
									$form.replaceWith( $itemdiv );
									self.loadMessage( data.id );
								}
							}, "json" );
							
							return false;
						});
						
						$form.$bCancel.show().click( function() {
							$form.replaceWith( $itemdiv );
							$form.remove();
							self.updateTooltips();
							return false;
						});
					}
					$tplItem.remove();
				});
				return false;
			},
			delete:function() {
				self.hideDropdowns();
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				
				var $link = $(this);
				var $itemdiv = $link.closest( '.itemdiv' );
				var idMessage = $itemdiv.attr( 'idMessage' );
				var $tplItem = self.$tplItem.clone();
				$itemdiv.replaceWith( $tplItem );
				$tplItem.show();
				self.loading = true;
				$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:idMessage}, function ( data ) {
					self.loading = false;
					self.enable();
					if( data.error ) {
						alert( data.error );
						$tplItem.replaceWith( $itemdiv );
					}
					$tplItem.remove();
				});
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );