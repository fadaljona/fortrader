var wJournalProfileWidgetOpen;
!function( $ ) {
	wJournalProfileWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lUnsubscribe: "You're subscribed",
			lSubscribe: 'Subscribe for magazine',
		};
		var defOption = {
			selector: '.wJournalProfileWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				
				self.$wSubscribe = self.$ns.find( '.wSubscribe' );
				self.$wVotesCount = self.$ns.find('.JournalRatingVotes span');
				self.$wSubscribe.click( self.switchSubscribe );
				
			},
			onChangeMark:function( value ) {
				var data = {
					idJournal: options.idJournal,
					value: value,
				};
				$.getJSON( options.ajaxAddMarkURL, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$wVotesCount.text( data.votes );
					}
				});
			},
			switchSubscribe:function() {
				if( self.$wSubscribe.hasClass( 'wSet' )) {
					self.subscribe();
				}
				else{
					self.unsubscribe();
				}
				return false;
			},
			subscribe:function() {
				self.$wSubscribe.find('a').html( options.ls.lSaving );
				$.getJSON( options.ajaxSubscribeURL, {}, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$wSubscribe.find('a').html( options.ls.lUnsubscribe );
						self.$wSubscribe.removeClass( 'wSet' );
					}
				});
			},
			unsubscribe:function() {
				self.$wSubscribe.find('a').html( options.ls.lSaving );
				$.getJSON( options.ajaxUnsubscribeURL, {}, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$wSubscribe.find('a').html( options.ls.lSubscribe );
						self.$wSubscribe.addClass( 'wSet' );
					}
				});
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );