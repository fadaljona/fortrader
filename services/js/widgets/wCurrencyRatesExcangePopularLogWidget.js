var wCurrencyRatesExcangePopularLogWidgetOpen;
!function( $ ) {
	wCurrencyRatesExcangePopularLogWidgetOpen = function( options ) {
		var defOption = {
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$histLog = self.$ns.find('.histLog');
				self.$histLogHeader = self.$histLog.find('.exchange-links-header');
			
				self.subscribeToLog();
				
			},
			insertLog:function(data){
				var $data = $(data);
				$data.css({'background-color':'#FFFF00'});
				self.$histLogHeader.after($data);
				self.$histLog.find('li').animate({
					backgroundColor: '#f1f1f1',
				}, 1500, function() {
				});
			},
			subscribeToLog:function() {		
				var conn = new ab.Session(options.connUrl,
					function() {
						console.warn('WS connection opened');
						conn.subscribe(options.wsKey, function(topic, data) {
							self.insertLog(data);
						});
					},
					function() {
						console.warn('WebSocket connection closed');
					},
					{'skipSubprotocolCheck': true}
				);
			},

		};
		self.init();
		return self;
	}
}( window.jQuery );