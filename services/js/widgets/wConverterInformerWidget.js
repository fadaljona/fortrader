var wConverterInformerWidgetOpen;
!function( $ ) {
	wConverterInformerWidgetOpen = function( options ) {
		var defOption = {
			ajax: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
	
				self.$ns = $( options.selector );
				self.$wFlipConverter = self.$ns.find( '.flipConverter' );
	
				self.$wConverterSelectFrom = self.$ns.find( 'select.converterSelectFrom' );
				self.$wConverterSelectTo = self.$ns.find( 'select.converterSelectTo' );
				self.$wConverterInpFrom = self.$ns.find( 'input.converterInpFrom' );
				self.$wConverterInpTo = self.$ns.find( 'input.converterInpTo' );
				
				self.prevToVal = parseFloat(self.$wConverterInpTo.val());
				
				self.$wConverterInps = self.$ns.find( 'input.form_input' );
				
				self.initFormStyler();

				self.converterInpBindKeyup();
				
				self.$wFlipConverter.click( self.changeConverterFromTo );
				self.recalculateConverter('From', 'To');
			},

			changeConverterFromTo:function(){
				if( self.loading ) return false;
				self.loading = true;
				
				var fromInpVal = self.$wConverterInpFrom.val();
				self.$wConverterInpFrom.val( self.$wConverterInpTo.val() );
				self.$wConverterInpTo.val( fromInpVal );
				
				var fromSelectVal = self.$wConverterSelectFrom.val();
				self.$wConverterSelectFrom.val( self.$wConverterSelectTo.val() );
				self.$wConverterSelectTo.val( fromSelectVal );
				
				self.$wConverterSelectFrom.trigger('refresh');
				self.$wConverterSelectTo.trigger('refresh');
				
				self.loading = false;
				
				return false;
			},

			converterInpBindKeyup:function(e){
				self.$wConverterInps.keyup(function(e) {
					var $this = $(this);
					var val = $this.val();
					val = val.replace(/[^\d.]*/g, '')
							.replace(/([.])[.]+/g, '$1')
							.replace(/^[^\d]*(\d+([.]\d{0,15})?).*$/g, '$1');
					$this.val( val );
					if( val != '' ){
						self.recalculateConverter('From', 'To');
					}
				});
			},
			initFormStyler:function(){
				var $forms = self.$ns.find('.select_type1');
				if( $forms.length ){
					$forms.styler({
						onSelectClosed: function() {
							var $this = $(this);
							if( $this.hasClass('converterSelect') ){
								self.recalculateConverter('From', 'To');
							}
						}
					});
				}
			},
			recalculateConverter:function( fromCur, toCur ){
				var $toSelect = self.$ns.find( 'select.converterSelect' + toCur );
				var $selectOptDest = $toSelect.find(":selected");
				var destNominal = parseInt( $selectOptDest.attr( 'data-nominal' ) );
				var destVal = parseFloat( $selectOptDest.attr( 'data-value' ) );
									
				var $fromInp = self.$ns.find( 'input.converterInp' + fromCur );
				var $toInp = self.$ns.find( 'input.converterInp' + toCur );
				var $fromSelect = self.$ns.find( 'select.converterSelect' + fromCur );					
				var $selectOpt = $fromSelect.find(":selected");
				var toVal = parseFloat( $fromInp.val() );
				var newNominal = parseInt( $selectOpt.attr( 'data-nominal' ) );
				var newVal = parseFloat( $selectOpt.attr( 'data-value' ) );
				
				$toInp.val( (( newVal/newNominal   ) / ( destVal / destNominal ) * toVal).toFixed(4) );			

			},
		};
		self.init();
		return self;
	}
}( window.jQuery );