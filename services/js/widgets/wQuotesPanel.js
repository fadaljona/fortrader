var wQuotesPanel = function () {
	var core = function () {
		this.options = {
			loginURL: "/default/login",
			logged:false,
			iTimer: 15,
			ids: [],
			ls: {},
			idContainer: "#wQuotesPanel",
			ajaxUpdateURL: '/quotesPanel/ajaxGetTable',
			ajaxSavePanel: '/quotesPanel/ajaxSavePanel',
			ajaxRefreshPanel: '/quotesPanel/ajaxRefreshPanel',
			quotesEditURL: "/quotesOld/list?m=editPanel",
			checkAuthURL: "/quotesPanel/checkAuth",
			quotes:[]
		};

		this.sInterval=null;
	};

	core.prototype = (function () {
		return {
			"init": function () {
				var self = this;

				jQuery(".wQuoteItemDelete").click(function () {
					if (self.checkAuth()) {
						self.removeItemWithSave(jQuery(this).data("id"));
						if (window.wQuotesWidgetList != undefined && wQuotesWidgetList.options.mode == "editPanel")
							wQuotesWidgetList.hideLine(jQuery(this).data("id"));
					} else {
						if (confirm(self.options.ls.lLoginForSave)) {
							location.href = yiiBaseURL + self.options.loginURL;
						}
					}
				});

				jQuery(".wPlusBtn").click(function () {
					if (self.checkAuth()) {
						location.href = yiiBaseURL + self.options.quotesEditURL;
					} else {
						if (confirm(self.options.ls.lLoginForSave)) {
							location.href = yiiBaseURL + self.options.loginURL;
						}
					}
				});

				this.sInterval = setInterval(function () {
						self.updateQuotes();
					},
					self.options.iTimer * 1000
				);
				
				jQuery('span.wQuoteItemPrice').css({opacity:1});
				jQuery('span.wQuoteItemPrice').animate({
					opacity: 0.5,
				}, (self.options.iTimer-1)*1000, function() {
					// Animation complete.
				});
				
			},

			"removeItem": function (_id) {
				var self = this;
				var id = _id + "";
				if (self.checkAuth()) {
					jQuery("#wQuoteItem__" + id).remove();
					var index = self.options.ids.indexOf(id);
					if (index > -1) {
						self.options.ids.splice(index, 1);
					}
				} else {
					if (confirm(self.options.ls.lLoginForSave)) {
						location.href = yiiBaseURL + self.options.loginURL;
					}
				}
			},
			
			"restartTimer":function(){
				var self=this;
				clearInterval(self.sInterval);
				self.sInterval = setInterval(function () {
						self.updateQuotes();
					},
					self.options.iTimer * 1000
				);
			},

			"removeItemWithSave": function (id) {
				this.removeItem(id);
				this.saveIds();
			},

			"addItemWithSave": function (elem) {
				this.addItem(elem);
				this.saveIds();
			},

			"addItem": function (elem) {
				var self = this;

				var divItem = jQuery('<div>', {
					"class": 'wQuoteItem',
					"id": 'wQuoteItem__' + elem['id']
				});
				var spanItem2 = jQuery('<span>', {
					"class": 'wQuoteItemPrice',
					text: elem['bid']
				});
				var spanItem1 = jQuery('<span>', {
					"class": 'wQuoteItemText',
					"title": elem['desc'],
					html: "<span class='wQuoteItemName'>"+(elem['nalias']!=undefined&&elem['nalias']?elem['nalias']:elem['name'])+"</span>" + "<span class='wSeparate'>:</span>"
				}).after(spanItem2);

				var deleteBtn = jQuery("<div>", {
					"class": "wQuoteItemDelete",
					"data-id": elem['id'],
					"title": self.options.ls.deleteItem,
					click: function () {
						if (self.checkAuth()) {
							self.removeItemWithSave(jQuery(this).data("id"));
							if (wQuotesWidgetList != undefined && wQuotesWidgetList.options.mode == "editPanel")
								wQuotesWidgetList.hideLine(jQuery(this).data("id"));
						} else {
							if (confirm(self.options.ls.lLoginForSave)) {
								location.href = yiiBaseURL + self.options.loginURL;
							}
						}
					}
				});
				deleteBtn.append('<div class="wQuoteItemBtnImage"></div>');
				spanItem1 = spanItem1.after(deleteBtn);
				spanItem1.after('<div class="wQuoteItemTrend ' + (elem['trend'] == 1 ? 'wGreen' : 'wRed') + '"></div>');
				divItem.append(spanItem1);

				self.options.ids.push(elem['id']);
				jQuery(this.options.idContainer + " .wQuotes").append(divItem);
			},

			"reloadPanel": function () {
				this.clearPanel();
				for (var key in this.options.ids) {
					var elem = this.options.quotes[this.options.ids[key]];
					this.addItem(elem);
				}
				this.updateQuotes();
				this.restartTimer();
			},
			
			"clearPanel":function(){
				jQuery(this.options.idContainer + " .wQuotes").html('');
			},

			"saveRemoveItem": function () {
				var self = this;
				jQuery.post(yiiBaseURL + self.options.ajaxSavePanel, {
					"values": self.options.ids
				}, function (data) {
					if (data.error == undefined) {

					} else {
						self.setError(data.error);
					}
				}, "json");
			},

			"saveIds": function () {
				var self = this;
				jQuery.post(yiiBaseURL + self.options.ajaxSavePanel, {
					"ids": self.options.ids
				}, function (data) {
					if (data.error == undefined) {
						/*window.location.href = window.location.href.slice(0, window.location.href.indexOf("?"));*/
					} else {
						self.setError(data.error);
					}
				}, "json");
			},

			"saveTimer": function () {
				var self = this;
				jQuery.post(yiiBaseURL + self.options.ajaxSavePanel, {
					"time_update_panel": self.options.iTimer
				}, function (data) {
					if (data.error == undefined) {
						jQuery('#timerPanel').html('');
						self.restartTimer();
					} else {
						self.setError(data.error);
					}
				}, "json");
			},

			"setOptions": function (options) {
				for (var i in options) {
					this.options[i] = options[i];
				}
			},

			/*"updateQuotes": function (callback) {
				var self = this;
				jQuery.post(yiiBaseURL + self.options.ajaxUpdateURL, {
					"idsQuotes": self.options.ids
				}, function (data) {
					if (data.error == undefined) {
						if(callback)
							callback();
						self.setQuotes(data);
						//console.log(data);
					} else {
						self.setError(data.error);
					}
				}, "json");
			},*/
			
			"updateQuotes": function (callback) {			
				var self = this;

				jQuery.get('/quotes_list/quotes.txt', function (data) {
					if (data.error == undefined) {
						if(callback)
							callback();
						
						var actual_quotes = JSON.parse(JSON.stringify( self.options.quotes ));
						var data_arr = data.split("\n");
						for (var key1 in data_arr) {
							var data_arr_str = data_arr[key1].split(";");
							
							for (var key2 in actual_quotes) {
								if( actual_quotes[ key2 ].name == data_arr_str[0] ){
									actual_quotes[ key2 ].bid = data_arr_str[1];
									actual_quotes[ key2 ].ask = data_arr_str[2];
									actual_quotes[ key2 ].trend = data_arr_str[6];
								}
							}
						}
						var actual_quotes_data = {};
						for (var key1 in actual_quotes) {
							actual_quotes_data[ actual_quotes[ key1 ].id ] = actual_quotes[ key1 ];
						}
						//console.log(actual_quotes_data);
						self.setQuotes(actual_quotes_data);
					} else {
						self.setError(data.error);
					}
				});
				
				
			},

			"setQuotes": function (data) {
				var panel = jQuery(this.options.idContainer).find(".wQuotes"), item;

				for (var key1 in data) {
					item = panel.find('#wQuoteItem__' + key1);
					for (var key2 in data[key1]) {
						item.find('span.wQuoteItemText > span.wQuoteItemName').text(data[key1]['nalias']!=undefined&&data[key1]['nalias']?data[key1]['nalias']:data[key1]['name']);
						item.find('span.wQuoteItemText').attr("title",data[key1]['desc']);
						
						var tmp_bid = item.find('span.wQuoteItemPrice').text();
						
						item.find('span.wQuoteItemPrice').text(data[key1]['bid']);
						
						if( tmp_bid != data[key1]['bid'] ){
							
							item.find('span.wQuoteItemPrice').css({opacity:1});
							item.find('span.wQuoteItemPrice').animate({
								opacity: 0.5,
							}, (this.options.iTimer-1)*1000, function() {
								// Animation complete.
							});
							
						}
						
						if (data[key1]['trend'] == "1") {
							item.find('div.wQuoteItemTrend').removeClass().toggleClass('wQuoteItemTrend wGreen');
						} else if (data[key1]['trend'] == "-1") {
							item.find('div.wQuoteItemTrend').removeClass().toggleClass('wQuoteItemTrend wRed');
						}
					}
				}
			},

			"checkAuth": function () {
				return this.options.logged;
			},

			"setError": function (error) {
				var span = jQuery('.wQuotesError');
				span.find('span.text').text(error);
				span.show();
			}

		};
	})();

	return new core();
};
