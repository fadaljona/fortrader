var wChatWidgetOpen;
!function( $ ) {
	wChatWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wChatWidget',
			ls: defLS,
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			slimScrolled: false,
			init:function() {
				self.$ns = $( options.selector );
				
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );