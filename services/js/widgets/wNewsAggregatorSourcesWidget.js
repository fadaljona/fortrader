var wNewsAggregatorSourcesWidgetOpen;
!function( $ ) {
	wNewsAggregatorSourcesWidgetOpen = function( options ) {
		var defOption = {
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$searchForm = self.$ns.find('.sourceSearchForm');
				self.$searchKey = self.$searchForm.find('.sourceSearchKey');
				self.$disabledCount = self.$ns.find('.sources_indicator');
			

				self.$ns.on( 'submit', '.sourceSearchForm', self.changePage );
				self.$ns.on( 'click', '.pagination a', self.changePage );
				self.$ns.on( 'click', '.box_radio_btn1 input:radio', self.saveDisabled );
				self.$ns.on( 'click', '.sources_enable_all', self.resetDisabledSources );
			},
			resetDisabledSources:function(){
				self.$ns.find('input').prop( 'checked', true );
				self.$disabledCount.text( '0' );
				self.setDisabledSourcesToCookie( '' );
				return false;
			},
			disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			saveDisabled:function(){
				$cc = self.$ns.find( '.box_radio_btn1 input.sources_off' );
				var disabledSources = [];
				var enabledSources = [];
				$cc.each(function(index, element){
					var name = $(this).attr('name');
					var el = self.$ns.find('input[name='+name+']:checked');
					
					if( el.val() == 'disabled' ){
						disabledSources.push( $(this).attr('data-source-id') );
					}else{
						enabledSources.push( $(this).attr('data-source-id') );
					}
				});
				
				var storedDisabledSources = [];
				if( self.getDisabledSources() != null ) {
					var tmpStr = self.getDisabledSources();
					if( tmpStr != '' ) storedDisabledSources = tmpStr.split(",");
				}
				
				var resultArr = self.uniqueArr( disabledSources.concat(storedDisabledSources) );
						
				if(  enabledSources.length ){
					for( var keye in enabledSources ){
						for( var keyd in resultArr ){
							if( enabledSources[keye] == resultArr[keyd] ){
								resultArr.splice(keyd, 1);
							} 
						}
					}
				}
				self.$disabledCount.text( resultArr.length );
				self.setDisabledSourcesToCookie( resultArr.join(",") );
			},
			uniqueArr:function(arr) {
				var result = [];

				nextInput:
				for (var i = 0; i < arr.length; i++) {
					var str = arr[i];
					for (var j = 0; j < result.length; j++) { 
						if (result[j] == str) continue nextInput; 
					}
					result.push(str);
				}

				return result;
			},
			getDisabledSources:function() {
				var c_name = options.cookieName;
				if (document.cookie.length > 0) {
					c_start = document.cookie.indexOf(c_name + "=");
					if (c_start != -1) {
						c_start = c_start + c_name.length + 1;
						c_end = document.cookie.indexOf(";", c_start);
						if (c_end == -1) {
							c_end = document.cookie.length;
						}
						return unescape(document.cookie.substring(c_start, c_end));
					}
				}
				return null;
			},
			setDisabledSourcesToCookie:function(value) {
				var expires;
				var date = new Date();
				date.setTime(date.getTime() + (100000 * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();	
				var name = options.cookieName;
				document.cookie = name + "=" + value + expires + "; path=/";
			},
			changePage:function() {
				
				var searchKey = self.$searchKey.val();
				
				var $link = $(this);
				var $li = $link.closest( 'li' );
				if( !$li.hasClass( 'disabled' ) && !$li.hasClass( 'active' ) || searchKey != '' ) {
					var $href = $link.attr( 'href' );
					var matchs = /sourcesPage=(\d+)/.exec( $href );
					var sourcesPage = matchs ? matchs[1] : 1;
					self.disable();
					$.getJSON( options.ajaxLoadListURL, {sourcesPage:sourcesPage, searchKey:searchKey}, function ( data ) {
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $content = $( data.list );
							self.$ns.replaceWith( $content );
							self.init();
						}
					});
				}
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );