var wCommonRatingWidgetOpen;
!function( $ ) {
	wCommonRatingWidgetOpen = function( options ) {
		var defLS = {
		
		};
		var defOption = {
			
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
                self.$ns = $( options.selector );
                self.$ns.addClass('spinner-margin').addClass('position-relative');
                
                
				
				
			},
			
			
			
	
			disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find('.spinner-wrap').remove();
			},

	
			onChangeMark:function( value, selector ) {
				var data = {
                    idModel: selector.replace(options.cutStrToGetId, ''),
                    modelName: options.modelName,
					value: value,
				};
				
                
                self.disable();
		
				$.getJSON( options.ajaxAddMarkURL, data, function ( data ) {
                    self.enable();
					if( data.error ) {
						alert( data.error );
					}
				});
			},

			
		};
		self.init();
		return self;
	}
}( window.jQuery );