var wUsersConversationWidgetOpen;
!function( $ ) {
	wUsersConversationWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lUpdating: 'Updating...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wUsersConversationWidget',
			ajax: false,
			ls: defLS,
			loginURL: '/default/login',
			ajaxSubmitURL: '/userMessage/ajaxAdd',
			ajaxDeleteURL: '/userMessage/ajaxDelete',
			ajaxLoadURL: '/userMessage/ajaxLoad',
			ajaxLoadItemURL: '/userMessage/ajaxLoadItem',
			ajaxLoadListURL: '/userMessage/ajaxLoadList',
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
			isGuest: false,
			iTimer:10,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			slimScrolled: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$wAnchorForm = self.$ns.find( options.ins+'.wAnchorForm' );
				self.$form = self.$ns.find( 'form' );
				
				self.$wMessagesList = self.$ns.find( options.ins+'.wMessagesList' );
								
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputIDMessageQuery = self.$ns.find( options.ins+'.wIDMessageQuery' );
				self.$inputText = self.$ns.find( options.ins+'.wText' );
				self.$tplItem = self.$ns.find( options.ins+'.wTplItem' );
												
				self.$wReload = self.$ns.find( options.ins+'.wReload' );
				
				self.$UsersConversationOnline = self.$ns.find( options.ins+'.UsersConversationOnline' );
				
				self.$wShowMore = self.$ns.find( options.ins+'.wShowMore' );
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bCancel = self.$ns.find( options.ins+'.wCancel' );
				self.$wText = self.$ns.find( options.ins+'.wText' );
				
				self.$wReload.click( self.reload );
				self.$wShowMore.click( self.showMore );
				self.$bSubmit.click( self.submit );
				
				self.$wText.focus( self.loadNewMessage );
				
				self.$wMessagesList.on( 'click', options.ins+'.wEdit', self.edit );
				self.$wMessagesList.on( 'click', options.ins+'.wDelete', self.delete );
				self.$wMessagesList.on( 'click', options.ins+'.wAnswer', self.answer );
				self.$wMessagesList.on( 'click', options.ins+'.wCancel', self.cancel );
				self.$wMessagesList.on( 'mouseenter mouseleave', '.itemdiv', self.hideDropdowns );
				
				self.submitSavedForm();
			},
			load:function( $form, model ) {
				$form.find( options.ins+'.wID' ).val( model.id );
				$form.find( options.ins+'.wText' ).val( model.text );
				$form.find( options.ins+'.wIDMessageQuery' ).val( model.idMessageQuery );
			},
			loadNewMessage:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					var $tplItem = self.$tplItem.clone();
					self.$wMessagesList.prepend( $tplItem );
					//$tplItem.show();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lUpdating );
					self.loading = true;
					var idFirstMessage = self.$wMessagesList.find( '[idMessage]:first' ).attr( 'idMessage' );
					var data = {};
					data.instance = options.instance;
					if( options.idLinkedObj ) data.idLinkedObj = options.idLinkedObj;
					if( idFirstMessage ) data.idFirstMessage = idFirstMessage;
					$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
						self.loading = false;
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $list = $( data.list );
							$tplItem.replaceWith( $list );
							self.updateTooltips();
							self.$UsersConversationOnline.html( data.usersOnline );
						}
						$tplItem.remove();
						self.$bSubmit.html( lSubmit );
					});
					
					if( self.sInterval === undefined ){
						self.sInterval = setInterval(function () {
							self.loadNewMessage();
							},
							options.iTimer * 1000
						);
					}
						
					return false;
				}
			},
			loadMessage: function( idMessage ) {
				if( self.loading ) return false;
				var $itemdiv = self.$wMessagesList.find( '[idMessage='+idMessage+']' );
				self.disable();
				var $tplItem = self.$tplItem.clone();
				$itemdiv.length ? $itemdiv.replaceWith( $tplItem ) : self.$wMessagesList.prepend( $tplItem );
				$tplItem.show();
				self.loading = true;
				if( !$itemdiv.length ) $.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear' });
				$.getJSON( yiiBaseURL+options.ajaxLoadItemURL, {id:idMessage}, function ( data ) {
					self.loading = false;
					self.enable();
					if( data.error ) {
						alert( data.error );
						if( $itemdiv.length ) $tplItem.replaceWith( $itemdiv )
					}
					else{
						var $itemdiv = $( data.item );
						$tplItem.replaceWith( $itemdiv );
						self.updateTooltips();
					}
					$tplItem.remove();
				});
			},
			disable: function() {
				self.$ns.find( options.ins+'.wSubmit' ).add( options.ins+'.wCancel' ).add( self.$wShowMore ).attr( 'disabled', 'disabled' );
			},
			enable: function() {
				self.$ns.find( options.ins+'.wSubmit' ).add( options.ins+'.wCancel' ).add( self.$wShowMore ).removeAttr( 'disabled' );
			},
			reload: function() {
				if( self.loading ) return false;
				self.disable();
				self.$wShowMore.parent().hide();
				if( options.ajax ) {
					self.$wMessagesList.html( '' );
					var $tplItem = self.$tplItem.clone();
					self.$wMessagesList.append( $tplItem );
					$tplItem.show();
					var data = {};
					data.instance = options.instance;
					self.loading = true;
					if( options.idLinkedObj ) data.idLinkedObj = options.idLinkedObj;
					$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
						self.loading = false;
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $list = $( data.list );
							$tplItem.replaceWith( $list );
							self.updateTooltips();
							if( data.more ) {
								self.$wShowMore.parent().show();
							}
						}
						$tplItem.remove();
					});
				}
				return false;
			},
			showMore:function() {
				if( self.loading ) return false;
				self.disable();
				self.$wShowMore.parent().hide();
				if( options.ajax ) {
					var $tplItem = self.$tplItem.clone();
					self.$wMessagesList.append( $tplItem );
					$tplItem.show();
					self.loading = true;
					var idLastMessage = self.$wMessagesList.find( '[idMessage]:last' ).attr( 'idMessage' );
					var data = {};
					data.instance = options.instance;
					if( options.idLinkedObj ) data.idLinkedObj = options.idLinkedObj;
					if( idLastMessage ) data.idLastMessage = idLastMessage;
					$.getJSON( yiiBaseURL+options.ajaxLoadListURL, data, function ( data ) {
						self.loading = false;
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							var $list = $( data.list );
							$tplItem.replaceWith( $list );
							self.updateTooltips();
							if( data.more ) {
								self.$wShowMore.parent().show();
							}
						}
						$tplItem.remove();
					});
					return false;
				}
			},
			saveForm:function() {
				$.cookie( 'userMessage_instance', options.instance, {path:'/'} );
				$.cookie( 'userMessage_idLinkedObj', options.idLinkedObj, {path:'/'} );
				$.cookie( 'userMessage_idMessageQuery', self.$inputIDMessageQuery.val(), {path:'/'} );
				$.cookie( 'userMessage_text', self.$inputText.val(), {path:'/'} );
			},
			clearCookie:function() {
				$.cookie( 'userMessage_instance', '', {path:'/'} );
				$.cookie( 'userMessage_idLinkedObj', '', {path:'/'} );
				$.cookie( 'userMessage_idMessageQuery', '', {path:'/'} );
				$.cookie( 'userMessage_text', '', {path:'/'} );
			},
			hideDropdowns:function() {
				self.$ns.find( '[data-toggle=dropdown]' ).each( function() {
					$(this).parent().removeClass( 'open' );
				});
			},
			updateTooltips:function() {
				self.$ns.find('[data-rel=tooltip]').tooltip();
			},
			returnForm:function() {
				self.$wAnchorForm.append( self.$form );
				self.$bCancel.hide();
				self.$inputID.val( '' );
				self.$inputIDMessageQuery.val( '' );
				self.$inputText.val( '' );
			},
			submit:function() {
				if( self.loading ) return false;
				if( options.isGuest ) {
					if( self.$inputText.val()) {
						self.saveForm();
					}
					document.location.assign( yiiBaseURL+options.loginURL+'?returnURL='+encodeURIComponent( document.location.href ));
					return false;
				}
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.returnForm();
							self.loadMessage( data.id );
						}
					}, "json" );
					return false;
				}
			},
			submitSavedForm:function() {
				if( options.isGuest ) return false;
				if( self.loading ) return false;
				if( options.ajax ) {
					var instance = $.cookie( 'userMessage_instance' );
					var idLinkedObj = $.cookie( 'userMessage_idLinkedObj' );
					var idMessageQuery = $.cookie( 'userMessage_idMessageQuery' );
					var text = $.cookie( 'userMessage_text' );
					if( instance == options.instance && idLinkedObj == options.idLinkedObj && text && text.length ) {
						self.$inputIDMessageQuery.val( idMessageQuery );
						self.$inputText.val( text );
						self.submit();
						self.clearCookie();
					}
				}
			},
			edit:function() {
				self.hideDropdowns();
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					var $link = $(this);
					var $itemdiv = $link.closest( '.itemdiv' );
					var idMessage = $itemdiv.attr( 'idMessage' );
					var $tplItem = self.$tplItem.clone();
					$itemdiv.find( '.tooltip' ).remove();
					$itemdiv.replaceWith( $tplItem );
					$tplItem.show();
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:idMessage}, function ( data ) {
						self.loading = false;
						self.enable();
						if( data.error ) {
							alert( data.error );
							$tplItem.replaceWith( $itemdiv );
						}
						else{
							var $form = self.$form.clone();
							$form.$inputText = $form.find( options.ins+'.wText' );
							$form.$bSubmit = $form.find( options.ins+'.wSubmit' );
							$form.$bCancel = $form.find( options.ins+'.wCancel' );
							
							$tplItem.replaceWith( $form );
							self.load( $form, data.model );
							
							$form.$inputText.focus();
							
							$form.$bSubmit.click( function() {
								if( self.loading ) return false;
								self.disable();
								$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
								
								var lSubmit = $form.$bSubmit.html();
								$form.$bSubmit.html( options.ls.lSaving );
								self.loading = true;
																
								$.post( yiiBaseURL+options.ajaxSubmitURL, $form.serialize(), function ( data ) {
									self.loading = false;
									$form.$bSubmit.html( lSubmit );
									self.enable();
									if( data.error ) {
										alert( data.error );
										if( data.errorField ) {
											var $errorField = $form.find( '*[name="'+data.errorField+'"]' );
											$errorField.addClass( options.errorClass );
											$errorField.focus();
										}
									}
									else{
										$form.replaceWith( $itemdiv );
										self.loadMessage( data.id );
									}
								}, "json" );
								
								return false;
							});
							
							$form.$bCancel.show().click( function() {
								$form.replaceWith( $itemdiv );
								$form.remove();
								self.updateTooltips();
								return false;
							});
						}
						$tplItem.remove();
					});
					return false;
				}
			},
			delete:function() {
				self.hideDropdowns();
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var $link = $(this);
					var $itemdiv = $link.closest( '.itemdiv' );
					var idMessage = $itemdiv.attr( 'idMessage' );
					var $tplItem = self.$tplItem.clone();
					$itemdiv.replaceWith( $tplItem );
					$tplItem.show();
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:idMessage}, function ( data ) {
						self.loading = false;
						self.enable();
						if( data.error ) {
							alert( data.error );
							$tplItem.replaceWith( $itemdiv );
						}
						$tplItem.remove();
					});
					return false;
				}
			},
			answer:function() {
				self.hideDropdowns();
				if( self.loading ) return false;
				var $link = $(this);
				var $itemdiv = $link.closest( '.itemdiv' );
				var idMessageQuery = $itemdiv.attr( 'idMessage' );
				var authorMessageQuery = $itemdiv.attr( 'authorMessage' );
				
				$itemdiv.after( self.$form );
				self.$bCancel.show();
				self.$inputText.focus().val( authorMessageQuery + ', ' );
				self.$inputIDMessageQuery.val( idMessageQuery );
				
				return false;
			},
			cancel:function() {
				self.returnForm();
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );