var wQuotesWidget = function () {
	var core = (function () {
		this.options = {
			ins: '',
			idContainer: '',
			idAllCats: 20,
			selector: '#wQuotesTableWidget',
			ajax: true,
			hidden: false,
			idsSearch: [],
			searchQuotes: {},
			ls: {},
			ajaxGetTableURL: '/quotesOld/ajaxGetTable',
			ajaxSearchURL: '/quotesOld/ajaxSearch',
			ajaxGetCheckBoxes: '/quotesOld/ajaxGetChecks',
			ajaxSearchQuote: '/quotesOld/ajaxSearchQuote',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			mode: "view",
			oPanel: null,
			iTimer: '',
			quotes: [],
			quotesSearch:[],
			defaultCheckIds: [],
			idCurrentCat: 0,
			idsQuotes: []
		};

		this.loading = false;
		this.iTimerLabel = '';
		this.sInterval=null;
	});

	core.prototype = (function () {

		return {
			/*--Установка опций ---*/
			"setOptions": function (options) {
				for (var i in options) {
					this.options[i] = options[i];
				}
			},
			/*--Инициализация событий--*/
			"init": function () {
				var self = this;
				$(self.options.idContainer).find('#setTimer').click(function () {
					self.setTimer();
					self.iTimerLabel = self.options.iTimer;
					self.setTimerLabel();
				});

				$(self.options.idContainer + ' #refreshTable').click(function () {
					self.refreshTable();
				});

				$(self.options.idContainer + " input[name=time_update_panel]").blur(function () {
					var val=parseInt(jQuery(this).val());
					if (val != self.options.oPanel.options.iTimer) {
						if(typeof(val)=="number" && val > 0){
							jQuery(self.options.idContainer + ' #timerPanel').html('<img width="15px" height="15px" src="' + yiiBaseURL + '/assets-static/images/load.gif' + '">');
							self.options.oPanel.options.iTimer = val;
							self.options.oPanel.saveTimer();
						}else{
							self.setError(self.options.ls.validNumber);
						}
					}
				});

				$(self.options.idContainer).find('#selectAllChecks').click(function () {
					var ids = [];
					$(".checksBoxes input[type=checkbox]").each(function (i) {
						$(this).attr('checked', true);
						var id = $(this).val();
						if ($.inArray(id, self.options.idsQuotes) == -1) {
							ids.push(id);
						}
					});
					if (ids.length) {
						self.getQuotes(ids);
					}
				});

				$(self.options.idContainer).find('.wEditPanelAdd').click(function () {
					self.addItemToPanel(jQuery(this).data('id'),this);
				});

				$(self.options.idContainer + ' .wEditPanelRemove').click(function () {
					self.removeItemFromPanel(jQuery(this).data('id'), this);
				});

				$(self.options.idContainer + ' #defaultChecks').click(function () {
					var ids = [];
					$(".checksBoxes input[type=checkbox]").each(function (i) {
						var id = $(this).val();
						if ($.inArray(id, self.options.defaultCheckIds) == -1) {
							$(this).attr('checked', false);
							self.hideLine(id);
						} else {
							if ($.inArray(id, self.options.idsQuotes) == -1) {
								ids.push(id);
							}
							$(this).attr('checked', true);
						}

					});
					if (ids.length) {
						self.getQuotes(ids);
					}
				});

				$(self.options.idContainer + ' #refreshPanel').click(function () {
					if (confirm(self.options.ls.refreshPanel)) {
						jQuery.post(yiiBaseURL + self.options.oPanel.options.ajaxRefreshPanel, {},
							function (data) {
								if (data.error == undefined) {
									self.options.oPanel.options.iTimer = data.iTimer;
									self.options.oPanel.options.ids = data.ids;
									self.options.oPanel.options.quotes = data.quotes;
									self.options.oPanel.reloadPanel();
									jQuery(self.options.idContainer + " input[name=time_update_panel]").val(data.iTimer);
								} else {
									self.setError(data.error);
								}
							},
							"json");
					}
				});

				$(self.options.idContainer + ' #categories').change(function () {
					if (self.options.idCurrentCat != $(this).val())
						self.changeCategory(this);
				});

				self.setClickCategory();

				$(self.options.idContainer + ' .wNameCategory').click(function () {
					self.setClickNameCategory(this);
				});

				$(self.options.idContainer + ' .wHideLine').click(function () {
					self.hideLine(jQuery(this).data('id'));
				});
				
				self.setClickCheckbox();

				self.sInterval=setInterval(function () {
					if (self.iTimerLabel)
						self.setTimerLabel(self.iTimerLabel);
					else {
						self.getTable();
						self.iTimerLabel = self.options.iTimer;
					}
					self.iTimerLabel--;
				}, 1000);
			},

			"setClickNameCategory": function (elem) {
				var hide = $(elem).data('hide');
				var iter = $(elem).data('iter');

				if (hide === 1) {
					$(this.options.idContainer + ' .quotesTable tr[data-id=hideShow_' + iter + ']').each(function () {
						if ($(this).data('deleted') != true)
							$(this).show(500)
					});
					$(elem).find('div.wArrow').removeClass().toggleClass('wArrow wArrowDown');
				} else {
					$(this.options.idContainer + ' .quotesTable tr[data-id=hideShow_' + iter + ']').each(function () {
						if ($(this).data('deleted') != true)
							$(this).hide(500)
					});
					$(elem).find('div.wArrow').removeClass().toggleClass('wArrow wArrowRight');
				}
				(hide === 1) ? $(elem).data('hide', 0) : $(elem).data('hide', 1);
			},

			"setClickCategory": function () {
				var self = this;
				$(self.options.idContainer).find('#wSubCats a').each(function () {
					jQuery(this).click(function () {
						var id_cat = $(this).parent('li').data('id');
						if (self.options.idCurrentCat != id_cat) {
							jQuery(self.options.idContainer + ' #loaderCategory').html('<img width="15px" height="15px" src="' + yiiBaseURL + '/assets-static/images/load.gif' + '">');
							self.options.idCurrentCat = id_cat;
							self.getCheckBoxes();
							self.setNewTable();
						}
						return false;
					});
				});
			},

			"setClickCheckbox": function () {
				var self = this;
				jQuery('input[name=quotesChecked]').click(function () {
					self.check(this);
				});
			},

			"getQuotes": function (ids) {
				var self = this;
				if (ids && jQuery.isArray(ids) && ids.length) {
					if (self.options.ajax) {
						self.setLoader();
						
						jQuery.post(yiiBaseURL + self.options.ajaxGetTableURL, {
							"idCat": self.options.idCurrentCat,
							"idsQuotes": ids
						}, function (data) {
							if (data.error) {
								self.setError(data.error);
							}
							else {
								self.addRows(data);
								self.setTimerLabel();
							}
						}, "json");
						self.loading = false;
						return false;
					}
				}
			},
			
			"getQuote":function(id_quote){
				var quote=this.options.quotes[id_quote]?this.options.quotes[id_quote]:undefined;
				if(quote){
					return quote;
				}
			},

			"getCategoryByQuote": function (id_quote) {
				return (this.options.quotes[id_quote]!=undefined?this.options.quotes[id_quote]['category_id']:undefined);
			},

			"hideLine": function (idSymbol) {

				var tr = $(this.options.idContainer).find('.wLineId' + idSymbol);
				tr.hide(500);
				tr.remove();
				var cat = this.getCategoryByQuote(idSymbol);
				var trs = $(this.options.idContainer).find('.wNameCategory.wCategory_' + cat);
				if (trs) {
					var count = trs.data('count') - 1;
					if (count) {
						trs.data('count', count);
					} else {
						trs.remove();
					}
				}
				var _id = idSymbol.toString().split("__");
				if (_id[1] == undefined) {
					for (var key in this.options.idsQuotes) {
						if (this.options.idsQuotes[key] == _id[0]) {
							this.options.idsQuotes.splice(key, 1);
						}
					}
					for (var key1 in this.options.quotes) {
						if (key1 == _id[0]) {
							delete this.options.quotes[key1];
						}
					}
					var cb = $(this.options.idContainer).find("#quoteCheck_" + _id[0]);
					if (cb.is(':checked')) {
						cb.attr('checked', false);
					}
				} else {
					for (var key in this.options.idsSearch) {
						if (this.options.idsSearch[key] == _id[0]) {
							this.options.idsSearch.splice(key, 1);
						}
					}
					for (var key1 in this.options.searchQuotes) {
						if (key1 == _id[0]) {
							delete this.options.searchQuotes[key1];
						}
					}
				}
			},

			"setLoader": function (id_loader) {
				var loader=(id_loader) ? id_loader : this.options.idContainer+" #timerLabel";
				jQuery(loader).html('<img width="15px" height="15px" src="' + yiiBaseURL + '/assets-static/images/load.gif' + '">');
			},

			"setTimerLabel": function (label) {
				if (label == undefined)
					jQuery(this.options.idContainer + ' #timerLabel').html(this.options.iTimer);
				else
					jQuery(this.options.idContainer + ' #timerLabel').html(label);
			},
			
			"startTimer":function(){
				var self=this;
				self.sInterval=setInterval(function () {
					if (self.iTimerLabel)
						self.setTimerLabel(self.iTimerLabel);
					else {
						self.getTable();
						self.iTimerLabel = self.options.iTimer;
					}
					self.iTimerLabel--;
				}, 1000);
			},

			"stopTimer":function(){
				clearInterval(this.sInterval);
			},

			"setTimer": function () {
				this.options.iTimer = jQuery(this.options.idContainer + " #timer").val();
			},

			"clearTable": function () {
				var table = jQuery(this.options.idContainer).find(".quotesTable");
				table.find('tbody').remove();
				table.append(jQuery("<tbody>"));
			},

			"clearQuotes": function () {
				this.options.quotes = {};
			},

			"clearIdsQuotes": function () {
				this.options.idsQuotes = [];
			},

			"clearDefaultCheckIds": function () {
				this.option.defaultCheckIds = [];
			},

			"addRowCategory": function (id, name, iter) {
				var self = this;
				var table = jQuery(this.options.idContainer).find(".quotesTable tbody");
				var tr = jQuery("<tr>", {
					"class": "wNameCategory wCategory_" + id,
					"data-iter": iter,
					"data-hide": 0,
					"data-count": 0,
					click: function () {
						self.setClickNameCategory(this);
					}
				});
				tr.append('<td style="text-align: center" colspan="10"><div class="wArrow wArrowDown"></div>' + name + '</td>');
				table.append(tr);
			},

			"addRowSearch": function (name) {
				var self = this;
				if (jQuery(".wCategory_search").length != 0)
					return;
				var table = jQuery(this.options.idContainer).find(".quotesTable tbody");
				var tr = jQuery("<tr>", {
					"class": "wNameCategory wCategory_search",
					"data-iter": "search",
					"data-hide": 0,
					click: function () {
						self.setClickNameCategory(this);
					}
				});
				var td = jQuery('<td>', {
						'style': 'text-align: center',
						'colspan': 10
					}
				);
				td.append('<div class="wArrow wArrowDown"></div><span>' + name + '</span>');
				td.append(jQuery('<span>', {
					"class": "wCloseTr",
					text: "X",
					click: function () {
						self.clearSearch()
					}
				}));
				tr.append(td);
				table.prepend(tr);
			},

			"clearSearch": function () {
				this.options.idsSearch = [];
				this.options.searchQuotes = {};
				jQuery(this.options.idContainer + " table .wCategory_search").remove();
				jQuery(this.options.idContainer + " table tr[data-id=hideShow_search]").remove();
			},

			"setNewTable": function () {
				var self = this;
				var iter = 1;
				self.setLoader();
				self.hideError();

				if (self.options.ajax) {
					jQuery.post(yiiBaseURL + self.options.ajaxGetTableURL, {
						"idCat": self.options.idCurrentCat
					}, function (data) {
						if (data.error != undefined) {
							self.setError(data.error);
						}
						else {
							self.clearIdsQuotes();
							self.clearQuotes();
							self.clearTable();
							for (var key in data) {
								if (key != "ids_quotes") {
									var key_ = key.split("__");
									self.addRowCategory(key_[1], key_[0], iter);
									self.addRows(data[key]);
									iter++;
								}
							}
							if (self.options.mode != "editPanel")
								self.setTimerLabel();
						}
						jQuery(self.options.idContainer + ' #loaderCategory').html('');
					}, "json");
					self.loading = false;
					return false;
				}
			},

			"getCategoryName": function (id) {
				if (this.options.idsCategories['categories'][id] == undefined) {
					for (var key in this.options.idsCategories['subcategories']) {
						for (var key2 in this.options.idsCategories['subcategories'][key]) {
							if (key2 == id) {
								return this.options.idsCategories['subcategories'][key][key2];
							}
						}
					}
				} else {
					return this.options.idsCategories['categories'][id];
				}

				return '';
			},

			"onSubmitSearchQuote": function (form) {
				var self = this;
				self.hideError();
				if (self.options.ajax) {
					var id_quote = jQuery(form).find('#AdminQuotesSymbolsFormModel_id').val();
					var name_quote = jQuery(form).find('#AdminQuotesSymbolsFormModel_desc').val();
					if (id_quote || name_quote) {
						jQuery.post(yiiBaseURL + self.options.ajaxSearchQuote, {
							'id_quote': id_quote,
							'name_quote': name_quote
						}, function (data) {
							self.upTable(data);
							if (data.error) {
								self.setError(data.error);
							}
							else {
								self.clearSearch();
								self.addRowSearch(data.name);
								self.addRows(data.result, true);
								self.options.quotesSearch=data.result;
							}
							jQuery(form).find('#AdminQuotesSymbolsFormModel_id').val('');
							jQuery(form).find('#AdminQuotesSymbolsFormModel_desc').val('');
							return false;
						}, "json");
						return false;
					}
				}
				return false;
			},

			"refreshTable": function () {
				this.setTimer();
				this.getTable();
			},

			/*"getTable": function () {
				var self = this;
				if (self.loading) return false;
				self.stopTimer();
				self.setLoader();
				self.hideError();
				
				if (self.options.ajax) {
					jQuery.post(yiiBaseURL + self.options.ajaxGetTableURL, {
						"idCat": self.options.idCurrentCat,
						"idsQuotes": self.options.idsQuotes.concat(self.options.idsSearch)
					}, function (data) {
						if (data.error == undefined) {
							self.upTable(data);
							self.startTimer();
						}
						else {
							self.setError(data.error);
						}
					}, "json");
					self.loading = false;
					return false;
				}
			},*/
			
			
			"getTable": function () {
				var self = this;
				if (self.loading) return false;
				self.stopTimer();
				self.setLoader();
				self.hideError();
				if (self.options.ajax) {
					jQuery.get('/quotes_list/quotes.txt', function (data) {
						if (data.error == undefined) {

							var actual_quotes = JSON.parse(JSON.stringify( self.options.quotes ));
							
							
							var data_arr = data.split("\n");
							for (var key1 in data_arr) {
								var data_arr_str = data_arr[key1].split(";");
								
								for (var key2 in actual_quotes) {
									if( actual_quotes[ key2 ].name == data_arr_str[0] ){
										actual_quotes[ key2 ].bid = data_arr_str[1];
										actual_quotes[ key2 ].ask = data_arr_str[2];
										actual_quotes[ key2 ].spread = data_arr_str[5];
										actual_quotes[ key2 ].trend = data_arr_str[6];
										actual_quotes[ key2 ].rate = '';
									}
								}
							}
							self.upTable(actual_quotes);
							self.startTimer();
						}
						else {
							self.setError(data.error);
						}
					});
					self.loading = false;
					return false;
				}
			},

			"upTable": function (data) {
				var table = jQuery(this.options.idContainer).find(".quotesTable"), tr;

				for (var key1 in data) {
					tr = table.find('.wLineId' + key1);
					for (var key2 in data[key1]) {
						if (key2 == 'name') {
							tr.find('.cell_name .wTextCell').text((data[key1]['desc'] ? data[key1]['desc'] : data[key1]['name']));
							if (data[key1]['trend'] == "1") {
								tr.find('.cell_name').find('div.wArrow').removeClass().toggleClass('wArrow wArrowGreen');
							} else if (data[key1]['trend'] == "-1") {
								tr.find('.cell_name').find('div.wArrow').removeClass().toggleClass('wArrow wArrowRed');
							}
						}
						else if (key2 != 'rate') {
							tr.find('.cell_' + key2 + ' .wTextCell').text(data[key1][key2]);
						}

						if (key2 == 'rate' && this.options.quotes[key1] != undefined) {

							var rate = (((data[key1]['bid'] - this.options.quotes[key1]['bid']) / this.options.quotes[key1]['bid']) * 100).toFixed(3) - 0;

							if (!isNaN(rate)) {
								var classTd = 'cell_rate';
								if (rate > 0)
									classTd = 'cell_rate wGreen';
								if (rate < 0)
									classTd = 'cell_rate wRed';

								if (rate != 0) {
									rate += '%';
									tr.find('.cell_rate .wTextCell').text(rate);
									tr.find('.cell_rate').removeClass().toggleClass(classTd);
								}
							}
						}

					}
				}
				this.options.quotes = data;
				this.upSearch(data);
			},

			"upSearch": function (data) {
				if (jQuery(".wCategory_search").length == 0)
					return;
				var table = jQuery(this.options.idContainer).find(".quotesTable"), tr;
				for (var key1 in this.options.idsSearch) {
					var key = this.options.idsSearch[key1];
					tr = table.find('.wLineId' + key + '__search');
					for (var key2 in data[key]) {
						if (key2 == 'name') {
							tr.find('.cell_name .wTextCell').text((data[key]['desc'] ? data[key]['desc'] : data[key]['name']));
							if (data[key]['trend'] == "1") {
								tr.find('.cell_name').find('div.wArrow').removeClass().toggleClass('wArrow wArrowGreen');
							} else if (data[key]['trend'] == "-1") {
								tr.find('.cell_name').find('div.wArrow').removeClass().toggleClass('wArrow wArrowRed');
							}
						}
						else if (key2 != 'rate') {
							tr.find('.cell_' + key2 + ' .wTextCell').text(data[key][key2]);
						}

						if (key2 == 'rate' && this.options.searchQuotes[key] != undefined) {
							var rate = (((data[key]['bid'] - this.options.searchQuotes[key]['bid']) / this.options.searchQuotes[key]['bid']) * 100).toFixed(3) - 0;
							if (!isNaN(rate)) {
								var classTd = 'cell_rate';
								if (rate > 0)
									classTd = 'cell_rate wGreen';
								if (rate < 0)
									classTd = 'cell_rate wRed';

								if (rate != 0) {
									rate += '%';
									tr.find('.cell_rate .wTextCell').text(rate);
									tr.find('.cell_rate').removeClass().toggleClass(classTd);
								}
							}
						}

					}
					if (data[key] != undefined)
						this.options.searchQuotes[key] = data[key];
				}
			},

			"check": function (checkBox) {
				var id = jQuery(checkBox).val();
				if (jQuery(checkBox).is(':checked')) {
					this.getQuote(id);
				} else {
					this.hideLine(id);
				}
			},

			"setError": function (error) {
				var span = jQuery(this.options.idContainer).find('.wQuotesError');
				span.find('span.text').text(error);
				span.show();
			},

			"hideError": function () {
				var span = jQuery(this.options.idContainer).find('.wQuotesError');
				if ($(span).is(':visible'))
					span.hide();
			},

			"getQuote": function (id) {
				var self = this;
				self.hideError();
				jQuery.post(
					yiiBaseURL + this.options.ajaxGetTableURL,
					{"idCat": this.options.idCurrentCat, "idsQuotes": [id]},
					function (data) {
						self.addRows(data);
					},
					"json"
				);
			},
			
			"addItemToPanel":function(id, el){
				var self = this;
				if(jQuery.inArray(id,self.options.oPanel.ids)==-1){
					if(jQuery(el).data('id').toString().indexOf("_search")+1){
						self.options.oPanel.addItemWithSave(self.options.quotesSearch[id]);
					}else{
						self.options.oPanel.addItemWithSave(self.options.quotes[id]);
					}
					
					jQuery(el).removeClass().toggleClass('wEditPanelRemove');
					jQuery(el).unbind("click");
					jQuery(el).bind('click', function () {
						self.removeItemFromPanel(id,this);
					});
				}
			},

			"removeItemFromPanel": function (id, el) {
				var self = this;
				self.options.oPanel.removeItemWithSave(id);
				
				jQuery(el).removeClass().toggleClass('wEditPanelAdd');
				jQuery(el).unbind("click");
				jQuery(el).bind('click', function () {
					self.addItemToPanel(id,this);
				});
			},

			"addRows": function (row, search) {
				var self = this;
				var table = jQuery(self.options.idContainer + " .quotesTable"), tr, new_tr, id_cat;
				if (search === true) {
					id_cat = "search";
				}

				for (var key1 in row) {
					if (search !== true) {
						id_cat = row[key1]['category_id'];
					}
					tr = table.find('.wCategory_' + id_cat);
					if (tr.length == 0 && search !== true) {
						var tr_count = table.find('.wNameCategory').length;
						self.addRowCategory(id_cat, row[key1]['category_name'], tr_count + 1);
						tr = table.find('.wCategory_' + id_cat);
					}
					var className = table.find('.wCategory_' + id_cat + '+tr:first');

					if (className.hasClass('odd'))
						className = "even";
					else
						className = "odd";

					new_tr = jQuery("<tr>", {
						"class": className + " wLineId" + key1 + (search === true ? "__search" : ''),
						"idSymbol": key1 + (search === true ? "__search" : ''),
						"data-id": "hideShow_" + jQuery(tr).data('iter')

					});
					var actionBtn;
					if (self.options.mode == "editPanel") {
						var sClass, sTitle;
						if (jQuery.inArray(row[key1]['id'], self.options.oPanel.options.ids) == -1) {
							sClass = 'wEditPanelAdd';
							sTitle = self.options.oPanel.options.ls.addItem;
							actionBtn = jQuery("<td>").append(jQuery("<div>", {
								"class": sClass,
								"title": sTitle,
								"data-id": row[key1]['id'] + (search === true ? "__search" : ''),
								"data-key": key1,
								click: function () {
									self.addItemToPanel(jQuery(this).data('key'),this);
								}
							}));
						} else {
							sTitle = self.options.oPanel.options.ls.deleteItem;
							sClass = 'wEditPanelRemove';
							actionBtn = jQuery("<td>").append(jQuery("<div>", {
								"class": sClass,
								"title": sTitle,
								"data-id": row[key1]['id'] + (search === true ? "__search" : ''),
								"data-key": key1,
								click: function () {
									self.removeItemFromPanel(jQuery(this).data('key'),this);
								}
							}));
						}
					} else {
						actionBtn = jQuery("<td>", {
							mouseout: function () {
								jQuery(this).find('div.wHideLine').hide()
							},
							mouseover: function () {
								jQuery(this).find('div.wHideLine').show()
							}
						}).append(jQuery("<div>", {
							"class": 'wHideLine',
							"data-id": row[key1]['id'] + (search === true ? "__search" : ''),
							click: function () {
								self.hideLine(jQuery(this).data('id'));
							}
						}));
					}
					new_tr.append(actionBtn);
					for (var key2 in self.options.columns) {
						if (row[key1][self.options.columns[key2]] == undefined)
							continue;

						var td = jQuery("<td>", {
							"class": "cell_" + self.options.columns[key2]
						});

						if (self.options.columns[key2] == 'name') {
							if (row[key1]['trend'] == "1") {
								td.append('<div class="wArrow wArrowGreen"></div>');
							} else if (row[key1]['trend'] == "-1") {
								td.append('<div class="wArrow wArrowRed"></div>');
							}
							td.append("<span class='wTextCellName' title='"+row[key1]['desc']+"'>" + (row[key1]['nalias']!=undefined && row[key1]['nalias']? row[key1]['nalias'] : row[key1]['name']) + "</span>");
						} else {
							td.append("<span class='wTextCell'>" + row[key1][self.options.columns[key2]] + "</span>")
						}

						new_tr.append(td);
					}
					if (search) {
						self.options.idsSearch.push(key1);
						self.options.searchQuotes[key1] = row[key1];
					} else {
						self.options.idsQuotes.push(key1);
						self.options.quotes[key1] = row[key1];
					}
					tr.after(new_tr);

					var trs = $(this.options.idContainer).find('.wNameCategory.wCategory_' + id_cat);
					if (trs) {
						var count = trs.data('count') * 1 + 1;
						trs.data('count', count);
					}
				}
			},

			"getCheckBoxes": function () {
				var self = this;
				self.hideError();
				if (self.options.ajax) {
					jQuery.post(yiiBaseURL + self.options.ajaxGetCheckBoxes, {
						"idCat": self.options.idCurrentCat
					}, function (data) {
						if (data.error) {
							self.setError(data.error)
						}
						jQuery(self.options.idContainer + ' #wCheckBoxes').html(data.html);
						self.options.defaultCheckIds = data.default;
						self.setClickCheckbox();
					}, "json");
					return false;
				}
			},

			"changeCategory": function (select) {
				var self = this;
				jQuery(self.options.idContainer + ' #loaderCategory').html('<img width="15px" height="15px" src="' + yiiBaseURL + '/assets-static/images/load.gif' + '">');
				if (self.options.idCurrentCat == 71) {
					window.location.href = window.location.href.slice(0, window.location.href.indexOf("?"));
				} else if ($(select).val() == 71) {
					window.location.href += "?m=editPanel";
					return;
				}
				self.options.idCurrentCat = $(select).val();

				self.getCheckBoxes();
				var subCatsHtml = '';
				if (self.options.idCurrentCat == self.options.idAllCats) {
					for (var key in self.options.idsCategories['categories']) {
						var li = '<li data-id="' + key + '" class="wSubCats_0">';
						li += '<a href="#">' + self.options.idsCategories['categories'][key] + '</a>';
						if (self.options.idsCategories['subcategories'][key] != undefined) {
							var subul = '<ul class="wSubCats">';
							for (var key2 in self.options.idsCategories['subcategories'][key]) {
								var subli = '<li data-id="' + key2 + '" class="wSubCats_1">';
								subli += '<a href="#">' + self.options.idsCategories['subcategories'][key][key2] + '</a>';
								if (self.options.idsCategories['subcategories'][key2] != undefined) {
									var subsubul = '<ul class="wSubCats">';
									for (var key3 in self.options.idsCategories['subcategories'][key2]) {
										var subsubli = '<li data-id="' + key3 + '" class="wSubCats_1">';
										subsubli += '<a href="#">' + self.options.idsCategories['subcategories'][key2][key3] + '</a>';
										subsubli += '</li>';
										subsubul += subsubli;
									}
									subsubul += "</ul>";
									subli += subsubul;
								}
								subli += '</li>';
								subul += subli;
							}
							subul += '</ul>';
							li += subul;
						}
						li += '</li>';
						subCatsHtml += li;
					}
				} else {
					if (self.options.idsCategories['subcategories'][self.options.idCurrentCat] != undefined) {
						for (var key in self.options.idsCategories['subcategories'][self.options.idCurrentCat]) {
							var li = '<li data-id="' + key + '" class="wSubCats_0">';
							li += '<a href="#">' + self.options.idsCategories['subcategories'][self.options.idCurrentCat][key] + '</a>';
							if (self.options.idsCategories['subcategories'][key] != undefined) {
								var subul = '<ul class="wSubCats">';
								for (var key2 in self.options.idsCategories['subcategories'][key]) {
									var subli = '<li data-id="' + key2 + '" class="wSubCats_1">';
									subli += '<a href="#">' + self.options.idsCategories['subcategories'][key][key2] + '</a>';
									subli += '</li>';
									subul += subli;
								}
								subul += '</ul>';
								li += subul;
							}
							li += '</li>';
							subCatsHtml += li;
						}
					}
				}
				jQuery(self.options.idContainer + ' #wSubCats').html(subCatsHtml);
				self.setClickCategory();
				self.setNewTable();
			}
		}
	})();
	return new core();
};

$(window).bind("load", function () {
	$(".custom-select-box").each(function () {
		var div = this;
		var ul = $("<ul>", {
			"class": "ul-custom-select"
		});
		var select = $(this).find("select");
		select.find('option').each(function () {
			var option = this;
			if ($(option).attr("selected"))
				$(div).children('.text-select').text($(option).text())
			var li = $("<li>", {
				"data-value": $(option).attr("value"),
				"data-selected": $(option).attr("selected"),
				text: $(option).text(),
				click: function (event) {
					$(div).children('.text-select').text($(this).text());
					$(ul).find("li[data-selected=selected]").removeAttr("data-selected");
					$(li).attr("data-selected", "selected");
					select.find('option').each(function () {
						if ($(this).val() == $(li).data("value")) {
							$(this).attr("selected", true);
						} else {
							$(this).attr("selected", false);
						}
					});
					select.change();
					$(ul).hide(200);
					event.preventDefault();
					event.stopPropagation();
				}
			});
			ul.append(li);
		});
		$(this).append(ul);
		$(this).click(function () {
			var ul = $(this).children('ul');
			if (ul.is(":visible"))
				ul.hide(200);
			else
				ul.show(200);
		});
	});
});