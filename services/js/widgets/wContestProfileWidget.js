var wContestProfileWidgetOpen;
!function( $ ) {
	wContestProfileWidgetOpen = function( options ) {
		var defLS = {
			lErrorAgreed: 'You did not agree with the terms of the competition!',
		};
		var defOption = {
			selector: '.wContestProfileWidget',
			ins: '',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.$wRegister = self.$ns.find( '.wRegister' );
				self.$wAgreed = self.$ns.find( '.wAgreed' );
				
				self.$wRegister.click( self.onClickRegister )
			},
			onClickRegister:function() {
				if( self.$wAgreed.length ) {
					var agreed = self.$wAgreed.prop( 'checked' );
					if( !agreed ) {
						alert( options.ls.lErrorAgreed );
						self.$wAgreed.closest( 'div' ).css({
							'border': 'solid red 2px',
							'padding': '5px',
						});
						return false;
					}
				}
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );