var wInformerConverterSettingsWidgetOpen;
!function( $ ) {
	wInformerConverterSettingsWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wInformerConverterSettingsWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			informerParams: {},
			informerUrl: '',
			loading:false,
			windowLoaded:true,
			changedDefaultColors: false,
			prevStyleListVal: false,
			prevFromCurrencyVal: false,
			prevToCurrencyVal: false,
			defaultColors: false,
			init:function() {
				self.$ns = $( options.selector );
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				$(window).load( self.setWindowLoaded );
				

				
				self.$styleIframeWrap = self.$ns.find( '.styleIframeWrap' );
				self.$informerColorsBox = self.$ns.find( '.informerColorsBox' );
				self.$previewInformerWrap = self.$ns.find( '.previewInformerWrap' );
				self.$informerCodeWrap = self.$ns.find( '.informerCodeWrap' );
				self.$informerColorsPreview = self.$ns.find( '.informerColorsPreview' );
				self.$stylesList = self.$ns.find( '.stylesList' );
				self.$fromCurrencyList = self.$ns.find( '.fromCurrencyList' );
				self.$toCurrencyList = self.$ns.find( '.toCurrencyList' );
				self.$defaultAmount = self.$ns.find( '.defaultAmount' );
				self.$restoreDefaults = self.$ns.find( '.restoreDefaults' );
				self.$fixedWidthBtn = self.$ns.find( '#fixedWidthBtn' );
				self.$sliderWidthBox = self.$ns.find( '.sliderWidthBox' );
				self.$wSliderWidthVal = self.$ns.find( '.sliderWidthVal' );
				self.$widthVal = self.$ns.find( '.widthVal' );
				
				self.initStylesList();
				self.initFromCurrencyList();
				self.initToCurrencyList();
				
				self.styleSelected();
				
				self.$defaultAmount.on('change', function() { self.changedSettings() });
				self.$restoreDefaults.click( self.restoreDefaultSettings );
				self.$fixedWidthBtn.click( self.changeWidthType );
				self.$widthVal.on('change', function() { self.changeWidth(); });
				self.$widthVal.keydown( self.filterInpWidth );
				self.$defaultAmount.keydown( self.filterInpWidth );
				
				self.initSlider();
				
				$('.colorInformer').click(function(){
					$(this).parent('.section_offset').find('.box2').slideToggle('slow');
				})
				

				var clipboard = new Clipboard('.copyCode');
				
				$(window).scroll( self.pageScrolled );
				
				self.$ns.find( '#hideGetInformerBtn:checkbox:checked' ).click( self.changedSettings );
				
			},
			pageScrolled:function(){
				clearTimeout(self.scrollTimeout);
				self.scrollTimeout = setTimeout(function(){
					var topWindowsOffset = self.$informerColorsPreview.offset().top - $(window).scrollTop() + self.$informerColorsPreview.outerHeight()*1.75;
					var bottomWindowsOffset = self.$informerColorsPreview.offset().top - $(window).scrollTop() - window.innerHeight - self.$informerColorsPreview.outerHeight()*1.25;

					var $visibleSection = self.findVisibleSection();

					if( $visibleSection ){
						if( topWindowsOffset < 0 ){
							$visibleSection.prev('section').after(self.$informerColorsPreview);
	
						}else if( bottomWindowsOffset > 0 ){
							$visibleSection.prev('section').after(self.$informerColorsPreview);
						}
					}
					
				},10);
			},
			findVisibleSection: function(){
				var el = false;
				self.$ns.find('>section').each(function( index ) {
					if( (index > 0) && ($(this).offset().top - $(window).scrollTop() > 0) ){
						el = $(this);
						return false;
					}
				});
				return el;
			},
			disable: function(){
				self.$ns.append( self.spinner );
			},
			enable: function(){
				self.$ns.find('.spinner-wrap').remove();
			},
			initStylesList: function(){
				self.prevStyleListVal = self.$stylesList.val();
				self.$stylesList.styler({
					onSelectClosed: function() {
						if( self.$stylesList.val() != self.prevStyleListVal ){
							self.prevStyleListVal = self.$stylesList.val();
							self.styleSelected();
						}
					}
				});
			},
			initFromCurrencyList: function(){
				self.prevFromCurrencyVal = self.$fromCurrencyList.val();
				self.$fromCurrencyList.styler({
					onSelectClosed: function() {
						if( self.$fromCurrencyList.val() != self.prevFromCurrencyVal ){
							self.prevFromCurrencyVal = self.$fromCurrencyList.val();
							self.changedSettings();
						}
					}
				});
			},
			initToCurrencyList: function(){
				self.prevToCurrencyVal = self.$toCurrencyList.val();
				self.$toCurrencyList.styler({
					onSelectClosed: function() {
						if( self.$toCurrencyList.val() != self.prevToCurrencyVal ){
							self.prevToCurrencyVal = self.$toCurrencyList.val();
							self.changedSettings();
						}
					}
				});
			},
			restoreDefaultSettings: function(){
				self.$informerColorsBox.html( self.defaultColors );
				if( self.windowLoaded ){
					self.initColorPicker();
				}else{
					$(window).load( self.initColorPicker );
				}
				self.$defaultAmount.val( options.convertFromVal );
				self.$fromCurrencyList.val( options.convertFrom );
				self.$toCurrencyList.val( options.convertTo );
				
				self.$fromCurrencyList.trigger('refresh');
				self.$toCurrencyList.trigger('refresh');
				self.$informerColorsPreview.html( '' );
				self.changedDefaultColors = false;
				self.changedSettings();
				return false;
			},
			
			
			
			styleSelected: function(){
				self.updateInformer();
				
				/*if( self.loading ) return false;
				self.loading = true;
				self.disable();
				
				self.informerParams = {};
				self.informerParams['st'] = self.$stylesList.val();
				self.informerParams['cat'] = options.catId;
				self.informerParams['w'] = 0;

				self.informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(self.informerParams);
				
				var frameCode = '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:100%" scrolling="no" src="' + self.informerUrl + '"></iframe>';
				*/

				self.$informerColorsPreview.html( '' );
				/*self.$styleIframeWrap.html( frameCode );*/
				
				self.$informerColorsBox.html('');
				self.changedDefaultColors = false;
				self.setDefaultStyleColors();
				
				/*self.$styleIframeWrap.find( 'iframe' ).load( self.correctIframeHeight );*/
								
			},
			correctIframeHeight:function() {
				var iframeHeight = this.contentWindow.document.body.scrollHeight + "px";
				this.style.height = iframeHeight;
				self.setDefaultStyleColors();
			},
			setDefaultStyleColors: function(){
				$.getJSON( options.getInformerColors, {st:self.$stylesList.val()}, function ( data ) {
				
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$informerColorsBox.html( data.data );
						self.defaultColors = data.data;
						
						if( self.defaultColors.indexOf('installLinkTextColor') == -1 ){
							self.$ns.find('.hideGetInformerBtnBox').slideUp('slow');
						}else{
							self.$ns.find('.hideGetInformerBtnBox').slideDown('slow');
						}
						
						if( self.windowLoaded ){
							self.initColorPicker();
						}else{
							$(window).load( self.initColorPicker );
						}
						if( data.width == '0' ){
							self.$fixedWidthBtn.prop( 'checked', false );
						}else{
							self.$fixedWidthBtn.prop( 'checked', false );
							self.$widthVal.val( data.width );
							self.$wSliderWidthVal.slider( "value", data.width );
						}
						self.changeWidthType(false);
					}
					self.changedSettings();
					self.loading = false;
					self.enable();
				});
			},
			
			
			
			
			setWindowLoaded: function(){
				self.windowLoaded = true;
			},
			
			
			
			initColorPicker: function(){
				if( self.$ns.find(".colorSelector").length){
					self.$ns.find(".colorSelector").each(function(){
						var $this = $(this);

						$this.css('background-color', $this.attr('data-bg') );
						$this.attr({'data-cpp-color': $this.attr('data-bg')});
						
						$this.closest('.change_informer_color_el').find('.col3 input').val( $this.attr('data-bg') );
						
						$this.colorpickerplus();
						
						$this.on('changeColor', function(e, color){					
							if(color!=null){
								$(this).css('background-color', color);
								$(this).closest('.change_informer_color_el').find('.col3 input').val( color );
							}
						});
						$this.on('hidePicker', function(e, color){					
							self.changedDefaultColors = true;
							self.showInformerColorsPreview();
							self.changedSettings();
						});
						
					});
				}
			},
			showInformerColorsPreview: function(){
				self.updateInformer();

				/*var informerColorsPreviewParams = {};
				informerColorsPreviewParams['st'] = self.$stylesList.val();
				informerColorsPreviewParams['colors'] = self.getColors();
				informerColorsPreviewParams['cat'] = options.catId;
				informerColorsPreviewParams['w'] = 0;
				var informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(informerColorsPreviewParams);
				
				var frameCode = '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:100%;margin-top:20px;" scrolling="no" src="' + informerUrl + '"></iframe>';
				
				self.$informerColorsPreview.html( frameCode );
				
				self.$informerColorsPreview.find( 'iframe' ).load( self.correctinformerColorsPreviewIframeHeight );*/
			},
			correctinformerColorsPreviewIframeHeight:function() {
				var iframeHeight = this.contentWindow.document.body.scrollHeight + "px";
				this.style.height = iframeHeight;
			},
			encodeQueryData:function( informerParams ){
				var ret = [];
				for ( var param in informerParams ){
					ret.push( encodeURIComponent( param ) + "=" + encodeURIComponent( informerParams[param] ) );
				}
				return ret.join("&");
			},
			changedSettings:function() {	
				self.disable();
				self.updateInformer();
			},
			updateInformer:function() {				
				self.informerParams = {};
				self.informerParams['st'] = self.$stylesList.val();
				self.informerParams['cat'] = options.catId;
				
				if( self.$ns.find( '#hideGetInformerBtn:checkbox:checked' ).length == 0 ){
					self.informerParams['showGetBtn'] = 1;
				}else{
					self.informerParams['showGetBtn'] = 0;
				}
				
				if( self.$ns.find( '#fixedWidthBtn:checkbox:checked' ).length == 0 ){
					self.informerParams['w'] = 0;
				}else{
					self.informerParams['w'] = self.$widthVal.val();
				}
				
				self.informerParams['from'] = self.$fromCurrencyList.val();
				self.informerParams['to'] = self.$toCurrencyList.val();
				self.informerParams['amount'] = self.$defaultAmount.val();
				var colorsSelected = self.getColors();
				if( colorsSelected ) self.informerParams['colors'] = colorsSelected;
			
				
				self.informerUrl = options.getInformerUrl + '?' + self.encodeQueryData(self.informerParams);
					
				var frameCode = '<iframe style="width:100%;border:0;overflow:hidden;background-color:transparent;height:100%" scrolling="no" src="' + self.informerUrl + '"></iframe>';
				
				self.$informerColorsPreview.html( frameCode )
				self.$informerColorsPreview.find( 'iframe' ).load( self.correctIframeHeightForPreview );

				return false;
			},
			correctIframeHeightForPreview:function( ) {	
				var iframeHeight = this.contentWindow.document.body.scrollHeight + "px";
				this.style.height = iframeHeight;
				if( self.informerUrl != '' ){
					if( self.informerParams['w'] == 0 ){
						var width = '100%';
					}else{
						var width = self.informerParams['w'] + 'px';
                    }
                    
                    self.$informerColorsPreview.css({ height: iframeHeight });

					self.$informerCodeWrap.val( '<iframe style="width:'+width+';border:0;overflow:hidden;background-color:transparent;height:'+iframeHeight+'" scrolling="no" src="' + self.informerUrl + '"></iframe>' );
				}
				self.enable();
			},

			getColors:function() {	
				if( !self.changedDefaultColors ) return false;
				var strVal = '';
				self.$informerColorsBox.find( 'input' ).each(function(index, element){
					var delimiter = '';
					var val = $(this).val();
					if( index > 0 ) delimiter = ',';
					if( val.charAt(0) === '#' ) val = val.substr(1);
					strVal = strVal + delimiter + $(this).attr('name') + '=' + val;
				});
				return strVal;	
			},

			initSlider:function() {	
				
				self.$wSliderWidthVal.slider({
					min : options.minWidth,
					max : options.maxWidth,
					step: 10,
					value: 320,
					stop: function( event, ui ) {
						self.changedSettings();
					},
					slide: function( event, ui ) {
						self.$widthVal.val( ui.value );
					},
				});
			},
			changeWidth: function(){
				var value = parseInt( self.$widthVal.val() );
				if( value < options.minWidth ){
					value = options.minWidth;
					self.$widthVal.val( value );
				}else if( value > options.maxWidth ){
					value = options.maxWidth;
					self.$widthVal.val( value );
				}
				self.$wSliderWidthVal.slider( "value", value );
				self.changedSettings();
			},
			filterInpWidth: function(e){
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
					// Allow: Ctrl+A
					(e.keyCode == 65 && e.ctrlKey === true) ||
					// Allow: Ctrl+C
					(e.keyCode == 67 && e.ctrlKey === true) ||
					// Allow: Ctrl+X
					(e.keyCode == 88 && e.ctrlKey === true) ||
					// Allow: home, end, left, right
					(e.keyCode >= 35 && e.keyCode <= 39)) {
					// let it happen, don't do anything
					return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			},
			changeWidthType: function( marker = true ){
				if( self.$ns.find( '#fixedWidthBtn:checkbox:checked' ).length == 0 ){
					self.$sliderWidthBox.slideUp('slow');
				}else{
					self.$sliderWidthBox.slideDown('slow');
				}
				if( marker ) self.changedSettings();
			}
			
			

			
			
		};
		self.init();
		return self;
	}
}( window.jQuery );