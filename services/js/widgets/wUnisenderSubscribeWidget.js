var wUnisenderSubscribeWidgetOpen;
!function( $ ) {
	wUnisenderSubscribeWidgetOpen = function( options ) {
		var defLS = {
		};
		var defOption = {
			ns: nsActionView,
			ins: '',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
                self.$ns = $( options.selector );

                self.$blockSuccess = self.$ns.find('.ml-block-success');
                self.$form = self.$ns.find('form');
                self.$email = self.$form.find('input');
                self.$email.blur( function(){ self.checkEmail( $(this) ); } );

                self.$form.submit(self.submitForm);
            },
            submitForm: function() {
                if (self.checkEmail( self.$email )) {
                    self.disable();
                    var data = {
                        email: self.$email.val(),
                        listId: options.listId
                    };
                    $.getJSON( options.ajaxSubscribeURL, data, function ( data ) {
                        if (data.error) {
                            alert( data.error );
                        } else {
                            self.$blockSuccess.show();
                            self.$form.hide();
                        }
                        self.enable();
                    });
                }
                return false;
            },
            checkEmail: function( elem ){

				if( elem.val() != '' ) {
					var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
					if( pattern.test( elem.val() ) ){
						elem.closest('div').removeClass('ml-error');
						return true;
					} else {
						elem.closest('div').addClass('ml-error');
						return false;
					}
				} else {
					elem.closest('div').addClass('ml-error');
					return false;
				}
			},
            disable:function() {
				self.$ns.append( self.spinner );
			},
			enable:function() {
				self.$ns.find('.spinner-wrap').remove();
			},

		};
		self.init();
		return self;
	}
}( window.jQuery );