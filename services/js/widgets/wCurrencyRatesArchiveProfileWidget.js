var wCurrencyRatesArchiveProfileWidgetOpen;
!function( $ ) {
	wCurrencyRatesArchiveProfileWidgetOpen = function( options ) {
		var defOption = {
			ajax: false,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			loading: false,
			init:function() {
				self.saveViews();
				
				self.$ns = $( options.selector );
				self.$wChangeCurrencyPageSelect = self.$ns.find( 'select.changeCurrencyPage' );
				
				self.$wSpinner = self.$ns.find( '.spinner-wrap' );
				self.$wPeriodLinks = self.$ns.find( 'ul.changePeriodLinks li a' );
				self.$wPeriodLinksLI = self.$ns.find( 'ul.changePeriodLinks li' );

				self.initFormStyler();
				
				self.$chartData = self.parseCSV( options.chartData );
				
				self.$chart = self.makeChart(options.lang);
				
				
				
				self.$chart.addListener("rendered", self.enable);
				self.$chart.addListener("dataUpdated", self.enable);
				self.$chart.addListener("dataUpdated", self.zoomToAll);
				
				self.$wPeriodLinks.click( self.changePeriod );

			},
			zoomToAll: function(){
				self.$chart.zoomOut();
			},
			saveViews:function(){
				$.post( options.saveViewsUrl, {"id": options.idCurrency}, function ( data ) {

				} );
			},
			sendImage:function( base64 ) {
				if( self.loading ) return false;
				self.loading = true;
				$.post( options.saveImageUrl, {"id": options.idCurrency, "img": base64}, function ( data ) {

					self.loading = false;
				} );
			},
			
			initFormStyler:function(){
				var $forms = self.$ns.find('.form_styler');
				if( $forms.length ){
					$forms.styler({
						selectSearch: true,
						onSelectClosed: function() {
							var $this = $(this);
							if( $this.hasClass('changeCurrencyPage') ){
								var newUrl = self.$wChangeCurrencyPageSelect.val();
								if( document.location.href.indexOf(newUrl) == -1 ) document.location.assign( newUrl );
							}
						}
					});
				}
			},
			changePeriod:function(){
				var $this = $(this);
				if( $this.closest('li').hasClass('active') ) return false;
				
				self.disable();
				var radius = $this.attr('data-radius');
				
				$.getJSON( options.loadChartDataUrl, {radius:radius, id:options.idCurrency, left:options.leftDate, right:options.rightDate, type:options.dataType }, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}else{
						self.$chartData = self.parseCSV( data.data );
						/*console.log( self.$chartData );*/
						self.$chart.dataSets[0].dataProvider = self.$chartData;
						self.$chart.validateData();			
						self.$wPeriodLinksLI.removeClass('active');
						$this.closest('li').addClass('active');
					}
				});

				return false;
			},
			makeChart:function( lang) {
				
				return AmCharts.makeChart( options.chartSelector, {
					"type": "stock",
					"theme": 'none',
					"zoomOutText": '',
					"autoMargins": false,
					"dataDateFormat": "YYYY-MM-DD",
					"useUTC": true,
					
					"dataSets": [{
						"color": "#2c88cb",
						"fieldMappings": [ {
							"fromField": "value",
							"toField": "value"
						}],
						"dataProvider":self.$chartData,
						"categoryField": "date",
						
						"stockEvents": options.chartEvents
						
					}],
  
					"panels": [{
						"stockGraphs": [ {
							"useNegativeColorIfDown": true,
							"title": options.label,
							"negativeLineColor": "#ff2d2e",
							"lineColor": "#2c88cb",
							"lineAlpha": "1",
							"lineThickness": 2,
							"fillColors": "#2c88cb",
							"fillAlphas": 0,
							"valueField": "value",
							"type": "line",
							"id": "g1",
							"balloonText": options.ls.lPrice + ": [[value]]",
							"balloonColor": "#2c88cb",
							"useLineColorForBulletBorder": true,	
						}],
						"stockLegend": false,
						
						
						
						"categoryAxis": {
							"guides": [{
								"date": options.leftDate,
								"toDate": options.rightDate,
								"lineAlpha": 1,
								"fillAlpha": 0.1,
								"fillColor": "#2c88cb",
								"lineColor": "#2c88cb",
								"expand": true
							}]
						},
						
					}],
					"panelsSettings":{
						"plotAreaBorderAlpha": 1,
						"plotAreaBorderColor": '#e6e6e6',
						"marginRight": 10,
						"marginLeft": 10,
						"marginBottom": 0,
						"marginTop": 30,
						'usePrefixes': true,
					},
					"chartScrollbarSettings": {
						"graph": 'g1',
						"scrollbarHeight": 54,
						"backgroundAlpha": 1,
						"backgroundColor": '#fafafa',
						"selectedBackgroundAlpha": 1,
						"selectedBackgroundColor": '#cdcdcd',
						"graphFillAlpha": 1,
						"graphLineAlpha": 0.5,
						"selectedGraphFillAlpha": 1,
						"selectedGraphLineAlpha": 1,
						"autoGridCount": true,
						"color": '#fff',
						"dragIconWidth": 25
					},
					"chartCursorSettings": {
						"cursorAlpha": 1,
						"pan": true,
						"categoryBalloonDateFormats": [{period:"DD",format:"MMMM DD YYYY"}],
						"categoryBalloonColor": "#ff2d2e",
						"cursorColor": "#ff2d2e",
						"valueBalloonsEnabled": true,
						"graphBulletSize": 1,
						"valueLineAlpha": 0.5
					},
					"valueAxesSettings": [{
						"position": "left",
						"id": 'v1',
						"axisAlpha": 0,
						"inside": true,
					}],
					"language": lang,	
					"categoryAxesSettings": {
						"dashLength": 5,
						"minorGridEnabled": true,
						"position": 'top',
						"labelOffset": 4,
						"axisAlpha": 0,
						"gridColor": '#e6e6e6',
						"gridAlpha": 1,
						"maxSeries": 100000000,
						"minPeriod": "DD",
						"parseDates": true,
						"equalSpacing": true,
					},
					"export": {
						"enabled": true,
						"libs": {
							"path": options.exportLibs
						},
						"menu": [{
						  "format": "UNDEFINED",
						  "label": "",
						  "class": "export-main",
						  "menu": ["JPG", "PNG"]
						}],
					},
				});
			},
			disable:function(){
				self.$wSpinner.removeClass('hide');
			},
			enable:function(){
				self.$wSpinner.addClass('hide');
			},
			parseCSV:function(data) {
				var returnArr = [];
				if(!data) return returnArr;
				data = data.replace (/\r\n/g, "\n");
				data = data.replace (/\r/g, "\n");
				var rows = data.split("\n");
				for (var i = 0; i < rows.length; i++) {
					if (rows[i]) {
						var column = rows[i].split(";");
						
						if( column[1].indexOf('E') != -1 ){
							column[1] = parseFloat(column[1]);
							column[1] = column[1] + '';
						}

						var splitedStr = column[1].split('.');
						
						if( splitedStr.length > 1 ){
							if( splitedStr[1].length > 4 ){
								var lastZeroIndex = splitedStr[1].search(/[1-9]/);
								if( lastZeroIndex == -1 ){
									var dateValue = splitedStr[0] + '.' + splitedStr[1].substr(0, 4);
								}else{
									var dateValue = splitedStr[0] + '.' + splitedStr[1].substr(0, parseInt(lastZeroIndex) + 4);
								}
								
							}else{
								var dateValue = column[1];
							}
						}else{
							var dateValue = column[1];
						}
						
						var dataObject = {
							date: column[0],
							value: dateValue,
						};
						returnArr.push(dataObject);
					}
				}
				return returnArr;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );