var wAdminJournalWebmasterFormWidgetOpen;
!function( $ ) {
	wAdminJournalWebmasterFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New journal',
			lEdit: 'Editor journal',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminJournalWebmasterFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/journal/iframeWebmasterAdd',
			ajaxDeleteURL: '/admin/journal/ajaxWebmasterDelete',
			ajaxLoadURL: '/admin/journal/ajaxWebmasterLoad',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			inner: [],
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
                console.log(self.$form);
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				self.$tabsInner = self.$ns.find( options.ins+'.wTabsInner' );
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputNumber = self.$ns.find( options.ins+'.wNumber' );
				self.$inputDate = self.$ns.find( options.ins+'.wDate' );
				
				self.$textareaDescription = self.$ns.find( options.ins+'.wDescription' );
													
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				self.$wHeader = self.$ns.find( options.ins+'.wHeader' );
				self.$wContent = self.$ns.find( options.ins+'.wContent' );
				
				self.$bAddInner = self.$ns.find( options.ins+'.wAddInner' );
				self.$tabInner = self.$ns.find( options.ins+'.wTabInner' );
				self.$wInner = self.$ns.find( options.ins+'.wInner' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				self.$form.on( 'submit', self.onSubmit );
				self.$bAddInner.click( self.addCurrentInner );
				
				self.$tabInner.on( "click", ".wDelete", self.deleteSpecificInner );
				
				/*self.$textareaDescription.ckeditor({
					language: yiiLanguage,
				});*/
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputNumber.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			addInnerRow:function( header ) {
				var $row = self.$tabInner.find( '.wTpl' ).clone();
				$row.removeClass( 'wTpl' ).show();
				$row.find( '.wTDName' ).html( header );
				self.$tabInner.find( 'tbody' ).append( $row );
			},
			addInner:function( headers, contents ) {
				self.inner.push( { headers: headers, contents: contents } );
				self.$wInner.val( JSON.stringify( self.inner ));
				self.addInnerRow( headers[ options.idLanguage ] );
			},
			addCurrentInner:function() {
				var headers = {};
				var contents = {};
				
				var emptyHeader = true;
				self.$wHeader.each( function() {
					var match = /w(\d+)/.exec( this.className );
					var idLanguage = match ? match[1] : null;
					if( idLanguage ) { 
						headers[ idLanguage ] = $(this).val();
						if( idLanguage == options.idLanguage && headers[ idLanguage ]) emptyHeader = false;
					}
				});
				
				self.$wContent.each( function() {
					var match = /w(\d+)/.exec( this.className );
					var idLanguage = match ? match[1] : null;
					if( idLanguage ) { 
						contents[ idLanguage ] = $(this).val();
					}
				});
				
				if( emptyHeader ) return false;
				
				self.addInner( headers, contents );
				self.$wHeader.val( '' );
				self.$wContent.val( '' );

				self.$tabsInner.find( 'a:first' ).tab( 'show' );
				
				return false;
			},
			deleteInner:function( index ) {
				var tempInner = self.inner;
				self.inner = [];
				for( var i=0; i<tempInner.length; i++ ) {
					if( i == index ) continue;
					self.inner.push( tempInner[i]);
				}
				
				self.$wInner.val( JSON.stringify( self.inner ));
				self.$tabInner.find( 'tr:not(.wTpl):eq('+index+')' ).remove();
			},
			deleteSpecificInner:function() {
				var $row = $(this).closest( "tr" );
				var index = self.$tabInner.find( 'tr:not(.wTpl)' ).index( $row );
				self.deleteInner( index );
				return false;
			},
			clearInner:function() {
				self.$tabInner.find( 'tr:not(.wTpl)' ).remove();
				self.inner = [];
				self.$wInner.val( JSON.stringify( self.inner ));
			},
			load:function( model ) {
				self.$inputID.val( model.idWebmaster );
                var $wDescription = self.$ns.find( options.ins+'.wDescription' );
                $wDescription.val( model.description);
                var $wUrl = self.$ns.find( options.ins+'.wUrl' );
                $wUrl.val( model.url);
				//self.$inputNumber.val( model.number );
				//self.$inputDate.val( model.date );
				
				/*for( var idLanguage in model.wideIDs ) {
					var $wWideID = self.$ns.find( options.ins+'.wWideID.w'+idLanguage );
					$wWideID.val( model.wideIDs[idLanguage] );
				}
				for( var idLanguage in model.covers ) {
					var $wCoverHref = self.$ns.find( options.ins+'.wCoverHref.w'+idLanguage );
					$wCoverHref.attr( 'href', model.covers[idLanguage] );
					$wCoverHref.html( commonLib.baseName( model.covers[idLanguage]));
					$wCoverHref.show();
				}
				
				for( var idLanguage in model.names ) {
					var $wName = self.$ns.find( options.ins+'.wName.w'+idLanguage );
					$wName.val( model.names[idLanguage] );
				}
				
				for( var idLanguage in model.digests ) {
					var $wDigests = self.$ns.find( options.ins+'.wDigests.w'+idLanguage );
					$wDigests.val( model.digests[idLanguage] );
				}
				
				for( var idLanguage in model.countPageses ) {
					var $wCountPageses = self.$ns.find( options.ins+'.wCountPageses.w'+idLanguage );
					$wCountPageses.val( model.countPageses[idLanguage] );
				}
				
				for( var idLanguage in model.countDownloadses ) {
					var $wCountDownloadses = self.$ns.find( options.ins+'.wCountDownloadses.w'+idLanguage );
					$wCountDownloadses.val( model.countDownloadses[idLanguage] );
				}
				
				for( var idLanguage in model.journals ) {
					var $wJournalHref = self.$ns.find( options.ins+'.wJournalHref.w'+idLanguage );
					$wJournalHref.attr( 'href', model.journals[idLanguage] );
					$wJournalHref.html( commonLib.baseName( model.journals[idLanguage]));
					$wJournalHref.show();
				}
				
				for( var idLanguage in model.frees ) {
					var $wFree = self.$ns.find( options.ins+'.wFree.w'+idLanguage );
					$wFree.prop( 'checked', model.frees[ idLanguage ] == '1' );
				}
				for( var idLanguage in model.prices ) {
					var $wPrice = self.$ns.find( options.ins+'.wPrice.w'+idLanguage );
					$wPrice.val( model.prices[idLanguage] );
				}
				for( var idLanguage in model.descriptions ) {
					
				}

                var $wStatus = self.$ns.find( options.ins+'.wStatus.w0' );
                $wStatus.prop( 'checked', model.status == '1' );

                var $wPush_notification = self.$ns.find( options.ins+'.wPush_notification.w0' );
                $wPush_notification.prop( 'checked', model.push_notification == '1' );
				
				for( i in model.inner ) {
					self.addInner( model.inner[i].headers, model.inner[i].contents );
				}*/
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'], input[type='file'], input[type='textarea'], input[type='date']" ).val( '' );
				var $wCoverHref = self.$ns.find( options.ins+'.wCoverHref' );
				$wCoverHref.hide();
				var $wJournalHref = self.$ns.find( options.ins+'.wJournalHref' );
				$wJournalHref.hide();
				self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );
				
				self.clearInner();
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					options.ls.lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.newRecord = !self.$inputID.val();
					
					self.$form[0].action = yiiBaseURL+options.ajaxSubmitURL;
					self.$form[0].target = 'iframeForJournal';
					return true;
				}
			},
			onSubmit:function() {
				self.disable();
				self.loading = true;
				return true;
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );