var wAdminEATradeAccountFormWidgetOpen;
!function( $ ) {
	wAdminEATradeAccountFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New trade account',
			lEdit: 'Editor trade account',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminEATradeAccountFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/eaTradeAccount/ajaxAdd',
			ajaxDeleteURL: '/admin/eaTradeAccount/ajaxDelete',
			ajaxLoadURL: '/admin/eaTradeAccount/ajaxLoad',
			ajaxLoadVersionsURL: '/admin/eaStatement/ajaxLoadVersions',
			ajaxLoadStatementsURL: '/admin/eaTradeAccount/ajaxLoadStatements',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$selectIDEA = self.$ns.find( options.ins+'.wIDEA' );
				self.$selectIDVersion = self.$ns.find( options.ins+'.wIDVersion' );
				self.$selectIDStatement = self.$ns.find( options.ins+'.wIDStatement' );
				self.$selectIDTradeAccount = self.$ns.find( options.ins+'.wIDTradeAccount' );
				self.$selectIDEAMonitoringServer = self.$ns.find( options.ins+'.wIDEAMonitoringServer' );
				self.$selectIDBroker = self.$ns.find( options.ins+'.wIDBroker' );
				self.$inputPlaneDrow = self.$ns.find( options.ins+'.wPlaneDrow' );
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				
				self.$selectIDEA.change( self.onChangeIDEA );
				self.$selectIDVersion.change( self.onChangeIDVersion );
				
				self.loadVersions();
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						//self.$inputName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			onChangeIDEA:function() {
				self.loadVersions();
			},
			onChangeIDVersion:function() {
				self.loadStatements();
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$selectIDEA.val( model.idEA );
				self.loadVersions( model.idVersion, model.idStatement );
				self.$selectIDTradeAccount.val( model.idTradeAccount );
				self.$selectIDEAMonitoringServer.val( model.idEAMonitoringServer );
				self.$inputPlaneDrow.val( model.planeDrow );
				self.$selectIDBroker.val( model.idBroker );
			},
			loadVersions:function( idVersionSelected, idStatementSelected ) {
				var idEA = self.$selectIDEA.val();
				if( idEA ) {
					var storedValue = self.$selectIDVersion.val();
					self.$selectIDVersion.html( '<option>'+options.ls.lLoading+'</option>' );
					self.$selectIDVersion.attr( 'disabled', 'disabled' );
					$.getJSON( yiiBaseURL+options.ajaxLoadVersionsURL, {idEA:idEA}, function ( data ) {
						self.$selectIDVersion.removeAttr( 'disabled' );
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.$selectIDVersion.html( '' );
							if( data.versions ) {
								self.$selectIDVersion.removeClass( 'error' );
								$.each( data.versions, function( id, name ) {
									var $option = $( '<option value="'+id+'">'+name+'</option>' );
									self.$selectIDVersion.append( $option );
								});
								if( idVersionSelected ) {
									self.$selectIDVersion.val( idVersionSelected );
								}
								else if( storedValue ) {
									self.$selectIDVersion.val( storedValue );
								}
								self.loadStatements( idStatementSelected );
							}
						}
					});
				}
				else{
					self.$selectIDVersion.find( 'option' ).remove();
				}
			},
			loadStatements:function( idStatementSelected ) {
				var idVersion = self.$selectIDVersion.val();
				if( idVersion ) {
					var storedValue = self.$selectIDStatement.val();
					self.$selectIDStatement.html( '<option>'+options.ls.lLoading+'</option>' );
					self.$selectIDStatement.attr( 'disabled', 'disabled' );
					$.getJSON( yiiBaseURL+options.ajaxLoadStatementsURL, {idVersion:idVersion}, function ( data ) {
						self.$selectIDStatement.removeAttr( 'disabled' );
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.$selectIDStatement.html( '' );
							if( data.statements ) {
								self.$selectIDStatement.removeClass( 'error' );
								$.each( data.statements, function( id, name ) {
									var $option = $( '<option value="'+id+'">'+name+'</option>' );
									self.$selectIDStatement.append( $option );
								});
								if( idStatementSelected ) {
									self.$selectIDStatement.val( idStatementSelected );
								}
								else if( storedValue ) {
									self.$selectIDStatement.val( storedValue );
								}
							}
						}
					});
				}
				else{
					self.$selectIDStatement.find( 'option' ).remove();
				}
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$title.html( options.ls.lNew );
					self.clear();
					self.loadVersions();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text']" ).val( '' );
				self.$selectIDEA.val( '' );
				self.$selectIDTradeAccount.val( '' );
				self.$selectIDEAMonitoringServer.val( '' );
				self.$selectIDBroker.val( '' );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.hide();
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );