var wUserPasswordFormWidgetOpen;
!function( $ ) {
	wUserPasswordFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lReseted: 'Reseted',
			lError: 'Error!',
		};
		var defOption = {
			ins: '',
			selector: '.wUserPasswordFormWidget',
			ajax: false,
			ls: defLS,
			delayTooltips: 1000,
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				
				self.$bSubmit = self.$ns.find( '.wSubmit' );
				
				self.$bSubmit.click( self.submit );
			},
			disable: function() {
				self.$ns.append( self.spinner );
			},
			enable: function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				
				if( options.ajax ) {
					var lSubmit = self.$bSubmit.text();
					
					self.$bSubmit.text( options.ls.lSaving );
					self.$ns.find( '.form-error' ).text('');

					self.loading = true;
					$.post( options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.text( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else if( data.errors ) {
							data.errors.reverse();
							$.each( data.errors, function ( i, el ) {
								var $errorField = self.$form.find( '*[name="'+el.field+'"]' );
								var $label = $errorField.closest( 'label' );
								var $errorTextContainer = $label.find( '.form-error' );
								$errorTextContainer.text(el.error);
								$errorField.focus();
							});
						}
						else{
							$.each( self.onEdit, function() { this( data.id ); });
							self.$form[0].reset();
							self.$bSubmit.text( options.ls.lPasswordSaved );
							setTimeout( function() { self.$bSubmit.text( options.ls.lSave ); } , 2000);

						}
					}, "json" );
					return false;
				}
			},
			onEdit: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );