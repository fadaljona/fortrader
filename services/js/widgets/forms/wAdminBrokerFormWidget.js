var wAdminBrokerFormWidgetOpen;
!function( $ ) {
	wAdminBrokerFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New broker',
			lEdit: 'Editor broker',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminBrokerFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/broker/iframeAdd',
			ajaxDeleteURL: '/admin/broker/ajaxDelete',
			ajaxLoadURL: '/admin/broker/ajaxLoad',
			imageUriPath: '/services/uploads/brokers',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				self.$inputID = self.$ns.find( options.ins+'.wID' );
                self.$inputShowInRating = self.$ns.find( options.ins+'.wShowInRating' );
                self.$toCryptoCurrencies = self.$ns.find( options.ins+'.wtoCryptoCurrencies' );
				
				self.$inputEarlyClosure = self.$ns.find( options.ins+'.wEarlyClosure' );
				self.$inputTradeOnWeekends = self.$ns.find( options.ins+'.wTradeOnWeekends' );
				self.$inputBinnaryDemoAccount = self.$ns.find( options.ins+'.wBinnaryDemoAccount' );
				
				self.$inputSlug = self.$ns.find( options.ins+'.wSlug' );
				self.$inputShowOnHome = self.$ns.find( options.ins+'.wShowOnHome' );
				self.$inputHomeImage = self.$ns.find( options.ins+'.wHomeImage' );
				self.$homeImageToAdmin = self.$ns.find( options.ins+'.wHomeImageToAdmin' );
				self.$nameImageToAdmin = self.$ns.find( options.ins+'.wNameImageToAdmin' );
				self.$inputOfficialName = self.$ns.find( options.ins+'.wOfficialName' );
				self.$selectYear = self.$ns.find( options.ins+'.wYear' );
				self.$selectIDCountry = self.$ns.find( options.ins+'.wIDCountry' );
				self.$selectIDSupportUsers = self.$ns.find( options.ins+'.wIDSupportUsers' );
				
				self.$inputMinDeposit = self.$ns.find( options.ins+'.wMinDeposit' );
				self.$inputMaxLeverage = self.$ns.find( options.ins+'.wMaxLeverage' );
				self.$inputConditions = self.$ns.find( options.ins+'.wConditions' );
				self.$inputTerms = self.$ns.find( options.ins+'.wTerms' );
				self.$inputSite = self.$ns.find( options.ins+'.wSite' );
				self.$inputOpenAccount = self.$ns.find( options.ins+'.wOpenAccount' );
				self.$inputOpenDemoAccount = self.$ns.find( options.ins+'.wOpenDemoAccount' );
				self.$inputContract = self.$ns.find( options.ins+'.wContract' );
									
				self.$accordionConditions = self.$ns.find( '#accordionConditions' );
									
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputOfficialName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
                self.$inputShowInRating.prop( 'checked', model.showInRating == '1' );
                self.$toCryptoCurrencies.prop( 'checked', model.toCryptoCurrencies == '1' );
				self.$inputShowOnHome.prop( 'checked', model.showOnHome == '1' );
				self.$inputSlug.val( model.slug );
				
				self.$inputEarlyClosure.prop( 'checked', model.earlyClosure == '1' );
				self.$inputTradeOnWeekends.prop( 'checked', model.tradeOnWeekends == '1' );
				self.$inputBinnaryDemoAccount.prop( 'checked', model.binnaryDemoAccount == '1' );
				
				
				for( var idLanguage in model.officialNames ) {
					var $wOfficialName = self.$ns.find( options.ins+'.wOfficialName.w'+idLanguage );
					$wOfficialName.val( model.officialNames[idLanguage] );
				}
				for( var idLanguage in model.brandNames ) {
					var $wBrandName = self.$ns.find( options.ins+'.wBrandName.w'+idLanguage );
					$wBrandName.val( model.brandNames[idLanguage] );
				}
				for( var idLanguage in model.shortNames ) {
					var $wShortName = self.$ns.find( options.ins+'.wShortName.w'+idLanguage );
					$wShortName.val( model.shortNames[idLanguage] );
				}
				
				
				
				for( var idLanguage in model.metaTitles ) {
					var $wMetaTitle = self.$ns.find( options.ins+'.wMetaTitle.w'+idLanguage );
					$wMetaTitle.val( model.metaTitles[idLanguage] );
				}
				for( var idLanguage in model.metaDescs ) {
					var $wMetaDesc = self.$ns.find( options.ins+'.wMetaDesc.w'+idLanguage );
					$wMetaDesc.val( model.metaDescs[idLanguage] );
				}
				for( var idLanguage in model.metaKeyss ) {
					var $wMetaKeys = self.$ns.find( options.ins+'.wMetaKeys.w'+idLanguage );
					$wMetaKeys.val( model.metaKeyss[idLanguage] );
				}
				
				for( var idLanguage in model.shortDesc ) {
					self.$ns.find( options.ins+'.wshortDesc.w'+idLanguage ).val( model.shortDesc[idLanguage] );
				}
				for( var idLanguage in model.fullDesc ) {
					self.$ns.find( options.ins+'.wfullDesc.w'+idLanguage ).val( model.fullDesc[idLanguage] );
				}
				
				for( var idLanguage in model.maxProfits ) {
					var $wMaxProfit = self.$ns.find( options.ins+'.wMaxProfit.w'+idLanguage );
					$wMaxProfit.val( model.maxProfits[idLanguage] );
				}
				
				
				self.$selectYear.val( model.year );
				self.$selectIDCountry.val( model.idCountry );
				self.$selectIDSupportUsers.val( model.idSupportUser );
				
				$.each( model.idsRegulators, function () {
					self.$ns.find( '.wIDsRegulators[value="'+this+'"]' ).prop( 'checked', true );
				});
				
				self.$inputMinDeposit.val( model.minDeposit );
				self.$inputMaxLeverage.val( model.maxLeverage );
				
				for( var idLanguage in model.AMs ) {
					var $wAM = self.$ns.find( options.ins+'.wAM.w'+idLanguage );
					$wAM.val( model.AMs[idLanguage] );
				}
				
				for( var idLanguage in model.demoAccounts ) {
					var $wDemoAccount = self.$ns.find( options.ins+'.wDemoAccount.w'+idLanguage );
					$wDemoAccount.val( model.demoAccounts[idLanguage] );
				}
				
				$.each( model.idsTradePlatforms, function () {
					self.$ns.find( '.wIDsTradePlatforms[value="'+this+'"]' ).prop( 'checked', true );
				});
				
				$.each( model.idsIOs, function () {
					self.$ns.find( '.wIDsIOs[value="'+this+'"]' ).prop( 'checked', true );
				});
				
				$.each( model.idsInstruments, function () {
					self.$ns.find( '.wIDsInstruments[value="'+this+'"]' ).prop( 'checked', true );
				});
				
				$.each( model.idsBinaryAssets, function () {
					self.$ns.find( '.wIDsBinaryAssets[value="'+this+'"]' ).prop( 'checked', true );
				});
				$.each( model.idsBinaryTypes, function () {
					self.$ns.find( '.wIDsBinaryTypes[value="'+this+'"]' ).prop( 'checked', true );
				});
				
				self.$inputConditions.val( model.conditions );
				self.$inputTerms.val( model.terms );
				self.$inputSite.val( model.site );
				self.$inputOpenAccount.val( model.openAccount );
				self.$inputOpenDemoAccount.val( model.openDemoAccount );
				self.$inputContract.val( model.contract );
				
				if( model.homeImage ){
					self.$homeImageToAdmin.attr({src:options.imageUriPath+'/'+model.homeImage});
					self.$homeImageToAdmin.removeClass('hide');
				}else{
					self.$homeImageToAdmin.addClass('hide');
				}
				if( model.nameImage ){
					self.$nameImageToAdmin.attr({src:options.imageUriPath+'/'+model.nameImage});
					self.$nameImageToAdmin.removeClass('hide');
				}else{
					self.$nameImageToAdmin.addClass('hide');
				}
				
			},
			accordionsReset:function() {
				self.$accordionConditions.find( '.collapse' ).removeClass( 'in' ).attr( 'style', '' );
				self.$accordionConditions.find( '.collapse:first' ).addClass( 'in' );
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.accordionsReset();
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.accordionsReset();
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'], input[type='file']" ).val( '' );
				self.$selectYear.val('');
				self.$selectIDCountry.val('');
				self.$selectIDSupportUsers.val('');
				self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );
				self.$inputShowInRating.prop( 'checked', true );
				self.$inputShowOnHome.prop( 'checked', false );	
				
				self.$inputEarlyClosure.prop( 'checked', false );
				self.$inputTradeOnWeekends.prop( 'checked', false );
				self.$inputBinnaryDemoAccount.prop( 'checked', false );
				
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					options.ls.lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					self.newRecord = !self.$inputID.val();
					
					self.$form[0].action = yiiBaseURL+options.ajaxSubmitURL;
					self.$form[0].target = 'iframeForBroker';
					self.$form[0].submit();
					
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );