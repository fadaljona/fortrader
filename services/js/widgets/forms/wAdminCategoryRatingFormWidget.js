var wAdminCategoryRatingFormWidgetOpen;
!function( $ ) {
	wAdminCategoryRatingFormWidgetOpen = function( options ) {
		var defOption = {
			ins: '',
			selector: '.wAdminCategoryRatingFormWidget',
			ajax: false,
			hidden: false,
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 10,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			state: 'showAdd',
			loading: false,
			
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );	
				self.$slug = self.$ns.find( options.ins+'.wslug' );	
				self.$catId = self.$ns.find( options.ins+'.wcatId' );	
									
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				
			
			
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$slug.val( model.slug );
				self.$catId.val( model.catId );
				if( model.ogImageFile ) self.$ns.find( options.ins+'.imageWrap' ).html( '<img width="210" src="' + model.ogImageFile + '" />' );
				
				for( var idLanguage in model.title ) self.$ns.find( options.ins+'.wtitle.w'+idLanguage ).val( model.title[idLanguage] );	
				for( var idLanguage in model.metaTitle ) self.$ns.find( options.ins+'.wmetaTitle.w'+idLanguage ).val( model.metaTitle[idLanguage] );
				for( var idLanguage in model.metaDesc ) self.$ns.find( options.ins+'.wmetaDesc.w'+idLanguage ).val( model.metaDesc[idLanguage] );
				for( var idLanguage in model.metaKeys ) self.$ns.find( options.ins+'.wmetaKeys.w'+idLanguage ).val( model.metaKeys[idLanguage] );
				for( var idLanguage in model.shortDesc ) self.$ns.find( options.ins+'.wshortDesc.w'+idLanguage ).val( model.shortDesc[idLanguage] );
				for( var idLanguage in model.fullDesc ) self.$ns.find( options.ins+'.wfullDesc.w'+idLanguage ).val( model.fullDesc[idLanguage] );

			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';

					
					return true;
				}
			},
		
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( options.ajaxLoadURL, {id:id, formModelName: options.formModelName}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'],input[type='file']" ).val( '' );
				self.$form.find( "textarea" ).val( '' );
				self.$ns.find( options.ins+'.ogImageWrap' ).html( '' );
				self.$ns.find( options.ins+'.nameImageWrap' ).html( '' );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				
				self.newRecord = !self.$inputID.val(); 
				
				self.$form[0].action = options.ajaxSubmitURL;
				self.$form[0].target = 'iframeCategoryRating';
				self.$form[0].submit();
			
				return false;
				
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( options.ajaxDeleteURL, {id:id, modelName:options.modelName }, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );