var wAdminJournalFormWidgetOpen;
!function( $ ) {
	wAdminJournalFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New journal',
			lEdit: 'Editor journal',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminJournalFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			inner: [],
			editIndex: -1,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );

				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputNumber = self.$ns.find( options.ins+'.wNumber' );
				self.$inputDate = self.$ns.find( options.ins+'.wDate' );
				
				self.$textareaDescription = self.$ns.find( options.ins+'.wdescription' );
													
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				self.$bAddInner = self.$ns.find( options.ins+'.wAddInner' );
				self.$tabInner = self.$ns.find( options.ins+'.wTabInner' );		
				self.$bCancelEdit = self.$ns.find( options.ins+'.wCancelEditInner' );
				
				
				self.$wInner = self.$ns.find( options.ins+'.wInner' );			
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				self.$form.on( 'submit', self.onSubmit );
				self.$bAddInner.click( self.addCurrentInner );
				self.$bCancelEdit.click( self.cancelEditInner );
				
				self.$tabInner.on( "click", ".wDelete", self.deleteSpecificInner );
				self.$tabInner.on( "click", ".wMoveInner", self.moveSpecificInner );
				self.$tabInner.on( "click", ".wEdit", self.editSpecificInner );
				
				self.$textareaDescription.ckeditor({
					language: yiiLanguage,
				});
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputNumber.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$inputNumber.val( model.number );
				self.$ns.find( options.ins+'.wslug' ).val( model.slug );
				self.$inputDate.val( model.date );
				self.$ns.find( options.ins+'.wisDigest' ).prop( 'checked', model.isDigest == '1' );
				
				for( var idLanguage in model.wideID ) {
					var $wWideID = self.$ns.find( options.ins+'.wWideID.w'+idLanguage );
					$wWideID.val( model.wideID[idLanguage] );
				}
				for( var idLanguage in model.covers ) {
					var $wCoverHref = self.$ns.find( options.ins+'.wCoverHref.w'+idLanguage );
					$wCoverHref.attr( 'href', model.covers[idLanguage] );
					$wCoverHref.html( commonLib.baseName( model.covers[idLanguage]));
					$wCoverHref.show();
				}
				for( var idLanguage in model.journals ) {
					var $wJournalHref = self.$ns.find( options.ins+'.wJournalHref.w'+idLanguage );
					$wJournalHref.attr( 'href', model.journals[idLanguage] );
					$wJournalHref.html( commonLib.baseName( model.journals[idLanguage]));
					$wJournalHref.show();
				}
				for( var idLanguage in model.frees ) {
					var $wFree = self.$ns.find( options.ins+'.wFree.w'+idLanguage );
					$wFree.prop( 'checked', model.frees[ idLanguage ] == '1' );
				}
				
				
				for( i in options.textFields ){
					var modelField = options.textFields[i];
					for( var idLanguage in model[modelField] ) {
						var $wEl = self.$ns.find( options.ins+'.w'+modelField+'.w'+idLanguage );
						$wEl.val( model[modelField][idLanguage] );
					}
				}
				
				
                var $wStatus = self.$ns.find( options.ins+'.wStatus.w0' );
                $wStatus.prop( 'checked', model.status == '1' );

                var $wPush_notification = self.$ns.find( options.ins+'.wPush_notification.w0' );
                $wPush_notification.prop( 'checked', model.push_notification == '1' );
				
				
				for( var idLanguage in model.inner ) {
					for( i in model.inner[idLanguage] ) {
						//console.log( model.inner );
						self.addInner( model.inner[idLanguage][i].headers, model.inner[idLanguage][i].contents, model.inner[idLanguage][i].url, idLanguage );
					}
				}
			},
			addInnerRow:function( header, idLanguage ) {
				var $tabInner = self.$ns.find( options.ins+'.wTabInner.w'+idLanguage );
				var $row = $tabInner.find( '.wTpl' ).clone();
				$row.removeClass( 'wTpl' ).show();
				$row.find( '.wTDName' ).html( header );
				$tabInner.find( 'tbody' ).append( $row );
			},
			addInner:function( headers, contents, innerUrl, idLanguage ) {
				if( self.editIndex >= 0 ){
					self.inner[idLanguage][self.editIndex].headers = headers;
					self.inner[idLanguage][self.editIndex].contents = contents;
					self.inner[idLanguage][self.editIndex].url = innerUrl;
					self.$ns.find( options.ins+'.wInner.w'+idLanguage ).val( JSON.stringify( self.inner[idLanguage] ));
					
					var $tabInner = self.$ns.find( options.ins+'.wTabInner.w'+idLanguage );
					var tr = $tabInner.find( 'tr:not(.wTpl):eq('+self.editIndex+')' );
					
					tr.css({opacity: 0});
					tr.find('.wTDName .innerHeader').html(headers);
					tr.find('.wTDName .innerContent').html(contents);
					tr.find('.wTDName .innerUrl').html(innerUrl);
					tr.animate({opacity: 1}, 500, function(){});
					
					self.cancelEditInner();
					return false;
				}
				self.inner[idLanguage].push( { headers: headers, contents: contents, url: innerUrl } );		
				self.$ns.find( options.ins+'.wInner.w'+idLanguage ).val( JSON.stringify( self.inner[idLanguage] ));
				if( innerUrl == undefined ) innerUrl = '';
				self.addInnerRow( '<span class="innerHeader">' + headers + '</span><br /><span class="innerContent">' + contents+'</span><br /><span class="innerUrl">' + innerUrl + '</span>', idLanguage );
			},
			addCurrentInner:function() {
				
				var idLanguage = $(this).attr('data-langId');
				
				var headers = '';
				var contents = '';
				
				var emptyHeader = true;
				
				var $wHeader = self.$ns.find( options.ins+'.wHeader.w'+idLanguage );
				var $wContent = self.$ns.find( options.ins+'.wContent.w'+idLanguage );
				var $wUrl = self.$ns.find( options.ins+'.wInnerUrl.w'+idLanguage );
				
				$wHeader.each( function() {
					headers = $(this).val();
					if( headers != '' )emptyHeader = false;
				});	
				$wContent.each( function() {
					contents = $(this).val();
				});
				if( emptyHeader ) return false;
				
				self.addInner( headers, contents, $wUrl.val(), idLanguage );
				$wHeader.val( '' );
				$wContent.val( '' );
				$wUrl.val( '' );
		
				return false;
			},
			deleteInner:function( index, idLanguage ) {
				var tempInner = self.inner[idLanguage];
				var $tabInner = self.$ns.find( options.ins+'.wTabInner.w'+idLanguage );
				
				self.inner[idLanguage] = [];
				
				for( var i=0; i<tempInner.length; i++ ) {
					if( i == index ) continue;
					self.inner[idLanguage].push( tempInner[i]);
				}
				
				self.$ns.find( options.ins+'.wInner.w'+idLanguage ).val( JSON.stringify( self.inner[idLanguage] ));
				$tabInner.find( 'tr:not(.wTpl):eq('+index+')' ).remove();
			},
			moveInner:function( index, idLanguage, moveTo ) {

				var indexFrom = index;
				var indexTo = index+moveTo;
				
				var tmpVal = self.inner[idLanguage][indexTo];
				self.inner[idLanguage][indexTo] = self.inner[idLanguage][indexFrom];
				self.inner[idLanguage][indexFrom] = tmpVal;
		
				var $tabInner = self.$ns.find( options.ins+'.wTabInner.w'+idLanguage );

				self.$ns.find( options.ins+'.wInner.w'+idLanguage ).val( JSON.stringify( self.inner[idLanguage] ));
				
				var trTo = $tabInner.find( 'tr:not(.wTpl):eq('+indexTo+')' );
				var trFrom = $tabInner.find( 'tr:not(.wTpl):eq('+indexFrom+')' );
				
				trTo.css({opacity: 0});
				trFrom.css({opacity: 0});
				
				var tmpHtml = trTo.html();
				trTo.html( trFrom.html() );
				trFrom.html(tmpHtml);
				
				trTo.animate({opacity: 1}, 500, function(){});
				trFrom.animate({opacity: 1}, 500, function(){});
			},
			moveSpecificInner:function() {
				var idLanguage = $(this).attr('data-langId');
				var $tabInner = self.$ns.find( options.ins+'.wTabInner.w'+idLanguage );
				var $row = $(this).closest( "tr" );
				var index = $tabInner.find( 'tr:not(.wTpl)' ).index( $row );
				
				if( $(this).hasClass('wMoveUp') ){
					var moveTo = -1;
				}else{
					var moveTo = 1;
				}
				
				self.moveInner( index, idLanguage, moveTo );
				return false;
			},
			cancelEditInner:function( ) {
				self.editIndex = -1;
				self.$ns.find( options.ins+'.wHeader' ).val( '' );
				self.$ns.find( options.ins+'.wContent' ).val( '' );
				self.$ns.find( options.ins+'.wInnerUrl' ).val( '' );
				self.$bAddInner.text( options.ls.lAddInner );
				self.$bCancelEdit.hide();
			},
			editInner:function( index, idLanguage ) {

				self.$ns.find( options.ins+'.wHeader.w'+idLanguage ).val( self.inner[idLanguage][index].headers );
				self.$ns.find( options.ins+'.wContent.w'+idLanguage ).val( self.inner[idLanguage][index].contents );
				self.$ns.find( options.ins+'.wInnerUrl.w'+idLanguage ).val( self.inner[idLanguage][index].url );
				
				self.editIndex = index;
				
				self.$bAddInner.text( options.ls.lEditInner );
				self.$bCancelEdit.show();
				
			},
			editSpecificInner:function() {
				var idLanguage = $(this).attr('data-langId');
				var $tabInner = self.$ns.find( options.ins+'.wTabInner.w'+idLanguage );
				var $row = $(this).closest( "tr" );
				var index = $tabInner.find( 'tr:not(.wTpl)' ).index( $row );
				
				self.editInner( index, idLanguage);
				return false;
			},
			deleteSpecificInner:function() {
				var idLanguage = $(this).attr('data-langId');
				var $tabInner = self.$ns.find( options.ins+'.wTabInner.w'+idLanguage );
				var $row = $(this).closest( "tr" );
				var index = $tabInner.find( 'tr:not(.wTpl)' ).index( $row );
				self.deleteInner( index, idLanguage );
				return false;
			},
			clearInner:function() {
				self.$tabInner.find( 'tr:not(.wTpl)' ).remove();
				
				for( i in options.langs ){
					self.inner[i] = [];
				}
				
				self.$wInner.val( JSON.stringify( [] ));
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'], input[type='file'], input[type='textarea'], textarea, input[type='date']" ).val( '' );
				var $wCoverHref = self.$ns.find( options.ins+'.wCoverHref' );
				$wCoverHref.hide();
				var $wJournalHref = self.$ns.find( options.ins+'.wJournalHref' );
				$wJournalHref.hide();
				self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );
				
				self.$ns.find( options.ins+'.wFree' ).prop( 'checked', true );
				
				self.clearInner();
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					options.ls.lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.newRecord = !self.$inputID.val();
					
					self.$form[0].action = options.ajaxSubmitURL;
					self.$form[0].target = 'iframeForJournal';
					return true;
				}
			},
			onSubmit:function() {
				self.disable();
				self.loading = true;
				return true;
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );
