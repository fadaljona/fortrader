var wAdminNewsAggregatorCatsFormWidgetOpen;
!function( $ ) {
	wAdminNewsAggregatorCatsFormWidgetOpen = function( options ) {
		var defOption = {
			ins: '',
			selector: '.wAdminNewsAggregatorCatsFormWidget',
			ajax: false,
			hidden: false,
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 10,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			state: 'showAdd',
			loading: false,
			keyTemplate: '<li class="row-fluid pull-left"><div class="pull-left span5 content"><p><span class="liKey" >{key}</span></p></div><div class="pull-right span1"><a class="icon-trash wDeleteKey" href="#"></a></div><div class="pull-right span1"><a class="icon-edit wEditKey" href="#"></a></div></li>',
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$order = self.$ns.find( options.ins+'.worder' );		
				self.$slug = self.$ns.find( options.ins+'.wslug' );		
				self.$ogImage = self.$ns.find( options.ins+'.wogImage' );		
				self.$enabled = self.$ns.find( options.ins+'.wenabled' );

												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				
				self.$bAddKey = self.$ns.find( options.ins+'.wAddKey' );
				self.$bAddKey.click( self.addCurrentKey );
				
				self.$langTabCats = self.$ns.find( '.langTabCats' );
				self.$langTabCats.on( "click", ".wDeleteKey", self.deleteKey );
				self.$langTabCats.on( "click", ".wEditKey", self.editKey );
			},
			keysValToHtml:function( mKeys ){
				self.$ns.find( options.ins+'.keysUl' ).html('');
				$.each( mKeys, function( idLanguage, keys ) { 	
					if( keys != '' ) {
						parsedKeys = JSON.parse(keys);
						$.each( parsedKeys, function( index, item ) { 
							var appendStr = self.keyTemplate;
							appendStr = appendStr.replace( '{key}', item );
							self.$ns.find( options.ins+'.keysUl'+idLanguage ).append( appendStr );
						});
					}
				});
			},
			generateKeysVal: function(){
				self.$langTabCats.each( function() {
					var $this = $(this);
					var idLanguage = $this.attr('data-langId');
					var valArr = [];
					$this.find('.keysUl li').each( function() {
						valArr.push( $(this).find('.liKey').text() );
					});
					self.$ns.find( options.ins+'.wkeys.w'+idLanguage ).val( JSON.stringify(valArr) );
				});	
			},
			editKey: function(){
				var parentUl = $(this).closest('ul');
				var parentLI = $(this).closest('li');
				
				var idLanguage = parentUl.attr('data-langid');
				var ruleHeader = parentLI.find('div.content p span.liKey').text();
				
				self.$ns.find( options.ins+'.wkey'+idLanguage ).val( ruleHeader );
				parentLI.remove();
				
				return false;
			},
			deleteKey:function(){
				$(this).closest('li').remove();
				return false;
			},
			addCurrentKey:function() {
				
				var idLanguage = $(this).attr('data-langId');
				var $key = self.$ns.find( options.ins+'.wkey'+idLanguage );
				var key = $key.val();
				
				if( key == '' ) return false;
				
				var appendStr = self.keyTemplate;
				appendStr = appendStr.replace( '{key}', key);
		
				self.$ns.find( options.ins+'.keysUl'+idLanguage ).append( appendStr );
				$key.val('');
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$order.val( model.order );
				self.$slug.val( model.slug );
				self.$ogImage.val( model.ogImage );
				self.$enabled.prop( 'checked', model.enabled == '1' );
				
				for( var idLanguage in model.title ) self.$ns.find( options.ins+'.wtitle.w'+idLanguage ).val( model.title[idLanguage] );		
				for( var idLanguage in model.pageTitle ) self.$ns.find( options.ins+'.wpageTitle.w'+idLanguage ).val( model.pageTitle[idLanguage] );
				for( var idLanguage in model.metaTitle ) self.$ns.find( options.ins+'.wmetaTitle.w'+idLanguage ).val( model.metaTitle[idLanguage] );
				for( var idLanguage in model.metaKeys ) self.$ns.find( options.ins+'.wmetaKeys.w'+idLanguage ).val( model.metaKeys[idLanguage] );
				for( var idLanguage in model.metaDesc ) self.$ns.find( options.ins+'.wmetaDesc.w'+idLanguage ).val( model.metaDesc[idLanguage] );
				for( var idLanguage in model.shortDesc ) self.$ns.find( options.ins+'.wshortDesc.w'+idLanguage ).val( model.shortDesc[idLanguage] );
				for( var idLanguage in model.fullDesc ) self.$ns.find( options.ins+'.wfullDesc.w'+idLanguage ).val( model.fullDesc[idLanguage] );
				
				for( var idLanguage in model.stemedKeys ){
					if(model.stemedKeys[idLanguage]){
						self.$ns.find( options.ins+'.wstemedKeys.w'+idLanguage ).val( JSON.parse(model.stemedKeys[idLanguage]) );
					} 
				} 
				
				self.keysValToHtml( model.keys );
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( options.ajaxLoadURL, {id:id, formModelName: options.formModelName}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text']" ).val( '' );
				self.$form.find( "textarea" ).val( '' );
				self.$form.find('.keysUl').html('');
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.generateKeysVal();
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}else{
							self.hide();
	
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
							
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( options.ajaxDeleteURL, {id:id, modelName:options.modelName }, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );