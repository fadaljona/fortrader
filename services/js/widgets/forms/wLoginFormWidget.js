var wLoginFormWidgetOpen;
!function( $ ) {
	wLoginFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lRegistered: 'Congrats! You are registered!',
			lMessageSend: 'Message send!',
		};
		var defOption = {
			selector: '.wAdminBrokerFormWidget',
			ls: {},
			ajaxRegisterSubmitURL: '/user/ajaxRegister',
			ajaxResetPasswordSubmitURL: '/user/ajaxResetPasswordReguest',
			delayTooltips: 1000,
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$registerBlock = self.$ns.find( '#signup-box' );
				self.$registerForm = self.$registerBlock.find( 'form' );
				self.$registerForm.$bSubmit = self.$registerForm.find( '[type=submit]' );
				
				self.$resetPasswordBlock = self.$ns.find( '#forgot-box' );
				self.$resetPasswordForm = self.$resetPasswordBlock.find( 'form' );
				self.$resetPasswordForm.$bSubmit = self.$resetPasswordBlock.find( '[type=submit]' );
				
				self.$registerForm.submit( self.registerFormSubmit );
				self.$resetPasswordForm.submit( self.resetPasswordFormSubmit );
				self.$controls = self.$ns.find( 'button' );
				
				self.$ns.find( '.wShowRegister' ).click( function() { self.show( '#signup-box' ); return false; });
				self.$ns.find( '.wShowForgot' ).click( function() { self.show( '#forgot-box' ); return false; });
				self.$ns.find( '.wShowLogin' ).click( function() { self.show( '#login-box' ); return false; });
			},
			show: function( selector ) {
				self.$ns.find( '.widget-box.visible' ).removeClass( 'visible' );
				self.$ns.find( selector ).addClass( 'visible' );
			},
			disable: function() {
				self.$controls.attr( 'disabled', 'disabled' );
			},
			enable: function() {
				self.$controls.removeAttr( 'disabled' );
			},
			registerFormSubmit:function() {
				if( self.loading ) return false;
				self.disable();
				self.$registerForm.find( '.'+options.errorClass ).removeClass( options.errorClass );
				var lSubmit = self.$registerForm.$bSubmit.html();
				self.$registerForm.$bSubmit.html( options.ls.lSaving );
				self.loading = true;
				
				$.post( options.ajaxRegisterSubmitURL, self.$registerForm.serialize(), function ( data ) {
					self.loading = false;
					self.$registerForm.$bSubmit.html( lSubmit );
					self.enable();
					if( data.error ) {
						alert( data.error );
						if( data.errorField ) {
							var $errorField = self.$registerForm.find( '*[name="'+data.errorField+'"]' );
							$errorField.addClass( options.errorClass );
							$errorField.focus();
						}
					}
					else{
						self.$registerForm[0].reset();
				//		_gaq.push(['_trackPageview','/wp-login.php?checkemail=registered']);
						alert( options.ls.lRegistered );
						self.show( '#login-box' );
					}
				}, "json" );
				return false;
			},
			resetPasswordFormSubmit:function() {
				if( self.loading ) return false;
				self.disable();
				self.$resetPasswordForm.find( '.'+options.errorClass ).removeClass( options.errorClass );
				var lSubmit = self.$resetPasswordForm.$bSubmit.html();
				self.$resetPasswordForm.$bSubmit.html( options.ls.lSaving );
				self.loading = true;
				
				$.post( options.ajaxResetPasswordSubmitURL, self.$resetPasswordForm.serialize(), function ( data ) {
					self.loading = false;
					self.$resetPasswordForm.$bSubmit.html( lSubmit );
					self.enable();
					if( data.error ) {
						alert( data.error );
						if( data.errorField ) {
							var $errorField = self.$resetPasswordForm.find( '*[name="'+data.errorField+'"]' );
							$errorField.addClass( options.errorClass );
							$errorField.focus();
						}
					}
					else{
						self.$resetPasswordForm[0].reset();
						alert( options.ls.lMessageSend );
						self.show( '#login-box' );
					}
				}, "json" );
				return false;
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );