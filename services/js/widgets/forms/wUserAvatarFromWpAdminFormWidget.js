var wUserAvatarFromWpAdminFormWidgetOpen;
!function( $ ) {
	wUserAvatarFromWpAdminFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lDeleting: 'Deleting...',
			lSaved: 'Saved',
			lError: 'Error!',
			lDeleteConfirm: 'Delete?',
			lNoFile: 'No file ...',
			lChoose: 'Choose',
			lChange: 'Change',
		};
		var defOption = {
			ins: '',
			selector: '.wUserAvatarFromWpAdminFormWidget',
			ajax: false,
			ls: defLS,
			delayTooltips: 1000,
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				
				self.$bDelete = self.$ns.find( '.wDelete' );
				self.$bChange = self.$ns.find( '.wChange' );
				self.$wAvatar = self.$ns.find( '.wAvatar' );
				self.$wAvatarHolder = self.$ns.find( '.wAvatarHolder' );

				var srcAvatar = self.$wAvatarHolder.find( 'img' ).attr( 'src' );
				if( !srcAvatar ){
					/*self.$bDelete.hide();*/
					self.$wAvatarHolder.hide();
					self.$bChange.text( options.ls.lSet );
				}
				
				/*self.$bDelete.click( self.delete );*/
				self.$bChange.click( self.change );
				self.$wAvatar.change( self.submit );
			},
			change:function(){
				self.$wAvatar.trigger('click');
				return false;
			},
			disable: function() {
				self.$ns.append( self.spinner );
			},
			enable: function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			
			submit:function() {
				if( self.loading ) return;
				if( options.ajax ) {

					self.$bChange.text( options.ls.lSaving );

					self.loading = true;
					self.$form[0].action = options.ajaxSubmitURL;
					self.$form[0].target = 'iframeForAvatar';
					self.$form[0].submit();
					
					self.disable();
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$form[0].reset();

			},
			submitError:function( error ) {
				self.submitComplete();
				alert( error );
			},
			submitSuccess:function( id, thumbPath, middlePath ) {
				var pathImg = yiiBaseURL + '/' + middlePath;
				self.submitComplete();
				self.$wAvatarHolder.find( 'img' ).attr( 'src', pathImg );
				self.$wAvatarHolder.show();
				self.$bChange.text( options.ls.lChange );
				/*self.$bDelete.show();*/
				$.each( self.onSubmit, function() { this( id ); });
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				var id = options.idUser;
				self.loading = true;
				self.disable();
				
				var lDelete = self.$bDelete.html();
				self.$bDelete.html( options.ls.lDeleting );

				
				$.get( options.ajaxDeleteURL, {id:id}, function ( data ) {
					self.loading = false;
					self.$bDelete.html( lDelete );
					self.enable();
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$wAvatarHolder.hide();
						self.$bDelete.hide();
						self.$bChange.text( options.ls.lSet );
						$.each( self.onDelete, function() { this( id ); });
					}
				}, "json" );
				return false;
			},
			onSubmit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );