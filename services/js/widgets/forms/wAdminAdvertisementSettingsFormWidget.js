var wAdminAdvertisementSettingsFormWidgetOpen;
!function( $ ) {
	wAdminAdvertisementSettingsFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lReseted: 'Reseted',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminAdvertisementSettingsFormWidget',
			ajax: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/advertisementSettings/ajaxUpdate',
			delayTooltips: 1000,
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bReset = self.$ns.find( options.ins+'.wReset' );
				
				self.$bSubmit.click( self.submit );
				self.$bReset.click( self.reset );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bReset ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bReset ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.showTooltip( self.$bSubmit, options.ls.lSaved, 'top', options.delayTooltips );
							$.each( self.onUpdate, function() { this( data.id ); });
						}
					}, "json" );
					return false;
				}
			},
			reset:function() {
				if( self.loading ) return false;
				self.$form[0].reset();
				self.showTooltip( self.$bReset, options.ls.lReseted, 'top', options.delayTooltips );
			},
			onUpdate: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );