var wAdminInformersCategoryNewRawSettingsFormWidgetOpen;
!function( $ ) {
	wAdminInformersCategoryNewRawSettingsFormWidgetOpen = function( options ) {
		var defOption = {
			ins: '',
			selector: '.wAdminInformersCategoryNewRawSettingsFormWidget',
			ajax: false,
			hidden: false,
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 10,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			state: 'showAdd',
			loading: false,
			
			init:function() {
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				self.$inputID = self.$ns.find( options.ins+'.wID' );	
				self.$rawSettings = self.$ns.find( options.ins+'.wrawSettings' );
				
				$.fn.select2.defaults.set("width", "100%");
				$.fn.select2.defaults.set("theme", "classic");

				self.$rawSettings.select2();
						
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				
				self.$bSubmit.click( self.submit );
				
			
			},

			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					$(this).attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					$(this).removeAttr( 'disabled' );
				});
			},
			
			submit:function() {
			
				self.newRecord = !self.$inputID.val(); 
				
				self.$form[0].action = options.ajaxSubmitURL;
				self.$form[0].target = 'iframeForCategories';
				self.$form[0].submit();
			
				return false;
				
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );