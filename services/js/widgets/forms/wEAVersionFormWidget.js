var wEAVersionFormWidgetOpen;
!function( $ ) {
	wEAVersionFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
		};
		var defOption = {
			ins: '',
			selector: '.wEAVersionFormWidget',
			ajaxSubmitURL: '/eaVersion/iframeAdd',
			singleURL: '/eaVersion/single',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$wDesc = self.$ns.find( options.ins+'.wDesc' );
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
								
				self.$wDesc.ckeditor({
					language: yiiLanguage,
					toolbar: [],
				});
				
				self.$bSubmit.click( self.submit );
				self.$form.on( 'submit', self.onSubmit );
			},
			disable: function() {
				$.each([ self.$bSubmit ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			submit:function() {
				if( self.loading ) return false;
				
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				options.ls.lSubmit = self.$bSubmit.html();
				self.$bSubmit.html( options.ls.lSaving );
									
				self.$form[0].action = yiiBaseURL+options.ajaxSubmitURL;
				self.$form[0].target = 'iframeForVersion';
				return true;
			},
			onSubmit:function() {
				self.disable();
				self.loading = true;
				return true;
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				document.location.assign( yiiBaseURL+options.singleURL + '?id=' + id );
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );