var wAdminEAFormWidgetOpen;
!function( $ ) {
	wAdminEAFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New EA',
			lEdit: 'Editor EA',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminEAFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/ea/iframeAdd',
			ajaxDeleteURL: '/admin/ea/ajaxDelete',
			ajaxLoadURL: '/admin/ea/ajaxLoad',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputName = self.$ns.find( options.ins+'.wName' );
				self.$inputNameUser = self.$ns.find( options.ins+'.wNameUser' );
				self.$inputCreator = self.$ns.find( options.ins+'.wCreator' );
				self.$inputLicense = self.$ns.find( options.ins+'.wLicense' );
				self.$selectType = self.$ns.find( options.ins+'.wType' );
				self.$selectStatus = self.$ns.find( options.ins+'.wStatus' );
				self.$inputDate = self.$ns.find( options.ins+'.wDate' );
				self.$inputDesc = self.$ns.find( options.ins+'.wDesc' );
				
				self.$textareaAbout = self.$ns.find( options.ins+'.wAbout' );
							
				self.$wImageHref = self.$ns.find( options.ins+'.wImageHref' );
							
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				self.$form.on( 'submit', self.onSubmit );
				
				
				self.$textareaAbout.ckeditor({
					language: yiiLanguage,
				});
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						//self.$inputOfficialName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$inputName.val( model.name );
				
				for( var idLanguage in model.abouts ) {
					var $wAbout = self.$ns.find( options.ins+'.wAbout.w'+idLanguage );
					$wAbout.val( model.abouts[idLanguage] );
				}
				
				self.$inputNameUser.val( model.nameUser );
				self.$inputCreator.val( model.creator );
				self.$inputLicense.val( model.license );
				self.$selectType.val( model.type );
				self.$selectStatus.val( model.status );
				self.$inputDate.val( model.date );
				
				if( model.image ) {
					self.$wImageHref.attr( 'href', model.image );
					self.$wImageHref.html( commonLib.baseName( model.image ) );
					self.$wImageHref.show();
				}
				
				self.$inputDesc.val( model.desc );
				
				$.each( model.idsTradePlatforms, function () {
					self.$ns.find( '.wIDsTradePlatforms[value="'+this+'"]' ).prop( 'checked', true );
				});
				
				$.each( model.params, function () {
					self.$ns.find( '.wParams[value="'+this+'"]' ).prop( 'checked', true );
				});
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'], input[type='file'], textarea" ).val( '' );
				self.$selectType.find( 'option:first' ).prop( 'selected', true );
				self.$selectStatus.find( 'option:first' ).prop( 'selected', true );
				self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );
				self.$wImageHref.hide();
				self.$inputDate.val( commonLib.getCurrentDate() );
				self.$inputNameUser.val( options.defaultNameUser );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					options.ls.lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.newRecord = !self.$inputID.val();
					
					self.$form[0].action = yiiBaseURL+options.ajaxSubmitURL;
					self.$form[0].target = 'iframeForEA';
					return true;
				}
			},
			onSubmit:function() {
				self.disable();
				self.loading = true;
				return true;
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );