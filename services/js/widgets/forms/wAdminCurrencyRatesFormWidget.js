var wAdminCurrencyRatesFormWidgetOpen;
!function( $ ) {
	wAdminCurrencyRatesFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New regulator',
			lEdit: 'Editor regulator',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminCurrencyRatesFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputName = self.$ns.find( options.ins+'.wName' );
				
				self.$inputCode = self.$ns.find( options.ins+'.wCode' );
				self.$inputPopular = self.$ns.find( options.ins+'.wPopular' );
				self.$inputTopInSelect = self.$ns.find( options.ins+'.wTopInSelect' );
				self.$toInformer = self.$ns.find( options.ins+'.wtoInformer' );
				self.$selectIDCountry = self.$ns.find( options.ins+'.wIDCountry' );
				self.$inputCurrencyOrder = self.$ns.find( options.ins+'.wCurrencyOrder' );
				self.$inputSymbol = self.$ns.find( options.ins+'.wSymbol' );
				
				self.$inputCbrDigitCode = self.$ns.find( options.ins+'.wCbrDigitCode' );
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$inputCode.val( model.code );
				self.$inputCurrencyOrder.val( model.currencyOrder );
				self.$inputSymbol.val( model.symbol );
				self.$inputCbrDigitCode.val( model.cbrDigitCode );
				self.$inputPopular.prop( 'checked', model.popular == '1' );
				self.$inputTopInSelect.prop( 'checked', model.topInSelect == '1' );
				self.$toInformer.prop( 'checked', model.toInformer == '1' );
				self.$selectIDCountry.val( model.idCountry );
				
				for( var idLanguage in model.names ) {
					var $wName = self.$ns.find( options.ins+'.wName.w'+idLanguage );
					$wName.val( model.names[idLanguage] );
				}
				
				for( var idLanguage in model.titles ) {
					var $wEL = self.$ns.find( options.ins+'.wTitle.w'+idLanguage );
					$wEL.val( model.titles[idLanguage] );
				}
				for( var idLanguage in model.metaTitles ) {
					var $wEL = self.$ns.find( options.ins+'.wMetaTitle.w'+idLanguage );
					$wEL.val( model.metaTitles[idLanguage] );
				}
				for( var idLanguage in model.metaDescs ) {
					var $wEL = self.$ns.find( options.ins+'.wMetaDesc.w'+idLanguage );
					$wEL.val( model.metaDescs[idLanguage] );
				}
				for( var idLanguage in model.metaKeyss ) {
					var $wEL = self.$ns.find( options.ins+'.wMetaKeys.w'+idLanguage );
					$wEL.val( model.metaKeyss[idLanguage] );
				}
				
				for( var idLanguage in model.shortDescs ) {
					var $wEL = self.$ns.find( options.ins+'.wShortDesc.w'+idLanguage );
					$wEL.val( model.shortDescs[idLanguage] );
				}
				for( var idLanguage in model.fullDescs ) {
					var $wEL = self.$ns.find( options.ins+'.wFullDesc.w'+idLanguage );
					$wEL.val( model.fullDescs[idLanguage] );
				}
				
				for( var idLanguage in model.chartTitles ) {
					var $wEL = self.$ns.find( options.ins+'.wChartTitle.w'+idLanguage );
					$wEL.val( model.chartTitles[idLanguage] );
				}
				for( var idLanguage in model.converterTitles ) {
					var $wEL = self.$ns.find( options.ins+'.wConverterTitle.w'+idLanguage );
					$wEL.val( model.converterTitles[idLanguage] );
				}
				for( var idLanguage in model.namePlurals ) {
					var $wEL = self.$ns.find( options.ins+'.wnamePlurals.w'+idLanguage );
					$wEL.val( model.namePlurals[idLanguage] );
				}
				for( var idLanguage in model.toName ) {
					var $wEL = self.$ns.find( options.ins+'.wtoName.w'+idLanguage );
					$wEL.val( model.toName[idLanguage] );
				}
				for( var idLanguage in model.informerName ) {
					var $wEL = self.$ns.find( options.ins+'.winformerName.w'+idLanguage );
					$wEL.val( model.informerName[idLanguage] );
				}
				
for( var idLanguage in model.singleArchiveYearTitle ) self.$ns.find( options.ins+'.wsingleArchiveYearTitle.w'+idLanguage ).val( model.singleArchiveYearTitle[idLanguage] );
for( var idLanguage in model.singleArchiveYearMetaTitle ) self.$ns.find( options.ins+'.wsingleArchiveYearMetaTitle.w'+idLanguage ).val( model.singleArchiveYearMetaTitle[idLanguage] );
for( var idLanguage in model.singleArchiveYearMetaDesc ) self.$ns.find( options.ins+'.wsingleArchiveYearMetaDesc.w'+idLanguage ).val( model.singleArchiveYearMetaDesc[idLanguage] );
for( var idLanguage in model.singleArchiveYearMetaKeys ) self.$ns.find( options.ins+'.wsingleArchiveYearMetaKeys.w'+idLanguage ).val( model.singleArchiveYearMetaKeys[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoTitle ) self.$ns.find( options.ins+'.wsingleArchiveYearMoTitle.w'+idLanguage ).val( model.singleArchiveYearMoTitle[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoMetaTitle ) self.$ns.find( options.ins+'.wsingleArchiveYearMoMetaTitle.w'+idLanguage ).val( model.singleArchiveYearMoMetaTitle[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoMetaDesc ) self.$ns.find( options.ins+'.wsingleArchiveYearMoMetaDesc.w'+idLanguage ).val( model.singleArchiveYearMoMetaDesc[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoMetaKeys ) self.$ns.find( options.ins+'.wsingleArchiveYearMoMetaKeys.w'+idLanguage ).val( model.singleArchiveYearMoMetaKeys[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoDayTitle ) self.$ns.find( options.ins+'.wsingleArchiveYearMoDayTitle.w'+idLanguage ).val( model.singleArchiveYearMoDayTitle[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoDayMetaTitle ) self.$ns.find( options.ins+'.wsingleArchiveYearMoDayMetaTitle.w'+idLanguage ).val( model.singleArchiveYearMoDayMetaTitle[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoDayMetaDesc ) self.$ns.find( options.ins+'.wsingleArchiveYearMoDayMetaDesc.w'+idLanguage ).val( model.singleArchiveYearMoDayMetaDesc[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoDayMetaKeys ) self.$ns.find( options.ins+'.wsingleArchiveYearMoDayMetaKeys.w'+idLanguage ).val( model.singleArchiveYearMoDayMetaKeys[idLanguage] );
for( var idLanguage in model.singleArchiveYearDescription ) self.$ns.find( options.ins+'.wsingleArchiveYearDescription.w'+idLanguage ).val( model.singleArchiveYearDescription[idLanguage] );
for( var idLanguage in model.singleArchiveYearFullDesc ) self.$ns.find( options.ins+'.wsingleArchiveYearFullDesc.w'+idLanguage ).val( model.singleArchiveYearFullDesc[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoDescription ) self.$ns.find( options.ins+'.wsingleArchiveYearMoDescription.w'+idLanguage ).val( model.singleArchiveYearMoDescription[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoFullDesc ) self.$ns.find( options.ins+'.wsingleArchiveYearMoFullDesc.w'+idLanguage ).val( model.singleArchiveYearMoFullDesc[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoDayDescription ) self.$ns.find( options.ins+'.wsingleArchiveYearMoDayDescription.w'+idLanguage ).val( model.singleArchiveYearMoDayDescription[idLanguage] );
for( var idLanguage in model.singleArchiveYearMoDayFullDesc ) self.$ns.find( options.ins+'.wsingleArchiveYearMoDayFullDesc.w'+idLanguage ).val( model.singleArchiveYearMoDayFullDesc[idLanguage] );
				
				
for( var idLanguage in model.ecbTitle ) self.$ns.find( options.ins+'.wecbTitle.w'+idLanguage ).val( model.ecbTitle[idLanguage] );
for( var idLanguage in model.ecbMetaTitle ) self.$ns.find( options.ins+'.wecbMetaTitle.w'+idLanguage ).val( model.ecbMetaTitle[idLanguage] );
for( var idLanguage in model.ecbMetaDesc ) self.$ns.find( options.ins+'.wecbMetaDesc.w'+idLanguage ).val( model.ecbMetaDesc[idLanguage] );
for( var idLanguage in model.ecbMetaKeys ) self.$ns.find( options.ins+'.wecbMetaKeys.w'+idLanguage ).val( model.ecbMetaKeys[idLanguage] );
for( var idLanguage in model.ecbShortDesc ) self.$ns.find( options.ins+'.wecbShortDesc.w'+idLanguage ).val( model.ecbShortDesc[idLanguage] );
for( var idLanguage in model.ecbFullDesc ) self.$ns.find( options.ins+'.wecbFullDesc.w'+idLanguage ).val( model.ecbFullDesc[idLanguage] );
for( var idLanguage in model.ecbChartTitle ) self.$ns.find( options.ins+'.wecbChartTitle.w'+idLanguage ).val( model.ecbChartTitle[idLanguage] );
for( var idLanguage in model.ecbConverterTitle ) self.$ns.find( options.ins+'.wecbConverterTitle.w'+idLanguage ).val( model.ecbConverterTitle[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearTitle ) self.$ns.find( options.ins+'.wecbSingleArchiveYearTitle.w'+idLanguage ).val( model.ecbSingleArchiveYearTitle[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMetaTitle ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMetaTitle.w'+idLanguage ).val( model.ecbSingleArchiveYearMetaTitle[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMetaDesc ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMetaDesc.w'+idLanguage ).val( model.ecbSingleArchiveYearMetaDesc[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMetaKeys ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMetaKeys.w'+idLanguage ).val( model.ecbSingleArchiveYearMetaKeys[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoTitle ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoTitle.w'+idLanguage ).val( model.ecbSingleArchiveYearMoTitle[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoMetaTitle ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoMetaTitle.w'+idLanguage ).val( model.ecbSingleArchiveYearMoMetaTitle[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoMetaDesc ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoMetaDesc.w'+idLanguage ).val( model.ecbSingleArchiveYearMoMetaDesc[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoMetaKeys ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoMetaKeys.w'+idLanguage ).val( model.ecbSingleArchiveYearMoMetaKeys[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoDayTitle ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoDayTitle.w'+idLanguage ).val( model.ecbSingleArchiveYearMoDayTitle[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoDayMetaTitle ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoDayMetaTitle.w'+idLanguage ).val( model.ecbSingleArchiveYearMoDayMetaTitle[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoDayMetaDesc ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoDayMetaDesc.w'+idLanguage ).val( model.ecbSingleArchiveYearMoDayMetaDesc[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoDayMetaKeys ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoDayMetaKeys.w'+idLanguage ).val( model.ecbSingleArchiveYearMoDayMetaKeys[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearDescription ) self.$ns.find( options.ins+'.wecbSingleArchiveYearDescription.w'+idLanguage ).val( model.ecbSingleArchiveYearDescription[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearFullDesc ) self.$ns.find( options.ins+'.wecbSingleArchiveYearFullDesc.w'+idLanguage ).val( model.ecbSingleArchiveYearFullDesc[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoDescription ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoDescription.w'+idLanguage ).val( model.ecbSingleArchiveYearMoDescription[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoFullDesc ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoFullDesc.w'+idLanguage ).val( model.ecbSingleArchiveYearMoFullDesc[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoDayDescription ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoDayDescription.w'+idLanguage ).val( model.ecbSingleArchiveYearMoDayDescription[idLanguage] );
for( var idLanguage in model.ecbSingleArchiveYearMoDayFullDesc ) self.$ns.find( options.ins+'.wecbSingleArchiveYearMoDayFullDesc.w'+idLanguage ).val( model.ecbSingleArchiveYearMoDayFullDesc[idLanguage] );
				
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( options.ajaxLoadURL, {id:id, formModelName: options.formModelName}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text']" ).val( '' );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.hide();
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( options.ajaxDeleteURL, {id:id, modelName: options.modelName}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );