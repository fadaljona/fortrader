var wCalendarSingleSubscriptionFormWidgetOpen;
!function( $ ) {
	wCalendarSingleSubscriptionFormWidgetOpen = function( options ) {
		var defLS = {
		};
		var defOption = {
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );

				self.$timeBeforeEvent = self.$ns.find( '.wtimeBeforeEvent' );

				self.$timeWrapper = self.$form.find('.informer-calendar__time');
				self.$timeInputs = self.$timeWrapper.find('input[type="text"]');
				self.$timeInputs.keydown( function(e){ self.filterTimeInp(e); } );
				
				self.$form.submit(function(){return false;});
				self.$form.find('.wSubscribe').click( self.subscribe );
				self.$form.find('.wUnsubscribe').click( self.unSubscribe );
			},
			filterTimeInp: function(e){
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
					// Allow: Ctrl/cmd+A				
					(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
					// Allow: Ctrl/cmd+C
					(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
					// Allow: Ctrl/cmd+X
					(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
					// Allow: home, end, left, right
					(e.keyCode >= 35 && e.keyCode <= 39)) {
					// let it happen, don't do anything
					return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			},
			disable: function() {
				self.$ns.append( self.spinner );
			},
			enable: function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			setTimeBeforeEvent: function(){
				var $checkedTime = self.$timeWrapper.find('input[type="radio"]:checked');
				if( $checkedTime.attr('id') == 'time_minutes' ){
					var timeBeforeEvent = 60 * parseInt( $checkedTime.closest('.informer-calendar__time-item').find('input[type="text"]').val() );
				}else{
					var timeBeforeEvent = 60 * 60 * parseInt( $checkedTime.closest('.informer-calendar__time-item').find('input[type="text"]').val() );
				}
				self.$timeBeforeEvent.val( timeBeforeEvent );
			},
			subscribe: function(){
				if( self.loading ) return false;
				self.disable();

				self.setTimeBeforeEvent();

				self.loading = true;
				$.post( options.subscribeUrl, self.$form.serialize(), function ( data ) {
					self.loading = false;

					self.enable();
					if( data.error ) {
						alert( data.error );
					}else{
						self.$timeWrapper.addClass('hide');
						self.$form.find('.wSubscribe').addClass('hide');
						self.$form.find('.wUnsubscribe').removeClass('hide');
					}
				}, "json" );
				return false;
			},
			unSubscribe: function(){
				if( self.loading ) return false;
				self.disable();
	
				self.loading = true;

				$.post( options.unSubscribeUrl, {idEvent: self.$form.find('.widEvent').val()}, function ( data ) {
					self.loading = false;

					self.enable();
					if( data.error ) {
						alert( data.error );
					}else{
						self.$timeWrapper.removeClass('hide');
						self.$form.find('.wSubscribe').removeClass('hide');
						self.$form.find('.wUnsubscribe').addClass('hide');
					}
				}, "json" );
				return false;
			},
			
			
		};
		self.init();
		return self;
	}
}( window.jQuery );