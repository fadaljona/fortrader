var wQuoteForecastFormWidgetOpen;
!function( $ ) {
	wQuoteForecastFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Forecast saved',
		};
		var defOption = {
			selector: '.insQuoteForecastFormWidget',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				self.loading = false;
				
				self.$wForecast = self.$ns.find( '.wForecast' );
				
				self.$wForecastsLink = self.$wForecast.find( '.forecatsLink' );
				self.$wForecastsLinkCount = self.$wForecastsLink.find( 'span' );
				
				self.$wForecastForm = self.$wForecast.find( 'form' );
				self.$wForecastError = self.$wForecast.find( '.wFormError' );
				self.$wForecastFormSubmit = self.$wForecastForm.find( '.wSubmit' );
				self.$wForecastFormSubmit.click( self.forecastSubmit );
				self.$wForecastFormValue = self.$wForecastForm.find('.wValue');
				

				self.$wMood = self.$ns.find( '.wMood' );
				self.$wMoodError = self.$wMood.find( '.wFormError' );
				self.$wYourMood = self.$wMood.find( '.yourMood' );
				self.$wYourMoodText = self.$wYourMood.find( 'span' );
				
				self.$wMoodButton = self.$wMood.find( '.wMoodButton' );
				self.$wMoodButton.click( self.moodSubmit );
				self.$wMoodForm = self.$wMood.find( 'form' );
				self.$wMoodFormValue = self.$wMoodForm.find('.wValue');

				self.$wMoodChartBox = self.$wMood.find('.mood_chart_box');
				
				self.$wMoodChartBull = self.$wMood.find('.mood_chart_bull');
				self.$wMoodChartBear = self.$wMood.find('.mood_chart_bear');
				
				self.$wMoodChartBullLabel = self.$wMood.find('.mood_chart_label1.bull');
				self.$wMoodChartBearLabel = self.$wMood.find('.mood_chart_label1.bear');

			},
			moodSubmit:function() {
				if( !options.canAddMood ) return false;
				if( self.loading ) return false;
				self.loading = true;
				var currButton = $(this);
				var currButtonLabel = currButton.find('span');
				
				if( currButton.hasClass('wBullish') ){
					self.$wMoodFormValue.val('1');
				}else{
					self.$wMoodFormValue.val('-1');
				}
				var lButton = currButtonLabel.html();
				currButtonLabel.html( options.ls.lSaving )
				$.post( options.ajaxSubmitURL, self.$wMoodForm.serialize(), function ( data ) {
					currButtonLabel.html( lButton );
					self.loading = false;
					
					if( data.error ) {
						self.$wMoodError.html( data.error );
					}
					else{
						self.$wMoodChartBox.removeClass('hide');
						self.$wMoodChartBull.css({ width: data.mood.bull+'%' });
						self.$wMoodChartBear.css({ width: data.mood.bear+'%' });
						self.$wMoodChartBullLabel.html(data.mood.bull+'%');
						self.$wMoodChartBearLabel.html(data.mood.bear+'%');
						
						self.$wYourMood.removeClass('hide');
						self.$wYourMoodText.html( data.yourMood );
						options.canAddMood = 0;
						self.$wMoodButton.addClass('clicked');
					}
				}, "json" );
				return false;
			},
			forecastSubmit:function() {
				if( self.loading ) return false;
				
				var lSubmit = self.$wForecastFormSubmit.html();
				self.$wForecastFormSubmit.html( options.ls.lSaving );
				self.loading = true;
				
				$.post( options.ajaxSubmitURL, self.$wForecastForm.serialize(), function ( data ) {
					self.$wForecastError.html('');
					self.$wForecastFormSubmit.html( lSubmit );
					self.loading = false;
					
					if( data.error ) {
						self.$wForecastError.html( data.error );
					}
					else{
						self.$wForecastsLink.removeClass('hide');
						self.$wForecastsLinkCount.html( '('+ data.count + ')' );
						self.$wForecastFormValue.val('');
					}
				}, "json" );

				return false;
			},
			
		};
		self.init();
		return self;
	}
}( window.jQuery );