var wAdminAdvertisementCampaignFormWidgetOpen;
!function( $ ) {
	wAdminAdvertisementCampaignFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New campaign',
			lEdit: 'Editor campaign',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminAdvertisementCampaignFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/advertisementCampaign/ajaxAdd',
			ajaxDeleteURL: '/admin/advertisementCampaign/ajaxDelete',
			ajaxLoadURL: '/admin/advertisementCampaign/ajaxLoad',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputName = self.$ns.find( options.ins+'.wName' );
				self.$inputBegin = self.$ns.find( options.ins+'.wBegin' );
				self.$inputEnd = self.$ns.find( options.ins+'.wEnd' );
				self.$inputClickCost = self.$ns.find( options.ins+'.wClickCost' );
				self.$inputMarketClickCost = self.$ns.find( options.ins+'.wMarketClickCost' );
				self.$selectStatus = self.$ns.find( options.ins+'.wStatus' );
				self.$selectIDUser = self.$ns.find( options.ins+'.wIDUser' );
				self.$inputDayLimitShows = self.$ns.find( options.ins+'.wDayLimitShows' );
				self.$inputTotalLimitShows = self.$ns.find( options.ins+'.wTotalLimitShows' );
				self.$inputDayLimitClicks = self.$ns.find( options.ins+'.wDayLimitClicks' );
				self.$inputWeight = self.$ns.find( options.ins+'.wWeight' );
				self.$inputMinPopup = self.$ns.find( options.ins+'.wMinPopup' );
				self.$inputHideOnMobiles = self.$ns.find( options.ins+'.wHideOnMobiles' );
				self.$inputOneAd = self.$ns.find( options.ins+'.wOneAd' );
				
				self.$inputStartTime = self.$ns.find( options.ins+'.wStartTime' );
				self.$inputEndTime = self.$ns.find( options.ins+'.wEndTime' );
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				
				commonLib.initDatepicker();
				self.$inputBegin.datepicker();
				self.$inputEnd.datepicker();
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$inputName.val( model.name );
				for( var i in model.type ) {
					var wType = self.$ns.find( options.ins+'.wType[value="'+model.type[i]+'"]' );
					wType.prop( 'checked', true );
				}
				self.$inputBegin.val( model.begin );
				self.$inputEnd.val( model.end );
				self.$inputClickCost.val( model.clickCost );
				self.$inputMarketClickCost.prop( 'checked', model.marketClickCost == '1' );
				self.$selectStatus.val( model.status );
				self.$selectIDUser.val( model.idUser );
				self.$inputDayLimitShows.val( model.dayLimitShows );
				self.$inputTotalLimitShows.val( model.totalLimitShows );
				self.$inputDayLimitClicks.val( model.dayLimitClicks );
				self.$inputWeight.val( model.weight );
				self.$inputMinPopup.val( model.minPopup );
				self.$inputHideOnMobiles.prop( 'checked', model.hideOnMobiles == '1' );
				self.$inputOneAd.prop( 'checked', model.oneAd == '1' );
				
				self.$inputStartTime.val( model.startTime );
				self.$inputEndTime.val( model.endTime );
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'],input[type='date'], select" ).val( '' );
				self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );
				self.$inputHideOnMobiles.prop( 'checked', true );
				self.$inputBegin.val( options.defBegin );
				self.$inputEnd.val( options.defEnd );
				self.$selectIDUser.val( options.idDefaultUser );
				self.$inputClickCost.val( options.adClickCost );

				self.$inputStartTime.val( '00:00:00' );
				self.$inputEndTime.val( '23:59:59' );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.hide();
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );