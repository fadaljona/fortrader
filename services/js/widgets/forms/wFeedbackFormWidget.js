var wFeedbackFormWidgetOpen;
!function( $ ) {
	wFeedbackFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lError: 'Error!',
			lSended: 'Message sent!',
		};
		var defOption = {
			ins: '',
			selector: '.wFeedbackFormWidget',
			ajax: false,
			ls: defLS,
			delayTooltips: 1000,
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				
				self.$bSubmit.click( self.submit );
			},
			disable: function() {
				$.each([ self.$bSubmit ], function () {
					this.attr( 'disabled', 'disabled' );
				});
				self.$form.find('input').prop('disabled', true);
				self.$form.find('textarea').prop('disabled', true);
			},
			enable: function() {
				$.each([ self.$bSubmit ], function () {
					this.removeAttr( 'disabled' );
				});
				self.$form.find('input').prop('disabled', false);
				self.$form.find('textarea').prop('disabled', false);
			},
			clear:function() {
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'], input[type='email'], textarea" ).val( '' );
			},
			submit:function() {
				if( self.loading ) return false;
				var formData = self.$form.serialize();
				self.disable();

				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				var lSubmit = self.$bSubmit.html();
				self.$bSubmit.html( options.ls.lSaving );
				
				self.loading = true;
				$.post( options.ajaxSubmitURL, formData, function ( data ) {
					self.loading = false;
					self.$bSubmit.html( lSubmit );
					self.enable();
					if( data.error ) {
						if( options.mode == 'feedback' ){
							alert( data.error );
						}
						if( options.mode == 'contest' ){
							self.$form.find( '.errorMess' ).text(data.error);
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
					}
					else{
						if( options.mode == 'feedback' ){
							alert( options.ls.lSended );
						}
						if( options.mode == 'contest' ){
							self.$form.find( '.errorMess' ).text( options.ls.lSended );
						}
						
						$.each( self.onAdd, function() { this( data.id ); });
						self.clear();
						
					}
				}, "json" );
				
				return false;
			},
			onAdd: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );