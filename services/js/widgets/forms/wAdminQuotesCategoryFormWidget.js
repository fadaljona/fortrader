var wAdminQuotesCategoryFormWidgetOpen;
!function( $ ) {
	wAdminQuotesCategoryFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New quotesCategory',
			lEdit: 'Editor quotesCategory',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?'
		};
		var defOption = {
			ins: '',
			selector: '.wAdminQuotesCategoryFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/quotesCategory/ajaxUpdate',
			ajaxDeleteURL: '/admin/quotesCategory/ajaxDelete',
			ajaxLoadURL: '/admin/quotesCategory/ajaxLoad',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputSlug = self.$ns.find( options.ins+'.wslug' );

				self.$inputOrder = self.$ns.find( options.ins+'.worder' );
				self.$inputParent = self.$ns.find( options.ins+'.wParent' );
				self.$inputIs_def = self.$ns.find( options.ins+'.wis_def' );
				self.$inputOgImageUrl = self.$ns.find( options.ins+'.wogImageUrl' );
				
				
				
				

				
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputSlug.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$inputOrder.val( model.order );
				self.$inputParent.val( model.parent );
				self.$inputIs_def.prop( 'checked', model.is_def == '1' );
				self.$inputSlug.val( model.slug );
				self.$inputOgImageUrl.val( model.ogImageUrl );

				console.log( model );

				for( var idLanguage in model.name ) self.$ns.find( options.ins+'.wname.w'+idLanguage ).val( model.name[idLanguage] );		
				for( var idLanguage in model.desc ) self.$ns.find( options.ins+'.wdesc.w'+idLanguage ).val( model.desc[idLanguage] );
				for( var idLanguage in model.fullDesc ) self.$ns.find( options.ins+'.wfullDesc.w'+idLanguage ).val( model.fullDesc[idLanguage] );
				for( var idLanguage in model.title ) self.$ns.find( options.ins+'.wtitle.w'+idLanguage ).val( model.title[idLanguage] );
				for( var idLanguage in model.metaTitle ) self.$ns.find( options.ins+'.wmetaTitle.w'+idLanguage ).val( model.metaTitle[idLanguage] );
				for( var idLanguage in model.metaDescription ) self.$ns.find( options.ins+'.wmetaDescription.w'+idLanguage ).val( model.metaDescription[idLanguage] );
				for( var idLanguage in model.metaKeywords ) self.$ns.find( options.ins+'.wmetaKeywords.w'+idLanguage ).val( model.metaKeywords[idLanguage] );
				for( var idLanguage in model.shortDesc ) self.$ns.find( options.ins+'.wshortDesc.w'+idLanguage ).val( model.shortDesc[idLanguage] );
				for( var idLanguage in model.orders ) self.$ns.find( options.ins+'.worders.w'+idLanguage ).val( model.orders[idLanguage] );
				
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'],input[type='checkbox'],select,textarea" ).val( '' );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					if(self.$inputIs_def.prop('checked')===true){
						self.$inputIs_def.val(1);
					}else{
						self.$inputIs_def.val(0);
					}
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.hide();
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: []
		};
		self.init();
		return self;
	}
}( window.jQuery );