var wAdminContestFormWidgetOpen;
!function( $ ) {
	wAdminContestFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New contest',
			lEdit: 'Editor contest',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminContestFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/contest/iframeAdd',
			ajaxDeleteURL: '/admin/contest/ajaxDelete',
			ajaxLoadURL: '/admin/contest/ajaxLoad',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			ruleTemplate: '<li><div class="alignleft width80 content"><p><span class="ruleHeader">{ruleHeader}</span>:<span class="green_color alignright ruleVal">{ruleVal}</span></p></div><div class="alignright width10"><a class="icon-trash wDeleteRule" href="#"></a></div><div class="alignright width10"><a class="icon-edit wEditRule" href="#"></a></div></li>',
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				self.$tabsI18N = self.$ns.find( options.ins+'.wTabsI18N' );
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$selectStatus = self.$ns.find( options.ins+'.wStatus' );
				self.$startDeposit = self.$ns.find( options.ins+'.wStartDeposit' );
				
				self.$slug = self.$ns.find( options.ins+'.wslug' );
				
				self.$selectModeEA = self.$ns.find( options.ins+'.wModeEA' );
				self.$inputEndReg = self.$ns.find( options.ins+'.wEndReg' );
				self.$inputBegin = self.$ns.find( options.ins+'.wBegin' );
				self.$inputEnd = self.$ns.find( options.ins+'.wEnd' );
											
				self.$selectServer = self.$ns.find( options.ins+'.wServer' );
				self.$bAddServer = self.$ns.find( options.ins+'.wAddServer' );
				self.$inputServers = self.$ns.find( options.ins+'.wServers' );
				self.$tabServers = self.$ns.find( options.ins+'.wTabServers' );
				
				self.$inputPrize = self.$ns.find( options.ins+'.wPrize' );
				self.$bAddPrize = self.$ns.find( options.ins+'.wAddPrize' );
				self.$inputPrizes = self.$ns.find( options.ins+'.wPrizes' );
				self.$tabPrizes = self.$ns.find( options.ins+'.wTabPrizes' );
				
				self.$inputsAccountOpening = self.$ns.find( options.ins+'.wAccountOpening' );
				self.$inputShowGraphLeaders = self.$ns.find( options.ins+'.wShowGraphLeaders' );
				self.$inputStopImport = self.$ns.find( options.ins+'.wStopImport' );
				self.$inputStopDeleteMMonitoring = self.$ns.find( options.ins+'.wStopDeleteMMonitoring' );
				self.$inputCompleteMembers = self.$ns.find( options.ins+'.wCompleteMembers' );
				
				self.$tabsRules = self.$ns.find( options.ins+'.wTabsRules' );
				
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bAddServer.click( self.addCurrentServer );
				self.$bAddPrize.click( self.addCurrentPrize );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				
				self.$tabServers.on( "click", ".wDelete", self.deleteSpecificServer );
				self.$tabPrizes.on( "click", ".wDelete", self.deleteSpecificPrize );
				
				self.$tabsRulesContent = self.$ns.find( options.ins+'.wTabsRulesContent' );
				self.$tabsRulesContent.on( "click", ".wDeleteRule", self.deleteRule );
				self.$tabsRulesContent.on( "click", ".wEditRule", self.editRule );
				
				self.$bAddRule = self.$ns.find( options.ins+'.wAddRule' );
				self.$bAddRule.click( self.addCurrentRule );
			},
			generateRuleVal: function(){
				self.$tabsRulesContent.find('.tab-pane').each( function() {
					var $this = $(this);
					var idLanguage = $this.attr('data-langId');
					var valArr = [];
					$this.find('.conditions_list_green li').each( function() {
						valArr.push( { header: $(this).find('.ruleHeader').text(), val: $(this).find('.ruleVal').text(), type: 'green' } );
					});
					$this.find('.conditions_list_red li').each( function() {
						valArr.push( { header: $(this).find('.ruleHeader').text(), val: $(this).find('.ruleVal').text(), type: 'red' } );
					});
					self.$ns.find( options.ins+'.wrules'+idLanguage ).val( JSON.stringify(valArr) );
				});	
			},
			ruleValToHtml:function( mRules ){
				self.$ns.find( options.ins+'.conditions_list_green' ).html('');
				self.$ns.find( options.ins+'.conditions_list_red' ).html('');
				$.each( mRules, function( idLanguage, rules ) { 	
					if( rules != '' ) {
						parsedRules = JSON.parse(rules);
						$.each( parsedRules, function( index, rule ) { 
							var appendStr = self.ruleTemplate;
							appendStr = appendStr.replace( '{ruleHeader}', rule.header );
							appendStr = appendStr.replace( '{ruleVal}', rule.val );
							self.$ns.find( options.ins+'.'+rule.type+'List'+idLanguage ).append( appendStr );
						});
					}
				});
			},
			editRule: function(){
				var parentUl = $(this).closest('ul');
				var parentLI = $(this).closest('li');
				if( parentUl.hasClass('conditions_list_green') ){
					var type = 'green';
				}else{
					var type = 'red';
				}
				
				var idLanguage = parentUl.attr('data-langid');
				var ruleHeader = parentLI.find('div.content p span.ruleHeader').text();
				var ruleVal = parentLI.find('div.content p span.ruleVal').text();
				self.$ns.find( options.ins+'.wruleHeader'+idLanguage ).val( ruleHeader );
				self.$ns.find( options.ins+'.wruleValue'+idLanguage ).val( ruleVal );
				self.$ns.find( options.ins+'.wruleType'+idLanguage ).val( type );
				parentLI.remove();
				
				return false;
			},
			deleteRule:function(){
				$(this).closest('li').remove();
				return false;
			},
			addCurrentRule:function() {
				
				var idLanguage = $(this).attr('data-langId');
				var $wruleHeader = self.$ns.find( options.ins+'.wruleHeader'+idLanguage );
				var $wruleValue = self.$ns.find( options.ins+'.wruleValue'+idLanguage );
				
				var ruleHeader = $wruleHeader.val();
				var ruleVal = self.$ns.find( options.ins+'.wruleValue'+idLanguage ).val();
				var ruleType = self.$ns.find( options.ins+'.wruleType'+idLanguage ).val();
				
				if( ruleHeader == '' ) return false;
				
				var appendStr = self.ruleTemplate;
				appendStr = appendStr.replace( '{ruleHeader}', ruleHeader);
				appendStr = appendStr.replace( '{ruleVal}', ruleVal);
		
				self.$ns.find( options.ins+'.'+ruleType+'List'+idLanguage ).append( appendStr );
				$wruleHeader.val('');
				$wruleValue.val('');
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						//self.$inputName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
				// servers
			getServersIDs:function() {
				var val = self.$inputServers.val();
				return val ? val.split(',') : [];
			},
			setServersIDs:function( ids ) {
				self.$inputServers.val( ids.join( ',' ));
			},
			clearServersIDs:function() {
				self.setServersIDs([]);
			},
			hasServerID:function( id ) {
				var ids = self.getServersIDs();
				return ids.indexOf( id ) !== -1;
			},
			addServerID:function( id ) {
				var ids = self.getServersIDs();
				ids.push( id );
				self.setServersIDs( ids );
			},
			deleteServerID:function( id ) {
				var ids = self.getServersIDs();
				var index = ids.indexOf( id );
				ids.splice( index, 1 );
				self.setServersIDs( ids );
			},
			addServerRow:function( id, title ) {
				var $row = self.$tabServers.find( '.wTpl' ).clone();
				$row.removeClass( 'wTpl' ).show();
				$row.attr( 'idServer', id );
				$row.find( '.wTDName' ).html( title );
				self.$tabServers.find( 'tbody' ).append( $row );
			},
			deleteServerRow:function( id ) {
				var $row = self.$tabServers.find( 'tr[idServer="'+id+'"]' );
				$row.remove();
			},
			clearServersRows:function() {
				self.$tabServers.find( 'tr[idServer]' ).remove();
			},
			addServer:function( id, title ) {
				if( !self.hasServerID( id )) {
					self.addServerID( id );
					self.addServerRow( id, title );
				}
			},
			deleteServer:function( id ) {
				if( self.hasServerID( id )) {
					self.deleteServerID( id );
					self.deleteServerRow( id );
				}
			},
			addCurrentServer:function() {
				var id = self.$selectServer.val();
				if( id ) {
					var $option = self.$selectServer.find( 'option:selected' );
					self.addServer( id, $option.html() );
				}
				return false;
			},
			deleteSpecificServer:function() {
				var $row = $(this).closest( "tr" );
				var id = $row.attr( 'idServer' );
				self.deleteServer( id );
				return false;
			},
				// prizes
			getPrizes:function() {
				var val = self.$inputPrizes.val();
				return val ? val.split(',') : [];
			},
			setPrizes:function( ids ) {
				self.$inputPrizes.val( ids.join( ',' ));
			},
			clearPrizes:function() {
				self.setPrizes([]);
			},
			addPrizeVal:function( val ) {
				var ids = self.getPrizes();
				ids.push( val );
				self.setPrizes( ids );
			},
			deletePrizeVal:function( index ) {
				var ids = self.getPrizes();
				ids.splice( index, 1 );
				self.setPrizes( ids );
			},
			addPrizeRow:function( val ) {
				var $row = self.$tabPrizes.find( '.wTpl' ).clone();
				$row.removeClass( 'wTpl' ).show();
				$row.find( '.wTDPrize' ).html( val );
				self.$tabPrizes.find( 'tbody' ).append( $row );
			},
			deletePrizeRow:function( index ) {
				var $row = self.$tabPrizes.find( 'tr:eq('+(index+1)+')' );
				$row.remove();
			},
			clearPrizesRows:function() {
				self.$tabPrizes.find( 'tr:gt(0)' ).remove();
			},
			addPrize:function( val ) {
				self.addPrizeVal( val );
				self.addPrizeRow( val );
			},
			deletePrize:function( index ) {
				self.deletePrizeVal( index );
				self.deletePrizeRow( index );
			},
			addCurrentPrize:function() {
				var val = parseFloat( self.$inputPrize.val( ));
				if( !isNaN( val )) {
					self.addPrize( val );
					self.$inputPrize.val( '' ).focus();
				}
				return false;
			},
			deleteSpecificPrize:function() {
				var $row = $(this).closest( "tr" );
				var index = self.$tabPrizes.find( 'tr:gt(0)' ).index( $row );
				self.deletePrize( index );
				return false;
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				
				$.each( model.names, function( idLanguage, name ) {
					var wName = self.$ns.find( options.ins+'.wName'+idLanguage );
					wName.val( name );
				});
				$.each( model.descriptions, function( idLanguage, description ) {
					var wDescription = self.$ns.find( options.ins+'.wDescription'+idLanguage );
					wDescription.val( description );
				});
				
				$.each( model.fullDesc, function( idLanguage, fullDesc ) { self.$ns.find( options.ins+'.wfullDesc'+idLanguage ).val( fullDesc ); });
				$.each( model.pageTitle, function( idLanguage, pageTitle ) { self.$ns.find( options.ins+'.wpageTitle'+idLanguage ).val( pageTitle ); });
				$.each( model.pageSeoTitle, function( idLanguage, pageSeoTitle ) { self.$ns.find( options.ins+'.wpageSeoTitle'+idLanguage ).val( pageSeoTitle ); });
				$.each( model.seoKeys, function( idLanguage, seoKeys ) { self.$ns.find( options.ins+'.wseoKeys'+idLanguage ).val( seoKeys ); });
				$.each( model.seoDesc, function( idLanguage, seoDesc ) { self.$ns.find( options.ins+'.wseoDesc'+idLanguage ).val( seoDesc ); });
				
				self.ruleValToHtml( model.rules );

				
				self.$selectStatus.val( model.status );
				self.$startDeposit.val( model.startDeposit );
				self.$slug.val( model.slug );
				self.$selectModeEA.val( model.modeEA );
				self.$inputEndReg.val( model.endReg );
				self.$inputBegin.val( model.begin );
				self.$inputEnd.val( model.end );
				
				var idsServers = model.servers ? model.servers.split(',') : [];
				$.each( idsServers, function( i, id ) {
					var $option = self.$selectServer.find( 'option[value="'+id+'"]' );
					self.addServer( id, $option.html() );
				});
				
				var prizes = model.prizes ? model.prizes.split(',') : [];
				$.each( prizes, function( i, val ) {
					self.addPrize( val );
				});
				
				self.$inputsAccountOpening.filter( '[value="'+model.accountOpening+'"]' ).prop( 'checked', true );
				self.$inputShowGraphLeaders.prop( 'checked', model.showGraphLeaders == '1' );
				self.$inputStopImport.prop( 'checked', model.stopImport == '1' );
				self.$inputStopDeleteMMonitoring.prop( 'checked', model.stopDeleteMMonitoring == '1' );
				self.$inputCompleteMembers.prop( 'checked', model.completedMembers == '1' );
				
				for( var idLanguage in model.images ) {
					var $wImageHref = self.$ns.find( options.ins+'.wImageHref.w'+idLanguage );
					$wImageHref.attr( 'href', model.images[idLanguage] );
					$wImageHref.html( commonLib.baseName( model.images[idLanguage]));
					$wImageHref.show();
				}
				
				
				$.each( model.urlsRules, function( idLanguage, urlRules ) {
					var wUrlRules = self.$ns.find( options.ins+'.wUrlRules'+idLanguage );
					wUrlRules.val( urlRules );
				});
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$tabsI18N.find( 'a:first' ).tab( 'show' );
					self.$tabsRules.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.$tabsI18N.find( 'a:first' ).tab( 'show' );
				self.$tabsRules.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'], input[type='file'], input[type='date'], textarea" ).val( '' );
				self.$selectStatus.find( 'option:first' ).prop( 'selected', true );
				self.$selectModeEA.find( 'option:first' ).prop( 'selected', true );
				self.$selectServer.find( 'option:first' ).prop( 'selected', true );
				self.$inputPrize.val( '' );
				self.$inputsAccountOpening.filter( '[value="manually trader"]' ).prop( 'checked', true );
				self.$inputShowGraphLeaders.prop( 'checked', true );
				self.$inputCompleteMembers.prop( 'checked', false );
				self.clearServersIDs();
				self.clearServersRows();
				self.clearPrizes();
				self.clearPrizesRows();
				var $wImageHref = self.$ns.find( options.ins+'.wImageHref' );
				$wImageHref.hide();
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				self.generateRuleVal();
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					options.ls.lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					self.newRecord = !self.$inputID.val();
					
					self.$form[0].action = yiiBaseURL+options.ajaxSubmitURL;
					self.$form[0].target = 'iframeForContest';
					self.$form[0].submit();
					
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );