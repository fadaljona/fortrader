var wAdminAdvertisementZoneFormWidgetOpen;
!function( $ ) {
	wAdminAdvertisementZoneFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New zone',
			lEdit: 'Editor zone',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminAdvertisementZoneFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/advertisementZone/ajaxAdd',
			ajaxDeleteURL: '/admin/advertisementZone/ajaxDelete',
			ajaxLoadURL: '/admin/advertisementZone/ajaxLoad',
			ajaxRenderPreviewURL: '/admin/advertisementZone/ajaxRenderPreview',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var htmlLoadingPreview;
		
		var self = {
			state: 'showAdd',
			loading: false,
			loadingPreview: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputName = self.$ns.find( options.ins+'.wName' );
				self.$selectTypeAds = self.$ns.find( options.ins+'.wTypeAds' );
				self.$selectTypeBlock = self.$ns.find( options.ins+'.wTypeBlock' );
				self.$selectSizeBlock = self.$ns.find( options.ins+'.wSizeBlock' );
				self.$selectCountAds = self.$ns.find( options.ins+'.wCountAds' );
				self.$selectTypeBorder = self.$ns.find( options.ins+'.wTypeBorder' );
				self.$inputColorBorder = self.$ns.find( options.ins+'.wColorBorder' );
				self.$inputRoundedShapeBlocks = self.$ns.find( options.ins+'.wRoundedShapeBlocks' );
				self.$inputMixedZone = self.$ns.find( options.ins+'.wMixedZone' );
				self.$inputColorHeader = self.$ns.find( options.ins+'.wColorHeader' );
				self.$inputColorHoverHeader = self.$ns.find( options.ins+'.wColorHoverHeader' );
				self.$inputSite = self.$ns.find( options.ins+'.wSite' );
				self.$inputColorBG = self.$ns.find( options.ins+'.wColorBG' );
				self.$inputColorText = self.$ns.find( options.ins+'.wColorText' );
				self.$inputColorURL = self.$ns.find( options.ins+'.wColorURL' );
				self.$inputColorHoverURL = self.$ns.find( options.ins+'.wColorHoverURL' );
				self.$selectFont = self.$ns.find( options.ins+'.wFont' );
				self.$selectSizeHeader = self.$ns.find( options.ins+'.wSizeHeader' );
				self.$selectSizeText = self.$ns.find( options.ins+'.wSizeText' );
																
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				self.$divBlockPreview = self.$ns.find( options.ins+'.wBlockPreview' );
				htmlLoadingPreview = self.$divBlockPreview.html();
				self.$divBlockPreview.html( '' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				
				self.$selectTypeBlock.change( self.onChangeTypeBlock );
				
				self.$ns.on( 'propertychange change input paste cute', 'input[type=text], input[type=color], select', self.onChange );
				self.$ns.on( 'click', 'input[type=checkbox]', self.onChange );
				
				self.$ns.find( options.ins+'.wReloadPreview' ).click( function() { self.loadPreview(); return false; } );
								
				self.onChangeTypeBlock();
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$inputName.val( model.name );
				self.$selectTypeAds.val( model.typeAds );
				self.$selectTypeBlock.val( model.typeBlock );
				self.$selectSizeBlock.val( model.sizeBlock );
				self.$selectCountAds.val( model.countAds );
				self.$selectTypeBorder.val( model.typeBorder );
				self.$inputColorBorder.val( model.colorBorder );
				self.$inputRoundedShapeBlocks.prop( 'checked', model.roundedShapeBlocks == '1' );
				self.$inputMixedZone.prop( 'checked', model.mixedZone == '1' );
				self.$inputColorHeader.val( model.colorHeader );
				self.$inputColorHoverHeader.val( model.colorHoverHeader );
				self.$inputSite.val( model.site );
				self.$inputColorBG.val( model.colorBG );
				self.$inputColorText.val( model.colorText );
				self.$inputColorURL.val( model.colorURL );
				self.$inputColorHoverURL.val( model.colorHoverURL );
				self.$selectFont.val( model.font );
				self.$selectSizeHeader.val( model.sizeHeader );
				self.$selectSizeText.val( model.sizeText );
				
				self.onChangeTypeBlock();
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					self.loadPreview();
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.loadPreview();
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'], select" ).val( '' );
				self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );
				
				self.$selectTypeBorder.val( 'All' );
				
				self.$inputRoundedShapeBlocks.prop( 'checked', true );
				self.$inputMixedZone.prop( 'checked', false );
				
				self.$inputColorBorder.val( '#e3e3e3' );
				self.$inputColorHeader.val( '#0088cc' );
				self.$inputColorHoverHeader.val( '#005580' );
				self.$inputColorBG.val( '#f5f5f5' );
				self.$inputColorText.val( '#333333' );
				self.$inputColorURL.val( '#0088cc' );
				self.$inputColorHoverURL.val( '#005580' );
				
				self.$selectSizeHeader.val( '14' );
				self.$selectSizeText.val( '14' );
				
				self.onChangeTypeBlock();
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.hide();
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			onChange:function() {
				var $obj = $(this);
				var $name = $obj.attr( 'name' );
				if( !/\[name\]/.test( $name )) {
					self.loadPreview();
				}
			},
			onChangeTypeBlock:function() {
				var typeBlock = self.$selectTypeBlock.val();
				if( typeBlock == 'Fixed' ) {
					self.$selectSizeBlock.parent().show();
					self.$selectCountAds.parent().hide();
				}
				else{
					self.$selectSizeBlock.parent().hide();
					self.$selectCountAds.parent().show();
				}
			},
			loadPreview:function() {
				if( self.loadingPreview ) return false;
				self.$divBlockPreview.html( htmlLoadingPreview );
				self.loadingPreview = true;
				$.post( yiiBaseURL+options.ajaxRenderPreviewURL, self.$form.serialize(), function ( html ) {
					self.loadingPreview = false;
					self.$divBlockPreview.html( html );
				});
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );