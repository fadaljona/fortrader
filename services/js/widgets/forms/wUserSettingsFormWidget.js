var wUserSettingsFormWidgetOpen;
!function( $ ) {
	wUserSettingsFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lReseted: 'Reseted',
			lError: 'Error!',
		};
		var defOption = {
			ins: '',
			selector: '.wUserSettingsFormWidget',
			ajax: false,
			ls: defLS,
			delayTooltips: 1000,
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				
				self.$bSubmit = self.$ns.find( '.wSubmit' );
				self.$bReset = self.$ns.find( '.wReset' );
				self.$userSettingsFormError = self.$ns.find( '.userSettingsFormError' );
				
				self.$bSubmit.click( self.submit );
				self.$bReset.click( self.reset );
			},
			disable: function() {
				self.$ns.append( self.spinner );
			},
			enable: function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				
				if( options.ajax ) {
					
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.$userSettingsFormError.html('');
		
					self.loading = true;
					$.post( options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else if( data.errors ) {
							data.errors.reverse();
							$.each( data.errors, function ( i, el ) {
								
								self.$userSettingsFormError.html( self.$userSettingsFormError.html() + '<br />' + el.error );
	
								$errorField.focus();
							});
						}
						else{
							$.each( self.onEdit, function() { this( data.id ); });
						}
					}, "json" );
					return false;
				}
			},
			reset:function() {
				if( self.loading ) return false;
				
				self.$form.find('input[type="checkbox"]').prop( 'checked', false ).trigger('refresh');
				return true;
			},
			onEdit: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );