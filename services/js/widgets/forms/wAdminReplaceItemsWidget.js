var wAdminReplaceItemsWidget;
!function( $ ) {
	wAdminReplaceItemsWidget = function( options ) {
		var defOption = {
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			init:function() {
                self.wrapper = $(options.selector);
                self.tbody = self.wrapper.find('tbody');
                
                self.wrapper.on('click','.list-cell__button .btn',function(){
                    self.addRemoveBtnClicked($(this));
                });

                self.wrapper.on('change', '.selectModelClass', function(){
                    self.modelClassChanged($(this));
                });
            },
            modelClassChanged: function (el) {
                if (el.val() == '') {
                    el.closest('tr').find('.selectObject').html(options.emptyOption);
                    return false;
                }

                if (options.objectOptions[el.val()] == undefined) {
                    el.closest('tr').find('.selectObject').html(options.emptyOption);
                    return false;
                }
                
                el.closest('tr').find('.selectObject').html(options.objectOptions[el.val()]);
            },
            addRemoveBtnClicked: function (btn) {
                if (btn.hasClass('btn-default')) {
                    self.addRow();
                } else {
                    self.removeRow(btn);
                }
                return false;
            },
            addRow: function () {
                self.tbody.append(options.row.replace(/{{index}}/g, self.tbody.find('tr').length));
            },
            removeRow: function (btn) {
                btn.closest('tr').remove();
                self.tbody.find('tr').each(function(index) {
                    var replace = options.field + '-[0-9]+-';
                    var re = new RegExp(replace,"g");

                    var replace2 = '\\[' + options.field + ']\\[[0-9]+]';
                    var re2 = new RegExp(replace2,"g");

                    var newContent = $(this).html().replace(re, options.field + '-' + index + '-');
                    newContent = newContent.replace(re2, '[' + options.field + '][' + index + ']');
                    $(this).html(newContent);
                });
            },
            removeAllRows: function () {
                self.tbody.find('tr').each(function(index) {
                    if (index) {
                        $(this).remove();
                    }
                });
            },

			load:function(val) {
                self.removeAllRows();
                
                $.each(val, function(index, value) {
                    if (index) {
                        self.addRow();
                    }

                    var $tr = self.tbody.find('tr').eq(index);

                    $tr.find('.selectModelClass').val(value.modelClass);
                    self.modelClassChanged($tr.find('.selectModelClass'));
                    $tr.find('.selectObject').val(value.object);
                    $tr.find('.objectNameInp').val(value.objectName);
                    $tr.find('.selectWhereToSearch').val(value.whereToSearch);
                    $tr.find('.selectLang').val(value.lang);
                });
			},
			clear:function() {
				self.removeAllRows();
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );