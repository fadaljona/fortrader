var wAdminInformersStyleFormWidgetOpen;
!function( $ ) {
	wAdminInformersStyleFormWidgetOpen = function( options ) {
		var defOption = {
			ins: '',
			selector: '.wAdminInformersStyleFormWidget',
			ajax: false,
			hidden: false,
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 10,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			state: 'showAdd',
			loading: false,
			prevCategoryTypes: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );	
				self.$viewFile = self.$ns.find( options.ins+'.wviewFile' );	
				self.$categoryTypes = self.$ns.find( options.ins+'.wcategoryTypes' );	
				self.$oneColumn = self.$ns.find( options.ins+'.woneColumn' );
				self.$hasDiffValue = self.$ns.find( options.ins+'.whasDiffValue' );	
				self.$hidden = self.$ns.find( options.ins+'.whidden' );
				self.$settings = self.$ns.find( options.ins+'.wsettings' );	
				self.$defaultStyle = self.$ns.find( options.ins+'.wdefaultStyle' );	
				self.$borderWidth = self.$ns.find( options.ins+'.wborderWidth' );	
				self.$width = self.$ns.find( options.ins+'.wwidth' );	
				self.$order = self.$ns.find( options.ins+'.worder' );	
				
				self.$categoryTypes.on('change', function() { self.categorySelected(); self.filterViewFiles(); });
				
				self.filterViewFiles();
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
			
			},
			categorySelected: function(){
				if( self.prevCategoryTypes != null && self.prevCategoryTypes != false ){
					var categoryTypesVal = self.$categoryTypes.val();
					if( self.prevCategoryTypes.indexOf('converter') != -1 && categoryTypesVal.length > 1 ){
						categoryTypesVal.splice(categoryTypesVal.indexOf('converter'), 1);
						self.$categoryTypes.val( categoryTypesVal );
					}else if( self.prevCategoryTypes.indexOf('converter') == -1 && categoryTypesVal.length > 1 && categoryTypesVal.indexOf('converter') != -1 ){
						self.$categoryTypes.val( categoryTypesVal.splice(categoryTypesVal.indexOf('converter'), 1) );
					}
						
				}
				self.prevCategoryTypes = self.$categoryTypes.val();
			},
			filterViewFiles: function(){
				var selectedType = self.$categoryTypes.val();
				
				self.$viewFile.find('optgroup').each(function( index ) {
					
					var needToDisable = false;
					
					for (var i in selectedType) {
						if( $(this).attr('label').indexOf( selectedType[i] ) == -1 )
							needToDisable = true;
					}
					
					if( needToDisable ){
						$(this).attr({'disabled':true});
					}else{
						$(this).attr({'disabled':false});
					}
					
				});
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$viewFile.val( model.viewFile );
				self.$oneColumn.prop( 'checked', model.oneColumn == '1' );
				self.$hasDiffValue.prop( 'checked', model.hasDiffValue == '1' );
				self.$hidden.prop( 'checked', model.hidden == '1' );
				self.$defaultStyle.prop( 'checked', model.defaultStyle == '1' );
				self.$settings.val( model.settings );
				self.$borderWidth.val( model.borderWidth );
				self.$width.val( model.width );
				self.$categoryTypes.val( model.categoryTypes );
				
				self.$order.val( model.order );
				
				self.filterViewFiles();
				
				for( var idLanguage in model.title ) self.$ns.find( options.ins+'.wtitle.w'+idLanguage ).val( model.title[idLanguage] );	

			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					
					return true;
				}
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( options.ajaxLoadURL, {id:id, formModelName: options.formModelName}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text']" ).val( '' );
				self.$form.find( "textarea" ).val( '' );
				self.$form.find( "select" ).val( '' );
				self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}else{
							self.hide();
	
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
							
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( options.ajaxDeleteURL, {id:id, modelName:options.modelName }, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );