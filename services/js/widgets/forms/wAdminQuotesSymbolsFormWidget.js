var wAdminQuotesSymbolsFormWidgetOpen;
!function( $ ) {
	wAdminQuotesSymbolsFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New Quotes',
			lEdit: 'Editor quotes',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?'
		};
		var defOption = {
			ins: '',
			selector: '.wAdminQuotesSymbolsFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/quotesSymbols/iframeAdd',
			ajaxDeleteURL: '/admin/quotesSymbols/ajaxDelete',
			ajaxLoadURL: '/admin/quotesSymbols/ajaxLoad',
			iconUriPath: '/services/uploads/quotes/icons',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
;
				self.$form = self.$ns.find( 'form' );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputName = self.$ns.find( options.ins+'.wName' );
				self.$inputHistName = self.$ns.find( options.ins+'.wHistName' );
				self.$inputYahooHistName = self.$ns.find( options.ins+'.wyahooHistName' );
				self.$inputTickName = self.$ns.find( options.ins+'.wTickName' );
				self.$inputCategory = self.$ns.find( options.ins+'.wCategory' );
				self.$inputPanel = self.$ns.find( options.ins+'.wPanel' );
                self.$inputSourceType = self.$ns.find( options.ins+'.wSourceType' );

				self.$bitzSymbol = self.$ns.find( options.ins+'.wbitzSymbol' );
                self.$convertToUsd = self.$ns.find( options.ins+'.wconvertToUsd' );
                
                self.$toCryptoCurrenciesPage = self.$ns.find( options.ins+'.wtoCryptoCurrenciesPage' );
                
                self.$okexSymbol = self.$ns.find( options.ins+'.wokexSymbol' );
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				
				
				self.$tabKeywords = self.$ns.find( options.ins+'.wTabKeywords' );
				self.$inputKeywords = self.$ns.find( options.ins+'.wKeywords' );
				self.$inpKeyword = self.$ns.find( options.ins+'.wKeyword' );
				self.$bAddKeyword = self.$ns.find( options.ins+'.wAddKeyword' );
				self.$bAddKeyword.click( self.addCurrentKeyword );
				self.$tabKeywords.on( "click", ".wDelete", self.deleteSpecificKeyword );

				self.$inputSourceType.change(function(){
                    if( $(this).val() == 'bitz' || $(this).val() == 'binance' ) {
                        self.$form.find('.convertToUsdWrap').show();

                        if ($(this).val() == 'bitz') {
                            self.$form.find('.bitzData').show();
                        } else {
                            self.$form.find('.bitzData').hide();
                        }
                    } else {
                        self.$form.find('.bitzData').hide();
                        self.$form.find('.convertToUsdWrap').hide();
                    }

                    if( $(this).val() == 'okex' ){
						self.$form.find('.okexData').show();
					}else{
						self.$form.find('.okexData').hide();
                    }
				});

				//self.$ns.find('.tab-content .tab-pane input[type="text"], .tab-content .tab-pane textarea').on('focusout', function() { if( self.$inputID.val() != '' ) self.submit(true); });
				
			},
			
			
			addCurrentKeyword:function() {
				var key = $(this).closest('.jsAddRows').find('input[type="text"]').val();
				if( key ) {
					self.addKeyword( key, $(this) );
				}
				return false;
			},
			addKeyword:function( key, clickedEl ) {
				if( !self.hasKeyword( key, clickedEl )) {
					self.addKeywordInp( key, clickedEl );
					self.addKeywordRow( key, clickedEl );
					clickedEl.closest('.jsAddRows').find('input[type="text"]').val('');
				}
			},
			hasKeyword:function( key, clickedEl ) {
				var keys = self.getKeywords(clickedEl);
				return keys.indexOf( key ) !== -1;
			},
			getKeywords:function(clickedEl) {
				var val = clickedEl.closest('.jsAddRows').find('input[type="hidden"]').val();
				return val ? val.split(',') : [];
			},
			addKeywordInp:function( key, clickedEl ) {
				var keys = self.getKeywords(clickedEl);
				keys.push( key );
				self.setKeywords( keys, clickedEl );
			},
			setKeywords:function( keys, clickedEl ) {
				clickedEl.closest('.jsAddRows').find('input[type="hidden"]').val( keys.join( ',' ) );
			},
			addKeywordRow:function( key, clickedEl ) {
				var $row = clickedEl.closest('.jsAddRows').find('.wTpl').clone();
				$row.removeClass( 'wTpl' ).show();
				$row.attr({'dataKey':key});
				$row.find( '.wTDName' ).text( key );
				clickedEl.closest('.jsAddRows').find('tbody').append( $row );
			},
			deleteSpecificKeyword:function() {
				var $row = $(this).closest( "tr" );
				var key = $row.find( '.wTDName' ).text();
				self.deleteKeyword( key, $(this) );
				return false;
			},	
			deleteKeyword:function( key, clickedEl ) {
				if( self.hasKeyword( key, clickedEl )) {
					self.deleteKeywordInp( key, clickedEl );
					self.deleteKeywordRow( key, clickedEl );
				}
			},
			deleteKeywordInp:function( key, clickedEl ) {
				var keys = self.getKeywords(clickedEl);
				var index = keys.indexOf( key );
				keys.splice( index, 1 );
				self.setKeywords( keys, clickedEl );
			},
			deleteKeywordRow:function( key, clickedEl ) {
				var $row = clickedEl.closest('.jsAddRows').find('tr[dataKey="'+key+'"]');
				$row.remove();
			},
			deleteAllKeywords: function() {
				self.$ns.find( ".jsAddRows .wDelete" ).trigger( 'click' );
			},
			
			
			
			
			
			
			
			
			
			
			
			
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$inputName.val( model.name );
				self.$inputHistName.val( model.histName );
				self.$inputYahooHistName.val( model.yahooHistName );
				self.$inputTickName.val( model.tickName );
				self.$inputCategory.val( model.category );
				self.$inputPanel.val( model.panel=='Yes'?1:2 );
                self.$inputSourceType.val( model.sourceType );
            

                if( model.sourceType == 'bitz' || model.sourceType == 'binance' ) {
                    self.$form.find('.convertToUsdWrap').show();

                    if (model.sourceType == 'bitz') {
                        self.$form.find('.bitzData').show();
                    } else {
                        self.$form.find('.bitzData').hide();
                    }
                } else {
                    self.$form.find('.bitzData').hide();
                    self.$form.find('.convertToUsdWrap').hide();
                }




				self.$bitzSymbol.val( model.bitzSymbol );
                self.$convertToUsd.prop( 'checked', model.convertToUsd == '1' );

                self.$toCryptoCurrenciesPage.prop( 'checked', model.toCryptoCurrenciesPage == '1' );
                

                if( model.sourceType == 'okex' ){
                    self.$form.find('.okexData').show();
				}else{
					self.$form.find('.okexData').hide();
                }
				self.$okexSymbol.val( model.okexSymbol );
				
				for( var idLanguage in model.nalias ) self.$ns.find( options.ins+'.wnalias.w'+idLanguage ).val( model.nalias[idLanguage] );
				for( var idLanguage in model.desc ) self.$ns.find( options.ins+'.wdesc.w'+idLanguage ).val( model.desc[idLanguage] );
				for( var idLanguage in model.default ) self.$ns.find( options.ins+'.wdefault.w'+idLanguage ).val( model.default[idLanguage]=='Yes'?1:2 );
				for( var idLanguage in model.visible ) self.$ns.find( options.ins+'.wvisible.w'+idLanguage ).val( model.visible[idLanguage]=='Unvisible'?2:1 );
				for( var idLanguage in model.toChartList ) self.$ns.find( options.ins+'.wtoChartList.w'+idLanguage ).prop( 'checked', model.toChartList[idLanguage] == '1' );
				for( var idLanguage in model.toInformer ) self.$ns.find( options.ins+'.wtoInformer.w'+idLanguage ).prop( 'checked', model.toInformer[idLanguage] == '1' );
				for( var idLanguage in model.title ) self.$ns.find( options.ins+'.wtitle.w'+idLanguage ).val( model.title[idLanguage] );
				for( var idLanguage in model.metaTitle ) self.$ns.find( options.ins+'.wmetaTitle.w'+idLanguage ).val( model.metaTitle[idLanguage] );
				for( var idLanguage in model.metaDescription ) self.$ns.find( options.ins+'.wmetaDescription.w'+idLanguage ).val( model.metaDescription[idLanguage] );
				for( var idLanguage in model.tooltip ) self.$ns.find( options.ins+'.wtooltip.w'+idLanguage ).val( model.tooltip[idLanguage] );
				for( var idLanguage in model.preview ) self.$ns.find( options.ins+'.wpreview.w'+idLanguage ).val( model.preview[idLanguage] );
				for( var idLanguage in model.chartTitle ) self.$ns.find( options.ins+'.wchartTitle.w'+idLanguage ).val( model.chartTitle[idLanguage] );
				for( var idLanguage in model.wpPostsTitle ) self.$ns.find( options.ins+'.wwpPostsTitle.w'+idLanguage ).val( model.wpPostsTitle[idLanguage] );
				for( var idLanguage in model.metaKeywords ) self.$ns.find( options.ins+'.wmetaKeywords.w'+idLanguage ).val( model.metaKeywords[idLanguage] );
				for( var idLanguage in model.chartLabel ) self.$ns.find( options.ins+'.wchartLabel.w'+idLanguage ).val( model.chartLabel[idLanguage] );
				for( var idLanguage in model.searchPhrases ) self.$ns.find( options.ins+'.wsearchPhrases.w'+idLanguage ).val( model.searchPhrases[idLanguage] );
				for( var idLanguage in model.weightInCat ) self.$ns.find( options.ins+'.wweightInCat.w'+idLanguage ).val( model.weightInCat[idLanguage] );
                for( var idLanguage in model.informerName ) self.$ns.find( options.ins+'.winformerName.w'+idLanguage ).val( model.informerName[idLanguage] );
                
                for( var idLanguage in model.cryptoCurrencyTitle ) self.$ns.find( options.ins+'.wcryptoCurrencyTitle.w'+idLanguage ).val( model.cryptoCurrencyTitle[idLanguage] );
                for( var idLanguage in model.cryptoCurrencySubTitle ) self.$ns.find( options.ins+'.wcryptoCurrencySubTitle.w'+idLanguage ).val( model.cryptoCurrencySubTitle[idLanguage] );
				
				self.deleteAllKeywords();
				for( var idLanguage in model.keywords ){
					var keys = model.keywords[idLanguage] ? model.keywords[idLanguage].split(',') : [];
					var addKeyBtn = self.$ns.find( ".tab-pane[data-langid="+idLanguage+"] .jsAddRows.keys .wAddKeyword" );
					$.each( keys, function( i, key ) {
						if( key.length ) self.addKeyword(key, addKeyBtn);
					});
				}
				

				var histAliases = model.histAliases ? model.histAliases.split(',') : [];
				var addKeyBtn = self.$ns.find( ".jsAddRows.histAliases .wAddKeyword" );
				$.each( histAliases, function( i, key ) {
					if( key.length ) self.addKeyword(key, addKeyBtn);
				});

				if( model.nameIcon ){
					self.$ns.find('.wnameIconToAdmin').attr({src:options.iconUriPath+'/'+model.nameIcon}).removeClass('hide');
				}else{
					self.$ns.find('.wnameIconToAdmin').addClass('hide');
				}
			},
			
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'],select,textarea,input[type='hidden']" ).val( '' );
				self.$form.find('.wTabKeywords tr').each(function( index ){
					if( !$(this).hasClass('wTpl') ) $(this).remove();
                });
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					options.ls.lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					self.newRecord = !self.$inputID.val();
					
					self.$form[0].action = yiiBaseURL+options.ajaxSubmitURL;
					self.$form[0].target = 'iframeForSymbol';
					self.$form[0].submit();
					
					return false;
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			/*submit:function( autoSave = false ) {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							if( autoSave === false ){
								self.hide();
								if( newRecord ) {
									$.each( self.onAdd, function() { this( data.id ); });
								}
								else{
									$.each( self.onEdit, function() { this( data.id ); });
								}
							}
						}
					}, "json" );
					return false;
				}
			},*/
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: []
		};
		self.init();
		return self;
	}
}( window.jQuery );