var wAdminPaymentSystemCurrencyFormWidget;
!function( $ ) {
	wAdminPaymentSystemCurrencyFormWidgetOpen = function( options ) {
		var defOption = {
			ins: '',
			selector: '.wAdminPaymentSystemCurrencyFormWidget',
			ajax: false,
			hidden: false,
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 10,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		
		var self = {
			state: 'showAdd',
			loading: false,
			prevCategoryTypes: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				
                self.$inputID = self.$ns.find( options.ins+'.wID' );	
                self.$slug = self.$ns.find( options.ins+'.wslug' );	
                self.$inputIdSystem = self.$ns.find( options.ins+'.widSystem' );
				
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
            
                self.$tabKeywords = self.$ns.find( options.ins+'.wTabKeywords' );
                self.$bAddKeyword = self.$ns.find( options.ins+'.wAddKeyword' );
				self.$bAddKeyword.click( self.addCurrentKeyword );
				self.$tabKeywords.on( "click", ".wDelete", self.deleteSpecificKeyword );
            },
            



            addCurrentKeyword:function() {
				var key = $(this).closest('.jsAddRows').find('input[type="text"]').val();
				if( key ) {
					self.addKeyword( key, $(this) );
				}
				return false;
			},
			addKeyword:function( key, clickedEl ) {
				if( !self.hasKeyword( key, clickedEl )) {
					self.addKeywordInp( key, clickedEl );
					self.addKeywordRow( key, clickedEl );
					clickedEl.closest('.jsAddRows').find('input[type="text"]').val('');
				}
			},
			hasKeyword:function( key, clickedEl ) {
				var keys = self.getKeywords(clickedEl);
				return keys.indexOf( key ) !== -1;
			},
			getKeywords:function(clickedEl) {
				var val = clickedEl.closest('.jsAddRows').find('input[type="hidden"]').val();
				return val ? val.split(',') : [];
			},
			addKeywordInp:function( key, clickedEl ) {
				var keys = self.getKeywords(clickedEl);
				keys.push( key );
				self.setKeywords( keys, clickedEl );
			},
			setKeywords:function( keys, clickedEl ) {
				clickedEl.closest('.jsAddRows').find('input[type="hidden"]').val( keys.join( ',' ) );
			},
			addKeywordRow:function( key, clickedEl ) {
				var $row = clickedEl.closest('.jsAddRows').find('.wTpl').clone();
				$row.removeClass( 'wTpl' ).show();
				$row.attr({'dataKey':key});
				$row.find( '.wTDName' ).text( key );
				clickedEl.closest('.jsAddRows').find('tbody').append( $row );
			},
			deleteSpecificKeyword:function() {
				var $row = $(this).closest( "tr" );
				var key = $row.find( '.wTDName' ).text();
				self.deleteKeyword( key, $(this) );
				return false;
			},	
			deleteKeyword:function( key, clickedEl ) {
				if( self.hasKeyword( key, clickedEl )) {
					self.deleteKeywordInp( key, clickedEl );
					self.deleteKeywordRow( key, clickedEl );
				}
			},
			deleteKeywordInp:function( key, clickedEl ) {
				var keys = self.getKeywords(clickedEl);
				var index = keys.indexOf( key );
				keys.splice( index, 1 );
				self.setKeywords( keys, clickedEl );
			},
			deleteKeywordRow:function( key, clickedEl ) {
				var $row = clickedEl.closest('.jsAddRows').find('tr[dataKey="'+key+'"]');
				$row.remove();
			},
			deleteAllKeywords: function() {
				self.$ns.find( ".jsAddRows .wDelete" ).trigger( 'click' );
			},

	
			load:function( model ) {
                self.$inputID.val( model.id );
                self.$inputIdSystem.val( model.idSystem );
                self.$slug.val( model.slug );
                
				for( var idLanguage in model.title ) self.$ns.find( options.ins+'.wtitle.w'+idLanguage ).val( model.title[idLanguage] );	

                self.deleteAllKeywords();

                var aliases = model.aliases ? model.aliases.split(',') : [];
				var addKeyBtn = self.$ns.find( ".jsAddRows.aliases .wAddKeyword" );
				$.each( aliases, function( i, key ) {
					if( key.length ) self.addKeyword(key, addKeyBtn);
				});
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					
					return true;
				}
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( options.ajaxLoadURL, {id:id, formModelName: options.formModelName}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text']" ).val( '' );
				self.$form.find( "textarea" ).val( '' );
				self.$form.find( "select" ).val( '' );
                self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );

                self.deleteAllKeywords();
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}else{
							self.hide();
	
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
							
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( options.ajaxDeleteURL, {id:id, modelName:options.modelName }, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );