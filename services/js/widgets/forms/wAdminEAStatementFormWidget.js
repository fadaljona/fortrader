var wAdminEAStatementFormWidgetOpen;
!function( $ ) {
	wAdminEAStatementFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New settings',
			lEdit: 'Editor settings',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminEAStatementFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/eaStatement/iframeAdd',
			ajaxUploadStatementURL: '/admin/eaStatement/iframeUploadStatment',
			ajaxDeleteURL: '/admin/eaStatement/ajaxDelete',
			ajaxLoadURL: '/admin/eaStatement/ajaxLoad',
			ajaxLoadVersionsURL: '/admin/eaStatement/ajaxLoadVersions',
			ajaxLoadServersURL: '/admin/eaStatement/ajaxLoadServers',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$tabs = self.$ns.find( options.ins+'.wTabs' );
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$selectIDEA = self.$ns.find( options.ins+'.wIDEA' );
				self.$selectIDVersion = self.$ns.find( options.ins+'.wIDVersion' );
				self.$inputNameUser = self.$ns.find( options.ins+'.wNameUser' );
				self.$inputSymbol = self.$ns.find( options.ins+'.wSymbol' );
				self.$selectPeriod = self.$ns.find( options.ins+'.wPeriod' );
				self.$selectType = self.$ns.find( options.ins+'.wType' );
				self.$selectIDBroker = self.$ns.find( options.ins+'.wIDBroker' );
				self.$selectIDServer = self.$ns.find( options.ins+'.wIDServer' );
				self.$inputDate = self.$ns.find( options.ins+'.wDate' );
				self.$inputGain = self.$ns.find( options.ins+'.wGain' );
				self.$inputDrow = self.$ns.find( options.ins+'.wDrow' );
				self.$inputTrades = self.$ns.find( options.ins+'.wTrades' );
				self.$inputOptimizationStartDate = self.$ns.find( options.ins+'.wOptimizationStartDate' );
				self.$inputOptimizationEndDate = self.$ns.find( options.ins+'.wOptimizationEndDate' );
				self.$inputTestStartDate = self.$ns.find( options.ins+'.wTestStartDate' );
				self.$inputTestEndDate = self.$ns.find( options.ins+'.wTestEndDate' );
				
				self.$wSetHref = self.$ns.find( options.ins+'.wSetHref' );
				self.$wStatementHref = self.$ns.find( options.ins+'.wStatementHref' );
				self.$wGraphHref = self.$ns.find( options.ins+'.wGraphHref' );
				
				self.$textareaDesc = self.$ns.find( options.ins+'.wDesc' );
							
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				self.$wStatementLoading = self.$ns.find( options.ins+'.wStatementLoading' );
				self.$wStatement = self.$ns.find( options.ins+'.wStatement' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				self.$form.on( 'submit', self.onSubmit );
				
				self.$selectIDEA.change( self.onChangeIDEA );
				self.$selectIDBroker.change( self.onChangeIDBroker );
				
				self.$wStatement.change( self.uploadStatement );
				
				self.$textareaDesc.ckeditor({
					language: yiiLanguage,
				});
				self.loadVersions();
				self.loadServers();
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						//self.$inputOfficialName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			onChangeIDEA:function() {
				self.loadVersions();
			},
			onChangeIDBroker:function() {
				self.loadServers();
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				for( var idLanguage in model.titles ) {
					var $wTitle = self.$ns.find( options.ins+'.wTitle.w'+idLanguage );
					$wTitle.val( model.titles[idLanguage] );
				}
				for( var idLanguage in model.desces ) {
					var $wDesc = self.$ns.find( options.ins+'.wDesc.w'+idLanguage );
					$wDesc.val( model.desces[idLanguage] );
				}
				self.$selectIDEA.val( model.idEA );
				self.loadVersions( model.idVersion );
				self.$inputNameUser.val( model.nameUser );
				
				self.$inputSymbol.val( model.symbol );
				self.$selectPeriod.val( model.period );
				self.$selectType.val( model.type );
				self.$selectIDBroker.val( model.idBroker );
				self.loadServers( model.idServer );
				self.$inputDate.val( model.date );

				if( model.set ) {
					self.$wSetHref.attr( 'href', model.set );
					self.$wSetHref.html( commonLib.baseName( model.set ) );
					self.$wSetHref.show();
				}
				
				self.$inputGain.val( model.gain );
				self.$inputDrow.val( model.drow );
				self.$inputTrades.val( model.trades );
				
				if( model.statement ) {
					self.$wStatementHref.attr( 'href', model.statement );
					self.$wStatementHref.html( commonLib.baseName( model.statement ) );
					self.$wStatementHref.show();
				}
				
				if( model.graph ) {
					self.$wGraphHref.attr( 'href', model.graph );
					self.$wGraphHref.html( commonLib.baseName( model.graph ) );
					self.$wGraphHref.show();
				}
				
				self.$inputOptimizationStartDate.val( model.optimizationStartDate );
				self.$inputOptimizationEndDate.val( model.optimizationEndDate );
				self.$inputTestStartDate.val( model.testStartDate );
				self.$inputTestEndDate.val( model.testEndDate );
			},
			loadVersions:function( idVersionSelected ) {
				var idEA = self.$selectIDEA.val();
				if( idEA ) {
					var storedValue = self.$selectIDVersion.val();
					self.$selectIDVersion.html( '<option>'+options.ls.lLoading+'</option>' );
					self.$selectIDVersion.attr( 'disabled', 'disabled' );
					$.getJSON( yiiBaseURL+options.ajaxLoadVersionsURL, {idEA:idEA}, function ( data ) {
						self.$selectIDVersion.removeAttr( 'disabled' );
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.$selectIDVersion.html( '' );
							if( data.versions ) {
								self.$selectIDVersion.removeClass( 'error' );
								$.each( data.versions, function( id, name ) {
									var $option = $( '<option value="'+id+'">'+name+'</option>' );
									self.$selectIDVersion.append( $option );
								});
								if( idVersionSelected ) {
									self.$selectIDVersion.val( idVersionSelected );
								}
								else if( storedValue ) {
									self.$selectIDVersion.val( storedValue );
								}
							}
						}
					});
				}
				else{
					self.$selectIDVersion.find( 'option' ).remove();
				}
			},
			loadServers:function( idServerSelected ) {
				var idBroker = self.$selectIDBroker.val();
				if( idBroker ) {
					var storedValue = self.$selectIDServer.val();
					self.$selectIDServer.html( '<option>'+options.ls.lLoading+'</option>' );
					self.$selectIDServer.attr( 'disabled', 'disabled' );
					$.getJSON( yiiBaseURL+options.ajaxLoadServersURL, {idBroker:idBroker}, function ( data ) {
						self.$selectIDServer.removeAttr( 'disabled' );
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.$selectIDServer.html( '' );
							if( data.servers ) {
								self.$selectIDServer.removeClass( 'error' );
								$.each( data.servers, function( id, name ) {
									var $option = $( '<option value="'+id+'">'+name+'</option>' );
									self.$selectIDServer.append( $option );
								});
								if( idServerSelected ) {
									self.$selectIDServer.val( idServerSelected );
								}
								else if( storedValue ) {
									self.$selectIDServer.val( storedValue );
								}
							}
						}
					});
				}
				else{
					self.$selectIDServer.find( 'option' ).remove();
				}
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$tabs.find( 'a:first' ).tab( 'show' );
					self.$title.html( options.ls.lNew );
					self.clear();
					self.loadVersions();
					self.loadServers();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.$tabs.find( 'a:first' ).tab( 'show' );
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'], input[type='file'], input[type='date'], textarea" ).val( '' );
				self.$selectIDEA.val('');
				self.$inputNameUser.val( options.defaultNameUser );
				self.$selectPeriod.val( '' );
				self.$selectType.val( '' );
				self.$selectIDBroker.val( '' );
				self.$wSetHref.hide();
				self.$wStatementHref.hide();
				self.$wGraphHref.hide();
				self.$inputDate.val( commonLib.getCurrentDate() );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			uploadStatement:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					self.loading = true;
					
					self.$form[0].action = yiiBaseURL+options.ajaxUploadStatementURL;
					self.$form[0].target = 'iframeForStatement';
					self.$form[0].submit();
					
					self.$wStatementLoading.show();
				}
			},
			uploadStatementComplete:function() {
				self.loading = false;
				self.enable();
				self.$wStatementLoading.hide();
			},
			uploadStatementError:function( error ) {
				self.uploadStatementComplete();
				alert( error );
			},
			uploadStatementSuccess:function( data ) {
				self.uploadStatementComplete();
				
				if( data.idEA ) self.$selectIDEA.val( data.idEA );
				if( data.idVersion ) self.loadVersions( data.idVersion );
				
				if( data.idBroker ) self.$selectIDBroker.val( data.idBroker );
				if( data.idServer ) self.loadServers( data.idServer );
				
				if( data.symbol ) self.$inputSymbol.val( data.symbol );
				if( data.period ) self.$selectPeriod.val( data.period );
				
				if( data.type ) self.$selectType.val( data.type );
				
				if( data.testStartDate ) self.$inputTestStartDate.val( data.testStartDate );
				if( data.testEndDate ) self.$inputTestEndDate.val( data.testEndDate );
				
				if( data.gain ) self.$inputGain.val( data.gain );
				if( data.drow ) self.$inputDrow.val( data.drow );
				if( data.trades ) self.$inputTrades.val( data.trades );
			},
			submit:function() {
				if( self.loading ) return false;
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					options.ls.lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.newRecord = !self.$inputID.val();
					
					self.$form[0].action = yiiBaseURL+options.ajaxSubmitURL;
					self.$form[0].target = 'iframeForStatement';
					return true;
				}
			},
			onSubmit:function() {
				self.disable();
				self.loading = true;
				return true;
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );