var wAdminMailingFormWidgetOpen;
!function( $ ) {
	wAdminMailingFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New mailing',
			lEdit: 'Editor mailing',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
			lCharsIP: 'chars',
			lCharsRP: 'chars',
			lCharsMRP: 'chars',
			lNumSendSMS: 'Will send {d} SMS',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminMailingFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/mailing/ajaxAdd',
			ajaxDeleteURL: '/admin/mailing/ajaxDelete',
			ajaxLoadURL: '/admin/mailing/ajaxLoad',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
			maxUnicodeMsgSingleLen: 70,
			maxUnicodeMsgPartLen: 63,
			delayCheckStat: 300,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$wTabsNav = self.$ns.find( options.ins+'.wTabsNav' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputIsSMS = self.$ns.find( options.ins+'.wIsSMS' );
				self.$inputIsEmail = self.$ns.find( options.ins+'.wIsEmail' );
				self.$selectUsersGroup = self.$ns.find( options.ins+'.wUsersGroup' );
				self.$selectIDType = self.$ns.find( options.ins+'.wIDType' );
				self.$textareaMessage = self.$ns.find( options.ins+'.wMessage' );
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				setInterval( self.checkStats, options.delayCheckStat );
				
				self.$textareaMessage.ckeditor({
					language: yiiLanguage,
				});
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$wTabsNav.find( 'a:first' ).tab( 'show' );
						self.$ns.find( 'input:eq(1)' ).focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$inputIsSMS.prop( 'checked', model.isSMS == '1' );
				self.$inputIsEmail.prop( 'checked', model.isEmail == '1' );
				self.$selectUsersGroup.val( model.usersGroup );
				self.$selectIDType.val( model.idType );
				if( model.names ) {
					for( var idLanguage in model.names ) {
						var wName = self.$ns.find( options.ins+'.wName.w'+idLanguage );
						wName.val( model.names[idLanguage] );
					}
				}
				if( model.messages ) {
					for( var idLanguage in model.messages ) {
						var wMessage = self.$ns.find( options.ins+'.wMessage.w'+idLanguage );						
						wMessage.val( model.messages[idLanguage] );
					}
				}
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$inputID.val( '' );
				self.$form.find( "input[type='text'],textarea" ).val( '' );
				self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );
				self.$selectUsersGroup.val( 0 );
				self.$selectIDType.val( 0 );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			detWord: function( x, ip, rp, mrp ) {
				var A = x % 100;
				var B = x % 10;
				if ( 11 <= A && A <= 20 ) return mrp;
				if ( B == 1 ) return ip;
				if ( 2 <= B && B <= 4 ) return rp;
				if ( 5 <= B && B <= 9 || B == 0 ) return mrp;
			},
			setStatVal:function( idLanguage, val ) {
				var $wStat = self.$ns.find( options.ins+'.wStat'+idLanguage );
				var word = self.detWord( val, options.ls.lCharsIP, options.ls.lCharsRP, options.ls.lCharsMRP );
				$wStat.removeClass( 'muted text-warning' );
				if( val <=options.maxUnicodeMsgSingleLen ) {
					$wStat.html( val+' '+word );
					$wStat.addClass( 'muted' );
				}
				else{
					$wStat.addClass( 'text-warning' );
					var send = options.ls.lNumSendSMS.replace( "{d}", Math.ceil( val / options.maxUnicodeMsgPartLen ))
					$wStat.html( val+' '+word+'. '+send );
				}
			},
			checkStats:function() {
				var $wMessages = self.$ns.find( options.ins+'.wMessage' );
				$wMessages.each( function () {
					var $wMessage = $(this);
					var matches = /wMessage w(\d+)/.exec( this.className );
					var id = matches[1];
					self.setStatVal( id, $(this).val().length );
				});
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.hide();
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );