var wUserProfileFormWidgetOpen;
!function( $ ) {
	wUserProfileFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lError: 'Error!',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wUserProfileFormWidget',
			ajax: false,
			itCurrentModel: false,
			cookieVarName: 'language',
			expires: 365,
			ls: defLS,
			delayTooltips: 1000,
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				
				
				
				self.$profileTabsWrapper = self.$ns.find('.mainSettingsProfileTabs');
				self.$profileTabsLinks = self.$profileTabsWrapper.find('a');
				self.$profileTabsContests = self.$ns.find('.settingsContentTabs > div');
				self.hideInactiveProfileTabs();	
				self.$profileTabsLinks.click( self.changeProfileTab );
				
				
				self.$wSelectLanguage = self.$ns.find( '.wSelectLanguage' );
				
				self.$bSubmit = self.$ns.find( '.wSubmit' );
				self.$bSubmit.click( self.submit );
			},
			hideInactiveProfileTabs: function(){
				var activeIndex = self.$profileTabsLinks.index( self.$ns.find('.inside-menu_sub-active') );
				
				self.$profileTabsContests.each(function( index, element ) {
					if( index == activeIndex ) 
						$(this).show();
					else
						$(this).hide();
				});
			},
			changeProfileTab: function(){
				self.$profileTabsLinks.removeClass('inside-menu_sub-active');
				$(this).addClass('inside-menu_sub-active');
				self.hideInactiveProfileTabs();
				return false;
			},
			
			
			disable: function() {
				self.$ns.append( self.spinner );
			},
			enable: function() {
				self.$ns.find('.spinner-wrap').remove();
			},
			submit:function() {
				
				if( self.loading ) return false;
				self.disable();

				if( options.ajax ) {
	
					self.$ns.find( '.form-error' ).text('');
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					
					self.loading = true;
					$.post( options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else if( data.errors ) {
							data.errors.reverse();
							$.each( data.errors, function ( i, el ) {
								var $errorField = self.$form.find( '*[name="'+el.field+'"]' );
								var $label = $errorField.closest( 'label' );
								var $errorTextContainer = $label.find( '.form-error' );
								$errorTextContainer.text(el.error);
								$errorField.focus();
							});
						}
						else{
							if( options.itCurrentModel ) {
								$.cookie( options.cookieVarName, self.$wSelectLanguage.val(), { expires: options.expires, path: "/" });
							}
							$.each( self.onEdit, function() { this( data.id ); });
						}
					}, "json" );
					return false;
				}
			},
			onEdit: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );
