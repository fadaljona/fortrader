var wAdminAdvertisementFormWidgetOpen;
!function( $ ) {
	wAdminAdvertisementFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New ad',
			lEdit: 'Editor ad',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminAdvertisementFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxDeleteURL: '/admin/advertisement/ajaxDelete',
			ajaxLoadURL: '/admin/advertisement/ajaxLoad',
			ajaxUploadImageURL: '/admin/advertisement/iframeUploadImage',
			ajaxPreviewImageURL: '/admin/advertisement/ajaxPreviewImage',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
			delayCheckStat: 300,
			type: 'Text',
			zones:[],
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		switch( options.type ) {
			case 'Text':{
				options.ajaxSubmitURL = '/admin/advertisement/ajaxAdd';
				break;
			}
			case 'Image':{
				options.ajaxSubmitURL = '/admin/advertisement/ajaxAddImage';
				break;
			}
			case 'Button':{
				options.ajaxSubmitURL = '/admin/advertisement/ajaxAddButton';
				break;
			}
			case 'Offer':{
				options.ajaxSubmitURL = '/admin/advertisement/ajaxAddOffer';
				break;
			}
		}
				
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$selectIDCampaign = self.$ns.find( options.ins+'.wIDCampaign' );
				self.$inputHeader = self.$ns.find( options.ins+'.wHeader' );
				self.$inputImage = self.$ns.find( options.ins+'.wImage' );
				self.$inputHoldedImage = self.$ns.find( options.ins+'.wHoldedImage' );
				self.$textareaText = self.$ns.find( options.ins+'.wText' );
				self.$inputUrl = self.$ns.find( options.ins+'.wUrl' );
				self.$inputMinScreenWidth = self.$ns.find( options.ins+'.wMinScreenWidth' );
				self.$inputMaxScreenWidth = self.$ns.find( options.ins+'.wMaxScreenWidth' );
				self.$inputLikes = self.$ns.find( options.ins+'.wLikes' );
                self.$inputShowEverywhere = self.$ns.find( options.ins+'.wShowEverywhere' );
                self.$inputIsDirectAdLink = self.$ns.find( options.ins+'.wIsDirectAdLink' );
				self.$inputDontShow = self.$ns.find( options.ins+'.wDontShow' );
				self.$inputHtmlBaner = self.$ns.find( options.ins+'.wHtmlBaner' );
				
				self.$wImageBlock = self.$ns.find( options.ins+'.wImageBlock' );
				self.$wUrlBlock = self.$ns.find( options.ins+'.wUrlBlock' );
				self.$wTextBlock = self.$ns.find( options.ins+'.wTextBlock' );
					
				self.$wImageLoading = self.$ns.find( options.ins+'.wImageLoading' );
				self.$wImageHolder = self.$ns.find( options.ins+'.wImageHolder' );
				self.$wWidth = self.$ns.find( options.ins+'.wWidth' );
				self.$wHeight = self.$ns.find( options.ins+'.wHeight' );
				self.$bPreview = self.$ns.find( options.ins+'.wPreview' );
				
				self.$selectZone = self.$ns.find( options.ins+'.wZone' );
				self.$bAddZone = self.$ns.find( options.ins+'.wAddZone' );
				self.$inputZones = self.$ns.find( options.ins+'.wZones' );
				self.$tabZones = self.$ns.find( options.ins+'.wTabZones' );
				
				self.$uploadIframeFileBlock = self.$ns.find( options.ins+'.wuploadIframeFileBlock' );
				self.$needUploadHtmlFile = self.$ns.find( options.ins+'.wneedUploadHtmlFile' );
				self.$uploadIframeFileBlockSettings = self.$ns.find( options.ins+'.wuploadIframeFileBlockSettings' );
				self.$uploadedFileSrc = self.$ns.find( options.ins+'.uploadedFileSrc' );
				
				self.$spanStatHeader = $( '<span></span>' );
				self.$inputHeader.after( self.$spanStatHeader );
				
				self.$spanStatText = $( '<span></span>' );
				self.$textareaText.after( self.$spanStatText );
					
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bAddZone.click( self.addCurrentZone );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				self.$bPreview.click( self.previewImage );
				
				self.$inputHtmlBaner.click( self.checkboxHtmlBaner );
				self.$needUploadHtmlFile.click( self.checkboxUploadIframe );
				
				self.$inputImage.change( self.uploadImage );
				self.$inputShowEverywhere.change( function() { if( $(this).prop( 'checked' )) self.deleteAllZones(); });
				self.$wImageHolder.find( 'img' ).on( 'load', function() {
					self.$wImageLoading.hide();
				});
				self.$tabZones.on( "click", ".wDelete", self.deleteSpecificZone );
				setInterval( self.checkStats, options.delayCheckStat );
			},
			checkboxUploadIframe:function(){
				if( self.$needUploadHtmlFile.is(':checked') ){
					self.$wTextBlock.find('textarea').attr({'disabled':true});
					self.$uploadIframeFileBlockSettings.show();
					
				}else{
					self.$wTextBlock.find('textarea').attr({'disabled':false});
					self.$uploadIframeFileBlockSettings.hide();
				}
			},
			checkboxHtmlBaner: function() {
				if( self.$inputHtmlBaner.is(':checked') ){
					self.$wTextBlock.show();
					self.$wImageBlock.hide();
					self.$wUrlBlock.hide();
					self.$uploadIframeFileBlock.show();
					self.$uploadIframeFileBlockSettings.hide();
				}else{
					self.$wTextBlock.hide();
					self.$wImageBlock.show();
					self.$wUrlBlock.show();
					
					self.$uploadIframeFileBlock.hide();
	
					self.$uploadIframeFileBlock.find( "input[type='text']" ).val( '' );
					self.$uploadIframeFileBlock.find( "textarea" ).val( '' );
					self.$uploadIframeFileBlock.find( "input[type='checkbox']" ).prop( 'checked', false );
				}
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputHeader.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
				// zones
			getZonesIDs:function() {
				var val = self.$inputZones.val();
				return val ? val.split(',') : [];
			},
			setZonesIDs:function( ids ) {
				self.$inputZones.val( ids.join( ',' ));
			},
			clearZonesIDs:function() {
				self.setZonesIDs([]);
			},
			hasZoneID:function( id ) {
				var ids = self.getZonesIDs();
				return ids.indexOf( id ) !== -1;
			},
			addZoneID:function( id ) {
				var ids = self.getZonesIDs();
				ids.push( id );
				self.setZonesIDs( ids );
			},
			deleteZoneID:function( id ) {
				var ids = self.getZonesIDs();
				var index = ids.indexOf( id );
				ids.splice( index, 1 );
				self.setZonesIDs( ids );
			},
			addZoneRow:function( id, title ) {
				var $row = self.$tabZones.find( '.wTpl' ).clone();
				$row.removeClass( 'wTpl' ).show();
				$row.attr( 'idZone', id );
				$row.find( '.wTDName' ).html( title );
				self.$tabZones.find( 'tbody' ).append( $row );
			},
			deleteZoneRow:function( id ) {
				var $row = self.$tabZones.find( 'tr[idZone="'+id+'"]' );
				$row.remove();
			},
			clearZonesRows:function() {
				self.$tabZones.find( 'tr[idZone]' ).remove();
			},
			addZone:function( id, title ) {
				if( !self.hasZoneID( id )) {
					self.addZoneID( id );
					self.addZoneRow( id, title );
				}
			},
			deleteZone:function( id ) {
				if( self.hasZoneID( id )) {
					self.deleteZoneID( id );
					self.deleteZoneRow( id );
				}
			},
			addCurrentZone:function() {
				var id = self.$selectZone.val();
				if( id ) {
					var $option = self.$selectZone.find( 'option:selected' );
					self.addZone( id, $option.html() );
					self.$inputShowEverywhere.prop( 'checked', false );
				}
				return false;
			},
			deleteSpecificZone:function() {
				var $row = $(this).closest( "tr" );
				var id = $row.attr( 'idZone' );
				self.deleteZone( id );
				return false;
			},
			deleteAllZones: function() {
				self.$tabZones.find( ".wDelete" ).trigger( 'click' );
			},
			
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$selectIDCampaign.val( model.idCampaign );
				self.$inputHeader.val( model.header );
				self.$textareaText.val( model.text );
				self.$inputUrl.val( model.url );
				self.$inputMinScreenWidth.val( model.minScreenWidth );
				self.$inputMaxScreenWidth.val( model.maxScreenWidth );
				self.$inputLikes.val( model.countLikes );
                self.$inputShowEverywhere.prop( 'checked', model.showEverywhere == '1' );
                self.$inputIsDirectAdLink.prop( 'checked', model.isDirectAdLink == '1' );
				self.$inputDontShow.prop( 'checked', model.dontShow == '1' );
				self.$inputHtmlBaner.prop( 'checked', model.htmlBaner == '1' );
				
				if( model.iframeFile != '' ){
					self.$uploadedFileSrc.html('<a href="'+model.iframeFile+'" target="_blank">'+model.iframeFile+'</a>');
				}
				
				
				if( $.inArray( options.type, [ 'Image', 'Button', 'Offer' ]) >= 0 ) {
					
					if( ( options.type == 'Image' || options.type == 'Button' ) && model.htmlBaner == '1' ){
						self.$wTextBlock.show();
						self.$wImageBlock.hide();
						self.$wUrlBlock.hide();
						
						if( model.iframeFile != '' ){
							self.$uploadIframeFileBlockSettings.show();
							self.$wUrlBlock.show();
							self.$needUploadHtmlFile.prop( 'checked', true );
						}else{
							self.$uploadIframeFileBlockSettings.hide();
						}
					}else{
						self.$uploadIframeFileBlock.hide();
						self.$wTextBlock.hide();
						self.$wImageBlock.show();
						self.$wUrlBlock.show();

						
						var pathImg = yiiBaseURL + '/' + model.image;
						self.$wImageLoading.show();
						self.$wImageHolder.find( 'object' ).hide();
						
						if( pathImg.substr(pathImg.lastIndexOf('.') + 1).toLowerCase() == 'swf' ){
							self.$wImageHolder.find( 'object' ).attr( 'data', pathImg );
							self.$wImageHolder.find( 'object' ).attr( 'width', model.width );
							self.$wImageHolder.find( 'object' ).attr( 'height', model.height );
							self.$wImageHolder.find( 'param' ).attr( 'value', pathImg );
							self.$wImageHolder.find( 'img' ).hide();
							self.$wImageHolder.find( 'object' ).show();
						}else{
							self.$wImageHolder.find( 'img' ).attr( 'src', pathImg );
						}
						
						self.$wImageLoading.hide();
						self.$wImageHolder.show();
						
						self.$wWidth.val( model.width );
						self.$wHeight.val( model.height );
						self.$inputHoldedImage.val( model.holdedImage );
					}
					
				}
				var idsZones = model.zones ? model.zones.split(',') : [];
				self.reloadZones();
				$.each( idsZones, function( i, id ) {
					var $option = self.$selectZone.find( 'option[value="'+id+'"]' );
					if( $option.length ) self.addZone( id, $option.html() );
				});
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$title.html( options.ls.lNew );
					self.clear();
					if( options.type == 'Image' ){
						self.$uploadIframeFileBlock.hide();
						self.$wTextBlock.hide();
						self.$wImageBlock.show();
						self.$wUrlBlock.show();
					}
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text'], textarea" ).val( '' );
				self.$form.find( "input[type='checkbox']" ).prop( 'checked', false );
				self.$selectIDCampaign.val( options.idCampaign );
				self.$wImageHolder.find( 'img' ).attr( 'src', '' );
				self.$wImageHolder.hide();
				self.$inputHoldedImage.val('');
                self.$inputShowEverywhere.prop( 'checked', true );
                self.$inputIsDirectAdLink.prop( 'checked', false );
				self.$selectZone.find( 'option:first' ).prop( 'selected', true );
				self.clearZonesIDs();
				self.clearZonesRows();
				self.$wTextBlock.find('textarea').removeAttr('disabled');
				self.$uploadedFileSrc.html('');
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			checkStats:function() {
				var toEndHeader = options.maxHeaderLen - self.$inputHeader.val().length
				self.$spanStatHeader.html( " " + toEndHeader );
				
				var toEndText = options.maxTextLen - self.$textareaText.val().length
				self.$spanStatText.html( " " + toEndText );
			},
			previewImage:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.loading = true;
					self.$wImageLoading.show();
					var path = self.$inputHoldedImage.val();
					var width = self.$wWidth.val();
					var height = self.$wHeight.val();
					$.getJSON( yiiBaseURL+options.ajaxPreviewImageURL, { path:path, width:width, height:height }, function ( data ) {
						self.loading = false;
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							var pathImg = yiiBaseURL + '/' + data.path //+ '?=' + Math.random();
							
							self.$wImageHolder.find( 'object' ).hide();
					
							if( pathImg.substr(pathImg.lastIndexOf('.') + 1).toLowerCase() == 'swf' ){
								self.$wImageHolder.find( 'object' ).attr( 'data', pathImg );
								self.$wImageHolder.find( 'object' ).attr( 'width', self.$wWidth.val() );
								self.$wImageHolder.find( 'object' ).attr( 'height', self.$wHeight.val() );
								self.$wImageHolder.find( 'param' ).attr( 'value', pathImg );
								self.$wImageHolder.find( 'img' ).hide();
								self.$wImageHolder.find( 'object' ).show();
							}else{
								self.$wImageHolder.find( 'img' ).attr( 'src', pathImg );
							}
								
							self.$wImageLoading.hide();
							
							self.reloadZones();
						}
					});
				}
				return false;
			},
			reloadZones:function() {
				if( $.inArray( options.type, [ 'Text', 'Offer' ]) >= 0 ) return;
				
				var width = self.$wWidth.val();
				var height = self.$wHeight.val();
				self.$selectZone.find( 'option' ).remove();
				$.each( options.zones, function( index, zone ) {
					if( zone.type != 'Fixed' || zone.width >= width &&  zone.height >= height ) {
						var $option = $( '<option></option>' );
						$option.html( zone.name );
						$option.attr( 'value', index );
						self.$selectZone.append( $option );
					}
				});
			},
			uploadImage:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					self.loading = true;
					
					self.$form[0].action = yiiBaseURL+options.ajaxUploadImageURL;
					self.$form[0].target = 'iframeForImage';
					self.$form[0].submit();
					
					self.$wImageLoading.show();
				}
			},
			uploadImageComplete:function() {
				self.loading = false;
				self.enable();
				self.$inputImage.val( '' );
			},
			uploadImageError:function( error ) {
				self.uploadImageComplete();
				alert( error );
				self.$wImageLoading.hide();
			},
			uploadImageSuccess:function( path, width, height ) {
				var pathImg = yiiBaseURL + '/' + path //+ '?='+Math.random();
				self.uploadImageComplete();
				self.$wImageHolder.find( 'img' ).attr( 'src', pathImg );
				
				self.$wImageHolder.find( 'object' ).hide();
					
				if( pathImg.substr(pathImg.lastIndexOf('.') + 1).toLowerCase() == 'swf' ){
					self.$wImageHolder.find( 'object' ).attr( 'data', pathImg );
					self.$wImageHolder.find( 'param' ).attr( 'value', pathImg );
					self.$wImageHolder.find( 'img' ).hide();
					self.$wImageHolder.find( 'object' ).show();
				}else{
					self.$wImageHolder.find( 'img' ).attr( 'src', pathImg );
				}
					
				self.$wImageLoading.hide();
				
				
				self.$wImageHolder.show();
				self.$wWidth.val( width );
				self.$wHeight.val( height );
				self.$inputHoldedImage.val( path );
				self.reloadZones();
			},
			submit:function() {
				if( self.loading ) return false;
				
				if( self.$needUploadHtmlFile.is(':checked') ){
					self.submitWithHtmlFile();
					return false;
				}
				
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.hide();
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
						}
					}, "json" );
					return false;
				}
			},
			submitWithHtmlFile:function() {
				
				self.$form[0].action = options.ajaxSubmitWithHtmlURL;
				self.$form[0].target = 'iframeForHtmlFile';
				self.$form[0].submit();
			
				return false;
				
			},
			submitComplete:function() {
				self.loading = false;
				self.enable();
				self.$bSubmit.html( options.ls.lSubmit );
			},
			submitError:function( error, errorField ) {
				self.submitComplete();
				
				alert( error );
				if( errorField ) {
					var $errorField = self.$form.find( '*[name="'+errorField+'"]' );
					$errorField.addClass( options.errorClass );
					$errorField.focus();
				}
			},
			submitSuccess:function( id ) {
				self.submitComplete();
				self.hide();
				if( self.newRecord ) {
					$.each( self.onAdd, function() { this( id ); });
				}
				else{
					$.each( self.onEdit, function() { this( id ); });
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );