var wCalendarGroupSubscriptionFormWidgetOpen;
!function( $ ) {
	wCalendarGroupSubscriptionFormWidgetOpen = function( options ) {
		var defLS = {
		};
		var defOption = {
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.spinner = '<div class="spinner-wrap"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>';
				self.$ns = $( options.selector );
                self.$form = self.$ns.find( 'form' );
                
                self.$timeBeforeEvent = self.$ns.find( '.wtimeBeforeEvent' );
                self.$timeWrapper = self.$form.find('.informer-calendar__time');
                self.$timeInputs = self.$timeWrapper.find('input[type="text"]');
                self.$timeInputs.keydown( function(e){ self.filterTimeInp(e); } );

                self.$serious = self.$form.find('.wserious');
                self.$seriousWrapper = self.$form.find('.informer-calendar__event');
                self.$seriousWrapper.find('li').click( self.seriousClicked );

                self.$countries = self.$form.find('.wcountries');
                self.$countriesWrapper = self.$form.find('.country_content');
                self.$selectResetCountries = self.$form.find('.country_btn');
                self.$selectResetCountries.click( self.selectResetBtnClicked );
                self.$countriesWrapper.find('input[type="checkbox"]').change( function(){ self.setCountries(); } );
                

                self.$form.submit(function(){return false;});
                self.$subscribe = self.$form.find('.wSubscribe');
                self.$unsubscribe = self.$form.find('.wUnsubscribe');
                self.$subscribe.click( self.subscribe );
                self.$unsubscribe.click( self.unSubscribe );

			},
			
			disable: function() {
				self.$ns.append( self.spinner );
			},
			enable: function() {
				self.$ns.find('.spinner-wrap').remove();
            },
            filterTimeInp: function(e){
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
					// Allow: Ctrl/cmd+A				
					(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
					// Allow: Ctrl/cmd+C
					(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
					// Allow: Ctrl/cmd+X
					(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
					// Allow: home, end, left, right
					(e.keyCode >= 35 && e.keyCode <= 39)) {
					// let it happen, don't do anything
					return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
            },
            setTimeBeforeEvent: function(){
				var $checkedTime = self.$timeWrapper.find('input[type="radio"]:checked');
				if( $checkedTime.attr('id') == 'time_minutes' ){
					var timeBeforeEvent = 60 * parseInt( $checkedTime.closest('.informer-calendar__time-item').find('input[type="text"]').val() );
				}else{
					var timeBeforeEvent = 60 * 60 * parseInt( $checkedTime.closest('.informer-calendar__time-item').find('input[type="text"]').val() );
				}
				self.$timeBeforeEvent.val( timeBeforeEvent );
            },
            setSerious: function(){
                var strVal = '';
                self.$seriousWrapper.find('li.active').each(function( index ) {
                    if( index == 0 ){
                        strVal = $(this).attr('data-val');
                    }else{
                        strVal = strVal + ',' + $(this).attr('data-val');
                    }
                });
                self.$serious.val( strVal );
            },
            setCountries: function(){
                var strVal = '';
                self.$countriesWrapper.find('input[type="checkbox"]:checked').each(function( index ) {
                    if( index == 0 ){
                        strVal = $(this).val();
                    }else{
                        strVal = strVal + ',' + $(this).val();
                    }
                });
                self.$countries.val( strVal );
            },
            seriousClicked: function(){
                $(this).toggleClass('active');
                self.setSerious();
            },
            selectResetBtnClicked: function(){
                var checkAll = true;
                if( $(this).hasClass('reset') ){
                    checkAll = false;
                }
                self.$countriesWrapper.find('input[type="checkbox"]').each(function( index ) {
                    $(this).prop( 'checked', checkAll );
                });
                $(this).toggleClass('reset');
                self.$countries.val( '' );
            },
			subscribe: function(){
				if( self.loading ) return false;
				self.disable();

				self.setTimeBeforeEvent();

				self.loading = true;
				$.post( options.subscribeUrl, self.$form.serialize(), function ( data ) {
					self.loading = false;

					self.enable();
					if( data.error ) {
						alert( data.error );
                    }else{
                        self.$subscribe.val( options.ls.lUpdate );
                        self.$unsubscribe.removeClass('hide');
                    }
				}, "json" );
				return false;
            },
            unSubscribe: function(){
				if( self.loading ) return false;
				self.disable();
	
				self.loading = true;

				$.post( options.unSubscribeUrl, {}, function ( data ) {
					self.loading = false;

					self.enable();
					if( data.error ) {
						alert( data.error );
					}else{
                        self.$subscribe.val( options.ls.lSubscribe );
                        self.$unsubscribe.addClass('hide');
					}
				}, "json" );
				return false;
			},
			
			
		};
		self.init();
		return self;
	}
}( window.jQuery );