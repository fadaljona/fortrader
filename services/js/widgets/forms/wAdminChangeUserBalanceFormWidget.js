var wAdminChangeUserBalanceFormWidgetOpen;
!function( $ ) {
	wAdminChangeUserBalanceFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminChangeUserBalanceFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/userBalance/ajaxChange',
			ajaxLoadURL: '/admin/userBalance/ajaxLoadChangeForm',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputLogin = self.$ns.find( options.ins+'.wLogin' );
				self.$inputValue = self.$ns.find( options.ins+'.wValue' );
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
								
				if( options.hidden ) self.hide( true );
				self.$bSubmit.click( self.submit );
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						self.$inputValue.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
			load:function( model ) {
				self.$inputID.val( model.id );
				self.$inputLogin.val( model.user_login );
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.hide();
						}
						else{
							self.load( data.model );
						}
					});
				}
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text']" ).val( '' );
			},
			disable: function() {
				$.each([ self.$bSubmit ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							self.hide();
							$.each( self.onEdit, function() { this( data.id ); });
						}
					}, "json" );
					return false;
				}
			},
			onEdit: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );