var wUnsubscribeCommentFormWidgetOpen;
!function( $ ) {
	wUnsubscribeCommentFormWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lError: 'Error!',
		};
		var defOption = {
			ins: '',
			selector: '.wUnsubscribeCommentFormWidget',
			ajax: false,
			ls: defLS,
			ajaxSubmitURL: '/user/ajaxAddUnsubscribeComment',
			ajaxCancelURL: '/user/ajaxCancelUnsubscribeMailingType',
			delayTooltips: 1000,
			errorClass: 'error',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$form = self.$ns.find( 'form' );
				
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bCancel = self.$ns.find( options.ins+'.wCancel' );
				
				self.$wStage1 = self.$ns.find( options.ins+'.wStage1' );
				self.$wStage2 = self.$ns.find( options.ins+'.wStage2' );
				self.$wStage3 = self.$ns.find( options.ins+'.wStage3' );
				
				self.$bSubmit.click( self.submit );
				self.$bCancel.click( self.cancel );
			},
			showStage: function( stage ) {
				self.$wStage1.hide();
				self.$wStage2.hide();
				self.$wStage3.hide();
				
				switch( stage ) {
					case 1:{
						self.$wStage1.show();
						break;
					}
					case 2:{
						self.$wStage2.show();
						break;
					}
					case 3:{
						self.$wStage3.show();
						break;
					}
				}
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bCancel ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bCancel ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();

				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				var lSubmit = self.$bSubmit.html();
				self.$bSubmit.html( options.ls.lSaving );
				
				self.loading = true;
				$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
					self.loading = false;
					self.$bSubmit.html( lSubmit );
					self.enable();
					if( data.error ) {
						alert( data.error );
					}
					else{
						$.each( self.onAdd, function() { this( data.id ); });
						self.showStage( 2 );
					}
				}, "json" );
				
				return false;
			},
			cancel:function() {
				if( self.loading ) return false;
				self.disable();
				
				var lCancel = self.$bCancel.html();
				self.$bCancel.html( options.ls.lCancel );
				
				var data = {
					key: options.key,
					idType: options.idType,
				};
				
				self.loading = true;
				$.getJSON( yiiBaseURL+options.ajaxCancelURL, data, function ( data ) {
					self.loading = false;
					self.$bCancel.html( lCancel );
					self.enable();
					if( data.error ) {
						alert( data.error );
					}
					else{
						$.each( self.onCancel, function() { this(); });
						self.showStage( 3 );
					}
				});
				
				return false;
			},
			onAdd: [],
			onCancel: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );