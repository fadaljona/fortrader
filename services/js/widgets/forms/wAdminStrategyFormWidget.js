var wAdminStrategyFormWidgetOpen;
!function( $ ) {
	wAdminStrategyFormWidgetOpen = function( options ) {
		var defLS = {
			lNew: 'New strategy',
			lEdit: 'Editor strategy',
			lDeleting: 'Deleting...',
			lDeleted: 'Deleted',
			lSaving: 'Saving...',
			lSaved: 'Saved',
			lLoading: 'Loading...',
			lDeleteConfirm: 'Delete?',
		};
		var defOption = {
			ins: '',
			selector: '.wAdminStrategyFormWidget',
			ajax: false,
			hidden: false,
			ls: defLS,
			ajaxSubmitURL: '/admin/strategy/ajaxAdd',
			ajaxDeleteURL: '/admin/strategy/ajaxDelete',
			ajaxLoadURL: '/admin/strategy/ajaxLoad',
			delayTooltips: 1000,
			errorClass: 'error',
			scrollSpeed: 200,
			scrollOffset: -50,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			state: 'showAdd',
			loading: false,
			init:function() {
				self.$ns = $( options.selector );
				self.$title = self.$ns.find( options.ins+'.wTitle' );
				self.$form = self.$ns.find( 'form' );
				
				self.$inputID = self.$ns.find( options.ins+'.wID' );
				self.$inputNameUser = self.$ns.find( options.ins+'.wNameUser' );
				self.$inputType = self.$ns.find( options.ins+'.wType' );
				
				self.$selectIndicator = self.$ns.find( options.ins+'.wIndicator' );
				self.$bAddIndicator = self.$ns.find( options.ins+'.wAddIndicator' );
				self.$inputIndicators = self.$ns.find( options.ins+'.wIndicators' );
				self.$tabIndicators = self.$ns.find( options.ins+'.wTabIndicators' );
				
				self.$selectTimeframe = self.$ns.find( options.ins+'.wTimeframe' );
				self.$bAddTimeframe = self.$ns.find( options.ins+'.wAddTimeframe' );
				self.$inputTimeframes = self.$ns.find( options.ins+'.wTimeframes' );
				self.$tabTimeframes = self.$ns.find( options.ins+'.wTabTimeframes' );
				
				self.$selectInstrument = self.$ns.find( options.ins+'.wInstrument' );
				self.$bAddInstrument = self.$ns.find( options.ins+'.wAddInstrument' );
				self.$inputInstruments = self.$ns.find( options.ins+'.wInstruments' );
				self.$tabInstruments = self.$ns.find( options.ins+'.wTabInstruments' );
				
				self.$inputIDEA = self.$ns.find( options.ins+'.wIDEA' );
				self.$inputPrice = self.$ns.find( options.ins+'.wPrice' );
												
				self.$bSubmit = self.$ns.find( options.ins+'.wSubmit' );
				self.$bDelete = self.$ns.find( options.ins+'.wDelete' );
				
				if( !!self.$inputID.val() ) {
					self.state = 'showEdit';
				}
				else{
					self.$bDelete.hide();
					self.state = 'showAdd';
				}
				if( options.hidden ) self.hide( true );
				self.$bAddIndicator.click( self.addCurrentIndicator );
				self.$bAddTimeframe.click( self.addCurrentTimeframe );
				self.$bAddInstrument.click( self.addCurrentInstrument );
				self.$bSubmit.click( self.submit );
				self.$bDelete.click( self.delete );
				
				self.$tabIndicators.on( "click", ".wDelete", self.deleteSpecificIndicator );
				self.$tabTimeframes.on( "click", ".wDelete", self.deleteSpecificTimeframe );
				self.$tabInstruments.on( "click", ".wDelete", self.deleteSpecificInstrument );
			},
			typedShow: function( complete ) {
				self.$ns.slideDown({ duration: options.scrollSpeed, easing: 'linear', complete: complete });
			},
			scrolledShow: function() {
				self.typedShow( function () {
					$.scrollTo( self.$ns, options.scrollSpeed, { offset: options.scrollOffset, easing: 'linear', onAfter: function () {
						//self.$inputName.focus();
					}});
				});
			},
			typedHide: function( immediately ) {
				if( immediately ) {
					self.$ns.hide();
				}
				else{
					self.$ns.slideUp();
				}
			},
				// indicators
			getIndicatorsIDs:function() {
				var val = self.$inputIndicators.val();
				return val ? val.split(',') : [];
			},
			setIndicatorsIDs:function( ids ) {
				self.$inputIndicators.val( ids.join( ',' ));
			},
			clearIndicatorsIDs:function() {
				self.setIndicatorsIDs([]);
			},
			hasIndicatorID:function( id ) {
				var ids = self.getIndicatorsIDs();
				return ids.indexOf( id ) !== -1;
			},
			addIndicatorID:function( id ) {
				var ids = self.getIndicatorsIDs();
				ids.push( id );
				self.setIndicatorsIDs( ids );
			},
			deleteIndicatorID:function( id ) {
				var ids = self.getIndicatorsIDs();
				var index = ids.indexOf( id );
				ids.splice( index, 1 );
				self.setIndicatorsIDs( ids );
			},
			addIndicatorRow:function( id, title ) {
				var $row = self.$tabIndicators.find( '.wTpl' ).clone();
				$row.removeClass( 'wTpl' ).show();
				$row.attr( 'idIndicator', id );
				$row.find( '.wTDName' ).html( title );
				self.$tabIndicators.find( 'tbody' ).append( $row );
			},
			deleteIndicatorRow:function( id ) {
				var $row = self.$tabIndicators.find( 'tr[idIndicator="'+id+'"]' );
				$row.remove();
			},
			clearIndicatorsRows:function() {
				self.$tabIndicators.find( 'tr[idIndicator]' ).remove();
			},
			addIndicator:function( id, title ) {
				if( !self.hasIndicatorID( id )) {
					self.addIndicatorID( id );
					self.addIndicatorRow( id, title );
				}
			},
			deleteIndicator:function( id ) {
				if( self.hasIndicatorID( id )) {
					self.deleteIndicatorID( id );
					self.deleteIndicatorRow( id );
				}
			},
			addCurrentIndicator:function() {
				var id = self.$selectIndicator.val();
				if( id ) {
					var $option = self.$selectIndicator.find( 'option:selected' );
					self.addIndicator( id, $option.html() );
				}
				return false;
			},
			deleteSpecificIndicator:function() {
				var $row = $(this).closest( "tr" );
				var id = $row.attr( 'idIndicator' );
				self.deleteIndicator( id );
				return false;
			},
				// timeframes
			getTimeframesIDs:function() {
				var val = self.$inputTimeframes.val();
				return val ? val.split(',') : [];
			},
			setTimeframesIDs:function( ids ) {
				self.$inputTimeframes.val( ids.join( ',' ));
			},
			clearTimeframesIDs:function() {
				self.setTimeframesIDs([]);
			},
			hasTimeframeID:function( id ) {
				var ids = self.getTimeframesIDs();
				return ids.indexOf( id ) !== -1;
			},
			addTimeframeID:function( id ) {
				var ids = self.getTimeframesIDs();
				ids.push( id );
				self.setTimeframesIDs( ids );
			},
			deleteTimeframeID:function( id ) {
				var ids = self.getTimeframesIDs();
				var index = ids.indexOf( id );
				ids.splice( index, 1 );
				self.setTimeframesIDs( ids );
			},
			addTimeframeRow:function( id, title ) {
				var $row = self.$tabTimeframes.find( '.wTpl' ).clone();
				$row.removeClass( 'wTpl' ).show();
				$row.attr( 'idTimeframe', id );
				$row.find( '.wTDName' ).html( title );
				self.$tabTimeframes.find( 'tbody' ).append( $row );
			},
			deleteTimeframeRow:function( id ) {
				var $row = self.$tabTimeframes.find( 'tr[idTimeframe="'+id+'"]' );
				$row.remove();
			},
			clearTimeframesRows:function() {
				self.$tabTimeframes.find( 'tr[idTimeframe]' ).remove();
			},
			addTimeframe:function( id, title ) {
				if( !self.hasTimeframeID( id )) {
					self.addTimeframeID( id );
					self.addTimeframeRow( id, title );
				}
			},
			deleteTimeframe:function( id ) {
				if( self.hasTimeframeID( id )) {
					self.deleteTimeframeID( id );
					self.deleteTimeframeRow( id );
				}
			},
			addCurrentTimeframe:function() {
				var id = self.$selectTimeframe.val();
				if( id ) {
					var $option = self.$selectTimeframe.find( 'option:selected' );
					self.addTimeframe( id, $option.html() );
				}
				return false;
			},
			deleteSpecificTimeframe:function() {
				var $row = $(this).closest( "tr" );
				var id = $row.attr( 'idTimeframe' );
				self.deleteTimeframe( id );
				return false;
			},
				// instruments
			getInstrumentsIDs:function() {
				var val = self.$inputInstruments.val();
				return val ? val.split(',') : [];
			},
			setInstrumentsIDs:function( ids ) {
				self.$inputInstruments.val( ids.join( ',' ));
			},
			clearInstrumentsIDs:function() {
				self.setInstrumentsIDs([]);
			},
			hasInstrumentID:function( id ) {
				var ids = self.getInstrumentsIDs();
				return ids.indexOf( id ) !== -1;
			},
			addInstrumentID:function( id ) {
				var ids = self.getInstrumentsIDs();
				ids.push( id );
				self.setInstrumentsIDs( ids );
			},
			deleteInstrumentID:function( id ) {
				var ids = self.getInstrumentsIDs();
				var index = ids.indexOf( id );
				ids.splice( index, 1 );
				self.setInstrumentsIDs( ids );
			},
			addInstrumentRow:function( id, title ) {
				var $row = self.$tabInstruments.find( '.wTpl' ).clone();
				$row.removeClass( 'wTpl' ).show();
				$row.attr( 'idInstrument', id );
				$row.find( '.wTDName' ).html( title );
				self.$tabInstruments.find( 'tbody' ).append( $row );
			},
			deleteInstrumentRow:function( id ) {
				var $row = self.$tabInstruments.find( 'tr[idInstrument="'+id+'"]' );
				$row.remove();
			},
			clearInstrumentsRows:function() {
				self.$tabInstruments.find( 'tr[idInstrument]' ).remove();
			},
			addInstrument:function( id, title ) {
				if( !self.hasInstrumentID( id )) {
					self.addInstrumentID( id );
					self.addInstrumentRow( id, title );
				}
			},
			deleteInstrument:function( id ) {
				if( self.hasInstrumentID( id )) {
					self.deleteInstrumentID( id );
					self.deleteInstrumentRow( id );
				}
			},
			addCurrentInstrument:function() {
				var id = self.$selectInstrument.val();
				if( id ) {
					var $option = self.$selectInstrument.find( 'option:selected' );
					self.addInstrument( id, $option.html() );
				}
				return false;
			},
			deleteSpecificInstrument:function() {
				var $row = $(this).closest( "tr" );
				var id = $row.attr( 'idInstrument' );
				self.deleteInstrument( id );
				return false;
			},
			
			load:function( model ) {
				self.$inputID.val( model.id );
				
				for( var idLanguage in model.names ) {
					var $wName = self.$ns.find( options.ins+'.wName.w'+idLanguage );
					$wName.val( model.names[idLanguage] );
				}
				
				self.$inputNameUser.val( model.nameUser );
				self.$inputType.val( model.type );
				
				var idsIndicators = model.indicators ? model.indicators.split(',') : [];
				$.each( idsIndicators, function( i, id ) {
					var $option = self.$selectIndicator.find( 'option[value="'+id+'"]' );
					self.addIndicator( id, $option.html() );
				});
				
				var idsTimeframes = model.timeframes ? model.timeframes.split(',') : [];
				$.each( idsTimeframes, function( i, id ) {
					var $option = self.$selectTimeframe.find( 'option[value="'+id+'"]' );
					self.addTimeframe( id, $option.html() );
				});
				
				var idsInstruments = model.instruments ? model.instruments.split(',') : [];
				$.each( idsInstruments, function( i, id ) {
					var $option = self.$selectInstrument.find( 'option[value="'+id+'"]' );
					self.addInstrument( id, $option.html() );
				});
				
				self.$inputIDEA.val( model.idEA );
				self.$inputPrice.val( model.price );
				
				for( var idLanguage in model.descs ) {
					var $wDesc = self.$ns.find( options.ins+'.wDesc.w'+idLanguage );
					$wDesc.val( model.descs[idLanguage] );
				}
			},
			showAdd:function() {
				if( self.loading ) return false;
				if( self.state == 'showAdd' ) {
					self.hide();
					return false;
				}
				else {
					self.$title.html( options.ls.lNew );
					self.clear();
					self.scrolledShow();
					self.$bDelete.hide();
					self.enable();
					self.state = 'showAdd';
					return true;
				}
			},
			showEdit:function( id ) {
				if( self.loading ) return false;
				self.scrolledShow();
				self.disable();
				if( options.ajax ) {
					self.$title.html( options.ls.lEdit );
					self.clear();
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lLoading );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxLoadURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						self.state = 'showEdit';
						if( data.error ) {
							alert( data.error );
							self.showAdd();
						}
						else{
							self.load( data.model );
							self.$bDelete.show();
						}
					});
				}
			},
			switchToEdit:function( id ) {
				self.$title.html( options.ls.lEdit );
				self.$inputID.val( id );
				self.$bDelete.show();
				self.state = 'showEdit';
			},
			hide:function( immediately ) {
				self.typedHide( immediately );
				self.state = 'hide';
			},
			clear:function() {
				self.$inputID.val( '' );
				self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
				self.$form.find( "input[type='text']" ).val( '' );
				
				self.$selectIndicator.find( 'option:first' ).prop( 'selected', true );
				self.clearIndicatorsIDs();
				self.clearIndicatorsRows();
				
				self.$selectTimeframe.find( 'option:first' ).prop( 'selected', true );
				self.clearTimeframesIDs();
				self.clearTimeframesRows();
				
				self.$selectInstrument.find( 'option:first' ).prop( 'selected', true );
				self.clearInstrumentsIDs();
				self.clearInstrumentsRows();
				
				self.$inputType.val( '' );
				self.$inputIDEA.val( '' );
				
				self.$inputNameUser.val( options.nameUserDef );
			},
			disable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.attr( 'disabled', 'disabled' );
				});
			},
			enable: function() {
				$.each([ self.$bSubmit, self.$bDelete ], function () {
					this.removeAttr( 'disabled' );
				});
			},
			showTooltip: function( $object, title, placement, delay ) {
				$object.tooltip({ 
					title: title,
					placement: placement,
					trigger: 'manual'
				});
				$object.tooltip( 'show' );
				if( delay ) {
					setTimeout( function() { $object.tooltip( 'destroy' ); }, delay );
				}
			},
			submit:function() {
				if( self.loading ) return false;
				self.disable();
				if( options.ajax ) {
					self.$form.find( '.'+options.errorClass ).removeClass( options.errorClass );
					var lSubmit = self.$bSubmit.html();
					self.$bSubmit.html( options.ls.lSaving );
					self.loading = true;
					var newRecord = !self.$inputID.val();
					$.post( yiiBaseURL+options.ajaxSubmitURL, self.$form.serialize(), function ( data ) {
						self.loading = false;
						self.$bSubmit.html( lSubmit );
						self.enable();
						if( data.error ) {
							alert( data.error );
							if( data.errorField ) {
								var $errorField = self.$form.find( '*[name="'+data.errorField+'"]' );
								$errorField.addClass( options.errorClass );
								$errorField.focus();
							}
						}
						else{
							options.nameUserDef = self.$inputNameUser.val();
							self.hide();
							if( newRecord ) {
								$.each( self.onAdd, function() { this( data.id ); });
							}
							else{
								$.each( self.onEdit, function() { this( data.id ); });
							}
						}
					}, "json" );
					return false;
				}
			},
			delete:function() {
				if( self.loading ) return false;
				if( !confirm( options.ls.lDeleteConfirm )) return false;
				self.disable();
				if( options.ajax ) {
					var id = self.$inputID.val();
					var lDelete = self.$bDelete.html();
					self.$bDelete.html( options.ls.lDeleting );
					self.loading = true;
					$.getJSON( yiiBaseURL+options.ajaxDeleteURL, {id:id}, function ( data ) {
						self.loading = false;
						self.$bDelete.html( lDelete );
						self.enable();
						if( data.error ) {
							alert( data.error );
						}
						else{
							self.clear();
							self.hide();
							$.each( self.onDelete, function() { this( id ); });
						}
					});
				}
			},
			onAdd: [],
			onEdit: [],
			onDelete: [],
		};
		self.init();
		return self;
	}
}( window.jQuery );