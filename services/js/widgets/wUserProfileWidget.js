var wUserProfileWidgetOpen;
!function( $ ) {
	wUserProfileWidgetOpen = function( options ) {
		var defLS = {
			lSaving: 'Saving...',
		};
		var defOption = {
			selector: '.wUserProfileWidget',
			ins: '',
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, defLS, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				if( options.canControl ) {
					
                }

				self.$profileTabsWrapper = self.$ns.find('.inside-menu__sub');
				self.$profileTabsLinks = self.$profileTabsWrapper.find('a');
				
				self.$profileTabsContests = self.$ns.find('.profile-card__right > div');
				
				self.hideInactiveProfileTabs();
				
				self.$profileTabsLinks.click( self.changeProfileTab );
				
			},
			hideInactiveProfileTabs: function(){
				var activeIndex = self.$profileTabsLinks.index( self.$ns.find('.inside-menu_sub-active') );
				
				self.$profileTabsContests.each(function( index, element ) {
					if( index == activeIndex ) 
						$(this).show();
					else
						$(this).hide();
				});
			},
			changeProfileTab: function(){
				self.$profileTabsLinks.removeClass('inside-menu_sub-active');
				$(this).addClass('inside-menu_sub-active');
				self.hideInactiveProfileTabs();
				return false;
			},
			
		};
		self.init();
		return self;
	}
}( window.jQuery );