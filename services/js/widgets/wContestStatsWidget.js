var wContestStatsWidgetOpen;
!function( $ ) {
	wContestStatsWidgetOpen = function( options ) {
		var defOption = {
			selector: '.wContestStatsWidget',
			ins: '',
			ajaxLoadStats: '/contest/getStats',
			iTimer:600,
		};
		options = options || {};
		options = $.extend( {}, defOption, options );
		options.ls = $.extend( {}, options.ls );
		
		var self = {
			init:function() {
				self.$ns = $( options.selector );
				
				self.$wScaleBox = self.$ns.find( '.scale_box' );
				self.$wStatisticsCompetition = self.$ns.find( '.l_statistics_competition' );
				
				self.$wGain = self.$ns.find( '.lider_statistics' );
				self.$wLeaderLink = self.$ns.find( '.leader-link' );
				self.$wTotalProfit = self.$ns.find( '.totalProfit' );
				self.$wTotalLoss = self.$ns.find( '.totalLoss' );
				self.$wCommittedDeals = self.$ns.find( '.committedDeals' );
				self.$wBestDeal = self.$ns.find( '.bestDeal' );
				self.$wBestProfitRiskMember = self.$ns.find( '.bestProfitRiskMember' );
				self.$wLastDeal = self.$ns.find( '.lastDeal' );
				self.$wTraders = self.$ns.find( '.Traders' );
				self.$wActiveTraders = self.$ns.find( '.activeTraders' );
				self.$wProfitTraders = self.$ns.find( '.profitTraders' );
				self.$wLossTraders = self.$ns.find( '.lossTraders' );
				
				
				self.$wDocument = $(document);
				self.$wDocument.ready( self.scale );
				self.$wDocument.ready( self.statisticsBg );
				self.$wDocument.ready( self.statisticsVal );
				
				if( self.sInterval === undefined && options.startTimer===true ){
					self.sInterval = setInterval(function () {
						self.getStats();
						},
						options.iTimer * 1000
					);
				}
				
				
			},
			getStats:function() {
				
				var data = {};
				data.idContest = options.idContest;
				$.getJSON( yiiBaseURL+options.ajaxLoadStats, data, function ( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						self.$wGain.attr({'data-val':data.gain, 'data-fullval':data.gain100});
						self.$wLeaderLink.html( data.leader );
						self.$wScaleBox.attr({'data-full':data.profit100});
						self.$wTotalProfit.attr({'data-val':data.totalProfit});
						self.$wTotalLoss.attr({'data-val':data.totalLoss});
						self.$wCommittedDeals.html( data.committedDeals );
						self.$wBestDeal.html( data.bestDeal );
						self.$wBestProfitRiskMember.html( data.bestProfitRiskMember );
						self.$wLastDeal.html( data.lastDeal );
						self.$wTraders.attr({'data-fullval':data.totalTraders});
						self.$wActiveTraders.attr({'data-val':data.activeTraders});
						self.$wProfitTraders.attr({'data-val':data.profitTraders});
						self.$wLossTraders.attr({'data-val':data.lossTraders});
						
						self.scale();
						self.statisticsBg();
						self.statisticsVal();
					}
				});

			},
			scale:function() {
				self.$wScaleBox.each(function(){
					var $this = $(this),
						val = $this.attr('data-val'),
						fullVal = $this.attr('data-full'),
						widthScale = (100/fullVal*(+val))+"%";
						
					$this.find('.scale_text_val>span').text( self.number_format(val)+" $");
					$this.find('.scale_box_item_inner').animate({
						'width':widthScale
					},700);

				});
			},
			statisticsBg:function() {
				self.$wStatisticsCompetition.each(function(){
					var $this = $(this),
						id = $this.find('canvas.statistics_graph_bg').attr('id'),
						canvas = document.getElementById(id),
						canvasWidth =$this.width(),
						canvasItem = canvas.getContext('2d');
					canvas.width =canvasWidth;
					canvas.height =canvasWidth;
					if($this.hasClass('lider_statistics')){
						canvasItem.beginPath();
						canvasItem.arc(canvasWidth/2, canvasWidth/2, canvasWidth/2-16, 0.75* Math.PI, 0.25 * Math.PI, false);
						canvasItem.lineWidth = 32;
						canvasItem.strokeStyle = "#d9d9d9";
						canvasItem.stroke();
					}
					else{
						canvasItem.beginPath();
						canvasItem.arc(canvasWidth/2, canvasWidth/2, canvasWidth/2-8, 0.75* Math.PI, 0.25 * Math.PI, false);
						canvasItem.lineWidth = 16;
						canvasItem.strokeStyle = "#d9d9d9";
						canvasItem.stroke();
					}

				});
			},
			statisticsVal:function() {
				self.$wStatisticsCompetition.each(function(){
					var $this = $(this),
						color = $this.attr('data-color'),
						val = $this.attr('data-val'),
						fullVal = $this.attr('data-fullval'),
						percent = 100/fullVal*val,
						id = $this.find('canvas.statistics_graph').attr('id'),
						canvas = document.getElementById(id),
						canvasWidth =$this.width(),
						canvasItem = canvas.getContext('2d');
					canvas.width =canvasWidth;
					canvas.height =canvasWidth;
					if($this.hasClass('lider_statistics')){
						canvasItem.beginPath();
						canvasItem.arc(canvasWidth/2, canvasWidth/2, canvasWidth/2-16, 0.75* Math.PI, (percent*0.015+0.75) * Math.PI, false);
						canvasItem.lineWidth = 32;
						canvasItem.strokeStyle = color;
						canvasItem.stroke();
						$this.find('.lider_statistics_val').text("+"+val+"%");
					}
					else{
						canvasItem.beginPath();
						canvasItem.arc(canvasWidth/2, canvasWidth/2, canvasWidth/2-8, 0.75* Math.PI, (percent*0.015+0.75) * Math.PI, false);
						canvasItem.lineWidth = 16;
						canvasItem.strokeStyle = color;
						canvasItem.stroke();
						$this.find('.statistics_block_text').text(val).css({"color":color});
					}

				});
			},
			number_format:function(number) {
				return number.toString().replace(
					/^(\-?)(\d{1,2})?(\d{3})?(\d{3})?(\d{3})?(\d{3})?(\d{3})?([\.\,](\d+)?)?$/,
					function($0,$sign,$6,$5,$4,$3,$2,$1,$pt,$dec){
						var z=
							($sign ||'')+
							($6?' '+$6:'')+
							($5?' '+$5:'')+
							($4?' '+$4:'')+
							($3?' '+$3:'')+
							($2?' '+$2:'')+
							($1?' '+$1:'')+
							($pt?','+($dec|0):'');
						return z;
				});
			},
		};
		self.init();
		return self;
	}
}( window.jQuery );

