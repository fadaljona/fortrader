var engine;
!function( $ ) {
	engine = {};
		
		// commonLib
	var commonLib = engine.commonLib = {};
	
	var mt_rand = commonLib.mt_rand = function() {
		var rnd = Math.random();
		rnd = rnd.toString();
		rnd = rnd.split( '.' );
		rnd = parseInt( rnd[1] );
		return rnd;
	};
	var between = commonLib.between = function( val, min, max ) {
		if( typeof val == 'object' ) {
			var count = 0;
			for( var key in val ) {
				count++;
				if( !between( val[key], min, max ) )
					return false;
			}
			return !!count;
		}
		return val >= min && val <= max;
	};
	var div = commonLib.div = function( val, by ) {
		return (val - val % by) / by;
	};
	var isHistoryAPI = commonLib.isHistoryAPI = function() {
		return !!window && !!window.history && !!window.history.pushState;
	};
		// strings
	var lcfirst = commonLib.lcfirst = function( str ) {
		return str.substr( 0, 1 ).toLowerCase() + str.substr( 1 );
	};
	var ucfirst = commonLib.ucfirst = function( str ) {
		return str.substr( 0, 1 ).toUpperCase() + str.substr( 1 );
	};
	var strtr = commonLib.strtr = function( str, tr ) {
		foreach( tr, function( key, value ) {
			if( key == value )
				return;
				
			while( inArray( str, key ))
				str = str.replace( key, value );
		});
		return str;
	};
	var objtr = commonLib.objtr = function( obj, keys, tr ) {
		for( var key in keys || obj ) {
			key = keys ? keys[ key ] : key;
			obj[ key ] = strtr( obj[ key ], tr );
		}
	};
	var strip_scripts = commonLib.strip_scripts = function( str ) {
		var str = str.replace( /<script[\s\S]*?<\/script>/gi, '' );
		return str;
	};
	var clip = commonLib.clip = function( str, cliper, side_clip, clip_cliper ) {
		var index = str.indexOf( cliper );
		if( index != -1 ) {
			if( !side_clip || side_clip == 'left' ) 
				str =  str.substr( index + (clip_cliper ? 1 : 0) );
			else
				str =  str.substr( 0, index + 1 - (clip_cliper ? 1 : 0) );
		}
		return str;
	};
		 // urls
	var filterURL = commonLib.filterURL = function( url ) {
		if( url.indexOf( '/' ) == 0 ) {
			url = yiiBaseURL + url;
		}
		return url;
	};	
	var getQueryString = commonLib.getQueryString = function( url, exceptEncode ) {
		if( typeof url == 'object' ) {
			var parts = [];
			if( !exceptEncode )
				exceptEncode = [];
			
			foreach( url, function( key, value ) {
				if( value && value != '' )
					parts.push( strtr( '{key}={value}', {
						'{key}': key,
						'{value}': inArray( exceptEncode, key ) ? value : encodeURI( value )
					}));
			});
			return parts.join( '&' );
		}
		url = url ? url : document.location.href;
		return inArray( url, '?' ) ? clip( url, '?', 'left', true ) : '';
	};
	var getQueryObj = commonLib.getQueryObj = function( url ) {
		var obj = {};
		var	queryString = getQueryString( url );
		
		if( queryString.length )
			foreach( queryString.split( '&' ), function( i, parts ) {
				if( parts.length && inArray( parts, '=' )) {
					parts = parts.split( '=' );
					if( parts[0].length )
						obj[ parts[0] ] = decodeURI( parts[1]);
				}
			});
			
		return obj;
	};
	var setQueryObj = commonLib.setQueryObj = function( obj ) {
		var queryString = getQueryString( obj );
		if( queryString.length )
			queryString = '?' + queryString;
		setURL( queryString );
	};
	var getQueryVar = commonLib.getQueryVar = function( name, url ) {
		var patt = new RegExp( '(?:\\?|&|^){name}=([^&$]+)'.replace( '{name}', name ) );
		var matchs = patt.exec( url || getQueryString() );
		return matchs ? decodeURI( matchs[1]) : undefined;
	};
	var stateToString = commonLib.stateToString = function( state ) {
		var parts = [];
		foreach( state, function( key, value ) {
			if( !value )
				return undefined;
			
			var part = [ key, value ].join( '=' );
			parts.push( part );
		});
		
		var str = parts.join( '|' );
		str = '{{inner}}'.replace( '{inner}', str );
		return str;
	};
	var stringToState = commonLib.stringToState = function( str ) {
		var state = {};
		
		var match = /^\{([\s\S]+)\}/.exec( str );
		var parts = match && match[1] ? match[1] : null;
		
		foreach( parts.split( '|' ), function( key, part ) {
			part = part.split( '=' );
			state[ part[0] ] = part[1];
		});
		
		return state;
	};
	var getQueryState = commonLib.getQueryState = function( name, url ) {
		var state = {};
		var queryVar = getQueryVar( name, url );
		
		if( queryVar && queryVar.length ) 
			state = stringToState( queryVar );
		
		return state;
	};
	var setURL = commonLib.setURL = function( url ) {
		document.location.href.assign( url );
	};
	var addToHistory = commonLib.addToHistory = function( url ) {
		if( !isHistoryAPI())
			return true;
			
		if( !url.length )
			url = clip( document.location.href, '?', 'right', true );
		
		//alert( 'addToHistory: ' + url );
		
		if( isHistoryAPI()) {
			window.history.pushState( null, null, url );
			return true;
		}
		else
			setURL( url );
	};
	var replaceIntoHistory = commonLib.replaceIntoHistory = function( url ) {
		if( !isHistoryAPI())
			return true;
		
		if( !url.length )
			url = clip( document.location.href, '?', 'right', true );
		
		//alert( 'replaceHistory: ' + url );
		
		if( isHistoryAPI()) {
			window.history.replaceState( null, null, url );
			return true;
		}
		else
			setURL( url );
	};
	var addQueryStateToHistory = commonLib.addQueryStateToHistory = function( name, state ) {
		var obj = getQueryObj();
		obj[ name ] = stateToString( state );
		
		var queryString = getQueryString( obj, [ name ] );
		if( queryString.length )
			queryString = '?' + queryString;
			
		return addToHistory( queryString );
	};
	var replaceQueryStateIntoHistory = commonLib.replaceQueryStateIntoHistory = function( name, state ) {
		var obj = getQueryObj();
		obj[ name ] = stateToString( state );
		
		var queryString = getQueryString( obj, [ name ] );
		if( queryString.length )
			queryString = '?' + queryString;
			
		return replaceIntoHistory( queryString );
	};
		// arrays, objects
	var foreach = commonLib.foreach = function( arr, context, func, start, end, step ) {
		if( typeof context == 'function' ) {
			step = end;
			end = start;
			start = func;
			func = context;
			context = null;
		}
		if( 'length' in arr ) {
			if( !arr.length )
				return;
				
			if( step === undefined ) 
				step = 1;
			
			var forward = step >= 0;
				
			if( start === undefined ) 
				start = forward ? 0 : arr.length - 1;
				
			if( end === undefined ) 
				end = forward ? arr.length - 1 : 0;
				
			var i;
			var stoped = false;
			var result;
			
			var controls = {
				setIndex: function( value ) {
					i = value;
				},
				stop: function() {
					stoped = true;
				}
			};
			
			for( 
				i = start; 
				forward ? i <= end && i < arr.length : i >= end && i >= 0; 
				i += step 
			) {
				result = func.call( context, i, arr[i], controls );
				if( result !== undefined || stoped )
					return result;
			}
		}
		else {
			var stoped = false;
			var result;
			var controls = {
				stop: function() {
					stoped = true;
				}
			};
			
			for( var key in arr ) {
				result = func.call( context, key, arr[key], controls );
				if( result !== undefined || stoped )
					return result;
			}
		}
	};
	var slice = commonLib.slice = function( arr, start, end ) {
		if( typeof arr.slice == 'function' )
			return arr.slice( start, end );
		
		var arrR = [];
		foreach( arr, function( i, value ) {
			arrR.push( value );
		}, start, end );
		return arrR;
	};
	var merge = commonLib.merge = function( objD, objS ) {
		foreach( arguments, function( i, objS ) {
			for( var key in objS ) {
				objD[ key ] = objS[ key ];
			}
		}, 1 );
		return objD;
	};
	var mergeDef = commonLib.mergeDef = function( objD, objS ) {
		foreach( arguments, function( i, objS ) {
			for( var key in objS ) {
				if( objD[ key ] == undefined ) {
					objD[ key ] = objS[ key ];
				}
			}
		}, 1 );
		return objD;
	};
	var mergeExists = commonLib.mergeExists = function( objD, objS ) {
		foreach( arguments, function( i, objS ) {
			for( var key in objD ) {
				objD[ key ] = objS[ key ];
			}
		}, 1 );
		return objD;
	};
	var removeFromArray = commonLib.removeFromArray = function( arr, value ) {
		foreach( arguments, function( i, value ) {
			var index;
			while(( index = arr.indexOf( value )) != -1 ) 
				arr.splice( index, 1 );
		}, 1 );
	}
	var inArray = commonLib.inArray = function( arr, value ) {
		return arr.indexOf( value ) != -1;
	};
	var addToArray = commonLib.addToArray = function( arr, value ) {
		foreach( arguments, function( i, value ) {
			if( arr.indexOf( value ) == -1 )
				arr.push( value );
		}, 1 );
	}
	var array_diff = commonLib.array_diff = function( arrA, arrB ) {
		var arrR = [].concat( arrA );
		
		foreach( arguments, function( i, arr ) {
			foreach( arr, function( j, value ) {
				removeFromArray( arrR, value );
			});
		}, 1 );
		
		return arrR;
	};
	var array_intersect = commonLib.array_intersect = function( arrA, arrB ) {
		var arrR = [].concat( arrA );
		
		foreach( arguments, function( i, arr ) {
			foreach( arrR, function( j, value, controls ) {
				if( !inArray( arr, value )) {
					arrR.splice( j, 1 );
					controls.setIndex( j - 1 );
				}
			});
		}, 1 );
		
		return arrR;
	};
	var array_filter = commonLib.array_filter = function( arr, context, func ) {
		var arrR = [];
		
		if( typeof context == 'function' ) {
			func = context;
			context = null;
		}
		
		foreach( arr, function( key, value ) {
			if( func.call( context, key, value ))
				arrR[key] = value;
		});
		
		return arrR;
	};
	var array_map = commonLib.array_map = function( arr, context, func ) {
		var arrR = [];
		
		if( typeof context == 'function' ) {
			func = context;
			context = null;
		}
		
		foreach( arr, function( key, value ) {
			arrR[ key ] = func.call( context, key, value );
		});
		
		return arrR;
	};
	var shifts = commonLib.shifts = function( arr, context, func ) {
		while( arr.length ) {
			var value = arr.shift();
			func.call( context, value );
		}
	};
	var compare = commonLib.compare = function( objA, objB ) {
		return !foreach( arguments, function( i, objB ) {
			return foreach( objB, function( key, value ) {
				if( objA[key] !== value ) 
					return true;
			}) || foreach( objA, function( key, value ) {
				if( objB[key] !== value ) 
					return true;
			});
		}, 1 );
	};
	var range = commonLib.range = function( start, end ) {
		var arrR = [];
		for( var i=start; i<=end && start<=end; i++ )
			arrR.push( i );
		return arrR;
	};
	var combine = commonLib.combine = function( arrKeys, arrValues ) {
		var obj = {};
		foreach( arrKeys, function( i, key ) {
			obj[ key ] = arrValues[ i ];
		});
		return obj;
	};
	var keys = commonLib.keys = function( arr ) {
		var keys = [];
		foreach( arr, function( key, value ) {
			keys.push( key );
		});
		return keys;
	};
		// funcs
	var applies = commonLib.applies = function( funcs, context, args ) {
		foreach( funcs, function( i, value ) {
			value.apply( context, args );
		});
	};
	var calls = commonLib.calls = function( funcs, context ) {
		var args = slice( arguments, 2 );
		applies( funcs, context, args );
	};
		// cookie
	var toCookie = commonLib.toCookie = function( key, value ) {
		$.cookie( key, value, { expires: 365, path: "/" });
	};
	var fromCookie = commonLib.fromCookie = function( key ) {
		return $.cookie( key );
	};
		// JSON
	var toJSON = commonLib.toJSON = function( mixed ) {
		return window.JSON.stringify( mixed );
	};
	var fromJSON = commonLib.fromJSON = function( str ) {
		return window.JSON.parse( str );
	};
		// jquery
	var foreach$ = commonLib.foreach$ = function( $collection, context, func, start, end, step ) {
		if( typeof context == 'function' ) {
			step = end;
			end = start;
			start = func;
			func = context;
			context = null;
		}
		return foreach( $collection, context, function( i, value ) {
			var $element = $( value );
			return func.call( context, i, $element );
		}, start, end, step );
	}
	var collectAttrs$ = commonLib.collectAttrs$ = function( $collection, attrName ) {
		var values = [];
		foreach$( $collection, function( i, $element ) {
			var value = $element.attr( attrName );
			values.push( value );
		});
		return values;
	};
	var set$SelectOptions = commonLib.setSelectOptions = function( $select, options, keysOptions ) {
		$select.html( '' );
		foreach( keysOptions, this, function( i, key ) {
			var title = options[key];
			var $option = $( '<option/>', {
				value: key
			}).html( title );
			$select.append( $option );
		});
	};
		// classes
	var extend = commonLib.extend = function( Child, Parent, metods ) {
		var F = function() { };
		F.prototype = Parent.prototype;
		Child.prototype = new F();
		Child.prototype.constructor = Child;
		Child.parent = Parent.prototype;
		if( metods ) merge( Child.prototype, metods );
	};
	
		// CLASSES
	var classes = engine.classes = {};
	var objects = engine.objects = {};
		// base
	var base = classes.base = function base( options ) { // constructor
		this.options = options || {};
		this.private = {};
	};
	merge( base.prototype, { // metods
		getClassName: function() {
			return this.options.className || this.constructor.name;
		},
		getQueryStateName: function() {
			return this.options.queryStateName || this.constructor.name;
		},
			// funcs
		proxy: function( func ) {
			var args = [ func, this ];
			args = args.concat( slice( arguments, 1 ));
			return $.proxy.apply( this, args );
		},
			// i18n
		t: function( key ) {
			return this.options.ls && this.options.ls[ key ] ? this.options.ls[ key ] : key;
		},
			// ajax
		ajaxError: function() {
			alert( this.t( 'Sorry. Request failed. Please try again later.' ));
		},
		ajax: function( settings ) {
			var settings = merge({
				context: this,
				error: this.ajaxError
			}, settings );
			return $.ajax( settings );
		},
			// state
		getQueryState: function( url ) {
			var name = this.getQueryStateName();
			return getQueryState( name, url );
		},
		addQueryStateToHistory: function( state ) {
			var name = this.getQueryStateName();
			return addQueryStateToHistory( name, state );
		},
		replaceQueryStateIntoHistory: function( state ) {
			var name = this.getQueryStateName();
			return replaceQueryStateIntoHistory( name, state );
		},
			// cookie
		getCookieKey: function() {
			return '{class}_cookie'.replace( '{class}', this.getClassName() );
		},
		getCookieObj: function() {
			var key = this.getCookieKey();
			var str = fromCookie( key );
			return str && fromJSON( str ) || {};
		},
		setCookieObj: function( obj ) {
			var key = this.getCookieKey();
			var str = toJSON( obj );
			toCookie( key, str );
		},
		getCookieVar: function( key ) {
			var obj = this.getCookieObj();
			return obj[key];
		},
		setCookieVar: function( key, value ) {
			var obj = this.getCookieObj();
			obj[key] = value;
			this.setCookieObj( obj );
		},
		pushCookieVar: function( key, value ) {
			var obj = this.getCookieObj();
			obj[key] = obj[key] || [];
			addToArray( obj[key], value );
			this.setCookieObj( obj );
		},
		popCookieVar: function( key, value ) {
			var obj = this.getCookieObj();
			removeFromArray( obj[key] || [], value );
			this.setCookieObj( obj );
		},
		inCookieVar: function( key, value ) {
			var obj = this.getCookieObj();
			return inArray( obj[key] || [], value );
		}
	});
		// doc
	var doc = classes.doc = function doc( options ) { // constructor
		doc.parent.constructor.call( this, options );
		
		$( document ).on( 'scroll', this.proxy( this.handlerScroll ));
	};
	extend( doc, base, { // metods
			// traversing
		handlerScroll: function() {
			this.scrolled = true;
		}
	});
	var docObj = objects.docObj = new doc();
	
		// WIDGETS
	var instanceWidget = engine.instanceWidget = function( className, baseClass, element, instanceOptions, options ) {
		var options = mergeDef( {
			className: className
		}, options, instanceOptions );
		return classes[ className ] ? new classes[ className ]( element, options ) 
		                            : new baseClass( element, options );
	};
	var widgetDataKey = engine.widgetDataKey = 'data-widget';
		
		// WidgetBase
	var WidgetBase = classes.WidgetBase = function WidgetBase( element, options ) { // constructor
			// options
		var options = merge({
			scrollSpeed: 300,
			scrollOffsetTop: -55,
			scrollOffsetBottom: 0
		}, options );
			// parent constructor
		WidgetBase.parent.constructor.call( this, options );
		
			// events
		this.onShow = [];
		this.onShowing = [];
		this.onHide = [];
		this.onHiding = [];
		this.onEnable = [];
		this.onDisable = [];

			// init
		WidgetBase.private.init.call( this, element );
	};
	WidgetBase.static = { // static metods
		instance: function( element, instanceOptions, options ) {
			return instanceWidget( 'WidgetBase', undefined, element, instanceOptions, options );
		}
	};	
	WidgetBase.private = { // private metods
		init: function( element ) {
			this.element = element;
			this.$w = $( element );
			this.$w.setEngineWidget( this );
			
			this.$wDummyReload = undefined;
			
			if( this.options.asCopy ) {
				this.remarkTabs();
			}
		}
	};
	extend( WidgetBase, base, { // metods
			// traversing
		findInINS: function( selector ) {
			var selector = selector + ( this.options.ins && this.options.ins.length ? '.'+this.options.ins : '' );
			var $element = this.$w.find( selector );
			if( this.debug ) {
				alert( selector );
				alert( $element.length );
			}
			return $element;
		},
			// animation
		scrollTo: function( options ) {
			var options = merge({
				offsetTop: this.options.scrollOffsetTop, 
				offsetBottom: this.options.scrollOffsetBottom, 
				easing: 'linear', 
				duration: this.options.scrollSpeed
			}, options );
			
			if( !options.target )
				options.target = this.$w;
			
			if( $(document).width() >= 979 ) {
				options.offsetTop -= 50;
			}
						
			var top = options.target.offset().top;
			var height = options.target.outerHeight();
			var bottom = top + height;
			top += options.offsetTop;
			
			var $html = $( 'html' );
			var htmlTop = $html.scrollTop();
			var htmlHeight = $html[0].clientHeight;
			var htmlBottom = htmlTop + htmlHeight;
				
			if( !between( [top, bottom], htmlTop, htmlBottom ) ) {
				var y = !between( top, htmlTop, htmlBottom ) || height > htmlHeight ? top : bottom - htmlHeight + options.offsetBottom;
				options.done = options.onAfter;
				$html.animate({
					'scrollTop': y
				}, options );
			}
			else
				if( options.onAfter )
					options.onAfter();
		},
		scrolledShow: function( options ) {
			calls( this.onShowing );
			docObj.scrolled = false;
			this.$w.slideDown({
				duration: this.options.scrollSpeed, 
				easing: 'linear', 
				done: this.proxy( function() {
					function onAfter() {
						if( options && options.onAfter ) 
							options.onAfter();
							
						calls( this.onShow );
					}
					
					if( !docObj.scrolled )
						this.scrollTo({
							onAfter: this.proxy( onAfter )
						});	
					else
						onAfter.call( this );
				})
			});
		},
		immediatelyShow: function( options ) {
			calls( this.onShowing );
			this.$w.show();
			this.scrollTo({
				onAfter: this.proxy( function () {
					if( options && options.onAfter ) 
						options.onAfter();
						
					calls( this.onShow );
				})
			});
		},
		show: function( options ) {
			this.options.sliding ? this.scrolledShow( options ) : this.immediatelyShow( options );
		},
		isVisible: function() {
			return this.$w.is( ':visible' );
		},
		scrolledHide: function( options ) {
			calls( this.onHiding );
			this.$w.slideUp({
				duration: this.options.scrollSpeed, 
				easing: 'linear', 
				done: this.proxy( function() {
					if( options && options.onAfter ) 
						options.onAfter();
						
					calls( this.onHide );
				})
			});
		},
		immediatelyHide: function( options ) {
			this.$w.hide();
			
			if( options && options.onAfter ) 
				options.onAfter();
				
			calls( this.onHide );
		},
		hide: function( options ) {
			this.options.sliding ? this.scrolledHide( options ) : this.immediatelyHide( options );
			return true;
		},
		isHidden: function() {
			return this.$w.is( ':hidden' );
		},
		slideLeft: function( options ) {
			var options = merge({
				target: this.$w
			}, options );
			
			if( !this.isVisible() ) {
				options.target.hide();
				return;
			}
			
			options.target.animate({
				'width': 'hide',
				'padding-left': 'hide',
				'padding-right': 'hide',
				'margin-left': 'hide',
				'margin-right': 'hide',
				'border-left-width': 'hide',
				'border-right-width': 'hide'
			}, options );
		},
		slideRight: function( options ) {
			var options = merge({
				target: this.$w
			}, options );
			
			if( !this.isVisible() ) {
				options.target.show();
				return;
			}
			
			options.target.animate({
				'width': 'show',
				'padding-left': 'show',
				'padding-right': 'show',
				'margin-left': 'show',
				'margin-right': 'show',
				'border-left-width': 'show',
				'border-right-width': 'show'
			}, options );
		},
		highlightW: function( $w, colors, options ) {
			var options = merge({
				easing: 'linear', 
				duration: 500
			}, options );
			$w.css( 'background-color', colors[0] );
			
			foreach( slice( colors, 1 ), function( i, color ) {
				// if( i == colors.length - 2 ) {
				//	options.done = function() {
				//		$w.css( 'background-color', '' );
				//	}
				// }
				$w.animate({ 
					'background-color': color
				}, options );
			});
			
		},
		showReloadDummy: function( immediately ) {
			this.$w.addClass( 'iRelative' );
				
			if( !this.$wDummyReload ) {
				this.$wDummyReload = this.findInINS( '.iDummyReload' );
				if( !this.$wDummyReload.length ) {
					var html = '<table><tr><td>{label}</td></tr></table>'.replace( '{label}', this.t( 'Loading...' ));
					this.$wDummyReload = $( html ).addClass( this.options.ins ).addClass( 'iDummyReload i02' ).hide();
					this.$w.append( this.$wDummyReload );
				}
			}
			
			if( !this.isVisible() )
				return;
			
			this.$wDummyReload.height( this.$w.outerHeight() );
			this.$wDummyReload.width( this.$w.outerWidth() );
			this.$wDummyReload.stop()[ immediately ? 'show' : 'fadeIn' ]();
		},
		hideReloadDummy: function() {
			if( this.$wDummyReload )
				this.$wDummyReload.stop().fadeOut();
		},
			// common
		getControllers: function() {
			return this.$w.find( "input, select, textarea, button" );
		},
		enable: function() {
			this.getControllers().filter( ':not( .manualDisable )' ).removeAttr( 'disabled' );
			calls( this.onEnable, this );
		},
		disable: function() {
			this.getControllers().filter( ':not( .manualEnable )' ).blur().attr( 'disabled', 'disabled' );
			calls( this.onDisable, this );
		},
		showTooltip: function( $target, options ) {
			var options = merge({
				placement: 'top',
				trigger: 'manual'
			}, options );
			$target.tooltip( options );
			if( options.trigger == 'manual' ) {
				$target.tooltip( 'show' );
				if( options.hideDelay ) {
					setTimeout( function() { $target.tooltip( 'destroy' ); }, options.hideDelay );
				}
			}
		},
		enableW: function( $w, save ) {
			$w.removeAttr( 'disabled', 'disabled' );
			$w.removeClass( 'manualDisable' );
			if( save )
				$w.addClass( 'manualEnable' );
		},
		disableW: function( $w, save ) {
			$w.attr( 'disabled', 'disabled' );
			$w.removeClass( 'manualEnable' );
			if( save )
				$w.addClass( 'manualDisable' );
		},
		setLoading: function( value ) {
			this.loading = value;
			value ?	this.showReloadDummy()
				  : this.hideReloadDummy();
		},
		getLoading: function() {
			return this.loading;
		},
		remarkTabs: function() {
			var rnd = mt_rand();
			var $tabs = this.$w.find( '*[data-toggle=tab]' );
			foreach$( $tabs, this, function( i, $tab ) {
				var href = $tab.attr( 'href' );
				var $pane =	this.$w.find( href );
				var id = $pane.attr( 'id' );
				$tab.attr( 'href', href+'_'+rnd );
				$pane.attr( 'id', id+'_'+rnd );
			});
		},
		replaceWith: function( $w ) {
			this.$w.replaceWith( $w );
			WidgetBase.private.init.call( this, $w );
		},
		clone: function() {
			var $w = this.$w.clone();
			var HTML = $w[0].outerHTML;
			HTML = strip_scripts( HTML );
			$w = $( HTML );
			
			var options = merge( {}, this.options, {
				asCopy: this
			});
			var widget = new this.constructor( $w[0], options );
			return widget;
		}
	});
	
		// AccordionWidget
	var AccordionWidget = classes.AccordionWidget = function AccordionWidget( element, options ) { // constructor
		AccordionWidget.parent.constructor.call( this, element, options );
		
			// events
		this.onOpen = [];
		this.onClose = [];
		
			// init
		AccordionWidget.private.init.call( this );
	};
	AccordionWidget.private = { // private metods
		init: function() {
			this.$w.on( 'click', '.accordion-toggle', this.proxy( this.handleClick ));
		}
	};
	extend( AccordionWidget, WidgetBase, { // metods
		findSecond: function( $element ) {
			if( $element.is( '.accordion-toggle' ))
				return $element.closest( '.accordion-heading' ).next();
			else
				return $element.prev().find( '.accordion-toggle' );
		},
		isControlOpened: function( $control ) {
			return !$control.hasClass( 'collapsed' );
		},
		isBodyOpened: function( $body ) {
			return this.isControlOpened( this.findSecond( $body ));
		},
		immediatelyCloseControl: function( $control ) {
			var $body = this.findSecond( $control );
			$control.addClass( 'collapsed' );
			$body.addClass( 'collapse' );
			calls( this.onClose, this, $control, $body );
		},
		closeControl: function( $control ) {
			var $body = this.findSecond( $control );
			
			$control.addClass( 'collapsed' );
			$body.slideUp({
				done: this.proxy( function() {
					$body.addClass( 'collapse' );
					calls( this.onClose, this, $control, $body );
				})
			});
		},
		closeBody: function( $body ) {
			this.closeControl( this.findSecond( $body ));
		},
		immediatelyClose: function() {
			var $controls = this.$w.find( '.accordion-toggle' );
			foreach$( $controls, this, function( i, $control ) {
				this.immediatelyCloseControl( $control );
			});
		},
		close: function() {
			var $controls = this.$w.find( '.accordion-toggle' );
			foreach$( $controls, this, function( i, $control ) {
				this.closeControl( $control );
			});
		},
		immediatelyOpenControl: function( $control ) {
			var $body = this.findSecond( $control );
			$control.removeClass( 'collapsed' );
			$body.removeClass( 'collapse' );
			$body.show();
			calls( this.onOpen, this, $control, $body );
		},
		openControl: function( $control ) {
			var $body = this.findSecond( $control );
			
			$control.removeClass( 'collapsed' );
			$body.hide();
			$body.removeClass( 'collapse' );
			$body.slideDown({
				done: this.proxy( function() {
					calls( this.onOpen, this, $control, $body );
				})
			});
		},
		openBody: function( $body ) {
			this.openControl( this.findSecond( $body ));
		},
		immediatelyOpen: function() {
			var $controls = this.$w.find( '.accordion-toggle' );
			foreach$( $controls, this, function( i, $control ) {
				this.immediatelyOpenControl( $control );
			});
		},
		open: function() {
			var $controls = this.$w.find( '.accordion-toggle' );
			foreach$( $controls, this, function( i, $control ) {
				this.openControl( $control );
			});
		},
		handleClick: function( e ) {
			var $control = $( e.target ).closest( '.accordion-toggle' );
			var opened = this.isControlOpened( $control );
			
			opened ? this.closeControl( $control ) 
			       : this.openControl( $control );
			
			return false;
		}
	});
	
		// ListWidgetBase 
	var ListWidgetBase = classes.ListWidgetBase = function ListWidgetBase( element, options ) { // constructor
			// options
		var options = merge({
			ajaxLoadListURL: '/{modelName}/ajaxLoadList',
			ajaxDeleteURL: '/admin/{modelName}/ajaxDelete',
			editURL: '/admin/{modelName}/list?list-state={filter-id={id}|selected={id}}',
			rangeResizeOn: 10,
			minHeaderSize: 15,
			queryStateName: 'list-state'
		}, options );
		objtr( options, 'ajaxLoadListURL,ajaxDeleteURL,editURL'.split(','), { '{modelName}': lcfirst( options.modelName ) });
			// parent constructor
		ListWidgetBase.parent.constructor.call( this, element, options );
		
			// vars
		this.loadingIDs = [];
		this.state = this.options.state;
		this.hiddenState = this.options.hiddenState;
				
			// init
		ListWidgetBase.private.initState.call( this );
		ListWidgetBase.private.init.call( this );
			
		if( isHistoryAPI()) 
			$(window).on( 'popstate', this.proxy( this.handlerPopstate ));
		
			// static handlers
		$(document).on( 'click', this.proxy( function( e ) {
			if( !$( e.target ).parents( '.wSettingsBlock' ).length ) {
				if( this.$wSettingsBlock.$wPane.is( ':visible' ))
					this.$wSettingsBlock.$wPane.slideUp( 'fast' );
			}
		}));
		
		$( document ).on( 'mousemove mouseup', this.proxy( this.handlerDocumentMouse ));
	}
	ListWidgetBase.static = { // static metods
		instance: function( element, instanceOptions, options ) {
			var className = '{modelName}ListWidget'.replace( '{modelName}', instanceOptions.modelName );
			return instanceWidget( className, ListWidgetBase, element, instanceOptions, options );
		}
	};
	ListWidgetBase.private = { // private metods
		fillSettingsSpotPage: function( currentPage, pagesCount ) {
			if( currentPage == undefined )
				currentPage = parseInt( this.$wSettingsBlock.$wSpotPage.attr( 'data-current-page' ));
				
			if( pagesCount == undefined )
				pagesCount = parseInt( this.$wSettingsBlock.$wSpotPage.attr( 'data-pages-count' ));
			
			if( pagesCount <= 100 ) {
				this.$wSettingsBlock.$page = $( '<select/>', {
					'class': 'input-small'
				});
				
				var keysOptions = range( 1, pagesCount );
				var options = combine( keysOptions, keysOptions );
				set$SelectOptions( this.$wSettingsBlock.$page, options, keysOptions );
			}
			else{
				this.$wSettingsBlock.$page = $( '<input/>', {
					'type': 'text',
					'class': 'input-mini'
				});
			}
			this.$wSettingsBlock.$page.val( currentPage + 1 );
			this.$wSettingsBlock.$wSpotPage.html( this.$wSettingsBlock.$page );
		},
		detElements: function() {
			function createFilterDummy() {
				var $tr = $( '<tr/>', {
					'class': 'iFilterDummy i01'
				}).appendTo( this.$wListTable.$wHead );
				var $td = $( '<th/>', {
					'colspan':this.$wListTable.$wHead.$headers.length
				}).appendTo( $tr );
				var $icon = $( '<i/>', {
					'class': 'icon-search'
				}).appendTo( $td );
				return $tr;
			}
			function createWEmptyRow() {
				var $tr = $( '<tr/>', {
					'class': 'iTr i04'
				}).appendTo( this.$wListTable.$wBody );
				var $td = $( '<td/>', {
					'colspan': this.$wListTable.$wHead.$headers.length
				}).html( this.t( 'No results found.' )).appendTo( $tr );
				return $tr;
			}
					
				// wListTable
			this.$wListTable = this.$w.find( '.wListTable' );
			this.$wListTable.$wHead = this.$wListTable.find( '> thead:first' );
			this.$wListTable.$wHead.$headers = this.$wListTable.$wHead.find( '> tr:first > th' );
			this.$wListTable.$wHead.$wFilter = this.$wListTable.$wHead.find( '> tr:eq(1)' );
			this.$wListTable.$wHead.$wFilter.$filters = this.$wListTable.$wHead.$wFilter.find( 'input, select' );
			this.$wListTable.$wHead.$wFilter.$wHide = this.$wListTable.$wHead.$wFilter.find( '.wHide' );
			this.$wListTable.$wBody = this.$wListTable.find( '> tbody:first' );
			this.$wListTable.$wHead.$wFilterDummy = createFilterDummy.call( this );
			
				// wSettingsBlock
			this.$wSettingsBlock = this.$w.find( '.wSettingsBlock' );
			this.$wSettingsBlock.$wToggle = this.$wSettingsBlock.find( '.wToggleSettingsBlock' );
			this.$wSettingsBlock.$wPane = this.$wSettingsBlock.find( '.wSettingsPane' );
			this.$wSettingsBlock.$wSave = this.$wSettingsBlock.find( '.wSaveSettings' );
			this.$wSettingsBlock.$allColumns = this.$wSettingsBlock.find( 'input[name=allColumns]' );
			this.$wSettingsBlock.$columns = this.$wSettingsBlock.find( 'input[name=column]' );
			this.$wSettingsBlock.$limit = this.$wSettingsBlock.find( 'select[name=limit]' );
			this.$wSettingsBlock.$wSpotPage = this.$wSettingsBlock.find( '.wSettingsSpotPage' );
			this.$wSettingsBlock.$resetSizes = this.$wSettingsBlock.find( 'input[name=resetSize]' );
			this.$wSettingsBlock.$resetSort = this.$wSettingsBlock.find( 'input[name=resetSort]' );
							
				// rows
			this.$wEmptyRow = createWEmptyRow.call( this );
		},
		initState: function() {
			var queryState = this.getQueryState();
			this.state.selected = queryState.selected;
			if( !compare( this.state, queryState )) {
				this.replaceQueryStateIntoHistory( this.state );
			}
		},
		popState: function() {
			var queryState = this.getQueryState();
			if( !compare( this.state, queryState )) {
				this.state = queryState;
				this.reload();
			}
		},
		addHandlers: function() {
				// pagination handler
			this.$w.on( 'click', ' > .pagination a', this.proxy( this.handlerPaginationClick ));
			
				// sort handlers
			this.$wListTable.$wHead.$headers.filter( '[data-sorting=yes]' ).on( 'click', this.proxy( this.handlerSortClick ));
			
				// filters handlers
			this.$wListTable.$wHead.$wFilterDummy.click( this.proxy( this.showFilter, true ));
			this.$wListTable.$wHead.$wFilter.$wHide.click( this.proxy( this.handlerFiltersClear ));
			this.$wListTable.$wHead.$wFilter.$filters.on( 'change keydown', this.proxy( this.handlerFiltersSubmit ));
						
				// settings handlers
			this.$wSettingsBlock.$wToggle.click( this.proxy( this.handlerSettingsButtonClick ));
			this.$wSettingsBlock.$wSave.click( this.proxy( this.handlerSaveSettingsClick ));
			this.$wSettingsBlock.$limit.change( this.proxy( this.handlerSettingsLimitChange ));
			this.$wSettingsBlock.$allColumns.change( this.proxy( function() {
				this.$wSettingsBlock.$columns.prop( 'checked', this.$wSettingsBlock.$allColumns.prop( 'checked' ));
			}));
			
				// resize handlers
			if( this.$wListTable.$wHead.$headers.filter( '[data-resize=yes]' ).length > 1 ) {
				this.$wListTable.$wHead.$headers.on( 'mousemove mousedown', this.proxy( this.handlerCommonMouse ));
				this.$wListTable.$wBody.find( ' > tr[data-id] > td' ).on( 'mousemove mousedown', this.proxy( this.handlerCommonMouse ));
			}
			
				// extra actions handlers
			this.$wListTable.$wBody.on( 'click', ' .wExtraActionsButton ', function( e ) {
				$(e.target).next( '.wExtraActionsPane' ).slideToggle( 'fast' );
				return false;
			});
			this.$wListTable.$wBody.on( 'click', ' .wExtraActionsBlock button, .wExtraActionsBlock a ', this.proxy( this.handlerExtraActionsButtons ));
		},
		init: function() {
			ListWidgetBase.private.detElements.call( this );
			!this.isEmpty() && this.$wEmptyRow.hide();
			this.isEmptyFilter() ? this.hideFilter() : this.showFilter();
			ListWidgetBase.private.fillSettingsSpotPage.call( this );
			ListWidgetBase.private.addHandlers.call( this );
			this.loadHeadersSizes();
			this.loadLastVisibleColumns();
		}
	};
	extend( ListWidgetBase, WidgetBase, { // metods
		saveHeadersSizes: function() {
			var $ths = this.$wListTable.$wHead.$headers.filter( '[data-resize=yes]:visible' );
			var widths = {};
			foreach$( $ths, function( i, $th ) {
				var name = $th.attr( 'data-name' );
				var width = $th.width();
				widths[ name ] = width;
			});
			this.setCookieVar( 'widthsHeaders', widths );
		},
		loadHeadersSizes: function() {
			var widths = this.getCookieVar( 'widthsHeaders' ) || {};
			foreach( widths, this, function( name, width ) {
				var selector = '[data-resize=yes][data-name="{name}"]'.replace( '{name}', name );
				this.$wListTable.$wHead.$headers.filter( selector ).width( width );
			});
		},
		loadLastVisibleColumns: function() {
			var lastVisibleColumns = this.getCookieVar( 'lastVisibleColumns' );
			if( !lastVisibleColumns )
				return;
				
			var classes = 'hidden-phone hidden-tablet';
			foreach( lastVisibleColumns.split( ',' ), this, function( i, name ) {
				var $th = this.$wListTable.$wHead.$headers.filter( '[data-name="{name}"]'.replace( '{name}', name ));
				if( !$th.length )
					return;
				
				var index = $th.parent().children().index( $th );
				
				this.$wListTable.$wHead.$headers.eq( index ).removeClass( classes );
				this.$wListTable.$wHead.$wFilter.find( ' > th' ).eq( index ).removeClass( classes );
				this.$wListTable.$wBody.find( ' > tr ' ).find( ' > td:eq( {index} ) '.replace( '{index}', index )).removeClass( classes );
			});
		},
		getRows: function() {
			return this.$wListTable.$wBody.find( 'tr[data-id]' );
		},
		isEmpty: function() {
			return !this.getRows().length;
		},
		isEmptyFilter: function() {
			return !this.$wListTable.$wHead.$wFilter.$filters.filter( '[value!=""]' ).length;
		},
		isEmptyStageFilter: function() {
			var fill = foreach( this.state, this, function( key, value ) {
				if( key.indexOf( 'filter-' ) == 0 && value )
					return true;
			});
			return !fill;
		},
		clearFilter: function() {
			this.$wListTable.$wHead.$wFilter.$filters.val( '' );
		},
		showFilter: function( focus ) {
			this.$wListTable.$wHead.$wFilterDummy.hide();
			this.$wListTable.$wHead.$wFilter.show();
			if( focus && this.$wListTable.$wHead.$wFilter.$filters.length )
				this.$wListTable.$wHead.$wFilter.$filters.eq(0).focus();
		},
		hideFilter: function() {
			this.$wListTable.$wHead.$wFilterDummy.show();
			this.$wListTable.$wHead.$wFilter.hide();
		},
		get: function( id ) {
			return this.getRows().filter( '[data-id="'+id+'"]' );
		},
		getIDs: function() {
			return collectAttrs$( this.getRows(), 'data-id' );
		},
		getStatesObj: function() {
			var obj = merge( {}, this.hiddenState );
			var queryStateName = this.getQueryStateName();
			obj[ queryStateName ] = stateToString( this.state );
			return obj;
		},
		getStatePage: function() {
			var page = this.state.page;
			if( !page )
				return 1;
			page = page.split( ':' );
			return page[0];
		},
		getStateLimit: function() {
			var page = this.state.page;
			if( !page )
				return this.options.limit;
			page = page.split( ':' );
			var limit = page[1];
			this.options.limit = limit;
			return limit;
		},
		reload: function() {
			if( this.getLoading() )
				return;
			
			this.setLoading( true );
			
			function success( data ) {
				var $wList = $( data.list );
				this.replaceWith( $wList );
				this.scrollTo();
				//this.showReloadDummy( true );
			}
			function error( error ) {
				alert( error );
			}
			function complete() {
				this.setLoading( false );
			}
			
			var data = this.getStatesObj();
			//alert( 'reload: ' + $.param( data ));
			
			this.ajax({
				dataType: 'json',
				url: yiiBaseURL + this.options.ajaxLoadListURL,
				data: data,
				success: function( data ) {
					data.error ? error.call( this, data.error )
					           : success.call( this, data );
				},
				complete: complete
			});
		},
		delete: function( id ) {
			if( this.getLoading() )
				return;
			
			var $row = this.get( id );
			if( !$row.length ) 
				return;
			
			this.setLoadingRow( id, true );
			
			var data = {
				id:id,
			};
			
			this.ajax({
				dataType: 'json',
				url: yiiBaseURL + this.options.ajaxDeleteURL,
				data: data,
				success: function( data, textStatus, jqXHR ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						this.reload();
					}
				},
				complete: function( jqXHR, textStatus ) {
					this.setLoadingRow( id, false );
				}
			});
		},
		submitFilters: function() {
			var newState = {};
			foreach$( this.$wListTable.$wHead.$wFilter.$filters, this, function( i, $filter ) {
				var value = $filter.val();
										
				var $th = $filter.closest( 'th' );
				var index = $th.parents().children().index( $th );
				var $header = this.$wListTable.$wHead.$headers.eq( index );
				var name = $header.attr( 'data-name' );
				
				name = 'filter-{name}'.replace( '{name}', name );
				newState[name] = value;
			});
			newState.page = undefined;
			newState.selected = undefined;
			this.changeState( newState );
		},
		getLoadingRows: function() {
			return this.$gridTable.find( 'tr.iLoading' );
		},
		setLoadingRow: function( id, flag ) {
			var $row = this.get( id );
			$row[ flag ? 'addClass' : 'removeClass' ]( 'iLoading' );
			commonLib[ flag ? 'addToArray' : 'removeFromArray' ]( this.loadingIDs, id );
		},
		replaceWith: function( $w ) {
			ListWidgetBase.parent.replaceWith.call( this, $w );
			ListWidgetBase.private.init.call( this );
		},
		changeState: function( obj ) {
			merge( this.state, obj );
			if( this.addQueryStateToHistory( this.state )) {
				this.reload();
			}
		},
		showSettingsBlock: function() {
			function initSettingsColumns() {
				var visibleHeadersNames = collectAttrs$( this.$wListTable.$wHead.$headers.filter( ':visible' ), 'data-name' );

				foreach$( this.$wSettingsBlock.$columns, this, function( i, $column ) {
					var name = $column.attr( 'data-name' );
					$column.prop( 'checked', inArray( visibleHeadersNames, name ));
				});
								
				this.$wSettingsBlock.$allColumns.prop( 'checked', !this.$wSettingsBlock.$columns.not( ':checked' ).length );
			}
			initSettingsColumns.call( this );
			this.$wSettingsBlock.$wPane.slideDown( 'fast' );
		},
		handlerCommonMouse: function( e ) {
			var $element = $( e.target ).closest( 'th, td' );
			var $th = $element.is( 'th' ) ? $element 
			                              : this.$wListTable.$wHead.$headers.eq( $element.parent().children().index( $element ));
			var $td = $element.is( 'td' ) ? $element : undefined;
			
			function getOffsetX() {
				return e.offsetX !== undefined ? e.offsetX + parseInt( $th.css( 'padding-left' ))
					                           : Math.round( e.originalEvent.layerX - $element.position().left );
			}
			
			switch( e.type ) {
				case 'mousemove': {
					if( this.startResizingHeader )
						break;
				
					if( !$th.is( '[data-resize=yes]' ))
						break;
					
					var prev = $th.prev().is( '[data-resize=yes]' );
					var next = $th.next().is( '[data-resize=yes]' );					
				
					if( !prev && !next )
						break;
					
					var offsetX = getOffsetX();
					
					var outerWidth = $th.outerWidth();
					
					this.canResizeHeader = prev && offsetX <= this.options.rangeResizeOn;
					this.canResizeHeader |= next && offsetX >= outerWidth - this.options.rangeResizeOn;
								
					( $td || $th ).css( 'cursor', this.canResizeHeader ? 'move' : '' );
										
					break;
				}
				case 'mousedown': {
					if( !this.canResizeHeader ) 
						break;
						
					if( !$th.is( '[data-resize=yes]' ))
						break;
						
					var offsetX = getOffsetX();
					var outerWidth = $th.outerWidth();	
					
					if( offsetX < outerWidth / 2 )
						$th = $th.prev();
						
					this.startResizingHeader = {
						$th: $th,
						pageX: e.pageX,
						width: $th.width(),
						widthNext: $th.next().width()
					};
					
					return false;
				}
			}
		},
		handlerHeaderResize: function( e ) {
			if( !this.startResizingHeader )
				return;
				
			var newWidth = this.startResizingHeader.width + e.pageX - this.startResizingHeader.pageX;
			var newWidthNext = this.startResizingHeader.widthNext - e.pageX + this.startResizingHeader.pageX;
			
			if( Math.min( newWidth, newWidthNext ) < this.options.minHeaderSize )
				return;
							
			this.startResizingHeader.$th.width( newWidth );
			this.startResizingHeader.$th.next().width( newWidthNext );
		},
		handlerHeaderStopResize: function( e ) {
			this.saveHeadersSizes();
			this.startResizingHeader = false;
		},
		handlerDocumentMouse: function( e ) {
			switch( e.type ) {
				case 'mousemove': {
					if( this.startResizingHeader ) {
						this.handlerHeaderResize( e );
						break;
					}
					break;
				}
				case 'mouseup': {
					if( this.startResizingHeader ) {
						this.handlerHeaderStopResize( e );
						break;
					}
					break;
				}
			}
		},
		handlerPaginationClick: function( e ) {
			if( this.loadingIDs.length ) 
				return false;
				
			var $link =  $( e.target );
			var $li = $link.closest( 'li' );
			
			if( $li.hasClass( 'active' )) {
				this.reload();
				return false;
			}
				
			if( $li.hasClass( 'disabled' ))
				return false;
				
			var href = $link.attr( 'href' );
			var page = parseInt( getQueryVar( 'page', href ));
			
			this.changeState( { 
				page: page > 1 ? strtr( '{page}:{limit}', { 
									'{page}': page, 
									'{limit}': this.getStateLimit()
								 })
							   : undefined,
				selected: undefined
			});
			return false;
		},
		handlerSortClick: function( e ) {
			if( this.loadingIDs.length ) 
				return false;
			
			var $th =  $( e.target ).closest( 'th' );
			
			if( !$th.is( '[class*=sorting]' ))
				return;
			
			var name = $th.attr( 'data-name' );
			var type = $th.attr( 'data-def-sorting-type' );
			
			if( $th.hasClass( 'sorting_asc' ))
				type = 'DESC';
				
			if( $th.hasClass( 'sorting_desc' ))
				type = 'ASC';
				
			var sort;
			if( e.shiftKey ) {
				var oldSort = this.state.sort;
				oldSort = oldSort.split( ',' );
				var exists = foreach( oldSort, function( i, part ) {
					var parts = part.split( ':' );
					var column = parts[0];
					var type = parts[1];
					if( column == name ) {
						if( type == $th.attr( 'data-def-sorting-type' ) || oldSort.length == 1 ) {
							type = type == 'ASC' ? 'DESC' : 'ASC';
							oldSort[i] = [ column, type ].join( ':' );
						}
						else{
							oldSort.splice( i, 1 );
						}
						return true;
					}
				});
				if( !exists )		
					oldSort.push( name + ':' + type );

				sort = oldSort.join( ',' );
			}
			else
				sort = name + ':' + type;
			
			this.changeState( { sort: sort, page: undefined, selected: undefined });
			return false;
		},
		handlerFiltersClear: function( e ) {
			if( this.isEmptyStageFilter() ) {
				this.clearFilter();
				this.hideFilter();
				return;
			}
			
			foreach$( this.$wListTable.$wHead.$wFilter.$filters, this, function( i, $filter ) {
				$filter.val( '' );
			});
			this.submitFilters();
		},
		handlerFiltersSubmit: function( e ) {
			if( e.keyCode != 13 && !$(e.target).is( 'select' ))
				return;
			
			this.submitFilters();
		},
		handlerSettingsButtonClick: function() {
			this.$wSettingsBlock.$wPane.is( ':visible' ) ? this.$wSettingsBlock.$wPane.slideUp( 'fast' )
			                                             : this.showSettingsBlock();		
		},
		handlerSaveSettingsClick: function() {
			var $checkedColumns = this.$wSettingsBlock.$columns.filter( ':checked' );
			var colNames = collectAttrs$( $checkedColumns, 'data-name' );
			var cols = colNames.join(',');
			var resetSizes = this.$wSettingsBlock.$resetSizes.prop( 'checked' );
			var resetSort = this.$wSettingsBlock.$resetSort.prop( 'checked' );
			var limit = parseInt( this.$wSettingsBlock.$limit.val());
			var page = parseInt( this.$wSettingsBlock.$page.val());
						
			if( cols == 'checker' ) {
				alert( this.t( "Can't select only checker!" ));
				return false;
			}
			if( cols == '' ) {
				alert( this.t( "Need select columns!" ));
				return false;
			}

			this.$wSettingsBlock.$wPane.hide();
			
			if( cols != this.hiddenState.cols || resetSizes ) {
				this.setCookieVar( 'widthsHeaders', undefined );
			}			
			
			var sort;
			if( !resetSort ) {
				sort= this.state.sort;
				sort = sort.split( ',' );
				sort = array_filter( sort, function( i, part ) {
					var parts = part.split( ':' );
					var column = parts[0];
					return inArray( colNames, column );
				});
				sort = sort.join( ',' );
				if( !sort.length ) {
					sort = foreach( colNames, this, function( i, name ) {
						var $th = this.$wListTable.$wHead.$headers.filter( '[data-name="{name}"]'.replace( '{name}', name ));
						if( !$th.is( '[data-sorting=yes]' )) 
							return undefined;
						
						var type = $th.attr( 'data-def-sorting-type' );
						return strtr( "{name}:{type}", {
							'{name}': name,
							'{type}': type
						});
					});
				}
			}
			if( !sort ) 
				sort = 'id:ASC';
			
			if( sort != this.state.sort )
				page = 1;
			
			var newState = {};
			if( page != this.getStatePage() || limit != this.getStateLimit()) {
				if( page == 1 && limit == this.getStateLimit()) {
					newState.page = undefined;
					this.options.limit = limit;
				}
				else{
					newState.page = strtr( '{page}:{limit}', {
						'{page}': page,
						'{limit}': limit
					});
				}
			}
									
			if( sort != this.state.sort )
				newState.sort = sort;
			
			this.hiddenState.cols = cols;
			
			this.setCookieVar( 'lastVisibleColumns', cols );
			
			if( this.state.selected && newState.page != this.state.page )
				newState.selected = undefined;
			
			keys( newState ).length ? this.changeState( newState )
			                        : this.reload();
						
			return false;
		},
		handlerPopstate: function() {
			ListWidgetBase.private.popState.call( this );
		},
		handlerSettingsLimitChange: function() {
			var page, pagesCount;
			var limit = parseInt( this.$wSettingsBlock.$limit.val());
			if( limit == -1 ) {
				page = 0;
				pagesCount = 1;
			}
			else{
				var stagePage = this.getStatePage();
				var stageLimit = this.getStateLimit();
				var oldStart = stageLimit * (stagePage - 1 || 0);
				var totalCount =  parseInt( this.$wSettingsBlock.$wSpotPage.attr( 'data-total-items-count' ));
				page = div( oldStart, stageLimit );
				pagesCount = div( totalCount + limit - 1, limit );
			}
			ListWidgetBase.private.fillSettingsSpotPage.call( this, page, pagesCount );
		},
		handlerExtraActionsButtons: function( e ) {
			var $b = $( e.target );
			var $pane = $b.closest( '.wExtraActionsPane' );
			var id = $b.closest( 'tr[data-id]' ).attr( 'data-id' );
			
			if( $b.is( '.wEdit' )) {
				var url = yiiBaseURL + strtr( this.options.editURL, {
					'{id}': id,
				});
				$b.attr( {
					'href': url,
					'target': '_blank'
				});
				$pane.slideUp( 'fast' );
				return true;
			}
			
			if( $b.is( '.wDelete' )) 
				if( confirm( this.t( 'Delete?' )))
					this.delete( id );
			
			$pane.slideUp( 'fast' );
			return false;
		}
	});
	
		// jquery funcs
	$.fn.showWidgetElementTab = function() {
		var $tab = this.closest( '*[class^="tab-pane"]' );
		if( $tab.length ) {
			var id = $tab.attr( 'id' );
			if( id && id.length ) {
				var $link = $( '*[href="#'+id+'"]' )
				if( $link.length ) {
					$link.tab( 'show' );
					$tab.parent().showWidgetElementTab();
				}
			}
		}
	};
	$.fn.getEngineWidget = function() {
		return this.data( widgetDataKey );
	};
	$.fn.setEngineWidget = function( widget ) {
		this.data( widgetDataKey, widget );
	};
	$.fn.constructorEngineWidget = function( className, options ) {
		var widgets = [];
		this.each( function() {
			var widget = new classes[ className ]( this, options );
			widgets.push( widget );
		});
		return widgets.length > 1 ? widgets : widgets[0];
	};
	$.fn.instanceEngineWidget = function( className, instanceOptions, options ) {
		var widgets = [];
		this.each( function() {
			var widget = classes[ className ].static.instance( this, instanceOptions, options );
			widgets.push( widget );
		});
		return widgets.length > 1 ? widgets : widgets[0];
	};
}( window.jQuery );

var time = {
	setautotz:function () {
		x = new Date();
		tz = -x.getTimezoneOffset()/60;
		time.settz(tz);
	},
	settz:function (tz,force,reload) {
		$.ajax({
			type: "GET",
			url: "/services/user/setTimezone",
			data: "utc="+tz+"&force="+force,
			success: function(msg){
			  console.log(force);
			  if ( reload === true ) {
				  location.reload();
			  }
			}
		});
	}
};

/*init*/

time.setautotz();

