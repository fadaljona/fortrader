var commonLib;
!function( $ ) {
	commonLib = {
		getCurrentDate:function() {
			var date = new Date();
			return commonLib.getDate( date );
		},
		getShortDate: function( date ) {
			var m = date.getMonth();
			var d = date.getDate();
			var mStr = ( m + 1 < 10 ? '0' : '' ) + ( m + 1 ).toString();
			var dStr = ( d < 10 ? '0' : '' ) + d.toString();
			var dateArray = [ mStr, dStr ];
			var dateStr = dateArray.join( '.' );
			return dateStr;
		},
		getDate: function( date ) {
			var y = date.getFullYear();
			var m = date.getMonth();
			var d = date.getDate();
			var yStr = y.toString();
			var mStr = ( m + 1 < 10 ? '0' : '' ) + ( m + 1 ).toString();
			var dStr = ( d < 10 ? '0' : '' ) + d.toString();
			var dateArray = [ yStr, mStr, dStr ];
			var dateStr = dateArray.join( '-' );
			return dateStr;
		},
		getDT: function( date ) {
			var y = date.getFullYear();
			var m = date.getMonth();
			var d = date.getDate();
			var h = date.getHours();
			var i = date.getMinutes();
			var s = date.getSeconds();
			
			var yStr = y.toString();
			var mStr = ( m + 1 < 10 ? '0' : '' ) + ( m + 1 ).toString();
			var dStr = ( d < 10 ? '0' : '' ) + d.toString();
			
			var dateArray = [ yStr, mStr, dStr ];
			var dateStr = dateArray.join( '-' );
			
			var hStr = ( h < 10 ? '0' : '' ) + ( h ).toString();
			var iStr = ( i < 10 ? '0' : '' ) + ( i ).toString();
			var sStr = ( s < 10 ? '0' : '' ) + ( s ).toString();
			
			var timeArray = [ hStr, iStr, sStr ];
			var timeStr = timeArray.join( ':' );
			
			var dtArray = [ dateStr, timeStr ];
			var dtStr = dtArray.join( ' ' );
			
			return dtStr;
		},
		formatDate: function( format, date ) {
			var y = date.getFullYear();
			var m = date.getMonth();
			var d = date.getDate();
			var h = date.getHours();
			var i = date.getMinutes();
			var s = date.getSeconds();
			
			var yStr = y.toString();
			var mStr = ( m + 1 < 10 ? '0' : '' ) + ( m + 1 ).toString();
			var dStr = ( d < 10 ? '0' : '' ) + d.toString();
			var hStr = ( h < 10 ? '0' : '' ) + ( h ).toString();
			var iStr = ( i < 10 ? '0' : '' ) + ( i ).toString();
			var sStr = ( s < 10 ? '0' : '' ) + ( s ).toString();
			
			format = format.replace( 'Y', yStr );
			format = format.replace( 'm', mStr );
			format = format.replace( 'd', dStr );
			format = format.replace( 'H', hStr );
			format = format.replace( 'i', iStr );
			format = format.replace( 's', sStr );
			
			return format;
		},
		getAbsoluteUrl:function() {
			var index = document.location.href.indexOf( '?' );
			return index == -1 ? document.location.href : document.location.href.substr( 0, index );
		},
		getQueryString:function( url ) {
			url = url ? url : document.location.href;
			var index = url.indexOf( '?' );
			return index == -1 ? '' : url.substr( index + 1 );
		},
		getVar:function( name ) {
			var patt = new RegExp( '(?:\\?|&)'+name+'=([^&$]+)' );
			var matchs = patt.exec( document.location.href );
			return matchs ? matchs[1] : false;
		},
		baseName:function( path ) {
			var index = path.lastIndexOf( '/' );
			return index == -1 ? path : path.substr( index+1 );
		},
		initDatepicker: function() {
			$.datepicker.regional['ru'] = { 
				closeText: 'Закрыть', 
				prevText: '&#x3c;Пред', 
				nextText: 'След&#x3e;', 
				currentText: 'Сегодня', 
				monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь', 'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'], 
				monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'], 
				dayNames: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'], 
				dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'], 
				dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'], 
				dateFormat: 'yy-mm-dd', 
				firstDay: 1, 
				isRTL: false 
			};
			if( yiiLanguage == 'ru' ) {
				$.datepicker.setDefaults( $.datepicker.regional['ru'] );
			}
		},
		explodeTimeDiff: function( seconds ) {
			var result = { seconds: seconds };
			result.minets = Math.floor( result.seconds / 60 );
			result.hours = Math.floor( result.minets / 60 );
			result.days = Math.floor( result.hours / 24 );
			result.months = Math.floor( result.days / 30 );
			result.years = Math.floor( result.months / 12 );
			return result;
		},
		detWord: function( x, ip, rp, mrp ) {
			var A = x % 100;
			var B = x % 10;
			if ( 11 <= A && A <= 20 ) return mrp;
			if ( B == 1 ) return ip;
			if ( 2 <= B && B <= 4 ) return rp;
			if ( 5 <= B && B <= 9 || B == 0 ) return mrp;
		},
		round: function( x, n ) {
			var e = Math.pow( 10, n );
			return Math.round( x * e ) / e;
		},
	};
}( window.jQuery );