!function( $ ) {
		// commonLib
	var commonLib = engine.commonLib;
	
	var mt_rand = commonLib.mt_rand;
	var between = commonLib.between;
	var div = commonLib.div;
	var isHistoryAPI = commonLib.isHistoryAPI;
		// strings
	var lcfirst = commonLib.lcfirst;
	var ucfirst = commonLib.ucfirst;
	var strtr = commonLib.strtr;
	var objtr = commonLib.objtr;
	var strip_scripts = commonLib.strip_scripts;
		// urls
	var filterURL = commonLib.filterURL;
	var getQueryString = commonLib.getQueryString;
	var getQueryObj = commonLib.getQueryObj;
	var getQueryVar = commonLib.getQueryVar;
	var stateToString = commonLib.stateToString;
		// arrays
	var foreach = commonLib.foreach;
	var slice = commonLib.slice;
	var merge = commonLib.merge;
	var mergeDef = commonLib.mergeDef;
	var mergeExists = commonLib.mergeExists;
	var removeFromArray = commonLib.removeFromArray;
	var addToArray = commonLib.addToArray;
	var array_diff = commonLib.array_diff;
	var array_intersect = commonLib.array_intersect;
	var array_filter = commonLib.array_filter;
	var array_map = commonLib.array_map;
	var inArray = commonLib.inArray;
	var shifts = commonLib.shifts;
	var compare = commonLib.compare;
	var range = commonLib.range;
	var combine = commonLib.combine;
	var keys = commonLib.keys;
		// funcs
	var applies = commonLib.applies;
	var calls = commonLib.calls;
		// jquery
	var foreach$ = commonLib.foreach$;
	var collectAttrs$ = commonLib.collectAttrs$;
	var set$SelectOptions = commonLib.setSelectOptions;
		// classes
	var extend = commonLib.extend;
			
		// CLASSES
	var classes = engine.classes;
	var base = classes.base;
	
	var objects = engine.objects;
	var docObj = objects.docObj;
	
		// WIDGETS
	var instanceWidget = engine.instanceWidget;
	
		// WidgetBase
	var WidgetBase = classes.WidgetBase;
	var AccordionWidget = classes.AccordionWidget;
	var ListWidgetBase = classes.ListWidgetBase;
	
		// AdminSelectWidget
	var AdminSelectWidget = classes.AdminSelectWidget = function AdminSelectWidget( element, options ) { // constructor
		AdminSelectWidget.parent.constructor.call( this, element, options );
		
			// events
		this.onLoadVariants = [];
		
		AdminSelectWidget.private.init.call( this );
	};
	AdminSelectWidget.private = { // private metods
		init: function() {
			this.createWReload();
		}
	};
	extend( AdminSelectWidget, WidgetBase, { // metods
		createWReload: function() {
			if( this.$w.next().is( '.wReloadList' )) {
				this.$wReload = this.$w.next();
			}
			else{
				var $icon = $( '<i />', {
					'class': 'icon-repeat'
				});
				this.$wReload = $( '<button />', {
					'class': 'btn btn-mini btn-inverse iReloadListButton i01 wReloadList',
					type: 'button',
					title: this.t( 'Refresh' )
				}).html( $icon );
							
				this.$w.after( this.$wReload );
			}
			this.$wReload.click( this.proxy( function() {
				this.reloadVariants();
			}));
		},
		loadVariants: function( variants, keysVariants ) {
			set$SelectOptions( this.$w, variants, keysVariants );
			calls( this.onLoadVariants, this );
		},
		reloadVariants: function( url ) {
			var name = this.$w.attr( 'name' );
			var $optionLoading = $( '<option/>' ).html( this.t( 'Loading...' ));
			var holdedOptions = this.$w.html();
			var holdedVal = this.$w.val();

			this.$w.html( $optionLoading );
			this.disableW( this.$w );
			this.disableW( this.$wReload );
			
			function success( data ) {
				var $optionLoaded = $( '<option/>' ).html( this.t( 'Loaded...' ));
				this.$w.html( $optionLoaded );
				setTimeout( this.proxy( load, data.variants, data.keysVariants ), 300 );
			}
			function load( variants, keysVariants ) {
				this.loadVariants( variants, keysVariants );
				this.$w.val( holdedVal );
				complete.call( this );
			}
			function revert() {
				this.$w.html( holdedOptions );
				this.$w.val( holdedVal );
				complete.call( this );
			}
			function error( message ) {
				alert( message );
				revert.call( this );
			}
			function ajaxError() {
				this.ajaxError();
				revert.call( this );
			}
			function complete() {
				this.enableW( this.$w );
				this.enableW( this.$wReload );
			}
			
			this.ajax({
				dataType: 'json',
				url: url ? url : this.options.ajaxLoadFieldVariantsURL,
				data: {name:name},
				success: function( data ) {
					if( data.error ) 
						error.call( this, data.error );
					else
						success.call( this, data );
				},
				error: ajaxError
			});
		}
	});
	
		// AdminSetListWidget
	var AdminSetListWidget = classes.AdminSetListWidget = function AdminSetListWidget( element, options ) { // constructor
		AdminSetListWidget.parent.constructor.call( this, element, options );
		
			// init
		AdminSetListWidget.private.init.call( this );
	};
	AdminSetListWidget.static = { // static metods
		classField: 'fSetListKeys',
		w: 'wSetList'
	};
	AdminSetListWidget.private = { // private metods
		init: function() {
			this.$wOptions = this.$w.find( '.wOptions' );
			this.$wHiddenOptions = this.$w.find( '.wHiddenOptions' );
			this.$wList = this.$w.find( '.wList' );
			this.$wEmpty = this.$wList.find( '.wEmpty' );
			this.$wTpl = this.$wList.find( '.wTpl' );
			this.$wField = this.$w.find( 'input[type=hidden]' );
			this.$w.on( 'click', '.wAdd', this.proxy( this.handlerAdd ));
			this.$w.on( 'click', '.wClear', this.proxy( this.handlerClear ));
			this.$w.on( 'click', '.wDelete', this.proxy( this.handlerDelete ));
			this.wSelect = this.$w.find( 'select' ).getEngineWidget();
			
			this.keysOptions = this.$wOptions.attr( 'data-keys' ).split( ',' );
				
				// handlers
			this.wSelect.onLoadVariants.push( this.proxy( this.handleLoadVariants ));
			
			this.showAllOptions();
		}
	};
	extend( AdminSetListWidget, WidgetBase, { // metods
		getOptions: function() {
			return this.getShowedOptions().add( this.getHiddenOptions() );
		},
		getShowedOptions: function() {
			return this.$wOptions.find( 'option' );
		},
		getHiddenOptions: function() {
			return this.$wHiddenOptions.find( 'option' );
		},
		getOption: function( key ) {
			var $option = this.getOptions().filter( '[value="'+key+'"]' );
			return $option;
		},
		showOption: function( $option, holdValue ) {
			if( this.$wOptions.has( $option ).length )
				return;
			
			var holdedValue;
			if( holdValue )
				holdedValue = this.$wOptions.val();
			
			var $before;
			var key = $option.attr( 'value' );
			var indexNew = this.keysOptions.indexOf( key );
			foreach$( this.getShowedOptions(), this, function( i, $showedOption ) {
				var key = $showedOption.attr( 'value' );
				if( this.keysOptions.indexOf( key ) > indexNew ) {
					$before = $showedOption;
					return false;
				}
			});
			
			$before ? $before.before( $option )
			        : this.$wOptions.append( $option );
					
			if( holdValue && holdedValue && holdedValue.length )
				this.$wOptions.val( holdedValue );
		},
		showAllOptions: function() {
			foreach$( this.getHiddenOptions(), this, function( i, $option ) {
				this.showOption( $option );
			});
			this.$wOptions.val( '' );
		},
		hideOption: function( $option ) {
			var selectedIndex = this.$wOptions[0].selectedIndex;
			this.$wHiddenOptions.append( $option );
			
			if( selectedIndex > this.getShowedOptions().length - 1 )
				selectedIndex--;
				
			this.$wOptions[0].selectedIndex = selectedIndex;
		},
		inList: function( key ) {
			return this.getKeys().indexOf( key ) != -1;
		},
		getKeys: function() {
			var value = this.$wField.val();
			return value.length ? value.split( ',' ) : [];
		},
		setKeys: function( keys ) {
			var value = keys.join( ',' );
			this.$wField.val( value );
		},
		addKey: function( key ) {
			var keys = this.getKeys();
			addToArray( keys, key );
			this.setKeys( keys );
		},
		deleteKey: function( key ) {
			var keys = this.getKeys();
			removeFromArray( keys, key );
			this.setKeys( keys );
		},
		getItems: function() {
			return this.$wList.find( '*[data-key]' );
		},
		getCountItems: function() {
			return this.getKeys().length;
		},
		getItem: function( key ) {
			return this.getItems().filter( '[data-key="'+key+'"]' );
		},
		setItemTitle: function( $item, title ) {
			$item.find( '.wTitle' ).html( title );
		},
		addItem: function( key, title, onTop, immediately ) {
			var $item = this.$wTpl.clone();
			$item.removeClass( 'wTpl' );
			$item.attr( 'data-key', key );
			this.setItemTitle( $item, title );
			
			var $before;
			var indexNew = this.keysOptions.indexOf( key );
			foreach$( this.getItems(), this, function( i, $item ) {
				var key = $item.attr( 'data-key' );
				if( this.keysOptions.indexOf( key ) > indexNew ) {
					$before = $item;
					return false;
				}
			});
			
			$before ? $before.before( $item )
			        : this.$wList.append( $item );
			
			immediately ? this.$wEmpty.hide() : this.$wEmpty.slideUp();
			immediately ? $item.show() : $item.slideDown();
			
			this.addKey( key );
			var $option = this.getOption( key )
			this.hideOption( $option );
		},
		deleteItem: function( key ) {
			this.deleteKey( key );
			
			var $div = this.getItem( key );
			$div.slideUp( function(){
				$div.remove();
			});
			
			if( this.getCountItems() == 0 ) 
				this.$wEmpty.slideDown();
			
			var $option = this.getOption( key )
			this.showOption( $option, true );
		},
		load: function( data ) {
			this.clear();
			
			if( !data ) 
				return;
			
			var keys = typeof data == 'string' ? data.split( ',' ) : data;
			foreach( keys, this, function( i, key ) {
				var $option = this.getOption( key );
				var title = $option.html();
				this.addItem( key, title, false, true );
			});
		},
		clear: function( scrolled ) {
			this.setKeys( [] );
			var $items = this.getItems();
			scrolled ? $items.slideUp( function() { $items.remove(); } ) : $items.remove();
			scrolled ? this.$wEmpty.slideDown() : this.$wEmpty.show();
			this.showAllOptions();
		},
		handlerAdd: function() {
			var $option = this.$wOptions.find( 'option:selected' );	
			var key = $option.attr( 'value' );
			var title = $option.html();	
			
			if( key == undefined ) return;
			if( !this.inList( key )) {
				this.addItem( key, title, true );
			}
		},
		handlerDelete: function( e ) {
			var $link = $(e.currentTarget);
			var $div = $link.closest( '*[data-key]' );
			var key = $div.attr( 'data-key' );
			
			if( this.inList( key )) {
				this.deleteItem( key );
			}
		},
		handlerClear: function( e ) {
			this.clear( true );
		},
		handleLoadVariants: function() {
			this.getHiddenOptions().remove();
			foreach( this.getKeys(), this, function( i, key ) {
				var $option = this.getOption( key );
				var $item = this.getItem( key );
				var title = $option.html();
				this.setItemTitle( $item, title );
				this.hideOption( $option );
			});
		}
	});
	
		// AdminEditorWidgetBase
	var AdminEditorWidgetBase = classes.AdminEditorWidgetBase = function AdminEditorWidgetBase( element, options ) { // constructor
			// options
		var options = merge({
			wForm: 'wAdmin{modelName}FormWidget',
			wList: 'wAdmin{modelName}ListWidget',
			ajaxDeleteURL: '/admin/{modelName}/ajaxDelete'
		}, options );
		objtr( options, [ 'wForm', 'wList' ], { '{modelName}': options.modelName });
		objtr( options, 'ajaxDeleteURL'.split(','), { '{modelName}': lcfirst( options.modelName )});
			// parent constructor
		AdminEditorWidgetBase.parent.constructor.call( this, element, options );
		
			// vars
		this.listSelectionIDs = [];
		
		AdminEditorWidgetBase.private.init.call( this );
	}
	AdminEditorWidgetBase.static = { // static metods
		instance: function( element, instanceOptions, options ) {
			var className = 'Admin{modelName}EditorWidget'.replace( '{modelName}', instanceOptions.modelName );
			return instanceWidget( className, AdminEditorWidgetBase, element, instanceOptions, options );
		}
	};
	AdminEditorWidgetBase.private = { // private metods
		init: function() {
			this.$wAlert = this.findInINS( '.wAlert' );
			this.$wAdd = this.findInINS( '.wAdd' );
			this.$wDelete = this.findInINS( '.wDelete' );
							
			this.wForm = $( '.' + this.options.wForm ).getEngineWidget();
			this.wList = $( '.' + this.options.wList ).getEngineWidget();

			this.$wAdd.click( this.proxy( this.handleAdd ));
			this.$wDelete.click( this.proxy( this.handleDelete ));
			this.wList.onSelect.push( this.proxy( this.onListSelect ));
			this.wList.onUnselect.push( this.proxy( this.onListUnselect ));
			
			var onListCheckedChanged = this.proxy( this.onListCheckedChanged );
			this.wList.onCheck.push( onListCheckedChanged );
			this.wList.onUncheck.push( onListCheckedChanged );
			
			this.wForm.onInsert.push( this.proxy( this.onFormInsert ));
			this.$wDelete.hide();
			
			foreach( this.wList.selectedIDs, this, function( i, id ) {
				this.onListSelect( id );
			});
		}
	};
	extend( AdminEditorWidgetBase, WidgetBase, { // metods
		getControllers: function() {
			return this.$wAdd.add( this.$wDelete );
		},
		hideForm: function( options ) {
			this.wForm.hide({
				onAfter: this.proxy( function() {
					this.scrollTo({
						onAfter: options && options.onAfter
					});
				})
			});
		},
		delete: function( ids ) {
			if( this.loading ) return;
			
			var lDelete = this.$wDelete.html();
			this.$wDelete.html( this.t( 'Deleting...' ));
			this.setLoading( true );
						
			function success() {
				this.wList.reload();
				this.$wDelete.html( this.t( 'Deleted' ));
				this.disableW( this.$wDelete, true );
				setTimeout( this.proxy( function() {
					function on() {
						this.enableW( this.$wDelete );
						this.$wDelete.html( lDelete );
					}
					// if( !this.wList.checkedIDs.length ) 
					this.$wDelete.fadeOut( this.proxy( on ));
					//else
					//	on.call( this );
				}), 1000 );
				complete.call( this );
			}
			function revert() {
				this.$wDelete.html( lDelete );
				complete.call( this );
			}
			function error( error ) {
				alert( error );
				revert.call( this );
			}
			function ajaxError() {
				this.ajaxError();
				revert.call( this );
			}
			function complete() {
				this.setLoading( false );
			}
			
			this.ajax({
				dataType: 'json',
				url: yiiBaseURL + this.options.ajaxDeleteURL,
				data: {id:ids.join(',')},
				success: function( data ) {
					data.error ? error.call( this, data.error )
					           : success.call( this );
				},
				error: ajaxError
			});
		},
		handleAdd: function() {
			if( this.wForm.getLoading() ) 
				return false;
		
			if( this.wForm.isHidden() || this.wForm.loadedModel ) {
				this.wForm.clear();
				this.wForm.show();
			}
			else
				this.hideForm();
		},
		handleDelete: function() {
			if( this.wList.loadingIDs.length )
				return;
			
			if( this.wList.checkedIDs.length ) {
				this.disableW( this.$wDelete );
				var revert = !confirm( this.t( 'Delete selected?' ));
				this.enableW( this.$wDelete );
				if( revert )
					return;
				
				this.delete( [].concat( this.wList.checkedIDs ));
			}
		},
		onListSelect: function( id ) {
			if( this.wList.isLoading( id ))
				return;
			
			var wForm = this.wList.getForm( id ).getEngineWidget();
			
			if( wForm && wForm.getID()) 
				return;
			
			function createForm() {
				var wForm = this.wForm.clone();
			
				function removeForm( options ) {
					wForm.hide({
						onAfter: this.proxy( function(){
							var $row = this.wList.get( id );
							this.wList.removeForm( id );
							this.scrollTo({
								target: $row,
								onAfter: function() {
									if( options && options.onAfter )
										options.onAfter();
								}
							});
						})
					});
				}
				function onInsert( newID ) {
					this.wList.unselect( id );
					removeForm.call( this, {
						onAfter: this.proxy( function () {
							this.wList.insert( newID, id );
						})
					});
				}
				function onUpdate() {
					removeForm.call( this, {
						onAfter: this.proxy( function () {
							this.wList.update( id );
						})
					});
				}
				function onDelete() {
					removeForm.call( this, {
						onAfter: this.proxy( function () {
							this.wList.reload();
						})
					});
				}
				function onClose() {
					removeForm.call( this, {
						onAfter: this.proxy( function () {
							this.wList.unselect( id );		
						})
					});
				}
				function onCopy() {
					this.wList.unselect( id );
				}
				function onEnable() {
					this.wList.setLoadingRow( id, false );
				}
				function onDisable() {
					this.wList.setLoadingRow( id, true );
				}
				
				wForm.onInsert.push( this.proxy( onInsert ));
				wForm.onUpdate.push( this.proxy( onUpdate ));
				wForm.onDelete.push( this.proxy( onDelete ));
				wForm.onClose.push( this.proxy( onClose ));
				wForm.onCopy.push( this.proxy( onCopy ));
				wForm.onEnable.push( this.proxy( onEnable ));
				wForm.onDisable.push( this.proxy( onDisable ));
				
				return wForm;
			}
			
			if( !wForm ) 
				wForm = createForm.call( this );
			
			this.wList.setLoadingRow( id, true );
			wForm.load( id, {
				onAfter: this.proxy( function () {
					if( this.wList.getLoading())
						return;
						
					this.wList.setLoadingRow( id, false );
					this.wList.appendForm( id, wForm.$w );
					wForm.show();
				})
			});
			
			if( !wForm.isVisible()) 
				wForm.hideReloadDummy();
		},
		onListUnselect: function( id ) {
			if( !this.wList.hasForm( id ))
				return;
				
			if( this.wList.isLoading( id ))
				return;
			
			var $wForm = this.wList.getForm( id );
			if( $wForm.length ) {
				var wForm = $wForm.getEngineWidget();
				
				if( !wForm.getID())
					return;
				
				wForm.hide({
					onAfter: this.proxy( function(){
						this.wList.removeForm( id );
					})
				});
			}
		},
		onListCheckedChanged: function() {
			if( this.loading ) 
				return;
				
			if( this.wList.checkedIDs.length ) 
				this.$wDelete.fadeIn( 'fast' );
			else
				this.$wDelete.fadeOut( 'fast' );
		},
		onFormInsert: function( id ) {
			this.hideForm({
				onAfter: this.proxy( function() {
					this.wList.insert( id );		
				})
			});
		}
	});
	
		// AdminFormWidgetBase
	var AdminFormWidgetBase = classes.AdminFormWidgetBase = function AdminFormWidgetBase( element, options ) { // constructor
			// options
		var options = merge({
			pkName: 'id',
			errorClass: 'error',
			sliding: true,
			formName: 'Admin{modelName}FormModel',
			ajaxSubmitURL: '/admin/{modelName}/ajaxAdd',
			ajaxLoadURL: '/admin/{modelName}/ajaxLoad',
			ajaxDeleteURL: '/admin/{modelName}/ajaxDelete',
			ajaxLoadFieldVariantsURL: '/admin/{modelName}/ajaxLoadFieldVariants'
		}, options );
		objtr( options, 'formName'.split(','), { '{modelName}': options.modelName });
		objtr( options, 'ajaxSubmitURL,ajaxLoadURL,ajaxDeleteURL,ajaxLoadFieldVariantsURL'.split(','), { '{modelName}': lcfirst( options.modelName )});
			// parent constructor
		AdminFormWidgetBase.parent.constructor.call( this, element, options );
		
			// events
		this.onInsert = [];
		this.onUpdate = [];
		this.onDelete = [];
		this.onCopy = [];
		this.onLoad = [];
		this.onClose = [];
		
			// init
		AdminFormWidgetBase.private.init.call( this );
	}
	AdminFormWidgetBase.static = { // static metods
		instance: function( element, instanceOptions, options ) {
			var className = 'Admin{modelName}FormWidget'.replace( '{modelName}', instanceOptions.modelName );
			return instanceWidget( className, AdminFormWidgetBase, element, instanceOptions, options );
		}
	};
	AdminFormWidgetBase.private = { // private metods
		init: function() {
			this.immediatelyHide();
			this.$wTitle = this.findInINS( '.wTitle' );
			this.$form = this.$w.find( 'form' );
				// buttons
			this.$wDelete = this.findInINS( '.wDelete' );
			this.$wClose = this.findInINS( '.wClose' );
			this.$wReset = this.findInINS( '.wReset' );
			this.$wCopy = this.findInINS( '.wCopy' );
			this.$wCopy.l = this.$wCopy.html();
			this.$wSubmit = this.findInINS( '.wSubmit' );
			
				// sent handlers
			this.$wDelete.click( this.proxy( this.delete ));
			this.$wClose.click( this.proxy( this.close ));
			this.$wReset.click( this.proxy( this.reset ));
			this.$wCopy.click( this.proxy( this.copy ));
			this.$wSubmit.click( this.proxy( this.submit ));
			this.onShowing.push( this.proxy( this.setAccordionsState ));
			this.onShow.push( this.proxy( this.setFirstFocus ));
			
			this.createSelectsWidgets();
			this.createSetListsWidgets();
			
			if( this.options.asCopy ) 
				this.copyAccordionsWidgets();
			
			this.addAccordionsHandlers();
		}
	};
	extend( AdminFormWidgetBase, WidgetBase, { // metods
		resolveName: function( fieldName ) {
			if( fieldName.indexOf( this.options.formName ) == -1 ) {
				var p = fieldName.indexOf( '[' );
				var ex = '';
				
				if( p != -1 ) {
					var ex = fieldName.substr( p );
					fieldName = fieldName.substr( 0, p );
				}
				
				var pattern = '{formName}[{fieldName}]{ex}';
				fieldName = strtr( pattern, {
					'{formName}': this.options.formName,
					'{fieldName}': fieldName,
					'{ex}': ex
				});
			}
			return fieldName;
		},
		unresolveName: function( fieldName ) {
			if( fieldName.indexOf( this.options.formName ) == 0 ) {
				fieldName = fieldName.substr( this.options.formName.length );
			}
			if( fieldName.indexOf( '[' ) == 0 ) {
				var p = fieldName.indexOf( ']' );
				if( p != -1 ) {
					fieldName = fieldName.substr( 1, p - 1 );
				}
			}
			return fieldName;
		},
		getField: function( fieldName, strict ) {
			if( strict == undefined ) strict = true;
			var resolvedName = this.resolveName( fieldName );
			
			var pattern = '*[name{eq}"{fieldName}"]';
			var selector = strtr( pattern, {
				'{eq}': strict ? '=' : '^=',
				'{fieldName}': resolvedName
			});
			
			var $field = this.$w.find( selector );
			return $field;
		},
		setTitle: function( value ) {
			this.$wTitle.find( 'span:first' ).html( value );
		},
		setID: function( value ) {
			this.getField( this.options.pkName ).val( value );
		},
		getID: function( value ) {
			return this.getField( this.options.pkName ).val();
		},
		setFirstFocus: function() {
			var $elements = this.$w.find( 'input[type="text"]:not([readonly])' );
			if( $elements.length ) {
				$element = $( $elements[0] );
				$element.showWidgetElementTab();
				this.scrollTo({
					target: $element,
					onAfter: function() {
						$elements[0].focus();
					}
				});
			}
		},
		load: function( id, options ) {
			if( this.getLoading() ) return false;
			this.loadingID = id;
			this.clearErrors();
			this.disableW( this.$wSubmit );
						
			var lSubmit = this.$wSubmit.html();
			this.setTitle( this.t( 'Editor object' ));
			this.$wSubmit.html( this.t( 'Loading...' ));
			this.setLoading( true );
									
			this.ajax({
				dataType: 'json',
				url: yiiBaseURL + this.options.ajaxLoadURL,
				data: {id:id},
				success: function( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						this.loadModel( data.model );
						this.$wDelete.show();
						this.$wCopy.removeClass( 'removal' )
						this.$wCopy.html( this.$wCopy.l );
						this.enableW( this.$wCopy );
						this.slideRight({ target: this.$wCopy });
						
						if( options && options.onAfter ) 
							options.onAfter();
						
						calls( this.onLoad, this, id );
					}
				},
				complete: function( jqXHR, textStatus ) {
					this.setLoading( false );
					this.$wSubmit.html( lSubmit );
					this.enableW( this.$wSubmit );
					//this.enable();
				}
			});
		},
		loadSetListField: function( field, data ) {
			var $field = this.getField( field );
			var $wSetList = $field.closest( '.' + AdminSetListWidget.static.w );
			var widget = $wSetList.getEngineWidget();
			widget.load( data );
		},
		loadField: function( field, data ) {
			var $field = this.getField( field );
			
			if( $field.hasClass( AdminSetListWidget.static.classField )) 
				return this.loadSetListField( field, data );
				
			switch( typeof( data )) {
				case 'string':{
					
					$field.val( data );
					break;
				}
				case 'object':{
					for( var key in data ) {
						var fieldKey = field + '[' + key + ']';
						this.loadField( fieldKey, data[key] );
					}
					break;
				}
			}
		},
		loadModel: function( model ) {
			this.loadDef();
			this.loadedModel = model;
			for( var field in model ) {
				this.loadField( field, model[field]);
			}
		},
		loadDef: function() {
			this.setID( '' );
			this.$w.find( "input[type=text], select" ).val( '' );
			this.clearSetLists();
			if( this.options.defModel ) {
				for( var field in this.options.defModel ) {
					this.loadField( field, this.options.defModel[field]);
				}	
			}
		},
		saveDefModel: function() {
			if( this.options.saveInDefModel ) {
				if( !this.options.defModel )
					this.options.defModel = {};
					
				for( var i=0; i<this.options.saveInDefModel.length; i++ ) {
					var fieldName = this.options.saveInDefModel[i];
					var $field = this.getField( fieldName );
					
					this.options.defModel[ fieldName ] = $field.val();
				}
			}
		},
		clearErrors: function() {
			this.$w.find( '.'+this.options.errorClass ).removeClass( this.options.errorClass );
		},
		clear: function() {
			this.setTitle( this.t( 'New object' ));
			this.loadedModel = false;
			this.loadDef();
			this.clearErrors();
			this.$wDelete.hide();
			this.$wCopy.hide();
		},
		clearSetLists: function() {
			for( var key in this.wSetLists ) {
				var wSetList = this.wSetLists[key];
				wSetList.clear();
			}
		},
		submit: function() {
			if( this.loading ) return false;
			
			var data = this.$form.serialize()
			this.disable();
			this.clearErrors();
			
			var lSubmit = this.$wSubmit.html();
			var holdedTitle = this.$wTitle.html();
			this.$wTitle.html( this.t( 'Saving...' ));
			this.$wSubmit.html( this.t( 'Saving...' ));
			this.loading = true;
						
			function success( data ) {
				calls( this.getID() ? this.onUpdate : this.onInsert, this, data.id );
				
				if( !this.getID())
					this.saveDefModel();
				
				this.$wTitle.html( this.t( 'Saved' ));
				this.$wSubmit.html(  this.t( 'Saved' ));
				
				setTimeout( this.proxy( function() {
					this.$wTitle.html( this.t( 'Editor object' ));
					this.$wSubmit.html( lSubmit );
				}), 1000 );
				
				complete.call( this );
			}
			function revert() {
				this.$wTitle.html( holdedTitle );
				this.$wSubmit.html( lSubmit );
				complete.call( this );
			}
			function error( data ) {
				alert( data.error );
				if( data.errorField ) {
					var $errorField = this.$form.find( '*[name="'+data.errorField+'"]' );
					$errorField.addClass( this.options.errorClass );
					$errorField.showWidgetElementTab();
					this.scrollTo({
						target: $errorField,
						onAfter: function() {
							$errorField.focus();
						}
					});
				}
				revert.call( this );
			}
			function ajaxError() {
				this.ajaxError();
				revert.call( this );
			}
			function complete() {
				this.loading = false;
				this.enable();
			}
			
			this.ajax({
				type: 'POST',
				dataType: 'json',
				url: yiiBaseURL + this.options.ajaxSubmitURL,
				data: data,
				success: function( data ) {
					if( data.error ) 
						error.call( this, data );
					else
						success.call( this, data );
				},
				error: ajaxError
			});
			
			return false;
		},
		delete: function() {
			if( this.loading ) return false;
			if( !confirm( this.t( 'Delete?' ))) return false;
			this.disable();

			var id = this.getID();
			var lDelete = this.$wDelete.html();
			this.$wDelete.html( this.t( 'Deleting...' ));
			this.loading = true;
			
			this.ajax({
				dataType: 'json',
				url: yiiBaseURL + this.options.ajaxDeleteURL,
				data: {id:id},
				success: function( data ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						this.clear();
						calls( this.onDelete, this, id );
					}
				},
				complete: function( jqXHR, textStatus ) {
					this.loading = false;
					this.$wDelete.html( lDelete );
					this.enable();
				}
			});
			
			return false;
		},
		reset: function() {
			if( this.loadedModel ) {
				this.loadModel( this.loadedModel );
			}
			else{
				this.loadDef();
			}
			this.showTooltip( this.$wReset, {
				title: this.t( 'Reseted' ),
				placement: 'top', 
				hideDelay: 1000
			});
		},
		copy: function() {
			var id = this.getID();
			this.setID( '' );
			this.loadedModel[this.options.pkName] = '';
			this.$wCopy.html( this.t( 'Copied' ));
			this.disableW( this.$wCopy, true );
			
			this.slideLeft({ target: this.$wDelete });
			this.$wCopy.addClass( 'removal' );
			setTimeout( this.proxy( function() {
				if( this.$wCopy.hasClass( 'removal' ))
					this.slideLeft({ target: this.$wCopy });
			}), 1000 );
			
			this.setTitle( this.t( 'New object' ));
			calls( this.onCopy, this, id );
		},
		close: function() {
			this.hide({
				onAfter: this.proxy( function() {
					calls( this.onClose, this );
				})
			});
		},
		createSelectsWidgets: function() {
			var $selects = this.$w.find( 'select' );
			$selects.constructorEngineWidget( 'AdminSelectWidget', { 
				ls: this.options.ls,
				ajaxLoadFieldVariantsURL: yiiBaseURL + this.options.ajaxLoadFieldVariantsURL 
			});
		},
		createSetListsWidgets: function() {
			this.wSetLists = {};
			var $wSetLists = this.findInINS( '.' + AdminSetListWidget.static.w );
			for( var i=0; i<$wSetLists.length; i++ ) {
				var $wSetList = $( $wSetLists[ i ]);
				var $field = $wSetList.find( '.' + AdminSetListWidget.static.classField );
				var fieldName = $field.attr( 'name' );
				var fieldName = this.unresolveName( fieldName );

				this.wSetLists[ fieldName ] = $wSetList.constructorEngineWidget( 'AdminSetListWidget' );
			}
		},
		copyAccordionsWidgets: function() {
			var $wAccordions = this.$w.find( '.accordion' );
			for( var i=0; i<$wAccordions.length; i++ ) {
				var $wAccordion = $( $wAccordions[i] );
				var className = $wAccordion.attr( 'class' );
				var $wAccordionSource = this.options.asCopy.$w.find( '[class="'+className+'"]' );
				var widgetSource = $wAccordionSource.getEngineWidget();
				
				var options = merge( {}, widgetSource.options, {
					asCopy: this.wForm
				});
				var wAccordion = new widgetSource.constructor( $wAccordion[0], options );
			}
		},
		addAccordionsHandlers: function() {
			var handlerOpen = this.proxy( this.handlerToggleAccordion, 'open' );
			var handlerClose = this.proxy( this.handlerToggleAccordion, 'close' );
			var $wAccordions = this.$w.find( '.accordion' );
			for( var i=0; i<$wAccordions.length; i++ ) {
				var $wAccordion = $( $wAccordions[i] );
				var widget = $wAccordion.getEngineWidget();
				widget.onOpen.push( handlerOpen );
				widget.onClose.push( handlerClose );
			}
		},
		handlerToggleAccordion: function( event, $coltrol, $body ) {
			var $field = $body.find( '[name^="'+this.options.formName+'"]' );
			var nameField = $field.attr( 'name' );
			nameField = this.unresolveName( nameField );
			this[ event == 'open' ? 'pushCookieVar' : 'popCookieVar' ]( 'openAccordions', nameField )
		},
		setAccordionsState: function() {
			var fieldNames = this.getCookieVar( 'openAccordions' ) || [];
			var $wAccordions = this.$w.find( '.accordion' );
			foreach$( $wAccordions, this, function( i, $wAccordion ) {
				var $field = $wAccordion.find( '[name^="'+this.options.formName+'"]' );
				var nameField = $field.attr( 'name' );
				if( nameField ) {
					nameField = this.unresolveName( nameField );
					var widget = $wAccordion.getEngineWidget();
					widget[ inArray( fieldNames, nameField ) ? 'immediatelyOpen' : 'immediatelyClose' ]();
				}
			});
		}
	});
	
		// AdminListWidgetBase 
	var AdminListWidgetBase = classes.AdminListWidgetBase = function AdminListWidgetBase( element, options ) { // constructor
			// options
		var options = merge({
			ajaxLoadURL: '/admin/{modelName}/ajaxLoadRow',
			ajaxLoadListURL: '/admin/{modelName}/ajaxLoadList'
		}, options );
		objtr( options, 'ajaxLoadURL,ajaxLoadListURL'.split(','), { '{modelName}': lcfirst( options.modelName ) });
			// parent constructor
		AdminListWidgetBase.parent.constructor.call( this, element, options );
		
			// vars
		this.selectedIDs = [];
		this.checkedIDs = [];
		this.highlightColors = '#DFF0D8, #f4fff0'.split( ', ' );
		
			// events
		this.onSelect = [];
		this.onUnselect = [];
		this.onCheck = [];
		this.onUncheck = [];
		
			// init
		AdminListWidgetBase.private.init.call( this );
	};
	AdminListWidgetBase.static = { // static metods
		instance: function( element, instanceOptions, options ) {
			var className = 'Admin{modelName}ListWidget'.replace( '{modelName}', instanceOptions.modelName );
			return instanceWidget( className, AdminListWidgetBase, element, instanceOptions, options );
		}
	};
	AdminListWidgetBase.private = { // private metods
		detElements: function() {
			function createWLoadingRow() {
				var $tr = $( '<tr/>' ).appendTo( this.$wListTable.$wBody );
				var $td = $( '<td/>', {
					'colspan': this.$wListTable.$wHead.$headers.length
				}).appendTo( $tr ).hide();
				return $tr;
			}
			function createWForFormRow() {
				var $tr = $( '<tr/>', {
					'class': 'iFormForm'
				}).appendTo( this.$wListTable.$wBody ).hide();
				var $td = $( '<td/>', {
					'colspan': this.$wListTable.$wHead.$headers.length
				}).appendTo( $tr );
				return $tr;
			}			
			
				// rows
			this.$wLoadingRow = createWLoadingRow.call( this );
			this.$wForFormRow = createWForFormRow.call( this );
		},
		releaseIDs: function() {
			shifts( this.selectedIDs, this, function( id ) {
				calls( this.onUnselect, this, id );
			});
			shifts( this.checkedIDs, this, function( id ) {
				calls( this.onUncheck, this, id );
			});
		},
		addHandlers: function() {
				// check handler
			this.$wListTable.on( 'click', '.wCheckerAll, .wChecker', this.proxy( this.handlerCheckersClick ));
				
				// select handler
			this.$wListTable.$wBody.on( 'click', ' > tr[data-id] > td', this.proxy( this.handlerRowClick ));
		},
		init: function() {
			AdminListWidgetBase.private.detElements.call( this );
			AdminListWidgetBase.private.releaseIDs.call( this );
			AdminListWidgetBase.private.addHandlers.call( this );
			
				// selected
			if( this.state.selected ) {
				foreach( this.state.selected.split( ',' ), this, function( i, id ) {
					this.select( id );
				});
			}
		}
	};
	extend( AdminListWidgetBase, ListWidgetBase, { // metods
		isSelected: function( id ) {
			return inArray( this.selectedIDs, id );
		},
		select: function( id ) {
			if( inArray( this.selectedIDs, id )) 
				return;

			$row = this.get( id );
			if( !$row.length )
				return;
				
			addToArray( this.selectedIDs, id );
			this.get( id ).addClass( 'iSelected' );
			calls( this.onSelect, this, id );
			
			var selected = this.state.selected ? this.state.selected.split( ',' ) : [];
			addToArray( selected, id );
			this.state.selected = selected.join( ',' );
			this.replaceQueryStateIntoHistory( this.state );
		},
		unselect: function( id ) {
			if( !inArray( this.selectedIDs, id )) 
				return;
				
			if( inArray( this.loadingIDs, id )) 
				return;
			
			removeFromArray( this.selectedIDs, id );
			var $row = this.get( id );
			$row.removeClass( 'iSelected' )
			this.highlightW( $row, this.highlightColors, {
				duration: this.highlightDuration
			});
			calls( this.onUnselect, this, id );
			
			var selected = this.state.selected ? this.state.selected.split( ',' ) : [];
			removeFromArray( selected, id );
			this.state.selected = selected.join( ',' );
			this.replaceQueryStateIntoHistory( this.state );
		},
		insert: function( id, after ) {
			if( this.getLoading() )
				return;
				
			var $wLoading = this.$wLoadingRow.clone();
			
			if( after && ($row = this.get( after )) && $row.length ) 
				$row.after( $wLoading );
			else
				this.$wListTable.$wBody.prepend( $wLoading );
			
			$wLoading.show();
			this.$wEmptyRow.hide();
			
			function success( data ) {
				var $row = $( data.row );
				$wLoading.replaceWith( $row );
				this.highlightW( $row, this.highlightColors, {
					duration: this.highlightDuration
				});
			}
			function revert() {
				$wLoading.remove();
				this.isEmpty() && this.$wEmptyRow.show();
			}
			function error( error ) {
				alert( error );
				revert.call( this );
			}
			function ajaxError() {
				this.ajaxError();
				revert.call( this );
			}
				
			var data = {
				id:id,
				cols: this.state.cols
			}
			this.ajax({
				dataType: 'json',
				url: yiiBaseURL + this.options.ajaxLoadURL,
				data: data,
				success: function( data ) {
					data.error ? error.call( this, data.error )
					           : success.call( this, data );
				},
				error: ajaxError
			});
		},
		update: function( id ) {
			if( this.getLoading() )
				return;
				
			var $row = this.get( id );
			if( !$row.length ) 
				return;
			
			this.setLoadingRow( id, true );
			
			var selected = this.isSelected( id );
			var checked = this.isChecked( id );
			
			var data = {
				id:id,
				cols: this.state.cols
			};
			this.ajax({
				dataType: 'json',
				url: yiiBaseURL + this.options.ajaxLoadURL,
				data: data,
				success: function( data, textStatus, jqXHR ) {
					if( data.error ) {
						alert( data.error );
					}
					else{
						this.setLoadingRow( id, false );
						var $newRow = $( data.row );
						this.removeForm( id );
						selected && this.unselect( id );
						
						$row.replaceWith( $newRow );
						this.highlightW( $newRow, this.highlightColors, {
							duration: this.highlightDuration
						});
													
						if( checked )
							this.getChecker( id ).prop( 'checked', true );
					}
				},
				complete: function( jqXHR, textStatus ) {
					this.setLoadingRow( id, false );
				}
			});
		},
		remove: function( id ) {
			if( this.getLoading() )
				return;
				
			if( typeof id == 'object' ) {
				foreach( id, this, function( i, id ) {
					this.remove( id );
				});
				return;
			}
			var $row = this.get( id );
			this.removeForm( id );
			
			var selected = this.isSelected( id );
			var checked = this.isChecked( id );
			
			selected && this.unselect( id );
			checked && this.uncheck( id );
			
			$row.remove();
			this.isEmpty() && this.$wEmptyRow.show();
		},
		hasForm: function( id ) {
			var $row = this.get( id );
			var $next = $row.next();
			return $next.has( 'form' ).length;
		},
		getForm: function( id ) {
			var $row = this.get( id );
			var $wForm = $row.next().find( ' > td:first > div' );
			return $wForm;
		},
		appendForm: function( id, $wForm ) {
			if( this.hasForm( id ))
				return;
			
			var $row = this.get( id );
			
			if( !$row.length )
				return;
			
			var $wForFormTR = this.$wForFormRow.clone();
			var $wForFormTD = $wForFormTR.find( 'td:first' );
			
			$wForFormTD.append( $wForm );
			$row.after( $wForFormTR );
			$wForFormTR.show();
		},
		removeForm: function( id ) {
			if( !this.hasForm( id ))
				return;
			
			var $row = this.get( id );
			$row.next().remove();
		},
		isLoading: function( id ) {
			return inArray( this.loadingIDs, id );
		},
		getChecker: function( id ) {
			var $row = this.get( id );
			return $row.find( '.wChecker' );
		},
		isChecked: function( id ) {
			return inArray( this.checkedIDs, id );
		},
		check: function( id ) {
			if( inArray( this.checkedIDs, id )) 
				return;
			
			var $wChecker = this.getChecker( id );
			
			if( this.isLoading( id )) {
				$wChecker.prop( 'checked', false );
				return;
			}

			addToArray( this.checkedIDs, id );
			$wChecker.prop( 'checked', true );
			calls( this.onCheck, this, id );
		},
		uncheck: function( id ) {
			if( !inArray( this.checkedIDs, id )) 
				return;
			
			removeFromArray( this.checkedIDs, id );
			this.getChecker( id ).prop( 'checked', false );
			calls( this.onUncheck, this, id );
		},
		replaceWith: function( $w ) {
			AdminListWidgetBase.parent.replaceWith.call( this, $w );
			AdminListWidgetBase.private.init.call( this );
		},
		handlerRowClick: function( e ) {
			var $td = $( e.target ).closest( 'td' );
			if( $td.has( '.wChecker' ).length )
				return;
			
			var $row = $td.closest( 'tr' );
			var id = $row.attr( 'data-id' );
			
			if( id )
				this[ this.isSelected( id ) ? 'unselect' : 'select' ]( id );
		},
		handlerCheckersClick: function( e ) {
			var $wChecker = $( e.target );
			var checked = $wChecker.is( ':checked' );
			
			if( $wChecker.is( '.wCheckerAll' )) {
				foreach( this.getIDs(), this, function( i, id ) {
					this[ checked ? 'check' : 'uncheck' ]( id );
				});
			}
			else{
				var $row = $wChecker.closest( '[data-id]' );
				var id = $row.attr( 'data-id' );
				this[ checked ? 'check' : 'uncheck' ]( id );
			}
		},
		handlerExtraActionsButtons: function( e ) {
			var $b = $( e.target );
			var $pane = $b.closest( '.wExtraActionsPane' );
			var id = $b.closest( 'tr[data-id]' ).attr( 'data-id' );
			
			if( $b.is( '.wEdit' )) {
				this.select( id );
				$pane.slideUp( 'fast' );
				return false;
			}
			
			return AdminListWidgetBase.parent.handlerExtraActionsButtons.call( this, e );
		},
	});
}( window.jQuery );