#!/bin/sh

documentRoot="$(realpath ${0%/*})/../";

cachethumbDir="$documentRoot/cachethumb/";

find $cachethumbDir -name '*.jpg' -exec jpegoptim --max=75 --strip-all '{}' \;
