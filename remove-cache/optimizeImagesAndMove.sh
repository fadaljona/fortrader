#!/bin/sh

documentRoot="$(realpath ${0%/*})/../";

cachethumbDir="$documentRoot/cachethumb/";

notOptimizedDir="$cachethumbDir/notOptimized/";

notOptimizedAllFiles="$notOptimizedDir/*";

notOptimizedJpgFiles="$notOptimizedDir/*.jpg";

find $notOptimizedDir -name '*.jpg' -exec jpegoptim --max=75 --strip-all '{}' \;

mv -f $notOptimizedAllFiles $cachethumbDir
mv -f $notOptimizedJpgFiles $cachethumbDir
