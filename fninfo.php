<?php
include './wp-load.php';

$max_count = 20;
$mem_time = 3600;

$mem_key = md5('fninfo-news-update');
$categories = array();
$colors = array('ffffff', '000000', '000000','000000');
$direction = 0;
$scale = '%';
$size = '100';
$encodings = array('cp1251', 'koi8-r');
$header = '';
$count = 5;
$with_photo = 1;
//end of settings

foreach ($_GET as $key => $value) {
    $tmp_key = strtolower($key);
    if ($tmp_key != $key)
        $_GET[$tmp_key] = $value;
}

if (isset($_GET['scale']) && $_GET['scale'] != $scale)
    $scale = 'px';

$newLayout = false;
if (isset($_GET['newLayout']) && $_GET['newLayout'] )
    $newLayout = true;

if (isset($_GET['direction']))
    $direction = 1;

if (isset($_GET['with_photo']))
    $with_photo = 0;

if (isset($_GET['size']) && intval($_GET['size']) > 0)
    $size = intval($_GET['size']);

if (isset($_GET['encoding']) && isset($encodings[(int) $_GET['encoding']]))
    $encoding = (int) $_GET['encoding'];

if (isset($_GET['categories'])) {
	
    $tmp_cats = explode(',', $_GET['categories']);
    if (!empty($tmp_cats)){
        $categories = array_filter($tmp_cats, 'tmp_filter_input_array_int');
	}
}

if (isset($_GET['header'])) {
    $tmp_header = trim(strip_tags(urldecode($_GET['header'])));
    if ($tmp_header)
        $header = $tmp_header;
}

if (isset($_GET['count'])) {
    $tmp_count = intval($_GET['count']);

    if ($tmp_count > 1 && $tmp_count <= $max_count)
        $count = $tmp_count;
}

$tmp_colors = array();
if (isset($_GET['colors'])) {
    $tmp_colors = explode(',', $_GET['colors']);
    if (!empty($tmp_colors))
        $tmp_colors = array_filter($tmp_colors, 'tmp_filter_input_array_hex');
}

for ($i = 0; $i < 3; $i++) {
    if (isset($tmp_colors[$i])) {
        $colors[$i] = $tmp_colors[$i];
    }

    if ($colors[$i] != 'transparent') {
        $colors[$i] = '#' . $colors[$i];
    }
    if ($i == 2 && $colors[$i] == 'transparent') {
        $colors[$i] == '#000';
    }
}

$params = array(
    'post_status' => 'publish',
    'posts_per_page' => $count,
);

$cat_str = null;
if (is_array($categories) && !empty($categories)) {
    $params['category'] = implode(',', $categories);
    $mem_key .= $params['category'];
}

//$posts = $memcache->get($mem_key);
if (!is_array($posts) || empty($posts)){
	//print_r($params);
	//$posts = new WP_Query();
	//$posts->query($params);
    //$posts = query_posts($params);
	$posts = get_posts($params);
}

ob_start();
if ($posts && !empty($posts)) {
    $colspan = (!$direction) ? 'colspan="' . count($posts) . '"' : '';
	
	if( $newLayout ){
		echo '<div class="sidebar_section news">';
		foreach ($posts as $post){
			setup_postdata($post);
			
			echo '<figure class="news_item">
                    <img src="'.p75GetThumbnail(get_the_ID(), 89, 68, "").'" alt="'.get_the_title().'">
                    <figcaption>
                        <a href="'.get_the_permalink().'" class="news_title">'.get_the_title().'</a>
                    </figcaption>
                </figure>';
		}
		echo '</div>';
	}else{
    ?>
    <div id="fortrader_newsinformer">
        <style>
        #fortrader_newsinformer table {border-collapse: collapse; border-style: solid; border-width: 1px; margin: 0; padding: 0;}
        #fortrader_newsinformer td { font: 10px verdana; margin: 0; padding: 4px; text-align: center; text-decoration: underline;vertical-align: top;}
        #fortrader_newsinformer a:hover, #fortrader_newsinformer a:active, #fortrader_newsinformer a:visited {color: <?php echo $colors[2] ?>}
        #fortrader_newsinformer th a:hover, #fortrader_newsinformer th a:active, #fortrader_newsinformer th a:visited {color: <?php echo $colors[0] ?>; font: 13px sans-serif; letter-spacing: 0; text-decoration: underline;}
        </style>
        <table style="width: <?php echo $size . $scale ?>; border-color: <?php echo $colors[1] ?>;">
            <tbody>
                <tr>
                    <th style="background-color: <?php echo $colors[1] ?>;" <?php echo $colspan ?>>
                       <?php if ($header) {?> <a style="color: <?php echo $colors[3] ?>;" href="<?php echo get_option('home') ?>" target="_blank"><?php echo $header ?></a><?php } ?>
                    </th>
                </tr>
                <?php foreach ($posts as $post): setup_postdata($post); ?>
                    <?php if ($direction): ?><tr><?php endif; ?>
                        <td style="background-color: <?php echo $colors[0] ?>;">
                            <a href="<?php the_permalink() ?>" style="color: <?php echo $colors[2] ?>;" target="_blank">
                            <img src="<?php echo p75GetThumbnail(get_the_ID(), 89, 68, ""); ?>" alt="<? the_title(); ?>"  title="<? the_title(); ?>" />
                            <br>
                            <span><?php the_title() ?></span>
                            </a>
                        </td>
                        <?php if ($direction): ?></tr><?php endif; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php
	}
}
/*if (isset($encoding)) {
    $contents = iconv('UTF-8', $encodings[$encoding], ob_get_contents());
} else {
*/    $contents = ob_get_contents();
//}

ob_end_clean();

if (isset($_GET['v']) && $_GET['v'] == 15)
    echo 'document.getElementById("block_container").innerHTML=' . json_encode($contents) . '';
else
    echo 'document.write(' . @json_encode($contents) . ')';

function tmp_filter_input_array_int($k) {
    if (!is_numeric($k))
        return;

    $k = (int) $k;
    if (!$k)
        return;

    return true;
}

function tmp_filter_input_array_hex($k) {
    if ($k == 'transparent')
        return true;

    if (!preg_match('@([0-9a-f]{2}){3}@ui', $k))
        return;

    return true;
}
?>