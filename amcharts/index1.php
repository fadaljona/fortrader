<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>

    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css"/>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <title>Amcharts</title>
</head>

<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container"><a href="/" class="brand">Amcharts</a>
            <ul id="yw1" class="nav">
                <li class="active"><a href="/">Home</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container" id="page">
    <!-- breadcrumbs -->
    <div class="hero-unit"><h1>Welcome to Amcharts</h1>
    </div>
    <form class="form-vertical" id="yw0" action="/" method="get">
        <input type="text" id="source" name="source"
               value="proxy.php?url=http://fortrader.org/services/calendarEvent/apiExportList?idsIndicators=6&amp;languages=ru&amp;sort=dt&amp;fields=dt,fact_value&amp;unstructI18N=1"
               placeholder="Источник данных" class="span12">
        <p>Рефреш каждые <input type="text" name="second" value="10" class="input-mini" id="refresh" > секунд</p>
        <div id="chartdiv" style="width: 100%; height: 400px;"></div>
        <p>Настройки:</p>
        <div class="row">
            <div class="span6">
                <textarea class="field pull-left span6" id="settings" rows="23"
                          placeholder="Enter settings code here"></textarea>
            </div>
            <div class="span6">
                <div id="parse-settings">
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button id="apply" class="btn btn-primary">Применить настройки</button>
            <button id="export" class="btn btn-secondary">Получить код</button>
        </div>
        <p>Код для вставки на сайт:</p>
        <textarea class="field span12" id="output" rows="6" autocomplete="off" placeholder=''></textarea>
    </form>
    <script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
    <script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
    <script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/none.js"></script>
    <script src="http://cdn.amcharts.com/lib/3/exporting/amexport.js" type="text/javascript"></script>
    <script src="http://cdn.amcharts.com/lib/3/exporting/canvg.js" type="text/javascript"></script>
    <script src="http://cdn.amcharts.com/lib/3/exporting/rgbcolor.js" type="text/javascript"></script>
    <script src="http://cdn.amcharts.com/lib/3/exporting/filesaver.js" type="text/javascript"></script>
    <script src="http://cdn.amcharts.com/lib/3/exporting/jspdf.js" type="text/javascript"></script>
    <script src="http://cdn.amcharts.com/lib/3/exporting/jspdf.plugin.addimage.js" type="text/javascript"></script>
    <script type="text/javascript" src="/themes/bootstrap/js/amcharts.min.js"></script>
    <div class="clear"></div>

    <div id="footer">
        Copyright &copy; 2014 by My Company.<br/>
        All Rights Reserved.<br/>
    </div><!-- footer -->

</div><!-- page -->

<script type="text/javascript">
    /*<![CDATA[*/
    jQuery(function ($) {
        jQuery('body').tooltip({'selector': 'a[rel=tooltip]'});
        jQuery('body').popover({'selector': 'a[rel=popover]'});
    });
    /*]]>*/
</script>
</body>
</html>