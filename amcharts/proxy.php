<?php
/**
 * @author Andrei Ivanouski aivanouski@gmail.com
 * Date: 21.07.14
 * Time: 18:19
 * @version 1.0
 */
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, trim($_GET['url']));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0");
$result = curl_exec($ch);
curl_close($ch);
echo $result;