var chart;
var chartData = [];
var disableOverride = {
    'dataProvider': '',
    'descriptionField': 'description',
    'valueField': 'fact_value',
    'categoryField': 'dt',
    'id': '',
    'categoryAxis': ''
};
var chartCursor;
var applySettings = {};
var defaultSettingsCopy = {
    dataProvider: chartData,
    pathToImages: "http://www.amcharts.com/lib/images/",
    categoryField: "dt",
    dataDateFormat: "YYYY-MM-DD HH:NN:SS",
    chartCursor: {},
    chartScrollbar: {},
    type: "serial",
    categoryAxis: {
        gridPosition: "start",
        parseDates: true, // as our data is date-based, we set parseDates to true
        dashLength: 1,
        gridAlpha: 0.15,
        minorGridEnabled: true,
        axisColor: "#DADADA"
    },
    graphs: [
        {
            type: "line",
            title: "%",
            descriptionField: "description",
            valueField: "fact_value",
            lineThickness: 2,
            bullet: "round",
            balloonText: "[[category]]: <b>[[description]]</b>"
        }
    ]
};
var defaultSettings = {
    dataProvider: chartData,
    pathToImages: "http://www.amcharts.com/lib/images/",
    categoryField: "dt",
    dataDateFormat: "YYYY-MM-DD HH:NN:SS",
    chartCursor: {},
    chartScrollbar: {},
    type: "serial",
    categoryAxis: {
        gridPosition: "start",
        parseDates: true, // as our data is date-based, we set parseDates to true
        dashLength: 1,
        gridAlpha: 0.15,
        minorGridEnabled: true,
        axisColor: "#DADADA"
    },
    graphs: [
        {
            type: "line",
            title: "%",
            descriptionField: "description",
            valueField: "fact_value",
            lineThickness: 2,
            bullet: "round",
            balloonText: "[[category]]: <b>[[description]]</b>"
        }
    ]
};
var lastTimeCall = 0;
function validateApplySettings() {
    var settingsArea = $('#settings');
    settingsArea.removeClass('error');
    applySettings = {};
    var html = '';

    var value = $('#settings').val();
    try {
        var settings = JSON.parse(value);
        $.each(settings, function (propertyName, value) {
            if (disableOverride[propertyName] == undefined) {
                if (value instanceof Array) {
                    html += '<span class="text-success">' + propertyName + "</span>" + '<ul>';
                    applySettings[propertyName] = [];
                    $.each(value, function (key, obj) {
                        html += '<span class="text-success">' + key + "</span>" + '<ul>';
                        if (obj instanceof Object) {
                            $.each(obj, function (propertyPropertyName, valueValue) {
                                if (disableOverride[propertyPropertyName] == undefined) {
                                    if (applySettings[propertyName][key] === undefined) {
                                        applySettings[propertyName][key] = {};
                                    }
                                    applySettings[propertyName][key][propertyPropertyName] = valueValue;
                                    html += '<li class="text-success">' + propertyPropertyName + '</li>';
                                } else {
                                    if (applySettings[propertyName][key] === undefined) {
                                        applySettings[propertyName][key] = {};
                                    }
                                    applySettings[propertyName][key][propertyPropertyName] = disableOverride[propertyPropertyName];
                                    html += '<li class="text-error">' + propertyPropertyName + '</li>';
                                }
                            });
                        }
                        html += '</ul>';
                    });
                    html += '</ul>';
                } else {
                    if (value instanceof Object) {
                        html += '<span class="text-success">' + propertyName + "</span>" + '<ul>';
                        applySettings[propertyName] = {};
                        $.each(value, function (propertyPropertyName, valueValue) {
                            if (disableOverride[propertyPropertyName] == undefined) {
                                applySettings[propertyName][propertyPropertyName] = valueValue;
                                html += '<li class="text-success">' + propertyPropertyName + '</li>';
                            } else {
                                html += '<li class="text-error">' + propertyPropertyName + '</li>';
                            }
                        });
                        html += '</ul>';
                    } else {
                        html += '<p class="text-success">' + propertyName + '</p>';
                        applySettings[propertyName] = value;
                    }
                }
            } else {
                html += '<span class="text-error">' + propertyName + "</span>";
            }
        });
    } catch (e) {
        console.log(e.stack);
        console.log(e.message);
        settingsArea.addClass('error');
    }
    return html;
}
$(function () {
    var settingsArea = $('#settings');
    $('#apply').on('click', function () {
        try {
            var html = validateApplySettings();
            $('#parse-settings').html(html);
            makeChart(defaultSettings, false);

        } catch (e) {
            console.log(e.stack);
            console.log(e.message);
            settingsArea.addClass('error');
        }

        return false;
    });

    $('#export').on('click', function () {
        var output = '';
        validateApplySettings();
        output = '<script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.min.js"></script><script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script><script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/none.js"></script><script src="http://cdn.amcharts.com/lib/3/exporting/amexport.js" type="text/javascript"></script><script src="http://cdn.amcharts.com/lib/3/exporting/canvg.js" type="text/javascript"></script><script src="http://cdn.amcharts.com/lib/3/exporting/rgbcolor.js" type="text/javascript"></script><script src="http://cdn.amcharts.com/lib/3/exporting/filesaver.js" type="text/javascript"></script><script src="http://cdn.amcharts.com/lib/3/exporting/jspdf.js" type="text/javascript"></script><script src="http://cdn.amcharts.com/lib/3/exporting/jspdf.plugin.addimage.js" type="text/javascript"></script>';
        output += '\r\n' + '<div id="chartdiv" style="width: 100%; height: 400px;"></div>' + '\r\n';
        output += '<script type="text/javascript">var chart; var chartData = []; var settings = ' + JSON.stringify(makeChart(defaultSettingsCopy, true)) + '; settings.dataProvider = chartData;';
        output += 'AmCharts.ready(function () {chart = AmCharts.makeChart("chartdiv", settings);});';
        output += 'var url = "' + $("#source").val() + '";';
        output += '$(function () { $.ajax({url: url,cache: false,dataType: "json", success: function(data){ ' +
            'chartData.length = 0;' +
            'data.events.forEach(function (single) {' +
            'var obj = {};' +
            'if (single.i18ns.en_us.fact_value) {' +
            '    obj.description = single.i18ns.en_us.fact_value;' +
            '    obj.fact_value = parseFloat(single.i18ns.en_us.fact_value);' +
            '    obj.percents = obj.fact_value + "%";' +
            '   obj.dt = single.dt;' +
            '    chartData.push(obj);' +
            '}' +
            '});' +
            'chart.validateData();' +
            'if (data.countPages > 1) {' +
            '    var page = 1;' +
            '    while (data.countPages > page) {' +
            '        $.ajax({' +
            '            url: url + "%page=" + page,' +
            '            cache: false,' +
            '            dataType: "json",' +
            '            success: function(data){' +
            '                chartData.length = 0;' +
            '                data.events.forEach(function (single) {' +
            '                    var obj = {};' +
            '                    if (single.i18ns.en_us.fact_value) {' +
            '                        obj.description = single.i18ns.en_us.fact_value;' +
            '                        obj.fact_value = parseFloat(single.i18ns.en_us.fact_value);' +
            '                        obj.percents = obj.fact_value + "%";' +
            '                        obj.dt = single.dt;' +
            '                        chartData.push(obj);' +
            '                    }' +
            '                });' +
            '                chart.validateData();' +
            '           }' +
            '        });' +
            '        page++;' +
            '    }' +
            '}' +
            '}' +
            '});' +
            '});</script>';
        $('#output').val(output);
        return false;
    });

    setInterval(function () {
        var seconds = parseInt($('#refresh').val());
        var url = $('#source').val();
        if (url && seconds > 0 && chart != 'undefined' && (lastTimeCall + seconds) < (Math.round((new Date()).getTime() / 1000))) {
            lastTimeCall = Math.round((new Date()).getTime() / 1000);

            $.ajax({
                url: url,
                cache: false,
                dataType: "json",
                success: function (data) {
                    chartData.length = 0;
                    data.events.forEach(function (single) {
                        var obj = {};
                        if (single.i18ns.en_us.fact_value) {
                            obj.description = single.i18ns.en_us.fact_value;
                            obj.fact_value = parseFloat(single.i18ns.en_us.fact_value);
                            obj.percents = obj.fact_value + '%';
                            obj.dt = single.dt;
                            chartData.push(obj);
                        }
                    });
                    chart.validateData();
                    lastTimeCall = Math.round((new Date()).getTime() / 1000);
                    if (data.countPages > 1) {
                        var page = 1;
                        while (data.countPages > page) {
                            $.ajax({
                                url: url + '%page=' + page,
                                cache: false,
                                dataType: "json",
                                success: function (data) {
                                    chartData.length = 0;
                                    data.events.forEach(function (single) {
                                        var obj = {};
                                        if (single.i18ns.en_us.fact_value) {
                                            obj.description = single.i18ns.en_us.fact_value;
                                            obj.fact_value = parseFloat(single.i18ns.en_us.fact_value);
                                            obj.percents = obj.fact_value + '%';
                                            obj.dt = single.dt;
                                            chartData.push(obj);
                                        }
                                    });
                                    chart.validateData();
                                    lastTimeCall = Math.round((new Date()).getTime() / 1000);
                                }
                            });
                            page++;
                        }
                    }
                }
            });
        }
    }, 200);
});

function makeChart(defaultSettings, output) {
    var chartSettings = {};
    if (output) {
        for (var propertyName in defaultSettingsCopy) {
            chartSettings[propertyName] = defaultSettingsCopy[propertyName];
        }
        for (var propertyName in applySettings) {
            chartSettings[propertyName] = applySettings[propertyName];
        }
        delete chartSettings.dataProvider;
        return chartSettings;
    }

    if (chart) {
        chart.clear();
    }
    for (var propertyName in defaultSettings) {
        chartSettings[propertyName] = defaultSettings[propertyName];
    }
    for (var propertyName in applySettings) {
        chartSettings[propertyName] = applySettings[propertyName];
    }
    chart = AmCharts.makeChart("chartdiv", chartSettings);
}

AmCharts.ready(function () {
    makeChart(defaultSettings, false);
});