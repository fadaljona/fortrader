<?php
include_once("DB.class.php");

$countnews = 5;      // Сколько новостей выводить
$categories = array(1, 641);      // Список категорий

$home_url = "http://".$_SERVER['SERVER_NAME']."/";    // URL главной страницы
$db_host = "192.168.1.4";     // Сервер SQL
$db_user = "mrdealer";     // Имя пользователя
$db_password = "d85ngyh";     // Пароль
$db_name = "mrdealer";     // База данных

$DB = new DB;
$DB->db_host = $db_host;
$DB->db_user = $db_user;
$DB->db_password = $db_password;
$DB->db_name = $db_name;
$DB->go();

$sql = "
    SELECT
          post_title as title, ID as nid, post_content as body,
          post_date_gmt as created, post_name,
          wp_terms.slug as cat_url,wp_postmeta.meta_value as imgurl,
          UNIX_TIMESTAMP(post_date_gmt) as unix
    FROM wp_posts
    INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id)
    INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id)
    INNER JOIN wp_terms ON (wp_term_taxonomy.term_id = wp_terms.term_id)
    INNER JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id)
    WHERE
        post_status = 'publish' AND post_type = 'post'
        AND wp_term_taxonomy.taxonomy = 'category'
        AND wp_postmeta.meta_key = '_p75_thumbnail'
        AND wp_term_taxonomy.term_id IN (" . implode(',', $categories) . ")
    ORDER by post_date_gmt desc
 LIMIT " . $countnews;
$DB->sql = $sql;
$DB->exec();
$mass = $DB->_query_fetch_assoc();
if ($DB->_num_rows()) {
?>
    <html>
        <head>
            <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
            <script type="text/javascript" src="http://img.yandex.net/webwidgets/1/WidgetApi.js"></script>
            <script type="text/javascript">
                widget.onload=function(){
                    widget.adjustIFrameHeight();
                }
            </script>
            <style type="text/css" media="screen">
                body {margin:0; background: transparent; font-family: Arial; font-size: 13px}
                table a, table {font-family: Verdana; font-size: 11px; }
            </style>
        </head>
        <body>
            <table>
                <tbody>
                <?php
                foreach ($mass as $mas):
                    $chen_link = $home_url . $mas['cat_url'] . "/" . $mas['post_name'] . ".html";
                    $date = date("d.m.y", $mas['unix'] + 3600 * 2);
                ?>
                    <tr>
                        <td>
                            <a href="<?php echo $chen_link; ?>" target="_blank">
                                <img style="border: 0" src="<?php echo $home_url ?>wp-content/plugins/simple-post-thumbnails/timthumb.php?src=/wp-content/thumbnails/<?php echo $mas['imgurl'] ?>&w=50&h=38&zc=1&ft=jpg" />
                            </a>
                        </td>
                        <td>
                            <a href="<?php echo $chen_link; ?>" target="_blank">
                            <?php echo $mas['title'] ?>
                        </a>&nbsp;<?php echo $date ?>
                    </td>
                </tr>
                <?php
                            endforeach;
                ?>
                        </tbody>
                    </table>

                </body>
            </html>
<?php
                        }
?>
