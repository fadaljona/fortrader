<?php
include $_SERVER['DOCUMENT_ROOT'].'/wp-load.php';
date_default_timezone_set( 'UTC' );
header("content-type: application/rss+xml; charset=utf-8");

// Общие настройки
$title = "Ежедневные экономические новости от ForTrader.org";
$description = "Ежедневные экономические новости";  // Описание

$home_url = get_site_url();
$rss_link = $home_url;   // Ссылка на ленту новостей
$self_rss_link = $home_url . '/rss/last.rss';   // Ссылка на ленту новостей
$logo = $home_url. "/images/ftlogo-100.png";       // Логотип
$logoSquare = $home_url. "/images/ftlogo-180.png";

$author = "ForTrader.org";      // Автор

$lastPostDate = get_lastpostdate();


global $fortraderCache;
$cache_key = 'theme_last.rss' . $lastPostDate;

if( $fortraderCache->beginCache( $cache_key, 60*60*24*5) ) {
	


echo ("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">
	<channel>
		<title>{$title}</title>
		<atom:link href=\"{$self_rss_link}\" rel=\"self\" type=\"application/rss+xml\" />
		<link>{$rss_link}</link>
		<description>{$description}</description>
		<image>
			<url>{$logo}</url>
			<link>{$rss_link}</link>
			<title>{$title}</title>
		</image>
		<lastBuildDate>".date("r", strtotime( $lastPostDate ) - 3600*4 )."</lastBuildDate>
");


$r = new WP_Query( array(
	'post_status'  => 'publish',
	'post_type' => 'post',
	'orderby' => 'modified',
	'order'   => 'DESC',
	'posts_per_page' => -1,
	'category__not_in' => array( get_option('questions_cat_id'), get_option('services_cat_id') ) ,
	'date_query' => array(
		array(
			'after' => date('Y-m-d H:i:s', time()-60*60*24*90),
		),
	),
) );

if ($r->have_posts()){
	while ( $r->have_posts() ){ $r->the_post();

        $chen_title = $post->post_title;
        $chen_link = get_the_permalink();
		
		$text = apply_filters('the_content', $post->post_content );
		$text = html_entity_decode(strip_tags( $text ));
		
		$text = wp_trim_words($text, 100, '...');
		$text = str_replace("\n"," ",$text);
		$text = str_replace("\r"," ",$text);

        $date = date("r", strtotime( $post->post_date ) - 3600*4 );

        echo ("
		<item>
			<title>" . $chen_title . "</title>
			<link>" . $chen_link . "</link>
			<pubDate>$date</pubDate>
			<description>" . $text . "</description>
			<guid>" . $chen_link . "</guid>
		</item>
		");
	}
}

echo ("
	</channel>
</rss> ");

$fortraderCache->endCache(); }

?>