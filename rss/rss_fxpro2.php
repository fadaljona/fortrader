<?

include $_SERVER['DOCUMENT_ROOT'].'/wp-config.php';
include_once("DB.class.php");
header("content-type: application/rss+xml");

// Общие настройки
$title = "Ежедневная аналитика от ForTrader.org";    // Заголовок
$rss_link = "http://fortrader.org/rss/rss_fxpro.php";   // Ссылка на ленту новостей
$description = "Ежедневная аналитика";  // Описание
$logo = "http://fortrader.org/images/logo_ya.gif";       // Логотип
$home_url = "http://fortrader.org/";    // URL главной страницы
$countnews = 30;      // Сколько новостей выводить
$author = "ForTrader.org";      // Автор

$categories = array(0);      // Список категорий

function do_excerpt($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));
    if (count($words) > $word_limit)
        array_pop($words);
    return implode(' ', $words) . ' ...';
}
function handleAttachment($matches){
	
	$url=wp_get_attachment_url(trim($matches[1]));
	$s='<img src="'.$url.'" width="'.$matches[2].'" alt="'.$matches[3].'" />';
	return $s;
}

$DB = new DB;

// Настройки базы
$DB->db_host = DB_HOST;
$DB->db_user = DB_USER;
$DB->db_password = DB_PASSWORD;
$DB->db_name = DB_NAME;
$DB->go();

echo ("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<rss version=\"2.0\">
  <channel>
    <title>" . $title . "</title>
      <link>" . $rss_link . "</link>
        <description>" . $description . "</description>
	  <image>
		<url>" . $logo . "</url>
		<title>" . $title . "</title>
		<link>" . $rss_link . "</link>
	  </image>
");

$sql = "
    SELECT
          post_title as title, ID as nid, post_content as body,
          post_date_gmt as created, post_name,
          wp_terms.slug as cat_url,
          UNIX_TIMESTAMP(post_date_gmt) as unix
    FROM wp_posts
    INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id)
    INNER JOIN wp_term_taxonomy ON (wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id)
    INNER JOIN wp_terms ON (wp_term_taxonomy.term_id = wp_terms.term_id)
    WHERE
        post_status = 'publish' AND post_type = 'post'
        AND wp_term_taxonomy.taxonomy = 'category'
	    AND wp_term_taxonomy.term_id IN (" . implode(',', $categories) . ")
    ORDER by post_date_gmt desc
 LIMIT " . $countnews;


$DB->sql = $sql;
$DB->exec();
$mass = $DB->_query_fetch_assoc();

if ($DB->_num_rows()) {
    foreach ($mass as $mas) {

        $chen_title = $mas['title'];
        $chen_link = $home_url . $mas['cat_url'] . "/" . $mas['post_name'] . ".html";
//  $text = substr($mas['firma_text'], 0, 256)."...";
       // $text = array_shift(explode('. ', htmlspecialchars(strip_tags(($mas['body'])), 1))) . '.';
		$text = htmlspecialchars(strip_tags(($mas['body'])), 1);

		//$text=preg_replace('#\[caption id="[^_]*_([^\"]*)"[^\]]*width="([^\"]*)"[^\]]*\]\s?([^\[]*)\[/caption\]#sie',"handleAttachment('$1','$2','$3')",$text);
		
		$text=preg_replace_callback('#\[caption id="[^_]*_([^\"]*)"[^\]]*width="([^\"]*)"[^\]]*\]\s?([^\[]*)\[/caption\]#si',"handleAttachment",$text);
		
        $vowels = array("&amp;", "nbsp;", "Источник", "\r", "\n");
        $date = date("r", $mas['unix'] + 3600 * 2);

        echo ("
		<item>
			<title>" . $chen_title . "</title>
			<link>" . $chen_link . "</link>

			<pubDate>$date</pubDate>
			<description>" . $text . "</description>
			<author>Юлия Апель</author>
			<guid>" . $chen_link . "</guid>
		</item>
");
    }
}

echo ("
	</channel>
</rss> ");
?>