<?


class DB {
/*
function DB(){
  $this->connect();
  $this->select_db();
  $this->set_charset();
}
*/
var $db_host;	// Сервер SQL
var $db_user;	// Имя пользователя
var $db_password;	// Парль
var $db_name;	// База данных
var $charset = 'utf8';  // Кодировка
var $connector;
var $sql; // SQl запрос
var $result; // результат после exec

  // Конектимся к БД
function connect(){
  $connector = mysqli_connect($this->db_host,$this->db_user,$this->db_password, $this->db_name) or die("Невозможно подключится к базе");
  $this->connector = $connector;
}



  // Выбираем кодировку
function set_charset(){
  $this->sql = 'SET NAMES "' . $this->charset . '"';
  $this->exec();
  $this->sql = 'SET CHARACTER_SET "' . $this->charset . '"';
  $this->exec();
}

  // Выполняем подключение, выбираем базу и выбираем кодировку.
function go(){
  $this->connect();
  $this->set_charset();
}

  // Делаем запрос
function exec(){
  $this->query_count++;
  $this->result = @mysqli_query($this->connector, $this->sql);
}

  // Выполняем запрос сразу
function execsql($sql){
  $this->sql = $sql;
  $this->exec();
}

  // Обезопасиваем переменные
function escape_string($str){
  if (function_exists('mysql_real_escape_string') AND is_resource($this->connector)){
    return mysql_real_escape_string($str, $this->connector);
  }elseif (function_exists('mysql_escape_string')){
    return mysql_escape_string($str);
  }else{
    return addslashes($str);
  }
}

function row(){
  return $posts = mysql_fetch_row($this->result);
}

// Добавления LIMIT
function limit($limit_first, $limit_second){
  $sql = '';
  if(is_numeric($limit_first)){
    $sql .= " LIMIT ".$limit_first;
    if(is_numeric($limit_second)){
      $sql .= ", ".$limit_second;
    }
  }
  return $sql;
}

function create_where($array){
// Добавка AND, но не во всех случаях
  $where = '';
  if(is_array($array)){
    foreach($array as $key => $value){
      $where .= "`".$key."` = '".$value."' AND ";
    }
  }
  if($where != ''){
    $where = substr($where, 0, strlen($where)-5);
  }
  return $where;
}

function where($where){
  return "WHERE ".$this->create_where($where);
}

function and_where($where){
  return "AND ".$this->create_where($where);
}

function num_ref($sql){
  $this->sql = $sql;
  $this->exec();
  return $this->_num_rows();
}

// Кол-во записей
function _num_rows(){
  return $num_rows = @mysqli_num_rows($this->result);
}

// Возвращаем масив с данными
function _query_fetch_assoc(){
  if(!$this->result){return false;} //echo "Ошибка в sql запросе";
  if(!$this->_num_rows()){return false;}  // Пустой результат
    while ($row = mysqli_fetch_assoc($this->result)){
      $field[] = array_filter($row, "stripslashes");
    }
  return $field;
}
}
?>