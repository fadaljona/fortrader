<?php
include $_SERVER['DOCUMENT_ROOT'].'/wp-load.php';
date_default_timezone_set( 'UTC' );
header("content-type: application/rss+xml");

// Общие настройки
$title = "Ежедневные экономические новости от ForTrader.org";
$description = "Ежедневные экономические новости";  // Описание

$home_url = get_site_url();
$rss_link = $home_url. "/rss/rss_finam.php";   // Ссылка на ленту новостей
$logo = $home_url. "/images/ftlogo-100.png";       // Логотип
$logoSquare = $home_url. "/images/ftlogo-180.png";
$countnews = 30;      // Сколько новостей выводить
$author = "ForTrader.org";      // Автор

$categories = array(4343,641,5,640,9527,5184,11723,5);      // Список категорий 

echo ("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<rss version=\"2.0\" xmlns:yandex=\"" . $rss_link . "\">
  <channel>
    <title>" . $title . "</title>
      <link>" . $rss_link . "</link>
        <description>" . $description . "</description>
		<yandex:logo>" . $logo . "</yandex:logo>
		<yandex:logo type=\"square\">" . $logoSquare . "</yandex:logo>
");


$r = new WP_Query( array(
	'post_status'  => 'publish',
	'post_type' => 'post',
	'orderby' => 'modified',
	'order'   => 'DESC',
	'posts_per_page' => $countnews,
	'category__in' => $categories,
) );

if ($r->have_posts()){
	while ( $r->have_posts() ){ $r->the_post();

        $chen_title = $post->post_title;
        $chen_link = get_the_permalink();
		
        $text = array_shift(explode('. ', htmlspecialchars(strip_tags(($post->post_content)), 1))) . '.';
		$text=preg_replace('/\[caption.*?\[\/caption\]/si', '', $text);
		
        $vowels = array("&amp;", "nbsp;", "Источник", "\r", "\n");
        $full_text = str_replace($vowels, "", htmlspecialchars(strip_tags($post->post_content)));
        $full_text = str_replace("Журнал трейдеров", "", $full_text);
		$full_text = preg_replace('/\[caption.*?\[\/caption\]/si', '', $full_text);
        $date = date("r", strtotime( $post->post_date ) - 3600*4 );

        echo ("
			<item>
				<title>" . $chen_title . "</title>
				<link>" . $chen_link . "</link>

				<pubDate>$date</pubDate>
				<description>" . $text . "</description>
				<author>Юлия Апель</author>
				<yandex:genre>message</yandex:genre>
				<yandex:full-text>" . $full_text . "</yandex:full-text>
				<guid>" . $chen_link . "</guid>
			</item>
		");
	}
    
}

echo ("
	</channel>
</rss> ");

?>