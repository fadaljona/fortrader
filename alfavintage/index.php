<?php
//header("Content-Type: plain/text");
header("Content-Type: text/plain");
set_time_limit(0);
ini_set('max_execution_time', 0);

require __DIR__ . '/vendor/autoload.php';

define('ALPHA_VANTAGE_KEY', '6587UG4WJF9X2HRU');// ключ для Api (еще тестовый - V3OX7A8FPHYQPP1D)
define('ALPHA_VANTAGE_URL', 'https://www.alphavantage.co'); //- адрес сервера для получения данных


/**
 * Class GlobalQuote - класс запроса Global Quote, отсутствующий в оригинальной либе
 */
class GlobalQuote extends \AlphaVantage\Api\AbstractApi {
    /**
     * @param string $symbol
     * @return array
     */
    public function globalQuoteValues(string $symbol)
    {
        return $this->get(
            'GLOBAL_QUOTE',
            $symbol
        );
    }
}


class AlphaVantageIndexer {
    private $apiUrl = '';
    private $apiKey = '';
    private $options = [];
    private $alphaClient;
    private $alphaOptions;
    private $indexTypes = ['exch', 'quote']; //типы запросов - ExchangeRate или GlobalQuote
    private $sleepMicro = 50000; //задержка перед следующим запросом
    private $debug = 0; //выводить сообщения об ошибках и пр. вещи для отладки
    private $beginTime = 0;

    public function __construct($apiKey, $url) {
        $this->apiKey = $apiKey;
        $this->apiUrl = $url;
        $this->beginTime = new DateTime();
    }

    /**
     * Запуск команды на выполнение
     * @param $command - имя команды (строка, например 'save_index_list')
     * @param $optionsLine - опции, если есть (хеш в виде name1:value1;name2:value2)
     */
    public function runCommand($command, $optionsLine) {
        $this->options = [];
        $optionsLines = explode(';', $optionsLine);
        foreach($optionsLines as $optionsLine_) {
            $optionsLineArr = explode(':',$optionsLine_);
            if(count($optionsLineArr)>1) {
                $this->options[$optionsLineArr[0]] = $optionsLineArr[1];
            }
        }
        switch ($command) {
            case 'save_index_list':
                $this->saveIndexList();
                break;
            case 'export':
                $this->exportIndexes();
                break;
            case 'save_indexes':
                $this->saveIndexes();
                break;
            default:
                $this->home();
        }
    }


    /**
     * Вывод по-умолчанию
     */
    private function home() {
        echo 'Use get-parameter "commmand" to run action:'."\n";
        echo '\'save_index_list\' to create blank index files'."\n";
        echo '\'export\' to show result index list'."\n";
        echo '\'save_indexes\' to download fresh indexes'."\n";
        echo "\n";
        echo 'You can download only one type (\'exch\' or \'quote\') and'.
                ' only one index by adding parameter "options", example:'."\n";
        echo 'Command=\'save_indexes\', options=\'type:exch;name:BTCUSD\''."\n";
    }


    /**
     * Первичная настройка приложения. Массив $indexList превратится в директории и файлы.
     * Файлы будут в последствии заполняться данными. При запуске содержимое существующих файлов обнулится.
     */
    private function saveIndexList() {
        $indexList = [
            'exch'=> [
                'AUDCAD', 'AUDCHF', 'AUDJPY', 'AUDNZD', 'AUDUSD',
                'BCHBTC', 'BCHUSD', 'BTCUSD', 'BTGBTC', 'BTGETH',
                'BTGUSD', 'CADCHF', 'CADJPY', 'CHFJPY', 'CHFSGD',
                'DSHBTC', 'DSHETH', 'DSHUSD', 'EOSETH', 'ETCETH',
                'ETCUSD', 'ETHBTC', 'ETHUSD', 'EURAUD', 'EURCAD',
                'EURCHF', 'EURCNH', 'EURGBP', 'EURJPY', 'EURMXN',
                'EURNOK', 'EURNZD', 'EURRUB', 'EURSEK', 'EURSGD',
                'EURTRY', 'EURUSD', 'GBPAUD', 'GBPCAD', 'GBPCHF',
                'GBPJPY', 'GBPNZD', 'GBPSEK', 'GBPUSD', 'IOTETH',
                'IOTUSD', 'LTCBTC', 'LTCETH', 'LTCUSD', 'NEOBTC',
                'NEOETH', 'NEOUSD', 'NZDCAD', 'NZDCHF', 'NZDJPY',
                'NZDUSD', 'OMGETH', 'OMGUSD', 'USDCAD', 'USDCHF',
                'USDCLP', 'USDCNH', 'USDDKK', 'USDHKD', 'USDILS',
                'USDJPY', 'USDMXN', 'USDNOK', 'USDRUB', 'USDSAR',
                'USDSEK', 'USDSGD', 'USDTRY', 'USDZAR', 'XAGUSD',
                'XAUUSD', 'XMRBTC', 'XMRETH', 'XMRUSD', 'XRPUSD',
                'ZECBTC', 'ZECETH', 'ZECUSD',

            ],
            'quote'=> [
                'AA',  'LNVG',  'BRN',  'NORNICKEL',  'DBK',  'QTMUSD',  'FB',  'T',  'HPQ',  'VFC',
                'AAPL',  'LUKOIL',  'BSAC',  'NOVATEK',  'DIS',  'RACE',  'FCE',  'TA25',  'HSI',  'VGK',
                'ADBE',  'MA',  'C',  'NQ',  'DROPBOX',  'RL',  'FDAX',  'TCTZ',  'IBM',  'VOD',
                'ADS',  'MCD',  'CAT',  'NVDA',  'EBAY',  'ROSNEFT',  'FESX',  'TF',  'IBX',  'VOW',
                'AEROFLOT',  'MIB',  'CELG',  'ORCL',  'EL',  'RUSSIA50',  'FTI',  'TIF',  'IDCB',  'VZ',
                'AGG',  'MSFT',  'CHILE',  'PA',  'ENI',  'SBERBANK',  'FXI',  'TM',  'IJH',  'WFC',
                'AMZN',  'MTS',  'CHL',  'PBR',  'EOC',  'SBUX',  'GAZPROM',  'TRIP',  'ILF',  'WHEAT',
                'AXP',  'NETFLIX',  'CL',  'PFE',  'ES',  'SGDJPY',  'GE',  'TRV',  'INTC',  'WSM',
                'BA',  'NG',  'COCOA',  'PG',  'ETH',  'SNAP',  'GOOGL',  'TSLA',  'ITX',  'WT',
                'BABA',  'NINTENDO_JP',  'COFFEE',  'PL',  'EWG',  'SOYBEAN',  'GS',  'TWTR',  'JNJ',  'XOM',
                'BAC',  'NINTENDO_US',  'CORN',  'PM',  'EWU',  'SPOTIFY',  'HD',  'UNH',  'JPM',  'XU',
                'BAYN',  'NKD',  'CRM',  'PTR',  'EWW',  'SPY',  'HG',  'USDX',  'JWN',  'YM',
                'BIDU',  'NKE',  'CSCO',  'PVH',  'EWZ',  'SQM',  'HO',  'V',  'KO',  'YNDX',
                'BMW',  'NOKJPY',  'DAI',  'QTMETH',  'F',  'SUGAR',  'HOG',  'VALE',  'KORS',  'Z',
            ],
        ];
        if(!file_exists(__DIR__.'/indexes')) {
            @mkdir('./indexes', 0775);
        }
        $paths = [];
        if(file_exists(__DIR__.'/indexes')) {
            foreach($this->indexTypes as $subdir) {
                $subPath = __DIR__.'/indexes/'.$subdir;
                $paths[] = $subPath;
                if(!file_exists($subPath)) {
                    @mkdir($subPath, 0775);
                }
            }
        }
        foreach($paths as $path) {
            if(!file_exists($path) or !is_writable($path)) {
                echo 'Error! Don\'t have permission to write indexes to /indexes';
                return;
            }
        }
        foreach($indexList as $type=>$cont) {
            foreach($cont as $indName) {
                file_put_contents(__DIR__.'/indexes/'.$type.'/'.$indName, '');
            }
        }
        echo 'Operation is completed';
    }


    /**
     * Загрузка содержимого индексов из alphavantage
     */
    private function saveIndexes() {
        $this->alphaOptions = new \AlphaVantage\Options();
        $this->alphaOptions->setApiKey($this->apiKey);
        $this->alphaOptions->setApiUrl($this->apiUrl);
        $this->alphaClient = new \AlphaVantage\Client($this->alphaOptions);

        if(isset($this->options['type'])) {
            if(isset($this->options['name'])) {
                $this->downloadIndex($this->options['type'], $this->options['name']);
            }else {
                $this->downloadIndexes($this->options['type']);
            }
        }else{
            foreach($this->indexTypes as $type) {
                $this->downloadIndexes($type);
            }
        }
        $endTime = new DateTime();
        $diff = $endTime->diff($this->beginTime);

        echo 'Operation is completed ('.$diff->format('%H:%i:%s').')';

    }


    /**
     * Загрузка конкретного индекса
     * @param $type - тип индекса
     * @param $fileName - имя индекса
     */
    private function downloadIndex($type, $fileName) {
        $forWrite = '';
        try {
            if ($type == 'exch') {
                $from = substr($fileName, 0, 3);
                $to = substr($fileName, 3);
                $res = $this->alphaClient->foreignexchange()->currencyExchangeRate($from, $to);
                if(!empty($res['Realtime Currency Exchange Rate']['5. Exchange Rate']) and
                        !empty($res['Realtime Currency Exchange Rate']['6. Last Refreshed'])) {
                    $forWrite = json_encode([
                        'value' => $res['Realtime Currency Exchange Rate']['5. Exchange Rate'],
                        'time' => $res['Realtime Currency Exchange Rate']['6. Last Refreshed']
                    ]);
                }
            } else {
                $alphaClient = new GlobalQuote($this->alphaOptions);
                $res = $alphaClient->globalQuoteValues($fileName);
                if(!empty($res['Global Quote']['05. price']) and
                        !empty($res['Global Quote']['07. latest trading day'])) {
                    $forWrite = json_encode([
                        'value' => $res['Global Quote']['05. price'],
                        'time' => $res['Global Quote']['07. latest trading day']
                    ]);
                }
            }
            if(!$forWrite and $this->debug){
                print_r([$fileName => $res]);
            }
        }catch (Exception $e) {
            if($this->debug) {
                echo "error during $type $fileName \n";
                //print_r($e);
            }
        }
        file_put_contents(__DIR__.'/indexes/'.$type.'/'.$fileName, $forWrite);
        $dt = new DateTime();
        if($this->debug) {
            print_r([$fileName=>$forWrite]);
            echo "\t\t" . $dt->format('d.m.Y H:i:s') . "\n";
        }
        usleep($this->sleepMicro);
    }


    /**
     * Загрузка из alphavantage всех индексов одного типа
     * @param $type - тип индекса
     */
    private function downloadIndexes($type) {
        //$res = $this->alphaClient->foreignexchange()->currencyExchangeRate('BTC', 'CNY');

        $iterator = new DirectoryIterator(__DIR__.'/indexes/'.$type);
        while($iterator->valid()) {
            $file = $iterator->current();
            $fileName = $file->getFilename();
            if(in_array($fileName, ['.', '..'])) { $iterator->next(); continue; };
            $this->downloadIndex($type, $fileName);
            $iterator->next();
        }

    }


    /**
     * Вывод значений индексов в csv-формате
     */
    private function exportIndexes() {
        //$lines = [];
        foreach($this->indexTypes as $type) {
            $iterator = new DirectoryIterator(__DIR__ . '/indexes/' . $type);
            while ($iterator->valid()) {
                $file = $iterator->current();
                $fileName = $file->getFilename();
                if (in_array($fileName, ['.', '..'])) {
                    $iterator->next();
                    continue;
                };
                $cont = file_get_contents(__DIR__ . '/indexes/' . $type . '/' . $fileName);
                if($cont) {
                    $values = json_decode($cont);
                    $time = '00:00:00';
                    $times = explode(' ',$values->time);
                    if(count($times)>1) {
                      $time = $times[1];
                    }
                    //$lines[] =
                    echo $fileName . ';' . $values->value . ';' . $values->value . ';' . $time . ';0;0;0;0;0;0;'."\n";
                }
                $iterator->next();
            }
        }
    }

}


$indexer = new AlphaVantageIndexer(ALPHA_VANTAGE_KEY, ALPHA_VANTAGE_URL);

$command = filter_input(INPUT_GET, 'command');
$options = filter_input(INPUT_GET, 'options');
if(!empty($argv[1])) {
    $command = $argv[1];
}
if(!empty($argv[2])) {
    $options = $argv[2];
}
$indexer->runCommand($command, $options);
